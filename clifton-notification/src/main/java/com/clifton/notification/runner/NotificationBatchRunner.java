package com.clifton.notification.runner;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.generator.NotificationGeneratorService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareObserver;

import java.util.Date;


/**
 * The <code>NotificationRunner</code> class is run based on it configured schedule see {@link NotificationBatchRunnerProvider}.
 * <p>
 * When the run method is called, a {@link NotificationDefinitionBatch} bean is obtained and the notification generator for that definition bean is executed.
 *
 * @author Mary Anderson
 */
public class NotificationBatchRunner<N extends NotificationDefinition> extends AbstractStatusAwareRunner {

	private final int notificationDefinitionId;
	private final boolean runIfInactive;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ContextHandler contextHandler;

	private NotificationDefinitionService<N> notificationDefinitionService;
	private NotificationGeneratorService<?> notificationGeneratorService;
	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public NotificationBatchRunner(Date runDate, int notificationDefinitionId) {
		this(runDate, notificationDefinitionId, false);
	}


	public NotificationBatchRunner(Date runDate, int notificationDefinitionId, boolean runIfInactive) {
		super("NOTIFICATION", Integer.toString(notificationDefinitionId), runDate);
		this.notificationDefinitionId = notificationDefinitionId;
		getStatus().setMessage("Scheduled for " + DateUtils.fromDate(runDate));
		this.runIfInactive = runIfInactive;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void run() {
		SecurityUser runAsUser = getRunAsUser();
		if (runAsUser == null) {
			LogUtils.error(getClass(), "Cannot run notification batch job.  No current user is set, and the default user [" + SecurityUser.SYSTEM_USER + "] is missing.");
			return;
		}

		// save the current user
		Object currentUser = getContextHandler().getBean(Context.USER_BEAN_NAME);
		try {
			// set the Run As User as current user
			getContextHandler().setBean(Context.USER_BEAN_NAME, runAsUser);

			getStatus().setMessage("Started on " + DateUtils.fromDate(new Date()));

			NotificationDefinitionBatch batchDefinition = (NotificationDefinitionBatch) getNotificationDefinitionService().getNotificationDefinition(this.notificationDefinitionId);

			// generation of batch notifications updates NotificationDefinition.status: disable the corresponding entity modify condition observer (require only EXECUTE permission)
			DaoUtils.executeWithSpecificObserversDisabled(() -> getNotificationGeneratorService().generateBatchedNotification(batchDefinition, this.runIfInactive, getRunDate()), SystemEntityModifyConditionAwareObserver.class);

			getStatus().setMessage("Finished on " + DateUtils.fromDate(new Date()));
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error generating notification(s) for notification definition [" + this.notificationDefinitionId + "]: " + e.getMessage(), e);
		}
		finally {
			// restore the current user
			if (currentUser == null) {
				getContextHandler().removeBean(Context.USER_BEAN_NAME);
			}
			else {
				getContextHandler().setBean(Context.USER_BEAN_NAME, currentUser);
			}
		}
	}


	/**
	 * Returns the run as user specified for the batch job or the default run as user if one is not set.
	 */
	private SecurityUser getRunAsUser() {
		SecurityUser result = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
		if (result == null) {
			result = getSecurityUserService().getSecurityUserByName(SecurityUser.SYSTEM_USER);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public NotificationGeneratorService<?> getNotificationGeneratorService() {
		return this.notificationGeneratorService;
	}


	public void setNotificationGeneratorService(NotificationGeneratorService<?> notificationGeneratorService) {
		this.notificationGeneratorService = notificationGeneratorService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public NotificationDefinitionService<N> getNotificationDefinitionService() {
		return this.notificationDefinitionService;
	}


	public void setNotificationDefinitionService(NotificationDefinitionService<N> notificationDefinitionService) {
		this.notificationDefinitionService = notificationDefinitionService;
	}
}
