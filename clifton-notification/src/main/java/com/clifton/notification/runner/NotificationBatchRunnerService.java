package com.clifton.notification.runner;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>NotificationBatchRunnerService</code> ...
 *
 * @author Mary Anderson
 */
public interface NotificationBatchRunnerService {

	/**
	 * Schedules NotificationDefinitionBatch with the specified id to run immediately.
	 * The call is asynchronous and the job will be executed in a different thread.
	 */
	@SecureMethod(dtoClass = NotificationDefinition.class)
	public void runNotificationBatch(int id);


	/**
	 * Schedules all active NotificationDefinitionBatch definitions to run now for
	 * a specified type
	 */
	@RequestMapping("notificationBatchRunForType")
	@SecureMethod(dtoClass = NotificationDefinition.class)
	public void runNotificationBatchByType(short typeId);


	/**
	 * Allows for the use of a command to configure how NotificatinDefinitionBatch definitions are run.  The
	 * command primarily allows for the configuration of asynchronous or synchronous execution modes.
	 */
	@SecureMethod(dtoClass = NotificationDefinition.class)
	public void runNotificationBatchByCommand(NotificationBatchRunnerCommand command);


	/**
	 * Returns a list of NotificationDefinitionBatch objects that are in invalid state: RUNNING status but there's nothing running.
	 * This could happen if the server is killed while the job is running.
	 * <p/>
	 * Job status must be updated so that the job can start again.
	 */
	public List<NotificationDefinitionBatch> getNotificationDefinitionBatchListInvalid();


	/**
	 * Fixes invalid notification definition batch (see getNotificationDefinitionBatchListInvalid) by setting its status to FAILED.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_WRITE)
	public void fixNotificationDefinitionBatch(int id);
}
