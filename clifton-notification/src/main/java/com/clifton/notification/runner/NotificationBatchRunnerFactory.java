package com.clifton.notification.runner;


import com.clifton.notification.definition.NotificationDefinition;

import java.util.Date;


/**
 * The <code>NotificationBatchRunnerFactory</code> interface defines methods for creating NotificationBatchRunner instances.
 *
 * @author Mary Anderson
 */
public interface NotificationBatchRunnerFactory<N extends NotificationDefinition> {

	/**
	 * Create a NotificationBatchRunner
	 *
	 * @param runDate
	 * @param definitionId
	 */
	public NotificationBatchRunner<N> createNotificationBatchRunner(Date runDate, int definitionId, boolean runIfInactive);
}
