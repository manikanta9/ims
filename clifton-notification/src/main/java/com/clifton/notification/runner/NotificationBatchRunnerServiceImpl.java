package com.clifton.notification.runner;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.NotificationHistory;
import com.clifton.notification.definition.NotificationStatuses;
import com.clifton.notification.definition.search.NotificationDefinitionSearchForm;
import com.clifton.notification.definition.search.NotificationHistorySearchForm;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class NotificationBatchRunnerServiceImpl<T extends NotificationDefinition> implements NotificationBatchRunnerService {

	private NotificationDefinitionService<T> notificationDefinitionService;

	private NotificationBatchRunnerFactory<T> notificationBatchRunnerFactory;

	private RunnerHandler runnerHandler;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	private SecurityAuthorizationService securityAuthorizationService;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public void runNotificationBatch(int id) {
		NotificationBatchRunnerCommand command = new NotificationBatchRunnerCommand();
		command.setNotificationDefinitionId(id);
		command.setRunIfInactive(true);
		runNotificationBatchByCommand(command);
	}


	@Override
	public void runNotificationBatchByType(short typeId) {
		List<NotificationDefinitionBatch> list = getNotificationDefinitionService().getNotificationDefinitionBatchListForTypeActiveOnDate(typeId, new Date());
		for (NotificationDefinitionBatch b : CollectionUtils.getIterable(list)) {
			NotificationBatchRunnerCommand command = new NotificationBatchRunnerCommand();
			command.setNotificationDefinitionId(b.getId());
			runNotificationBatchByCommand(command);
		}
	}


	@Override
	public void runNotificationBatchByCommand(NotificationBatchRunnerCommand command) {
		ValidationUtils.assertNotNull(command, "An instance of a NotificationBatchRunnerCommand is required.");
		validateNotificationIsRunnable(command.getNotificationDefinitionId());
		NotificationBatchRunner<T> runner = getNotificationBatchRunnerFactory().createNotificationBatchRunner(new Date(), command.getNotificationDefinitionId(), command.isRunIfInactive());
		if (command.isSynchronous()) {
			runner.run();
		}
		else {
			getRunnerHandler().runNow(runner);
		}
	}


	@Override
	public List<NotificationDefinitionBatch> getNotificationDefinitionBatchListInvalid() {
		List<NotificationDefinitionBatch> invalidList = null;

		// 1. get all running batch jobs
		NotificationDefinitionSearchForm searchForm = new NotificationDefinitionSearchForm();
		searchForm.setStatus(NotificationStatuses.RUNNING);
		List<NotificationDefinition> runningList = getNotificationDefinitionService().getNotificationDefinitionGenericList(searchForm);
		runningList = BeanUtils.filter(runningList, notificationDefinition -> !notificationDefinition.isInstant());

		if (CollectionUtils.getSize(runningList) > 0) {
			for (NotificationDefinition job : runningList) {
				if (!isNotificationDefinitionRunning(job)) {
					// SHOULD NEVER HAVE A NOTIFICATION IN "RUNNING" STATUS THAT IS NOT ACTUALLY RUNNING
					// this could happen if the server is killed while the job is running
					if (invalidList == null) {
						invalidList = new ArrayList<>();
					}
					invalidList.add((NotificationDefinitionBatch) job);
				}
			}
		}

		return invalidList;
	}


	@Override
	@Transactional
	public void fixNotificationDefinitionBatch(int id) {
		// validate current notification state to make sure we're not fixing a good job
		NotificationDefinitionBatch def = (NotificationDefinitionBatch) getNotificationDefinitionService().getNotificationDefinition(id);
		ValidationUtils.assertNotNull(def, "Cannot find notification definition with id = " + id);
		ValidationUtils.assertTrue(NotificationStatuses.RUNNING == def.getStatus(), "Cannot fix notification definition that is not in RUNNING status: " + def.getStatus());
		ValidationUtils.assertFalse(isNotificationDefinitionRunning(def), "Cannot fix the notification with id = " + id + " because it maybe currently running.");

		// set job to FAILED and update history
		def.setStatus(NotificationStatuses.FAILED);
		getNotificationDefinitionService().saveNotificationDefinitionBatch(def);

		// find last history
		NotificationHistorySearchForm searchForm = new NotificationHistorySearchForm();
		searchForm.setNotificationDefinitionId(id);
		searchForm.setLimit(1);
		searchForm.setOrderBy("id:desc");
		List<NotificationHistory> historyList = getNotificationDefinitionService().getNotificationHistoryList(searchForm);
		NotificationHistory history = CollectionUtils.getFirstElement(historyList);
		if (history != null && NotificationStatuses.RUNNING == history.getStatus()) {
			history.setStatus(NotificationStatuses.FAILED);
			history.setEndDate(new Date());
			history.setDescription("Fixed notification in RUNNING status that was not running (server restart?)");
			getNotificationDefinitionService().saveNotificationHistory(history);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Runnable Validation                             ////////
	////////////////////////////////////////////////////////////////////////////


	//Validate run condition if one exists
	private void validateNotificationIsRunnable(int id) {
		if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
			NotificationDefinition notificationDefinition = getNotificationDefinitionService().getNotificationDefinition(id);
			SystemCondition systemCondition = notificationDefinition.getType().getRunSystemCondition();
			//Validate the run condition on the definition
			if (systemCondition != null) {
				String runnableResult = getSystemConditionEvaluationHandler().getConditionFalseMessage(systemCondition, notificationDefinition);
				if (StringUtils.isEmpty(runnableResult)) {
					throw new ValidationException("Notification Run Condition did not evaluate to True: " + runnableResult);
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	private boolean isNotificationDefinitionRunning(NotificationDefinition definition) {
		return getRunnerHandler().isRunnerWithTypeAndIdActive("NOTIFICATION", Integer.toString(definition.getId()));
	}

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public NotificationDefinitionService<T> getNotificationDefinitionService() {
		return this.notificationDefinitionService;
	}


	public void setNotificationDefinitionService(NotificationDefinitionService<T> notificationDefinitionService) {
		this.notificationDefinitionService = notificationDefinitionService;
	}


	public NotificationBatchRunnerFactory<T> getNotificationBatchRunnerFactory() {
		return this.notificationBatchRunnerFactory;
	}


	public void setNotificationBatchRunnerFactory(NotificationBatchRunnerFactory<T> notificationBatchRunnerFactory) {
		this.notificationBatchRunnerFactory = notificationBatchRunnerFactory;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
