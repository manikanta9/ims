package com.clifton.notification.definition.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.search.NotificationDefinitionSearchForm;
import com.clifton.notification.generator.Notification;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>NotificationDefinitionCountCacheImpl</code> caches for each notification definition the number of notifications associated with it
 * <p>
 * When the count changes from/to 0, there is a second cache key that stores the notification definitions that have no notifications that is updated
 *
 * @author manderson
 */
@Component
public class NotificationDefinitionCountCacheImpl<N extends NotificationDefinition> extends SelfRegisteringSimpleDaoCache<Notification, String, Object> implements NotificationDefinitionCountCache {

	private NotificationDefinitionService<N> notificationDefinitionService;
	private ReadOnlyDAO<Notification> notificationDAO;

	private static final String NOTIFICATION_DEFINITION_EMPTY_CACHE_KEY = "NotificationDefinitionEmpty";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public Set<Integer> getNotificationDefinitionEmptySet() {
		Object result = getCacheHandler().get(getCacheName(), NOTIFICATION_DEFINITION_EMPTY_CACHE_KEY);
		if (result == null) {
			setupNotificationDefinitionEmptySet();
			result = getCacheHandler().get(getCacheName(), NOTIFICATION_DEFINITION_EMPTY_CACHE_KEY);
		}
		return (Set<Integer>) result;
	}

	////////////////////////////////////////////////////////////////////////////////


	private synchronized void setupNotificationDefinitionEmptySet() {
		getCacheHandler().clear(getCacheName());

		List<NotificationDefinition> definitionList = getNotificationDefinitionService().getNotificationDefinitionGenericList(new NotificationDefinitionSearchForm());
		List<Notification> allNotificationList = getNotificationDAO().findAll();

		Map<Integer, Integer> countMap = new HashMap<>();
		for (NotificationDefinition def : CollectionUtils.getIterable(definitionList)) {
			countMap.put(def.getId(), 0);
		}
		for (Notification notification : CollectionUtils.getIterable(allNotificationList)) {
			Integer defId = notification.getDefinition().getId();
			countMap.put(defId, countMap.get(defId) + 1);
		}

		Set<Integer> emptySet = CollectionUtils.newConcurrentHashSet();
		for (Map.Entry<Integer, Integer> integerIntegerEntry : countMap.entrySet()) {
			getCacheHandler().put(getCacheName(), integerIntegerEntry.getKey() + "", integerIntegerEntry.getValue());
			if (integerIntegerEntry.getValue() == 0) {
				emptySet.add(integerIntegerEntry.getKey());
			}
		}

		getCacheHandler().put(getCacheName(), NOTIFICATION_DEFINITION_EMPTY_CACHE_KEY, emptySet);
	}


	private synchronized void updateNotificationDefinitionCache(int definitionId, boolean add) {
		Integer count = (Integer) getCacheHandler().get(getCacheName(), Integer.toString(definitionId));
		// Note: Count would be null for new Definitions, so return 0 and add when new notifications are added to that definition
		if (count == null) {
			count = 0;
		}

		boolean updateEmptySet = false;
		Set<Integer> emptySet = getNotificationDefinitionEmptySet();
		if (count == 0 && add) {
			emptySet.remove(definitionId);
			updateEmptySet = true;
		}
		else if (count == 1 && !add) {
			emptySet.add(definitionId);
			updateEmptySet = true;
		}
		count = count + (add ? 1 : -1);
		getCacheHandler().put(getCacheName(), Integer.toString(definitionId), count);
		if (updateEmptySet) {
			getCacheHandler().put(getCacheName(), NOTIFICATION_DEFINITION_EMPTY_CACHE_KEY, emptySet);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////              Notification DAO Observer Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.INSERT_OR_DELETE;
	}


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<Notification> dao, DaoEventTypes event, Notification bean, Throwable e) {
		if (e == null) {
			if (event.isInsert() || event.isDelete()) {
				updateNotificationDefinitionCache(bean.getDefinition().getId(), event.isInsert());
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public NotificationDefinitionService<N> getNotificationDefinitionService() {
		return this.notificationDefinitionService;
	}


	public void setNotificationDefinitionService(NotificationDefinitionService<N> notificationDefinitionService) {
		this.notificationDefinitionService = notificationDefinitionService;
	}


	public ReadOnlyDAO<Notification> getNotificationDAO() {
		return this.notificationDAO;
	}


	public void setNotificationDAO(ReadOnlyDAO<Notification> notificationDAO) {
		this.notificationDAO = notificationDAO;
	}
}
