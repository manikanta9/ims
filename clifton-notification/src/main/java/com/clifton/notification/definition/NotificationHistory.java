package com.clifton.notification.definition;


import com.clifton.core.beans.hierarchy.HierarchicalSimpleEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityUser;

import java.util.Date;


/**
 * The <code>NotificationDefinitionBatchHistory</code> defines a scheduled instance of a batch notification definition.
 *
 * @author manderson
 */
public class NotificationHistory extends HierarchicalSimpleEntity<NotificationHistory, Integer> {

	private NotificationDefinition notificationDefinition;
	private Date scheduledDate;
	private Date startDate;
	private Date endDate;
	private NotificationStatuses status;
	private String description;
	private SecurityUser runnerUser;


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(50);
		result.append("{id=");
		result.append(getId());
		result.append(", ");
		result.append(getStatus());
		result.append(", ");
		result.append(getDescription());
		result.append('}');
		return result.toString();
	}


	public String getExecutionTimeFormatted() {
		if (getStartDate() == null) {
			return null;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getTimeDifferenceShort(toDate, getStartDate());
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public NotificationDefinition getNotificationDefinition() {
		return this.notificationDefinition;
	}


	public void setNotificationDefinition(NotificationDefinition notificationDefinition) {
		this.notificationDefinition = notificationDefinition;
	}


	public void setStatus(NotificationStatuses status) {
		this.status = status;
	}


	public NotificationStatuses getStatus() {
		return this.status;
	}


	public SecurityUser getRunnerUser() {
		return this.runnerUser;
	}


	public void setRunnerUser(SecurityUser runnerUser) {
		this.runnerUser = runnerUser;
	}
}
