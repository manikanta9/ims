package com.clifton.notification.definition.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class NotificationTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField(searchField = "notificationModifyCondition.id")
	private Integer notificationModifyConditionId;

	@SearchField(searchField = "definitionModifyCondition.id")
	private Integer definitionModifyConditionId;

	@SearchField(searchField = "runSystemCondition.id")
	private Integer runSystemConditionId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Integer getNotificationModifyConditionId() {
		return this.notificationModifyConditionId;
	}


	public void setNotificationModifyConditionId(Integer notificationModifyConditionId) {
		this.notificationModifyConditionId = notificationModifyConditionId;
	}


	public Integer getDefinitionModifyConditionId() {
		return this.definitionModifyConditionId;
	}


	public void setDefinitionModifyConditionId(Integer definitionModifyConditionId) {
		this.definitionModifyConditionId = definitionModifyConditionId;
	}


	public Integer getRunSystemConditionId() {
		return this.runSystemConditionId;
	}


	public void setRunSystemConditionId(Integer runSystemConditionId) {
		this.runSystemConditionId = runSystemConditionId;
	}
}
