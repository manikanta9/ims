package com.clifton.notification.definition;


import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.runner.ScheduleAware;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.schema.SystemTable;

import javax.persistence.Table;


/**
 * The <code>NotificationDefinitionBatch</code> provides the definition of a batch notification.
 * <p/>
 * The {@link SystemTable} field is used to determine which {@link SystemQuery}s are available to select
 * from.
 * <p/>
 * The {@link CalendarSchedule} determines when the batched notification should be run.
 *
 * @author manderson
 */
@Table(name = "NotificationDefinition")
public class NotificationDefinitionBatch extends NotificationDefinition implements ScheduleAware<Integer> {

	private CalendarSchedule calendarSchedule;
	private SystemQuery systemQuery;
	private SystemBean generatorSystemBean;

	private NotificationStatuses status;

	/**
	 * If batchResults is true it will batch all of the results into one notification,
	 * otherwise will create a different notification for each result from the query.
	 */
	private boolean batchResults;

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	@Override
	public boolean isInstant() {
		return false;
	}


	@Override
	public String getTemplateConfigBeanName() {
		if (isBatchResults()) {
			return "dataTable";
		}
		return "row";
	}


	@Override
	public boolean isEnabled() {
		return isActive();
	}


	@Override
	public CalendarSchedule getSchedule() {
		return getCalendarSchedule();
	}
	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	public CalendarSchedule getCalendarSchedule() {
		return this.calendarSchedule;
	}


	public void setCalendarSchedule(CalendarSchedule calendarSchedule) {
		this.calendarSchedule = calendarSchedule;
	}


	public SystemQuery getSystemQuery() {
		return this.systemQuery;
	}


	public void setSystemQuery(SystemQuery systemQuery) {
		this.systemQuery = systemQuery;
	}


	public boolean isBatchResults() {
		return this.batchResults;
	}


	public void setBatchResults(boolean batchResults) {
		this.batchResults = batchResults;
	}


	public NotificationStatuses getStatus() {
		return this.status;
	}


	public void setStatus(NotificationStatuses status) {
		this.status = status;
	}


	public SystemBean getGeneratorSystemBean() {
		return this.generatorSystemBean;
	}


	public void setGeneratorSystemBean(SystemBean generatorSystemBean) {
		this.generatorSystemBean = generatorSystemBean;
	}
}
