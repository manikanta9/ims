package com.clifton.notification.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>NotificationType</code> class is used to group notifications
 * <p/>
 * e.g. Data Integrity Checks, Status change, etc.
 *
 * @author manderson
 */
public class NotificationType extends NamedEntity<Short> {

	private SystemCondition notificationModifyCondition;
	private SystemCondition definitionModifyCondition;
	private SystemCondition runSystemCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getNotificationModifyCondition() {
		return this.notificationModifyCondition;
	}


	public void setNotificationModifyCondition(SystemCondition notificationModifyCondition) {
		this.notificationModifyCondition = notificationModifyCondition;
	}


	public SystemCondition getDefinitionModifyCondition() {
		return this.definitionModifyCondition;
	}


	public void setDefinitionModifyCondition(SystemCondition definitionModifyCondition) {
		this.definitionModifyCondition = definitionModifyCondition;
	}


	public SystemCondition getRunSystemCondition() {
		return this.runSystemCondition;
	}


	public void setRunSystemCondition(SystemCondition runSystemCondition) {
		this.runSystemCondition = runSystemCondition;
	}
}
