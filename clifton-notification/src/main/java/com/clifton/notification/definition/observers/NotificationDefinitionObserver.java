package com.clifton.notification.definition.observers;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.runner.RunnerUtils;
import com.clifton.notification.NotificationRegistrator;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.notification.generator.NotificationGeneratorObserver;
import com.clifton.notification.runner.NotificationBatchRunnerProvider;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Component;


/**
 * The <code>NotificationDefinitionObserver</code> class is a {@link DaoEventObserver} that registers/unregisters
 * the {@link NotificationGeneratorObserver} observers to DAOs based upon
 * changes to {@link NotificationDefinition} {@link SystemTable}
 * <p/>
 * For Batch notificationDefinitions, will also handle any rescheduling.
 * A reschedule is deemed necessary during NotificationDefinitionBatch
 * Inserts and Deletes (only if the definition is active)
 * Updates to the following: Active and/or Schedule
 *
 * @author manderson
 */
@Component
public class NotificationDefinitionObserver extends SelfRegisteringDaoObserver<NotificationDefinition> {

	public NotificationRegistrator notificationRegistrator;

	private NotificationBatchRunnerProvider notificationBatchRunnerProvider;
	private RunnerHandler runnerHandler;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	@SuppressWarnings("unused")
	public void beforeMethodCallImpl(ReadOnlyDAO<NotificationDefinition> dao, DaoEventTypes event, NotificationDefinition bean) {
		// NOTHING
	}


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<NotificationDefinition> dao, DaoEventTypes event, NotificationDefinition bean, Throwable e) {
		// Instant Notification Post Processing
		if (bean.isInstant()) {
			handleNotificationDefinitionInstant((NotificationDefinitionInstant) bean, dao, event);
		}
		// Batch Notification Post Processing		
		else {
			handleNotificationDefinitionBatch((NotificationDefinitionBatch) bean, dao, event);
		}
	}


	/**
	 * Handles registering/un-registering observers based upon NotificationDefinitionInstant properties
	 *
	 * @param instantBean
	 * @param dao
	 * @param event
	 */
	private void handleNotificationDefinitionInstant(NotificationDefinitionInstant instantBean, ReadOnlyDAO<NotificationDefinition> dao, DaoEventTypes event) {
		// Register Observer for Inserts
		if (event.isInsert() && instantBean.getTable() != null) {
			getNotificationRegistrator().registerObservers(instantBean.getTable().getName(), instantBean);
			return;
		}
		// Unregister Observers for Deletes
		if (event.isDelete() && instantBean.getTable() != null) {
			getNotificationRegistrator().unregisterObservers(instantBean.getTable().getName(), instantBean);
			return;
		}

		NotificationDefinitionInstant original = (NotificationDefinitionInstant) getOriginalBean(dao, instantBean);
		if (event.isUpdate()) {
			// Unregister Original Definition Observers
			getNotificationRegistrator().unregisterObservers(original.getTable().getName(), original);
			if (instantBean.isActive()) {
				// Register New Definition Observers
				getNotificationRegistrator().registerObservers(instantBean.getTable().getName(), instantBean);
			}
		}
	}


	/**
	 * Handles scheduling/rescheduling notification batch runners based upon NotificationDefinitionBatch properties
	 */
	private void handleNotificationDefinitionBatch(NotificationDefinitionBatch batchBean, ReadOnlyDAO<NotificationDefinition> dao, DaoEventTypes event) {
		// If job is changed to active, or schedule changes, reschedule 
		if (isRescheduleNecessary(dao, event, batchBean)) {
			RunnerUtils.rescheduleRunnersForEntity(batchBean, s -> s.isEnabled() && s.getSchedule() != null, Integer.toString(batchBean.getId()), getNotificationBatchRunnerProvider(), getRunnerHandler());
		}
	}


	private boolean isRescheduleNecessary(ReadOnlyDAO<NotificationDefinition> dao, DaoEventTypes event, NotificationDefinitionBatch bean) {
		// Reschedule for Inserts/Deletes only if the definition is enabled.
		if (event.isInsert() || event.isDelete()) {
			return bean.isActive();
		}

		// Updates
		NotificationDefinitionBatch original = (NotificationDefinitionBatch) getOriginalBean(dao, bean);

		// Enable/Disable
		if (original.isActive() != bean.isActive()) {
			return true;
		}

		// Schedule Change
		if (original.getCalendarSchedule() == null) {
			if (bean.getCalendarSchedule() != null) {
				return true;
			}
			return false;
		}
		if (bean.getCalendarSchedule() == null) {
			return true;
		}
		return !original.getCalendarSchedule().equals(bean.getCalendarSchedule());
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public NotificationRegistrator getNotificationRegistrator() {
		return this.notificationRegistrator;
	}


	public void setNotificationRegistrator(NotificationRegistrator notificationRegistrator) {
		this.notificationRegistrator = notificationRegistrator;
	}


	public NotificationBatchRunnerProvider getNotificationBatchRunnerProvider() {
		return this.notificationBatchRunnerProvider;
	}


	public void setNotificationBatchRunnerProvider(NotificationBatchRunnerProvider notificationBatchRunnerProvider) {
		this.notificationBatchRunnerProvider = notificationBatchRunnerProvider;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
