package com.clifton.notification.definition;


import com.clifton.core.beans.BaseEntity;


/**
 * The <code>NotificationDefinitionLink</code> class allows assigning {@link NotificationDefinition}s
 * to specific entities.
 * <p/>
 * For example, NotificationDefinitions that track changes to contracts, would be linked to Clients.
 * This class allows linking definitions to specific clients.
 *
 * @author manderson
 */
public class NotificationDefinitionLink extends BaseEntity<Integer> {

	private NotificationDefinition definition;
	private int fkFieldId;


	public NotificationDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(NotificationDefinition definition) {
		this.definition = definition;
	}


	public int getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(int fkFieldId) {
		this.fkFieldId = fkFieldId;
	}
}
