package com.clifton.notification.definition.cache;

import java.util.List;


/**
 * The <code>NotificationDefinitionRecipientUserCache</code> caches a map of UserID to the list
 * of notification definition ids that user is a recipient of. Used for "My Notifications" lookup
 * <p>
 * NOTE: This cache is registered in spring xml and observers SecurityUserGroup changes and well as NotificationRecipient changes
 *
 * @author manderson
 */
public interface NotificationDefinitionRecipientUserCache {

	public List<Integer> getNotificationDefinitionIdsForRecipientUser(short securityUserId);


	public void setNotificationDefinitionIdsForRecipientUser(short securityUserId, List<Integer> definitionIds);
}
