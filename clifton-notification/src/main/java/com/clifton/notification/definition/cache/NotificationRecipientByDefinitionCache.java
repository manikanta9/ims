package com.clifton.notification.definition.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationRecipient;
import org.springframework.stereotype.Component;


/**
 * An entity cache for retrieving lists of {@link NotificationRecipient} entities by {@link NotificationDefinition} ID.
 *
 * @author MikeH
 */
@Component
public class NotificationRecipientByDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<NotificationRecipient, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "notificationDefinition.id";
	}


	@Override
	protected Integer getBeanKeyValue(NotificationRecipient bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getNotificationDefinition());
		}
		return null;
	}
}
