package com.clifton.notification.definition.cache;

import java.util.Set;


/**
 * The <code>NotificationDefinitionCountCache</code> caches for each notification definition the number of notifications associated with it
 * <p>
 * When the count changes from/to 0, there is a second cache key that stores the notification definitions that have no notifications that is updated
 *
 * @author manderson
 */
public interface NotificationDefinitionCountCache {


	public Set<Integer> getNotificationDefinitionEmptySet();
}
