package com.clifton.notification.definition;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.NamedEntity;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * The <code>NotificationDefinition</code> class defines basic properties used to
 * define a notification.
 * <p>
 * Each notification definition has a {@link NotificationType} & {@link SystemPriority} used for classification purposes.
 *
 * @author manderson
 */
public abstract class NotificationDefinition extends NamedEntity<Integer> implements SystemEntityModifyConditionAwareAdminRequired {

	private NotificationType type;
	private SystemPriority priority;

	private SecurityUser assigneeUser;
	private SecurityGroup assigneeGroup;
	private SystemTable table;

	/**
	 * The subject and text of a notification definition are templates that use replacements to
	 * generate a bean specific subject and text for each notification.
	 */
	private String subject;
	private String text;

	private boolean active;
	private boolean systemAcknowledged;
	/**
	 * Used for System Acknowledged Definitions, where in some cases
	 * the data is what it is and we can't fix it.  Need to be able to mark these
	 * notifications as acknowledged directly, since the system will never be able to
	 * delete them in all cases.
	 */
	private boolean systemAcknowledgedIgnorable;

	/**
	 * When notification subject/text changes and the notification has been acknowledged
	 * the system adds a comment and automatically unacknowledges it.  With this option
	 * the notification will still be updated and comment entered, but the notification will not be unacknowledged
	 */
	private boolean doNotUnacknowledgeIfUpdated;

	private Integer daysDueFromCreation;

	/**
	 * Used to determine the DueDate on the notification.
	 * Only applicable if using business days in calculations
	 */
	private Calendar businessDaysDueCalendar;

	/**
	 * If a notification already exists for the same entity & definition
	 * and hasn't been acknowledged, only create a new notification
	 * if duplicates are allowed.
	 */
	private boolean duplicatesAllowed;

	/**
	 * When notifications are generated the fkFieldId is populated on the Notification with the Id of the main table
	 * for the notification definition.  This field is used to determine uniqueness for existing notifications to check if it needs to be inserted/updated/deleted.
	 * <p>
	 * There are a few times where the id could apply to multiple notifications, but we don't really have a good table/id combination to determine a unique case.  For these,
	 * can use this option, which will drive "getExistingNotification" to determine if it's the same notification by the subject value.
	 * <p>
	 * Overall, should try not to use this feature when possible as any change in the subject would result in delete/insert instead of just an update.
	 */
	private boolean useSubjectForUniqueness;

	private SystemTable linkedTable;
	private String linkedRelationshipPath;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract boolean isInstant();


	/**
	 * Returns the bean name for the object when applying freemarker text template
	 * Instant: Uses "bean" and passes the actual bean
	 * Batch - One Per Row uses "row" and passes the row
	 * Batch - One for all uses "dataTable" and passes the entire table
	 */
	public abstract String getTemplateConfigBeanName();


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.getType().getDefinitionModifyCondition();
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getters & Setters                               ////////
	////////////////////////////////////////////////////////////////////////////


	public NotificationType getType() {
		return this.type;
	}


	public void setType(NotificationType type) {
		this.type = type;
	}


	public SystemPriority getPriority() {
		return this.priority;
	}


	public void setPriority(SystemPriority priority) {
		this.priority = priority;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public SecurityUser getAssigneeUser() {
		return this.assigneeUser;
	}


	public void setAssigneeUser(SecurityUser assigneeUser) {
		this.assigneeUser = assigneeUser;
	}


	public SecurityGroup getAssigneeGroup() {
		return this.assigneeGroup;
	}


	public void setAssigneeGroup(SecurityGroup assigneeGroup) {
		this.assigneeGroup = assigneeGroup;
	}


	public Integer getDaysDueFromCreation() {
		return this.daysDueFromCreation;
	}


	public void setDaysDueFromCreation(Integer daysDueFromCreation) {
		this.daysDueFromCreation = daysDueFromCreation;
	}


	public Calendar getBusinessDaysDueCalendar() {
		return this.businessDaysDueCalendar;
	}


	public void setBusinessDaysDueCalendar(Calendar businessDaysDueCalendar) {
		this.businessDaysDueCalendar = businessDaysDueCalendar;
	}


	public boolean isDuplicatesAllowed() {
		return this.duplicatesAllowed;
	}


	public void setDuplicatesAllowed(boolean duplicatesAllowed) {
		this.duplicatesAllowed = duplicatesAllowed;
	}


	public SystemTable getLinkedTable() {
		return this.linkedTable;
	}


	public void setLinkedTable(SystemTable linkedTable) {
		this.linkedTable = linkedTable;
	}


	public String getLinkedRelationshipPath() {
		return this.linkedRelationshipPath;
	}


	public void setLinkedRelationshipPath(String linkedRelationshipPath) {
		this.linkedRelationshipPath = linkedRelationshipPath;
	}


	public boolean isSystemAcknowledged() {
		return this.systemAcknowledged;
	}


	public void setSystemAcknowledged(boolean systemAcknowledged) {
		this.systemAcknowledged = systemAcknowledged;
	}


	public boolean isSystemAcknowledgedIgnorable() {
		return this.systemAcknowledgedIgnorable;
	}


	public void setSystemAcknowledgedIgnorable(boolean systemAcknowledgedIgnorable) {
		this.systemAcknowledgedIgnorable = systemAcknowledgedIgnorable;
	}


	public boolean isUseSubjectForUniqueness() {
		return this.useSubjectForUniqueness;
	}


	public void setUseSubjectForUniqueness(boolean useSubjectForUniqueness) {
		this.useSubjectForUniqueness = useSubjectForUniqueness;
	}


	public boolean isDoNotUnacknowledgeIfUpdated() {
		return this.doNotUnacknowledgeIfUpdated;
	}


	public void setDoNotUnacknowledgeIfUpdated(boolean doNotUnacknowledgeIfUpdated) {
		this.doNotUnacknowledgeIfUpdated = doNotUnacknowledgeIfUpdated;
	}
}
