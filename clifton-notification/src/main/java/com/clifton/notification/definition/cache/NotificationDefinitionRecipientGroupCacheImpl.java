package com.clifton.notification.definition.cache;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.notification.definition.NotificationRecipient;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserGroup;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>NotificationDefinitionRecipientGroupCacheImpl</code> caches a map of GroupID to the list
 * of notification definition ids. Used for "My Notifications" lookup
 * <p>
 * NOTE: This cache is registered in spring xml and observers SecurityUserGroup changes and well as NotificationRecipient changes
 */
@Component
public class NotificationDefinitionRecipientGroupCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<Short, List<Integer>>, NotificationDefinitionRecipientGroupCache {

	private CacheHandler<Short, List<Integer>> cacheHandler;

	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Integer> getNotificationDefinitionIdsForRecipientGroup(short securityGroupId) {
		return getCacheHandler().get(getCacheName(), securityGroupId);
	}


	@Override
	public void setNotificationDefinitionIdsForRecipientGroup(short securityGroupId, List<Integer> definitionIds) {
		getCacheHandler().put(getCacheName(), securityGroupId, definitionIds);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                      Observer Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			T originalBean = null;
			if (event.isUpdate()) {
				originalBean = getOriginalBean(dao, bean);
			}
			if (bean instanceof SecurityUserGroup) {
				clearCacheForSecurityUserGroupChange(((SecurityUserGroup) bean), originalBean == null ? null : ((SecurityUserGroup) originalBean));
			}
			else if (bean instanceof NotificationRecipient) {
				clearCacheForNotificationRecipientChange(((NotificationRecipient) bean), originalBean == null ? null : ((NotificationRecipient) originalBean));
			}
			else {
				throw new IllegalStateException("NotificationDefinitionUserCache: Observable changes to NotificationRecipients and SecurityUserGroups are supported for cache clearing only. Bean ["
						+ bean + "] is of type " + dao.getConfiguration().getBeanClass().getName() + ".");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Cache Clearing Methods                  ////////////
	////////////////////////////////////////////////////////////////////////////////


	private void clearCacheForSecurityUserGroupChange(SecurityUserGroup bean, SecurityUserGroup originalBean) {
		clearCacheForSecurityUser(bean.getReferenceOne());

		if (originalBean != null) {
			clearCacheForSecurityUser(originalBean.getReferenceOne());
		}
	}


	private void clearCacheForNotificationRecipientChange(NotificationRecipient bean, NotificationRecipient originalBean) {
		clearCacheForSecurityUser(bean.getSecurityUser());
		clearCacheForSecurityGroup(bean.getSecurityGroup());

		if (originalBean != null) {
			clearCacheForSecurityUser(originalBean.getSecurityUser());
			clearCacheForSecurityGroup(originalBean.getSecurityGroup());
		}
	}


	private void clearCacheForSecurityUser(SecurityUser securityUser) {
		if (securityUser != null) {
			getCacheHandler().remove(getCacheName(), securityUser.getId());
		}
	}


	private void clearCacheForSecurityGroup(SecurityGroup securityGroup) {
		if (securityGroup != null) {
			List<SecurityUser> userList = getSecurityUserService().getSecurityUserListByGroup(securityGroup.getId());
			for (SecurityUser user : CollectionUtils.getIterable(userList)) {
				clearCacheForSecurityUser(user);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public CacheHandler<Short, List<Integer>> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Short, List<Integer>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
