package com.clifton.notification.definition;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateBuilder;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableGenerator;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.notification.definition.cache.NotificationDefinitionCountCache;
import com.clifton.notification.definition.cache.NotificationDefinitionRecipientGroupCache;
import com.clifton.notification.definition.cache.NotificationDefinitionRecipientUserCache;
import com.clifton.notification.definition.search.NotificationDefinitionSearchForm;
import com.clifton.notification.definition.search.NotificationHistorySearchForm;
import com.clifton.notification.definition.search.NotificationTypeSearchForm;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.label.SystemLabel;
import com.clifton.system.label.SystemLabelService;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>NotificationDefinitionServiceImpl</code> class provides a basic implementation
 * for the {@link NotificationDefinitionService}
 *
 * @author manderson
 */
@Service
public class NotificationDefinitionServiceImpl<T extends NotificationDefinition> implements NotificationDefinitionService<T> {

	private AdvancedUpdatableDAO<T, Criteria> notificationDefinitionDAO;
	private AdvancedUpdatableDAO<NotificationRecipient, Criteria> notificationRecipientDAO;
	private UpdatableDAO<NotificationDefinitionLink> notificationDefinitionLinkDAO;
	private AdvancedUpdatableDAO<NotificationType, Criteria> notificationTypeDAO;
	private AdvancedUpdatableDAO<NotificationHistory, Criteria> notificationHistoryDAO;

	private NotificationDefinitionCountCache notificationDefinitionCountCache;
	private NotificationDefinitionRecipientUserCache notificationDefinitionRecipientUserCache;
	private NotificationDefinitionRecipientGroupCache notificationDefinitionRecipientGroupCache;
	private DaoSingleKeyListCache<NotificationRecipient, Integer> notificationRecipientByDefinitionCache;

	private SecurityUserService securityUserService;
	private SystemBeanService systemBeanService;
	private SystemLabelService systemLabelService;
	private SystemQueryExecutionService systemQueryExecutionService;
	private TemplateBuilder templateBuilder;
	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	//////           Notification Definition Business Methods             //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public T getNotificationDefinition(int id) {
		return getNotificationDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<NotificationDefinition> getNotificationDefinitionGenericList(final NotificationDefinitionSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getTableName() != null) {
					criteria.createAlias("linkedTable", "lt", JoinType.LEFT_OUTER_JOIN);
					Criterion mainTable = Restrictions.eq(getPathAlias("table", criteria) + ".name", searchForm.getTableName());
					Criterion linkedTable = Restrictions.eq("lt.name", searchForm.getTableName());

					// Only makes sense to filter on entityId if tableName is defined
					if (searchForm.getEntityId() != null) {
						// No Links Defined
						DetachedCriteria notExists = DetachedCriteria.forClass(NotificationDefinitionLink.class, "ndl");
						notExists.setProjection(Projections.property("id"));
						notExists.add(Restrictions.eqProperty("definition.id", criteria.getAlias() + ".id"));

						// Specific Link Defined
						DetachedCriteria exists = DetachedCriteria.forClass(NotificationDefinitionLink.class, "ndl2");
						exists.setProjection(Projections.property("id"));
						exists.add(Restrictions.eqProperty("definition.id", criteria.getAlias() + ".id"));
						exists.add(Restrictions.eq("fkFieldId", searchForm.getEntityId()));

						LogicalExpression orEntityId = Restrictions.or(Subqueries.notExists(notExists), Subqueries.exists(exists));
						LogicalExpression linked = Restrictions.and(linkedTable, orEntityId);

						LogicalExpression or = Restrictions.or(mainTable, linked);
						criteria.add(or);
					}
					else {
						LogicalExpression or = Restrictions.or(mainTable, linkedTable);
						criteria.add(or);
					}
				}
			}
		};
		return (List<NotificationDefinition>) getNotificationDefinitionDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	@Transactional
	public T copyNotificationDefinition(int id, String name) {
		T original = getNotificationDefinition(id);
		T newBean = BeanUtils.cloneBean(original, false, false);
		newBean.setName(name);
		getNotificationDefinitionDAO().save(newBean);

		List<NotificationRecipient> recipientList = getNotificationRecipientListByNotificationDefinition(id);
		for (NotificationRecipient rec : CollectionUtils.getIterable(recipientList)) {
			NotificationRecipient newRec = BeanUtils.cloneBean(rec, false, false);
			newRec.setNotificationDefinition(newBean);
			getNotificationRecipientDAO().save(newRec);
		}
		return newBean;
	}


	@Override
	@Transactional
	public void deleteNotificationDefinition(int id) {
		getNotificationRecipientDAO().deleteList(getNotificationRecipientListByNotificationDefinition(id));
		getNotificationDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////      Instant Notification Definition Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public List<NotificationDefinitionInstant> getNotificationDefinitionInstantList(boolean activeOnly) {
		if (activeOnly) {
			return (List<NotificationDefinitionInstant>) getNotificationDefinitionDAO().findByFields(new String[]{"active", "systemQuery", "generatorSystemBean"}, new Object[]{true, null, null});
		}
		return (List<NotificationDefinitionInstant>) getNotificationDefinitionDAO().findByFields(new String[]{"systemQuery", "generatorSystemBean"}, new Object[]{null, null});
	}


	@Override
	@SuppressWarnings("unchecked")
	public NotificationDefinitionInstant saveNotificationDefinitionInstant(NotificationDefinitionInstant bean) {
		return (NotificationDefinitionInstant) getNotificationDefinitionDAO().save((T) bean);
	}


	////////////////////////////////////////////////////////////////////////////
	//////       Batch Notification Definition Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public NotificationDefinitionBatch saveNotificationDefinitionBatch(NotificationDefinitionBatch bean) {
		return (NotificationDefinitionBatch) getNotificationDefinitionDAO().save((T) bean);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<NotificationDefinitionBatch> getNotificationDefinitionBatchListActiveOnDate(final Date startDateTime) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			Date startDate = DateUtils.clearTime(startDateTime);
			// Time startTime = new Time(startDateTime);
			criteria.add(Restrictions.eq("active", true));
			criteria.add(Restrictions.or(Restrictions.isNotNull("systemQuery"), Restrictions.isNotNull("generatorSystemBean")));
			criteria.createCriteria("calendarSchedule", "s")
					.add(Restrictions.le("s.startDate", startDate))
					.add(Restrictions.or(Restrictions.isNull("s.endDate"), Restrictions.ge("s.endDate", startDate)));
			// CANNOT FILTER ON TIMES - EACH FREQUENCY TYPE USES START/END TIMES TO HAVE A DIFFERENT MEANING
			// JUST RETURN ANY ACTIVE ON THE GIVEN DATE WITHOUT TIME.  THE SCHEDULER ITSELF WILL DETERMINE IF THERE
			// ARE OCCURRENCES AT THAT TIME.  HOURLY ALWAYS SEEMS TO WORK
			// BUT DAILY OR ONCE DEFINITELY DON'T RETURN ACTIVE DEFINITIONS WHEN THEY SHOULD
			//       .add(Restrictions.or(Restrictions.isNull("s.startTime"), Restrictions.le("s.startTime", startTime)))
			//       .add(Restrictions.or(Restrictions.isNull("s.endTime"), Restrictions.ge("s.endTime", startTime)));
		};

		return (List<NotificationDefinitionBatch>) getNotificationDefinitionDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<NotificationDefinitionBatch> getNotificationDefinitionBatchListForTypeActiveOnDate(final short typeId, final Date startDateTime) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			Date startDate = DateUtils.clearTime(startDateTime);
			// Time startTime = new Time(startDateTime);
			criteria.add(Restrictions.eq("active", true));
			criteria.add(Restrictions.or(Restrictions.isNotNull("systemQuery"), Restrictions.isNotNull("generatorSystemBean")));
			criteria.createCriteria("calendarSchedule", "s")
					.add(Restrictions.le("s.startDate", startDate))
					.add(Restrictions.or(Restrictions.isNull("s.endDate"), Restrictions.ge("s.endDate", startDate)));
			criteria.add(Restrictions.eq("type.id", typeId));
		};

		return (List<NotificationDefinitionBatch>) getNotificationDefinitionDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	@Transactional(timeout = 180)
	public DataTable getNotificationDefinitionBatchDataTableResults(NotificationDefinitionBatch bean) {
		DataTable results;

		if (bean.getSystemQuery() != null) {
			results = getSystemQueryExecutionService().getSystemQueryResult(bean.getSystemQuery());
		}
		else {
			DataTableGenerator generator = (DataTableGenerator) getSystemBeanService().getBeanInstance(bean.getGeneratorSystemBean());
			results = generator.getDataTable();
		}
		results = filterResultsByLinkedTable(bean, results);
		return results;
	}


	private DataTable filterResultsByLinkedTable(NotificationDefinition def, DataTable results) {

		// If results & a linked table, check if specific links entered
		if (results != null && results.getTotalRowCount() != 0 && def.getLinkedTable() != null) {
			List<NotificationDefinitionLink> links = getNotificationDefinitionLinkListByDefinition(def.getId());
			// If specific links entered, need to filter
			if (!CollectionUtils.isEmpty(links)) {

				DataTable filteredResults = new PagingDataTableImpl(results.getColumnList());
				String linkedField = (def.getLinkedTable() == null ? null : def.getLinkedRelationshipPath());
				Integer[] linkIds = (Integer[]) BeanUtils.getPropertyValues(links, "fkFieldId");

				for (int i = 0; i < results.getTotalRowCount(); i++) {
					DataRow row = results.getRow(i);
					if (linkedField != null) {
						Integer linkedId = (Integer) row.getValue(linkedField);
						// Include this row
						if (ArrayUtils.contains(linkIds, linkedId)) {
							filteredResults.addRow(row);
						}
					}
				}
				return filteredResults;
			}
		}
		return results;
	}


	////////////////////////////////////////////////////////////////////////////
	/////        Notification Default Text Business Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getNotificationDefaultText(int id) {
		NotificationDefinition bean = getNotificationDefinition(id);
		// Instant Definitions
		if (bean.isInstant()) {
			DAOConfiguration<?> config = (getDaoLocator().locate(bean.getTable().getName())).getConfiguration();
			return getTemplateBuilder().buildDTOObjectTemplate(config, "bean");
		}

		// Batch Definitions
		NotificationDefinitionBatch batch = (NotificationDefinitionBatch) bean;
		DataTable table = getNotificationDefinitionBatchDataTableResults(batch);
		// Entire DataTable in Message
		if (batch.isBatchResults()) {
			return getTemplateBuilder().buildDataTableTemplate(table, "dataTable");
		}
		// Each Individual Row in Message
		return getTemplateBuilder().buildDataRowTemplate(table, "row");
	}


	////////////////////////////////////////////////////////////////////////////
	//////           Notification Recipient Business Methods             ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NotificationRecipient getNotificationRecipient(int id) {
		return getNotificationRecipientDAO().findByPrimaryKey(id);
	}


	@Override
	public List<NotificationRecipient> getNotificationRecipientListByNotificationDefinition(int notificationDefinitionId) {
		return getNotificationRecipientByDefinitionCache().getBeanListForKeyValue(getNotificationRecipientDAO(), notificationDefinitionId);
	}


	@Override
	public List<Integer> getNotificationDefinitionIdsForRecipientUserOrGroup(Short securityUserId, Short securityGroupId, boolean excludeEmpty) {
		return CollectionUtils.buildStream(getNotificationDefinitionIdsForRecipientUser(securityUserId, excludeEmpty), getNotificationDefinitionIdsForRecipientGroup(securityGroupId, excludeEmpty))
				.distinct()
				.collect(Collectors.toList());
	}


	@Override
	public List<Integer> getNotificationDefinitionIdsForRecipientUser(final Short securityUserId, boolean excludeEmpty) {
		if (securityUserId == null) {
			return null;
		}
		List<Integer> definitionList = getNotificationDefinitionRecipientUserCache().getNotificationDefinitionIdsForRecipientUser(securityUserId);
		if (definitionList == null) {
			final List<SecurityGroup> groupList = getSecurityUserService().getSecurityGroupListByUser(securityUserId);
			if (CollectionUtils.isEmpty(groupList)) {
				return null;
			}
			HibernateSearchConfigurer searchConfig = criteria -> criteria.add(Restrictions.or(Restrictions.eq("securityUser.id", securityUserId), Restrictions.in("securityGroup.id", BeanUtils.getPropertyValues(groupList, "id"))));
			List<NotificationRecipient> recipientList = getNotificationRecipientDAO().findBySearchCriteria(searchConfig);
			definitionList = new ArrayList<>();
			for (NotificationRecipient recipient : CollectionUtils.getIterable(recipientList)) {
				if (!definitionList.contains(recipient.getNotificationDefinition().getId())) {
					definitionList.add(recipient.getNotificationDefinition().getId());
				}
			}
			getNotificationDefinitionRecipientUserCache().setNotificationDefinitionIdsForRecipientUser(securityUserId, definitionList);
		}
		if (excludeEmpty && !CollectionUtils.isEmpty(definitionList)) {
			Set<Integer> emptyList = getNotificationDefinitionCountCache().getNotificationDefinitionEmptySet();
			List<Integer> filteredList = new ArrayList<>();
			for (Integer definitionId : definitionList) {
				if (!emptyList.contains(definitionId)) {
					filteredList.add(definitionId);
				}
			}
			return filteredList;
		}

		return definitionList;
	}


	@Override
	public List<Integer> getNotificationDefinitionIdsForRecipientGroup(Short securityGroupId, boolean excludeEmpty) {
		if (securityGroupId == null) {
			return null;
		}
		List<Integer> definitionList = getNotificationDefinitionRecipientGroupCache().getNotificationDefinitionIdsForRecipientGroup(securityGroupId);
		if (definitionList == null) {
			HibernateSearchConfigurer searchConfig = criteria -> criteria.add(Restrictions.eq("securityGroup.id", securityGroupId));
			List<NotificationRecipient> recipientList = getNotificationRecipientDAO().findBySearchCriteria(searchConfig);
			definitionList = new ArrayList<>();
			for (NotificationRecipient recipient : CollectionUtils.getIterable(recipientList)) {
				if (!definitionList.contains(recipient.getNotificationDefinition().getId())) {
					definitionList.add(recipient.getNotificationDefinition().getId());
				}
			}
			getNotificationDefinitionRecipientGroupCache().setNotificationDefinitionIdsForRecipientGroup(securityGroupId, definitionList);
		}
		if (excludeEmpty && !CollectionUtils.isEmpty(definitionList)) {
			Set<Integer> emptyList = getNotificationDefinitionCountCache().getNotificationDefinitionEmptySet();
			List<Integer> filteredList = new ArrayList<>();
			for (Integer definitionId : definitionList) {
				if (!emptyList.contains(definitionId)) {
					filteredList.add(definitionId);
				}
			}
			return filteredList;
		}

		return definitionList;
	}


	@Override
	public void linkNotificationDefinitionToSecurityUser(short userId, int notificationDefinitionId) {
		NotificationRecipient recipient = new NotificationRecipient();
		recipient.setSecurityUser(getSecurityUserService().getSecurityUser(userId));
		recipient.setNotificationDefinition(getNotificationDefinition(notificationDefinitionId));
		getNotificationRecipientDAO().save(recipient);
	}


	@Override
	public void linkNotificationDefinitionToSecurityGroup(short groupId, int notificationDefinitionId) {
		NotificationRecipient recipient = new NotificationRecipient();
		recipient.setSecurityGroup(getSecurityUserService().getSecurityGroup(groupId));
		recipient.setNotificationDefinition(getNotificationDefinition(notificationDefinitionId));
		getNotificationRecipientDAO().save(recipient);
	}


	@Override
	public void deleteNotificationRecipient(int id) {
		getNotificationRecipientDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////        Notification Definition Link Business Methods             /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NotificationDefinitionLink getNotificationDefinitionLink(int id) {
		return getNotificationDefinitionLinkDAO().findByPrimaryKey(id);
	}


	private NotificationDefinitionLink getNotificationDefinitionLinkByDefinitionAndEntity(int definitionId, int fkFieldId) {
		return getNotificationDefinitionLinkDAO().findOneByFields(new String[]{"definition.id", "fkFieldId"}, new Object[]{definitionId, fkFieldId});
	}


	@Override
	public List<NotificationDefinitionLink> getNotificationDefinitionLinkListByDefinition(int definitionId) {
		return getNotificationDefinitionLinkDAO().findByField("definition.id", definitionId);
	}


	@Override
	public void linkNotificationDefinitionToEntity(int fkFieldId, int definitionId) {
		NotificationDefinitionLink link = new NotificationDefinitionLink();
		link.setFkFieldId(fkFieldId);
		link.setDefinition(getNotificationDefinition(definitionId));
		getNotificationDefinitionLinkDAO().save(link);
	}


	@Override
	public void deleteNotificationDefinitionLinkByDefinitionAndEntity(int definitionId, int fkFieldId) {
		getNotificationDefinitionLinkDAO().delete(getNotificationDefinitionLinkByDefinitionAndEntity(definitionId, fkFieldId));
	}


	@Override
	public List<SystemLabel> getNotificationDefinitionLinkEntityAvailableList(int definitionId, NotificationDefinitionSearchForm searchForm) {
		final NotificationDefinition def = getNotificationDefinition(definitionId);
		if (def.getLinkedTable() == null) {
			return null;
		}
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(Restrictions.eq("systemTable.id", def.getLinkedTable().getId()));
			}
		};
		return getSystemLabelService().getSystemLabelListByConfigurer(searchConfigurer);
	}


	@Override
	public List<SystemLabel> getNotificationDefinitionLinkEntitySelectedList(int definitionId) {
		final NotificationDefinition def = getNotificationDefinition(definitionId);
		if (def.getLinkedTable() == null) {
			return null;
		}
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			DetachedCriteria sub = DetachedCriteria.forClass(NotificationDefinitionLink.class, "ndl");
			sub.setProjection(Projections.property("fkFieldId"));
			sub.add(Restrictions.eqProperty("ndl.fkFieldId", criteria.getAlias() + ".pkFieldId"));
			criteria.add(Restrictions.eq("systemTable.id", def.getLinkedTable().getId()));
			criteria.add(Subqueries.exists(sub));
		};
		return getSystemLabelService().getSystemLabelListByConfigurer(searchConfigurer);
	}


	////////////////////////////////////////////////////////////////////////////
	//////              Notification Type Business Methods                //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NotificationType getNotificationType(short id) {
		return getNotificationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<NotificationType> getNotificationTypeList(NotificationTypeSearchForm searchForm) {
		return getNotificationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public NotificationType saveNotificationType(NotificationType bean) {
		return getNotificationTypeDAO().save(bean);
	}


	@Override
	public void deleteNotificationType(short id) {
		getNotificationTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Batch Job History Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NotificationHistory getNotificationHistory(int id) {
		return getNotificationHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<NotificationHistory> getNotificationHistoryList(NotificationHistorySearchForm searchForm) {
		return getNotificationHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public NotificationHistory getNotificationHistoryByDate(int definitionId, Date scheduledDate) {
		return getNotificationHistoryDAO().findOneByFields(new String[]{"notificationDefinition.id", "scheduledDate"}, new Object[]{definitionId, scheduledDate});
	}


	@Override
	public NotificationHistory saveNotificationHistory(NotificationHistory bean) {
		return getNotificationHistoryDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<T, Criteria> getNotificationDefinitionDAO() {
		return this.notificationDefinitionDAO;
	}


	public void setNotificationDefinitionDAO(AdvancedUpdatableDAO<T, Criteria> notificationDefinitionDAO) {
		this.notificationDefinitionDAO = notificationDefinitionDAO;
	}


	public AdvancedUpdatableDAO<NotificationRecipient, Criteria> getNotificationRecipientDAO() {
		return this.notificationRecipientDAO;
	}


	public void setNotificationRecipientDAO(AdvancedUpdatableDAO<NotificationRecipient, Criteria> notificationRecipientDAO) {
		this.notificationRecipientDAO = notificationRecipientDAO;
	}


	public UpdatableDAO<NotificationDefinitionLink> getNotificationDefinitionLinkDAO() {
		return this.notificationDefinitionLinkDAO;
	}


	public void setNotificationDefinitionLinkDAO(UpdatableDAO<NotificationDefinitionLink> notificationDefinitionLinkDAO) {
		this.notificationDefinitionLinkDAO = notificationDefinitionLinkDAO;
	}


	public AdvancedUpdatableDAO<NotificationType, Criteria> getNotificationTypeDAO() {
		return this.notificationTypeDAO;
	}


	public void setNotificationTypeDAO(AdvancedUpdatableDAO<NotificationType, Criteria> notificationTypeDAO) {
		this.notificationTypeDAO = notificationTypeDAO;
	}


	public AdvancedUpdatableDAO<NotificationHistory, Criteria> getNotificationHistoryDAO() {
		return this.notificationHistoryDAO;
	}


	public void setNotificationHistoryDAO(AdvancedUpdatableDAO<NotificationHistory, Criteria> notificationHistoryDAO) {
		this.notificationHistoryDAO = notificationHistoryDAO;
	}


	public NotificationDefinitionCountCache getNotificationDefinitionCountCache() {
		return this.notificationDefinitionCountCache;
	}


	public void setNotificationDefinitionCountCache(NotificationDefinitionCountCache notificationDefinitionCountCache) {
		this.notificationDefinitionCountCache = notificationDefinitionCountCache;
	}


	public NotificationDefinitionRecipientUserCache getNotificationDefinitionRecipientUserCache() {
		return this.notificationDefinitionRecipientUserCache;
	}


	public void setNotificationDefinitionRecipientUserCache(NotificationDefinitionRecipientUserCache notificationDefinitionRecipientUserCache) {
		this.notificationDefinitionRecipientUserCache = notificationDefinitionRecipientUserCache;
	}


	public NotificationDefinitionRecipientGroupCache getNotificationDefinitionRecipientGroupCache() {
		return this.notificationDefinitionRecipientGroupCache;
	}


	public void setNotificationDefinitionRecipientGroupCache(NotificationDefinitionRecipientGroupCache notificationDefinitionRecipientGroupCache) {
		this.notificationDefinitionRecipientGroupCache = notificationDefinitionRecipientGroupCache;
	}


	public DaoSingleKeyListCache<NotificationRecipient, Integer> getNotificationRecipientByDefinitionCache() {
		return this.notificationRecipientByDefinitionCache;
	}


	public void setNotificationRecipientByDefinitionCache(DaoSingleKeyListCache<NotificationRecipient, Integer> notificationRecipientByDefinitionCache) {
		this.notificationRecipientByDefinitionCache = notificationRecipientByDefinitionCache;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemLabelService getSystemLabelService() {
		return this.systemLabelService;
	}


	public void setSystemLabelService(SystemLabelService systemLabelService) {
		this.systemLabelService = systemLabelService;
	}


	public SystemQueryExecutionService getSystemQueryExecutionService() {
		return this.systemQueryExecutionService;
	}


	public void setSystemQueryExecutionService(SystemQueryExecutionService systemQueryExecutionService) {
		this.systemQueryExecutionService = systemQueryExecutionService;
	}


	public TemplateBuilder getTemplateBuilder() {
		return this.templateBuilder;
	}


	public void setTemplateBuilder(TemplateBuilder templateBuilder) {
		this.templateBuilder = templateBuilder;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
