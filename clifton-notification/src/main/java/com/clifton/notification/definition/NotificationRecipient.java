package com.clifton.notification.definition;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>NotificationRecipient</code> class is used to define whom should receive {@link com.clifton.notification.generator.Notification}s
 * for a given {@link NotificationDefinition}.
 * <p/>
 * Notification Recipients can be defined as specific {@link SecurityUser}, {@link SecurityGroup}.
 * <p/>
 * POTENTIAL ENHANCEMENT:
 * Use {@link SystemCondition} to determine if user(s) should receive {@link com.clifton.notification.generator.Notification} for a specific bean. i.e. send
 * notifications for a particular bean to the bean's create user.
 *
 * @author manderson
 */
public class NotificationRecipient extends BaseEntity<Integer> implements LabeledObject {

	private NotificationDefinition notificationDefinition;

	private SecurityUser securityUser;
	private SecurityGroup securityGroup;


	@Override
	public String getLabel() {
		if (getSecurityUser() != null) {
			return "User: " + getSecurityUser().getLabel();
		}
		if (getSecurityGroup() != null) {
			return "Group: " + getSecurityGroup().getLabel();
		}
		return null;
	}


	public NotificationDefinition getNotificationDefinition() {
		return this.notificationDefinition;
	}


	public void setNotificationDefinition(NotificationDefinition notificationDefinition) {
		this.notificationDefinition = notificationDefinition;
	}


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public SecurityGroup getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(SecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
	}
}
