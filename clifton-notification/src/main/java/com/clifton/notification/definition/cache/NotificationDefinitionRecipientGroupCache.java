package com.clifton.notification.definition.cache;

import java.util.List;


/**
 * The <code>NotificationDefinitionRecipientGroupCache</code> caches a map of GroupID to the list
 * of notification definition ids that group is a recipient of. Used for "My Notifications" lookup
 */
public interface NotificationDefinitionRecipientGroupCache {

	public List<Integer> getNotificationDefinitionIdsForRecipientGroup(short securityGroupId);


	public void setNotificationDefinitionIdsForRecipientGroup(short securityGroupId, List<Integer> definitionIds);
}
