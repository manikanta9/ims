package com.clifton.notification.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntityDateRangeWithTimeSearchForm;
import com.clifton.notification.definition.NotificationStatuses;

import java.util.Date;


/**
 * The <code>NotificationDefinitionHistorySearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class NotificationHistorySearchForm extends BaseEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "notificationDefinition.id", sortField = "notificationDefinition.name")
	private Integer notificationDefinitionId;

	@SearchField(searchField = "name", searchFieldPath = "notificationDefinition")
	private String notificationDefinitionName;

	@SearchField
	private NotificationStatuses status;

	@SearchField
	private Date scheduledDate;

	@SearchField
	private String description;

	@SearchField(searchField = "runnerUser.id")
	private Short runnerUserId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getNotificationDefinitionId() {
		return this.notificationDefinitionId;
	}


	public void setNotificationDefinitionId(Integer notificationDefinitionId) {
		this.notificationDefinitionId = notificationDefinitionId;
	}


	public String getNotificationDefinitionName() {
		return this.notificationDefinitionName;
	}


	public void setNotificationDefinitionName(String notificationDefinitionName) {
		this.notificationDefinitionName = notificationDefinitionName;
	}


	public NotificationStatuses getStatus() {
		return this.status;
	}


	public void setStatus(NotificationStatuses status) {
		this.status = status;
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getRunnerUserId() {
		return this.runnerUserId;
	}


	public void setRunnerUserId(Short runnerUserId) {
		this.runnerUserId = runnerUserId;
	}
}
