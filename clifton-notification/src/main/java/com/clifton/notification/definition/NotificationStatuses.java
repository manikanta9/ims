package com.clifton.notification.definition;


/**
 * The <code>NotificationStatus</code> represents the status of a notification execution
 *
 * @author Mary Anderson
 */
public enum NotificationStatuses {

	COMPLETED, COMPLETED_WITH_NO_RESULTS, FAILED, RUNNING, NONE, SKIPPED
}
