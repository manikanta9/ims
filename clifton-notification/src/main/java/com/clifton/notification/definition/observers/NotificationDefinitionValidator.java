package com.clifton.notification.definition.observers;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.notification.generator.NotificationGeneratorService;
import com.clifton.notification.generator.NotificationSearchForm;
import org.springframework.stereotype.Component;


/**
 * The <code>NotificationDefinitionValidator</code> class handles all validation prior to
 * inserting/updating/deleting a {@link NotificationDefinition}
 *
 * @author manderson
 */
@Component
public class NotificationDefinitionValidator extends SelfRegisteringDaoValidator<NotificationDefinition> {

	private NotificationGeneratorService<NotificationDefinition> notificationGeneratorService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(NotificationDefinition bean, DaoEventTypes config) throws ValidationException {
		if (!config.isDelete()) {
			if (bean.getLinkedTable() != null) {
				if (StringUtils.isEmpty(bean.getLinkedRelationshipPath())) {
					throw new FieldValidationException(
							"Please enter the linked relationship path for the given link.  For instant notifications, please provide the path as the bean property path.  For batched notifications, please provide the path as a SQL field to apply in the WHERE clause.",
							"linkedRelationshipPath");
				}
			}
		}
		else {
			NotificationSearchForm searchForm = new NotificationSearchForm();
			searchForm.setDefinitionId(bean.getId());
			if (!CollectionUtils.isEmpty(getNotificationGeneratorService().getNotificationListSecure(searchForm))) {
				throw new ValidationException("Cannot delete notification definition [" + bean.getName() + "] because there are existing notifications associated with it.");
			}
		}

		// Make sure not changing from Instant to Batched Notification
		if (config.isUpdate()) {
			NotificationDefinition originalBean = getOriginalBean(bean);
			if (originalBean != null && originalBean.isInstant() != bean.isInstant()) {
				throw new ValidationException("Cannot update a Notification Definition from/to Instant vs. Batched");
			}
		}

		if (bean.isInstant()) {
			NotificationDefinitionInstant instantBean = (NotificationDefinitionInstant) bean;
			if (bean.isActive() && (!(instantBean.isInsertEvent() || instantBean.isUpdateEvent() || instantBean.isDeleteEvent()))) {
				throw new ValidationException("Active instant notifications must have at least one insert/update/delete option selected");
			}
		}
		else {
			NotificationDefinitionBatch batchBean = (NotificationDefinitionBatch) bean;
			if (batchBean.getSystemQuery() == null && batchBean.getGeneratorSystemBean() == null) {
				throw new FieldValidationException("System Query or Generator Bean is required", "systemQuery");
			}
			if (batchBean.getSystemQuery() != null && !batchBean.getSystemQuery().getTable().equals(batchBean.getTable())) {
				throw new FieldValidationException("System Query must be associated with the same table that was selected for this notification", "systemQuery");
			}
			if (bean.isActive() && batchBean.getCalendarSchedule() == null) {
				throw new FieldValidationException("Active batch notifications must have a calendar schedule selected.", "calendarSchedule");
			}
		}
	}


	public NotificationGeneratorService<NotificationDefinition> getNotificationGeneratorService() {
		return this.notificationGeneratorService;
	}


	public void setNotificationGeneratorService(NotificationGeneratorService<NotificationDefinition> notificationGeneratorService) {
		this.notificationGeneratorService = notificationGeneratorService;
	}
}
