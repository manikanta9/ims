package com.clifton.notification.definition;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.notification.definition.search.NotificationDefinitionSearchForm;
import com.clifton.notification.definition.search.NotificationHistorySearchForm;
import com.clifton.notification.definition.search.NotificationTypeSearchForm;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.label.SystemLabel;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;


/**
 * The <code>NotificationDefinitionService</code> defines the methods for retrieving/updating
 * NotificationDefinition & related entities
 *
 * @author manderson
 */
public interface NotificationDefinitionService<T extends NotificationDefinition> {

	////////////////////////////////////////////////////////////////////////////
	//////          Notification Definition Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	public T getNotificationDefinition(int id);


	public List<NotificationDefinition> getNotificationDefinitionGenericList(NotificationDefinitionSearchForm searchForm);


	public T copyNotificationDefinition(int id, String name);


	public void deleteNotificationDefinition(int id);


	////////////////////////////////////////////////////////////////////////////
	//////      Instant Notification Definition Business Methods       /////////
	////////////////////////////////////////////////////////////////////////////


	public List<NotificationDefinitionInstant> getNotificationDefinitionInstantList(boolean activeOnly);


	public NotificationDefinitionInstant saveNotificationDefinitionInstant(NotificationDefinitionInstant bean);


	////////////////////////////////////////////////////////////////////////////
	//////       Batch Notification Definition Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public List<NotificationDefinitionBatch> getNotificationDefinitionBatchListActiveOnDate(final Date startDateTime);


	public List<NotificationDefinitionBatch> getNotificationDefinitionBatchListForTypeActiveOnDate(short typeId, final Date startDateTime);


	public NotificationDefinitionBatch saveNotificationDefinitionBatch(NotificationDefinitionBatch bean);


	public DataTable getNotificationDefinitionBatchDataTableResults(NotificationDefinitionBatch bean);


	////////////////////////////////////////////////////////////////////////////
	/////        Notification Default Text Business Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns String representation of the standard text for
	 * the given Definition.  i.e. If it's an instant definition
	 * the standard text would be the field names & values.  If it's a
	 * batch definition, the standard text would represent the table of results
	 * or each row depending on its configuration
	 */
	@ResponseBody
	@SecureMethod(dtoClass = NotificationDefinition.class)
	public String getNotificationDefaultText(int id);


	////////////////////////////////////////////////////////////////////////////
	////////         Notification Recipient Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	public NotificationRecipient getNotificationRecipient(int id);


	public List<NotificationRecipient> getNotificationRecipientListByNotificationDefinition(int notificationDefinitionId);


	/**
	 * Returns a list of definitions ids:
	 * securityUserId is a direct recipient of a notification or a recipient through the security group(s) the user is associated with
	 * or
	 * securityGroupId is a direct recipient of a notification
	 * <p>
	 * If securityUserId and securityGroupId IS NULL returns null
	 *
	 * @param securityUserId
	 * @param securityGroupId
	 * @param excludeEmpty    - If true, will filter definition list to exclude those that do not have any notifications associated with them
	 */
	@SecureMethod(dtoClass = NotificationRecipient.class)
	public List<Integer> getNotificationDefinitionIdsForRecipientUserOrGroup(Short securityUserId, Short securityGroupId, boolean excludeEmpty);


	/**
	 * Returns a list of definitions ids where security user is a direct recipient of a notification
	 * or a recipient through the security group(s) the user is associated with
	 * <p>
	 * If securityUserId IS NULL returns null
	 *
	 * @param securityUserId
	 * @param excludeEmpty   - If true, will filter definition list to exclude those that do not have any notifications associated with them
	 */
	@SecureMethod(dtoClass = NotificationRecipient.class)
	public List<Integer> getNotificationDefinitionIdsForRecipientUser(Short securityUserId, boolean excludeEmpty);


	/**
	 * Returns a list of definitions ids where the security group is a direct recipient of a notification.
	 * <p>
	 * If securityGroupId IS NULL returns null
	 *
	 * @param securityGroupId
	 * @param excludeEmpty    - If true, will filter definition list to exclude those that do not have any notifications associated with them
	 */
	@SecureMethod(dtoClass = NotificationRecipient.class)
	public List<Integer> getNotificationDefinitionIdsForRecipientGroup(Short securityGroupId, boolean excludeEmpty);


	/**
	 * Creates a new {@link NotificationRecipient} for the selected {@link SecurityUser} and {@link NotificationDefinition}
	 *
	 * @param userId
	 * @param notificationDefinitionId
	 */
	@SecureMethod(dtoClass = NotificationDefinition.class)
	public void linkNotificationDefinitionToSecurityUser(short userId, int notificationDefinitionId);


	/**
	 * Creates a new {@link NotificationRecipient} for the selected {@link SecurityGroup} and {@link NotificationDefinition}
	 *
	 * @param groupId
	 * @param notificationDefinitionId
	 */
	@SecureMethod(dtoClass = NotificationDefinition.class)
	public void linkNotificationDefinitionToSecurityGroup(short groupId, int notificationDefinitionId);


	public void deleteNotificationRecipient(int id);


	////////////////////////////////////////////////////////////////////////////
	//////        Notification Definition Link Business Methods           //////
	////////////////////////////////////////////////////////////////////////////


	public NotificationDefinitionLink getNotificationDefinitionLink(int id);


	public List<NotificationDefinitionLink> getNotificationDefinitionLinkListByDefinition(int definitionId);


	/**
	 * Creates a new {@link NotificationDefinitionLink} for the selected FKFieldID and {@link NotificationDefinition}
	 *
	 * @param fkFieldId
	 * @param definitionId
	 */
	@SecureMethod(dtoClass = NotificationDefinition.class)
	public void linkNotificationDefinitionToEntity(int fkFieldId, int definitionId);


	public void deleteNotificationDefinitionLinkByDefinitionAndEntity(int definitionId, int fkFieldId);


	public List<SystemLabel> getNotificationDefinitionLinkEntityAvailableList(int definitionId, NotificationDefinitionSearchForm searchForm);


	public List<SystemLabel> getNotificationDefinitionLinkEntitySelectedList(int definitionId);

	////////////////////////////////////////////////////////////////////////////
	//////              Notification Type Business Methods                //////
	////////////////////////////////////////////////////////////////////////////


	public NotificationType getNotificationType(short id);


	public List<NotificationType> getNotificationTypeList(NotificationTypeSearchForm searchForm);


	public NotificationType saveNotificationType(NotificationType bean);


	public void deleteNotificationType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////            Notification History Business Methods        ///////////
	////////////////////////////////////////////////////////////////////////////


	public NotificationHistory getNotificationHistory(int id);


	public NotificationHistory getNotificationHistoryByDate(int definitionId, Date scheduledDate);


	public List<NotificationHistory> getNotificationHistoryList(NotificationHistorySearchForm searchForm);


	public NotificationHistory saveNotificationHistory(NotificationHistory bean);
}
