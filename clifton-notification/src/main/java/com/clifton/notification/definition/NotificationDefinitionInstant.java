package com.clifton.notification.definition;


import com.clifton.notification.generator.NotificationGeneratorObserver;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;

import javax.persistence.Table;


/**
 * The <code>NotificationDefinitionInstant</code> provides the definition of an instant notification.
 * <p/>
 * Instant Notifications are generated through the {@link NotificationGeneratorObserver} class which adds
 * observers to the selected {@link SystemTable} dao after inserts/updates/deletes defined in this class.
 * <p/>
 * The  {@link SystemCondition} field can be used to determine whether or not the entity should generate a notification.
 * i.e. Send notification when an amount entered > 1000.
 *
 * @author manderson
 */
@Table(name = "NotificationDefinition")
public class NotificationDefinitionInstant extends NotificationDefinition {

	private SystemCondition condition;
	private boolean insertEvent;
	private boolean updateEvent;
	private boolean deleteEvent;


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	@Override
	public boolean isInstant() {
		return true;
	}


	@Override
	public String getTemplateConfigBeanName() {
		return "bean";
	}


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	public SystemCondition getCondition() {
		return this.condition;
	}


	public void setCondition(SystemCondition condition) {
		this.condition = condition;
	}


	public boolean isInsertEvent() {
		return this.insertEvent;
	}


	public void setInsertEvent(boolean insertEvent) {
		this.insertEvent = insertEvent;
	}


	public boolean isUpdateEvent() {
		return this.updateEvent;
	}


	public void setUpdateEvent(boolean updateEvent) {
		this.updateEvent = updateEvent;
	}


	public boolean isDeleteEvent() {
		return this.deleteEvent;
	}


	public void setDeleteEvent(boolean deleteEvent) {
		this.deleteEvent = deleteEvent;
	}
}
