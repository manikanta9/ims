package com.clifton.notification.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.notification.definition.NotificationStatuses;


public class NotificationDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField
	private NotificationStatuses status;

	@SearchField(searchField = "priority.id", sortField = "priority.order")
	private Short priorityId;

	@SearchField(searchField = "order", searchFieldPath = "priority")
	private Integer priorityOrder;

	@SearchField(searchField = "type.definitionModifyCondition.id")
	private Integer entityModifyConditionId;

	@SearchField(searchField = "type.runSystemCondition.id")
	private Integer runSystemConditionId;

	@SearchField(searchField = "calendarSchedule.id", sortField = "calendarSchedule.name")
	private Integer calendarScheduleId;

	@SearchField
	private Boolean active;

	@SearchField
	private Boolean systemAcknowledged;

	@SearchField(searchField = "table.id")
	private Short tableId;

	@SearchField(searchField = "linkedTable.id")
	private Short linkedTableId;

	// Custom Search Fields - table name checks table & linked table
	private String tableName;
	// EntityId only - only applies to linked table
	private Integer entityId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Integer getEntityId() {
		return this.entityId;
	}


	public void setEntityId(Integer entityId) {
		this.entityId = entityId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Boolean getSystemAcknowledged() {
		return this.systemAcknowledged;
	}


	public void setSystemAcknowledged(Boolean systemAcknowledged) {
		this.systemAcknowledged = systemAcknowledged;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public Integer getEntityModifyConditionId() {
		return this.entityModifyConditionId;
	}


	public void setEntityModifyConditionId(Integer entityModifyConditionId) {
		this.entityModifyConditionId = entityModifyConditionId;
	}


	public Integer getRunSystemConditionId() {
		return this.runSystemConditionId;
	}


	public void setRunSystemConditionId(Integer runSystemConditionId) {
		this.runSystemConditionId = runSystemConditionId;
	}


	public NotificationStatuses getStatus() {
		return this.status;
	}


	public void setStatus(NotificationStatuses status) {
		this.status = status;
	}


	public Integer getCalendarScheduleId() {
		return this.calendarScheduleId;
	}


	public void setCalendarScheduleId(Integer calendarScheduleId) {
		this.calendarScheduleId = calendarScheduleId;
	}


	public Integer getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Integer priorityOrder) {
		this.priorityOrder = priorityOrder;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public Short getLinkedTableId() {
		return this.linkedTableId;
	}


	public void setLinkedTableId(Short linkedTableId) {
		this.linkedTableId = linkedTableId;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
