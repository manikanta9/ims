package com.clifton.notification.subscription;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.subscription.message.NotificationCountSubscriptionMessage;
import com.clifton.notification.subscription.message.NotificationPopupSubscriptionMessage;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;

import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * The service interface for subscriptions to {@link Notification}s. This interface provides methods for subscribing to {@link Notification} changes.
 * <p>
 * Updates for the provided channels are sent to users through the {@link NotificationSubscriptionObserver} DAO observer.
 *
 * @author MikeH
 */
public interface NotificationSubscriptionService {

	/**
	 * The WebSocket channel for user-specific notification counts.
	 */
	public static final String CHANNEL_USER_TOPIC_NOTIFICATION_COUNT = "/user/topic/notification/count";
	/**
	 * The WebSocket channel for user-specific notification pop-ups.
	 */
	public static final String CHANNEL_USER_TOPIC_NOTIFICATION_POPUP = "/user/topic/notification/popup";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the notification count which will appear for the given user. This method handles the STOMP <tt>SUBSCRIBE</tt> frames for the {@link
	 * #CHANNEL_USER_TOPIC_NOTIFICATION_COUNT} channel.
	 *
	 * @see NotificationSubscriptionUtils#PRIORITY_THRESHOLD_VISIBLE
	 */
	@SecureMethod(disableSecurity = true)
	@SubscribeMapping(CHANNEL_USER_TOPIC_NOTIFICATION_COUNT)
	@DoNotAddRequestMapping
	public NotificationCountSubscriptionMessage getNotificationCountSubscriptionMessageForUser(SecurityUser user);


	/**
	 * Retrieves all pop-up notifications for the given user. This method handles the STOMP <tt>SUBSCRIBE</tt> frames for the {@link #CHANNEL_USER_TOPIC_NOTIFICATION_POPUP}
	 * channel.
	 *
	 * @see NotificationSubscriptionUtils#PRIORITY_THRESHOLD_CRITICAL
	 */
	@SecureMethod(disableSecurity = true)
	@SubscribeMapping(CHANNEL_USER_TOPIC_NOTIFICATION_POPUP)
	@DoNotAddRequestMapping
	public NotificationPopupSubscriptionMessage getNotificationPopupSubscriptionMessageForUser(SecurityUser user);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of notifications which qualify criteria to be sent as visible notifications for the given user. These notifications will be included in the current active
	 * notification count for the given user.
	 *
	 * @see NotificationSubscriptionUtils#isVisibleNotification(Notification)
	 */
	@DoNotAddRequestMapping
	public List<Notification> getVisibleNotificationListForUser(SecurityUser user);


	/**
	 * Filters the given list of notifications to those which qualify criteria to be sent as pop-up notifications for the given user.
	 * <p>
	 * A map for caching must be passed to this method. This cache is used for caching {@link SecurityUser} collections by {@link SecurityGroup} association and can be used to
	 * improve performance when querying for pop-up notifications for multiple users.
	 *
	 * @param user                           the user for whom pop-up notifications should be filtered
	 * @param userVisibleNotificationList    the list of {@link #getVisibleNotificationListForUser(SecurityUser) visible notifications} for the given user
	 * @param securityUserListByGroupIdCache the cache object to use for caching {@link SecurityUser} collections by {@link SecurityGroup}
	 * @see NotificationSubscriptionUtils#isPopupNotificationForUser(Notification, Collection, SecurityUser)
	 */
	@DoNotAddRequestMapping
	public List<Notification> getPopupNotificationListForUser(SecurityUser user, List<Notification> userVisibleNotificationList, Map<Short, List<SecurityUser>> securityUserListByGroupIdCache);
}
