package com.clifton.notification.subscription.message;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.subscription.NotificationSubscriptionService;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monitorjbl.json.JsonView;
import com.monitorjbl.json.Match;

import java.util.Collections;
import java.util.List;


/**
 * The message DTO for sending subscription information regarding changes to {@link Notification} entities which should be displayed as pop-up notifications.
 *
 * @author MikeH
 * @see NotificationSubscriptionService
 */
public class NotificationPopupSubscriptionMessage {

	/**
	 * If {@code true}, this message indicates that the client-side data should be reset before processing the remainder of this message.
	 */
	private final boolean reset;
	/**
	 * The list of added notifications.
	 */
	private final List<Notification> added;
	/**
	 * The list of removed notifications.
	 */
	private final List<Notification> removed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private NotificationPopupSubscriptionMessage(boolean reset, List<Notification> added, List<Notification> removed) {
		this.reset = reset;
		this.added = ObjectUtils.coalesce(added, Collections.emptyList());
		this.removed = ObjectUtils.coalesce(removed, Collections.emptyList());
	}


	public static NotificationPopupSubscriptionMessage reset(List<Notification> added) {
		return new NotificationPopupSubscriptionMessage(true, added, null);
	}


	public static NotificationPopupSubscriptionMessage add(Notification added) {
		return new NotificationPopupSubscriptionMessage(false, Collections.singletonList(added), null);
	}


	public static NotificationPopupSubscriptionMessage remove(Notification removed) {
		return new NotificationPopupSubscriptionMessage(false, null, Collections.singletonList(removed));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@JsonIgnore
	public List<Notification> getAdded() {
		return this.added;
	}


	@JsonGetter("added")
	public JsonView<List<Notification>> getAddedView() {
		return JsonView.with(this.added).onClass(Notification.class, Match.match()
				.exclude("*")
				.include("assigneeUser.id", "assigneeGroup.id", "definition.id", "definition.name", "fkFieldId", "id", "priority.cssStyle", "priority.name", "priority.order", "subject", "table.detailScreenClass"));
	}


	@JsonIgnore
	public List<Notification> getRemoved() {
		return this.removed;
	}


	@JsonGetter("removed")
	public List<Integer> getRemovedView() {
		return CollectionUtils.getConverted(this.removed, BaseSimpleEntity::getId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isReset() {
		return this.reset;
	}
}
