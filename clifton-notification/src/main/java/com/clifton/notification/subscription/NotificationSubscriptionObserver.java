package com.clifton.notification.subscription;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.NotificationRecipient;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.subscription.message.NotificationCountSubscriptionMessage;
import com.clifton.notification.subscription.message.NotificationPopupSubscriptionMessage;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserGroup;
import com.clifton.security.user.SecurityUserService;
import com.clifton.websocket.WebSocketHandler;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


/**
 * The DAO observer for {@link Notification} subscription operations.
 * <p>
 * This observer uses {@link WebSocketHandler WebSockets} to send messages to subscribed clients. Clients may subscribe through the channels listed in {@link
 * NotificationSubscriptionService}.
 *
 * @author MikeH
 */
@Component
public class NotificationSubscriptionObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private NotificationDefinitionService<?> notificationDefinitionService;
	private NotificationSubscriptionService notificationSubscriptionService;
	private SecurityUserService securityUserService;
	private WebSocketHandler webSocketHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// Cache reference to original bean
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// Do not run observer during exceptions
		if (e != null) {
			return;
		}

		// Prepare data
		T originalBean = determineOriginalBean(dao, event, bean);
		T newBean = determineNewBean(event, bean);
		Collection<SecurityUser> subscribedUserList = new HashSet<>();
		subscribedUserList.addAll(getWebSocketHandler().getSubscribedUserList(NotificationSubscriptionService.CHANNEL_USER_TOPIC_NOTIFICATION_COUNT));
		subscribedUserList.addAll(getWebSocketHandler().getSubscribedUserList(NotificationSubscriptionService.CHANNEL_USER_TOPIC_NOTIFICATION_POPUP));

		// Handle events
		if (bean instanceof Notification) {
			handleNotificationUpdate((Notification) originalBean, (Notification) newBean, subscribedUserList);
		}
		else if (bean instanceof NotificationRecipient) {
			Collection<SecurityUser> changedUserSet = new HashSet<>();
			changedUserSet.addAll(getUserListForRecipient((NotificationRecipient) originalBean, subscribedUserList));
			changedUserSet.addAll(getUserListForRecipient((NotificationRecipient) newBean, subscribedUserList));
			refreshUserNotificationLists(changedUserSet);
		}
		else if (bean instanceof SecurityUserGroup) {
			Collection<SecurityUser> changedUserSet = new HashSet<>();
			changedUserSet.addAll(getUserListForSecurityUserGroup((SecurityUserGroup) originalBean, subscribedUserList));
			changedUserSet.addAll(getUserListForSecurityUserGroup((SecurityUserGroup) newBean, subscribedUserList));
			refreshUserNotificationLists(changedUserSet);
		}
		else {
			// Unrecognized event
			throw new IllegalStateException(String.format("Unexpected branch reached. The bean type [%s] is not recognized by the observer.", bean.getClass()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Handles a single {@link Notification} update, sending incremental updates to all subscribed users.
	 */
	private void handleNotificationUpdate(Notification originalNotification, Notification newNotification, Collection<SecurityUser> subscribedUserList) {
		// Guard-clause: No removals or additions are needed; return early
		if (!NotificationSubscriptionUtils.isVisibleNotification(originalNotification)
				&& !NotificationSubscriptionUtils.isVisibleNotification(newNotification)) {
			return;
		}

		// Get users who can see the notification
		Collection<SecurityUser> originalAssigneeList = getNotificationAssigneeList(originalNotification, subscribedUserList);
		Collection<SecurityUser> newAssigneeList = getNotificationAssigneeList(newNotification, subscribedUserList);
		Collection<SecurityUser> originalUserList = getNotificationAllRecipientList(originalNotification, subscribedUserList, originalAssigneeList);
		Collection<SecurityUser> newUserList = getNotificationAllRecipientList(newNotification, subscribedUserList, newAssigneeList);

		// Send updates
		handleNotificationCountUpdate(originalNotification, newNotification, originalUserList, newUserList);
		handleNotificationPopupUpdate(originalNotification, newNotification, originalAssigneeList, newAssigneeList, originalUserList, newUserList);
	}


	/**
	 * Handles all {@link NotificationSubscriptionService#CHANNEL_USER_TOPIC_NOTIFICATION_COUNT} channel updates for the given notification change.
	 */
	private void handleNotificationCountUpdate(Notification originalNotification, Notification newNotification, Collection<SecurityUser> originalUserList, Collection<SecurityUser> newUserList) {
		// Determine users for count decrements and increments
		Collection<SecurityUser> decrementCountUserList = new HashSet<>();
		Collection<SecurityUser> incrementCountUserList = new HashSet<>();
		if (NotificationSubscriptionUtils.isVisibleNotification(originalNotification)) {
			decrementCountUserList.addAll(originalUserList);
		}
		if (NotificationSubscriptionUtils.isVisibleNotification(newNotification)) {
			incrementCountUserList.addAll(newUserList);
		}

		// Prevent redundant operations for users
		Collection<SecurityUser> redundantUserList = CollectionUtils.getIntersection(decrementCountUserList, incrementCountUserList);
		decrementCountUserList.removeAll(redundantUserList);
		incrementCountUserList.removeAll(redundantUserList);

		// Send count updates
		decrementCountUserList.forEach(user -> sendNotificationCountMessage(user, NotificationCountSubscriptionMessage.DECREMENT));
		incrementCountUserList.forEach(user -> sendNotificationCountMessage(user, NotificationCountSubscriptionMessage.INCREMENT));
	}


	/**
	 * Handles all {@link NotificationSubscriptionService#CHANNEL_USER_TOPIC_NOTIFICATION_POPUP} channel updates for the given notification change.
	 */
	private void handleNotificationPopupUpdate(Notification originalNotification, Notification newNotification, Collection<SecurityUser> originalAssigneeList, Collection<SecurityUser> newAssigneeList, Collection<SecurityUser> originalUserList, Collection<SecurityUser> newUserList) {
		// Determine users for pop-up removals and additions
		Collection<SecurityUser> removePopupUserList = new HashSet<>();
		Collection<SecurityUser> addPopupUserList = new HashSet<>();
		if (NotificationSubscriptionUtils.isCriticalNotification(originalNotification)) {
			removePopupUserList.addAll(CollectionUtils.getFiltered(originalUserList,
					user -> NotificationSubscriptionUtils.isPopupNotificationForUser(originalNotification, originalAssigneeList, user)));
		}
		if (NotificationSubscriptionUtils.isCriticalNotification(newNotification)) {
			addPopupUserList.addAll(CollectionUtils.getFiltered(newUserList,
					user -> NotificationSubscriptionUtils.isPopupNotificationForUser(newNotification, newAssigneeList, user)));
		}

		// Prevent redundant operations for users; duplicates are not removed from the "add" list since content for these messages may include detail modifications
		Collection<SecurityUser> redundantUserList = CollectionUtils.getIntersection(removePopupUserList, addPopupUserList);
		removePopupUserList.removeAll(redundantUserList);

		// Send pop-up updates
		removePopupUserList.forEach(user -> sendNotificationPopupMessage(user, NotificationPopupSubscriptionMessage.remove(originalNotification)));
		addPopupUserList.forEach(user -> sendNotificationPopupMessage(user, NotificationPopupSubscriptionMessage.add(newNotification)));
	}


	/**
	 * Re-evaluates notification counts and pop-ups for all users in the given user list.
	 * <p>
	 * This sends the full state of subscribed notifications for each subscribed user in the given list of users.
	 */
	private void refreshUserNotificationLists(Collection<SecurityUser> userList) {
		Map<Short, List<SecurityUser>> securityUserListByGroupIdCache = new HashMap<>();
		for (SecurityUser user : userList) {
			// Send notification count update for user
			List<Notification> userNotificationList = getNotificationSubscriptionService().getVisibleNotificationListForUser(user);
			sendNotificationCountMessage(user, NotificationCountSubscriptionMessage.reset(userNotificationList.size()));

			// Send notification pop-up update for user
			List<Notification> userNotificationPopupList = getNotificationSubscriptionService().getPopupNotificationListForUser(user, userNotificationList, securityUserListByGroupIdCache);
			sendNotificationPopupMessage(user, NotificationPopupSubscriptionMessage.reset(userNotificationPopupList));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Notification Sending Methods                    ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sends the {@link NotificationCountSubscriptionMessage} to the given user.
	 */
	private void sendNotificationCountMessage(SecurityUser user, NotificationCountSubscriptionMessage message) {
		getWebSocketHandler().sendMessageToUser(user, NotificationSubscriptionService.CHANNEL_USER_TOPIC_NOTIFICATION_COUNT, message);
	}


	/**
	 * Sends the {@link NotificationPopupSubscriptionMessage} to the given user.
	 */
	private void sendNotificationPopupMessage(SecurityUser user, NotificationPopupSubscriptionMessage message) {
		getWebSocketHandler().sendMessageToUser(user, NotificationSubscriptionService.CHANNEL_USER_TOPIC_NOTIFICATION_POPUP, message);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Notification Recipient Retrieval Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the matching list of assignees, including assignee group members, for the given notification. The resulting list is filtered to those matching a member in the given
	 * subscribed user list.
	 */
	private Collection<SecurityUser> getNotificationAssigneeList(Notification notification, Collection<SecurityUser> subscribedUserList) {
		// Guard-clause: Use empty list if no notification was provided
		if (notification == null) {
			return Collections.emptyList();
		}

		// Get assignee users
		Collection<SecurityUser> assigneeUserList = new HashSet<>();
		if (notification.getAssigneeUser() != null) {
			assigneeUserList.add(notification.getAssigneeUser());
		}
		if (notification.getAssigneeGroup() != null) {
			assigneeUserList.addAll(getSecurityUserService().getSecurityUserListByGroup(notification.getAssigneeGroup().getId()));
		}

		// Filter to applicable users
		return CollectionUtils.getIntersection(assigneeUserList, subscribedUserList);
	}


	/**
	 * Gets the matching list of recipients, including all assignees, for the given notification. The resulting list is filtered to those matching a member in the given subscribed
	 * user list.
	 */
	private Collection<SecurityUser> getNotificationAllRecipientList(Notification notification, Collection<SecurityUser> subscribedUserList, Collection<SecurityUser> assigneeGroupUserList) {
		// Guard-clause: Use empty list if no notification was provided
		if (notification == null) {
			return Collections.emptyList();
		}

		// Add recipient users
		Collection<SecurityUser> userList = new HashSet<>(assigneeGroupUserList);
		List<NotificationRecipient> recipientList = getNotificationDefinitionService().getNotificationRecipientListByNotificationDefinition(notification.getDefinition().getId());
		List<SecurityUser> recipientUserList = CollectionUtils.getConvertedFlattened(recipientList, recipient -> getUserListForRecipient(recipient, subscribedUserList));
		userList.addAll(recipientUserList);
		return userList;
	}


	/**
	 * Gets the list of all users corresponding to the given {@link NotificationRecipient} entity. This includes the direct recipient as well as all group members. The resulting
	 * list is filtered to those matching a member in the given subscribed user list.
	 */
	private List<SecurityUser> getUserListForRecipient(NotificationRecipient recipient, Collection<SecurityUser> subscribedUserList) {
		// Guard-clause: No recipient provided
		if (recipient == null) {
			return Collections.emptyList();
		}
		return getMatchingUserList(recipient.getSecurityUser(), recipient.getSecurityGroup(), subscribedUserList);
	}


	/**
	 * Gets the list of all users corresponding to the given {@link SecurityUserGroup} entity. This includes users on both sides of the association: Both the user and all members
	 * of the associated group are attached. The resulting list is filtered to those matching a member in the given subscribed user list.
	 */
	private List<SecurityUser> getUserListForSecurityUserGroup(SecurityUserGroup securityUserGroup, Collection<SecurityUser> subscribedUserList) {
		// Guard-clause: No group provided
		if (securityUserGroup == null) {
			return Collections.emptyList();
		}
		return getMatchingUserList(securityUserGroup.getReferenceOne(), securityUserGroup.getReferenceTwo(), subscribedUserList);
	}


	/**
	 * Gets the list including the given {@link SecurityUser} and all users corresponding to the given {@link SecurityGroup}. The resulting list is filtered to those matching a
	 * member in the given subscribed user list.
	 */
	private List<SecurityUser> getMatchingUserList(SecurityUser securityUser, SecurityGroup securityGroup, Collection<SecurityUser> subscribedUserList) {
		Collection<SecurityUser> userList = new HashSet<>();
		if (securityUser != null) {
			userList.add(securityUser);
		}
		if (securityGroup != null) {
			userList.addAll(getSecurityUserService().getSecurityUserListByGroup(securityGroup.getId()));
		}

		// Filter to applicable users
		return CollectionUtils.getIntersection(userList, subscribedUserList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            DAO Bean Retrieval Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	private T determineOriginalBean(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		final T originalBean;
		if (event.isDelete()) {
			originalBean = bean;
		}
		else if (event.isUpdate()) {
			originalBean = getOriginalBean(dao, bean);
		}
		else if (event.isInsert()) {
			originalBean = null;
		}
		else {
			throw new IllegalStateException(String.format("Unexpected branch reached. The DAO event type [%s] is not recognized by the observer.", event));
		}
		return originalBean;
	}


	private T determineNewBean(DaoEventTypes event, T bean) {
		final T newBean;
		if (event.isDelete()) {
			newBean = null;
		}
		else if (event.isUpdate()) {
			newBean = bean;
		}
		else if (event.isInsert()) {
			newBean = bean;
		}
		else {
			throw new IllegalStateException(String.format("Unexpected branch reached. The DAO event type [%s] is not recognized by the observer.", event));
		}
		return newBean;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public NotificationDefinitionService<?> getNotificationDefinitionService() {
		return this.notificationDefinitionService;
	}


	public void setNotificationDefinitionService(NotificationDefinitionService<?> notificationDefinitionService) {
		this.notificationDefinitionService = notificationDefinitionService;
	}


	public NotificationSubscriptionService getNotificationSubscriptionService() {
		return this.notificationSubscriptionService;
	}


	public void setNotificationSubscriptionService(NotificationSubscriptionService notificationSubscriptionService) {
		this.notificationSubscriptionService = notificationSubscriptionService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}
}
