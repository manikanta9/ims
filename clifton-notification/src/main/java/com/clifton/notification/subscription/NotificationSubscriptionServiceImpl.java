package com.clifton.notification.subscription;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.generator.NotificationGeneratorService;
import com.clifton.notification.generator.NotificationSearchForm;
import com.clifton.notification.subscription.message.NotificationCountSubscriptionMessage;
import com.clifton.notification.subscription.message.NotificationPopupSubscriptionMessage;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The default implementation of {@link NotificationSubscriptionService}.
 *
 * @author MikeH
 */
@Controller
public class NotificationSubscriptionServiceImpl implements NotificationSubscriptionService {

	private NotificationGeneratorService<?> notificationGeneratorService;
	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public NotificationCountSubscriptionMessage getNotificationCountSubscriptionMessageForUser(SecurityUser user) {
		List<Notification> notificationList = getVisibleNotificationListForUser(user);
		return NotificationCountSubscriptionMessage.reset(notificationList.size());
	}


	@Override
	public NotificationPopupSubscriptionMessage getNotificationPopupSubscriptionMessageForUser(SecurityUser user) {
		// Get all notifications with qualifying priority level
		NotificationSearchForm searchForm = new NotificationSearchForm();
		searchForm.setAcknowledged(false);
		searchForm.setUserId(user.getId());
		searchForm.addSearchRestriction(new SearchRestriction("priorityOrder", ComparisonConditions.LESS_THAN_OR_EQUALS, NotificationSubscriptionUtils.PRIORITY_THRESHOLD_CRITICAL));
		List<Notification> notificationList = getNotificationGeneratorService().getNotificationList(searchForm);

		// Filter notifications to those applicable to user
		List<Notification> userNotificationPopupList = getPopupNotificationListForUser(user, notificationList, new HashMap<>());
		return NotificationPopupSubscriptionMessage.reset(userNotificationPopupList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Notification> getVisibleNotificationListForUser(SecurityUser user) {
		NotificationSearchForm searchForm = new NotificationSearchForm();
		searchForm.setAcknowledged(false);
		searchForm.setUserId(user.getId());
		searchForm.addSearchRestriction(new SearchRestriction("priorityOrder", ComparisonConditions.LESS_THAN_OR_EQUALS, NotificationSubscriptionUtils.PRIORITY_THRESHOLD_VISIBLE));
		return getNotificationGeneratorService().getNotificationList(searchForm);
	}


	@Override
	public List<Notification> getPopupNotificationListForUser(SecurityUser user, List<Notification> userVisibleNotificationList, Map<Short, List<SecurityUser>> securityUserListByGroupIdCache) {
		return CollectionUtils.getStream(userVisibleNotificationList)
				.filter(NotificationSubscriptionUtils::isCriticalNotification)
				.filter(notification -> {
					// Determine if notification qualifies for user pop-up using shared user-list-by-group cache
					List<SecurityUser> assigneeGroupUserList = notification.getAssigneeGroup() != null
							? securityUserListByGroupIdCache.computeIfAbsent(notification.getAssigneeGroup().getId(), getSecurityUserService()::getSecurityUserListByGroup)
							: Collections.emptyList();
					return NotificationSubscriptionUtils.isPopupNotificationForUser(notification, assigneeGroupUserList, user);
				})
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public NotificationGeneratorService<?> getNotificationGeneratorService() {
		return this.notificationGeneratorService;
	}


	public void setNotificationGeneratorService(NotificationGeneratorService<?> notificationGeneratorService) {
		this.notificationGeneratorService = notificationGeneratorService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
