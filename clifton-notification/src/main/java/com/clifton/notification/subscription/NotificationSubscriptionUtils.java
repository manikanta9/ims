package com.clifton.notification.subscription;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.notification.definition.NotificationRecipient;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.subscription.message.NotificationPopupSubscriptionMessage;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.priority.SystemPriority;

import java.util.Collection;


/**
 * A utility class for notification subscription methods.
 *
 * @author MikeH
 */
public class NotificationSubscriptionUtils {

	/**
	 * The priority threshold for visible messages. All notifications with an equal or higher priority (i.e. a lower {@link SystemPriority#order} value) than this value will be
	 * included in the current notification count for assignees and recipients.
	 */
	public static final int PRIORITY_THRESHOLD_VISIBLE = 35;
	/**
	 * The priority threshold for critical messages. All notifications with an equal or higher priority (i.e. a lower {@link SystemPriority#order} value) than this value will be
	 * sent to assignees as {@link NotificationPopupSubscriptionMessage} messages.
	 */
	public static final int PRIORITY_THRESHOLD_CRITICAL = 10;
	/**
	 * The priority threshold for immediate messages. All notifications with an equal or higher priority (i.e. a lower {@link SystemPriority#order} value) than this value will be
	 * sent as {@link NotificationPopupSubscriptionMessage} messages to the assignee if an assignee exists, or to all {@link NotificationRecipient recipients} if no assignee
	 * exists.
	 */
	public static final int PRIORITY_THRESHOLD_IMMEDIATE = 5;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private NotificationSubscriptionUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Notification Visiblity Utility Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns {@code true} if the notification is a <i>visible notification</i>, meaning that it will show up in notification counts for all assignees and recipients.
	 */
	public static boolean isVisibleNotification(Notification notification) {
		return notification != null
				&& !notification.isAcknowledged()
				&& notification.getPriority().getOrder() <= PRIORITY_THRESHOLD_VISIBLE;
	}


	/**
	 * Returns {@code true} if the notification is a <i>critical notification</i>, meaning that it will show up in notification counts for all assignees and recipients and that it
	 * will show up in notification pop-ups for assignees.
	 */
	public static boolean isCriticalNotification(Notification notification) {
		return isVisibleNotification(notification) && notification.getPriority().getOrder() <= PRIORITY_THRESHOLD_CRITICAL;
	}


	/**
	 * Returns {@code true} if the notification is an <i>immediate notification</i>, meaning that it will show up in notification pop-ups for the assignee or for any recipients if
	 * no assignee is set. These are a subset of {@link #isCriticalNotification(Notification) critical notifications}.
	 */
	public static boolean isImmediateNotification(Notification notification) {
		return isVisibleNotification(notification) && notification.getPriority().getOrder() <= PRIORITY_THRESHOLD_IMMEDIATE;
	}


	/**
	 * Returns {@code true} if the notification is a <i>pop-up notification</i> for the given recipient user, meaning either that it is a {@link
	 * #isCriticalNotification(Notification) critical notification} and is assigned to the user or that it is an {@link #isImmediateNotification(Notification) immediate
	 * notification} and has no assignee. As a result, it will show up in the notification pop-up for the given user if the user is a recipient or assignee.
	 * <p>
	 * This method assumes that the user is a recipient or assignee for the given notification. This should be validated before executing this method.
	 */
	public static boolean isPopupNotificationForUser(Notification notification, Collection<SecurityUser> assigneeGroupUserList, SecurityUser recipientUser) {
		final boolean popupForUser;
		if (isImmediateNotification(notification) && notification.getAssigneeUser() == null && notification.getAssigneeGroup() == null) {
			// Immediate notifications show up for assignees if present or for recipients otherwise
			popupForUser = true;
		}
		else if (isCriticalNotification(notification) && (CompareUtils.isEqual(notification.getAssigneeUser(), recipientUser) || CollectionUtils.contains(assigneeGroupUserList, recipientUser))) {
			// Critical notifications show up for assignees only
			popupForUser = true;
		}
		else {
			// All other notifications do not pop-up
			popupForUser = false;
		}
		return popupForUser;
	}
}

