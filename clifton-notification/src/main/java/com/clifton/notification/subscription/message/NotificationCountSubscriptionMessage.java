package com.clifton.notification.subscription.message;


import com.clifton.notification.generator.Notification;
import com.clifton.notification.subscription.NotificationSubscriptionService;


/**
 * The message DTO for sending subscription information regarding active {@link Notification} count changes.
 *
 * @author MikeH
 * @see NotificationSubscriptionService
 */
public class NotificationCountSubscriptionMessage {

	/**
	 * A static message indicating that the number of notifications has been decremented.
	 */
	public static final NotificationCountSubscriptionMessage DECREMENT = NotificationCountSubscriptionMessage.add(-1);
	/**
	 * A static message indicating that the number of notifications has been incremented.
	 */
	public static final NotificationCountSubscriptionMessage INCREMENT = NotificationCountSubscriptionMessage.add(1);

	/**
	 * If {@code true}, this message indicates that the client-side data should be reset before processing the remainder of this message.
	 */
	private final boolean reset;
	/**
	 * The number of notifications added.
	 */
	private final int numAdded;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private NotificationCountSubscriptionMessage(boolean reset, int numAdded) {
		this.reset = reset;
		this.numAdded = numAdded;
	}


	public static NotificationCountSubscriptionMessage reset(int num) {
		return new NotificationCountSubscriptionMessage(true, num);
	}


	public static NotificationCountSubscriptionMessage add(int numAdded) {
		return new NotificationCountSubscriptionMessage(false, numAdded);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getNumAdded() {
		return this.numAdded;
	}


	public boolean isReset() {
		return this.reset;
	}
}
