package com.clifton.notification;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.generator.NotificationGeneratorObserver;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.List;


/**
 * The <code>NotificationRegistratorImpl</code> class is a Spring {@link ApplicationListener} that
 * registers {@link NotificationGeneratorObserver} for DAO's whose table exists
 * in the {@link NotificationDefinition} table: INSTANT NOTIFICATIONS.
 *
 * @author manderson
 */
public class NotificationRegistratorImpl<T extends IdentityObject> implements NotificationRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {

	private NotificationDefinitionService<NotificationDefinitionInstant> notificationDefinitionService;

	private DaoLocator daoLocator;


	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			List<NotificationDefinitionInstant> instantDefinitionList = getNotificationDefinitionService().getNotificationDefinitionInstantList(true);
			for (NotificationDefinitionInstant def : CollectionUtils.getIterable(instantDefinitionList)) {
				registerObservers(def.getTable().getName(), def);
			}
		}
		catch (Throwable e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering instant notifications: " + event, e);
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	public void registerObservers(String tableName, NotificationDefinitionInstant def) {
		// get DAO that's being observed
		ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(tableName);
		DaoEventObserver<T> observer = new NotificationGeneratorObserver<>(def);
		getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(observer, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		// register observers as necessary

		if (def.isInsertEvent()) {
			dao.registerEventObserver(DaoEventTypes.INSERT, observer);
		}

		if (def.isUpdateEvent()) {
			dao.registerEventObserver(DaoEventTypes.UPDATE, observer);
		}

		if (def.isDeleteEvent()) {
			dao.registerEventObserver(DaoEventTypes.DELETE, observer);
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	public void unregisterObservers(String tableName, NotificationDefinitionInstant def) {
		ObserverableDAO<T> dao = (ObserverableDAO<T>) getDaoLocator().locate(tableName);
		DaoEventObserver<T> observer = new NotificationGeneratorObserver<>(def);

		// unregister observers 
		dao.unregisterEventObserver(DaoEventTypes.INSERT, observer);
		dao.unregisterEventObserver(DaoEventTypes.UPDATE, observer);
		dao.unregisterEventObserver(DaoEventTypes.DELETE, observer);
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public NotificationDefinitionService<NotificationDefinitionInstant> getNotificationDefinitionService() {
		return this.notificationDefinitionService;
	}


	public void setNotificationDefinitionService(NotificationDefinitionService<NotificationDefinitionInstant> notificationDefinitionService) {
		this.notificationDefinitionService = notificationDefinitionService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
