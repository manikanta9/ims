package com.clifton.notification.generator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionInstant;

import java.util.Date;
import java.util.List;


/**
 * The <code>NotificationGeneratorService</code> interface defines the methods
 * for generating {@link Notification) for a given bean as well as {@link Notification}
 * database retrieval methods
 *
 * @author manderson
 */
public interface NotificationGeneratorService<T extends IdentityObject> {

	//////////////////////////////////////////////////////////////////////////// 
	//////         Notification Generator Business Methods             ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If generate is true - Generates and Saves a new Notification bean for the given definition
	 * and bean.
	 * <p>
	 * Otherwise, if the system "acknowledges" i.e. not users acknowledge the notification, and generate is false
	 * will see if a warning exists and delete it.
	 */
	@DoNotAddRequestMapping
	public void generateInstantNotification(NotificationDefinitionInstant definition, T bean, boolean generate);


	/**
	 * Generates and Saves a new Notification bean(s) for the given definition
	 * <p>
	 * Scheduled date is defined when running from the NotificationBatchRunner and is used to update
	 * the history associated with the notification run.  If scheduledDate is null, it's considered a "Run Now"
	 * and the schedule date will be defaulted to the current date/time.
	 */
	@DoNotAddRequestMapping
	public void generateBatchedNotification(NotificationDefinitionBatch definition, boolean runIfInactive, Date scheduleDate);


	//////////////////////////////////////////////////////////////////////////// 
	////////            Notification Business Methods                /////////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a new instance of a {@link Notification) this is not saved in the database
	 * but created for subject & text testing purposes.
	 * <p>
	 * For {@link NotificationDefinitionInstant) will pull one bean from the database and if they
	 * don't exist will create a new instance of a bean.
	 * <p>
	 * For (@link NotificationDefinitionBatch) will show results for batching results, or will
	 * show notification for one row of the results.
	 */
	public Notification getNotificationSample(int id, String subjectTemplate, String textTemplate);


	public Notification getNotification(int id);


	/**
	 * Searches for and returns a list of Notifications.  Will only
	 * return records for {@link Notification}s that current user
	 * is a recipient of.
	 */
	@SecureMethod(disableSecurity = true)
	public List<Notification> getNotificationList(NotificationSearchForm searchForm);


	/**
	 * Searches for and returns a List of Notifications based upon search criteria
	 * Does not take into account current user as a recipient, but uses
	 * Notification security (used in setup screens)
	 */
	public List<Notification> getNotificationListSecure(NotificationSearchForm searchForm);


	public Notification saveNotification(Notification bean);


	/**
	 * Assigns multiple notifications to a specific user
	 */
	@SecureMethod(dtoClass = Notification.class)
	public void saveNotificationListAssignee(int[] notificationIds, short userId);


	/**
	 * Assigns multiple notifications to a user group
	 */
	@SecureMethod(dtoClass = Notification.class)
	public void saveNotificationListGroupAssignee(int[] notificationIds, short groupId);


	/**
	 * Acknowledges multiple notifications with comment
	 */
	@SecureMethod(dtoClass = Notification.class)
	public void saveNotificationListAcknowledged(int[] notificationIds, String comment);


	/**
	 * Deletes all notifications for the given definition that haven't been acknowledged
	 */
	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void clearNotificationList(int definitionId);
}
