package com.clifton.notification.generator;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.FalseCriterion;
import com.clifton.core.dataaccess.search.hibernate.expression.InExpressionForNumbers;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.notification.comment.NotificationComment;
import com.clifton.notification.comment.NotificationCommentService;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.notification.definition.NotificationDefinitionLink;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.NotificationHistory;
import com.clifton.notification.definition.NotificationStatuses;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserGroup;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.priority.SystemPriorityService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>NotificationGeneratorServiceImpl</code> class implements the {@link NotificationGeneratorService}
 * interface.
 *
 * @author manderson
 */
@Service
public class NotificationGeneratorServiceImpl<T extends IdentityObject, N extends NotificationDefinition> implements NotificationGeneratorService<T> {

	private static final String BATCHED_ID_COLUMN_NAME = "ID";
	private static final String BATCHED_PRIORITY_OVERRIDE_COLUMN_NAME = "SystemPriority";
	private static final String BATCHED_ASSIGNEE_OVERRIDE_COLUMN_NAME = "AssigneeUserID";
	private static final String BATCHED_ASSIGNEE_GROUP_OVERRIDE_COLUMN_NAME = "AssigneeGroupID";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AdvancedUpdatableDAO<Notification, Criteria> notificationDAO;
	private ReadOnlyDAO<SecurityUserGroup> securityUserGroupDAO;

	private CalendarBusinessDayService calendarBusinessDayService;
	private NotificationCommentService notificationCommentService;
	private NotificationDefinitionService<N> notificationDefinitionService;
	private SecurityAuthorizationService securityAuthorizationService;
	private SecurityUserService securityUserService;
	private SystemPriorityService systemPriorityService;
	private DaoSingleKeyListCache<SecurityUserGroup, Short> securityUserGroupListByUserCache;

	private ContextHandler contextHandler;
	private DaoLocator daoLocator;

	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	//////         Notification Generator Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void generateInstantNotification(NotificationDefinitionInstant definition, T bean, boolean generate) {
		if (!definition.isActive()) {
			return;
		}

		// For those that we need subject to determine duplicates, generate the subject now, otherwise it will be generated if/when the notification is inserted/updated
		String subject = null;
		if (definition.isUseSubjectForUniqueness()) {
			subject = generateNotificationSubject(definition, bean);
		}

		// Only generate if doesn't exist, or duplicates allowed
		Notification existing = getExistingNotification(definition, (Number) bean.getIdentity(), subject, null);
		if (generate) {
			Number linkedId = null;
			// Only generate dupe if was acknowledged
			if (existing == null || (definition.isDuplicatesAllowed() && existing.isAcknowledged())) {
				boolean generateNotification = true;
				// If linked, only generate if no specific links exists, or linked id is in valid list
				if (definition.getLinkedTable() != null) {
					linkedId = (Number) BeanUtils.getPropertyValue(bean, definition.getLinkedRelationshipPath());
					List<NotificationDefinitionLink> linkList = getNotificationDefinitionService().getNotificationDefinitionLinkListByDefinition(definition.getId());
					if (!CollectionUtils.isEmpty(linkList) && CollectionUtils.isEmpty(BeanUtils.filter(linkList, NotificationDefinitionLink::getFkFieldId, linkedId))) {
						generateNotification = false;
					}
				}
				if (generateNotification) {
					Notification notification = newNotification(definition);
					Number value = (Number) bean.getIdentity();
					notification.setFkFieldId(value == null ? null : value.longValue());
					notification.setLinkedFkFieldId(linkedId == null ? null : linkedId.longValue());
					populateNotification(notification, bean, linkedId, subject, null, null, null);
					getNotificationDAO().save(notification);
				}
			}
			else {
				boolean update = populateNotification(existing, bean, linkedId, subject, null, null, null);
				if (update) {
					// Make sure proper fields are updated
					getNotificationDAO().save(existing);
				}
			}
		}
		else if (definition.isSystemAcknowledged() && existing != null) {
			getNotificationDAO().delete(existing.getId());
		}
	}


	/**
	 * Generates notifications based on the BatchedNotificationDefinition.  Updates and saves the definition status and
	 * notification history.
	 *
	 * @param definition    - NotificationDefinitionBatch instance to determine the notification type
	 * @param runIfInactive - will run the batch notification even if it is inactive.  Active definitions must have a {@link CalendarSchedule}, however if
	 *                      someone decides to click "Run Now" shouldn't expect them to active the definition.
	 *                      <p>
	 *                      Note - removed Transactional annotation.  Not necessary, because each case that could happen only calls one save method Moved the
	 *                      transactional annotation to the method that actually executes the SQL to get the results so if it goes past that time it'll throw an
	 *                      exception and be logged as a failure. ADDING TRANSACTION BACK IN - DELETING NOTIFICATIONS ALSO DELETES COMMENTS IN THE OBSERVER
	 *                      WITHOUT THIS TRANSACTION THERE IS A HIBERNATE ERROR THAT A DIFFERENT ENTITY WITH THE SAME ID IS ASSOCIATED WITH THE SESSION IF THE
	 *                      TRANSACTION ITSELF DOESN'T WORK WILL NEED TO REFACTOR AND ADD EXPLICIT SAVE LIST AND DELETE LIST METHODS ON THIS SERVICE TO LOOP
	 *                      THROUGH DELETES AND EXPLICITLY DELETE THE COMMENTS FIRST THEN THE NOTIFICATION AND REMOVE THAT LOGIC FROM THE OBSERVER
	 *                      <p>
	 *                      Note - Refactored to break run batched notifications in a separate method with its own transaction.  This method
	 *                      will determine the completion status and update the definition and notification history instances, which will be saved
	 *                      separately (outside of the notification transaction).
	 */
	@Override
	public void generateBatchedNotification(NotificationDefinitionBatch definition, boolean runIfInactive, Date scheduleDate) {
		if (!definition.isActive() && !runIfInactive) {
			LogUtils.info(getClass(), "Skipping notification definition " + definition + " run because its currently inactive");
			return;
		}

		if (NotificationStatuses.RUNNING == definition.getStatus()) {
			// do not start another instance of the same notification definition is still running
			LogUtils.info(getClass(), "Skipping notification definition " + definition + " run because its previous run is still in progress");
			return;
		}

		definition.setStatus(NotificationStatuses.RUNNING);
		definition = getNotificationDefinitionService().saveNotificationDefinitionBatch(definition);

		NotificationHistory history = new NotificationHistory();
		Date start = new Date();
		history.setNotificationDefinition(definition);
		history.setRunnerUser(getSecurityUserService().getSecurityUserCurrent());
		history.setStatus(NotificationStatuses.RUNNING);
		history.setScheduledDate(scheduleDate);
		history.setStartDate(start);
		getNotificationDefinitionService().saveNotificationHistory(history);

		try {
			// notifications will be generated and saved by this call, and rolled-back if an error occurs
			createBatchedNotifications(definition, scheduleDate, history);
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error running notification definition " + definition, e);
		}
		finally {
			getNotificationDefinitionService().saveNotificationDefinitionBatch(definition);
			getNotificationDefinitionService().saveNotificationHistory(history);
		}
	}


	/**
	 * A helper method to generate the notifications specified by the NotificationDefinitionBatch.  Updates the definition and
	 * notification history parameters.
	 */
	@Transactional(timeout = 180)
	protected void createBatchedNotifications(NotificationDefinitionBatch definition, Date scheduleDate, NotificationHistory history) {

		String completedMessage = null;
		NotificationStatuses completedStatus = NotificationStatuses.FAILED;

		if (definition == null) {
			throw new ValidationException("NotificationGeneratorService.generateBatchedNotification(): 'definition' parameter cannot be null.");
		}

		if (history == null) {
			throw new ValidationException("NotificationGeneratorService.generateBatchedNotification(): ' history' parameter cannot be null.");
		}

		try {
			DataTable results = getNotificationDefinitionService().getNotificationDefinitionBatchDataTableResults(definition);
			List<Notification> originalList = getNotificationDAO().findByField("definition.id", definition.getId());

			// If no results - don't create notification
			if (results == null || results.getTotalRowCount() == 0) {
				if (definition.isSystemAcknowledged()) {
					// Delete all notification for this definition since none apply
					getNotificationDAO().deleteList(originalList);
				}
				completedStatus = NotificationStatuses.COMPLETED_WITH_NO_RESULTS;
			}
			else {

				// One Notification for all of the results
				if (definition.isBatchResults()) {

					// For those that we need subject to determine duplicates, generate the subject now, otherwise it will be generated if/when the notification is inserted/updated
					String subject = null;
					if (definition.isUseSubjectForUniqueness()) {
						subject = generateNotificationSubject(definition, results);
					}

					// Only generate if doesn't exist, or duplicates allowed
					Notification existing = getExistingNotification(definition, null, subject, originalList);
					// Only generate dupe if was acknowledged
					if (existing == null || (definition.isDuplicatesAllowed() && existing.isAcknowledged())) {
						Notification notification = newNotification(definition);
						populateNotification(notification, results, null, subject, null, null, null);
						getNotificationDAO().save(notification);
					}
					else {
						boolean update = populateNotification(existing, results, null, subject, null, null, null);
						if (update) {
							getNotificationDAO().save(existing);
						}
					}
					completedStatus = NotificationStatuses.COMPLETED;
					completedMessage = results.getTotalRowCount() + " matching notifications batched into 1 notification.";
				}
				// One Notification for EACH row in the results
				else {
					List<Notification> newList = new ArrayList<>();
					String linkedField = (definition.getLinkedTable() == null ? null : definition.getLinkedRelationshipPath());
					for (int i = 0; i < results.getTotalRowCount(); i++) {
						DataRow row = results.getRow(i);

						// Is there a Priority Override?
						// Should just return null if missing
						String priorityOverride = (String) row.getValue(BATCHED_PRIORITY_OVERRIDE_COLUMN_NAME);
						Number assigneeOverride = (Number) row.getValue(BATCHED_ASSIGNEE_OVERRIDE_COLUMN_NAME);
						Number assigneeGroupOverride = (Number) row.getValue(BATCHED_ASSIGNEE_GROUP_OVERRIDE_COLUMN_NAME);

						// For those that we need subject to determine duplicates, generate the subject now, otherwise it will be generated if/when the notification is inserted/updated
						String subject = null;
						if (definition.isUseSubjectForUniqueness()) {
							subject = generateNotificationSubject(definition, row);
						}

						// Only generate if doesn't exist, or duplicates allowed
						Notification existing = getExistingNotification(definition, (Number) row.getValue(BATCHED_ID_COLUMN_NAME), subject, originalList);
						Number linkedFkFieldId = null;
						if (linkedField != null) {
							linkedFkFieldId = (Number) row.getValue(linkedField);
						}
						// Only generate dupe if was acknowledged
						if (existing == null || (definition.isDuplicatesAllowed() && existing.isAcknowledged())) {
							Notification notification = newNotification(definition);
							Number value = (Number) row.getValue(BATCHED_ID_COLUMN_NAME);
							notification.setFkFieldId(value == null ? null : value.longValue());
							notification.setLinkedFkFieldId(linkedFkFieldId == null ? null : linkedFkFieldId.longValue());
							populateNotification(notification, row, linkedFkFieldId, subject, priorityOverride, assigneeOverride, assigneeGroupOverride);
							newList.add(notification);
						}
						else {
							boolean update = populateNotification(existing, row, linkedFkFieldId, subject, priorityOverride, assigneeOverride, assigneeGroupOverride);
							if (update) {
								newList.add(existing);
							}
							else {
								// If no update is necessary - Remove the bean from the original list so the database isn't touched for that record
								originalList.remove(existing);
							}
						}
					}
					if (definition.isSystemAcknowledged()) {
						getNotificationDAO().saveList(newList, originalList);
					}
					else {
						getNotificationDAO().saveList(newList);
					}

					completedStatus = NotificationStatuses.COMPLETED;
					completedMessage = results.getTotalRowCount() + " matching notifications. " + CollectionUtils.getSize(newList) + " inserts/updates.";
				}
			}
		}
		catch (Throwable e) {
			// the job being executed failed so set the status
			completedStatus = NotificationStatuses.FAILED;
			completedMessage = ExceptionUtils.getFullStackTrace(e);
			// re-throw exception to rollback the transaction
			throw e;
		}
		finally {
			definition.setStatus(completedStatus);
			history.setStatus(completedStatus);
			history.setEndDate(new Date());
			// truncate the message if necessary: 4000 characters limit
			history.setDescription(StringUtils.formatStringUpToNCharsWithDots(completedMessage, DataTypes.DESCRIPTION_LONG.getLength(), true));
		}
	}


	protected String generateNotificationText(NotificationDefinition def, Object bean) {
		if (!StringUtils.isEmpty(def.getText())) {
			try {
				TemplateConfig config = new TemplateConfig(def.getText());
				config.addBeanToContext(def.getTemplateConfigBeanName(), bean);
				if (bean instanceof BaseEntity<?>) {
					SecurityUser createUser = getSecurityUserService().getSecurityUser(((BaseEntity<?>) bean).getCreateUserId());
					if (createUser != null) {
						config.addBeanToContext("createUser", createUser);
					}
				}
				if (bean instanceof UpdatableOnlyEntity) {
					SecurityUser updateUser = getSecurityUserService().getSecurityUser(((UpdatableOnlyEntity) bean).getUpdateUserId());
					if (updateUser != null) {
						config.addBeanToContext("updateUser", updateUser);
					}
				}
				return StringUtils.formatStringUpToNCharsWithDots(getTemplateConverter().convert(config), DataTypes.DESCRIPTION_LONG.getLength(), true);
			}
			catch (Throwable e) {
				LogUtils.errorOrInfo(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Error generating Notification Text for Definition [" + def.getName() + "]: " + ExceptionUtils.getOriginalMessage(e)));
				throw new ValidationException("Error generating Notification Text for Definition [" + def.getName() + "]: " + ExceptionUtils.getOriginalMessage(e));
			}
		}
		return null;
	}


	/**
	 * Specialized method that compares if the two text values are equal.
	 * <p>
	 * Also converts all "&amp;" to "&" to ensure it's not marked as changed when really the same.
	 */
	protected boolean isNotificationTextEqual(String text1, String text2) {
		if (StringUtils.compare(text1, text2) == 0) {
			return true;
		}
		if (StringUtils.isEmpty(text1)) {
			return (StringUtils.isEmpty(text2));
		}
		else if (StringUtils.isEmpty(text2)) {
			// We already know text1 is not empty at this point - return false
			return false;
		}
		// Remove all whitespace
		text1 = text1.replaceAll("\\s", "");
		text2 = text2.replaceAll("\\s", "");

		// Convert special characters that are really the same...
		text1 = text1.replaceAll("&amp;", "&");
		text2 = text2.replaceAll("&amp;", "&");

		// Then compare again if really equal
		return (StringUtils.compare(text1, text2) == 0);
	}


	/**
	 * Formats and sets the Notification Subject & Text based upon the definition and bean
	 * <p>
	 * Returns false if it was an existing bean and the subject/text are exactly the same & Priority notification changed.
	 * <p>
	 * For cases where subject was already generated to determine existing vs. new (def.isUseSubjectForUniqueness) - don't regenerate it - pass it into the
	 * method
	 */
	private boolean populateNotification(Notification notification, Object bean, Number linkedFkFieldId, String subject, String priorityOverride, Number assigneeOverride, Number assigneeGroupOverride) {
		NotificationDefinition definition = notification.getDefinition();
		if (StringUtils.isEmpty(subject)) {
			subject = generateNotificationSubject(definition, bean);
		}
		String text = generateNotificationText(definition, bean);

		SystemPriority priority = definition.getPriority();
		if (!StringUtils.isEmpty(priorityOverride)) {
			priority = getSystemPriorityService().getSystemPriorityByName(priorityOverride);
		}

		SecurityUser assignee = notification.getAssigneeUser();
		if (!MathUtils.isNullOrZero(assigneeOverride)) {
			assignee = getSecurityUserService().getSecurityUser(assigneeOverride.shortValue());
			ValidationUtils.assertNotNull(assignee, "Cannot find Security User with id = " + assigneeOverride);
		}

		SecurityGroup assigneeGroup = notification.getAssigneeGroup();
		if (!MathUtils.isNullOrZero(assigneeGroupOverride)) {
			assigneeGroup = getSecurityUserService().getSecurityGroup(assigneeGroupOverride.shortValue());
			ValidationUtils.assertNotNull(assigneeGroup, "Cannot find Security Group with id = " + assigneeGroupOverride);
		}

		if (!notification.isNewBean()) {
			if (isNotificationTextEqual(subject, notification.getSubject()) && isNotificationTextEqual(text, notification.getText())) {
				// Only Update Acknowledged if there a Subject/Text Change
				if (notification.isAcknowledged()) {
					return false;
				}
				// No other changes: Priority Linked Fields, etc. Don't update.
				if (CompareUtils.isEqual(priority, notification.getPriority()) && CompareUtils.isEqual(assignee, notification.getAssigneeUser())
						&& MathUtils.isEqual(linkedFkFieldId, notification.getLinkedFkFieldId()) && CompareUtils.isEqual(assigneeGroup, notification.getAssigneeGroup())) {
					return false;
				}
			}
		}

		// If "un-acknowledging" the notification - add a comment as to why
		if (!notification.isNewBean() && notification.isAcknowledged()) {
			NotificationComment comment = new NotificationComment();
			comment.setNotification(notification);

			// Only Unacknowledge if Not Opted to Keep Acknowledged
			StringBuilder commentDescription;
			if (notification.getDefinition().isDoNotUnacknowledgeIfUpdated()) {
				commentDescription = new StringBuilder("System Updated Acknowledged Notification but did not clear the acknowledged flag because notification definition has option set not to clear this.  Previous Notification Was:");
			}
			else {
				notification.setAcknowledged(false);
				commentDescription = new StringBuilder("System Un-Acknowledged Notification because of an update to the Notification.  Previous Notification Was:");
			}

			commentDescription.append(StringUtils.NEW_LINE);
			commentDescription.append(notification.getSubject());
			commentDescription.append(StringUtils.NEW_LINE);
			commentDescription.append(notification.getText());
			commentDescription.append(StringUtils.NEW_LINE);
			comment.setDescription(commentDescription.toString());
			getNotificationCommentService().saveNotificationComment(comment);
		}

		notification.setSubject(subject);
		notification.setText(text);
		notification.setPriority(priority);
		notification.setAssigneeUser(assignee);
		notification.setAssigneeGroup(assigneeGroup);
		notification.setLinkedTable(definition.getLinkedTable());
		notification.setLinkedFkFieldId(linkedFkFieldId == null ? null : linkedFkFieldId.longValue());
		notification.setType(definition.getType());

		return true;
	}


	private String generateNotificationSubject(NotificationDefinition def, Object bean) {
		try {
			TemplateConfig config = new TemplateConfig(def.getSubject());
			config.addBeanToContext(def.getTemplateConfigBeanName(), bean);
			return StringUtils.formatStringUpToNCharsWithDots(getTemplateConverter().convert(config), DataTypes.DESCRIPTION.getLength(), true);
		}
		catch (Throwable e) {
			LogUtils.errorOrInfo(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Error generating Notification Subject for Definition [" + def.getName() + "]: " + ExceptionUtils.getOriginalMessage(e)));
			throw new ValidationException("Error generating Notification Subject for Definition [" + def.getName() + "]: " + ExceptionUtils.getOriginalMessage(e));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Notification Business Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Notification getNotificationSample(int id, String subjectTemplate, String textTemplate) {
		NotificationDefinition definition = getNotificationDefinitionService().getNotificationDefinition(id);
		definition.setSubject(subjectTemplate);
		definition.setText(textTemplate);
		Notification notification = newNotification(definition);
		if (definition.isInstant()) {
			ReadOnlyDAO<T> dao = getDaoLocator().locate(definition.getTable().getName());
			T bean = CollectionUtils.getFirstElement(dao.findAll());
			if (bean == null) {
				bean = dao.newBean();
			}
			populateNotification(notification, bean, null, null, null, null, null);
		}
		else {
			NotificationDefinitionBatch batch = (NotificationDefinitionBatch) definition;
			DataTable results = getNotificationDefinitionService().getNotificationDefinitionBatchDataTableResults(batch);
			if (results == null || results.getTotalRowCount() == 0) {
				throw new ValidationException("Unable to generate a sample notification.  There is no data returned from the notification generator.");
			}
			if (batch.isBatchResults()) {
				populateNotification(notification, results, null, null, null, null, null);
			}
			else {
				DataRow row = results.getRow(0);
				if (row == null) {
					row = new DataRowImpl(results, new Object[results.getColumnCount()]);
				}
				populateNotification(notification, row, null, null, (String) row.getValue(BATCHED_PRIORITY_OVERRIDE_COLUMN_NAME), (Number) row.getValue(BATCHED_ASSIGNEE_OVERRIDE_COLUMN_NAME), (Number) row.getValue(BATCHED_ASSIGNEE_GROUP_OVERRIDE_COLUMN_NAME));
			}
		}
		return notification;
	}


	@Override
	public Notification getNotification(int id) {
		return getNotificationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<Notification> getNotificationListSecure(NotificationSearchForm searchForm) {
		return getNotificationListImpl(searchForm);
	}


	@Override
	public List<Notification> getNotificationList(NotificationSearchForm searchForm) {
		SecurityUser currentUser = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
		Short currentUserId = currentUser.getId();
		// Only check if the user set on the search form isn't the current user
		if (searchForm.getUserId() != null && !MathUtils.isEqual(currentUserId, searchForm.getUserId())) {
			// Make sure user is Admin or has Full Control to Notifications
			if (!getSecurityAuthorizationService().isSecurityAccessAllowed("Notification", SecurityPermission.PERMISSION_FULL_CONTROL)) {
				throw new ValidationException("You cannot view notifications for a different user because you do not have full control access to Notification security resource");
			}
		}
		else if (searchForm.getGroupId() != null) {
			List<Short> userGroups = CollectionUtils.buildStream(getSecurityUserGroupListByUserCache().getBeanListForKeyValue(getSecurityUserGroupDAO(), currentUserId))
					.map(SecurityUserGroup::getReferenceTwo).map(SecurityGroup::getId).collect(Collectors.toList());
			// user is not a member of the selected security group, make sure the current user has permissions to view notifications that do not belong to them.
			if (!userGroups.contains(searchForm.getGroupId()) && !getSecurityAuthorizationService().isSecurityAccessAllowed("Notification", SecurityPermission.PERMISSION_FULL_CONTROL)) {
				throw new ValidationException("You cannot view notifications for a group you are not a member of because you do not have full control access to Notification security resource");
			}
		}
		else if (!searchForm.isDoNotSetUserId()) {
			searchForm.setUserId(currentUserId);
		}

		return getNotificationListImpl(searchForm);
	}


	@Override
	public Notification saveNotification(Notification bean) {
		return getNotificationDAO().save(bean);
	}


	@Override
	@Transactional
	public void saveNotificationListAssignee(int[] notificationIds, short userId) {
		SecurityUser user = getSecurityUserService().getSecurityUser(userId);
		ValidationUtils.assertNotNull(user, "No user selected to make as the assignee.");

		if (notificationIds == null || notificationIds.length == 0) {
			throw new ValidationException("No notifications selected to assign [" + user.getDisplayName() + "] to.");
		}

		for (int notificationId : notificationIds) {
			Notification bean = getNotification(notificationId);
			if (bean != null) {
				bean.setAssigneeUser(user);
				saveNotification(bean);
			}
		}
	}


	@Override
	@Transactional
	public void saveNotificationListGroupAssignee(int[] notificationIds, short groupId) {
		SecurityGroup group = getSecurityUserService().getSecurityGroup(groupId);
		ValidationUtils.assertNotNull(group, "No group selected to make as the assignees.");

		if (notificationIds == null || notificationIds.length == 0) {
			throw new ValidationException("No notifications selected to assign [" + group.getLabel() + "] to.");
		}

		for (int notificationId : notificationIds) {
			Notification bean = getNotification(notificationId);
			if (bean != null) {
				bean.setAssigneeGroup(group);
				saveNotification(bean);
			}
		}
	}


	@Override
	@Transactional
	public void saveNotificationListAcknowledged(int[] notificationIds, String comment) {
		// Currently comments are required when acknowledging notifications in bulk only
		ValidationUtils.assertFalse(StringUtils.isEmpty(comment), "Comment is required");
		if (notificationIds == null || notificationIds.length == 0) {
			throw new ValidationException("No notifications selected to acknowledge.");
		}

		for (int notificationId : notificationIds) {
			Notification bean = getNotification(notificationId);
			if (bean != null) {
				bean.setAcknowledged(true);
				saveNotification(bean);

				NotificationComment commentBean = new NotificationComment();
				commentBean.setDescription(comment);
				commentBean.setNotification(bean);
				getNotificationCommentService().saveNotificationComment(commentBean);
			}
		}
	}


	@Override
	public void clearNotificationList(int definitionId) {
		getNotificationDAO().deleteList(getNotificationDAO().findByFields(new String[]{"definition.id", "acknowledged"}, new Object[]{definitionId, false}));
	}


	/**
	 * Creates a new instance of a {@link Notification} bean with defaults
	 * set based upon the given {@link NotificationDefinition}
	 */
	private Notification newNotification(NotificationDefinition definition) {
		Notification notification = new Notification();
		notification.setDefinition(definition);
		notification.setTable(definition.getTable());
		notification.setType(definition.getType());
		notification.setPriority(definition.getPriority());
		notification.setAssigneeUser(definition.getAssigneeUser());
		notification.setAssigneeGroup(definition.getAssigneeGroup());
		notification.setLinkedTable(definition.getLinkedTable());

		if (definition.getDaysDueFromCreation() != null) {
			if (definition.getBusinessDaysDueCalendar() != null) {
				notification.setDueDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forToday(definition.getBusinessDaysDueCalendar().getId()), definition.getDaysDueFromCreation()));
			}
			else {
				java.util.Calendar cal = Calendar.getInstance();
				cal.setTime(new Date());
				cal.add(java.util.Calendar.DATE, definition.getDaysDueFromCreation());
				notification.setDueDate(cal.getTime());
			}
		}
		return notification;
	}


	private List<Notification> getNotificationListImpl(final NotificationSearchForm searchForm) {
		// When searching for a specific user, this will first get the list of notification definition ids that user is a recipient of (as user or member of group)
		final List<Integer> definitionIdList = getNotificationDefinitionService().getNotificationDefinitionIdsForRecipientUserOrGroup(searchForm.getUserId(), searchForm.getGroupId(), true);

		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!StringUtils.isEmpty(searchForm.getTableName())) {
					criteria.createAlias("table", "t");
					criteria.createAlias("linkedTable", "lt", JoinType.LEFT_OUTER_JOIN);

					Criterion eqMainTableName = Restrictions.eq("t.name", searchForm.getTableName());
					Criterion eqLinkedTableName = Restrictions.eq("lt.name", searchForm.getTableName());

					// Only makes sense to filter on entityId if tableName is defined
					if (searchForm.getEntityId() != null) {
						Criterion eqMainTableKey = Restrictions.eq("fkFieldId", searchForm.getEntityId());
						Criterion eqLinkedTableKey = Restrictions.eq("linkedFkFieldId", searchForm.getEntityId());
						Criterion eqMainTableNameAndKey = Restrictions.and(eqMainTableName, eqMainTableKey);
						Criterion eqLinkedTableNameAndKey = Restrictions.and(eqLinkedTableName, eqLinkedTableKey);
						criteria.add(Restrictions.or(eqMainTableNameAndKey, eqLinkedTableNameAndKey));
					}
					else {
						criteria.add(Restrictions.or(eqMainTableName, eqLinkedTableName));
					}
				}
				// assigneeName is a custom search field, it can be either assigneeUser or assigneeGroup
				if (!StringUtils.isEmpty(searchForm.getAssigneeName())) {
					criteria.createAlias("assigneeUser", "au", JoinType.LEFT_OUTER_JOIN);
					criteria.createAlias("assigneeGroup", "ag", JoinType.LEFT_OUTER_JOIN);
					String[] temp = searchForm.getAssigneeName().split(", ");
					if (temp.length < 2) {
						temp = new String[]{searchForm.getAssigneeName(), searchForm.getAssigneeName()};
					}
					Criterion eqAssigneeUserName = Restrictions.like("au.userName", temp[0]);
					Criterion eqAssigneeUserDisplayName = Restrictions.like("au.displayName", temp[0]);
					Criterion eqAssigneeGroupName = Restrictions.like("ag.name", temp[1]);

					criteria.add(Restrictions.or(eqAssigneeUserName, eqAssigneeUserDisplayName, eqAssigneeGroupName));
				}

				// If definitionIdList is not empty, then these are the definitions the user is a recipient of
				if (!CollectionUtils.isEmpty(definitionIdList)) {
					Criterion definitionIdInUserDefinitions = new InExpressionForNumbers("definition.id", definitionIdList.toArray());
					// Need to OR with Assignee = User
					if (searchForm.getUserId() != null) {
						Short[] userGroupIds = getSecurityUserGroupIDs(searchForm.getUserId());
						Criterion inUserGroupIds = userGroupIds.length > 0
								? new InExpressionForNumbers("assigneeGroup.id", userGroupIds)
								: new FalseCriterion();
						Criterion eqAssigneeId = Restrictions.eq("assigneeUser.id", searchForm.getUserId());
						criteria.add(Restrictions.or(eqAssigneeId, definitionIdInUserDefinitions, inUserGroupIds));
					}
					// Need to OR with Assignee Group = Group
					else if (searchForm.getGroupId() != null) {
						Criterion eqGroupId = Restrictions.eq("assigneeGroup.id", searchForm.getGroupId());
						criteria.add(Restrictions.or(eqGroupId, definitionIdInUserDefinitions));
					}
					else {
						criteria.add(definitionIdInUserDefinitions);
					}
				}
				// Otherwise the user can be assigned a notification, without actually being a recipient of any definitions
				// Also select based on the user's assignee group membership.
				else if (searchForm.getUserId() != null) {
					Short[] userGroupIds = getSecurityUserGroupIDs(searchForm.getUserId());
					Criterion inUserGroupIds = userGroupIds.length > 0
							? new InExpressionForNumbers("assigneeGroup.id", userGroupIds)
							: new FalseCriterion();
					SimpleExpression eqAssigneeId = Restrictions.eq("assigneeUser.id", searchForm.getUserId());
					criteria.add(Restrictions.or(eqAssigneeId, inUserGroupIds));
				}
				// Otherwise a group can be assigned a notification, without actually being a recipient of any definitions
				else if (searchForm.getGroupId() != null) {
					SimpleExpression eqAssigneeGroupId = Restrictions.eq("assigneeGroup.id", searchForm.getGroupId());
					criteria.add(eqAssigneeGroupId);
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				if (!super.configureOrderBy(criteria)) {
					String priorityAlias = getPathAlias("priority", criteria);
					criteria.addOrder(Order.asc(priorityAlias + ".order"));
					criteria.addOrder(Order.asc(getPathAlias("definition", criteria) + ".name"));
					criteria.addOrder(Order.asc("subject"));
				}
				return true;
			}
		};
		return getNotificationDAO().findBySearchCriteria(searchConfigurer);
	}


	private Short[] getSecurityUserGroupIDs(Short securityUserId) {
		return CollectionUtils.buildStream(getSecurityUserGroupListByUserCache().getBeanListForKeyValue(getSecurityUserGroupDAO(), securityUserId))
				.map(SecurityUserGroup::getReferenceTwo).map(SecurityGroup::getId).distinct().toArray(Short[]::new);
	}


	/**
	 * Returns the last notification that already exists with the given parameters and IsAcknowledged = false.
	 * Otherwise - If one exists that is acknowledged, returns that one.
	 */
	private Notification getExistingNotification(NotificationDefinition definition, Number fkFieldId, String subject, List<Notification> existingList) {
		List<Notification> list = getExistingNotificationList(definition, fkFieldId, subject, existingList);

		Notification bean = CollectionUtils.getFirstElement(BeanUtils.filter(list, notification -> !notification.isAcknowledged()));
		if (bean == null) {
			bean = CollectionUtils.getFirstElement(list);
		}
		// Return a clone of the bean so properly updates and doesn't change original bean properties
		return BeanUtils.cloneBean(bean, false, true);
	}


	private List<Notification> getExistingNotificationList(NotificationDefinition def, Number fkFieldId, String subject, List<Notification> existingList) {
		List<Notification> list;
		if (existingList != null) {
			list = BeanUtils.filter(existingList, Notification::getFkFieldId, fkFieldId == null ? null : fkFieldId.longValue());
		}
		else {
			list = getNotificationDAO().findByFields(new String[]{"definition.id", "fkFieldId"}, new Object[]{def.getId(), fkFieldId == null ? null : fkFieldId.longValue()});
		}

		// Need to manually check subject for matches because of html text changes that sometimes happen
		if (def.isUseSubjectForUniqueness() && !CollectionUtils.isEmpty(list)) {
			List<Notification> filteredList = new ArrayList<>();
			for (Notification ex : list) {
				if (isNotificationTextEqual(subject, ex.getSubject())) {
					filteredList.add(ex);
				}
			}
			return filteredList;
		}
		return list;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////               Getter & Setter Methods                 ///////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<Notification, Criteria> getNotificationDAO() {
		return this.notificationDAO;
	}


	public void setNotificationDAO(AdvancedUpdatableDAO<Notification, Criteria> notificationDAO) {
		this.notificationDAO = notificationDAO;
	}


	public ReadOnlyDAO<SecurityUserGroup> getSecurityUserGroupDAO() {
		return this.securityUserGroupDAO;
	}


	public void setSecurityUserGroupDAO(ReadOnlyDAO<SecurityUserGroup> securityUserGroupDAO) {
		this.securityUserGroupDAO = securityUserGroupDAO;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public NotificationDefinitionService<N> getNotificationDefinitionService() {
		return this.notificationDefinitionService;
	}


	public void setNotificationDefinitionService(NotificationDefinitionService<N> notificationDefinitionService) {
		this.notificationDefinitionService = notificationDefinitionService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public NotificationCommentService getNotificationCommentService() {
		return this.notificationCommentService;
	}


	public void setNotificationCommentService(NotificationCommentService notificationCommentService) {
		this.notificationCommentService = notificationCommentService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemPriorityService getSystemPriorityService() {
		return this.systemPriorityService;
	}


	public void setSystemPriorityService(SystemPriorityService systemPriorityService) {
		this.systemPriorityService = systemPriorityService;
	}


	public DaoSingleKeyListCache<SecurityUserGroup, Short> getSecurityUserGroupListByUserCache() {
		return this.securityUserGroupListByUserCache;
	}


	public void setSecurityUserGroupListByUserCache(DaoSingleKeyListCache<SecurityUserGroup, Short> securityUserGroupListByUserCache) {
		this.securityUserGroupListByUserCache = securityUserGroupListByUserCache;
	}
}
