package com.clifton.notification.generator;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.ObjectUtils;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationType;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.util.Date;
import java.util.StringJoiner;


/**
 * The <code>Notification</code> class is a notification record with subject & text populated
 * for a given bean that was "sent" to a {@link SecurityUser}.
 * <p>
 * The {@link SystemTable} & fkFieldId reference which bean the notification was populated for specifically.
 * <p>
 * The acknowledged fields are flags that track whether or not the has been acknowledged
 *
 * @author manderson
 */
public class Notification extends BaseEntity<Integer> implements LabeledObject, SystemEntityModifyConditionAware {

	private NotificationDefinition definition;

	private SecurityUser assigneeUser;
	private SecurityGroup assigneeGroup;

	// type priority and table are copied from {@link NotificationDefinition} object so that they stay historically accurate.
	private NotificationType type;
	private SystemPriority priority;
	private SystemTable table;

	@SoftLinkField(tableBeanPropertyName = "table")
	private Long fkFieldId;

	private String subject;
	private String text;

	private Date dueDate;

	/**
	 * A notification is considered closed/addressed once it's acknowledged.
	 * UpdateUserID and UpdateDate will indicate the user and date/time of acknowledgment.
	 */
	private boolean acknowledged;

	/**
	 * Used to track Linked Information,
	 * if used by the notification definition
	 */
	private SystemTable linkedTable;

	@SoftLinkField(tableBeanPropertyName = "linkedTable")
	private Long linkedFkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAssignee() {
		StringJoiner stringJoiner = new StringJoiner(", ");
		ObjectUtils.map(this.assigneeUser, LabeledObject::getLabel).thenConsume(stringJoiner::add);
		ObjectUtils.map(this.assigneeGroup, LabeledObject::getLabel).thenConsume(stringJoiner::add);
		return stringJoiner.toString();
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getType().getNotificationModifyCondition();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getSubject();
	}


	public NotificationDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(NotificationDefinition definition) {
		this.definition = definition;
	}


	public SystemTable getTable() {
		return this.table;
	}


	public void setTable(SystemTable table) {
		this.table = table;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public NotificationType getType() {
		return this.type;
	}


	public void setType(NotificationType type) {
		this.type = type;
	}


	public SystemPriority getPriority() {
		return this.priority;
	}


	public void setPriority(SystemPriority priority) {
		this.priority = priority;
	}


	public boolean isAcknowledged() {
		return this.acknowledged;
	}


	public void setAcknowledged(boolean acknowledged) {
		this.acknowledged = acknowledged;
	}


	public SecurityUser getAssigneeUser() {
		return this.assigneeUser;
	}


	public void setAssigneeUser(SecurityUser assigneeUser) {
		this.assigneeUser = assigneeUser;
	}


	public SecurityGroup getAssigneeGroup() {
		return this.assigneeGroup;
	}


	public void setAssigneeGroup(SecurityGroup assigneeGroup) {
		this.assigneeGroup = assigneeGroup;
	}


	public Date getDueDate() {
		return this.dueDate;
	}


	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	public SystemTable getLinkedTable() {
		return this.linkedTable;
	}


	public void setLinkedTable(SystemTable linkedTable) {
		this.linkedTable = linkedTable;
	}


	public Long getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Long fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Long getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(Long linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}
}
