package com.clifton.notification.generator;


import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.notification.comment.NotificationComment;
import com.clifton.notification.comment.NotificationCommentValidator;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>NotificationValidator</code> class handles all validation prior to
 * updating a {@link Notification}
 * <p/>
 * Validation:  A notification that has been acknowledged is read only.
 * <p/>
 * Deleting (Used By System Acknowledged Notifications) Delete Comments if any
 *
 * @author manderson
 */
@Component
public class NotificationObserver extends BaseDaoEventObserver<Notification> {

	private UpdatableDAO<NotificationComment> notificationCommentDAO;


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<Notification> dao, DaoEventTypes event, Notification bean) {
		if (event.isUpdate()) {
			Notification original = getOriginalBean(dao, bean);
			// If acknowledged flag is cleared - it's OK to update it
			// Or definition specifically says update OK without clearing acknowledged flag
			if (bean.isAcknowledged() && original.isAcknowledged() && !(bean.getDefinition().isSystemAcknowledgedIgnorable() || bean.getDefinition().isDoNotUnacknowledgeIfUpdated())) {
				throw new ValidationException("This notification has already been acknowledged and cannot be edited.");
			}
			else if (bean.getDefinition().isSystemAcknowledged() && bean.isAcknowledged() && !bean.getDefinition().isSystemAcknowledgedIgnorable()) {
				throw new ValidationException("This notification cannot be acknowledged.  The system will delete the notification automatically when it it no longer valid.");
			}
		}
		if (event.isDelete()) {
			if (!bean.getDefinition().isSystemAcknowledged()) {
				throw new ValidationException("This notification is user acknowledged and cannot be deleted.");
			}
			// System acknowledged and deleting - delete any comments tied to it
			List<NotificationComment> notificationComments = getNotificationCommentDAO().findByField("notification.id", bean.getId());
			if (!CollectionUtils.isEmpty(notificationComments)) {
				// Always delete comments when a notification is deleted: bypass validation that requires only the author of the comment can delete it
				DaoUtils.executeWithSpecificObserversDisabled(() -> getNotificationCommentDAO().deleteList(notificationComments), NotificationCommentValidator.class);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public UpdatableDAO<NotificationComment> getNotificationCommentDAO() {
		return this.notificationCommentDAO;
	}


	public void setNotificationCommentDAO(UpdatableDAO<NotificationComment> notificationCommentDAO) {
		this.notificationCommentDAO = notificationCommentDAO;
	}
}
