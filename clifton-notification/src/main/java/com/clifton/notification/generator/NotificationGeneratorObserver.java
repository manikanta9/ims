package com.clifton.notification.generator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;

import java.util.Date;


/**
 * The <code>NotificationGeneratorObserver</code> class is a {@link DaoEventObserver} that triggers
 * generating {@link Notification} for a given bean.
 *
 * @param <T>
 * @author manderson
 */
public class NotificationGeneratorObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	private final NotificationDefinitionInstant notificationDefinition;
	private NotificationGeneratorService<T> notificationGeneratorService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SecurityUserService securityUserService;


	public NotificationGeneratorObserver(NotificationDefinitionInstant notificationDefinition) {
		AssertUtils.assertNotNull(notificationDefinition, "Required argument 'notificationDefinition' cannot be null.");
		this.notificationDefinition = notificationDefinition;
	}


	/**
	 * To check if two NotificationGeneratorObservers are equal
	 * check if the notification definitions are equal.
	 *
	 * @param obj
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof NotificationGeneratorObserver<?>)) {
			return false;
		}
		return this.notificationDefinition.equals(((NotificationGeneratorObserver<?>) obj).notificationDefinition);
	}


	@Override
	public int hashCode() {
		return this.notificationDefinition.hashCode();
	}


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			// process notifications only if there was no exception (rollback anyway)
			if (isNotificationDefinitionForEvent(event)) {
				boolean generate = true;
				if (this.notificationDefinition.getCondition() != null) {
					if (!getSystemConditionEvaluationHandler().isConditionTrue(this.notificationDefinition.getCondition(), bean)) {
						// If user acknowledged, not system acknowledged don't touch it
						if (!this.notificationDefinition.isSystemAcknowledged()) {
							return;
						}
						// Otherwise if system acknowledged - delete it since it no longer applies
						generate = false;
					}
				}
				if (event.isDelete() && bean instanceof UpdatableOnlyEntity) {
					((UpdatableOnlyEntity) bean).setUpdateDate(new Date());
					SecurityUser updateUser = getSecurityUserService().getSecurityUserCurrent();
					if (updateUser != null) {
						((UpdatableOnlyEntity) bean).setUpdateUserId(updateUser.getId());
					}
				}
				getNotificationGeneratorService().generateInstantNotification(this.notificationDefinition, bean, generate);
			}
		}
	}


	private boolean isNotificationDefinitionForEvent(DaoEventTypes event) {
		if (this.notificationDefinition.isInsertEvent() && event.isInsert()) {
			return true;
		}
		if (this.notificationDefinition.isUpdateEvent() && event.isUpdate()) {
			return true;
		}
		if (this.notificationDefinition.isDeleteEvent() && event.isDelete()) {
			return true;
		}
		return false;
	}


	@Override
	@SuppressWarnings("unused")
	public void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// DO NOTHING
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public NotificationGeneratorService<T> getNotificationGeneratorService() {
		return this.notificationGeneratorService;
	}


	public void setNotificationGeneratorService(NotificationGeneratorService<T> notificationGeneratorService) {
		this.notificationGeneratorService = notificationGeneratorService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
