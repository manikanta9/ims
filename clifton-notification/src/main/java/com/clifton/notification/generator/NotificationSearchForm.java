package com.clifton.notification.generator;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class NotificationSearchForm extends BaseAuditableEntitySearchForm {

	private String tableName;
	private Long entityId;

	@SearchField(searchField = "subject,text,definition.name", sortField = "subject")
	private String searchPattern;

	@SearchField(searchField = "definition.id")
	private Integer definitionId;

	@SearchField(searchField = "assigneeUser.id")
	private Short assigneeUserId;

	@SearchField(searchField = "assigneeGroup.id")
	private Short assigneeGroupId;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchFieldPath = "type", searchField = "name")
	private String typeName;

	@SearchField(searchField = "priority.id")
	private Short priorityId;

	@SearchField(searchField = "order", searchFieldPath = "priority")
	private Integer priorityOrder;

	@SearchField(searchField = "type.notificationModifyCondition.id")
	private Integer entityModifyConditionId;

	@SearchField
	private Date dueDate;

	@SearchField
	private Boolean acknowledged;

	@SearchField
	private String subject;

	// Custom Search Filter - Filters by assignee user or group.
	private String assigneeName;

	// Custom Search Filter - Filters by user as recipient, member of recipient group, assignee or assignee group membership.
	private Short userId;
	// This flag is used to avoid filtering notifications by current user. The initial use case is Integration tests where we want to get all notifications.
	private boolean doNotSetUserId;

	// Custom Search Filter - Filters by group as recipient or assignee group.
	private Short groupId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Short getAssigneeUserId() {
		return this.assigneeUserId;
	}


	public void setAssigneeUserId(Short assigneeUserId) {
		this.assigneeUserId = assigneeUserId;
	}


	public Short getAssigneeGroupId() {
		return this.assigneeGroupId;
	}


	public void setAssigneeGroupId(Short assigneeGroupId) {
		this.assigneeGroupId = assigneeGroupId;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public Integer getEntityModifyConditionId() {
		return this.entityModifyConditionId;
	}


	public void setEntityModifyConditionId(Integer entityModifyConditionId) {
		this.entityModifyConditionId = entityModifyConditionId;
	}


	public Date getDueDate() {
		return this.dueDate;
	}


	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	public Boolean getAcknowledged() {
		return this.acknowledged;
	}


	public void setAcknowledged(Boolean acknowledged) {
		this.acknowledged = acknowledged;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public Short getUserId() {
		return this.userId;
	}


	public void setUserId(Short userId) {
		this.userId = userId;
	}


	public boolean isDoNotSetUserId() {
		return this.doNotSetUserId;
	}


	public void setDoNotSetUserId(boolean doNotSetUserId) {
		this.doNotSetUserId = doNotSetUserId;
	}


	public String getAssigneeName() {
		return this.assigneeName;
	}


	public void setAssigneeName(String assigneeName) {
		this.assigneeName = assigneeName;
	}


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public Long getEntityId() {
		return this.entityId;
	}


	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}


	public Integer getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Integer priorityOrder) {
		this.priorityOrder = priorityOrder;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
