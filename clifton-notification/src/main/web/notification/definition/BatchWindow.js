TCG.use('Clifton.notification.definition.DefinitionWindowBase');

Clifton.notification.definition.BatchWindow = Ext.extend(Clifton.notification.definition.DefinitionWindowBase, {
	titlePrefix: 'Batch Notification Definition',
	iconCls: 'clock',

	getSaveURL: function() {
		return 'notificationDefinitionBatchSave.json';
	},

	windowOnShow: function(w) {
		const tabs = w.items.get(0).items.items.find(x => x.xtype === 'tabpanel');
		tabs.add(this.batchHistoryTab);
		Clifton.notification.definition.BatchWindow.superclass.windowOnShow(w);
	},

	batchHistoryTab: {
		title: 'Run History',
		items: [{
			name: 'notificationHistoryListFind',
			xtype: 'gridpanel',
			appendStandardColumns: false,
			instructions: 'History of runs for batched notifications for selected definition.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Scheduled', width: 50, dataIndex: 'scheduledDate', hidden: true},
				{header: 'Runner', width: 50, dataIndex: 'runnerUser.label', filter: {type: 'combo', searchFieldName: 'runnerUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true}},
				{header: 'Started', width: 60, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
				{header: 'Ended', width: 60, dataIndex: 'endDate', hidden: true},
				{header: 'Duration', width: 50, dataIndex: 'executionTimeFormatted'},
				{header: 'Status', width: 80, dataIndex: 'status', renderer: Clifton.notification.renderNotificationStatus, filter: {type: 'list', options: Clifton.notification.StatusOptions}},
				{header: 'Message', width: 190, dataIndex: 'description'}
			],
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to last 10 days of runs
					this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -10)});
				}
				return {
					readUncommittedRequested: true,
					notificationDefinitionId: this.getWindow().getMainFormId()
				};
			},
			editor: {
				drillDownOnly: true,
				detailPageClass: 'Clifton.notification.definition.HistoryWindow'
			}
		}]
	},
	generalInfoTab: {
		title: 'General',
		tbar: [{
			text: 'Run Now',
			tooltip: 'Run Batch Notification Now',
			iconCls: 'config',
			handler: function() {
				const f = TCG.getParentFormPanel(this);
				const w = f.getWindow();
				if (w.isModified() || !w.isMainFormSaved()) {
					TCG.showError('Please save your changes before running the notification generator.');
					return false;
				}
				const params = {id: w.getMainFormId()};
				const loader = new TCG.data.JsonLoader({
					waitTarget: f,
					waitMsg: 'Running...',
					timeout: 60000,
					params: params,
					conf: params,
					onLoad: function(record, conf) {
						Ext.Msg.alert('Status', 'Notification Batch has been scheduled to run now and will be completed shortly.');
					}
				});
				loader.load('notificationBatchRun.json');
			}
		}],
		items: {
			xtype: 'formfragment',
			instructions: 'Batch notifications are sent periodically to recipients based upon the selected schedule.  The system query selected is executed and if there are results the results are used to generate the notifications.',
			getWarningMessage: function(f) {
				return f.findField('active').getValue() ? undefined : 'To enable this notification you must check the <i>Active</i> checkbox.';
			},
			items: [
				{fieldLabel: 'Last Run Status', name: 'status', xtype: 'displayfield', submitValue: false},
				{fieldLabel: 'Definition Name', name: 'name'},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
				{fieldLabel: 'Notification Type', name: 'type.label', hiddenName: 'type.id', displayField: 'label', xtype: 'combo', url: 'notificationTypeListFind.json', detailPageClass: 'Clifton.notification.definition.TypeWindow'},
				{
					fieldLabel: 'Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
					qtip: 'The modify condition for this object. Modifications on this object may only be performed if this condition is satisfied.'
				},
				{
					fieldLabel: 'Run Condition', name: 'type.runSystemCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'type.runSystemCondition.id', submitDetailField: false,
					qtip: 'The run condition for this object. Running this object may only be performed if this condition is satisfied.'
				},
				{fieldLabel: 'Priority', name: 'priority.label', hiddenName: 'priority.id', displayField: 'label', xtype: 'combo', loadAll: true, url: 'systemPriorityListFind.json'},
				{
					fieldLabel: 'Assignee User', name: 'assigneeUser.label', hiddenName: 'assigneeUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false',
					qtip: 'Automatically populated by the \'Trigger Query\' from the column value with the alias \'AssigneeUserID\''
				},
				{
					fieldLabel: 'Assignee Group', name: 'assigneeGroup.label', hiddenName: 'assigneeGroup.id', displayField: 'label', xtype: 'combo', url: 'securityGroupListFind.json?disabled=false',
					qtip: 'Automatically populated by the \'Trigger Query\' from the column value with the alias \'AssigneeGroupID\''
				},
				{fieldLabel: 'Active', name: 'active', xtype: 'checkbox'},
				{boxLabel: 'System automatically deletes notifications that are no longer valid (no more results)', name: 'systemAcknowledged', xtype: 'checkbox'},
				{boxLabel: 'Allows users to acknowledge notifications that the system cannot delete: data cannot be fixed', name: 'systemAcknowledgedIgnorable', xtype: 'checkbox', requiredFields: ['systemAcknowledged']},

				{fieldLabel: 'Schedule', name: 'calendarSchedule.label', hiddenName: 'calendarSchedule.id', displayField: 'name', xtype: 'combo', url: 'calendarScheduleListFind.json?calendarScheduleTypeName=Standard Schedules&systemDefined=false', detailPageClass: 'Clifton.calendar.schedule.ScheduleWindow'},
				{fieldLabel: 'Main Table', name: 'table.name', hiddenName: 'table.id', displayField: 'nameExpanded', xtype: 'combo', url: 'systemTableListFind.json', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.TableWindow'},
				{
					fieldLabel: 'Trigger Query', name: 'systemQuery.name', hiddenName: 'systemQuery.id', displayField: 'name', xtype: 'combo', url: 'systemQueryListFind.json?categoryName=System Query Tags&categoryHierarchyName=Notification', detailPageClass: 'Clifton.system.query.QueryWindow',
					qtip: 'Special Query Column Names:<br/><br/> "<b>ID</b>" - required column in the projection which should return the Primary Key of the Main Table<br/> "<b>SystemPriority</b>" - optional notification priority override for each row returned. If used, the value returned for that column in the query should be a valid system Priority name (Critical, High, Medium, Low, Trivial)<br/>"<b>AssigneeUserID</b>" - optional notification assignee user override: valid SecurityUserID<br/>"<b>AssigneeGroupID</b>" - optional notification assignee group override: valid SecurityGroupID',
					requiredFields: ['table.id'],
					mutuallyExclusiveFields: ['generatorSystemBean.id'],
					beforequery: function(queryEvent) {
						const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
						queryEvent.combo.store.baseParams = {tableId: form.findField('table.id').value};
					},
					getDefaultData: function(f) { // defaults table for "add new" drill down
						const t = f.findField('table.id');
						return {
							table: {
								id: t.getValue(),
								name: t.getRawValue()
							}
						};
					}
				},
				{
					fieldLabel: 'Generator Bean', name: 'generatorSystemBean.name', hiddenName: 'generatorSystemBean.id', displayField: 'name', xtype: 'combo', url: 'systemBeanListFind.json?groupName=DataTable Generator', detailPageClass: 'Clifton.system.bean.BeanWindow',
					getDefaultData: function() {
						return {type: {group: {name: 'DataTable Generator'}}};
					},
					mutuallyExclusiveFields: ['systemQuery.id']
				},
				{
					boxLabel: 'Use Notification Subject to determine unique notifications (as opposed to ID)', name: 'useSubjectForUniqueness', xtype: 'checkbox',
					qtip: 'If checked, then notifications will be inserted/updated/deleted based on the subject of the notification.  This can be used when there will be multiple valid records for an entity for the main table.'
				},
				{
					boxLabel: 'Do Not Clear Acknowledged Flag if Notification data is updated', name: 'doNotUnacknowledgeIfUpdated', xtype: 'checkbox',
					qtip: 'If checked, then notifications will will still be updated and comments on the change, however if the notification has been acknowledged it will remain acknowledged.'
				},
				{boxLabel: 'Combine all result rows into one notification message', name: 'batchResults', xtype: 'checkbox'},
				{boxLabel: 'Allow generating duplicate notifications. (If a notification already exists for the same entity and has been acknowledged, select this box to generate another notification.)', name: 'duplicatesAllowed', xtype: 'checkbox'},
				{
					xtype: 'fieldset',
					collapsible: true,
					collapsed: true,
					title: 'Due Date Options',
					instructions: 'If generated notifications should have due dates defaulted, use the following fields to calculate due dates.  If a calendar is selected, it will be used to determine business days (which will be skipped in calculations).',
					labelWidth: 0,
					items: [
						{fieldLabel: 'Days Until Due', name: 'daysDueFromCreation', xtype: 'spinnerfield', minValue: 0},
						{fieldLabel: 'Calendar', name: 'businessDaysDueCalendar.name', hiddenName: 'businessDaysDueCalendar.id', xtype: 'combo', displayField: 'label', url: 'calendarListFind.json', requiredFields: ['daysDueFromCreation']}
					]
				}
			]
		}
	}
});
