TCG.use('Clifton.notification.definition.DefinitionWindowBase');

Clifton.notification.definition.InstantWindow = Ext.extend(Clifton.notification.definition.DefinitionWindowBase, {
	titlePrefix: 'Instant Notification Definition',
	iconCls: 'run',

	getSaveURL: function() {
		return 'notificationDefinitionInstantSave.json';
	},
	generalInfoTab: {
		title: 'General',
		items: {
			xtype: 'formfragment',
			getWarningMessage: function(f) {
				return f.findField('active').getValue() ? undefined : 'To enable this notification you must check the <i>Active</i> checkbox.';
			},
			instructions: 'Instant notifications are generated immediately when an entity for the given table is inserted, updated, or deleted depending on the selected options.  System conditions can be used to restrict when notifications are generated for these entities.',
			items: [
				{fieldLabel: 'Name', name: 'name'},
				{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
				{fieldLabel: 'Notification Type', name: 'type.label', hiddenName: 'type.id', displayField: 'label', xtype: 'combo', url: 'notificationTypeListFind.json', detailPageClass: 'Clifton.notification.definition.TypeWindow'},
				{
					fieldLabel: 'Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
					qtip: 'The modify condition for this object. Modifications on this object may only be performed if this condition is satisfied.'
				},
				{
					fieldLabel: 'Run Condition', name: 'type.runSystemCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'type.runSystemCondition.id', submitDetailField: false,
					qtip: 'The run condition for this object. Running this object may only be performed if this condition is satisfied.'
				},
				{fieldLabel: 'Priority', name: 'priority.label', hiddenName: 'priority.id', displayField: 'label', xtype: 'combo', loadAll: true, url: 'systemPriorityListFind.json'},
				{fieldLabel: 'Assignee User', name: 'assigneeUser.label', hiddenName: 'assigneeUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false'},
				{fieldLabel: 'Assignee Group', name: 'assigneeGroup.label', hiddenName: 'assigneeGroup.id', displayField: 'label', xtype: 'combo', url: 'securityGroupListFind.json?disabled=false'},
				{fieldLabel: 'Active', name: 'active', xtype: 'checkbox'},
				{boxLabel: 'System automatically deletes notifications that are no longer valid.', name: 'systemAcknowledged', xtype: 'checkbox'},
				{boxLabel: 'Allows users to acknowledge notifications that the system cannot delete on its own.  (Data cannot be fixed)', name: 'systemAcknowledgedIgnorable', xtype: 'checkbox', requiredFields: ['systemAcknowledged']},

				{fieldLabel: 'Main Table', name: 'table.name', hiddenName: 'table.id', xtype: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.TableWindow'},
				{fieldLabel: 'Condition', name: 'condition.label', hiddenName: 'condition.id', displayField: 'name', xtype: 'system-condition-combo'},
				{
					fieldLabel: 'Trigger On', xtype: 'checkboxgroup', columns: 3, items: [
						{boxLabel: 'Insert', name: 'insertEvent'},
						{boxLabel: 'Update', name: 'updateEvent'},
						{boxLabel: 'Delete', name: 'deleteEvent'}
					]
				},
				{
					boxLabel: 'Use Notification Subject to determine unique notifications', name: 'useSubjectForUniqueness', xtype: 'checkbox',
					qtip: 'If checked, then notifications will be inserted/updated/deleted based on the subject of the notification.  This can be used when there will be multiple valid records for an entity for the main table.'
				},
				{
					boxLabel: 'Do Not Clear Acknowledged Flag If Updated', name: 'doNotUnacknowledgeIfUpdated', xtype: 'checkbox',
					qtip: 'If checked, then notifications will will still be updated and comments on the change, however if the notification has been acknowledged it will remain acknowledged.'
				},
				{boxLabel: 'Allow generating duplicate notifications. (If a notification already exists for the same entity and has been acknowledged, select this box to generate another notification.)', name: 'duplicatesAllowed', xtype: 'checkbox'},
				{
					xtype: 'fieldset',
					collapsible: false,
					title: 'Due Date Options',
					instructions: 'If generated notifications should have due dates defaulted, use the following fields to calculate due dates.  If a calendar is selected, it will be used to determine business days (which will be skipped in calculations).',
					labelWidth: 0,
					items: [
						{fieldLabel: 'Days Until Due', name: 'daysDueFromCreation', xtype: 'spinnerfield'},
						{fieldLabel: 'Calendar', name: 'businessDaysDueCalendar.name', hiddenName: 'businessDaysDueCalendar.id', xtype: 'combo', displayField: 'label', url: 'calendarListFind.json', requiredFields: ['daysDueFromCreation']}
					]
				}
			]
		}
	}
});
