Clifton.notification.definition.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Notification Type',
	iconCls: 'flag-red',
	width: 900,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Notification Types are used for classification purposes to group definitions into categories.',
					url: 'notificationType.json',
					labelWidth: 160,
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Notification Modify Condition', xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: false, name: 'notificationModifyCondition.name', hiddenName: 'notificationModifyCondition.id'},
						{fieldLabel: 'Definition Modify Condition', xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true, name: 'definitionModifyCondition.name', hiddenName: 'definitionModifyCondition.id'},
						{fieldLabel: 'Run Condition', xtype: 'system-condition-combo', name: 'runSystemCondition.name', hiddenName: 'runSystemCondition.id'}
					]
				}]
			},


			{
				title: 'Notification Definitions',
				items: [{
					xtype: 'notification-definition-grid',
					hideNotificationTypeFilters: true,
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('active', true);
						}
						return {typeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: {
							getItemText: function(rowItem) {
								return (rowItem.get('instant') === true) ? 'Instant' : 'Batch';
							},
							items: [
								{text: 'Instant', iconCls: 'run', className: 'Clifton.notification.definition.InstantWindow'},
								{text: 'Batch', iconCls: 'clock', className: 'Clifton.notification.definition.BatchWindow'}
							]
						},
						copyURL: 'notificationDefinitionCopy.json',
						copyNameMaxLength: 50,
						getDeleteURL: function() {
							return 'notificationDefinitionDelete.json';
						},
						addEditButtons: function(t, grid) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

							const typeId = this.getWindow().getMainFormId();

							t.add({
								text: 'Run Now',
								tooltip: 'Run All Active Batch Notification(s) Now for selected Type',
								iconCls: 'config',
								handler: function() {
									const params = {typeId: typeId};
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid,
										waitMsg: 'Running...',
										timeout: 60000,
										params: params,
										conf: params,
										onLoad: function(record, conf) {
											Ext.Msg.alert('Status', 'Batch Job(s) have been scheduled to run now and will be completed shortly.');
										}
									});
									loader.load('notificationBatchRunForType.json');
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Notifications',
				items: [{
					xtype: 'notification-grid',
					hideNotificationTypeFilters: true,
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to not acknowledged
							this.setFilterValue('acknowledged', false);
						}

						const t = this.getTopToolbar();
						const u = TCG.getChildByName(t, 'userId');
						if (u.getValue() !== '') {
							return {userId: u.getValue(), typeId: this.getWindow().getMainFormId()};
						}
						return {typeId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
