Clifton.notification.definition.DefinitionSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'notificationDefinitionSetupWindow',
	title: 'Notification Setup',
	iconCls: 'flag-red',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Notification Definitions',
				items: [{
					xtype: 'notification-definition-grid'
				}]
			},


			{
				title: 'Notification Types',
				items: [{
					name: 'notificationTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Notification Types are used for classification purposes to group definitions into categories.',
					topToolbarSearchParameter: 'searchPattern',
					wikiPage: 'IT/Notifications',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Description', width: 200, dataIndex: 'description', filter: false},
						{header: 'Notification Modify Condition', dataIndex: 'notificationModifyCondition.name', width: 55, filter: {searchFieldName: 'notificationModifyConditionId', type: 'combo', url: 'systemConditionListFind.json?emptySystemTableOnly=true'}},
						{header: 'Definition Modify Condition', dataIndex: 'definitionModifyCondition.name', width: 55, filter: {searchFieldName: 'definitionModifyConditionId', type: 'combo', url: 'systemConditionListFind.json?emptySystemTableOnly=true'}},
						{header: 'Run Condition', dataIndex: 'runSystemCondition.name', width: 55, filter: {searchFieldName: 'runSystemConditionId', type: 'combo', url: 'systemConditionListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.notification.definition.TypeWindow'
					}
				}]
			},


			{


				title: 'Notifications',
				items: [{
					xtype: 'notification-grid'
				}]
			},


			{
				title: 'Batch Run History',
				items: [{
					name: 'notificationHistoryListFind',
					appendStandardColumns: false,
					xtype: 'gridpanel',
					instructions: 'History of runs for all batched notifications',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition', width: 100, dataIndex: 'notificationDefinition.name', filter: {searchFieldName: 'notificationDefinitionName'}},
						{header: 'Schedule', width: 100, dataIndex: 'notificationDefinition.calendarSchedule.label', filter: false},
						{header: 'Scheduled', width: 50, dataIndex: 'scheduledDate', hidden: true},
						{header: 'Runner', width: 50, dataIndex: 'runnerUser.label', filter: {type: 'combo', searchFieldName: 'runnerUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true}},
						{header: 'Started', width: 60, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Ended', width: 60, dataIndex: 'endDate', hidden: true},
						{header: 'Duration', width: 50, dataIndex: 'executionTimeFormatted'},
						{header: 'Status', width: 80, dataIndex: 'status', renderer: Clifton.notification.renderNotificationStatus, filter: {type: 'list', options: Clifton.notification.StatusOptions}},
						{header: 'Message', width: 190, dataIndex: 'description'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 10 days of runs
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -10)});
						}
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.notification.definition.HistoryWindow'
					}
				}]
			},


			{
				title: 'Scheduled Runs',
				items: [{
					xtype: 'core-scheduled-runner-grid',
					typeName: 'NOTIFICATION'
				}]
			},


			{
				title: 'Invalid Runs',
				items: [{
					name: 'notificationDefinitionBatchListInvalid',
					xtype: 'gridpanel',
					instructions: 'The following notification definitions are in invalid state: RUNNING status but nothing is running. This is likely resulted from server restart during notification generation. Use "Fix Job" button to mark the notification as FAILED and allow future executions.',

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},

						{header: 'Definition', width: 150, dataIndex: 'name', filter: {searchFieldName: 'notificationDefinitionName'}},
						{header: 'Schedule', width: 100, dataIndex: 'calendarSchedule.label', filter: false},
						{header: 'Status', width: 80, dataIndex: 'status', filter: {type: 'list', options: [['NONE', 'NONE'], ['RUNNING', 'RUNNING'], ['COMPLETED', 'COMPLETED'], ['COMPLETED_WITH_NO_RESULTS', 'COMPLETED_WITH_NO_RESULTS'], ['FAILED', 'FAILED']]}},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.notification.definition.DefinitionWindow',
						addEditButtons: function(t) {
							t.add({
								text: 'Fix Notification',
								tooltip: 'Mark selected notification as FAILED',
								iconCls: 'tools',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a notification to be fixed.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Fix Selected Notification', 'Would you like to mark selected notification as FAILED?', function(a) {
											if (a === 'yes') {
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Fixing...',
													params: {id: sm.getSelected().id},
													onLoad: function(record, conf) {
														grid.getStore().remove(sm.getSelected());
														grid.ownerCt.updateCount();
													}
												});
												loader.load('notificationDefinitionBatchFix.json');
											}
										});
									}
								}
							});
							t.add('-');
						}
					}
				}]
			}
		]
	}]
});

