Clifton.notification.definition.HistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Batch Notification Run History',
	width: 1100,
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'notificationHistory.json',
		readOnly: true,

		items: [
			{fieldLabel: 'Definition', name: 'notificationDefinition.name', xtype: 'linkfield', detailPageClass: 'Clifton.notification.definition.DefinitionWindow', detailIdField: 'notificationDefinition.id'},
			{fieldLabel: 'Status', name: 'status'},
			{fieldLabel: 'Runner User', name: 'runnerUser.label', detailIdField: 'runnerUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.security.user.UserWindow'},
			{fieldLabel: 'Scheduled Time', name: 'scheduledDate'},
			{fieldLabel: 'Start Time', name: 'startDate'},
			{fieldLabel: 'End Time', name: 'endDate'},
			{fieldLabel: 'Message', name: 'description', xtype: 'textarea', anchor: '-35 -170'}
		]
	}]
});
