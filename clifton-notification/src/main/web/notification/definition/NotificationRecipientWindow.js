// selects Notification Definition window for the Recipient: no need to have a separate window for recipient
TCG.use('Clifton.notification.definition.DefinitionWindow');

Clifton.notification.definition.NotificationRecipientWindow = Ext.extend(Clifton.notification.definition.DefinitionWindow, {
	url: 'notificationRecipient.json',

	prepareEntity: function(entity) {
		return entity ? entity.notificationDefinition : entity;
	}
});
