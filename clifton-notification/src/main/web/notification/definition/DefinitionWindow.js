// not the actual window but a window selector based on 'instant' property
Clifton.notification.definition.DefinitionWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'notificationDefinition.json',

	getClassName: function(config, entity) {
		const instant = entity ? entity.instant : config.defaultData.instant;
		if (instant) {
			return 'Clifton.notification.definition.InstantWindow';
		}
		return 'Clifton.notification.definition.BatchWindow';
	}
});
