Clifton.notification.definition.DefinitionWindowBase = Ext.extend(TCG.app.DetailWindow, {
	height: 700,
	width: 900,
	enableRefreshWindow: true,

	generalInfoTab: {
		title: 'OVERRIDE ME',
		items: [{}]
	},
	getSaveURL: function() {
		return 'URL-TO-BE-OVERRIDDEN';
	},

	init: function() {
		// replace the first tab with the override
		const tabs = this.items[0].items[0].items;
		tabs[0] = this.generalInfoTab;
		Clifton.notification.definition.DefinitionWindowBase.superclass.init.apply(this, arguments);
	},

	items: [{
		xtype: 'formwithtabs',
		url: 'notificationDefinition.json',
		getSaveURL: function() {
			return this.ownerCt.getSaveURL();
		},
		items: [{
			xtype: 'tabpanel',
			requiredFormIndex: [0, 1, 2],
			items: [
				{}, // first tab to be overridden


				{
					title: 'Message',
					tbar: [
						{
							text: 'Append Default',
							tooltip: 'Append standard text to the message text.',
							iconCls: 'pencil',
							handler: function() {
								const f = TCG.getParentFormPanel(this);
								if (!f.getWindow().isMainFormSaved()) {
									TCG.showError('Default Messages cannot be created until after the notification definition has been created.');
									return;
								}
								const sample = TCG.getResponseText('notificationDefaultText.json?id=' + f.getWindow().getMainFormId(), this);
								const t = f.getForm().findField('text');
								t.setValue(t.getValue() + sample);
							}
						},
						'-', {
							text: 'Clear Text',
							tooltip: 'Clear message text.',
							iconCls: 'clear',
							handler: function() {
								const f = TCG.getParentFormPanel(this).getForm();
								const t = f.findField('text');
								t.setValue('');
							}
						},
						'-', {
							text: 'View Sample',
							tooltip: 'Create Sample Notification.',
							iconCls: 'view',
							handler: function() {
								const p = TCG.getParentFormPanel(this);
								const f = p.getForm();
								const subject = f.findField('subject');
								const text = f.findField('text');
								const params = {id: p.getWindow().getMainFormId(), subjectTemplate: subject.getValue(), textTemplate: text.getValue()};
								TCG.createComponent('Clifton.notification.SampleWindow', {
									params: params,
									openerCt: f,
									defaultIconCls: p.getWindow().iconCls
								});
							}
						}, '-'
					],
					items: {
						xtype: 'formfragment',
						instructions: 'The message subject and text are generated for each notification.  These two fields are templates and use special formatting to replace notification specific data. Click on the Append Default button to add the standard information to the text message, i.e. Bean Fields and Values. For instant notifications, `bean`, as well as `createUser`, and `updateUser`, if applicable, are available for use in freemarker, e.g. `${(createUser.displayName)!}`',
						items: [
							{fieldLabel: 'Subject', name: 'subject'},
							{
								fieldLabel: 'Text', name: 'textEditor', xtype: 'htmleditor',
								anchor: '-35 -100',
								submitValue: false,
								hiddenName: 'text',
								plugins: [new TCG.form.HtmlEditorFreemarker()]
							}
						]
					}
				},


				{
					title: 'Links',
					layout: 'border',
					items: [
						{
							xtype: 'formfragment',
							region: 'north',
							height: 250,
							instructions: 'Notification Definition Links are used to associate Notifications/Definitions to specific entities.  Use the bottom of the screen to add specific entities to this notification definition. If no specific links are created, than all entities will be included. Links can be used for defining notification definitions at a specific level.' +
									'<ul class="c-list">' +
									'<li>For Instant Notifications, the relationship path should be the bean field name for the primary key of the linked table.</li>' +
									'<li>For Batched Notifications, the relationship path should be a valid field in the projection of the query for the primary key of the linked table.</li>' +
									'</ul>',
							items: [
								{fieldLabel: 'Linked Table', name: 'linkedTable.name', hiddenName: 'linkedTable.id', displayField: 'nameExpanded', xtype: 'combo', url: 'systemTableListFind.json', disableAddNewItem: true, detailPageClass: 'Clifton.system.schema.TableWindow'},
								{fieldLabel: 'Relationship Path', name: 'linkedRelationshipPath', requiredFields: ['linkedTable.id']}
							]
						},

						{
							region: 'center',
							xtype: 'gridpanel',
							name: 'notificationDefinitionLinkEntitySelectedList',
							instructions: 'The following links exists for this definition.',
							columns: [
								{header: 'FkFieldId', dataIndex: 'pkFieldId', hidden: true},
								{header: 'Entity', dataIndex: 'label'}
							],
							editor: {
								deleteURL: 'notificationDefinitionLinkDeleteByDefinitionAndEntity.json',
								getDeleteParams: function(selectionModel) {
									const gridPanel = this.getGridPanel();
									return {
										definitionId: gridPanel.getWindow().getMainFormId(),
										fkFieldId: selectionModel.getSelected().get('pkFieldId')
									};
								},
								addToolbarAddButton: function(toolBar) {
									const gridPanel = this.getGridPanel();
									toolBar.add(new TCG.form.ComboBox({
										name: 'fkFieldId', displayField: 'label', valueField: 'pkFieldId', url: 'notificationDefinitionLinkEntityAvailableListFind.json', loadAll: false, width: 120, listWidth: 230,
										beforequery: function(queryEvent) {
											queryEvent.combo.store.baseParams = {
												definitionId: gridPanel.getWindow().getMainFormId()
											};
										}
									}));
									toolBar.add({
										text: 'Add',
										tooltip: 'Add Linked Entity',
										iconCls: 'add',
										handler: function() {
											const w = gridPanel.getWindow();
											if (!w.isMainFormSaved()) {
												TCG.showError('Please save your changes before creating any linked entities.');
											}
											const definitionId = w.getMainFormId();
											const fkFieldId = TCG.getChildByName(toolBar, 'fkFieldId').getValue();
											if (fkFieldId === '') {
												TCG.showError('You must select an entity from the list.');
											}
											else {
												const loader = new TCG.data.JsonLoader({
													waitTarget: gridPanel,
													waitMsg: 'Adding...',
													params: {definitionId: definitionId, fkFieldId: fkFieldId},
													onLoad: function(record, conf) {
														gridPanel.reload();
														TCG.getChildByName(toolBar, 'fkFieldId').reset();
													}
												});
												loader.load('notificationDefinitionToEntityLink.json');
											}
										}
									});
									toolBar.add('-');
								}
							},
							getLoadParams: function() {
								const w = this.getWindow();
								if (w.isMainFormSaved()) {
									return {definitionId: w.getMainFormId()};
								}
								return false;
							}
						}
					]
				},


				{
					title: 'Recipients',
					items: [{
						name: 'notificationRecipientListByNotificationDefinition',
						xtype: 'gridpanel',
						instructions: 'Recipients are those that receive each notification generated from this definition.  Recipients can be specific users or user groups.',
						getLoadParams: function() {
							return {'notificationDefinitionId': this.getWindow().getMainFormId()};
						},
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'User ID', width: 15, dataIndex: 'securityUser.id', hidden: true},
							{header: 'Group ID', width: 15, dataIndex: 'securityGroup.id', hidden: true},
							{header: 'Recipients', width: 200, dataIndex: 'label'}
						],
						editor: {
							deleteURL: 'notificationRecipientDelete.json',
							getDetailPageClass: function(grid, row) {
								return row.json.securityUser ? 'Clifton.security.user.UserWindow' : 'Clifton.security.user.GroupWindow';
							},
							getDetailPageId: function(gridPanel, row) {
								return row.json.securityUser ? row.json.securityUser.id : row.json.securityGroup.id;
							},
							addToolbarAddButton: function(toolBar) {
								const gridPanel = this.getGridPanel();
								toolBar.add({
									text: 'Add',
									tooltip: 'Add a new recipient',
									iconCls: 'add',
									menu: new Ext.menu.Menu({
										items: [
											// 	these items will render as dropdown menu items when the arrow is clicked:
											{
												text: 'User', iconCls: 'user',
												menu: new Ext.menu.Menu({
													layout: 'fit',
													style: {overflow: 'visible'},
													width: 200,
													items: [
														{
															xtype: 'combo', name: 'user', url: 'securityUserListFind.json?disabled=false', displayField: 'label',
															getListParent: function() {
																return this.el.up('.x-menu');
															},
															listeners: {
																'select': function(combo) {
																	const notificationDefinitionId = gridPanel.getWindow().getMainFormId();
																	const userId = combo.getValue();
																	combo.reset();
																	const loader = new TCG.data.JsonLoader({
																		waitTarget: gridPanel,
																		waitMsg: 'Adding...',
																		params: {userId: userId, notificationDefinitionId: notificationDefinitionId},
																		onLoad: function(record, conf) {
																			gridPanel.reload();
																		}
																	});
																	loader.load('notificationDefinitionToSecurityUserLink.json');
																}
															}
														}
													]
												})
											},
											{
												text: 'Group', iconCls: 'group',
												menu: new Ext.menu.Menu({
													layout: 'fit',
													style: {overflow: 'visible'},
													width: 200,
													items: [
														{
															xtype: 'combo', name: 'group', url: 'securityGroupListFind.json', displayField: 'label',
															getListParent: function() {
																return this.el.up('.x-menu');
															},
															listeners: {
																'select': function(combo) {
																	const notificationDefinitionId = gridPanel.getWindow().getMainFormId();
																	const groupId = combo.getValue();
																	combo.reset();
																	const loader = new TCG.data.JsonLoader({
																		waitTarget: gridPanel,
																		waitMsg: 'Adding...',
																		params: {groupId: groupId, notificationDefinitionId: notificationDefinitionId},
																		onLoad: function(record, conf) {
																			gridPanel.reload();
																		}
																	});
																	loader.load('notificationDefinitionToSecurityGroupLink.json');
																}
															}
														}
													]
												})
											}
										]
									})
								});
								toolBar.add('-');
							}
						}
					}]
				},


				{
					title: 'Notifications',
					items: [{
						xtype: 'notification-grid',
						instructions: 'The following notifications exist in the system for this definition.',
						getLoadParams: function() {
							return {'definitionId': this.getWindow().getMainFormId()};
						},
						editor: {
							detailPageClass: 'Clifton.notification.NotificationWindow',
							addEnabled: false,
							addToolbarDeleteButton: function(toolBar) {
								const grid = this.getGridPanel();
								const params = grid.getLoadParams();
								toolBar.add({
									text: 'Clear',
									tooltip: 'Clear all notifications that have not been acknowledged for this definition.',
									iconCls: 'remove',
									scope: this,
									handler: function() {
										Ext.Msg.confirm('Delete Notifications', 'Would you like to delete all notifications that have not been acknowledged for this definition?', function(a) {
											if (a === 'yes') {
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid,
													waitMsg: 'Deleting...',
													params: params,
													conf: params,
													onLoad: function(record, conf) {
														grid.reload();
													},
													onFailure: function() {
														// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
														grid.reload();
													}
												});
												loader.load('notificationListClear.json');
											}
										});
									}
								});
								toolBar.add('-');
							}
						}
					}]
				}
			]
		}]
	}]
});
