Clifton.notification.comment.CommentWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Notification Comment',
	iconCls: 'flag-red',
	width: 700,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Please enter your comment below:',
		url: 'notificationComment.json',
		labelWidth: 20,
		items: [
			{fieldLabel: 'NotificationID', name: 'notification.id', xtype: 'hidden'},
			{fieldLabel: 'NotificationAuthorID', name: 'author.id', xtype: 'hidden'},
			{fieldLabel: '', name: 'description', xtype: 'htmleditor', anchor: '95% -50'}
		]
	}]
});
