Clifton.notification.SampleWindow = function(conf) {
	this.win = new TCG.app.Window(Ext.apply({
		title: 'Notification Sample',
		width: 800,
		height: 500,
		buttons: [{
			text: 'Close',
			tooltip: 'Close this window',
			handler: function() {
				this.ownerCt.ownerCt.closeWindow();
			}
		}],

		closeWindow: function() {
			const win = this;
			win.closing = true;
			win.close();
		},

		items: [{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'The following is a sample notification generated for selected Notification Definition, Subject, & Text templates.',
			url: 'notificationSample.json',
			items: [
				{fieldLabel: 'Definition', name: 'definition.label', xtype: 'displayfield'},
				{fieldLabel: 'Priority', name: 'priority.name', xtype: 'displayfield'},
				{fieldLabel: 'Table', name: 'table.label', xtype: 'displayfield'},
				{fieldLabel: 'Assignee User', name: 'assigneeUser.label', xtype: 'displayfield'},
				{fieldLabel: 'Assignee Group', name: 'assigneeGroup.label', xtype: 'displayfield'},

				{fieldLabel: 'Notification Date', name: 'createDate', xtype: 'displayfield'},
				{fieldLabel: 'Subject', name: 'subject', xtype: 'displayfield'},
				{fieldLabel: 'Text', name: 'text', xtype: 'displayfield'}
			]
		}]
	}, conf));
};
