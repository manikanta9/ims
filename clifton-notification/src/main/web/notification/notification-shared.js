Ext.ns('Clifton.notification', 'Clifton.notification.comment', 'Clifton.notification.definition');


Clifton.notification.StatusOptions = [
	['NONE', 'NONE', 'This is usually the very first state of a new batch notification that has never ran.'],
	['RUNNING', 'RUNNING', 'The notification is currently running and we do not know yet if it will succeed or fail.'],
	['COMPLETED', 'COMPLETED', 'The notification completed its intended processing successfully.'],
	['COMPLETED_WITH_NO_RESULTS', 'COMPLETED_WITH_NO_RESULTS', 'The notification completed and returned no results.'],
	['FAILED', 'FAILED', 'Processing of this notification failed.  See run history message for more details.']
];

Clifton.notification.renderNotificationStatus = function(v) {
	if (v === 'FAILED') {
		return '<div class="amountNegative">' + v + '</div>';
	}
	if (v === 'RUNNING') {
		return '<div class="amountPositive">' + v + '</div>';
	}
	return v;
};


Clifton.notification.NotificationGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'notificationListSecureFind',
	instructions: 'The following notifications exist in the system. View as allows you to view notifications that others see as long as you have Full Control access to notifications.',
	wikiPage: 'IT/Notifications',
	groupField: 'definition.name',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Notifications" : "Notification"]})',
	additionalPropertiesToRequest: 'id|definition.id|definition.systemAcknowledged|definition.systemAcknowledgedIgnorable',
	remoteSort: true,

	rowSelectionModel: 'multiple',

	// Dynamic Tooltips
	useDynamicTooltipColumns: true,
	cacheDynamicTooltip: false,

	getTopToolbarFilters: function(toolbar) {
		const filters = [
			{
				fieldLabel: 'View As', name: 'toggleViewAs', xtype: 'button', iconCls: 'user', enableToggle: true, tooltip: 'View Notifications as the selected user or security group.', toggleHandler: function(btn, pressed) {
					const userCombo = TCG.getChildByName(toolbar, 'userId');
					const groupCombo = TCG.getChildByName(toolbar, 'groupId');
					if (pressed) {
						btn.setIconClass('group');
						userCombo.setVisible(false);
						groupCombo.setVisible(true);
						groupCombo.fireEvent('select', groupCombo, groupCombo.getValue());
					}
					else {
						btn.setIconClass('user');
						userCombo.setVisible(true);
						groupCombo.setVisible(false);
						userCombo.fireEvent('select', userCombo, userCombo.getValue());
					}
				}
			},
			{name: 'userId', width: 120, xtype: 'toolbar-combo', url: 'securityUserListFind.json?disabled=false', displayField: 'label'},
			{name: 'groupId', width: 120, xtype: 'toolbar-combo', url: 'securityGroupListFind.json?disabled=false', displayField: 'label', hidden: true}
		];
		if (this.hideNotificationTypeFilters !== true) {
			filters.push({fieldLabel: 'Notification Type', name: 'typeIdId', width: 150, xtype: 'combo', url: 'notificationTypeListFind.json', linkedFilter: 'type.name'});
		}
		filters.push({fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'});
		return filters;
	},
	columns: [
		{header: 'ID', dataIndex: 'id', hidden: true},
		{
			header: '&nbsp;', width: 10, filter: false, sortable: false,
			renderer: function(v, metaData, r) {
				return TCG.renderDynamicTooltipColumn('bubble', 'ShowSubjectAndLastComment');
			}
		},
		{
			header: 'Notification Subject', dataIndex: 'subject', width: 220, renderer: function(v, c, r) {
				const style = r.data['priority.cssStyle'];
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{header: 'Definition', hidden: true, dataIndex: 'definition.name', filter: {searchFieldName: 'definitionId', type: 'combo', displayField: 'name', url: 'notificationDefinitionGenericListFind.json'}},
		{header: 'Table', hidden: true, dataIndex: 'table.label', filter: {searchFieldName: 'tableId', type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'FKFieldID', hidden: true, dataIndex: 'fkFieldId', filter: false},
		{
			header: 'Cause', width: 40, dataIndex: 'table.detailScreenClass', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.fkFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('table.label', r.json) + ' that caused this notification', 'ShowCause');
				}
				return 'N/A';
			}
		},
		{header: 'Linked Table', hidden: true, dataIndex: 'linkedTable.label', filter: {type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'LinkedFKFieldID', hidden: true, dataIndex: 'linkedFkFieldId', filter: false},
		// NOTE: Moving this Link column out of position 8 will affect the drill down for the Link
		{
			header: 'Link', width: 40, dataIndex: 'linkedTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.linkedFkFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('linkedTable.label', r.json) + ' that is linked to this notification', 'ShowLink');
				}
				return 'N/A';
			}
		},
		{header: 'Notification Type', dataIndex: 'type.name', width: 50, hidden: true, filter: {type: 'combo', displayField: 'name', url: 'notificationTypeListFind.json', searchFieldName: 'typeId'}},
		{
			header: 'Priority', dataIndex: 'priority.name', width: 40, filter: {searchFieldName: 'priorityId', type: 'combo', displayField: 'name', url: 'systemPriorityListFind.json'},
			renderer: function(v, c, r) {
				const style = r.data['priority.cssStyle'];
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{header: 'Definition Order', dataIndex: 'order', width: 40, filter: {searchFieldName: 'order'}, type: 'int', hidden: true},
		{header: 'Modify Condition', dataIndex: 'type.notificationModifyCondition.name', width: 60, filter: {searchFieldName: 'entityModifyConditionId', type: 'combo', url: 'systemConditionListFind.json?emptySystemTableOnly=true'}, hidden: true},
		{header: 'CSS Style', dataIndex: 'priority.cssStyle', filter: false, sortable: false, hidden: true},
		{header: 'Assignee', width: 50, dataIndex: 'assignee', filter: {searchFieldName: 'assigneeName'}, sortable: false},
		{header: 'Assignee User', width: 50, dataIndex: 'assigneeUser.label', hidden: true, filter: {type: 'combo', searchFieldName: 'assigneeUserId', url: 'securityUserListFind.json', displayField: 'label'}},
		{header: 'Assignee Group', width: 50, dataIndex: 'assigneeGroup.label', hidden: true, filter: {type: 'combo', searchFieldName: 'assigneeGroupId', url: 'securityGroupListFind.json', displayField: 'label'}},
		{header: 'Due Date', hidden: true, dataIndex: 'dueDate', filter: {type: 'date'}, width: 35},
		{header: 'Acknowledged', dataIndex: 'acknowledged', type: 'boolean', width: 50},
		{header: 'Last Updated On', dataIndex: 'updateDate', width: 60},
		{
			header: 'Text', dataIndex: 'text', hidden: true,
			renderer: function(v) {
				return '-';
			}
		}
	],

	getLoadParams: function(firstLoad) {
		const searchPattern = TCG.getChildByName(this.getTopToolbar(), 'searchPattern');
		if (firstLoad) {
			// default to not acknowledged
			this.setFilterValue('acknowledged', false);
			searchPattern.focus();
		}

		const t = this.getTopToolbar();
		let u = TCG.getChildByName(t, 'userId');
		if (!u.isVisible()) {
			u = TCG.getChildByName(t, 'groupId');
		}
		const params = {readUncommittedRequested: true};
		if (u.getValue() !== '') {
			params[u.getName()] = u.getValue();
		}
		if (searchPattern.getValue() !== '') {
			params.searchPattern = searchPattern.getValue();
		}
		return params;
	},
	editor: {
		detailPageClass: 'Clifton.notification.NotificationWindow',
		deleteEnabled: false,
		addToolbarAddButton: function(toolBar) {
			toolBar.add({
				text: 'Run Now',
				tooltip: 'Run selected Batch Notification definition Now',
				iconCls: 'config',
				scope: this.getGridPanel(),
				handler: function() {
					const gridPanel = this;
					const sm = this.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a notification to run.', 'No Notification Selected');
					}
					else {
						const n = sm.getSelections()[0].json;
						const params = {id: n.definition.id};
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							waitMsg: 'Running...',
							timeout: 60000,
							params: params,
							conf: params,
							onLoad: function(record, conf) {
								Ext.Msg.alert('Status', 'Notification Batch has been scheduled to run now and will be completed shortly.',
									function() {
										this.reload();
									}, gridPanel);
							}
						});
						loader.load('notificationBatchRun.json');
					}
				}
			});
			toolBar.add('-');
		}
	},
	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'Assign',
			tooltip: 'Assign selected notifications to a user.',
			iconCls: 'user',
			menu: new Ext.menu.Menu({
				items: [
					{
						text: 'Assignee User',
						menu: new Ext.menu.Menu({
							items: [{
								xtype: 'combo', name: 'user', url: 'securityUserListFind.json?disabled=false', displayField: 'label',
								getListParent: function() {
									return this.el.up('.x-menu');
								},
								listeners: {
									'select': function(combo) {
										const userId = combo.getValue();
										const userName = combo.getRawValue();
										combo.reset();

										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a notification.', 'No Row(s) Selected');
										}
										else {
											Ext.Msg.confirm('Assign Selected Notification(s)', 'Are you sure you want to assign the [' + sm.getCount() + '] selected notifications to [' + userName + ']?', function(a) {
												if (a === 'yes') {
													const ids = [];
													const ut = sm.getSelections();

													for (let i = 0; i < ut.length; i++) {
														ids.push(ut[i].json.id);
													}

													const loader = new TCG.data.JsonLoader({
														waitTarget: gridPanel,
														params: {notificationIds: ids, userId: userId},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('notificationListAssigneeSave.json');
												}
											});
										}
									}
								}
							}]
						})
					},
					{
						text: 'Assignee Group',
						menu: new Ext.menu.Menu({
							items: [{
								xtype: 'combo',
								name: 'userGroup', url: 'securityGroupListFind.json?disabled=false',
								getListParent: function() {
									return this.el.up('.x-menu');
								},
								listeners: {
									'select': function(combo) {
										const groupId = combo.getValue();
										const group = combo.getRawValue();
										combo.reset();

										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a notification.', 'No Row(s) Selected');
										}
										else {
											Ext.Msg.confirm('Assign Selected Notification(s)', 'Are you sure you want to assign the [' + sm.getCount() + '] selected notifications to [' + group + ']?', function(a) {
												if (a === 'yes') {
													const ids = [];
													const ut = sm.getSelections();

													for (let i = 0; i < ut.length; i++) {
														ids.push(ut[i].json.id);
													}

													const loader = new TCG.data.JsonLoader({
														waitTarget: gridPanel,
														params: {notificationIds: ids, groupId: groupId},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('notificationListGroupAssigneeSave.json');
												}
											});
										}
									}
								}
							}]
						})
					}
				]
			})
		});

		toolBar.add('-');


		toolBar.add({
			text: 'Acknowledge',
			tooltip: 'Acknowledge selected notifications.',
			iconCls: 'checked',
			handler: function() {
				const grid = gridPanel.grid;
				const sm = grid.getSelectionModel();

				if (sm.getCount() === 0) {
					TCG.showError('Please select a notification.', 'No Row(s) Selected');
				}
				else {
					const ids = [];
					let defName = undefined;
					const ut = sm.getSelections();

					// Check which ones are acknowledgable
					for (let i = 0; i < ut.length; i++) {
						if (TCG.isNotNull(defName)) {
							if (defName !== ut[i].json.definition.name) {
								TCG.showError('To acknowledge multiple notifications, please select those that belong to the same definition. Found [' + defName + ', ' + ut[i].json.definition.name + '].', 'Invalid Selection(s)');
								return;
							}
						}
						else {
							let valid = false;
							defName = ut[i].json.definition.name;
							if (ut[i].json.definition.systemAcknowledged === false || ut[i].json.definition.systemAcknowledgedIgnorable === true) {
								valid = true;
							}
							if (valid === false) {
								TCG.showError('Selected notification definition [' + defName + '] cannot be acknowledged by users.  The system will delete the notification automatically when it it no longer valid.', 'Invalid Selection(s)');
								return;
							}
						}
						ids.push(ut[i].json.id);
					}


					Ext.Msg.show({
						title: 'Comment',
						msg: 'Please enter a comment for acknowledging the selected notifications:',
						width: 300,
						buttons: Ext.MessageBox.OKCANCEL,
						multiline: true,
						fn: function(btn, text) {
							if (btn === 'ok') {
								if (text === '') {
									TCG.showError('A comment is required.', 'No Comment Entered');
									return;
								}
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									params: {notificationIds: ids, comment: text},
									onLoad: function(record, conf) {
										gridPanel.reload();
									}
								});
								loader.load('notificationListAcknowledgedSave.json');
							}
						}

					});

				}
			}
		});

		toolBar.add('-');
	},
	configureToolsMenu: function(menu) {
		menu.add('-');
		menu.add({
			text: 'Notification Setup',
			iconCls: 'flag-red',
			scope: this,
			handler: function() {
				TCG.createComponent('Clifton.notification.definition.DefinitionSetupWindow', {openerCt: this});
			}
		});
	},

	getTooltipTextForRow: function(row, id, tooltipEventName) {
		let text = '';
		if (TCG.isNotNull(row.data.text)) {
			text = row.data.text;
			if (text.length > 2000) {
				text = 'Double click to see details';
			}
		}

		// Don't cache this so we can always load the latest
		const cmt = TCG.data.getData('notificationCommentLastByNotification.json?notificationId=' + id + '&requestedPropertiesRoot=data&requestedProperties=author.label|updated|createDate|updateDate|description', this);
		let cmtText;
		if (TCG.isNull(cmt)) {
			cmtText = '<div style="WHITE-SPACE: normal; BORDER-TOP: 1px solid #909090; PADDING: 3px">No comments added to this Notification.</div>';
		}
		else {
			let str = '<div style="WHITE-SPACE: normal; BORDER-TOP: 1px solid #909090; PADDING: 3px"><b>{0}</b> added a comment on <b>{1}</b>';
			if (cmt.updated === true) {
				str = str + ' (last updated on {2})';
			}
			str = str + '</div>' + TCG.renderText('{3}', 'PADDING-LEFT: 10px');
			cmtText = String.format(str, cmt.author.label, TCG.renderDate(cmt.createDate), TCG.renderDate(cmt.updateDate), cmt.description);
		}
		return text + '<br><br><b>Last Comment</b><br>' + cmtText;
	},


	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];

					if (eventName === 'ShowCause' || eventName === 'ShowLink') {
						let clz = row.json.table.detailScreenClass;
						let id = row.json.fkFieldId;

						if (eventName === 'ShowLink') {
							clz = row.json.linkedTable.detailScreenClass;
							id = row.json.linkedFkFieldId;
						}
						const gridPanel = grid.ownerGridPanel;
						gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
					}
				}
			}
		}
	}
});
Ext.reg('notification-grid', Clifton.notification.NotificationGridPanel);


Clifton.notification.NotificationLinkedGridPanel = Ext.extend(Clifton.notification.NotificationGridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	instructions: 'The following notifications exist in the system for this entity.',
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to not acknowledged
			this.setFilterValue('acknowledged', false);
		}
		return {
			tableName: this.tableName,
			entityId: this.getEntityId()
		};
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	onRender: function() {
		Clifton.notification.NotificationGridPanel.superclass.onRender.apply(this, arguments);

		const tbName = this.tableName;
		const entId = this.getEntityId();

		// add view button to toolbar
		const t = this.getTopToolbar();
		const vw = {
			text: 'View Definitions',
			tooltip: 'View Definitions defined for this entity',
			iconCls: 'view',
			handler: function() {
				TCG.createComponent('Clifton.notification.NotificationDefinitionLinkedListWindow',
					{
						tableName: tbName,
						entityId: entId
					}
				);
			}
		};
		t.add(vw);
		t.add('-');
	}
});
Ext.reg('notification-linked-grid', Clifton.notification.NotificationLinkedGridPanel);


Clifton.notification.NotificationDefinitionGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'notificationDefinitionGenericListFind',
	xtype: 'gridpanel',
	topToolbarSearchParameter: 'searchPattern',
	instructions: 'Notification Definitions define when notifications are generated and what they are generated for.  Use batch definitions to create scheduled notifications, and instant definitions to generate notifications as soon as an entity is updated.',
	wikiPage: 'IT/Notifications',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Definition Name', width: 150, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
		{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
		{header: 'Notification Type', width: 100, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'notificationTypeListFind.json'}},
		{header: 'Priority', width: 80, dataIndex: 'priority.name', filter: {searchFieldName: 'priorityId', type: 'combo', url: 'systemPriorityListFind.json'}},
		{header: 'Last Run Status', width: 100, dataIndex: 'status', renderer: Clifton.notification.renderNotificationStatus, filter: {type: 'list', options: Clifton.notification.StatusOptions}, hidden: true},
		{header: 'Priority Order', dataIndex: 'priority.order', width: 60, filter: {searchFieldName: 'priorityOrder'}, type: 'int', hidden: true},
		{header: 'Modify Condition', dataIndex: 'entityModifyCondition.name', width: 60, filter: {searchFieldName: 'entityModifyConditionId', type: 'combo', url: 'systemConditionListFind.json?emptySystemTableOnly=true'}, hidden: true},
		{header: 'Run Condition', dataIndex: 'type.runSystemCondition.name', width: 60, filter: {searchFieldName: 'runSystemConditionId', type: 'combo', url: 'systemConditionListFind.json'}, hidden: true},
		{header: 'Main Table', width: 80, dataIndex: 'table.label', filter: {searchFieldName: 'tableId', type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'Linked Table', width: 80, dataIndex: 'linkedTable.label', filter: {searchFieldName: 'linkedTableId', type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'Schedule', width: 80, dataIndex: 'calendarSchedule.name', filter: {searchFieldName: 'calendarScheduleId', type: 'combo', url: 'calendarScheduleListFind.json?calendarScheduleTypeName=Standard Schedules&systemDefined=false'}},
		{header: 'Instant', width: 50, dataIndex: 'instant', type: 'boolean', filter: false, sortable: false},
		{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'}
	],
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		if (this.hideNotificationTypeFilters !== true) {
			filters.push({fieldLabel: 'Notification Type', name: 'typeIdId', width: 150, xtype: 'combo', url: 'notificationTypeListFind.json', linkedFilter: 'type.name'});
		}
		filters.push({fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'});
		return filters;
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to active
			this.setFilterValue('active', true);
		}
		return {};
	},
	editor: {
		detailPageClass: {
			getItemText: function(rowItem) {
				return (rowItem.get('instant') === true) ? 'Instant' : 'Batch';
			},
			items: [
				{text: 'Instant', iconCls: 'run', className: 'Clifton.notification.definition.InstantWindow'},
				{text: 'Batch', iconCls: 'clock', className: 'Clifton.notification.definition.BatchWindow'}
			]
		},
		copyURL: 'notificationDefinitionCopy.json',
		copyNameMaxLength: 100,
		getDeleteURL: function() {
			return 'notificationDefinitionDelete.json';
		}
	}
});
Ext.reg('notification-definition-grid', Clifton.notification.NotificationDefinitionGridPanel);


Clifton.notification.NotificationDefinitionLinkedListWindow = Ext.extend(TCG.app.Window, {
	title: 'Notification Definitions',
	iconCls: 'flag-red',

	tableName: 'REQUIRED-TABLE-NAME',
	entityId: 'REQUIRED-ENTITY-ID',

	items: [{
		xtype: 'notification-definition-grid',
		instructions: 'The following Notification Definitions are defined for this entity.  Notification Definitions define when notifications are generated and what they are generated for.  Use batch definitions to create scheduled notifications, and instant definitions to generate notifications as soon as an entity is updated.',
		getLoadParams: function() {
			return {
				tableName: this.getWindow().tableName,
				entityId: this.getWindow().entityId
			};
		}
	}]
});


Clifton.notification.Toast = Ext.extend(Clifton.websocket.alert.TransientToast, {
	id: 'toast',
	title: 'My Immediate and Critical Notifications',
	stackOrder: 0,
	tpl: [
		'<tpl for=".">',
		'<p style="padding-top: 5px; padding-bottom: 3px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; color:#5f5f5f;">',
		'<b id="notification_category_{definitionId}">{[fm.htmlEncode(values.category)]}</b>',
		'</p>',
		'<tpl for="details">',
		'<p style="text-indent: 5px; padding-bottom: 3px; white-space: nowrap; {styles}; text-overflow: ellipsis; overflow: hidden; white-space: nowrap" ',
		'id="notification_detail_{id}" onmouseover="this.style.cursor=\'pointer\'">',
		'<span style="width: 16px; height: 16px; float: left; margin-left: 5px; " ',
		'onmouseover="this.style.cursor=\'pointer\'" ',
		'class="view" ext:qtip="Click to view the Entity that caused this Notification" id="notification_cause_{values.fkFieldId}_{parent.detailScreenClass}">&nbsp;</span>',
		'&nbsp;{[fm.htmlEncode(values.detail)]}',
		'</p>',
		'</tpl>',
		'</tpl>'
	],

	itemClicked: function(event, target) {
		if (typeof target != 'undefined' && typeof target.id != 'undefined') {
			const splits = target.id.split('_');
			if (splits.length >= 3) {
				if (splits[2].length > 0) {
					let classname = '';
					if (splits[1] === 'detail') {
						classname = 'Clifton.notification.NotificationWindow';
					}
					if (splits[1] === 'cause' && splits.length === 4) {
						classname = splits[3];
					}
					if (classname) {
						TCG.createComponent(classname, {
							id: splits[2],
							params: {id: splits[2]}
						});
					}
				}
				else {
					TCG.createComponent('Clifton.notification.NotificationListWindow', {});
				}
			}
		}
	},


	// Transformation functions

	mapRecordToNotification: function(record) {
		// Map the record to a simple object with its relevant details
		return {
			// Individual notification
			id: record.id,
			detail: record.subject,
			styles: record.priority.cssStyle,
			assigneeUser: record.assigneeUser && record.assigneeUser.id,
			assigneeGroup: record.assigneeGroup && record.assigneeGroup.id,
			fkFieldId: record.fkFieldId,
			priorityName: record.priority.name,
			priorityOrder: record.priority.order,

			// Category (definition)
			definitionId: record.definition && record.definition.id,
			category: record.definition && record.definition.name,
			detailScreenClass: record.table.detailScreenClass
		};
	}
});


// Manage toast subscriptions
{
	// Notification counter setup
	let numActiveNotifications = 0;
	TCG.websocket.registerSubscription('notification-count', '/user/topic/notification/count', message => {
		// Determine number of notifications
		const taskMessage = Ext.util.JSON.decode(message.body);
		numActiveNotifications = (taskMessage.reset ? 0 : numActiveNotifications) + taskMessage.numAdded;

		// Update notification count
		const notificationCounterCmp = Ext.getCmp('myNotifications');
		if (numActiveNotifications > 0) {
			notificationCounterCmp.setText(`(${numActiveNotifications})`);
			notificationCounterCmp.show();
		}
		else {
			notificationCounterCmp.hide();
		}
	});

	// Notification pop-ups setup
	const notificationPopupToast = new Clifton.notification.Toast({renderTo: Ext.getBody()});
	Clifton.websocket.alert.ToastManager.addToast(notificationPopupToast);
	let notificationPopupMap = {};
	TCG.websocket.registerSubscription('notification-popup', '/user/topic/notification/popup', message => {
		const notificationMessage = Ext.util.JSON.decode(message.body);
		if (notificationMessage.reset) {
			notificationPopupMap = {};
		}
		for (const added of notificationMessage.added) {
			notificationPopupMap[added.id] = added;
		}
		for (const removed of notificationMessage.removed) {
			delete notificationPopupMap[removed];
		}
		const notificationPopupList = Object.values(notificationPopupMap);
		notificationPopupToast.updateToastContent(notificationPopupList);
		// Pop-up notification only if any new notifications exist
		if (notificationMessage.added.length > 0 || notificationMessage.removed.length > 0) {
			notificationPopupToast.showAndHighlightToast();
			Clifton.websocket.alert.ToastManager.updatePositions();
		}
	});
}
