Clifton.notification.NotificationListWindow = Ext.extend(TCG.app.Window, {
	id: 'notificationWindow',
	title: 'My Notifications',
	iconCls: 'flag-red',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'notification-grid',
		name: 'notificationListFind',
		instructions: 'Please review the following notifications and acknowledge them when completed.  You may assign/reassign notifications that have not been acknowledged.  System acknowledged notifications will be automatically deleted during a future notification run if the condition no longer applies.',
		initComponent: function() {
			Clifton.notification.NotificationGridPanel.superclass.initComponent.call(this);

			const i = this.getColumnModel().findColumnIndex('dueDate');
			this.getColumnModel().config[i].hidden = false;
		}
	}]
});
