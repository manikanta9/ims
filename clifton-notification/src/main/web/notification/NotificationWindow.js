Clifton.notification.NotificationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Notification',
	iconCls: 'flag-red',
	height: 700,
	width: 900,

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Notification',
				tbar: [
					{
						text: 'View Cause',
						tooltip: 'View entity that caused this notification',
						iconCls: 'view',
						handler: function() {
							const fp = this.ownerCt.ownerCt.items.get(0);
							const f = fp.getForm();
							const causeId = TCG.getValue('fkFieldId', f.formValues);
							const causeDetailScreen = TCG.getValue('table.detailScreenClass', f.formValues);
							if (TCG.isNotBlank(causeDetailScreen) && TCG.isNotBlank(causeId)) {
								const cmpId = TCG.getComponentId(causeDetailScreen, causeId);
								TCG.createComponent(causeDetailScreen, {
									id: cmpId,
									params: {id: causeId}
								});
							}
							else {
								TCG.showError('Link to the cause entity is not available for this notification.', 'Not Available');
							}
						}
					}, '-', {
						text: 'View Link',
						tooltip: 'View entity that is linked to this notification',
						iconCls: 'view',
						handler: function() {
							const fp = this.ownerCt.ownerCt.items.get(0);
							const f = fp.getForm();
							const causeId = TCG.getValue('linkedFkFieldId', f.formValues);
							const causeDetailScreen = TCG.getValue('linkedTable.detailScreenClass', f.formValues);
							if (TCG.isNotBlank(causeDetailScreen) && TCG.isNotBlank(causeId)) {
								const cmpId = TCG.getComponentId(causeDetailScreen, causeId);
								TCG.createComponent(causeDetailScreen, {
									id: cmpId,
									params: {id: causeId}
								});
							}
							else {
								TCG.showError('Link to the linked entity is not available for this notification.', 'Not Available');
							}
						}
					}, '-'
				],
				items: [{
					xtype: 'formpanel',
					instructions: 'To reassign the notification, select a new assignee from the list.  If the notification is assigned to you, please acknowledge the notification when completed.  Once a notification has been acknowledged it is read only.  If the notification definition is marked as \'System Acknowledged\' then the notification will be deleted once it\'s resolved and notifications have been run again.',
					url: 'notification.json',
					labelWidth: 90,
					defaults: {},
					listeners: {
						afterload: function(panel) {
							// If company type is system defined disable name & type fields
							const f = panel.getForm();
							const sd = TCG.getValue('definition.systemAcknowledged', f.formValues);
							if (TCG.isTrue(sd)) {
								const sdi = TCG.getValue('definition.systemAcknowledgedIgnorable', f.formValues);
								if (TCG.isFalse(sdi)) {
									// Get acknowledged field and disable it
									const field = f.findField('acknowledged');
									field.setDisabled(true);
								}
							}
						}
					},
					items: [
						{
							xtype: 'panel',
							layout: 'column',
							items: [{
								columnWidth: .65,
								layout: 'form',
								labelWidth: 100,
								items: [
									{fieldLabel: 'Definition', name: 'definition.label', xtype: 'linkfield', detailPageClass: 'Clifton.notification.definition.DefinitionWindow', detailIdField: 'definition.id'},
									{fieldLabel: 'Priority', name: 'priority.name', xtype: 'displayfield'},
									{fieldLabel: 'Table', name: 'table.label', xtype: 'displayfield'},
									{fieldLabel: 'Linked Table', name: 'linkedTable.label', xtype: 'displayfield'},
									{
										fieldLabel: 'Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
										qtip: 'This Field represents the modify condition of this notification.'
									}
								]
							}, {
								columnWidth: .35,
								layout: 'form',
								labelWidth: 90,
								items: [
									{fieldLabel: 'Assignee User', name: 'assigneeUser.label', width: 150, hiddenName: 'assigneeUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false'},
									{fieldLabel: 'Assignee Group', name: 'assigneeGroup.label', width: 150, hiddenName: 'assigneeGroup.id', displayField: 'label', xtype: 'combo', url: 'securityGroupListFind.json?disabled=false'},
									{fieldLabel: 'Due Date', name: 'dueDate', xtype: 'datefield', width: 150},
									{fieldLabel: 'Acknowledged', name: 'acknowledged', xtype: 'checkbox'},
									{fieldLabel: 'Created On', name: 'createDate', xtype: 'displayfield', type: 'date'}
								]
							}]
						},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Label', name: 'label', xtype: 'hidden'},
						{fieldLabel: 'Subject', name: 'subject', xtype: 'displayfield'},
						{
							fieldLabel: 'Text', name: 'textEditor', xtype: 'htmleditor',
							anchor: '0 -230',
							submitValue: false,
							readOnly: true,
							hiddenName: 'text',
							plugins: [new TCG.form.HtmlEditorFreemarker()]
						}
					]
				}]
			},


			{
				title: 'Comments',
				items: [{
					name: 'notificationCommentListByNotification',
					xtype: 'gridpanel',
					instructions: 'The following comments are tied to this notification.',
					getLoadParams: function() {
						return {'notificationId': this.getWindow().getMainFormId()};
					},
					useBufferView: false, // allow cell wrapping
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{
							header: 'Comment', width: 400, dataIndex: 'description',
							renderer: function(value, p, record) {
								let str = '<div style="WHITE-SPACE: normal; BACKGROUND-COLOR: #f0f0f0; BORDER-TOP: 1px solid #909090; PADDING: 3px"><b>{0}</b> added a comment on <b>{1}</b>';
								if (TCG.isTrue(record.data['updated'])) {
									str = str + ' (last updated on {2})';
								}
								str = str + '</div>' + TCG.renderText('{3}', 'PADDING-LEFT: 10px');
								return String.format(str, record.data['author.label'], TCG.renderDate(record.data['createDate']), TCG.renderDate(record.data['updateDate']), value);
							}
						},
						{header: 'Author', width: 35, dataIndex: 'author.label', hidden: true},
						{header: 'Updated?', width: 20, dataIndex: 'updated', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.notification.comment.CommentWindow',
						deleteURL: 'notificationCommentDelete.json',
						getDefaultData: function(gridPanel) { // defaults notification id for the detail page
							return {
								notification: {id: gridPanel.getWindow().getMainFormId()}
							};
						}
					}
				}]
			}
		]
	}]
});
