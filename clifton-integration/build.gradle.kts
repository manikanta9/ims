import com.clifton.gradle.plugin.build.registerVariant

val apiVariant = registerVariant("api", upstreamForMain = true)

dependencies {

	////////////////////////////////////////////////////////////////////////////
	////////            Main Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	// Nothing yet

	////////////////////////////////////////////////////////////////////////////
	////////            API Dependencies                                ////////
	////////////////////////////////////////////////////////////////////////////

	apiVariant.api("com.prowidesoftware:pw-swift-integrator-data:SRU2019-8.0.0")
	apiVariant.api("com.prowidesoftware:pw-swift-integrator:SRU2019-8.0.0")

	runtimeOnly("org.apache.commons:commons-lang3")

	apiVariant.api(project(":clifton-core"))
	apiVariant.api(project(":clifton-security"))
	apiVariant.api(project(":clifton-system"))
	apiVariant.api(project(":clifton-export-messaging"))

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-security")))
}
