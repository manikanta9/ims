package com.clifton.integration.incoming.definition;

/**
 * @author terrys
 */
public final class IntegrationImportStatusBuilder {

	private String name;
	private String description;
	private Short id;


	private IntegrationImportStatusBuilder() {
	}


	public static IntegrationImportStatusBuilder anIntegrationImportStatus() {
		return new IntegrationImportStatusBuilder();
	}


	public IntegrationImportStatusBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public IntegrationImportStatusBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public IntegrationImportStatusBuilder withId(Short id) {
		this.id = id;
		return this;
	}


	public IntegrationImportStatus build() {
		IntegrationImportStatus integrationImportStatus = new IntegrationImportStatus();
		integrationImportStatus.setName(this.name);
		integrationImportStatus.setDescription(this.description);
		integrationImportStatus.setId(this.id);
		return integrationImportStatus;
	}
}
