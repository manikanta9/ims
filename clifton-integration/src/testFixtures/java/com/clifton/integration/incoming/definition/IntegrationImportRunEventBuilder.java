package com.clifton.integration.incoming.definition;

import com.clifton.integration.target.IntegrationTargetApplication;


/**
 * @author terrys
 */
public final class IntegrationImportRunEventBuilder {

	private IntegrationImportRun importRun;
	private IntegrationImportStatus status;
	private IntegrationTargetApplication targetApplication;
	private String error;
	private Long id;


	private IntegrationImportRunEventBuilder() {
	}


	public static IntegrationImportRunEventBuilder anIntegrationImportRunEvent() {
		return new IntegrationImportRunEventBuilder();
	}


	public IntegrationImportRunEventBuilder withImportRun(IntegrationImportRun importRun) {
		this.importRun = importRun;
		return this;
	}


	public IntegrationImportRunEventBuilder withStatus(IntegrationImportStatus status) {
		this.status = status;
		return this;
	}


	public IntegrationImportRunEventBuilder withTargetApplication(IntegrationTargetApplication targetApplication) {
		this.targetApplication = targetApplication;
		return this;
	}


	public IntegrationImportRunEventBuilder withError(String error) {
		this.error = error;
		return this;
	}


	public IntegrationImportRunEventBuilder withId(Long id) {
		this.id = id;
		return this;
	}


	public IntegrationImportRunEvent build() {
		IntegrationImportRunEvent integrationImportRunEvent = new IntegrationImportRunEvent();
		integrationImportRunEvent.setImportRun(this.importRun);
		integrationImportRunEvent.setStatus(this.status);
		integrationImportRunEvent.setTargetApplication(this.targetApplication);
		integrationImportRunEvent.setError(this.error);
		integrationImportRunEvent.setId(this.id);
		return integrationImportRunEvent;
	}
}
