package com.clifton.integration.incoming.definition;

import com.clifton.integration.file.IntegrationFile;

import java.util.Date;


/**
 * @author terrys
 */
public final class IntegrationImportRunBuilder {

	private Integer id;
	private IntegrationImportDefinition integrationImportDefinition;
	private IntegrationImportStatus status;
	private Date startProcessDate;
	private Date endProcessDate;
	private Date effectiveDate;
	private String error;
	private IntegrationFile file;


	private IntegrationImportRunBuilder() {
	}


	public static IntegrationImportRunBuilder anIntegrationImportRun() {
		return new IntegrationImportRunBuilder();
	}


	public IntegrationImportRunBuilder withId(Integer id) {
		this.id = id;
		return this;
	}


	public IntegrationImportRunBuilder withIntegrationImportDefinition(IntegrationImportDefinition integrationImportDefinition) {
		this.integrationImportDefinition = integrationImportDefinition;
		return this;
	}


	public IntegrationImportRunBuilder withStatus(IntegrationImportStatus status) {
		this.status = status;
		return this;
	}


	public IntegrationImportRunBuilder withStartProcessDate(Date startProcessDate) {
		this.startProcessDate = startProcessDate;
		return this;
	}


	public IntegrationImportRunBuilder withEndProcessDate(Date endProcessDate) {
		this.endProcessDate = endProcessDate;
		return this;
	}


	public IntegrationImportRunBuilder withEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
		return this;
	}


	public IntegrationImportRunBuilder withError(String error) {
		this.error = error;
		return this;
	}


	public IntegrationImportRunBuilder withFile(IntegrationFile file) {
		this.file = file;
		return this;
	}


	public IntegrationImportRun build() {
		IntegrationImportRun integrationImportRun = new IntegrationImportRun();
		integrationImportRun.setId(this.id);
		integrationImportRun.setIntegrationImportDefinition(this.integrationImportDefinition);
		integrationImportRun.setStatus(this.status);
		integrationImportRun.setStartProcessDate(this.startProcessDate);
		integrationImportRun.setEndProcessDate(this.endProcessDate);
		integrationImportRun.setEffectiveDate(this.effectiveDate);
		integrationImportRun.setError(this.error);
		integrationImportRun.setFile(this.file);
		return integrationImportRun;
	}
}
