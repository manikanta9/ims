Clifton.integration.transformation.TransformationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Transformation',
	iconCls: 'transformation',
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				tbar: [{
					text: 'Run Now',
					iconCls: 'run',
					tooltip: 'Run transformation.',
					handler: function() {
						const fp = this.ownerCt.ownerCt.items.get(0);
						const w = fp.getWindow();
						if (w.isModified() || !w.isMainFormSaved()) {
							TCG.showError('Please save your changes before running the transformation.');
							return false;
						}
						const id = fp.getForm().formValues.id;
						const defaultData = {
							transformationId: id,
							showRunDetails: true,
							integrationTransformation: {
								name: fp.getForm().formValues.name
							}
						};
						TCG.createComponent(
							'Clifton.integration.transformation.TransformationRunWindow',
							{
								defaultData: defaultData
							}
						);
					}
				}, '-', {
					text: 'Help',
					iconCls: 'help',
					handler: function() {
						TCG.openWIKI('IT/Pentaho+Data+Transformations', 'Transformations Help');
					}
				}],
				items: [{
					xtype: 'formpanel',
					url: 'integrationTransformation.json',
					labelWidth: 125,
					instructions: 'Transformation defines an ETL file or a set of files used to load data from incoming source files to destination table.',
					items: [
						{
							fieldLabel: 'Transformation Type', name: 'type', displayField: 'name', hiddenName: 'type', valueField: 'value', xtype: 'combo', mode: 'local',
							store: {
								xtype: 'arraystore', fields: ['name', 'value', 'description'], data: [
									['Data Transformation', 'DATA_TRANSFORMATION', 'Indicates that the output of this transformation is data in database tables.'],
									['File Transformation', 'FILE_TRANSFORMATION', 'Indicates that the output of this transformation is a new reformatted file (i.e. Blommberg file transformations).']]
							}
						},
						{fieldLabel: 'Definition Type', name: 'definitionType.name', hiddenName: 'definitionType.id', xtype: 'combo', url: 'integrationImportDefinitionTypeListFind.json', detailPageClass: 'Clifton.integration.definition.ImportTypeWindow'},
						{fieldLabel: 'Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{boxLabel: 'Is Template Transformation', xtype: 'checkbox', name: 'templateTransformation'},
						{boxLabel: 'Is System Defined', xtype: 'checkbox', name: 'systemDefined', disabled: true}
					]
				}]
			},


			{
				title: 'Transformation Files',
				items: [{
					xtype: 'integration-transformationFileGrid',
					rowSelectionModel: 'checkbox',
					columnOverrides: [{dataIndex: 'transformation.name', hidden: true}],
					getLoadParams: function() {
						return {transformationId: this.getWindow().getMainFormId()};
					},
					deployTransformations: function(transformationFiles, count) {
						const gridPanel = this;
						const grid = this.grid;
						const transformationFile = transformationFiles[count];
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Deploying...',
							params: {id: transformationFile.id},
							conf: {rowId: transformationFile.id},
							onLoad: function(record, conf) {
								count++;
								if (count === transformationFiles.length) { // refresh after all repos were transitioned
									const sm = grid.getSelectionModel();
									sm.clearSelections();
									gridPanel.reload();
								}
								else {
									gridPanel.deployTransformations(transformationFiles, count);
								}
							}
						});
						loader.load('integrationTransformationFileDeploy.json');
					},
					editor: {
						detailPageClass: 'Clifton.integration.transformation.TransformationFileWindow',
						getDefaultData: function(gridPanel) { // defaults client account for the detail page
							return {
								transformation: gridPanel.getWindow().getMainForm().formValues
							};
						},
						addEditButtons: function(t, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

							t.add({
								text: 'Deploy Transformations',
								tooltip: 'Set the \'Is Update Required\ flag on each transformation which will force the integrtion server to download the latest transformation file.',
								iconCls: 'run',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be reprocess.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Deploy Transformations', 'Would you like to deploy all selected transformation files?', function(a) {
											if (a === 'yes') {
												const ut = sm.getSelections();
												const transformationFiles = [];
												for (let i = 0; i < ut.length; i++) {
													const transformationFile = ut[i].json;
													if (transformationFiles.indexOf(transformationFile.id) === -1) {
														transformationFiles.push(transformationFile);
													}
												}
												const count = 0;
												gridPanel.deployTransformations(transformationFiles, count);
											}
										});
									}
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Transformation Parameters',
				items: [{
					xtype: 'gridpanel',
					name: 'integrationTransformationParameterLinkListByParent',
					columns: [
						{header: 'Link ID', dataIndex: 'id', width: 10, hidden: true},
						{header: 'Parameter ID', dataIndex: 'referenceTwo.id', width: 10, hidden: true},
						{header: 'Name', dataIndex: 'referenceTwo.name', width: 50},
						{header: 'Description', dataIndex: 'referenceTwo.description', width: 150},
						{header: 'Type', dataIndex: 'referenceTwo.typeName', width: 50},
						{header: 'Key', width: 50, dataIndex: 'referenceTwo.key'},
						{header: 'Value', width: 50, dataIndex: 'referenceTwo.value'},
						{header: 'Data Source', dataIndex: 'referenceTwo.systemDataSource.label'},
						{header: 'Secret Value', dataIndex: 'referenceTwo.secretParameterValue', type: 'boolean', width: 30}
					],
					getLoadParams: function() {
						return {
							'parentId': this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.integration.transformation.TransformationParameterWindow',
						getDefaultData: function(gridPanel) {
							return {
								referenceOne: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					addFirstToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add(new TCG.form.ComboBox({
							name: 'integrationTransformationParameter',
							url: 'integrationTransformationParameterListFind.json',
							emptyText: '< Select Parameter >',
							instructions: 'Select an Existing Parameter to use on this transformation.',
							loadAll: false,
							disableAddNewItem: true,
							width: 150,
							listWidth: 230
						}));
						toolBar.add(' ');
						toolBar.add({
							text: 'Add Existing',
							tooltip: 'Use an Existing Parameter',
							iconCls: 'add',
							handler: function() {
								const resetTask = new Ext.util.DelayedTask(function() {
									TCG.getChildByName(toolBar, 'integrationTransformationParameter').reset();
								});
								const integrationTransformationParameterId = TCG.getChildByName(toolBar, 'integrationTransformationParameter').getValue();
								if (integrationTransformationParameterId === '') {
									TCG.getChildByName(toolBar, 'integrationTransformationParameter').markInvalid('Select an Existing Parameter');
									resetTask.delay(1000);
								}
								else {
									const params = {
										'parentId': gridPanel.getWindow().getMainFormId(),
										'childId': integrationTransformationParameterId
									};
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Adding...',
										params: params,
										onLoad: function(record, conf) {
											gridPanel.reload();
											TCG.getChildByName(toolBar, 'integrationTransformationParameter').reset();
										}
									});
									loader.load('integrationTransformationParameterLinkSave.json');
								}
							}
						});
					}
				}]
			},


			{
				title: 'Import Definitions',
				items: [{
					xtype: 'integration-importDefinitionGrid',
					getLoadParams: function() {
						const params = {};
						if (this.definitionTypeName) {
							if (firstLoad) {
								const cm = this.getColumnModel();
								cm.setHidden(cm.findColumnIndex('type.name'), true);
							}
							params.typeName = this.definitionTypeName;
						}
						params.transformationId = this.getWindow().getMainFormId();
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.integration.definition.ImportDefinitionWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
})
;
