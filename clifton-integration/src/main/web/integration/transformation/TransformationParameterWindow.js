Clifton.integration.transformation.TransformationParameterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Transformation Parameter',
	iconCls: 'transformation',
	width: 700,
	height: 310,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Parameter',
			items: [{
				xtype: 'formpanel',
				url: 'integrationTransformationParameterLink.json',
				getValidationBeanName: function(url) {
					return 'integrationTransformationParameter';
				},
				getSaveURL: function() {
					return 'integrationTransformationParameterEntryLinkSave.json';
				},
				setValidationMetaData: function(data) {
					const f = this.getForm();
					for (let i = 0; i < data.length; i++) {
						const d = data[i];
						let field = f.findField('referenceTwo.' + d.name);
						if (!field) {
							field = f.findField('referenceTwo.' + d.name + '.id');
						}
						if (field && field.skipValidation !== true) {
							if (d.required === true) {
								field.allowBlank = false;
							}
							else if (d.required === false) {
								field.allowBlank = true;
							}
							if (d.maxLength && field.maxLength === Number.MAX_VALUE && field.xtype !== 'combo') {
								field.maxLength = d.maxLength;
							}
						}
					}
					if (!f.trackResetOnLoad) {
						f.isValid();
					}
				},
				setupFormFields: function(fp, type) {
					if (type) {
						switch (type) {
							case 'DATA_SOURCE':
								fp.hideField('referenceTwo.value');
								fp.hideField('referenceTwo.securitySecretValue');
								break;
							case 'STRING':
								fp.hideField('referenceTwo.systemDataSource.label');
								fp.hideField('referenceTwo.securitySecretValue');
								break;
							case 'SECRET':
								fp.hideField('referenceTwo.value');
								fp.hideField('referenceTwo.systemDataSource.label');
								break;
							default:
								break;
						}
						fp.setReadOnlyField('referenceTwo.type', true);
						fp.setEnabled(fp, true);
					}
				},
				setEnabled: function(fp, enable) {
					fp.items.each(function(fld) {
						if (fld.xtype === 'secretfield') {
							fld.innerCt.items.items.forEach(f => {
								if (f.xtype === 'button') {
									fp.setEnableField(f, enable);
								}
							});
							fp.setEnableField(fld, enable);
						}
						else if (fld.name !== 'referenceTwo.type') {
							fp.setEnableField(fld, enable);
						}
					});
				},
				setEnableField: function(field, enable) {
					if (enable) {
						field.enable();
					}
					else {
						field.disable();
					}
				},
				items: [
					{fieldLabel: 'Transformation', name: 'referenceOne.name', xtype: 'linkfield', detailPageClass: 'Clifton.integration.transformation.TransformationWindow', detailIdField: 'referenceOne.id'},
					{fieldLabel: 'Parameter ID', name: 'referenceTwo.id', hidden: true, skipValidation: true},
					{
						fieldLabel: 'Parameter Type', name: 'referenceTwo.type', hiddenName: 'referenceTwo.type', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', disableAddNewItem: true,
						store: new Ext.data.ArrayStore({
							fields: ['value', 'name', 'description'],
							data: [
								['STRING', 'String', 'Nonformatted string value.'],
								['SECRET', 'Secret', 'Encrypted secret value.'],
								['DATA_SOURCE', 'Data Source', 'Connection parameters for an existing data source.']
							]
						}),
						listeners: {
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.setupFormFields(fp, record.data.value);
							}
						}
					},
					{fieldLabel: 'Name', name: 'referenceTwo.name'},
					{fieldLabel: 'Description', name: 'referenceTwo.description'},
					{fieldLabel: 'Key', name: 'referenceTwo.key'},
					{fieldLabel: 'Value', name: 'referenceTwo.value'},
					{fieldLabel: 'Data Source', name: 'referenceTwo.systemDataSource.label', hiddenName: 'referenceTwo.systemDataSource.id', xtype: 'combo', url: 'systemDataSourceListFind.json', loadAll: true, disableAddNewItem: true},
					{fieldLabel: 'Secret Value', xtype: 'secretfield', name: 'referenceTwo.securitySecretValue', tableName: 'IntegrationTransformationParameterLink'}
				],
				listeners: {
					afterload: function(fp) {
						const formValues = fp.getForm().formValues;
						const type = TCG.getValue('referenceTwo.type', formValues);
						fp.setupFormFields(fp, type);
					},
					afterRender: function() {
						const fp = this;
						fp.setEnabled(fp, false);
					}
				}
			}]
		}
			, {
				title: 'Transformations',
				items: [{
					xtype: 'gridpanel',
					name: 'integrationTransformationParameterLinkListByChild',
					additionalPropertiesToRequest: 'referenceOne.id',
					columns: [
						{header: 'Transformation Name', dataIndex: 'referenceOne.name', width: 100, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'referenceOne.description', width: 200, hidden: true},
						{header: 'Transformation Type', dataIndex: 'referenceOne.type', width: 60},
						{header: 'Import Type', dataIndex: 'referenceOne.definitionType.name', width: 60, filter: {searchFieldName: 'definitionTypeName'}},
						{header: 'Template', dataIndex: 'referenceOne.templateTransformation', type: 'boolean', width: 30},
						{header: 'System Defined', dataIndex: 'referenceOne.systemDefined', type: 'boolean', width: 30}
					],
					getLoadParams: function(firstLoad) {
						return {
							'childId': TCG.getValue('referenceTwo.id', this.getWindow().getMainForm().formValues)
						};
					},
					editor: {
						detailPageClass: 'Clifton.integration.transformation.TransformationWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceOne.id;
						}
					}
				}]
			}]
	}]
});
