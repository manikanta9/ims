Clifton.integration.transformation.IntegrationImportTransformationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Import Transformation',
	iconCls: 'transformation',

	items: [{
		xtype: 'formpanel',
		instructions: 'Import Types are used for classification purposes to group definitions into categories.',
		url: 'integrationImportTransformation.json',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Import Definition', name: 'definition.name', detailIdField: 'definition.id', xtype: 'linkfield', detailPageClass: 'Clifton.integration.definition.ImportDefinitionWindow'},
			{
				fieldLabel: 'Transformation Type', name: 'transformationSystemBean.type.name', hiddenName: 'transformationSystemBean.type.id', xtype: 'combo', url: 'systemBeanTypeListFind.json?groupName=Import Transformation Generator',
				detailPageClass: 'Clifton.system.bean.TypeWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Import Transformation Generator', alias: 'Transformation Type'}}};
				},
				listeners: {
					select: function(combo, record, index) {
						const f = combo.getParentForm().getForm();
						f.findField('transformationSystemBean.id').clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'Transformation Bean', name: 'transformationSystemBean.name', hiddenName: 'transformationSystemBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Import Transformation Generator',
				detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Import Transformation Generator', alias: 'Transformation Generator'}}};
				},
				listeners: {
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						const transformationTypeId = f.findField('transformationSystemBean.type.id').value;
						if (!transformationTypeId) {
							f.findField('transformationSystemBean.id').clearAndReset();
						}

						queryEvent.combo.store.baseParams = {
							typeId: transformationTypeId
						};
					},
					select: function(combo, record, index) {
						combo.getParentForm().setFormValue('transformationSystemBean.description', record.json.description, true);
					}
				}
			},
			{name: 'transformationSystemBean.description', xtype: 'textarea', readOnly: true},
			{fieldLabel: 'Execution Order', name: 'order', xtype: 'spinnerfield'}
		]
	}]
});
