Clifton.integration.transformation.TransformationFileWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Transformation File',
	iconCls: 'transformation',
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Info',
			tbar: {
				xtype: 'document-toolbar',
				tableName: 'IntegrationTransformationFile',
				addToolbarButtons: function() {
					this.items.push({
						text: 'Help',
						iconCls: 'help',
						handler: function() {
							TCG.openWIKI('IT/How+to+Create+or+Update+a+Data+Transformation', 'Transformations Help');
						}
					});
					this.items.push({xtype: 'tbseparator', hidden: true});
				}
			},
			items: [{
				xtype: 'formpanel',
				url: 'integrationTransformationFile.json',
				labelWidth: 145,
				instructions: '',
				wikiPage: 'IT/Investment Rebalancing',
				items: [
					{fieldLabel: 'Transformation', name: 'transformation.name', xtype: 'linkfield', detailPageClass: 'Clifton.integration.transformation.TransformationWindow', detailIdField: 'transformation.id'},
					{
						fieldLabel: 'File Type', name: 'fileType', displayField: 'name', hiddenName: 'fileType', valueField: 'value', xtype: 'combo', mode: 'local',
						store: {
							xtype: 'arraystore', fields: ['name', 'value', 'description'], data: [
								['Effective Date Transformation File', 'EFFECTIVE_DATE_TRANSFORMATION', 'Indicates that this file is used to update the effective date on the integration import run.'],
								['Data Transformation File', 'DATA_TRANSFORMATION', 'Indicates that this file is used for data transformations.'],
								['File to File Transformation', 'FILE_TO_FILE_TRANSFORMATION', 'Indicates that this file is used for to transform a file to another file.'],
								['Data File', 'DATA_FILE', 'A file that contains data used for the transformation.  For example, security symbol mapping data.']]
						}
					},
					{fieldLabel: 'File Name', name: 'fileName'},
					{fieldLabel: 'Order', name: 'order'},
					{boxLabel: 'Append file', xtype: 'checkbox', name: 'appendFile', checked: false},
					{boxLabel: 'Is Runnable', xtype: 'checkbox', name: 'runnable', checked: true},
					{boxLabel: 'Force file to update on next run', xtype: 'checkbox', checked: true, name: 'updateRequired', qtip: 'Will force the integration server to download the latest version of the transformation.'}
				]
			}]
		}, {
			title: 'Revision History',
			items: [{
				xtype: 'document-version-grid',
				tableName: 'IntegrationTransformationFile'
			}]
		}]
	}]
});
