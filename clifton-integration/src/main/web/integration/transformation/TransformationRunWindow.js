Clifton.integration.transformation.TransformationRunWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'transformationRunWindow',
	iconCls: 'run',
	title: 'Transformation Run',
	width: 800,
	height: 300,
	modal: true,
	forceModified: true,
	doNotWarnOnCloseModified: true,
	applyButtonText: 'Run',
	cancelButtonText: 'Close',
	hideOKButton: true,
	items: [{
		xtype: 'formpanel',
		items: [
			{fieldLabel: 'id', name: 'transformationId', xtype: 'hidden'},
			{fieldLabel: 'Show Run Buffer', name: 'showRunDetails', xtype: 'hidden'},
			{fieldLabel: 'Transformation', name: 'integrationTransformation.name', readOnly: true, submitValue: false},
			{fieldLabel: 'Results', name: 'results.message', xtype: 'textarea', height: 150, readOnly: true, submitValue: false}
		],
		getSaveURL: function() {
			return 'integrationTransformationExecute.json';
		}
	}]
});
