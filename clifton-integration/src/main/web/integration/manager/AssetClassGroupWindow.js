Clifton.integration.manager.AssetClassGroupWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'integrationManagerAssetClassGroup.json',

	getClassName: function(config, entity) {
		return 'Clifton.integration.manager.AssetClassGroupWindowImpl';
	}
});

Clifton.integration.manager.AssetClassGroupWindowImpl = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Asset Class Group',
	iconCls: 'manager',
	width: 750,
	defaultDataIsReal: true,
	loadDefaultDataAfterRender: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Group',
				items: [{
					xtype: 'formpanel',
					url: 'integrationManagerAssetClassGroup.json',
					instructions: 'Import group defines a unique custodian file format and includes mappings of custodian specific asset classes to our position types. Optional "Unmapped Type" field, specifies the type to be assigned to unmapped rows. Avoid using this field, if possible, to catch and properly map new asset classes.',
					getDefaultData: function(win) {
						const dd = win.defaultData || {};
						if (dd.businessCompanyId) {
							const businessCompany = TCG.data.getData('businessCompany.json?id=' + dd.businessCompanyId, this);
							if (businessCompany) {
								dd.businessCompany = businessCompany;
							}
						}
						return dd;
					}
					,
					items: [
						{fieldLabel: 'Name', name: 'name'},
						{
							fieldLabel: 'Business Company',
							xtype: 'combo',
							url: 'businessCompanyListFind.json?companyTypeNames=Custodian,Investment Manager,Broker,Other Issuer',
							disableAddNewItem: true,
							detailPageClass: 'Clifton.business.company.CompanyWindow',
							name: 'businessCompany.nameExpandedWithType',
							hiddenName: 'businessCompanyId',
							displayField: 'nameExpandedWithType'
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Unmapped Type', name: 'unmappedPositionType.name', hiddenName: 'unmappedPositionType.id', xtype: 'combo', url: 'integrationManagerPositionTypeList.json', loadAll: true, displayField: 'label'},
						{boxLabel: 'Fail import as MAPPING_ERROR if unmapped row is present', name: 'requireAllAssetClassMappings', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Asset Class Mappings',
				items: [{
					name: 'integrationManagerAssetClassListByGroup',
					xtype: 'gridpanel',
					instructions: 'The asset classes have been mapped to corresponding position types for selected group.  Show Bank Code column to see Bank Code for duplicate Asset Classes.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Asset Class', width: 100, dataIndex: 'assetClass'},
						{header: 'Bank Code', width: 50, dataIndex: 'managerBankCode'},
						{header: 'Security Description', width: 70, dataIndex: 'securityDescriptionPattern'},
						{header: 'Position Type', width: 100, dataIndex: 'type.name'},
						{header: 'Mapping Note', width: 100, dataIndex: 'mappingNote', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.integration.manager.AssetClassMappingWindow',
						deleteURL: 'integrationManagerAssetClassDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								group: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {
							'groupId': this.getWindow().getMainFormId()
						};
					}
				}]
			}
			,


			{
				title: 'Bank Code Mappings',
				items: [{
					name: 'integrationManagerBankMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Bank code mappings that are used to modify bank accounts during the manager load.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Asset Class Group', width: 100, dataIndex: 'group.name'},
						{header: 'From Bank Code', width: 50, dataIndex: 'fromBankCode'},
						{header: 'From Bank Code', width: 50, dataIndex: 'toBankCode'}
					],
					editor: {
						detailPageClass: 'Clifton.integration.manager.PositionBankMappingWindow',
						deleteURL: 'integrationManagerBankMappingDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								group: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {'assetClassGroupId': this.getWindow().getMainFormId()};
					}
				}]
			}
			,


			{
				title: 'Import Definitions',
				items: [{
					xtype: 'integration-importDefinitionGrid',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('type.name'), true);
							cm.setHidden(cm.findColumnIndex('raiseExternalEvent'), true);
						}

						return {'managerAssetClassGroupId': this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.integration.definition.ImportDefinitionWindow',
						getDeleteURL: function() {
							return 'integrationManagerAssetClassGroupImportDefinitionDelete.json';
						},
						getDeleteParams: function(selectionModel) {
							const definitionId = selectionModel.getSelected().id;
							const importGroupId = this.getWindow().getMainForm().formValues.id;
							return {
								importDefinitionId: definitionId,
								importGroupId: importGroupId
							};
						},
						addEditButtons: function(t, gp) {
							t.add(new TCG.form.ComboBox({
								name: 'importDefinitionName',
								displayField: 'name',
								url: 'integrationImportDefinitionListFind.json',
								emptyText: '< Select Import Definition >',
								width: 200,
								listWidth: 250
							}));
							t.add({
								text: 'Add Selected',
								xtype: 'button',
								tooltip: 'Add a destination to this import group',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const importDestination = TCG.getChildByName(t, 'importDefinitionName');
									const importDestinationId = importDestination.getValue();
									if (importDestinationId === '') {
										TCG.showError('You must first select an Import Destination from the list.');
									}
									else {
										const importGroupId = gp.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gp,
											waitMsg: 'Adding...',
											params: {importGroupId: importGroupId, importDefinitionId: importDestinationId},
											onLoad: function(record, conf) {
												gp.reload();
												TCG.getChildByName(t, 'importDefinitionName').reset();
											}
										});
										loader.load('integrationManagerAssetClassGroupImportDefinitionLink.json');
									}
								}
							});
							t.add('-');
							TCG.grid.GridEditor.prototype.addToolbarDeleteButton.apply(this, arguments);
						}
					}
				}]
			}
		]
	}]
})
;
