Clifton.integration.manager.AssetClassMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Asset Class Mapping',
	iconCls: 'manager',
	height: 600,

	items: [{
		xtype: 'formpanel',
		url: 'integrationManagerAssetClass.json',
		instructions: 'This is a mapping of a custodian specific asset class to our position type. If a Bank Code is specified, this mapping applies to only one account. ' +
				'Note: Duplicate Asset Classes are allowed only if one or more of them have a Bank Code set.',
		labelFieldName: 'assetClass',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Group', name: 'group.name', detailIdField: 'group.id', xtype: 'linkfield', detailPageClass: 'Clifton.integration.manager.AssetClassGroupWindow'},
			{fieldLabel: 'Position Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', loadAll: true, url: 'integrationManagerPositionTypeList.json'},
			{fieldLabel: 'Asset Class', name: 'assetClass'},
			{fieldLabel: 'Bank Code', name: 'managerBankCode', mutuallyExclusiveFields: ['mapSecurityForAllManagers'], qtip: 'Add a Bank Code to make this an account-specific mapping.'},
			{fieldLabel: 'Security Description', name: 'securityDescriptionPattern', qtip: 'Add a Security Description to make this a security-specific mapping.'},
			{boxLabel: ' Use exact mapping on security description instead of LIKE with wildcards on either side.', name: 'securityDescriptionPatternExactMatch', xtype: 'checkbox', requiredFields: ['securityDescriptionPattern']},
			{boxLabel: ' Map Security Description across all managers', name: 'mapSecurityForAllManagers', xtype: 'checkbox', mutuallyExclusiveFields: ['managerBankCode']},
			{fieldLabel: 'Mapping Note', name: 'mappingNote', xtype: 'textarea'}
		]
	}]
});
