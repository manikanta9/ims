Clifton.integration.manager.PositionBankMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Bank Code Mapping',
	iconCls: 'manager',

	items: [{
		xtype: 'formpanel',
		url: 'integrationManagerBankMapping.json',
		instructions: 'Allows mapping a bank one bank code to another.  ' +
				'NOTE: the From Bank Code uses contains.  So, if the from code is [00P3_GMO] then it will match [00P3_GMO INTERNATIONAL INTRINSIC VA].',
		labelFieldName: 'label',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Group', name: 'group.name', detailIdField: 'group.id', xtype: 'linkfield', detailPageClass: 'Clifton.integration.manager.AssetClassGroupWindow'},
			{fieldLabel: 'From Bank Code', name: 'fromBankCode', qtip: 'If the target code contains this code it will match.  For example, [00P3_GMO] will match [00P3_GMO INTERNATIONAL INTRINSIC VA].'},
			{fieldLabel: 'To Bank Code', name: 'toBankCode'},
			{fieldLabel: 'Asset Class', name: 'assetClass'},
			{fieldLabel: 'Security Description Pattern', name: 'securityDescriptionPattern'}
		]
	}]
});
