Clifton.integration.file.FileWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'integrationFile.json',

	getClassName: function(config, entity) {
		let fileSourceType = '';
		if (entity && entity.fileSourceType) {
			fileSourceType = entity.fileSourceType;
		}
		// TODO: Make separate classes with an xtype for the form tab instead of changing default data
		if (config.defaultData) {
			config.defaultData.fileSourceType = fileSourceType;
		}
		return 'Clifton.integration.file.FileWindowImpl';
	}
});

Clifton.integration.file.FileWindowImpl = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Integration File',
	iconCls: 'run',
	width: 1000,
	height: 800,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const typeName = (w.defaultData) ? w.defaultData.fileSourceType : undefined;

		switch (typeName) {
			case 'EXPORT':
				tabs.insert(1, this.exportTab);
				break;
			default:
				tabs.insert(1, this.importRunTab);
				break;
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Integration File',
			tbar: [{
				text: 'Download File',
				tooltip: 'View the file that was processed.',
				iconCls: 'view',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();

					TCG.downloadFile('integrationFileDownload.json?id=' + w.getMainFormId(), null, this);
				}
			}, '-', {
				text: 'Reprocess File',
				tooltip: 'Re-run the entire import processing with the file from this run.',
				iconCls: 'run',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();

					const params = {integrationFileId: w.getMainFormId()};
					const loader = new TCG.data.JsonLoader({
						waitTarget: f,
						waitMsg: 'Starting...',
						params: params,
						onLoad: function(record, conf) {
							Ext.Msg.alert('Status', 'The process was started.');
						}
					});
					loader.load('integrationFileRerun.json');
				}
			}],
			items: [{
				xtype: 'formpanel',
				url: 'integrationFile.json',
				labelFieldName: 'id',
				readOnly: true,
				instructions: 'An Integration File is an actual file from external data provider. It is stored and can be browsed on our file system.',
				items: [

					{fieldLabel: 'File Name', name: 'fileDefinition.fileName', xtype: 'linkfield', detailIdField: 'fileDefinition.id', detailPageClass: 'Clifton.integration.file.FileDefinitionWindow'},
					{fieldLabel: 'External File Name', name: 'fileDefinition.externalFileName'},
					{fieldLabel: 'Path', name: 'fileNameWithPath'},
					{fieldLabel: 'Encrypted Path', name: 'encryptedFileNameWithPath'},
					{fieldLabel: 'Password', name: 'password'},
					{fieldLabel: 'Received Date', name: 'receivedDate'},
					{fieldLabel: 'Effective Date', name: 'effectiveDate'},
					{fieldLabel: 'File Source', name: 'fileSourceType', qtip: 'Indicates whether this file was imported via FTP or manually'},
					{
						fieldLabel: 'Import Source', name: 'receivedFromSource.name', xtype: 'linkfield', detailIdField: 'receivedFromSource.id', detailPageClass: 'Clifton.integration.source.SourceWindow',
						qtip: 'Only populated in cases like FTP where the source can be determined from the input location'
					},
					{
						boxLabel: 'Source Data Loaded', name: 'sourceDataLoaded', xtype: 'checkbox',
						qtip: 'Indicates whether source data has been loaded.  Important when multiple transformations are ran against a single file'
					},
					{boxLabel: 'Deleted', name: 'deleted', xtype: 'checkbox'},
					{boxLabel: 'Error Loading', qtip: 'An error occured in the archiving process before any transformation were run.', name: 'errorOnLoad', xtype: 'checkbox'},
					{fieldLabel: 'Error Message', name: 'error', xtype: 'textarea', anchor: '-35 -310'}

				]
			}]
		}]
	}],

	importRunTab: {
		title: 'Import Runs',
		items: [{
			xtype: 'integration-importRunEventGrid',
			getLoadParams: function() {
				return {fileId: this.getWindow().getMainFormId()};
			},
			listeners: {
				beforeRender: function(grid) {
					const cm = grid.getColumnModel();
					cm.setHidden(cm.findColumnIndex('error'), true);
				}
			}
		}]
	},

	exportTab: {
		title: 'Exports',
		items: [{
			xtype: 'integration-outgoing-exportGrid',
			getLoadParams: function() {
				return {integrationFileId: this.getWindow().getMainFormId()};
			}
		}]
	}
});
