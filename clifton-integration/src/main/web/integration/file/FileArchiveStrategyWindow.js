Clifton.integration.file.FileArchiveStrategyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'File Archive Strategy',
	iconCls: 'run',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Archive Strategy',
				items: [{
					xtype: 'formpanel',
					url: 'integrationFileArchiveStrategy.json',
					readOnly: true,
					labelWidth: 170,
					instructions: 'A File Archive Strategy describes how files from a particular import source will be archived.  ' +
							'For example, files from Goldman Sachs might be kept in a folder called "GSC" for 90 days. ' +
							'Use 0 days to never delete files.',
					items: [
						{fieldLabel: 'Strategy Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Path To Archive Folder', name: 'pathToArchiveFolder'},
						{fieldLabel: 'Days To Keep File', name: 'daysToKeepFile'},
						{fieldLabel: 'Days To Keep Encrypted File', name: 'daysToKeepEncryptedFile'},
						{fieldLabel: 'Days To Keep Source Data', name: 'daysToKeepSourceData'},
						{boxLabel: 'Delete history when new file arrives', xtype: 'checkbox', name: 'deleteOnNewFile', qtip: 'If checked, any related archive files will be deleted when a new one loads'}
					]
				}]
			},


			{
				title: 'Import Sources',
				items: [{
					xtype: 'integration-sourceGrid',
					columnOverrides: [{dataIndex: 'sourceCompanyId', hidden: true}, {dataIndex: 'fileArchiveStrategy.name', hidden: true}],
					getLoadParams: function() {
						return {'fileArchiveStrategyId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
