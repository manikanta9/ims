Clifton.integration.file.FileDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'File Definition',
	iconCls: 'run',
	width: 800,
	height: 850,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'File Definition',
			items: [{
				xtype: 'formpanel',
				url: 'integrationFileDefinition.json',
				labelFieldName: 'id',
				labelWidth: 150,
				instructions: 'File Definitions represent external files that are imported from a manager or broker.  When a file is loaded, ' +
						'the file name of the uploaded file will be used to look up the definition by matching to the [File Name] property.  If no file is found, ' +
						'the definition will be looked up using the [External File Name].  Last, only for files that arrive from an import source (i.e. via FTP), ' +
						'if no definition is found then it uses the [File Pattern] to attempt to locate a definition.  ' +
						'If a definition is still not found, the [UNKNOWN] file definition will be used.',
				getDefaultData: function(win) {
					const dd = win.defaultData || {};

					if (dd.priceMarketDataSourcePurposeId) {
						const purpose = TCG.data.getData('marketDataSourcePurpose.json?id=' + dd.priceMarketDataSourcePurposeId, this, 'marketdata.purpose.' + dd.priceMarketDataSourcePurposeId);
						dd.priceMarketDataSourcePurpose = purpose;
					}

					return dd;
				},
				items: [
					{fieldLabel: 'File Type', name: 'type.name', xtype: 'combo', hiddenName: 'type.id', url: 'integrationFileDefinitionTypeList.json', loadAll: true, searchFieldName: 'name', detailPageClass: 'Clifton.integration.file.FileDefinitionTypeWindow'},
					{fieldLabel: 'Import Source', name: 'source.name', hiddenName: 'source.id', queryParam: 'name', xtype: 'combo', url: 'integrationSourceListFind.json', detailPageClass: 'Clifton.integration.source.SourceWindow'},
					{fieldLabel: 'File Description', name: 'fileDescription', xtype: 'textarea', height: 70, qtip: 'Description of the file contents'},
					{fieldLabel: 'Source Table List', name: 'sourceTableNameWithSchemaList', xtype: 'textarea', height: 70, qtip: 'Need for 2 step transformations that load data into source tables then final tables.'},
					{fieldLabel: 'Pricing Purpose', name: 'priceMarketDataSourcePurpose.name', hiddenName: 'priceMarketDataSourcePurposeId', queryParam: 'name', xtype: 'combo', url: 'marketDataSourcePurposeListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourcePurposeWindow'},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'File Name', name: 'fileName'},
					{fieldLabel: 'File Pattern', name: 'fileNamePattern', qtip: 'Regular expression for matching the entire file name, including date and time.'},
					{fieldLabel: 'Date Pattern', name: 'effectiveDatePattern', qtip: 'Regular expression for matching the date portion of the file name. For patterns matching the entire file name, return the date in the first caputure group.'},
					{fieldLabel: 'Date Format', name: 'effectiveDateFormat', qtip: 'Date format "MM-dd-yyy" for matching the date portion of the file name.'},
					{fieldLabel: 'External File Name', name: 'externalFileName'},
					{
						fieldLabel: 'Encryption Type', name: 'encryptionType', displayField: 'name', valueField: 'value', xtype: 'combo', mode: 'local', emptyText: 'None',
						store: {
							xtype: 'arraystore',
							fields: ['value', 'name'],
							data: [
								['PGP', 'PGP'],
								['DEFAULT', 'DEFAULT']
							]
						}
					},
					{fieldLabel: 'File Column Names', name: 'fileColumnNames', xtype: 'textarea', qtip: 'Optional comma-delimited list of column names.'},
					{
						fieldLabel: 'Time Zone Name', name: 'timeZoneName', xtype: 'combo', url: 'calendarTimeZoneListFind.json', displayField: 'name',
						emptyText: '', valueField: 'name', qtip: 'The time zone of the file name\'s timestamp. Needed to convert the timestamp into an accurate effective date'
					},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Override Archive Strategy', name: 'overrideFileArchiveStrategy.name', url: 'integrationFileArchiveStrategyListFind.json', xtype: 'combo', hiddenName: 'overrideFileArchiveStrategy.id', detailPageClass: 'Clifton.integration.file.FileArchiveStrategyWindow'},
					{fieldLabel: 'Additional Archive Path', name: 'additionalArchivePath'},
					{boxLabel: 'Archive Duplicates', name: 'archiveDuplicates', xtype: 'checkbox'},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Received By', name: 'receivedByTime', xtype: 'timefield'},
					{
						boxLabel: 'One Per Day', name: 'onePerDay', xtype: 'checkbox', qtip: 'Checked if the file is sent only once per day',
						listeners: {
							check: function(f, checked) {
								// On changes reset contract dropdown
								const panel = TCG.getParentFormPanel(f);
								const tz = panel.getForm().findField('timeZoneName');
								if (checked) {
									panel.hideField('timeZoneName');
								}
								else {
									tz.show();
								}
							}
						}
					},
					{boxLabel: 'File Not Expected Daily', name: 'fileNotExpectedDaily', xtype: 'checkbox', qtip: 'Indicates whether a file should NOT be expected on a daily basis'}
				],
				listeners: {
					afterload: function(panel) {
						if (panel.getFormValue('onePerDay')) {
							panel.hideField('timeZoneName');
						}
					}
				}
			}]
		},
			{
				title: 'Integration Files',
				items: [{
					xtype: 'integration-fileGrid',
					getLoadParams: function() {
						return {'fileDefinitionId': this.getWindow().getMainFormId()};
					}
				}]
			},
			{
				title: 'Import Definitions',
				items: [{
					xtype: 'integration-importDefinitionGrid',
					getLoadParams: function() {
						return {'fileDefinitionId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
