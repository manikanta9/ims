Clifton.integration.file.FileDefinitionTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'File Definition Type',
	iconCls: 'run',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'File Type',
			items: [{
				xtype: 'formpanel',
				url: 'integrationFileDefinitionType.json',
				instructions: 'File Definition Types distinguish files from one another, for instance, broker files vs. manager files.',
				readOnly: true,
				items: [
					{fieldLabel: 'Type Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
				]
			}]
		},


			{
				title: 'File Definitions',
				items: [{
					xtype: 'integration-fileDefinitionGrid',
					columnOverrides: [{dataIndex: 'type.name', hidden: true}, {dataIndex: 'archiveDuplicates', hidden: true}],
					getLoadParams: function() {
						return {'typeId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
