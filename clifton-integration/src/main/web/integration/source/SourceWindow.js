Clifton.integration.source.SourceWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'integrationSource.json',

	getClassName: function(config, entity) {
		return 'Clifton.integration.source.SourceWindowImpl';
	}
});

Clifton.integration.source.SourceWindowImpl = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Import Source',
	iconCls: 'run',
	height: 500,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const typeName = (w.defaultData) ? w.defaultData.type.name : undefined;

		switch (typeName) {
			case 'Export':
				tabs.insert(1, this.exportTab);
				break;
			default:
				tabs.insert(1, this.fileDefinitionsTab);
				tabs.insert(2, this.filesTab);
				break;
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Source',
			items: [{
				xtype: 'formpanel',
				url: 'integrationSource.json',
				labelWidth: 130,
				instructions: 'Import Sources describe where a particular file comes from and which folders it is associated with.',
				getDefaultData: function(win) {
					const dd = win.defaultData || {};
					if (dd.sourceCompanyId) {
						const sourceCompany = TCG.data.getData('businessCompany.json?id=' + dd.sourceCompanyId, this, 'integration.source.company' + dd.sourceCompanyId);
						if (sourceCompany) {
							dd.sourceCompany = sourceCompany;
						}
					}
					return dd;
				},
				listeners: {
					afterload: function(fp) {
						const f = this.getForm();
						const privateKeySecret = TCG.getValue('decryptionPrivateKeySecuritySecret', f.formValues);
						if (privateKeySecret) {
							fp.setFormValue('decryptionPrivateKeySecuritySecret.secretString', '[ENCRYPTED]', true);
						}
						const privateKeyPasswordSecret = TCG.getValue('decryptionPrivateKeyPasswordSecuritySecret', f.formValues);
						if (privateKeyPasswordSecret) {
							fp.setFormValue('decryptionPrivateKeyPasswordSecuritySecret.secretString', '[ENCRYPTED]', true);
						}
					}
				},
				items: [
					{fieldLabel: 'Source Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', url: 'integrationSourceTypeListFind.json', detailPageClass: 'Clifton.integration.source.SourceTypeWindow'},
					{fieldLabel: 'Source Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{fieldLabel: 'Source Company', name: 'sourceCompany.nameExpandedWithType', hiddenName: 'sourceCompanyId', displayField: 'nameExpandedWithType', xtype: 'combo', loadAll: false, url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', qtip: 'Id of the BusinessCompany representing this source'},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'Archive Strategy', name: 'fileArchiveStrategy.name', hiddenName: 'fileArchiveStrategy.id', detailIdField: 'fileArchiveStrategy.id', disableAddNewItem: true, xtype: 'combo', url: 'integrationFileArchiveStrategyListFind.json', detailPageClass: 'Clifton.integration.file.FileArchiveStrategyWindow'},
					{fieldLabel: 'Additional Archive Path', name: 'additionalArchivePath'},
					{xtype: 'label', html: '<hr/>'},
					{fieldLabel: 'FTP Root Folder', name: 'ftpRootFolder'},
					{boxLabel: 'FTP watching is enabled', name: 'ftpWatchingEnabled', xtype: 'checkbox'},
					{boxLabel: 'Source input folder watching is enabled', name: 'sourceInputFolderWatchingEnabled', xtype: 'checkbox'},
					{boxLabel: 'Decryption is forced regardless of file extension', name: 'forceDecryption', xtype: 'checkbox'},
					{
						boxLabel: 'Force effective date to load using the effective date transformation',
						name: 'forceEffectiveDateLoadFromJobFile', xtype: 'checkbox',
						qtip: 'Force effective date to be loaded using the effective date transformation file on the import definition.  This will override the effective date on the file name.  So if this is true and an effective date transformation is exists, you will need to edit the effect in the file to override it.'
					},
					{boxLabel: 'Source sends encrypted messages', name: 'encryptedDataSendingEnabled', xtype: 'checkbox'},
					{
						xtype: 'fieldset',
						title: 'Encryption Options',
						collapsed: true,
						items: [
							{fieldLabel: 'Encryption Public Key', name: 'encryptionPublicKey', xtype: 'textarea'},
							{fieldLabel: 'Decryption Public Key', name: 'decryptionPublicKey', xtype: 'textarea'},
							{fieldLabel: 'Decryption Private Key', xtype: 'secrettextarea', name: 'decryptionPrivateKeySecuritySecret', tableName: 'IntegrationSource'},
							{fieldLabel: 'Decryption Private Key Password', xtype: 'secretfield', name: 'decryptionPrivateKeyPasswordSecuritySecret', tableName: 'IntegrationSource'}
						]
					}
				]
			}]
		}]
	}],

	fileDefinitionsTab: {
		title: 'File Definitions',
		items: [{
			xtype: 'integration-fileDefinitionGrid',
			columnOverrides: [{dataIndex: 'source.name', hidden: true}, {dataIndex: 'archiveDuplicates', hidden: true}],
			getLoadParams: function() {
				return {'sourceId': this.getWindow().getMainFormId()};
			},
			editor: {
				detailPageClass: 'Clifton.integration.file.FileDefinitionWindow',
				getDefaultData: function(gridPanel, row) {
					return {
						source: gridPanel.getWindow().getMainForm().formValues
					};
				}
			}
		}]
	},

	filesTab: {
		title: 'Files',
		items: [{
			xtype: 'integration-fileGrid',
			columnOverrides: [{dataIndex: 'source.name', hidden: true}],
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to last 30 days
					this.setFilterValue('receivedDate', {'after': new Date().add(Date.DAY, -30)});
				}
				return {'sourceId': this.getWindow().getMainFormId()};
			}
		}]
	},

	exportTab: {
		title: 'Exports',
		items: [{
			xtype: 'integration-outgoing-exportGrid',
			getLoadParams: function() {
				return {'integrationSourceId': this.getWindow().getMainFormId()};
			}
		}]
	}
});
