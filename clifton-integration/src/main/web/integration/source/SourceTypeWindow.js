Clifton.integration.source.SourceTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Source Type',
	iconCls: 'run',
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Source Type',
			items: [{
				xtype: 'formpanel',
				url: 'integrationSourceType.json',
				labelWidth: 145,
				items: [
					{fieldLabel: 'Name', name: 'name', readOnly: true},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{boxLabel: 'File source requires a FileDefinition for archiving.', name: 'fileDefinitionRequired', xtype: 'checkbox', readOnly: true}
				]
			}]
		},
			{
				title: 'Sources',
				items: [{
					xtype: 'integration-sourceGrid',
					getLoadParams: function() {
						return {typeId: this.getWindow().getMainFormId()};
					}
				}]
			}]
	}]
});
