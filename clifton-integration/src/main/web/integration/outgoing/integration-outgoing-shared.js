Ext.ns('Clifton.integration.outgoing', 'Clifton.integration.outgoing.destination');

Clifton.integration.outgoing.ExportHistoryGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationExportHistoryListFind',
	xtype: 'gridpanel',
	instructions: 'History of runs for selected Export.',
	rowSelectionModel: 'checkbox',
	additionalPropertiesToRequest: 'id|export.id',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Source Definition', dataIndex: 'export.sourceSystemDefinitionName', width: 100},
		{header: 'Export Source', width: 70, dataIndex: 'export.integrationSource.label', filter: {searchFieldName: 'integrationSourceId', type: 'combo', url: 'integrationSourceListFind.json'}},
		{header: 'Started', width: 80, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Ended', width: 80, dataIndex: 'endDate'},
		{header: 'Duration', width: 50, dataIndex: 'executionTimeFormatted'},
		{
			header: 'Status', width: 75, dataIndex: 'exportStatus.name', filter: {type: 'list', options: [['RECEIVED', 'RECEIVED'], ['PROCESSED', 'PROCESSED'], ['REPROCESSED', 'REPROCESSED'], ['FAILED', 'FAILED']]},
			renderer: function(v, p, r) {
				return (Clifton.export.renderStatus(v));
			}
		},
		{header: 'Attempt', width: 35, dataIndex: 'retryAttemptNumber', type: 'int', useNull: true},
		{header: 'Message', width: 150, dataIndex: 'description'}
	],
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('startDate', {'after': Clifton.calendar.getBusinessDayFrom(-2)});
		}
	},
	resendIntegrationExports: function(list, count, gridPanel) {
		const grid = this.grid;
		const item = list[count];
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Reprocessing...',
			params: {integrationExportId: item.id},
			conf: {rowId: item.id},
			onLoad: function(record, conf) {
				count++;
				if (count === list.length) { // refresh after all repos were transitioned
					gridPanel.reload();
				}
				else {
					gridPanel.resendIntegrationExports(list, count, gridPanel);
				}
			}
		});
		loader.load('integrationExportResend.json');
	},
	editor: {
		detailPageClass: 'Clifton.integration.outgoing.IntegrationExportHistoryWindow',
		drillDownOnly: true,
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Re-Send',
				tooltip: 'Re-run the export processing.',
				iconCls: 'run',
				handler: function() {
					const grid = gridPanel.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be re-sent.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Resend Selected Files', 'Would you like to resend the selected exports?', function(a) {
							if (a === 'yes') {
								const ut = sm.getSelections();
								// sort repos by tradeDate and then averageUnitPrice: proper booking order
								const integrationExports = [];
								let addedIntegrationExports = '';
								for (let i = 0; i < ut.length; i++) {
									const integrationExport = ut[i].json['export'];
									if (addedIntegrationExports.indexOf(':' + integrationExport.id)) {
										addedIntegrationExports += ':' + integrationExport.id;
										integrationExports.push(integrationExport);
									}
								}
								const count = 0;
								gridPanel.resendIntegrationExports(integrationExports, count, gridPanel);
							}
						});
					}
				}
			});
			t.add('-');
		}
	}
});
Ext.reg('integration-outgoing-exportHistoryGrid', Clifton.integration.outgoing.ExportHistoryGrid);


Clifton.integration.outgoing.ExportGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationExportListFind',
	xtype: 'gridpanel',
	rowSelectionModel: 'checkbox',
	instructions: 'Integration Exports define what was sent in the Exports and where.',
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('receivedDate', {'after': Clifton.calendar.getBusinessDayFrom(-5)});
		}
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Source Definition', dataIndex: 'sourceSystemDefinitionName', width: 60},
		{header: 'Export Source', width: 70, dataIndex: 'integrationSource.label', filter: {searchFieldName: 'integrationSourceId', type: 'combo', url: 'integrationSourceListFind.json'}},
		{header: 'Source Identifier', dataIndex: 'sourceSystemIdentifier', width: 40, hidden: true},
		{header: 'Destination Type', dataIndex: 'destinationType.name', width: 40, filter: {searchFieldName: 'destinationTypeName'}},
		{
			header: 'Status', width: 60, dataIndex: 'status.name', filter: {type: 'list', searchFieldName: 'statusNameList', options: [['RECEIVED', 'RECEIVED'], ['PROCESSED', 'PROCESSED'], ['REPROCESSED', 'REPROCESSED'], ['FAILED', 'FAILED']]},
			renderer: function(v, p, r) {
				return (Clifton.integration.outgoing.renderStatus(v));
			}
		},
		{header: 'Filename Prefix', dataIndex: 'filenamePrefix', width: 75, hidden: true},
		{header: 'Message Subject', dataIndex: 'messageSubject', width: 100},
		{header: 'Retry Count', dataIndex: 'retryCount', width: 60, type: 'int', title: 'Number of times to retry in case of failure', hidden: true, useNull: true},
		{header: 'Retry Delay In Seconds', dataIndex: 'retryDelayInSeconds', width: 60, type: 'int', title: 'Time in seconds before a retry is attempted again', hidden: true, useNull: true},
		{header: 'Received Date', width: 70, dataIndex: 'receivedDate', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Sent Date', width: 70, dataIndex: 'sentDate', type: 'date'},
		{
			width: 25, fixed: true, dataIndex: 'sourceSystemIdentifier', filter: false, sortable: false,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('export', '', 'Open IMS Export History', 'ShowIMSExportHistory');
			}
		}
	],
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					if (eventName === 'ShowIMSExportHistory') {
						const row = grid.store.data.items[rowIndex];
						const actId = row.json.sourceSystemIdentifier;
						const gridPanel = grid.ownerGridPanel;

						gridPanel.editor.openDetailPage('Clifton.export.definition.RunHistoryWindow', gridPanel, actId, row);
					}
				}
			}
		}
	},
	resendIntegrationExports: function(list, count, gridPanel) {
		const grid = this.grid;
		const item = list[count];
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Reprocessing...',
			params: {integrationExportId: item.id},
			conf: {rowId: item.id},
			onLoad: function(record, conf) {
				count++;
				if (count === list.length) { // refresh after all repos were transitioned
					gridPanel.reload();
				}
				else {
					gridPanel.resendIntegrationExports(list, count, gridPanel);
				}
			}
		});
		loader.load('integrationExportResend.json');
	},
	editor: {
		detailPageClass: 'Clifton.integration.outgoing.IntegrationExportWindow',
		drillDownOnly: true,
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Re-Send',
				tooltip: 'Re-run the export processing.',
				iconCls: 'run',
				handler: function() {
					const grid = gridPanel.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be re-sent.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Resend Selected Files', 'Would you like to resend the selected exports?', function(a) {
							if (a === 'yes') {
								const ut = sm.getSelections();
								// sort repos by tradeDate and then averageUnitPrice: proper booking order
								const integrationExports = [];
								let addedIntegrationExports = '';
								for (let i = 0; i < ut.length; i++) {
									const integrationExport = ut[i].json;
									if (addedIntegrationExports.indexOf(':' + integrationExport.id)) {
										addedIntegrationExports += ':' + integrationExport.id;
										integrationExports.push(integrationExport);
									}
								}
								const count = 0;
								gridPanel.resendIntegrationExports(integrationExports, count, gridPanel);
							}
						});
					}
				}
			});
			t.add('-');
		}
	}
});
Ext.reg('integration-outgoing-exportGrid', Clifton.integration.outgoing.ExportGrid);


Clifton.integration.outgoing.ExportDestinationGrid = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	name: 'integrationExportDestinationListFind',
	instructions: 'Destinations define/configure where Exports are to be sent: Email, FTP, FAX, etc.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Destination', dataIndex: 'destinationValue', width: 250},
		{header: 'Text', dataIndex: 'destinationText', width: 250},
		{header: 'Username', dataIndex: 'destinationUserName', width: 250, hidden: true},
		{header: 'Type', dataIndex: 'type.label', width: 50, filter: {type: 'combo', searchFieldName: 'typeId', url: 'integrationExportDestinationTypeListFind.json', loadAll: true}}
	],
	editor: {
		detailPageClass: 'Clifton.integration.outgoing.destination.ExportDestinationWindow',
		drillDownOnly: true
	}
});
Ext.reg('integration-outgoing-exportDestinationGrid', Clifton.integration.outgoing.ExportDestinationGrid);
