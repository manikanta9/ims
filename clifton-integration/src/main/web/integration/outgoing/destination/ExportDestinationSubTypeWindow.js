Clifton.integration.outgoing.destination.ExportDestinationSubTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'integrationExportDestinationSubWindow',
	title: 'Integration Destination Type',
	iconCls: 'export',
	width: 750,
	height: 500,
	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Destination Sub Type',
			items: [{
				xtype: 'formpanel',
				url: 'integrationExportDestinationSubType.json',
				instructions: '',
				items: [
					{fieldLabel: 'Name', name: 'name', readOnly: true},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{fieldLabel: 'Type', name: 'destinationType.name', xtype: 'linkfield', detailIdField: 'destinationType.id', detailPageClass: 'Clifton.integration.outgoing.destination.ExportDestinationTypeWindow', readOnly: true}
				]
			}]
		},

			{
				title: 'Destinations',
				items: [{
					xtype: 'integration-outgoing-exportDestinationGrid',
					getLoadParams: function(firstLoad) {
						return {destinationSubTypeId: this.getWindow().getMainFormId()};
					}
				}]
			}]
	}]
});
