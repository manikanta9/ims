Clifton.integration.outgoing.destination.ExportDestinationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'integrationExportDestinationWindow',
	title: 'Integration Destination Type',
	iconCls: 'export',
	width: 750,
	height: 500,
	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Destination Type',
			items: [{
				xtype: 'formpanel',
				url: 'integrationExportDestinationType.json',
				instructions: '',
				items: [
					{fieldLabel: 'Name', name: 'name', readOnly: true},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
				]
			}]
		}, {
			title: 'Destination Sub Types',
			items: [{
				name: 'integrationExportDestinationSubTypeListFind',
				xtype: 'gridpanel',
				instructions: 'Export Destination Sub Types determine the sub type of export like EMAIL_CC, EMAIL_FROM, FTP, FTPS.',
				getLoadParams: function(firstLoad) {
					return {destinationTypeId: this.getWindow().getMainFormId()};
				},
				columns: [
					{header: 'Destination Type', dataIndex: 'destinationType.name', filter: {type: 'combo', searchFieldName: 'destinationTypeId', url: 'integrationExportDestinationTypeListFind.json', loadAll: true}},
					{header: 'Name', dataIndex: 'name'},
					{header: 'Description', dataIndex: 'description', width: 200}
				],
				editor: {
					detailPageClass: 'Clifton.integration.outgoing.destination.ExportDestinationSubTypeWindow',
					drillDownOnly: true
				}
			}]
		}, {
			title: 'Destinations',
			items: [{
				xtype: 'integration-outgoing-exportDestinationGrid',
				getLoadParams: function(firstLoad) {
					return {destinationTypeId: this.getWindow().getMainFormId()};
				}
			}]
		}]
	}]
});
