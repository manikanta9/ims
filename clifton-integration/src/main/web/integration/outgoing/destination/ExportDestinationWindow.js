Clifton.integration.outgoing.destination.ExportDestinationWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'integrationExportDestinationWindow',
	title: 'Integration Export Destination',
	iconCls: 'export',
	width: 750,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Destination',
			items: [{
				xtype: 'formpanel',
				url: 'integrationExportDestination.json',
				instructions: '',
				listeners: {
					afterload: function(fp) {
						const f = this.getForm();
						const passswordSecret = TCG.getValue('destinationPasswordSecuritySecret', f.formValues);
						if (passswordSecret) {
							fp.setFormValue('destinationPasswordSecuritySecret.secretString', '[ENCRYPTED]', true);
						}
						const sshKeySecret = TCG.getValue('destinationSshPrivateKeySecuritySecret', f.formValues);
						if (sshKeySecret) {
							fp.setFormValue('destinationSshPrivateKeySecuritySecret.secretString', '[ENCRYPTED]', true);
						}
					}
				},
				items: [
					{fieldLabel: 'Type', name: 'type.name', xtype: 'linkfield', detailIdField: 'type.id', detailPageClass: 'Clifton.integration.outgoing.destination.ExportDestinationTypeWindow', readOnly: true},
					{fieldLabel: 'Value', name: 'destinationValue', readOnly: true},
					{fieldLabel: 'Text', name: 'destinationText', readOnly: true},
					{fieldLabel: 'Username', name: 'destinationUserName', readOnly: true},
					{fieldLabel: 'Password', xtype: 'secretfield', name: 'destinationPasswordSecuritySecret', tableName: 'IntegrationExportDestination', readOnly: true},
					{fieldLabel: 'PGP Public Key', name: 'pgpPublicKey', readOnly: true},
					{fieldLabel: 'SSH Private Key', xtype: 'secrettextarea', name: 'destinationSshPrivateKeySecuritySecret', tableName: 'IntegrationExportDestination', readOnly: true}
				]
			}]
		},
			{
				title: 'Exports',
				items: [{
					xtype: 'integration-outgoing-exportGrid',
					getLoadParams: function(firstLoad) {
						return {exportDestinationId: this.getWindow().getMainFormId()};
					}
				}]
			}]
	}]
});
