Clifton.integration.outgoing.IntegrationExportFileWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Integration Export File',
	height: 450,

	items: [{
		xtype: 'formpanel',
		url: 'integrationExportIntegrationFile.json',
		labelFieldName: 'referenceTwo.fileNameWithPath',
		readOnly: true,

		items: [
			{fieldLabel: 'Export', name: 'referenceOne.messageSubject', xtype: 'linkfield', detailPageClass: 'Clifton.integration.outgoing.IntegrationExportWindow', detailIdField: 'referenceOne.id'},
			{fieldLabel: 'File', name: 'referenceTwo.fileNameWithPath', xtype: 'linkfield', detailPageClass: 'Clifton.integration.file.FileWindow', detailIdField: 'referenceTwo.id'},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Status', name: 'status.exportFileStatusName'},
			{fieldLabel: 'Status Description', name: 'statusDescription', xtype: 'textarea'}
		]
	}]
});
