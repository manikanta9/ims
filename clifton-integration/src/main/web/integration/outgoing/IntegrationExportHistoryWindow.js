Clifton.integration.outgoing.IntegrationExportHistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Integration Export',
	iconCls: 'export',
	width: 750,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'General',
			items: {
				xtype: 'formpanel',
				readOnly: true,
				url: 'integrationExportHistory.json',
				labelWidth: 135,
				items: [
					{fieldLabel: 'Export', name: 'export.sourceSystemDefinitionName', detailIdField: 'export.id', hiddenName: 'export.id', xtype: 'linkfield', detailPageClass: 'Clifton.integration.outgoing.IntegrationExportWindow'},
					{fieldLabel: 'Status', name: 'exportStatus.name', hiddenName: 'exportStatus.id', xtype: 'displayfield'},
					{fieldLabel: 'Attempt', name: 'retryAttemptNumber'},
					{fieldLabel: 'Run Start', name: 'startDate', xtype: 'displayfield', type: 'date'},
					{fieldLabel: 'Run End', name: 'endDate', xtype: 'displayfield', type: 'date'},
					{fieldLabel: 'Duration', name: 'executionTimeFormatted', xtype: 'displayfield'},
					{fieldLabel: 'Message', name: 'description', xtype: 'textarea', anchor: '-35 -240'}
				]
			}
		}]
	}]
});
