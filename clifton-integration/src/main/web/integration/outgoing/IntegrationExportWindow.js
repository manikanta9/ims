Clifton.integration.outgoing.IntegrationExportWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Integration Export',
	iconCls: 'export',
	width: 800,
	height: 650,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'General',
				tbar: [{
					text: 'Resend File',
					tooltip: 'Resend the file to the destination.',
					iconCls: 'run',
					handler: function() {
						const f = this.ownerCt.ownerCt.items.get(0);
						const w = f.getWindow();
						Ext.Msg.confirm('Resend', 'Would you like to re-send the files?', function(a) {
							if (a === 'yes') {
								const params = {integrationExportId: w.getMainFormId()};
								const loader = new TCG.data.JsonLoader({
									waitTarget: f,
									waitMsg: 'Sending...',
									params: params,
									conf: params,
									onLoad: function(record, conf) {
										Ext.Msg.alert('Status', 'The process was started.');
									}
								});
								loader.load('integrationExportResend.json');
							}
						});
					}
				}],
				items: {
					xtype: 'formpanel',
					readOnly: true,
					url: 'integrationExport.json',
					labelWidth: 135,
					labelFieldName: 'messageSubject',
					items: [
						{fieldLabel: 'Source System', name: 'integrationSource.name', xtype: 'linkfield', detailIdField: 'integrationSource.id', detailPageClass: 'Clifton.integration.source.SourceWindow'},
						{fieldLabel: 'Source System Identifier', name: 'sourceSystemIdentifier'},
						{fieldLabel: 'Status', name: 'status.name', xtype: 'displayfield'},
						{fieldLabel: 'Received Date', name: 'receivedDate'},
						{fieldLabel: 'Sent Date', name: 'sentDate'},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Retry Count', name: 'retryCount'},
						{fieldLabel: 'Retry Delay in Seconds', name: 'retryDelayInSeconds'},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Filename Prefix', name: 'filenamePrefix'},
						{fieldLabel: 'Message Subject', name: 'messageSubject'},
						{
							fieldLabel: 'Text', name: 'textEditor', xtype: 'htmleditor',
							anchor: '0 -250',
							submitValue: false,
							readOnly: true,
							hiddenName: 'messageText',
							plugins: [new TCG.form.HtmlEditorFreemarker()]
						}
					]
				}
			},


			{
				title: 'Destinations',
				items: [{
					xtype: 'gridpanel',
					name: 'integrationExportIntegrationExportDestinationListFind',
					instructions: 'Destinations define/configure where Exports are to be sent: Email, FTP, FAX, etc.',
					additionalPropertiesToRequest: 'id|referenceTwo.id',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type', dataIndex: 'referenceTwo.type.label', width: 50, filter: {type: 'combo', searchFieldName: 'integrationExportDestinationTypeId', url: 'integrationExportDestinationTypeListFind.json', loadAll: true}},
						{header: 'Sub Type', dataIndex: 'destinationSubType.label', width: 50, filter: {type: 'combo', searchFieldName: 'integrationExportDestinationSubTypeId', url: 'integrationExportDestinationSubTypeListFind.json', loadAll: true}},
						{header: 'Destination', dataIndex: 'referenceTwo.destinationValue', width: 175, filter: {searchFieldName: 'integrationExportDestinationValue'}},
						{header: 'Text', dataIndex: 'referenceTwo.destinationText', width: 250, filter: {searchFieldName: 'integrationExportDestinationText'}},
						{header: 'Username', dataIndex: 'referenceTwo.destinationUserName', width: 250, hidden: true, filter: {searchFieldName: 'integrationExportDestinationUserName'}},
						{header: 'Password', dataIndex: 'referenceTwo.destinationPassword', width: 250, hidden: true, filter: {searchFieldName: 'integrationExportDestinationPassword'}}
					],
					getLoadParams: function(firstLoad) {
						return {exportId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.integration.outgoing.destination.ExportDestinationWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceTwo.id;
						}
					}
				}]
			},


			{
				title: 'Files',
				items: [{
					name: 'integrationExportIntegrationFileListFind.json',
					xtype: 'gridpanel',
					instructions: 'The list of files that have been sent for this export.',

					columns: [
						{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
						{header: 'File ID', dataIndex: 'referenceTwo.id', width: 30, hidden: true},
						{header: 'Path', dataIndex: 'referenceTwo.fileNameWithPath', width: 150},
						{header: 'Date', dataIndex: 'referenceTwo.receivedDate', width: 100},
						{
							header: 'Status', dataIndex: 'status', width: 50,
							renderer: function(v, p, r) {
								return (Clifton.integration.outgoing.renderStatus(v.name));
							}
						},
						{header: 'Status Description', dataIndex: 'statusDescription', width: 150}
					],
					getLoadParams: function() {
						return {'exportId': this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.integration.outgoing.IntegrationExportFileWindow',
						getDefaultData: function(gridPanel) {
							return {
								referenceOne: gridPanel.getWindow().getMainForm().formValues
							};
						},
						addEditButtons: function(t, gp) {
							const sm = gp.grid.getSelectionModel();

							t.add({
								text: 'Download File',
								tooltip: 'Download the selected file.',
								iconCls: 'view',
								scope: gp,
								handler: function() {
									const selection = sm.getSelections()[0];
									if (selection) {
										TCG.downloadFile('integrationFileDownload.json?id=' + selection.data['referenceTwo.id'], null, gp);
									}
									else {
										TCG.showError('Please select a file to download.', 'Incorrect Selection');
									}

								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Run History',
				items: [{
					name: 'integrationExportHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'History of runs for selected Export.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Started', width: 80, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Ended', width: 80, dataIndex: 'endDate'},
						{header: 'Duration', width: 50, dataIndex: 'executionTimeFormatted'},
						{
							header: 'Status', width: 75, dataIndex: 'exportStatus.name', filter: {type: 'list', options: [['RECEIVED', 'RECEIVED'], ['PROCESSED', 'PROCESSED'], ['REPROCESSED', 'REPROCESSED'], ['FAILED', 'FAILED']]},
							renderer: function(v, p, r) {
								return (Clifton.export.renderStatus(v));
							}
						},
						{header: 'Attempt', width: 35, dataIndex: 'retryAttemptNumber', type: 'int', useNull: true},
						{header: 'Message', width: 150, dataIndex: 'description'}
					],
					getLoadParams: function(firstLoad) {
						return {exportId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.integration.outgoing.IntegrationExportHistoryWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
