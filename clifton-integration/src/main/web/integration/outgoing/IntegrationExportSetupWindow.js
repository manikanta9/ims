Clifton.integration.outgoing.IntegrationExportSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'integrationExportSetupWindow',
	title: 'Integration Exports',
	iconCls: 'export',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [

			{
				title: 'Exports',
				items: [{
					xtype: 'integration-outgoing-exportGrid'
				}]
			},

			{
				title: 'Run History',
				items: [{
					xtype: 'integration-outgoing-exportHistoryGrid'
				}]
			},

			{
				title: 'Files',
				items: [{
					xtype: 'integration-fileGrid',
					columnOverrides: [
						{dataIndex: 'fileDefinition.type.name', hidden: true},
						{dataIndex: 'fileDefinition.fileName', hidden: true},
						{dataIndex: 'fileNameWithPath', hidden: false},
						{dataIndex: 'sourceDataLoaded', hidden: true},
						{dataIndex: 'deleted', hidden: true},
						{dataIndex: 'errorOnLoad', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('receivedDate', {'after': Clifton.calendar.getBusinessDayFrom(-2)});
						}
						return {fileSourceType: 'EXPORT'};
					},
					editor: {
						detailPageClass: 'Clifton.integration.file.FileWindow',
						drillDownOnly: true
					}
				}]
			}, {
				title: 'Destinations',
				items: [{
					xtype: 'integration-outgoing-exportDestinationGrid'
				}]
			}, {
				title: 'Destination Type',
				items: [{
					name: 'integrationExportDestinationTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Export Destination Types determine the type of export destination such as EMAIL, FTP and FAX.',
					columns: [
						{header: 'Name', dataIndex: 'name'},
						{header: 'Description', dataIndex: 'description', width: 200}
					],
					editor: {
						detailPageClass: 'Clifton.integration.outgoing.destination.ExportDestinationTypeWindow',
						drillDownOnly: true
					}
				}]
			}, {
				title: 'Destination Sub Type',
				items: [{
					name: 'integrationExportDestinationSubTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Export Destination Sub Types determine the sub type of export like EMAIL_CC, EMAIL_FROM, FTP, FTPS.',
					columns: [
						{header: 'Destination Type', dataIndex: 'destinationType.name', filter: {type: 'combo', searchFieldName: 'destinationTypeId', url: 'integrationExportDestinationTypeListFind.json', loadAll: true}},
						{header: 'Name', dataIndex: 'name'},
						{header: 'Description', dataIndex: 'description', width: 200}
					],
					editor: {
						detailPageClass: 'Clifton.integration.outgoing.destination.ExportDestinationSubTypeWindow',
						drillDownOnly: true
					}
				}]
			},

			{
				title: 'Sources',
				items: [{
					xtype: 'integration-sourceGrid',
					getLoadParams: function(firstLoad) {
						return {typeName: 'Export'};
					}
				}]
			}]
	}]
});
