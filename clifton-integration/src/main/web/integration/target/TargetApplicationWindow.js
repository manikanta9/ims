Clifton.integration.target.TargetApplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Target Application',
	iconCls: 'application',
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'integrationTargetApplication.json?requestedMaxDepth=4',
					labelWidth: 140,
					instructions: 'A Target Application describe what applications receive events raised after integration processes files',
					items: [
						{fieldLabel: 'Application Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Request Timeout', name: 'requestTimeout', xtype: 'integerfield'}
					]
				}]
			},


			{
				title: 'Definition Types',
				items: [{
					xtype: 'gridpanel',
					name: 'integrationImportDefinitionTypeListFind',
					instructions: 'Import type defines the type of data received from external sources. Each type usually maps to a destination table in the integration system that stores normalized source data for that type. Corresponding Event can be raised to notify the Main System that new data is available for consumption.',
					columns: [
						{header: 'Type Name', dataIndex: 'name'},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
						{header: 'Destination Table', dataIndex: 'destinationTableName', tooltip: 'The final table in Integration that this import will eventually populate'},
						{header: 'Event Name', dataIndex: 'eventName', width: 50}
					],
					editor: {
						detailPageClass: 'Clifton.integration.definition.ImportTypeWindow',
						getDeleteURL: function() {
							return 'integrationTargetApplicationDefinitionTypeDelete.json';
						},
						getDeleteParams: function(selectionModel) {
							const definitionTypeId = selectionModel.getSelected().id;
							const targetApplicationId = this.getWindow().getMainFormId();
							return {
								definitionTypeId: definitionTypeId,
								targetApplicationId: targetApplicationId
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'definitionType', url: 'integrationImportDefinitionTypeListFind.json', emptyText: '< Select Definition Type >', width: 175, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a definition type to this target application.',
								iconCls: 'add',
								scope: gridPanel,
								handler: function() {
									const definitionType = TCG.getChildByName(toolBar, 'definitionType');
									const definitionTypeId = definitionType.getValue();
									const targetApplicationId = this.getWindow().getMainFormId();
									definitionType.clearValue();

									const params = {};
									params['definitionTypeId'] = definitionTypeId;
									params['targetApplicationId'] = targetApplicationId;

									if (definitionTypeId === '') {
										TCG.showError('You must first select desired definition type from the list.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											timeout: 120000,
											waitMsg: 'Saving: Please Wait.',
											params: params,
											scope: gridPanel,
											success: function(form, action) {
												const result = Ext.decode(form.responseText);
												if (result.success) {
													const prompt = this;
													if (prompt.isUseWaitMsg()) {
														prompt.getMsgTarget().mask('Saving Definition Type for Target Application... Success');
														const unmaskFunction = function() {
															prompt.getMsgTarget().unmask();
														};
														unmaskFunction.defer(600, this);
														this.scope.reload();
													}
												}
												else {
													this.onFailure();
													this.getMsgTarget().unmask();
													TCG.data.ErrorHandler.handleFailure(this, result);
												}
											},
											error: function(form, action) {
												Ext.Msg.alert('Failed to save Definition Type for Target Application', action.result.data);
											}
										});
										loader.load('integrationTargetApplicationDefinitionTypeLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function() {
						return {targetApplicationId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
