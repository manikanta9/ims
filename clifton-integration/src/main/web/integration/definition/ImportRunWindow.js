Clifton.integration.definition.ImportRunWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'integrationImportRun.json',

	getClassName: function(config, entity) {
		let destinationTableName;
		if (entity && entity.integrationImportDefinition && entity.integrationImportDefinition.type && entity.integrationImportDefinition.type.destinationTableName) {
			destinationTableName = entity.integrationImportDefinition.type.destinationTableName;
		}
		// TODO: Make separate classes with an xtype for the form tab instead of changing default data
		if (config.defaultData) {
			config.defaultData.integrationImportDefinition = {
				type: {
					destinationTableName: destinationTableName
				}
			};
		}
		return 'Clifton.integration.definition.ImportRunWindowImpl';
	}
});

Clifton.integration.definition.ImportRunWindowImpl = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Import Run',
	iconCls: 'run',
	width: 1200,
	height: 700,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const typeName = (w.defaultData) ? w.defaultData.integrationImportDefinition.type.destinationTableName : undefined;

		switch (typeName) {

			case 'IntegrationReconcileDailyPNLHistory':
				tabs.insert(1, this.dailyPNLTab);
				break;

			case 'IntegrationReconcileAccountBalancesByAccountType':
				tabs.insert(1, this.accountBalancesTab);
				break;

			case 'IntegrationReconcileAccountM2MDailyExpense':
				tabs.insert(1, this.m2mExpensesTab);
				break;

			case 'IntegrationMarketDataExchangeRate':
				tabs.insert(1, this.exchangeRatesTab);
				break;

			case 'IntegrationMarketDataFieldValue':
				tabs.insert(1, this.marketDataFieldValuesTab);
				break;

			case 'IntegrationTradeIntraday':
				tabs.insert(1, this.intradayTab);
				break;

			case 'IntegrationReconcilePositionHistory':
				tabs.insert(1, this.positionsTab);
				break;

			case 'IntegrationManagerPositionHistory':
				tabs.get(0).getTopToolbar().insertButton(0, '-');
				tabs.insert(1, this.managerPositionsTab);
				tabs.insert(2, this.unmappedManagerPositionsTab);
				break;
			case 'IntegrationManagerTransaction':
				tabs.insert(1, this.managerTransactionsTab);
				break;
			case 'IntegrationBusinessCompany':
				tabs.insert(1, this.companiesTab);
				break;
			case 'IntegrationTrade':
				tabs.insert(1, this.integrationTradeTab);
				break;
			case 'IntegrationCorporateAction':
				tabs.insert(1, this.corporateActionTab);
				break;
			case 'IntegrationCorporateActionPositionRequest':
				tabs.insert(1, this.corporateActionPositionRequestTab);
				break;
			default:
				break;
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Import Run',
			tbar: [{
				text: 'Reprocess Everything',
				tooltip: 'Re-run the entire import processing with the file from this run.',
				iconCls: 'run',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();
					Ext.Msg.confirm('Reprocess Everything', 'Would you like to re-run the entire processing for this file?', function(a) {
						if (a === 'yes') {
							const params = {runId: w.getMainFormId(), limitToImportRun: false};
							const loader = new TCG.data.JsonLoader({
								waitTarget: f,
								waitMsg: 'Starting...',
								params: params,
								conf: params,
								onLoad: function(record, conf) {
									Ext.Msg.alert('Status', 'The process was started.');
								}
							});
							loader.load('integrationImportRunRerun.json');
						}
					});
				}
			}, {
				text: 'Reprocess Import',
				tooltip: 'Re-run the import processing with just the import definition from this run.',
				iconCls: 'run',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();
					Ext.Msg.confirm('Reprocess Everything', 'Would you like to re-run the processing for this import definition?', function(a) {
						if (a === 'yes') {
							const params = {runId: w.getMainFormId(), limitToImportRun: true};
							const loader = new TCG.data.JsonLoader({
								waitTarget: f,
								waitMsg: 'Starting...',
								params: params,
								conf: params,
								onLoad: function(record, conf) {
									Ext.Msg.alert('Status', 'The process was started.');
								}
							});
							loader.load('integrationImportRunRerun.json');
						}
					});
				}
			}, '-', {
				text: 'Download File',
				tooltip: 'View the file that was processed.',
				iconCls: 'view',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();
					TCG.downloadFile('integrationFileDownload.json?id=' + w.defaultData.file.id, null, this);
				}
			}, '-', {
				text: 'Help',
				iconCls: 'help',
				handler: function() {
					TCG.openWIKI('IT/Editing+CSV+Files', 'Import Run Help');
				}
			}],

			items: [{
				xtype: 'formpanel',
				url: 'integrationImportRun.json',
				labelFieldName: 'id',
				readOnly: true,
				instructions: 'A run is created each time an external file with active import definition(s) is uploaded to the FTP, SFTP or Input folder.',
				items: [
					{fieldLabel: 'Import Definition', name: 'integrationImportDefinition.name', xtype: 'linkfield', detailIdField: 'integrationImportDefinition.id', detailPageClass: 'Clifton.integration.definition.ImportDefinitionWindow'},
					{fieldLabel: 'File Definition', name: 'integrationImportDefinition.fileDefinition.fileName', xtype: 'linkfield', detailIdField: 'integrationImportDefinition.fileDefinition.id', detailPageClass: 'Clifton.integration.file.FileDefinitionWindow'},
					{
						fieldLabel: 'Integration File', name: 'file.fileNameWithPath', xtype: 'linkfield', detailIdField: 'file.id', detailPageClass: 'Clifton.integration.file.FileWindow',
						qtip: 'The path to the file that started this run'
					},
					{
						xtype: 'columnpanel',
						defaults: {xtype: 'displayfield', type: 'date'},
						columns: [
							{rows: [{fieldLabel: 'Effective Date', name: 'effectiveDate'}], config: {columnWidth: 0.34}},
							{rows: [{fieldLabel: 'Run Start', name: 'startProcessDate'}], config: {columnWidth: 0.33}},
							{rows: [{fieldLabel: 'Run End', name: 'endProcessDate'}], config: {columnWidth: 0.33}}
						]
					},
					{fieldLabel: 'Status', name: 'status.name'},
					{fieldLabel: 'Error Message', name: 'error', xtype: 'textarea', anchor: '-35 -320'},
					{
						xtype: 'gridpanel',
						height: 150,
						name: 'integrationImportRunEventListFind',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Run ID', width: 85, dataIndex: 'importRun.id', hidden: true},
							{header: 'Application', width: 40, dataIndex: 'targetApplication.name', filter: {searchFieldName: 'targetApplicationName'}},
							{header: 'Import Definition', width: 95, dataIndex: 'importRun.integrationImportDefinition.name', filter: {searchFieldName: 'importDefinitionName'}},
							{header: 'Import Definition Type', width: 60, dataIndex: 'importRun.integrationImportDefinition.type.name', filter: {searchFieldName: 'typeName'}},
							{
								header: 'Status',
								width: 40,
								dataIndex: 'status.name',
								filter: {type: 'combo', searchFieldName: 'statusId', url: 'integrationImportStatusListFind.json', displayField: 'name'}
							},
							{header: 'Error Message', dataIndex: 'error', sortable: false},
							{header: 'Processed File', dataIndex: 'importRun.file.fileDefinition.fileName', filter: {searchFieldName: 'processedFileName'}, hidden: true},
							{header: 'Effective Date', dataIndex: 'importRun.effectiveDate', width: 40, filter: {searchFieldName: 'effectiveDate'}},
							{header: 'Start Date', dataIndex: 'importRun.startProcessDate', width: 40, filter: {searchFieldName: 'startProcessDate'}, defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Duration', width: 30, dataIndex: 'importRun.executionTimeFormatted', filter: false, sortable: false},
							{header: 'End Date', dataIndex: 'importRun.endProcessDate', width: 40, filter: {searchFieldName: 'endProcessDate'}, hidden: true},
							{header: 'Test Definition', dataIndex: 'importRun.integrationImportDefinition.testDefinition', width: 30, type: 'boolean', sortable: false, filter: {searchFieldName: 'testDefinition'}, hidden: true}
						],
						getLoadParams: function(firstLoad) {
							return {
								readUncommittedRequested: true,
								importRunId: this.getWindow().getMainFormId()
							};
						},
						editor: {
							detailPageClass: 'Clifton.integration.definition.ImportRunEventWindow',
							drillDownOnly: true,
							getDefaultData: function() {
								if (this.getGridPanel().importEventName) {
									return {importEventName: this.getGridPanel().importEventName};
								}
							}
						}
					}
				]
			}]
		}]
	}],


	managerPositionsTab: {
		title: 'Manager Positions',
		items: [{
			name: 'integrationManagerPositionHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following manager positions were imported and processed for selected run.',
			columns: [
				{header: 'Bank Code', width: 40, dataIndex: 'managerBank.bankCode'},
				{header: 'Original Bank Code', width: 60, dataIndex: 'managerBank.originalBankCode'},
				{header: 'Asset Class', width: 80, dataIndex: 'managerSecurity.assetClass'},
				{header: 'Source Asset Class', width: 80, dataIndex: 'managerSecurity.sourceAssetClass', hidden: true},
				{header: 'Security', width: 75, dataIndex: 'managerSecurity.securityIdentifier', hidden: true},
				{header: 'Security Type', width: 75, dataIndex: 'managerSecurity.securityIdentifierType', hidden: true},
				{header: 'Security Description', width: 110, dataIndex: 'managerSecurity.securityDescription'},
				{header: 'Type', width: 75, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, url: 'integrationManagerPositionTypeList.json'}},
				{header: 'Cash', width: 60, dataIndex: 'cashValue', type: 'currency'},
				{header: 'Securities', width: 60, dataIndex: 'securityValue', type: 'currency'},
				{header: 'Value', width: 60, dataIndex: 'value', type: 'currency'}
			],
			getLoadParams: function() {
				return {
					readUncommittedRequested: true,
					runId: this.getWindow().getMainFormId()
				};
			}
		}]
	},

	unmappedManagerPositionsTab: {
		title: 'Unmapped Manager Positions',
		items: [{
			name: 'integrationManagerPositionHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following manager positions were imported but could not be mapped to a corresponding position type because of a missing mapping.',
			columns: [
				{header: 'Bank Code', width: 60, dataIndex: 'managerBank.bankCode'},
				{header: 'Asset Class', width: 130, dataIndex: 'managerSecurity.assetClass'},
				{header: 'Source Asset Class', width: 80, dataIndex: 'managerSecurity.sourceAssetClass', hidden: true},
				{header: 'Security', width: 75, dataIndex: 'managerSecurity.securityIdentifier', hidden: true},
				{header: 'Security Type', width: 75, dataIndex: 'managerSecurity.securityIdentifierType', hidden: true},
				{header: 'Security Description', width: 110, dataIndex: 'managerSecurity.securityDescription'},
				{header: 'Type', width: 75, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, url: 'integrationManagerPositionTypeList.json'}},
				{header: 'Cash', width: 80, dataIndex: 'cashValue', type: 'currency'},
				{header: 'Securities', width: 80, dataIndex: 'securityValue', type: 'currency'},
				{header: 'Value', width: 80, dataIndex: 'value', type: 'currency'}
			],
			getLoadParams: function() {
				return {
					readUncommittedRequested: true,
					unmappedOnly: true,
					runId: this.getWindow().getMainFormId()
				};
			}
		}]
	},

	accountBalancesTab: {
		title: 'Account Balances',
		items: [{
			name: 'integrationReconcileAccountBalancesByAccountTypeListFind',
			xtype: 'gridpanel',
			instructions: 'The following account balances were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Position Date', width: 80, dataIndex: 'positionDate'},

				{header: 'Base Currency', width: 80, dataIndex: 'currencySymbolBase'},
				{header: 'Local Currency', width: 80, dataIndex: 'currencySymbolLocal'},


				{header: 'Expected Transfer Local', width: 80, dataIndex: 'expectedTransferAmountLocal', type: 'currency'},
				{header: 'Expected Transfer Base', width: 80, dataIndex: 'expectedTransferAmountBase', type: 'currency'},
				{header: 'Collateral Requirement Local', width: 80, dataIndex: 'collateralRequirementLocal', type: 'currency'},
				{header: 'Collateral Requirement Base', width: 80, dataIndex: 'collateralRequirementBase', type: 'currency'},
				{header: 'Collateral Amount Local', width: 80, dataIndex: 'collateralAmountLocal', type: 'currency'},
				{header: 'Collateral Amount Base', width: 80, dataIndex: 'collateralAmountBase', type: 'currency'},
				{header: 'Is Group', width: 40, dataIndex: 'groupAccount', type: 'boolean'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},
	m2mExpensesTab: {
		title: 'M2M Expenses',
		items: [{
			name: 'integrationReconcileAccountM2MDailyExpenseListFind',
			xtype: 'gridpanel',
			instructions: 'The following account balances were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Expense Type', width: 80, dataIndex: 'expenseType'},
				{header: 'Currency', width: 80, dataIndex: 'currencySymbol'},
				{header: 'Amount', width: 80, dataIndex: 'expenseAmount', type: 'currency'},
				{header: 'Position Date', width: 80, dataIndex: 'positionDate'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},
	dailyPNLTab: {
		title: 'Daily PNL',
		items: [{
			name: 'integrationReconcileDailyPNLHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following daily pnl values are imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Account Name', width: 60, dataIndex: 'accountName'},
				{header: 'Client Account Number', width: 60, dataIndex: 'clientAccountNumber'},

				{header: 'Position Date', width: 60, dataIndex: 'positionDate'},

				{header: 'Security', width: 80, dataIndex: 'securitySymbol'},
				{header: 'Security Type', width: 80, dataIndex: 'securitySymbolType'},

				{header: 'Position Currency', width: 80, dataIndex: 'positionCurrencySymbol'},
				{header: 'Account Currency', width: 80, dataIndex: 'accountCurrencySymbol'},

				{header: 'Fx Rate', width: 80, dataIndex: 'fxRate', type: 'float'},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Prior Quantity', width: 60, dataIndex: 'priorQuantity', type: 'float'},
				{header: 'Today Closed Quantity', width: 60, dataIndex: 'todayClosedQuantity', type: 'float'},
				{header: 'Is Short', width: 80, dataIndex: 'shortPosition', type: 'boolean'},

				{header: 'Today Commission Local', width: 60, dataIndex: 'todayCommissionLocal', type: 'currency'},
				{header: 'Today Commission Base', width: 60, dataIndex: 'todayCommissionBase', type: 'currency'},
				{header: 'Today Realized Gain Loss Local', width: 60, dataIndex: 'todayRealizedGainLossLocal', type: 'currency'},
				{header: 'Today Realized Gain Loss Base', width: 60, dataIndex: 'todayRealizedGainLossBase', type: 'currency'},
				{header: 'Open Trade Equity Local', width: 60, dataIndex: 'openTradeEquityLocal', type: 'currency'},
				{header: 'Open Trade Equity Base', width: 60, dataIndex: 'openTradeEquityBase', type: 'currency'},
				{header: 'Prior Open Trade Equity Local', width: 60, dataIndex: 'priorOpenTradeEquityLocal', type: 'currency'},
				{header: 'Prior Open Trade Equity Base', width: 60, dataIndex: 'priorOpenTradeEquityBase', type: 'currency'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},
	positionsTab: {
		title: 'Positions',
		items: [{
			name: 'integrationReconcilePositionHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following positions were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Account Name', width: 130, dataIndex: 'accountName'},
				{header: 'Client Account Number', width: 130, dataIndex: 'clientAccountNumber'},
				{header: 'Unique Position ID', width: 130, dataIndex: 'uniquePositionID', hidden: true},

				{header: 'Position Date', width: 80, dataIndex: 'positionDate'},
				{header: 'Trade Date', width: 80, dataIndex: 'tradeDate'},
				{header: 'Settle Date', width: 80, dataIndex: 'settleDate'},

				{header: 'Security', width: 80, dataIndex: 'securitySymbol'},
				{header: 'Security Type', width: 80, dataIndex: 'securitySymbolType'},

				{header: 'Position Currency', width: 80, dataIndex: 'positionCurrencySymbol'},
				{header: 'Account Currency', width: 80, dataIndex: 'accountCurrencySymbol'},

				{header: 'Fx Rate', width: 80, dataIndex: 'fxRate', type: 'float'},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Is Short', width: 80, dataIndex: 'shortPosition', type: 'boolean'},

				{header: 'Trade Price', width: 80, dataIndex: 'tradePrice', type: 'currency'},
				{header: 'MarketPrice', width: 80, dataIndex: 'marketPrice', type: 'currency'},

				{header: 'Cost Basis Local', width: 80, dataIndex: 'costBasisLocal', type: 'currency'},
				{header: 'Cost Basis Base', width: 80, dataIndex: 'costBasisBase', type: 'currency'},
				{header: 'Market Value Local', width: 80, dataIndex: 'marketValueLocal', type: 'currency'},
				{header: 'Market Value Base', width: 80, dataIndex: 'marketValueBase', type: 'currency'},
				{header: 'Option Value Local', width: 80, dataIndex: 'optionMarketValueLocal', type: 'currency', hidden: true},
				{header: 'Option Value Base', width: 80, dataIndex: 'optionMarketValueBase', type: 'currency', hidden: true},
				{header: 'OTE Local', width: 80, dataIndex: 'openTradeEquityLocal', type: 'currency'},
				{header: 'OTE Base', width: 80, dataIndex: 'openTradeEquityBase', type: 'currency'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},
	intradayTab: {
		title: 'External Trades',
		items: [{
			name: 'integrationTradeIntradayListFind',
			xtype: 'gridpanel',
			instructions: 'The following trades were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Executing Broker', width: 130, dataIndex: 'executingBrokerCompanyName'},
				{header: 'Clearing Broker', width: 130, dataIndex: 'clearingBrokerCompanyName'},

				{header: 'Investment Security', width: 80, dataIndex: 'investmentSecuritySymbol'},
				{header: 'Paying Security', width: 80, dataIndex: 'payingSecuritySymbol'},
				{header: 'Symbol Type', width: 80, dataIndex: 'securitySymbolType'},
				{
					header: 'B/S', width: 30, dataIndex: 'buy', type: 'boolean',
					renderer: function(v, metaData) {
						metaData.css = v ? 'buy-light' : 'sell-light';
						return v ? 'BUY' : 'SELL';
					}
				},
				{header: 'Trade Date', width: 12, dataIndex: 'tradeDate', hidden: true},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Trade Price', width: 80, dataIndex: 'price', type: 'float'},
				{
					header: 'Status',
					width: 80,
					dataIndex: 'status',
					filter: {
						type: 'list', options: [
							['CONFIRMED', 'CONFIRMED'],
							['PENDING_BROKER_ACCEPTANCE', 'PENDING_BROKER_ACCEPTANCE'],
							['UNCONFIRMED', 'UNCONFIRMED']]
					}
				},
				{
					header: 'Type',
					width: 80,
					dataIndex: 'type',
					filter: {
						type: 'list', options: [
							['ADDED', 'ADDED'],
							['CHANGED', 'CHANGED'],
							['DELETED', 'DELETED']]
					}
				},
				{header: 'Unique Trade Id', width: 80, dataIndex: 'uniqueTradeId'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},
	integrationTradeTab: {
		title: 'External Trades',
		items: [{
			name: 'integrationTradeListFind',
			xtype: 'gridpanel',
			instructions: 'The following trades were imported and processed for as trade imports.',
			columns: [
				{header: 'Unique Identifier', width: 60, dataIndex: 'uniqueIdentifier'},

				{header: 'Holding Account', width: 60, dataIndex: 'holdingAccountNumber'},
				{header: 'Client Account', width: 60, dataIndex: 'clientAccountNumber', hidden: true},
				{header: 'Broker Code', width: 60, dataIndex: 'executingBrokerCode'},

				{header: 'Security Symbol', width: 80, dataIndex: 'securitySymbol'},
				{header: 'Security Description', width: 130, dataIndex: 'securityDescription'},
				{header: 'CUSIP', width: 80, dataIndex: 'cusip'},
				{header: 'SEDOL', width: 80, dataIndex: 'sedol', hidden: true},
				{header: 'ISIN', width: 80, dataIndex: 'isin', hidden: true},

				{header: 'Paying Security', width: 50, dataIndex: 'payingSecuritySymbol'},
				{
					header: 'B/S', width: 40, dataIndex: 'buy', type: 'boolean',
					renderer: function(v, metaData) {
						metaData.css = v ? 'buy-light' : 'sell-light';
						return v ? 'BUY' : 'SELL';
					}
				},
				{header: 'Trade Date', width: 80, dataIndex: 'tradeDate', type: 'date'},
				{header: 'Settlement Date', width: 80, dataIndex: 'settlementDate', type: 'date'},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Trade Price', width: 80, dataIndex: 'price', type: 'float'},
				{header: 'Trade Price Base', width: 80, dataIndex: 'priceBase', type: 'float', hidden: true},
				{header: 'Trade Amount', width: 80, dataIndex: 'tradeAmount', type: 'float'},
				{header: 'Trade Amount Base', width: 80, dataIndex: 'tradeAmountBase', type: 'float', hidden: true},

				{header: 'Commission Per Unit', width: 80, dataIndex: 'commissionPerUnit', type: 'float'},
				{header: 'Commission', width: 80, dataIndex: 'commissionAmount', type: 'float'},
				{header: 'Fee Amount', width: 80, dataIndex: 'feeAmount', type: 'float'},
				{header: 'Opening Price', width: 80, dataIndex: 'openingPrice', type: 'float'},
				{header: 'Opening Date', width: 80, dataIndex: 'openingDate', type: 'date'},
				{header: 'FX Rate', width: 80, dataIndex: 'exchangeRateToBase', type: 'float', hidden: true}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},
	exchangeRatesTab: {
		title: 'Exchange Rates',
		items: [{
			name: 'integrationMarketDataExchangeRateListFind',
			xtype: 'gridpanel',
			instructions: 'The following exchange rates were imported and processed for selected run.',
			columns: [
				{header: 'From Currency', dataIndex: 'fromCurrency'},
				{header: 'To Currency', dataIndex: 'toCurrency'},
				{header: 'Exchange Rate', dataIndex: 'rate', type: 'float'},
				{header: 'Inverse Rate', dataIndex: 'inverseRate', type: 'float'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},
	marketDataFieldValuesTab: {
		title: 'Market Data Field Values',
		items: [{
			name: 'integrationMarketDataFieldValueListFind',
			xtype: 'gridpanel',
			instructions: 'The following exchange rates were imported and processed for selected run.',
			columns: [
				{header: 'Field Name', dataIndex: 'marketDataFieldName'},
				{header: 'Security Symbol', dataIndex: 'investmentSecuritySymbol'},
				{header: 'CUSIP', dataIndex: 'investmentSecurityCusip'},
				{header: 'Measure Date', dataIndex: 'measureDate'},
				{header: 'Measure Time', dataIndex: 'measureTime', type: 'int', hidden: true},
				{header: 'Measure Value', dataIndex: 'measureValue'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},

	managerTransactionsTab: {
		title: 'Manager Transactions',
		items: [{
			name: 'investmentManagerCustodianTransactionListFind',
			xtype: 'gridpanel',
			topToolbarSearchParameter: 'searchPattern',
			instructions: 'The following manager transactions were imported and processed for selected run.',
			pageSize: 100,
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Transaction Date', width: 25, dataIndex: 'transactionDate', defaultSortColumn: true},
				{header: 'Transaction Amount', width: 25, dataIndex: 'transactionAmount', type: 'currency', summaryType: 'sum', negativeInRed: true},
				{header: 'Description', width: 100, dataIndex: 'description'},
				{header: 'Custodian Account', width: 50, dataIndex: 'custodianAccount.label', filter: {searchFieldName: 'custodianAccountNumber'}},
				{header: 'Custodian Holding Account', width: 50, dataIndex: 'custodianAccount.holdingAccount.label', filter: false},
				{header: 'Source Data Identifier', width: 25, dataIndex: 'sourceDataIdentifier', hidden: true}
			],
			isPagingEnabled: function() {
				return true;
			},
			getTopToolbarInitialLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to last 7 days of balances
					this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -7)});
				}
				return {sourceDataIdentifier: this.getWindow().getMainFormId()};
			}
		}]
	},

	companiesTab: {
		title: 'Companies',
		items: [{
			name: 'integrationBusinessCompanyListFind',
			xtype: 'gridpanel',
			pageSize: 100,
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Bloomberg Company Identifier', width: 25, dataIndex: 'bloombergCompanyIdentifier', defaultSortColumn: true, type: 'int'},
				{header: 'Parent Bloomberg Company Identifier', width: 25, dataIndex: 'parentBloombergCompanyIdentifier', type: 'int'},
				{header: 'Ultimate Parent Bloomberg Company Identifier', width: 25, dataIndex: 'ultimateParentBloombergCompanyIdentifier', type: 'int'},
				{header: 'Obligor Bloomberg Company Identifier', width: 25, dataIndex: 'obligorBloombergCompanyIdentifier', type: 'int'},

				{header: 'Long Company Name', width: 25, dataIndex: 'longCompanyName'},
				{header: 'Legal Company Name', width: 25, dataIndex: 'companyLegalName'},
				{header: 'Alternate Company Name', width: 25, dataIndex: 'alternateCompanyName', hidden: true},

				{header: 'Company Corporate Ticker', width: 25, dataIndex: 'companyCorporateTicker', hidden: true},
				{header: 'Ultimate Parent Exchange', width: 25, dataIndex: 'ultimateParentTickerExchange', hidden: true},
				{header: 'Ultimate Parent Ticker', width: 25, dataIndex: 'ultimateParentTickerExchangeCorporateTicker', hidden: true},

				{header: 'Sector Name', width: 25, dataIndex: 'industrySectorName', hidden: true},
				{header: 'Sector Group Name', width: 25, dataIndex: 'industrySectorGroupName', hidden: true},
				{header: 'Sector Sub Group Name', width: 25, dataIndex: 'industrySectorSubGroupName', hidden: true},

				{header: 'Sector Number', width: 25, dataIndex: 'industrySectorName', type: 'int', hidden: true},
				{header: 'Sector Group Number', width: 25, dataIndex: 'industrySectorGroupName', type: 'int', hidden: true},
				{header: 'Sector Sub Group Number', width: 25, dataIndex: 'industrySectorSubGroupName', type: 'int', hidden: true},

				{header: 'Country Of Domicile', width: 25, dataIndex: 'countryOfDomicile'},
				{header: 'Country Of Incorporation', width: 25, dataIndex: 'countryOfIncorporation'},
				{header: 'Country Of Risk', width: 25, dataIndex: 'countryOfRisk'},
				{header: 'State Of Domicile', width: 25, dataIndex: 'stateOfDomicile', hidden: true},
				{header: 'State Of Incorporation', width: 25, dataIndex: 'stateOfIncorporation', hidden: true},

				{header: 'Is Ultimate Parent', width: 25, dataIndex: 'ultimateParent', type: 'boolean', hidden: true},
				{header: 'Is Acquired By Parent', width: 25, dataIndex: 'acquiredByParent', type: 'boolean', hidden: true},
				{header: 'Company To Parent Relationship', width: 25, dataIndex: 'companyToParentRelationship'},

				{header: 'LEI', width: 25, dataIndex: 'legalEntityIdentifier'},
				{header: 'LEI Status', width: 25, dataIndex: 'legalEntityStatus', hidden: true},
				{header: 'LEI Assigned Date', width: 25, dataIndex: 'legalEntityAssignedDate', type: 'date', hidden: true},
				{header: 'LEI Disabled Date', width: 25, dataIndex: 'legalEntityDisabledDate', type: 'date', hidden: true},
				{header: 'LEI Registration Address', width: 25, dataIndex: 'legalEntityRegistrationAddress', hidden: true},


				{header: 'Address', width: 25, dataIndex: 'companyAddress', hidden: true},
				{header: 'Fax Number', width: 25, dataIndex: 'companyFaxNumber', hidden: true},
				{header: 'Telephone Number', width: 25, dataIndex: 'companyTelephoneNumber', hidden: true},
				{header: 'Web Address', width: 25, dataIndex: 'companyWebAddress', hidden: true},
				{header: 'Is Private Company', width: 25, dataIndex: 'privateCompany', type: 'boolean', hidden: true}
			],
			isPagingEnabled: function() {
				return true;
			},
			getLoadParams: function() {
				return {'runId': this.getWindow().getMainFormId()};
			}
		}]
	},

	corporateActionTab: {
		title: 'Corporate Actions',
		items: [{
			name: 'integrationCorporateActionListFind',
			xtype: 'gridpanel',
			instructions: 'The following corporate actions were imported and processed for the selected run.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true, doNotFormat: true},
				{header: 'CA ID', width: 30, dataIndex: 'corporateActionIdentifier', type: 'int', tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.', doNotFormat: true},
				{header: 'Election Number', width: 25, dataIndex: 'electionNumber', type: 'int', useNull: true},
				{header: 'Payout Number', width: 25, dataIndex: 'payoutNumber', type: 'int', useNull: true},
				{header: 'Event Type Name', width: 50, dataIndex: 'eventTypeName'},
				{header: 'Event Status Name', width: 50, dataIndex: 'eventStatusName'},
				{header: 'Security Identifier', width: 40, dataIndex: 'investmentSecurityIdentifier', type: 'int', doNotFormat: true},
				{header: 'Declare Date', width: 25, dataIndex: 'declareDate', type: 'date', hidden: true},
				{header: 'Expiration Date', width: 25, dataIndex: 'exDate', type: 'date', hidden: true},
				{header: 'Record Date', width: 25, dataIndex: 'recordDate', type: 'date', hidden: true},
				{header: 'Event Date', width: 25, dataIndex: 'eventDate', type: 'date'},
				{header: 'Additional Date', width: 25, dataIndex: 'additionalEventDate', type: 'date', hidden: true},
				{header: 'Additional Event Value', width: 80, dataIndex: 'additionalEventValue', type: 'float', useNull: true, hidden: true},
				{header: 'Description', width: 60, dataIndex: 'eventDescription'},
				{header: 'Payout Security Identifier', width: 30, dataIndex: 'payoutSecurityIdentifier'},
				{header: 'Payout Security Type Identifier', width: 25, dataIndex: 'payoutSecurityTypeIdentifier', type: 'int', useNull: true, hidden: true},
				{header: 'Before Event Value', width: 25, dataIndex: 'beforeEventValue', type: 'float', useNull: true},
				{header: 'After Event Value', width: 25, dataIndex: 'afterEventValue', type: 'float', useNull: true},
				{header: 'Additional Payout Value', width: 80, dataIndex: 'additionalPayoutValue', type: 'float', useNull: true, hidden: true},
				{header: 'Payout Description', width: 60, dataIndex: 'payoutDescription'},
				{header: 'Is Voluntary', width: 25, dataIndex: 'voluntary', type: 'boolean', hidden: true},
				{header: 'Is Taxable', width: 25, dataIndex: 'taxable', type: 'boolean', hidden: true},
				{header: 'Is Deleted', width: 25, dataIndex: 'deleted', type: 'boolean', hidden: true},
				{header: 'Is Default Election', width: 25, dataIndex: 'defaultElection', type: 'boolean', hidden: true},
				{header: 'Payout Type Identifier', width: 25, dataIndex: 'payoutTypeIdentifier', useNull: true, type: 'int', hidden: true}
			],
			getLoadParams: function() {
				return {'importRunId': this.getWindow().getMainFormId()};
			}
		}]
	},

	corporateActionPositionRequestTab: {
		title: 'Corporate Action Position Requests',
		items: [{
			name: 'integrationCorporateActionPositionRequestListFind',
			xtype: 'gridpanel',
			instructions: 'The following corporate actions were imported and processed for the selected run.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true, doNotFormat: true},
				{header: 'CA ID', width: 30, dataIndex: 'corporateActionIdentifier', type: 'int', tooltip: 'A unique identifier provided by the Corporate Action Provider. The same identifier is used to lookup/update existing events. Markit uses 16 digit identifiers.', doNotFormat: true},

				{header: 'Event Type Name', width: 50, dataIndex: 'eventTypeName'},
				{header: 'Event Indicator Name', width: 50, dataIndex: 'eventIndicatorName'},
				{header: 'Event Status Name', width: 50, dataIndex: 'eventStatusName'},

				{header: 'Security Identifier', width: 40, dataIndex: 'investmentSecurityIdentifier', type: 'int', doNotFormat: true},

				{header: 'CUSIP', width: 40, dataIndex: 'cusip'},
				{header: 'SEDOL', width: 40, dataIndex: 'sedol'},
				{header: 'ISIN', width: 40, dataIndex: 'isin'},


				{header: 'Reference Identifier', width: 25, dataIndex: 'referenceIdentifier', useNull: true, type: 'int', hidden: true},
				{header: 'Business Identifier', width: 25, dataIndex: 'businessIdentifierCode', useNull: true, type: 'int', hidden: true},

				{header: 'Effective Date', width: 25, dataIndex: 'eventDate', type: 'date'}
			],
			getLoadParams: function() {
				return {'importRunId': this.getWindow().getMainFormId()};
			}
		}]
	}
});
