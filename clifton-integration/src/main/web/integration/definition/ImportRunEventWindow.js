Clifton.integration.definition.ImportRunEventWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'integrationImportRunEvent.json',
	getClassName: function(config, entity) {
		if (config.defaultData) {
			config.defaultData.importRun = entity.importRun;
		}
		return 'Clifton.integration.definition.ImportRunEventWindowImpl';
	}
});

Clifton.integration.definition.ImportRunEventWindowImpl = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Import Run Event',
	iconCls: 'run',
	width: 1200,
	height: 550,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const typeName = (w.defaultData) ? w.defaultData.importRun.integrationImportDefinition.type.destinationTableName : undefined;

		switch (typeName) {

			case 'IntegrationReconcileDailyPNLHistory':
				tabs.insert(1, this.dailyPNLTab);
				break;

			case 'IntegrationReconcileAccountBalancesByAccountType':
				tabs.insert(1, this.accountBalancesTab);
				break;

			case 'IntegrationReconcileAccountM2MDailyExpense':
				tabs.insert(1, this.m2mExpensesTab);
				break;

			case 'IntegrationMarketDataExchangeRate':
				tabs.insert(1, this.exchangeRatesTab);
				break;

			case 'IntegrationMarketDataFieldValue':
				tabs.insert(1, this.marketDataFieldValuesTab);
				break;

			case 'IntegrationTradeIntraday':
				tabs.insert(1, this.intradayTab);
				break;

			case 'IntegrationReconcilePositionHistory':
				tabs.insert(1, this.positionsTab);
				break;

			case 'IntegrationManagerPositionHistory':
				tabs.insert(1, this.managerPositionsTab);
				tabs.insert(2, this.unmappedManagerPositionsTab);
				break;
			case 'IntegrationManagerTransaction':
				tabs.insert(1, this.managerTransactionsTab);
				break;
			case 'IntegrationBusinessCompany':
				tabs.insert(1, this.companiesTab);
				break;
			case 'IntegrationTrade':
				tabs.insert(1, this.integrationTradeTab);
				break;
			default:
				break;
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Run Event',
			tbar: [{
				text: 'Reprocess File',
				tooltip: 'Re-run the import processing for this event with the file from this run.',
				iconCls: 'run',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();
					Ext.Msg.confirm('Reprocess File', 'Would you like to re-run the entire processing for this file?', function(a) {
						if (a === 'yes') {
							const params = {runEventId: w.getMainFormId(), limitToImportRun: false};
							const loader = new TCG.data.JsonLoader({
								waitTarget: f,
								waitMsg: 'Starting...',
								params: params,
								conf: params,
								onLoad: function(record, conf) {
									Ext.Msg.alert('Status', 'The process was started.');
								}
							});
							loader.load('integrationImportRunEventRerun.json');
						}
					});
				}
			}, {
				text: 'Reprocess Import',
				tooltip: 'Re-run the import processing for this import run.',
				iconCls: 'run',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();
					Ext.Msg.confirm('Reprocess Import', 'Would you like to re-run the entire processing for this event for this file?', function(a) {
						if (a === 'yes') {
							const params = {runEventId: w.getMainFormId(), limitToImportRun: true};
							const loader = new TCG.data.JsonLoader({
								waitTarget: f,
								waitMsg: 'Starting...',
								params: params,
								conf: params,
								onLoad: function(record, conf) {
									Ext.Msg.alert('Status', 'The process was started.');
								}
							});
							loader.load('integrationImportRunEventRerun.json');
						}
					});
				}
			}, '-', {
				text: 'Download File',
				tooltip: 'View the file that was processed.',
				iconCls: 'view',
				handler: function() {
					const f = this.ownerCt.ownerCt.items.get(0);
					const w = f.getWindow();
					TCG.downloadFile('integrationFileDownload.json?id=' + w.defaultData.importRun.file.id, null, this);
				}
			}, '-', {
				text: 'Help',
				iconCls: 'help',
				handler: function() {
					TCG.openWIKI('IT/Editing+CSV+Files', 'Import Run Help');
				}
			}],

			items: [{
				xtype: 'formpanel',
				url: 'integrationImportRunEvent.json',
				labelFieldName: 'id',
				readOnly: true,
				instructions: 'A Run Event is raised/sent to each Target Application after the Import Run was processed. Target Application(s) are defined on Run\'s Definition Type.',
				items: [
					{fieldLabel: 'Import Run', name: 'importRun.id', xtype: 'linkfield', detailIdField: 'importRun.id', detailPageClass: 'Clifton.integration.definition.ImportRunWindow'},
					{fieldLabel: 'Import Definition', name: 'importRun.integrationImportDefinition.name', xtype: 'linkfield', detailIdField: 'importRun.integrationImportDefinition.id', detailPageClass: 'Clifton.integration.definition.ImportDefinitionWindow'},
					{fieldLabel: 'File Definition', name: 'importRun.integrationImportDefinition.fileDefinition.fileName', xtype: 'linkfield', detailIdField: 'importRun.integrationImportDefinition.fileDefinition.id', detailPageClass: 'Clifton.integration.file.FileDefinitionWindow'},
					{
						fieldLabel: 'Integration File', name: 'importRun.file.fileNameWithPath', xtype: 'linkfield', detailIdField: 'importRun.file.id', detailPageClass: 'Clifton.integration.file.FileWindow',
						qtip: 'The path to the file that started this run'
					},
					{
						xtype: 'columnpanel',
						defaults: {xtype: 'displayfield', type: 'date'},
						columns: [
							{rows: [{fieldLabel: 'Effective Date', name: 'importRun.effectiveDate'}], config: {columnWidth: 0.34}},
							{rows: [{fieldLabel: 'Run Start', name: 'importRun.startProcessDate'}], config: {columnWidth: 0.33}},
							{rows: [{fieldLabel: 'Run End', name: 'importRun.endProcessDate'}], config: {columnWidth: 0.33}}
						]
					},
					{fieldLabel: 'Status', name: 'status.name'},
					{fieldLabel: 'Error Message', name: 'error', xtype: 'textarea', anchor: '-35 -200'}
				]
			}]
		}]
	}],


	managerPositionsTab: {
		title: 'Manager Positions',
		items: [{
			name: 'integrationManagerPositionHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following manager positions were imported and processed for selected run.',
			columns: [
				{header: 'Bank Code', width: 40, dataIndex: 'managerBank.bankCode'},
				{header: 'Original Bank Code', width: 60, dataIndex: 'managerBank.originalBankCode'},
				{header: 'Asset Class', width: 80, dataIndex: 'managerSecurity.assetClass'},
				{header: 'Source Asset Class', width: 80, dataIndex: 'managerSecurity.sourceAssetClass', hidden: true},
				{header: 'Security', width: 75, dataIndex: 'managerSecurity.securityIdentifier', hidden: true},
				{header: 'Security Type', width: 75, dataIndex: 'managerSecurity.securityIdentifierType', hidden: true},
				{header: 'Security Description', width: 110, dataIndex: 'managerSecurity.securityDescription'},
				{header: 'Type', width: 75, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, url: 'integrationManagerPositionTypeList.json'}},
				{header: 'Cash', width: 60, dataIndex: 'cashValue', type: 'currency'},
				{header: 'Securities', width: 60, dataIndex: 'securityValue', type: 'currency'},
				{header: 'Value', width: 60, dataIndex: 'value', type: 'currency'}
			],
			getLoadParams: function() {
				return {
					readUncommittedRequested: true,
					runId: this.getWindow().defaultData.importRun.id
				};
			}
		}]
	},

	unmappedManagerPositionsTab: {
		title: 'Unmapped Manager Positions',
		items: [{
			name: 'integrationManagerPositionHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following manager positions were imported but could not be mapped to a corresponding position type because of a missing mapping.',
			columns: [
				{header: 'Bank Code', width: 60, dataIndex: 'managerBank.bankCode'},
				{header: 'Asset Class', width: 130, dataIndex: 'managerSecurity.assetClass'},
				{header: 'Source Asset Class', width: 80, dataIndex: 'managerSecurity.sourceAssetClass', hidden: true},
				{header: 'Security', width: 75, dataIndex: 'managerSecurity.securityIdentifier', hidden: true},
				{header: 'Security Type', width: 75, dataIndex: 'managerSecurity.securityIdentifierType', hidden: true},
				{header: 'Security Description', width: 110, dataIndex: 'managerSecurity.securityDescription'},
				{header: 'Type', width: 75, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', loadAll: true, url: 'integrationManagerPositionTypeList.json'}},
				{header: 'Cash', width: 80, dataIndex: 'cashValue', type: 'currency'},
				{header: 'Securities', width: 80, dataIndex: 'securityValue', type: 'currency'},
				{header: 'Value', width: 80, dataIndex: 'value', type: 'currency'}
			],
			getLoadParams: function() {
				return {
					readUncommittedRequested: true,
					unmappedOnly: true,
					'runId': this.getWindow().defaultData.importRun.id
				};
			}
		}]
	},

	accountBalancesTab: {
		title: 'Account Balances',
		items: [{
			name: 'integrationReconcileAccountBalancesByAccountTypeListFind',
			xtype: 'gridpanel',
			instructions: 'The following account balances were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Position Date', width: 80, dataIndex: 'positionDate'},

				{header: 'Base Currency', width: 80, dataIndex: 'currencySymbolBase'},
				{header: 'Local Currency', width: 80, dataIndex: 'currencySymbolLocal'},


				{header: 'Expected Transfer Local', width: 80, dataIndex: 'expectedTransferAmountLocal', type: 'currency'},
				{header: 'Expected Transfer Base', width: 80, dataIndex: 'expectedTransferAmountBase', type: 'currency'},
				{header: 'Collateral Requirement Local', width: 80, dataIndex: 'collateralRequirementLocal', type: 'currency'},
				{header: 'Collateral Requirement Base', width: 80, dataIndex: 'collateralRequirementBase', type: 'currency'},
				{header: 'Collateral Amount Local', width: 80, dataIndex: 'collateralAmountLocal', type: 'currency'},
				{header: 'Collateral Amount Base', width: 80, dataIndex: 'collateralAmountBase', type: 'currency'},
				{header: 'Is Group', width: 40, dataIndex: 'groupAccount', type: 'boolean'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},
	m2mExpensesTab: {
		title: 'M2M Expenses',
		items: [{
			name: 'integrationReconcileAccountM2MDailyExpenseListFind',
			xtype: 'gridpanel',
			instructions: 'The following account balances were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Expense Type', width: 80, dataIndex: 'expenseType'},
				{header: 'Currency', width: 80, dataIndex: 'currencySymbol'},
				{header: 'Amount', width: 80, dataIndex: 'expenseAmount', type: 'currency'},
				{header: 'Position Date', width: 80, dataIndex: 'positionDate'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},
	dailyPNLTab: {
		title: 'Daily PNL',
		items: [{
			name: 'integrationReconcileDailyPNLHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following daily pnl values are imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Account Name', width: 60, dataIndex: 'accountName'},
				{header: 'Client Account Number', width: 60, dataIndex: 'clientAccountNumber'},

				{header: 'Position Date', width: 60, dataIndex: 'positionDate'},

				{header: 'Security', width: 80, dataIndex: 'securitySymbol'},
				{header: 'Security Type', width: 80, dataIndex: 'securitySymbolType'},

				{header: 'Position Currency', width: 80, dataIndex: 'positionCurrencySymbol'},
				{header: 'Account Currency', width: 80, dataIndex: 'accountCurrencySymbol'},

				{header: 'Fx Rate', width: 80, dataIndex: 'fxRate', type: 'float'},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Prior Quantity', width: 60, dataIndex: 'priorQuantity', type: 'float'},
				{header: 'Today Closed Quantity', width: 60, dataIndex: 'todayClosedQuantity', type: 'float'},
				{header: 'Is Short', width: 80, dataIndex: 'shortPosition', type: 'boolean'},

				{header: 'Today Commission Local', width: 60, dataIndex: 'todayCommissionLocal', type: 'currency'},
				{header: 'Today Commission Base', width: 60, dataIndex: 'todayCommissionBase', type: 'currency'},
				{header: 'Today Realized Gain Loss Local', width: 60, dataIndex: 'todayRealizedGainLossLocal', type: 'currency'},
				{header: 'Today Realized Gain Loss Base', width: 60, dataIndex: 'todayRealizedGainLossBase', type: 'currency'},
				{header: 'Open Trade Equity Local', width: 60, dataIndex: 'openTradeEquityLocal', type: 'currency'},
				{header: 'Open Trade Equity Base', width: 60, dataIndex: 'openTradeEquityBase', type: 'currency'},
				{header: 'Prior Open Trade Equity Local', width: 60, dataIndex: 'priorOpenTradeEquityLocal', type: 'currency'},
				{header: 'Prior Open Trade Equity Base', width: 60, dataIndex: 'priorOpenTradeEquityBase', type: 'currency'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},
	positionsTab: {
		title: 'Positions',
		items: [{
			name: 'integrationReconcilePositionHistoryListFind',
			xtype: 'gridpanel',
			instructions: 'The following positions were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Account Name', width: 130, dataIndex: 'accountName'},
				{header: 'Client Account Number', width: 130, dataIndex: 'clientAccountNumber'},
				{header: 'Unique Position ID', width: 130, dataIndex: 'uniquePositionID', hidden: true},

				{header: 'Position Date', width: 80, dataIndex: 'positionDate'},
				{header: 'Trade Date', width: 80, dataIndex: 'tradeDate'},
				{header: 'Settle Date', width: 80, dataIndex: 'settleDate'},

				{header: 'Security', width: 80, dataIndex: 'securitySymbol'},
				{header: 'Security Type', width: 80, dataIndex: 'securitySymbolType'},

				{header: 'Position Currency', width: 80, dataIndex: 'positionCurrencySymbol'},
				{header: 'Account Currency', width: 80, dataIndex: 'accountCurrencySymbol'},

				{header: 'Fx Rate', width: 80, dataIndex: 'fxRate', type: 'float'},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Is Short', width: 80, dataIndex: 'shortPosition', type: 'boolean'},

				{header: 'Trade Price', width: 80, dataIndex: 'tradePrice', type: 'currency'},
				{header: 'MarketPrice', width: 80, dataIndex: 'marketPrice', type: 'currency'},

				{header: 'Cost Basis Local', width: 80, dataIndex: 'costBasisLocal', type: 'currency'},
				{header: 'Cost Basis Base', width: 80, dataIndex: 'costBasisBase', type: 'currency'},
				{header: 'Market Value Local', width: 80, dataIndex: 'marketValueLocal', type: 'currency'},
				{header: 'Market Value Base', width: 80, dataIndex: 'marketValueBase', type: 'currency'},
				{header: 'Option Value Local', width: 80, dataIndex: 'optionMarketValueLocal', type: 'currency', hidden: true},
				{header: 'Option Value Base', width: 80, dataIndex: 'optionMarketValueBase', type: 'currency', hidden: true},
				{header: 'OTE Local', width: 80, dataIndex: 'openTradeEquityLocal', type: 'currency'},
				{header: 'OTE Base', width: 80, dataIndex: 'openTradeEquityBase', type: 'currency'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},
	intradayTab: {
		title: 'External Trades',
		items: [{
			name: 'integrationTradeIntradayListFind',
			xtype: 'gridpanel',
			instructions: 'The following trades were imported and processed for selected run.',
			columns: [
				{header: 'Account', width: 60, dataIndex: 'accountNumber'},
				{header: 'Executing Broker', width: 130, dataIndex: 'executingBrokerCompanyName'},
				{header: 'Clearing Broker', width: 130, dataIndex: 'clearingBrokerCompanyName'},

				{header: 'Investment Security', width: 80, dataIndex: 'investmentSecuritySymbol'},
				{header: 'Paying Security', width: 80, dataIndex: 'payingSecuritySymbol'},
				{header: 'Symbol Type', width: 80, dataIndex: 'securitySymbolType'},
				{
					header: 'B/S', width: 30, dataIndex: 'buy', type: 'boolean',
					renderer: function(v, metaData) {
						metaData.css = v ? 'buy-light' : 'sell-light';
						return v ? 'BUY' : 'SELL';
					}
				},
				{header: 'Trade Date', width: 12, dataIndex: 'tradeDate', hidden: true},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Trade Price', width: 80, dataIndex: 'price', type: 'float'},
				{
					header: 'Status',
					width: 80,
					dataIndex: 'status',
					filter: {
						type: 'list', options: [
							['CONFIRMED', 'CONFIRMED'],
							['PENDING_BROKER_ACCEPTANCE', 'PENDING_BROKER_ACCEPTANCE'],
							['UNCONFIRMED', 'UNCONFIRMED']]
					}
				},
				{
					header: 'Type',
					width: 80,
					dataIndex: 'type',
					filter: {
						type: 'list', options: [
							['ADDED', 'ADDED'],
							['CHANGED', 'CHANGED'],
							['DELETED', 'DELETED']]
					}
				},
				{header: 'Unique Trade Id', width: 80, dataIndex: 'uniqueTradeId'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},
	integrationTradeTab: {
		title: 'External Trades',
		items: [{
			name: 'integrationTradeListFind',
			xtype: 'gridpanel',
			instructions: 'The following trades were imported and processed for as trade imports.',
			columns: [
				{header: 'Unique Identifier', width: 60, dataIndex: 'uniqueIdentifier'},

				{header: 'Holding Account', width: 60, dataIndex: 'holdingAccountNumber'},
				{header: 'Client Account', width: 60, dataIndex: 'clientAccountNumber', hidden: true},
				{header: 'Broker Code', width: 60, dataIndex: 'executingBrokerCode'},

				{header: 'Security Symbol', width: 80, dataIndex: 'securitySymbol'},
				{header: 'Security Description', width: 130, dataIndex: 'securityDescription'},
				{header: 'CUSIP', width: 80, dataIndex: 'cusip'},
				{header: 'SEDOL', width: 80, dataIndex: 'sedol', hidden: true},
				{header: 'ISIN', width: 80, dataIndex: 'isin', hidden: true},

				{header: 'Paying Security', width: 50, dataIndex: 'payingSecuritySymbol'},
				{
					header: 'B/S', width: 40, dataIndex: 'buy', type: 'boolean',
					renderer: function(v, metaData) {
						metaData.css = v ? 'buy-light' : 'sell-light';
						return v ? 'BUY' : 'SELL';
					}
				},
				{header: 'Trade Date', width: 80, dataIndex: 'tradeDate', type: 'date'},
				{header: 'Settlement Date', width: 80, dataIndex: 'settlementDate', type: 'date'},
				{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float'},
				{header: 'Trade Price', width: 80, dataIndex: 'price', type: 'float'},
				{header: 'Trade Price Base', width: 80, dataIndex: 'priceBase', type: 'float', hidden: true},
				{header: 'Trade Amount', width: 80, dataIndex: 'tradeAmount', type: 'float'},
				{header: 'Trade Amount Base', width: 80, dataIndex: 'tradeAmountBase', type: 'float', hidden: true},

				{header: 'Commission Per Unit', width: 80, dataIndex: 'commissionPerUnit', type: 'float'},
				{header: 'Commission', width: 80, dataIndex: 'commissionAmount', type: 'float'},
				{header: 'Fee Amount', width: 80, dataIndex: 'feeAmount', type: 'float'},
				{header: 'Opening Price', width: 80, dataIndex: 'openingPrice', type: 'float'},
				{header: 'Opening Date', width: 80, dataIndex: 'openingDate', type: 'date'},
				{header: 'FX Rate', width: 80, dataIndex: 'exchangeRateToBase', type: 'float', hidden: true},
				{header: 'CCY', dataIndex: 'currencyTrade', type: 'boolean', hidden: true}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},
	exchangeRatesTab: {
		title: 'Exchange Rates',
		items: [{
			name: 'integrationMarketDataExchangeRateListFind',
			xtype: 'gridpanel',
			instructions: 'The following exchange rates were imported and processed for selected run.',
			columns: [
				{header: 'From Currency', dataIndex: 'fromCurrency'},
				{header: 'To Currency', dataIndex: 'toCurrency'},
				{header: 'Exchange Rate', dataIndex: 'rate', type: 'float'},
				{header: 'Inverse Rate', dataIndex: 'inverseRate', type: 'float'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},
	marketDataFieldValuesTab: {
		title: 'Market Data Field Values',
		items: [{
			name: 'integrationMarketDataFieldValueListFind',
			xtype: 'gridpanel',
			instructions: 'The following exchange rates were imported and processed for selected run.',
			columns: [
				{header: 'Field Name', dataIndex: 'marketDataFieldName'},
				{header: 'Security Symbol', dataIndex: 'investmentSecuritySymbol'},
				{header: 'Measure Date', dataIndex: 'measureDate'},
				{header: 'Measure Time', dataIndex: 'measureTime', type: 'int', hidden: true},
				{header: 'Measure Value', dataIndex: 'measureValue'}
			],
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	},

	managerTransactionsTab: {
		title: 'Manager Transactions',
		items: [{
			name: 'investmentManagerCustodianTransactionListFind',
			xtype: 'gridpanel',
			topToolbarSearchParameter: 'searchPattern',
			instructions: 'The following manager transactions were imported and processed for selected run.',
			pageSize: 100,
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Transaction Date', width: 25, dataIndex: 'transactionDate', defaultSortColumn: true},
				{header: 'Transaction Amount', width: 25, dataIndex: 'transactionAmount', type: 'currency', summaryType: 'sum', negativeInRed: true},
				{header: 'Description', width: 100, dataIndex: 'description'},
				{header: 'Custodian Account', width: 50, dataIndex: 'custodianAccount.label', filter: {searchFieldName: 'custodianAccountNumber'}},
				{header: 'Custodian Holding Account', width: 50, dataIndex: 'custodianAccount.holdingAccount.label', filter: false},
				{header: 'Source Data Identifier', width: 25, dataIndex: 'sourceDataIdentifier', hidden: true}
			],
			isPagingEnabled: function() {
				return true;
			},
			getTopToolbarInitialLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to last 7 days of balances
					this.setFilterValue('transactionDate', {'after': new Date().add(Date.DAY, -7)});
				}
				return {sourceDataIdentifier: this.getWindow().defaultData.importRun.id};
			}
		}]
	},

	companiesTab: {
		title: 'Companies',
		items: [{
			name: 'integrationBusinessCompanyListFind',
			xtype: 'gridpanel',
			pageSize: 100,
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Bloomberg Company Identifier', width: 25, dataIndex: 'bloombergCompanyIdentifier', defaultSortColumn: true, type: 'int'},
				{header: 'Parent Bloomberg Company Identifier', width: 25, dataIndex: 'parentBloombergCompanyIdentifier', type: 'int'},
				{header: 'Ultimate Parent Bloomberg Company Identifier', width: 25, dataIndex: 'ultimateParentBloombergCompanyIdentifier', type: 'int'},
				{header: 'Obligor Bloomberg Company Identifier', width: 25, dataIndex: 'obligorBloombergCompanyIdentifier', type: 'int'},

				{header: 'Long Company Name', width: 25, dataIndex: 'longCompanyName'},
				{header: 'Legal Company Name', width: 25, dataIndex: 'companyLegalName'},
				{header: 'Alternate Company Name', width: 25, dataIndex: 'alternateCompanyName', hidden: true},

				{header: 'Company Corporate Ticker', width: 25, dataIndex: 'companyCorporateTicker', hidden: true},
				{header: 'Ultimate Parent Exchange', width: 25, dataIndex: 'ultimateParentTickerExchange', hidden: true},
				{header: 'Ultimate Parent Ticker', width: 25, dataIndex: 'ultimateParentTickerExchangeCorporateTicker', hidden: true},

				{header: 'Sector Name', width: 25, dataIndex: 'industrySectorName', hidden: true},
				{header: 'Sector Group Name', width: 25, dataIndex: 'industrySectorGroupName', hidden: true},
				{header: 'Sector Sub Group Name', width: 25, dataIndex: 'industrySectorSubGroupName', hidden: true},

				{header: 'Sector Number', width: 25, dataIndex: 'industrySectorName', type: 'int', hidden: true},
				{header: 'Sector Group Number', width: 25, dataIndex: 'industrySectorGroupName', type: 'int', hidden: true},
				{header: 'Sector Sub Group Number', width: 25, dataIndex: 'industrySectorSubGroupName', type: 'int', hidden: true},

				{header: 'Country Of Domicile', width: 25, dataIndex: 'countryOfDomicile'},
				{header: 'Country Of Incorporation', width: 25, dataIndex: 'countryOfIncorporation'},
				{header: 'Country Of Risk', width: 25, dataIndex: 'countryOfRisk'},
				{header: 'State Of Domicile', width: 25, dataIndex: 'stateOfDomicile', hidden: true},
				{header: 'State Of Incorporation', width: 25, dataIndex: 'stateOfIncorporation', hidden: true},

				{header: 'Is Ultimate Parent', width: 25, dataIndex: 'ultimateParent', type: 'boolean', hidden: true},
				{header: 'Is Acquired By Parent', width: 25, dataIndex: 'acquiredByParent', type: 'boolean', hidden: true},
				{header: 'Company To Parent Relationship', width: 25, dataIndex: 'companyToParentRelationship'},

				{header: 'LEI', width: 25, dataIndex: 'legalEntityIdentifier'},
				{header: 'LEI Status', width: 25, dataIndex: 'legalEntityStatus', hidden: true},
				{header: 'LEI Assigned Date', width: 25, dataIndex: 'legalEntityAssignedDate', type: 'date', hidden: true},
				{header: 'LEI Disabled Date', width: 25, dataIndex: 'legalEntityDisabledDate', type: 'date', hidden: true},
				{header: 'LEI Registration Address', width: 25, dataIndex: 'legalEntityRegistrationAddress', hidden: true},


				{header: 'Address', width: 25, dataIndex: 'companyAddress', hidden: true},
				{header: 'Fax Number', width: 25, dataIndex: 'companyFaxNumber', hidden: true},
				{header: 'Telephone Number', width: 25, dataIndex: 'companyTelephoneNumber', hidden: true},
				{header: 'Web Address', width: 25, dataIndex: 'companyWebAddress', hidden: true},
				{header: 'Is Private Company', width: 25, dataIndex: 'privateCompany', type: 'boolean', hidden: true}
			],
			isPagingEnabled: function() {
				return true;
			},
			getLoadParams: function() {
				return {'runId': this.getWindow().defaultData.importRun.id};
			}
		}]
	}
});
