Clifton.integration.definition.IntegrationSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'integrationDefinitionSetupWindow',
	title: 'Integration Imports',
	iconCls: 'run',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Import Runs',
				items: [{
					xtype: 'integration-importRunGrid',
					columnOverrides: [{dataIndex: 'integrationImportDefinition.type.name', hidden: false}]
				}]
			},


			{
				title: 'Import Run Events',
				items: [{
					xtype: 'integration-importRunEventGrid'
				}]
			},


			{
				title: 'Integration Files',
				items: [{
					xtype: 'integration-fileGrid',
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Display', xtype: 'combo', name: 'displayType', width: 150, minListWidth: 150, mode: 'local', value: 'ALL', displayField: 'name', valueField: 'value',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name', 'description'],
									data: [['ALL', 'All Files', 'View all external files that we have received'],
										['KNOWN', 'Known Files', 'View files that have a corresponding file definition'],
										['UNKNOWN', 'Unknown Files', 'View files that we have received, but that do not have a corresponding file definition']]
								}),
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}
						];
					},
					getLoadParams: function(firstLoad) {
						const result = {excludeFileSourceType: 'EXPORT'};
						if (firstLoad) {
							this.setFilterValue('receivedDate', {'after': Clifton.calendar.getBusinessDayFrom(-2)});
						}
						else {

							const displayType = TCG.getChildByName(this.getTopToolbar(), 'displayType').getValue();

							if (displayType === 'KNOWN') {
								result.known = true;

							}
							else if (displayType === 'UNKNOWN') {
								result.known = false;
							}
						}
						return result;
					}
				}]
			},


			{
				title: 'System Files',
				items: [{
					name: 'integrationSystemFileList',
					xtype: 'gridpanel',
					groupField: 'integrationDirectory',
					forceLocalFiltering: true,
					appendStandardColumns: false,
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]}',
					instructions: 'If "All Folders" is selected, this page will show all files in the Input, In Process, Errors, FTP, and SFTP ' +
							'directories grouped by directory.  Select a specific folder in the top-right corner to view files from that folder only.',
					columns: [
						{header: 'Directory', dataIndex: 'integrationDirectory', width: 50, filter: false, tooltip: 'Use display filter in top-right to filter Directory Column'},
						{header: 'File Name', dataIndex: 'fileName', width: 150},
						{header: 'File Path', dataIndex: 'filePath', width: 150, hidden: true},
						{header: 'Folder', dataIndex: 'folder', type: 'boolean', width: 30},
						{header: 'Last Modified', dataIndex: 'modifiedDate', width: 50},
						{
							header: 'Size', dataIndex: 'size', width: 40, renderer: function(value, metaData, record) {
								return TCG.fileSize(value);
							}, filter: false
						},
						{
							header: 'Download', width: 30, filter: false, renderer: function(value, metadata) {
								return TCG.renderActionColumn('view', 'Download', 'Download this file', 'systemFileDownload');
							}
						}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Display', xtype: 'combo', name: 'directory', width: 150, minListWidth: 150, mode: 'local', value: 'ERRORS', displayField: 'name', valueField: 'value',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name', 'description'],
									data: [
										['INPUT', 'Input Folder', 'View files in the Input folder'],
										['BATCH', 'Batch Input Folder', 'View files in the Batch Input folder'],
										['INPROCESS', 'In Process Folder', 'View files in the In Process folder'],
										['ERRORS', 'Errors Folder', 'View files in the Errors folder']
									]
								}),
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}
						];
					},
					editor: {
						deleteEnabled: true,
						addEnabled: false
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('folder', false);
						}

						const directory = TCG.getChildByName(this.getTopToolbar(), 'directory').getValue();

						return {directory: directory};
					},
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, e) {
								if (TCG.isActionColumn(e.target)) {
									if (TCG.getActionColumnEventName(e) === 'systemFileDownload') {
										const row = grid.getStore().getAt(rowIndex);
										const systemFileId = row.id;
										TCG.downloadFile('integrationSystemFileDownload.json?id=' + systemFileId, null, grid);
									}
								}
							}
						}
					}
				}]
			},


			{
				title: 'File Definitions',
				items: [{
					xtype: 'integration-fileDefinitionGrid',
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Display', xtype: 'combo', name: 'displayType', width: 150, minListWidth: 150, mode: 'local', value: 'ALL', displayField: 'name', valueField: 'value',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name', 'description'],
									data: [['ALL', 'All File Definitions', 'View all file definitions'],
										['USED', 'Used File Definitions', 'View file definitions that have a corresponding import'],
										['UNUSED', 'Unused File Definitions', 'View file definitions that do not have a defined import']]
								}),
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}
						];
					},
					getLoadParams: function(firstLoad) {
						if (!firstLoad) {
							const displayType = TCG.getChildByName(this.getTopToolbar(), 'displayType').getValue();

							if (displayType === 'USED') {
								return {used: true};

							}
							else if (displayType === 'UNUSED') {
								return {used: false};
							}
						}
						// no default params on first load
					},
					editor: {
						detailPageClass: 'Clifton.integration.file.FileDefinitionWindow'
					}
				}]
			},


			{
				title: 'File Definition Types',
				items: [{
					name: 'integrationFileDefinitionTypeList',
					xtype: 'gridpanel',
					instructions: 'File Definition Types distinguish files from one another, for instance, broker files vs. manager files.',
					columns: [
						{header: 'Type Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 300}
					],
					editor: {
						detailPageClass: 'Clifton.integration.file.FileDefinitionTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Import Definitions',
				items: [{
					xtype: 'integration-importDefinitionGrid'
				}]
			},


			{
				title: 'Transformations',
				items: [{
					xtype: 'integration-transformationGrid'
				}]
			},


			{
				title: 'Import Types',
				items: [{
					xtype: 'integration-importTypeGrid'
				}]
			},


			{
				title: 'Sources',
				items: [{
					xtype: 'integration-sourceGrid',
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {typeName: 'Import'};
					}
				}]
			},


			{
				title: 'Target Applications',
				items: [{
					xtype: 'gridpanel',
					name: 'integrationTargetApplicationListFind',
					instructions: 'Target Applications describe what applications receive events raised after integration processes files.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Application Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 200},
						{header: 'Request Timeout', dataIndex: 'requestTimeout', width: 50, type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.integration.target.TargetApplicationWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Archive Strategies',
				items: [{
					name: 'integrationFileArchiveStrategyListFind',
					xtype: 'gridpanel',
					instructions: 'A File Archive Strategy describes how files from a particular import source will be archived.  ' +
							'For example, files from Goldman Sachs might be kept in a folder called "GSC" for 90 days.  ' +
							'Use 0 days to never delete files.',
					columns: [
						{header: 'Strategy Name', dataIndex: 'name', width: 150},
						{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
						{header: 'Path To Archive Folder', dataIndex: 'pathToArchiveFolder', width: 100},
						{header: 'Days To Keep File', dataIndex: 'daysToKeepFile', width: 50, type: 'int'},
						{header: 'Days To Keep Encrypted File', dataIndex: 'daysToKeepEncryptedFile', width: 70, type: 'int'},
						{header: 'Days To Keep Source Data', dataIndex: 'daysToKeepSourceData', width: 70, type: 'int'},
						{header: 'Delete On New File', dataIndex: 'deleteOnNewFile', type: 'boolean', width: 50, tooltip: 'If checked, any related archive files will be deleted when a new one loads'}
					],
					editor: {
						detailPageClass: 'Clifton.integration.file.FileArchiveStrategyWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
