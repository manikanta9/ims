Clifton.integration.definition.ImportTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Import Definition Type',
	iconCls: 'run',
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Definition Type',
				items: [{
					xtype: 'formpanel',
					url: 'integrationImportDefinitionType.json',
					labelWidth: 145,
					instructions: 'Import type defines the type of data received from external sources. Each type usually maps to a destination table in the integration system that stores normalized source data for that type. Corresponding Event can be raised to notify the Main System that new data is available for consumption.',
					items: [
						{fieldLabel: 'Type Name', name: 'name', readOnly: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Destination Table Name', name: 'destinationTableName', readOnly: true, qtip: 'The final table in Integration that this import will eventually populate'},
						{fieldLabel: 'Event Name', name: 'eventName', readOnly: true},
						{fieldLabel: 'Transformation Folder', name: 'transformationFolder', readOnly: true, qtip: 'The name of the folder on the local drive where integration will store a local copy of the transformation(s) file.'},
						{fieldLabel: 'Transformation File Name Prefix', name: 'transformationFileNamePrefix', readOnly: true, qtip: 'The prefix that will be added to document name of each file uploaded for transformations linked to this type.'}
					]
				}]
			},


			{
				title: 'Import Definitions',
				items: [{
					xtype: 'integration-importDefinitionGrid',
					columnOverrides: [{dataIndex: 'type.name', hidden: true}],
					getLoadParams: function() {
						return {typeId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Target Applications',
				items: [{
					xtype: 'gridpanel',
					name: 'integrationTargetApplicationListFind',
					instructions: 'Target Applications describe what applications receive events raised after integration processes files.',
					columns: [
						{header: 'ID', data: 'id', width: 50, hidden: true},
						{header: 'Application Name', dataIndex: 'name', width: 100},
						{header: 'Description', dataIndex: 'description', width: 200},
						{header: 'Request Timeout', dataIndex: 'requestTimeout', width: 50, type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.integration.target.TargetApplicationWindow',
						getDeleteURL: function() {
							return 'integrationTargetApplicationDefinitionTypeDelete.json';
						},
						getDeleteParams: function(selectionModel) {
							const targetApplicationId = selectionModel.getSelected().id;
							const definitionTypeId = this.getWindow().getMainFormId();
							return {
								targetApplicationId: targetApplicationId,
								definitionTypeId: definitionTypeId
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'targetApplication', url: 'integrationTargetApplicationListFind.json', emptyText: '< Select Target Application >', width: 175, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a target application to this definition type.',
								iconCls: 'add',
								scope: gridPanel,
								handler: function() {
									const targetApplication = TCG.getChildByName(toolBar, 'targetApplication');
									const targetApplicationId = targetApplication.getValue();
									const definitionTypeId = this.getWindow().getMainFormId();
									targetApplication.clearValue();

									const params = {};
									params['targetApplicationId'] = targetApplicationId;
									params['definitionTypeId'] = definitionTypeId;

									if (targetApplicationId === '') {
										TCG.showError('You must first select desired target application from the list.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											timeout: 120000,
											waitMsg: 'Saving: Please Wait.',
											params: params,
											scope: gridPanel,
											success: function(form, action) {
												const result = Ext.decode(form.responseText);
												if (result.success) {
													const prompt = this;
													if (prompt.isUseWaitMsg()) {
														prompt.getMsgTarget().mask('Saving Target Application for Definition Type... Success');
														const unmaskFunction = function() {
															prompt.getMsgTarget().unmask();
														};
														unmaskFunction.defer(600, this);
														this.scope.reload();
													}
												}
												else {
													this.onFailure();
													this.getMsgTarget().unmask();
													TCG.data.ErrorHandler.handleFailure(this, result);
												}
											},
											error: function(form, action) {
												Ext.Msg.alert('Failed to save Target Application for Definition Type', action.result.data);
											}
										});
										loader.load('integrationTargetApplicationDefinitionTypeLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function() {
						return {definitionTypeId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
