Clifton.integration.definition.ImportDefinitionWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'integrationImportDefinition.json',

	getClassName: function(config, entity) {
		return 'Clifton.integration.definition.ImportDefinitionWindowImpl';
	}
});


Clifton.integration.definition.ImportDefinitionWindowImpl = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Import Definition',
	iconCls: 'run',
	width: 800,
	height: 650,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);

		if (w.defaultData && w.defaultData.type && w.defaultData.type.name === 'Manager Download') {
			tabs.insert(3, this.importGroupTab);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Import Definition',
				items: [{
					xtype: 'formpanel',
					labelWidth: 130,
					url: 'integrationImportDefinition.json',
					instructions: 'Import Definitions describe what transformations and what file we use to import external data.  ' +
						'If the definition name is "UBSAccountBalancesImport," then the transformation names will be "UBSAccountBalancesImport" and "UBSAccountBalancesImportToFinal."',
					items: [
						{fieldLabel: 'Definition Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', detailIdField: 'type.id', url: 'integrationImportDefinitionTypeListFind.json', detailPageClass: 'Clifton.integration.definition.ImportTypeWindow'},
						{fieldLabel: 'Source', submitValue: false, name: 'fileDefinition.source.name', hiddenName: 'fileDefinition.source.id', xtype: 'combo', doNotAddContextMenu: true, url: 'integrationSourceListFind.json', detailPageClass: 'Clifton.integration.source.SourceWindow'},
						{
							fieldLabel: 'File Definition', name: 'fileDefinition.fileName', displayField: 'fileName', hiddenName: 'fileDefinition.id', xtype: 'combo', url: 'integrationFileDefinitionListFind.json', detailPageClass: 'Clifton.integration.file.FileDefinitionWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {sourceId: f.findField('fileDefinition.source.name').getValue()};
								}
							},
							getDefaultData: function(f) {
								return {source: f.findField('fileDefinition.source.name').getValueObject()};
							}
						},
						{fieldLabel: 'Definition Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'File To File Transformation', name: 'fileTransformation.name', hiddenName: 'fileTransformation.id', xtype: 'combo', url: 'integrationTransformationListFind.json', detailPageClass: 'Clifton.integration.transformation.TransformationWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {
										definitionTypeId: f.findField('type.name').getValue(),
										type: 'FILE_TRANSFORMATION'
									};
								}
							}
							,
							getDefaultData: function(f) {
								const name = f.findField('name').getValue();

								return {
									definitionType: f.findField('type.name').getValueObject(),
									type: 'FILE_TRANSFORMATION',
									name: name ? name.replace('Import', 'FileTransformation') : undefined
								};
							}
						},
						{
							fieldLabel: 'Transformation', name: 'transformation.name', hiddenName: 'transformation.id', xtype: 'combo', url: 'integrationTransformationListFind.json', detailPageClass: 'Clifton.integration.transformation.TransformationWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {
										definitionTypeId: f.findField('type.name').getValue(),
										type: 'DATA_TRANSFORMATION'
									};
								}
							}
							,
							getDefaultData: function(f) {
								const name = f.findField('name').getValue();

								return {
									definitionType: f.findField('type.name').getValueObject(),
									type: 'DATA_TRANSFORMATION',
									name: name ? name.replace('Import', 'Transformation') : undefined
								};
							}
						},
						{
							fieldLabel: 'Effective Date Transformation File', name: 'effectiveDateTransformationFile.fileName', displayField: 'fileName', hiddenName: 'effectiveDateTransformationFile.id', xtype: 'combo',
							url: 'integrationTransformationFileListFind.json?fileType=EFFECTIVE_DATE_TRANSFORMATION', detailPageClass: 'Clifton.integration.transformation.TransformationFileWindow'
						},
						{fieldLabel: 'Data Source', name: 'dataSourceName', hiddenName: 'dataSourceName', valueField: 'name', xtype: 'combo', url: 'marketDataSourceListFind.json', qtip: 'Datasource to use during imports for security and price lookups.'},
						{fieldLabel: 'Order', name: 'order', qtip: 'Specifies the relative order in which this transformation will be executed'},
						{boxLabel: 'Disabled', name: 'disabled', xtype: 'checkbox'},
						{boxLabel: 'Raise External Event to notify other Applications that the data is ready', name: 'raiseExternalEvent', xtype: 'checkbox', qtip: 'If checked, this import will trigger a load-event in IMS'},
						{boxLabel: 'Test Definition (used for testing purposes only)', name: 'testDefinition', xtype: 'checkbox', disabled: 'true', qtip: 'Specifies that this Definition is used for testing purposes only'}
					]
				}]
			},

			{
				title: 'Secondary Transformations',
				items: [{
					name: 'integrationImportTransformationListFind',
					xtype: 'gridpanel',
					instructions: 'Defines transformations that will be applied to this Import in the specified order. Runs after the \'File to File Transformation\' but before the main \'Transformation\'.  The output file must be handled in the main transformation, it is marked for deletion.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Transformation Bean', dataIndex: 'transformationSystemBean.name', width: 300},
						{header: 'Bean Description', dataIndex: 'transformationSystemBean.description', width: 500, hidden: true},
						{header: 'Transformation Type', dataIndex: 'transformationSystemBean.type.name', width: 150},
						{header: 'Order', dataIndex: 'order', type: 'int', width: 50, defaultSortColumn: true}
					],
					getLoadParams: function(firstLoad) {
						return {definitionId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.integration.transformation.IntegrationImportTransformationWindow',
						getDefaultData: function(gridPanel) {
							const content = gridPanel.getWindow().getMainForm().formValues;
							return {
								definition: {
									name: content.name,
									id: content.id
								}
							};
						}
					}
				}]
			},


			{
				title: 'Column Mappings',
				items: [{
					xtype: 'formpanel',
					url: 'integrationImportDefinition.json',

					columnNameStore: {
						xtype: 'arraystore',
						fields: ['columnName'],
						data: ['null'] //will be overridden
					},

					listeners: {
						afterrender: function(fp, isClosing) {
							const window = fp.getWindow();
							const integrationImportDefinitionId = window.getMainFormId();
							const fileDefinitionId = TCG.data.getData('integrationImportDefinition.json?id=' + integrationImportDefinitionId, fp).fileDefinition.id;
							const fileColumnsStr = TCG.data.getData('integrationFileDefinition.json?id=' + fileDefinitionId, fp).fileColumnNames;
							let fileColumns = [];
							if (fileColumnsStr) {
								fileColumns = fileColumnsStr.split(',');
							}

							const storeData = [[]];
							for (let i = 0; i < fileColumns.length; i++) {
								storeData.push([fileColumns[i]]);
							}

							const cm = TCG.getChildByName(fp, 'columnMappingsGrid').getColumnModel();
							const editor = cm.getColumnById(cm.findColumnIndex('sourceColumnName')).editor;
							const store = editor.store;
							store.loadData(storeData);
							editor.setValue(editor.getValue());
						}
					},

					items: [{
						name: 'columnMappingsGrid',
						xtype: 'formgrid',
						storeRoot: 'columnMappings',
						dtoClass: 'com.clifton.integration.incoming.definition.IntegrationImportDefinitionColumnMapping',
						columnsConfig: [
							{
								header: 'Source Column Name', width: 150, dataIndex: 'sourceColumnName',
								editor: {
									xtype: 'combo',
									mode: 'local',
									displayField: 'sourceColumnName',
									valueField: 'sourceColumnName',
									store: {
										xtype: 'arraystore',
										fields: ['sourceColumnName'],
										data: ['null'] //will be overridden
									}
								}
							},
							{header: 'Destination Field', width: 150, dataIndex: 'destinationColumn.name', idDataIndex: 'destinationColumn.id', editor: {xtype: 'combo', loadAll: true, displayField: 'name', url: 'integrationImportDefinitionTypeColumnListFind.json'}}
						]
					}]
				}]
			},


			{
				title: 'Import Runs',
				items: [{
					xtype: 'integration-importRunEventGrid',
					columnOverrides: [{dataIndex: 'integrationImportDefinition.name', hidden: true}],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('importRun.effectiveDate', {'after': Clifton.calendar.getBusinessDayFrom(-5)});
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('error'), true);
						}
						return {integrationImportDefinitionId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}],

	importGroupTab: {
		title: 'Import Groups',
		items: [{
			xtype: 'integration-assetClassGroupGrid',
			getLoadParams: function(firstLoad) {
				return {importDefinitionId: this.getWindow().getMainFormId()};
			},
			editor: {
				detailPageClass: 'Clifton.integration.manager.AssetClassGroupWindow',
				getDeleteURL: function() {
					return 'integrationManagerAssetClassGroupImportDefinitionDelete.json';
				},
				getDeleteParams: function(selectionModel) {
					const importGroupId = selectionModel.getSelected().id;
					const definitionId = this.getWindow().getMainForm().formValues.id;
					return {
						importDefinitionId: definitionId,
						importGroupId: importGroupId
					};
				},
				addEditButtons: function(t, gp) {
					t.add(new TCG.form.ComboBox({
						name: 'importGroupName',
						displayField: 'name',
						url: 'integrationManagerAssetClassGroupListFind.json',
						emptyText: '< Select Import Group >',
						width: 200,
						listWidth: 250
					}));
					t.add({
						text: 'Add Selected',
						xtype: 'button',
						tooltip: 'Add an import group to this destination',
						iconCls: 'add',
						scope: this,
						handler: function() {
							const importGroup = TCG.getChildByName(t, 'importGroupName');
							const importGroupId = importGroup.getValue();
							if (importGroupId === '') {
								TCG.showError('You must first select an Import Group from the list.');
							}
							else {
								const definitionId = gp.getWindow().getMainFormId();
								const loader = new TCG.data.JsonLoader({
									waitTarget: gp,
									waitMsg: 'Adding...',
									params: {importGroupId: importGroupId, importDefinitionId: definitionId},
									onLoad: function(record, conf) {
										gp.reload();
										TCG.getChildByName(t, 'importGroupName').reset();
									}
								});
								loader.load('integrationManagerAssetClassGroupImportDefinitionLink.json');
							}
						}
					});
					t.add('-');
					TCG.grid.GridEditor.prototype.addToolbarDeleteButton.apply(this, arguments);
				}
			}
		}]
	}
});
