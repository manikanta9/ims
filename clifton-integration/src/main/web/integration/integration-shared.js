Ext.ns('Clifton.core', 'Clifton.integration', 'Clifton.integration.outgoing', 'Clifton.integration.definition', 'Clifton.integration.manager', 'Clifton.integration.file', 'Clifton.integration.source', 'Clifton.integration.target', 'Clifton.integration.transformation', 'Clifton.integration.contact');

Clifton.integration.definition.IntegrationImportRunEventGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationImportRunEventListFind',
	topToolbarSearchParameter: 'searchPattern',
	instructions: 'The following Events have been raised/sent to the following applications for processed Import Runs. A Run Event is raised/sent to each Target Application after the Import Run was processed. Target Application(s) are defined on Run\'s Definition Type.',
	rowSelectionModel: 'checkbox',
	additionalPropertiesToRequest: 'importRun.file.id',
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('importRun.effectiveDate', {'after': Clifton.calendar.getBusinessDayFrom(-5)});
			this.setFilterValue('importRun.integrationImportDefinition.testDefinition', false);
		}
		const params = {readUncommittedRequested: true};
		if (this.targetApplicationName) {
			params.targetApplicationName = this.targetApplicationName;
		}
		if (this.importEventName) {
			params.importEventNameEquals = this.importEventName;
		}
		return params;
	},
	onBeforeCreateFilters: function(filters) {
		for (let i = 0; i < filters.length; i++) {
			const filter = filters[i];
			if (filter.dataIndex && filter.dataIndex === 'integrationImportDefinition.name' && this.importEventName) {
				filter.url = filter.url + '?importEventName=' + this.importEventName;
				if (this.targetApplicationName) {
					filter.url = filter.url + '&targetApplicationName=' + this.targetApplicationName;
				}
				break;
			}
		}
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Run ID', width: 85, dataIndex: 'importRun.id', hidden: true},
		{header: 'Application', width: 40, dataIndex: 'targetApplication.name', idDataIndex: 'targetApplication.id', filter: {type: 'combo', searchFieldName: 'targetApplicationId', displayField: 'name', url: 'integrationTargetApplicationListFind.json'}},
		{header: 'Import Definition', width: 95, dataIndex: 'importRun.integrationImportDefinition.name', filter: {searchFieldName: 'importDefinitionName'}},
		{header: 'Import Definition Type', width: 60, dataIndex: 'importRun.integrationImportDefinition.type.name', filter: {searchFieldName: 'typeName'}},
		{
			header: 'Status',
			width: 40,
			dataIndex: 'status.name',
			filter: {type: 'combo', searchFieldName: 'statusId', url: 'integrationImportStatusListFind.json', displayField: 'name'}
		},
		{header: 'Error Message', dataIndex: 'error', sortable: false},
		{header: 'Processed File', dataIndex: 'importRun.file.fileDefinition.fileName', filter: {searchFieldName: 'processedFileName'}, hidden: true},
		{header: 'Effective Date', dataIndex: 'importRun.effectiveDate', width: 40, filter: {searchFieldName: 'effectiveDate'}},
		{header: 'Start Date', dataIndex: 'importRun.startProcessDate', width: 40, filter: {searchFieldName: 'startProcessDate'}, defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Duration', width: 30, dataIndex: 'importRun.executionTimeFormatted', filter: false, sortable: false},
		{header: 'End Date', dataIndex: 'importRun.endProcessDate', width: 40, filter: {searchFieldName: 'endProcessDate'}, hidden: true},
		{header: 'Test Definition', dataIndex: 'importRun.integrationImportDefinition.testDefinition', width: 30, type: 'boolean', sortable: false, filter: {searchFieldName: 'testDefinition'}, hidden: true}
	],
	reprocessRunEvents: function(runEvents, count) {
		const gridPanel = this;
		const grid = this.grid;
		const runEvent = runEvents[count].json;
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Reprocessing...',
			params: {integrationFileId: runEvent.importRun.file.id, targetApplicationName: runEvent.targetApplication.name},
			conf: {rowId: runEvent.importRun.file.id},
			onLoad: function(record, conf) {
				count++;
				if (count === runEvents.length) { // refresh after all repos were transitioned
					gridPanel.reload();
				}
				else {
					gridPanel.reprocessRunEvents(runEvents, count);
				}
			}
		});
		loader.load('integrationFileRerun.json');
	},
	editor: {
		detailPageClass: 'Clifton.integration.definition.ImportRunEventWindow',
		drillDownOnly: true,
		getDefaultData: function() {
			if (this.getGridPanel().importEventName) {
				return {importEventName: this.getGridPanel().importEventName};
			}
		},
		addEditButtons: function(t, gridPanel) {
			const button = t.add({
				text: 'Reprocess Run Events',
				tooltip: 'Re-run the entire import processing for the selected run events.',
				iconCls: 'run',
				scope: this,
				handler: function() {
					const gridPanel = this.getGridPanel();
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be reprocess.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Reprocess Selected Run Events', 'Would you like to re-process these selected files?', function(a) {
							if (a === 'yes') {
								let processEvents = true;
								const uniqueValues = [];
								const runEvents = sm.getSelections();
								Ext.each(runEvents, function(runEvent) {
									const importRunId = runEvent.json.importRun.id;
									if (!uniqueValues.includes(importRunId)) {
										uniqueValues.push(importRunId);
									}
									else {
										processEvents = false;
									}
								});
								if (processEvents) {
									gridPanel.reprocessRunEvents(runEvents, 0);
								}
								else {
									TCG.showError('You may not select multiple events for the same run to reprocess.  If you want to reprocess all events for a run, then reprocess the run from the \'Import Runs\' tab!', 'Multiple Events for the same Import Run');
								}
							}
						});
					}
				}
			});
			t.add('-');
			t.add({
				text: 'Upload',
				xtype: 'splitbutton',
				iconCls: 'import',
				scope: gridPanel,
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Broker/Manager Import',
							tooltip: 'Manually upload a broker or manager file (same as drag and drop)',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								TCG.createComponent('Clifton.integration.definition.IntegrationFileUploadWindow');
							}
						}, {
							text: 'File Revision',
							tooltip: 'Upload a file revision (e.g., statement revisions).  This will bypass processing and archive the file with the following name format: [effective date].[account number].[file extension]',
							iconCls: 'import',
							scope: gridPanel,
							handler: function() {
								TCG.createComponent('Clifton.integration.definition.IntegrationFileRevisionUploadWindow');
							}
						}
					]
				})
			});
			t.add('-');
			t.add({
				text: 'Fix Run Events',
				tooltip: 'Set selected run events to ERROR status',
				iconCls: 'tools',
				scope: this,
				handler: function() {
					const grid = this.grid;
					const rows = grid.getSelectionModel().getSelections();
					if (rows.length === 0) {
						TCG.showError('Please select a run event to fix.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Fix Selected Run Events', 'Would you like to set the selected run events to ERROR status?', function(a) {
							if (a === 'yes') {
								const ids = [];
								for (let i = 0; i < rows.length; i++) {
									ids.push(rows[i].data.id);
								}

								const loader = new TCG.data.JsonLoader({
									waitTarget: grid.ownerCt,
									waitMsg: 'Fixing...',
									params: {runEventIds: ids},
									onLoad: function(record, conf) {
										grid.ownerCt.reload();
									},
									onFailure: function() {
										grid.ownerCt.reload();
									}
								});
								loader.load('integrationImportRunEventFix.json');
							}
						});
					}
				}
			});
			t.add('-');

			TCG.file.enableDD(TCG.getParentByClass(button, Ext.Panel), null, gridPanel, 'integrationFileUpload.json', true);
		}
	}
});
Ext.reg('integration-importRunEventGrid', Clifton.integration.definition.IntegrationImportRunEventGrid);


Clifton.integration.definition.IntegrationImportRunGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationImportRunListFind',
	topToolbarSearchParameter: 'searchPattern',
	instructions: 'The following imports of external data have been processed by the system.  ' +
			'A run is created each time an external file with active import definition(s) is uploaded to the FTP, SFTP or Input folder.  ' +
			'Each run is associated with a corresponding Import Definition.',
	rowSelectionModel: 'checkbox',
	additionalPropertiesToRequest: 'id|file.id',
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('effectiveDate', {'after': Clifton.calendar.getBusinessDayFrom(-5)});
			this.setFilterValue('integrationImportDefinition.testDefinition', false);
		}
		const params = {readUncommittedRequested: true};
		if (this.importEventName) {
			params.importEventNameEquals = this.importEventName;
		}
		return params;
	},
	onBeforeCreateFilters: function(filters) {
		for (let i = 0; i < filters.length; i++) {
			const filter = filters[i];
			if (filter.dataIndex && filter.dataIndex === 'integrationImportDefinition.name' && this.importEventName) {
				filter.url = filter.url + '?importEventName=' + this.importEventName;
				break;
			}
		}
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Import Definition', width: 85, dataIndex: 'integrationImportDefinition.name', filter: {searchFieldName: 'importDefinitionName'}},
		{header: 'Import Definition Type', width: 60, dataIndex: 'integrationImportDefinition.type.name', filter: {searchFieldName: 'typeName'}, hidden: true},
		{
			header: 'Status',
			width: 50,
			dataIndex: 'status.name',
			filter: {type: 'combo', searchFieldName: 'statusId', url: 'integrationImportStatusListFind.json', displayField: 'name'}
		},
		{header: 'Processed File', dataIndex: 'file.fileDefinition.fileName', filter: {searchFieldName: 'processedFileName'}, hidden: true},
		{header: 'Effective Date', dataIndex: 'effectiveDate', width: 30},
		{header: 'Start Date', dataIndex: 'startProcessDate', width: 40, defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Duration', width: 30, dataIndex: 'executionTimeFormatted', filter: false, sortable: false},
		{header: 'End Date', dataIndex: 'endProcessDate', width: 30, hidden: true},
		{header: 'Test Definition', dataIndex: 'integrationImportDefinition.testDefinition', width: 30, type: 'boolean', sortable: false, filter: {searchFieldName: 'testDefinition'}, hidden: true}
	],
	reprocessIntegrationFiles: function(integrationFiles, count) {
		const gridPanel = this;
		const grid = this.grid;
		const integrationFile = integrationFiles[count];
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Reprocessing...',
			params: {integrationFileId: integrationFile.id},
			conf: {rowId: integrationFile.id},
			onLoad: function(record, conf) {
				count++;
				if (count === integrationFiles.length) { // refresh after all repos were transitioned
					gridPanel.reload();
				}
				else {
					gridPanel.reprocessIntegrationFiles(integrationFiles, count);
				}
			}
		});
		loader.load('integrationFileRerun.json');
	},
	editor: {
		detailPageClass: 'Clifton.integration.definition.ImportRunWindow',
		drillDownOnly: true,
		getDefaultData: function() {
			if (this.getGridPanel().importEventName) {
				return {importEventName: this.getGridPanel().importEventName};
			}
		},
		addEditButtons: function(t, gridPanel) {
			const button = t.add({
				text: 'Reprocess Runs',
				tooltip: 'Re-run the entire import processing for the selected runs.',
				iconCls: 'run',
				scope: this,
				handler: function() {
					const gridPanel = this.getGridPanel();
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be reprocess.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Reprocess Selected Runs', 'Would you like to re-process these selected files?', function(a) {
							if (a === 'yes') {
								const ut = sm.getSelections();
								// sort repos by tradeDate and then averageUnitPrice: proper booking order
								const integrationFiles = [];
								let addedIntegrationFiles = '';
								for (let i = 0; i < ut.length; i++) {
									const integrationRun = ut[i].json;
									if (integrationFiles.indexOf(integrationRun.file.id)) {
										addedIntegrationFiles += integrationRun.file.id;
										integrationFiles.push(integrationRun.file);
									}
								}
								const count = 0;
								gridPanel.reprocessIntegrationFiles(integrationFiles, count);
							}
						});
					}
				}
			});
			t.add('-');
			t.add({
				text: 'Upload',
				xtype: 'splitbutton',
				iconCls: 'import',
				scope: gridPanel,
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Broker/Manager Import',
							tooltip: 'Manually upload a broker or manager file (same as drag and drop)',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								TCG.createComponent('Clifton.integration.definition.IntegrationFileUploadWindow');
							}
						}, {
							text: 'File Revision',
							tooltip: 'Upload a file revision (e.g., statement revisions).  This will bypass processing and archive the file with the following name format: [effective date].[account number].[file extension]',
							iconCls: 'import',
							scope: gridPanel,
							handler: function() {
								TCG.createComponent('Clifton.integration.definition.IntegrationFileRevisionUploadWindow');
							}
						}
					]
				})
			});
			t.add('-');
			t.add({
				text: 'Fix Runs',
				tooltip: 'Set selected runs to ERROR status',
				iconCls: 'tools',
				scope: this,
				handler: function() {
					const grid = this.grid;
					const rows = grid.getSelectionModel().getSelections();
					if (rows.length === 0) {
						TCG.showError('Please select a run to fix.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Fix Selected Runs', 'Would you like to set the selected runs to ERROR status?', function(a) {
							if (a === 'yes') {
								const ids = [];
								for (let i = 0; i < rows.length; i++) {
									ids.push(rows[i].data.id);
								}

								const loader = new TCG.data.JsonLoader({
									waitTarget: grid.ownerCt,
									waitMsg: 'Fixing...',
									params: {runIds: ids},
									onLoad: function(record, conf) {
										grid.ownerCt.reload();
									},
									onFailure: function() {
										grid.ownerCt.reload();
									}
								});
								loader.load('integrationImportRunFix.json');
							}
						});
					}
				}
			});
			t.add('-');

			TCG.file.enableDD(TCG.getParentByClass(button, Ext.Panel), null, gridPanel, 'integrationFileUpload.json', true);
		}
	}
});
Ext.reg('integration-importRunGrid', Clifton.integration.definition.IntegrationImportRunGrid);


Clifton.integration.definition.IntegrationFileUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Upload (Broker/Manager Import)',
	iconCls: 'import',
	height: 150,
	modal: true,
	enableShowInfo: false,
	hideApplyButton: true,
	width: 500,
	items: [{
		xtype: 'formpanel',
		fileUpload: true,
		labelWidth: 80,
		instructions: 'Browse for the external file, and click OK to upload.',
		items: [
			{fieldLabel: 'External File', name: 'file', xtype: 'fileuploadfield', allowBlank: false}
		],

		getSaveURL: function() {
			return 'integrationFileUpload.json';
		}
	}]
});


Clifton.integration.definition.IntegrationFileRevisionUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Upload (File Revision)',
	iconCls: 'import',
	modal: true,
	enableShowInfo: false,
	hideApplyButton: true,
	height: 250,
	width: 500,
	items: [{
		xtype: 'formpanel',
		fileUpload: true,
		labelWidth: 100,
		instructions: 'Please populate all fields, and click OK to upload.',
		items: [
			{fieldLabel: 'External File', name: 'file', xtype: 'fileuploadfield'},
			{fieldLabel: 'File Definition', name: 'id', xtype: 'combo', url: 'integrationFileDefinitionListFind.json', displayField: 'fileName', hiddenName: 'fileDefinitionId', queryParam: 'fileName', detailPageClass: 'Clifton.integration.file.FileDefinitionWindow'},
			{fieldLabel: 'Holding Account', name: 'number', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=false', displayField: 'label', valueField: 'number', hiddenName: 'holdingAccountNumber', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Effective Date', xtype: 'datefield', name: 'effectiveDate'},
			{fieldLabel: 'File Password', name: 'password'}
		],

		getSaveURL: function() {
			return 'integrationFileRevisionUpload.json';
		}
	}]
});


Clifton.integration.definition.IntegrationImportDefinitionGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationImportDefinitionListFind',
	definitionTypeName: undefined, // optionally restrict to this definition type
	topToolbarSearchParameter: 'searchPattern',
	instructions: 'Import Definitions describe what transformations and what file we use to import external data.  ' +
			'If the definition name is "UBSAccountBalancesImport," then the transformation names will be "UBSAccountBalancesImport" and "UBSAccountBalancesImportToFinal."',
	columns: [
		{header: 'Definition Name', dataIndex: 'name'},
		{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
		{header: 'Import Type', dataIndex: 'type.name', width: 60, filter: {searchFieldName: 'typeId', type: 'combo', url: 'integrationImportDefinitionTypeListFind.json'}},
		{header: 'File Definition', dataIndex: 'fileDefinition.fileName', width: 80},
		{header: 'Raise External Event', dataIndex: 'raiseExternalEvent', type: 'boolean', width: 35, tooltip: 'If checked, the import will trigger a load-event in IMS'},
		{header: 'Disabled', dataIndex: 'disabled', type: 'boolean', width: 25},
		{header: 'One Per Day', dataIndex: 'fileDefinition.onePerDay', type: 'boolean', width: 30},
		{header: 'Test', dataIndex: 'testDefinition', type: 'boolean', width: 30, hidden: true, tooltip: 'Test definitions are used for testing purposes only'},
		{header: 'Order', dataIndex: 'order', width: 40, type: 'int', hidden: true},
		{header: 'Data Source', dataIndex: 'dataSourceName', width: 40, hidden: true},
		{header: 'Test Definition', dataIndex: 'testDefinition', type: 'boolean', width: 40, hidden: true}
	],
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('testDefinition', false);
		}
		if (this.definitionTypeName) {
			if (firstLoad) {
				const cm = this.getColumnModel();
				cm.setHidden(cm.findColumnIndex('type.name'), true);
			}
			return {typeName: this.definitionTypeName};
		}
	},
	editor: {
		detailPageClass: 'Clifton.integration.definition.ImportDefinitionWindow'
	}
});
Ext.reg('integration-importDefinitionGrid', Clifton.integration.definition.IntegrationImportDefinitionGrid);


Clifton.integration.definition.IntegrationImportTypeGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationImportDefinitionTypeListFind',
	instructions: 'Import type defines the type of data received from external sources. Each type usually maps to a destination table in the integration system that stores normalized source data for that type. Corresponding Event can be raised to notify the Main System that new data is available for consumption.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'Type Name', dataIndex: 'name'},
		{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
		{header: 'Destination Table', dataIndex: 'destinationTableName', tooltip: 'The final table in Integration that this import will eventually populate'},
		{header: 'Event Name', dataIndex: 'eventName', width: 50}
	],
	editor: {
		detailPageClass: 'Clifton.integration.definition.ImportTypeWindow',
		drillDownOnly: true
	}
});
Ext.reg('integration-importTypeGrid', Clifton.integration.definition.IntegrationImportTypeGrid);

Clifton.integration.transformation.IntegrationTransformationGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationTransformationListFind',
	topToolbarSearchParameter: 'searchPattern',
	instructions: 'Transformation defines an ETL file or a set of files used to load data from incoming source files to destination table.',
	wikiPage: 'IT/Pentaho+Data+Transformations',
	columns: [
		{header: 'Transformation Name', dataIndex: 'name', width: 100, defaultSortColumn: true},
		{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
		{header: 'Transformation Type', dataIndex: 'type', width: 50},
		{header: 'Import Type', dataIndex: 'definitionType.name', width: 60, filter: {searchFieldName: 'definitionTypeName'}},
		{header: 'Template', dataIndex: 'templateTransformation', type: 'boolean', width: 30},
		{header: 'System Defined', dataIndex: 'systemDefined', type: 'boolean', width: 30}
	],
	editor: {
		detailPageClass: 'Clifton.integration.transformation.TransformationWindow'
	}
});
Ext.reg('integration-transformationGrid', Clifton.integration.transformation.IntegrationTransformationGrid);

Clifton.integration.transformation.IntegrationTransformationFileGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationTransformationFileListFind',
	topToolbarSearchParameter: 'searchPattern',
	wikiPage: 'IT/How+to+Create+or+Update+a+Data+Transformation',
	instructions: 'One or more ETL transformation files defined here are applied in the specified order to corresponding import definition source files.',
	columns: [
		{header: 'Transformation', dataIndex: 'transformation.name', filter: {searchFieldName: 'transformationId', type: 'combo', url: 'integrationTransformationListFind.json'}},
		{header: 'File Name', dataIndex: 'fileName', defaultSortColumn: true},
		{
			header: 'Type',
			width: 30,
			dataIndex: 'fileType',
			filter: {
				type: 'list', options: [
					['EFFECTIVE_DATE_TRANSFORMATION', 'EFFECTIVE_DATE_TRANSFORMATION'],
					['DATA_TRANSFORMATION', 'DATA_TRANSFORMATION'],
					['DATA_FILE', 'DATA_FILE']]
			}
		},
		{header: 'Order', dataIndex: 'order', width: 40, type: 'int'},
		{header: 'Runnable', dataIndex: 'runnable', type: 'boolean', width: 30},
		{header: 'Deploy', dataIndex: 'updateRequired', type: 'boolean', width: 30}
	],
	editor: {
		detailPageClass: 'Clifton.integration.transformation.TransformationFileWindow'
	}
});
Ext.reg('integration-transformationFileGrid', Clifton.integration.transformation.IntegrationTransformationFileGrid);

Clifton.integration.definition.IntegrationSourceGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationSourceListFind',
	instructions: 'Sources describe where a particular file comes from and which folders it is associated with.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'Source Name', dataIndex: 'name', width: 60},
		{header: 'Description', dataIndex: 'description', width: 200, hidden: true},
		{header: 'Type', dataIndex: 'type.name', width: 30, filter: {searchFieldName: 'typeName'}},
		{header: 'Source Company ID', dataIndex: 'sourceCompanyId', width: 35, type: 'int', useNull: true},
		{header: 'Archive Strategy', dataIndex: 'fileArchiveStrategy.name', width: 70, filter: {searchFieldName: 'fileArchiveStrategyName'}},
		{header: 'Additional Archive Path', dataIndex: 'additionalArchivePath', width: 40},
		{header: 'FTP Root Folder', dataIndex: 'ftpRootFolder', width: 35},
		{header: 'FTP Watching Enabled', dataIndex: 'ftpWatchingEnabled', type: 'boolean', width: 40},
		{header: 'Input Folder Watching Enabled', dataIndex: 'sourceInputFolderWatchingEnabled', type: 'boolean', width: 40},
		{header: 'Force Decryption', dataIndex: 'forceDecryption', type: 'boolean', width: 30, hidden: true}
	],
	editor: {
		detailPageClass: 'Clifton.integration.source.SourceWindow'
	}
});
Ext.reg('integration-sourceGrid', Clifton.integration.definition.IntegrationSourceGrid);


Clifton.integration.file.IntegrationFileDefinitionGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationFileDefinitionListFind',
	instructions: 'Each File Definition represents a specific file format received from external source. The same file can be used by one or more Import Definitions.',
	columns: [
		{header: 'File Type', dataIndex: 'type.name', filter: {searchFieldName: 'typeName'}, width: 40},
		{header: 'File Name', dataIndex: 'fileName'},
		{header: 'Source Table List', dataIndex: 'sourceTableNameWithSchemaList', width: 100, hidden: true},
		{header: 'Source', dataIndex: 'source.name', width: 50, filter: {searchFieldName: 'sourceName'}},
		{header: 'File Pattern', dataIndex: 'fileNamePattern', width: 50, hidden: true},
		{header: 'Date Pattern', dataIndex: 'effectiveDatePattern', width: 50, hidden: true},
		{header: 'Date Format', dataIndex: 'effectiveDateFormat', width: 50, hidden: true},
		{header: 'External File Name', dataIndex: 'externalFileName', hidden: true},
		{header: 'Time Zone Name', dataIndex: 'timeZoneName', hidden: true, width: 50},
		{
			header: 'Encryption Type', dataIndex: 'encryptionType', width: 50, hidden: true,
			filter: {
				type: 'list', options: [
					['PGP', 'PGP'],
					['DEFAULT', 'DEFAULT']]
			}
		},
		{header: 'Override Archive Strategy', dataIndex: 'overrideFileArchiveStrategy.name', filter: {searchFieldName: 'overrideFileArchiveStrategyName'}, width: 50, hidden: true},
		{header: 'Additional Archive Path', dataIndex: 'additionalArchivePath', width: 60, hidden: true},
		{header: 'Archive Duplicates', dataIndex: 'archiveDuplicates', type: 'boolean', width: 45},
		{header: 'One Per Day', dataIndex: 'onePerDay', type: 'boolean', width: 40, tooltip: 'Checked if the file is sent only once per day'},
		{header: 'File Not Expected Daily', dataIndex: 'fileNotExpectedDaily', type: 'boolean', width: 40, tooltip: 'Indicates whether a file should NOT be expected on a daily basis'},
		{header: 'Received By', dataIndex: 'receivedByTime', width: 30, filter: false}
	],
	editor: {
		detailPageClass: 'Clifton.integration.file.FileDefinitionWindow'//,
		//drillDownOnly: true
	}
});
Ext.reg('integration-fileDefinitionGrid', Clifton.integration.file.IntegrationFileDefinitionGrid);


Clifton.integration.file.IntegrationFileGrid = Ext.extend(TCG.grid.GridPanel, {
	rowSelectionModel: 'checkbox',
	name: 'integrationFileListFind',
	instructions: 'A list of individual files that we have received from external data providers. Most files are processed by corresponding import run(s).',
	columns: [
		{header: 'File Type', dataIndex: 'fileDefinition.type.name', filter: {searchFieldName: 'typeName'}, width: 35},
		{header: 'File Name', dataIndex: 'fileDefinition.fileName', width: 50, filter: {searchFieldName: 'fileDefinitionName'}},
		{header: 'External File Name', dataIndex: 'fileDefinition.externalFileName', width: 60, hidden: true, filter: {searchFieldName: 'externalFileName'}},
		{
			header: 'File Source', dataIndex: 'fileSourceType', width: 25,
			filter: {
				type: 'list',
				options: [
					['FTP', 'FTP/FTPS'],
					['SFTP', 'SFTP'],
					['MANUAL', 'MANUAL']
				]
			}
		},
		{header: 'Import Source', dataIndex: 'source.name', width: 45, filter: {searchFieldName: 'sourceName'}},
		{header: 'Path', dataIndex: 'fileNameWithPath', hidden: true},
		{header: 'Encrypted Path', dataIndex: 'encryptedFileNameWithPath', width: 60, hidden: true},
		{header: 'Received Date', dataIndex: 'receivedDate', width: 30, defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Effective Date', dataIndex: 'effectiveDate', width: 30, hidden: true},
		{
			header: 'Source Data Loaded', dataIndex: 'sourceDataLoaded', type: 'boolean', width: 30,
			tooltip: 'Indicates whether source data has been loaded.  Important when multiple transformations are ran against a single file'
		},
		{header: 'Deleted', dataIndex: 'deleted', type: 'boolean', width: 20},
		{header: 'Data Deleted', dataIndex: 'sourceDataDeleted', type: 'boolean', width: 20, hidden: true},
		{header: 'Error On Load', dataIndex: 'errorOnLoad', type: 'boolean', width: 25},
		{header: 'Error Message', dataIndex: 'error', hidden: true, sortable: false},
		{
			header: 'File Size', dataIndex: 'fileSize', width: 20, filter: false,
			renderer: function(value, metaData, record) {
				return TCG.fileSize(value);
			}
		},
		{
			header: 'Encrypted File Size', dataIndex: 'encryptedFileSize', width: 20, filter: false, hidden: true,
			renderer: function(value, metaData, record) {
				return TCG.fileSize(value);
			}
		}
	],
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('receivedDate', {'after': Clifton.calendar.getBusinessDayFrom(-2)});
		}
	},
	reprocessIntegrationFiles: function(integrationFiles, count) {
		const gridPanel = this;
		const grid = this.grid;
		const integrationFile = integrationFiles[count];
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Reprocessing...',
			params: {integrationFileId: integrationFile.id},
			conf: {rowId: integrationFile.id},
			onLoad: function(record, conf) {
				count++;
				if (count === integrationFiles.length) { // refresh after all repos were transitioned
					gridPanel.reload();
				}
				else {
					gridPanel.reprocessIntegrationFiles(integrationFiles, count);
				}
			}
		});
		loader.load('integrationFileRerun.json');
	},
	editor: {
		detailPageClass: 'Clifton.integration.file.FileWindow',
		drillDownOnly: true,
		addEditButtons: function(t, gridPanel) {
			const button = t.add({
				text: 'Reprocess Files',
				tooltip: 'Re-run the entire import processing with the file from this run.',
				iconCls: 'run',
				scope: this,
				handler: function() {
					const gridPanel = this.getGridPanel();
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be reprocess.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Reprocess Selected Files', 'Would you like to re-process these selected files?', function(a) {
							if (a === 'yes') {
								const ut = sm.getSelections();
								// sort repos by tradeDate and then averageUnitPrice: proper booking order
								const integrationFiles = [];
								let addedIntegrationFiles = '';
								for (let i = 0; i < ut.length; i++) {
									const integrationFile = ut[i].json;
									if (addedIntegrationFiles.indexOf(':' + integrationFile.id) < 0) {
										addedIntegrationFiles += ':' + integrationFile.id;
										integrationFiles.push(integrationFile);
									}
								}
								const count = 0;
								gridPanel.reprocessIntegrationFiles(integrationFiles, count);
							}
						});
					}
				}
			});
			t.add('-');
			t.add({
				text: 'Upload',
				xtype: 'splitbutton',
				iconCls: 'import',
				scope: gridPanel,
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Broker/Manager Import',
							tooltip: 'Manually upload a broker or manager file (same as drag and drop)',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								TCG.createComponent('Clifton.integration.definition.IntegrationFileUploadWindow');
							}
						}, {
							text: 'File Revision',
							tooltip: 'Upload a file revision (e.g., statement revisions).  This will bypass processing and archive the file with the following name format: [effective date].[account number].[file extension]',
							iconCls: 'import',
							scope: gridPanel,
							handler: function() {
								TCG.createComponent('Clifton.integration.definition.IntegrationFileRevisionUploadWindow');
							}
						}
					]
				})
			});
			t.add('-');

			TCG.file.enableDD(TCG.getParentByClass(button, Ext.Panel), null, gridPanel, 'integrationFileUpload.json', true);
		}
	}
});
Ext.reg('integration-fileGrid', Clifton.integration.file.IntegrationFileGrid);


Clifton.integration.manager.AssetClassGroupGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'integrationManagerAssetClassGroupListFind',
	xtype: 'gridpanel',
	instructions: 'Import group defines a unique custodian file format and includes mappings of custodian specific asset classes to our position types.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Group Name', width: 100, dataIndex: 'name'},
		{header: 'Business Company ID', width: 100, dataIndex: 'businessCompanyId', filter: false},
		{header: 'Description', width: 200, dataIndex: 'description'},
		{header: 'Unmapped Type', width: 80, dataIndex: 'unmappedPositionType.name'},
		{header: 'Require All', width: 50, dataIndex: 'requireAllAssetClassMappings', type: 'boolean'}
	],
	editor: {
		detailPageClass: 'Clifton.integration.manager.AssetClassGroupWindow'
	}
});
Ext.reg('integration-assetClassGroupGrid', Clifton.integration.manager.AssetClassGroupGrid);


Clifton.integration.outgoing.StatusOptions = [['RECEIVED', 'RECEIVED'], ['PROCESSED', 'PROCESSED'], ['REPROCESSED', 'REPROCESSED'], ['FAILED', 'FAILED']];

Clifton.integration.outgoing.renderStatus = function(v) {
	if (v === 'PROCESSING') {
		return 'PROCESSING';
	}
	if (v === 'FAILED') {
		return '<div class="amountNegative">' + v + '</div>';
	}
	if (v === 'RECEIVED') {
		return '<div class="amountPositive">' + v + '</div>';
	}
	if (v === 'REPROCESSED') {
		return '<div class="amountAdjusted">' + v + '</div>';
	}
	if (v === 'PROCESSED') {
		return '<div class="important">' + v + '</div>';
	}

	return v;
};

// Added here so that the integration transport encryption setup tab appears in the security setup window
Clifton.security.SecuritySetupWindowAdditionalTabs[Clifton.security.SecuritySetupWindowAdditionalTabs.length] = {
	title: 'Integration Transport Encryption',
	tbar: [
		{
			text: 'Setup Key',
			tooltip: 'Runs the setup process to retrieve the public key from Integration and store it in the specified key file.',
			iconCls: 'run',
			getWindow: function() {
				let result = this.findParentByType(Ext.Window);
				if (TCG.isNull(result)) {
					result = this.findParentBy(function(o) {
						return o.baseCls === 'x-window';
					});
				}
				return result;
			},
			handler: function() {
				const fp = TCG.getChildrenByClass(TCG.getParentByClass(this, Ext.Panel), TCG.form.FormPanel)[0];
				Ext.Msg.confirm('Confirm Setup Request', 'Are you sure you want to run the transport encryption setup process?', function(a) {
					if (a === 'yes') {
						const loader = new TCG.data.JsonLoader({
							waitTarget: fp,
							waitMsg: 'Running...',
							timeout: 60000,
							onLoad: function(record, conf) {
								fp.doLoad();
								Ext.Msg.alert('Completed', 'The public key has been retrieved from Integration and stored at the specified location. <br/><br/>NOTE: Please copy the checksum into the appropriate property file.');
							}
						});
						loader.load('integrationTransportEncryptionKeySetup.json');
					}
				});
			}
		}
	],
	items: [{
		xtype: 'formpanel',
		url: 'integrationTransportEncryptionKeyDetails.json',
		instructions: 'Displays detail about the transport encryption key needed to communicate with the Integration Server. Sensitive fields (password, etc.) are encrypted using these keys before they are sent to the Integration Server via JMS.',
		loadValidation: false,
		readOnly: true,
		labelWidth: 140,
		getLoadURL: function() {
			const win = this.getWindow();
			if (win.params && win.params.loadUrl) {
				return win.params.loadUrl;
			}
			return this.url;
		},
		items: [
			{fieldLabel: 'Public Key File Location', name: 'publicKeyFileLocation', qtip: 'The location of the public key file on the file system'},
			{fieldLabel: 'Public Key File Checksum', name: 'publicKeyFileChecksum', qtip: 'The checksum of the public key - this value should be copied into the appropriate properties file.'}
		]
	}]
};
