package com.clifton.integration.file;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.file.search.IntegrationFileDefinitionSearchForm;
import com.clifton.integration.file.search.IntegrationFileSearchForm;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionSearchForm;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.integration.source.IntegrationSourceService;
import com.clifton.integration.source.search.IntegrationSourceSearchForm;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>IntegrationFileServiceImpl</code> provides business methods for integration file classes.
 * <p>
 * NOTE: This class must be manually injected to set the root directory.
 *
 * @author mwacker
 */
@Service
public class IntegrationFileServiceImpl implements IntegrationFileService {

	private static final Map<String, Pattern> patternCache = new ConcurrentHashMap<>();

	private AdvancedUpdatableDAO<IntegrationFileDefinition, Criteria> integrationFileDefinitionDAO;
	private AdvancedUpdatableDAO<IntegrationFileDefinitionType, Criteria> integrationFileDefinitionTypeDAO;
	private AdvancedUpdatableDAO<IntegrationFile, Criteria> integrationFileDAO;

	private IntegrationSourceService integrationSourceService;
	private IntegrationImportService integrationImportService;

	private SecurityAuthorizationService securityAuthorizationService;

	private Map<String, String> rootArchiveDirectoryMap;
	private Map<IntegrationDirectory, String> integrationSystemFilePathMap;

	////////////////////////////////////////////////////////////////////////////
	////////      IntegrationFileDefinition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationFileDefinition getIntegrationFileDefinition(int id) {
		return getIntegrationFileDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationFileDefinition getIntegrationFileDefinitionByFileName(String fileName) {
		return getIntegrationFileDefinitionDAO().findOneByField("fileName", fileName);
	}


	@Override
	public IntegrationFileDefinition getIntegrationFileDefinitionByExternalFileName(final String externalFileName, final Integer importSourceId) {
		// TODO: pass in the FTP folder to find the source, then all the file definitions and use the pattern if no file is found
		HibernateSearchConfigurer config = criteria -> {
			criteria.add(Restrictions.like("externalFileName", externalFileName, MatchMode.START));

			if (importSourceId != null) {
				criteria.add(Restrictions.eq("source.id", importSourceId));
			}
		};
		return CollectionUtils.getOnlyElement(getIntegrationFileDefinitionDAO().findBySearchCriteria(config));
	}


	/**
	 * This needs to be merged with com.clifton.integration.file.archive.IntegrationFileArchiveServiceImpl#getIntegrationDefinitionForFile
	 */
	@Override
	public IntegrationFileDefinition getIntegrationDefinitionForFile(File input, Date effectiveDate, IntegrationSource integrationSource) {
		AssertUtils.assertNotNull(input, "The input integration file is required.");

		String originalFilename = IntegrationFileUtils.getOriginalFileName(input.getName(), effectiveDate != null);
		IntegrationFileDefinition definition = getIntegrationFileDefinitionByFileName(
				!StringUtils.isEmpty(originalFilename) ? originalFilename : input.getName());
		if (definition == null) {
			// no match on internal file name, so lookup using external file name.
			String searchFileName = !StringUtils.isEmpty(originalFilename) ? originalFilename : input.getName();

			// if the file is encrypted, remove the extension and set the parameter
			if (IntegrationFileUtils.isFileEncrypted(searchFileName)) {
				searchFileName = FileUtils.getFileNameWithoutExtension(searchFileName);
			}

			definition = getIntegrationFileDefinitionByExternalFileName(searchFileName, integrationSource != null ? integrationSource.getId() : null);
			// only use pattern matching when an import source is present, i.e. for FTP loading.
			if (definition == null && integrationSource != null) {
				definition = getFileDefinitionByPattern(input.getName(), effectiveDate, integrationSource);
			}
		}
		if (definition == null) {
			definition = getIntegrationFileDefinitionByFileName(IntegrationFileService.UNKNOWN_FILE_DEFINITION_FILE_NAME);
		}
		return definition;
	}


	@Override
	public IntegrationFileDefinition getFileDefinitionByPattern(String inputFileName, Date effectiveDate, IntegrationSource integrationSource) {
		IntegrationFileDefinition definition = null;
		if (!StringUtils.isEmpty(inputFileName) && integrationSource != null) {
			// use effective date format from definition(s)
			definition = getFileDefinitionByEffectiveDateFormat(inputFileName, effectiveDate, integrationSource);
			if (definition == null) {
				// use file name pattern from definition(s)
				IntegrationFileDefinitionSearchForm searchForm = new IntegrationFileDefinitionSearchForm();
				searchForm.setSourceId(integrationSource.getId());
				searchForm.setFileNamePatternNotNull(true);
				List<IntegrationFileDefinition> definitionList = getIntegrationFileDefinitionList(searchForm);

				for (IntegrationFileDefinition searchDefinition : CollectionUtils.getIterable(definitionList)) {
					String fileNamePattern = searchDefinition.getFileNamePattern();
					if (!StringUtils.isEmpty(fileNamePattern)) {
						Pattern p = patternCache.computeIfAbsent(fileNamePattern, Pattern::compile);
						Matcher m = p.matcher(inputFileName);
						if (m.find()) {
							definition = searchDefinition;
							break;
						}
					}
				}
			}
		}
		return definition;
	}


	private IntegrationFileDefinition getFileDefinitionByEffectiveDateFormat(String inputFileName, Date effectiveDate, IntegrationSource integrationSource) {
		IntegrationFileDefinition definition = null;
		if (!StringUtils.isEmpty(inputFileName) && effectiveDate != null && integrationSource != null) {
			IntegrationFileDefinitionSearchForm searchForm = new IntegrationFileDefinitionSearchForm();
			searchForm.setSourceId(integrationSource.getId());
			searchForm.setEffectiveDateFormatNotNull(true);
			List<IntegrationFileDefinition> definitionList = getIntegrationFileDefinitionList(searchForm);
			for (IntegrationFileDefinition fileDefinition : CollectionUtils.getIterable(definitionList)) {
				String originalFileName = IntegrationFileUtils.getOriginalFileName(inputFileName, fileDefinition.getEffectiveDateFormat(), effectiveDate);
				if (!StringUtils.isEmpty(originalFileName)) {
					definition = getIntegrationFileDefinitionByFileName(originalFileName);
				}
				if (definition != null) {
					break;
				}
			}
		}
		return definition;
	}


	@Override
	public List<IntegrationFileDefinition> getIntegrationFileDefinitionList(final IntegrationFileDefinitionSearchForm searchForm) {

		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getUsed() != null) {
					DetachedCriteria subSelect = DetachedCriteria.forClass(IntegrationImportDefinition.class, "importDefinition");
					subSelect.setProjection(Projections.id());
					subSelect.add(Restrictions.eqProperty("importDefinition.fileDefinition.id", criteria.getAlias() + ".id"));

					if (searchForm.getUsed()) {
						criteria.add(Subqueries.exists(subSelect));
					}
					else {
						criteria.add(Subqueries.notExists(subSelect));
					}
				}
				if (searchForm.getFileArchiveStrategyId() != null) {
					String sourceAlias = getPathAlias("source", criteria);
					Criterion sourceCriterion = Restrictions.and(Restrictions.eq(sourceAlias + ".fileArchiveStrategy.id", searchForm.getFileArchiveStrategyId()),
							Restrictions.isNull("overrideFileArchiveStrategy.id"));
					criteria.add(Restrictions.or(sourceCriterion, Restrictions.eq("overrideFileArchiveStrategy.id", searchForm.getFileArchiveStrategyId())));
				}
			}
		};

		return getIntegrationFileDefinitionDAO().findBySearchCriteria(config);
	}


	@Override
	public IntegrationFileDefinition saveIntegrationFileDefinition(IntegrationFileDefinition bean) {
		if (!bean.isNewBean()) {
			IntegrationImportDefinitionSearchForm definitionSearchForm = new IntegrationImportDefinitionSearchForm();
			definitionSearchForm.setTestDefinition(true);
			definitionSearchForm.setFileDefinitionId(bean.getId());
			definitionSearchForm.setLimit(1);
			if (!CollectionUtils.isEmpty(getIntegrationImportService().getIntegrationImportDefinitionList(definitionSearchForm))) {
				ValidationUtils.assertTrue(getSecurityAuthorizationService().isSecurityUserAdmin(), "Only Administrators may make changes to File Definitions used by Test Import Definitions");
			}
			if (!StringUtils.isEmpty(bean.getFileNamePattern())) {
				try {
					Pattern.compile(bean.getFileNamePattern());
				}
				catch (Exception e) {
					throw new ValidationException("File Name Pattern is invalid", e);
				}
			}
		}
		return getIntegrationFileDefinitionDAO().save(bean);
	}


	@Override
	public void deleteIntegrationFileDefinition(int id) {
		getIntegrationFileDefinitionDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////    IntegrationFileDefinitionType Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationFileDefinitionType getIntegrationFileDefinitionType(int id) {
		return getIntegrationFileDefinitionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationFileDefinitionType> getIntegrationFileDefinitionTypeList() {
		return getIntegrationFileDefinitionTypeDAO().findAll();
	}

	////////////////////////////////////////////////////////////////////////////
	////////          IntegrationFile Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationFile getIntegrationFile(int id) {
		return getIntegrationFileDAO().findByPrimaryKey(id);
	}


	@Override
	public FileWrapper downloadIntegrationFile(int id) {
		IntegrationFile integrationFile = getIntegrationFile(id);
		File file = new File(getIntegrationFileAbsolutePath(integrationFile));
		String fileName = file.getName();
		if (integrationFile.getFileDefinition() != null && integrationFile.getFileDefinition().getType().isDefinitionFileNameUsedForDownload()) {
			Date fileTimeStamp = IntegrationFileUtils.getTimeStamp(file.getName());
			fileName = IntegrationFileUtils.appendTimeStamp(integrationFile.getFileDefinition().getFileName(), fileTimeStamp);
		}
		return new FileWrapper(file, fileName, false);
	}


	@Override
	public String getIntegrationFileAbsolutePath(IntegrationDirectory dir, String fileName) {
		return FileUtils.combinePath(getIntegrationSystemFilePathMap().get(dir), fileName);
	}


	@Override
	public String getIntegrationFileAbsolutePath(IntegrationFile bean) {
		IntegrationSource source = getIntegrationFileSource(bean, true);
		return FileUtils.combinePath(getRootArchiveDirectoryMap().get(source.getType().getName()), bean.getFileNameWithPath());
	}


	@Override
	public String getIntegrationFileAbsoluteEncryptedPath(IntegrationFile bean) {
		IntegrationSource source = getIntegrationFileSource(bean, true);
		return FileUtils.combinePath(getRootArchiveDirectoryMap().get(source.getType().getName()), bean.getEncryptedFileNameWithPath());
	}


	@Override
	public IntegrationSource getIntegrationFileSource(IntegrationFile file, boolean exceptionIfNull) {
		IntegrationSource integrationSource = file.getReceivedFromSource();

		if (integrationSource == null) {
			integrationSource = file.getFileDefinition().getSource();
		}

		if (integrationSource == null && exceptionIfNull) {
			throw new ValidationException("IntegrationFile must have a 'receivedFromSource' set, or a FileDefinition set.");
		}

		return integrationSource;
	}


	@Override
	public List<IntegrationFile> getIntegrationFileList(final IntegrationFileSearchForm searchForm) {

		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getKnown() != null) {
					String fd = getPathAlias("fileDefinition", criteria);
					if (searchForm.getKnown()) {
						criteria.add(Restrictions.ne(fd + ".fileName", "UNKNOWN"));
					}
					else {
						criteria.add(Restrictions.eq(fd + ".fileName", "UNKNOWN"));
					}
				}
				if (!StringUtils.isEmpty(searchForm.getSourceName())) {
					String sourceAlias = getPathAlias("fileDefinition.source", criteria);
					String alias = getPathAlias("receivedFromSource", criteria, JoinType.LEFT_OUTER_JOIN);
					criteria.add(Restrictions.or(Restrictions.and(Restrictions.isNotNull(alias + ".id"), Restrictions.like(alias + ".name", "%" + searchForm.getSourceName() + "%")),
							Restrictions.and(Restrictions.isNull(alias + ".id"), Restrictions.like(sourceAlias + ".name", "%" + searchForm.getSourceName() + "%"))));
				}
				if (searchForm.getSourceId() != null) {
					String sourceAlias = getPathAlias("fileDefinition.source", criteria);
					String alias = getPathAlias("receivedFromSource", criteria, JoinType.LEFT_OUTER_JOIN);
					criteria.add(Restrictions.or(Restrictions.and(Restrictions.isNotNull(alias + ".id"), Restrictions.eq(alias + ".id", searchForm.getSourceId())),
							Restrictions.and(Restrictions.isNull(alias + ".id"), Restrictions.like(sourceAlias + ".id", searchForm.getSourceId()))));
				}
			}


			@Override
			protected String getOrderByFieldName(OrderByField field, Criteria criteria) {
				if ("sourceName".equals(field.getName())) {
					return getPathAlias("receivedFromSource", criteria, JoinType.LEFT_OUTER_JOIN) + ".name";
				}
				return super.getOrderByFieldName(field, criteria);
			}
		};

		return getIntegrationFileDAO().findBySearchCriteria(config);
	}


	@Override
	public IntegrationFile saveIntegrationFile(IntegrationFile bean) {
		if (bean.getFileDefinition() == null) {
			ValidationUtils.assertTrue(bean.getReceivedFromSource() != null && !bean.getReceivedFromSource().getType().isFileDefinitionRequired(),
					"File definition can only be null when the receivedFromSource type does not require a definition.");
		}
		return getIntegrationFileDAO().save(bean);
	}


	@Override
	public void deleteIntegrationFile(int id) {
		getIntegrationFileDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                  SystemFile Methods                   /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of files within the given directory, as mapped in the integrationSystemFilePathMap (defined in Spring context).
	 * This method traverses sub folders recursively, gathering files at all levels of the tree, directories included.
	 */
	@Override
	public List<IntegrationSystemFile> getIntegrationSystemFileList(IntegrationDirectory directory) {
		FilePath viewingDirectory = FilePath.forPath(getIntegrationSystemFilePathMap().get(directory));
		boolean ftpFolder = (directory == IntegrationDirectory.FTP || directory == IntegrationDirectory.SFTP);

		List<IntegrationSystemFile> systemFiles;
		Map<FilePath, BasicFileAttributes> fileMap;
		try {
			if (ftpFolder) {
				List<String> ftpFolderNames = getFtpFolderNames(true);
				fileMap = FileUtils.getFilesFromDirectory(viewingDirectory, true, ftpFolderNames);
			}
			else {
				fileMap = FileUtils.getFilesFromDirectory(viewingDirectory, true);
			}

			systemFiles = this.convertPathAttributeMapToSystemFileList(fileMap, directory);
		}
		catch (IOException e) {
			throw new RuntimeException("Problem retrieving file list: " + e.getMessage(), e);
		}

		return systemFiles;
	}


	@Override
	public void deleteIntegrationSystemFile(String id) {
		// Parses and validates the id
		String fullPath = parseSystemFileId(id);

		File fileToDelete = new File(fullPath);

		ValidationUtils.assertFalse(fileToDelete.isDirectory(), "Deleting folders is not allowed");
		ValidationUtils.assertFalse(fileToDelete.isHidden(), "This delete operation is not supported");

		if (!fileToDelete.delete()) {
			throw new RuntimeException("Unknown error: The file could not be deleted");
		}
	}


	@Override
	public FileWrapper downloadIntegrationSystemFile(String id) {
		// Parses and validates the id
		String fullPath = parseSystemFileId(id);

		File downloadFile = new File(fullPath);

		ValidationUtils.assertFalse(downloadFile.isDirectory(), "Downloading folders is not allowed");
		ValidationUtils.assertFalse(downloadFile.isHidden(), "This download operation is not supported");

		return new FileWrapper(downloadFile, downloadFile.getName(), false);
	}


	/**
	 * Parses and validates the id of an IntegrationSystemFile.
	 *
	 * @param id A String in the format [IntegrationDirectory]:[filePath] (e.g., "ERRORS:JPMorgan/JPM_CWD_Trade_Confirmations_BBG.csv).
	 * @return The absolute path of the systemFile indicated by the id.  Proper errors are thrown if there is no correlated integration directory in the
	 * integrationSystemFilePathMap
	 */
	private String parseSystemFileId(String id) {
		ValidationUtils.assertNotNull(id, "Invalid system file id [" + id + "]");

		int delimiterIndex = id.indexOf(":");
		ValidationUtils.assertFalse(delimiterIndex == -1, "Invalid system file id [" + id + "]");

		String path = id.substring(delimiterIndex + 1);
		String directory = id.substring(0, delimiterIndex);
		IntegrationDirectory integrationDirectory = IntegrationDirectory.valueOf(directory);
		ValidationUtils.assertNotNull(integrationDirectory, "The specified Integration Directory is not valid");

		return getIntegrationFileAbsolutePath(integrationDirectory, path);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	private List<IntegrationSystemFile> convertPathAttributeMapToSystemFileList(Map<FilePath, BasicFileAttributes> map, IntegrationDirectory directory) {
		List<IntegrationSystemFile> files = new ArrayList<>();

		for (Map.Entry<FilePath, BasicFileAttributes> pathBasicFileAttributesEntry : map.entrySet()) {
			File file = (pathBasicFileAttributesEntry.getKey()).toFile();
			BasicFileAttributes attributes = pathBasicFileAttributesEntry.getValue();
			// normalize the root path, e.g., change "C:/integration/input" to "C:\\integration\\input"
			String rootPath = new File(getIntegrationSystemFilePathMap().get(directory)).getAbsolutePath();

			// slice the root path from the absolute path, with ending "/" or "\"
			String relativePath = StringUtils.substringAfterLast(file.getAbsolutePath(), rootPath + System.getProperty("file.separator"));

			IntegrationSystemFile systemFile = new IntegrationSystemFile(directory, relativePath);
			systemFile.setFileName(FileUtils.getFileName(file.getAbsolutePath()));
			systemFile.setModifiedDate(new Date(attributes.lastModifiedTime().toMillis()));
			systemFile.setSize(attributes.size());
			systemFile.setFolder(attributes.isDirectory());
			files.add(systemFile);
		}

		return files;
	}


	//This is called using SpEL from 'app-clifton-integration-incoming-sftp-import-context.xml'
	//It needs to be public; but does not need to be url mapped.
	@Override
	public List<String> getFtpFolderNamesWithRoot(String rootDirectory, boolean getAsLowercase) {
		rootDirectory = rootDirectory == null ? "" : rootDirectory;
		IntegrationSourceSearchForm searchForm = new IntegrationSourceSearchForm();
		searchForm.setFtpRootFolderNotNull(true);
		searchForm.setFtpWatchingEnabled(true);
		List<IntegrationSource> sourceList = getIntegrationSourceService().getIntegrationSourceList(searchForm);
		// get list of lower case ftp root folder names, and initialize field
		String ftpFolders = rootDirectory + BeanUtils.getPropertyValues(sourceList, "ftpRootFolder", ":" + rootDirectory);
		if (getAsLowercase) {
			return Arrays.asList(ftpFolders.toLowerCase().split(":"));
		}
		return Arrays.asList(ftpFolders.split(":"));
	}


	private List<String> getFtpFolderNames(boolean getAsLowercase) {
		return getFtpFolderNamesWithRoot(null, getAsLowercase);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////
	public AdvancedUpdatableDAO<IntegrationFile, Criteria> getIntegrationFileDAO() {
		return this.integrationFileDAO;
	}


	public void setIntegrationFileDAO(AdvancedUpdatableDAO<IntegrationFile, Criteria> integrationFileDAO) {
		this.integrationFileDAO = integrationFileDAO;
	}


	public AdvancedUpdatableDAO<IntegrationFileDefinitionType, Criteria> getIntegrationFileDefinitionTypeDAO() {
		return this.integrationFileDefinitionTypeDAO;
	}


	public void setIntegrationFileDefinitionTypeDAO(AdvancedUpdatableDAO<IntegrationFileDefinitionType, Criteria> integrationFileDefinitionTypeDAO) {
		this.integrationFileDefinitionTypeDAO = integrationFileDefinitionTypeDAO;
	}


	public AdvancedUpdatableDAO<IntegrationFileDefinition, Criteria> getIntegrationFileDefinitionDAO() {
		return this.integrationFileDefinitionDAO;
	}


	public void setIntegrationFileDefinitionDAO(AdvancedUpdatableDAO<IntegrationFileDefinition, Criteria> integrationFileDefinitionDAO) {
		this.integrationFileDefinitionDAO = integrationFileDefinitionDAO;
	}


	@Override
	public Map<String, String> getRootArchiveDirectoryMap() {
		return this.rootArchiveDirectoryMap;
	}


	public void setRootArchiveDirectoryMap(Map<String, String> rootArchiveDirectoryMap) {
		this.rootArchiveDirectoryMap = rootArchiveDirectoryMap;
	}


	@Override
	public Map<IntegrationDirectory, String> getIntegrationSystemFilePathMap() {
		return this.integrationSystemFilePathMap;
	}


	public void setIntegrationSystemFilePathMap(Map<IntegrationDirectory, String> systemFilePathMap) {
		this.integrationSystemFilePathMap = systemFilePathMap;
	}


	public IntegrationSourceService getIntegrationSourceService() {
		return this.integrationSourceService;
	}


	public void setIntegrationSourceService(IntegrationSourceService integrationSourceService) {
		this.integrationSourceService = integrationSourceService;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
