package com.clifton.integration.file.archive;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.PGPFileUtils;
import com.clifton.core.util.SecurityUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.file.archive.naming.SourceSystemDefinitionNameArchiveFilePathGenerator;
import com.clifton.integration.file.search.IntegrationFileDefinitionSearchForm;
import com.clifton.integration.file.search.IntegrationFileSearchForm;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.integration.source.IntegrationSourceService;
import com.clifton.security.secret.SecuritySecretService;
import org.hibernate.Criteria;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class IntegrationFileArchiveServiceImpl implements IntegrationFileArchiveService {

	private AdvancedUpdatableDAO<IntegrationFileArchiveStrategy, Criteria> integrationFileArchiveStrategyDAO;

	private IntegrationFileService integrationFileService;
	private IntegrationImportService integrationImportService;
	private IntegrationSourceService integrationSourceService;

	private SecuritySecretService securitySecretService;

	private JdbcTemplate jdbcTemplate;

	private String additionalArchiveDirectoryForEncryptedFile = "encrypted";


	////////////////////////////////////////////////////////////////////////////
	////////              Archiving Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getIntegrationFileRelativeArchiveFilePath(IntegrationFileArchiveParameters parameters) {
		if (parameters.getDefinition() == null) {
			IntegrationFileDefinition definition = getIntegrationFileService().getIntegrationFileDefinitionByFileName(
					!StringUtils.isEmpty(parameters.getOriginalFilename()) ? parameters.getOriginalFilename() : parameters.getFileToArchive().getName());
			if (definition == null) {
				definition = getIntegrationDefinitionForFile(parameters);
			}
			if (definition == null) {
				definition = getIntegrationFileService().getIntegrationFileDefinitionByFileName(IntegrationFileService.UNKNOWN_FILE_DEFINITION_FILE_NAME);
			}
			parameters.setDefinition(definition);
		}
		return getArchiveFilePath(parameters.getFileToArchive().getName(), parameters, false, null);
	}


	@Override
	public void decryptArchivedIntegrationFile(IntegrationFile bean) {
		IntegrationFileDefinition definition = bean.getFileDefinition();
		if (IntegrationFileUtils.isFileEncrypted(bean.getFileNameWithPath())) {
			File fileToDecrypt = new File(getIntegrationFileService().getIntegrationFileAbsolutePath(bean));
			IntegrationFileArchiveParameters parameters = new IntegrationFileArchiveParameters(fileToDecrypt, definition);
			parameters.setImportSource(bean.getReceivedFromSource());
			parameters.setEffectiveDate(bean.getEffectiveDate() != null ? bean.getEffectiveDate() : IntegrationFileUtils.getEffectiveDateFromFileName(fileToDecrypt.getName(),
					definition.getTimeZoneName(), definition.getEffectiveDatePattern(), definition.getEffectiveDateFormat()));

			IntegrationSource integrationSource = getIntegrationFileService().getIntegrationFileSource(bean, true);
			String rootArchiveDirectory = getRootArchiveDirectoryMap().get(integrationSource.getType().getName());
			doDecryptAndArchiveFileIntegrationFile(parameters, rootArchiveDirectory, bean);
			bean.setErrorOnLoad(false);
			getIntegrationFileService().saveIntegrationFile(bean);
		}
	}


	@Override
	@Transactional
	public void rearchiveExistingIntegrationFile(IntegrationFile bean) {
		// get the current files
		File archiveFile = new File(getIntegrationFileService().getIntegrationFileAbsolutePath(bean));
		File encryptedArchiveFile = new File(getIntegrationFileService().getIntegrationFileAbsoluteEncryptedPath(bean));

		Date effectiveDate = bean.getEffectiveDate() != null ? bean.getEffectiveDate() : IntegrationFileUtils.getEffectiveDateFromFileName(archiveFile.getName(), bean.getFileDefinition()
				.getTimeZoneName(), bean.getFileDefinition().getEffectiveDatePattern(), bean.getFileDefinition().getEffectiveDateFormat());
		String originalFileName = IntegrationFileUtils.getOriginalFileName(archiveFile.getName(), effectiveDate != null);

		IntegrationFileArchiveParameters parameters = new IntegrationFileArchiveParameters(archiveFile, originalFileName, bean.getFileSourceType(), effectiveDate);
		parameters.setImportSource(bean.getReceivedFromSource());

		// find the new definition
		IntegrationFileDefinition definition = getIntegrationDefinitionForFile(parameters);
		// if a new definition that is not UNKNOWN is found, the move the files
		if ((definition != null) && (!bean.getFileDefinition().getFileName().equals(definition.getFileName()))
				&& !IntegrationFileService.UNKNOWN_FILE_DEFINITION_FILE_NAME.equals(definition.getFileName())) {
			bean.setFileDefinition(definition);
			parameters.setDefinition(definition);

			// get the new paths
			Date timestamp = IntegrationFileUtils.getTimeStamp(archiveFile.getName());
			String newFilePath = getArchiveFilePath(IntegrationFileUtils.getOriginalFileName(archiveFile.getName(), false), parameters, false, timestamp);
			String newEncryptedFilePath = getArchiveFilePath(IntegrationFileUtils.getOriginalFileName(encryptedArchiveFile.getName(), false), parameters, true, timestamp);

			IntegrationSource integrationSource = getIntegrationFileService().getIntegrationFileSource(bean, true);
			String rootArchiveDirectory = getRootArchiveDirectoryMap().get(integrationSource.getType().getName());
			String newFullFilePath = FileUtils.combinePath(rootArchiveDirectory, newFilePath);
			String newFullEncryptedFilePath = FileUtils.combinePath(rootArchiveDirectory, newEncryptedFilePath);

			try {
				// move the main file
				FileUtils.copyFileCreatePath(archiveFile, newFullFilePath);
				bean.setFileNameWithPath(newFilePath);

				// if the file is already decrypted, just copy it otherwise decrypt it
				if (!parameters.isDecryptionRequired() && encryptedArchiveFile.exists()) {
					FileUtils.copyFileCreatePath(encryptedArchiveFile, newFullEncryptedFilePath);
					bean.setEncryptedFileNameWithPath(newEncryptedFilePath);
				}
				else {
					// this is a safe guard in case this method is called on a file that is not decrypted
					bean.setEncryptedFileNameWithPath(newFilePath);
					doDecryptAndArchiveFileIntegrationFile(parameters, rootArchiveDirectory, bean);
				}
				bean.setErrorOnLoad(false);
				getIntegrationFileService().saveIntegrationFile(bean);
			}
			catch (Throwable e) {
				// delete the file copies that were created
				File newFile = new File(newFullFilePath);
				if (newFile.exists()) {
					FileUtils.delete(newFile);
				}
				File newEncryptedFile = new File(newFullEncryptedFilePath);
				if (newEncryptedFile.exists()) {
					FileUtils.delete(newEncryptedFile);
				}

				throw new RuntimeException("Failed to move archived file from definition [" + bean.getFileDefinition().getFileName() + "] to [" + definition.getFileName() + "].", e);
			}
			// remove the old file if everything worked
			if (archiveFile.exists()) {
				FileUtils.delete(archiveFile);
			}
			if (encryptedArchiveFile.exists()) {
				FileUtils.delete(encryptedArchiveFile);
			}
		}
	}


	@Override
	@Transactional
	public IntegrationFile archiveIntegrationFile(IntegrationFileArchiveParameters parameters) {
		// get the definition
		if (parameters.getDefinition() == null
				&& (parameters.getImportSource() == null || parameters.getImportSource().getType().isFileDefinitionRequired())) {
			IntegrationFileDefinition definition = getIntegrationDefinitionForFile(parameters);
			parameters.setDefinition(definition);
		}

		// add the encrypted extension if needed
		parameters.setFileToArchive(getFileWithEncryptionExtension(parameters));

		// determine the effective date based on the date pattern specified on the definition
		if (parameters.getDefinition() != null && !StringUtils.isEmpty(parameters.getDefinition().getEffectiveDatePattern())) {
			Date effectiveDate = IntegrationFileUtils.getEffectiveDateFromFileName(parameters.getFileToArchive().getName(), parameters.getDefinition().getTimeZoneName(),
					parameters.getDefinition().getEffectiveDatePattern(), parameters.getDefinition().getEffectiveDateFormat());
			parameters.setEffectiveDate(effectiveDate);
		}

		// adjust the effective date if the definition has a time zone
		if (parameters.getDefinition() != null && !StringUtils.isEmpty(parameters.getDefinition().getTimeZoneName())) {
			Date effectiveDate = IntegrationFileUtils.getEffectiveDateFromFileName(parameters.getFileToArchive().getName(), parameters.getDefinition().getTimeZoneName(),
					parameters.getDefinition().getEffectiveDatePattern(), parameters.getDefinition().getEffectiveDateFormat());
			parameters.setEffectiveDate(effectiveDate);
		}

		IntegrationSource integrationSource = parameters.getImportSource();
		if (integrationSource == null && parameters.getDefinition() != null) {
			integrationSource = parameters.getDefinition().getSource();
		}

		if (integrationSource == null) {
			throw new ValidationException("Could not archive file because no import source was found");
		}
		String rootArchiveDirectory = getRootArchiveDirectoryMap().get(integrationSource.getType().getName());

		// copy/decrypt the file
		IntegrationFile result = null;
		try {
			if (parameters.isDecryptionRequired() && (parameters.getDefinition().getEncryptionType() != null)) {
				result = doArchiveFileIntegrationFile(parameters, rootArchiveDirectory);
				doDecryptAndArchiveFileIntegrationFile(parameters, rootArchiveDirectory, result);
			}
			else {
				result = doArchiveFileIntegrationFile(parameters, rootArchiveDirectory);
			}
		}
		catch (Exception e) {
			// if the file was partially archived, then set the error
			if (result != null) {
				result.setError(ExceptionUtils.getDetailedMessage(e));
				result.setErrorOnLoad(true);
				LogUtils.error(getClass(), "Failed to archive file.", e);
			}
			else {
				throw new RuntimeException("Failed to archive file [" + parameters.getFileToArchive().getAbsolutePath() + "].", e);
			}
		}

		result.setReceivedFromSource(parameters.getImportSource());
		if (parameters.getFileSourceType() != null) {
			result.setFileSourceType(parameters.getFileSourceType());
		}
		if (parameters.getEffectiveDate() != null && result.getFileDefinition() != null) {
			result.setEffectiveDate(result.getFileDefinition().isOnePerDay() ? DateUtils.clearTime(parameters.getEffectiveDate()) : parameters.getEffectiveDate());
		}
		getIntegrationFileService().saveIntegrationFile(result);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////    IntegrationFileArchiveStrategy Business Methods    /////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationFileArchiveStrategy getIntegrationFileArchiveStrategy(int id) {
		return getIntegrationFileArchiveStrategyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationFileArchiveStrategy> getIntegrationFileArchiveStrategyList(IntegrationFileArchiveStrategySearchForm searchForm) {
		return getIntegrationFileArchiveStrategyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationFileArchiveResult deleteIntegrationExpiredFiles(Date date) {
		IntegrationFileArchiveResult result = new IntegrationFileArchiveResult();
		List<IntegrationFileArchiveStrategy> strategyList = getIntegrationFileArchiveStrategyList(new IntegrationFileArchiveStrategySearchForm());
		doExecuteArchiveStrategyList(date, strategyList, result);
		doExecuteArchiveStrategyListData(date, strategyList, result);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////
	private Map<String, String> getRootArchiveDirectoryMap() {
		return getIntegrationFileService().getRootArchiveDirectoryMap();
	}


	private void doExecuteArchiveStrategyList(Date date, List<IntegrationFileArchiveStrategy> strategyList, IntegrationFileArchiveResult result) {
		for (IntegrationFileArchiveStrategy strategy : CollectionUtils.getIterable(strategyList)) {
			// delete the encrypted files
			result.setNumberOfEncryptedFilesDeleted(result.getNumberOfEncryptedFilesDeleted() + doExecuteArchiveStrategyFile(date, strategy, true));
			// delete the files
			result.setNumberOfFilesDeleted(result.getNumberOfFilesDeleted() + doExecuteArchiveStrategyFile(date, strategy, false));
		}
	}


	private int doExecuteArchiveStrategyFile(Date date, IntegrationFileArchiveStrategy strategy, boolean encryptedFiles) {
		int numberOfFilesDeleted = 0;
		int days = encryptedFiles ? strategy.getDaysToKeepEncryptedFile() : strategy.getDaysToKeepFile();
		// number of days set to 0 means no archiving
		if (days != 0) {
			List<IntegrationFile> archiveFileList = getIntegrationFiles(DateUtils.addDays(date, -days), strategy, encryptedFiles);
			for (IntegrationFile archiveFile : CollectionUtils.getIterable(archiveFileList)) {
				String fullPath = encryptedFiles ? getIntegrationFileService().getIntegrationFileAbsoluteEncryptedPath(archiveFile) : getIntegrationFileService().getIntegrationFileAbsolutePath(
						archiveFile);
				File file = new File(fullPath);
				if (file.exists()) {
					FileUtils.delete(file);
					numberOfFilesDeleted++;
					LogUtils.info(getClass(), "Deleting [" + fullPath + "] from the archive.");
				}
				if (!encryptedFiles) {
					archiveFile.setDeleted(true);
				}
				getIntegrationFileService().saveIntegrationFile(archiveFile);
			}
		}
		return numberOfFilesDeleted;
	}


	private void doExecuteArchiveStrategyListData(Date date, List<IntegrationFileArchiveStrategy> strategyList, IntegrationFileArchiveResult result) {
		for (IntegrationFileArchiveStrategy strategy : CollectionUtils.getIterable(strategyList)) {
			doExecuteArchiveStrategyData(date, strategy, result);
		}
	}


	private void doExecuteArchiveStrategyData(Date date, IntegrationFileArchiveStrategy strategy, IntegrationFileArchiveResult result) {
		int days = strategy.getDaysToKeepSourceData();
		// number of days set to 0 means no archiving
		if (days != 0) {
			Date archiveDataBefore = DateUtils.addDays(date, -days);
			List<IntegrationFileDefinition> definitionList = getIntegrationFileDefinitionList(strategy);
			for (IntegrationFileDefinition definition : CollectionUtils.getIterable(definitionList)) {
				IntegrationImportRunSearchForm searchForm = new IntegrationImportRunSearchForm();
				searchForm.setReceivedDateBefore(archiveDataBefore);
				searchForm.setFileDefinitionId(definition.getId());
				searchForm.setSourceDataDeleted(false);
				List<IntegrationImportRun> runList = getIntegrationImportService().getIntegrationImportRunList(searchForm);

				for (IntegrationImportRun run : CollectionUtils.getIterable(runList)) {
					result.setNumberOfSourceDataRowsDeleted(result.getNumberOfSourceDataRowsDeleted() + deleteSourceDataForRun(run));
					result.setNumberOfRunsCleared(result.getNumberOfRunsCleared() + 1);

					IntegrationFile file = run.getFile();
					file.setSourceDataDeleted(true);
					getIntegrationFileService().saveIntegrationFile(file);
				}
			}
		}
	}


	private int deleteSourceDataForRun(IntegrationImportRun run) {
		int rows = 0;
		if ((run.getFile().getFileDefinition() != null) && !StringUtils.isEmpty(run.getFile().getFileDefinition().getSourceTableNameWithSchemaList())) {
			String[] tables = run.getFile().getFileDefinition().getSourceTableNameWithSchemaList().split(",");
			for (final String table : tables) {
				if (!StringUtils.isEmpty(table)) {
					StringBuilder sql = new StringBuilder();
					sql.append("DELETE FROM ");
					sql.append(table);
					sql.append(" WHERE IntegrationImportRunID = ").append(run.getId());
					rows += getJdbcTemplate().update(sql.toString(), ps -> {
						try {
							ps.setQueryTimeout(5 * 60);
						}
						catch (Throwable e) {
							throw new RuntimeException("Failed to delete source data from table [" + table + "]", e);
						}
					});
				}
			}
		}
		return rows;
	}


	private List<IntegrationFileDefinition> getIntegrationFileDefinitionList(IntegrationFileArchiveStrategy strategy) {
		IntegrationFileDefinitionSearchForm searchForm = new IntegrationFileDefinitionSearchForm();
		searchForm.setFileArchiveStrategyId(strategy.getId());
		searchForm.setSourceTableNameWithSchemaListExists(true);
		return getIntegrationFileService().getIntegrationFileDefinitionList(searchForm);
	}


	private List<IntegrationFile> getIntegrationFiles(Date date, IntegrationFileArchiveStrategy strategy, boolean encrypted) {
		// get the file with this strategy as there override
		IntegrationFileSearchForm searchForm = new IntegrationFileSearchForm();
		searchForm.setReceivedDateBefore(date);
		searchForm.setOverrideFileArchiveStrategyId(strategy.getId());
		searchForm.setDeleted(false);
		if (encrypted) {
			searchForm.setEncryptedFileExists(true);
		}
		List<IntegrationFile> result = new ArrayList<>(getIntegrationFileService().getIntegrationFileList(searchForm));

		// get the file with this strategy and no override
		searchForm = new IntegrationFileSearchForm();
		searchForm.setReceivedDateBefore(date);
		searchForm.setIntegrationFileArchiveStrategyId(strategy.getId());
		searchForm.setOverrideArchiveStrategyDoesNotExist(true);
		searchForm.setDeleted(false);
		if (encrypted) {
			searchForm.setEncryptedFileExists(true);
		}
		result.addAll(getIntegrationFileService().getIntegrationFileList(searchForm));

		return result;
	}


	private File getFileWithEncryptionExtension(IntegrationFileArchiveParameters parameters) {
		if (parameters.getImportSource() != null && !IntegrationFileUtils.isFileEncrypted(parameters.getFileToArchive().getName()) && parameters.getImportSource().isForceDecryption()) {
			IntegrationFileDefinition definition = parameters.getDefinition();
			switch (definition.getEncryptionType()) {
				case PGP:
				case DEFAULT: {
					File newInput = new File(parameters.getFileToArchive().getAbsoluteFile() + ".pgp");
					parameters.getFileToArchive().renameTo(newInput);
					parameters.setDecryptionRequired(true);
					return newInput;
				}
			}
		}
		return parameters.getFileToArchive();
	}


	private IntegrationFileDefinition getIntegrationDefinitionForFile(IntegrationFileArchiveParameters parameters) {
		IntegrationFileDefinition definition = getIntegrationFileService().getIntegrationFileDefinitionByFileName(
				!StringUtils.isEmpty(parameters.getOriginalFilename()) ? parameters.getOriginalFilename() : parameters.getFileToArchive().getName());
		if (definition == null) {
			// no match on internal file name, so lookup using external file name.
			String searchFileName = !StringUtils.isEmpty(parameters.getOriginalFilename()) ? parameters.getOriginalFilename() : parameters.getFileToArchive().getName();

			// if the file is encrypted, remove the extension and set the parameter
			if (IntegrationFileUtils.isFileEncrypted(searchFileName)) {
				searchFileName = FileUtils.getFileNameWithoutExtension(searchFileName);
				parameters.setDecryptionRequired(true);
			}

			definition = getIntegrationFileService().getIntegrationFileDefinitionByExternalFileName(searchFileName, parameters.getImportSource() != null ? parameters.getImportSource().getId() : null);

			// only use pattern matching when an import source is present, i.e. for FTP loading.
			if (definition == null && parameters.getImportSource() != null) {
				definition = getIntegrationFileService().getFileDefinitionByPattern(parameters.getFileToArchive().getName(), parameters.getEffectiveDate(), parameters.getImportSource());
			}
		}
		if (definition == null) {
			definition = getIntegrationFileService().getIntegrationFileDefinitionByFileName(IntegrationFileService.UNKNOWN_FILE_DEFINITION_FILE_NAME);
		}
		return definition;
	}


	private IntegrationFile doArchiveFileIntegrationFile(IntegrationFileArchiveParameters parameters, String rootDirectory) throws Exception {
		File fileToArchive = parameters.getFileToArchive();
		long fileSize = fileToArchive.length();
		IntegrationFileDefinition definition = parameters.getDefinition();

		String filePath = moveFileToFinalArchiveLocation(parameters, rootDirectory);
		String encryptedFilePath = encryptArchivedFile(parameters, rootDirectory);

		IntegrationFile result = new IntegrationFile();
		result.setReceivedFromSource(parameters.getImportSource());
		result.setFileDefinition(definition);
		result.setFileNameWithPath(filePath);
		result.setEncryptedFileNameWithPath(encryptedFilePath);
		result.setFileSize(fileSize);
		result.setPassword(parameters.getFilePassword());
		if (parameters.isDecryptionRequired()) {
			result.setEncryptedFileNameWithPath(filePath);
			result.setEncryptedFileSize(fileSize);
		}
		result.setReceivedDate(new Date());
		return result;
	}


	private String encryptArchivedFile(IntegrationFileArchiveParameters parameters, String rootDirectory) {
		if (StringUtils.isEmpty(parameters.getPgpPublicKey())) {
			return null;
		}
		File fileToArchive = parameters.getFileToArchive();

		String filePath = getArchiveFilePath(fileToArchive.getName(), parameters, parameters.isDecryptionRequired(), null);
		String fullFilePath = FileUtils.combinePath(rootDirectory != null ? rootDirectory : "", filePath);

		String encryptedFilePath = FileUtils.combinePath(FileUtils.getFilePath(filePath), FileUtils.combinePath(getAdditionalArchiveDirectoryForEncryptedFile(), FileUtils.getFileName(filePath)));
		String fullEncryptedFilePath = FileUtils.combinePath(rootDirectory != null ? rootDirectory : "", encryptedFilePath);

		PGPFileUtils.encrypt(fullFilePath, fullEncryptedFilePath, parameters.getPgpPublicKey());
		return encryptedFilePath;
	}


	private String moveFileToFinalArchiveLocation(IntegrationFileArchiveParameters parameters, String rootDirectory) throws Exception {
		File fileToArchive = parameters.getFileToArchive();
		IntegrationFileDefinition definition = parameters.getDefinition();

		String archiveFileName;
		if (parameters.getDestinationFileNameOverride() != null) {
			archiveFileName = parameters.getDestinationFileNameOverride();
		}
		else {
			archiveFileName = fileToArchive.getName();
		}

		String filePath = getArchiveFilePath(archiveFileName, parameters, parameters.isDecryptionRequired(), null);
		String fullFilePath = FileUtils.combinePath(rootDirectory != null ? rootDirectory : "", filePath);


		if (!StringUtils.isEmpty(parameters.getFilePassword())) {
			SecurityUtils.secureFile(fileToArchive.getAbsolutePath(), fullFilePath, parameters.getFilePassword());

			if (parameters.isDeleteSourceFile()) {
				FileUtils.delete(fileToArchive);
			}
		}
		else {
			File newFile = new File(fullFilePath);
			if (parameters.isDeleteSourceFile()) {
				if (definition != null && !definition.isArchiveDuplicates() && newFile.exists()) {
					FileUtils.moveFileCreatePathAndOverwrite(fileToArchive, newFile);
				}
				else {
					FileUtils.moveFileCreatePath(fileToArchive, newFile);
				}
			}
			else {
				if (definition != null && !definition.isArchiveDuplicates() && newFile.exists()) {
					FileUtils.copyFileCreatePathAndOverwrite(fileToArchive, newFile);
				}
				else {
					FileUtils.copyFileCreatePath(fileToArchive, newFile);
				}
			}
		}

		return filePath;
	}


	// TODO: if we get more encryption types make this logic more configurable
	private void doDecryptAndArchiveFileIntegrationFile(IntegrationFileArchiveParameters parameters, String rootDirectory, IntegrationFile integrationFile) {
		//File fileToArchive = parameters.getFileToArchive();
		IntegrationFileDefinition definition = parameters.getDefinition();

		switch (definition.getEncryptionType()) {
			case PGP:
			case DEFAULT: {
				IntegrationSource source = parameters.getImportSource() != null ? parameters.getImportSource() : definition.getSource();

				String fullEncryptedFilePath = FileUtils.combinePath(rootDirectory != null ? rootDirectory : "", integrationFile.getEncryptedFileNameWithPath());
				Date originalTimeStamp = IntegrationFileUtils.getTimeStamp(fullEncryptedFilePath);

				String filePath = new File(IntegrationFileUtils.getOriginalFileName(fullEncryptedFilePath, false)).getName();
				filePath = getArchiveFilePath(filePath, parameters, false, null);
				filePath = FileUtils.getFileNameWithoutExtension(IntegrationFileUtils.getOriginalFileName(filePath, false));
				filePath = definition.isArchiveDuplicates() ? IntegrationFileUtils.appendTimeStamp(filePath, originalTimeStamp) : filePath;
				String fullFilePath = FileUtils.combinePath(rootDirectory != null ? rootDirectory : "", filePath);

				PGPFileUtils.decrypt(fullEncryptedFilePath,
						fullFilePath,
						getSecuritySecretService().decryptSecuritySecret(source.getDecryptionPrivateKeySecuritySecret()).getSecretString(),
						getSecuritySecretService().decryptSecuritySecret(source.getDecryptionPrivateKeyPasswordSecuritySecret()).getSecretString());

				integrationFile.setFileNameWithPath(filePath);
				integrationFile.setFileSize(new File(fullFilePath).length());
				break;
			}
			default:
				break;
		}
	}


	/**
	 * The resulting file name and path is:
	 * <p>
	 * [Strategy Archive Folder]\[Import Source Archive Path]\[File Definition Archive Path]\[Path created by path generator]\[Encrypted Folder (if
	 * encrypted)]\[File Name]
	 *
	 * @param newFileName
	 * @param parameters
	 * @param addEncryptedPath
	 * @param timestamp
	 */
	private String getArchiveFilePath(String newFileName, IntegrationFileArchiveParameters parameters, boolean addEncryptedPath, Date timestamp) {
		boolean archiveDuplicates = true;
		String definitionAdditionalArchivePath = null;

		String archivePath;
		String sourceFolder;
		IntegrationFileArchiveStrategy strategy = parameters.getArchiveStrategy();
		IntegrationFileDefinition definition = parameters.getDefinition();
		if (definition != null) {
			archiveDuplicates = definition.isArchiveDuplicates();
			definitionAdditionalArchivePath = definition.getAdditionalArchivePath();

			IntegrationSource receivedSource = parameters.getImportSource();
			strategy = definition.getOverrideFileArchiveStrategy() != null ?
					definition.getOverrideFileArchiveStrategy() : strategy != null ?
					strategy : definition.getSource().getFileArchiveStrategy();

			archivePath = strategy.getPathToArchiveFolder();

			if (IntegrationFileService.UNKNOWN_FILE_DEFINITION_FILE_NAME.equals(definition.getFileName()) && (receivedSource != null)
					&& !StringUtils.isEmpty(receivedSource.getAdditionalArchivePath())) {
				sourceFolder = receivedSource.getAdditionalArchivePath();
			}
			else {
				sourceFolder = definition.getSource().getAdditionalArchivePath() != null ? definition.getSource().getAdditionalArchivePath() : "";
			}
		}
		else {
			IntegrationSource source = parameters.getImportSource();
			strategy = strategy != null ?
					strategy : source.getFileArchiveStrategy();
			archivePath = strategy.getPathToArchiveFolder();
			sourceFolder = source.getAdditionalArchivePath() != null ? source.getAdditionalArchivePath() : "";
		}

		archivePath = FileUtils.combinePath(archivePath, sourceFolder);
		String filePath = FileUtils.combinePath(archivePath != null ? archivePath : "", !StringUtils.isEmpty(definitionAdditionalArchivePath) ? definitionAdditionalArchivePath : "");
		//Prepend the type if it is populated
		if (!StringUtils.isEmpty(parameters.getBaseArchivePath())) {
			filePath = FileUtils.combinePaths(parameters.getBaseArchivePath(), filePath);
		}
		String relativeFilePath = archiveDuplicates ? IntegrationFileUtils.appendTimeStamp(newFileName, timestamp) : newFileName;
		if (addEncryptedPath) {
			relativeFilePath = FileUtils.combinePath(getAdditionalArchiveDirectoryForEncryptedFile(), relativeFilePath);
		}

		Map<String, Object> pathGeneratorParamMap = new HashMap<>();
		if (parameters.getSourceSystemDefinitionName() != null) {
			pathGeneratorParamMap.put(SourceSystemDefinitionNameArchiveFilePathGenerator.SOURCE_SYSTEM_DEFINITION_NAME_PARAMETER, parameters.getSourceSystemDefinitionName());
		}

		if (parameters.getAdditionalProperties() != null) {
			pathGeneratorParamMap.putAll(parameters.getAdditionalProperties());
		}

		return strategy.getPathConvention().getPathGenerator().generateFullFileNameWithPath(filePath, relativeFilePath, parameters.getEffectiveDate(), pathGeneratorParamMap);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////
	public AdvancedUpdatableDAO<IntegrationFileArchiveStrategy, Criteria> getIntegrationFileArchiveStrategyDAO() {
		return this.integrationFileArchiveStrategyDAO;
	}


	public void setIntegrationFileArchiveStrategyDAO(AdvancedUpdatableDAO<IntegrationFileArchiveStrategy, Criteria> integrationFileArchiveStrategyDAO) {
		this.integrationFileArchiveStrategyDAO = integrationFileArchiveStrategyDAO;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}


	public String getAdditionalArchiveDirectoryForEncryptedFile() {
		return this.additionalArchiveDirectoryForEncryptedFile;
	}


	public void setAdditionalArchiveDirectoryForEncryptedFile(String additionalArchiveDirectoryForEncryptedFile) {
		this.additionalArchiveDirectoryForEncryptedFile = additionalArchiveDirectoryForEncryptedFile;
	}


	public IntegrationSourceService getIntegrationSourceService() {
		return this.integrationSourceService;
	}


	public void setIntegrationSourceService(IntegrationSourceService integrationSourceService) {
		this.integrationSourceService = integrationSourceService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
