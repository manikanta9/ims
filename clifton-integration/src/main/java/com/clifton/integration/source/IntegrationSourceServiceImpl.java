package com.clifton.integration.source;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.source.search.IntegrationSourceSearchForm;
import com.clifton.integration.source.search.IntegrationSourceTypeSearchForm;
import com.clifton.security.secret.SecuritySecretService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationSourceServiceImpl implements IntegrationSourceService {

	private AdvancedUpdatableDAO<IntegrationSource, Criteria> integrationSourceDAO;
	private AdvancedUpdatableDAO<IntegrationSourceType, Criteria> integrationSourceTypeDAO;

	private SecuritySecretService securitySecretService;


	////////////////////////////////////////////////////////////////////////////
	////////          IntegrationSource Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationSource getIntegrationSource(int id) {
		return this.getIntegrationSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationSource getIntegrationSourceByName(String name) {
		return getIntegrationSourceDAO().findOneByField("name", name);
	}


	@Override
	public IntegrationSource getIntegrationSourceByFtpRootFolder(String ftpRootFolder) {
		List<IntegrationSource> resultList = getIntegrationSourceDAO().findByField("ftpRootFolder", ftpRootFolder);
		if (resultList.size() != 1) {
			return null;
		}
		return CollectionUtils.getOnlyElement(resultList);
	}


	@Override
	public List<IntegrationSource> getIntegrationSourceList(final IntegrationSourceSearchForm searchForm) {
		return getIntegrationSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationSource saveIntegrationSource(IntegrationSource bean) {
		bean.setDecryptionPrivateKeySecuritySecret(getSecuritySecretService().saveSecuritySecret(bean.getDecryptionPrivateKeySecuritySecret()));
		bean.setDecryptionPrivateKeyPasswordSecuritySecret(getSecuritySecretService().saveSecuritySecret(bean.getDecryptionPrivateKeyPasswordSecuritySecret()));
		return getIntegrationSourceDAO().save(bean);
	}


	@Override
	public void deleteIntegrationSource(int id) {
		getIntegrationSourceDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////        IntegrationSourceType Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationSourceType getIntegrationSourceType(short id) {
		return getIntegrationSourceTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationSourceType> getIntegrationSourceTypeList(final IntegrationSourceTypeSearchForm searchForm) {
		return getIntegrationSourceTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<IntegrationSource, Criteria> getIntegrationSourceDAO() {
		return this.integrationSourceDAO;
	}


	public void setIntegrationSourceDAO(AdvancedUpdatableDAO<IntegrationSource, Criteria> integrationSourceDAO) {
		this.integrationSourceDAO = integrationSourceDAO;
	}


	public AdvancedUpdatableDAO<IntegrationSourceType, Criteria> getIntegrationSourceTypeDAO() {
		return this.integrationSourceTypeDAO;
	}


	public void setIntegrationSourceTypeDAO(AdvancedUpdatableDAO<IntegrationSourceType, Criteria> integrationSourceTypeDAO) {
		this.integrationSourceTypeDAO = integrationSourceTypeDAO;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
