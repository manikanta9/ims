package com.clifton.integration.target;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.target.search.IntegrationTargetApplicationSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationTargetApplicationServiceImpl implements IntegrationTargetApplicationService {

	private IntegrationImportService integrationImportService;

	private DaoNamedEntityCache<IntegrationTargetApplication> integrationTargetApplicationCache;
	private AdvancedUpdatableDAO<IntegrationTargetApplicationDefinitionType, Criteria> integrationTargetApplicationDefinitionTypeDAO;
	private AdvancedUpdatableDAO<IntegrationTargetApplication, Criteria> integrationTargetApplicationDAO;

	//////////////////////////////////////////////////////////////////////////////
	////////        IntegrationTargetApplication Business Methods         ////////
	//////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationTargetApplication getIntegrationTargetApplication(short id) {
		return getIntegrationTargetApplicationDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationTargetApplication getIntegrationTargetApplicationByName(String name) {
		return getIntegrationTargetApplicationCache().getBeanForKeyValueStrict(getIntegrationTargetApplicationDAO(), name);
	}


	@Override
	public List<IntegrationTargetApplication> getIntegrationTargetApplicationList(IntegrationTargetApplicationSearchForm searchForm) {
		return getIntegrationTargetApplicationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationTargetApplication saveIntegrationTargetApplication(IntegrationTargetApplication targetApplication) {
		return getIntegrationTargetApplicationDAO().save(targetApplication);
	}


	@Override
	public void deleteIntegrationTargetApplication(short id) {
		getIntegrationTargetApplicationDAO().delete(id);
	}


	//////////////////////////////////////////////////////////////////////////////
	////  IntegrationTargetApplicationDefinitionType Business Methods   ////
	//////////////////////////////////////////////////////////////////////////////


	@Override
	public void linkIntegrationTargetApplicationDefinitionType(short targetApplicationId, int definitionTypeId) {
		IntegrationTargetApplication targetApplication = getIntegrationTargetApplication(targetApplicationId);
		ValidationUtils.assertNotNull(targetApplication, "A Target Application was not found with ID: " + targetApplicationId);

		IntegrationImportDefinitionType definitionType = getIntegrationImportService().getIntegrationImportDefinitionType(definitionTypeId);
		ValidationUtils.assertNotNull(definitionType, "A Definition Type was not found with ID: " + definitionTypeId);

		IntegrationTargetApplicationDefinitionType targetApplicationDefinitionType = new IntegrationTargetApplicationDefinitionType();
		targetApplicationDefinitionType.setReferenceOne(targetApplication);
		targetApplicationDefinitionType.setReferenceTwo(definitionType);
		getIntegrationTargetApplicationDefinitionTypeDAO().save(targetApplicationDefinitionType);
	}


	@Override
	public void deleteIntegrationTargetApplicationDefinitionType(short targetApplicationId, int definitionTypeId) {
		IntegrationTargetApplication targetApplication = getIntegrationTargetApplication(targetApplicationId);
		ValidationUtils.assertNotNull(targetApplication, "A Target Application was not found with ID: " + targetApplicationId);

		IntegrationImportDefinitionType definitionType = getIntegrationImportService().getIntegrationImportDefinitionType(definitionTypeId);
		ValidationUtils.assertNotNull(definitionType, "A Definition Type was not found with ID: " + definitionTypeId);

		getIntegrationTargetApplicationDefinitionTypeDAO().delete(CollectionUtils.getOnlyElementStrict(getIntegrationTargetApplicationDefinitionTypeDAO().findByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Number[]{targetApplicationId, definitionTypeId})).getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public DaoNamedEntityCache<IntegrationTargetApplication> getIntegrationTargetApplicationCache() {
		return this.integrationTargetApplicationCache;
	}


	public void setIntegrationTargetApplicationCache(DaoNamedEntityCache<IntegrationTargetApplication> integrationTargetApplicationCache) {
		this.integrationTargetApplicationCache = integrationTargetApplicationCache;
	}


	public AdvancedUpdatableDAO<IntegrationTargetApplicationDefinitionType, Criteria> getIntegrationTargetApplicationDefinitionTypeDAO() {
		return this.integrationTargetApplicationDefinitionTypeDAO;
	}


	public void setIntegrationTargetApplicationDefinitionTypeDAO(AdvancedUpdatableDAO<IntegrationTargetApplicationDefinitionType, Criteria> integrationTargetApplicationDefinitionTypeDAO) {
		this.integrationTargetApplicationDefinitionTypeDAO = integrationTargetApplicationDefinitionTypeDAO;
	}


	public AdvancedUpdatableDAO<IntegrationTargetApplication, Criteria> getIntegrationTargetApplicationDAO() {
		return this.integrationTargetApplicationDAO;
	}


	public void setIntegrationTargetApplicationDAO(AdvancedUpdatableDAO<IntegrationTargetApplication, Criteria> integrationTargetApplicationDAO) {
		this.integrationTargetApplicationDAO = integrationTargetApplicationDAO;
	}
}
