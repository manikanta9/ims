package com.clifton.integration.incoming.transformation.parameter;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.datasource.EnhancedDataSource;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;
import org.hibernate.Criteria;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author TerryS
 */
@Service
public class IntegrationTransformationParameterServiceImpl implements IntegrationTransformationParameterService, ApplicationContextAware {

	private ApplicationContext applicationContext;
	private AdvancedUpdatableDAO<IntegrationTransformationParameter, Criteria> integrationTransformationParameterDAO;
	private UpdatableDAO<IntegrationTransformationParameterLink> integrationTransformationParameterLinkDAO;
	private AdvancedReadOnlyDAO<IntegrationTransformation, Criteria> integrationTransformationDAO;
	private SecuritySecretService securitySecretService;
	private IntegrationTransformationParameterHandler integrationTransformationParameterHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationTransformationParameter getIntegrationTransformationParameter(int id) {
		return getIntegrationTransformationParameterDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationTransformationParameter> getIntegrationTransformationParameterList(IntegrationTransformationParameterSearchForm searchForm) {
		return getIntegrationTransformationParameterDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationTransformationParameter saveIntegrationTransformationParameter(IntegrationTransformationParameter parameter) {
		return getIntegrationTransformationParameterDAO().save(parameter);
	}


	@Override
	@Transactional
	public void deleteIntegrationTransformationParameter(int id) {
		List<IntegrationTransformationParameterLink> parameterLinkList = getIntegrationTransformationParameterLinkListByChild(id);
		ValidationUtils.assertEmpty(parameterLinkList, () -> "The parameter is being used by the transformation " + parameterLinkList.get(0).getReferenceOne().getName());

		IntegrationTransformationParameter integrationTransformationParameter = getIntegrationTransformationParameter(id);
		ValidationUtils.assertNotNull(integrationTransformationParameter, "Could not find transformation parameter " + id);

		if (integrationTransformationParameter.getSecuritySecretValue() != null) {
			getSecuritySecretService().deleteSecuritySecret(integrationTransformationParameter.getSecuritySecretValue().getId());
		}
		getIntegrationTransformationParameterDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationTransformationParameterLink getIntegrationTransformationParameterLink(int id) {
		return getIntegrationTransformationParameterLinkDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationTransformationParameterLink> getIntegrationTransformationParameterLinkListByParent(int parentId) {
		return getIntegrationTransformationParameterLinkDAO().findByField("referenceOne.id", parentId);
	}


	@Override
	public List<IntegrationTransformationParameterLink> getIntegrationTransformationParameterLinkListByChild(int childId) {
		return getIntegrationTransformationParameterLinkDAO().findByField("referenceTwo.id", childId);
	}


	@Override
	public IntegrationTransformationParameterLink saveIntegrationTransformationParameterLink(int parentId, int childId) {
		IntegrationTransformation parent = getIntegrationTransformationDAO().findByPrimaryKey(parentId);
		IntegrationTransformationParameter child = getIntegrationTransformationParameter(childId);

		IntegrationTransformationParameterLink link = new IntegrationTransformationParameterLink();
		link.setReferenceOne(parent);
		link.setReferenceTwo(child);
		return getIntegrationTransformationParameterLinkDAO().save(link);
	}


	@Override
	@Transactional
	public IntegrationTransformationParameterLink saveIntegrationTransformationParameterEntryLink(IntegrationTransformationParameterLink link) {
		IntegrationTransformation transformation = getIntegrationTransformationDAO().findByPrimaryKey(link.getReferenceOne().getId());
		ValidationUtils.assertNotNull(transformation, "Transformation could not be loaded " + link.getReferenceOne().getId());

		if (link.isNewBean()) {
			boolean duplicate = CollectionUtils.getStream(getIntegrationTransformationParameterLinkListByParent(transformation.getId()))
					.map(IntegrationTransformationParameterLink::getReferenceTwo)
					.map(IntegrationTransformationParameter::getKey)
					.anyMatch(k -> StringUtils.isEqual(k, link.getReferenceTwo().getKey()));
			ValidationUtils.assertFalse(duplicate, String.format("The transformation already has a parameter with the key [%s]", link.getReferenceTwo().getKey()));
		}

		IntegrationTransformationParameter parameter = link.getReferenceTwo();
		if (parameter.getSecuritySecretValue() != null) {
			SecuritySecret securitySecret = parameter.getSecuritySecretValue();
			if (parameter.isNewBean()) {
				if (!StringUtils.isEmpty(securitySecret.getSecretString())) {
					securitySecret = getSecuritySecretService().saveSecuritySecret(parameter.getSecuritySecretValue());
					parameter.setSecuritySecretValue(securitySecret);
				}
				else {
					parameter.setSecuritySecretValue(null);
				}
			}
			else if (securitySecret.isNewBean()) {
				parameter.setSecuritySecretValue(null);
			}
			else {
				securitySecret = getSecuritySecretService().saveSecuritySecret(parameter.getSecuritySecretValue());
				parameter.setSecuritySecretValue(securitySecret);
			}
		}
		ValidationUtils.assertFalse(parameter.getSecuritySecretValue() == null && StringUtils.isEmpty(parameter.getValue()) && parameter.getSystemDataSource() == null, "Parameter must have a value, secret value or data source.");
		if (parameter.isDataSource()) {
			ValidationUtils.assertNotNull(parameter.getSystemDataSource(), "Data source is required.");
			boolean beanExists = this.applicationContext.containsBean(parameter.getSystemDataSource().getName());
			ValidationUtils.assertTrue(beanExists, "Data source details could not be found for data source record " + parameter.getSystemDataSource().getName());
			Object dataSourceBeanObject = this.applicationContext.getBean(parameter.getSystemDataSource().getName());
			ValidationUtils.assertTrue(dataSourceBeanObject instanceof EnhancedDataSource, "Detailed data source object not found " + dataSourceBeanObject.getClass().getName());
		}
		parameter = saveIntegrationTransformationParameter(parameter);
		link.setReferenceOne(transformation);
		link.setReferenceTwo(parameter);
		// validate the data source parameters.
		getIntegrationTransformationParameterHandler().validateIntegrationTransformationDataSourceParameterMap(transformation);

		return getIntegrationTransformationParameterLinkDAO().save(link);
	}


	@Override
	public void deleteIntegrationTransformationParameterLink(int id) {
		getIntegrationTransformationParameterLinkDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	public AdvancedUpdatableDAO<IntegrationTransformationParameter, Criteria> getIntegrationTransformationParameterDAO() {
		return this.integrationTransformationParameterDAO;
	}


	public void setIntegrationTransformationParameterDAO(AdvancedUpdatableDAO<IntegrationTransformationParameter, Criteria> integrationTransformationParameterDAO) {
		this.integrationTransformationParameterDAO = integrationTransformationParameterDAO;
	}


	public UpdatableDAO<IntegrationTransformationParameterLink> getIntegrationTransformationParameterLinkDAO() {
		return this.integrationTransformationParameterLinkDAO;
	}


	public void setIntegrationTransformationParameterLinkDAO(UpdatableDAO<IntegrationTransformationParameterLink> integrationTransformationParameterLinkDAO) {
		this.integrationTransformationParameterLinkDAO = integrationTransformationParameterLinkDAO;
	}


	public AdvancedReadOnlyDAO<IntegrationTransformation, Criteria> getIntegrationTransformationDAO() {
		return this.integrationTransformationDAO;
	}


	public void setIntegrationTransformationDAO(AdvancedReadOnlyDAO<IntegrationTransformation, Criteria> integrationTransformationDAO) {
		this.integrationTransformationDAO = integrationTransformationDAO;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public IntegrationTransformationParameterHandler getIntegrationTransformationParameterHandler() {
		return this.integrationTransformationParameterHandler;
	}


	public void setIntegrationTransformationParameterHandler(IntegrationTransformationParameterHandler integrationTransformationParameterHandler) {
		this.integrationTransformationParameterHandler = integrationTransformationParameterHandler;
	}
}
