package com.clifton.integration.incoming.definition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;
import org.springframework.stereotype.Component;


/**
 * @author vgomelsky
 */
@Component
public class IntegrationImportDefinitionTypeByEventNameCache extends SelfRegisteringSingleKeyDaoListCache<IntegrationImportDefinitionType, String> {

	@Override
	protected String getBeanKeyProperty() {
		return "eventName";
	}


	@Override
	protected String getBeanKeyValue(IntegrationImportDefinitionType bean) {
		return bean.getEventName();
	}
}
