package com.clifton.integration.incoming.server;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessageStatus;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.file.IntegrationDirectory;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.security.encryption.transport.server.SecurityRSATransportEncryptionServerHandler;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;


@Service
public class IntegrationServerStatusProcessorServiceImpl implements IntegrationServerStatusProcessorService {

	private IntegrationImportService integrationImportService;
	private IntegrationFileService integrationFileService;


	private MessageChannel integrationFileProcessingChannel;
	private MessageChannel sftpStatusListCommandChannel;
	private SubscribableChannel sftpStatusListCommandReplyChannel;

	private SecurityRSATransportEncryptionServerHandler securityRSATransportEncryptionServerHandler;


	@Override
	public MessageStatus getIntegrationStatus() {
		MessageStatus messageStatus = new MessageStatus();
		messageStatus.setResponseName(MessageStatus.INTEGRATION_FTP_STATUS);
		messageStatus.setResponseStatus("OK");
		return messageStatus;
	}


	@Override
	public MessageStatus getIntegrationFtpStatus() {
		MessageStatus messageStatus = new MessageStatus();
		try {
			MessageChannel channel = getSftpStatusListCommandChannel();
			//set a blank payload since for this channel the payload is specified in the context file
			channel.send(MessageBuilder.withPayload("").build());
			SubscribableChannel replyChannel = getSftpStatusListCommandReplyChannel();
			replyChannel.subscribe(message1 -> {
				//Do nothing - simply here to accept a reply from the command channel.
			});
			messageStatus.setResponseName(MessageStatus.INTEGRATION_FTP_STATUS);
			messageStatus.setResponseStatus("OK");
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error occurred when getting FTP Status Response", e);
			messageStatus.setError(true);
			messageStatus.setException(e);
		}
		return messageStatus;
	}


	@Override
	public List<MessageStatus> getIntegrationTransportEncryptionStatus(String value) {
		List<MessageStatus> messageStatusList = new ArrayList<>();
		if (!StringUtils.isEmpty(value)) {
			try {
				MessageStatus status = new MessageStatus();
				status.setResponseName(MessageStatus.INTEGRATION_DECRYPTION_VALUE);
				status.setResponseStatus(getSecurityRSATransportEncryptionServerHandler().decryptValueFromTransport(value));
				messageStatusList.add(status);
			}
			catch (Throwable e) {
				LogUtils.error(getClass(), "Error occurred when getting the Encryption Status Response", e);
				MessageStatus status = new MessageStatus();
				status.setError(true);
				status.setException(e);
				return Collections.singletonList(status);
			}
			MessageStatus status = new MessageStatus();
			status.setResponseName(MessageStatus.INTEGRATION_DECRYPTION_STATUS);
			status.setResponseStatus("OK");
			messageStatusList.add(status);
		}
		return messageStatusList;
	}


	@Override
	public MessageStatus getIntegrationTransportEncryptionPublicKey() {
		MessageStatus status = new MessageStatus();
		status.setResponseName(MessageStatus.INTEGRATION_PUBLIC_KEY);
		status.setResponseStatus(getSecurityRSATransportEncryptionServerHandler().getPublicEncryptionKey());
		return status;
	}


	@Override
	public MessageStatus getIntegrationTransformationStatus() {
		final String inputDirectory = getIntegrationFileService().getIntegrationSystemFilePathMap().get(IntegrationDirectory.INPUT);
		final int pollSleepTime = 1000;
		final int pollLimitCount = 60;

		MessageStatus status = new MessageStatus();
		try {
			IntegrationImportDefinitionSearchForm definitionSearchForm = new IntegrationImportDefinitionSearchForm();
			definitionSearchForm.setTestDefinition(true);
			definitionSearchForm.setDisabled(false);
			List<IntegrationImportDefinition> testDefinitions = getIntegrationImportService().getIntegrationImportDefinitionList(definitionSearchForm);

			List<IntegrationImportRun> initialRuns = getLatestRunsForDefinitions(testDefinitions, null);

			Date startDate = new Date();

			for (IntegrationImportRun run : initialRuns) {
				File file = new File(getIntegrationFileService().getIntegrationFileAbsolutePath(run.getFile()));
				ValidationUtils.assertTrue(file.exists(), "File from previous IntegrationImportRun [" + run.getId() + "] does not exist at location: " + file.getPath());
				//Copy the file into the input directory to trigger processing
				FileUtils.copyFile(file, FileUtils.combinePath(inputDirectory, IntegrationFileUtils.getOriginalFileName(file.getName())));
			}

			//Poll for runs that occur after the reprocessing started
			int pollCounter = 0;
			while (pollCounter < pollLimitCount) {
				if (CollectionUtils.isEmpty(testDefinitions)) {
					break;
				}
				List<IntegrationImportRun> newRuns = getLatestRunsForDefinitions(testDefinitions, startDate);
				if (!CollectionUtils.isEmpty(newRuns)) {
					for (IntegrationImportRun run : newRuns) {
						if (run.getStatus().getIntegrationImportStatusType() == IntegrationImportStatusTypes.SUCCESSFUL) {
							testDefinitions.remove(run.getIntegrationImportDefinition());
						}
					}
				}
				pollCounter += 1;
				Thread.sleep(pollSleepTime);
			}
			ValidationUtils.assertEmpty(testDefinitions, "Not all test definitions completed before the health check timed out: " + Arrays.toString(BeanUtils.getPropertyValues(testDefinitions, "name")));
			status.setResponseName(MessageStatus.INTEGRATION_TRANSFORMATION);
			status.setResponseStatus("OK");
		}
		catch (Exception e) {
			if (e instanceof InterruptedException) {
				Thread.currentThread().interrupt();
			}
			LogUtils.error(getClass(), "Error occurred when getting the transformation status response", e);
			status.setError(true);
			status.setException(e);
		}
		return status;
	}


	private List<IntegrationImportRun> getLatestRunsForDefinitions(List<IntegrationImportDefinition> definitions, Date startDate) {
		List<IntegrationImportRun> runs = new ArrayList<>(0);
		for (IntegrationImportDefinition definition : definitions) {
			IntegrationImportRunSearchForm runSearchForm = new IntegrationImportRunSearchForm();
			runSearchForm.setIntegrationImportDefinitionId(definition.getId());
			runSearchForm.setOrderBy("effectiveDate:DESC");
			runSearchForm.setLimit(1);
			if (startDate != null) {
				runSearchForm.setEndProcessDateAfter(startDate);
			}
			IntegrationImportRun run = CollectionUtils.getFirstElement(getIntegrationImportService().getIntegrationImportRunList(runSearchForm));
			if (run != null) {
				runs.add(run);
			}
		}
		return runs;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public MessageChannel getIntegrationFileProcessingChannel() {
		return this.integrationFileProcessingChannel;
	}


	public void setIntegrationFileProcessingChannel(MessageChannel integrationFileProcessingChannel) {
		this.integrationFileProcessingChannel = integrationFileProcessingChannel;
	}


	public MessageChannel getSftpStatusListCommandChannel() {
		return this.sftpStatusListCommandChannel;
	}


	public void setSftpStatusListCommandChannel(MessageChannel sftpStatusListCommandChannel) {
		this.sftpStatusListCommandChannel = sftpStatusListCommandChannel;
	}


	public SubscribableChannel getSftpStatusListCommandReplyChannel() {
		return this.sftpStatusListCommandReplyChannel;
	}


	public void setSftpStatusListCommandReplyChannel(SubscribableChannel sftpStatusListCommandReplyChannel) {
		this.sftpStatusListCommandReplyChannel = sftpStatusListCommandReplyChannel;
	}


	public SecurityRSATransportEncryptionServerHandler getSecurityRSATransportEncryptionServerHandler() {
		return this.securityRSATransportEncryptionServerHandler;
	}


	public void setSecurityRSATransportEncryptionServerHandler(SecurityRSATransportEncryptionServerHandler securityRSATransportEncryptionServerHandler) {
		this.securityRSATransportEncryptionServerHandler = securityRSATransportEncryptionServerHandler;
	}
}
