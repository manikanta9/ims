package com.clifton.integration.incoming.trade.intraday;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.integration.incoming.trade.intraday.search.IntegrationTradeIntradaySearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationTradeIntradayServiceImpl implements IntegrationTradeIntradayService {

	private AdvancedUpdatableDAO<IntegrationTradeIntraday, Criteria> integrationTradeIntradayDAO;


	@Override
	public List<IntegrationTradeIntraday> getIntegrationTradeIntradayList(final IntegrationTradeIntradaySearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				// TODO Auto-generated method stub
				super.configureCriteria(criteria);

				if (searchForm.getExcludeOverriddenTrades() != null && searchForm.getExcludeOverriddenTrades()) {
					// NOTE: This work because the priority is DELETED, CHANGED then ADDED which happens to be alphabetical
					criteria.add(Restrictions.sqlRestriction("{alias}.IntegrationTradeIntradayID = " + //
							"( " + //
							"SELECT TOP(1) IntegrationTradeIntradayID " + //
							"FROM CliftonIntegration.dbo.IntegrationTradeIntraday " + //
							"WHERE UniqueTradeId = {alias}.UniqueTradeId " + //
							"AND (TradeDate >= DATEADD(MONTH,-2,{alias}.TradeDate) OR TradeDate <= DATEADD(MONTH,2,{alias}.TradeDate)) " + //
							"ORDER BY Type DESC, IntegrationTradeIntradayID ASC " + //
							")"));
				}
			}
		};
		return getIntegrationTradeIntradayDAO().findBySearchCriteria(config);
	}


	public AdvancedUpdatableDAO<IntegrationTradeIntraday, Criteria> getIntegrationTradeIntradayDAO() {
		return this.integrationTradeIntradayDAO;
	}


	public void setIntegrationTradeIntradayDAO(AdvancedUpdatableDAO<IntegrationTradeIntraday, Criteria> integrationTradeIntradayDAO) {
		this.integrationTradeIntradayDAO = integrationTradeIntradayDAO;
	}
}
