package com.clifton.integration.incoming.definition.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class IntegrationImportDefinitionValidator extends SelfRegisteringDaoValidator<IntegrationImportDefinition> {

	@Override
	public void validate(IntegrationImportDefinition bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {

		IntegrationImportDefinition originalBean = getOriginalBean(bean);

		List<String> unequalProperties = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false);
		if (!CollectionUtils.isEmpty(unequalProperties)) {
			unequalProperties.remove("description");
			unequalProperties.remove("disabled");

			//TODO Do we still want to limit modification of IntegrationImportDefinition?
			//if (!CollectionUtils.isEmpty(unequalProperties)) {
			//	throw new ValidationException("Only \"Description\" and \"Is Disabled\" may be modified");
			//}
		}
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}
}
