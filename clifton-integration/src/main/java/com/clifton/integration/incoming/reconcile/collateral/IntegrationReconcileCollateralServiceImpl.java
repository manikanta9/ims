package com.clifton.integration.incoming.reconcile.collateral;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationReconcileCollateralServiceImpl implements IntegrationReconcileCollateralService {

	private AdvancedReadOnlyDAO<IntegrationReconcileCollateralDailyImport, Criteria> integrationReconcileCollateralDailyImportDAO;


	@Override
	public List<IntegrationReconcileCollateralDailyImport> getIntegrationReconcileCollateralDailyImportListByRun(int runId) {
		return getIntegrationReconcileCollateralDailyImportDAO().findByNamedQuery("integration-loadReconcileCollateralDaily", runId, true);
	}


	public AdvancedReadOnlyDAO<IntegrationReconcileCollateralDailyImport, Criteria> getIntegrationReconcileCollateralDailyImportDAO() {
		return this.integrationReconcileCollateralDailyImportDAO;
	}


	public void setIntegrationReconcileCollateralDailyImportDAO(AdvancedReadOnlyDAO<IntegrationReconcileCollateralDailyImport, Criteria> integrationReconcileCollateralDailyImportDAO) {
		this.integrationReconcileCollateralDailyImportDAO = integrationReconcileCollateralDailyImportDAO;
	}
}
