package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.integration.incoming.manager.search.IntegrationManagerAssetClassGroupSearchForm;
import com.clifton.integration.incoming.manager.search.IntegrationManagerBankMappingSearchForm;
import com.clifton.integration.incoming.manager.search.IntegrationManagerPositionHistorySearchForm;
import com.clifton.integration.incoming.manager.search.IntegrationManagerTransactionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>IntegrationManagerServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class IntegrationManagerServiceImpl implements IntegrationManagerService {

	private AdvancedUpdatableDAO<IntegrationManagerAssetClass, Criteria> integrationManagerAssetClassDAO;
	private AdvancedUpdatableDAO<IntegrationManagerAssetClassGroup, Criteria> integrationManagerAssetClassGroupDAO;
	private AdvancedUpdatableDAO<IntegrationManagerAssetClassGroupImportDefinition, Criteria> integrationManagerAssetClassGroupImportDefinitionDAO;
	private AdvancedUpdatableDAO<IntegrationManagerBankMapping, Criteria> integrationManagerBankMappingDAO;
	private AdvancedUpdatableDAO<IntegrationManagerTransaction, Criteria> integrationManagerTransactionDAO;

	private AdvancedUpdatableDAO<IntegrationManagerPositionHistory, Criteria> integrationManagerPositionHistoryDAO;
	private AdvancedReadOnlyDAO<IntegrationManagerPositionType, Criteria> integrationManagerPositionTypeDAO;

	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;
	private IntegrationImportService integrationImportService;

	private String integrationDataSourceName = "dataSource";


	//////////////////////////////////////////////////////////
	/////       Manager Asset Class Group Methods        /////
	//////////////////////////////////////////////////////////


	@Override
	public IntegrationManagerAssetClassGroup getIntegrationManagerAssetClassGroup(int id) {
		return getIntegrationManagerAssetClassGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationManagerAssetClassGroup saveIntegrationManagerAssetClassGroup(IntegrationManagerAssetClassGroup bean) {
		return getIntegrationManagerAssetClassGroupDAO().save(bean);
	}


	@Override
	public void deleteIntegrationManagerAssetClassGroup(int id) {
		getIntegrationManagerAssetClassGroupDAO().delete(id);
	}


	@Override
	public List<String> getIntegrationUnmappedAssetClassesForGroup(int integrationRunId) {
		DataTable dt = getDataTableRetrievalHandler().findDataTable(new SqlSelectCommand(
				"SELECT DISTINCT imps.AssetClass FROM IntegrationManagerPositionHistory mph " +
						"INNER JOIN IntegrationManagerSecurity imps ON imps.IntegrationManagerSecurityID = mph.IntegrationManagerSecurityID " +
						"INNER JOIN IntegrationImportRun iir ON iir.IntegrationImportRunID = mph.IntegrationImportRunID " +
						"INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID " +
						"INNER JOIN IntegrationManagerAssetClassGroupImportDefinition gid ON gid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID " +
						"INNER JOIN IntegrationManagerAssetClassGroup acg ON acg.IntegrationManagerAssetClassGroupID = gid.IntegrationManagerAssetClassGroupID " +
						"WHERE mph.IntegrationManagerPositionTypeID IS NULL AND acg.IsRequireAllAssetClassMappings = 1 AND iir.IntegrationImportRunID = ?")
				.addIntegerParameterValue(integrationRunId)
		);
		List<String> list = new ArrayList<>();
		if (dt == null || dt.getTotalRowCount() == 0) {
			return null;
		}
		for (int i = 0; i < dt.getTotalRowCount(); i++) {
			DataRow row = dt.getRow(i);
			list.add((String) row.getValue("AssetClass"));
		}
		return list;
	}


	@Override
	public List<IntegrationManagerAssetClassGroup> getIntegrationManagerAssetClassGroupList(IntegrationManagerAssetClassGroupSearchForm searchForm) {
		return getIntegrationManagerAssetClassGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationManagerAssetClassGroup getIntegrationManagerAssetClassGroupByName(String groupName) {
		return getIntegrationManagerAssetClassGroupDAO().findOneByField("name", groupName);
	}


	@Override
	public List<IntegrationManagerAssetClassGroup> getIntegrationManagerAssetClassGroupListByBusinessCompanyId(int businessCompanyId) {
		return getIntegrationManagerAssetClassGroupDAO().findByField("businessCompanyId", businessCompanyId);
	}


	@Override
	public IntegrationManagerAssetClassGroup getIntegrationManagerAssetClassGroupByDefinition(final int definitionId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createAlias("definitionList", "d").add(Restrictions.eq("d.id", definitionId));

		List<IntegrationManagerAssetClassGroup> groups = getIntegrationManagerAssetClassGroupDAO().findBySearchCriteria(searchConfigurer);
		if (!groups.isEmpty()) {
			return getIntegrationManagerAssetClassGroupDAO().findBySearchCriteria(searchConfigurer).get(0);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Manager Asset Class Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationManagerAssetClass getIntegrationManagerAssetClass(int id) {
		return getIntegrationManagerAssetClassDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationManagerAssetClass> getIntegrationManagerAssetClassListByGroup(int groupId) {
		return getIntegrationManagerAssetClassDAO().findByField("group.id", groupId);
	}


	@Override
	public IntegrationManagerAssetClass saveIntegrationManagerAssetClass(IntegrationManagerAssetClass bean) {
		if (!StringUtils.isEmpty(bean.getSecurityDescriptionPattern())) {
			if (!bean.getMapSecurityForAllManagers()) {
				ValidationUtils.assertNotNull(bean.getManagerBankCode(), "A Bank Code is necessary unless Security Description is mapped across all managers (see checkbox)");
			}
			ValidationUtils.assertTrue(bean.getSecurityDescriptionPattern().length() > 2, "Security Description must be longer than 2 characters", "securityDescriptionPattern");
		}
		else {
			ValidationUtils.assertFalse(bean.isSecurityDescriptionPatternExactMatch(), "Security Description must be specified to require an exact match.", "securityDescriptionPatternExactMatch");
		}

		if (bean.getManagerBankCode() != null || bean.getSecurityDescriptionPattern() != null) {
			ValidationUtils.assertNotNull(bean.getMappingNote(), "A Mapping Note is necessary if Bank Code or Security Description is set.", "mappingNote");
		}
		return getIntegrationManagerAssetClassDAO().save(bean);
	}


	@Override
	public void deleteIntegrationManagerAssetClass(int id) {
		getIntegrationManagerAssetClassDAO().delete(id);
	}


	//////////////////////////////////////////////////////////
	// Manager Asset Class Group Import Definition Methods  //
	//////////////////////////////////////////////////////////


	@Override
	public List<IntegrationManagerAssetClassGroupImportDefinition> getIntegrationManagerAssetClassGroupImportDefinitionListByGroup(IntegrationManagerAssetClassGroup group) {
		return getIntegrationManagerAssetClassGroupImportDefinitionDAO().findByField("referenceTwo.id", group.getId());
	}


	@Override
	public void linkIntegrationManagerAssetClassGroupImportDefinition(Integer importDefinitionId, Integer importGroupId) {
		ValidationUtils.assertNotNull(importDefinitionId, "Import definition must be specified to create a link to an import group");
		ValidationUtils.assertNotNull(importGroupId, "Import group must be specified to create a link");

		IntegrationImportDefinition definition = getIntegrationImportService().getIntegrationImportDefinition(importDefinitionId);
		ValidationUtils.assertNotNull(definition, "An Import Definition was not found with ID: " + importDefinitionId);

		IntegrationManagerAssetClassGroup group = getIntegrationManagerAssetClassGroup(importGroupId);
		ValidationUtils.assertNotNull(importGroupId, "An Import Group was not found with ID: " + importGroupId);

		IntegrationManagerAssetClassGroupImportDefinition bean = new IntegrationManagerAssetClassGroupImportDefinition();
		bean.setReferenceOne(definition);
		bean.setReferenceTwo(group);
		getIntegrationManagerAssetClassGroupImportDefinitionDAO().save(bean);
	}


	@Override
	public void deleteIntegrationManagerAssetClassGroupImportDefinition(Integer importDefinitionId, Integer importGroupId) {
		IntegrationManagerAssetClassGroupImportDefinition link = getIntegrationManagerAssetClassGroupImportDefinitionDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{importDefinitionId, importGroupId});
		ValidationUtils.assertNotNull(link, "IntegrationManagerAssetClassGroupImportDefinitionLink does not exist for DefinitionID: " + importDefinitionId + " and ImportGroupID: " + importGroupId);
		getIntegrationManagerAssetClassGroupImportDefinitionDAO().delete(link.getId());
	}


	//////////////////////////////////////////////////////////
	////       Manager Position History Methods           ////
	//////////////////////////////////////////////////////////


	@Override
	public List<IntegrationManagerPositionHistory> getIntegrationManagerPositionHistoryList(IntegrationManagerPositionHistorySearchForm searchForm) {
		return getIntegrationManagerPositionHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<IntegrationManagerPositionHistory> getIntegrationManagerPositionHistoryList(Integer businessCompanyId, String bankCode, Date balanceDate) {
		List<IntegrationManagerAssetClassGroup> groupList = getIntegrationManagerAssetClassGroupListByBusinessCompanyId(businessCompanyId);
		ValidationUtils.assertNotNull(groupList, "Missing Integration Manager Asset Class Group With Company ID [" + businessCompanyId + "]");
		ValidationUtils.assertFalse(groupList.isEmpty(), "Missing Integration Manager Asset Class Group With Company ID [" + businessCompanyId + "]");

		// IntegrationImportRun.IntegrationImportDefinition.assetClassGroupList (this uses a link property on the IntegrationImportDefinition schema)
		final List<IntegrationImportRun> runList = getIntegrationManagerImportRunList(businessCompanyId, balanceDate);
		if (CollectionUtils.isEmpty(runList)) {
			return null;
		}
		// IntegrationManagerPositionHistory.IntegrationManagerBank.bankCode
		HibernateSearchConfigurer searchConfig = criteria -> {
			criteria.createAlias("managerBank", "b").add(Restrictions.eq("b.bankCode", bankCode));
			criteria.add(Restrictions.in("run.id", BeanUtils.getPropertyValues(runList, "id")));
		};
		return getIntegrationManagerPositionHistoryDAO().findBySearchCriteria(searchConfig);
	}


	//////////////////////////////////////////////////////////
	/////         Manager Position Type  Methods         /////
	//////////////////////////////////////////////////////////


	@Override
	public IntegrationManagerPositionType getIntegrationManagerPositionType(short id) {
		return getIntegrationManagerPositionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationManagerPositionType> getIntegrationManagerPositionTypeList() {
		return getIntegrationManagerPositionTypeDAO().findAll();
	}


	//////////////////////////////////////////////////////////
	///////          Manager Summary Methods           ///////
	//////////////////////////////////////////////////////////


	@Override
	public List<IntegrationManagerPositionHistorySummary> getIntegrationManagerPositionHistorySummaryList(Integer custodianId, Date balanceDate) {
		List<IntegrationManagerAssetClassGroup> groupList = getIntegrationManagerAssetClassGroupListByBusinessCompanyId(custodianId);
		ValidationUtils.assertNotNull(groupList, "Missing Integration Manager Asset Class Group With Custodian ID [" + custodianId + "]");
		ValidationUtils.assertFalse(groupList.isEmpty(), "Missing Integration Manager Asset Class Group With Custodian ID [" + custodianId + "]");

		List<IntegrationImportRun> runList = getIntegrationManagerImportRunList(custodianId, balanceDate);
		if (CollectionUtils.isEmpty(runList)) {
			return null;
		}

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT mpb.ManagerBankCode AS [BankCode], SUM(COALESCE(mph.CashValue,0)) AS [CashValue], SUM(COALESCE(mph.SecurityValue,0)) AS [SecurityValue] ");
		sql.append("FROM IntegrationManagerPositionHistory mph ");
		sql.append("INNER JOIN IntegrationManagerBank mpb ON mpb.IntegrationManagerBankID = mph.IntegrationManagerBankID ");
		sql.append("WHERE mph.IntegrationImportRunID IN (").append(BeanUtils.getPropertyValues(runList, "id", ",")).append(") ");
		sql.append("GROUP BY mpb.ManagerBankCode ");
		sql.append("ORDER BY mpb.ManagerBankCode ");

		DataTable dt = getDataTableRetrievalHandler().findDataTable(sql.toString());
		List<IntegrationManagerPositionHistorySummary> list = new ArrayList<>();
		if (dt == null || dt.getTotalRowCount() == 0) {
			return null;
		}
		for (int i = 0; i < dt.getTotalRowCount(); i++) {
			DataRow row = dt.getRow(i);
			IntegrationManagerPositionHistorySummary bean = new IntegrationManagerPositionHistorySummary();
			bean.setBankCode((String) row.getValue("BankCode"));
			bean.setCashValue((BigDecimal) row.getValue("CashValue"));
			bean.setSecurityValue((BigDecimal) row.getValue("SecurityValue"));
			bean.setPositionDate(balanceDate);

			list.add(bean);
		}
		return list;
	}


	private List<IntegrationImportRun> getIntegrationManagerImportRunList(Integer custodianId, Date balanceDate) {
		// Then get a List of Import Runs to look up for
		IntegrationImportRunSearchForm searchForm = new IntegrationImportRunSearchForm();
		searchForm.setEffectiveDate(balanceDate);
		searchForm.setStatusName(IntegrationImportStatusTypes.SUCCESSFUL.name());
		searchForm.setBusinessCompanyId(custodianId);
		return getIntegrationImportService().getIntegrationImportRunList(searchForm);
	}


	//////////////////////////////////////////////////////////
	///////       Account Mapping Summary Methods      ///////
	//////////////////////////////////////////////////////////
	@Override
	public IntegrationManagerBankMapping getIntegrationManagerBankMapping(int id) {
		return getIntegrationManagerBankMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationManagerBankMapping> getIntegrationManagerBankMappingList(IntegrationManagerBankMappingSearchForm searchForm) {
		return getIntegrationManagerBankMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationManagerBankMapping saveIntegrationManagerBankMapping(IntegrationManagerBankMapping bean) {
		return getIntegrationManagerBankMappingDAO().save(bean);
	}


	@Override
	public void deleteIntegrationManagerBankMapping(int id) {
		getIntegrationManagerBankMappingDAO().delete(id);
	}


	//////////////////////////////////////////////////////////
	///////       Manager Transaction Methods          ///////
	//////////////////////////////////////////////////////////


	@Override
	public IntegrationManagerTransaction getIntegrationManagerTransaction(int id) {
		return getIntegrationManagerTransactionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationManagerTransaction> getIntegrationManagerTransactionList(IntegrationManagerTransactionSearchForm searchForm) {
		return getIntegrationManagerTransactionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	//////////////////////////////////////////////////////////
	///////          Getter & Setter Methods           ///////
	//////////////////////////////////////////////////////////


	private DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return getDataTableRetrievalHandlerLocator().locate(getIntegrationDataSourceName());
	}


	public DataTableRetrievalHandlerLocator getDataTableRetrievalHandlerLocator() {
		return this.dataTableRetrievalHandlerLocator;
	}


	public void setDataTableRetrievalHandlerLocator(DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator) {
		this.dataTableRetrievalHandlerLocator = dataTableRetrievalHandlerLocator;
	}


	public AdvancedUpdatableDAO<IntegrationManagerAssetClassGroup, Criteria> getIntegrationManagerAssetClassGroupDAO() {
		return this.integrationManagerAssetClassGroupDAO;
	}


	public void setIntegrationManagerAssetClassGroupDAO(AdvancedUpdatableDAO<IntegrationManagerAssetClassGroup, Criteria> integrationManagerAssetClassGroupDAO) {
		this.integrationManagerAssetClassGroupDAO = integrationManagerAssetClassGroupDAO;
	}


	public AdvancedUpdatableDAO<IntegrationManagerAssetClassGroupImportDefinition, Criteria> getIntegrationManagerAssetClassGroupImportDefinitionDAO() {
		return this.integrationManagerAssetClassGroupImportDefinitionDAO;
	}


	public void setIntegrationManagerAssetClassGroupImportDefinitionDAO(
			AdvancedUpdatableDAO<IntegrationManagerAssetClassGroupImportDefinition, Criteria> integrationManagerAssetClassGroupImportDefinitionDAO) {
		this.integrationManagerAssetClassGroupImportDefinitionDAO = integrationManagerAssetClassGroupImportDefinitionDAO;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public AdvancedUpdatableDAO<IntegrationManagerPositionHistory, Criteria> getIntegrationManagerPositionHistoryDAO() {
		return this.integrationManagerPositionHistoryDAO;
	}


	public void setIntegrationManagerPositionHistoryDAO(AdvancedUpdatableDAO<IntegrationManagerPositionHistory, Criteria> integrationManagerPositionHistoryDAO) {
		this.integrationManagerPositionHistoryDAO = integrationManagerPositionHistoryDAO;
	}


	public AdvancedUpdatableDAO<IntegrationManagerAssetClass, Criteria> getIntegrationManagerAssetClassDAO() {
		return this.integrationManagerAssetClassDAO;
	}


	public void setIntegrationManagerAssetClassDAO(AdvancedUpdatableDAO<IntegrationManagerAssetClass, Criteria> integrationManagerAssetClassDAO) {
		this.integrationManagerAssetClassDAO = integrationManagerAssetClassDAO;
	}


	public AdvancedReadOnlyDAO<IntegrationManagerPositionType, Criteria> getIntegrationManagerPositionTypeDAO() {
		return this.integrationManagerPositionTypeDAO;
	}


	public void setIntegrationManagerPositionTypeDAO(AdvancedReadOnlyDAO<IntegrationManagerPositionType, Criteria> integrationManagerPositionTypeDAO) {
		this.integrationManagerPositionTypeDAO = integrationManagerPositionTypeDAO;
	}


	public String getIntegrationDataSourceName() {
		return this.integrationDataSourceName;
	}


	@Override
	public void setIntegrationDataSourceName(String integrationDataSourceName) {
		this.integrationDataSourceName = integrationDataSourceName;
	}


	public AdvancedUpdatableDAO<IntegrationManagerBankMapping, Criteria> getIntegrationManagerBankMappingDAO() {
		return this.integrationManagerBankMappingDAO;
	}


	public void setIntegrationManagerBankMappingDAO(AdvancedUpdatableDAO<IntegrationManagerBankMapping, Criteria> integrationManagerBankMappingDAO) {
		this.integrationManagerBankMappingDAO = integrationManagerBankMappingDAO;
	}


	public AdvancedUpdatableDAO<IntegrationManagerTransaction, Criteria> getIntegrationManagerTransactionDAO() {
		return this.integrationManagerTransactionDAO;
	}


	public void setIntegrationManagerTransactionDAO(AdvancedUpdatableDAO<IntegrationManagerTransaction, Criteria> integrationManagerTransactionDAO) {
		this.integrationManagerTransactionDAO = integrationManagerTransactionDAO;
	}
}
