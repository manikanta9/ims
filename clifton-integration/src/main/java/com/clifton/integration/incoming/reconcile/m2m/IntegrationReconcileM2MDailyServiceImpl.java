package com.clifton.integration.incoming.reconcile.m2m;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationReconcileM2MDailyServiceImpl implements IntegrationReconcileM2MDailyService {

	private AdvancedReadOnlyDAO<IntegrationReconcileM2MDailyImport, Criteria> integrationReconcileM2MDailyImportDAO;


	@Override
	public List<IntegrationReconcileM2MDailyImport> getIntegrationReconcileM2MDailyImportListByRun(int runId) {
		return getIntegrationReconcileM2MDailyImportDAO().findByNamedQuery("integration-loadReconcileM2MDaily", runId, true);
	}


	public AdvancedReadOnlyDAO<IntegrationReconcileM2MDailyImport, Criteria> getIntegrationReconcileM2MDailyImportDAO() {
		return this.integrationReconcileM2MDailyImportDAO;
	}


	public void setIntegrationReconcileM2MDailyImportDAO(AdvancedReadOnlyDAO<IntegrationReconcileM2MDailyImport, Criteria> integrationReconcileM2MDailyImportDAO) {
		this.integrationReconcileM2MDailyImportDAO = integrationReconcileM2MDailyImportDAO;
	}
}
