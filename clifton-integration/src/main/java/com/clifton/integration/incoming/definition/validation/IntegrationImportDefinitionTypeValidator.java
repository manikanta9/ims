package com.clifton.integration.incoming.definition.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class IntegrationImportDefinitionTypeValidator extends SelfRegisteringDaoValidator<IntegrationImportDefinitionType> {

	@Override
	public void validate(IntegrationImportDefinitionType bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {

		IntegrationImportDefinitionType originalBean = getOriginalBean(bean);

		List<String> unequalProperties = CoreCompareUtils.getNoEqualProperties(bean, originalBean, false);
		if (!CollectionUtils.isEmpty(unequalProperties)) {
			unequalProperties.remove("description");

			if (!CollectionUtils.isEmpty(unequalProperties)) {
				throw new ValidationException("Only \"Description\" may be modified");
			}
		}
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}
}
