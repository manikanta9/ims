package com.clifton.integration.incoming.trade;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.integration.incoming.trade.search.IntegrationTradeSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationTradeServiceImpl implements IntegrationTradeService {

	private AdvancedUpdatableDAO<IntegrationTrade, Criteria> integrationTradeDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<IntegrationTrade> getIntegrationTradeList(IntegrationTradeSearchForm searchForm) {
		return getIntegrationTradeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<IntegrationTrade, Criteria> getIntegrationTradeDAO() {
		return this.integrationTradeDAO;
	}


	public void setIntegrationTradeDAO(AdvancedUpdatableDAO<IntegrationTrade, Criteria> integrationTradeDAO) {
		this.integrationTradeDAO = integrationTradeDAO;
	}
}
