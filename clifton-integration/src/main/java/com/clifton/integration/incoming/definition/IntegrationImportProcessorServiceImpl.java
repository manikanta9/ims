package com.clifton.integration.incoming.definition;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.file.IntegrationDirectory;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileSourceType;
import com.clifton.integration.file.archive.IntegrationFileArchiveParameters;
import com.clifton.integration.file.archive.IntegrationFileArchiveService;
import com.clifton.integration.file.archive.IntegrationFileArchiveStrategySearchForm;
import com.clifton.integration.file.archive.naming.InvestmentInstructionArchiveFilePathGenerator;
import com.clifton.integration.source.IntegrationSourceService;
import com.clifton.integration.source.search.IntegrationSourceSearchForm;
import com.clifton.integration.target.IntegrationTargetApplication;
import com.clifton.integration.target.IntegrationTargetApplicationService;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.clifton.integration.file.IntegrationFileUtils.FILE_NAME_WRITING_EXTENSION;


@Service
public class IntegrationImportProcessorServiceImpl implements IntegrationImportProcessorService {

	private Map<IntegrationDirectory, String> systemFilePathMap;

	private IntegrationImportService integrationImportService;
	private IntegrationSourceService integrationSourceService;
	private IntegrationTargetApplicationService integrationTargetApplicationService;

	private IntegrationFileService integrationFileService;
	private IntegrationFileArchiveService integrationFileArchiveService;

	private ExecutorService executorService;
	private MessageChannel integrationFileProcessingChannel;


	@Override
	public void rerunIntegrationRunImport(int runId, boolean limitToImportRun) {
		IntegrationImportRun importRun = getIntegrationImportService().getIntegrationImportRun(runId);
		ValidationUtils.assertNotNull(importRun, "Import run cannot be null!");
		if (invalidRerunStatus(importRun)) {
			throw new ValidationException("Cannot re-run integration run with status '" + importRun.getStatus() + "'.");
		}
		process(importRun, null, limitToImportRun);
	}


	@Override
	public void rerunIntegrationRunEventImport(int runEventId, boolean limitToImportRun) {
		IntegrationImportRunEvent importRunEvent = getIntegrationImportService().getIntegrationImportRunEvent(runEventId);
		ValidationUtils.assertNotNull(importRunEvent, "Import run event cannot be null!");
		IntegrationImportRun importRun = importRunEvent.getImportRun();
		ValidationUtils.assertNotNull(importRun, "Import run cannot be null!");
		if (invalidRerunStatus(importRun)) {
			throw new ValidationException("Cannot re-run integration run with status '" + importRun.getStatus() + "'.");
		}
		process(importRun, importRunEvent.getTargetApplication(),  limitToImportRun);
	}


	@Override
	public void rerunIntegrationFile(int integrationFileId, String targetApplicationName) {
		IntegrationTargetApplication targetApplication = null;
		if (targetApplicationName != null) {
			targetApplication = getIntegrationTargetApplicationService().getIntegrationTargetApplicationByName(targetApplicationName);
		}
		process(null, getIntegrationFileService().getIntegrationFile(integrationFileId), targetApplication, false);
	}


	private boolean invalidRerunStatus(IntegrationImportRun run) {
		return !run.getStatus().isError() && run.getStatus().getIntegrationImportStatusType() != IntegrationImportStatusTypes.SUCCESSFUL;
	}


	@Override
	public void uploadIntegrationFile(FileUploadWrapper fileWrapper) {
		try {
			String newFileNameAndPath = getIntegrationFileService().getIntegrationFileAbsolutePath(IntegrationDirectory.INPUT, fileWrapper.getFile().getOriginalFilename());
			ValidationUtils.assertFalse("ktr".equals(FileUtils.getFileExtension(newFileNameAndPath)), "This method does not support uploading transformation files.");
			ValidationUtils.assertFalse(FileUtils.fileExists(newFileNameAndPath), "Cannot upload file [" + fileWrapper.getFile().getOriginalFilename() + "] because it already exists.");

			File file = FileUtils.convertInputStreamToFile(newFileNameAndPath + "." + FILE_NAME_WRITING_EXTENSION, fileWrapper.getFile().getInputStream());
			file.renameTo(new File(newFileNameAndPath));
		}
		catch (Exception e) {
			if (fileWrapper.getFile() != null) {
				throw new RuntimeException("Failed to post file [" + fileWrapper.getFile().getOriginalFilename() + "].", e);
			}
			throw new RuntimeException("Failed to post requested document.", e);
		}
	}


	@Override
	public void uploadFileRevision(FileUploadWrapper file, Integer fileDefinitionId, Date effectiveDate, String holdingAccountNumber, String password) {
		ValidationUtils.assertNotNull(file, "Please choose a file to upload.", "file");
		ValidationUtils.assertNotNull(file.getFile(), "Upload failed.  Please check file name and location.");
		ValidationUtils.assertNotNull(fileDefinitionId, "Please choose a file definition for this statement revision.", "fileDefinitionId");
		ValidationUtils.assertNotNull(effectiveDate, "Please choose a an effective date for this file.", "effectiveDate");

		IntegrationFileDefinition definition = getIntegrationFileService().getIntegrationFileDefinition(fileDefinitionId);

		File tempUploadFile = null;
		try {
			String extension = FileUtils.getFileExtension(file.getFile().getOriginalFilename());

			StringBuilder newFileName = new StringBuilder();
			newFileName.append(DateUtils.fromDate(effectiveDate, DateUtils.FIX_DATE_FORMAT_INPUT));
			newFileName.append('.');

			//Allow Holding Account Number to be null in case the upload is for multiple accounts
			if (holdingAccountNumber != null) {
				newFileName.append(holdingAccountNumber);
				newFileName.append('.');
			}

			String fileDefFileNameWithoutExt = FileUtils.getFileNameWithoutExtension(definition.getFileName());
			if (fileDefFileNameWithoutExt != null) {
				newFileName.append(fileDefFileNameWithoutExt);
			}
			else {
				newFileName.append(definition.getFileName());
			}

			if (!StringUtils.isEmpty(password)) {
				newFileName.append('.');
				newFileName.append(password);
			}

			newFileName.append('.');
			newFileName.append(extension);

			tempUploadFile = new File(FileUtils.combinePath(getSystemFilePathMap().get(IntegrationDirectory.INPROCESS), newFileName.toString()));

			if (!tempUploadFile.createNewFile()) {
				tempUploadFile.delete();

				if (!tempUploadFile.createNewFile()) {
					throw new RuntimeException("File creation failed for File [" + tempUploadFile + "].");
				}
			}

			file.getFile().transferTo(tempUploadFile);

			IntegrationFileArchiveParameters params = new IntegrationFileArchiveParameters();
			params.setFileToArchive(tempUploadFile);
			params.setDefinition(definition);
			params.setEffectiveDate(effectiveDate);
			params.setFileSourceType(IntegrationFileSourceType.MANUAL);

			getIntegrationFileArchiveService().archiveIntegrationFile(params);
		}
		catch (IOException e) {
			throw new RuntimeException("File creation or file transfer failed for File [" + tempUploadFile + "].", e);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to upload requested document.", e);
		}
		finally {
			if (tempUploadFile != null && tempUploadFile.exists()) {
				tempUploadFile.delete();
			}
		}
	}


	@Override
	public void uploadInstructionFileRevision(FileUploadWrapper file, Integer instructionId, String recipientCompanyName, String custodianAccountName, Date effectiveDate) {
		ValidationUtils.assertNotNull(file, "Please choose a file to upload.", "file");
		ValidationUtils.assertNotNull(file.getFile(), "Upload failed.  Please check file name and location.");
		ValidationUtils.assertNotNull(recipientCompanyName, "Please Select a Recipient Company.", "recipientCompanyId");
		ValidationUtils.assertNotNull(effectiveDate, "Please choose a an effective date for this file.", "effectiveDate");

		String instructionNumber = instructionId != null ? instructionId.toString() : "UNKNOWN";

		File tempUploadFile = null;
		try {
			StringBuilder newFileName = new StringBuilder(recipientCompanyName);
			newFileName.append("_");
			newFileName.append(instructionNumber);
			newFileName.append("_");
			newFileName.append(!StringUtils.isEmpty(custodianAccountName) ? FileUtils.replaceInvalidCharacters(custodianAccountName, "-") : "MANUAL");
			newFileName.append(".");
			newFileName.append(FileUtils.getFileExtension(file.getFile().getOriginalFilename()));

			tempUploadFile = new File(FileUtils.combinePath(getSystemFilePathMap().get(IntegrationDirectory.INPROCESS), newFileName.toString()));

			if (!tempUploadFile.createNewFile()) {
				tempUploadFile.delete();

				if (!tempUploadFile.createNewFile()) {
					throw new RuntimeException("File creation failed for File [" + tempUploadFile + "].");
				}
			}

			file.getFile().transferTo(tempUploadFile);

			IntegrationFileArchiveParameters params = new IntegrationFileArchiveParameters();
			params.setFileToArchive(tempUploadFile);
			params.setEffectiveDate(effectiveDate);
			params.setFileSourceType(IntegrationFileSourceType.MANUAL);

			//Set the Archive Strategy
			IntegrationFileArchiveStrategySearchForm strategySearchForm = new IntegrationFileArchiveStrategySearchForm();
			strategySearchForm.setName("Investment Instruction Archive Strategy");
			params.setArchiveStrategy(CollectionUtils.getFirstElementStrict(getIntegrationFileArchiveService().getIntegrationFileArchiveStrategyList(strategySearchForm)));
			params.setBaseArchivePath("Operations");

			//Set the Import Source
			IntegrationSourceSearchForm sourceSearchForm = new IntegrationSourceSearchForm();
			sourceSearchForm.setName("IMS_INSTRUCTION");
			params.setImportSource(CollectionUtils.getFirstElementStrict(getIntegrationSourceService().getIntegrationSourceList(sourceSearchForm)));

			//Set the Additional Parameters
			Map<String, Object> additionalParams = new HashMap<>();
			additionalParams.put(InvestmentInstructionArchiveFilePathGenerator.RECIPIENT_COMPANY_NAME_PARAMETER, recipientCompanyName);
			params.setAdditionalProperties(additionalParams);

			getIntegrationFileArchiveService().archiveIntegrationFile(params);
		}
		catch (IOException e) {
			throw new RuntimeException("File creation or file transfer failed for File [" + tempUploadFile + "].", e);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to upload requested document.", e);
		}
		finally {
			if (tempUploadFile != null && tempUploadFile.exists()) {
				tempUploadFile.delete();
			}
		}
	}


	private void process(IntegrationImportRun importRun, IntegrationTargetApplication targetApplication, Boolean limitToImportRun) {
		ValidationUtils.assertNotNull(importRun, "Import run cannot be null!");
		process(importRun, importRun.getFile(), targetApplication, limitToImportRun);
	}


	private void process(IntegrationImportRun importRun, IntegrationFile integrationFile, IntegrationTargetApplication targetApplication, Boolean limitToImportRun) {
		AssertUtils.assertFalse(integrationFile.isDeleted(), "Cannot reprocess a deleted file.");
		AssertUtils.assertFalse(integrationFile.isSourceDataDeleted(), "Cannot reprocess a file for which the source data has been deleted.");

		final File file = new File(getIntegrationFileService().getIntegrationFileAbsolutePath(integrationFile));
		Message<File> msg = MessageBuilder.withPayload(file)
				.setHeader("EffectiveDate", importRun != null ? importRun.getEffectiveDate() : null)
				.setHeader("IntegrationImportRun", importRun)
				.setHeader("IntegrationFile", integrationFile)
				.setHeader("TargetApplication", targetApplication)
				.setHeader("LimitToImportRun", limitToImportRun)
				.build();
		sendMessageAsynchronously(msg);
	}


	private void sendMessageAsynchronously(final Message<File> message) {
		try {

			final MessageChannel channel = getIntegrationFileProcessingChannel();
			Runnable runner = () -> channel.send(message);
			getExecutorService().execute(runner);
		}
		catch (Throwable e) {
			LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Error occurred while asynchronously sending Integration Request message " + message.toString()));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////      IntegrationImportStatus Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	@ValueIgnoringGetter
	public ExecutorService getExecutorService() {
		if (this.executorService == null) {
			this.executorService = Executors.newCachedThreadPool();
		}
		return this.executorService;
	}


	public void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
	}


	public Map<IntegrationDirectory, String> getSystemFilePathMap() {
		return this.systemFilePathMap;
	}


	public void setSystemFilePathMap(Map<IntegrationDirectory, String> systemFilePathMap) {
		this.systemFilePathMap = systemFilePathMap;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public IntegrationSourceService getIntegrationSourceService() {
		return this.integrationSourceService;
	}


	public void setIntegrationSourceService(IntegrationSourceService integrationSourceService) {
		this.integrationSourceService = integrationSourceService;
	}


	public IntegrationTargetApplicationService getIntegrationTargetApplicationService() {
		return this.integrationTargetApplicationService;
	}


	public void setIntegrationTargetApplicationService(IntegrationTargetApplicationService integrationTargetApplicationService) {
		this.integrationTargetApplicationService = integrationTargetApplicationService;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public IntegrationFileArchiveService getIntegrationFileArchiveService() {
		return this.integrationFileArchiveService;
	}


	public void setIntegrationFileArchiveService(IntegrationFileArchiveService integrationFileArchiveService) {
		this.integrationFileArchiveService = integrationFileArchiveService;
	}


	public MessageChannel getIntegrationFileProcessingChannel() {
		return this.integrationFileProcessingChannel;
	}


	public void setIntegrationFileProcessingChannel(MessageChannel integrationFileProcessingChannel) {
		this.integrationFileProcessingChannel = integrationFileProcessingChannel;
	}
}
