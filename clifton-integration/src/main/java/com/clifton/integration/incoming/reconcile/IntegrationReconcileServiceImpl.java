package com.clifton.integration.incoming.reconcile;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcileAccountBalancesByAccountTypeSearchForm;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcileAccountM2MDailyExpenseSearchForm;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcileDailyPNLHistorySearchForm;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcilePositionHistorySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationReconcileServiceImpl implements IntegrationReconcileService {

	private AdvancedReadOnlyDAO<IntegrationReconcilePositionHistory, Criteria> integrationReconcilePositionHistoryDAO;
	private AdvancedReadOnlyDAO<IntegrationReconcileAccountBalancesByAccountType, Criteria> integrationReconcileAccountBalancesByAccountTypeDAO;
	private AdvancedReadOnlyDAO<IntegrationReconcileAccountM2MDailyExpense, Criteria> integrationReconcileAccountM2MDailyExpenseDAO;
	private AdvancedReadOnlyDAO<IntegrationReconcileDailyPNLHistory, Criteria> integrationReconcileDailyPNLHistoryDAO;


	////////////////////////////////////////////////////////////////////////////////////
	////////    IntegrationReconcilePositionHistory Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////////////
	@Override
	public List<IntegrationReconcilePositionHistory> getIntegrationReconcilePositionHistoryList(IntegrationReconcilePositionHistorySearchForm searchForm) {
		return getIntegrationReconcilePositionHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////    IntegrationReconcileAccountBalancesByAccountType Business Methods       /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public List<IntegrationReconcileAccountBalancesByAccountType> getIntegrationReconcileAccountBalancesByAccountTypeList(IntegrationReconcileAccountBalancesByAccountTypeSearchForm searchForm) {
		return getIntegrationReconcileAccountBalancesByAccountTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////    IntegrationReconcileAccountM2MDailyExpense Business Methods       /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<IntegrationReconcileAccountM2MDailyExpense> getIntegrationReconcileAccountM2MDailyExpenseList(IntegrationReconcileAccountM2MDailyExpenseSearchForm searchForm) {
		return getIntegrationReconcileAccountM2MDailyExpenseDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<IntegrationReconcileAccountM2MDailyExpense> getIntegrationReconcileAccountM2MDailyExpenseListByRun(int runId) {
		IntegrationReconcileAccountM2MDailyExpenseSearchForm searchForm = new IntegrationReconcileAccountM2MDailyExpenseSearchForm();
		searchForm.setRunId(runId);

		return getIntegrationReconcileAccountM2MDailyExpenseList(searchForm);
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////           IntegrationReconcileDailyPNLHistory Business Methods             /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public List<IntegrationReconcileDailyPNLHistory> getIntegrationReconcileDailyPNLHistoryList(IntegrationReconcileDailyPNLHistorySearchForm searchForm) {
		return getIntegrationReconcileDailyPNLHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////
	public AdvancedReadOnlyDAO<IntegrationReconcilePositionHistory, Criteria> getIntegrationReconcilePositionHistoryDAO() {
		return this.integrationReconcilePositionHistoryDAO;
	}


	public void setIntegrationReconcilePositionHistoryDAO(AdvancedReadOnlyDAO<IntegrationReconcilePositionHistory, Criteria> integrationReconcilePositionHistoryDAO) {
		this.integrationReconcilePositionHistoryDAO = integrationReconcilePositionHistoryDAO;
	}


	public AdvancedReadOnlyDAO<IntegrationReconcileAccountBalancesByAccountType, Criteria> getIntegrationReconcileAccountBalancesByAccountTypeDAO() {
		return this.integrationReconcileAccountBalancesByAccountTypeDAO;
	}


	public void setIntegrationReconcileAccountBalancesByAccountTypeDAO(
			AdvancedReadOnlyDAO<IntegrationReconcileAccountBalancesByAccountType, Criteria> integrationReconcileAccountBalancesByAccountTypeDAO) {
		this.integrationReconcileAccountBalancesByAccountTypeDAO = integrationReconcileAccountBalancesByAccountTypeDAO;
	}


	public AdvancedReadOnlyDAO<IntegrationReconcileDailyPNLHistory, Criteria> getIntegrationReconcileDailyPNLHistoryDAO() {
		return this.integrationReconcileDailyPNLHistoryDAO;
	}


	public void setIntegrationReconcileDailyPNLHistoryDAO(AdvancedReadOnlyDAO<IntegrationReconcileDailyPNLHistory, Criteria> integrationReconcileDailyPNLHistoryDAO) {
		this.integrationReconcileDailyPNLHistoryDAO = integrationReconcileDailyPNLHistoryDAO;
	}


	public AdvancedReadOnlyDAO<IntegrationReconcileAccountM2MDailyExpense, Criteria> getIntegrationReconcileAccountM2MDailyExpenseDAO() {
		return this.integrationReconcileAccountM2MDailyExpenseDAO;
	}


	public void setIntegrationReconcileAccountM2MDailyExpenseDAO(AdvancedReadOnlyDAO<IntegrationReconcileAccountM2MDailyExpense, Criteria> integrationReconcileAccountM2MDailyExpenseDAO) {
		this.integrationReconcileAccountM2MDailyExpenseDAO = integrationReconcileAccountM2MDailyExpenseDAO;
	}
}
