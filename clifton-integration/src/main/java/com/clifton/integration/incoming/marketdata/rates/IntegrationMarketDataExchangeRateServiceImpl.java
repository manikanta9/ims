package com.clifton.integration.incoming.marketdata.rates;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.integration.incoming.marketdata.rates.search.IntegrationMarketDataExchangeRateSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>IntegrationMarketDataExchangeRateServiceImpl</code> is an implementation of IntegrationMarketDataExchangeRateService
 *
 * @author rbrooks
 */
@Service
public class IntegrationMarketDataExchangeRateServiceImpl implements IntegrationMarketDataExchangeRateService {

	private AdvancedReadOnlyDAO<IntegrationMarketDataExchangeRate, Criteria> integrationMarketDataExchangeRateDAO;


	@Override
	public List<IntegrationMarketDataExchangeRate> getIntegrationMarketDataExchangeRateList(IntegrationMarketDataExchangeRateSearchForm searchForm) {
		return getIntegrationMarketDataExchangeRateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	public AdvancedReadOnlyDAO<IntegrationMarketDataExchangeRate, Criteria> getIntegrationMarketDataExchangeRateDAO() {
		return this.integrationMarketDataExchangeRateDAO;
	}


	public void setIntegrationMarketDataExchangeRateDAO(AdvancedReadOnlyDAO<IntegrationMarketDataExchangeRate, Criteria> integrationMarketDataExchangeRateDAO) {
		this.integrationMarketDataExchangeRateDAO = integrationMarketDataExchangeRateDAO;
	}
}
