package com.clifton.integration.incoming.business;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.incoming.business.search.IntegrationBusinessCompanySearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author mwacker
 */
@Service
public class IntegrationBusinessCompanyServiceImpl implements IntegrationBusinessCompanyService {

	private AdvancedReadOnlyDAO<IntegrationBusinessCompany, Criteria> integrationBusinessCompanyDAO;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationBusinessCompany getIntegrationBusinessCompany(int id) {
		return getIntegrationBusinessCompanyDAO().findByPrimaryKey(id);
	}


	/**
	 * The Integration Business Company Table Column BloombergCompanyId is unique, the restriction for returning one entity only is valid for this table.
	 */
	@Override
	public IntegrationBusinessCompany getIntegrationBusinessCompanyByBloombergCompanyId(int bloombergCompanyId) {
		IntegrationBusinessCompanySearchForm searchForm = new IntegrationBusinessCompanySearchForm();
		searchForm.setBloombergCompanyIdentifier(bloombergCompanyId);
		return CollectionUtils.getOnlyElement(getIntegrationBusinessCompanyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm)));
	}


	@Override
	public List<IntegrationBusinessCompany> getIntegrationBusinessCompanyList(IntegrationBusinessCompanySearchForm searchForm) {
		return getIntegrationBusinessCompanyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<IntegrationBusinessCompany, Criteria> getIntegrationBusinessCompanyDAO() {
		return this.integrationBusinessCompanyDAO;
	}


	public void setIntegrationBusinessCompanyDAO(AdvancedReadOnlyDAO<IntegrationBusinessCompany, Criteria> integrationBusinessCompanyDAO) {
		this.integrationBusinessCompanyDAO = integrationBusinessCompanyDAO;
	}
}
