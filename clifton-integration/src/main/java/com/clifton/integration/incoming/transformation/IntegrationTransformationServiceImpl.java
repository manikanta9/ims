package com.clifton.integration.incoming.transformation;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.transformation.search.IntegrationTransformationFileSearchForm;
import com.clifton.integration.incoming.transformation.search.IntegrationTransformationSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author mwacker
 */
@Service
public class IntegrationTransformationServiceImpl implements IntegrationTransformationService {

	private AdvancedUpdatableDAO<IntegrationTransformation, Criteria> integrationTransformationDAO;
	private AdvancedUpdatableDAO<IntegrationTransformationFile, Criteria> integrationTransformationFileDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationTransformation getIntegrationTransformation(int id) {
		return getIntegrationTransformationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationTransformation> getIntegrationTransformationList(IntegrationTransformationSearchForm searchForm) {
		return getIntegrationTransformationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void deleteIntegrationTransformation(int id) {
		getIntegrationTransformationDAO().delete(id);
	}


	@Override
	public IntegrationTransformation saveIntegrationTransformation(IntegrationTransformation bean) {
		ValidationUtils.assertFalse(bean.isSystemDefined(), "System defined transformations may not be edited.");
		return getIntegrationTransformationDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationTransformationFile getIntegrationTransformationFile(int id) {
		return getIntegrationTransformationFileDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationTransformationFile> getIntegrationTransformationFileListForTransformation(int transformationId) {
		IntegrationTransformationFileSearchForm searchForm = new IntegrationTransformationFileSearchForm();
		searchForm.setTransformationId(transformationId);
		searchForm.setRunnable(true);

		searchForm.setOrderBy("order:asc");
		return getIntegrationTransformationFileList(searchForm);
	}


	@Override
	public List<IntegrationTransformationFile> getIntegrationTransformationFileList(IntegrationTransformationFileSearchForm searchForm) {
		return getIntegrationTransformationFileDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void deployIntegrationTransformationFile(int id) {
		IntegrationTransformationFile file = getIntegrationTransformationFile(id);
		file.setUpdateRequired(true);
		saveIntegrationTransformationFile(file);
	}


	@Override
	public void deleteIntegrationTransformationFile(int id) {
		getIntegrationTransformationFileDAO().delete(id);
	}


	@Override
	public IntegrationTransformationFile saveIntegrationTransformationFile(IntegrationTransformationFile bean) {
		if (bean.getTransformation() == null) {
			ValidationUtils.assertFalse(bean.getFileType().isTransformationRequired(), "Transformation is required for transformation file type [" + bean.getFileType() + "].");
		}
		else {
			ValidationUtils.assertTrue(bean.getFileType() != IntegrationTransformationFileTypes.EFFECTIVE_DATE_TRANSFORMATION, "Transformation cannot be set for transformation file type [" + bean.getFileType() + "].");
			ValidationUtils.assertFalse(bean.getTransformation().isSystemDefined(), "This transformation file is associated with a System Defined transformation and cannot be edited.");
			if (bean.getTransformation().getType().getRequiredTransformationFileType() != null) {
				ValidationUtils.assertTrue(bean.getTransformation().getType().getRequiredTransformationFileType() == bean.getFileType(), "Transformations of type [" + bean.getTransformation().getType() + "] requires transformation files of type [" + bean.getTransformation().getType().getRequiredTransformationFileType() + "].  The current type is [" + bean.getFileType() + "].");
			}
		}
		if (bean.getFileType() == IntegrationTransformationFileTypes.DATA_FILE) {
			ValidationUtils.assertFalse(bean.isRunnable(), "Data files cannot be marked as runnable.");
		}
		return getIntegrationTransformationFileDAO().save(bean);
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<IntegrationTransformationFile, Criteria> getIntegrationTransformationFileDAO() {
		return this.integrationTransformationFileDAO;
	}


	public void setIntegrationTransformationFileDAO(AdvancedUpdatableDAO<IntegrationTransformationFile, Criteria> integrationTransformationFileDAO) {
		this.integrationTransformationFileDAO = integrationTransformationFileDAO;
	}


	public AdvancedUpdatableDAO<IntegrationTransformation, Criteria> getIntegrationTransformationDAO() {
		return this.integrationTransformationDAO;
	}


	public void setIntegrationTransformationDAO(AdvancedUpdatableDAO<IntegrationTransformation, Criteria> integrationTransformationDAO) {
		this.integrationTransformationDAO = integrationTransformationDAO;
	}
}
