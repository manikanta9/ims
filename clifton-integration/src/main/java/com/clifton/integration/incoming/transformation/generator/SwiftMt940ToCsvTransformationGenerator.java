package com.clifton.integration.incoming.transformation.generator;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.swift.SwiftMt940ToCsvConverter;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import java.util.UUID;


/**
 * @author TerryS
 */
public class SwiftMt940ToCsvTransformationGenerator implements ImportTransformationGenerator<File, File> {

	private boolean includeStatementLines;


	@Override
	public File generateTransformation(File input, Integer runId, IntegrationImportDefinition definition, Map<String, String> context) {
		SwiftMt940ToCsvConverter converter = new SwiftMt940ToCsvConverter();
		converter.setIncludeStatementLines(isIncludeStatementLines());
		File csvOutputFile = createNewTempFileName(input);
		try (Reader swift = new FileReader(input); Writer csv = new FileWriter(csvOutputFile)) {
			converter.output(swift, csv);
		}
		catch (Exception e) {
			throw new RuntimeException("Could not run swift to csv conversion", e);
		}
		return csvOutputFile;
	}


	private File createNewTempFileName(File originalInputFile) {
		String newTempFileName = FileUtils.getFileNameWithoutExtension(originalInputFile.getAbsolutePath()) + "." + UUID.randomUUID() + "." + FileUtils.getFileExtension(originalInputFile.getName());
		return new File(newTempFileName);
	}


	@Override
	public Class<File> getInputClass() {
		return File.class;
	}


	@Override
	public Class<File> getOutputClass() {
		return File.class;
	}


	public boolean isIncludeStatementLines() {
		return this.includeStatementLines;
	}


	public void setIncludeStatementLines(boolean includeStatementLines) {
		this.includeStatementLines = includeStatementLines;
	}
}
