package com.clifton.integration.incoming.definition;


import com.clifton.core.cache.TimedEvictionCache;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionTypeColumnSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionTypeSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunEventSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportStatusSearchForm;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFileTypes;
import com.clifton.integration.incoming.transformation.IntegrationTransformationTypes;
import com.clifton.integration.target.IntegrationTargetApplication;
import com.clifton.integration.target.IntegrationTargetApplicationService;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Service
public class IntegrationImportServiceImpl implements IntegrationImportService {

	private static final int RUN_AGE_MINUTES = 30;

	private AdvancedUpdatableDAO<IntegrationImportDefinition, Criteria> integrationImportDefinitionDAO;
	private AdvancedUpdatableDAO<IntegrationImportRun, Criteria> integrationImportRunDAO;
	private AdvancedUpdatableDAO<IntegrationImportRunEvent, Criteria> integrationImportRunEventDAO;
	private AdvancedUpdatableDAO<IntegrationImportStatus, Criteria> integrationImportStatusDAO;
	private AdvancedUpdatableDAO<IntegrationImportDefinitionType, Criteria> integrationImportDefinitionTypeDAO;
	private AdvancedUpdatableDAO<IntegrationImportDefinitionTypeColumn, Criteria> integrationImportDefinitionTypeColumnDAO;
	private AdvancedUpdatableDAO<IntegrationImportDefinitionColumnMapping, Criteria> integrationImportDefinitionColumnMappingDAO;

	private DaoNamedEntityCache<IntegrationImportStatus> integrationImportStatusCache;
	private DaoSingleKeyListCache<IntegrationImportDefinitionType, String> integrationImportDefinitionTypeByEventNameCache;

	private IntegrationTargetApplicationService integrationTargetApplicationService;
	private SecurityAuthorizationService securityAuthorizationService;

	private TimedEvictionCache<IntegrationImportRunEvent> integrationRunEventTimedEvictionCache;

	////////////////////////////////////////////////////////////////////////////
	////////    IntegrationImportDefinition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationImportDefinition getIntegrationImportDefinition(int id) {
		return populateImportDefinition(getIntegrationImportDefinitionDAO().findByPrimaryKey(id));
	}


	@Override
	public IntegrationImportDefinition getIntegrationImportDefinitionByName(String name) {
		return populateImportDefinition(getIntegrationImportDefinitionDAO().findOneByField("name", name));
	}


	@Override
	public IntegrationImportDefinition getIntegrationImportDefinitionByFileName(String inputFileName) {
		return populateImportDefinition(getIntegrationImportDefinitionDAO().findOneByField("inputFileName", inputFileName));
	}


	@Override
	public List<IntegrationImportDefinition> getIntegrationImportDefinitionListByFileDefinition(int fileDefinitionId) {
		List<IntegrationImportDefinition> definitions = getIntegrationImportDefinitionDAO().findByField("fileDefinition.id", fileDefinitionId);
		for (IntegrationImportDefinition definition : CollectionUtils.getIterable(definitions)) {
			populateImportDefinition(definition);
		}

		return definitions;
	}


	@Override
	public List<IntegrationImportDefinition> getIntegrationImportDefinitionList(IntegrationImportDefinitionSearchForm searchForm) {
		return getIntegrationImportDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private IntegrationImportDefinition populateImportDefinition(IntegrationImportDefinition definition) {
		if (definition != null) {
			List<IntegrationImportDefinitionColumnMapping> columnMappings = getIntegrationImportDefinitionColumnMappingDAO().findByField("importDefinition.id", definition.getId());
			definition.setColumnMappings(columnMappings);
		}

		return definition;
	}


	/**
	 * @see com.clifton.integration.incoming.definition.validation.IntegrationImportDefinitionValidator
	 */
	@Override
	@Transactional
	public IntegrationImportDefinition saveIntegrationImportDefinition(IntegrationImportDefinition bean) {
		List<IntegrationImportDefinitionColumnMapping> originalMappingList = null;
		if (!bean.isNewBean()) {
			originalMappingList = getIntegrationImportDefinitionColumnMappingDAO().findByField("importDefinition.id", bean.getId());
		}

		List<IntegrationImportDefinitionColumnMapping> newMappingList = bean.getColumnMappings();
		if (newMappingList != null) {
			for (IntegrationImportDefinitionColumnMapping mapping : CollectionUtils.getIterable(newMappingList)) {
				mapping.setImportDefinition(bean);
			}
		}

		if (!bean.isDisabled()) {
			ValidationUtils.assertNotNull(bean.getTransformation(), "Transformation is required for active imports.  Select a transformation or disable the import to proceed.");
		}
		if (bean.getEffectiveDateTransformationFile() != null) {
			ValidationUtils.assertTrue(bean.getEffectiveDateTransformationFile().getFileType() == IntegrationTransformationFileTypes.EFFECTIVE_DATE_TRANSFORMATION, "Cannot set effective date file to [" + bean.getEffectiveDateTransformationFile() + "] because it must be of type [" + IntegrationTransformationFileTypes.EFFECTIVE_DATE_TRANSFORMATION + "].");
		}
		if (bean.getFileTransformation() != null) {
			ValidationUtils.assertTrue(IntegrationTransformationTypes.FILE_TRANSFORMATION == bean.getFileTransformation().getType(), "Transformation type of [" + bean.getFileTransformation().getType() + "] is not support for file transformations.  Type must be [" + IntegrationTransformationTypes.FILE_TRANSFORMATION + "].", "fileTransformation");
		}
		if (bean.isTestDefinition()) {
			ValidationUtils.assertTrue(getSecurityAuthorizationService().isSecurityUserAdmin(), "Only Administrators may make changes to Test Import Definitions");
		}
		IntegrationImportDefinition importDefinition = getIntegrationImportDefinitionDAO().save(bean);
		getIntegrationImportDefinitionColumnMappingDAO().saveList(newMappingList, originalMappingList);
		return importDefinition;
	}


	@Override
	public void deleteIntegrationImportDefinition(int id) {
		getIntegrationImportDefinitionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	///   IntegrationImportDefinitionTypeColumn History Business Methods   /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<IntegrationImportDefinitionTypeColumn> getIntegrationImportDefinitionTypeColumnList(IntegrationImportDefinitionTypeColumnSearchForm searchForm) {
		return getIntegrationImportDefinitionTypeColumnDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////     IntegrationImportRun History Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationImportRun getIntegrationImportRun(int id) {
		return getIntegrationImportRunDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationImportRun> getIntegrationImportRunList(final IntegrationImportRunSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getImportEventNameEquals())) {
			// rely on caching to avoid extra joins and improve performance
			Serializable[] typeIds = getIntegrationImportDefinitionTypeByEventNameCache().getBeanIdListForKeyValue(getIntegrationImportDefinitionTypeDAO(), searchForm.getImportEventNameEquals());
			if (typeIds == null || typeIds.length == 0) {
				return null;
			}
			else if (typeIds.length == 1) {
				searchForm.setTypeId((Integer) typeIds[0]);
			}
			else {
				searchForm.setTypeIds(ArrayUtils.toIntegerArray(typeIds));
			}
			searchForm.setImportEventNameEquals(null);
		}

		return getIntegrationImportRunDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationImportRun saveIntegrationImportRun(IntegrationImportRun importRun) {
		//IntegrationImportRun are issued to multiple apps asynchronously so we need to ensure if one system errors that we keep the error status.
		if (!importRun.isNewBean()) {
			IntegrationImportRun existingImportRun = getIntegrationImportRun(importRun.getId());
			if (existingImportRun != null && existingImportRun.getStatus().getIntegrationImportStatusType().isError()) {
				importRun.setStatus(existingImportRun.getStatus());
			}
		}
		return getIntegrationImportRunDAO().save(importRun);
	}


	@Override
	@Transactional
	public void deleteIntegrationImportRun(int id) {
		IntegrationImportRunEventSearchForm searchForm = new IntegrationImportRunEventSearchForm();
		searchForm.setImportRunId(id);
		for (IntegrationImportRunEvent runEvent : CollectionUtils.getIterable(getIntegrationImportRunEventList(searchForm))) {
			getIntegrationImportRunEventDAO().delete(runEvent.getId());
		}
		getIntegrationImportRunDAO().delete(id);
	}


	@Override
	@Transactional
	public void fixIntegrationImportRun(Integer[] runIds) {
		Throwable firstCause = null;
		for (Integer runId : runIds) {
			try {
				IntegrationImportRun run = getIntegrationImportRun(runId);
				ValidationUtils.assertNotNull(run, "Cannot find integration run with id = " + runId);
				fixIntegrationImportRun(run);
			}
			catch (ValidationException e) {
				if (firstCause == null) {
					firstCause = e;
				}
			}
		}
		if (firstCause != null) {
			throw new ValidationException("Error fixing one or more Import Runs:  " + firstCause.getMessage());
		}
	}


	private IntegrationImportRun fixIntegrationImportRun(IntegrationImportRun run) {
		ValidationUtils.assertNotNull(run, "Run cannot be null!");

		validateFixAllowed(run.getId(), run.getStatus(), run.getStartProcessDate(), false);

		boolean hasErrors = false;
		IntegrationImportRunEventSearchForm searchForm = new IntegrationImportRunEventSearchForm();
		searchForm.setImportRunId(run.getId());
		for (IntegrationImportRunEvent runEvent : CollectionUtils.getIterable(getIntegrationImportRunEventList(searchForm))) {
			runEvent = fixIntegrationImportRunEvent(runEvent, false);
			if (runEvent.getStatus().isError()) {
				hasErrors = true;
			}
		}
		return updateIntegrationImportRun(run, hasErrors ? "This run was determined to be stuck in RUNNING status.  Status manually changed from RUNNING to ERROR." : null);
	}


	private IntegrationImportRun updateIntegrationImportRun(IntegrationImportRun run, String error) {
		AssertUtils.assertNotNull(run, "Run cannot be null!");
		AssertUtils.assertNotNull(run.getId(), "You cannot update the status of a run that has not yet been saved!");
		if (error == null) {
			//Ensure success status is set if no error is set.
			run.setStatus(getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL));
		}
		else {
			//Truncate the error message if necessary
			if (error.length() > DataTypes.DESCRIPTION_LONG.getLength()) {
				error = error.substring(0, DataTypes.DESCRIPTION_LONG.getLength() - 4).concat("...");
			}
			run.setError(error);
			run.setStatus(getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR));
		}
		if (run.getEndProcessDate() == null) {
			run.setEndProcessDate(new Date());
		}
		return saveIntegrationImportRun(run);
	}

	////////////////////////////////////////////////////////////////////////////
	////////     IntegrationImportRunEvent Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationImportRunEvent getIntegrationImportRunEvent(long id) {
		return getIntegrationImportRunEventDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationImportRunEvent> getIntegrationImportRunEventList(IntegrationImportRunEventSearchForm searchForm) {

		if (searchForm.isSearchRestrictionSetAny("importEventName", "importEventNameEquals")) {
			// rely on caching to avoid extra joins and improve performance
			String fName = searchForm.isSearchRestrictionSet("importEventName") ? "importEventName" : "importEventNameEquals";
			Serializable[] typeIds = getIntegrationImportDefinitionTypeByEventNameCache().getBeanIdListForKeyValue(getIntegrationImportDefinitionTypeDAO(), (String) searchForm.getSearchRestrictionValue(fName));
			if (typeIds != null && typeIds.length == 0) {
				typeIds = null;
			}
			ValidationUtils.assertNotNull(typeIds, () -> fName + " '" + searchForm.getSearchRestrictionValue(fName) + "' does not exist.");
			if (typeIds != null) {
				if (typeIds.length == 1) {
					searchForm.setTypeId((Integer) typeIds[0]);
				}
				else {
					searchForm.setTypeIds(ArrayUtils.toIntegerArray(typeIds));
				}
			}
			searchForm.removeSearchRestrictionAndBeanField(fName);
		}

		if (searchForm.isSearchRestrictionSet("targetApplicationName")) {
			IntegrationTargetApplication targetApplication = getIntegrationTargetApplicationService().getIntegrationTargetApplicationByName((String) searchForm.getSearchRestrictionValue("targetApplicationName"));
			ValidationUtils.assertNotNull(targetApplication, () -> "targetApplicationName '" + searchForm.getSearchRestrictionValue("targetApplicationName") + "' does not exist.");
			searchForm.setTargetApplicationId(targetApplication.getId());
			searchForm.removeSearchRestrictionAndBeanField("targetApplicationName");
		}
		return getIntegrationImportRunEventDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationImportRunEvent saveIntegrationImportRunEvent(IntegrationImportRunEvent runEvent) {
		return getIntegrationImportRunEventDAO().save(runEvent);
	}


	@Override
	public void deleteIntegrationImportRunEvent(long id) {
		getIntegrationImportRunEventDAO().delete(id);
	}


	@Override
	@Transactional
	public void fixIntegrationImportRunEvent(Long[] runEventIds) {
		Throwable firstCause = null;
		for (Long runEventId : runEventIds) {
			try {
				IntegrationImportRunEvent runEvent = getIntegrationImportRunEvent(runEventId);
				ValidationUtils.assertNotNull(runEvent, "Cannot find integration run event with id = " + runEventId);
				fixIntegrationImportRunEvent(runEvent, true);
			}
			catch (ValidationException e) {
				if (firstCause == null) {
					firstCause = e;
				}
			}
		}

		if (firstCause != null) {
			throw new ValidationException("Error fixing one or more Import Run events:  " + firstCause.getMessage());
		}
	}


	/**
	 * Because fixIntegrationImportRunEvent is called from 'fixIntegrationImportRun' we pass a boolean flag to determine
	 * if validation should be executed to see if the run can be fixed; this is because one event may be running, and another
	 * may be complete (error or success); since we already validated the run has been going for longer than a minute, then
	 * we want to bypass the check on the status, and instead simply update if in running status, or otherwise just return.
	 */
	private IntegrationImportRunEvent fixIntegrationImportRunEvent(IntegrationImportRunEvent runEvent, boolean validate) {
		ValidationUtils.assertNotNull(runEvent, "Run event may not be null!");
		if (validate) {
			validateFixAllowed(runEvent.getId(), runEvent.getStatus(), runEvent.getImportRun().getStartProcessDate(), true);
		}
		if (runEvent.getStatus().getIntegrationImportStatusType() == IntegrationImportStatusTypes.RUNNING) {
			//When manually fixing a run; we set the targetApplication to 'INTEGRATION' since it is unknown which application forced the fix.
			runEvent = updateIntegrationImportRunEvent(runEvent.getId(), IntegrationImportStatusTypes.ERROR, "This run event was determined to be stuck in RUNNING status.  Status manually changed from RUNNING to ERROR.");
			//Since we manually fixed it; just remove from the eviction cache manually.
			getIntegrationRunEventTimedEvictionCache().remove(runEvent.getId());
		}
		return runEvent;
	}


	/**
	 * Called by external applications to update a runEvents status after processing.
	 * No provided statusType or error parameter values, the status will default to SUCCESS.
	 *
	 * @param runEventId Required.  Must reference an existing {@link IntegrationImportRunEvent}
	 * @param statusType Not Required.  If not provided, the status will be SUCCESS or FAILURE based the error parameter.
	 *                   ERROR statuses do not require an error parameter.
	 * @param error      Not Required.  If provided, the status depends on statusType.
	 */
	@Override
	public IntegrationImportRunEvent updateIntegrationImportRunEvent(long runEventId, IntegrationImportStatusTypes statusType, String error) {
		IntegrationImportRunEvent runEvent = getIntegrationImportRunEvent(runEventId);
		AssertUtils.assertNotNull(runEvent, "Unable to locate run event with id: " + runEventId);

		if (statusType != null) {
			// priority on provided statusType
			runEvent.setStatus(getIntegrationImportStatusByStatusType(statusType));
		}
		else if (error != null) {
			// if there is an error message, assume error status
			runEvent.setStatus(getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR));
		}
		else {
			// no status or error, assume successful
			runEvent.setStatus(getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL));
		}

		// always updated the error message when provided.
		if (error != null) {
			// Truncate the error message if necessary
			if (error.length() > DataTypes.DESCRIPTION_LONG.getLength()) {
				error = error.substring(0, DataTypes.DESCRIPTION_LONG.getLength() - 4).concat("...");
			}
			runEvent.setError(error);
		}

		runEvent = saveIntegrationImportRunEvent(runEvent);
		getIntegrationRunEventTimedEvictionCache().evict(runEvent.getId());

		return runEvent;
	}


	////////////////////////////////////////////////////////////////////////////
	////////      IntegrationImportStatus Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationImportStatus getIntegrationImportStatus(short id) {
		return getIntegrationImportStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationImportStatus getIntegrationImportStatusByName(String name) {
		return getIntegrationImportStatusCache().getBeanForKeyValueStrict(getIntegrationImportStatusDAO(), name);
	}


	@Override
	public IntegrationImportStatus getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes statusType) {
		return getIntegrationImportStatusByName(statusType.name());
	}


	@Override
	public List<IntegrationImportStatus> getIntegrationImportStatusList(IntegrationImportStatusSearchForm searchForm) {
		return getIntegrationImportStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationImportStatus saveIntegrationImportStatus(IntegrationImportStatus importStatus) {
		return getIntegrationImportStatusDAO().save(importStatus);
	}


	@Override
	public void deleteIntegrationImportStatus(short id) {
		getIntegrationImportStatusDAO().delete(id);
	}

	//////////////////////////////////////////////////////////////////////////////
	////////    IntegrationImportDefinitionType Business Methods     /////////////
	//////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationImportDefinitionType getIntegrationImportDefinitionType(int id) {
		return getIntegrationImportDefinitionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationImportDefinitionType> getIntegrationImportDefinitionTypeList(IntegrationImportDefinitionTypeSearchForm searchForm) {
		return getIntegrationImportDefinitionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see com.clifton.integration.incoming.definition.validation.IntegrationImportDefinitionTypeValidator
	 */
	@Override
	public IntegrationImportDefinitionType saveIntegrationImportDefinitionType(IntegrationImportDefinitionType bean) {
		return getIntegrationImportDefinitionTypeDAO().save(bean);
	}


	private void validateFixAllowed(Serializable id, IntegrationImportStatus status, Date startDate, boolean runEvent) {
		boolean isAdmin = getSecurityAuthorizationService().isSecurityUserAdmin();
		long minutes = DateUtils.getMinutesDifference(new Date(), startDate);
		if (!isAdmin) {
			ValidationUtils.assertTrue(minutes >= RUN_AGE_MINUTES, "Run " + (runEvent ? "Event " : "") + "[" + id + "] is [" + minutes + "] minutes old and it cannot be fixed until it is at least [" + RUN_AGE_MINUTES
					+ "] minutes old.");
		}
		ValidationUtils.assertTrue(status.getIntegrationImportStatusType() == IntegrationImportStatusTypes.RUNNING, "Cannot fix run" + (runEvent ? " event" : "") + "s in statuses other than RUNNING");
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationTargetApplicationService getIntegrationTargetApplicationService() {
		return this.integrationTargetApplicationService;
	}


	public void setIntegrationTargetApplicationService(IntegrationTargetApplicationService integrationTargetApplicationService) {
		this.integrationTargetApplicationService = integrationTargetApplicationService;
	}


	public AdvancedUpdatableDAO<IntegrationImportStatus, Criteria> getIntegrationImportStatusDAO() {
		return this.integrationImportStatusDAO;
	}


	public void setIntegrationImportStatusDAO(AdvancedUpdatableDAO<IntegrationImportStatus, Criteria> integrationImportStatusDAO) {
		this.integrationImportStatusDAO = integrationImportStatusDAO;
	}


	public DaoNamedEntityCache<IntegrationImportStatus> getIntegrationImportStatusCache() {
		return this.integrationImportStatusCache;
	}


	public void setIntegrationImportStatusCache(DaoNamedEntityCache<IntegrationImportStatus> integrationImportStatusCache) {
		this.integrationImportStatusCache = integrationImportStatusCache;
	}


	public AdvancedUpdatableDAO<IntegrationImportRunEvent, Criteria> getIntegrationImportRunEventDAO() {
		return this.integrationImportRunEventDAO;
	}


	public void setIntegrationImportRunEventDAO(AdvancedUpdatableDAO<IntegrationImportRunEvent, Criteria> integrationImportRunEventDAO) {
		this.integrationImportRunEventDAO = integrationImportRunEventDAO;
	}


	public AdvancedUpdatableDAO<IntegrationImportDefinition, Criteria> getIntegrationImportDefinitionDAO() {
		return this.integrationImportDefinitionDAO;
	}


	public void setIntegrationImportDefinitionDAO(AdvancedUpdatableDAO<IntegrationImportDefinition, Criteria> integrationImportDefinitionDAO) {
		this.integrationImportDefinitionDAO = integrationImportDefinitionDAO;
	}


	public AdvancedUpdatableDAO<IntegrationImportRun, Criteria> getIntegrationImportRunDAO() {
		return this.integrationImportRunDAO;
	}


	public void setIntegrationImportRunDAO(AdvancedUpdatableDAO<IntegrationImportRun, Criteria> integrationImportRunDAO) {
		this.integrationImportRunDAO = integrationImportRunDAO;
	}


	public AdvancedUpdatableDAO<IntegrationImportDefinitionType, Criteria> getIntegrationImportDefinitionTypeDAO() {
		return this.integrationImportDefinitionTypeDAO;
	}


	public void setIntegrationImportDefinitionTypeDAO(AdvancedUpdatableDAO<IntegrationImportDefinitionType, Criteria> integrationImportDefinitionTypeDAO) {
		this.integrationImportDefinitionTypeDAO = integrationImportDefinitionTypeDAO;
	}


	public AdvancedUpdatableDAO<IntegrationImportDefinitionTypeColumn, Criteria> getIntegrationImportDefinitionTypeColumnDAO() {
		return this.integrationImportDefinitionTypeColumnDAO;
	}


	public void setIntegrationImportDefinitionTypeColumnDAO(AdvancedUpdatableDAO<IntegrationImportDefinitionTypeColumn, Criteria> integrationImportDefinitionTypeColumnDAO) {
		this.integrationImportDefinitionTypeColumnDAO = integrationImportDefinitionTypeColumnDAO;
	}


	public AdvancedUpdatableDAO<IntegrationImportDefinitionColumnMapping, Criteria> getIntegrationImportDefinitionColumnMappingDAO() {
		return this.integrationImportDefinitionColumnMappingDAO;
	}


	public void setIntegrationImportDefinitionColumnMappingDAO(AdvancedUpdatableDAO<IntegrationImportDefinitionColumnMapping, Criteria> integrationImportDefinitionColumnMappingDAO) {
		this.integrationImportDefinitionColumnMappingDAO = integrationImportDefinitionColumnMappingDAO;
	}


	public DaoSingleKeyListCache<IntegrationImportDefinitionType, String> getIntegrationImportDefinitionTypeByEventNameCache() {
		return this.integrationImportDefinitionTypeByEventNameCache;
	}


	public void setIntegrationImportDefinitionTypeByEventNameCache(DaoSingleKeyListCache<IntegrationImportDefinitionType, String> integrationImportDefinitionTypeByEventNameCache) {
		this.integrationImportDefinitionTypeByEventNameCache = integrationImportDefinitionTypeByEventNameCache;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public TimedEvictionCache<IntegrationImportRunEvent> getIntegrationRunEventTimedEvictionCache() {
		return this.integrationRunEventTimedEvictionCache;
	}


	public void setIntegrationRunEventTimedEvictionCache(TimedEvictionCache<IntegrationImportRunEvent> integrationRunEventTimedEvictionCache) {
		this.integrationRunEventTimedEvictionCache = integrationRunEventTimedEvictionCache;
	}
}
