package com.clifton.integration.incoming.corporate.action;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.integration.incoming.corporate.action.search.IntegrationCorporateActionPositionRequestSearchForm;
import com.clifton.integration.incoming.corporate.action.search.IntegrationCorporateActionSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author TerryS
 */
@Service
public class IntegrationCorporateActionServiceImpl implements IntegrationCorporateActionService {

	private AdvancedUpdatableDAO<IntegrationCorporateAction, Criteria> integrationCorporateActionDAO;
	private AdvancedUpdatableDAO<IntegrationCorporateActionPositionRequest, Criteria> integrationCorporateActionPositionRequestDAO;


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////                IntegrationCorporateAction Business Methods                 /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationCorporateAction getIntegrationCorporateAction(int id) {
		return this.getIntegrationCorporateActionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationCorporateAction> getIntegrationCorporateActionList(IntegrationCorporateActionSearchForm searchForm) {
		return getIntegrationCorporateActionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationCorporateAction saveIntegrationCorporateAction(IntegrationCorporateAction integrationCorporateAction) {
		return getIntegrationCorporateActionDAO().save(integrationCorporateAction);
	}


	@Override
	public void deleteIntegrationCorporateAction(int id) {
		getIntegrationCorporateActionDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////        IntegrationCorporateActionPositionRequest Business Methods          /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationCorporateActionPositionRequest getIntegrationCorporateActionPositionRequest(int id) {
		return this.getIntegrationCorporateActionPositionRequestDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationCorporateActionPositionRequest> getIntegrationCorporateActionPositionRequestList(IntegrationCorporateActionPositionRequestSearchForm searchForm) {
		return this.getIntegrationCorporateActionPositionRequestDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<IntegrationCorporateAction, Criteria> getIntegrationCorporateActionDAO() {
		return this.integrationCorporateActionDAO;
	}


	public void setIntegrationCorporateActionDAO(AdvancedUpdatableDAO<IntegrationCorporateAction, Criteria> integrationCorporateActionDAO) {
		this.integrationCorporateActionDAO = integrationCorporateActionDAO;
	}


	public AdvancedUpdatableDAO<IntegrationCorporateActionPositionRequest, Criteria> getIntegrationCorporateActionPositionRequestDAO() {
		return this.integrationCorporateActionPositionRequestDAO;
	}


	public void setIntegrationCorporateActionPositionRequestDAO(AdvancedUpdatableDAO<IntegrationCorporateActionPositionRequest, Criteria> integrationCorporateActionPositionRequestDAO) {
		this.integrationCorporateActionPositionRequestDAO = integrationCorporateActionPositionRequestDAO;
	}
}
