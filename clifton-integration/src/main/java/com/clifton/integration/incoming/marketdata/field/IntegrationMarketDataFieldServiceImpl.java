package com.clifton.integration.incoming.marketdata.field;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.integration.incoming.marketdata.field.search.IntegrationMarketDataFieldValueSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationMarketDataFieldServiceImpl implements IntegrationMarketDataFieldService {

	private AdvancedReadOnlyDAO<IntegrationMarketDataFieldValue, Criteria> integrationMarketDataFieldValueDAO;


	@Override
	public List<IntegrationMarketDataFieldValue> getIntegrationMarketDataFieldValueList(IntegrationMarketDataFieldValueSearchForm searchForm) {
		return getIntegrationMarketDataFieldValueDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	public AdvancedReadOnlyDAO<IntegrationMarketDataFieldValue, Criteria> getIntegrationMarketDataFieldValueDAO() {
		return this.integrationMarketDataFieldValueDAO;
	}


	public void setIntegrationMarketDataFieldValueDAO(AdvancedReadOnlyDAO<IntegrationMarketDataFieldValue, Criteria> integrationMarketDataFieldValueDAO) {
		this.integrationMarketDataFieldValueDAO = integrationMarketDataFieldValueDAO;
	}
}
