package com.clifton.integration.outgoing.export;

import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.source.IntegrationSource;

import java.util.List;


/**
 * @author theodorez
 */
public class IntegrationExportBuildCommand {

	private final ExportMessagingMessage exportMessagingExport;
	private final IntegrationSource source;
	private final List<IntegrationFile> filesToExportList;
	private final ExportMessagingDestination destination;
	private final IntegrationExportStatus status;


	public IntegrationExportBuildCommand(ExportMessagingMessage exportMessagingExport, IntegrationSource source, List<IntegrationFile> filesToExportList) {
		this(exportMessagingExport, source, filesToExportList, null, null);
	}


	public IntegrationExportBuildCommand(IntegrationExportBuildCommand command, IntegrationExportStatus status, ExportMessagingDestination destination) {
		this(command.getExportMessagingExport(), command.getSource(), command.getFilesToExportList(), status, destination);
	}


	private IntegrationExportBuildCommand(ExportMessagingMessage exportMessagingExport, IntegrationSource source, List<IntegrationFile> filesToExportList, IntegrationExportStatus status, ExportMessagingDestination destination) {
		this.exportMessagingExport = exportMessagingExport;
		this.source = source;
		this.filesToExportList = filesToExportList;
		this.status = status;
		this.destination = destination;
	}


	public ExportMessagingMessage getExportMessagingExport() {
		return this.exportMessagingExport;
	}


	public IntegrationSource getSource() {
		return this.source;
	}


	public List<IntegrationFile> getFilesToExportList() {
		return this.filesToExportList;
	}


	public ExportMessagingDestination getDestination() {
		return this.destination;
	}


	public IntegrationExportStatus getStatus() {
		return this.status;
	}
}
