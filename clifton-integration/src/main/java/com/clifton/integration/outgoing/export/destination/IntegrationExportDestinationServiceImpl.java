package com.clifton.integration.outgoing.export.destination;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportDestinationSearchForm;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportDestinationSubTypeSearchForm;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportDestinationTypeSearchForm;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportIntegrationExportDestinationSearchForm;
import com.clifton.security.secret.SecuritySecretService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationExportDestinationServiceImpl implements IntegrationExportDestinationService {

	private AdvancedUpdatableDAO<IntegrationExportDestination, Criteria> integrationExportDestinationDAO;
	private AdvancedUpdatableDAO<IntegrationExportDestinationType, Criteria> integrationExportDestinationTypeDAO;
	private AdvancedUpdatableDAO<IntegrationExportDestinationSubType, Criteria> integrationExportDestinationSubTypeDAO;
	private AdvancedUpdatableDAO<IntegrationExportIntegrationExportDestination, Criteria> integrationExportIntegrationExportDestinationDAO;

	private DaoNamedEntityCache<IntegrationExportDestinationType> integrationExportDestinationTypeCache;

	private DaoNamedEntityCache<IntegrationExportDestinationSubType> integrationExportDestinationSubTypeCache;

	private SecuritySecretService securitySecretService;


	//////////////////////////////////////////////////////////////////////////////////////////
	//////                      Export  Type Business Methods                           //////
	//////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationExportDestinationType getIntegrationExportDestinationType(short id) {
		return getIntegrationExportDestinationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationExportDestinationType getIntegrationExportDestinationTypeByName(String name) {
		if (name == null) {
			throw new ValidationException("getIntegrationExportDestinationTypeByName requires IntegrationExportDestinationTypes name parameter");
		}
		return getIntegrationExportDestinationTypeCache().getBeanForKeyValueStrict(getIntegrationExportDestinationTypeDAO(), name);
	}


	@Override
	public IntegrationExportDestinationSubType getIntegrationExportDestinationSubTypeByName(String name) {
		if (name == null) {
			throw new ValidationException("getIntegrationExportDestinationSubTypeByName requires IntegrationExportDestinationSubTypes name parameter");
		}
		return getIntegrationExportDestinationSubTypeCache().getBeanForKeyValue(getIntegrationExportDestinationSubTypeDAO(), name);
	}


	@Override
	public List<IntegrationExportDestinationType> getIntegrationExportDestinationTypeList(IntegrationExportDestinationTypeSearchForm searchForm) {
		return getIntegrationExportDestinationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationExportDestinationType saveIntegrationExportDestinationType(IntegrationExportDestinationType bean) {
		return getIntegrationExportDestinationTypeDAO().save(bean);
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////                      Export  SubType Business Methods                        //////
	//////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationExportDestinationSubType getIntegrationExportDestinationSubType(short id) {
		return getIntegrationExportDestinationSubTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationExportDestinationSubType> getIntegrationExportDestinationSubTypeList(IntegrationExportDestinationSubTypeSearchForm searchForm) {
		return getIntegrationExportDestinationSubTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationExportDestinationSubType saveIntegrationExportDestinationSubType(IntegrationExportDestinationSubType bean) {
		return getIntegrationExportDestinationSubTypeDAO().save(bean);
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////                   Export Destination Business Methods                        //////
	//////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationExportDestination getIntegrationExportDestination(int id) {
		return getIntegrationExportDestinationDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationExportDestination getIntegrationExportDestinationByValue(short destinationTypeId, String destinationValue, String destinationUserName, String destinationText) {
		IntegrationExportDestinationSearchForm searchForm = new IntegrationExportDestinationSearchForm();
		searchForm.setTypeId(destinationTypeId);
		searchForm.setDestinationValue(destinationValue);
		if (StringUtils.isEmpty(destinationUserName)) {
			searchForm.setDestinationUserNameIsNull(true);
		}
		else {
			searchForm.setDestinationUserName(destinationUserName);
		}
		if (StringUtils.isEmpty(destinationText)) {
			searchForm.setDestinationTextIsNull(true);
		}
		else {
			searchForm.setDestinationTextEquals(destinationText);
		}

		return CollectionUtils.getOnlyElement(getIntegrationExportDestinationList(searchForm));
	}

	@Override
	public IntegrationExportDestination getIntegrationExportDestinationByValueAndFlags(IntegrationExportDestination integrationExportDestination){
		IntegrationExportDestinationSearchForm searchForm = new IntegrationExportDestinationSearchForm();

		searchForm.setTypeId(integrationExportDestination.getType().getId());
		searchForm.setDestinationValue(integrationExportDestination.getDestinationValue());

		if (StringUtils.isEmpty(integrationExportDestination.getDestinationUserName())) {
			searchForm.setDestinationUserNameIsNull(true);
		}
		else {
			searchForm.setDestinationUserName(integrationExportDestination.getDestinationUserName());
		}
		if (StringUtils.isEmpty(integrationExportDestination.getDestinationText())) {
			searchForm.setDestinationTextIsNull(true);
		}
		else {
			searchForm.setDestinationTextEquals(integrationExportDestination.getDestinationText());
		}

		searchForm.setOverwriteFile(integrationExportDestination.isOverwriteFile());
		searchForm.setWriteExtension(integrationExportDestination.isWriteExtension());

		return CollectionUtils.getOnlyElement(getIntegrationExportDestinationList(searchForm));
	}


	@Override
	public List<IntegrationExportDestination> getIntegrationExportDestinationList(IntegrationExportDestinationSearchForm searchForm) {
		return getIntegrationExportDestinationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationExportDestination saveIntegrationExportDestination(IntegrationExportDestination bean) {
		bean.setDestinationPasswordSecuritySecret(getSecuritySecretService().saveSecuritySecret(bean.getDestinationPasswordSecuritySecret()));
		bean.setDestinationSshPrivateKeySecuritySecret(getSecuritySecretService().saveSecuritySecret(bean.getDestinationSshPrivateKeySecuritySecret()));
		return getIntegrationExportDestinationDAO().save(bean);
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////      Integration Export Integration Export Destination Business Methods      //////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationExportIntegrationExportDestination getIntegrationExportIntegrationExportDestination(int id) {
		return getIntegrationExportIntegrationExportDestinationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationExportIntegrationExportDestination> getIntegrationExportIntegrationExportDestinationList(final IntegrationExportIntegrationExportDestinationSearchForm searchForm) {
		return getIntegrationExportIntegrationExportDestinationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationExportIntegrationExportDestination saveIntegrationExportIntegrationExportDestination(IntegrationExportIntegrationExportDestination bean) {
		IntegrationExportIntegrationExportDestinationSearchForm searchForm = new IntegrationExportIntegrationExportDestinationSearchForm();
		searchForm.setExportDestinationTypeIdNotEquals(bean.getReferenceTwo().getType().getId());
		searchForm.setExportId(bean.getReferenceOne().getId());
		List<IntegrationExportIntegrationExportDestination> existingLinksOfOtherTypes = getIntegrationExportIntegrationExportDestinationList(searchForm);
		ValidationUtils.assertEmpty(existingLinksOfOtherTypes, "Cannot link destinations of different types to the same export.  Failed to link " + bean.getReferenceTwo()
				+ " to IntegrationExport [id=" + bean.getReferenceOne().getId() + "].");
		return getIntegrationExportIntegrationExportDestinationDAO().save(bean);
	}


	@Override
	public void saveIntegrationExportIntegrationExportDestinationList(List<IntegrationExportIntegrationExportDestination> beanList) {
		getIntegrationExportIntegrationExportDestinationDAO().saveList(beanList);
	}


	////////////////////////////////////////////////////////////////////////////
	//////                       Getters and Setters                      //////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<IntegrationExportDestination, Criteria> getIntegrationExportDestinationDAO() {
		return this.integrationExportDestinationDAO;
	}


	public void setIntegrationExportDestinationDAO(AdvancedUpdatableDAO<IntegrationExportDestination, Criteria> integrationExportDestinationDAO) {
		this.integrationExportDestinationDAO = integrationExportDestinationDAO;
	}


	public AdvancedUpdatableDAO<IntegrationExportDestinationType, Criteria> getIntegrationExportDestinationTypeDAO() {
		return this.integrationExportDestinationTypeDAO;
	}


	public void setIntegrationExportDestinationTypeDAO(AdvancedUpdatableDAO<IntegrationExportDestinationType, Criteria> integrationExportDestinationTypeDAO) {
		this.integrationExportDestinationTypeDAO = integrationExportDestinationTypeDAO;
	}


	public AdvancedUpdatableDAO<IntegrationExportDestinationSubType, Criteria> getIntegrationExportDestinationSubTypeDAO() {
		return this.integrationExportDestinationSubTypeDAO;
	}


	public void setIntegrationExportDestinationSubTypeDAO(AdvancedUpdatableDAO<IntegrationExportDestinationSubType, Criteria> integrationExportDestinationSubTypeDAO) {
		this.integrationExportDestinationSubTypeDAO = integrationExportDestinationSubTypeDAO;
	}


	public DaoNamedEntityCache<IntegrationExportDestinationType> getIntegrationExportDestinationTypeCache() {
		return this.integrationExportDestinationTypeCache;
	}


	public void setIntegrationExportDestinationTypeCache(DaoNamedEntityCache<IntegrationExportDestinationType> integrationExportDestinationTypeCache) {
		this.integrationExportDestinationTypeCache = integrationExportDestinationTypeCache;
	}


	public DaoNamedEntityCache<IntegrationExportDestinationSubType> getIntegrationExportDestinationSubTypeCache() {
		return this.integrationExportDestinationSubTypeCache;
	}


	public void setIntegrationExportDestinationSubTypeCache(DaoNamedEntityCache<IntegrationExportDestinationSubType> integrationExportDestinationSubTypeCache) {
		this.integrationExportDestinationSubTypeCache = integrationExportDestinationSubTypeCache;
	}


	public AdvancedUpdatableDAO<IntegrationExportIntegrationExportDestination, Criteria> getIntegrationExportIntegrationExportDestinationDAO() {
		return this.integrationExportIntegrationExportDestinationDAO;
	}


	public void setIntegrationExportIntegrationExportDestinationDAO(AdvancedUpdatableDAO<IntegrationExportIntegrationExportDestination, Criteria> integrationExportIntegrationExportDestinationDAO) {
		this.integrationExportIntegrationExportDestinationDAO = integrationExportIntegrationExportDestinationDAO;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
