package com.clifton.integration.outgoing.export;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.search.IntegrationFileSearchForm;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus.IntegrationExportFileStatusNames;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestinationService;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportIntegrationExportDestinationSearchForm;
import com.clifton.integration.outgoing.export.search.IntegrationExportHistorySearchForm;
import com.clifton.integration.outgoing.export.search.IntegrationExportIntegrationFileSearchForm;
import com.clifton.integration.outgoing.export.search.IntegrationExportSearchForm;
import com.clifton.integration.outgoing.export.search.IntegrationExportStatusSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class IntegrationExportServiceImpl implements IntegrationExportService {

	private AdvancedUpdatableDAO<IntegrationExport, Criteria> integrationExportDAO;
	private AdvancedReadOnlyDAO<IntegrationExportStatus, Criteria> integrationExportStatusDAO;
	private AdvancedReadOnlyDAO<IntegrationExportFileStatus, Criteria> integrationExportFileStatusDAO;
	private AdvancedUpdatableDAO<IntegrationExportHistory, Criteria> integrationExportHistoryDAO;
	private AdvancedUpdatableDAO<IntegrationExportIntegrationFile, Criteria> integrationExportIntegrationFileDAO;

	private IntegrationExportDestinationService integrationExportDestinationService;

	private DaoNamedEntityCache<IntegrationExportStatus> integrationExportStatusCache;
	private DaoNamedEntityCache<IntegrationExportFileStatus> integrationExportFileStatusCache;

	private IntegrationFileService integrationFileService;

	private IntegrationExportProcessingHandler integrationExportProcessingHandler;


	////////////////////////////////////////////////////////////////////////////
	///////                Export  Status Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationExportStatus getIntegrationExportStatusByName(String name) {
		if (name == null) {
			throw new ValidationException("getIntegrationExportStatusByName requires IntegrationExportStatusNames name parameter");
		}
		return getIntegrationExportStatusCache().getBeanForKeyValueStrict(getIntegrationExportStatusDAO(), name);
	}


	@Override
	public List<IntegrationExportStatus> getIntegrationExportStatusList() {
		return getIntegrationExportStatusDAO().findAll();
	}


	@Override
	public List<IntegrationExportStatus> getIntegrationExportStatusList(IntegrationExportStatusSearchForm searchForm) {
		return getIntegrationExportStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	//////                   Export  Business Methods                     //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationExport getIntegrationExport(int id) {
		return getIntegrationExportDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationExport getIntegrationExportPopulated(int id) {
		IntegrationExport integrationExport = getIntegrationExport(id);
		if (integrationExport != null) {
			IntegrationExportIntegrationExportDestinationSearchForm searchForm = new IntegrationExportIntegrationExportDestinationSearchForm();
			searchForm.setExportId(integrationExport.getId());
			integrationExport.setLinkedDestinationList(getIntegrationExportDestinationService().getIntegrationExportIntegrationExportDestinationList(searchForm));

			IntegrationFileSearchForm csf = new IntegrationFileSearchForm();
			csf.setIntegrationExportId(integrationExport.getId());
			integrationExport.setIntegrationFileList(getIntegrationFileService().getIntegrationFileList(csf));
		}
		return integrationExport;
	}


	@Override
	public List<IntegrationExport> getIntegrationExportList() {
		return getIntegrationExportDAO().findAll();
	}


	@Override
	public List<IntegrationExport> getIntegrationExportList(final IntegrationExportSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getExportDestinationId() != null) {

					DetachedCriteria ne = DetachedCriteria.forClass(IntegrationExportIntegrationExportDestination.class, "ed");
					ne.setProjection(Projections.property("id"));
					ne.createAlias("referenceOne", "e");
					ne.add(Restrictions.eqProperty("e.id", criteria.getAlias() + ".id"));
					ne.add(Restrictions.eq("referenceTwo.id", searchForm.getExportDestinationId()));
					criteria.add(Subqueries.exists(ne));
				}
			}
		};
		return getIntegrationExportDAO().findBySearchCriteria(config);
	}


	@Override
	public IntegrationExport saveIntegrationExport(IntegrationExport bean) {
		return getIntegrationExportDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	///////                Export History Business Methods               ///////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationExportHistory getIntegrationExportHistory(int id) {
		return getIntegrationExportHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public IntegrationExportHistory getIntegrationExportHistoryByStartDate(int exportId, Date startDate) {
		return getIntegrationExportHistoryDAO().findOneByFields(new String[]{"export.id", "startDate"}, new Object[]{exportId, startDate});
	}


	@Override
	public List<IntegrationExportHistory> getIntegrationExportHistoryList(IntegrationExportHistorySearchForm searchForm) {

		return getIntegrationExportHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationExportHistory saveIntegrationExportHistory(IntegrationExportHistory bean) {
		return getIntegrationExportHistoryDAO().save(bean);
	}


	///////////////////////////////////////////////////////////////////////////////////////
	///////           IntegrationExportIntegrationFile Business Methods          ///////
	///////////////////////////////////////////////////////////////////////////////////////
	@Override
	public IntegrationExportIntegrationFile getIntegrationExportIntegrationFile(int id) {
		return getIntegrationExportIntegrationFileDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationExportIntegrationFile> getIntegrationExportIntegrationFileList(IntegrationExportIntegrationFileSearchForm searchForm) {
		return getIntegrationExportIntegrationFileDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationExportIntegrationFile getIntegrationExportIntegrationFileByExportIdAndFileId(int exportId, int fileId) {
		IntegrationExportIntegrationFileSearchForm searchForm = new IntegrationExportIntegrationFileSearchForm();
		searchForm.setExportId(exportId);
		searchForm.setFileId(fileId);

		return CollectionUtils.getOnlyElement(getIntegrationExportIntegrationFileList(searchForm));
	}


	@Override
	public IntegrationExportIntegrationFile saveIntegrationExportIntegrationFile(IntegrationExportIntegrationFile bean) {
		return getIntegrationExportIntegrationFileDAO().save(bean);
	}


	@Override
	public void linkIntegrationExportIntegrationFile(IntegrationExport export, IntegrationFile file, IntegrationExportFileStatusNames status) {
		IntegrationExportFileStatus fileStatus = this.getIntegrationExportFileStatusByName(status);

		IntegrationExportIntegrationFile ef = new IntegrationExportIntegrationFile();
		ef.setReferenceOne(export);
		ef.setReferenceTwo(file);
		ef.setStatus(fileStatus);
		getIntegrationExportIntegrationFileDAO().save(ef);
	}


	@Override
	public IntegrationExportFileStatus getIntegrationExportFileStatusByName(IntegrationExportFileStatusNames name) {
		if (name == null) {
			throw new ValidationException("getIntegrationExportFileStatusByName requires IntegrationExportFileStatusNames name parameter");
		}

		return getIntegrationExportFileStatusCache().getBeanForKeyValueStrict(getIntegrationExportFileStatusDAO(), name.name());
	}

	///////////////////////////////////////////////////////////////////////////////////////
	///////                    Integration Export Reprocess Methods                 ///////
	///////////////////////////////////////////////////////////////////////////////////////


	@Override
	public void reprocessIntegrationExport(int integrationExportId) {
		IntegrationExport export = getIntegrationExport(integrationExportId);
		getIntegrationExportProcessingHandler().reprocessIntegrationExport(export);
	}


	////////////////////////////////////////////////////////////////////////////
	//////                       Getters and Setters                      //////
	////////////////////////////////////////////////////////////////////////////
	public AdvancedUpdatableDAO<IntegrationExport, Criteria> getIntegrationExportDAO() {
		return this.integrationExportDAO;
	}


	public void setIntegrationExportDAO(AdvancedUpdatableDAO<IntegrationExport, Criteria> integrationExportDAO) {
		this.integrationExportDAO = integrationExportDAO;
	}


	public AdvancedReadOnlyDAO<IntegrationExportStatus, Criteria> getIntegrationExportStatusDAO() {
		return this.integrationExportStatusDAO;
	}


	public void setIntegrationExportStatusDAO(AdvancedReadOnlyDAO<IntegrationExportStatus, Criteria> integrationExportStatusDAO) {
		this.integrationExportStatusDAO = integrationExportStatusDAO;
	}


	public AdvancedUpdatableDAO<IntegrationExportHistory, Criteria> getIntegrationExportHistoryDAO() {
		return this.integrationExportHistoryDAO;
	}


	public void setIntegrationExportHistoryDAO(AdvancedUpdatableDAO<IntegrationExportHistory, Criteria> integrationExportHistoryDAO) {
		this.integrationExportHistoryDAO = integrationExportHistoryDAO;
	}


	public DaoNamedEntityCache<IntegrationExportStatus> getIntegrationExportStatusCache() {
		return this.integrationExportStatusCache;
	}


	public void setIntegrationExportStatusCache(DaoNamedEntityCache<IntegrationExportStatus> integrationExportStatusCache) {
		this.integrationExportStatusCache = integrationExportStatusCache;
	}


	public DaoNamedEntityCache<IntegrationExportFileStatus> getIntegrationExportFileStatusCache() {
		return this.integrationExportFileStatusCache;
	}


	public void setIntegrationExportFileStatusCache(DaoNamedEntityCache<IntegrationExportFileStatus> integrationExportFileStatusCache) {
		this.integrationExportFileStatusCache = integrationExportFileStatusCache;
	}


	public AdvancedUpdatableDAO<IntegrationExportIntegrationFile, Criteria> getIntegrationExportIntegrationFileDAO() {
		return this.integrationExportIntegrationFileDAO;
	}


	public void setIntegrationExportIntegrationFileDAO(AdvancedUpdatableDAO<IntegrationExportIntegrationFile, Criteria> integrationExportIntegrationFileDAO) {
		this.integrationExportIntegrationFileDAO = integrationExportIntegrationFileDAO;
	}


	public IntegrationExportDestinationService getIntegrationExportDestinationService() {
		return this.integrationExportDestinationService;
	}


	public void setIntegrationExportDestinationService(IntegrationExportDestinationService integrationExportDestinationService) {
		this.integrationExportDestinationService = integrationExportDestinationService;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public AdvancedReadOnlyDAO<IntegrationExportFileStatus, Criteria> getIntegrationExportFileStatusDAO() {
		return this.integrationExportFileStatusDAO;
	}


	public void setIntegrationExportFileStatusDAO(AdvancedReadOnlyDAO<IntegrationExportFileStatus, Criteria> integrationExportFileStatusDAO) {
		this.integrationExportFileStatusDAO = integrationExportFileStatusDAO;
	}


	public IntegrationExportProcessingHandler getIntegrationExportProcessingHandler() {
		return this.integrationExportProcessingHandler;
	}


	public void setIntegrationExportProcessingHandler(IntegrationExportProcessingHandler integrationExportProcessingHandler) {
		this.integrationExportProcessingHandler = integrationExportProcessingHandler;
	}
}
