package com.clifton.integration.outgoing.export;

import com.clifton.core.context.DoNotAddRequestMapping;


/**
 * Handles processing and reprocessing of IntegrationExport(s)
 *
 * @author theodorez
 */
public interface IntegrationExportProcessingHandler {

	@DoNotAddRequestMapping
	public void processIntegrationExport(IntegrationExportBuildCommand command);


	public void reprocessIntegrationExport(IntegrationExport export);
}
