package com.clifton.integration.swift;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.prowidesoftware.swift.io.RJEReader;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.field.Field21;
import com.prowidesoftware.swift.model.field.Field25;
import com.prowidesoftware.swift.model.field.Field25P;
import com.prowidesoftware.swift.model.field.Field28C;
import com.prowidesoftware.swift.model.field.Field60F;
import com.prowidesoftware.swift.model.field.Field60M;
import com.prowidesoftware.swift.model.field.Field61;
import com.prowidesoftware.swift.model.field.Field62F;
import com.prowidesoftware.swift.model.field.Field62M;
import com.prowidesoftware.swift.model.field.Field64;
import com.prowidesoftware.swift.model.field.Field65;
import com.prowidesoftware.swift.model.field.Field86;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt9xx.MT940;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;


/**
 * Field 20 (M)
 * Field 21 (O)
 * Field 25 P,NONE (M)
 * Field 28 C (M)
 * Field 60 F,M (M)
 * Sequence _A (O) (repetitive)
 * - Field 61 (O)
 * - Field 86 (O)
 * Field 62 F,M (M)
 * Field 64 (O)
 * Field 65 (O) (repetitive)
 * Field 86 (O)
 */
public class SwiftMt940ToCsvConverter extends BaseSwiftMtToCsvConverter {

	public static final List<ColumnHeader> headers = Collections.unmodifiableList(
			Arrays.asList(
					new ColumnHeader("Reference", Field20.NAME),
					new ColumnHeader("Related Reference", Field21.NAME),
					new ColumnHeader("Account", Field25P.NAME, Field25.NAME),
					new ColumnHeader("Statement Number", Field28C.NAME),
					new ColumnHeader("Statement Sequence", Field28C.NAME),
					new ColumnHeader("Opening Balance Date", Field60F.NAME, Field60M.NAME),
					new ColumnHeader("Opening Balance Currency", Field60F.NAME, Field60M.NAME),
					new ColumnHeader("Opening Balance Amount", Field60F.NAME, Field60M.NAME),
					new ColumnHeader("Value Date", Field61.NAME),
					new ColumnHeader("Entry Date", Field61.NAME),
					new ColumnHeader("Funds Code", Field61.NAME),
					new ColumnHeader("Amount", Field61.NAME),
					new ColumnHeader("Transaction Type", Field61.NAME),
					new ColumnHeader("Identification Code", Field61.NAME),
					new ColumnHeader("Account Owner Reference", Field61.NAME),
					new ColumnHeader("Account Servicing Institution Reference", Field61.NAME),
					new ColumnHeader("Supplementary Details", Field61.NAME),
					new ColumnHeader("Transaction Narrative 1", Field86.NAME),
					new ColumnHeader("Transaction Narrative 2", Field86.NAME),
					new ColumnHeader("Transaction Narrative 3", Field86.NAME),
					new ColumnHeader("Transaction Narrative 4", Field86.NAME),
					new ColumnHeader("Transaction Narrative 5", Field86.NAME),
					new ColumnHeader("Transaction Narrative 6", Field86.NAME),
					new ColumnHeader("Booked Closing Balance Date", Field62F.NAME, Field62M.NAME),
					new ColumnHeader("Booked Closing Balance Currency", Field62F.NAME, Field62M.NAME),
					new ColumnHeader("Booked Closing Balance Amount", Field62F.NAME, Field62M.NAME),
					new ColumnHeader("Closing Available Balance Date", Field64.NAME),
					new ColumnHeader("Closing Available Balance Currency", Field64.NAME),
					new ColumnHeader("Closing Available Balance Amount", Field64.NAME),
					new ColumnHeader("Forward Available Balance Date", Field65.NAME),
					new ColumnHeader("Forward Available Balance Currency", Field65.NAME),
					new ColumnHeader("Forward Available Balance Amount", Field65.NAME),
					new ColumnHeader("Account Owner Narrative 1", Field86.NAME),
					new ColumnHeader("Account Owner Narrative 2", Field86.NAME),
					new ColumnHeader("Account Owner Narrative 3", Field86.NAME),
					new ColumnHeader("Account Owner Narrative 4", Field86.NAME),
					new ColumnHeader("Account Owner Narrative 5", Field86.NAME),
					new ColumnHeader("Account Owner Narrative 6", Field86.NAME)
			));

	private boolean includeStatementLines = true;
	private List<ColumnHeader> workingHeaders;


	public SwiftMt940ToCsvConverter() {
		this.workingHeaders = buildWorkingHeaders();
	}


	@Override
	public int output(Reader swift, Writer csv) throws IOException {
		int records = 0;

		this.workingHeaders = buildWorkingHeaders();
		CSVPrinter csvFilePrinter = new CSVPrinter(csv, CSVFormat.DEFAULT.withHeader(getWorkingHeaders().stream().map(ColumnHeader::getHeader).toArray(String[]::new)));

		RJEReader reader = new RJEReader(swift);
		while (reader.hasNext()) {
			AbstractMT abstractMT = reader.nextMT();
			if (abstractMT == null) {
				continue;
			}
			if (MT940.NAME.equals(abstractMT.getMessageType())) {
				output((MT940) abstractMT, csvFilePrinter);
				records++;
			}
		}

		return records;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                   Private Business Methods                 /////////
	////////////////////////////////////////////////////////////////////////////


	private List<ColumnHeader> buildWorkingHeaders() {
		return isIncludeStatementLines() ? headers : headers.stream()
				.filter(h -> !h.getNumbers().contains(Field61.NAME))
				.filter(h -> !h.getHeader().startsWith("Transaction Narrative "))
				.collect(Collectors.toList());
	}


	private void output(MT940 mt940, CSVPrinter csv) throws IOException {
		// returns fields in order
		List<Field> fields = mt940.getFields();
		List<DataRow> dataRows = new ArrayList<>();
		DataRow dataRow = new DataRow(getWorkingHeaders());

		Iterator<Field> fieldIterator = fields.iterator();
		if (!fieldIterator.hasNext()) {
			return;
		}

		// 20 - R
		Field field = fieldIterator.next();
		dataRow.addRequiredColumnValue(field, f -> convertComponentString(f, 1), f -> ValidationUtils.assertTrue(Field20.NAME.equals(f.getName()), "A required field (20) is missing."));
		field = fieldIterator.next();
		// 21 - O
		if (dataRow.addOptionalColumnValue(field, f -> convertComponentString(f, 1), Field21.NAME)) {
			field = fieldIterator.next();
		}
		// 25 - R
		dataRow.addRequiredColumnValue(field, f -> convertComponentString(f, 1), f -> ValidationUtils.assertTrue(Field25P.NAME.equals(f.getName()) || Field25.NAME.equals(f.getName()), "A required field (25) is missing."));
		field = fieldIterator.next();
		// 28 - R
		dataRow.addRequiredColumnValue(field, f -> convertComponentString(f, 1), f -> ValidationUtils.assertTrue(Field28C.NAME.equals(f.getName()), "A required field (28) is missing."));
		dataRow.addRequiredColumnValue(field, f -> convertComponentString(f, 2), f -> ValidationUtils.assertTrue(Field28C.NAME.equals(f.getName()), "A required field (28) is missing."));
		field = fieldIterator.next();
		// 60 - R
		addField6XValues(dataRow, field, f -> ValidationUtils.assertTrue(Field60M.NAME.equals(f.getName()) || Field60F.NAME.equals(f.getName()), "A required field (60) is missing."));
		field = fieldIterator.next();
		// 61, 86 - O
		if (isIncludeStatementLines()) {
			List<DataRow> statementLines = new ArrayList<>();
			field = getStatementLines(statementLines, field, fieldIterator);
			appendToDataRows(dataRows, dataRow, statementLines);
		}
		else {
			field = skipStatementLines(field, fieldIterator);
			DataRow dataRowStart = new DataRow(dataRow);
			dataRows.add(dataRowStart);
		}

		// 62 - M, 64 - O, 65 - O, 86 - O
		int endRowIndex = isIncludeStatementLines() ? 23 : 8;
		DataRow endRow = new DataRow(getWorkingHeaders().subList(endRowIndex, getWorkingHeaders().size()));
		// 62 - M
		addField6XValues(endRow, field, f -> ValidationUtils.assertTrue(Field62M.NAME.equals(f.getName()) || Field62F.NAME.equals(f.getName()), "A required field (62) is missing."));
		// 64 - O
		if (fieldIterator.hasNext()) {
			field = fieldIterator.next();
		}
		if (field != null && Field64.NAME.equals(field.getName())) {
			addField6XValues(endRow, field);
		}
		else {
			endRow.addEmptyColumnValue(3, Field64.NAME);
		}
		if (fieldIterator.hasNext()) {
			field = fieldIterator.next();
		}
		// 65 - O
		if (field != null && Field65.NAME.equals(field.getName())) {
			addField6XValues(endRow, field);
		}
		else {
			endRow.addEmptyColumnValue(3, Field65.NAME);
		}
		if (fieldIterator.hasNext()) {
			field = fieldIterator.next();
		}
		// 86 - O
		if (field != null && Field86.NAME.equals(field.getName())) {
			addField86Values(endRow, field);
		}
		else {
			endRow.addEmptyColumnValue(6, Field86.NAME);
		}
		ValidationUtils.assertFalse(fieldIterator.hasNext(), () -> "Expected end of message " + fieldIterator.next().getName());
		// append - endings to existing rows
		Iterator<DataRow> startRowIterator = dataRows.iterator();
		do {
			DataRow startRowValues = startRowIterator.next();
			startRowValues.addColumnValues(endRow);
		}
		while (startRowIterator.hasNext());
		// output
		for (DataRow row : dataRows) {
			AssertUtils.assertEquals(getWorkingHeaders().size(), row.getValues().size(), String.format("Expected [%s] but received [%s] column values.", getWorkingHeaders().size(), row.getValues().size()));
			csv.printRecord(row.getValues());
		}
	}


	private void appendToDataRows(List<DataRow> dataRows, DataRow dataRow, List<DataRow> joins) {
		Iterator<DataRow> iterator = joins.iterator();
		do {
			DataRow statementLine = iterator.next();
			DataRow dataRowStart = new DataRow(dataRow);
			dataRowStart.addColumnValues(statementLine);
			dataRows.add(dataRowStart);
		}
		while (iterator.hasNext());
	}


	private Field skipStatementLines(Field field, Iterator<Field> fieldIterator) {
		do {
			if (Field61.NAME.equals(field.getName())) {
				ValidationUtils.assertTrue(fieldIterator.hasNext(), "Unexpected end of message.");
				field = fieldIterator.next();
			}
			if (Field61.NAME.equals(field.getName()) || Field86.NAME.equals(field.getName())) {
				ValidationUtils.assertTrue(fieldIterator.hasNext(), "Unexpected end of message.");
				field = fieldIterator.next();
			}
		}
		while (Field61.NAME.equals(field.getName()) || Field86.NAME.equals(field.getName()));

		return field;
	}


	private Field getStatementLines(List<DataRow> statementLines, Field field, Iterator<Field> fieldIterator) {
		do {
			DataRow statementLine = new DataRow(getWorkingHeaders().subList(8, 23));
			if (addField61Values(statementLine, field)) {
				ValidationUtils.assertTrue(fieldIterator.hasNext(), "Unexpected end of message.");
				field = fieldIterator.next();
			}
			else {
				statementLine.addEmptyColumnValue(9, Field61.NAME);
			}

			if (!addField86Values(statementLine, field)) {
				statementLine.addEmptyColumnValue(6, Field86.NAME);
			}
			statementLines.add(statementLine);

			if (Field61.NAME.equals(field.getName()) || Field86.NAME.equals(field.getName())) {
				ValidationUtils.assertTrue(fieldIterator.hasNext(), "Unexpected end of message.");
				field = fieldIterator.next();
			}
		}
		while (Field61.NAME.equals(field.getName()) || Field86.NAME.equals(field.getName()));

		return field;
	}


	private boolean addField61Values(DataRow dataRow, Field field) {
		if (Field61.NAME.equals(field.getName())) {
			dataRow.addColumnValue(convertDate(field.getComponent(1), "yyMMdd"), field.getName()); // value date
			dataRow.addColumnValue(convertComponentString(field, 2), field.getName()); // entry date
			dataRow.addColumnValue(convertComponentString(field, 4), field.getName()); // funds code
			dataRow.addColumnValue(convertNumber(field.getComponent(3), field.getComponentAsNumber(5)), field.getName()); // amount
			dataRow.addColumnValue(convertComponentString(field, 6), field.getName()); // transaction type
			dataRow.addColumnValue(convertComponentString(field, 7), field.getName()); // identification code
			dataRow.addColumnValue(convertComponentString(field, 8), field.getName()); // reference for the account owner
			dataRow.addColumnValue(convertComponentString(field, 9), field.getName()); // reference for the servicing account
			dataRow.addColumnValue(convertComponentString(field, 10), field.getName()); // Supplementary Details
			return true;
		}
		return false;
	}


	private boolean addField86Values(DataRow dataRow, Field field) {
		if (Field86.NAME.equals(field.getName())) {
			dataRow.addColumnValue(convertComponentString(field, 1), field.getName());
			dataRow.addColumnValue(convertComponentString(field, 2), field.getName());
			dataRow.addColumnValue(convertComponentString(field, 3), field.getName());
			dataRow.addColumnValue(convertComponentString(field, 4), field.getName());
			dataRow.addColumnValue(convertComponentString(field, 5), field.getName());
			dataRow.addColumnValue(convertComponentString(field, 6), field.getName());
			return true;
		}
		return false;
	}


	private void addField6XValues(DataRow dataRow, Field field, Consumer<Field> validation) {
		validation.accept(field);
		addField6XValues(dataRow, field);
	}


	private void addField6XValues(DataRow dataRow, Field field) {
		dataRow.addColumnValue(convertDate(field.getComponent(2), "yyMMdd"), field.getName());
		dataRow.addColumnValue(convertComponentString(field, 3), field.getName());
		dataRow.addColumnValue(convertNumber(field.getComponent(1), field.getComponentAsNumber(4)), field.getName());
	}


	public boolean isIncludeStatementLines() {
		return this.includeStatementLines;
	}


	public void setIncludeStatementLines(boolean includeStatementLines) {
		this.includeStatementLines = includeStatementLines;
	}


	public List<ColumnHeader> getWorkingHeaders() {
		return this.workingHeaders;
	}


	public void setWorkingHeaders(List<ColumnHeader> workingHeaders) {
		this.workingHeaders = workingHeaders;
	}
}
