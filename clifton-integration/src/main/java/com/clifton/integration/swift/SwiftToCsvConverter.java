package com.clifton.integration.swift;


import java.io.IOException;
import java.io.Reader;
import java.io.Writer;


/**
 * @author TerryS
 */
public interface SwiftToCsvConverter {

	public int output(Reader swift, Writer csv) throws IOException;
}
