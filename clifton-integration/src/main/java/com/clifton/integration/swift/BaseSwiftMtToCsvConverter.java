package com.clifton.integration.swift;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.prowidesoftware.swift.model.field.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;


/**
 * @author TerryS
 */
public abstract class BaseSwiftMtToCsvConverter implements SwiftToCsvConverter {

	protected String convertDate(String strDate, String format) {
		if (StringUtils.isEmpty(strDate)) {
			return "";
		}
		Date date = DateUtils.toDate(strDate, format);
		return DateUtils.fromDateISOSimple(date);
	}


	protected String convertNumber(String sign, Number number) {
		if (number == null) {
			return "";
		}
		StringBuilder builder = new StringBuilder();
		if ("D".equalsIgnoreCase(sign)) {
			builder.append("-");
		}
		builder.append(number);
		return builder.toString();
	}


	protected String convertComponentString(Field field, int component) {
		return Optional.ofNullable(field.getComponent(component)).orElse("");
	}


	protected void addEmptyString(List<String> values, int repeats) {
		for (int i = 0; i < repeats; i++) {
			values.add("");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	///////                      Inner  Classes                        /////////
	////////////////////////////////////////////////////////////////////////////

	public static class ColumnHeader {

		private final String header;
		private final List<String> numbers;


		public ColumnHeader(String header, String... numbers) {
			this.header = header;
			this.numbers = Arrays.asList(numbers);
		}


		public String getHeader() {
			return this.header;
		}


		public List<String> getNumbers() {
			return this.numbers;
		}
	}

	public static class DataRow {

		private final List<ColumnHeader> columnHeaders;
		private final List<String> values;
		private final List<String> columns;
		private final AtomicInteger index = new AtomicInteger(0);


		public DataRow(DataRow dataRow) {
			this.columnHeaders = dataRow.columnHeaders;
			this.values = new ArrayList<>(dataRow.values);
			this.columns = new ArrayList<>(dataRow.columns);
			this.index.set(dataRow.index.get());
		}


		public DataRow(List<ColumnHeader> columnHeaders) {
			this.columnHeaders = columnHeaders;
			this.values = new ArrayList<>(columnHeaders.size());
			this.columns = new ArrayList<>(columnHeaders.size());
		}


		public void addRequiredColumnValue(Field field, Function<Field, String> supplier, Consumer<Field> validation) {
			validation.accept(field);
			addColumnValue(supplier.apply(field), field.getName());
		}


		public boolean addOptionalColumnValue(Field field, Function<Field, String> supplier, String fieldName) {
			if (fieldName.equals(field.getName())) {
				addColumnValue(supplier.apply(field), field.getName());
				return true;
			}
			else {
				addEmptyColumnValue(fieldName);
				return false;
			}
		}


		public void addColumnValue(String value, String column) {
			ColumnHeader header = this.columnHeaders.get(this.index.get());
			AssertUtils.assertTrue(header.getNumbers().contains(column), () -> String.format("Column value mismatch.  Expected [%s] but is [%s].", header.getNumbers(), column));
			this.values.add(value);
			this.columns.add(column);
			this.index.getAndIncrement();
		}


		public void addColumnValues(DataRow row) {
			for (int i = 0; i < row.columnHeaders.size(); i++) {
				addColumnValue(row.values.get(i), row.columns.get(i));
			}
		}


		public void addEmptyColumnValue(String column) {
			addEmptyColumnValue(1, column);
		}


		public void addEmptyColumnValue(int repeats, String column) {
			for (int i = 0; i < repeats; i++) {
				this.values.add("");
				this.columns.add(column);
				this.index.getAndIncrement();
			}
		}


		public List<String> getValues() {
			AssertUtils.assertEquals(this.values.size(), this.index.get(), "Column value size mismatch.");
			return this.values;
		}
	}
}
