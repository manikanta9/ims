package com.clifton.integration.security.encryption;

import com.clifton.core.messaging.MessageStatus;
import com.clifton.core.util.StringUtils;
import com.clifton.integration.incoming.server.IntegrationServerStatusProcessorService;
import com.clifton.security.encryption.transport.SecurityRSATransportEncryptionKeyDetail;
import com.clifton.security.encryption.transport.client.SecurityRSATransportEncryptionClientHandler;
import org.springframework.stereotype.Service;


/**
 * @author theodorez
 */
@Service
public class IntegrationSecurityRSATransportEncryptionServiceImpl implements IntegrationSecurityRSATransportEncryptionService {

	private IntegrationServerStatusProcessorService integrationServerStatusProcessorService;
	private SecurityRSATransportEncryptionClientHandler securityRSATransportEncryptionClientHandler;


	@Override
	public SecurityRSATransportEncryptionKeyDetail getIntegrationTransportEncryptionKeyDetails() {
		return getSecurityRSATransportEncryptionClientHandler().getPublicEncryptionKeyDetails();
	}


	@Override
	public void setupIntegrationTransportEncryptionKey() {
		String publicKey = null;
		MessageStatus messageStatus = getIntegrationServerStatusProcessorService().getIntegrationTransportEncryptionPublicKey();
		if (messageStatus.isError()) {
			throw new RuntimeException("Error occurred when trying retrieve Integration Public Key", messageStatus.getException());
		}

		if (StringUtils.isEqual(messageStatus.getResponseName(), MessageStatus.INTEGRATION_PUBLIC_KEY)) {
			publicKey = messageStatus.getResponseStatus();
		}
		if (StringUtils.isEmpty(publicKey)) {
			throw new RuntimeException("No public key was returned from Integration");
		}
		else {
			getSecurityRSATransportEncryptionClientHandler().setupPublicEncryptionKey(publicKey);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public IntegrationServerStatusProcessorService getIntegrationServerStatusProcessorService() {
		return this.integrationServerStatusProcessorService;
	}


	public void setIntegrationServerStatusProcessorService(IntegrationServerStatusProcessorService integrationServerStatusProcessorService) {
		this.integrationServerStatusProcessorService = integrationServerStatusProcessorService;
	}


	public SecurityRSATransportEncryptionClientHandler getSecurityRSATransportEncryptionClientHandler() {
		return this.securityRSATransportEncryptionClientHandler;
	}


	public void setSecurityRSATransportEncryptionClientHandler(SecurityRSATransportEncryptionClientHandler securityRSATransportEncryptionClientHandler) {
		this.securityRSATransportEncryptionClientHandler = securityRSATransportEncryptionClientHandler;
	}
}
