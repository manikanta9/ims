package com.clifton.integration.incoming.definition.cache;

import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportRunBuilder;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRunEventBuilder;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatus;
import com.clifton.integration.incoming.definition.IntegrationImportStatusBuilder;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
@Disabled("Asynchronous testing failures occurring too frequently; research alternatives.")
public class IntegrationRunEventTimedEvictionCacheTest {

	@InjectMocks
	private IntegrationRunEventTimedEvictionCache integrationRunEventTimedEvictionCache = new IntegrationRunEventTimedEvictionCache();

	@Mock(lenient = true)
	private IntegrationImportService integrationImportService;

	private Map<IntegrationImportStatusTypes, IntegrationImportStatus> statusMap = new HashMap<>();


	@BeforeEach
	public void setupMocks() {
		IntegrationImportStatus running = IntegrationImportStatusBuilder.anIntegrationImportStatus()
				.withId((short) 1)
				.withName("RUNNING")
				.withDescription("RUNNING")
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING)).thenReturn(running);
		this.statusMap.put(IntegrationImportStatusTypes.RUNNING, running);

		IntegrationImportStatus successful = IntegrationImportStatusBuilder.anIntegrationImportStatus()
				.withId((short) 2)
				.withName("SUCCESSFUL")
				.withDescription("SUCCESSFUL")
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL)).thenReturn(successful);
		this.statusMap.put(IntegrationImportStatusTypes.SUCCESSFUL, successful);

		IntegrationImportStatus processedWithErrors = IntegrationImportStatusBuilder.anIntegrationImportStatus()
				.withId((short) 3)
				.withName("PROCESSED_WITH_ERRORS")
				.withDescription("PROCESSED_WITH_ERRORS")
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.PROCESSED_WITH_ERRORS)).thenReturn(processedWithErrors);
		this.statusMap.put(IntegrationImportStatusTypes.PROCESSED_WITH_ERRORS, processedWithErrors);

		IntegrationImportStatus error = IntegrationImportStatusBuilder.anIntegrationImportStatus()
				.withId((short) 4)
				.withName("ERROR")
				.withDescription("ERROR")
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR)).thenReturn(error);
		this.statusMap.put(IntegrationImportStatusTypes.ERROR, error);

		IntegrationImportStatus notProcessed = IntegrationImportStatusBuilder.anIntegrationImportStatus()
				.withId((short) 9)
				.withName("NOT_PROCESSED")
				.withDescription("NOT_PROCESSED")
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.NOT_PROCESSED)).thenReturn(notProcessed);
		this.statusMap.put(IntegrationImportStatusTypes.NOT_PROCESSED, notProcessed);
	}


	@AfterEach
	public void teardown() {
		this.integrationRunEventTimedEvictionCache.destroy();
	}


	@Test
	public void singleEventNotProcessed() {
		IntegrationImportStatus running = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING);
		Assertions.assertNotNull(running);
		IntegrationImportRun integrationImportRun = IntegrationImportRunBuilder.anIntegrationImportRun()
				.withId(1)
				.withStatus(running)
				.build();
		IntegrationImportStatus notProcessed = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.NOT_PROCESSED);
		Assertions.assertNotNull(notProcessed);
		IntegrationImportRunEvent integrationImportRunEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 1)
				.withStatus(notProcessed)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(integrationImportRunEvent.getId())).thenReturn(integrationImportRunEvent);
		this.integrationRunEventTimedEvictionCache.add(integrationImportRunEvent, Duration.of(2, ChronoUnit.MINUTES));

		this.integrationRunEventTimedEvictionCache.evict(integrationImportRunEvent.getId());

		// allow executor to process the evictions
		Assertions.assertTimeoutPreemptively(Duration.of(15, ChronoUnit.SECONDS), () -> {
			for (; ; ) {
				if (this.integrationRunEventTimedEvictionCache.getImportRunRunEventIdMap().get(integrationImportRun.getId()).isEmpty()) {
					break;
				}
			}
		});

		ArgumentCaptor<IntegrationImportRun> integrationImportRunCaptor = ArgumentCaptor.forClass(IntegrationImportRun.class);
		Mockito.verify(this.integrationImportService, Mockito.timeout(60).times(1)).saveIntegrationImportRun(integrationImportRunCaptor.capture());
		Assertions.assertEquals(this.statusMap.get(IntegrationImportStatusTypes.SUCCESSFUL).getName(), integrationImportRunCaptor.getAllValues().get(0).getStatus().getName());
	}


	@Test
	public void multipleEventsNotProcessedSuccess() {
		IntegrationImportStatus running = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING);
		Assertions.assertNotNull(running);
		IntegrationImportRun integrationImportRun = IntegrationImportRunBuilder.anIntegrationImportRun()
				.withId(1)
				.withStatus(running)
				.build();

		IntegrationImportStatus notProcessed = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.NOT_PROCESSED);
		Assertions.assertNotNull(notProcessed);
		IntegrationImportRunEvent notProcessedEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 1)
				.withStatus(notProcessed)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(notProcessedEvent.getId())).thenReturn(notProcessedEvent);
		this.integrationRunEventTimedEvictionCache.add(notProcessedEvent, Duration.of(2, ChronoUnit.MINUTES));

		IntegrationImportStatus successful = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL);
		Assertions.assertNotNull(successful);
		IntegrationImportRunEvent successfulEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 2)
				.withStatus(successful)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(successfulEvent.getId())).thenReturn(successfulEvent);
		this.integrationRunEventTimedEvictionCache.add(successfulEvent, Duration.of(2, ChronoUnit.MINUTES));

		this.integrationRunEventTimedEvictionCache.evict(notProcessedEvent.getId());
		this.integrationRunEventTimedEvictionCache.evict(successfulEvent.getId());

		// allow executor to process the evictions
		Assertions.assertTimeoutPreemptively(Duration.of(15, ChronoUnit.SECONDS), () -> {
			for (; ; ) {
				if (this.integrationRunEventTimedEvictionCache.getImportRunRunEventIdMap().get(integrationImportRun.getId()).isEmpty()) {
					break;
				}
			}
		});

		ArgumentCaptor<IntegrationImportRun> integrationImportRunCaptor = ArgumentCaptor.forClass(IntegrationImportRun.class);
		Mockito.verify(this.integrationImportService, Mockito.timeout(60).times(1)).saveIntegrationImportRun(integrationImportRunCaptor.capture());
		Assertions.assertEquals(this.statusMap.get(IntegrationImportStatusTypes.SUCCESSFUL).getName(), integrationImportRunCaptor.getAllValues().get(0).getStatus().getName());
	}


	@Test
	public void multipleEventsSuccessNotProcessed() {
		IntegrationImportStatus running = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING);
		Assertions.assertNotNull(running);
		IntegrationImportRun integrationImportRun = IntegrationImportRunBuilder.anIntegrationImportRun()
				.withId(1)
				.withStatus(running)
				.build();

		IntegrationImportStatus successful = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL);
		Assertions.assertNotNull(successful);
		IntegrationImportRunEvent successfulEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 2)
				.withStatus(successful)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(successfulEvent.getId())).thenReturn(successfulEvent);
		this.integrationRunEventTimedEvictionCache.add(successfulEvent, Duration.of(2, ChronoUnit.MINUTES));

		IntegrationImportStatus notProcessed = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.NOT_PROCESSED);
		Assertions.assertNotNull(notProcessed);
		IntegrationImportRunEvent notProcessedEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 1)
				.withStatus(notProcessed)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(notProcessedEvent.getId())).thenReturn(notProcessedEvent);
		this.integrationRunEventTimedEvictionCache.add(notProcessedEvent, Duration.of(2, ChronoUnit.MINUTES));

		this.integrationRunEventTimedEvictionCache.evict(successfulEvent.getId());
		this.integrationRunEventTimedEvictionCache.evict(notProcessedEvent.getId());

		// allow executor to process the evictions
		Assertions.assertTimeoutPreemptively(Duration.of(15, ChronoUnit.SECONDS), () -> {
			for (; ; ) {
				if (this.integrationRunEventTimedEvictionCache.getImportRunRunEventIdMap().get(integrationImportRun.getId()).isEmpty()) {
					break;
				}
			}
		});

		ArgumentCaptor<IntegrationImportRun> integrationImportRunCaptor = ArgumentCaptor.forClass(IntegrationImportRun.class);
		Mockito.verify(this.integrationImportService, Mockito.timeout(60).times(1)).saveIntegrationImportRun(integrationImportRunCaptor.capture());
		Assertions.assertEquals(this.statusMap.get(IntegrationImportStatusTypes.SUCCESSFUL).getName(), integrationImportRunCaptor.getAllValues().get(0).getStatus().getName());
	}


	@Test
	public void multipleEventsNotProcessedError() {
		IntegrationImportStatus running = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING);
		Assertions.assertNotNull(running);
		IntegrationImportRun integrationImportRun = IntegrationImportRunBuilder.anIntegrationImportRun()
				.withId(1)
				.withStatus(running)
				.build();

		IntegrationImportStatus notProcessed = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.NOT_PROCESSED);
		Assertions.assertNotNull(notProcessed);
		IntegrationImportRunEvent notProcessedEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 1)
				.withStatus(notProcessed)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(notProcessedEvent.getId())).thenReturn(notProcessedEvent);
		this.integrationRunEventTimedEvictionCache.add(notProcessedEvent, Duration.of(2, ChronoUnit.MINUTES));

		IntegrationImportStatus error = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR);
		Assertions.assertNotNull(error);
		IntegrationImportRunEvent errorEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 3)
				.withStatus(error)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(errorEvent.getId())).thenReturn(errorEvent);
		this.integrationRunEventTimedEvictionCache.add(errorEvent, Duration.of(2, ChronoUnit.MINUTES));

		this.integrationRunEventTimedEvictionCache.evict(notProcessedEvent.getId());
		this.integrationRunEventTimedEvictionCache.evict(errorEvent.getId());

		// allow executor to process the evictions
		Assertions.assertTimeoutPreemptively(Duration.of(15, ChronoUnit.SECONDS), () -> {
			for (; ; ) {
				if (this.integrationRunEventTimedEvictionCache.getImportRunRunEventIdMap().get(integrationImportRun.getId()).isEmpty()) {
					break;
				}
			}
		});

		ArgumentCaptor<IntegrationImportRun> integrationImportRunCaptor = ArgumentCaptor.forClass(IntegrationImportRun.class);
		Mockito.verify(this.integrationImportService, Mockito.timeout(60).times(2)).saveIntegrationImportRun(integrationImportRunCaptor.capture());
		Assertions.assertEquals(this.statusMap.get(IntegrationImportStatusTypes.PROCESSED_WITH_ERRORS).getName(), integrationImportRunCaptor.getAllValues().get(1).getStatus().getName());
	}


	@Test
	public void multipleEventsErrorNotProcessed() {
		IntegrationImportStatus running = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING);
		Assertions.assertNotNull(running);
		IntegrationImportRun integrationImportRun = IntegrationImportRunBuilder.anIntegrationImportRun()
				.withId(1)
				.withStatus(running)
				.build();

		IntegrationImportStatus error = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR);
		Assertions.assertNotNull(error);
		IntegrationImportRunEvent errorEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 3)
				.withStatus(error)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(errorEvent.getId())).thenReturn(errorEvent);
		this.integrationRunEventTimedEvictionCache.add(errorEvent, Duration.of(2, ChronoUnit.MINUTES));

		IntegrationImportStatus notProcessed = this.integrationImportService.getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.NOT_PROCESSED);
		Assertions.assertNotNull(notProcessed);
		IntegrationImportRunEvent notProcessedEvent = IntegrationImportRunEventBuilder.anIntegrationImportRunEvent()
				.withId((long) 1)
				.withStatus(notProcessed)
				.withImportRun(integrationImportRun)
				.build();
		Mockito.when(this.integrationImportService.getIntegrationImportRunEvent(notProcessedEvent.getId())).thenReturn(notProcessedEvent);
		this.integrationRunEventTimedEvictionCache.add(notProcessedEvent, Duration.of(2, ChronoUnit.MINUTES));

		this.integrationRunEventTimedEvictionCache.evict(errorEvent.getId());
		this.integrationRunEventTimedEvictionCache.evict(notProcessedEvent.getId());

		// allow executor to process the evictions
		Assertions.assertTimeoutPreemptively(Duration.of(15, ChronoUnit.SECONDS), () -> {
			for (; ; ) {
				if (this.integrationRunEventTimedEvictionCache.getImportRunRunEventIdMap().get(integrationImportRun.getId()).isEmpty()) {
					break;
				}
			}
		});

		ArgumentCaptor<IntegrationImportRun> integrationImportRunCaptor = ArgumentCaptor.forClass(IntegrationImportRun.class);
		Mockito.verify(this.integrationImportService, Mockito.timeout(60).times(2)).saveIntegrationImportRun(integrationImportRunCaptor.capture());
		Assertions.assertEquals(this.statusMap.get(IntegrationImportStatusTypes.PROCESSED_WITH_ERRORS).getName(), integrationImportRunCaptor.getAllValues().get(1).getStatus().getName());
	}
}
