package com.clifton.integration.incoming.transformation.parameter;

import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.system.schema.SystemDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;


/**
 * @author TerryS
 */
@ExtendWith({SpringExtension.class, MockitoExtension.class})
@ContextConfiguration
public class IntegrationTransformationParameterHandlerImplTest {

	@Resource
	private IntegrationTransformationParameterHandler integrationTransformationParameterHandler;

	@Resource
	private IntegrationTransformationParameterService integrationTransformationParameterService;

	private IntegrationTransformationParameterLink link;


	@BeforeEach
	public void setup() {
		IntegrationTransformation integrationTransformation = new IntegrationTransformation();
		integrationTransformation.setId(1);

		SystemDataSource systemDataSource = new SystemDataSource();
		systemDataSource.setId((short) 4);
		systemDataSource.setName("testDataSource");
		systemDataSource.setDescription("Test Database");
		systemDataSource.setDatabaseName("testDataSource");

		IntegrationTransformationParameter integrationTransformationParameter = new IntegrationTransformationParameter();
		integrationTransformationParameter.setId(1);
		integrationTransformationParameter.setType(IntegrationTransformationParameterTypes.DATA_SOURCE);
		integrationTransformationParameter.setName("testDataSource");
		integrationTransformationParameter.setKey("TEST");
		integrationTransformationParameter.setSystemDataSource(systemDataSource);

		this.link = new IntegrationTransformationParameterLink();
		this.link.setId(1);
		this.link.setReferenceOne(integrationTransformation);
		this.link.setReferenceTwo(integrationTransformationParameter);
	}


	@Test
	public void testJtds() {
		Mockito.when(this.integrationTransformationParameterService.getIntegrationTransformationParameterLinkListByParent(1)).thenReturn(Collections.singletonList(this.link));
		Map<String, String> parameterMap = this.integrationTransformationParameterHandler.getIntegrationTransformationParameterMap(this.link.getReferenceOne());

		Assertions.assertEquals("testUser", parameterMap.get("TEST_USERNAME"));
		Assertions.assertEquals("testDataSource", parameterMap.get("TEST_DATABASE"));
		Assertions.assertEquals("1433", parameterMap.get("TEST_PORT"));
		Assertions.assertEquals("testPassword", parameterMap.get("TEST_PASSWORD"));
		Assertions.assertEquals("testing.paraport.com", parameterMap.get("TEST_HOSTNAME"));
	}


	@Test
	public void testMsSql() {
		IntegrationTransformationParameter parameter = this.link.getReferenceTwo();
		parameter.setName("dataSource");
		SystemDataSource dataSource = parameter.getSystemDataSource();
		dataSource.setId((short) 1);
		dataSource.setName("dataSource");
		dataSource.setDatabaseName("CliftonIntegration");
		dataSource.setConnectionUrl("jdbc:sqlserver://localhost:1433;databaseName=CliftonIntegration;encrypt=false");
		dataSource.setDefaultDataSource(true);

		Mockito.when(this.integrationTransformationParameterService.getIntegrationTransformationParameterLinkListByParent(1)).thenReturn(Collections.singletonList(this.link));
		Map<String, String> parameterMap = this.integrationTransformationParameterHandler.getIntegrationTransformationParameterMap(this.link.getReferenceOne());
		commonAssertions(parameterMap);

		Assertions.assertEquals("usr", parameterMap.get("TEST_USERNAME"));
		Assertions.assertEquals("CliftonIntegration", parameterMap.get("TEST_DATABASE"));
		Assertions.assertEquals("1433", parameterMap.get("TEST_PORT"));
		Assertions.assertEquals("psswd", parameterMap.get("TEST_PASSWORD"));
		Assertions.assertEquals("localhost", parameterMap.get("TEST_HOSTNAME"));
	}


	private void commonAssertions(Map<String, String> parameterMap) {
		Assertions.assertTrue(parameterMap.containsKey("TEST_USERNAME"), "Should contain TEST_USERNAME");
		Assertions.assertTrue(parameterMap.containsKey("TEST_DATABASE"), "Should contain TEST_DATABASE");
		Assertions.assertTrue(parameterMap.containsKey("TEST_PORT"), "Should contain TEST_PORT");
		Assertions.assertTrue(parameterMap.containsKey("TEST_PASSWORD"), "Should contain TEST_PASSWORD");
		Assertions.assertTrue(parameterMap.containsKey("TEST_HOSTNAME"), "Should contain TEST_HOSTNAME");
	}
}
