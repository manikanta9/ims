package com.clifton.integration;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class IntegrationProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "integration";
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> result = super.getAllowedContextManagedBeanSuffixNames();
		// NOTE: should we have better names instead of ignoring?
		result.add("integrationClientIncomingQueue");
		result.add("integrationClientJmsTemplate");
		result.add("integrationClientMessageSender");
		result.add("integrationMessageConsumer");
		result.add("integrationSynchronousListener");
		return result;
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		Set<String> methodsToSkip = new HashSet<>();
		methodsToSkip.add("getIntegrationReconcilePositionHistoryList");
		methodsToSkip.add("getIntegrationManagerPositionHistoryList");
		methodsToSkip.add("getIntegrationReconcileAccountBalancesByAccountTypeList");
		methodsToSkip.add("getIntegrationReconcileDailyPNLHistoryList");
		methodsToSkip.add("saveIntegrationExportIntegrationExportDestination");
		methodsToSkip.add("getIntegrationReconcileAccountM2MDailyExpenseList");
		methodsToSkip.add("saveIntegrationImportDefinition");
		methodsToSkip.add("saveIntegrationTransformationFile");

		return methodsToSkip.contains(method.getName());
	}


	@Override
	public Set<String> getEnumAllowedNamesOverride() {
		// Not refactoring integration at this time
		Set<String> names = new HashSet<>();
		names.add("com.clifton.integration.definition.ImportStatus");
		names.add("com.clifton.integration.file.archive.naming.IntegrationFileArchivePathConvention");
		names.add("com.clifton.integration.file.IntegrationDirectory");
		names.add("com.clifton.integration.file.IntegrationFileDecryptionType");
		names.add("com.clifton.integration.file.IntegrationFileEncryptionType");
		names.add("com.clifton.integration.file.IntegrationFileSourceType");
		names.add("com.clifton.integration.messages.IntegrationAction");
		names.add("com.clifton.integration.trade.intraday.IntegrationTradeIntradayStatus");
		names.add("com.clifton.integration.trade.intraday.IntegrationTradeIntradayType");
		return names;
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.text.SimpleDateFormat");
		imports.add("java.nio.");
		imports.add("java.security.");
		imports.add("org.apache.commons.io.FilenameUtils");
		imports.add("org.apache.commons.csv.CSVFormat");
		imports.add("org.apache.commons.csv.CSVPrinter");
		imports.add("org.apache.commons.csv.CSVParser");
		imports.add("org.apache.commons.csv.CSVRecord");
		imports.add("org.springframework.messaging.");
		imports.add("com.prowidesoftware.swift.");
		imports.add("org.springframework.beans.BeansException");
		imports.add("java.net.URI");
	}
}
