package com.clifton.integration.file;

import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


public class IntegrationFileUtilsTests {

	@Test
	public void testSeattleFileNames() {
		Assertions.assertEquals("MN_Appraisal.txt", IntegrationFileUtils.getOriginalFileName("MN_Appraisal_031816.txt"));
		Assertions.assertEquals("MN_Appraisal.txt", IntegrationFileUtils.getOriginalFileName("MN_Appraisal.031816.txt"));
		Assertions.assertEquals("MN_Appraisal.txt", IntegrationFileUtils.getOriginalFileName("MN_Appraisal031816.txt"));

		Assertions.assertEquals(DateUtils.toDate("20160318", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("PMN_Appraisal_031816.txt"));
		Assertions.assertEquals(DateUtils.toDate("20160318", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("PMN_Appraisal.031816.txt"));
		Assertions.assertEquals(DateUtils.toDate("20160318", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("PMN_Appraisal031816.txt"));
	}


	@Test
	public void testMarkitFileNames() {
		Assertions.assertEquals("Parametric_NY_Nextday.csv", IntegrationFileUtils.getOriginalFileName("Parametric_NY_Nextday.19Feb16.csv"));
		Assertions.assertEquals(DateUtils.toDate("20160219", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("Parametric_NY_Nextday.19Feb16.csv"));
		Assertions.assertEquals(DateUtils.toDate("20160219", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("Parametric_NY_Nextday.19Feb16_part_1456118967364.csv"));
		Assertions.assertEquals(DateUtils.toDate("20160224", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("Parametric_NY_Nextday.24Feb16_part_1456377492407.csv"));
		Assertions.assertEquals(DateUtils.toDate("20161231", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("Parametric_NY_Nextday.31Dec16_part_1456377492407.csv"));
	}


	@Test
	public void testEndingDash() {
		Assertions.assertEquals("cliftonClearedPositionsIceTrust.csv", IntegrationFileUtils.getOriginalFileName("cliftonClearedPositionsIceTrust-20131106.csv"));
	}


	@Test
	public void testBNYMellonFileName() {
		Assertions.assertEquals(DateUtils.toDate("20141022", "yyyyMMdd"), DateUtils.clearTime(IntegrationFileUtils.getEffectiveDateFromFileName("Mellon_Daily_20141022_121222516_49123977.xls")));
	}


	@Test
	public void testIntradayTradeFileName() {
		Assertions.assertEquals(DateUtils.toDate("2016-06-17 12:01:00", DateUtils.DATE_FORMAT_FULL), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intraday_trades_20160617-1201.csv")); // Goldman
		Assertions.assertEquals(DateUtils.toDate("2016-06-17 12:49:00", DateUtils.DATE_FORMAT_FULL), IntegrationFileUtils.getEffectiveDateFromFileName("CliftonIntraday_201606171249.csv")); // UBS
		Assertions.assertEquals(DateUtils.toDate("2016-06-17 07:54:00", DateUtils.DATE_FORMAT_FULL), IntegrationFileUtils.getEffectiveDateFromFileName("rosecgmiTradesINT_rosemary3_201606170754.csv")); // CITI
		Assertions.assertEquals(DateUtils.toDate("2016-06-17 07:30:00", DateUtils.DATE_FORMAT_FULL), IntegrationFileUtils.getEffectiveDateFromFileName("CWD_Trade_Confirmations_BBG_201606170730.csv")); // J.P. Morgan
		Assertions.assertEquals(DateUtils.toDate("2016-04-22 18:12:45", DateUtils.DATE_FORMAT_FULL), IntegrationFileUtils.getEffectiveDateFromFileName("IntradayCustomConfirms.20160422.181245.csv")); // Morgan Stanley
		Assertions.assertEquals(DateUtils.toDate("2016-06-16 06:43:48", DateUtils.DATE_FORMAT_FULL), IntegrationFileUtils.getEffectiveDateFromFileName("IntradayCustomConfirms.20160616.064348.csv")); // Morgan Stanley
	}


	@Test
	//cliftonClearedPositionsIceTrust-20131106.csv
	public void testGoldmanSwapFileName() {
		Assertions.assertEquals(DateUtils.toDate("20131001", "yyyyMMdd"), DateUtils.clearTime(IntegrationFileUtils.getEffectiveDateFromFileName("rosecgmiMonthlyStatement_2769112E_20131001.pdf")));

		Assertions.assertEquals(DateUtils.toDate("20131031", "yyyyMMdd"),
				DateUtils.clearTime(IntegrationFileUtils.getEffectiveDateFromFileName("GS-OTCSDIReporting-Interest_Report-10_29_GMT-20131031-14457-0__2013-10-31_05_29_31.csv")));
		Assertions.assertEquals(DateUtils.toDate("20131031", "yyyyMMdd"),
				DateUtils.clearTime(IntegrationFileUtils.getEffectiveDateFromFileName("GS-OTCSDIReporting-Interest_Report-10_29_GMT-20131031-14457-0__2013-10-31_05_29_31.csv")));
	}


	@Test
	public void testGetOriginalFileName() {
		Assertions.assertEquals("Northern26.txt", IntegrationFileUtils.getOriginalFileName("Northern26.txt"));
		Assertions.assertEquals("Northern26.txt", IntegrationFileUtils.getOriginalFileName("Northern26__2011-01-10_10_54_17.txt"));

		Assertions.assertEquals("GSC_confirmed_trades.csv".toLowerCase(), IntegrationFileUtils.getOriginalFileName("20120723.GSC_confirmed_trades__2012-07-23_22_43_17.csv").toLowerCase());
		Assertions.assertEquals("CLIFTON_cash.CSV.pgp".toLowerCase(), IntegrationFileUtils.getOriginalFileName("CLIFTON_cash_20120620.CSV.pgp").toLowerCase());
		Assertions.assertEquals("CLIFTON_cash.CSV.pgp".toLowerCase(), IntegrationFileUtils.getOriginalFileName("CLIFTON_cash_20120620.CSV__2012-07-23_22_43_17.pgp").toLowerCase());
		Assertions.assertEquals("CLIFTON_cash.CSV".toLowerCase(), IntegrationFileUtils.getOriginalFileName("CLIFTON_cash_20120620.CSV").toLowerCase());
		Assertions.assertEquals("CLIFTON_cash.CSV".toLowerCase(), IntegrationFileUtils.getOriginalFileName("CLIFTON_cash_20120620__2012-07-23_22_43_17.CSV").toLowerCase());

		Assertions.assertEquals("clifton_intra.CSV.pgp".toLowerCase(), IntegrationFileUtils.getOriginalFileName("clifton_intra20120621070005.CSV.pgp").toLowerCase());
		Assertions.assertEquals("clifton_intra.CSV".toLowerCase(), IntegrationFileUtils.getOriginalFileName("clifton_intra20120621070005.CSV").toLowerCase());
		Assertions.assertEquals("clifton_intra.CSV.pgp".toLowerCase(), IntegrationFileUtils.getOriginalFileName("clifton_intra20120621070005.CSV__2012-07-23_22_43_17.pgp").toLowerCase());
		Assertions.assertEquals("clifton_intra.CSV".toLowerCase(), IntegrationFileUtils.getOriginalFileName("clifton_intra20120621070005__2012-07-23_22_43_17.CSV").toLowerCase());

		Assertions.assertEquals("CLIFTON.CSV.pgp".toLowerCase(), IntegrationFileUtils.getOriginalFileName("CLIFTON20120620.CSV.pgp").toLowerCase());

		Assertions.assertEquals("MS_CLIFTON_GR_26516491.zip.pgp".toLowerCase(), IntegrationFileUtils.getOriginalFileName("20120620_MS_CLIFTON_GR_26516491.zip.pgp").toLowerCase());

		Assertions.assertEquals("CliftonSettlements.csv".toLowerCase(), IntegrationFileUtils.getOriginalFileName("CliftonSettlements_20120723.csv").toLowerCase());
		Assertions.assertEquals("rosecgmiTradesCOB_rosemary.csv.pgp".toLowerCase(), IntegrationFileUtils.getOriginalFileName("rosecgmiTradesCOB_rosemary_201206200000.csv.pgp").toLowerCase());

		Assertions.assertEquals("OTC-CliftonOTC200X.csv", IntegrationFileUtils.getOriginalFileName("OTC-CliftonOTC200X.20140820__2014-08-20_23_53_09.csv"));
	}


	@Test
	public void testGetEffectiveDate() {
		Assertions.assertNull(IntegrationFileUtils.getEffectiveDateFromFileName("Northern26.txt"));
		Assertions.assertNull(IntegrationFileUtils.getEffectiveDateFromFileName("Northern26__2011-01-10_10_54_17.txt"));

		Assertions.assertEquals(DateUtils.toDate("20120723", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("20120723.GSC_confirmed_trades__2012-07-23_22_43_17.csv"));
		Assertions.assertEquals(DateUtils.toDate("20120620", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("CLIFTON_cash_20120620.CSV.pgp"));
		Assertions.assertEquals(DateUtils.toDate("20120620", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("CLIFTON_cash_20120620.CSV__2012-07-23_22_43_17.pgp"));
		Assertions.assertEquals(DateUtils.toDate("20120620", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("CLIFTON_cash_20120620.CSV"));
		Assertions.assertEquals(DateUtils.toDate("20120620", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("CLIFTON_cash_20120620__2012-07-23_22_43_17.CSV"));

		Assertions.assertEquals(DateUtils.toDate("20120621070005", "yyyyMMddHHmmss"), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intra20120621070005.CSV.pgp"));
		Assertions.assertEquals(DateUtils.toDate("20120621070005", "yyyyMMddHHmmss"), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intra20120621070005.CSV"));
		Assertions.assertEquals(DateUtils.toDate("20120620", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("CLIFTON20120620.CSV.pgp"));

		Assertions.assertEquals(DateUtils.toDate("20120620", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("20120620_MS_CLIFTON_GR_26516491.zip.pgp"));

		Assertions.assertEquals(DateUtils.toDate("20120723", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("CliftonSettlements_20120723.csv"));
		Assertions.assertEquals(DateUtils.toDate("201206200000", "yyyyMMddHHmm"), IntegrationFileUtils.getEffectiveDateFromFileName("rosecgmiTradesCOB_rosemary_201206200000.csv.pgp"));

		Assertions.assertEquals(DateUtils.toDate("20121206093108", "yyyyMMddHHmmss"), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intra20121206103108.CSV.pgp", "Etc/GMT+5"));
		Assertions.assertEquals(DateUtils.toDate("20121206043108", "yyyyMMddHHmmss"), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intra20121206103108.CSV.pgp", "Etc/GMT+0"));
		Assertions.assertEquals(DateUtils.toDate("201212060931", "yyyyMMddHHmm"), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intra201212061031.CSV.pgp", "Etc/GMT+5"));
		Assertions.assertEquals(DateUtils.toDate("201212060431", "yyyyMMddHHmm"), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intra201212061031.CSV.pgp", "Etc/GMT+0"));
		Assertions.assertEquals(DateUtils.toDate("201212052001", "yyyyMMddHHmm"), IntegrationFileUtils.getEffectiveDateFromFileName("clifton_intraday_trades_20121206-0201.csv.pgp", "Etc/GMT+0"));

		Assertions.assertEquals(DateUtils.toDate("20200921", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("EWATPOS200921.csv"));
		Assertions.assertEquals(DateUtils.toDate("20201015", "yyyyMMdd"), IntegrationFileUtils.getEffectiveDateFromFileName("EWATPOS201015.csv"));
	}


	@Test
	public void testFileHandlerUtils() {
		String fileName = "Chase Pending Transactions Summary__2011-01-06_13_15_09.xls";
		Assertions.assertEquals("Chase Pending Transactions Summary.xls", IntegrationFileUtils.getOriginalFileName(fileName));
		fileName = "20101208.open_positions__2011-01-20_16_17_26.csv";
		Assertions.assertEquals("open_positions.csv", IntegrationFileUtils.getOriginalFileName(fileName));
		//Assertions.assertEquals(DateUtils.toDate("12/08/2010"), IntegrationIntegrationFileUtils.getEffectiveDateFromFileName(fileName));
		fileName = "open_positions.csv";
		Assertions.assertEquals("open_positions.csv", IntegrationFileUtils.getOriginalFileName(fileName));
		Date date = DateUtils.toDate("12/08/2010");
		Assertions.assertEquals("open_positions__2010-12-08_00_00_00.csv", IntegrationFileUtils.appendTimeStamp(fileName, date));
	}


	@Test
	public void testFileNameUtils() {
		Assertions.assertEquals("roseFXRates.csv.pgp", IntegrationFileUtils.getOriginalFileName("roseFXRates__201208131700.csv.pgp"));
		Assertions.assertEquals("roseFXRates__201208141700.csv", IntegrationFileUtils.getOriginalFileName("roseFXRates__201208141700__2012-08-15_16_07_01.csv", false));
		Assertions.assertEquals("roseFXRates__201208141700.csv", IntegrationFileUtils.getOriginalFileName("roseFXRates__201208141700.csv", false));
		Assertions.assertEquals("roseFXRates.csv", IntegrationFileUtils.getOriginalFileName("roseFXRates___201208141700.csv", true));
		Assertions.assertEquals("roseFXRates.csv", IntegrationFileUtils.getOriginalFileName("roseFXRates-__201208141700.csv", true));
		Assertions.assertEquals("roseFXRates_201208141700.csv", IntegrationFileUtils.getOriginalFileName("roseFXRates_201208141700_.csv", false));
		Assertions.assertEquals("roseFXRates.csv", IntegrationFileUtils.getOriginalFileName("roseFXRates-201208141700.csv", true));
		Assertions.assertEquals("roseFXRates.csv", IntegrationFileUtils.getOriginalFileName("roseFXRates__201208141700.csv", true));

		Date date = DateUtils.toDate("201208141700", "yyyyMMddHHmm");
		Assertions.assertEquals(0, DateUtils.compare(date, IntegrationFileUtils.getEffectiveDateFromFileName("roseFXRates__201208141700__2012-08-15_16_07_01.csv"), true));
		Assertions.assertEquals(0, DateUtils.compare(date, IntegrationFileUtils.getEffectiveDateFromFileName("roseFXRates__201208141700.csv"), true));

		date = DateUtils.toDate("12/08/2010");
		Assertions.assertEquals("open_positions__2010-12-08_00_00_00.csv", IntegrationFileUtils.appendTimeStamp("open_positions.csv", date));
		Assertions.assertEquals("open_positions__2010-12-08_00_00_00.csv", IntegrationFileUtils.appendTimeStamp("open_positions__2010-12-08_00_00_00.csv", date));

		Assertions.assertEquals(0, DateUtils.compare(date, IntegrationFileUtils.getTimeStamp("open_positions__2010-12-08_00_00_00.csv"), true));

		// use a custom date pattern
		date = DateUtils.toDate("20201207", "yyyyMMdd");
		Assertions.assertEquals(0, DateUtils.compare(date, IntegrationFileUtils.getEffectiveDateFromFileName("ASL_1607365180236528_20201207.xml", null, ".*_(\\d{8})(?:\\.xml)$"), false));
	}
}
