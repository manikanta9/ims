package com.clifton.integration.swift;

import com.prowidesoftware.swift.io.RJEReader;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class SwiftMessageCsvConverterTest {

	private File ansp;
	private File casp;
	private File cash;

	private File output;


	@BeforeEach
	public void loadMessageFile() throws IOException {
		ClassPathResource classPathResource = new ClassPathResource("com/clifton/integration/swift/ANSP_MT940.D201230053030 (NON SGD).txt");
		this.ansp = classPathResource.getFile();
		classPathResource = new ClassPathResource("com/clifton/integration/swift/CASP_MT940.D201230053030 (SGD ACCOUNTS).txt");
		this.casp = classPathResource.getFile();
		classPathResource = new ClassPathResource("com/clifton/integration/swift/Sample Cash report.txt");
		this.cash = classPathResource.getFile();
	}


	@AfterEach
	public void deleteFiles() {
		Optional.ofNullable(this.output).ifPresent(File::delete);
	}


	@Test
	public void anspStatementLines() throws IOException {
		this.output = File.createTempFile("ansp", null);
		SwiftMt940ToCsvConverter converter = new SwiftMt940ToCsvConverter();
		try (Reader swift = new FileReader(this.ansp); Writer csv = new FileWriter(this.output)) {
			int messageCount = converter.output(swift, csv);
			Assertions.assertEquals(4, messageCount);
		}
		Assertions.assertNotNull(this.output);
		try (CSVParser parser = CSVParser.parse(this.output, StandardCharsets.UTF_8, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
			List<String> headerNames = parser.getHeaderNames();
			List<String> expected = converter.getWorkingHeaders().stream()
					.map(SwiftMt940ToCsvConverter.ColumnHeader::getHeader)
					.collect(Collectors.toList());
			expected.removeAll(headerNames);
			Assertions.assertTrue(expected.isEmpty(), expected::toString);
			List<CSVRecord> recordList = parser.getRecords();
			Assertions.assertEquals(4, recordList.size());
		}
	}


	@Test
	public void anspNoStatementLines() throws IOException {
		this.output = File.createTempFile("ansp", null);
		SwiftMt940ToCsvConverter converter = new SwiftMt940ToCsvConverter();
		try (Reader swift = new FileReader(this.ansp); Writer csv = new FileWriter(this.output)) {
			converter.setIncludeStatementLines(false);
			int messageCount = converter.output(swift, csv);
			Assertions.assertEquals(4, messageCount);
		}
		Assertions.assertNotNull(this.output);
		try (CSVParser parser = CSVParser.parse(this.output, StandardCharsets.UTF_8, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
			List<String> headerNames = parser.getHeaderNames();
			List<String> expected = converter.getWorkingHeaders().stream()
					.map(SwiftMt940ToCsvConverter.ColumnHeader::getHeader)
					.collect(Collectors.toList());
			expected.removeAll(headerNames);
			Assertions.assertTrue(expected.isEmpty(), expected::toString);
			List<CSVRecord> recordList = parser.getRecords();
			Assertions.assertEquals(4, recordList.size());
		}
	}


	@Test
	public void caspNoStatementLines() throws IOException {
		this.output = File.createTempFile("casp", null);
		SwiftMt940ToCsvConverter converter = new SwiftMt940ToCsvConverter();
		try (Reader swift = new FileReader(this.casp); Writer csv = new FileWriter(this.output)) {
			converter.setIncludeStatementLines(false);
			int messageCount = converter.output(swift, csv);
			Assertions.assertEquals(2, messageCount);
		}
		Assertions.assertNotNull(this.output);
		try (CSVParser parser = CSVParser.parse(this.output, StandardCharsets.UTF_8, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
			List<String> headerNames = parser.getHeaderNames();
			List<String> expected = converter.getWorkingHeaders().stream()
					.map(SwiftMt940ToCsvConverter.ColumnHeader::getHeader)
					.collect(Collectors.toList());
			expected.removeAll(headerNames);
			Assertions.assertTrue(expected.isEmpty(), expected::toString);
			List<CSVRecord> recordList = parser.getRecords();
			Assertions.assertEquals(2, recordList.size());
		}
	}


	@Test
	public void caspStatementLines() throws IOException {
		this.output = File.createTempFile("casp", null);
		SwiftMt940ToCsvConverter converter = new SwiftMt940ToCsvConverter();
		try (Reader swift = new FileReader(this.casp); Writer csv = new FileWriter(this.output)) {
			int messageCount = converter.output(swift, csv);
			Assertions.assertEquals(2, messageCount);
		}
		Assertions.assertNotNull(this.output);
		try (CSVParser parser = CSVParser.parse(this.output, StandardCharsets.UTF_8, CSVFormat.DEFAULT.withFirstRecordAsHeader())) {
			List<String> headerNames = parser.getHeaderNames();
			List<String> expected = converter.getWorkingHeaders().stream()
					.map(SwiftMt940ToCsvConverter.ColumnHeader::getHeader)
					.collect(Collectors.toList());
			expected.removeAll(headerNames);
			Assertions.assertTrue(expected.isEmpty(), expected::toString);
			List<CSVRecord> recordList = parser.getRecords();
			Assertions.assertEquals(6, recordList.size());
		}
	}


	@Test
	public void sampleCash() throws IOException {
		int count = 0;
		try (Reader reader = new FileReader(this.cash)) {
			RJEReader rjeReader = new RJEReader(reader);
			while (rjeReader.hasNext()) {
				AbstractMT message = rjeReader.nextMT();
				Assertions.assertNotNull(message);
				count++;
			}
		}
		Assertions.assertEquals(2, count);
	}
}
