package com.clifton.integration.security.encryption;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.security.encryption.transport.SecurityRSATransportEncryptionKeyDetail;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author theodorez
 */
public interface IntegrationSecurityRSATransportEncryptionService {

	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public SecurityRSATransportEncryptionKeyDetail getIntegrationTransportEncryptionKeyDetails();


	@RequestMapping("integrationTransportEncryptionKeySetup")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void setupIntegrationTransportEncryptionKey();
}
