package com.clifton.integration.target.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationTargetApplicationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "name")
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "id", searchFieldPath = "definitionTypeList")
	private Integer definitionTypeId;

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getDefinitionTypeId() {
		return this.definitionTypeId;
	}


	public void setDefinitionTypeId(Integer definitionTypeId) {
		this.definitionTypeId = definitionTypeId;
	}
}
