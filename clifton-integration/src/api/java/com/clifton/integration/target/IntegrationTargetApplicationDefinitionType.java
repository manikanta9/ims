package com.clifton.integration.target;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;


/**
 * The IntegrationTargetApplicationDefinitionType class specifies which Target Application(s) will receive Import Events for which Import Definition types.
 */
public class IntegrationTargetApplicationDefinitionType extends ManyToManyEntity<IntegrationTargetApplication, IntegrationImportDefinitionType, Integer> {

}
