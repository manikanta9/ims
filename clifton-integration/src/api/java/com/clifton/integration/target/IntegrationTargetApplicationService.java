package com.clifton.integration.target;


import com.clifton.integration.target.search.IntegrationTargetApplicationSearchForm;

import java.util.List;


public interface IntegrationTargetApplicationService {

	//////////////////////////////////////////////////////////////////////////////
	////////   IntegrationTargetApplication Business Methods   /////////////
	//////////////////////////////////////////////////////////////////////////////


	public IntegrationTargetApplication getIntegrationTargetApplication(short id);


	public IntegrationTargetApplication getIntegrationTargetApplicationByName(String name);


	public List<IntegrationTargetApplication> getIntegrationTargetApplicationList(IntegrationTargetApplicationSearchForm searchForm);


	public IntegrationTargetApplication saveIntegrationTargetApplication(IntegrationTargetApplication targetApplication);


	public void deleteIntegrationTargetApplication(short id);

	//////////////////////////////////////////////////////////////////////////////
	////  IntegrationTargetApplicationDefinitionType Business Methods   ////
	//////////////////////////////////////////////////////////////////////////////


	public void linkIntegrationTargetApplicationDefinitionType(short targetApplicationId, int definitionTypeId);


	public void deleteIntegrationTargetApplicationDefinitionType(short targetApplicationId, int definitionTypeId);
}
