package com.clifton.integration.target;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The IntegrationTargetApplication class describes what applications receive events raised after integration processes files.
 */
@CacheByName
public class IntegrationTargetApplication extends NamedEntity<Short> {

	public static final String TARGET_APPLICATION_INTEGRATION = "INTEGRATION";

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////

	private Integer requestTimeout;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
}
