package com.clifton.integration.incoming.transformation;

import com.clifton.integration.incoming.transformation.search.IntegrationImportTransformationSearchForm;

import java.util.List;


/**
 * Defines the service methods for IntegrationImportTransformation objects.
 */
public interface IntegrationImportTransformationService {

	public IntegrationImportTransformation getIntegrationImportTransformation(int id);


	public List<IntegrationImportTransformation> getIntegrationImportTransformationList(IntegrationImportTransformationSearchForm searchForm);


	public IntegrationImportTransformation saveIntegrationImportTransformation(IntegrationImportTransformation bean);


	public void deleteIntegrationImportTransformation(int id);
}
