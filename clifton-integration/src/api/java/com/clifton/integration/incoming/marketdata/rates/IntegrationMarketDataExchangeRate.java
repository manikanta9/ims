package com.clifton.integration.incoming.marketdata.rates;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>IntegrationMarketDataExchangeRate</code> class represents an exchange rate sent
 * to us by brokers, before it has been imported into IMS.
 */
public class IntegrationMarketDataExchangeRate extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	private Date rateDate;
	private String fromCurrency;
	private String toCurrency;
	private BigDecimal rate = BigDecimal.ZERO;
	private BigDecimal inverseRate = BigDecimal.ZERO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public Date getRateDate() {
		return this.rateDate;
	}


	public void setRateDate(Date rateDate) {
		this.rateDate = rateDate;
	}


	public String getFromCurrency() {
		return this.fromCurrency;
	}


	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}


	public String getToCurrency() {
		return this.toCurrency;
	}


	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public BigDecimal getInverseRate() {
		return this.inverseRate;
	}


	public void setInverseRate(BigDecimal inverseRate) {
		this.inverseRate = inverseRate;
	}
}
