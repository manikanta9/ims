package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;


/**
 * The <code>IntegrationManagerAssetClass</code> class defines entities that map specific assetClass names (bank specific names)
 * to corresponding position types for a grouping that represents an import definition.
 *
 * @author vgomelsky
 */
public class IntegrationManagerAssetClass extends BaseEntity<Integer> {

	private IntegrationManagerAssetClassGroup group;
	private IntegrationManagerPositionType type;

	private String assetClass;
	private String managerBankCode;
	private String securityDescriptionPattern;
	private boolean securityDescriptionPatternExactMatch;

	private String mappingNote;

	@NonPersistentField
	private Boolean mapSecurityForAllManagers;


	///////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////


	public IntegrationManagerAssetClassGroup getGroup() {
		return this.group;
	}


	public void setGroup(IntegrationManagerAssetClassGroup group) {
		this.group = group;
	}


	public String getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}


	public IntegrationManagerPositionType getType() {
		return this.type;
	}


	public void setType(IntegrationManagerPositionType type) {
		this.type = type;
	}


	public String getManagerBankCode() {
		return this.managerBankCode;
	}


	public void setManagerBankCode(String managerBankCode) {
		this.managerBankCode = managerBankCode;
	}


	public String getSecurityDescriptionPattern() {
		return this.securityDescriptionPattern;
	}


	public void setSecurityDescriptionPattern(String securityDescriptionPattern) {
		this.securityDescriptionPattern = securityDescriptionPattern;
	}


	public String getMappingNote() {
		return this.mappingNote;
	}


	public void setMappingNote(String mappingNote) {
		this.mappingNote = mappingNote;
	}


	public boolean isSecurityDescriptionPatternExactMatch() {
		return this.securityDescriptionPatternExactMatch;
	}


	public void setSecurityDescriptionPatternExactMatch(boolean securityDescriptionPatternExactMatch) {
		this.securityDescriptionPatternExactMatch = securityDescriptionPatternExactMatch;
	}


	public Boolean getMapSecurityForAllManagers() {
		return this.mapSecurityForAllManagers;
	}


	public void setMapSecurityForAllManagers(Boolean mapSecurityForAllManagers) {
		this.mapSecurityForAllManagers = mapSecurityForAllManagers;
	}
}
