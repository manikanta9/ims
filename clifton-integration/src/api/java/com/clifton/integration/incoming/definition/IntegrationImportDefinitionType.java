package com.clifton.integration.incoming.definition;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>IntegrationImportDefinitionType</code> class defines the type of data received from external sources.
 * Each type usually maps to a destination table in the integration system that stores normalized source data for that type.
 * Corresponding Event can be raised to notify the Main System that new data is available for consumption.
 * <p/>
 * Examples of types: Market Data Field Value Import, Market Data Exchange Rate Import, Manager Download, Broker Import Trade Intraday, etc.
 */
public class IntegrationImportDefinitionType extends NamedEntity<Integer> {

	public static final String BROKER_IMPORT_ACCOUNT_BALANCES = "Broker Import Account Balances";
	public static final String BROKER_IMPORT_M2M_EXPENSES = "Broker Import M2M Expenses";
	public static final String BROKER_IMPORT_TRADE_INTRADAY = "Broker Import Trade Intraday";


	private String destinationTableName;
	private String eventName;
	private boolean tempTableUsed;

	/**
	 * The sub folder were transformations for the this definition type will be stored.
	 */
	private String transformationFolder;
	/**
	 * The prefix to add to the transformation file name when uploaded to the document server.
	 */
	private String transformationFileNamePrefix;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDestinationTableName() {
		return this.destinationTableName;
	}


	public void setDestinationTableName(String destinationTableName) {
		this.destinationTableName = destinationTableName;
	}


	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	public boolean isTempTableUsed() {
		return this.tempTableUsed;
	}


	public void setTempTableUsed(boolean tempTableUsed) {
		this.tempTableUsed = tempTableUsed;
	}


	public String getTransformationFolder() {
		return this.transformationFolder;
	}


	public void setTransformationFolder(String transformationFolder) {
		this.transformationFolder = transformationFolder;
	}


	public String getTransformationFileNamePrefix() {
		return this.transformationFileNamePrefix;
	}


	public void setTransformationFileNamePrefix(String transformationFileNamePrefix) {
		this.transformationFileNamePrefix = transformationFileNamePrefix;
	}
}
