package com.clifton.integration.incoming.client.event;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.event.EventNotProcessedException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import com.clifton.integration.incoming.messages.IntegrationDataEventRequestMessage;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;


public class IntegrationEventJmsMessageProcessor implements AsynchronousProcessor<AsynchronousMessage> {


	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;

	private IntegrationImportService integrationImportService;

	private ContextHandler contextHandler;
	private SecurityUserService securityUserService;
	private EventHandler eventHandler;

	private String applicationName;


	@Override
	public void process(AsynchronousMessage asynchronousMessage, AsynchronousMessageHandler handler) {
		getContextHandler().setBean(Context.USER_BEAN_NAME, getRunAsUser());
		ValidationUtils.assertNotNull(getApplicationName(), "You must define an applicationName when processing virtual topics!");
		if (asynchronousMessage instanceof IntegrationDataEventRequestMessage) {
			IntegrationDataEventRequestMessage message = (IntegrationDataEventRequestMessage) asynchronousMessage;

			AssertUtils.assertNotNull(message.getIntegrationImportRunEventId(), "Context value \"integrationImportRunId\" is null for event with name [" + message.getIntegrationEventName() + "].");

			IntegrationImportRunEvent runEvent = getIntegrationImportService().getIntegrationImportRunEvent(message.getIntegrationImportRunEventId());
			AssertUtils.assertNotNull(runEvent, "No run event was found with id: " + message.getIntegrationImportRunEventId());
			try {
				IntegrationImportRun run = runEvent.getImportRun();
				AssertUtils.assertNotNull(run.getIntegrationImportDefinition(), "Run for runEvent with id [ " + runEvent.getId() + "] was null!");
				AssertUtils.assertNotNull(run.getIntegrationImportDefinition(), "Integration Definition for run with id [" + run.getId() + " for runEvent with id [ " + runEvent.getId() + "] was null!");
				AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition(), "File Definition for run with id [" + run.getId() + " for runEvent with id [" + run.getId() + " was null!");
				AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition().getSource(), "An import source is required for events processing.");

				if (getEventHandler().raiseEvent(IntegrationImportEvent.ofEventTarget(message.getIntegrationEventName(), run))) {
					getIntegrationImportService().updateIntegrationImportRunEvent(runEvent.getId(), IntegrationImportStatusTypes.SUCCESSFUL, null);
				}
			}
			catch (Exception e) {
				String errorMessage;
				IntegrationImportStatusTypes statusType;
				if (e.getCause() instanceof EventNotProcessedException) {
					errorMessage = "Event Not Processed: " + ExceptionUtils.getDetailedMessage(e);
					statusType = IntegrationImportStatusTypes.NOT_PROCESSED;
					LogUtils.warn(IntegrationEventJmsMessageProcessor.class, errorMessage, e);
				}
				else {
					errorMessage = ExceptionUtils.getDetailedMessage(e);
					statusType = IntegrationImportStatusTypes.ERROR;
					LogUtils.error(IntegrationEventJmsMessageProcessor.class, errorMessage, e);
				}
				getIntegrationImportService().updateIntegrationImportRunEvent(runEvent.getId(), statusType, errorMessage);
			}
		}
		else {
			throw new RuntimeException("Cannot process message because no type is specified.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	private SecurityUser getRunAsUser() {
		return getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
	}


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public String getApplicationName() {
		return this.applicationName;
	}


	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
}
