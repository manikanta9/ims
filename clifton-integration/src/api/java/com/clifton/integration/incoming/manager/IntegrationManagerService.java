package com.clifton.integration.incoming.manager;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.integration.incoming.manager.search.IntegrationManagerAssetClassGroupSearchForm;
import com.clifton.integration.incoming.manager.search.IntegrationManagerBankMappingSearchForm;
import com.clifton.integration.incoming.manager.search.IntegrationManagerPositionHistorySearchForm;
import com.clifton.integration.incoming.manager.search.IntegrationManagerTransactionSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>IntegrationManagerService</code> ...
 *
 * @author Mary Anderson
 */
public interface IntegrationManagerService {

	/**
	 * Used to set the dataSource name from the app-integration project
	 *
	 * @param integrationDataSourceName
	 */
	@DoNotAddRequestMapping
	public void setIntegrationDataSourceName(String integrationDataSourceName);


	//////////////////////////////////////////////////////////
	/////       Manager Asset Class Group Methods        /////
	//////////////////////////////////////////////////////////


	public IntegrationManagerAssetClassGroup getIntegrationManagerAssetClassGroup(int id);


	public IntegrationManagerAssetClassGroup saveIntegrationManagerAssetClassGroup(IntegrationManagerAssetClassGroup bean);


	public void deleteIntegrationManagerAssetClassGroup(int id);


	public List<IntegrationManagerAssetClassGroup> getIntegrationManagerAssetClassGroupList(IntegrationManagerAssetClassGroupSearchForm searchForm);


	public IntegrationManagerAssetClassGroup getIntegrationManagerAssetClassGroupByName(String groupName);


	public List<IntegrationManagerAssetClassGroup> getIntegrationManagerAssetClassGroupListByBusinessCompanyId(int businessCompanyId);


	public IntegrationManagerAssetClassGroup getIntegrationManagerAssetClassGroupByDefinition(int definitionId);


	////////////////////////////////////////////////////////////////////////////
	////////          Manager Asset Class Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationManagerAssetClass getIntegrationManagerAssetClass(int id);


	@DoNotAddRequestMapping
	public List<String> getIntegrationUnmappedAssetClassesForGroup(int integrationRunId);


	public List<IntegrationManagerAssetClass> getIntegrationManagerAssetClassListByGroup(int groupId);


	public IntegrationManagerAssetClass saveIntegrationManagerAssetClass(IntegrationManagerAssetClass bean);


	public void deleteIntegrationManagerAssetClass(int id);


	//////////////////////////////////////////////////////////
	// Manager Asset Class Group Import Definition Methods  //
	//////////////////////////////////////////////////////////


	public List<IntegrationManagerAssetClassGroupImportDefinition> getIntegrationManagerAssetClassGroupImportDefinitionListByGroup(IntegrationManagerAssetClassGroup group);


	public void linkIntegrationManagerAssetClassGroupImportDefinition(Integer importDefinitionId, Integer importGroupId);


	public void deleteIntegrationManagerAssetClassGroupImportDefinition(Integer importDefinitionId, Integer importGroupId);


	//////////////////////////////////////////////////////////
	////       Manager Position History Methods           ////
	//////////////////////////////////////////////////////////


	public List<IntegrationManagerPositionHistory> getIntegrationManagerPositionHistoryList(IntegrationManagerPositionHistorySearchForm searchForm);


	public List<IntegrationManagerPositionHistory> getIntegrationManagerPositionHistoryList(Integer businessCompanyId, String bankCode, Date balanceDate);


	//////////////////////////////////////////////////////////
	/////         Manager Position Type  Methods         /////
	//////////////////////////////////////////////////////////


	public IntegrationManagerPositionType getIntegrationManagerPositionType(short id);


	public List<IntegrationManagerPositionType> getIntegrationManagerPositionTypeList();


	//////////////////////////////////////////////////////////
	///////          Manager Summary Methods           ///////
	//////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = IntegrationManagerPositionHistory.class)
	public List<IntegrationManagerPositionHistorySummary> getIntegrationManagerPositionHistorySummaryList(Integer custodianId, Date balanceDate);


	//////////////////////////////////////////////////////////
	///////       Account Mapping Summary Methods      ///////
	//////////////////////////////////////////////////////////
	public IntegrationManagerBankMapping getIntegrationManagerBankMapping(int id);


	public List<IntegrationManagerBankMapping> getIntegrationManagerBankMappingList(IntegrationManagerBankMappingSearchForm searchForm);


	public IntegrationManagerBankMapping saveIntegrationManagerBankMapping(IntegrationManagerBankMapping bean);


	public void deleteIntegrationManagerBankMapping(int id);


	//////////////////////////////////////////////////////////
	///////        Manager Transaction Methods         ///////
	//////////////////////////////////////////////////////////


	public IntegrationManagerTransaction getIntegrationManagerTransaction(int id);


	public List<IntegrationManagerTransaction> getIntegrationManagerTransactionList(IntegrationManagerTransactionSearchForm searchForm);
}
