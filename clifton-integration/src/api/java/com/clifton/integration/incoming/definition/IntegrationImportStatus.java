package com.clifton.integration.incoming.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The IntegrationImportStatus class represents a status for an Import Run.  The following statuses are available:
 * RUNNING, SUCCESSFUL, PROCESSED_WITH_ERRORS, ERROR, REPROCESSED, MAPPING_ERROR, TRANSFORMATION_DEPLOYMENT_SUCCESSFUL, TRANSFORMATION_DEPLOYMENT_ERROR
 */
@CacheByName
public class IntegrationImportStatus extends NamedEntity<Short> {

	public IntegrationImportStatusTypes getIntegrationImportStatusType() {
		return IntegrationImportStatusTypes.findIntegrationImportStatusType(this.getName());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isError() {
		return getIntegrationImportStatusType().isError();
	}
}
