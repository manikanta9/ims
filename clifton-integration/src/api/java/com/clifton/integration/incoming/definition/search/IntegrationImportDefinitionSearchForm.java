package com.clifton.integration.incoming.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationImportDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Boolean disabled;

	@SearchField
	private Integer order;

	@SearchField(searchField = "fileName", searchFieldPath = "fileDefinition")
	private String fileName;

	@SearchField(searchField = "onePerDay", searchFieldPath = "fileDefinition")
	private Boolean onePerDay;

	@SearchField
	private String dataSourceName;

	@SearchField(searchField = "sourceCompanyId", searchFieldPath = "fileDefinition.source")
	private Integer sourceCompanyId;

	@SearchField(searchField = "type.id")
	private Integer typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField(searchField = "eventName", searchFieldPath = "type")
	private String eventName;

	@SearchField(searchField = "fileDefinition.id")
	private Integer fileDefinitionId;

	@SearchField(searchField = "id", searchFieldPath = "assetClassGroupList")
	private Integer managerAssetClassGroupId;

	@SearchField
	private Boolean raiseExternalEvent;

	@SearchField(searchField = "name", searchFieldPath = "transformation")
	private String transformationName;

	@SearchField(searchField = "transformation.id")
	private Integer transformationId;

	@SearchField(searchField = "testDefinition")
	private Boolean testDefinition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Integer getSourceCompanyId() {
		return this.sourceCompanyId;
	}


	public void setSourceCompanyId(Integer sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String groupName) {
		this.eventName = groupName;
	}


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Integer getManagerAssetClassGroupId() {
		return this.managerAssetClassGroupId;
	}


	public void setManagerAssetClassGroupId(Integer managerAssetClassGroupId) {
		this.managerAssetClassGroupId = managerAssetClassGroupId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public Boolean getOnePerDay() {
		return this.onePerDay;
	}


	public void setOnePerDay(Boolean onePerDay) {
		this.onePerDay = onePerDay;
	}


	public Boolean getRaiseExternalEvent() {
		return this.raiseExternalEvent;
	}


	public void setRaiseExternalEvent(Boolean raiseExternalEvent) {
		this.raiseExternalEvent = raiseExternalEvent;
	}


	public String getTransformationName() {
		return this.transformationName;
	}


	public void setTransformationName(String transformationName) {
		this.transformationName = transformationName;
	}


	public Integer getTransformationId() {
		return this.transformationId;
	}


	public void setTransformationId(Integer transformationId) {
		this.transformationId = transformationId;
	}


	public Boolean getTestDefinition() {
		return this.testDefinition;
	}


	public void setTestDefinition(Boolean testDefinition) {
		this.testDefinition = testDefinition;
	}
}
