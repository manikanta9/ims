package com.clifton.integration.incoming.reconcile.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcilePositionHistorySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id", required = true)
	private Integer runId;

	@SearchField
	private String accountNumber;

	@SearchField
	private String accountName;

	@SearchField
	private String clientAccountNumber;

	@SearchField
	private String uniquePositionId;

	@SearchField
	private Date positionDate;

	@SearchField
	private Date tradeDate;

	@SearchField
	private Date settleDate;

	@SearchField
	private String securitySymbol;

	@SearchField
	private String securitySymbolType;

	@SearchField
	private String positionCurrencySymbol;

	@SearchField
	private String accountCurrencySymbol;

	@SearchField
	private BigDecimal fxRate;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private Boolean shortPosition;

	@SearchField
	private BigDecimal tradePrice;

	@SearchField
	private BigDecimal marketPrice;

	@SearchField
	private BigDecimal costBasisLocal;

	@SearchField
	private BigDecimal costBasisBase;

	@SearchField
	private BigDecimal marketValueLocal;

	@SearchField
	private BigDecimal marketValueBase;

	@SearchField
	private BigDecimal optionMarketValueLocal;

	@SearchField
	private BigDecimal optionMarketValueBase;

	@SearchField
	private BigDecimal openTradeEquityLocal;

	@SearchField
	private BigDecimal openTradeEquityBase;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public String getUniquePositionId() {
		return this.uniquePositionId;
	}


	public void setUniquePositionId(String uniquePositionId) {
		this.uniquePositionId = uniquePositionId;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettleDate() {
		return this.settleDate;
	}


	public void setSettleDate(Date settleDate) {
		this.settleDate = settleDate;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecuritySymbolType() {
		return this.securitySymbolType;
	}


	public void setSecuritySymbolType(String securitySymbolType) {
		this.securitySymbolType = securitySymbolType;
	}


	public String getPositionCurrencySymbol() {
		return this.positionCurrencySymbol;
	}


	public void setPositionCurrencySymbol(String positionCurrencySymbol) {
		this.positionCurrencySymbol = positionCurrencySymbol;
	}


	public String getAccountCurrencySymbol() {
		return this.accountCurrencySymbol;
	}


	public void setAccountCurrencySymbol(String accountCurrencySymbol) {
		this.accountCurrencySymbol = accountCurrencySymbol;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public Boolean getShortPosition() {
		return this.shortPosition;
	}


	public void setShortPosition(Boolean shortPosition) {
		this.shortPosition = shortPosition;
	}


	public BigDecimal getTradePrice() {
		return this.tradePrice;
	}


	public void setTradePrice(BigDecimal tradePrice) {
		this.tradePrice = tradePrice;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getCostBasisLocal() {
		return this.costBasisLocal;
	}


	public void setCostBasisLocal(BigDecimal costBasisLocal) {
		this.costBasisLocal = costBasisLocal;
	}


	public BigDecimal getCostBasisBase() {
		return this.costBasisBase;
	}


	public void setCostBasisBase(BigDecimal costBasisBase) {
		this.costBasisBase = costBasisBase;
	}


	public BigDecimal getMarketValueLocal() {
		return this.marketValueLocal;
	}


	public void setMarketValueLocal(BigDecimal marketValueLocal) {
		this.marketValueLocal = marketValueLocal;
	}


	public BigDecimal getMarketValueBase() {
		return this.marketValueBase;
	}


	public void setMarketValueBase(BigDecimal marketValueBase) {
		this.marketValueBase = marketValueBase;
	}


	public BigDecimal getOptionMarketValueLocal() {
		return this.optionMarketValueLocal;
	}


	public void setOptionMarketValueLocal(BigDecimal optionMarketValueLocal) {
		this.optionMarketValueLocal = optionMarketValueLocal;
	}


	public BigDecimal getOptionMarketValueBase() {
		return this.optionMarketValueBase;
	}


	public void setOptionMarketValueBase(BigDecimal optionMarketValueBase) {
		this.optionMarketValueBase = optionMarketValueBase;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}
}
