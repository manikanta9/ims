package com.clifton.integration.incoming.marketdata.field.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


public class IntegrationMarketDataFieldValueSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id")
	private Integer runId;

	@SearchField
	private String marketDataFieldName;

	@SearchField
	private String investmentSecuritySymbol;

	@SearchField
	private Date measureDate;

	@SearchField
	private String measureValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public String getMarketDataFieldName() {
		return this.marketDataFieldName;
	}


	public void setMarketDataFieldName(String marketDataFieldName) {
		this.marketDataFieldName = marketDataFieldName;
	}


	public String getInvestmentSecuritySymbol() {
		return this.investmentSecuritySymbol;
	}


	public void setInvestmentSecuritySymbol(String investmentSecuritySymbol) {
		this.investmentSecuritySymbol = investmentSecuritySymbol;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public String getMeasureValue() {
		return this.measureValue;
	}


	public void setMeasureValue(String measureValue) {
		this.measureValue = measureValue;
	}
}
