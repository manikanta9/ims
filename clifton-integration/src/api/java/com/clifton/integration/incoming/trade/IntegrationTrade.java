package com.clifton.integration.incoming.trade;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationTrade extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	private String uniqueIdentifier;

	private String holdingAccountNumber;
	private String clientAccountNumber;
	/**
	 * If this is populated, it should be used to look up the executing broker.  Otherwise the broker company name should be used.
	 */
	private String executingBrokerCode;
	private String executingBrokerCompanyName;

	private String securitySymbol;
	private String securityDescription;
	private String cusip;
	private String sedol;
	private String isin;
	private String payingSecuritySymbol;

	private boolean buy;
	private Date tradeDate;
	private Date settlementDate;

	private BigDecimal quantity;
	private BigDecimal price;
	private BigDecimal priceBase;
	private BigDecimal tradeAmount; // local trade amount - should be accounting notional
	private BigDecimal tradeAmountBase;

	private BigDecimal commissionPerUnit; // commissionAmount/quantityThatCommissionWasChargedFor
	private BigDecimal commissionAmount; // total commission amount charged for this trade
	private BigDecimal feeAmount; // total fee for the trade

	private BigDecimal openingPrice;
	private Date openingDate;

	private BigDecimal exchangeRateToBase;

	private boolean currencyTrade;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public String getUniqueIdentifier() {
		return this.uniqueIdentifier;
	}


	public void setUniqueIdentifier(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}


	public String getHoldingAccountNumber() {
		return this.holdingAccountNumber;
	}


	public void setHoldingAccountNumber(String holdingAccountNumber) {
		this.holdingAccountNumber = holdingAccountNumber;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public String getExecutingBrokerCompanyName() {
		return this.executingBrokerCompanyName;
	}


	public void setExecutingBrokerCompanyName(String executingBrokerCompanyName) {
		this.executingBrokerCompanyName = executingBrokerCompanyName;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecurityDescription() {
		return this.securityDescription;
	}


	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public String getPayingSecuritySymbol() {
		return this.payingSecuritySymbol;
	}


	public void setPayingSecuritySymbol(String payingSecuritySymbol) {
		this.payingSecuritySymbol = payingSecuritySymbol;
	}


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getOpeningPrice() {
		return this.openingPrice;
	}


	public void setOpeningPrice(BigDecimal openingPrice) {
		this.openingPrice = openingPrice;
	}


	public Date getOpeningDate() {
		return this.openingDate;
	}


	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}


	public BigDecimal getTradeAmount() {
		return this.tradeAmount;
	}


	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getExecutingBrokerCode() {
		return this.executingBrokerCode;
	}


	public void setExecutingBrokerCode(String executingBrokerCode) {
		this.executingBrokerCode = executingBrokerCode;
	}


	public BigDecimal getTradeAmountBase() {
		return this.tradeAmountBase;
	}


	public void setTradeAmountBase(BigDecimal tradeAmountBase) {
		this.tradeAmountBase = tradeAmountBase;
	}


	public BigDecimal getPriceBase() {
		return this.priceBase;
	}


	public void setPriceBase(BigDecimal priceBase) {
		this.priceBase = priceBase;
	}


	public boolean isCurrencyTrade() {
		return this.currencyTrade;
	}


	public void setCurrencyTrade(boolean currencyTrade) {
		this.currencyTrade = currencyTrade;
	}
}
