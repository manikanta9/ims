package com.clifton.integration.incoming.corporate.action;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.util.Date;


/**
 * The <code>IntegrationCorporateActionPositionRequest</code> represents a position request for corporate actions.
 *
 * @author mwacker
 */
public class IntegrationCorporateActionPositionRequest extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun importRun;

	private Long corporateActionIdentifier;
	private String eventTypeName;
	private String eventIndicatorName;
	private String eventStatusName;

	private Integer investmentSecurityIdentifier;
	private String cusip;
	private String sedol;
	private String isin;

	private String referenceIdentifier;
	private String businessIdentifierCode;

	private Date effectiveDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getImportRun() {
		return this.importRun;
	}


	public void setImportRun(IntegrationImportRun importRun) {
		this.importRun = importRun;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public String getEventTypeName() {
		return this.eventTypeName;
	}


	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}


	public String getEventIndicatorName() {
		return this.eventIndicatorName;
	}


	public void setEventIndicatorName(String eventIndicatorName) {
		this.eventIndicatorName = eventIndicatorName;
	}


	public String getEventStatusName() {
		return this.eventStatusName;
	}


	public void setEventStatusName(String eventStatusName) {
		this.eventStatusName = eventStatusName;
	}


	public Integer getInvestmentSecurityIdentifier() {
		return this.investmentSecurityIdentifier;
	}


	public void setInvestmentSecurityIdentifier(Integer investmentSecurityIdentifier) {
		this.investmentSecurityIdentifier = investmentSecurityIdentifier;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getReferenceIdentifier() {
		return this.referenceIdentifier;
	}


	public void setReferenceIdentifier(String referenceIdentifier) {
		this.referenceIdentifier = referenceIdentifier;
	}


	public String getBusinessIdentifierCode() {
		return this.businessIdentifierCode;
	}


	public void setBusinessIdentifierCode(String businessIdentifierCode) {
		this.businessIdentifierCode = businessIdentifierCode;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
}
