package com.clifton.integration.incoming.transformation.generator;


import com.clifton.integration.incoming.definition.IntegrationImportDefinition;

import java.util.Map;


/**
 * Transforms import content from the input type to the output type.
 */
public interface ImportTransformationGenerator<I, O> {

	public O generateTransformation(I input, Integer runId, IntegrationImportDefinition definition, Map<String, String> context);


	public Class<I> getInputClass();


	public Class<O> getOutputClass();
}
