package com.clifton.integration.incoming.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class IntegrationImportRunSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "integrationImportDefinition.name,integrationImportDefinition.description")
	private String searchPattern;

	@SearchField(dateFieldIncludesTime = true)
	private Date effectiveDate;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS, searchField = "effectiveDate")
	private Date effectiveDateBefore;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS, searchField = "receivedDate", searchFieldPath = "file")
	private Date receivedDateBefore;

	@SearchField(searchField = "status.id")
	private Short statusId;

	@SearchField(searchField = "status.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short statusIdNotEquals;

	@SearchField(searchFieldPath = "status", searchField = "name")
	private String statusName;

	@SearchField(searchFieldPath = "status", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String statusNameNotEquals;

	@SearchField(searchFieldPath = "status", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] statusNames;

	@SearchField(dateFieldIncludesTime = true)
	private Date startProcessDate;

	@SearchField(dateFieldIncludesTime = true)
	private Date endProcessDate;

	@SearchField(comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS, searchField = "endProcessDate")
	private Date endProcessDateAfter;

	@SearchField
	private String error;

	@SearchField(searchField = "fileName", searchFieldPath = "file.fileDefinition")
	private String processedFileName;

	@SearchField(searchField = "integrationImportDefinition.id", comparisonConditions = {ComparisonConditions.IN})
	private Integer[] definitionIds;

	@SearchField(searchField = "integrationImportDefinition.id")
	private Integer integrationImportDefinitionId;

	@SearchField(searchField = "type.id", searchFieldPath = "integrationImportDefinition")
	private Integer typeId;

	@SearchField(searchField = "type.id", searchFieldPath = "integrationImportDefinition")
	private Integer[] typeIds;

	@SearchField(searchField = "name", searchFieldPath = "integrationImportDefinition.type")
	private String typeName;

	@SearchField(searchField = "businessCompanyId", searchFieldPath = "integrationImportDefinition.assetClassGroupList")
	private Integer businessCompanyId;

	@SearchField(searchField = "eventName", searchFieldPath = "integrationImportDefinition.type")
	private String importEventName;

	@SearchField(searchField = "eventName", searchFieldPath = "integrationImportDefinition.type", comparisonConditions = ComparisonConditions.EQUALS)
	private String importEventNameEquals;

	@SearchField(searchField = "file.id")
	private Integer fileId;

	@SearchField(searchField = "name", searchFieldPath = "integrationImportDefinition")
	private String importDefinitionName;

	@SearchField(searchField = "fileDefinition.id", searchFieldPath = "integrationImportDefinition")
	private Integer fileDefinitionId;

	@SearchField(searchField = "sourceDataDeleted", searchFieldPath = "file")
	private Boolean sourceDataDeleted;

	@SearchField(searchField = "testDefinition", searchFieldPath = "integrationImportDefinition")
	private Boolean testDefinition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Integer[] getDefinitionIds() {
		return this.definitionIds;
	}


	public void setDefinitionIds(Integer[] definitionIds) {
		this.definitionIds = definitionIds;
	}


	public Short getStatusId() {
		return this.statusId;
	}


	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}


	public Short getStatusIdNotEquals() {
		return this.statusIdNotEquals;
	}


	public void setStatusIdNotEquals(Short statusIdNotEquals) {
		this.statusIdNotEquals = statusIdNotEquals;
	}


	public String getStatusName() {
		return this.statusName;
	}


	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}


	public String getStatusNameNotEquals() {
		return this.statusNameNotEquals;
	}


	public void setStatusNameNotEquals(String statusNameNotEquals) {
		this.statusNameNotEquals = statusNameNotEquals;
	}


	public Integer getIntegrationImportDefinitionId() {
		return this.integrationImportDefinitionId;
	}


	public void setIntegrationImportDefinitionId(Integer integrationImportDefinitionId) {
		this.integrationImportDefinitionId = integrationImportDefinitionId;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public Integer[] getTypeIds() {
		return this.typeIds;
	}


	public void setTypeIds(Integer[] typeIds) {
		this.typeIds = typeIds;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Date getStartProcessDate() {
		return this.startProcessDate;
	}


	public void setStartProcessDate(Date startProcessDate) {
		this.startProcessDate = startProcessDate;
	}


	public Date getEndProcessDate() {
		return this.endProcessDate;
	}


	public void setEndProcessDate(Date endProcessDate) {
		this.endProcessDate = endProcessDate;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public String getProcessedFileName() {
		return this.processedFileName;
	}


	public void setProcessedFileName(String processedFileName) {
		this.processedFileName = processedFileName;
	}


	public String getImportEventName() {
		return this.importEventName;
	}


	public void setImportEventName(String importEventName) {
		this.importEventName = importEventName;
	}


	public String getImportEventNameEquals() {
		return this.importEventNameEquals;
	}


	public void setImportEventNameEquals(String importEventNameEquals) {
		this.importEventNameEquals = importEventNameEquals;
	}


	public Integer getFileId() {
		return this.fileId;
	}


	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}


	public String getImportDefinitionName() {
		return this.importDefinitionName;
	}


	public void setImportDefinitionName(String importDefinitionName) {
		this.importDefinitionName = importDefinitionName;
	}


	public Date getEffectiveDateBefore() {
		return this.effectiveDateBefore;
	}


	public void setEffectiveDateBefore(Date effectiveDateBefore) {
		this.effectiveDateBefore = effectiveDateBefore;
	}


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}


	public Date getReceivedDateBefore() {
		return this.receivedDateBefore;
	}


	public void setReceivedDateBefore(Date receivedDateBefore) {
		this.receivedDateBefore = receivedDateBefore;
	}


	public Boolean getSourceDataDeleted() {
		return this.sourceDataDeleted;
	}


	public void setSourceDataDeleted(Boolean sourceDataDeleted) {
		this.sourceDataDeleted = sourceDataDeleted;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getTestDefinition() {
		return this.testDefinition;
	}


	public void setTestDefinition(Boolean testDefinition) {
		this.testDefinition = testDefinition;
	}


	public Date getEndProcessDateAfter() {
		return this.endProcessDateAfter;
	}


	public void setEndProcessDateAfter(Date endProcessDateAfter) {
		this.endProcessDateAfter = endProcessDateAfter;
	}


	public String[] getStatusNames() {
		return this.statusNames;
	}


	public void setStatusNames(String[] statusNames) {
		this.statusNames = statusNames;
	}
}
