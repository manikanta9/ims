package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;


/**
 * The <code>IntegrationManagerPositionHistory</code> class represents most detailed manager position entry
 * that we received from the bank. It may contain systemGenerated rows that were added after calculation of
 * a missing position: get cash and total then need to calculate securities value.
 *
 * @author vgomelsky
 */
public class IntegrationManagerPositionHistory extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	////////////////////////////////////////////////////////////////////////////////////////////
	////// the following fields are populated by Pentaho transformations from raw data   ///////

	/**
	 * The value is used to
	 */
	private BigDecimal value;

	private IntegrationManagerSecurity managerSecurity;
	private IntegrationManagerBank managerBank;

	////////////////////////////////////////////////////////////////////////////////////////////////
	////// the following fields are generated based on assetClass value after initial load   ///////

	private IntegrationManagerPositionType type;
	private BigDecimal cashValue;
	private BigDecimal securityValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public IntegrationManagerPositionType getType() {
		return this.type;
	}


	public void setType(IntegrationManagerPositionType type) {
		this.type = type;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public BigDecimal getSecurityValue() {
		return this.securityValue;
	}


	public void setSecurityValue(BigDecimal securityValue) {
		this.securityValue = securityValue;
	}


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public IntegrationManagerSecurity getManagerSecurity() {
		return this.managerSecurity;
	}


	public void setManagerSecurity(IntegrationManagerSecurity managerSecurity) {
		this.managerSecurity = managerSecurity;
	}


	public IntegrationManagerBank getManagerBank() {
		return this.managerBank;
	}


	public void setManagerBank(IntegrationManagerBank managerBank) {
		this.managerBank = managerBank;
	}
}
