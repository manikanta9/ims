package com.clifton.integration.incoming.server;

import com.clifton.core.messaging.MessageStatus;

import java.util.List;


//TODO - moved to 'HealthCheckAware' implementation.
public interface IntegrationServerStatusProcessorService {

	public MessageStatus getIntegrationStatus();


	public MessageStatus getIntegrationFtpStatus();


	public List<MessageStatus> getIntegrationTransportEncryptionStatus(String value);


	public MessageStatus getIntegrationTransportEncryptionPublicKey();


	public MessageStatus getIntegrationTransformationStatus();
}
