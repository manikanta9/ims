package com.clifton.integration.incoming.business;

import com.clifton.integration.incoming.business.search.IntegrationBusinessCompanySearchForm;

import java.util.List;


/**
 * @author mwacker
 */
public interface IntegrationBusinessCompanyService {

	public IntegrationBusinessCompany getIntegrationBusinessCompany(int id);


	public IntegrationBusinessCompany getIntegrationBusinessCompanyByBloombergCompanyId(int bloombergCompanyId);


	public List<IntegrationBusinessCompany> getIntegrationBusinessCompanyList(IntegrationBusinessCompanySearchForm searchForm);
}
