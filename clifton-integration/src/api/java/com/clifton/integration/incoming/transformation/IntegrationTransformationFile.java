package com.clifton.integration.incoming.transformation;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.comparison.Ordered;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;


/**
 * @author mwacker
 */
public class IntegrationTransformationFile extends BaseEntity<Integer> implements Ordered {

	private IntegrationTransformation transformation;
	private IntegrationTransformationFileTypes fileType;
	/**
	 * The path and filename for the transformation file relative to the transformation folder root.
	 */
	private String fileName;
	private int order;
	private boolean appendFile;
	private boolean runnable = true;
	/**
	 * Indicates that the transformation file needs to be updated.
	 */
	private boolean updateRequired;
	/**
	 * Non-persisted field that indicates if the transformation file name field has the full path or just the file name.
	 * Typically set to true during testing.
	 */
	@NonPersistentField
	private boolean transformationFileNameFullPath = false;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFileNameWithoutExtension() {
		return FileUtils.getFileNameWithoutExtension(getFileName());
	}


	public String getFileNamePrefix() {
		if ((getTransformation() != null) && (getTransformation().getDefinitionType() != null) && !StringUtils.isEmpty(getTransformation().getDefinitionType().getTransformationFileNamePrefix())) {
			return getTransformation().getDefinitionType().getTransformationFileNamePrefix();
		}
		return "";
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public boolean isAppendFile() {
		return this.appendFile;
	}


	public void setAppendFile(boolean appendFile) {
		this.appendFile = appendFile;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public boolean isTransformationFileNameFullPath() {
		return this.transformationFileNameFullPath;
	}


	public void setTransformationFileNameFullPath(boolean transformationFileNameFullPath) {
		this.transformationFileNameFullPath = transformationFileNameFullPath;
	}


	public IntegrationTransformation getTransformation() {
		return this.transformation;
	}


	public void setTransformation(IntegrationTransformation transformation) {
		this.transformation = transformation;
	}


	public IntegrationTransformationFileTypes getFileType() {
		return this.fileType;
	}


	public void setFileType(IntegrationTransformationFileTypes fileType) {
		this.fileType = fileType;
	}


	public boolean isRunnable() {
		return this.runnable;
	}


	public void setRunnable(boolean runnable) {
		this.runnable = runnable;
	}


	public boolean isUpdateRequired() {
		return this.updateRequired;
	}


	public void setUpdateRequired(boolean updateRequired) {
		this.updateRequired = updateRequired;
	}
}
