package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.BaseSimpleEntity;


public class IntegrationManagerBank extends BaseSimpleEntity<Integer> {

	private String bankCode;
	private String originalBankCode;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBankCode() {
		return this.bankCode;
	}


	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}


	public String getOriginalBankCode() {
		return this.originalBankCode;
	}


	public void setOriginalBankCode(String originalBankCode) {
		this.originalBankCode = originalBankCode;
	}
}
