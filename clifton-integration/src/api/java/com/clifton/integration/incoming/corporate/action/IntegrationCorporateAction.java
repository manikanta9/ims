package com.clifton.integration.incoming.corporate.action;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>IntegrationCorporateAction</code> Entity created by incoming integration data for corporate actions. IntegrationCorporateAction entities
 * are translated into InvestmentSecurityEvent or InvestmentSecurityEventPayout in the CorporateActionEventListener based on the type of corporate action.
 */
public class IntegrationCorporateAction extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun importRun;

	/**
	 * {@link com.clifton.investment.instrument.event.InvestmentSecurityEvent}
	 */
	private Long corporateActionIdentifier;
	private String eventTypeName;
	private String eventStatusName;
	private Integer investmentSecurityIdentifier;
	private Date declareDate;
	private Date exDate;
	private Date recordDate;
	private Date eventDate;
	private Date additionalEventDate;
	private BigDecimal additionalEventValue;
	private String eventDescription;
	private boolean voluntary;
	private boolean taxable;

	/**
	 * {@link com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout}
	 */
	private Short electionNumber;
	private Short payoutNumber;
	private String payoutSecurityIdentifier;
	private Short payoutSecurityTypeIdentifier;
	private BigDecimal additionalPayoutValue;
	private BigDecimal additionalPayoutValue2;
	private BigDecimal prorationRate;
	private Date additionalPayoutDate;
	private String payoutDescription;
	private boolean deleted;
	private boolean defaultElection;
	private boolean dtcOnly;
	private Short payoutTypeIdentifier;
	private String fractionalSharesMethodName;


	/**
	 * Shared by {@link com.clifton.investment.instrument.event.InvestmentSecurityEvent} and {@link com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout}
	 */
	private BigDecimal beforeEventValue;
	private BigDecimal afterEventValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getImportRun() {
		return this.importRun;
	}


	public void setImportRun(IntegrationImportRun importRun) {
		this.importRun = importRun;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public String getEventTypeName() {
		return this.eventTypeName;
	}


	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}


	public String getEventStatusName() {
		return this.eventStatusName;
	}


	public void setEventStatusName(String eventStatusName) {
		this.eventStatusName = eventStatusName;
	}


	public Integer getInvestmentSecurityIdentifier() {
		return this.investmentSecurityIdentifier;
	}


	public void setInvestmentSecurityIdentifier(Integer investmentSecurityIdentifier) {
		this.investmentSecurityIdentifier = investmentSecurityIdentifier;
	}


	public Date getDeclareDate() {
		return this.declareDate;
	}


	public void setDeclareDate(Date declareDate) {
		this.declareDate = declareDate;
	}


	public Date getExDate() {
		return this.exDate;
	}


	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}


	public Date getRecordDate() {
		return this.recordDate;
	}


	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Date getAdditionalEventDate() {
		return this.additionalEventDate;
	}


	public void setAdditionalEventDate(Date additionalEventDate) {
		this.additionalEventDate = additionalEventDate;
	}


	public BigDecimal getAdditionalEventValue() {
		return this.additionalEventValue;
	}


	public void setAdditionalEventValue(BigDecimal additionalEventValue) {
		this.additionalEventValue = additionalEventValue;
	}


	public String getEventDescription() {
		return this.eventDescription;
	}


	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}


	public boolean isVoluntary() {
		return this.voluntary;
	}


	public void setVoluntary(boolean voluntary) {
		this.voluntary = voluntary;
	}


	public boolean isTaxable() {
		return this.taxable;
	}


	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}


	public Short getElectionNumber() {
		return this.electionNumber;
	}


	public void setElectionNumber(Short electionNumber) {
		this.electionNumber = electionNumber;
	}


	public Short getPayoutNumber() {
		return this.payoutNumber;
	}


	public void setPayoutNumber(Short payoutNumber) {
		this.payoutNumber = payoutNumber;
	}


	public String getPayoutSecurityIdentifier() {
		return this.payoutSecurityIdentifier;
	}


	public void setPayoutSecurityIdentifier(String payoutSecurityIdentifier) {
		this.payoutSecurityIdentifier = payoutSecurityIdentifier;
	}


	public Short getPayoutSecurityTypeIdentifier() {
		return this.payoutSecurityTypeIdentifier;
	}


	public void setPayoutSecurityTypeIdentifier(Short payoutSecurityTypeIdentifier) {
		this.payoutSecurityTypeIdentifier = payoutSecurityTypeIdentifier;
	}


	public BigDecimal getAdditionalPayoutValue() {
		return this.additionalPayoutValue;
	}


	public void setAdditionalPayoutValue(BigDecimal additionalPayoutValue) {
		this.additionalPayoutValue = additionalPayoutValue;
	}


	public BigDecimal getAdditionalPayoutValue2() {
		return this.additionalPayoutValue2;
	}


	public void setAdditionalPayoutValue2(BigDecimal additionalPayoutValue2) {
		this.additionalPayoutValue2 = additionalPayoutValue2;
	}


	public BigDecimal getProrationRate() {
		return this.prorationRate;
	}


	public void setProrationRate(BigDecimal prorationRate) {
		this.prorationRate = prorationRate;
	}


	public Date getAdditionalPayoutDate() {
		return this.additionalPayoutDate;
	}


	public void setAdditionalPayoutDate(Date additionalPayoutDate) {
		this.additionalPayoutDate = additionalPayoutDate;
	}


	public String getPayoutDescription() {
		return this.payoutDescription;
	}


	public void setPayoutDescription(String payoutDescription) {
		this.payoutDescription = payoutDescription;
	}


	public boolean isDeleted() {
		return this.deleted;
	}


	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	public boolean isDefaultElection() {
		return this.defaultElection;
	}


	public void setDefaultElection(boolean defaultElection) {
		this.defaultElection = defaultElection;
	}


	public boolean isDtcOnly() {
		return this.dtcOnly;
	}


	public void setDtcOnly(boolean dtcOnly) {
		this.dtcOnly = dtcOnly;
	}


	public Short getPayoutTypeIdentifier() {
		return this.payoutTypeIdentifier;
	}


	public void setPayoutTypeIdentifier(Short payoutTypeIdentifier) {
		this.payoutTypeIdentifier = payoutTypeIdentifier;
	}


	public String getFractionalSharesMethodName() {
		return this.fractionalSharesMethodName;
	}


	public void setFractionalSharesMethodName(String fractionalSharesMethodName) {
		this.fractionalSharesMethodName = fractionalSharesMethodName;
	}


	public BigDecimal getBeforeEventValue() {
		return this.beforeEventValue;
	}


	public void setBeforeEventValue(BigDecimal beforeEventValue) {
		this.beforeEventValue = beforeEventValue;
	}


	public BigDecimal getAfterEventValue() {
		return this.afterEventValue;
	}


	public void setAfterEventValue(BigDecimal afterEventValue) {
		this.afterEventValue = afterEventValue;
	}
}
