package com.clifton.integration.incoming.definition;


import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.integration.file.IntegrationFile;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


public interface IntegrationImportProcessorService {

	@RequestMapping("integrationImportRunRerun")
	@SecureMethod(dtoClass = IntegrationImportRun.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void rerunIntegrationRunImport(int runId, boolean limitToImportRun);


	@RequestMapping("integrationImportRunEventRerun")
	@SecureMethod(dtoClass = IntegrationImportRunEvent.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void rerunIntegrationRunEventImport(int runEventId, boolean limitToImportRun);


	@RequestMapping("integrationFileRerun")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void rerunIntegrationFile(int integrationFileId, String targetApplicationName);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void uploadIntegrationFile(FileUploadWrapper fileWrapper);


	@RequestMapping("integrationFileRevisionUpload")
	@SecureMethod(dtoClass = IntegrationFile.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void uploadFileRevision(FileUploadWrapper file, Integer fileDefinitionId, Date effectiveDate, String holdingAccountNumber, String password);


	@RequestMapping("integrationInvestmentInstructionRevisionUpload")
	@SecureMethod(dtoClass = IntegrationFile.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void uploadInstructionFileRevision(FileUploadWrapper file, Integer instructionId, String recipientCompanyName, String custodianAccountName, Date effectiveDate);
}
