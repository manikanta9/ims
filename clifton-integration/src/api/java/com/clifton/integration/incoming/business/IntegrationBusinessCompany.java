package com.clifton.integration.incoming.business;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.util.Date;


/**
 * @author mwacker
 */
public class IntegrationBusinessCompany extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	private int bloombergCompanyIdentifier;
	private Integer parentBloombergCompanyIdentifier;
	private Integer ultimateParentBloombergCompanyIdentifier;
	private Integer obligorBloombergCompanyIdentifier;

	private String longCompanyName;
	private String companyLegalName;
	private String alternateCompanyName;

	private String companyCorporateTicker;
	private String ultimateParentTickerExchange;
	private String ultimateParentTickerExchangeCorporateTicker;

	private String industrySectorName;
	private String industrySectorGroupName;
	private String industrySectorSubGroupName;

	private Integer industrySectorNumber;
	private Integer industrySectorGroupNumber;
	private Integer industrySectorSubGroupNumber;


	private String countryOfDomicile;
	private String countryOfIncorporation;
	private String countryOfRisk;
	private String stateOfDomicile;
	private String stateOfIncorporation;


	private boolean ultimateParent;
	private boolean acquiredByParent;
	private String companyToParentRelationship;

	private String legalEntityIdentifier;
	private String legalEntityStatus;
	private Date legalEntityAssignedDate;
	private Date legalEntityDisabledDate;
	private String legalEntityRegistrationAddress;


	private String companyAddress;
	private String companyFaxNumber;
	private String companyTelephoneNumber;
	private String companyWebAddress;
	private boolean privateCompany;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public int getBloombergCompanyIdentifier() {
		return this.bloombergCompanyIdentifier;
	}


	public void setBloombergCompanyIdentifier(int bloombergCompanyIdentifier) {
		this.bloombergCompanyIdentifier = bloombergCompanyIdentifier;
	}


	public Integer getParentBloombergCompanyIdentifier() {
		return this.parentBloombergCompanyIdentifier;
	}


	public void setParentBloombergCompanyIdentifier(Integer parentBloombergCompanyIdentifier) {
		this.parentBloombergCompanyIdentifier = parentBloombergCompanyIdentifier;
	}


	public Integer getUltimateParentBloombergCompanyIdentifier() {
		return this.ultimateParentBloombergCompanyIdentifier;
	}


	public void setUltimateParentBloombergCompanyIdentifier(Integer ultimateParentBloombergCompanyIdentifier) {
		this.ultimateParentBloombergCompanyIdentifier = ultimateParentBloombergCompanyIdentifier;
	}


	public Integer getObligorBloombergCompanyIdentifier() {
		return this.obligorBloombergCompanyIdentifier;
	}


	public void setObligorBloombergCompanyIdentifier(Integer obligorBloombergCompanyIdentifier) {
		this.obligorBloombergCompanyIdentifier = obligorBloombergCompanyIdentifier;
	}


	public String getLongCompanyName() {
		return this.longCompanyName;
	}


	public void setLongCompanyName(String longCompanyName) {
		this.longCompanyName = longCompanyName;
	}


	public String getCompanyLegalName() {
		return this.companyLegalName;
	}


	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}


	public String getAlternateCompanyName() {
		return this.alternateCompanyName;
	}


	public void setAlternateCompanyName(String alternateCompanyName) {
		this.alternateCompanyName = alternateCompanyName;
	}


	public String getCompanyCorporateTicker() {
		return this.companyCorporateTicker;
	}


	public void setCompanyCorporateTicker(String companyCorporateTicker) {
		this.companyCorporateTicker = companyCorporateTicker;
	}


	public String getUltimateParentTickerExchange() {
		return this.ultimateParentTickerExchange;
	}


	public void setUltimateParentTickerExchange(String ultimateParentTickerExchange) {
		this.ultimateParentTickerExchange = ultimateParentTickerExchange;
	}


	public String getUltimateParentTickerExchangeCorporateTicker() {
		return this.ultimateParentTickerExchangeCorporateTicker;
	}


	public void setUltimateParentTickerExchangeCorporateTicker(String ultimateParentTickerExchangeCorporateTicker) {
		this.ultimateParentTickerExchangeCorporateTicker = ultimateParentTickerExchangeCorporateTicker;
	}


	public String getIndustrySectorName() {
		return this.industrySectorName;
	}


	public void setIndustrySectorName(String industrySectorName) {
		this.industrySectorName = industrySectorName;
	}


	public String getIndustrySectorGroupName() {
		return this.industrySectorGroupName;
	}


	public void setIndustrySectorGroupName(String industrySectorGroupName) {
		this.industrySectorGroupName = industrySectorGroupName;
	}


	public String getIndustrySectorSubGroupName() {
		return this.industrySectorSubGroupName;
	}


	public void setIndustrySectorSubGroupName(String industrySectorSubGroupName) {
		this.industrySectorSubGroupName = industrySectorSubGroupName;
	}


	public Integer getIndustrySectorNumber() {
		return this.industrySectorNumber;
	}


	public void setIndustrySectorNumber(Integer industrySectorNumber) {
		this.industrySectorNumber = industrySectorNumber;
	}


	public Integer getIndustrySectorGroupNumber() {
		return this.industrySectorGroupNumber;
	}


	public void setIndustrySectorGroupNumber(Integer industrySectorGroupNumber) {
		this.industrySectorGroupNumber = industrySectorGroupNumber;
	}


	public Integer getIndustrySectorSubGroupNumber() {
		return this.industrySectorSubGroupNumber;
	}


	public void setIndustrySectorSubGroupNumber(Integer industrySectorSubGroupNumber) {
		this.industrySectorSubGroupNumber = industrySectorSubGroupNumber;
	}


	public String getCountryOfDomicile() {
		return this.countryOfDomicile;
	}


	public void setCountryOfDomicile(String countryOfDomicile) {
		this.countryOfDomicile = countryOfDomicile;
	}


	public String getCountryOfIncorporation() {
		return this.countryOfIncorporation;
	}


	public void setCountryOfIncorporation(String countryOfIncorporation) {
		this.countryOfIncorporation = countryOfIncorporation;
	}


	public String getCountryOfRisk() {
		return this.countryOfRisk;
	}


	public void setCountryOfRisk(String countryOfRisk) {
		this.countryOfRisk = countryOfRisk;
	}


	public String getStateOfDomicile() {
		return this.stateOfDomicile;
	}


	public void setStateOfDomicile(String stateOfDomicile) {
		this.stateOfDomicile = stateOfDomicile;
	}


	public String getStateOfIncorporation() {
		return this.stateOfIncorporation;
	}


	public void setStateOfIncorporation(String stateOfIncorporation) {
		this.stateOfIncorporation = stateOfIncorporation;
	}


	public boolean isUltimateParent() {
		return this.ultimateParent;
	}


	public void setUltimateParent(boolean ultimateParent) {
		this.ultimateParent = ultimateParent;
	}


	public boolean isAcquiredByParent() {
		return this.acquiredByParent;
	}


	public void setAcquiredByParent(boolean acquiredByParent) {
		this.acquiredByParent = acquiredByParent;
	}


	public String getCompanyToParentRelationship() {
		return this.companyToParentRelationship;
	}


	public void setCompanyToParentRelationship(String companyToParentRelationship) {
		this.companyToParentRelationship = companyToParentRelationship;
	}


	public String getLegalEntityIdentifier() {
		return this.legalEntityIdentifier;
	}


	public void setLegalEntityIdentifier(String legalEntityIdentifier) {
		this.legalEntityIdentifier = legalEntityIdentifier;
	}


	public String getLegalEntityStatus() {
		return this.legalEntityStatus;
	}


	public void setLegalEntityStatus(String legalEntityStatus) {
		this.legalEntityStatus = legalEntityStatus;
	}


	public Date getLegalEntityAssignedDate() {
		return this.legalEntityAssignedDate;
	}


	public void setLegalEntityAssignedDate(Date legalEntityAssignedDate) {
		this.legalEntityAssignedDate = legalEntityAssignedDate;
	}


	public Date getLegalEntityDisabledDate() {
		return this.legalEntityDisabledDate;
	}


	public void setLegalEntityDisabledDate(Date legalEntityDisabledDate) {
		this.legalEntityDisabledDate = legalEntityDisabledDate;
	}


	public String getLegalEntityRegistrationAddress() {
		return this.legalEntityRegistrationAddress;
	}


	public void setLegalEntityRegistrationAddress(String legalEntityRegistrationAddress) {
		this.legalEntityRegistrationAddress = legalEntityRegistrationAddress;
	}


	public String getCompanyAddress() {
		return this.companyAddress;
	}


	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}


	public String getCompanyFaxNumber() {
		return this.companyFaxNumber;
	}


	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}


	public String getCompanyTelephoneNumber() {
		return this.companyTelephoneNumber;
	}


	public void setCompanyTelephoneNumber(String companyTelephoneNumber) {
		this.companyTelephoneNumber = companyTelephoneNumber;
	}


	public String getCompanyWebAddress() {
		return this.companyWebAddress;
	}


	public void setCompanyWebAddress(String companyWebAddress) {
		this.companyWebAddress = companyWebAddress;
	}


	public boolean isPrivateCompany() {
		return this.privateCompany;
	}


	public void setPrivateCompany(boolean privateCompany) {
		this.privateCompany = privateCompany;
	}
}
