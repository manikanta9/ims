package com.clifton.integration.incoming.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.comparison.Ordered;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;

import java.util.List;


/**
 * The IntegrationImportDefinition class describes what transformations and what file we use to import external data.
 * If the definition name is "UBSAccountBalancesImport," then the transformation names will be "UBSAccountBalancesImport" and "UBSAccountBalancesImportToFinal."
 */
public class IntegrationImportDefinition extends NamedEntity<Integer> implements Ordered {

	/**
	 * The transformation that will be used to for the ETL processing.
	 */
	private IntegrationTransformation transformation;

	/**
	 * The transformation the will convert the file to a format that is readable in the ETL transformation.
	 * <p>
	 * For example, converting from Bloomberg file format to delimited text file.
	 */
	private IntegrationTransformation fileTransformation;

	/**
	 * The data source used to determine the security symbol and/or market data location.
	 */
	private String dataSourceName;

	private IntegrationImportDefinitionType type;

	/**
	 * Indicates if the import is disabled.  This will cause the run to archive the file without processing.
	 */
	private boolean disabled;

	/**
	 * The file definition for this transformation.
	 */
	private IntegrationFileDefinition fileDefinition;

	/**
	 * This definition raises an event to notify other applications that data is ready.
	 */
	private boolean raiseExternalEvent;

	/**
	 * Defines the order that the transformations will be executed in.
	 */
	private int order;
	/**
	 * The transformation file used to get the effective date for the run.
	 */
	private IntegrationTransformationFile effectiveDateTransformationFile;

	private List<IntegrationImportDefinitionColumnMapping> columnMappings;

	/**
	 * Denotes that the definition is used for testing purposes only
	 */
	private boolean testDefinition;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public IntegrationImportDefinitionType getType() {
		return this.type;
	}


	public void setType(IntegrationImportDefinitionType type) {
		this.type = type;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public IntegrationFileDefinition getFileDefinition() {
		return this.fileDefinition;
	}


	public void setFileDefinition(IntegrationFileDefinition fileDefinition) {
		this.fileDefinition = fileDefinition;
	}


	public boolean isRaiseExternalEvent() {
		return this.raiseExternalEvent;
	}


	public void setRaiseExternalEvent(boolean raiseExternalEvent) {
		this.raiseExternalEvent = raiseExternalEvent;
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public List<IntegrationImportDefinitionColumnMapping> getColumnMappings() {
		return this.columnMappings;
	}


	public void setColumnMappings(List<IntegrationImportDefinitionColumnMapping> columnMappings) {
		this.columnMappings = columnMappings;
	}


	public IntegrationTransformation getTransformation() {
		return this.transformation;
	}


	public void setTransformation(IntegrationTransformation transformation) {
		this.transformation = transformation;
	}


	public IntegrationTransformationFile getEffectiveDateTransformationFile() {
		return this.effectiveDateTransformationFile;
	}


	public void setEffectiveDateTransformationFile(IntegrationTransformationFile effectiveDateTransformationFile) {
		this.effectiveDateTransformationFile = effectiveDateTransformationFile;
	}


	public IntegrationTransformation getFileTransformation() {
		return this.fileTransformation;
	}


	public void setFileTransformation(IntegrationTransformation fileTransformation) {
		this.fileTransformation = fileTransformation;
	}


	public boolean isTestDefinition() {
		return this.testDefinition;
	}


	public void setTestDefinition(boolean testDefinition) {
		this.testDefinition = testDefinition;
	}
}
