package com.clifton.integration.incoming.marketdata.rates;


import com.clifton.integration.incoming.marketdata.rates.search.IntegrationMarketDataExchangeRateSearchForm;

import java.util.List;


/**
 * The <code>IntegrationMarketDataExchangeRateService</code> provides methods for working with
 * IntegrationMarketDataExchangeRates
 *
 * @author rbrooks
 */
public interface IntegrationMarketDataExchangeRateService {

	public List<IntegrationMarketDataExchangeRate> getIntegrationMarketDataExchangeRateList(IntegrationMarketDataExchangeRateSearchForm searchForm);
}
