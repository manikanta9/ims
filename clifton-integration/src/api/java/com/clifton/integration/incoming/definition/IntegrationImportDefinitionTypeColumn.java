package com.clifton.integration.incoming.definition;


import com.clifton.core.beans.BaseEntity;


public class IntegrationImportDefinitionTypeColumn extends BaseEntity<Integer> {

	private String name;
	private TransformationDataTypes dataType;
	private IntegrationImportDefinitionType importType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public TransformationDataTypes getDataType() {
		return this.dataType;
	}


	public void setDataType(TransformationDataTypes dataType) {
		this.dataType = dataType;
	}


	public IntegrationImportDefinitionType getImportType() {
		return this.importType;
	}


	public void setImportType(IntegrationImportDefinitionType importType) {
		this.importType = importType;
	}
}
