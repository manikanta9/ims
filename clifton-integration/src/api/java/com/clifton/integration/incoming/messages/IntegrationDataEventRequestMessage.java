package com.clifton.integration.incoming.messages;


import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.asynchronous.AbstractAsynchronousMessage;


@MessageType(MessageTypes.XML)
public class IntegrationDataEventRequestMessage extends AbstractAsynchronousMessage {

	private long integrationImportRunEventId;

	private String integrationEventName;
	private String importDefinitionTypeName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public long getIntegrationImportRunEventId() {
		return this.integrationImportRunEventId;
	}


	public void setIntegrationImportRunEventId(long integrationImportRunEventId) {
		this.integrationImportRunEventId = integrationImportRunEventId;
	}


	public String getIntegrationEventName() {
		return this.integrationEventName;
	}


	public void setIntegrationEventName(String integrationEventName) {
		this.integrationEventName = integrationEventName;
	}


	public String getImportDefinitionTypeName() {
		return this.importDefinitionTypeName;
	}


	public void setImportDefinitionTypeName(String importDefinitionTypeName) {
		this.importDefinitionTypeName = importDefinitionTypeName;
	}
}
