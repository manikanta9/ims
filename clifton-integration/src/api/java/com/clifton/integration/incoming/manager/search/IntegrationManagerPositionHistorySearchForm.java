package com.clifton.integration.incoming.manager.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>IntegrationManagerPositionHistorySearchForm</code> class defines search configuration for IntegrationManagerPositionHistory objects.
 *
 * @author vgomelsky
 */
public class IntegrationManagerPositionHistorySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id", required = true)
	private Integer runId;

	@SearchField(searchFieldPath = "managerBank", searchField = "bankCode")
	private String bankCode;
	@SearchField(searchFieldPath = "managerSecurity", searchField = "assetClass")
	private String assetClass;
	@SearchField(searchFieldPath = "managerSecurity", searchField = "sourceAssetClass")
	private String sourceAssetClass;
	@SearchField(searchFieldPath = "managerSecurity", searchField = "securityIdentifier")
	private String securityIdentifier;
	@SearchField(searchFieldPath = "managerSecurity", searchField = "securityIdentifierType")
	private String securityIdentifierType;
	@SearchField(searchFieldPath = "managerSecurity", searchField = "securityDescription")
	private String securityDescription;

	@SearchField(searchField = "type.id")
	private Short typeId;
	@SearchField(searchField = "type.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean unmappedOnly;

	@SearchField
	private BigDecimal value;
	@SearchField
	private BigDecimal cashValue;
	@SearchField
	private BigDecimal securityValue;
	@SearchField(searchField = "effectiveDate", searchFieldPath = "run")
	private Date positionDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public String getBankCode() {
		return this.bankCode;
	}


	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}


	public String getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}


	public String getSourceAssetClass() {
		return this.sourceAssetClass;
	}


	public void setSourceAssetClass(String sourceAssetClass) {
		this.sourceAssetClass = sourceAssetClass;
	}


	public String getSecurityDescription() {
		return this.securityDescription;
	}


	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}


	public BigDecimal getValue() {
		return this.value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public BigDecimal getSecurityValue() {
		return this.securityValue;
	}


	public void setSecurityValue(BigDecimal securityValue) {
		this.securityValue = securityValue;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Boolean getUnmappedOnly() {
		return this.unmappedOnly;
	}


	public void setUnmappedOnly(Boolean unmappedOnly) {
		this.unmappedOnly = unmappedOnly;
	}


	public String getSecurityIdentifier() {
		return this.securityIdentifier;
	}


	public void setSecurityIdentifier(String securityIdentifier) {
		this.securityIdentifier = securityIdentifier;
	}


	public String getSecurityIdentifierType() {
		return this.securityIdentifierType;
	}


	public void setSecurityIdentifierType(String securityIdentifierType) {
		this.securityIdentifierType = securityIdentifierType;
	}
}
