package com.clifton.integration.incoming.reconcile.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcileAccountBalancesByAccountTypeSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id", required = true)
	private Integer runId;

	@SearchField
	private String accountNumber;

	@SearchField
	private Boolean groupAccount;

	@SearchField
	private Date positionDate;

	@SearchField
	private BigDecimal expectedTransferAmountLocal;

	@SearchField
	private BigDecimal expectedTransferAmountBase;

	@SearchField
	private String currencySymbolLocal;

	@SearchField
	private String currencySymbolBase;

	@SearchField
	private BigDecimal collateralRequirementLocal;

	@SearchField
	private BigDecimal collateralRequirementBase;

	@SearchField
	private BigDecimal collateralAmountLocal;

	@SearchField
	private BigDecimal collateralAmountBase;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getExpectedTransferAmountLocal() {
		return this.expectedTransferAmountLocal;
	}


	public void setExpectedTransferAmountLocal(BigDecimal expectedTransferAmountLocal) {
		this.expectedTransferAmountLocal = expectedTransferAmountLocal;
	}


	public BigDecimal getExpectedTransferAmountBase() {
		return this.expectedTransferAmountBase;
	}


	public void setExpectedTransferAmountBase(BigDecimal expectedTransferAmountBase) {
		this.expectedTransferAmountBase = expectedTransferAmountBase;
	}


	public String getCurrencySymbolLocal() {
		return this.currencySymbolLocal;
	}


	public void setCurrencySymbolLocal(String currencySymbolLocal) {
		this.currencySymbolLocal = currencySymbolLocal;
	}


	public String getCurrencySymbolBase() {
		return this.currencySymbolBase;
	}


	public void setCurrencySymbolBase(String currencySymbolBase) {
		this.currencySymbolBase = currencySymbolBase;
	}


	public BigDecimal getCollateralRequirementLocal() {
		return this.collateralRequirementLocal;
	}


	public void setCollateralRequirementLocal(BigDecimal collateralRequirementLocal) {
		this.collateralRequirementLocal = collateralRequirementLocal;
	}


	public BigDecimal getCollateralRequirementBase() {
		return this.collateralRequirementBase;
	}


	public void setCollateralRequirementBase(BigDecimal collateralRequirementBase) {
		this.collateralRequirementBase = collateralRequirementBase;
	}


	public BigDecimal getCollateralAmountLocal() {
		return this.collateralAmountLocal;
	}


	public void setCollateralAmountLocal(BigDecimal collateralAmountLocal) {
		this.collateralAmountLocal = collateralAmountLocal;
	}


	public BigDecimal getCollateralAmountBase() {
		return this.collateralAmountBase;
	}


	public void setCollateralAmountBase(BigDecimal collateralAmountBase) {
		this.collateralAmountBase = collateralAmountBase;
	}


	public Boolean getGroupAccount() {
		return this.groupAccount;
	}


	public void setGroupAccount(Boolean groupAccount) {
		this.groupAccount = groupAccount;
	}
}
