package com.clifton.integration.incoming.definition;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.integration.file.IntegrationFile;

import java.util.Date;


/**
 * The IntegrationImportRun class represents run that is created each time an external file with active import definition(s) is uploaded to the FTP, SFTP or Input folder.
 */
public class IntegrationImportRun extends BaseEntity<Integer> {

	public static final String INTEGRATION_EVENT_ACCOUNT_GROUP_IMPORT = "APX Account Groups Import";
	public static final String INTEGRATION_EVENT_BROKER_DAILY_CASH = "Broker Daily Cash";
	public static final String INTEGRATION_EVENT_BROKER_M2M = "Broker Daily M2M";
	public static final String INTEGRATION_EVENT_BROKER_M2M_EXPENSE = "Broker Daily M2M Expense";
	public static final String INTEGRATION_EVENT_BROKER_DAILY_POSITIONS = "Broker Daily Positions";
	public static final String INTEGRATION_EVENT_BROKER_IMPORT = "Broker Import";
	public static final String INTEGRATION_EVENT_BROKER_INTRADAY_TRADES = "Broker Intraday Trades";
	public static final String INTEGRATION_EVENT_INTEGRATION_TRADE_IMPORT = "Integration Trade Import";
	public static final String INTEGRATION_EVENT_MANAGER_DOWNLOAD = "Manager Download";
	public static final String INTEGRATION_EVENT_MANAGER_TRANSACTION_IMPORT = "Manager Transaction Import";
	public static final String INTEGRATION_EVENT_MARKET_DATA_DAILY_EXCHANGE_RATES = "Market Data Broker Daily Exchange Rates";
	public static final String INTEGRATION_EVENT_MARKET_DATA_FIELD_VALUE_IMPORT = "Market Data Field Value Import";
	public static final String INTEGRATION_EVENT_MARKET_DATA_SECURITIES_IMPORT = "Market Data Securities Import";
	public static final String INTEGRATION_EVENT_MARKET_DATA_ACCOUNTS_IMPORT = "Market Data Accounts Import";
	public static final String INTEGRATION_EVENT_SECURITY_RESTRICTION_LIST_IMPORT = "Security Restriction List Import";
	public static final String INTEGRATION_EVENT_ARGOS_CASH_ADJUSTMENTS = "Cash Adjustments Import";
	public static final String INTEGRATION_EVENT_UNKNOWN_FILE = "UNKNOWN FILE";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private IntegrationImportDefinition integrationImportDefinition;
	private IntegrationImportStatus status;
	private Date startProcessDate;
	private Date endProcessDate;
	private Date effectiveDate;
	private String error;

	private IntegrationFile file;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun() {
		//
	}


	public IntegrationImportRun(IntegrationImportDefinition integrationImportDefinition, Date startProcessDate) {
		this.integrationImportDefinition = integrationImportDefinition;
		this.startProcessDate = startProcessDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getExecutionTimeFormatted() {
		if (getStartProcessDate() == null) {
			return null;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndProcessDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getTimeDifferenceShort(toDate, getStartProcessDate());
	}


	public IntegrationImportDefinition getIntegrationImportDefinition() {
		return this.integrationImportDefinition;
	}


	public void setIntegrationImportDefinition(IntegrationImportDefinition integrationImportDefinition) {
		this.integrationImportDefinition = integrationImportDefinition;
	}


	public IntegrationImportStatus getStatus() {
		return this.status;
	}


	public void setStatus(IntegrationImportStatus status) {
		this.status = status;
	}


	public Date getStartProcessDate() {
		return this.startProcessDate;
	}


	public void setStartProcessDate(Date startProcessDate) {
		this.startProcessDate = startProcessDate;
	}


	public Date getEndProcessDate() {
		return this.endProcessDate;
	}


	public void setEndProcessDate(Date endProcessDate) {
		this.endProcessDate = endProcessDate;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public IntegrationFile getFile() {
		return this.file;
	}


	public void setFile(IntegrationFile fileArchive) {
		this.file = fileArchive;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}
}
