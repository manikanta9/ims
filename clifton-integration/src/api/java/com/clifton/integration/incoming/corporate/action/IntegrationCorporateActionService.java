package com.clifton.integration.incoming.corporate.action;

import com.clifton.integration.incoming.corporate.action.search.IntegrationCorporateActionPositionRequestSearchForm;
import com.clifton.integration.incoming.corporate.action.search.IntegrationCorporateActionSearchForm;

import java.util.List;


/**
 * <code>IntegrationCorporateActionService</code> IntegrationCorporateAction entity persistence methods.
 *
 * @author TerryS
 */
public interface IntegrationCorporateActionService {

	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////                IntegrationCorporateAction Business Methods                 /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////

	public IntegrationCorporateAction getIntegrationCorporateAction(int id);


	public List<IntegrationCorporateAction> getIntegrationCorporateActionList(IntegrationCorporateActionSearchForm searchForm);


	public IntegrationCorporateAction saveIntegrationCorporateAction(IntegrationCorporateAction integrationCorporateAction);


	public void deleteIntegrationCorporateAction(int id);


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////        IntegrationCorporateActionPositionRequest Business Methods          /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////

	public IntegrationCorporateActionPositionRequest getIntegrationCorporateActionPositionRequest(int id);


	public List<IntegrationCorporateActionPositionRequest> getIntegrationCorporateActionPositionRequestList(IntegrationCorporateActionPositionRequestSearchForm searchForm);
}
