package com.clifton.integration.incoming.transformation.parameter;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;


/**
 * @author TerryS
 */
public class IntegrationTransformationParameterLink extends ManyToManyEntity<IntegrationTransformation, IntegrationTransformationParameter, Integer> {
	// allow transformations to share the same parameter values
}
