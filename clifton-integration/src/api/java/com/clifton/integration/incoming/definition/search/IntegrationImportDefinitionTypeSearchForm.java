package com.clifton.integration.incoming.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationImportDefinitionTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String eventName;

	@SearchField
	private String destinationTableName;

	@SearchField(searchField = "id", searchFieldPath = "targetApplicationList")
	private Short targetApplicationId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	public String getDestinationTableName() {
		return this.destinationTableName;
	}


	public void setDestinationTableName(String destinationTableName) {
		this.destinationTableName = destinationTableName;
	}


	public Short getTargetApplicationId() {
		return this.targetApplicationId;
	}


	public void setTargetApplicationId(Short targetApplicationId) {
		this.targetApplicationId = targetApplicationId;
	}
}
