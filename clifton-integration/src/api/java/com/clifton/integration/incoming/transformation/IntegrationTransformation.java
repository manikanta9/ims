package com.clifton.integration.incoming.transformation;

import com.clifton.core.beans.NamedEntity;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;

import java.util.List;


/**
 * @author mwacker
 */
public class IntegrationTransformation extends NamedEntity<Integer> {

	private IntegrationTransformationTypes type;
	private IntegrationImportDefinitionType definitionType;
	private boolean templateTransformation;
	private boolean systemDefined;


	private List<IntegrationTransformationFile> transformationFileList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isTemplateTransformation() {
		return this.templateTransformation;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportDefinitionType getDefinitionType() {
		return this.definitionType;
	}


	public void setDefinitionType(IntegrationImportDefinitionType definitionType) {
		this.definitionType = definitionType;
	}


	public void setTemplateTransformation(boolean templateTransformation) {
		this.templateTransformation = templateTransformation;
	}


	public List<IntegrationTransformationFile> getTransformationFileList() {
		return this.transformationFileList;
	}


	public void setTransformationFileList(List<IntegrationTransformationFile> transformationFileList) {
		this.transformationFileList = transformationFileList;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean isSystemDefined) {
		this.systemDefined = isSystemDefined;
	}


	public IntegrationTransformationTypes getType() {
		return this.type;
	}


	public void setType(IntegrationTransformationTypes type) {
		this.type = type;
	}
}
