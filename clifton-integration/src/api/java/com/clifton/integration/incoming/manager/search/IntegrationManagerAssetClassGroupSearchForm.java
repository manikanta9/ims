package com.clifton.integration.incoming.manager.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationManagerAssetClassGroupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "unmappedPositionType.id")
	private Short unmappedPositionTypeId;

	@SearchField(searchField = "name", searchFieldPath = "unmappedPositionType")
	private String unmappedPositionTypeName;

	@SearchField
	private Boolean requireAllAssetClassMappings;

	@SearchField
	private Integer businessCompanyId;

	@SearchField(searchField = "id", searchFieldPath = "definitionList")
	private Integer importDefinitionId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getUnmappedPositionTypeName() {
		return this.unmappedPositionTypeName;
	}


	public void setUnmappedPositionTypeName(String unmappedPositionTypeName) {
		this.unmappedPositionTypeName = unmappedPositionTypeName;
	}


	public Short getUnmappedPositionTypeId() {
		return this.unmappedPositionTypeId;
	}


	public void setUnmappedPositionTypeId(Short unmappedPositionTypeId) {
		this.unmappedPositionTypeId = unmappedPositionTypeId;
	}


	public Boolean getRequireAllAssetClassMappings() {
		return this.requireAllAssetClassMappings;
	}


	public void setRequireAllAssetClassMappings(Boolean requireAllAssetClassMappings) {
		this.requireAllAssetClassMappings = requireAllAssetClassMappings;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Integer getImportDefinitionId() {
		return this.importDefinitionId;
	}


	public void setImportDefinitionId(Integer importDefinitionId) {
		this.importDefinitionId = importDefinitionId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
