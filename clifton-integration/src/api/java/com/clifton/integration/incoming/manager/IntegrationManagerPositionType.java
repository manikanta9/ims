package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>IntegrationManagerPositionType</code> class defines a type of position row.
 * The type of a row determines the type of corresponding value. Is it cash, security, liability, etc.
 *
 * @author vgomelsky
 */
public class IntegrationManagerPositionType extends NamedEntity<Short> {

	/**
	 * Defines whether position value represents cash position.
	 */
	private boolean cash;
	/**
	 * Defines whether position value represents a security position (non-cash).
	 */
	private boolean security;
	/**
	 * Defines whether position value represents a total of cash and securities.
	 * Total values are used to calculate cash or security total balance if one one is available.
	 */
	private boolean total;
	/**
	 * Optionally, position value can be negated before being considered as cash or security: LIABILITY maybe sent to us as a positive number.
	 */
	private boolean negative;

	/**
	 * Specifies whether this position row is system generated.  When cash row and total rows are imported, in order to calculate securities balance,
	 * we subtract total cash from total total and insert a new systemGenerated row with securities total.
	 */
	private boolean systemGenerated;

	/**
	 * Specifies whether this position row is ignored.
	 */
	private boolean ignore;

	/**
	 * Specifies weather the position is pending.  Pending purchase or sale that will be added to cash or securities.
	 * For example, if well sell $100 or a security, we will add $100 to cash and remove $100 from securities or if we purchase
	 * $100 then we add $100 to securities and remove $100 from cash.
	 */
	private boolean pending;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCash() {
		return this.cash;
	}


	public void setCash(boolean cash) {
		this.cash = cash;
	}


	public boolean isSecurity() {
		return this.security;
	}


	public void setSecurity(boolean security) {
		this.security = security;
	}


	public boolean isTotal() {
		return this.total;
	}


	public void setTotal(boolean total) {
		this.total = total;
	}


	public boolean isNegative() {
		return this.negative;
	}


	public void setNegative(boolean negative) {
		this.negative = negative;
	}


	public boolean isSystemGenerated() {
		return this.systemGenerated;
	}


	public void setSystemGenerated(boolean systemGenerated) {
		this.systemGenerated = systemGenerated;
	}


	public boolean isIgnore() {
		return this.ignore;
	}


	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}


	public boolean isPending() {
		return this.pending;
	}


	public void setPending(boolean pending) {
		this.pending = pending;
	}
}
