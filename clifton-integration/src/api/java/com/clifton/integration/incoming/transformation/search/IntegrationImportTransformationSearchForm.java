package com.clifton.integration.incoming.transformation.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationImportTransformationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "definition.id")
	private Integer definitionId;


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}
}
