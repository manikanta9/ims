package com.clifton.integration.incoming.reconcile.m2m;


import com.clifton.core.beans.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcileM2MDailyImport extends BaseEntity<Integer> {

	private String uuid;
	private int integrationImportRunId;
	private String accountNumber;
	private Date positionDate;
	private BigDecimal expectedTransferAmountBase;
	private BigDecimal expectedTransferAmountLocal;
	private String currencySymbolLocal;
	private String currencySymbolBase;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public String getCurrencySymbolLocal() {
		return this.currencySymbolLocal;
	}


	public void setCurrencySymbolLocal(String currencySymbolLocal) {
		this.currencySymbolLocal = currencySymbolLocal;
	}


	public String getCurrencySymbolBase() {
		return this.currencySymbolBase;
	}


	public void setCurrencySymbolBase(String currencySymbolBase) {
		this.currencySymbolBase = currencySymbolBase;
	}


	public int getIntegrationImportRunId() {
		return this.integrationImportRunId;
	}


	public void setIntegrationImportRunId(int integrationImportRunId) {
		this.integrationImportRunId = integrationImportRunId;
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public BigDecimal getExpectedTransferAmountBase() {
		return this.expectedTransferAmountBase;
	}


	public void setExpectedTransferAmountBase(BigDecimal expectedTransferAmountBase) {
		this.expectedTransferAmountBase = expectedTransferAmountBase;
	}


	public BigDecimal getExpectedTransferAmountLocal() {
		return this.expectedTransferAmountLocal;
	}


	public void setExpectedTransferAmountLocal(BigDecimal expectedTransferAmountLocal) {
		this.expectedTransferAmountLocal = expectedTransferAmountLocal;
	}
}
