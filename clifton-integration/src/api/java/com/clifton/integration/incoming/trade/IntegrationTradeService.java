package com.clifton.integration.incoming.trade;

import com.clifton.integration.incoming.trade.search.IntegrationTradeSearchForm;

import java.util.List;


public interface IntegrationTradeService {

	public List<IntegrationTrade> getIntegrationTradeList(IntegrationTradeSearchForm searchForm);
}
