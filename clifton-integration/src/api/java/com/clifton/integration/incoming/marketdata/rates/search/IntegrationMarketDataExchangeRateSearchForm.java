package com.clifton.integration.incoming.marketdata.rates.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>IntegrationMarketDataExchangeRateSearchForm</code> represents search configuration for
 * an IntegrationMarketDataExchangeRate
 *
 * @author rbrooks
 */
public class IntegrationMarketDataExchangeRateSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id")
	private Integer runId;

	@SearchField
	private Date rateDate;

	@SearchField
	private String fromCurrency;

	@SearchField
	private String toCurrency;

	@SearchField
	private BigDecimal rate;

	@SearchField
	private BigDecimal inverseRate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Date getRateDate() {
		return this.rateDate;
	}


	public void setRateDate(Date rateDate) {
		this.rateDate = rateDate;
	}


	public String getFromCurrency() {
		return this.fromCurrency;
	}


	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}


	public String getToCurrency() {
		return this.toCurrency;
	}


	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public BigDecimal getInverseRate() {
		return this.inverseRate;
	}


	public void setInverseRate(BigDecimal inverseRate) {
		this.inverseRate = inverseRate;
	}
}
