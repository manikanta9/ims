package com.clifton.integration.incoming.reconcile.collateral;


import java.util.List;


public interface IntegrationReconcileCollateralService {

	public List<IntegrationReconcileCollateralDailyImport> getIntegrationReconcileCollateralDailyImportListByRun(int runId);
}
