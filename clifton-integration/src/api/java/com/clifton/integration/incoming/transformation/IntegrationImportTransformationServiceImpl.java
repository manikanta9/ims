package com.clifton.integration.incoming.transformation;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.integration.incoming.transformation.search.IntegrationImportTransformationSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class IntegrationImportTransformationServiceImpl implements IntegrationImportTransformationService {

	private AdvancedUpdatableDAO<IntegrationImportTransformation, Criteria> integrationImportTransformationDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationImportTransformation getIntegrationImportTransformation(int id) {
		return getIntegrationImportTransformationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<IntegrationImportTransformation> getIntegrationImportTransformationList(IntegrationImportTransformationSearchForm searchForm) {
		return getIntegrationImportTransformationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public IntegrationImportTransformation saveIntegrationImportTransformation(IntegrationImportTransformation bean) {
		return getIntegrationImportTransformationDAO().save(bean);
	}


	@Override
	public void deleteIntegrationImportTransformation(int id) {
		getIntegrationImportTransformationDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                  Getters and Setters                           //////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<IntegrationImportTransformation, Criteria> getIntegrationImportTransformationDAO() {
		return this.integrationImportTransformationDAO;
	}


	public void setIntegrationImportTransformationDAO(AdvancedUpdatableDAO<IntegrationImportTransformation, Criteria> integrationImportTransformationDAO) {
		this.integrationImportTransformationDAO = integrationImportTransformationDAO;
	}
}
