package com.clifton.integration.incoming.transformation;

/**
 * @author mwacker
 */
public enum IntegrationTransformationFileTypes {

	EFFECTIVE_DATE_TRANSFORMATION(false), DATA_TRANSFORMATION(true), FILE_TO_FILE_TRANSFORMATION(true), DATA_FILE(true);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final boolean transformationRequired;


	IntegrationTransformationFileTypes(boolean transformationRequired) {
		this.transformationRequired = transformationRequired;
	}


	public boolean isTransformationRequired() {
		return this.transformationRequired;
	}
}

