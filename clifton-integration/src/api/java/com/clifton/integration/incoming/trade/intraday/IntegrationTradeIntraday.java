package com.clifton.integration.incoming.trade.intraday;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationTradeIntraday extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	private String accountNumber;
	private String clientAccountNumber;
	private String executingBrokerCompanyName;
	private String clearingBrokerCompanyName;

	private String investmentSecuritySymbol;
	private String payingSecuritySymbol;
	private String securitySymbolType;

	private boolean buy;
	private Date tradeDate;
	private BigDecimal quantity;
	private BigDecimal price;

	private IntegrationTradeIntradayStatuses status;
	private IntegrationTradeIntradayTypes type;

	private String uniqueTradeId;
	private String fkFixAllocationId;

	private Integer fkSourceId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getExecutingBrokerCompanyName() {
		return this.executingBrokerCompanyName;
	}


	public void setExecutingBrokerCompanyName(String executingBrokerCompanyName) {
		this.executingBrokerCompanyName = executingBrokerCompanyName;
	}


	public String getClearingBrokerCompanyName() {
		return this.clearingBrokerCompanyName;
	}


	public void setClearingBrokerCompanyName(String clearingBrokerCompanyName) {
		this.clearingBrokerCompanyName = clearingBrokerCompanyName;
	}


	public String getInvestmentSecuritySymbol() {
		return this.investmentSecuritySymbol;
	}


	public void setInvestmentSecuritySymbol(String investmentSecuritySymbol) {
		this.investmentSecuritySymbol = investmentSecuritySymbol;
	}


	public String getPayingSecuritySymbol() {
		return this.payingSecuritySymbol;
	}


	public void setPayingSecuritySymbol(String payingSecuritySymbol) {
		this.payingSecuritySymbol = payingSecuritySymbol;
	}


	public String getSecuritySymbolType() {
		return this.securitySymbolType;
	}


	public void setSecuritySymbolType(String securitySymbolType) {
		this.securitySymbolType = securitySymbolType;
	}


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public IntegrationTradeIntradayStatuses getStatus() {
		return this.status;
	}


	public void setStatus(IntegrationTradeIntradayStatuses status) {
		this.status = status;
	}


	public IntegrationTradeIntradayTypes getType() {
		return this.type;
	}


	public void setType(IntegrationTradeIntradayTypes type) {
		this.type = type;
	}


	public String getUniqueTradeId() {
		return this.uniqueTradeId;
	}


	public void setUniqueTradeId(String uniqueTradeId) {
		this.uniqueTradeId = uniqueTradeId;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public String getFkFixAllocationId() {
		return this.fkFixAllocationId;
	}


	public void setFkFixAllocationId(String fkFixAllocationId) {
		this.fkFixAllocationId = fkFixAllocationId;
	}


	public Integer getFkSourceId() {
		return this.fkSourceId;
	}


	public void setFkSourceId(Integer fkSourceId) {
		this.fkSourceId = fkSourceId;
	}
}
