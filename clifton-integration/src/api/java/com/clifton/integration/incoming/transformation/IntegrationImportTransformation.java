package com.clifton.integration.incoming.transformation;


import com.clifton.core.beans.BaseEntity;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>IntegrationImportTransformation</code> class applies optional transformation to
 * import content.
 */
public class IntegrationImportTransformation extends BaseEntity<Integer> {

	private IntegrationImportDefinition definition;
	private SystemBean transformationSystemBean;
	private Integer order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(IntegrationImportDefinition definition) {
		this.definition = definition;
	}


	public SystemBean getTransformationSystemBean() {
		return this.transformationSystemBean;
	}


	public void setTransformationSystemBean(SystemBean transformationSystemBean) {
		this.transformationSystemBean = transformationSystemBean;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}
}
