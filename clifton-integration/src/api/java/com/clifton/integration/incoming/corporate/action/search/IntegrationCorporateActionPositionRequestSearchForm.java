package com.clifton.integration.incoming.corporate.action.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * @author mwacker
 */
public class IntegrationCorporateActionPositionRequestSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	/**
	 * importRun
	 */
	@SearchField(searchField = "importRun.integrationImportDefinition.name,importRun.integrationImportDefinition.description")
	private String searchPattern;

	@SearchField(searchField = "importRun.id")
	private Integer importRunId;

	/**
	 * InvestmentSecurityEvent
	 */
	@SearchField
	private Long corporateActionIdentifier;

	@SearchField
	private String investmentEventTypeName;

	@SearchField
	private String investmentEventIndicatorName;

	@SearchField
	private String investmentEventStatusName;


	@SearchField
	private Integer investmentSecurityIdentifier;

	@SearchField
	private String cusip;

	@SearchField
	private String sedol;

	@SearchField
	private String isin;


	@SearchField
	private String referenceIdentifier;

	@SearchField
	private String businessIdentifierCode;


	@SearchField
	private Date effectiveDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getImportRunId() {
		return this.importRunId;
	}


	public void setImportRunId(Integer importRunId) {
		this.importRunId = importRunId;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public String getInvestmentEventTypeName() {
		return this.investmentEventTypeName;
	}


	public void setInvestmentEventTypeName(String investmentEventTypeName) {
		this.investmentEventTypeName = investmentEventTypeName;
	}


	public String getInvestmentEventIndicatorName() {
		return this.investmentEventIndicatorName;
	}


	public void setInvestmentEventIndicatorName(String investmentEventIndicatorName) {
		this.investmentEventIndicatorName = investmentEventIndicatorName;
	}


	public String getInvestmentEventStatusName() {
		return this.investmentEventStatusName;
	}


	public void setInvestmentEventStatusName(String investmentEventStatusName) {
		this.investmentEventStatusName = investmentEventStatusName;
	}


	public Integer getInvestmentSecurityIdentifier() {
		return this.investmentSecurityIdentifier;
	}


	public void setInvestmentSecurityIdentifier(Integer investmentSecurityIdentifier) {
		this.investmentSecurityIdentifier = investmentSecurityIdentifier;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getReferenceIdentifier() {
		return this.referenceIdentifier;
	}


	public void setReferenceIdentifier(String referenceIdentifier) {
		this.referenceIdentifier = referenceIdentifier;
	}


	public String getBusinessIdentifierCode() {
		return this.businessIdentifierCode;
	}


	public void setBusinessIdentifierCode(String businessIdentifierCode) {
		this.businessIdentifierCode = businessIdentifierCode;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
}
