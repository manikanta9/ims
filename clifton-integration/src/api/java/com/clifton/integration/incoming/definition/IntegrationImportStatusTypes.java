package com.clifton.integration.incoming.definition;


public enum IntegrationImportStatusTypes {

	RUNNING(false),
	SUCCESSFUL(false),
	REPROCESSED(false),
	TRANSFORMATION_DEPLOYMENT_SUCCESSFUL(false),
	PROCESSED_WITH_ERRORS(true),
	ERROR(true),
	MAPPING_ERROR(true),
	TRANSFORMATION_DEPLOYMENT_ERROR(true),
	NOT_PROCESSED(false);

	private boolean error;


	IntegrationImportStatusTypes(boolean error) {
		this.error = error;
	}


	public static IntegrationImportStatusTypes findIntegrationImportStatusType(String name) {
		return IntegrationImportStatusTypes.valueOf(name);
	}


	public boolean isError() {
		return this.error;
	}
}
