package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.BaseSimpleEntity;


/**
 * The <code>IntegrationManagerSecurity</code> ...
 *
 * @author mwacker
 */
public class IntegrationManagerSecurity extends BaseSimpleEntity<Integer> {

	/**
	 * Our interpretation of bank's sourceAssetClass: could be CASH for anything that starts with CASH, etc.
	 * This is the asset class that's used to determine position type.
	 */
	private String assetClass;
	/**
	 * Unmodified asset class name that we received from the bank
	 */
	private String sourceAssetClass;
	/**
	 * When available, security identifier and its type (bloomberg, etc.)
	 */
	private String securityIdentifier;
	private String securityIdentifierType;
	private String securityDescription;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}


	public String getSourceAssetClass() {
		return this.sourceAssetClass;
	}


	public void setSourceAssetClass(String sourceAssetClass) {
		this.sourceAssetClass = sourceAssetClass;
	}


	public String getSecurityIdentifier() {
		return this.securityIdentifier;
	}


	public void setSecurityIdentifier(String securityId) {
		this.securityIdentifier = securityId;
	}


	public String getSecurityIdentifierType() {
		return this.securityIdentifierType;
	}


	public void setSecurityIdentifierType(String securityIdType) {
		this.securityIdentifierType = securityIdType;
	}


	public String getSecurityDescription() {
		return this.securityDescription;
	}


	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}
}
