package com.clifton.integration.incoming.reconcile.m2m;


/**
 * The <code>IntegrationReconcileM2MExpenses</code> is an enum used to describe expense types (or revenue types) when
 * loading M2M data from brokers.
 * <p/>
 * <p/>
 * <p/>
 * <b>Note: These enum names are used in transformations. Changing an enum name will require a change in all transformations
 * that reference it, or else the transformations will break.</b>
 * <p/>
 * <p/>
 * <p/>
 * Transformations that use these expense enums typically contain "OtcM2MExpenses" in the name. For example, see MorganStanleyOtcM2MExpensesImport.ktr.
 *
 * @author jgommels
 */
public enum IntegrationReconcileM2MExpenses {
	PAI, CLEARING_ACTIVITY_FEE, INTEREST
}
