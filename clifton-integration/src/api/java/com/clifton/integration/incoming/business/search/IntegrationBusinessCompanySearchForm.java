package com.clifton.integration.incoming.business.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author mwacker
 */
public class IntegrationBusinessCompanySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id")
	private Integer runId;

	@SearchField
	private Integer bloombergCompanyIdentifier;
	@SearchField
	private Integer parentBloombergCompanyIdentifier;
	@SearchField
	private Integer ultimateParentBloombergCompanyIdentifier;
	@SearchField
	private Integer obligorBloombergCompanyIdentifier;

	@SearchField
	private String longCompanyName;
	@SearchField
	private String companyLegalName;

	@SearchField
	private String companyCorporateTicker;
	@SearchField
	private String ultimateParentTickerExchange;
	@SearchField
	private String ultimateParentTickerExchangeCorporateTicker;

	@SearchField
	private String industrySectorName;
	@SearchField
	private String industrySectorGroupName;
	@SearchField
	private String industrySectorSubGroupName;

	@SearchField
	private Integer industrySectorNumber;
	@SearchField
	private Integer industrySectorGroupNumber;
	@SearchField
	private Integer industrySectorSubGroupNumber;


	@SearchField
	private String countryOfDomicile;
	@SearchField
	private String countryOfIncorporation;
	@SearchField
	private String countryOfRisk;
	@SearchField
	private String stateOfDomicile;
	@SearchField
	private String stateOfIncorporation;


	@SearchField
	private Boolean ultimateParent;
	@SearchField
	private Boolean acquiredByParent;
	@SearchField
	private String companyToParentRelationship;

	@SearchField
	private String legalEntityIdentifier;
	@SearchField
	private String legalEntityStatus;
	@SearchField
	private Date legalEntityAssignedDate;
	@SearchField
	private Date legalEntityDisabledDate;
	@SearchField
	private String legalEntityRegistrationAddress;


	@SearchField
	private String companyAddress;
	@SearchField
	private String companyFaxNumber;
	@SearchField
	private String companyTelephoneNumber;
	@SearchField
	private String companyWebAddress;
	@SearchField
	private Boolean privateCompany;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public Integer getBloombergCompanyIdentifier() {
		return this.bloombergCompanyIdentifier;
	}


	public void setBloombergCompanyIdentifier(Integer bloombergCompanyIdentifier) {
		this.bloombergCompanyIdentifier = bloombergCompanyIdentifier;
	}


	public Integer getParentBloombergCompanyIdentifier() {
		return this.parentBloombergCompanyIdentifier;
	}


	public void setParentBloombergCompanyIdentifier(Integer parentBloombergCompanyIdentifier) {
		this.parentBloombergCompanyIdentifier = parentBloombergCompanyIdentifier;
	}


	public Integer getUltimateParentBloombergCompanyIdentifier() {
		return this.ultimateParentBloombergCompanyIdentifier;
	}


	public void setUltimateParentBloombergCompanyIdentifier(Integer ultimateParentBloombergCompanyIdentifier) {
		this.ultimateParentBloombergCompanyIdentifier = ultimateParentBloombergCompanyIdentifier;
	}


	public Integer getObligorBloombergCompanyIdentifier() {
		return this.obligorBloombergCompanyIdentifier;
	}


	public void setObligorBloombergCompanyIdentifier(Integer obligorBloombergCompanyIdentifier) {
		this.obligorBloombergCompanyIdentifier = obligorBloombergCompanyIdentifier;
	}


	public String getLongCompanyName() {
		return this.longCompanyName;
	}


	public void setLongCompanyName(String longCompanyName) {
		this.longCompanyName = longCompanyName;
	}


	public String getCompanyLegalName() {
		return this.companyLegalName;
	}


	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}


	public String getCompanyCorporateTicker() {
		return this.companyCorporateTicker;
	}


	public void setCompanyCorporateTicker(String companyCorporateTicker) {
		this.companyCorporateTicker = companyCorporateTicker;
	}


	public String getUltimateParentTickerExchange() {
		return this.ultimateParentTickerExchange;
	}


	public void setUltimateParentTickerExchange(String ultimateParentTickerExchange) {
		this.ultimateParentTickerExchange = ultimateParentTickerExchange;
	}


	public String getUltimateParentTickerExchangeCorporateTicker() {
		return this.ultimateParentTickerExchangeCorporateTicker;
	}


	public void setUltimateParentTickerExchangeCorporateTicker(String ultimateParentTickerExchangeCorporateTicker) {
		this.ultimateParentTickerExchangeCorporateTicker = ultimateParentTickerExchangeCorporateTicker;
	}


	public String getIndustrySectorName() {
		return this.industrySectorName;
	}


	public void setIndustrySectorName(String industrySectorName) {
		this.industrySectorName = industrySectorName;
	}


	public String getIndustrySectorGroupName() {
		return this.industrySectorGroupName;
	}


	public void setIndustrySectorGroupName(String industrySectorGroupName) {
		this.industrySectorGroupName = industrySectorGroupName;
	}


	public String getIndustrySectorSubGroupName() {
		return this.industrySectorSubGroupName;
	}


	public void setIndustrySectorSubGroupName(String industrySectorSubGroupName) {
		this.industrySectorSubGroupName = industrySectorSubGroupName;
	}


	public Integer getIndustrySectorNumber() {
		return this.industrySectorNumber;
	}


	public void setIndustrySectorNumber(Integer industrySectorNumber) {
		this.industrySectorNumber = industrySectorNumber;
	}


	public Integer getIndustrySectorGroupNumber() {
		return this.industrySectorGroupNumber;
	}


	public void setIndustrySectorGroupNumber(Integer industrySectorGroupNumber) {
		this.industrySectorGroupNumber = industrySectorGroupNumber;
	}


	public Integer getIndustrySectorSubGroupNumber() {
		return this.industrySectorSubGroupNumber;
	}


	public void setIndustrySectorSubGroupNumber(Integer industrySectorSubGroupNumber) {
		this.industrySectorSubGroupNumber = industrySectorSubGroupNumber;
	}


	public String getCountryOfDomicile() {
		return this.countryOfDomicile;
	}


	public void setCountryOfDomicile(String countryOfDomicile) {
		this.countryOfDomicile = countryOfDomicile;
	}


	public String getCountryOfIncorporation() {
		return this.countryOfIncorporation;
	}


	public void setCountryOfIncorporation(String countryOfIncorporation) {
		this.countryOfIncorporation = countryOfIncorporation;
	}


	public String getCountryOfRisk() {
		return this.countryOfRisk;
	}


	public void setCountryOfRisk(String countryOfRisk) {
		this.countryOfRisk = countryOfRisk;
	}


	public String getStateOfDomicile() {
		return this.stateOfDomicile;
	}


	public void setStateOfDomicile(String stateOfDomicile) {
		this.stateOfDomicile = stateOfDomicile;
	}


	public String getStateOfIncorporation() {
		return this.stateOfIncorporation;
	}


	public void setStateOfIncorporation(String stateOfIncorporation) {
		this.stateOfIncorporation = stateOfIncorporation;
	}


	public Boolean getUltimateParent() {
		return this.ultimateParent;
	}


	public void setUltimateParent(Boolean ultimateParent) {
		this.ultimateParent = ultimateParent;
	}


	public Boolean getAcquiredByParent() {
		return this.acquiredByParent;
	}


	public void setAcquiredByParent(Boolean acquiredByParent) {
		this.acquiredByParent = acquiredByParent;
	}


	public String getCompanyToParentRelationship() {
		return this.companyToParentRelationship;
	}


	public void setCompanyToParentRelationship(String companyToParentRelationship) {
		this.companyToParentRelationship = companyToParentRelationship;
	}


	public String getLegalEntityIdentifier() {
		return this.legalEntityIdentifier;
	}


	public void setLegalEntityIdentifier(String legalEntityIdentifier) {
		this.legalEntityIdentifier = legalEntityIdentifier;
	}


	public String getLegalEntityStatus() {
		return this.legalEntityStatus;
	}


	public void setLegalEntityStatus(String legalEntityStatus) {
		this.legalEntityStatus = legalEntityStatus;
	}


	public Date getLegalEntityAssignedDate() {
		return this.legalEntityAssignedDate;
	}


	public void setLegalEntityAssignedDate(Date legalEntityAssignedDate) {
		this.legalEntityAssignedDate = legalEntityAssignedDate;
	}


	public Date getLegalEntityDisabledDate() {
		return this.legalEntityDisabledDate;
	}


	public void setLegalEntityDisabledDate(Date legalEntityDisabledDate) {
		this.legalEntityDisabledDate = legalEntityDisabledDate;
	}


	public String getLegalEntityRegistrationAddress() {
		return this.legalEntityRegistrationAddress;
	}


	public void setLegalEntityRegistrationAddress(String legalEntityRegistrationAddress) {
		this.legalEntityRegistrationAddress = legalEntityRegistrationAddress;
	}


	public String getCompanyAddress() {
		return this.companyAddress;
	}


	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}


	public String getCompanyFaxNumber() {
		return this.companyFaxNumber;
	}


	public void setCompanyFaxNumber(String companyFaxNumber) {
		this.companyFaxNumber = companyFaxNumber;
	}


	public String getCompanyTelephoneNumber() {
		return this.companyTelephoneNumber;
	}


	public void setCompanyTelephoneNumber(String companyTelephoneNumber) {
		this.companyTelephoneNumber = companyTelephoneNumber;
	}


	public String getCompanyWebAddress() {
		return this.companyWebAddress;
	}


	public void setCompanyWebAddress(String companyWebAddress) {
		this.companyWebAddress = companyWebAddress;
	}


	public Boolean getPrivateCompany() {
		return this.privateCompany;
	}


	public void setPrivateCompany(Boolean privateCompany) {
		this.privateCompany = privateCompany;
	}
}
