package com.clifton.integration.incoming.reconcile.collateral;


import com.clifton.core.beans.BaseSimpleEntity;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcileCollateralDailyImport extends BaseSimpleEntity<Integer> {

	private String uuid;
	private int integrationImportRunId;
	private String accountNumber;
	private Date positionDate;
	private BigDecimal collateralRequirementLocal;
	private BigDecimal collateralRequirementBase;
	private BigDecimal collateralAmountLocal;
	private BigDecimal collateralAmountBase;
	private BigDecimal expectedTransferAmountLocal;
	private BigDecimal expectedTransferAmountBase;
	private String currencySymbolLocal;
	private String currencySymbolBase;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public int getIntegrationImportRunId() {
		return this.integrationImportRunId;
	}


	public void setIntegrationImportRunId(int integrationImportRunId) {
		this.integrationImportRunId = integrationImportRunId;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getCollateralRequirementLocal() {
		return this.collateralRequirementLocal;
	}


	public void setCollateralRequirementLocal(BigDecimal collateralRequirementLocal) {
		this.collateralRequirementLocal = collateralRequirementLocal;
	}


	public BigDecimal getCollateralRequirementBase() {
		return this.collateralRequirementBase;
	}


	public void setCollateralRequirementBase(BigDecimal collateralRequirementBase) {
		this.collateralRequirementBase = collateralRequirementBase;
	}


	public BigDecimal getCollateralAmountLocal() {
		return this.collateralAmountLocal;
	}


	public void setCollateralAmountLocal(BigDecimal collateralAmountLocal) {
		this.collateralAmountLocal = collateralAmountLocal;
	}


	public BigDecimal getCollateralAmountBase() {
		return this.collateralAmountBase;
	}


	public void setCollateralAmountBase(BigDecimal collateralAmountBase) {
		this.collateralAmountBase = collateralAmountBase;
	}


	public BigDecimal getExpectedTransferAmountLocal() {
		return this.expectedTransferAmountLocal;
	}


	public void setExpectedTransferAmountLocal(BigDecimal expectedTransferAmountLocal) {
		this.expectedTransferAmountLocal = expectedTransferAmountLocal;
	}


	public BigDecimal getExpectedTransferAmountBase() {
		return this.expectedTransferAmountBase;
	}


	public void setExpectedTransferAmountBase(BigDecimal expectedTransferAmountBase) {
		this.expectedTransferAmountBase = expectedTransferAmountBase;
	}


	public String getCurrencySymbolLocal() {
		return this.currencySymbolLocal;
	}


	public void setCurrencySymbolLocal(String currencySymbolLocal) {
		this.currencySymbolLocal = currencySymbolLocal;
	}


	public String getCurrencySymbolBase() {
		return this.currencySymbolBase;
	}


	public void setCurrencySymbolBase(String currencySymbolBase) {
		this.currencySymbolBase = currencySymbolBase;
	}
}
