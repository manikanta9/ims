package com.clifton.integration.incoming.transformation.parameter;

import com.clifton.core.beans.NamedEntity;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.system.schema.SystemDataSource;


/**
 * @author TerryS
 */
public class IntegrationTransformationParameter extends NamedEntity<Integer> {

	private IntegrationTransformationParameterTypes type;
	private String key;
	private String value;
	private SystemDataSource systemDataSource;
	private SecuritySecret securitySecretValue;

	////////////////////////////////////////////////////////////////////////////


	public boolean isSecretParameterValue() {
		return getType() != null && getType().name().equals(IntegrationTransformationParameterTypes.SECRET.name());
	}


	public boolean isDataSource() {
		return getType() != null && getType().name().equals(IntegrationTransformationParameterTypes.DATA_SOURCE.name());
	}


	public String getTypeName() {
		return this.type.getName();
	}

	////////////////////////////////////////////////////////////////////////////


	public IntegrationTransformationParameterTypes getType() {
		return this.type;
	}


	public void setType(IntegrationTransformationParameterTypes type) {
		this.type = type;
	}


	public String getKey() {
		return this.key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public SystemDataSource getSystemDataSource() {
		return this.systemDataSource;
	}


	public void setSystemDataSource(SystemDataSource systemDataSource) {
		this.systemDataSource = systemDataSource;
	}


	public SecuritySecret getSecuritySecretValue() {
		return this.securitySecretValue;
	}


	public void setSecuritySecretValue(SecuritySecret securitySecretValue) {
		this.securitySecretValue = securitySecretValue;
	}
}
