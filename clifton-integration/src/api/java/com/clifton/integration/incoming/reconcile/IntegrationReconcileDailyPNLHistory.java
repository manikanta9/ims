package com.clifton.integration.incoming.reconcile;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcileDailyPNLHistory extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	private String accountNumber;
	private String accountName;
	private String clientAccountNumber;

	private Date positionDate;

	private String securitySymbol;
	private String securitySymbolType;

	private String positionCurrencySymbol;
	private String accountCurrencySymbol;

	private BigDecimal fxRate;
	private BigDecimal marketPrice;
	private BigDecimal quantity = BigDecimal.ZERO;
	private BigDecimal priorQuantity = BigDecimal.ZERO;
	private BigDecimal todayClosedQuantity = BigDecimal.ZERO;
	private boolean shortPosition;

	private BigDecimal todayCommissionLocal = BigDecimal.ZERO;
	private BigDecimal todayCommissionBase = BigDecimal.ZERO;
	private BigDecimal todayRealizedGainLossLocal = BigDecimal.ZERO;
	private BigDecimal todayRealizedGainLossBase = BigDecimal.ZERO;
	private BigDecimal openTradeEquityLocal = BigDecimal.ZERO;
	private BigDecimal openTradeEquityBase = BigDecimal.ZERO;
	private BigDecimal priorOpenTradeEquityLocal = BigDecimal.ZERO;
	private BigDecimal priorOpenTradeEquityBase = BigDecimal.ZERO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecuritySymbolType() {
		return this.securitySymbolType;
	}


	public void setSecuritySymbolType(String securitySymbolType) {
		this.securitySymbolType = securitySymbolType;
	}


	public String getPositionCurrencySymbol() {
		return this.positionCurrencySymbol;
	}


	public void setPositionCurrencySymbol(String positionCurrencySymbol) {
		this.positionCurrencySymbol = positionCurrencySymbol;
	}


	public String getAccountCurrencySymbol() {
		return this.accountCurrencySymbol;
	}


	public void setAccountCurrencySymbol(String accountCurrencySymbol) {
		this.accountCurrencySymbol = accountCurrencySymbol;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getPriorQuantity() {
		return this.priorQuantity;
	}


	public void setPriorQuantity(BigDecimal priorQuantity) {
		this.priorQuantity = priorQuantity;
	}


	public BigDecimal getTodayClosedQuantity() {
		return this.todayClosedQuantity;
	}


	public void setTodayClosedQuantity(BigDecimal todayClosedQuantity) {
		this.todayClosedQuantity = todayClosedQuantity;
	}


	public boolean isShortPosition() {
		return this.shortPosition;
	}


	public void setShortPosition(boolean shortPosition) {
		this.shortPosition = shortPosition;
	}


	public BigDecimal getTodayCommissionLocal() {
		return this.todayCommissionLocal;
	}


	public void setTodayCommissionLocal(BigDecimal todayCommissionLocal) {
		this.todayCommissionLocal = todayCommissionLocal;
	}


	public BigDecimal getTodayCommissionBase() {
		return this.todayCommissionBase;
	}


	public void setTodayCommissionBase(BigDecimal todayCommissionBase) {
		this.todayCommissionBase = todayCommissionBase;
	}


	public BigDecimal getTodayRealizedGainLossLocal() {
		return this.todayRealizedGainLossLocal;
	}


	public void setTodayRealizedGainLossLocal(BigDecimal todayRealizedGainLossLocal) {
		this.todayRealizedGainLossLocal = todayRealizedGainLossLocal;
	}


	public BigDecimal getTodayRealizedGainLossBase() {
		return this.todayRealizedGainLossBase;
	}


	public void setTodayRealizedGainLossBase(BigDecimal todayRealizedGainLossBase) {
		this.todayRealizedGainLossBase = todayRealizedGainLossBase;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getPriorOpenTradeEquityLocal() {
		return this.priorOpenTradeEquityLocal;
	}


	public void setPriorOpenTradeEquityLocal(BigDecimal priorOpenTradeEquityLocal) {
		this.priorOpenTradeEquityLocal = priorOpenTradeEquityLocal;
	}


	public BigDecimal getPriorOpenTradeEquityBase() {
		return this.priorOpenTradeEquityBase;
	}


	public void setPriorOpenTradeEquityBase(BigDecimal priorOpenTradeEquityBase) {
		this.priorOpenTradeEquityBase = priorOpenTradeEquityBase;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
}
