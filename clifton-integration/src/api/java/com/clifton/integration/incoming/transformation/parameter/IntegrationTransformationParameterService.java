package com.clifton.integration.incoming.transformation.parameter;

import java.util.List;


/**
 * @author TerryS
 */
public interface IntegrationTransformationParameterService {

	public IntegrationTransformationParameter getIntegrationTransformationParameter(int id);


	public List<IntegrationTransformationParameter> getIntegrationTransformationParameterList(IntegrationTransformationParameterSearchForm searchForm);


	public void deleteIntegrationTransformationParameter(int id);


	public IntegrationTransformationParameter saveIntegrationTransformationParameter(IntegrationTransformationParameter parameter);

	////////////////////////////////////////////////////////////////////////////
	///////      Integration Transformation Parameter Link Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationTransformationParameterLink getIntegrationTransformationParameterLink(int id);


	public List<IntegrationTransformationParameterLink> getIntegrationTransformationParameterLinkListByParent(int parentId);


	public List<IntegrationTransformationParameterLink> getIntegrationTransformationParameterLinkListByChild(int childId);


	public IntegrationTransformationParameterLink saveIntegrationTransformationParameterLink(int parentId, int childId);


	public IntegrationTransformationParameterLink saveIntegrationTransformationParameterEntryLink(IntegrationTransformationParameterLink link);


	public void deleteIntegrationTransformationParameterLink(int id);
}
