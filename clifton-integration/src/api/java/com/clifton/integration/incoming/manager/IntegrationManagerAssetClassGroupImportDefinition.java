package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;


/**
 * The <code>IntegrationManagerAssetClassGroupImportDefinition</code> class maps one or more import definitions
 * to a group of asset class mappings.
 *
 * @author vgomelsky
 */
public class IntegrationManagerAssetClassGroupImportDefinition extends ManyToManyEntity<IntegrationImportDefinition, IntegrationManagerAssetClassGroup, Integer> {

	// empty
}
