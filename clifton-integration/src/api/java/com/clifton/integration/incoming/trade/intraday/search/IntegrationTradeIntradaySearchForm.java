package com.clifton.integration.incoming.trade.intraday.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.integration.incoming.trade.intraday.IntegrationTradeIntradayStatuses;
import com.clifton.integration.incoming.trade.intraday.IntegrationTradeIntradayTypes;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationTradeIntradaySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id")
	private Integer runId;

	@SearchField
	private IntegrationTradeIntradayStatuses status;

	@SearchField
	private String accountNumber;

	@SearchField
	private Date tradeDate;

	@SearchField
	private String executingBrokerCompanyName;

	@SearchField
	private String clearingBrokerCompanyName;

	@SearchField
	private String investmentSecuritySymbol;

	@SearchField
	private String payingSecuritySymbol;

	@SearchField
	private String securitySymbolType;

	@SearchField
	private Boolean buy;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal price;

	@SearchField
	private IntegrationTradeIntradayTypes type;

	@SearchField
	private String uniqueTradeId;

	@SearchField(searchField = "name", searchFieldPath = "run.file.fileDefinition.source")
	private String fileSourceName;

	@SearchField(searchField = "sourceCompanyId", searchFieldPath = "run.file.fileDefinition.source")
	private Integer sourceCompanyId;

	// Custom Search Field
	private Boolean excludeOverriddenTrades;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public IntegrationTradeIntradayStatuses getStatus() {
		return this.status;
	}


	public void setStatus(IntegrationTradeIntradayStatuses status) {
		this.status = status;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public String getExecutingBrokerCompanyName() {
		return this.executingBrokerCompanyName;
	}


	public void setExecutingBrokerCompanyName(String executingBrokerCompanyName) {
		this.executingBrokerCompanyName = executingBrokerCompanyName;
	}


	public String getClearingBrokerCompanyName() {
		return this.clearingBrokerCompanyName;
	}


	public void setClearingBrokerCompanyName(String clearingBrokerCompanyName) {
		this.clearingBrokerCompanyName = clearingBrokerCompanyName;
	}


	public String getInvestmentSecuritySymbol() {
		return this.investmentSecuritySymbol;
	}


	public void setInvestmentSecuritySymbol(String investmentSecuritySymbol) {
		this.investmentSecuritySymbol = investmentSecuritySymbol;
	}


	public String getPayingSecuritySymbol() {
		return this.payingSecuritySymbol;
	}


	public void setPayingSecuritySymbol(String payingSecuritySymbol) {
		this.payingSecuritySymbol = payingSecuritySymbol;
	}


	public String getSecuritySymbolType() {
		return this.securitySymbolType;
	}


	public void setSecuritySymbolType(String securitySymbolType) {
		this.securitySymbolType = securitySymbolType;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public IntegrationTradeIntradayTypes getType() {
		return this.type;
	}


	public void setType(IntegrationTradeIntradayTypes type) {
		this.type = type;
	}


	public String getUniqueTradeId() {
		return this.uniqueTradeId;
	}


	public void setUniqueTradeId(String uniqueTradeId) {
		this.uniqueTradeId = uniqueTradeId;
	}


	public String getFileSourceName() {
		return this.fileSourceName;
	}


	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}


	public Integer getSourceCompanyId() {
		return this.sourceCompanyId;
	}


	public void setSourceCompanyId(Integer sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}


	public Boolean getExcludeOverriddenTrades() {
		return this.excludeOverriddenTrades;
	}


	public void setExcludeOverriddenTrades(Boolean excludeOverriddenTrades) {
		this.excludeOverriddenTrades = excludeOverriddenTrades;
	}
}
