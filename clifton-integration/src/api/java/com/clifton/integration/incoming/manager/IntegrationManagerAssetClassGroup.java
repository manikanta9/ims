package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>IntegrationManagerAssetClassGroup</code> class represents a single grouping (usually a bank)
 * that has the same assetClass mappings but may consist of multiple downloads.
 *
 * @author vgomelsky
 */
public class IntegrationManagerAssetClassGroup extends NamedEntity<Integer> {

	/**
	 * The type for all unmapped asset classes.
	 */
	private IntegrationManagerPositionType unmappedPositionType;

	/**
	 * Fail import if the are unmapped Asset Classes.
	 */
	private boolean requireAllAssetClassMappings = true;

	/**
	 * Reference to a Business Company's primary key value.
	 * UI restricted to types: Custodian, Investment Manager, Broker, and Other Issuer.
	 * This usually references an IMS Business Company record for the custodian bank.
	 */
	private Integer businessCompanyId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public IntegrationManagerPositionType getUnmappedPositionType() {
		return this.unmappedPositionType;
	}


	public void setUnmappedPositionType(IntegrationManagerPositionType unmappedPositionType) {
		this.unmappedPositionType = unmappedPositionType;
	}


	public boolean isRequireAllAssetClassMappings() {
		return this.requireAllAssetClassMappings;
	}


	public void setRequireAllAssetClassMappings(boolean requireAllAssetClassMappings) {
		this.requireAllAssetClassMappings = requireAllAssetClassMappings;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}
}
