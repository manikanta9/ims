package com.clifton.integration.incoming.definition;


/**
 * The <code>MappingException</code> class represents a data mapping exception that should result
 * in ImportStatus.MAPPING_ERROR as opposed to ImportStatus.ERROR run status.
 *
 * @author vgomelsky
 */
public class MappingException extends RuntimeException {

	public MappingException(String message) {
		super(message);
	}
}
