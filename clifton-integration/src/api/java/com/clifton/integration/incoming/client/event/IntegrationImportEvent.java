package com.clifton.integration.incoming.client.event;

import com.clifton.core.util.event.EventObject;
import com.clifton.integration.incoming.definition.IntegrationImportRun;


/**
 * Represents and integration import event.  Used to consolidate the BaseEventListener<Event<IntegrationImportRun, Object>> to
 * BaseEventListener<IntegrationImportEvent>.
 *
 * @author mwacker
 */
public class IntegrationImportEvent extends EventObject<IntegrationImportRun, Object> {

	public static IntegrationImportEvent ofEventTarget(String eventName, IntegrationImportRun run) {
		IntegrationImportEvent event = new IntegrationImportEvent();
		event.setEventName(eventName);
		event.setTarget(run);
		return event;
	}
}
