package com.clifton.integration.incoming.definition;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionTypeColumnSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionTypeSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunEventSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportStatusSearchForm;

import java.util.List;


public interface IntegrationImportService {

	////////////////////////////////////////////////////////////////////////////
	////////    IntegrationImportDefinition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportDefinition getIntegrationImportDefinition(int id);


	public IntegrationImportDefinition getIntegrationImportDefinitionByName(String name);


	public IntegrationImportDefinition getIntegrationImportDefinitionByFileName(String inputFileName);


	public List<IntegrationImportDefinition> getIntegrationImportDefinitionList(IntegrationImportDefinitionSearchForm searchForm);


	public List<IntegrationImportDefinition> getIntegrationImportDefinitionListByFileDefinition(int fileDefinitionId);


	public IntegrationImportDefinition saveIntegrationImportDefinition(IntegrationImportDefinition bean);


	public void deleteIntegrationImportDefinition(int id);

	////////////////////////////////////////////////////////////////////////////
	///   IntegrationImportDefinitionTypeColumn History Business Methods   /////
	////////////////////////////////////////////////////////////////////////////


	public List<IntegrationImportDefinitionTypeColumn> getIntegrationImportDefinitionTypeColumnList(IntegrationImportDefinitionTypeColumnSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////        IntegrationImportRun Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getIntegrationImportRun(int id);


	public List<IntegrationImportRun> getIntegrationImportRunList(IntegrationImportRunSearchForm searchForm);


	@DoNotAddRequestMapping
	public IntegrationImportRun saveIntegrationImportRun(IntegrationImportRun importRun);


	@DoNotAddRequestMapping(allowAPI = true)
	public void deleteIntegrationImportRun(int id);


	public void fixIntegrationImportRun(Integer[] runIds);


	////////////////////////////////////////////////////////////////////////////
	////////      IntegrationImportStatus Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportStatus getIntegrationImportStatus(short id);


	public IntegrationImportStatus getIntegrationImportStatusByName(String name);


	public IntegrationImportStatus getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes statusType);


	public List<IntegrationImportStatus> getIntegrationImportStatusList(IntegrationImportStatusSearchForm searchForm);


	public IntegrationImportStatus saveIntegrationImportStatus(IntegrationImportStatus importStatus);


	public void deleteIntegrationImportStatus(short id);


	////////////////////////////////////////////////////////////////////////////
	////////     IntegrationImportRunEvent Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRunEvent getIntegrationImportRunEvent(long id);


	public List<IntegrationImportRunEvent> getIntegrationImportRunEventList(IntegrationImportRunEventSearchForm searchForm);


	public IntegrationImportRunEvent saveIntegrationImportRunEvent(IntegrationImportRunEvent runEvent);


	public void deleteIntegrationImportRunEvent(long id);


	public void fixIntegrationImportRunEvent(Long[] runEventIds);


	public IntegrationImportRunEvent updateIntegrationImportRunEvent(long runEventId, IntegrationImportStatusTypes statusType, String error);


	//////////////////////////////////////////////////////////////////////////////
	////////    IntegrationImportDefinitionType Business Methods     /////////////
	//////////////////////////////////////////////////////////////////////////////


	public IntegrationImportDefinitionType getIntegrationImportDefinitionType(int id);


	public List<IntegrationImportDefinitionType> getIntegrationImportDefinitionTypeList(IntegrationImportDefinitionTypeSearchForm searchForm);


	public IntegrationImportDefinitionType saveIntegrationImportDefinitionType(IntegrationImportDefinitionType bean);
}
