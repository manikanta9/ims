package com.clifton.integration.incoming.trade.intraday;


public enum IntegrationTradeIntradayStatuses {
	PENDING_BROKER_ACCEPTANCE, CONFIRMED, UNCONFIRMED
}
