package com.clifton.integration.incoming.trade.intraday;


import com.clifton.integration.incoming.trade.intraday.search.IntegrationTradeIntradaySearchForm;

import java.util.List;


public interface IntegrationTradeIntradayService {

	public List<IntegrationTradeIntraday> getIntegrationTradeIntradayList(IntegrationTradeIntradaySearchForm searchForm);
}
