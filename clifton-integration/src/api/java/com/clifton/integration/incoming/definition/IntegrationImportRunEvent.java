package com.clifton.integration.incoming.definition;


import com.clifton.core.beans.BaseEntity;
import com.clifton.integration.target.IntegrationTargetApplication;


/**
 * The IntegrationImportRunEvent class defines an event that is raised/sent to each Target Application after the Import Run was processed.
 * Target Application(s) are defined on Run's Definition Type. A single import file may need to be consumed differently by more than 1 application.
 */
public class IntegrationImportRunEvent extends BaseEntity<Long> {

	private IntegrationImportRun importRun;
	private IntegrationImportStatus status;
	private IntegrationTargetApplication targetApplication;

	private String error;

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getImportRun() {
		return this.importRun;
	}


	public void setImportRun(IntegrationImportRun importRun) {
		this.importRun = importRun;
	}


	public IntegrationTargetApplication getTargetApplication() {
		return this.targetApplication;
	}


	public void setTargetApplication(IntegrationTargetApplication targetApplication) {
		this.targetApplication = targetApplication;
	}


	public IntegrationImportStatus getStatus() {
		return this.status;
	}


	public void setStatus(IntegrationImportStatus status) {
		this.status = status;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}
}
