package com.clifton.integration.incoming.manager;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>IntegrationManagerTransaction</code> is the raw data received from a manager
 * about cash transactions / moves
 */
public class IntegrationManagerTransaction extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	/**
	 * Populated by Pentaho transformations from raw data
	 */
	private IntegrationManagerBank managerBank;
	private Date transactionDate;
	private BigDecimal transactionAmount;
	private String transactionDescription;


	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public IntegrationManagerBank getManagerBank() {
		return this.managerBank;
	}


	public void setManagerBank(IntegrationManagerBank managerBank) {
		this.managerBank = managerBank;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public String getTransactionDescription() {
		return this.transactionDescription;
	}


	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}
}
