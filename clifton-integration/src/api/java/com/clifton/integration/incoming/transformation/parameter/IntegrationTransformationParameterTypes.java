package com.clifton.integration.incoming.transformation.parameter;

/**
 * @author TerryS
 */
public enum IntegrationTransformationParameterTypes {
	STRING("String", "Nonformatted string value."),
	SECRET("Secret", "Encrypted secret value."),
	DATA_SOURCE("Data Source", "Connection parameters for an existing data source.");

	private final String name;
	private final String description;


	IntegrationTransformationParameterTypes(String name, String description) {
		this.name = name;
		this.description = description;
	}


	public String getName() {
		return this.name;
	}


	public String getDescription() {
		return this.description;
	}
}
