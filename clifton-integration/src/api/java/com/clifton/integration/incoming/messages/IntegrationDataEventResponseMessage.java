package com.clifton.integration.incoming.messages;


import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;


@MessageType(MessageTypes.XML)
public class IntegrationDataEventResponseMessage extends AbstractSynchronousResponseMessage {

	//
}
