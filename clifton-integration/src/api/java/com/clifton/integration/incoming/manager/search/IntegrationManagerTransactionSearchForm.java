package com.clifton.integration.incoming.manager.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationManagerTransactionSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id")
	private Integer runId;

	@SearchField(searchFieldPath = "managerBank", searchField = "bankCode")
	private String managerAccountNumber;

	@SearchField
	private Date transactionDate;

	@SearchField
	private BigDecimal transactionAmount;

	@SearchField
	private String transactionDescription;

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public String getManagerAccountNumber() {
		return this.managerAccountNumber;
	}


	public void setManagerAccountNumber(String managerAccountNumber) {
		this.managerAccountNumber = managerAccountNumber;
	}


	public Date getTransactionDate() {
		return this.transactionDate;
	}


	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}


	public BigDecimal getTransactionAmount() {
		return this.transactionAmount;
	}


	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}


	public String getTransactionDescription() {
		return this.transactionDescription;
	}


	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}
}
