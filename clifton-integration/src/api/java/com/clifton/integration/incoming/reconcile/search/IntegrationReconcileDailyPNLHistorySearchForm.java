package com.clifton.integration.incoming.reconcile.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcileDailyPNLHistorySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id", required = true)
	private Integer runId;

	@SearchField
	private String accountNumber;

	@SearchField
	private String accountName;

	@SearchField
	private String clientAccountNumber;

	@SearchField
	private Date positionDate;

	@SearchField
	private String securitySymbol;

	@SearchField
	private String securitySymbolType;

	@SearchField
	private String positionCurrencySymbol;

	@SearchField
	private String accountCurrencySymbol;

	@SearchField
	private BigDecimal fxRate;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal priorQuantity;

	@SearchField
	private BigDecimal todayClosedQuantity;

	@SearchField
	private Boolean shortPosition;

	@SearchField
	private BigDecimal todayCommissionLocal;

	@SearchField
	private BigDecimal todayCommissionBase;

	@SearchField
	private BigDecimal todayRealizedGainLossLocal;

	@SearchField
	private BigDecimal todayRealizedGainLossBase;

	@SearchField
	private BigDecimal openTradeEquityLocal;

	@SearchField
	private BigDecimal openTradeEquityBase;

	@SearchField
	private BigDecimal priorOpenTradeEquityLocal;

	@SearchField
	private BigDecimal priorOpenTradeEquityBase;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public String getAccountName() {
		return this.accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecuritySymbolType() {
		return this.securitySymbolType;
	}


	public void setSecuritySymbolType(String securitySymbolType) {
		this.securitySymbolType = securitySymbolType;
	}


	public String getPositionCurrencySymbol() {
		return this.positionCurrencySymbol;
	}


	public void setPositionCurrencySymbol(String positionCurrencySymbol) {
		this.positionCurrencySymbol = positionCurrencySymbol;
	}


	public String getAccountCurrencySymbol() {
		return this.accountCurrencySymbol;
	}


	public void setAccountCurrencySymbol(String accountCurrencySymbol) {
		this.accountCurrencySymbol = accountCurrencySymbol;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPriorQuantity() {
		return this.priorQuantity;
	}


	public void setPriorQuantity(BigDecimal priorQuantity) {
		this.priorQuantity = priorQuantity;
	}


	public BigDecimal getTodayClosedQuantity() {
		return this.todayClosedQuantity;
	}


	public void setTodayClosedQuantity(BigDecimal todayClosedQuantity) {
		this.todayClosedQuantity = todayClosedQuantity;
	}


	public Boolean getShortPosition() {
		return this.shortPosition;
	}


	public void setShortPosition(Boolean shortPosition) {
		this.shortPosition = shortPosition;
	}


	public BigDecimal getTodayCommissionLocal() {
		return this.todayCommissionLocal;
	}


	public void setTodayCommissionLocal(BigDecimal todayCommissionLocal) {
		this.todayCommissionLocal = todayCommissionLocal;
	}


	public BigDecimal getTodayCommissionBase() {
		return this.todayCommissionBase;
	}


	public void setTodayCommissionBase(BigDecimal todayCommissionBase) {
		this.todayCommissionBase = todayCommissionBase;
	}


	public BigDecimal getTodayRealizedGainLossLocal() {
		return this.todayRealizedGainLossLocal;
	}


	public void setTodayRealizedGainLossLocal(BigDecimal todayRealizedGainLossLocal) {
		this.todayRealizedGainLossLocal = todayRealizedGainLossLocal;
	}


	public BigDecimal getTodayRealizedGainLossBase() {
		return this.todayRealizedGainLossBase;
	}


	public void setTodayRealizedGainLossBase(BigDecimal todayRealizedGainLossBase) {
		this.todayRealizedGainLossBase = todayRealizedGainLossBase;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getPriorOpenTradeEquityLocal() {
		return this.priorOpenTradeEquityLocal;
	}


	public void setPriorOpenTradeEquityLocal(BigDecimal priorOpenTradeEquityLocal) {
		this.priorOpenTradeEquityLocal = priorOpenTradeEquityLocal;
	}


	public BigDecimal getPriorOpenTradeEquityBase() {
		return this.priorOpenTradeEquityBase;
	}


	public void setPriorOpenTradeEquityBase(BigDecimal priorOpenTradeEquityBase) {
		this.priorOpenTradeEquityBase = priorOpenTradeEquityBase;
	}
}
