package com.clifton.integration.incoming.trade.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationTradeSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id")
	private Integer runId;

	@SearchField
	private String holdingAccountNumber;

	@SearchField
	private String clientAccountNumber;

	@SearchField
	private String executingBrokerCompanyName;


	@SearchField
	private String securitySymbol;

	@SearchField
	private String securityDescription;

	@SearchField
	private String cusip;

	@SearchField
	private String payingSecuritySymbol;


	@SearchField
	private Boolean buy;

	@SearchField
	private Date tradeDate;

	@SearchField
	private Date settlementDate;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal price;


	@SearchField
	private BigDecimal openingPrice;

	@SearchField
	private BigDecimal openingDate;


	@SearchField
	private BigDecimal tradeAmount;

	@SearchField
	private BigDecimal tradeAmountBase;

	@SearchField
	private Boolean currencyTrade;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}


	public String getHoldingAccountNumber() {
		return this.holdingAccountNumber;
	}


	public void setHoldingAccountNumber(String holdingAccountNumber) {
		this.holdingAccountNumber = holdingAccountNumber;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public String getExecutingBrokerCompanyName() {
		return this.executingBrokerCompanyName;
	}


	public void setExecutingBrokerCompanyName(String executingBrokerCompanyName) {
		this.executingBrokerCompanyName = executingBrokerCompanyName;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecurityDescription() {
		return this.securityDescription;
	}


	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public String getPayingSecuritySymbol() {
		return this.payingSecuritySymbol;
	}


	public void setPayingSecuritySymbol(String payingSecuritySymbol) {
		this.payingSecuritySymbol = payingSecuritySymbol;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getOpeningPrice() {
		return this.openingPrice;
	}


	public void setOpeningPrice(BigDecimal openingPrice) {
		this.openingPrice = openingPrice;
	}


	public BigDecimal getOpeningDate() {
		return this.openingDate;
	}


	public void setOpeningDate(BigDecimal openingDate) {
		this.openingDate = openingDate;
	}


	public BigDecimal getTradeAmount() {
		return this.tradeAmount;
	}


	public void setTradeAmount(BigDecimal tradeAmount) {
		this.tradeAmount = tradeAmount;
	}


	public BigDecimal getTradeAmountBase() {
		return this.tradeAmountBase;
	}


	public void setTradeAmountBase(BigDecimal tradeAmountBase) {
		this.tradeAmountBase = tradeAmountBase;
	}


	public Boolean getCurrencyTrade() {
		return this.currencyTrade;
	}


	public void setCurrencyTrade(Boolean currencyTrade) {
		this.currencyTrade = currencyTrade;
	}
}
