package com.clifton.integration.incoming.definition;


import com.clifton.core.beans.BaseEntity;


public class IntegrationImportDefinitionColumnMapping extends BaseEntity<Integer> {

	private String sourceColumnName;
	private IntegrationImportDefinitionTypeColumn destinationColumn;
	private IntegrationImportDefinition importDefinition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSourceColumnName() {
		return this.sourceColumnName;
	}


	public void setSourceColumnName(String sourceColumnName) {
		this.sourceColumnName = sourceColumnName;
	}


	public IntegrationImportDefinitionTypeColumn getDestinationColumn() {
		return this.destinationColumn;
	}


	public void setDestinationColumn(IntegrationImportDefinitionTypeColumn destinationColumn) {
		this.destinationColumn = destinationColumn;
	}


	public IntegrationImportDefinition getImportDefinition() {
		return this.importDefinition;
	}


	public void setImportDefinition(IntegrationImportDefinition importDefinition) {
		this.importDefinition = importDefinition;
	}
}
