package com.clifton.integration.incoming.corporate.action;


/**
 * <code>IntegrationCorporateActionPayoutSecurityTypes</code> represents the payment security identifier and type provided
 * by incoming integration data and populated on
 * {@link IntegrationCorporateAction#payoutSecurityTypeIdentifier}
 * {@link IntegrationCorporateAction#payoutSecurityIdentifier}.
 *
 * @author TerryS
 */
public enum IntegrationCorporateActionPayoutSecurityTypes {

	ISO_4217_CURRENCY_CODE(0, "ISO 4217 Currency Code"),
	CINS_CUSIP_CODE(2, "CINS/CUSIP Code"),
	SEDOL_CODE(3, "SEDOL Code"),
	ISIN_CODE(4, "ISIN Code"),
	FIGI_CODE(5, "FIGI"),
	TICKER_SYMBOL(6, "Ticker Symbol"),
	VALOREN_CODE(14, "Valoren Code");

	private final int code;
	private final String name;


	IntegrationCorporateActionPayoutSecurityTypes(int code, String name) {
		this.code = code;
		this.name = name;
	}


	public static IntegrationCorporateActionPayoutSecurityTypes resolveByCode(short payoutSecurityIdentifier) {
		for (IntegrationCorporateActionPayoutSecurityTypes type : IntegrationCorporateActionPayoutSecurityTypes.values()) {
			if (type.code == payoutSecurityIdentifier) {
				return type;
			}
		}
		return null;
	}


	public int getCode() {
		return this.code;
	}


	public String getName() {
		return this.name;
	}
}
