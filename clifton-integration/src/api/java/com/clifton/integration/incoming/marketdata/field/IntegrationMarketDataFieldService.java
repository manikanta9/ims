package com.clifton.integration.incoming.marketdata.field;


import com.clifton.integration.incoming.marketdata.field.search.IntegrationMarketDataFieldValueSearchForm;

import java.util.List;


public interface IntegrationMarketDataFieldService {

	public List<IntegrationMarketDataFieldValue> getIntegrationMarketDataFieldValueList(IntegrationMarketDataFieldValueSearchForm searchForm);
}
