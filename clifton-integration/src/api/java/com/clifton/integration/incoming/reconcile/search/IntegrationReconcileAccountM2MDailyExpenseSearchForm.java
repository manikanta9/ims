package com.clifton.integration.incoming.reconcile.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class IntegrationReconcileAccountM2MDailyExpenseSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "run.id", required = true)
	private Integer runId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public void setRunId(Integer runId) {
		this.runId = runId;
	}
}
