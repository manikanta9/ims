package com.clifton.integration.incoming.transformation.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.integration.incoming.transformation.IntegrationTransformationTypes;


/**
 * @author mwacker
 */
public class IntegrationTransformationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "name")
	private String name;

	@SearchField(searchField = "name", searchFieldPath = "definitionType")
	private String definitionTypeName;

	@SearchField(searchField = "definitionType.id")
	private Integer definitionTypeId;

	@SearchField
	private Boolean templateTransformation;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private IntegrationTransformationTypes type;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDefinitionTypeName() {
		return this.definitionTypeName;
	}


	public void setDefinitionTypeName(String definitionTypeName) {
		this.definitionTypeName = definitionTypeName;
	}


	public Integer getDefinitionTypeId() {
		return this.definitionTypeId;
	}


	public void setDefinitionTypeId(Integer definitionTypeId) {
		this.definitionTypeId = definitionTypeId;
	}


	public Boolean getTemplateTransformation() {
		return this.templateTransformation;
	}


	public void setTemplateTransformation(Boolean templateTransformation) {
		this.templateTransformation = templateTransformation;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public IntegrationTransformationTypes getType() {
		return this.type;
	}


	public void setType(IntegrationTransformationTypes type) {
		this.type = type;
	}
}
