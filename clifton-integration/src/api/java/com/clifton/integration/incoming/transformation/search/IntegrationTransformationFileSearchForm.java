package com.clifton.integration.incoming.transformation.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFileTypes;


/**
 * @author mwacker
 */
public class IntegrationTransformationFileSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(leftJoin = true, searchField = "fileName,transformation.name")
	private String searchPattern;

	@SearchField
	private String fileName;

	@SearchField(searchField = "name", searchFieldPath = "transformation")
	private String transformationName;

	@SearchField(searchField = "transformation.id")
	private Integer transformationId;

	@SearchField
	private Integer order;

	@SearchField
	private Boolean runnable;

	@SearchField
	private IntegrationTransformationFileTypes fileType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public Integer getTransformationId() {
		return this.transformationId;
	}


	public void setTransformationId(Integer transformationId) {
		this.transformationId = transformationId;
	}


	public String getTransformationName() {
		return this.transformationName;
	}


	public void setTransformationName(String transformationName) {
		this.transformationName = transformationName;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public IntegrationTransformationFileTypes getFileType() {
		return this.fileType;
	}


	public void setFileType(IntegrationTransformationFileTypes fileType) {
		this.fileType = fileType;
	}


	public Boolean getRunnable() {
		return this.runnable;
	}


	public void setRunnable(Boolean runnable) {
		this.runnable = runnable;
	}
}
