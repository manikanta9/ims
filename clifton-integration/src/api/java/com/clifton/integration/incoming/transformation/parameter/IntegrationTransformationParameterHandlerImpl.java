package com.clifton.integration.incoming.transformation.parameter;

import com.clifton.core.dataaccess.datasource.EnhancedDataSource;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.security.secret.SecuritySecretService;
import com.clifton.system.schema.SystemDataSource;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
@Component
public class IntegrationTransformationParameterHandlerImpl implements IntegrationTransformationParameterHandler, ApplicationContextAware {

	private ApplicationContext applicationContext;
	private IntegrationTransformationParameterService integrationTransformationParameterService;
	private SecuritySecretService securitySecretService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<String, String> getIntegrationTransformationParameterMap(IntegrationTransformation integrationTransformation) {
		AssertUtils.assertNotNull(integrationTransformation, "Integration Transformation is required.");

		List<IntegrationTransformationParameterLink> parameterList = getIntegrationTransformationParameterService()
				.getIntegrationTransformationParameterLinkListByParent(integrationTransformation.getId());
		Map<String, String> parameterMap = CollectionUtils.getStream(parameterList)
				.map(IntegrationTransformationParameterLink::getReferenceTwo)
				.filter(p -> !p.isDataSource())
				.collect(Collectors.toMap(IntegrationTransformationParameter::getKey, v ->
						(v.isSecretParameterValue() && v.getSecuritySecretValue() != null)
								? getSecuritySecretService().decryptSecuritySecret(v.getSecuritySecretValue()).getSecretString() : v.getValue()
				));
		Map<String, String> results = new HashMap<>(parameterMap);
		Map<String, String> dataSourceParameters = getSystemDataSourceParameters(parameterList);
		results.putAll(dataSourceParameters);

		return results;
	}


	@Override
	public void validateIntegrationTransformationDataSourceParameterMap(IntegrationTransformation integrationTransformation) {
		AssertUtils.assertNotNull(integrationTransformation, "Integration Transformation is required.");

		List<IntegrationTransformationParameterLink> parameterList = getIntegrationTransformationParameterService()
				.getIntegrationTransformationParameterLinkListByParent(integrationTransformation.getId());
		getSystemDataSourceParameters(parameterList);
	}

	////////////////////////////////////////////////////////////////////////////////


	private Map<String, String> getSystemDataSourceParameters(List<IntegrationTransformationParameterLink> parameterList) {
		Map<String, String> results = new HashMap<>();
		for (IntegrationTransformationParameterLink link : parameterList) {
			IntegrationTransformationParameter parameter = link.getReferenceTwo();
			if (parameter.isDataSource()) {
				final String prefix = parameter.getKey();
				SystemDataSource dataSource = parameter.getSystemDataSource();
				// get the data source from the database, parsing the url is driver specific.
				ObjectUtils.doIfPresent(dataSource.getDatabaseName(), d -> results.put(prefix + "_DATABASE", d));
				Object obj = this.applicationContext.getBean(dataSource.getName());
				// an extension of the Tomcat data source implementation.
				if (obj instanceof EnhancedDataSource) {
					EnhancedDataSource enhancedDataSource = (EnhancedDataSource) obj;
					ObjectUtils.doIfPresent(enhancedDataSource.getPoolProperties().getUsername(), u -> results.put(prefix + "_USERNAME", u));
					ObjectUtils.doIfPresent(enhancedDataSource.getPoolProperties().getPassword(), p -> results.put(prefix + "_PASSWORD", p));
					populateBasicUrlValues(enhancedDataSource.getUrl(), prefix, results);
				}
				validate(prefix, results);
			}
		}
		return results;
	}


	private void validate(String prefix, Map<String, String> results) {
		ValidationUtils.assertTrue(results.containsKey(prefix + "_USERNAME"), "Data source value not found " + prefix + "_USERNAME");
		ValidationUtils.assertTrue(results.containsKey(prefix + "_PASSWORD"), "Data source value not found " + prefix + "_PASSWORD");
		ValidationUtils.assertTrue(results.containsKey(prefix + "_PORT"), "Data source value not found " + prefix + "_PORT");
		ValidationUtils.assertTrue(results.containsKey(prefix + "_HOSTNAME"), "Data source value not found " + prefix + "_HOSTNAME");
		ValidationUtils.assertTrue(results.containsKey(prefix + "_DATABASE"), "Data source value not found " + prefix + "_DATABASE");
	}


	private void populateBasicUrlValues(String connectionUrl, String prefix, Map<String, String> results) {
		if (!StringUtils.isEmpty(connectionUrl)) {
			try {
				// can only have one scheme
				if (connectionUrl.contains("://")) {
					String tmp = connectionUrl.substring(0, connectionUrl.indexOf("://"));
					int index = tmp.lastIndexOf(':');
					if (index > 0) {
						connectionUrl = connectionUrl.substring(index + 1);
					}
				}
				// remove the 'options' portion of the url.
				int index = connectionUrl.indexOf(';');
				if (index > 0) {
					connectionUrl = connectionUrl.substring(0, index);
				}
				URI uri = URI.create(connectionUrl);
				int port = uri.getPort();
				if (port > 0) {
					results.put(prefix + "_PORT", Integer.toString(port));
				}
				String hostName = uri.getHost();
				if (!StringUtils.isEmpty(hostName)) {
					results.put(prefix + "_HOSTNAME", hostName);
				}
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Could not parse connection url " + connectionUrl, e);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	public IntegrationTransformationParameterService getIntegrationTransformationParameterService() {
		return this.integrationTransformationParameterService;
	}


	public void setIntegrationTransformationParameterService(IntegrationTransformationParameterService integrationTransformationParameterService) {
		this.integrationTransformationParameterService = integrationTransformationParameterService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
