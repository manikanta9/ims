package com.clifton.integration.incoming.reconcile;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.reconcile.m2m.IntegrationReconcileM2MExpenses;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcileAccountM2MDailyExpense extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;
	private String accountNumber;
	private IntegrationReconcileM2MExpenses expenseType;
	private String currencySymbol;
	private BigDecimal expenseAmount;
	private Date positionDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	/**
	 * @return the {@link #expenseType}
	 */

	public IntegrationReconcileM2MExpenses getExpenseType() {
		return this.expenseType;
	}


	/**
	 * Sets {@link #expenseType}.
	 *
	 * @param expenseType the expenseType to set
	 */

	public void setExpenseType(IntegrationReconcileM2MExpenses expenseType) {
		this.expenseType = expenseType;
	}


	public String getCurrencySymbol() {
		return this.currencySymbol;
	}


	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getExpenseAmount() {
		return this.expenseAmount;
	}


	public void setExpenseAmount(BigDecimal expenseAmount) {
		this.expenseAmount = expenseAmount;
	}
}
