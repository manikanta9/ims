package com.clifton.integration.incoming.definition;


public enum TransformationDataTypes {
	STRING("String"), //
	NUMBER("Number"), //
	BIG_NUMBER("BigNumber"), //
	DATE("Date"), //
	BOOLEAN("Boolean");

	private final String name;


	TransformationDataTypes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}
}
