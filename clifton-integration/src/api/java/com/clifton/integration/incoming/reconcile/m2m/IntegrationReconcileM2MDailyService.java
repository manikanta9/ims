package com.clifton.integration.incoming.reconcile.m2m;


import java.util.List;


public interface IntegrationReconcileM2MDailyService {

	public List<IntegrationReconcileM2MDailyImport> getIntegrationReconcileM2MDailyImportListByRun(int runId);
}
