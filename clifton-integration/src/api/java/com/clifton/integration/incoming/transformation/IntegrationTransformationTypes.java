package com.clifton.integration.incoming.transformation;

/**
 * @author mwacker
 */
public enum IntegrationTransformationTypes {

	DATA_TRANSFORMATION(null, false), FILE_TRANSFORMATION(IntegrationTransformationFileTypes.FILE_TO_FILE_TRANSFORMATION, true);


	private final IntegrationTransformationFileTypes requiredTransformationFileType;
	private final boolean fileReturned;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	IntegrationTransformationTypes(IntegrationTransformationFileTypes requiredTransformationFileType, boolean fileReturned) {
		this.requiredTransformationFileType = requiredTransformationFileType;
		this.fileReturned = fileReturned;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationTransformationFileTypes getRequiredTransformationFileType() {
		return this.requiredTransformationFileType;
	}


	public boolean isFileReturned() {
		return this.fileReturned;
	}
}
