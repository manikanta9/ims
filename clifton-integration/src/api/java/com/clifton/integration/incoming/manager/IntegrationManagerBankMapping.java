package com.clifton.integration.incoming.manager;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;


/**
 * The <code>IntegrationManagerBankMapping</code> allows mapping a multiple bank codes to a single account.
 *
 * @author mwacker
 */
public class IntegrationManagerBankMapping extends BaseEntity<Integer> implements LabeledObject {

	private IntegrationManagerAssetClassGroup group;

	private String fromBankCode;
	private String toBankCode;

	private String assetClass;
	private String securityDescriptionPattern;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAssetClass() {
		return this.assetClass;
	}


	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}


	public String getSecurityDescriptionPattern() {
		return this.securityDescriptionPattern;
	}


	public void setSecurityDescriptionPattern(String securityDescriptionPattern) {
		this.securityDescriptionPattern = securityDescriptionPattern;
	}


	@Override
	public String getLabel() {
		return getFromBankCode() + "->" + getToBankCode();
	}


	public IntegrationManagerAssetClassGroup getGroup() {
		return this.group;
	}


	public void setGroup(IntegrationManagerAssetClassGroup group) {
		this.group = group;
	}


	public String getFromBankCode() {
		return this.fromBankCode;
	}


	public void setFromBankCode(String fromBankCode) {
		this.fromBankCode = fromBankCode;
	}


	public String getToBankCode() {
		return this.toBankCode;
	}


	public void setToBankCode(String toBankCode) {
		this.toBankCode = toBankCode;
	}
}
