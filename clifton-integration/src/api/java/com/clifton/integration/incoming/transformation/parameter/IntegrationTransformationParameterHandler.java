package com.clifton.integration.incoming.transformation.parameter;

import com.clifton.integration.incoming.transformation.IntegrationTransformation;

import java.util.Map;


/**
 * @author TerryS
 */
public interface IntegrationTransformationParameterHandler {

	public Map<String, String> getIntegrationTransformationParameterMap(IntegrationTransformation integrationTransformation);


	public void validateIntegrationTransformationDataSourceParameterMap(IntegrationTransformation integrationTransformation);
}
