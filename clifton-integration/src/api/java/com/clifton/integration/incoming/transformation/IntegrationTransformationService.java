package com.clifton.integration.incoming.transformation;

import com.clifton.integration.incoming.transformation.search.IntegrationTransformationFileSearchForm;
import com.clifton.integration.incoming.transformation.search.IntegrationTransformationSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * @author mwacker
 */
public interface IntegrationTransformationService {

	public IntegrationTransformation getIntegrationTransformation(int id);


	public List<IntegrationTransformation> getIntegrationTransformationList(IntegrationTransformationSearchForm searchForm);


	public void deleteIntegrationTransformation(int id);


	public IntegrationTransformation saveIntegrationTransformation(IntegrationTransformation bean);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationTransformationFile getIntegrationTransformationFile(int id);


	public List<IntegrationTransformationFile> getIntegrationTransformationFileListForTransformation(int transformationId);


	public List<IntegrationTransformationFile> getIntegrationTransformationFileList(IntegrationTransformationFileSearchForm searchForm);


	@RequestMapping("integrationTransformationFileDeploy")
	public void deployIntegrationTransformationFile(int id);


	public void deleteIntegrationTransformationFile(int id);


	public IntegrationTransformationFile saveIntegrationTransformationFile(IntegrationTransformationFile bean);
}
