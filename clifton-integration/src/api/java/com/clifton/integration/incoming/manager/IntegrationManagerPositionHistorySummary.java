package com.clifton.integration.incoming.manager;


import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>IntegrationManagerPositionHistorySummary</code> class is a summary class that is not
 * directly tied to a database table.  It is populated using GROUP BY and will have the
 * SUM of securities and cash balances per manager per day.
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class IntegrationManagerPositionHistorySummary {

	private String bankCode;

	private BigDecimal cashValue;
	private BigDecimal securityValue;
	private Date positionDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBankCode() {
		return this.bankCode;
	}


	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}


	public BigDecimal getCashValue() {
		return this.cashValue;
	}


	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}


	public BigDecimal getSecurityValue() {
		return this.securityValue;
	}


	public void setSecurityValue(BigDecimal securityValue) {
		this.securityValue = securityValue;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}
}
