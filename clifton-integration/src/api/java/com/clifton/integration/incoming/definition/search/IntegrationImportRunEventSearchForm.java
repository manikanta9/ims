package com.clifton.integration.incoming.definition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


@SearchForm
public class IntegrationImportRunEventSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "importRun.integrationImportDefinition.name,importRun.integrationImportDefinition.description")
	private String searchPattern;

	@SearchField(searchField = "status.id")
	private Short statusId;

	@SearchField(searchField = "status.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short statusIdNotEquals;

	@SearchField(searchFieldPath = "status", searchField = "name")
	private String statusName;

	@SearchField(searchFieldPath = "status", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String statusNameNotEquals;

	@SearchField(searchFieldPath = "status", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] statusNames;

	@SearchField(searchFieldPath = "importRun", searchField = "effectiveDate", dateFieldIncludesTime = true)
	private Date effectiveDate;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS, searchFieldPath = "importRun", searchField = "effectiveDate")
	private Date effectiveDateBefore;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS, searchFieldPath = "importRun.file", searchField = "receivedDate")
	private Date receivedDateBefore;

	@SearchField(searchFieldPath = "importRun", searchField = "startProcessDate", dateFieldIncludesTime = true)
	private Date startProcessDate;

	@SearchField(searchFieldPath = "importRun", searchField = "endProcessDate", dateFieldIncludesTime = true)
	private Date endProcessDate;

	@SearchField(comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS, searchFieldPath = "importRun", searchField = "endProcessDate")
	private Date endProcessDateAfter;

	@SearchField(searchField = "importRun.id")
	private Integer importRunId;

	@SearchField(searchField = "name", searchFieldPath = "importRun.integrationImportDefinition")
	private String importDefinitionName;

	@SearchField(searchField = "fileName", searchFieldPath = "importRun.file.fileDefinition")
	private String processedFileName;

	@SearchField(searchField = "testDefinition", searchFieldPath = "importRun.integrationImportDefinition")
	private Boolean testDefinition;

	@SearchField(searchField = "eventName", searchFieldPath = "importRun.integrationImportDefinition.type")
	private String importEventName;

	@SearchField(searchField = "eventName", searchFieldPath = "importRun.integrationImportDefinition.type", comparisonConditions = ComparisonConditions.EQUALS)
	private String importEventNameEquals;

	@SearchField(searchFieldPath = "importRun", searchField = "integrationImportDefinition.id", comparisonConditions = {ComparisonConditions.IN})
	private Integer[] definitionIds;

	@SearchField(searchFieldPath = "importRun", searchField = "integrationImportDefinition.id")
	private Integer integrationImportDefinitionId;

	@SearchField(searchField = "type.id", searchFieldPath = "importRun.integrationImportDefinition")
	private Integer typeId;

	@SearchField(searchField = "type.id", searchFieldPath = "importRun.integrationImportDefinition")
	private Integer[] typeIds;

	@SearchField(searchField = "name", searchFieldPath = "importRun.integrationImportDefinition.type")
	private String typeName;

	@SearchField(searchField = "businessCompanyId", searchFieldPath = "importRun.integrationImportDefinition.assetClassGroupList")
	private Integer businessCompanyId;

	@SearchField(searchField = "targetApplication.id")
	private Short targetApplicationId;

	@SearchField(searchField = "name", searchFieldPath = "targetApplication", comparisonConditions = ComparisonConditions.EQUALS)
	private String targetApplicationName;

	@SearchField(searchField = "name", searchFieldPath = "targetApplication", comparisonConditions = ComparisonConditions.IN)
	private String[] targetApplicationNames;

	@SearchField(searchFieldPath = "importRun", searchField = "file.id")
	private Integer fileId;

	@SearchField(searchField = "fileDefinition.id", searchFieldPath = "importRun.integrationImportDefinition")
	private Integer fileDefinitionId;

	@SearchField(searchField = "fileName", searchFieldPath = "importRun.integrationImportDefinition.fileDefinition")
	private String fileDefinitionFileName;

	@SearchField(searchField = "fileName", searchFieldPath = "importRun.integrationImportDefinition.fileDefinition")
	private String[] fileDefinitionFileNames;

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getStatusId() {
		return this.statusId;
	}


	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}


	public Short getStatusIdNotEquals() {
		return this.statusIdNotEquals;
	}


	public void setStatusIdNotEquals(Short statusIdNotEquals) {
		this.statusIdNotEquals = statusIdNotEquals;
	}


	public String getStatusName() {
		return this.statusName;
	}


	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}


	public String getStatusNameNotEquals() {
		return this.statusNameNotEquals;
	}


	public void setStatusNameNotEquals(String statusNameNotEquals) {
		this.statusNameNotEquals = statusNameNotEquals;
	}


	public String[] getStatusNames() {
		return this.statusNames;
	}


	public void setStatusNames(String[] statusNames) {
		this.statusNames = statusNames;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getEffectiveDateBefore() {
		return this.effectiveDateBefore;
	}


	public void setEffectiveDateBefore(Date effectiveDateBefore) {
		this.effectiveDateBefore = effectiveDateBefore;
	}


	public Date getReceivedDateBefore() {
		return this.receivedDateBefore;
	}


	public void setReceivedDateBefore(Date receivedDateBefore) {
		this.receivedDateBefore = receivedDateBefore;
	}


	public Date getStartProcessDate() {
		return this.startProcessDate;
	}


	public void setStartProcessDate(Date startProcessDate) {
		this.startProcessDate = startProcessDate;
	}


	public Date getEndProcessDate() {
		return this.endProcessDate;
	}


	public void setEndProcessDate(Date endProcessDate) {
		this.endProcessDate = endProcessDate;
	}


	public Date getEndProcessDateAfter() {
		return this.endProcessDateAfter;
	}


	public void setEndProcessDateAfter(Date endProcessDateAfter) {
		this.endProcessDateAfter = endProcessDateAfter;
	}


	public Integer getImportRunId() {
		return this.importRunId;
	}


	public void setImportRunId(Integer importRunId) {
		this.importRunId = importRunId;
	}


	public String getImportDefinitionName() {
		return this.importDefinitionName;
	}


	public void setImportDefinitionName(String importDefinitionName) {
		this.importDefinitionName = importDefinitionName;
	}


	public String getProcessedFileName() {
		return this.processedFileName;
	}


	public void setProcessedFileName(String processedFileName) {
		this.processedFileName = processedFileName;
	}


	public Boolean getTestDefinition() {
		return this.testDefinition;
	}


	public void setTestDefinition(Boolean testDefinition) {
		this.testDefinition = testDefinition;
	}


	public String getImportEventName() {
		return this.importEventName;
	}


	public void setImportEventName(String importEventName) {
		this.importEventName = importEventName;
	}


	public String getImportEventNameEquals() {
		return this.importEventNameEquals;
	}


	public void setImportEventNameEquals(String importEventNameEquals) {
		this.importEventNameEquals = importEventNameEquals;
	}


	public Integer[] getDefinitionIds() {
		return this.definitionIds;
	}


	public void setDefinitionIds(Integer[] definitionIds) {
		this.definitionIds = definitionIds;
	}


	public Integer getIntegrationImportDefinitionId() {
		return this.integrationImportDefinitionId;
	}


	public void setIntegrationImportDefinitionId(Integer integrationImportDefinitionId) {
		this.integrationImportDefinitionId = integrationImportDefinitionId;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public Integer[] getTypeIds() {
		return this.typeIds;
	}


	public void setTypeIds(Integer[] typeIds) {
		this.typeIds = typeIds;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Short getTargetApplicationId() {
		return this.targetApplicationId;
	}


	public void setTargetApplicationId(Short targetApplicationId) {
		this.targetApplicationId = targetApplicationId;
	}


	public String getTargetApplicationName() {
		return this.targetApplicationName;
	}


	public void setTargetApplicationName(String targetApplicationName) {
		this.targetApplicationName = targetApplicationName;
	}


	public String[] getTargetApplicationNames() {
		return this.targetApplicationNames;
	}


	public void setTargetApplicationNames(String[] targetApplicationNames) {
		this.targetApplicationNames = targetApplicationNames;
	}


	public Integer getFileId() {
		return this.fileId;
	}


	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}


	public String getFileDefinitionFileName() {
		return this.fileDefinitionFileName;
	}


	public void setFileDefinitionFileName(String fileDefinitionFileName) {
		this.fileDefinitionFileName = fileDefinitionFileName;
	}


	public String[] getFileDefinitionFileNames() {
		return this.fileDefinitionFileNames;
	}


	public void setFileDefinitionFileNames(String[] fileDefinitionFileNames) {
		this.fileDefinitionFileNames = fileDefinitionFileNames;
	}
}
