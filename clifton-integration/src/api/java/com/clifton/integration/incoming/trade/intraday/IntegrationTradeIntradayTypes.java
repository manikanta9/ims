package com.clifton.integration.incoming.trade.intraday;


public enum IntegrationTradeIntradayTypes {
	ADDED, CHANGED, DELETED
}
