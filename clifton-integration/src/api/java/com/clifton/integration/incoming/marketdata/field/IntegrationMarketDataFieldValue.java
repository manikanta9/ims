package com.clifton.integration.incoming.marketdata.field;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.date.Time;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.util.Date;


public class IntegrationMarketDataFieldValue extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	private String marketDataFieldName;
	private String investmentSecuritySymbol;
	private String investmentSecurityCusip;

	private Date measureDate;
	/**
	 * Applies only when dataField.timeSensitive = true
	 */
	private Time measureTime;
	private String measureValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public String getMarketDataFieldName() {
		return this.marketDataFieldName;
	}


	public void setMarketDataFieldName(String marketDataFieldName) {
		this.marketDataFieldName = marketDataFieldName;
	}


	public String getInvestmentSecuritySymbol() {
		return this.investmentSecuritySymbol;
	}


	public void setInvestmentSecuritySymbol(String investmentSecuritySymbol) {
		this.investmentSecuritySymbol = investmentSecuritySymbol;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Time getMeasureTime() {
		return this.measureTime;
	}


	public void setMeasureTime(Time measureTime) {
		this.measureTime = measureTime;
	}


	public String getMeasureValue() {
		return this.measureValue;
	}


	public void setMeasureValue(String measureValue) {
		this.measureValue = measureValue;
	}


	public String getInvestmentSecurityCusip() {
		return this.investmentSecurityCusip;
	}


	public void setInvestmentSecurityCusip(String investmentSecurityCusip) {
		this.investmentSecurityCusip = investmentSecurityCusip;
	}
}
