package com.clifton.integration.incoming.reconcile;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.integration.incoming.definition.IntegrationImportRun;

import java.math.BigDecimal;
import java.util.Date;


public class IntegrationReconcileAccountBalancesByAccountType extends BaseSimpleEntity<Integer> {

	private IntegrationImportRun run;

	private String accountNumber;
	private Date positionDate;
	private BigDecimal expectedTransferAmountLocal;
	private BigDecimal expectedTransferAmountBase;
	private String currencySymbolLocal;
	private String currencySymbolBase;
	private BigDecimal collateralRequirementLocal;
	private BigDecimal collateralRequirementBase;
	private BigDecimal collateralAmountLocal;
	private BigDecimal collateralAmountBase;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getExpectedTransferAmountLocal() {
		return this.expectedTransferAmountLocal;
	}


	public void setExpectedTransferAmountLocal(BigDecimal expectedTransferAmountLocal) {
		this.expectedTransferAmountLocal = expectedTransferAmountLocal;
	}


	public BigDecimal getExpectedTransferAmountBase() {
		return this.expectedTransferAmountBase;
	}


	public void setExpectedTransferAmountBase(BigDecimal expectedTransferAmountBase) {
		this.expectedTransferAmountBase = expectedTransferAmountBase;
	}


	public String getCurrencySymbolLocal() {
		return this.currencySymbolLocal;
	}


	public void setCurrencySymbolLocal(String currencySymbolLocal) {
		this.currencySymbolLocal = currencySymbolLocal;
	}


	public String getCurrencySymbolBase() {
		return this.currencySymbolBase;
	}


	public void setCurrencySymbolBase(String currencySymbolBase) {
		this.currencySymbolBase = currencySymbolBase;
	}


	public BigDecimal getCollateralRequirementLocal() {
		return this.collateralRequirementLocal;
	}


	public void setCollateralRequirementLocal(BigDecimal collateralRequirementLocal) {
		this.collateralRequirementLocal = collateralRequirementLocal;
	}


	public BigDecimal getCollateralRequirementBase() {
		return this.collateralRequirementBase;
	}


	public void setCollateralRequirementBase(BigDecimal collateralRequirementBase) {
		this.collateralRequirementBase = collateralRequirementBase;
	}


	public BigDecimal getCollateralAmountLocal() {
		return this.collateralAmountLocal;
	}


	public void setCollateralAmountLocal(BigDecimal collateralAmountLocal) {
		this.collateralAmountLocal = collateralAmountLocal;
	}


	public BigDecimal getCollateralAmountBase() {
		return this.collateralAmountBase;
	}


	public void setCollateralAmountBase(BigDecimal collateralAmountBase) {
		this.collateralAmountBase = collateralAmountBase;
	}
}
