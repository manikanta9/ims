package com.clifton.integration.incoming.messages;


import com.clifton.core.messaging.synchronous.AbstractSynchronousRequestMessage;


public class IntegrationRequestMessage extends AbstractSynchronousRequestMessage {

	private Integer integrationImportRunId;
	private Integer integrationFileId;
	private String importDefinitionTypeName;
	private String targetApplicationName;
	private boolean processAsynchronously;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getResponseClass() {
		return IntegrationResponseMessage.class;
	}


	public Integer getIntegrationImportRunId() {
		return this.integrationImportRunId;
	}


	public void setIntegrationImportRunId(Integer integrationImportRunId) {
		this.integrationImportRunId = integrationImportRunId;
	}


	public Integer getIntegrationFileId() {
		return this.integrationFileId;
	}


	public void setIntegrationFileId(Integer integrationFileId) {
		this.integrationFileId = integrationFileId;
	}


	public String getImportDefinitionTypeName() {
		return this.importDefinitionTypeName;
	}


	public void setImportDefinitionTypeName(String importDefinitionTypeName) {
		this.importDefinitionTypeName = importDefinitionTypeName;
	}


	public String getTargetApplicationName() {
		return this.targetApplicationName;
	}


	public void setTargetApplicationName(String targetApplicationName) {
		this.targetApplicationName = targetApplicationName;
	}


	public boolean isProcessAsynchronously() {
		return this.processAsynchronously;
	}


	public void setProcessAsynchronously(boolean processAsynchronously) {
		this.processAsynchronously = processAsynchronously;
	}
}
