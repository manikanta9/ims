package com.clifton.integration.incoming.definition.cache;

import com.clifton.core.cache.AbstractTimedEvictionCache;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatus;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <code>TradeOrderStatusNotificationHandler</code> handle sending notifications for trades that exceed a timeout period before receiving their execution report.
 */
@Component
public class IntegrationRunEventTimedEvictionCache extends AbstractTimedEvictionCache<IntegrationImportRunEvent> {

	private ContextHandler contextHandler;
	private IntegrationImportService integrationImportService;
	private SecurityUserService securityUserService;

	// Map is concurrent, event list is synchronized.
	// Only one thread puts a specific key to the map but two different threads mutate the same event list, the eviction thread depends on a volatile list size.
	private final Map<Integer, List<Long>> importRunRunEventIdMap = new ConcurrentHashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationRunEventTimedEvictionCache() {
		super(Duration.ofSeconds(10), Duration.ofMinutes(5));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void add(IntegrationImportRunEvent runEvent) {
		add(runEvent, getEvictionTimeout());
	}


	/**
	 * Creates an internal map of all runEvents created for a run so that during eviction the parent run
	 * status can be updated properly once all associated runEvents are evicted
	 */
	@Override
	public void add(IntegrationImportRunEvent runEvent, Duration evictionTimeout) {
		Integer importRunId = runEvent.getImportRun().getId();

		List<Long> runEventIdList = getImportRunRunEventIdMap().computeIfAbsent(importRunId, k -> Collections.synchronizedList(new ArrayList<>()));
		runEventIdList.add(runEvent.getId());
		super.add(runEvent, evictionTimeout);
	}


	@Override
	@Transactional
	public void process(Serializable identifier) {
		// set the user for database
		setRunAsUser();
		IntegrationImportRunEvent runEvent = getIntegrationImportService().getIntegrationImportRunEvent((Long) identifier);
		IntegrationImportRun importRun = runEvent.getImportRun();
		IntegrationImportStatus runEventStatus = runEvent.getStatus();
		if (runEventStatus == null || runEventStatus.getIntegrationImportStatusType() == IntegrationImportStatusTypes.RUNNING) {
			runEvent.setError("Run event response status was not received in time; Event status is forcibly being set to 'ERROR'");
			runEvent.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR));
			getIntegrationImportService().saveIntegrationImportRunEvent(runEvent);
		}
		if (runEvent.getStatus().isError()) {
			//Update the status of the parent run to 'PROCESSED_WITH_ERRORS'
			importRun.setError("Import run processed successfully; however, one or more runEvents failed with errors.");
			importRun.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.PROCESSED_WITH_ERRORS));
			getIntegrationImportService().saveIntegrationImportRun(importRun);
		}

		//Now that it has been processed, remove the event from the importRunRunEventIdMap and update the main run if this was the last event
		processRemoval(runEvent);
	}


	private void processRemoval(IntegrationImportRunEvent runEvent) {
		IntegrationImportRun importRun = runEvent.getImportRun();
		getImportRunRunEventIdMap().get(runEvent.getImportRun().getId()).remove(runEvent.getId());

		//If this was the last object to be evicted for this run; then we need to update the importRun status to 'SUCCESSFUL' if not already set to an error status
		if (CollectionUtils.isEmpty(getImportRunRunEventIdMap().get(runEvent.getImportRun().getId()))) {
			if (!importRun.getStatus().getIntegrationImportStatusType().isError()) {
				importRun.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL));
			}
			//We set the endProcessDate now that the last item has been evicted.
			importRun.setEndProcessDate(new Date());
			getIntegrationImportService().saveIntegrationImportRun(importRun);
		}
	}


	private void setRunAsUser() {
		if ((getContextHandler() != null) && getContextHandler().getBean(Context.USER_BEAN_NAME) == null) {
			SecurityUser result = getSecurityUserService().getSecurityUserByName(SecurityUser.SYSTEM_USER);
			getContextHandler().setBean(Context.USER_BEAN_NAME, result);
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Map<Integer, List<Long>> getImportRunRunEventIdMap() {
		return this.importRunRunEventIdMap;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}
}
