package com.clifton.integration.incoming.corporate.action.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class IntegrationCorporateActionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	/**
	 * importRun
	 */
	@SearchField(searchField = "importRun.integrationImportDefinition.name,importRun.integrationImportDefinition.description")
	private String searchPattern;

	@SearchField(searchField = "importRun.id")
	private Integer importRunId;

	/**
	 * InvestmentSecurityEvent
	 */
	@SearchField
	private Long corporateActionIdentifier;

	@SearchField
	private Short electionNumber;

	@SearchField
	private Short payoutNumber;

	@SearchField
	private String investmentEventTypeName;

	@SearchField
	private String investmentEventStatusName;

	@SearchField
	private Integer investmentSecurityIdentifier;

	@SearchField
	private Date declareDate;

	@SearchField
	private Date exDate;

	@SearchField
	private Date recordDate;

	@SearchField
	private Date eventDate;

	@SearchField
	private Date additionalEventDate;

	@SearchField
	private BigDecimal additionalEventValue;

	@SearchField
	private String eventDescription;

	@SearchField
	private Boolean voluntary;

	@SearchField
	private Boolean taxable;

	/**
	 * InvestmentSecurityEventPayout
	 */
	@SearchField
	private String payoutSecurityIdentifier;

	@SearchField
	private Short payoutSecurityTypeIdentifier;

	@SearchField
	private BigDecimal beforeEventValue;

	@SearchField
	private BigDecimal afterEventValue;

	@SearchField
	private BigDecimal additionalPayoutValue;

	@SearchField
	private Date additionalPayoutDate;

	@SearchField
	private String payoutDescription;

	@SearchField
	private Boolean deleted;

	@SearchField
	private Boolean defaultElection;

	@SearchField
	private Boolean dtcOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getImportRunId() {
		return this.importRunId;
	}


	public void setImportRunId(Integer importRunId) {
		this.importRunId = importRunId;
	}


	public Long getCorporateActionIdentifier() {
		return this.corporateActionIdentifier;
	}


	public void setCorporateActionIdentifier(Long corporateActionIdentifier) {
		this.corporateActionIdentifier = corporateActionIdentifier;
	}


	public Short getElectionNumber() {
		return this.electionNumber;
	}


	public void setElectionNumber(Short electionNumber) {
		this.electionNumber = electionNumber;
	}


	public Short getPayoutNumber() {
		return this.payoutNumber;
	}


	public void setPayoutNumber(Short payoutNumber) {
		this.payoutNumber = payoutNumber;
	}


	public String getInvestmentEventTypeName() {
		return this.investmentEventTypeName;
	}


	public void setInvestmentEventTypeName(String investmentEventTypeName) {
		this.investmentEventTypeName = investmentEventTypeName;
	}


	public String getInvestmentEventStatusName() {
		return this.investmentEventStatusName;
	}


	public void setInvestmentEventStatusName(String investmentEventStatusName) {
		this.investmentEventStatusName = investmentEventStatusName;
	}


	public Integer getInvestmentSecurityIdentifier() {
		return this.investmentSecurityIdentifier;
	}


	public void setInvestmentSecurityIdentifier(Integer investmentSecurityIdentifier) {
		this.investmentSecurityIdentifier = investmentSecurityIdentifier;
	}


	public Date getDeclareDate() {
		return this.declareDate;
	}


	public void setDeclareDate(Date declareDate) {
		this.declareDate = declareDate;
	}


	public Date getExDate() {
		return this.exDate;
	}


	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}


	public Date getRecordDate() {
		return this.recordDate;
	}


	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public Date getAdditionalEventDate() {
		return this.additionalEventDate;
	}


	public void setAdditionalEventDate(Date additionalEventDate) {
		this.additionalEventDate = additionalEventDate;
	}


	public BigDecimal getAdditionalEventValue() {
		return this.additionalEventValue;
	}


	public void setAdditionalEventValue(BigDecimal additionalEventValue) {
		this.additionalEventValue = additionalEventValue;
	}


	public String getEventDescription() {
		return this.eventDescription;
	}


	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}


	public Boolean getVoluntary() {
		return this.voluntary;
	}


	public void setVoluntary(Boolean voluntary) {
		this.voluntary = voluntary;
	}


	public Boolean getTaxable() {
		return this.taxable;
	}


	public void setTaxable(Boolean taxable) {
		this.taxable = taxable;
	}


	public String getPayoutSecurityIdentifier() {
		return this.payoutSecurityIdentifier;
	}


	public void setPayoutSecurityIdentifier(String payoutSecurityIdentifier) {
		this.payoutSecurityIdentifier = payoutSecurityIdentifier;
	}


	public Short getPayoutSecurityTypeIdentifier() {
		return this.payoutSecurityTypeIdentifier;
	}


	public void setPayoutSecurityTypeIdentifier(Short payoutSecurityTypeIdentifier) {
		this.payoutSecurityTypeIdentifier = payoutSecurityTypeIdentifier;
	}


	public BigDecimal getBeforeEventValue() {
		return this.beforeEventValue;
	}


	public void setBeforeEventValue(BigDecimal beforeEventValue) {
		this.beforeEventValue = beforeEventValue;
	}


	public BigDecimal getAfterEventValue() {
		return this.afterEventValue;
	}


	public void setAfterEventValue(BigDecimal afterEventValue) {
		this.afterEventValue = afterEventValue;
	}


	public BigDecimal getAdditionalPayoutValue() {
		return this.additionalPayoutValue;
	}


	public void setAdditionalPayoutValue(BigDecimal additionalPayoutValue) {
		this.additionalPayoutValue = additionalPayoutValue;
	}


	public Date getAdditionalPayoutDate() {
		return this.additionalPayoutDate;
	}


	public void setAdditionalPayoutDate(Date additionalPayoutDate) {
		this.additionalPayoutDate = additionalPayoutDate;
	}


	public String getPayoutDescription() {
		return this.payoutDescription;
	}


	public void setPayoutDescription(String payoutDescription) {
		this.payoutDescription = payoutDescription;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


	public Boolean getDefaultElection() {
		return this.defaultElection;
	}


	public void setDefaultElection(Boolean defaultElection) {
		this.defaultElection = defaultElection;
	}


	public Boolean getDtcOnly() {
		return this.dtcOnly;
	}


	public void setDtcOnly(Boolean dtcOnly) {
		this.dtcOnly = dtcOnly;
	}
}
