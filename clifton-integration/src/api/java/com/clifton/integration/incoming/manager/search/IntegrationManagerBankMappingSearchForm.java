package com.clifton.integration.incoming.manager.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationManagerBankMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "group.id")
	private Integer assetClassGroupId;

	@SearchField(searchField = "name", searchFieldPath = "group")
	private String assetClassGroupName;

	@SearchField
	private String fromBankCode;

	@SearchField
	private String toBankCode;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getAssetClassGroupId() {
		return this.assetClassGroupId;
	}


	public void setAssetClassGroupId(Integer assetClassGroupId) {
		this.assetClassGroupId = assetClassGroupId;
	}


	public String getAssetClassGroupName() {
		return this.assetClassGroupName;
	}


	public void setAssetClassGroupName(String assetClassGroupName) {
		this.assetClassGroupName = assetClassGroupName;
	}


	public String getFromBankCode() {
		return this.fromBankCode;
	}


	public void setFromBankCode(String fromBankCode) {
		this.fromBankCode = fromBankCode;
	}


	public String getToBankCode() {
		return this.toBankCode;
	}


	public void setToBankCode(String toBankCode) {
		this.toBankCode = toBankCode;
	}
}
