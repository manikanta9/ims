package com.clifton.integration.incoming.reconcile;


import com.clifton.integration.incoming.reconcile.search.IntegrationReconcileAccountBalancesByAccountTypeSearchForm;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcileAccountM2MDailyExpenseSearchForm;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcileDailyPNLHistorySearchForm;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcilePositionHistorySearchForm;

import java.util.List;


public interface IntegrationReconcileService {

	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////           IntegrationReconcilePositionHistory Business Methods             /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	public List<IntegrationReconcilePositionHistory> getIntegrationReconcilePositionHistoryList(IntegrationReconcilePositionHistorySearchForm searchForm);


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////    IntegrationReconcileAccountBalancesByAccountType Business Methods       /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	public List<IntegrationReconcileAccountBalancesByAccountType> getIntegrationReconcileAccountBalancesByAccountTypeList(IntegrationReconcileAccountBalancesByAccountTypeSearchForm searchForm);


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////    IntegrationReconcileAccountM2MDailyExpense Business Methods       /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	public List<IntegrationReconcileAccountM2MDailyExpense> getIntegrationReconcileAccountM2MDailyExpenseList(IntegrationReconcileAccountM2MDailyExpenseSearchForm searchForm);


	public List<IntegrationReconcileAccountM2MDailyExpense> getIntegrationReconcileAccountM2MDailyExpenseListByRun(int runId);


	/////////////////////////////////////////////////////////////////////////////////////////////////
	////////           IntegrationReconcileDailyPNLHistory Business Methods             /////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	public List<IntegrationReconcileDailyPNLHistory> getIntegrationReconcileDailyPNLHistoryList(IntegrationReconcileDailyPNLHistorySearchForm searchForm);
}
