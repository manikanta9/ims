package com.clifton.integration.file;


import com.clifton.core.beans.NamedEntity;


public class IntegrationFileDefinitionType extends NamedEntity<Integer> {

	/**
	 * Indicates that the file name on the definition should be used when downloading the file.
	 */
	private boolean definitionFileNameUsedForDownload;


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isDefinitionFileNameUsedForDownload() {
		return this.definitionFileNameUsedForDownload;
	}


	public void setDefinitionFileNameUsedForDownload(boolean definitionFileNameUsedForDownload) {
		this.definitionFileNameUsedForDownload = definitionFileNameUsedForDownload;
	}
}
