package com.clifton.integration.file;


import com.clifton.core.beans.BaseEntity;
import com.clifton.integration.source.IntegrationSource;

import java.util.Date;


public class IntegrationFile extends BaseEntity<Integer> {

	private IntegrationFileDefinition fileDefinition;

	/**
	 * The full path the original encrypted file.
	 * <p/>
	 * NOTE:  The path is relative to the root archive directory.
	 */
	private String encryptedFileNameWithPath;

	/**
	 * The full path the original or the decrypted file.
	 * <p/>
	 * NOTE:  The path is relative to the root archive directory.
	 */
	private String fileNameWithPath;

	/**
	 * Indicates if the file was deleted.
	 */
	private boolean deleted;

	/**
	 * Indicates if the file was deleted.
	 */
	private boolean sourceDataDeleted;

	/**
	 * The date the file was received.
	 */
	private Date receivedDate;

	/**
	 * Flag the indicates that source data has been loaded.  Used when multiple transformations are run against a single file.
	 */
	private boolean sourceDataLoaded;

	/**
	 * An enum representing the way the file was received.
	 */
	private IntegrationFileSourceType fileSourceType;

	/**
	 * The import source that the file was received from.  This is only populated in cases like FTP where the source can be determined from the input location.
	 */
	private IntegrationSource receivedFromSource;

	/**
	 * There was an error archiving the file.
	 */
	private boolean errorOnLoad;

	/**
	 * The error string.
	 */
	private String error;

	/**
	 * The date the file is effective for.  For example, if the file is a position file for 6/20/12 then effectiveDate should be 6/20/12.
	 * <p/>
	 * NOTE: This is a duplicate of the effective date on the IntegrationImportRun table, which can't be removed because it is used in multiple transformations.
	 * It's on this table so it can be displayed easily on the UI and to avoid having to get a list of IntegrationImportRun to get the effective date for a file.
	 */
	private Date effectiveDate;

	/**
	 * The size of the file.  Size is in # of bytes.
	 */
	private Long fileSize;

	/**
	 * The size of the encrypted file.  Size is in # of bytes.
	 */
	private Long encryptedFileSize;
	/**
	 * The password used to access the file.  This currently used by outgoing files to encrypt pdf files
	 * and a password used to access the file.
	 */
	private String password;


	public IntegrationSource getSource() {
		return getReceivedFromSource() != null ? getReceivedFromSource() : (getFileDefinition() != null ? getFileDefinition().getSource() : null);
	}


	public String getEncryptedFileNameWithPath() {
		return this.encryptedFileNameWithPath;
	}


	public void setEncryptedFileNameWithPath(String encryptedFilePath) {
		this.encryptedFileNameWithPath = encryptedFilePath;
	}


	public String getFileNameWithPath() {
		return this.fileNameWithPath;
	}


	public void setFileNameWithPath(String filePath) {
		this.fileNameWithPath = filePath;
	}


	public boolean isDeleted() {
		return this.deleted;
	}


	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}


	public Date getReceivedDate() {
		return this.receivedDate;
	}


	public void setReceivedDate(Date received) {
		this.receivedDate = received;
	}


	public IntegrationFileDefinition getFileDefinition() {
		return this.fileDefinition;
	}


	public void setFileDefinition(IntegrationFileDefinition fileDefinition) {
		this.fileDefinition = fileDefinition;
	}


	public boolean isSourceDataLoaded() {
		return this.sourceDataLoaded;
	}


	public void setSourceDataLoaded(boolean sourceDataLoaded) {
		this.sourceDataLoaded = sourceDataLoaded;
	}


	public IntegrationFileSourceType getFileSourceType() {
		return this.fileSourceType;
	}


	public void setFileSourceType(IntegrationFileSourceType fileSourceType) {
		this.fileSourceType = fileSourceType;
	}


	public boolean isErrorOnLoad() {
		return this.errorOnLoad;
	}


	public void setErrorOnLoad(boolean errorOnLoad) {
		this.errorOnLoad = errorOnLoad;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public IntegrationSource getReceivedFromSource() {
		return this.receivedFromSource;
	}


	public void setReceivedFromSource(IntegrationSource receivedFromSource) {
		this.receivedFromSource = receivedFromSource;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Long getFileSize() {
		return this.fileSize;
	}


	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}


	public Long getEncryptedFileSize() {
		return this.encryptedFileSize;
	}


	public void setEncryptedFileSize(Long encryptedFileSize) {
		this.encryptedFileSize = encryptedFileSize;
	}


	public boolean isSourceDataDeleted() {
		return this.sourceDataDeleted;
	}


	public void setSourceDataDeleted(boolean sourceDataDeleted) {
		this.sourceDataDeleted = sourceDataDeleted;
	}


	public String getPassword() {
		return this.password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
}
