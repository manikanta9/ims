package com.clifton.integration.file;


import java.util.Date;


/**
 * The <code>IntegrationSystemFile</code> class is a virtual dto which represents a file
 * on the file system.
 *
 * @author rbrooks
 */
public class IntegrationSystemFile {

	/*
	 * A String in the format [IntegrationDirectory]:[filePath], e.g.,
	 * "ERRORS:JPMorgan/JPM_CWD_Trade_Confirmations_BBG.csv. Allows deletion and downloading of system files in a safe
	 * manner, and uniquely identifies the file.
	 */
	private String id;
	private IntegrationDirectory integrationDirectory;
	private String filePath;
	private String fileName;
	private Date modifiedDate;
	private Long size;
	private Boolean folder;


	public IntegrationSystemFile() {
		//Public constructor necessary for JSON deserialization
	}


	/**
	 * Only available constructor.  Creates this file's id property, which is derived from the integrationDirectory and filePath fields
	 *
	 * @param integrationDirectory An IntegrationDirectory used as a key to find the base path of system file
	 * @param filePath             The relative path to the file within the IntegrationDirectory
	 */
	public IntegrationSystemFile(IntegrationDirectory integrationDirectory, String filePath) {
		this.setIntegrationDirectory(integrationDirectory);
		this.setFilePath(filePath);
		this.setId(integrationDirectory.toString() + ":" + filePath);
	}


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public IntegrationDirectory getIntegrationDirectory() {
		return this.integrationDirectory;
	}


	public void setIntegrationDirectory(IntegrationDirectory integrationDirectory) {
		this.integrationDirectory = integrationDirectory;
	}


	public String getFilePath() {
		return this.filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public Date getModifiedDate() {
		return this.modifiedDate;
	}


	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public Long getSize() {
		return this.size;
	}


	public void setSize(Long size) {
		this.size = size;
	}


	public Boolean getFolder() {
		return this.folder;
	}


	public void setFolder(Boolean folder) {
		this.folder = folder;
	}
}
