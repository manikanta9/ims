package com.clifton.integration.file.archive;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Used for test runs and deletes all data (Runs and files) that is older than the daysToKeepFile specified on the archive strategy.
 * <p>
 * CAUTION: Should only be used for non-business data (i.e. to cleanup test data for the health check)
 *
 * @author theodorez
 */
public class IntegrationFileArchiveHealthCheckJob implements Task {

	private IntegrationImportService integrationImportService;
	private IntegrationFileService integrationFileService;


	@Override
	public Status run(Map<String, Object> context) {
		StringBuilder message = new StringBuilder();

		//Get all the test definitions
		IntegrationImportDefinitionSearchForm definitionSearchForm = new IntegrationImportDefinitionSearchForm();
		definitionSearchForm.setTestDefinition(true);
		List<IntegrationImportDefinition> definitions = getIntegrationImportService().getIntegrationImportDefinitionList(definitionSearchForm);

		for (IntegrationImportDefinition definition : definitions) {
			IntegrationFileDefinition fileDefinition = definition.getFileDefinition();
			//Determine the archive strategy
			IntegrationFileArchiveStrategy archiveStrategy = fileDefinition.getOverrideFileArchiveStrategy() != null ? fileDefinition.getOverrideFileArchiveStrategy() : fileDefinition.getSource().getFileArchiveStrategy();

			List<Integer> fileIds = new ArrayList<>();
			//Get all of the runs older than specified on the archive strategy
			IntegrationImportRunSearchForm runSearchForm = new IntegrationImportRunSearchForm();
			runSearchForm.setEffectiveDateBefore(DateUtils.addDays(new Date(), -archiveStrategy.getDaysToKeepFile()));
			runSearchForm.setFileDefinitionId(fileDefinition.getId());
			List<IntegrationImportRun> runs = getIntegrationImportService().getIntegrationImportRunList(runSearchForm);

			for (IntegrationImportRun run : runs) {
				getIntegrationImportService().deleteIntegrationImportRun(run.getId());
				IntegrationFile runFile = run.getFile();

				if (!fileIds.contains(runFile.getId())) {
					fileIds.add(runFile.getId());
				}
			}

			for (Integer fileId : fileIds) {
				getIntegrationFileService().deleteIntegrationFile(fileId);
			}
			message.append("Processing the following Import Definition: ");
			message.append(definition.getId());
			message.append("\n");
			message.append("Deleted the following integration runs: ");
			message.append(runs);
			message.append("\n");
			message.append("Deleted the following integration files: ");
			message.append(fileIds);
			message.append("\n\n");
		}
		return Status.ofMessage(message.toString());
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}
}
