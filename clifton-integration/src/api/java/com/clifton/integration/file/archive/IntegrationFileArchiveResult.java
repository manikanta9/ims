package com.clifton.integration.file.archive;


public class IntegrationFileArchiveResult {

	private int numberOfFilesDeleted = 0;
	private int numberOfEncryptedFilesDeleted = 0;
	private int numberOfRunsCleared = 0;
	private int numberOfSourceDataRowsDeleted = 0;


	public int getNumberOfFilesDeleted() {
		return this.numberOfFilesDeleted;
	}


	public void setNumberOfFilesDeleted(int numberOfFilesDeleted) {
		this.numberOfFilesDeleted = numberOfFilesDeleted;
	}


	public int getNumberOfEncryptedFilesDeleted() {
		return this.numberOfEncryptedFilesDeleted;
	}


	public void setNumberOfEncryptedFilesDeleted(int numberOfEncryptedFilesDeleted) {
		this.numberOfEncryptedFilesDeleted = numberOfEncryptedFilesDeleted;
	}


	public int getNumberOfRunsCleared() {
		return this.numberOfRunsCleared;
	}


	public void setNumberOfRunsCleared(int numberOfRunsCleared) {
		this.numberOfRunsCleared = numberOfRunsCleared;
	}


	public int getNumberOfSourceDataRowsDeleted() {
		return this.numberOfSourceDataRowsDeleted;
	}


	public void setNumberOfSourceDataRowsDeleted(int numberOfSourceDataRowsDeleted) {
		this.numberOfSourceDataRowsDeleted = numberOfSourceDataRowsDeleted;
	}
}
