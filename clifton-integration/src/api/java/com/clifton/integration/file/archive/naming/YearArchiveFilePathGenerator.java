package com.clifton.integration.file.archive.naming;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;
import java.util.Map;


/**
 * The <code>YearThenMonthNameGenerator</code> generates the file name with year and month path.  If the file date is 8/16/12 and the input path is
 * \\tcg\test\newFile.csv then the result is \\tcg\test\2012\newFile.csv
 * <p/>
 * For example,  if we want to folder '/integration/broker/2012' with the encrypted file in '/integration/broker/2012/encrypted'.  The inputs would be
 * '/integration/broker','encrypted/theFileName.csv',8/12/2012
 *
 * @author mwacker
 */
public class YearArchiveFilePathGenerator implements IntegrationFileArchivePathGenerator {

	@Override
	public String generateFullFileNameWithPath(String path, String relativeFileNameWithPath, Date fileDate, @SuppressWarnings("unused") Map<String, Object> params) {
		Integer year = DateUtils.getYear(fileDate);

		return FileUtils.combinePath(FileUtils.combinePath(path, year.toString()), relativeFileNameWithPath);
	}
}
