package com.clifton.integration.file.archive.naming;


import com.clifton.core.dataaccess.file.FileUtils;

import java.util.Date;
import java.util.Map;


public class SourceSystemDefinitionNameArchiveFilePathGenerator implements IntegrationFileArchivePathGenerator {

	public static final String SOURCE_SYSTEM_DEFINITION_NAME_PARAMETER = "SOURCE_SYSTEM_DEFINITION_NAME";


	@Override
	public String generateFullFileNameWithPath(String path, String relativeFileNameWithPath, @SuppressWarnings("unused") Date fileDate, Map<String, Object> params) {
		String sourceFolderName = null;
		if (params.containsKey(SOURCE_SYSTEM_DEFINITION_NAME_PARAMETER)) {
			String sourceDefinitionName = (String) params.get(SOURCE_SYSTEM_DEFINITION_NAME_PARAMETER);
			sourceFolderName = FileUtils.replaceInvalidCharacters(sourceDefinitionName, "_");
		}

		return FileUtils.combinePaths(path, sourceFolderName, relativeFileNameWithPath);
	}
}
