package com.clifton.integration.file.archive;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.integration.file.IntegrationFile;

import java.util.Date;
import java.util.List;


public interface IntegrationFileArchiveService {

	@DoNotAddRequestMapping
	public void decryptArchivedIntegrationFile(IntegrationFile bean);


	@DoNotAddRequestMapping
	public IntegrationFile archiveIntegrationFile(IntegrationFileArchiveParameters parameters);


	/**
	 * Decrypt the file if needed, look up a the correct definition and move to the correct archive.
	 *
	 * @param bean
	 */
	@DoNotAddRequestMapping
	public void rearchiveExistingIntegrationFile(IntegrationFile bean);


	@DoNotAddRequestMapping
	public String getIntegrationFileRelativeArchiveFilePath(IntegrationFileArchiveParameters parameters);


	////////////////////////////////////////////////////////////////////////////
	////////    IntegrationFileArchiveStrategy Business Methods    /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationFileArchiveStrategy getIntegrationFileArchiveStrategy(int id);


	public List<IntegrationFileArchiveStrategy> getIntegrationFileArchiveStrategyList(IntegrationFileArchiveStrategySearchForm searchForm);


	@DoNotAddRequestMapping(allowAPI = true)
	public IntegrationFileArchiveResult deleteIntegrationExpiredFiles(Date date);
}
