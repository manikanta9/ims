package com.clifton.integration.file.archive.naming;


import java.util.Date;
import java.util.Map;


public interface IntegrationFileArchivePathGenerator {

	/**
	 * Generates the full file path for an archive file.  The path is the archive folder, and relativeFilePath path is location of the file in the final folder.
	 * <p/>
	 * For example,  if we want to folder '/integration/broker/' with the encrypted files in '/integration/broker/encrypted'.  The inputs for a decrypted file would be
	 * '/integration/broker'
	 * 'theFileName.csv'
	 * 8/12/2012
	 * <p/>
	 * and for an encrypted file
	 * <p/>
	 * '/integration/broker'
	 * 'encrypted/theFileName.csv'
	 * 8/12/2012
	 *
	 * @param path
	 * @param relativeFileNameWithPath
	 * @param fileDate
	 */
	public String generateFullFileNameWithPath(String path, String relativeFileNameWithPath, Date fileDate, Map<String, Object> params);
}
