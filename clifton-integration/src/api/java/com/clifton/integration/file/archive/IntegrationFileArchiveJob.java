package com.clifton.integration.file.archive;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;

import java.util.Date;
import java.util.Map;


public class IntegrationFileArchiveJob implements Task {

	private IntegrationFileArchiveService integrationFileArchiveService;


	@Override
	public Status run(Map<String, Object> context) {
		Date date = DateUtils.clearTime(new Date());
		IntegrationFileArchiveResult result = getIntegrationFileArchiveService().deleteIntegrationExpiredFiles(date);

		StringBuilder message = new StringBuilder();
		message.append("Number of encrypted files deleted was [").append(result.getNumberOfEncryptedFilesDeleted()).append("].");
		message.append("Number of data files deleted was [").append(result.getNumberOfFilesDeleted()).append("].");
		message.append("Total number of deleted files was [").append(result.getNumberOfEncryptedFilesDeleted() + result.getNumberOfFilesDeleted()).append("].");

		message.append("Number of import runs cleared was [").append(result.getNumberOfRunsCleared()).append("].");
		message.append("Total number of source data rows deleted was [").append(result.getNumberOfSourceDataRowsDeleted()).append("].");

		return Status.ofMessage(message.toString());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////
	public IntegrationFileArchiveService getIntegrationFileArchiveService() {
		return this.integrationFileArchiveService;
	}


	public void setIntegrationFileArchiveService(IntegrationFileArchiveService integrationFileArchiveService) {
		this.integrationFileArchiveService = integrationFileArchiveService;
	}
}
