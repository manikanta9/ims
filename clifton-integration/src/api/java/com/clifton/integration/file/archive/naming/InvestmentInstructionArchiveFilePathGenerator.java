package com.clifton.integration.file.archive.naming;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;
import java.util.Map;


/**
 * Archive path generator for investment instructions
 */
public class InvestmentInstructionArchiveFilePathGenerator implements IntegrationFileArchivePathGenerator {

	public static final String DATE_FORMAT_YEAR = "yyyy";
	public static final String DATE_FORMAT_MONTH = "M-MMMM";
	public static final String DATE_FORMAT_DAY = "dd";

	public static final String RECIPIENT_COMPANY_NAME_PARAMETER = "RECIPIENT_COMPANY_NAME";


	@Override
	public String generateFullFileNameWithPath(String path, String relativeFileNameWithPath, Date fileDate, Map<String, Object> params) {
		fileDate = fileDate != null ? fileDate : new Date();

		String combinedPaths = FileUtils.combinePaths(path,
				DateUtils.fromDate(fileDate, DATE_FORMAT_YEAR),
				DateUtils.fromDate(fileDate, DATE_FORMAT_MONTH),
				DateUtils.fromDate(fileDate, DATE_FORMAT_DAY));

		Object recipientCompanyObj = params.get(RECIPIENT_COMPANY_NAME_PARAMETER);
		if (recipientCompanyObj != null) {
			String recipientCompanyName = (String) recipientCompanyObj;
			combinedPaths = FileUtils.combinePath(combinedPaths, FileUtils.replaceInvalidCharacters(recipientCompanyName, "_"));
		}

		return FileUtils.combinePath(combinedPaths, relativeFileNameWithPath);
	}
}
