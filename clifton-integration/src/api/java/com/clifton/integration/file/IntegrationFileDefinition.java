package com.clifton.integration.file;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.Time;
import com.clifton.integration.file.archive.IntegrationFileArchiveStrategy;
import com.clifton.integration.source.IntegrationSource;


public class IntegrationFileDefinition extends BaseEntity<Integer> {

	private IntegrationSource source;

	private IntegrationFileDefinitionType type;

	/**
	 * The file name that will be received from the integration source.
	 * <p/>
	 * Name of the file that will be looked for in the input directory.
	 * <p/>
	 * NOTE: The file name cannot contain 2 underscores in the file name i.e. "__".
	 * A double underscore cannot be used because that is how the system finds the date stamp so that the original file name can be determined.
	 */
	private String fileName;

	/**
	 * The full file name that will be received from the broker.
	 */
	private String externalFileName;

	/**
	 * Optional regular expression for file lookups.
	 */
	private String fileNamePattern;

	/**
	 * Optional regular expression used to find the effective date from a file name.
	 */
	private String effectiveDatePattern;

	/**
	 * Optional {@link java.text.SimpleDateFormat} (MM-dd-yyyy) used to find the effective date from a file name.
	 */
	private String effectiveDateFormat;

	/**
	 * Optional comma-delimited list of column names.
	 */
	private String fileColumnNames;

	/**
	 * Option pattern match for file lookup.
	 */
	private String timeZoneName;

	/**
	 * Override the archive strategy if the import source.
	 */
	private IntegrationFileArchiveStrategy overrideFileArchiveStrategy;

	/**
	 * Additional folder path to add to base archive dir where source files will be archived.
	 */
	private String additionalArchivePath;

	/**
	 * Distinguishes intraday files from ones that come once per day
	 */
	private boolean onePerDay = true;

	/**
	 * The time of day that the file should have arrived by
	 */
	private Time receivedByTime;

	/**
	 * If this is true a time stamp will be appended to file when it is archive to allow for multiple version of the same file.
	 */
	private boolean archiveDuplicates = false;

	/**
	 * The type of encryption used by this file.
	 */
	private IntegrationFileEncryptionType encryptionType;

	/**
	 * The full source table name (i.e. source.BrokerCitigroupExchangeRates) were the file data will be loaded.
	 * <p/>
	 * This assumes that each transformations will only load the source data into a single table.  This can be extended in
	 * the future if this in not found to be the case.
	 */
	private String sourceTableNameWithSchemaList;

	/**
	 * The data source purpose used to look up the a specific price multipliers.
	 */
	private Short priceMarketDataSourcePurposeId;

	/**
	 * Description of the file contents
	 */
	private String fileDescription;

	/**
	 * Denotes whether a file is expected for the current day or not
	 */
	private boolean fileNotExpectedDaily;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationSource getSource() {
		return this.source;
	}


	public void setSource(IntegrationSource source) {
		this.source = source;
	}


	public IntegrationFileDefinitionType getType() {
		return this.type;
	}


	public void setType(IntegrationFileDefinitionType type) {
		this.type = type;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getFileNamePattern() {
		return this.fileNamePattern;
	}


	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}


	public String getEffectiveDatePattern() {
		return this.effectiveDatePattern;
	}


	public void setEffectiveDatePattern(String effectiveDatePattern) {
		this.effectiveDatePattern = effectiveDatePattern;
	}


	public String getEffectiveDateFormat() {
		return this.effectiveDateFormat;
	}


	public void setEffectiveDateFormat(String effectiveDateFormat) {
		this.effectiveDateFormat = effectiveDateFormat;
	}


	public String getFileColumnNames() {
		return this.fileColumnNames;
	}


	public void setFileColumnNames(String fileColumnNames) {
		this.fileColumnNames = fileColumnNames;
	}


	public String[] getFileColumnNamesArray() {
		return this.fileColumnNames == null ? null : this.fileColumnNames.split(",");
	}


	public String getTimeZoneName() {
		return this.timeZoneName;
	}


	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}


	public IntegrationFileArchiveStrategy getOverrideFileArchiveStrategy() {
		return this.overrideFileArchiveStrategy;
	}


	public void setOverrideFileArchiveStrategy(IntegrationFileArchiveStrategy overrideFileArchiveStrategy) {
		this.overrideFileArchiveStrategy = overrideFileArchiveStrategy;
	}


	public String getExternalFileName() {
		return this.externalFileName;
	}


	public void setExternalFileName(String encryptedFileName) {
		this.externalFileName = encryptedFileName;
	}


	public String getAdditionalArchivePath() {
		return this.additionalArchivePath;
	}


	public void setAdditionalArchivePath(String additionalArchivePath) {
		this.additionalArchivePath = additionalArchivePath;
	}


	public boolean isOnePerDay() {
		return this.onePerDay;
	}


	public void setOnePerDay(boolean onePerDay) {
		this.onePerDay = onePerDay;
	}


	public Time getReceivedByTime() {
		return this.receivedByTime;
	}


	public void setReceivedByTime(Time receivedByTime) {
		this.receivedByTime = receivedByTime;
	}


	public boolean isArchiveDuplicates() {
		return this.archiveDuplicates;
	}


	public void setArchiveDuplicates(boolean archiveDuplicates) {
		this.archiveDuplicates = archiveDuplicates;
	}


	public IntegrationFileEncryptionType getEncryptionType() {
		return this.encryptionType;
	}


	public void setEncryptionType(IntegrationFileEncryptionType encryptionType) {
		this.encryptionType = encryptionType;
	}


	public String getSourceTableNameWithSchemaList() {
		return this.sourceTableNameWithSchemaList;
	}


	public void setSourceTableNameWithSchemaList(String sourceTableList) {
		this.sourceTableNameWithSchemaList = sourceTableList;
	}


	public Short getPriceMarketDataSourcePurposeId() {
		return this.priceMarketDataSourcePurposeId;
	}


	public void setPriceMarketDataSourcePurposeId(Short priceMarketDataSourcePurposeId) {
		this.priceMarketDataSourcePurposeId = priceMarketDataSourcePurposeId;
	}


	public String getFileDescription() {
		return this.fileDescription;
	}


	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}


	public boolean isFileNotExpectedDaily() {
		return this.fileNotExpectedDaily;
	}


	public void setFileNotExpectedDaily(boolean fileNotExpectedDaily) {
		this.fileNotExpectedDaily = fileNotExpectedDaily;
	}
}
