package com.clifton.integration.file.archive;


import com.clifton.core.beans.NamedEntity;
import com.clifton.integration.file.archive.naming.IntegrationFileArchivePathConvention;


public class IntegrationFileArchiveStrategy extends NamedEntity<Integer> {

	/**
	 * The number of days to keep the encrypted file if one was received.
	 */
	private int daysToKeepEncryptedFile = 0;

	/**
	 * The number of days to keep the original data file (or the decrypted file).
	 */
	private int daysToKeepFile = 0;

	/**
	 * The number of days to keep the source data.
	 */
	private int daysToKeepSourceData;

	/**
	 * The path to the folder where the files will be archived.
	 */
	private String pathToArchiveFolder;

	/**
	 * If a new file is archived, delete all other corresponding archived files.
	 */
	private boolean deleteOnNewFile;

	/**
	 * The naming convention to use.
	 */
	private IntegrationFileArchivePathConvention pathConvention;


	public int getDaysToKeepEncryptedFile() {
		return this.daysToKeepEncryptedFile;
	}


	public void setDaysToKeepEncryptedFile(int daysToKeepEncryptedFile) {
		this.daysToKeepEncryptedFile = daysToKeepEncryptedFile;
	}


	public int getDaysToKeepFile() {
		return this.daysToKeepFile;
	}


	public void setDaysToKeepFile(int daysToKeepFile) {
		this.daysToKeepFile = daysToKeepFile;
	}


	public String getPathToArchiveFolder() {
		return this.pathToArchiveFolder;
	}


	public void setPathToArchiveFolder(String pathToArchiveFolder) {
		this.pathToArchiveFolder = pathToArchiveFolder;
	}


	public boolean isDeleteOnNewFile() {
		return this.deleteOnNewFile;
	}


	public void setDeleteOnNewFile(boolean deleteOnNewFile) {
		this.deleteOnNewFile = deleteOnNewFile;
	}


	public int getDaysToKeepSourceData() {
		return this.daysToKeepSourceData;
	}


	public void setDaysToKeepSourceData(int daysToKeepSourceData) {
		this.daysToKeepSourceData = daysToKeepSourceData;
	}


	public IntegrationFileArchivePathConvention getPathConvention() {
		return this.pathConvention;
	}


	public void setPathConvention(IntegrationFileArchivePathConvention pathConvention) {
		this.pathConvention = pathConvention;
	}
}
