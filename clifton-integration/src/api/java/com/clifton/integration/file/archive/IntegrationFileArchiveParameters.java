package com.clifton.integration.file.archive;


import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.file.IntegrationFileSourceType;
import com.clifton.integration.source.IntegrationSource;

import java.io.File;
import java.util.Date;
import java.util.Map;


public class IntegrationFileArchiveParameters {

	/**
	 * Additional properties that can be passed along
	 */
	private Map<String, Object> additionalProperties;

	/**
	 * Used to separate exports into logical type groupings
	 */
	private String baseArchivePath;

	private String pgpPublicKey;
	private File fileToArchive;
	private IntegrationFileArchiveStrategy archiveStrategy;
	/**
	 * Optional override to name the final archived file. May be null.
	 */
	private String destinationFileNameOverride;
	private String originalFilename;
	private IntegrationFileDefinition definition;
	private IntegrationFileSourceType fileSourceType;
	private IntegrationSource importSource;
	/**
	 * Optional name used to identify the source system definition (for example, the name of an IMS Export).
	 */
	private String sourceSystemDefinitionName;
	private boolean decryptionRequired;
	private Date effectiveDate;
	private String filePassword;
	private boolean deleteSourceFile = true;


	public IntegrationFileArchiveParameters() {
		//
	}


	public IntegrationFileArchiveParameters(File fileToArchive, IntegrationFileDefinition definition) {
		this.fileToArchive = fileToArchive;
		this.definition = definition;
	}


	public IntegrationFileArchiveParameters(File fileToArchive, String originalFilename, IntegrationFileSourceType fileSourceType) {
		this(fileToArchive, originalFilename, fileSourceType, null);
	}


	public IntegrationFileArchiveParameters(File fileToArchive, String originalFilename, IntegrationFileSourceType fileSourceType, Date effectiveDate) {
		this.fileToArchive = fileToArchive;
		this.originalFilename = originalFilename;
		this.fileSourceType = fileSourceType;
		this.effectiveDate = effectiveDate;
	}


	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}


	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}


	public String getBaseArchivePath() {
		return this.baseArchivePath;
	}


	public void setBaseArchivePath(String baseArchivePath) {
		this.baseArchivePath = baseArchivePath;
	}


	public IntegrationFileArchiveStrategy getArchiveStrategy() {
		return this.archiveStrategy;
	}


	public void setArchiveStrategy(IntegrationFileArchiveStrategy archiveStrategy) {
		this.archiveStrategy = archiveStrategy;
	}


	public File getFileToArchive() {
		return this.fileToArchive;
	}


	public void setFileToArchive(File fileToArchive) {
		this.fileToArchive = fileToArchive;
	}


	public IntegrationFileDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(IntegrationFileDefinition definition) {
		this.definition = definition;
	}


	public IntegrationFileSourceType getFileSourceType() {
		return this.fileSourceType;
	}


	public void setFileSourceType(IntegrationFileSourceType fileSourceType) {
		this.fileSourceType = fileSourceType;
	}


	/**
	 * @return {@link #sourceSystemDefinitionName}
	 */

	public String getSourceSystemDefinitionName() {
		return this.sourceSystemDefinitionName;
	}


	/**
	 * Sets {@link #sourceSystemDefinitionName}.
	 */

	public void setSourceSystemDefinitionName(String sourceSystemDefinitionName) {
		this.sourceSystemDefinitionName = sourceSystemDefinitionName;
	}


	public String getOriginalFilename() {
		return this.originalFilename;
	}


	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}


	public String getDestinationFileNameOverride() {
		return this.destinationFileNameOverride;
	}


	public void setDestinationFileNameOverride(String destinationFileNameOverride) {
		this.destinationFileNameOverride = destinationFileNameOverride;
	}


	public IntegrationSource getImportSource() {
		return this.importSource;
	}


	public void setImportSource(IntegrationSource importSource) {
		this.importSource = importSource;
	}


	public boolean isDecryptionRequired() {
		return this.decryptionRequired;
	}


	public void setDecryptionRequired(boolean decryptionRequired) {
		this.decryptionRequired = decryptionRequired;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public String getFilePassword() {
		return this.filePassword;
	}


	public void setFilePassword(String filePassword) {
		this.filePassword = filePassword;
	}


	public boolean isDeleteSourceFile() {
		return this.deleteSourceFile;
	}


	public void setDeleteSourceFile(boolean deleteSourceFile) {
		this.deleteSourceFile = deleteSourceFile;
	}


	public String getPgpPublicKey() {
		return this.pgpPublicKey;
	}


	public void setPgpPublicKey(String pgpPublicKey) {
		this.pgpPublicKey = pgpPublicKey;
	}
}
