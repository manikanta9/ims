package com.clifton.integration.file.archive.naming;


import com.clifton.core.dataaccess.file.FileUtils;

import java.util.Date;
import java.util.Map;


public class DefaultArchiveFilePathGenerator implements IntegrationFileArchivePathGenerator {

	@Override
	public String generateFullFileNameWithPath(String path, String relativeFileNameWithPath, @SuppressWarnings("unused") Date fileDate, @SuppressWarnings("unused") Map<String, Object> params) {
		return FileUtils.combinePath(path, relativeFileNameWithPath);
	}
}
