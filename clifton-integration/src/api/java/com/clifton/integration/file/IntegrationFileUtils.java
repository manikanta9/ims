package com.clifton.integration.file;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.apache.commons.io.FilenameUtils;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>IntegrationFileUtils</code> integration file helper methods.
 * <p>
 * NOTE: This is a static file helper class used only for integration files.
 *
 * @author mwacker
 */
public class IntegrationFileUtils {

	public static final String FILE_NAME_READY_EXTENSION = "ready";
	public static final String FILE_NAME_WRITING_EXTENSION = "write";

	/**
	 * A Regex to find date stamps with formats yyyyMMddHHmmss, yyyyMMddHHmm, yyyyMMdd, yyyyMMdd-HHmm, MMdd
	 */
	private static final String DATE_TIME_REGEX = "" //
			+ "((_\\.|\\.|_|)([0-9]{4})([0-1][0-9])([0-3][0-9])([0-1][0-9]|[2][0-3])([0-5][0-9])([0-5][0-9]))" // _.yyyyMMdd.HHmmss or .yyyyMMddHHmmss or _yyyyMMddHHmmss or yyyyMMddHHmmss
			+ "|((_\\.|\\.|_|)([0-9]{4})([0-1][0-9])([0-3][0-9])(\\.)([0-1][0-9]|[2][0-3])([0-5][0-9])([0-5][0-9]))" // _.yyyyMMdd.HHmmss or .yyyyMMdd.HHmmss or _yyyyMMdd.HHmmss or yyyyMMdd.HHmmss
			+ "|((_|)([0-9]{4})([0-1][0-9])([0-3][0-9])([0-1][0-9]|[2][0-3])([0-5][0-9]))" // _yyyyMMddHHmm or yyyyMMddHHmm
			+ "|((_|)([0-9]{4})([0-1][0-9])([0-3][0-9])-([0-1][0-9]|[2][0-3])([0-5][0-9]))" // _yyyyMMdd-HHmm or yyyyMMdd-HHmm
			+ "|((\\.|_|)([0-9]{4})([0-1][0-9])([0-3][0-9]))" // .yyyyMMdd or _yyyyMMdd or yyyyMMdd
			+ "|((\\.|_|)([0-9]{2})([0-1][0-9])([0-3][0-9]))" // .yyMMdd or _yyMMdd or yyMMdd

			// for Seattle files
			+ "|((\\.|_|)([0-1][0-9])([0-3][0-9])([0-9][0-9]))" // .MMddyy or _MMddyy or MMddyy
			+ "|((_|)([0-1][0-9])([0-3][0-9]))" // _MMdd or MMdd

			// for Markit files
			+ "|((\\.|_|)([0-3][0-9])([a-zA-Z]{3,3})([0-9][0-9]))"; // .ddMMMyy or ddMMMyy or _ddMMMyy


	private static final String TIME_STAMP_REGEX = "__(([0-9]{4})-([0-1][0-9])-([0-3][0-9])_([0-1][0-9]|[2][0-3])_([0-5][0-9])_([0-5][0-9]))";

	/**
	 * List of date formats received for brokers.
	 */
	private static final String[] DATE_FORMAT_LIST = new String[]{"yyyyMMdd", "yyyyMMdd-HHmm", "yyyyMMddHHmmss", "yyyyMMddHHmm", "MMddyy", "MMdd", "yyyyMMdd.HHmmss", "ddMMMyy", "yyMMdd"};

	/**
	 * List of extensions for encrypted files.
	 */
	private static final List<String> ENCRYPTED_FILE_EXTENSIONS = Collections.unmodifiableList(CollectionUtils.createList("pgp"));


	public static boolean isFileEncrypted(String fileName) {
		return ENCRYPTED_FILE_EXTENSIONS.contains(FileUtils.getFileExtension(fileName));
	}


	public static String getOriginalFileName(String fileName) {
		return getOriginalFileName(fileName, getEffectiveDateFromFileName(fileName, null) != null);
	}


	public static String getOriginalFileName(String fileName, boolean removeFileDate) {
		String result = removeTimeStamp(fileName);
		if (removeFileDate) {
			result = StringUtils.removeAll(result, DATE_TIME_REGEX);
			if (result.startsWith(".")) {
				result = result.substring(1);
			}
			if (result.startsWith("_")) {
				result = result.substring(1);
			}
		}
		return result.replaceAll("__\\.", ".").replaceAll("_\\.", ".").replaceAll("-\\.", ".");
	}

	public static String getOriginalFileName(String fileName, String dateFormat, Date effectiveDate) {
		String result = IntegrationFileUtils.removeTimeStamp(fileName);
		if (!StringUtils.isEmpty(result) && !StringUtils.isEmpty(dateFormat) && effectiveDate != null) {
			String formattedDate = DateUtils.fromDate(effectiveDate, dateFormat);
			if (result.contains("_." + formattedDate)) {
				formattedDate = "_." + formattedDate;
			}
			else if (result.contains("." + formattedDate)) {
				formattedDate = "." + formattedDate;
			}
			else if (result.contains("_" + formattedDate)) {
				formattedDate = "_" + formattedDate;
			}
			result = StringUtils.removeAll(result, Pattern.quote(formattedDate));
			if (result.startsWith(".")) {
				result = result.substring(1);
			}
			if (result.startsWith("_")) {
				result = result.substring(1);
			}
			result = result.replaceAll("__\\.", ".").replaceAll("_\\.", ".").replaceAll("-\\.", ".");
		}
		return result;
	}


	public static Date getEffectiveDateFromFileName(String fileName) {
		return getEffectiveDateFromFileName(fileName, null);
	}


	public static Date getEffectiveDateFromFileName(String fileName, String timeZoneId) {
		return getEffectiveDateFromFileName(fileName, timeZoneId, null);
	}


	public static Date getEffectiveDateFromFileName(String fileName, String timeZoneId, String datePattern) {
		return getEffectiveDateFromFileName(fileName, timeZoneId, datePattern, null);
	}


	public static Date getEffectiveDateFromFileName(String fileName, String timeZoneId, String datePattern, String dateFormat) {
		String origFileName = fileName;
		if (isTimeStampPresent(fileName)) {
			origFileName = removeTimeStamp(fileName);
		}

		String effectiveDateRegex = StringUtils.isEmpty(datePattern) ? DATE_TIME_REGEX : datePattern;
		String dateString = getDateString(origFileName, effectiveDateRegex);
		if (!StringUtils.isEmpty(dateString)) {
			if (dateString.endsWith(".")) {
				dateString = dateString.substring(0, dateString.length() - 1);
			}
			if (dateString.startsWith("_") || dateString.startsWith(".")) {

				if (dateString.startsWith("_.")) {
					dateString = dateString.substring(2);
				}
				else {
					dateString = dateString.substring(1);
				}
			}
		}
		return parseEffectiveDate(dateString, timeZoneId, dateFormat);
	}


	private static String getDateString(String fileName, String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(fileName);
		if (m.find()) {
			String group0 = m.group();
			// the pattern matches the entire fileName
			if (StringUtils.isEqual(group0, fileName)) {
				// assume the date is capture group(1)
				return m.group(1);
			}
			else {
				// only matching a portion of the fileName
				return group0;
			}
		}
		return null;
	}


	private static Date parseEffectiveDate(String dateStr, String timeZoneId, String dateFormat) {
		if (!StringUtils.isEmpty(dateStr)) {
			String[] nullSafeFormatList = StringUtils.isEmpty(dateFormat) ? DATE_FORMAT_LIST : ArrayUtils.createArray(dateFormat);
			for (String formatStr : nullSafeFormatList) {
				if (formatStr.length() == dateStr.length()) {
					try {
						SimpleDateFormat format = new SimpleDateFormat(formatStr);
						if (timeZoneId != null) {
							format.setTimeZone(TimeZone.getTimeZone(timeZoneId));
						}
						if (formatStr.startsWith("MM") && Integer.parseInt(dateStr.substring(0, 2)) > 12) {
							continue;
						}
						Date result = format.parse(dateStr);

						if ("MMdd".equals(formatStr)) {
							result = DateUtils.addYears(result, DateUtils.getYearsDifference(new Date(), result));
						}
						return result;
					}
					catch (Throwable e) {
						// Ignore and try next date parser
					}
				}
			}
		}
		// All parsers failed
		return null;
	}


	public static String removeTimeStamp(String fileName) {
		return StringUtils.removeAll(fileName, TIME_STAMP_REGEX);
	}


	public static boolean isTimeStampPresent(String fileName) {
		Pattern p = Pattern.compile(TIME_STAMP_REGEX);
		Matcher m = p.matcher(fileName);
		return m.find();
	}


	public static Date getTimeStamp(String fileName) {
		String dateString = getDateString(fileName, TIME_STAMP_REGEX);
		if (!StringUtils.isEmpty(dateString)) {
			return DateUtils.toDate(dateString.substring(2), DateUtils.DATE_FORMAT_FILE);
		}
		return null;
	}


	//This is purposely a non-static method so it can be called via SPeL using the bean annotation rather than static method calling
	//which appears to lose the context making it impossible to pass the payload values as parameters.
	public String flattenNameAndAppendTimeStamp(String fileName) {
		fileName = FilenameUtils.getName(fileName);
		//Since there doesn't appear to be a way to force a download to overwrite using the spring integration ftp functionality
		//then if a file on the FTP server has a timestamp, remove it and issue a new one to prevent an issue where an existing
		//file with that same timestamp may still exist in the inprocess folder (e.g. if the server was restarted, or some other failure.
		if (isTimeStampPresent(fileName)) {
			fileName = removeTimeStamp(fileName);
		}
		return appendTimeStamp(fileName, null);
	}


	public static String appendTimeStamp(String fileName, Date date) {
		if (isTimeStampPresent(fileName)) {
			return fileName;
		}
		if (date == null) {
			date = new Date();
		}
		int extensionIndex = fileName.lastIndexOf('.');
		String extension = "";
		if (extensionIndex >= 0) {
			extension = fileName.substring(extensionIndex);
		}
		String name = extensionIndex >= 0 ? fileName.substring(0, fileName.lastIndexOf('.')) : fileName;
		return name + "__" + DateUtils.fromDate(date, DateUtils.DATE_FORMAT_FILE) + extension;
	}
}
