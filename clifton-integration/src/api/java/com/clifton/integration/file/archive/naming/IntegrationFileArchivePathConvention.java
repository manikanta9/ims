package com.clifton.integration.file.archive.naming;


/**
 * The <code>IntegrationFileArchiveNamingConvention</code> defines a naming convention for archiving files.
 *
 * @author mwacker
 */
public enum IntegrationFileArchivePathConvention {
	DEFAULT(new DefaultArchiveFilePathGenerator()), //
	BY_YEAR(new YearArchiveFilePathGenerator()), //
	BY_YEAR_THEN_MONTH(new YearThenMonthArchiveFilePathGenerator()), //
	BY_SOURCE_SYSTEM_DEFINITION_NAME(new SourceSystemDefinitionNameArchiveFilePathGenerator()),
	INVESTMENT_INSTRUCTION(new InvestmentInstructionArchiveFilePathGenerator());

	private final IntegrationFileArchivePathGenerator pathGenerator;


	IntegrationFileArchivePathConvention(IntegrationFileArchivePathGenerator nameGenerator) {
		this.pathGenerator = nameGenerator;
	}


	public IntegrationFileArchivePathGenerator getPathGenerator() {
		return this.pathGenerator;
	}
}
