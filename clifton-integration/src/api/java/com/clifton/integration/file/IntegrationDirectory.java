package com.clifton.integration.file;


/**
 * The <code>IntegrationDirectory</code> enum defines various directories used by integration, which may
 * be of relevance to the user for troubleshooting, etc.
 *
 * @author rbrooks
 */
public enum IntegrationDirectory {
	INPUT, BATCH, INPROCESS, PROCESSED, ERRORS, FTP, SFTP
}
