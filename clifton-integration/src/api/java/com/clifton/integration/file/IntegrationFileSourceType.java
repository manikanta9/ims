package com.clifton.integration.file;


public enum IntegrationFileSourceType {
	BATCH(false), EXPORT(false), FTP(true), MANUAL(false), SFTP(true);

	private final boolean ftp;


	IntegrationFileSourceType(boolean ftp) {
		this.ftp = ftp;
	}


	public boolean isFtp() {
		return this.ftp;
	}
}
