package com.clifton.integration.file;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.integration.file.search.IntegrationFileDefinitionSearchForm;
import com.clifton.integration.file.search.IntegrationFileSearchForm;
import com.clifton.integration.source.IntegrationSource;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;


public interface IntegrationFileService {

	public static final String UNKNOWN_FILE_DEFINITION_FILE_NAME = "UNKNOWN";


	/**
	 * Used by the archive service the get the root directory when storing files.
	 */
	@DoNotAddRequestMapping
	public Map<String, String> getRootArchiveDirectoryMap();


	@DoNotAddRequestMapping
	public Map<IntegrationDirectory, String> getIntegrationSystemFilePathMap();


	////////////////////////////////////////////////////////////////////////////
	////////      IntegrationFileDefinition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////
	public IntegrationFileDefinition getIntegrationFileDefinition(int id);


	public IntegrationFileDefinition getIntegrationFileDefinitionByFileName(String fileName);


	public IntegrationFileDefinition getIntegrationFileDefinitionByExternalFileName(String externalFileName, Integer importSourceId);


	public IntegrationFileDefinition getIntegrationDefinitionForFile(File input, Date effectiveDate, IntegrationSource integrationSource);


	public List<IntegrationFileDefinition> getIntegrationFileDefinitionList(IntegrationFileDefinitionSearchForm searchForm);


	@DoNotAddRequestMapping
	public IntegrationFileDefinition getFileDefinitionByPattern(String inputFileName, Date effectiveDate, IntegrationSource integrationSource);


	public IntegrationFileDefinition saveIntegrationFileDefinition(IntegrationFileDefinition bean);


	public void deleteIntegrationFileDefinition(int id);


	////////////////////////////////////////////////////////////////////////////
	////////    IntegrationFileDefinitionType Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////
	public IntegrationFileDefinitionType getIntegrationFileDefinitionType(int id);


	public List<IntegrationFileDefinitionType> getIntegrationFileDefinitionTypeList();


	////////////////////////////////////////////////////////////////////////////
	////////          IntegrationFile Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationFile getIntegrationFile(int id);


	@SecureMethod
	public FileWrapper downloadIntegrationFile(int id);


	@DoNotAddRequestMapping
	public String getIntegrationFileAbsolutePath(IntegrationDirectory dir, String fileName);


	@DoNotAddRequestMapping
	public String getIntegrationFileAbsolutePath(IntegrationFile bean);


	@DoNotAddRequestMapping
	public String getIntegrationFileAbsoluteEncryptedPath(IntegrationFile bean);


	@DoNotAddRequestMapping
	public IntegrationSource getIntegrationFileSource(IntegrationFile file, boolean exceptionIfNull);


	public List<IntegrationFile> getIntegrationFileList(IntegrationFileSearchForm searchForm);


	public IntegrationFile saveIntegrationFile(IntegrationFile bean);


	@DoNotAddRequestMapping(allowAPI = true)
	public void deleteIntegrationFile(int id);

	////////////////////////////////////////////////////////////////////////////
	////////             SystemFile Business Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of all ftpFolderNames with the root dir prepended.
	 */
	@DoNotAddRequestMapping
	public List<String> getFtpFolderNamesWithRoot(String rootDirectory, boolean getAsLowercase);


	/**
	 * Returns a list of files within the given IntegrationDirectory
	 */
	@SecureMethod(dtoClass = IntegrationFile.class)
	public List<IntegrationSystemFile> getIntegrationSystemFileList(IntegrationDirectory directory);


	/**
	 * Allows a download of the system file indicated by "id."
	 *
	 * @param id A String in the format [IntegrationDirectory]:[filePath] (e.g., "ERRORS:JPMorgan/JPM_CWD_Trade_Confirmations_BBG.csv).
	 */
	@SecureMethod(dtoClass = IntegrationFile.class)
	public FileWrapper downloadIntegrationSystemFile(String id);


	/**
	 * Deletes the system file indicated by "id."
	 *
	 * @param id A String in the format [IntegrationDirectory]:[filePath] (e.g., "ERRORS:JPMorgan/JPM_CWD_Trade_Confirmations_BBG.csv).
	 */
	@SecureMethod(dtoClass = IntegrationFile.class)
	public void deleteIntegrationSystemFile(String id);
}
