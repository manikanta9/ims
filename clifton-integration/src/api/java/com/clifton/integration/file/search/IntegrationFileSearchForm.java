package com.clifton.integration.file.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.integration.file.IntegrationFileSourceType;

import java.util.Date;


public class IntegrationFileSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "id", searchFieldPath = "integrationExportList")
	private Integer integrationExportId;

	@SearchField(searchField = "fileDefinition.id")
	private Integer fileDefinitionId;

	@SearchField(searchField = "name", searchFieldPath = "fileDefinition.type")
	private String typeName;

	@SearchField
	private String encryptedFileNameWithPath;

	@SearchField(searchField = "encryptedFileNameWithPath", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean encryptedFileExists;

	@SearchField
	private String fileNameWithPath;

	@SearchField
	private Boolean deleted;

	@SearchField
	private Date receivedDate;

	@SearchField(comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS, searchField = "receivedDate")
	private Date receivedDateBefore;

	@SearchField
	private Date effectiveDate;

	@SearchField
	private Boolean sourceDataLoaded;

	@SearchField
	private Boolean sourceDataDeleted;

	@SearchField
	private Boolean errorOnLoad;

	@SearchField
	private String error;

	@SearchField(searchField = "receivedFromSource.id")
	private Integer receivedFromSourceId;

	@SearchField(searchField = "name", searchFieldPath = "receivedFromSource")
	private String receivedFromSourceName;

	@SearchField
	private IntegrationFileSourceType fileSourceType;

	@SearchField(searchField = "fileSourceType", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private IntegrationFileSourceType excludeFileSourceType;

	@SearchField(searchField = "fileName", searchFieldPath = "fileDefinition")
	private String fileDefinitionName;

	@SearchField(searchField = "externalFileName", searchFieldPath = "fileDefinition")
	private String externalFileName;

	@SearchField(searchField = "fileArchiveStrategy.id", searchFieldPath = "fileDefinition.source")
	private Integer integrationFileArchiveStrategyId;

	@SearchField(searchField = "overrideFileArchiveStrategy.id", searchFieldPath = "fileDefinition")
	private Integer overrideFileArchiveStrategyId;

	@SearchField(searchField = "overrideFileArchiveStrategy.id", searchFieldPath = "fileDefinition", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean overrideArchiveStrategyDoesNotExist;

	// Custom search field for filtering known/unknown files.  If null, defaults to ALL
	private Boolean known;

	// Custom search field 
	private String sourceName;

	// Custom search field
	private Integer sourceId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Integer getFileDefinitionId() {
		return this.fileDefinitionId;
	}


	public void setFileDefinitionId(Integer fileDefinitionId) {
		this.fileDefinitionId = fileDefinitionId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getEncryptedFileNameWithPath() {
		return this.encryptedFileNameWithPath;
	}


	public void setEncryptedFileNameWithPath(String encryptedFileNameWithPath) {
		this.encryptedFileNameWithPath = encryptedFileNameWithPath;
	}


	public String getFileNameWithPath() {
		return this.fileNameWithPath;
	}


	public void setFileNameWithPath(String fileNameWithPath) {
		this.fileNameWithPath = fileNameWithPath;
	}


	public Boolean getDeleted() {
		return this.deleted;
	}


	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}


	public Date getReceivedDate() {
		return this.receivedDate;
	}


	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}


	public Boolean getSourceDataLoaded() {
		return this.sourceDataLoaded;
	}


	public void setSourceDataLoaded(Boolean sourceDataLoaded) {
		this.sourceDataLoaded = sourceDataLoaded;
	}


	public Boolean getSourceDataDeleted() {
		return this.sourceDataDeleted;
	}


	public void setSourceDataDeleted(Boolean sourceDataDeleted) {
		this.sourceDataDeleted = sourceDataDeleted;
	}


	public Boolean getErrorOnLoad() {
		return this.errorOnLoad;
	}


	public void setErrorOnLoad(Boolean errorOnLoad) {
		this.errorOnLoad = errorOnLoad;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public IntegrationFileSourceType getFileSourceType() {
		return this.fileSourceType;
	}


	public void setFileSourceType(IntegrationFileSourceType fileSourceType) {
		this.fileSourceType = fileSourceType;
	}


	public String getFileDefinitionName() {
		return this.fileDefinitionName;
	}


	public void setFileDefinitionName(String fileDefinitionName) {
		this.fileDefinitionName = fileDefinitionName;
	}


	public Integer getIntegrationFileArchiveStrategyId() {
		return this.integrationFileArchiveStrategyId;
	}


	public void setIntegrationFileArchiveStrategyId(Integer integrationFileArchiveStrategyId) {
		this.integrationFileArchiveStrategyId = integrationFileArchiveStrategyId;
	}


	public Integer getOverrideFileArchiveStrategyId() {
		return this.overrideFileArchiveStrategyId;
	}


	public void setOverrideFileArchiveStrategyId(Integer overrideFileArchiveStrategyId) {
		this.overrideFileArchiveStrategyId = overrideFileArchiveStrategyId;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getReceivedDateBefore() {
		return this.receivedDateBefore;
	}


	public void setReceivedDateBefore(Date receivedDateBefore) {
		this.receivedDateBefore = receivedDateBefore;
	}


	public Boolean getEncryptedFileExists() {
		return this.encryptedFileExists;
	}


	public void setEncryptedFileExists(Boolean encryptedFileExists) {
		this.encryptedFileExists = encryptedFileExists;
	}


	public Boolean getOverrideArchiveStrategyDoesNotExist() {
		return this.overrideArchiveStrategyDoesNotExist;
	}


	public void setOverrideArchiveStrategyDoesNotExist(Boolean overrideArchiveStrategyDoesNotExist) {
		this.overrideArchiveStrategyDoesNotExist = overrideArchiveStrategyDoesNotExist;
	}


	public String getExternalFileName() {
		return this.externalFileName;
	}


	public void setExternalFileName(String externalFileName) {
		this.externalFileName = externalFileName;
	}


	public Boolean getKnown() {
		return this.known;
	}


	public void setKnown(Boolean known) {
		this.known = known;
	}


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Integer getReceivedFromSourceId() {
		return this.receivedFromSourceId;
	}


	public void setReceivedFromSourceId(Integer receivedFromSourceId) {
		this.receivedFromSourceId = receivedFromSourceId;
	}


	public String getReceivedFromSourceName() {
		return this.receivedFromSourceName;
	}


	public void setReceivedFromSourceName(String receivedFromSourceName) {
		this.receivedFromSourceName = receivedFromSourceName;
	}


	public Integer getIntegrationExportId() {
		return this.integrationExportId;
	}


	public void setIntegrationExportId(Integer integrationExportId) {
		this.integrationExportId = integrationExportId;
	}


	public IntegrationFileSourceType getExcludeFileSourceType() {
		return this.excludeFileSourceType;
	}


	public void setExcludeFileSourceType(IntegrationFileSourceType excludeFileSourceType) {
		this.excludeFileSourceType = excludeFileSourceType;
	}


	public Integer getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
}
