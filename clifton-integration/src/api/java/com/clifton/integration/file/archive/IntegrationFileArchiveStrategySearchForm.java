package com.clifton.integration.file.archive;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationFileArchiveStrategySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Integer daysToKeepEncryptedFile;

	@SearchField
	private Integer daysToKeepFile;

	@SearchField
	private Integer daysToKeepSourceData;

	@SearchField
	private String pathToArchiveFolder;

	@SearchField
	private Boolean deleteOnNewFile;

	@SearchField(searchField = "name")
	private String searchPattern;


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getDaysToKeepEncryptedFile() {
		return this.daysToKeepEncryptedFile;
	}


	public void setDaysToKeepEncryptedFile(Integer daysToKeepEncryptedFile) {
		this.daysToKeepEncryptedFile = daysToKeepEncryptedFile;
	}


	public Integer getDaysToKeepFile() {
		return this.daysToKeepFile;
	}


	public void setDaysToKeepFile(Integer daysToKeepFile) {
		this.daysToKeepFile = daysToKeepFile;
	}


	public Integer getDaysToKeepSourceData() {
		return this.daysToKeepSourceData;
	}


	public void setDaysToKeepSourceData(Integer daysToKeepSourceData) {
		this.daysToKeepSourceData = daysToKeepSourceData;
	}


	public String getPathToArchiveFolder() {
		return this.pathToArchiveFolder;
	}


	public void setPathToArchiveFolder(String pathToArchiveFolder) {
		this.pathToArchiveFolder = pathToArchiveFolder;
	}


	public Boolean getDeleteOnNewFile() {
		return this.deleteOnNewFile;
	}


	public void setDeleteOnNewFile(Boolean deleteOnNewFile) {
		this.deleteOnNewFile = deleteOnNewFile;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
