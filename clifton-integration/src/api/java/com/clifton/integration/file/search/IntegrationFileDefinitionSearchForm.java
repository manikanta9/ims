package com.clifton.integration.file.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.integration.file.IntegrationFileEncryptionType;


public class IntegrationFileDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "fileName")
	private String searchPattern;

	@SearchField(searchField = "name", searchFieldPath = "source")
	private String sourceName;

	@SearchField(searchField = "source.id")
	private Integer sourceId;

	@SearchField(searchField = "type.id")
	private Integer typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField
	private String fileName;

	@SearchField
	private String externalFileName;

	@SearchField
	private String fileNamePattern;

	@SearchField(searchField = "fileNamePattern", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean fileNamePatternNotNull;

	@SearchField(searchField = "effectiveDateFormat", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean effectiveDateFormatNotNull;

	@SearchField(searchField = "overrideFileArchiveStrategy.id")
	private Integer overrideFileArchiveStrategyId;

	@SearchField(searchField = "name", searchFieldPath = "overrideFileArchiveStrategy")
	private String overrideFileArchiveStrategyName;

	@SearchField(searchField = "overrideFileArchiveStrategy.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean overrideArchiveStrategyDoesNotExist;

	// Custom search field
	/**
	 * Custom search field for filtering all definitions with a given strategy.
	 * <p/>
	 * (source.fileArchiveStrategy.id = fileArchiveStrategyId AND overrideFileArchiveStrategy.id IS NULL)
	 * OR overrideFileArchiveStrategy.id = fileArchiveStrategyId
	 */
	private Integer fileArchiveStrategyId;

	@SearchField
	private IntegrationFileEncryptionType encryptionType;

	@SearchField
	private String additionalArchivePath;

	@SearchField
	private Boolean onePerDay;

	@SearchField
	private Boolean archiveDuplicates;

	// Custom search field for filtering used/unused definitions.  If null, defaults to ALL
	private Boolean used;

	@SearchField
	private String sourceTableNameWithSchemaList;

	@SearchField(searchField = "sourceTableNameWithSchemaList", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean sourceTableNameWithSchemaListExists;


	public String getSourceName() {
		return this.sourceName;
	}


	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}


	public Integer getSourceId() {
		return this.sourceId;
	}


	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}


	public Integer getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getExternalFileName() {
		return this.externalFileName;
	}


	public void setExternalFileName(String externalFileName) {
		this.externalFileName = externalFileName;
	}


	public String getFileNamePattern() {
		return this.fileNamePattern;
	}


	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}


	public Integer getOverrideFileArchiveStrategyId() {
		return this.overrideFileArchiveStrategyId;
	}


	public void setOverrideFileArchiveStrategyId(Integer overrideFileArchiveStrategyId) {
		this.overrideFileArchiveStrategyId = overrideFileArchiveStrategyId;
	}


	public String getOverrideFileArchiveStrategyName() {
		return this.overrideFileArchiveStrategyName;
	}


	public void setOverrideFileArchiveStrategyName(String overrideFileArchiveStrategyName) {
		this.overrideFileArchiveStrategyName = overrideFileArchiveStrategyName;
	}


	public String getAdditionalArchivePath() {
		return this.additionalArchivePath;
	}


	public void setAdditionalArchivePath(String additionalArchivePath) {
		this.additionalArchivePath = additionalArchivePath;
	}


	public Boolean getOnePerDay() {
		return this.onePerDay;
	}


	public void setOnePerDay(Boolean onePerDay) {
		this.onePerDay = onePerDay;
	}


	public Boolean getArchiveDuplicates() {
		return this.archiveDuplicates;
	}


	public void setArchiveDuplicates(Boolean archiveDuplicates) {
		this.archiveDuplicates = archiveDuplicates;
	}


	public Boolean getFileNamePatternNotNull() {
		return this.fileNamePatternNotNull;
	}


	public void setFileNamePatternNotNull(Boolean fileNamePatternNotNull) {
		this.fileNamePatternNotNull = fileNamePatternNotNull;
	}


	public Boolean getEffectiveDateFormatNotNull() {
		return this.effectiveDateFormatNotNull;
	}


	public void setEffectiveDateFormatNotNull(Boolean effectiveDateFormatNotNull) {
		this.effectiveDateFormatNotNull = effectiveDateFormatNotNull;
	}


	public IntegrationFileEncryptionType getEncryptionType() {
		return this.encryptionType;
	}


	public void setEncryptionType(IntegrationFileEncryptionType encryptionType) {
		this.encryptionType = encryptionType;
	}


	public Boolean getUsed() {
		return this.used;
	}


	public void setUsed(Boolean used) {
		this.used = used;
	}


	public Boolean getOverrideArchiveStrategyDoesNotExist() {
		return this.overrideArchiveStrategyDoesNotExist;
	}


	public void setOverrideArchiveStrategyDoesNotExist(Boolean overrideArchiveStrategyDoesNotExist) {
		this.overrideArchiveStrategyDoesNotExist = overrideArchiveStrategyDoesNotExist;
	}


	public Integer getFileArchiveStrategyId() {
		return this.fileArchiveStrategyId;
	}


	public void setFileArchiveStrategyId(Integer fileArchiveStrategyId) {
		this.fileArchiveStrategyId = fileArchiveStrategyId;
	}


	public String getSourceTableNameWithSchemaList() {
		return this.sourceTableNameWithSchemaList;
	}


	public void setSourceTableNameWithSchemaList(String sourceTableNameWithSchemaList) {
		this.sourceTableNameWithSchemaList = sourceTableNameWithSchemaList;
	}


	public Boolean getSourceTableNameWithSchemaListExists() {
		return this.sourceTableNameWithSchemaListExists;
	}


	public void setSourceTableNameWithSchemaListExists(Boolean sourceTableNameWithSchemaListExists) {
		this.sourceTableNameWithSchemaListExists = sourceTableNameWithSchemaListExists;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
