package com.clifton.integration.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.web.bind.WebBindingDataRetriever;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;


/**
 * @author vgomelsky
 */
public class IntegrationWebBindingDataRetriever implements WebBindingDataRetriever {

	private IntegrationWebBindingDataRetrieverService integrationWebBindingDataRetrieverService;

	private Pattern requestUriPattern;
	private int order;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	@Override
	public boolean isApplicableForRequest(WebRequest request) {
		if (this.requestUriPattern == null) {
			return true;
		}
		String uri = request.getDescription(false);
		return this.requestUriPattern.matcher(uri).matches();
	}


	@Override
	public <T extends IdentityObject> T getEntity(Class<T> entityType, Serializable entityId) {
		return getIntegrationWebBindingDataRetrieverService().getIntegrationWebBindingObject(entityType.getName(), (Number) entityId);
	}


	@Override
	public List<Column> getEntityColumnList(Class<? extends IdentityObject> entityType) {
		return getIntegrationWebBindingDataRetrieverService().getIntegrationWebBindingObjectColumnList(entityType.getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationWebBindingDataRetrieverService getIntegrationWebBindingDataRetrieverService() {
		return this.integrationWebBindingDataRetrieverService;
	}


	public void setIntegrationWebBindingDataRetrieverService(IntegrationWebBindingDataRetrieverService integrationWebBindingDataRetrieverService) {
		this.integrationWebBindingDataRetrieverService = integrationWebBindingDataRetrieverService;
	}


	public Pattern getRequestUriPattern() {
		return this.requestUriPattern;
	}


	public void setRequestUriPattern(Pattern requestUriPattern) {
		this.requestUriPattern = requestUriPattern;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
