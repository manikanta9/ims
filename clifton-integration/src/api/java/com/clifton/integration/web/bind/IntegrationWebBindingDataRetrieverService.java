package com.clifton.integration.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The ReconciliationWebBindingDataRetrieverService interface defines methods that external callers
 * need to do proper binding and validation of integration objects.
 *
 * @author stevenf
 */
@Service
public interface IntegrationWebBindingDataRetrieverService {

	/**
	 * Returns the DTO of the specified type for the specified primary key.
	 */
	@SecureMethod(disableSecurity = true)
	public <T extends IdentityObject> T getIntegrationWebBindingObject(String objectClassName, Number objectId);


	/**
	 * Returns Column meta-data for each column of the specified DTO type.
	 */
	@SecureMethod(disableSecurity = true)
	public List<Column> getIntegrationWebBindingObjectColumnList(String objectClassName);
}
