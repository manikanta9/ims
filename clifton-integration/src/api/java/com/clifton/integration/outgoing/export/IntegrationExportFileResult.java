package com.clifton.integration.outgoing.export;


import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus.IntegrationExportFileStatusNames;


public class IntegrationExportFileResult {

	private IntegrationFile file;
	private IntegrationExportFileStatusNames status;
	private Throwable exception;


	public IntegrationFile getFile() {
		return this.file;
	}


	public void setFile(IntegrationFile file) {
		this.file = file;
	}


	public IntegrationExportFileStatusNames getStatus() {
		return this.status;
	}


	public void setStatus(IntegrationExportFileStatusNames status) {
		this.status = status;
	}


	public Throwable getException() {
		return this.exception;
	}


	public void setException(Throwable exception) {
		this.exception = exception;
	}
}
