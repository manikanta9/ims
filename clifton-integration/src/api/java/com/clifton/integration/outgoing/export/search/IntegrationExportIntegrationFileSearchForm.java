package com.clifton.integration.outgoing.export.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationExportIntegrationFileSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer exportId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer fileId;


	public Integer getExportId() {
		return this.exportId;
	}


	public void setExportId(Integer exportId) {
		this.exportId = exportId;
	}


	public Integer getFileId() {
		return this.fileId;
	}


	public void setFileId(Integer fileId) {
		this.fileId = fileId;
	}
}
