package com.clifton.integration.outgoing.export;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus.IntegrationExportFileStatusNames;
import com.clifton.integration.outgoing.export.search.IntegrationExportHistorySearchForm;
import com.clifton.integration.outgoing.export.search.IntegrationExportIntegrationFileSearchForm;
import com.clifton.integration.outgoing.export.search.IntegrationExportSearchForm;
import com.clifton.integration.outgoing.export.search.IntegrationExportStatusSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>IntegrationExportService</code> interface defines methods for
 * working with {@link IntegrationExport} and related objects.
 *
 * @author mwacker
 */
public interface IntegrationExportService {

	///////////////////////////////////////////////////////////////////////////////////////
	///////               Integration Export  Status Business Methods             ///////// 
	///////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExportStatus getIntegrationExportStatusByName(String name);


	public List<IntegrationExportStatus> getIntegrationExportStatusList();


	public List<IntegrationExportStatus> getIntegrationExportStatusList(IntegrationExportStatusSearchForm searchForm);


	///////////////////////////////////////////////////////////////////////////////////////
	//////                  Integration Export  Business Methods                     ////// 
	///////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExport getIntegrationExport(int id);


	public List<IntegrationExport> getIntegrationExportList();


	public List<IntegrationExport> getIntegrationExportList(IntegrationExportSearchForm searchForm);


	public IntegrationExport saveIntegrationExport(IntegrationExport bean);


	public IntegrationExport getIntegrationExportPopulated(int id);


	///////////////////////////////////////////////////////////////////////////////////////
	///////               Integration Export History Business Methods               ///////
	///////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExportHistory getIntegrationExportHistory(int id);


	public IntegrationExportHistory getIntegrationExportHistoryByStartDate(int exportId, Date startDate);


	public List<IntegrationExportHistory> getIntegrationExportHistoryList(IntegrationExportHistorySearchForm searchForm);


	public IntegrationExportHistory saveIntegrationExportHistory(IntegrationExportHistory bean);


	///////////////////////////////////////////////////////////////////////////////////////
	///////           Integration Export Integration File Business Methods          ///////
	///////////////////////////////////////////////////////////////////////////////////////
	public IntegrationExportIntegrationFile getIntegrationExportIntegrationFile(int id);


	public List<IntegrationExportIntegrationFile> getIntegrationExportIntegrationFileList(IntegrationExportIntegrationFileSearchForm searchForm);


	public IntegrationExportFileStatus getIntegrationExportFileStatusByName(IntegrationExportFileStatusNames name);


	public IntegrationExportIntegrationFile getIntegrationExportIntegrationFileByExportIdAndFileId(int exportId, int fileId);


	public IntegrationExportIntegrationFile saveIntegrationExportIntegrationFile(IntegrationExportIntegrationFile bean);


	public void linkIntegrationExportIntegrationFile(IntegrationExport export, IntegrationFile file, IntegrationExportFileStatusNames status);

	///////////////////////////////////////////////////////////////////////////////////////
	///////                    Integration Export Reprocess Methods                 ///////
	///////////////////////////////////////////////////////////////////////////////////////


	@RequestMapping("integrationExportResend")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_EXECUTE)
	public void reprocessIntegrationExport(int integrationExportId);
}
