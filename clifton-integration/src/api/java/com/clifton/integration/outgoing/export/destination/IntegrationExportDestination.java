package com.clifton.integration.outgoing.export.destination;


import com.clifton.core.beans.BaseEntity;
import com.clifton.security.secret.SecuritySecret;


/**
 * The <code>IntegrationExportDestination</code> specifies the actual email
 * addresses, FTP URLs, or phone numbers.
 *
 * @author msiddiqui
 */
public class IntegrationExportDestination extends BaseEntity<Integer> {

	private IntegrationExportDestinationType type;

	/**
	 * Actual destination values. e.g email address, FTP URL etc.
	 */
	private String destinationValue;

	/**
	 * optional text. e.g a name of the person in case of Email export.
	 */
	private String destinationText;

	private String destinationUserName;

	private SecuritySecret destinationPasswordSecuritySecret;

	private SecuritySecret destinationSshPrivateKeySecuritySecret;

	private String pgpPublicKey;
	private boolean pgpArmor;
	private boolean pgpIntegrityCheck;

	private Boolean activeFtp;

	private boolean overwriteFile;

	private boolean writeExtension;


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "IntegrationExportDestination [type=" + this.type.getName() + ", value=" + this.destinationValue + "]";
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExportDestinationType getType() {
		return this.type;
	}


	public void setType(IntegrationExportDestinationType type) {
		this.type = type;
	}


	public String getDestinationValue() {
		return this.destinationValue;
	}


	public void setDestinationValue(String destinationValue) {
		this.destinationValue = destinationValue;
	}


	public String getDestinationText() {
		return this.destinationText;
	}


	public void setDestinationText(String destinationText) {
		this.destinationText = destinationText;
	}


	public String getDestinationUserName() {
		return this.destinationUserName;
	}


	public void setDestinationUserName(String destinationUserName) {
		this.destinationUserName = destinationUserName;
	}


	public SecuritySecret getDestinationPasswordSecuritySecret() {
		return this.destinationPasswordSecuritySecret;
	}


	public void setDestinationPasswordSecuritySecret(SecuritySecret destinationPasswordSecuritySecret) {
		this.destinationPasswordSecuritySecret = destinationPasswordSecuritySecret;
	}


	public SecuritySecret getDestinationSshPrivateKeySecuritySecret() {
		return this.destinationSshPrivateKeySecuritySecret;
	}


	public void setDestinationSshPrivateKeySecuritySecret(SecuritySecret destinationSshPrivateKeySecuritySecret) {
		this.destinationSshPrivateKeySecuritySecret = destinationSshPrivateKeySecuritySecret;
	}


	public String getPgpPublicKey() {
		return this.pgpPublicKey;
	}


	public void setPgpPublicKey(String pgpPublicKey) {
		this.pgpPublicKey = pgpPublicKey;
	}


	public Boolean getActiveFtp() {
		return this.activeFtp;
	}


	public void setActiveFtp(Boolean activeFtp) {
		this.activeFtp = activeFtp;
	}


	public boolean isPgpArmor() {
		return this.pgpArmor;
	}


	public void setPgpArmor(boolean pgpArmor) {
		this.pgpArmor = pgpArmor;
	}


	public boolean isPgpIntegrityCheck() {
		return this.pgpIntegrityCheck;
	}


	public void setPgpIntegrityCheck(boolean pgpIntegrityCheck) {
		this.pgpIntegrityCheck = pgpIntegrityCheck;
	}


	public boolean isOverwriteFile() {
		return this.overwriteFile;
	}


	public void setOverwriteFile(boolean overwriteFile) {
		this.overwriteFile = overwriteFile;
	}


	public boolean isWriteExtension() {
		return this.writeExtension;
	}


	public void setWriteExtension(boolean writeExtension) {
		this.writeExtension = writeExtension;
	}
}
