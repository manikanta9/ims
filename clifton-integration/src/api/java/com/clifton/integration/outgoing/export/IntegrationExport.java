package com.clifton.integration.outgoing.export;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestinationType;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;
import com.clifton.integration.source.IntegrationSource;

import java.util.Date;
import java.util.List;


/**
 * The <code>IntegrationExport</code> represents each request received for
 * doing an Export.
 *
 * @author msiddiqui
 */
public class IntegrationExport extends BaseEntity<Integer> {

	/**
	 * The archive strategy to be used for this export
	 */
	@NonPersistentField
	private String archiveStrategyName;

	private IntegrationExportStatus status;

	private IntegrationSource integrationSource;

	/**
	 * The destination type associated with this export. This field is primarily available for convenience so that the UI can be more clear about the export type. The same value could be determined
	 * by getting the type off of the {@link IntegrationExportDestination} within the {@link IntegrationExportIntegrationExportDestination} many-to-many table. Although there can be multiple
	 * destinations associated with an IntegrationExport, they will always be of the same type (e.g. there may be multiple e-mail destinations, but the type for each is still email).
	 */
	private IntegrationExportDestinationType destinationType;

	/**
	 * Supplied by the client. Used to associate the export to a source system.
	 */
	private String sourceSystemIdentifier;

	/**
	 * A name supplied by the client used to identify the export.
	 */
	private String sourceSystemDefinitionName;

	private Date receivedDate;

	private Date sentDate;

	private String messageSubject;

	/**
	 * For an email, it will be email content, for a FAX or TEXT etc. it will the respective messageText.
	 */
	private String messageText;

	private Integer retryCount;

	private Integer retryDelayInSeconds;

	/**
	 * The list of many-to-many entries to the export.  Need the many to many list because it has the sub type for the destination.
	 */
	private List<IntegrationExportIntegrationExportDestination> linkedDestinationList;

	private List<IntegrationFile> integrationFileList;

	/**
	 * Hostname and port are used to specify the smtp server and port to send emails to.
	 * This is used for testing only.
	 */
	@NonPersistentField
	private String serverHostname;

	@NonPersistentField
	private Integer serverPort;

	@NonPersistentField
	private String awsS3EndpointOverride;

	private String awsS3Bucket;

	private String awsS3Region;

	private String awsS3AccessKeyId;

	private String awsS3SecretKey;


	/**
	 * If set, will be prepended to the content filename before the file is sent.
	 * <p>
	 * NOTE: The prefix will NOT be added to the filename of a document when it is archived.
	 */
	private String filenamePrefix;


	public String getArchiveStrategyName() {
		return this.archiveStrategyName;
	}


	public void setArchiveStrategyName(String archiveStrategyName) {
		this.archiveStrategyName = archiveStrategyName;
	}


	public IntegrationExportStatus getStatus() {
		return this.status;
	}


	public void setStatus(IntegrationExportStatus status) {
		this.status = status;
	}


	public IntegrationSource getIntegrationSource() {
		return this.integrationSource;
	}


	public void setIntegrationSource(IntegrationSource integrationSource) {
		this.integrationSource = integrationSource;
	}


	/**
	 * @return {@link #destinationType}
	 */

	public IntegrationExportDestinationType getDestinationType() {
		return this.destinationType;
	}


	/**
	 * Sets {@link #destinationType}.
	 */

	public void setDestinationType(IntegrationExportDestinationType destinationType) {
		this.destinationType = destinationType;
	}


	public String getSourceSystemIdentifier() {
		return this.sourceSystemIdentifier;
	}


	public void setSourceSystemIdentifier(String sourceSystemIdentifier) {
		this.sourceSystemIdentifier = sourceSystemIdentifier;
	}


	public Date getReceivedDate() {
		return this.receivedDate;
	}


	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}


	public Date getSentDate() {
		return this.sentDate;
	}


	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}


	public Integer getRetryCount() {
		return this.retryCount;
	}


	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}


	public Integer getRetryDelayInSeconds() {
		return this.retryDelayInSeconds;
	}


	public void setRetryDelayInSeconds(Integer retryDelayInSeconds) {
		this.retryDelayInSeconds = retryDelayInSeconds;
	}


	public String getMessageSubject() {
		return this.messageSubject;
	}


	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}


	public String getMessageText() {
		return this.messageText;
	}


	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}


	public List<IntegrationFile> getIntegrationFileList() {
		return this.integrationFileList;
	}


	public void setIntegrationFileList(List<IntegrationFile> integrationFileList) {
		this.integrationFileList = integrationFileList;
	}


	public List<IntegrationExportIntegrationExportDestination> getLinkedDestinationList() {
		return this.linkedDestinationList;
	}


	public void setLinkedDestinationList(List<IntegrationExportIntegrationExportDestination> linkedDestinationList) {
		this.linkedDestinationList = linkedDestinationList;
	}


	public String getSourceSystemDefinitionName() {
		return this.sourceSystemDefinitionName;
	}


	public void setSourceSystemDefinitionName(String sourceSystemDefinitionName) {
		this.sourceSystemDefinitionName = sourceSystemDefinitionName;
	}


	public String getServerHostname() {
		return this.serverHostname;
	}


	public void setServerHostname(String serverHostname) {
		this.serverHostname = serverHostname;
	}


	public Integer getServerPort() {
		return this.serverPort;
	}


	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}


	public String getAwsS3Bucket() {
		return this.awsS3Bucket;
	}


	public void setAwsS3Bucket(String awsS3Bucket) {
		this.awsS3Bucket = awsS3Bucket;
	}


	public String getAwsS3Region() {
		return this.awsS3Region;
	}


	public void setAwsS3Region(String awsS3Region) {
		this.awsS3Region = awsS3Region;
	}


	public String getAwsS3EndpointOverride() {
		return this.awsS3EndpointOverride;
	}


	public String getAwsS3AccessKeyId() {
		return this.awsS3AccessKeyId;
	}


	public void setAwsS3AccessKeyId(String awsS3AccessKeyId) {
		this.awsS3AccessKeyId = awsS3AccessKeyId;
	}


	public String getAwsS3SecretKey() {
		return this.awsS3SecretKey;
	}


	public void setAwsS3SecretKey(String awsS3SecretKey) {
		this.awsS3SecretKey = awsS3SecretKey;
	}


	public void setAwsS3EndpointOverride(String awsS3EndpointOverride) {
		this.awsS3EndpointOverride = awsS3EndpointOverride;
	}


	public String getFilenamePrefix() {
		return this.filenamePrefix;
	}


	public void setFilenamePrefix(String filenamePrefix) {
		this.filenamePrefix = filenamePrefix;
	}
}
