package com.clifton.integration.outgoing.export.destination.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationExportDestinationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "type.id", sortField = "type.name")
	private Short typeId;

	// Custom Search Field
	private Short destinationSubTypeId;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String destinationValue;

	@SearchField
	private String destinationText;

	@SearchField(searchField = "destinationText", comparisonConditions = ComparisonConditions.EQUALS)
	private String destinationTextEquals;

	@SearchField(searchField = "destinationText", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean destinationTextIsNull;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String destinationUserName;

	@SearchField(searchField = "destinationUserName", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean destinationUserNameIsNull;

	@SearchField(searchField = "destinationPasswordSecret", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean destinationHasPasswordSecret;

	@SearchField(searchField = "destinationSshPrivateKeySecret", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean destinationHasSshPrivateKeySecret;

	@SearchField
	private Boolean activeFtp;

	@SearchField
	private Boolean pgpArmor;

	@SearchField
	private Boolean pgpIntegrityCheck;

	@SearchField
	private Boolean overwriteFile;

	@SearchField
	private Boolean writeExtension;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getDestinationValue() {
		return this.destinationValue;
	}


	public void setDestinationValue(String destinationValue) {
		this.destinationValue = destinationValue;
	}


	public String getDestinationText() {
		return this.destinationText;
	}


	public void setDestinationText(String destinationText) {
		this.destinationText = destinationText;
	}


	public String getDestinationTextEquals() {
		return this.destinationTextEquals;
	}


	public void setDestinationTextEquals(String destinationTextEquals) {
		this.destinationTextEquals = destinationTextEquals;
	}


	public Boolean getDestinationTextIsNull() {
		return this.destinationTextIsNull;
	}


	public void setDestinationTextIsNull(Boolean destinationTextIsNull) {
		this.destinationTextIsNull = destinationTextIsNull;
	}


	public String getDestinationUserName() {
		return this.destinationUserName;
	}


	public void setDestinationUserName(String destinationUserName) {
		this.destinationUserName = destinationUserName;
	}


	public Short getDestinationSubTypeId() {
		return this.destinationSubTypeId;
	}


	public void setDestinationSubTypeId(Short destinationSubTypeId) {
		this.destinationSubTypeId = destinationSubTypeId;
	}


	public Boolean getDestinationUserNameIsNull() {
		return this.destinationUserNameIsNull;
	}


	public void setDestinationUserNameIsNull(Boolean destinationUserNameIsNull) {
		this.destinationUserNameIsNull = destinationUserNameIsNull;
	}


	public Boolean getDestinationHasPasswordSecret() {
		return this.destinationHasPasswordSecret;
	}


	public void setDestinationHasPasswordSecret(Boolean destinationHasPasswordSecret) {
		this.destinationHasPasswordSecret = destinationHasPasswordSecret;
	}


	public Boolean getDestinationHasSshPrivateKeySecret() {
		return this.destinationHasSshPrivateKeySecret;
	}


	public void setDestinationHasSshPrivateKeySecret(Boolean destinationHasSshPrivateKeySecret) {
		this.destinationHasSshPrivateKeySecret = destinationHasSshPrivateKeySecret;
	}


	public Boolean getActiveFtp() {
		return this.activeFtp;
	}


	public void setActiveFtp(Boolean activeFtp) {
		this.activeFtp = activeFtp;
	}


	public Boolean getPgpArmor() {
		return this.pgpArmor;
	}


	public void setPgpArmor(Boolean pgpArmor) {
		this.pgpArmor = pgpArmor;
	}


	public Boolean getPgpIntegrityCheck() {
		return this.pgpIntegrityCheck;
	}


	public void setPgpIntegrityCheck(Boolean pgpIntegrityCheck) {
		this.pgpIntegrityCheck = pgpIntegrityCheck;
	}


	public Boolean getOverwriteFile() {
		return this.overwriteFile;
	}


	public void setOverwriteFile(Boolean overwriteFile) {
		this.overwriteFile = overwriteFile;
	}


	public Boolean getWriteExtension() {
		return this.writeExtension;
	}


	public void setWriteExtension(Boolean writeExtension) {
		this.writeExtension = writeExtension;
	}
}
