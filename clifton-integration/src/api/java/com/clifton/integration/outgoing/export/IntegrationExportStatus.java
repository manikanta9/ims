package com.clifton.integration.outgoing.export;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.export.messaging.ExportStatuses;


/**
 * The <code>IntegrationExportStatus</code> specifies Status for the Export.
 * <ul>
 * <li>RECEIVED</li>
 * <li>PROCESSED</li>
 * <li>REPROCESSED</li>
 * <li>FAILED</li>
 * </ul>
 *
 * @author msiddiqui
 */
@CacheByName
public class IntegrationExportStatus extends NamedEntity<Short> {

	public ExportStatuses getIntegrationExportStatusName() {
		return ExportStatuses.valueOf(getName());
	}
}
