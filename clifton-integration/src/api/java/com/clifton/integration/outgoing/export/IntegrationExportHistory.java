package com.clifton.integration.outgoing.export;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>IntegrationExportHistory</code> class is a history of an Export
 * executed.
 *
 * @author msiddiqui
 */
public class IntegrationExportHistory extends BaseEntity<Integer> {

	private IntegrationExport export;
	private IntegrationExportStatus exportStatus;

	private Date startDate;
	private Date endDate;

	private Short retryAttemptNumber;

	/**
	 * Contains specific information regarding the execution of the export.
	 */
	@NonPersistentField
	private String historyDescription;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(50);
		result.append("{id=");
		result.append(getId());
		result.append(", ");
		result.append(getExportStatus());
		result.append(", ");
		result.append(getDescription());
		result.append('}');
		return result.toString();
	}


	public String getExecutionTimeFormatted() {
		if (getStartDate() == null) {
			return null;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getTimeDifferenceShort(toDate, getStartDate());
	}


	////////////////////////////////////////////////////////////////////////////


	public IntegrationExport getExport() {
		return this.export;
	}


	public void setExport(IntegrationExport export) {
		this.export = export;
	}


	public IntegrationExportStatus getExportStatus() {
		return this.exportStatus;
	}


	public void setExportStatus(IntegrationExportStatus exportStatus) {
		this.exportStatus = exportStatus;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getDescription() {
		return this.historyDescription;
	}


	public void setDescription(String description) {
		this.historyDescription = description;
	}


	public Short getRetryAttemptNumber() {
		return this.retryAttemptNumber;
	}


	public void setRetryAttemptNumber(Short retryAttemptNumber) {
		this.retryAttemptNumber = retryAttemptNumber;
	}
}
