package com.clifton.integration.outgoing.export.destination;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.integration.outgoing.export.IntegrationExport;


/**
 * The <code>IntegrationExportIntegrationExportDestination</code> class
 * associates Exports with Destinations.
 *
 * @author Masood Siddiqui
 */
public class IntegrationExportIntegrationExportDestination extends ManyToManyEntity<IntegrationExport, IntegrationExportDestination, Integer> {

	/**
	 * Identifies what is the sub type of the destination.
	 * See {@link IntegrationExportDestinationSubType}.
	 */
	private IntegrationExportDestinationSubType destinationSubType;


	public IntegrationExportDestinationSubType getDestinationSubType() {
		return this.destinationSubType;
	}


	public void setDestinationSubType(IntegrationExportDestinationSubType destinationSubType) {
		this.destinationSubType = destinationSubType;
	}
}
