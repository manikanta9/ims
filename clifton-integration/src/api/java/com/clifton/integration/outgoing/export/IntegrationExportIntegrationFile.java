package com.clifton.integration.outgoing.export;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.integration.file.IntegrationFile;


/**
 * The <code>IntegrationExportIntegrationFile</code> defines a link to any integration file for an export.
 * A single IntegrationExport may contains multiple files.
 *
 * @author mwacker
 */
public class IntegrationExportIntegrationFile extends ManyToManyEntity<IntegrationExport, IntegrationFile, Integer> {

	private IntegrationExportFileStatus status;
	private String statusDescription;


	public IntegrationExportFileStatus getStatus() {
		return this.status;
	}


	public void setStatus(IntegrationExportFileStatus status) {
		this.status = status;
	}


	public String getStatusDescription() {
		return this.statusDescription;
	}


	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
}
