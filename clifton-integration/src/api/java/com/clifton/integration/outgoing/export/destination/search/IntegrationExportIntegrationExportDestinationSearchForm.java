package com.clifton.integration.outgoing.export.destination.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;


/**
 * The <code>IntegrationExportIntegrationExportDestinationSearchForm</code> has
 * search fields for the link table {@link IntegrationExportIntegrationExportDestination}
 *
 * @author Masood Siddiqui
 */
public class IntegrationExportIntegrationExportDestinationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer exportId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer exportDestinationId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "destinationValue")
	private String exportDestinationValue;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "destinationText")
	private String exportDestinationText;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "destinationUserName")
	private String exportDestinationUserName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "type.id")
	private Short exportDestinationTypeId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "type.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short exportDestinationTypeIdNotEquals;

	@SearchField(searchFieldPath = "referenceTwo.type", searchField = "name")
	private String exportDestinationTypeName;

	@SearchField(searchField = "destinationSubType.id", sortField = "destinationSubType.name")
	private Short exportDestinationSubTypeId;

	@SearchField(searchFieldPath = "destinationSubType", searchField = "name")
	private String exportDestinationSubTypeName;


	public Integer getExportId() {
		return this.exportId;
	}


	public void setExportId(Integer exportId) {
		this.exportId = exportId;
	}


	public Integer getExportDestinationId() {
		return this.exportDestinationId;
	}


	public void setExportDestinationId(Integer exportDestinationId) {
		this.exportDestinationId = exportDestinationId;
	}


	public String getExportDestinationValue() {
		return this.exportDestinationValue;
	}


	public void setExportDestinationValue(String exportDestinationValue) {
		this.exportDestinationValue = exportDestinationValue;
	}


	public String getExportDestinationText() {
		return this.exportDestinationText;
	}


	public void setExportDestinationText(String exportDestinationText) {
		this.exportDestinationText = exportDestinationText;
	}


	public String getExportDestinationUserName() {
		return this.exportDestinationUserName;
	}


	public void setExportDestinationUserName(String exportDestinationUserName) {
		this.exportDestinationUserName = exportDestinationUserName;
	}


	public Short getExportDestinationTypeId() {
		return this.exportDestinationTypeId;
	}


	public void setExportDestinationTypeId(Short exportDestinationTypeId) {
		this.exportDestinationTypeId = exportDestinationTypeId;
	}


	public String getExportDestinationTypeName() {
		return this.exportDestinationTypeName;
	}


	public void setExportDestinationTypeName(String exportDestinationTypeName) {
		this.exportDestinationTypeName = exportDestinationTypeName;
	}


	public Short getExportDestinationSubTypeId() {
		return this.exportDestinationSubTypeId;
	}


	public void setExportDestinationSubTypeId(Short exportDestinationSubTypeId) {
		this.exportDestinationSubTypeId = exportDestinationSubTypeId;
	}


	public String getExportDestinationSubTypeName() {
		return this.exportDestinationSubTypeName;
	}


	public void setExportDestinationSubTypeName(String exportDestinationSubTypeName) {
		this.exportDestinationSubTypeName = exportDestinationSubTypeName;
	}


	public Short getExportDestinationTypeIdNotEquals() {
		return this.exportDestinationTypeIdNotEquals;
	}


	public void setExportDestinationTypeIdNotEquals(Short exportDestinationTypeIdNotEquals) {
		this.exportDestinationTypeIdNotEquals = exportDestinationTypeIdNotEquals;
	}
}
