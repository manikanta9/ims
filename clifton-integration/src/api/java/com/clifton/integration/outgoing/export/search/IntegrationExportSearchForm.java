package com.clifton.integration.outgoing.export.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class IntegrationExportSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "integrationSource.id", sortField = "integrationSource.name")
	private Integer integrationSourceId;

	@SearchField(searchField = "integrationSource.name", comparisonConditions = {ComparisonConditions.EQUALS})
	private String integrationSourceName;

	@SearchField
	private String sourceSystemDefinitionName;

	@SearchField(comparisonConditions = {ComparisonConditions.EQUALS})
	private String sourceSystemIdentifier;

	@SearchField(searchField = "id", searchFieldPath = "integrationFileList")
	private Integer integrationFileId;

	// Custom Search Field
	private Integer exportDestinationId;

	@SearchField
	private Date receivedDate;

	@SearchField(searchField = "receivedDate", comparisonConditions = {ComparisonConditions.EQUALS_OR_IS_NULL})
	private Date receivedDateOrNull;

	@SearchField
	private Date sentDate;

	@SearchField(searchField = "sentDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean noSentDate;

	@SearchField(searchField = "name", searchFieldPath = "destinationType")
	private String destinationTypeName;


	@SearchField(searchField = "name", searchFieldPath = "status", comparisonConditions = {ComparisonConditions.IN})
	private String[] statusNameList;

	@SearchField
	private String messageSubject;

	@SearchField
	private String messageText;

	@SearchField
	private Integer retryCount;

	@SearchField
	private Integer retryDelayInSeconds;

	@SearchField
	private String filenamePrefix;


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getIntegrationSourceId() {
		return this.integrationSourceId;
	}


	public void setIntegrationSourceId(Integer integrationSourceId) {
		this.integrationSourceId = integrationSourceId;
	}


	public String getIntegrationSourceName() {
		return this.integrationSourceName;
	}


	public void setIntegrationSourceName(String integrationSourceName) {
		this.integrationSourceName = integrationSourceName;
	}


	public String getSourceSystemDefinitionName() {
		return this.sourceSystemDefinitionName;
	}


	public void setSourceSystemDefinitionName(String sourceSystemDefinitionName) {
		this.sourceSystemDefinitionName = sourceSystemDefinitionName;
	}


	public String getSourceSystemIdentifier() {
		return this.sourceSystemIdentifier;
	}


	public void setSourceSystemIdentifier(String sourceSystemIdentifier) {
		this.sourceSystemIdentifier = sourceSystemIdentifier;
	}


	public Date getReceivedDate() {
		return this.receivedDate;
	}


	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}


	public Date getReceivedDateOrNull() {
		return this.receivedDateOrNull;
	}


	public void setReceivedDateOrNull(Date receivedDateOrNull) {
		this.receivedDateOrNull = receivedDateOrNull;
	}


	public Date getSentDate() {
		return this.sentDate;
	}


	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}


	public Boolean getNoSentDate() {
		return this.noSentDate;
	}


	public void setNoSentDate(Boolean noSentDate) {
		this.noSentDate = noSentDate;
	}


	public Integer getExportDestinationId() {
		return this.exportDestinationId;
	}


	public void setExportDestinationId(Integer exportDestinationId) {
		this.exportDestinationId = exportDestinationId;
	}


	public Integer getIntegrationFileId() {
		return this.integrationFileId;
	}


	public void setIntegrationFileId(Integer integrationFileId) {
		this.integrationFileId = integrationFileId;
	}


	public String getDestinationTypeName() {
		return this.destinationTypeName;
	}


	public void setDestinationTypeName(String destinationTypeName) {
		this.destinationTypeName = destinationTypeName;
	}


	public String[] getStatusNameList() {
		return this.statusNameList;
	}


	public void setStatusNameList(String[] statusNameList) {
		this.statusNameList = statusNameList;
	}


	public String getMessageSubject() {
		return this.messageSubject;
	}


	public void setMessageSubject(String messageSubject) {
		this.messageSubject = messageSubject;
	}


	public String getMessageText() {
		return this.messageText;
	}


	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}


	public Integer getRetryCount() {
		return this.retryCount;
	}


	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}


	public Integer getRetryDelayInSeconds() {
		return this.retryDelayInSeconds;
	}


	public void setRetryDelayInSeconds(Integer retryDelayInSeconds) {
		this.retryDelayInSeconds = retryDelayInSeconds;
	}


	public String getFilenamePrefix() {
		return this.filenamePrefix;
	}


	public void setFilenamePrefix(String filenamePrefix) {
		this.filenamePrefix = filenamePrefix;
	}
}
