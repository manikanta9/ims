package com.clifton.integration.outgoing.export.destination.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationExportDestinationSubTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "name", searchFieldPath = "destinationType")
	private String destinationTypeName;

	@SearchField(searchField = "destinationType.id")
	private Short destinationTypeId;


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDestinationTypeName() {
		return this.destinationTypeName;
	}


	public void setDestinationTypeName(String destinationTypeName) {
		this.destinationTypeName = destinationTypeName;
	}


	public Short getDestinationTypeId() {
		return this.destinationTypeId;
	}


	public void setDestinationTypeId(Short destinationTypeId) {
		this.destinationTypeId = destinationTypeId;
	}
}
