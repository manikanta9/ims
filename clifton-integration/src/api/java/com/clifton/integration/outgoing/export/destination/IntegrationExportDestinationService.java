package com.clifton.integration.outgoing.export.destination;


import com.clifton.integration.outgoing.export.destination.search.IntegrationExportDestinationSearchForm;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportDestinationSubTypeSearchForm;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportDestinationTypeSearchForm;
import com.clifton.integration.outgoing.export.destination.search.IntegrationExportIntegrationExportDestinationSearchForm;

import java.util.List;


public interface IntegrationExportDestinationService {

	//////////////////////////////////////////////////////////////////////////////////////////
	//////                      Export  Type Business Methods                           //////
	//////////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExportDestinationType getIntegrationExportDestinationType(short id);


	public IntegrationExportDestinationSubType getIntegrationExportDestinationSubTypeByName(String name);


	public IntegrationExportDestinationType getIntegrationExportDestinationTypeByName(String name);


	public List<IntegrationExportDestinationType> getIntegrationExportDestinationTypeList(IntegrationExportDestinationTypeSearchForm searchForm);


	public IntegrationExportDestinationType saveIntegrationExportDestinationType(IntegrationExportDestinationType bean);


	//////////////////////////////////////////////////////////////////////////////////////////
	//////                      Export  SubType Business Methods                        //////
	//////////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExportDestinationSubType getIntegrationExportDestinationSubType(short id);


	public List<IntegrationExportDestinationSubType> getIntegrationExportDestinationSubTypeList(IntegrationExportDestinationSubTypeSearchForm searchForm);


	public IntegrationExportDestinationSubType saveIntegrationExportDestinationSubType(IntegrationExportDestinationSubType bean);


	//////////////////////////////////////////////////////////////////////////////////////////
	//////                   Export Destination Business Methods                        //////
	//////////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExportDestination getIntegrationExportDestination(int id);


	public IntegrationExportDestination getIntegrationExportDestinationByValue(short destinationTypeId, String destinationValue, String destinationUserName, String destinationText);

	public IntegrationExportDestination getIntegrationExportDestinationByValueAndFlags(IntegrationExportDestination integrationExportDestination);

	public List<IntegrationExportDestination> getIntegrationExportDestinationList(IntegrationExportDestinationSearchForm searchForm);


	public IntegrationExportDestination saveIntegrationExportDestination(IntegrationExportDestination bean);


	//////////////////////////////////////////////////////////////////////////////////////////
	//////      Integration Export Integration Export Destination Business Methods      //////
	//////////////////////////////////////////////////////////////////////////////////////////


	public IntegrationExportIntegrationExportDestination getIntegrationExportIntegrationExportDestination(int id);


	public List<IntegrationExportIntegrationExportDestination> getIntegrationExportIntegrationExportDestinationList(IntegrationExportIntegrationExportDestinationSearchForm searchForm);


	public IntegrationExportIntegrationExportDestination saveIntegrationExportIntegrationExportDestination(IntegrationExportIntegrationExportDestination bean);


	public void saveIntegrationExportIntegrationExportDestinationList(List<IntegrationExportIntegrationExportDestination> beanList);
}
