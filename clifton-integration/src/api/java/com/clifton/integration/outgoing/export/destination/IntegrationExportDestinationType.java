package com.clifton.integration.outgoing.export.destination;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.export.messaging.ExportDestinationTypes;


/**
 * The <code>IntegrationExportDestinationType</code> specifies the Type of
 * Destination for an Export.
 * <ul>
 * <li>EMAIL</li>
 * <li>FTP</li>
 * <li>PHONE</li>
 * <li>AWS_S3</li>
 * </ul>
 *
 * @author msiddiqui
 */
@CacheByName
public class IntegrationExportDestinationType extends NamedEntity<Short> {

	public ExportDestinationTypes getIntegrationExportDestinationTypeName() {
		return ExportDestinationTypes.valueOf(getName());
	}
}
