package com.clifton.integration.outgoing.export.destination;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.export.messaging.ExportDestinationSubTypes;


/**
 * The <code>IntegrationExportDestinationSubType</code> specifies the sub-type of
 * Destination for an Export.
 * <ul>
 * <li>FTP</li>
 * <li>FTPS</li>
 * <li>SFTP</li>
 * <li>EMAIL_FROM_ADDRESS</li>
 * <li>EMAIL_TO</li>
 * <li>EMAIL_CC</li>
 * <li>EMAIL_BCC</li>
 * </ul>
 *
 * @author msiddiqui
 */
@CacheByName
public class IntegrationExportDestinationSubType extends NamedEntity<Short> {

	private IntegrationExportDestinationType destinationType;


	public ExportDestinationSubTypes getIntegrationExportDestinationSubTypeName() {
		return ExportDestinationSubTypes.valueOf(getName());
	}


	public IntegrationExportDestinationType getDestinationType() {
		return this.destinationType;
	}


	public void setDestinationType(IntegrationExportDestinationType destinationType) {
		this.destinationType = destinationType;
	}
}
