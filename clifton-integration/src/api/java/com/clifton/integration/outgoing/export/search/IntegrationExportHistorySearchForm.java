package com.clifton.integration.outgoing.export.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>IntegrationExportHistorySearchForm</code> provides methods for searching Export
 * History.
 *
 * @author Masood Siddiqui
 */
public class IntegrationExportHistorySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "export.id")
	private Integer exportId;

	@SearchField(searchField = "exportStatus.id", sortField = "exportStatus.name")
	private Short exportStatusId;

	@SearchField(dateFieldIncludesTime = true)
	private Date startDate;

	@SearchField
	private String description;

	@SearchField(searchField = "sourceSystemDefinitionName", searchFieldPath = "export")
	private String sourceSystemDefinitionName;

	@SearchField(searchField = "integrationSource.id", searchFieldPath = "export")
	private Integer integrationSourceId;


	public Integer getExportId() {
		return this.exportId;
	}


	public void setExportId(Integer exportId) {
		this.exportId = exportId;
	}


	public Short getExportStatusId() {
		return this.exportStatusId;
	}


	public void setExportStatusId(Short exportStatusId) {
		this.exportStatusId = exportStatusId;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public String getSourceSystemDefinitionName() {
		return this.sourceSystemDefinitionName;
	}


	public void setSourceSystemDefinitionName(String sourceSystemDefinitionName) {
		this.sourceSystemDefinitionName = sourceSystemDefinitionName;
	}


	public Integer getIntegrationSourceId() {
		return this.integrationSourceId;
	}


	public void setIntegrationSourceId(Integer integrationSourceId) {
		this.integrationSourceId = integrationSourceId;
	}
}
