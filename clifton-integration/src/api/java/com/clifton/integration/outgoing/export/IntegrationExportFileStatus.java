package com.clifton.integration.outgoing.export;


import com.clifton.core.beans.NamedSimpleEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


@CacheByName
public class IntegrationExportFileStatus extends NamedSimpleEntity<Short> {

	public enum IntegrationExportFileStatusNames {
		PROCESSING, PROCESSED, FAILED
	}


	public IntegrationExportFileStatusNames getExportFileStatusName() {
		return IntegrationExportFileStatusNames.valueOf(getName());
	}
}
