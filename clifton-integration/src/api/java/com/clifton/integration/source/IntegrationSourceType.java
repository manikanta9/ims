package com.clifton.integration.source;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>IntegrationSourceType</code> defines the integration source type.  For now this is Import or Export.
 *
 * @author mwacker
 */
public class IntegrationSourceType extends NamedEntity<Short> {

	private boolean fileDefinitionRequired;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isFileDefinitionRequired() {
		return this.fileDefinitionRequired;
	}


	public void setFileDefinitionRequired(boolean fileDefinitionRequired) {
		this.fileDefinitionRequired = fileDefinitionRequired;
	}
}
