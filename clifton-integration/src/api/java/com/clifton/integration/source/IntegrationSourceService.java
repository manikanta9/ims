package com.clifton.integration.source;


import com.clifton.integration.source.search.IntegrationSourceSearchForm;
import com.clifton.integration.source.search.IntegrationSourceTypeSearchForm;

import java.util.List;


public interface IntegrationSourceService {

	////////////////////////////////////////////////////////////////////////////
	////////          IntegrationSource Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////
	public IntegrationSource getIntegrationSource(int id);


	public IntegrationSource getIntegrationSourceByName(String name);


	public IntegrationSource getIntegrationSourceByFtpRootFolder(String ftpRootFolder);


	public List<IntegrationSource> getIntegrationSourceList(final IntegrationSourceSearchForm searchForm);


	public IntegrationSource saveIntegrationSource(IntegrationSource bean);


	public void deleteIntegrationSource(int id);


	////////////////////////////////////////////////////////////////////////////
	////////        IntegrationSourceType Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////
	public IntegrationSourceType getIntegrationSourceType(short id);


	public List<IntegrationSourceType> getIntegrationSourceTypeList(final IntegrationSourceTypeSearchForm searchForm);
}
