package com.clifton.integration.source;


import com.clifton.core.beans.NamedEntity;
import com.clifton.integration.file.archive.IntegrationFileArchiveStrategy;
import com.clifton.security.secret.SecuritySecret;


/**
 * The IntegrationSource class defines where a particular file comes from and which folders it is associated with.
 */
public class IntegrationSource extends NamedEntity<Integer> {

	private IntegrationSourceType type;

	/**
	 * The id of the company from which the file was received.  The id correlates to the id in the BusinessCompany table in IMS.
	 */
	private Integer sourceCompanyId;

	/**
	 * Public key to be used for encrypting outgoing data
	 */
	private String encryptionPublicKey;

	/**
	 * Public key used for decrypting data
	 */
	private String decryptionPublicKey;

	private SecuritySecret decryptionPrivateKeySecuritySecret;

	private SecuritySecret decryptionPrivateKeyPasswordSecuritySecret;

	/**
	 * Additional folder path to add to base archive dir where source files will be archived.
	 */
	private String additionalArchivePath;

	/**
	 * The root ftp folder where files from this source arrive.
	 */
	private String ftpRootFolder;

	/**
	 * Enable watching for files in the ftp directory.
	 */
	private boolean ftpWatchingEnabled;

	/**
	 * Defines how the files will be archived.
	 */
	private IntegrationFileArchiveStrategy fileArchiveStrategy;

	/**
	 * Indicates that all files received from the source are encrypted and should be decrypted regardless of extension.
	 */
	private boolean forceDecryption;
	/**
	 * If true, the effective date will be load from the file using 'effectiveDateJobFileName', if it exists, even if
	 * the file name has an effective date.
	 */
	private boolean forceEffectiveDateLoadFromJobFile;

	/**
	 * If true, a watcher will be added to the input folder on a subdirectory matching the name of the Source Name
	 */
	private boolean sourceInputFolderWatchingEnabled;

	/**
	 * Determines whether to expect sections of the JMS message to contain data that was encrypted using Integration's Public Encryption Key
	 */
	private boolean encryptedDataSendingEnabled;

	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////


	public Integer getSourceCompanyId() {
		return this.sourceCompanyId;
	}


	public void setSourceCompanyId(Integer sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}


	public String getEncryptionPublicKey() {
		return this.encryptionPublicKey;
	}


	public void setEncryptionPublicKey(String encryptionPublicKey) {
		this.encryptionPublicKey = encryptionPublicKey;
	}


	public String getDecryptionPublicKey() {
		return this.decryptionPublicKey;
	}


	public void setDecryptionPublicKey(String decryptionPublicKey) {
		this.decryptionPublicKey = decryptionPublicKey;
	}


	public SecuritySecret getDecryptionPrivateKeySecuritySecret() {
		return this.decryptionPrivateKeySecuritySecret;
	}


	public void setDecryptionPrivateKeySecuritySecret(SecuritySecret decryptionPrivateKeySecuritySecret) {
		this.decryptionPrivateKeySecuritySecret = decryptionPrivateKeySecuritySecret;
	}


	public SecuritySecret getDecryptionPrivateKeyPasswordSecuritySecret() {
		return this.decryptionPrivateKeyPasswordSecuritySecret;
	}


	public void setDecryptionPrivateKeyPasswordSecuritySecret(SecuritySecret decryptionPrivateKeyPasswordSecuritySecret) {
		this.decryptionPrivateKeyPasswordSecuritySecret = decryptionPrivateKeyPasswordSecuritySecret;
	}


	public String getAdditionalArchivePath() {
		return this.additionalArchivePath;
	}


	public void setAdditionalArchivePath(String additionalArchivePath) {
		this.additionalArchivePath = additionalArchivePath;
	}


	public String getFtpRootFolder() {
		return this.ftpRootFolder;
	}


	public void setFtpRootFolder(String ftpRootFolder) {
		this.ftpRootFolder = ftpRootFolder;
	}


	public IntegrationFileArchiveStrategy getFileArchiveStrategy() {
		return this.fileArchiveStrategy;
	}


	public void setFileArchiveStrategy(IntegrationFileArchiveStrategy fileArchiveStrategy) {
		this.fileArchiveStrategy = fileArchiveStrategy;
	}


	public boolean isFtpWatchingEnabled() {
		return this.ftpWatchingEnabled;
	}


	public void setFtpWatchingEnabled(boolean ftpWatchingEnabled) {
		this.ftpWatchingEnabled = ftpWatchingEnabled;
	}


	public boolean isForceDecryption() {
		return this.forceDecryption;
	}


	public void setForceDecryption(boolean forceDecryption) {
		this.forceDecryption = forceDecryption;
	}


	public IntegrationSourceType getType() {
		return this.type;
	}


	public void setType(IntegrationSourceType type) {
		this.type = type;
	}


	public boolean isForceEffectiveDateLoadFromJobFile() {
		return this.forceEffectiveDateLoadFromJobFile;
	}


	public void setForceEffectiveDateLoadFromJobFile(boolean forceEffectiveDateLoadFromJobFile) {
		this.forceEffectiveDateLoadFromJobFile = forceEffectiveDateLoadFromJobFile;
	}


	public boolean isSourceInputFolderWatchingEnabled() {
		return this.sourceInputFolderWatchingEnabled;
	}


	public void setSourceInputFolderWatchingEnabled(boolean sourceInputFolderWatchingEnabled) {
		this.sourceInputFolderWatchingEnabled = sourceInputFolderWatchingEnabled;
	}


	public boolean isEncryptedDataSendingEnabled() {
		return this.encryptedDataSendingEnabled;
	}


	public void setEncryptedDataSendingEnabled(boolean encryptedDataSendingEnabled) {
		this.encryptedDataSendingEnabled = encryptedDataSendingEnabled;
	}
}
