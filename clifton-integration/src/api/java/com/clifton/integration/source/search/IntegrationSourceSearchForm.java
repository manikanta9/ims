package com.clifton.integration.source.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class IntegrationSourceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Integer sourceCompanyId;

	@SearchField
	private String additionalArchivePath;

	@SearchField
	private String ftpRootFolder;

	@SearchField(searchField = "name", searchFieldPath = "fileArchiveStrategy")
	private String fileArchiveStrategyName;

	@SearchField(searchField = "fileArchiveStrategy.id")
	private Integer fileArchiveStrategyId;

	@SearchField(searchField = "ftpRootFolder", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean ftpRootFolderNotNull;

	@SearchField
	private Boolean forceDecryption;

	@SearchField
	private Boolean ftpWatchingEnabled;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField
	private Boolean sourceInputFolderWatchingEnabled;

	@SearchField(searchField = "ftpWatchingEnabled,sourceInputFolderWatchingEnabled", searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Boolean folderWatchingEnabled;

	@SearchField(searchField = "decryptionPrivateKeySecret", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean sourceHasDecryptionPrivateKeySecret;

	@SearchField(searchField = "decryptionPrivateKeyPasswordSecret", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean sourceHasDecryptionPrivateKeyPasswordSecret;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getFtpRootFolderNotNull() {
		return this.ftpRootFolderNotNull;
	}


	public void setFtpRootFolderNotNull(Boolean ftpRootFolderNotNull) {
		this.ftpRootFolderNotNull = ftpRootFolderNotNull;
	}


	public Integer getSourceCompanyId() {
		return this.sourceCompanyId;
	}


	public void setSourceCompanyId(Integer sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}


	public String getAdditionalArchivePath() {
		return this.additionalArchivePath;
	}


	public void setAdditionalArchivePath(String additionalArchivePath) {
		this.additionalArchivePath = additionalArchivePath;
	}


	public String getFtpRootFolder() {
		return this.ftpRootFolder;
	}


	public void setFtpRootFolder(String ftpRootFolder) {
		this.ftpRootFolder = ftpRootFolder;
	}


	public Integer getFileArchiveStrategyId() {
		return this.fileArchiveStrategyId;
	}


	public void setFileArchiveStrategyId(Integer fileArchiveStrategyId) {
		this.fileArchiveStrategyId = fileArchiveStrategyId;
	}


	public Boolean getFtpWatchingEnabled() {
		return this.ftpWatchingEnabled;
	}


	public void setFtpWatchingEnabled(Boolean ftpWatchingEnabled) {
		this.ftpWatchingEnabled = ftpWatchingEnabled;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getFileArchiveStrategyName() {
		return this.fileArchiveStrategyName;
	}


	public void setFileArchiveStrategyName(String fileArchiveStrategyName) {
		this.fileArchiveStrategyName = fileArchiveStrategyName;
	}


	public Boolean getForceDecryption() {
		return this.forceDecryption;
	}


	public void setForceDecryption(Boolean forceDecryption) {
		this.forceDecryption = forceDecryption;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getSourceInputFolderWatchingEnabled() {
		return this.sourceInputFolderWatchingEnabled;
	}


	public void setSourceInputFolderWatchingEnabled(Boolean sourceInputFolderWatchingEnabled) {
		this.sourceInputFolderWatchingEnabled = sourceInputFolderWatchingEnabled;
	}


	public Boolean getFolderWatchingEnabled() {
		return this.folderWatchingEnabled;
	}


	public void setFolderWatchingEnabled(Boolean folderWatchingEnabled) {
		this.folderWatchingEnabled = folderWatchingEnabled;
	}


	public Boolean getSourceHasDecryptionPrivateKeySecret() {
		return this.sourceHasDecryptionPrivateKeySecret;
	}


	public void setSourceHasDecryptionPrivateKeySecret(Boolean sourceHasDecryptionPrivateKeySecret) {
		this.sourceHasDecryptionPrivateKeySecret = sourceHasDecryptionPrivateKeySecret;
	}


	public Boolean getSourceHasDecryptionPrivateKeyPasswordSecret() {
		return this.sourceHasDecryptionPrivateKeyPasswordSecret;
	}


	public void setSourceHasDecryptionPrivateKeyPasswordSecret(Boolean sourceHasDecryptionPrivateKeyPasswordSecret) {
		this.sourceHasDecryptionPrivateKeyPasswordSecret = sourceHasDecryptionPrivateKeyPasswordSecret;
	}
}
