package com.clifton.batch;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BatchProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "batch";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("job");
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
