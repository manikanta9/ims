package com.clifton.batch.runner;


import com.clifton.batch.job.BatchJob;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BatchTestRunnerProvider</code> class extends the ThreadPoolRunnerHandler,
 * however hard-codes a given Date/Time for testing purposes.
 *
 * @author manderson
 */
public class BatchTestRunnerProvider extends BatchRunnerProvider {

	private ReadOnlyDAO<BatchJob> batchJobDAO;


	@Override
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		startDateTime = DateUtils.toDate("10/01/2009 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		endDateTime = DateUtils.toDate("10/01/2009 9:00 AM", DateUtils.DATE_FORMAT_SHORT);

		List<BatchJob> activeJobs = this.batchJobDAO.findByField("enabled", true);
		List<Runner> results = new ArrayList<>();
		for (BatchJob batchJob : activeJobs) {
			if (batchJob.getSchedule() != null) {
				List<Date> occurrences = getScheduleApiService().getScheduleOccurrences(ScheduleOccurrenceCommand.forOccurrencesBetween(batchJob.getSchedule().getId(), startDateTime, endDateTime));
				for (Date date : occurrences) {
					BatchRunner runner = getBatchRunnerFactory().createChildBatchRunner(date, batchJob.getId());
					results.add(runner);
				}
			}
		}
		return results;
	}


	public ReadOnlyDAO<BatchJob> getBatchJobDAO() {
		return this.batchJobDAO;
	}


	public void setBatchJobDAO(ReadOnlyDAO<BatchJob> batchJobDAO) {
		this.batchJobDAO = batchJobDAO;
	}
}
