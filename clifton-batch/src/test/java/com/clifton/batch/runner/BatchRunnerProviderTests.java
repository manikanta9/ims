package com.clifton.batch.runner;

import com.clifton.batch.job.BatchJob;
import com.clifton.batch.job.BatchJobService;
import com.clifton.batch.job.search.BatchJobSearchForm;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.runner.config.RunnerConfigService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BatchRunnerProviderTests {

	@Resource
	private BatchRunnerProvider batchRunnerProvider;
	@Resource
	private BatchJobService batchJobService;
	@Resource
	private CalendarScheduleService calendarScheduleService;
	@Resource
	private RunnerHandler runnerHandler;
	@Resource
	private RunnerConfigService runnerConfigService;


	@Disabled
	public void testRescheduleOnUpdateBatchJob() {
		synchronized (this.runnerConfigService) {
			// Setup Scheduler
			this.runnerHandler.addRunnerProvider(this.batchRunnerProvider);
			this.runnerConfigService.restartRunnerHandler();

			// Initial Scheduling should have 1 batch job with 3 scheduled times
			List<Runner> runners = attemptGetSchedulerRunnerList(3);
			verifyRunner(runners, 1, "10/01/2009 8:00 AM");
			verifyRunner(runners, 1, "10/01/2009 8:30 AM");
			verifyRunner(runners, 1, "10/01/2009 9:00 AM");

			// Update the Batch Job Schedule
			BatchJob job = this.batchJobService.getBatchJob(1);
			job.setSchedule(this.calendarScheduleService.getCalendarSchedule(102));
			job = this.batchJobService.saveBatchJob(job);

			// Make sure scheduler is updated
			runners = attemptGetSchedulerRunnerList(2);
			verifyRunner(runners, 1, "10/01/2009 8:00 AM");
			verifyRunner(runners, 1, "10/01/2009 8:45 AM");

			// Add a new Batch Job
			BatchJob newJob = BeanUtils.cloneBean(job, false, false);
			newJob.setSchedule(this.calendarScheduleService.getCalendarSchedule(101));
			newJob = this.batchJobService.saveBatchJob(newJob);

			// Verify both original batch job and new batch job are in the scheduler
			runners = attemptGetSchedulerRunnerList(5);
			verifyRunner(runners, 1, "10/01/2009 8:00 AM");
			verifyRunner(runners, 1, "10/01/2009 8:45 AM");
			verifyRunner(runners, newJob.getId(), "10/01/2009 8:00 AM");
			verifyRunner(runners, newJob.getId(), "10/01/2009 8:30 AM");
			verifyRunner(runners, newJob.getId(), "10/01/2009 9:00 AM");

			// Disable the most recently added Batch Job
			newJob.setEnabled(false);
			newJob = this.batchJobService.saveBatchJob(newJob);

			// Scheduler should just have first job with 2 occurrences
			runners = attemptGetSchedulerRunnerList(2);
			verifyRunner(runners, 1, "10/01/2009 8:00 AM");
			verifyRunner(runners, 1, "10/01/2009 8:45 AM");
		}
	}


	@Test
	public void testBatchRunnerProvider() {
		BatchJobService mockBatchJobService = Mockito.mock(BatchJobService.class);
		List<BatchJob> batchJobs = this.batchJobService.getBatchJobList(new BatchJobSearchForm());
		Mockito.when(mockBatchJobService.getBatchJobListActiveOnDate(DateUtils.toDate("10/01/2009 8:00 AM", DateUtils.DATE_FORMAT_SHORT))).thenReturn(batchJobs);

		this.batchRunnerProvider.setBatchJobService(mockBatchJobService);

		Date startDate = DateUtils.toDate("10/01/2009 8:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date endDate = DateUtils.toDate("10/01/2009 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		List<Runner> runners = this.batchRunnerProvider.getOccurrencesBetween(startDate, endDate);
		Assertions.assertEquals(3, runners.size());

		Assertions.assertEquals("BATCH-1", runners.get(0).getType() + "-" + runners.get(0).getTypeId());
		Assertions.assertEquals("BATCH-1", runners.get(1).getType() + "-" + runners.get(1).getTypeId());
		Assertions.assertEquals("BATCH-1", runners.get(2).getType() + "-" + runners.get(2).getTypeId());

		Assertions.assertEquals("10/01/2009 8:00 AM", DateUtils.fromDate(runners.get(0).getRunDate(), DateUtils.DATE_FORMAT_SHORT));
		Assertions.assertEquals("10/01/2009 8:30 AM", DateUtils.fromDate(runners.get(1).getRunDate(), DateUtils.DATE_FORMAT_SHORT));
		Assertions.assertEquals("10/01/2009 9:00 AM", DateUtils.fromDate(runners.get(2).getRunDate(), DateUtils.DATE_FORMAT_SHORT));
	}


	private List<Runner> getSchedulerRunnerList(int expectedSize) {
		try {
			Thread.sleep(1000);
			List<Runner> runners = this.runnerHandler.getScheduledRunnerList("BATCH");
			Assertions.assertEquals(expectedSize, CollectionUtils.getSize(runners));
			//System.out.println("Runners: " + CollectionUtils.getSize(runners));
			//for (Runner runner : CollectionUtils.getIterable(runners)) {
			//	System.out.println(runner.getType() + "-" + runner.getTypeId());
			//	System.out.println(DateUtils.fromDate(runner.getRunDate(), DateUtils.DATE_FORMAT_SHORT));
			//}
			return runners;
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	private List<Runner> attemptGetSchedulerRunnerList(int expectedSize) {
		int attemptCount = 0;

		while (true) {

			try {
				return getSchedulerRunnerList(expectedSize);
			}
			catch (Exception e) {
				if (attemptCount < 30) {
					attemptCount++;
				}
				else {
					throw new RuntimeException(e);
				}
			}
		}
	}


	private void verifyRunner(List<Runner> runners, int typeId, String date) {
		String typeLabel = "BATCH-" + typeId;
		for (Runner runner : CollectionUtils.getIterable(runners)) {
			if (typeLabel.equals(runner.getType() + "-" + runner.getTypeId())) {
				if (date.equals(DateUtils.fromDate(runner.getRunDate(), DateUtils.DATE_FORMAT_SHORT))) {
					return;
				}
			}
		}
		throw new RuntimeException("Cannot find Runner [" + typeLabel + "] for run date [" + date + "]");
	}
}
