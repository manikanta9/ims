package com.clifton.batch.runner;

import com.clifton.batch.job.BatchJob;
import com.clifton.batch.job.BatchJobHistory;
import com.clifton.batch.job.BatchJobService;
import com.clifton.batch.job.BatchJobStatuses;
import com.clifton.batch.job.search.BatchJobHistorySearchForm;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BatchRunnerTests {

	@Resource
	private ReadOnlyDAO<BatchJob> batchJobDAO;
	@Resource
	private ReadOnlyDAO<BatchJobHistory> batchJobHistoryDAO;
	@Resource
	private BatchJobService batchJobService;
	@Resource
	private BatchRunnerService batchRunnerService;
	@Resource
	private BatchRunnerFactory batchRunnerFactory;
	@Resource
	private ContextHandler contextHandler;
	@Resource
	private SecurityUserService securityUserService;


	@BeforeEach
	public void resetTests() {
		// set the current user which is used by batch runner
		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "TestUser");
	}


	@Test
	public void testSingleJobCompletionWithNoResults() {
		Date runDate = new Date();
		BatchJob batchJob = run(1, runDate);

		BatchJobHistory batchJobHistory = this.batchJobService.getBatchJobHistoryByDate(batchJob.getId(), runDate);

		Assertions.assertEquals(BatchJobStatuses.COMPLETED_WITH_NO_RESULTS, batchJob.getStatus());
		Assertions.assertEquals(BatchJobStatuses.COMPLETED_WITH_NO_RESULTS, batchJobHistory.getStatus());

		BatchJobHistorySearchForm searchForm = new BatchJobHistorySearchForm();
		searchForm.setBatchJobId(batchJob.getId());
		searchForm.setOrderBy("startDate");
		List<BatchJobHistory> batchJobHistoryList = this.batchJobService.getBatchJobHistoryList(searchForm);
		Assertions.assertEquals(1, batchJobHistoryList.size());

		searchForm = new BatchJobHistorySearchForm();
		searchForm.setParentId(batchJobHistoryList.get(0).getId());
		batchJobHistoryList = this.batchJobService.getBatchJobHistoryList(searchForm);
		Assertions.assertNull(batchJobHistoryList);
	}


	@Test
	public void testSingleJobThatFails() {
		Date runDate = new Date();
		BatchJob batchJob = run(2, runDate);
		BatchJobHistory batchJobHistory = this.batchJobService.getBatchJobHistoryByDate(batchJob.getId(), runDate);

		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJob.getStatus());
		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJobHistory.getStatus());
	}


	@Test
	public void testSingleJobThatFailsWithRetry() {
		Date runDate = new Date();
		BatchJob batchJob = run(16, runDate);
		BatchJobHistorySearchForm sf = new BatchJobHistorySearchForm();
		sf.setBatchJobId(16);
		List<BatchJobHistory> batchJobHistoryList = this.batchJobService.getBatchJobHistoryList(sf);

		Assertions.assertEquals(4, batchJobHistoryList.size());

		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJob.getStatus());
		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJobHistoryList.get(0).getStatus());
		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJobHistoryList.get(1).getStatus());
		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJobHistoryList.get(2).getStatus());
		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJobHistoryList.get(3).getStatus());

		Assertions.assertEquals(new Short((short) 0), batchJobHistoryList.get(0).getRetryAttemptNumber());
		Assertions.assertEquals(new Short((short) 1), batchJobHistoryList.get(1).getRetryAttemptNumber());
		Assertions.assertEquals(new Short((short) 2), batchJobHistoryList.get(2).getRetryAttemptNumber());
		Assertions.assertEquals(new Short((short) 3), batchJobHistoryList.get(3).getRetryAttemptNumber());
	}


	@Test
	public void testJobWith2StepsAndSuccessfulResults() {
		Date runDate = new Date();
		BatchJobHistorySearchForm searchForm = new BatchJobHistorySearchForm();
		int historyCountBefore = CollectionUtils.getSize(this.batchJobService.getBatchJobHistoryList(searchForm));

		BatchJob batchJob = run(3, runDate);
		BatchJobHistory batchJobHistory = this.batchJobService.getBatchJobHistoryByDate(batchJob.getId(), runDate);

		Assertions.assertEquals(BatchJobStatuses.COMPLETED, batchJob.getStatus());
		Assertions.assertEquals(BatchJobStatuses.COMPLETED, batchJobHistory.getStatus());

		int historyCountAfter = CollectionUtils.getSize(this.batchJobService.getBatchJobHistoryList(searchForm));
		Assertions.assertEquals(historyCountBefore + 3, historyCountAfter);

		searchForm.setParentId(batchJobHistory.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(this.batchJobService.getBatchJobHistoryList(searchForm)));
	}


	@Test
	public void testJobWith3ChildrenThatAllowsChildFailure() {
		// third child fails but it's configured not to stop the main job
		Date runDate = new Date();
		BatchJob batchJob = run(4, runDate);
		BatchJobHistory batchJobHistory = this.batchJobService.getBatchJobHistoryByDate(batchJob.getId(), runDate);

		Assertions.assertEquals(BatchJobStatuses.COMPLETED_WITH_ERRORS, batchJob.getStatus());
		Assertions.assertEquals(BatchJobStatuses.COMPLETED_WITH_ERRORS, batchJobHistory.getStatus());
	}


	@Test
	public void testJobWith3ChildrenThatDoNotAllowsChildFailure() {
		// third job fails and it's configured to fail the main job
		Date runDate = new Date();

		BatchJob batchJob = run(5, runDate);
		BatchJobHistory batchJobHistory = this.batchJobService.getBatchJobHistoryByDate(batchJob.getId(), runDate);

		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJob.getStatus());
		Assertions.assertEquals(BatchJobStatuses.FAILED, batchJobHistory.getStatus());

		BatchJobHistorySearchForm searchForm = new BatchJobHistorySearchForm();
		searchForm.setParentId(batchJobHistory.getId());
		List<BatchJobHistory> batchJobHistoryList = this.batchJobService.getBatchJobHistoryList(searchForm);
		Assertions.assertEquals(3, batchJobHistoryList.size());
	}


	@Test
	public void testJobWithRunAsUser() {
		Date runDate = new Date();

		BatchJob batchJob = run(15, runDate);
		Assertions.assertEquals(BatchJobStatuses.COMPLETED_WITH_NO_RESULTS, batchJob.getStatus());
	}


	@Test
	public void testRunJobNow() {
		int countBefore = CollectionUtils.getSize(this.batchJobHistoryDAO.findByField("batchJob.id", 6));
		this.batchRunnerService.runBatchJob(3);
		Assertions.assertEquals(countBefore + 1, CollectionUtils.getSize(this.batchJobHistoryDAO.findByField("batchJob.id", 3)));
	}


	@Test
	public void testDeleteJob() {
		this.batchRunnerService.runBatchJob(3);
		List<BatchJobHistory> originalHistoryList = this.batchJobHistoryDAO.findByField("parent.id", 7);
		this.batchJobService.deleteBatchJob(3);

		BatchJobHistorySearchForm searchForm = new BatchJobHistorySearchForm();
		searchForm.setBatchJobId(3);
		List<BatchJobHistory> historyList = this.batchJobService.getBatchJobHistoryList(searchForm);
		Assertions.assertTrue(CollectionUtils.isEmpty(historyList));

		// only checking 1 level deep maybe deeper later
		for (BatchJobHistory originalHistory : CollectionUtils.getIterable(originalHistoryList)) {
			historyList = this.batchJobHistoryDAO.findByField("parent.id", originalHistory.getId());
			Assertions.assertNull(historyList);
		}
	}


	@Test
	public void testExecute_WithCondition_False() {
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> {
			this.batchRunnerService.runBatchJob(17);
		});
		Assertions.assertEquals("You do not have permission to run this batch job because: FALSE", ve.getMessage());
	}


	@Test
	public void testExecute_Admin_WithCondition_False() {
		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "AdminUser");
		this.batchRunnerService.runBatchJob(17);
	}


	@Test
	public void testExecute_WithCondition_True() {
		this.batchRunnerService.runBatchJob(18);
	}


	private BatchJob run(int id, Date runDate) {
		BatchRunner runner = this.batchRunnerFactory.createChildBatchRunner(runDate, id);
		runner.run();
		// job 14 has no system bean or child jobs so no context is created because it is never executed
		if (id != 14) {
			Assertions.assertEquals("Dude what the!", runner.getRunContext().get("entry1"));
		}
		if ((id == 3) || (id == 6) || (id == 7)) {
			BatchJobHistory history = this.batchJobHistoryDAO.findOneByField("batchJob.id", 3);
			Assertions.assertEquals("results for id 3", history.getDescription());
		}
		return this.batchJobDAO.findByPrimaryKey(id);
	}
}
