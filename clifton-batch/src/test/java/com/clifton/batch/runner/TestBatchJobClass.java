package com.clifton.batch.runner;


import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;

import java.util.Map;


public class TestBatchJobClass implements Task {

	private boolean fail = false;


	@Override
	public Status run(Map<String, Object> context) {
		context.put("entry1", "Dude what the!");
		Integer jobId = (Integer) context.get("currentBatchJobId");
		if (jobId.equals(3)) {
			return Status.ofMessage("results for id 3");
		}
		if (isFail()) {
			throw new RuntimeException("test");
		}
		return Status.ofMessage("Other Result", false);
	}


	public boolean isFail() {
		return this.fail;
	}


	public void setFail(boolean fail) {
		this.fail = fail;
	}
}
