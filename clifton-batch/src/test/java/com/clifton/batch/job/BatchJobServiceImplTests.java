package com.clifton.batch.job;


import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.ExceptionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BatchJobServiceImplTests {


	@Resource
	private BatchJobService batchJobService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBatchJobStatusUpdate() {
		BatchJob batchJob = this.batchJobService.getBatchJob(1);

		validateBatchJobStatusUpdate(batchJob, BatchJobStatuses.NONE, null);

		validateBatchJobStatusUpdate(batchJob, BatchJobStatuses.RUNNING, null);

		validateBatchJobStatusUpdate(batchJob, BatchJobStatuses.RUNNING, "Batch Job [Test Job 1] status is already RUNNING.");

		validateBatchJobStatusUpdate(batchJob, BatchJobStatuses.NONE, "Batch Job [Test Job 1] status of NONE should only apply to jobs that have never ran.");

		validateBatchJobStatusUpdate(batchJob, BatchJobStatuses.COMPLETED_WITH_ERRORS, null);

		validateBatchJobStatusUpdate(batchJob, BatchJobStatuses.COMPLETED, "Batch Job [Test Job 1] status must be RUNNING in order to update to status COMPLETED.");

		validateBatchJobStatusUpdate(batchJob, BatchJobStatuses.FAILED, "Batch Job [Test Job 1] status must be RUNNING in order to update to status FAILED.");
	}


	private void validateBatchJobStatusUpdate(BatchJob batchJob, BatchJobStatuses status, String expectedErrorMessage) {
		String errorMessage = null;
		try {
			this.batchJobService.updateBatchJobStatus(batchJob.getId(), status);
		}
		catch (Exception e) {
			errorMessage = ExceptionUtils.getOriginalMessage(e);
		}
		Assertions.assertEquals(expectedErrorMessage, errorMessage);
	}
}
