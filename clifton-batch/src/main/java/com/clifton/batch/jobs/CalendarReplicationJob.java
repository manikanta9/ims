package com.clifton.batch.jobs;

import com.clifton.calendar.holiday.CalendarHoliday;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarDay;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.web.api.client.locator.ExternalServiceLocator;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemSchemaService;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The CalendarReplicationJob batch job replicates specified Calendars and corresponding Holidays from a source system
 * to one or more target systems. It will create missing Calendars and Holidays in target system(s) and will
 * insert/delete holidays to make sure that all target systems match the source system data.
 *
 * @author vgomelsky
 */
public class CalendarReplicationJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	private short sourceDataSourceId;
	private Short[] sourceCalendarIds;
	private Short[] targetDataSourceIds;


	private StatusHolderObject<Status> statusHolder;


	private ExternalServiceLocator externalServiceLocator;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() throws ValidationException {
		// validate that all data sources and calendars exist
		SystemDataSource sourceDataSource = getSystemSchemaService().getSystemDataSource(getSourceDataSourceId());
		CalendarSetupService sourceCalendarService = getExternalServiceLocator().getExternalService(CalendarSetupService.class, sourceDataSource.getName());
		getExternalServiceLocator().getExternalService(CalendarHolidayService.class, sourceDataSource.getName());
		for (short calendarId : getSourceCalendarIds()) {
			if (sourceCalendarService.getCalendar(calendarId) == null) {
				throw new ValidationException("Cannot find source calendar with id = " + calendarId);
			}
		}

		// validate that all target data source have necessary Calendar services
		for (short dataSourceId : getTargetDataSourceIds()) {
			SystemDataSource targetDataSource = getSystemSchemaService().getSystemDataSource(dataSourceId);
			getExternalServiceLocator().getExternalService(CalendarSetupService.class, targetDataSource.getName());
			getExternalServiceLocator().getExternalService(CalendarHolidayService.class, targetDataSource.getName());
		}

		// validate that source and target data sources are not the same
		if (ArrayUtils.contains(getTargetDataSourceIds(), getSourceDataSourceId())) {
			throw new FieldValidationException("Source and Target Data Sources cannot be the same.", "sourceDataSourceId");
		}
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolder.getStatus();
		status.setMessage(null); // details will be appended for each replication

		SystemDataSource sourceDataSource = getSystemSchemaService().getSystemDataSource(getSourceDataSourceId());
		CalendarSetupService sourceCalendarService = getExternalServiceLocator().getExternalService(CalendarSetupService.class, sourceDataSource.getName());
		CalendarHolidayService sourceHolidayService = getExternalServiceLocator().getExternalService(CalendarHolidayService.class, sourceDataSource.getName());

		Map<Date, CalendarDay> targetDayCache = new HashMap<>();
		Map<String, CalendarHoliday> targetHolidayCache = new HashMap<>();
		for (short calendarId : getSourceCalendarIds()) {
			Calendar sourceCalendar = sourceCalendarService.getCalendar(calendarId);
			if (sourceCalendar == null) {
				status.addError("Cannot find source Calendar with id = " + calendarId + " for Data Source: " + sourceDataSource);
			}
			else {
				List<CalendarHolidayDay> holidayList = sourceHolidayService.getCalendarHolidayDayListByCalendar(calendarId);
				for (short targetDataSourceId : getTargetDataSourceIds()) {
					try {
						SystemDataSource targetDataSource = getSystemSchemaService().getSystemDataSource(targetDataSourceId);
						replicateCalendar(sourceCalendar, holidayList, targetDataSource, targetDayCache, targetHolidayCache, status);
					}
					catch (Throwable e) {
						// continue processing if there is an error
						status.addError("Error replicating calendar data for " + sourceCalendar + ": " + ExceptionUtils.getDetailedMessage(e));
						LogUtils.error(getClass(), "Error replicating calendar data for " + sourceCalendar, e);
					}
				}
			}
		}

		return status;
	}


	private void replicateCalendar(Calendar sourceCalendar, List<CalendarHolidayDay> sourceHolidayList, SystemDataSource targetDataSource, Map<Date, CalendarDay> dayCache, Map<String, CalendarHoliday> holidayCache, Status status) {
		CalendarSetupService targetCalendarService = getExternalServiceLocator().getExternalService(CalendarSetupService.class, targetDataSource.getName());
		Calendar targetCalendar = targetCalendarService.getCalendarByName(sourceCalendar.getName());
		if (targetCalendar == null) {
			// need to create new target calendar
			targetCalendar = new Calendar();
			targetCalendar.setName(sourceCalendar.getName());
			targetCalendar.setDescription(sourceCalendar.getDescription());
			targetCalendar.setCode(sourceCalendar.getCode());
			targetCalendar.setSystemDefined(sourceCalendar.isSystemDefined());
			targetCalendar = targetCalendarService.saveCalendar(targetCalendar);
		}

		CalendarHolidayService targetHolidayService = getExternalServiceLocator().getExternalService(CalendarHolidayService.class, targetDataSource.getName());
		List<CalendarHolidayDay> targetHolidayList = targetHolidayService.getCalendarHolidayDayListByCalendar(targetCalendar.getId());

		Map<Long, CalendarHolidayDay> sourceMap = BeanUtils.getBeanMapUnique(sourceHolidayList, holiday -> holiday.getDay().getStartDate().getTime());
		Map<Long, CalendarHolidayDay> targetMap = BeanUtils.getBeanMapUnique(targetHolidayList, holiday -> holiday.getDay().getStartDate().getTime());
		// remove common elements
		sourceMap.keySet().removeIf(time -> targetMap.remove(time) != null);

		Collection<CalendarHolidayDay> insertList = sourceMap.values();
		for (CalendarHolidayDay sourceDay : insertList) {
			CalendarHolidayDay targetDay = new CalendarHolidayDay();
			targetDay.setCalendar(targetCalendar);
			targetDay.setDay(dayCache.computeIfAbsent(sourceDay.getDay().getStartDate(), date -> {
				CalendarDay day = targetCalendarService.getCalendarDayByDate(date);
				if (day == null) {
					// the day may not be present in the target system because the year is missing: try to create it
					targetCalendarService.saveCalendarYearForYear((short) DateUtils.getYear(date));
					day = targetCalendarService.getCalendarDayByDate(date);
					if (day == null) {
						throw new ValidationException("Cannot find CalendarDay for " + date + " in " + targetDataSource);
					}
				}
				return day;
			}));
			targetDay.setHoliday(getTargetHolidayDay(sourceDay.getHoliday(), holidayCache, targetHolidayService));
			targetDay.setTradeHoliday(sourceDay.isTradeHoliday());
			targetDay.setSettlementHoliday(sourceDay.isSettlementHoliday());
			targetDay.setFullDayHoliday(sourceDay.isFullDayHoliday());
			targetHolidayService.saveCalendarHolidayDay(targetDay);
		}

		Collection<CalendarHolidayDay> deleteList = targetMap.values();
		for (CalendarHolidayDay holidayDay : deleteList) {
			targetHolidayService.deleteCalendarHolidayDay(holidayDay.getId());
		}

		status.appendToMessage(sourceCalendar.getName() + ": " + targetDataSource.getName() + ": " + insertList.size() + " added; " + deleteList.size() + " deleted");
	}


	private CalendarHoliday getTargetHolidayDay(CalendarHoliday sourceHoliday, Map<String, CalendarHoliday> holidayCache, CalendarHolidayService targetHolidayService) {
		return holidayCache.computeIfAbsent(sourceHoliday.getName(), holidayName -> {
			CalendarHoliday targetHoliday = targetHolidayService.getCalendarHolidayByName(holidayName);
			if (targetHoliday == null) {
				// need to create one
				targetHoliday = new CalendarHoliday();
				targetHoliday.setName(holidayName);
				targetHoliday.setDescription(sourceHoliday.getDescription());
				targetHoliday = targetHolidayService.saveCalendarHoliday(targetHoliday);
			}
			return targetHoliday;
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public short getSourceDataSourceId() {
		return this.sourceDataSourceId;
	}


	public void setSourceDataSourceId(short sourceDataSourceId) {
		this.sourceDataSourceId = sourceDataSourceId;
	}


	public Short[] getSourceCalendarIds() {
		return this.sourceCalendarIds;
	}


	public void setSourceCalendarIds(Short[] sourceCalendarIds) {
		this.sourceCalendarIds = sourceCalendarIds;
	}


	public Short[] getTargetDataSourceIds() {
		return this.targetDataSourceIds;
	}


	public void setTargetDataSourceIds(Short[] targetDataSourceIds) {
		this.targetDataSourceIds = targetDataSourceIds;
	}


	public StatusHolderObject<Status> getStatusHolder() {
		return this.statusHolder;
	}


	public void setStatusHolder(StatusHolderObject<Status> statusHolder) {
		this.statusHolder = statusHolder;
	}


	public ExternalServiceLocator getExternalServiceLocator() {
		return this.externalServiceLocator;
	}


	public void setExternalServiceLocator(ExternalServiceLocator externalServiceLocator) {
		this.externalServiceLocator = externalServiceLocator;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
