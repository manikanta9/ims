package com.clifton.batch.jobs;


import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>BeanMethodExecutorJob</code> class is a batch job that executes the specified method (with no arguments)
 * on context managed bean with the specified name.
 *
 * @author vgomelsky
 */
public class BeanMethodExecutorJob implements Task {

	private String beanName;
	private String methodName;

	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Object bean = getApplicationContextService().getContextBean(getBeanName());
		if (bean == null) {
			throw new RuntimeException("Cannot find bean with name = '" + getBeanName() + "' in application context.");
		}

		Method method;
		try {
			method = bean.getClass().getMethod(getMethodName());
		}
		catch (Throwable e) {
			throw new RuntimeException("Cannot find method '" + getMethodName() + "' on bean '" + getBeanName() + "': " + e.getMessage(), e);
		}

		Object beanResult = null;
		Throwable cause = null;
		try {
			beanResult = method.invoke(bean);
		}
		catch (InvocationTargetException e) {
			cause = ObjectUtils.coalesce(e.getCause(), e);
		}
		catch (Throwable e) {
			cause = e;
		}

		if (cause != null) {
			throw new RuntimeException("Error executing method " + getBeanName() + "." + getMethodName() + "(): " + cause.getMessage(), cause);
		}

		return (beanResult instanceof Status) ?
				(Status) beanResult :
				Status.ofMessage("Executed " + getBeanName() + "." + getMethodName() + "()");
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanName() {
		return this.beanName;
	}


	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}


	public String getMethodName() {
		return this.methodName;
	}


	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
