package com.clifton.batch.jobs;


import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlHandlerLocator;
import com.clifton.core.dataaccess.sql.SqlStoredProcedureCommand;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;

import java.util.Map;


/**
 * The <code>StoredProcedureExecutorJob</code> is a Task that runs the specified stored procedure using
 * the specified data source connection.
 *
 * @author mwacker
 */
public class StoredProcedureExecutorJob implements Task {

	private String storedProcedureName;
	private String dataSourceName;
	private Integer transactionTimeOutSeconds;

	// TODO: add support for parameters
	private Map<String, String> parameters;

	private SqlHandlerLocator sqlHandlerLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		SqlHandler sqlHandler = getSqlHandlerLocator().locate(getDataSourceName());

		Map<String, Object> result = sqlHandler.executeStoredProcedure(SqlStoredProcedureCommand.forProcedure(getStoredProcedureName()).withTimeoutInSeconds(getTransactionTimeOutSeconds()));
		return Status.ofMessage((result == null) ? "No Results" : result.toString());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public String getStoredProcedureName() {
		return this.storedProcedureName;
	}


	public void setStoredProcedureName(String storedProcedureName) {
		this.storedProcedureName = storedProcedureName;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Map<String, String> getParameters() {
		return this.parameters;
	}


	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}


	public Integer getTransactionTimeOutSeconds() {
		return this.transactionTimeOutSeconds;
	}


	public void setTransactionTimeOutSeconds(Integer transactionTimeOutSeconds) {
		this.transactionTimeOutSeconds = transactionTimeOutSeconds;
	}


	public SqlHandlerLocator getSqlHandlerLocator() {
		return this.sqlHandlerLocator;
	}


	public void setSqlHandlerLocator(SqlHandlerLocator sqlHandlerLocator) {
		this.sqlHandlerLocator = sqlHandlerLocator;
	}
}
