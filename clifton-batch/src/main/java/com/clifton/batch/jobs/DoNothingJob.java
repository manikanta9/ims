package com.clifton.batch.jobs;


import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;

import java.util.Map;


/**
 * The <code>DoNothingJob</code> class defines a task that does nothing. It can be used as a parent
 * job in a multi-step job.  You can also specify optional 'executionDelay' in milliseconds.
 *
 * @author vgomelsky
 */
public class DoNothingJob implements Task {

	/**
	 * Optional execution delay/pause time in milliseconds.
	 */
	private long executionDelay;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(@SuppressWarnings("unused") Map<String, Object> context) {
		if (getExecutionDelay() > 0) {
			try {
				Thread.sleep(getExecutionDelay());
			}
			catch (InterruptedException e) {
				// should never be interrupted but it's OK for this job
				Thread.currentThread().interrupt();
			}
		}
		return Status.ofMessage("Completed after waiting for " + getExecutionDelay() + " milliseconds.", false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public long getExecutionDelay() {
		return this.executionDelay;
	}


	public void setExecutionDelay(long executionDelay) {
		this.executionDelay = executionDelay;
	}
}
