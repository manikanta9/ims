package com.clifton.batch.runner;


import com.clifton.batch.job.BatchJob;
import com.clifton.batch.job.BatchJobService;
import com.clifton.calendar.schedule.runner.AbstractScheduleAwareRunnerProvider;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BatchRunnerProvider</code> class returns batch job Runner instances that are scheduled for the specified time period.
 *
 * @author vgomelsky
 */
@Component
public class BatchRunnerProvider extends AbstractScheduleAwareRunnerProvider<BatchJob> {

	private BatchJobService batchJobService;
	private BatchRunnerFactory batchRunnerFactory;


	@Override
	@Transactional(readOnly = true)
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		long startMillis = System.currentTimeMillis();
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Starting getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE)));
		List<BatchJob> activeJobs = getBatchJobService().getBatchJobListActiveOnDate(startDateTime);
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Active Batch Jobs list compiled at: " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
		List<Runner> results = new ArrayList<>();
		for (BatchJob batchJob : activeJobs) {
			List<Runner> batchJobRunners = getOccurrencesBetweenForEntity(batchJob, startDateTime, endDateTime);
			if (!CollectionUtils.isEmpty(batchJobRunners)) {
				results.addAll(batchJobRunners);
			}
		}
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Ending getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
		return results;
	}


	@Override
	public Runner createRunnerForEntityAndDate(BatchJob entity, Date runnerDate) {
		return getBatchRunnerFactory().createChildBatchRunner(runnerDate, entity.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  /////////// 
	////////////////////////////////////////////////////////////////////////////


	public BatchJobService getBatchJobService() {
		return this.batchJobService;
	}


	public void setBatchJobService(BatchJobService batchJobService) {
		this.batchJobService = batchJobService;
	}


	public BatchRunnerFactory getBatchRunnerFactory() {
		return this.batchRunnerFactory;
	}


	public void setBatchRunnerFactory(BatchRunnerFactory batchRunnerFactory) {
		this.batchRunnerFactory = batchRunnerFactory;
	}
}
