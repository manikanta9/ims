package com.clifton.batch.runner;


import java.util.Date;


/**
 * The <code>BatchRunnerFactory</code> interface defines methods for creating BatchRunner instances.
 *
 * @author vgomelsky
 */
public interface BatchRunnerFactory {

	/**
	 * Create a top level batch job runner.
	 */
	public BatchRunner createChildBatchRunner(Date runDate, int jobId);


	/**
	 * Create a child level batch job runner.
	 */
	public BatchRunner createChildBatchRunner(int jobId, BatchRunner parentRunner);
}
