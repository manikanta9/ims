package com.clifton.batch.runner;


import com.clifton.batch.job.BatchJob;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;

import java.util.List;


/**
 * The <code>BatchRunnerService</code> interface defines methods for running batch jobs.
 *
 * @author vgomelsky
 */
public interface BatchRunnerService {

	/**
	 * Schedules BatchJob with the specified id to run immediately.
	 * The call is asynchronous and the job will be executed in a different thread.
	 */
	public void runBatchJob(int id);


	/**
	 * Returns a list of BatchJob objects that are in invalid state: RUNNING status but there's nothing running.
	 * This could happen if the server is killed while the job is running.
	 * <p>
	 * Job status must be updated so that the job can start again.
	 */
	public List<BatchJob> getBatchJobListInvalid();


	/**
	 * Fixes invalid batch job (see getBatchJobListInvalid) by setting its status to FAILED.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_WRITE)
	public void fixBatchJob(int id);
}
