package com.clifton.batch.runner;


import com.clifton.batch.job.BatchJob;
import com.clifton.batch.job.BatchJobHistory;
import com.clifton.batch.job.BatchJobService;
import com.clifton.batch.job.BatchJobStatuses;
import com.clifton.batch.job.search.BatchJobHistorySearchForm;
import com.clifton.batch.job.search.BatchJobSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BatchRunnerServiceImpl</code> class provides basic implementation for BatchRunnerService interface.
 *
 * @author vgomelsky
 */
@Service
public class BatchRunnerServiceImpl implements BatchRunnerService {

	private BatchJobService batchJobService;
	private BatchRunnerFactory batchRunnerFactory;

	private RunnerHandler runnerHandler;
	private SecurityUserService securityUserService;

	private SecurityAuthorizationService securityAuthorizationService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void runBatchJob(int id) {
		BatchJob job = getBatchJobService().getBatchJob(id);
		validateExecuteCondition(job);

		BatchRunner runner = getBatchRunnerFactory().createChildBatchRunner(new Date(), id);
		// Use Current User
		runner.setDefaultRunAsUserName(getSecurityUserService().getSecurityUserCurrent().getUserName());

		getRunnerHandler().runNow(runner);
	}


	@Override
	public List<BatchJob> getBatchJobListInvalid() {
		List<BatchJob> invalidList = null;

		// 1. get all running batch jobs
		BatchJobSearchForm searchForm = new BatchJobSearchForm();
		searchForm.setStatus(BatchJobStatuses.RUNNING);
		List<BatchJob> runningList = getBatchJobService().getBatchJobList(searchForm);

		if (CollectionUtils.getSize(runningList) > 0) {
			for (BatchJob job : runningList) {
				if (!isJobRunning(job)) {
					// SHOULD NEVER HAVE A BATCH JOB IN "RUNNING" STATUS THAT IS NOT ACTUALLY RUNNING
					// this could happen if the server is killed while the job is running
					// It's possible the job completed, double check status
					job = getBatchJobService().getBatchJob(job.getId());
					if (BatchJobStatuses.RUNNING == job.getStatus()) {
						if (invalidList == null) {
							invalidList = new ArrayList<>();
						}
						invalidList.add(job);
					}
				}
			}
		}

		return invalidList;
	}


	@Override
	@Transactional
	public void fixBatchJob(int id) {
		// validate current job state to make sure we're not fixing a good job
		BatchJob job = getBatchJobService().getBatchJob(id);
		ValidationUtils.assertNotNull(job, "Cannot find batch job with id = " + id);
		ValidationUtils.assertTrue(BatchJobStatuses.RUNNING == job.getStatus(), "Cannot fix batch job that is not in RUNNING status: " + job.getStatus());
		ValidationUtils.assertFalse(isJobRunning(job), "Cannot fix the job with id = " + id + " because it maybe currently running.");

		// set job to FAILED and update history
		getBatchJobService().updateBatchJobStatus(job.getId(), BatchJobStatuses.FAILED);

		// find last running history
		BatchJobHistorySearchForm searchForm = new BatchJobHistorySearchForm();
		searchForm.setBatchJobId(id);
		searchForm.setLimit(1);
		// Need to include Running status only because there can be more recent history records when the job was scheduled but skipped because it thought it was still running.
		searchForm.setStatus(BatchJobStatuses.RUNNING);
		searchForm.setOrderBy("id:desc");
		List<BatchJobHistory> historyList = getBatchJobService().getBatchJobHistoryList(searchForm);
		BatchJobHistory jobHistory = CollectionUtils.getFirstElement(historyList);
		if (jobHistory != null && BatchJobStatuses.RUNNING == jobHistory.getStatus()) {
			jobHistory.setStatus(BatchJobStatuses.FAILED);
			jobHistory.setEndDate(new Date());
			jobHistory.setDescription("Fixed job in RUNNING status that was not running (server restart?)");
			getBatchJobService().saveBatchJobHistory(jobHistory);
		}
	}


	/**
	 * Need to check up through parents, because the parent may be the one that is scheduled as running
	 */
	private boolean isJobRunning(BatchJob job) {
		boolean running = getRunnerHandler().isRunnerWithTypeAndIdActive(BatchRunner.RUNNER_TYPE, Integer.toString(job.getId()));
		if (!running && job.getParent() != null) {
			return isJobRunning(job.getParent());
		}
		return running;
	}


	private void validateExecuteCondition(BatchJob job) {
		// do not check security if current user is an admin - always allowed
		if (getSecurityAuthorizationService().isSecurityUserAdmin()) {
			return;
		}

		SystemCondition condition = job.getEntityExecuteCondition();

		if (condition == null) {
			// Allowed
			return;
		}

		String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(condition, job);
		if (!StringUtils.isEmpty(error)) {
			throw new ValidationException("You do not have permission to run this batch job because: " + error);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public BatchRunnerFactory getBatchRunnerFactory() {
		return this.batchRunnerFactory;
	}


	public void setBatchRunnerFactory(BatchRunnerFactory batchRunnerFactory) {
		this.batchRunnerFactory = batchRunnerFactory;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public BatchJobService getBatchJobService() {
		return this.batchJobService;
	}


	public void setBatchJobService(BatchJobService batchJobService) {
		this.batchJobService = batchJobService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
