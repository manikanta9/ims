package com.clifton.batch.runner;


import com.clifton.core.context.ApplicationContextService;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class BatchRunnerFactoryImpl implements BatchRunnerFactory {

	private ApplicationContextService applicationContextService;

	/**
	 * This user will be used in case runAsUser is not set on the batch job.
	 */
	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BatchRunner createChildBatchRunner(Date runDate, int jobId) {
		BatchRunner runner = new BatchRunner(runDate, jobId);
		return createChildBatchRunner(runner);
	}


	@Override
	public BatchRunner createChildBatchRunner(int jobId, BatchRunner parentRunner) {
		BatchRunner runner = new BatchRunner(jobId, parentRunner);
		return createChildBatchRunner(runner);
	}


	/**
	 * Autowire the specified runner and set default run as user
	 */
	private BatchRunner createChildBatchRunner(BatchRunner runner) {
		getApplicationContextService().autowireBean(runner);
		runner.setDefaultRunAsUserName(getDefaultRunAsUserName());
		return runner;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
