package com.clifton.batch.runner;


import com.clifton.batch.job.BatchJob;
import com.clifton.batch.job.BatchJobHistory;
import com.clifton.batch.job.BatchJobService;
import com.clifton.batch.job.BatchJobStatuses;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.ProcessTerminationException;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBeanService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BatchRunner</code> class represents a Runner for a specific (scheduled or instant)
 * run of a batch job. It will manage updates to job's status as well as corresponding history record.
 * <p>
 * The run will create and wire the Task defined by batch job's SystemBean and will execute it.
 * <p>
 * If the job has steps, each step will be executed in their respective order after running the job's Task.
 *
 * @author mwacker
 */
public class BatchRunner extends AbstractStatusAwareRunner {

	public static final String RUNNER_TYPE = "BATCH";
	public static final String CURRENT_BATCH_JOB_ID = "currentBatchJobId";

	////////////////////////////////////////////////////////////////////////////

	private final int jobId;
	/**
	 * Optional runner of the parent job (if any)
	 */
	private BatchRunner parentRunner;
	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;
	/**
	 * Run Context can be used to share data among job steps and to return results
	 * ("result" key) back to the runner which will be stored in job history description.
	 */
	private Map<String, Object> runContext = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ContextHandler contextHandler;
	private BatchJobService batchJobService;
	private BatchRunnerFactory batchRunnerFactory;
	private SystemBeanService systemBeanService;
	private SecurityUserService securityUserService;
	private SecurityImpersonationHandler securityImpersonationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BatchRunner(Date runDate, int jobId) {
		super(RUNNER_TYPE, Integer.toString(jobId), runDate);
		this.jobId = jobId;
	}


	public BatchRunner(int jobId, BatchRunner parentRunner) {
		this(parentRunner.getRunDate(), jobId);
		this.parentRunner = parentRunner;
		this.runContext = parentRunner.getRunContext();
	}


	@Override
	public void run() {
		BatchJob job = getBatchJobService().getBatchJob(this.jobId);
		SecurityUser runAsUser = getRunAsUser(job);
		if (runAsUser == null) {
			LogUtils.error(getClass(), "Cannot run batch job.  No 'run as' user is defined for " + job);
		}
		getSecurityImpersonationHandler().runAsSecurityUser(runAsUser, () -> {
			if (BatchJobStatuses.RUNNING == job.getStatus()) {
				// do not start another instance of the same job if the job is still running
				getStatus().setMessage("Skipping job " + job + " run because its previous run is still in progress");
				LogUtils.info(getClass(), getStatus().getMessage());
				saveJobHistoryForJobAsSkipped(job, getStatus().getMessage());
			}
			else {
				getStatus().setMessage("Running job " + job + "' on '" + (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)) + "'.");
				LogUtils.info(getClass(), getStatus().toString());
				doRun(job, null);

				getStatus().setMessage("Completed job " + job + "' on '" + (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)) + "'.");
				LogUtils.info(getClass(), getStatus().toString());
			}
		});
	}


	private void doRun(BatchJob job, Short retryAttempt) {
		if (retryAttempt == null) {
			retryAttempt = job.getRetryCount() != null ? (short) 0 : null;
		}
		if (!doRunAttempt(job, retryAttempt) && (retryAttempt != null) && (retryAttempt < job.getRetryCount())) {
			try {
				Thread.sleep(job.getRetryDelayInSeconds() * 1000);
			}
			catch (InterruptedException e) {
				LogUtils.error(getClass(), "Retry thread wait interrupted", e);
				Thread.currentThread().interrupt();
			}
			retryAttempt++;
			doRun(job, retryAttempt);
		}
	}


	@SuppressWarnings("unchecked")
	private boolean doRunAttempt(BatchJob job, Short retryAttempt) {
		BatchJobHistory jobHistory = new BatchJobHistory();
		StringBuilder completedMessage = new StringBuilder();
		BatchJobStatuses completedStatus = BatchJobStatuses.FAILED;

		try {
			// set the job status and history to running
			job = getBatchJobService().updateBatchJobStatus(job.getId(), BatchJobStatuses.RUNNING);

			jobHistory.setBatchJob(job);
			jobHistory.setStatus(BatchJobStatuses.RUNNING);
			jobHistory.setRunnerUser(getSecurityUserService().getSecurityUserCurrent());

			if (this.parentRunner != null) {
				jobHistory.setParent((BatchJobHistory) this.runContext.get("batchJobHistory-" + this.parentRunner.getTypeId()));
				// update parent's status with the current child
				Status parentStatus = this.parentRunner.getStatus();
				if (parentStatus != null) {
					parentStatus.setCurrentChildStatus(getStatus());
				}
			}
			jobHistory.setScheduledDate(getRunDate());
			jobHistory.setStartDate(new Date());
			// set the retry number on the job history
			if (retryAttempt != null) {
				jobHistory.setRetryAttemptNumber(retryAttempt);
			}
			jobHistory = getBatchJobService().saveBatchJobHistory(jobHistory);

			// add the history to the execution runContext
			this.runContext.put("batchJobHistory-" + getTypeId(), jobHistory);
			this.runContext.put(CURRENT_BATCH_JOB_ID, job.getId());

			// get the instance of system bean and run it
			Task task = (Task) getSystemBeanService().getBeanInstance(job.getSystemBean());
			if (task instanceof StatusHolderObjectAware) {
				((StatusHolderObjectAware<Status>) task).setStatusHolderObject(this);
			}
			Status status = task.run(this.runContext);

			// update result message and job status
			if (!StringUtils.isEmpty(status.getStatusTitle())) {
				if (completedMessage.length() > 0) {
					completedMessage.append("\n");
				}
				completedMessage.append(status.getStatusTitle());
			}
			if (!StringUtils.isEmpty(status.getMessage())) {
				if (completedMessage.length() > 0) {
					completedMessage.append("\n");
				}
				completedMessage.append(status.getMessage());
			}

			if (status.getErrorCount() > 0) {
				if (this.parentRunner != null) {
					// pass status to parent so that it is also marked as completed with errors
					this.runContext.put("error-" + this.parentRunner.jobId, true);
				}
				if (retryAttempt != null) {
					return false;
				}
				completedStatus = BatchJobStatuses.COMPLETED_WITH_ERRORS;
			}
			else if (status.getWarningCount() > 0) {
				completedStatus = BatchJobStatuses.COMPLETED_WITH_WARNINGS;
			}
			else if (status.isActionPerformed()) {
				completedStatus = BatchJobStatuses.COMPLETED;
			}
			else {
				completedStatus = BatchJobStatuses.COMPLETED_WITH_NO_RESULTS;
			}

			// run the child jobs
			List<String> errorMessages = new ArrayList<>();
			runChildJobs(job, errorMessages);

			// if child errors found: update main parent status/message
			if (!errorMessages.isEmpty()) {
				completedStatus = BatchJobStatuses.COMPLETED_WITH_ERRORS;
				for (String error : errorMessages) {
					status.addError(error);
				}
			}
			else if (BooleanUtils.isTrue(this.runContext.get("error-" + this.jobId))) {
				if (completedMessage.length() == 0) {
					completedMessage.append("Child failure(s) detected.");
				}
				else {
					completedMessage.insert(0, "Child failure(s) detected. Original Message:\n\n");
				}
				completedStatus = BatchJobStatuses.COMPLETED_WITH_ERRORS;
			}

			for (StatusDetail detail : status.getDetailListForCategory(StatusDetail.CATEGORY_ERROR)) {
				completedMessage.append("\n\n").append(detail.getNote());
			}
			for (StatusDetail detail : status.getDetailListForCategory(StatusDetail.CATEGORY_WARNING)) {
				completedMessage.append("\n\n").append(detail.getNote());
			}
		}
		catch (Throwable e) {
			// the job being executed failed so set the status
			completedStatus = BatchJobStatuses.FAILED;
			if (completedMessage.length() > 0) {
				completedMessage.append("\n\n");
			}
			completedMessage.append(ExceptionUtils.getFullStackTrace(e));
			LogUtils.errorOrInfo(getClass(), "Error running batch job " + job, e);

			if (e instanceof ProcessTerminationException) {
				// clear termination request to enable proper Status object serialization
				getStatus().setTerminationRequested(false);
				if (this.parentRunner != null) {
					this.parentRunner.getStatus().setTerminationRequested(false);
				}
				throw e;
			}

			// if the job is configured with retries then return false to force a retry
			if (retryAttempt != null && retryAttempt < job.getRetryCount()) {
				return false;
			}

			// if running a child job, re-throw the error so it is caught by the try...catch around the child job.
			if (this.parentRunner != null) {
				throw new RuntimeException("Error running child batch job: " + job, e);
			}
		}
		finally {
			getBatchJobService().updateBatchJobStatus(job.getId(), completedStatus);

			jobHistory.setStatus(completedStatus);
			jobHistory.setEndDate(new Date());

			if (retryAttempt != null) {
				completedMessage.insert(0, String.format("Attempt: %d.  ", retryAttempt));
			}

			// truncate the message if necessary: 4000 characters limit
			String loggedMessage = StringUtils.formatStringUpToNCharsWithDots(completedMessage.toString(), DataTypes.DESCRIPTION_LONG.getLength());
			jobHistory.setDescription(loggedMessage);
			jobHistory = getBatchJobService().saveBatchJobHistory(jobHistory);
		}
		return !jobHistory.getStatus().isFailedStatus();
	}


	/**
	 * Run the child jobs of the specified job.
	 *
	 * @param errorMessages list of error messages
	 */
	private void runChildJobs(BatchJob job, List<String> errorMessages) {
		List<BatchJob> childJobs = getBatchJobService().getBatchJobListByParent(job.getId());
		for (BatchJob childJob : CollectionUtils.getIterable(childJobs)) {
			if (childJob.isEnabled()) {
				// create and autowire the child runner
				BatchRunner runner = getBatchRunnerFactory().createChildBatchRunner(childJob.getId(), this);

				try {
					runner.run();
				}
				catch (Exception e) {
					// child level error and parent fails when child does
					if (childJob.isFailParentOnThisFailure() || e instanceof ProcessTerminationException) {
						// clear termination request to enable proper Status object serialization
						runner.getStatus().setTerminationRequested(false);
						if (runner.parentRunner != null) {
							runner.parentRunner.getStatus().setTerminationRequested(false);
						}
						throw new RuntimeException("Failing the parent because of child job failure: " + childJob, e);
					}
					// parent does not fail when child does so add an error indicating which child failed
					errorMessages.add("Child job '" + childJob + "' failed with error: " + e.getMessage());
				}
			}
		}
	}


	/**
	 * Returns the run as user specified for the batch job or the default run as user if one is not set.
	 */
	private SecurityUser getRunAsUser(BatchJob job) {
		SecurityUser result = job.getRunAsUser();
		if (result == null) {
			// try to user parent's user which must be set
			if (this.parentRunner != null) {
				result = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
			}
			else {
				result = getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
			}
		}
		return result;
	}


	private void saveJobHistoryForJobAsSkipped(BatchJob job, String description) {
		BatchJobHistory jobHistory = new BatchJobHistory();
		jobHistory.setBatchJob(job);
		jobHistory.setStatus(BatchJobStatuses.SKIPPED);
		jobHistory.setRunnerUser(getSecurityUserService().getSecurityUserCurrent());
		jobHistory.setScheduledDate(getRunDate());
		jobHistory.setStartDate(new Date());
		jobHistory.setEndDate(new Date());
		jobHistory.setDescription(description);
		getBatchJobService().saveBatchJobHistory(jobHistory);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public BatchJobService getBatchJobService() {
		return this.batchJobService;
	}


	public void setBatchJobService(BatchJobService batchJobService) {
		this.batchJobService = batchJobService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public Map<String, Object> getRunContext() {
		return this.runContext;
	}


	public BatchRunnerFactory getBatchRunnerFactory() {
		return this.batchRunnerFactory;
	}


	public void setBatchRunnerFactory(BatchRunnerFactory batchRunnerFactory) {
		this.batchRunnerFactory = batchRunnerFactory;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public SecurityImpersonationHandler getSecurityImpersonationHandler() {
		return this.securityImpersonationHandler;
	}


	public void setSecurityImpersonationHandler(SecurityImpersonationHandler securityImpersonationHandler) {
		this.securityImpersonationHandler = securityImpersonationHandler;
	}
}
