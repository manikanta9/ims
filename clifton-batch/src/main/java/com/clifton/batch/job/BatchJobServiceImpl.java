package com.clifton.batch.job;


import com.clifton.batch.job.search.BatchJobCategorySearchForm;
import com.clifton.batch.job.search.BatchJobHistorySearchForm;
import com.clifton.batch.job.search.BatchJobSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUserService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>BatchJobServiceImpl</code> class provides basic implementation of the BatchJobService interface.
 *
 * @author mwacher
 */
@Service
public class BatchJobServiceImpl implements BatchJobService {

	private AdvancedUpdatableDAO<BatchJob, Criteria> batchJobDAO;
	private AdvancedUpdatableDAO<BatchJobHistory, Criteria> batchJobHistoryDAO;
	private AdvancedUpdatableDAO<BatchJobCategory, Criteria> batchJobCategoryDAO;

	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	////////            Batch Job Business Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BatchJob getBatchJob(int id) {
		return getBatchJobDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BatchJob> getBatchJobList(BatchJobSearchForm searchForm) {
		HibernateSearchFormConfigurer batchJobListSearchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getOwnedByUserId() != null) {
					List<Short> userGroupIdList = BeanUtils.getBeanIdentityList(getSecurityUserService().getSecurityGroupListByUser(searchForm.getOwnedByUserId()));
					criteria.add(Restrictions.or(Restrictions.eq("ownerUser.id", searchForm.getOwnedByUserId()), Restrictions.in("ownerGroup.id", userGroupIdList)));
				}
			}
		};

		return getBatchJobDAO().findBySearchCriteria(batchJobListSearchFormConfigurer);
	}


	@Override
	public List<BatchJob> getBatchJobListActiveOnDate(final Date startDateTime) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			Date startDate = DateUtils.clearTime(startDateTime);
			criteria.add(Restrictions.eq("enabled", true));
			criteria.createCriteria("schedule", "s").add(Restrictions.le("s.startDate", startDate)).add(Restrictions.or(Restrictions.isNull("s.endDate"), Restrictions.ge("s.endDate", startDate)));
		};

		return getBatchJobDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<BatchJob> getBatchJobListByParent(int id) {
		return getBatchJobDAO().findByField("parent.id", id);
	}


	/**
	 * @see BatchJobObserver
	 */
	@Override
	public BatchJob saveBatchJob(BatchJob bean) {
		if (bean.getRetryCount() != null) {
			ValidationUtils.assertTrue(bean.getRetryDelayInSeconds() != null, "RetryDelayInSeconds is required when RetryCount in populated.", "retryDelayInSeconds");
		}
		if (bean.getRetryDelayInSeconds() != null) {
			ValidationUtils.assertTrue(bean.getRetryCount() != null, "RetryCount is required when RetryDelayInSeconds in populated.", "retryCount");
		}

		return getBatchJobDAO().save(bean);
	}


	@Override
	public BatchJob updateBatchJobStatus(int id, BatchJobStatuses status) {
		BatchJob batchJob = getBatchJob(id);

		if (status == BatchJobStatuses.NONE) {
			// Note: There is no code that would update status to NONE
			ValidationUtils.assertTrue(batchJob.getStatus() == null || batchJob.getStatus() == BatchJobStatuses.NONE, "Batch Job [" + batchJob.getName() + "] status of NONE should only apply to jobs that have never ran.");
		}
		// Job Cannot Currently be Running
		else if (status == BatchJobStatuses.RUNNING) {
			ValidationUtils.assertFalse(batchJob.getStatus() == BatchJobStatuses.RUNNING, "Batch Job [" + batchJob.getName() + "] status is already RUNNING.");
		}
		// Job MUST be running
		else {
			ValidationUtils.assertTrue(batchJob.getStatus() == BatchJobStatuses.RUNNING, "Batch Job [" + batchJob.getName() + "] status must be RUNNING in order to update to status " + status.name() + ".");
		}
		batchJob.setStatus(status);
		return getBatchJobDAO().save(batchJob);
	}


	/**
	 * @see BatchJobObserver
	 */
	@Override
	@Transactional
	public void deleteBatchJob(int id) {
		BatchJobHistorySearchForm searchForm = new BatchJobHistorySearchForm();
		searchForm.setBatchJobId(id);
		List<BatchJobHistory> historyList = getBatchJobHistoryList(searchForm);
		// this could be EXTREMELY slow, but it's the only way for now because attaching a
		// real list of BatchJobHistory's (for cascading) to the BatchJob object could make the json huge.
		for (BatchJobHistory history : CollectionUtils.getIterable(historyList)) {
			deleteBatchJobHistory(history.getId());
		}
		getBatchJobDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Batch Job History Business Methods        //////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BatchJobHistory getBatchJobHistory(int id) {
		return getBatchJobHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public BatchJobHistory getBatchJobHistoryByDate(int jobId, Date scheduledDate) {
		return getBatchJobHistoryDAO().findOneByFields(new String[]{"batchJob.id", "scheduledDate"}, new Object[]{jobId, scheduledDate});
	}


	@Override
	public List<BatchJobHistory> getBatchJobHistoryList(BatchJobHistorySearchForm searchForm) {
		HibernateSearchFormConfigurer batchJobHistoryListSearchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getOwnedByUserId() != null) {
					List<Short> userGroupIdList = BeanUtils.getBeanIdentityList(getSecurityUserService().getSecurityGroupListByUser(searchForm.getOwnedByUserId()));
					criteria.add(Restrictions.or(
							Restrictions.eq(getPathAlias("batchJob", criteria) + ".ownerUser.id", searchForm.getOwnedByUserId()),
							Restrictions.in(getPathAlias("batchJob", criteria) + ".ownerGroup.id", userGroupIdList)
					));
				}
			}
		};

		return getBatchJobHistoryDAO().findBySearchCriteria(batchJobHistoryListSearchFormConfigurer);
	}


	@Override
	public BatchJobHistory saveBatchJobHistory(BatchJobHistory bean) {
		return getBatchJobHistoryDAO().save(bean);
	}


	@Transactional
	protected void deleteBatchJobHistory(int id) {
		BatchJobHistorySearchForm searchForm = new BatchJobHistorySearchForm();
		searchForm.setParentId(id);
		List<BatchJobHistory> historyList = getBatchJobHistoryList(searchForm);
		// this could be EXTREMELY slow, but it's the only way for now because attaching a
		// real list of BatchJobHistory's (for cascading) to the BatchJob object could make the json huge.
		for (BatchJobHistory history : CollectionUtils.getIterable(historyList)) {
			deleteBatchJobHistory(history.getId());
		}
		getBatchJobHistoryDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Batch Job Category Business Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BatchJobCategory getBatchJobCategory(short id) {
		return getBatchJobCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BatchJobCategory> getBatchJobCategoryList(BatchJobCategorySearchForm searchForm) {
		return getBatchJobCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BatchJobCategory saveBatchJobCategory(BatchJobCategory category) {
		return getBatchJobCategoryDAO().save(category);
	}


	@Override
	public void deleteBatchJobCategory(short id) {
		getBatchJobCategoryDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BatchJob, Criteria> getBatchJobDAO() {
		return this.batchJobDAO;
	}


	public void setBatchJobDAO(AdvancedUpdatableDAO<BatchJob, Criteria> batchJobDAO) {
		this.batchJobDAO = batchJobDAO;
	}


	public AdvancedUpdatableDAO<BatchJobHistory, Criteria> getBatchJobHistoryDAO() {
		return this.batchJobHistoryDAO;
	}


	public void setBatchJobHistoryDAO(AdvancedUpdatableDAO<BatchJobHistory, Criteria> batchJobHistoryDAO) {
		this.batchJobHistoryDAO = batchJobHistoryDAO;
	}


	public AdvancedUpdatableDAO<BatchJobCategory, Criteria> getBatchJobCategoryDAO() {
		return this.batchJobCategoryDAO;
	}


	public void setBatchJobCategoryDAO(AdvancedUpdatableDAO<BatchJobCategory, Criteria> batchJobCategoryDAO) {
		this.batchJobCategoryDAO = batchJobCategoryDAO;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
