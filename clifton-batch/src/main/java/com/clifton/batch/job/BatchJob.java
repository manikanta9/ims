package com.clifton.batch.job;


import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.runner.ScheduleAware;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.util.ObjectUtils;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>BatchJob</code> class defines a batch job. If parent is defined for a job, then the job is a step of
 * the parent job.  Job steps cannot be scheduled as they execute immediately after successful parent
 * job execution in predefined order.
 *
 * @author mwacker
 */
public class BatchJob extends NamedHierarchicalEntity<BatchJob, Integer> implements ScheduleAware<Integer>, SystemEntityModifyConditionAware {

	/**
	 * SystemBean that implements batch job logic and will be executed
	 * based on corresponding schedule.
	 * It must implement {@link com.clifton.core.util.runner.Task} interface.
	 */
	private SystemBean systemBean;
	private BatchJobStatuses status;
	/**
	 * Optionally this job can be executed as the following user.
	 */
	private SecurityUser runAsUser;
	private boolean enabled;

	// FIELDS THAT APPLY ONLY TO TOP LEVEL JOBS
	private CalendarSchedule schedule;

	// JOB STEP FIELDS THAT DO NOT APPLY TO TOP LEVEL JOBS
	private Integer order;
	/**
	 * Set to true to fail the parent job when this step fails: stop execution on failure
	 * of this step and do not execute the following steps.
	 */
	private boolean failParentOnThisFailure;

	/**
	 * Number of times to retry the batch job.
	 */
	private Short retryCount;
	/**
	 * The number of seconds to wait to retry the batch job.
	 */
	private Integer retryDelayInSeconds;


	/**
	 * Value higher than this triggers SLA
	 */
	private Integer maxExecutionTimeMillis;

	/**
	 * The SecurityUser associated with this batch job.
	 */
	private SecurityUser ownerUser;

	/**
	 * The SecurityGroup associated with this batch job.
	 */
	private SecurityGroup ownerGroup;

	/**
	 * The SecurityUser that supports the export.
	 */
	private SecurityUser supportUser;

	/**
	 * The SecurityGroup that supports the export.
	 */
	private SecurityGroup supportGroup;

	/**
	 * The BatchJobCategory associated with this batch job.
	 */
	private BatchJobCategory category;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the owner label for this entity which is either the label of the OwnerUser property (if defined) or the OwnerGroup property (if defined).
	 */
	public String getOwnerLabel() {
		LabeledObject ownerEntity = ObjectUtils.coalesce(getOwnerUser(), getOwnerGroup());
		return ownerEntity != null ? ownerEntity.getLabel() : null;
	}


	@Override
	public String toString() {
		return "{id=" + getId() + ", " + getStatus() + ", " + getLabel() + '}';
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getCategory().getEntityModifyCondition();
	}


	public SystemCondition getEntityExecuteCondition() {
		return getCategory().getEntityExecuteCondition();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isEnabled() {
		return this.enabled;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public boolean isSchedulePopulated() {
		return getSchedule() != null;
	}


	@Override
	public CalendarSchedule getSchedule() {
		return this.schedule;
	}


	public void setSchedule(CalendarSchedule schedule) {
		this.schedule = schedule;
	}


	public SystemBean getSystemBean() {
		return this.systemBean;
	}


	public void setSystemBean(SystemBean systemBean) {
		this.systemBean = systemBean;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public BatchJobStatuses getStatus() {
		return this.status;
	}


	public void setStatus(BatchJobStatuses status) {
		this.status = status;
	}


	public SecurityUser getRunAsUser() {
		return this.runAsUser;
	}


	public void setRunAsUser(SecurityUser runAsUser) {
		this.runAsUser = runAsUser;
	}


	public boolean isFailParentOnThisFailure() {
		return this.failParentOnThisFailure;
	}


	public void setFailParentOnThisFailure(boolean failParentOnThisFailure) {
		this.failParentOnThisFailure = failParentOnThisFailure;
	}


	public Short getRetryCount() {
		return this.retryCount;
	}


	public void setRetryCount(Short retryCount) {
		this.retryCount = retryCount;
	}


	public Integer getRetryDelayInSeconds() {
		return this.retryDelayInSeconds;
	}


	public void setRetryDelayInSeconds(Integer retryDelayInSeconds) {
		this.retryDelayInSeconds = retryDelayInSeconds;
	}


	public Integer getMaxExecutionTimeMillis() {
		return this.maxExecutionTimeMillis;
	}


	public void setMaxExecutionTimeMillis(Integer maxExecutionTimeMillis) {
		this.maxExecutionTimeMillis = maxExecutionTimeMillis;
	}


	public SecurityUser getOwnerUser() {
		return this.ownerUser;
	}


	public void setOwnerUser(SecurityUser ownerUser) {
		this.ownerUser = ownerUser;
	}


	public SecurityGroup getOwnerGroup() {
		return this.ownerGroup;
	}


	public void setOwnerGroup(SecurityGroup ownerGroup) {
		this.ownerGroup = ownerGroup;
	}


	public SecurityUser getSupportUser() {
		return this.supportUser;
	}


	public void setSupportUser(SecurityUser supportUser) {
		this.supportUser = supportUser;
	}


	public SecurityGroup getSupportGroup() {
		return this.supportGroup;
	}


	public void setSupportGroup(SecurityGroup supportGroup) {
		this.supportGroup = supportGroup;
	}


	public BatchJobCategory getCategory() {
		return this.category;
	}


	public void setCategory(BatchJobCategory category) {
		this.category = category;
	}
}
