package com.clifton.batch.job;


import com.clifton.batch.runner.BatchRunnerProvider;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.runner.RunnerUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BatchJobObserver</code> will clear and reschedule the jobs if necessary.
 * A reschedule is deemed necessary during BatchJob Inserts and Deletes (only if the batch job is enabled)
 * and Updates to the following fields: Enabled and/or Schedule
 *
 * @author manderson
 */
@Component
public class BatchJobObserver extends SelfRegisteringDaoObserver<BatchJob> {

	private BatchRunnerProvider batchRunnerProvider;

	private RunnerHandler runnerHandler;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<BatchJob> dao, DaoEventTypes event, BatchJob bean) {
		if (event.isUpdate()) {
			// Call this so original bean is stored
			getOriginalBean(dao, bean);
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<BatchJob> dao, DaoEventTypes event, BatchJob bean, Throwable e) {
		// If job is changed to enabled, or schedule changes, reschedule jobs
		if (isRescheduleNecessary(dao, event, bean)) {
			RunnerUtils.rescheduleRunnersForEntity(bean, s -> s.isEnabled() && s.getSchedule() != null, Integer.toString(bean.getId()), getBatchRunnerProvider(), getRunnerHandler());
		}
	}


	private boolean isRescheduleNecessary(ReadOnlyDAO<BatchJob> dao, DaoEventTypes event, BatchJob bean) {
		// Reschedule for Inserts/Deletes only if the batch job is enabled.
		if (event.isInsert() || event.isDelete()) {
			return bean.isEnabled();
		}

		// Batch Job Updates
		BatchJob original = getOriginalBean(dao, bean);

		// Enable/Disable
		if (original.isEnabled() != bean.isEnabled()) {
			return true;
		}

		// Schedule Change
		if (!original.isSchedulePopulated()) {
			return bean.isSchedulePopulated();
		}
		if (!bean.isSchedulePopulated()) {
			return true;
		}
		return !original.getSchedule().equals(bean.getSchedule());
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public BatchRunnerProvider getBatchRunnerProvider() {
		return this.batchRunnerProvider;
	}


	public void setBatchRunnerProvider(BatchRunnerProvider batchRunnerProvider) {
		this.batchRunnerProvider = batchRunnerProvider;
	}
}
