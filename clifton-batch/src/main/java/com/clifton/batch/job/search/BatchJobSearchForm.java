package com.clifton.batch.job.search;


import com.clifton.batch.job.BatchJobStatuses;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BatchJobSearchForm</code> class defines search configuration for BatchJob objects.
 *
 * @author vgomelsky
 */
public class BatchJobSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,description,category.name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "name", searchFieldPath = "parent")
	private String parentName;

	@SearchField
	private BatchJobStatuses status;

	@SearchField(searchField = "parent.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean rootNodesOnly;

	@SearchField
	private Integer order;

	@SearchField
	private Boolean enabled;

	@SearchField
	private Integer maxExecutionTimeMillis;

	@SearchField(searchField = "ownerUser.id")
	private Short ownerUserId;

	@SearchField(searchField = "ownerGroup.id")
	private Short ownerGroupId;

	@SearchField(searchField = "ownerUser.displayName,ownerUser.userName,ownerGroup.name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String ownerUserOrGroupName;

	@SearchField(searchField = "category.id")
	private Short categoryId;

	@SearchField(searchField = "systemBean.id", sortField = "systemBean.name")
	private Integer jobRunnerId;

	/**
	 * Customized search field that returns entities that have an ownerUserId that matches the ownedByUserId or a ownerGroupId that falls within the security groups assigned
	 * the user.
	 */
	private Short ownedByUserId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getRootNodesOnly() {
		return this.rootNodesOnly;
	}


	public void setRootNodesOnly(Boolean rootNodesOnly) {
		this.rootNodesOnly = rootNodesOnly;
	}


	public Boolean getEnabled() {
		return this.enabled;
	}


	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}


	public BatchJobStatuses getStatus() {
		return this.status;
	}


	public void setStatus(BatchJobStatuses status) {
		this.status = status;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getParentName() {
		return this.parentName;
	}


	public void setParentName(String parentName) {
		this.parentName = parentName;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Integer getMaxExecutionTimeMillis() {
		return this.maxExecutionTimeMillis;
	}


	public void setMaxExecutionTimeMillis(Integer maxExecutionTimeMillis) {
		this.maxExecutionTimeMillis = maxExecutionTimeMillis;
	}


	public Short getOwnerUserId() {
		return this.ownerUserId;
	}


	public void setOwnerUserId(Short ownerUserId) {
		this.ownerUserId = ownerUserId;
	}


	public Short getOwnerGroupId() {
		return this.ownerGroupId;
	}


	public void setOwnerGroupId(Short ownerGroupId) {
		this.ownerGroupId = ownerGroupId;
	}


	public String getOwnerUserOrGroupName() {
		return this.ownerUserOrGroupName;
	}


	public void setOwnerUserOrGroupName(String ownerUserOrGroupName) {
		this.ownerUserOrGroupName = ownerUserOrGroupName;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Integer getJobRunnerId() {
		return this.jobRunnerId;
	}


	public void setJobRunnerId(Integer jobRunnerId) {
		this.jobRunnerId = jobRunnerId;
	}


	public Short getOwnedByUserId() {
		return this.ownedByUserId;
	}


	public void setOwnedByUserId(Short ownedByUserId) {
		this.ownedByUserId = ownedByUserId;
	}
}
