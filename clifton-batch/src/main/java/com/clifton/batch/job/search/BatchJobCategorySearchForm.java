package com.clifton.batch.job.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * The <code>BatchJobCategorySearchForm</code> class defines search configuration for BatchJobCategory objects.
 *
 * @author lnaylor
 */
public class BatchJobCategorySearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "entityModifyCondition.name")
	private String entityModifyConditionName;

	@SearchField(searchField = "entityExecuteCondition.name")
	private String entityExecuteConditionName;

	@SearchField(searchField = "defaultOwnerSecurityGroup.id")
	private Short defaultOwnerSecurityGroupId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getEntityModifyConditionName() {
		return this.entityModifyConditionName;
	}


	public void setEntityModifyConditionName(String entityModifyConditionName) {
		this.entityModifyConditionName = entityModifyConditionName;
	}


	public String getEntityExecuteConditionName() {
		return this.entityExecuteConditionName;
	}


	public void setEntityExecuteConditionName(String entityExecuteConditionName) {
		this.entityExecuteConditionName = entityExecuteConditionName;
	}


	public Short getDefaultOwnerSecurityGroupId() {
		return this.defaultOwnerSecurityGroupId;
	}


	public void setDefaultOwnerSecurityGroupId(Short defaultOwnerSecurityGroupId) {
		this.defaultOwnerSecurityGroupId = defaultOwnerSecurityGroupId;
	}
}
