package com.clifton.batch.job;


import com.clifton.batch.job.search.BatchJobCategorySearchForm;
import com.clifton.batch.job.search.BatchJobHistorySearchForm;
import com.clifton.batch.job.search.BatchJobSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>BatchJobService</code> interface defines methods for working with BatchJob objects.
 *
 * @author mwacker
 */
public interface BatchJobService {

	////////////////////////////////////////////////////////////////////////////
	////////          Batch Job Business Methods 		             ///////////
	////////////////////////////////////////////////////////////////////////////


	public BatchJob getBatchJob(int id);


	public List<BatchJob> getBatchJobList(BatchJobSearchForm searchForm);


	/**
	 * Returns a list of active batch jobs for give time.
	 */
	public List<BatchJob> getBatchJobListActiveOnDate(Date startDateTime);


	public List<BatchJob> getBatchJobListByParent(int id);


	public BatchJob saveBatchJob(BatchJob bean);


	/**
	 * Updates just the status of the job.  In case other properties are updated by a user while a job is running, this will prevent
	 * any conflicts for versioning
	 * <p>
	 * Additional Validation is performed based on the status that is being moved to.
	 * Examples:
	 * Update BatchJob to Running Status - Job cannot currently be in Running status, but can be in anything else
	 * Update BatchJob to a completed or failed status - Job must only be in Running status
	 */
	@DoNotAddRequestMapping
	public BatchJob updateBatchJobStatus(int id, BatchJobStatuses status);


	public void deleteBatchJob(int id);


	////////////////////////////////////////////////////////////////////////////
	////////            Batch Job History Business Methods        //////////////
	////////////////////////////////////////////////////////////////////////////


	public BatchJobHistory getBatchJobHistory(int id);


	public BatchJobHistory getBatchJobHistoryByDate(int jobId, Date scheduledDate);


	public List<BatchJobHistory> getBatchJobHistoryList(BatchJobHistorySearchForm searchForm);


	public BatchJobHistory saveBatchJobHistory(BatchJobHistory bean);

	////////////////////////////////////////////////////////////////////////////
	////////            Batch Job Category Business Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	public BatchJobCategory getBatchJobCategory(short id);


	public List<BatchJobCategory> getBatchJobCategoryList(BatchJobCategorySearchForm searchForm);


	public BatchJobCategory saveBatchJobCategory(BatchJobCategory category);


	public void deleteBatchJobCategory(short id);
}
