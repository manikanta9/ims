package com.clifton.batch.job;

import com.clifton.core.beans.NamedEntity;
import com.clifton.security.user.SecurityGroup;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>BatchJobCategory</code> class defines a batch job category.  Categories are used to classify/group related jobs together
 * as well as control permissions around job execution and changes.
 *
 * @author lnaylor
 */
public class BatchJobCategory extends NamedEntity<Short> {

	/**
	 * Optionally restricts insert/update/delete access to batch jobs that belongs to this category.
	 */
	private SystemCondition entityModifyCondition;

	/**
	 * Optionally restricts execution access (who can run the job) for batch jobs that belong to this category.
	 */
	private SystemCondition entityExecuteCondition;

	private SecurityGroup defaultOwnerSecurityGroup;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public SystemCondition getEntityExecuteCondition() {
		return this.entityExecuteCondition;
	}


	public void setEntityExecuteCondition(SystemCondition entityExecuteCondition) {
		this.entityExecuteCondition = entityExecuteCondition;
	}


	public SecurityGroup getDefaultOwnerSecurityGroup() {
		return this.defaultOwnerSecurityGroup;
	}


	public void setDefaultOwnerSecurityGroup(SecurityGroup defaultOwnerSecurityGroup) {
		this.defaultOwnerSecurityGroup = defaultOwnerSecurityGroup;
	}
}
