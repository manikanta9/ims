package com.clifton.batch.job.search;


import com.clifton.batch.job.BatchJobStatuses;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseEntityDateRangeWithTimeSearchForm;

import java.util.Date;


/**
 * The <code>BatchJobHistorySearchForm</code> class defines search configuration for BatchJobHistory objects.
 *
 * @author vgomelsky
 */
public class BatchJobHistorySearchForm extends BaseEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "batchJob.name,batchJob.description,description,batchJob.category.name")
	private String searchPattern;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "batchJob.id")
	private Integer batchJobId;

	@SearchField(searchField = "name", searchFieldPath = "batchJob")
	private String batchJobName;

	@SearchField
	private BatchJobStatuses status;

	@SearchField(dateFieldIncludesTime = true)
	private Date scheduledDate;

	@SearchField
	private String description;

	@SearchField(searchField = "status", comparisonConditions = ComparisonConditions.IN)
	private BatchJobStatuses[] statusList;

	@SearchField(searchField = "retryCount", searchFieldPath = "batchJob", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean retryCountNotNull;

	@SearchField
	private Short retryAttemptNumber;

	@SearchField(searchFieldPath = "batchJob")
	private Integer maxExecutionTimeMillis;

	@SearchField(searchField = "batchJob.ownerUser.id")
	private Short ownerUserId;

	@SearchField(searchField = "batchJob.ownerGroup.id")
	private Short ownerGroupId;

	@SearchField(searchField = "batchJob.ownerUser.displayName,batchJob.ownerUser.userName,batchJob.ownerGroup.name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String ownerUserOrGroupName;

	@SearchField(searchField = "batchJob.category.id")
	private Short categoryId;

	@SearchField(searchField = "batchJob.systemBean.id", sortField = "batchJob.systemBean.name")
	private Integer jobRunnerId;

	@SearchField(searchField = "runnerUser.id")
	private Short runnerUserId;

	/**
	 * Customized search field that returns entities that have an ownerUserId that matches the ownedByUserId or a ownerGroupId that falls within the security groups assigned
	 * the user.
	 */
	private Short ownedByUserId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Integer getBatchJobId() {
		return this.batchJobId;
	}


	public void setBatchJobId(Integer batchJobId) {
		this.batchJobId = batchJobId;
	}


	public String getBatchJobName() {
		return this.batchJobName;
	}


	public void setBatchJobName(String batchJobName) {
		this.batchJobName = batchJobName;
	}


	public BatchJobStatuses getStatus() {
		return this.status;
	}


	public void setStatus(BatchJobStatuses status) {
		this.status = status;
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public BatchJobStatuses[] getStatusList() {
		return this.statusList;
	}


	public void setStatusList(BatchJobStatuses[] statusList) {
		this.statusList = statusList;
	}


	public Boolean getRetryCountNotNull() {
		return this.retryCountNotNull;
	}


	public void setRetryCountNotNull(Boolean retryCountNotNull) {
		this.retryCountNotNull = retryCountNotNull;
	}


	public Short getRetryAttemptNumber() {
		return this.retryAttemptNumber;
	}


	public void setRetryAttemptNumber(Short retryAttemptNumber) {
		this.retryAttemptNumber = retryAttemptNumber;
	}


	public Integer getMaxExecutionTimeMillis() {
		return this.maxExecutionTimeMillis;
	}


	public void setMaxExecutionTimeMillis(Integer maxExecutionTimeMillis) {
		this.maxExecutionTimeMillis = maxExecutionTimeMillis;
	}


	public Short getOwnerUserId() {
		return this.ownerUserId;
	}


	public void setOwnerUserId(Short ownerUserId) {
		this.ownerUserId = ownerUserId;
	}


	public Short getOwnerGroupId() {
		return this.ownerGroupId;
	}


	public void setOwnerGroupId(Short ownerGroupId) {
		this.ownerGroupId = ownerGroupId;
	}


	public String getOwnerUserOrGroupName() {
		return this.ownerUserOrGroupName;
	}


	public void setOwnerUserOrGroupName(String ownerUserOrGroupName) {
		this.ownerUserOrGroupName = ownerUserOrGroupName;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Integer getJobRunnerId() {
		return this.jobRunnerId;
	}


	public void setJobRunnerId(Integer jobRunnerId) {
		this.jobRunnerId = jobRunnerId;
	}


	public Short getOwnedByUserId() {
		return this.ownedByUserId;
	}


	public void setOwnedByUserId(Short ownedByUserId) {
		this.ownedByUserId = ownedByUserId;
	}


	public Short getRunnerUserId() {
		return this.runnerUserId;
	}


	public void setRunnerUserId(Short runnerUserId) {
		this.runnerUserId = runnerUserId;
	}
}
