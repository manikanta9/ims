package com.clifton.batch.job;


/**
 * The <code>BatchJobStatus</code> represents the status of a batch job execution.
 *
 * @author mwacker
 */
public enum BatchJobStatuses {

	/**
	 * This is usually the very first state of a new job that has never ran.
	 */
	NONE(false),
	/**
	 * Execution of this job was skipped. This could happen because previous instance of this job is still running.
	 */
	SKIPPED(false),
	/**
	 * The job is currently running and we do not know yet if it will succeed or fail.
	 */
	RUNNING(false),
	/**
	 * The job completed its intended processing successfully.
	 */
	COMPLETED(false),
	/**
	 * The job completed but it did not do anything because the data required for processing was not available or
	 * condition required for processing did not occur.
	 */
	COMPLETED_WITH_NO_RESULTS(false),
	/**
	 * The job completed its intended processing successfully. It identified and recorded in history some information
	 * that maybe useful to know.  However, these cases are not errors.
	 */
	COMPLETED_WITH_WARNINGS(false),
	/**
	 * The job completed its processing and ran into errors that are usually recorded in job history. When this happens,
	 * often times some cases are processed successfully by the job while others fail.
	 */
	COMPLETED_WITH_ERRORS(true),
	/**
	 * Processing of this batch job failed. This usually means that non of intended logic of the job was executed because
	 * of an issue with logic or unavailable dependent resources (lost database connection). These cases require immediate
	 * attention, correction of the problem and rerun of the job.
	 */
	FAILED(true);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	BatchJobStatuses(boolean failedStatus) {
		this.failedStatus = failedStatus;
	}


	private final boolean failedStatus;


	public boolean isFailedStatus() {
		return this.failedStatus;
	}
}
