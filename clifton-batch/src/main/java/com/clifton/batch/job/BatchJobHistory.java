package com.clifton.batch.job;


import com.clifton.core.beans.hierarchy.HierarchicalSimpleEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityUser;

import java.util.Date;


/**
 * The <code>BatchJobHistory</code> defines a scheduled instance of a batch job.
 * There will be a history record for each run.
 * <p>
 * 'description' field may include run stats for successful jobs or error message for failed jobs
 *
 * @author mwacker
 */
public class BatchJobHistory extends HierarchicalSimpleEntity<BatchJobHistory, Integer> {

	private BatchJob batchJob;
	private BatchJobStatuses status;

	private SecurityUser runnerUser;

	private Date scheduledDate;
	private Date startDate;
	private Date endDate;
	private String description;

	/**
	 * Indicates the retry attempt that the history entry is for.
	 */
	private Short retryAttemptNumber;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "{id=" + getId() + ", " + getStatus() + ", " + getDescription() + '}';
	}


	public String getExecutionTimeFormatted() {
		if (getStartDate() == null) {
			return null;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getTimeDifferenceShort(toDate, getStartDate());
	}


	public long getExecutionTimeInSeconds() {
		if (getStartDate() == null) {
			return 0;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}

		return DateUtils.getSecondsDifference(toDate, getStartDate());
	}


	public long getExecutionTimeInMillis() {
		if (getStartDate() == null) {
			return 0;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getMillisecondsDifference(toDate, getStartDate());
	}


	public BatchJob getBatchJob() {
		return this.batchJob;
	}


	public void setBatchJob(BatchJob batchJob) {
		this.batchJob = batchJob;
	}


	public BatchJobStatuses getStatus() {
		return this.status;
	}


	public void setStatus(BatchJobStatuses status) {
		this.status = status;
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getRetryAttemptNumber() {
		return this.retryAttemptNumber;
	}


	public void setRetryAttemptNumber(Short retryAttemptNumber) {
		this.retryAttemptNumber = retryAttemptNumber;
	}


	public SecurityUser getRunnerUser() {
		return this.runnerUser;
	}


	public void setRunnerUser(SecurityUser runnerUser) {
		this.runnerUser = runnerUser;
	}
}
