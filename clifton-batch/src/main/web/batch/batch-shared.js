Ext.ns('Clifton.batch.job');

Clifton.batch.job.StatusOptions = [
	['NONE', 'NONE', 'This is usually the very first state of a new job that has never ran.'],
	['SKIPPED', 'SKIPPED', 'Execution of this job was skipped. This could happen because previous instance of this job is still running.'],
	['RUNNING', 'RUNNING', 'The job is currently running and we do not know yet if it will succeed or fail.'],
	['COMPLETED', 'COMPLETED', 'The job completed its intended processing successfully.'],
	['COMPLETED_WITH_NO_RESULTS', 'COMPLETED_WITH_NO_RESULTS', 'The job completed but it did not do anything because the data required for processing was not available or condition required for processing did not occur.'],
	['COMPLETED_WITH_WARNINGS', 'COMPLETED_WITH_WARNINGS', 'The job completed its intended processing successfully. It identified and recorded in history some information that maybe useful to know.  However, these cases are not errors.'],
	['COMPLETED_WITH_ERRORS', 'COMPLETED_WITH_ERRORS', 'The job completed its processing and ran into errors that are usually recorded in job history. When this happens, often times some cases are processed successfully by the job while others fail.'],
	['FAILED', 'FAILED', 'Processing of this batch job failed. This usually means that non of intended logic of the job was executed because of an issue with logic or unavailable dependent resources (lost database connection). These cases require immediate attention, correction of the problem and rerun of the job.']
];

Clifton.batch.job.DisplayOptions = [
	['ALL', 'All', 'Display all batch jobs'],
	['OWNED_BY_ME', 'Owned By Me', 'Display batch jobs where current user is the Owner User or belongs to the Owner Group.'],
	['COMPLETED', 'Completed', 'Display only completed batch jobs'],
	['REQUIRES_ATTENTION', 'Requires Attention', 'Display only batch jobs that may need manual intervention'],
	['FAILED', 'Failed', 'Display only Failed batch jobs'],
	['RUNNING', 'Running', 'Display only running batch jobs'],
	['SKIPPED', 'Skipped', 'Display only skipped batch jobs']
];


Clifton.batch.job.renderJobStatus = function(v) {
	if (v === 'FAILED' || v === 'COMPLETED_WITH_ERRORS') {
		return '<div class="amountNegative">' + v + '</div>';
	}
	if (v === 'RUNNING') {
		return '<div class="amountPositive">' + v + '</div>';
	}
	if (v === 'COMPLETED_WITH_WARNINGS') {
		return '<div class="amountAdjusted">' + v + '</div>';
	}
	return v;
};
