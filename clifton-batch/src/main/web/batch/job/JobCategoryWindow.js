Clifton.batch.job.JobCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Batch Job Category',
	iconCls: 'rule',
	enableRefreshWindow: true,
	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Category',
				items: [{
					xtype: 'formpanel',
					url: 'batchJobCategory.json',
					instructions: 'A batch job category is used to group batch jobs and to assign properties and conditions to its members.',
					labelWidth: 140,
					items: [
						{fieldLabel: 'Category Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Job Modify Condition', xtype: 'system-entity-modify-condition-combo'},
						{fieldLabel: 'Job Execute Condition', xtype: 'system-condition-combo', name: 'entityExecuteCondition.label', hiddenName: 'entityExecuteCondition.id', qtip: 'Optionally specify condition that must evaluate to true in order to execute/run batch jobs for this category.'},
						{fieldLabel: 'Default Owner Group', name: 'defaultOwnerSecurityGroup.label', hiddenName: 'defaultOwnerSecurityGroup.id', xtype: 'combo', displayField: 'label', url: 'securityGroupListFind.json', detailPageClass: 'Clifton.security.user.GroupWindow'}
					]
				}]
			}
		]
	}]

});
