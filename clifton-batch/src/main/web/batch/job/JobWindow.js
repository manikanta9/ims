// REQUIRED PROPERTY FOR STEP NEW WINDOWS: defaultData.batchJobStep = true
Clifton.batch.job.JobWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'batchJob.json',

	getClassName: function(config, entity) {
		// need to load existing job first in order to determine if it's parent or child
		return 'Clifton.batch.job.DelayedJobWindow';
	}
});

Clifton.batch.job.DelayedJobWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Batch Job',
	iconCls: 'clock',
	width: 1100,
	height: 570,
	enableRefreshWindow: true,

	windowOnShow: function(w) {
		// only add if the workflow project is present
		if (Clifton.workflow) {
			const tabs = w.items.get(0);
			tabs.add({
				title: 'Tasks',
				disabled: true,
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'BatchJob'
				}]
			});
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				tbar: [{
					text: 'Run Now',
					tooltip: 'Run this Batch Job now',
					iconCls: 'config',
					handler: function() {
						const f = this.ownerCt.ownerCt.items.get(0);
						const w = f.getWindow();
						Ext.Msg.confirm('Run Now', 'Would you like to run this batch job now?', function(a) {
							if (a === 'yes') {
								if (w.isModified() || !w.isMainFormSaved()) {
									TCG.showError('Please save your changes before running the batch job.');
									return false;
								}
								const params = {id: w.getMainFormId()};
								const loader = new TCG.data.JsonLoader({
									waitTarget: f,
									waitMsg: 'Starting...',
									params: params,
									conf: params,
									onLoad: function(record, conf) {
										Ext.Msg.alert('Status', 'The job was scheduled to start immediately.');
									}
								});
								loader.load('batchJobRun.json');
							}
						});
					}
				}],
				items: [{
					xtype: 'formpanel',
					instructions: 'Batch job is a task that runs on a predefined schedule. Use steps only for multi-step jobs.',
					url: 'batchJob.json',
					labelWidth: 150,
					getWarningMessage: function(f) {
						return f.findField('enabled').getValue() ? undefined : 'To enable this batch job you must check the <i>Enable</i> checkbox.';
					},
					// dynamically set items (different for steps vs parent jobs)
					initComponent: function() {
						const w = this.getWindow();
						const step = (TCG.getValue('defaultData.batchJobStep', w) || TCG.getValue('defaultData.parent', w));
						this.items = [
							{
								fieldLabel: 'Job Category', name: 'category.name', hiddenName: 'category.id', requestedProps: 'defaultOwnerSecurityGroup', displayField: 'name',
								xtype: 'combo', url: 'batchJobCategoryListFind.json', detailPageClass: 'Clifton.batch.job.JobCategoryWindow',
								listeners: {
									beforeselect: (combo, record, index) => {
										if (!this.getIdFieldValue()) {
											const ownerGroupId = this.getForm().findField('ownerGroup.id').getValue();
											if (TCG.isBlank(ownerGroupId) || ownerGroupId === combo.getValueObject()?.defaultOwnerSecurityGroup?.id) {
												const categoryInfo = record.json.defaultOwnerSecurityGroup;
												this.getForm().findField('ownerGroup.label').setValue({text: categoryInfo?.label, value: categoryInfo?.id});
											}
										}
									}
								}
							},
							{fieldLabel: 'Job Name', name: 'name'},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
							{
								fieldLabel: 'Job Runner', name: 'systemBean.name', hiddenName: 'systemBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Batch Job',
								detailPageClass: 'Clifton.system.bean.BeanWindow',
								getDefaultData: function() {
									return {type: {group: {name: 'Batch Job', alias: 'Batch Job'}}};
								}
							},
							{fieldLabel: 'Run as', name: 'runAsUser.label', hiddenName: 'runAsUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', detailPageClass: 'Clifton.security.user.UserWindow'},
							{
								xtype: 'panel', layout: 'column', defaults: {layout: 'form'},
								items: [
									{
										columnWidth: .50,
										labelWidth: 150,
										defaults: {anchor: '0'},
										items: [
											{fieldLabel: 'Owner User', name: 'ownerUser.label', hiddenName: 'ownerUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json', detailPageClass: 'Clifton.security.user.UserWindow'},
											{fieldLabel: 'Owner Group', name: 'ownerGroup.label', hiddenName: 'ownerGroup.id', xtype: 'combo', displayField: 'label', url: 'securityGroupListFind.json', detailPageClass: 'Clifton.security.user.GroupWindow'}
										]
									},
									{
										columnWidth: .50,
										labelWidth: 130,
										defaults: {anchor: '0'},
										style: {paddingLeft: '20px'},
										items: [
											{fieldLabel: 'Support User', name: 'supportUser.label', hiddenName: 'supportUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json', detailPageClass: 'Clifton.security.user.UserWindow', qtip: 'The user responsible for supporting the job in case of failures.'},
											{fieldLabel: 'Support Group', name: 'supportGroup.label', hiddenName: 'supportGroup.id', xtype: 'combo', displayField: 'label', url: 'securityGroupListFind.json', detailPageClass: 'Clifton.security.user.GroupWindow', qtip: 'The user group responsible for supporting the job in case of failures.'}

										]
									}
								]
							},
							{fieldLabel: 'Retry Count', name: 'retryCount', xtype: 'integerfield', qtip: 'This will cause the job to retry and should be used only be used on jobs without children unless totally necessary.'},
							{fieldLabel: 'Retry Delay In Seconds', name: 'retryDelayInSeconds', xtype: 'integerfield'},
							{fieldLabel: 'Max Execution Time (ms)', name: 'maxExecutionTimeMillis', xtype: 'integerfield', qtip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'}
						];
						const addItemsAfter = (name, ...newItems) => this.items.splice(this.items.findIndex(i => i.name === name) + 1, 0, ...newItems);
						if (step) {
							w.titlePrefix = 'Batch Job Step';
							this.instructions = 'Batch job steps execute immediately after successful execution of parent batch job in the order defined here.';
							this.items.splice(0, 0, {fieldLabel: 'Parent Job', name: 'parent.name', xtype: 'linkfield', detailPageClass: 'Clifton.batch.job.JobWindow', detailIdField: 'parent.id'});
							addItemsAfter('ownerGroup.label', {fieldLabel: 'Step Order', name: 'order', xtype: 'spinnerfield', minValue: -10000, allowBlank: false});
							addItemsAfter('maxExecutionTimeMillis', {boxLabel: 'Enable this batch job step', name: 'enabled', xtype: 'checkbox', labelSeparator: ''});
							addItemsAfter('enabled', {boxLabel: 'Stop execution and fail parent job if this step fails', name: 'failOnChildJobs', xtype: 'checkbox', labelSeparator: ''});
						}
						else {
							addItemsAfter('systemBean.name', {fieldLabel: 'Schedule', name: 'schedule.name', hiddenName: 'schedule.id', xtype: 'combo', url: 'calendarScheduleListFind.json?calendarScheduleTypeName=Standard Schedules&systemDefined=false', detailPageClass: 'Clifton.calendar.schedule.ScheduleWindow', allowBlank: false});
							addItemsAfter('maxExecutionTimeMillis', {boxLabel: 'Enable this batch job', name: 'enabled', xtype: 'checkbox', labelSeparator: ''});
						}
						TCG.form.FormPanel.superclass.initComponent.call(this);
					}
				}]
			},


			{
				title: 'Job Steps',
				items: [{
					name: 'batchJobListByParent',
					xtype: 'gridpanel',
					instructions: 'Use steps when more than one related step is required. After executing the main job, steps will be executed in the specified order.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Order', width: 30, dataIndex: 'order', type: 'int'},
						{header: 'Job Name', width: 250, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Run As', width: 50, dataIndex: 'runAsUser.label'},
						{header: 'Status', width: 110, dataIndex: 'status', renderer: Clifton.batch.job.renderJobStatus, filter: {type: 'list', options: Clifton.batch.job.StatusOptions}},
						{header: 'Retry Count', width: 45, dataIndex: 'retryCount', type: 'int', useNull: true},
						{header: 'Retry Delay Seconds', width: 100, dataIndex: 'retryDelayInSeconds', type: 'int', useNull: true, hidden: true},
						{header: 'Enabled', width: 40, dataIndex: 'enabled', type: 'boolean'}
					],
					getLoadParams: function() {
						return {id: this.getWindow().getMainFormId()};
					},
					editor: {
						deleteURL: 'batchJobDelete.json',
						detailPageClass: 'Clifton.batch.job.JobWindow',
						getDefaultData: function(gridPanel) {
							return {
								batchJobStep: true,
								parent: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'History',
				items: [{
					name: 'batchJobHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'History of runs for the current batch job.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'id|batchJob.maxExecutionTimeMillis',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Job Name', width: 100, dataIndex: 'batchJob.name', filter: {searchFieldName: 'batchJobName'}, hidden: true},
						{header: 'Scheduled', width: 80, dataIndex: 'scheduledDate', hidden: true},
						{header: 'Started', width: 55, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Ended', width: 55, dataIndex: 'endDate'},
						{
							header: 'Duration', width: 55, dataIndex: 'executionTimeFormatted', filter: {type: 'int', sortFieldName: 'executionTimeInSeconds', searchFieldName: 'executionTimeInSeconds'},
							renderer: function(v, metaData, r) {
								return TCG.renderMaxSLA(v, TCG.getValue('executionTimeInMillis', r.json), TCG.getValue('batchJob.maxExecutionTimeMillis', r.json), metaData);
							}
						},
						{header: 'Runner', width: 40, dataIndex: 'runnerUser.label', filter: {type: 'combo', searchFieldName: 'runnerUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true}},
						{header: 'Duration In Millis', width: 50, dataIndex: 'executionTimeInMillis', type: 'int', hidden: true},
						{header: 'Retry', dataIndex: 'retryCountNumber', width: 60, type: 'int', title: 'The current retry for the job.', hidden: true, useNull: true},
						{header: 'Status', width: 85, dataIndex: 'status', renderer: Clifton.batch.job.renderJobStatus, filter: {type: 'list', options: Clifton.batch.job.StatusOptions}},
						{header: 'Message', width: 230, dataIndex: 'description', renderer: TCG.stripTags}
					],
					getTopToolbarInitialLoadParams: function() {
						return {batchJobId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.batch.job.HistoryWindow'
					}
				}]
			},


			{
				title: 'Live Status',
				items: [{
					xtype: 'core-scheduled-runner-grid',
					typeName: 'BATCH',
					getLoadParams: function() {
						return {
							type: 'BATCH',
							typeId: this.getWindow().getMainFormId()
						};
					}
				}]
			}
		]
	}]
});
