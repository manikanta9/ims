Clifton.batch.job.JobSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'jobSetupWindow',
	title: 'Batch Jobs',
	iconCls: 'clock',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Jobs',
				items: [{
					name: 'batchJobListFind',
					xtype: 'gridpanel',
					instructions: 'Batch jobs are periodic tasks that run on a predefined schedule. Depending on Display filter choice, this list contains main jobs only or both main jobs and excludes steps.',
					wikiPage: 'IT/Batch+Job+Processing',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'category.name',
					groupTextTpl: '{values.text}: {[values.rs.length]} {[values.rs.length > 1 ? "Jobs" : "Job"]}',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Parent Job', width: 100, dataIndex: 'parent.name', filter: {searchFieldName: 'parentName'}, hidden: true},
						{
							header: 'Job Category', width: 40, dataIndex: 'category.name', hidden: true,
							filter: {type: 'combo', searchFieldName: 'categoryId', displayField: 'name', url: 'batchJobCategoryListFind.json'}
						},
						{header: 'Job Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Schedule', width: 70, dataIndex: 'schedule.label'},
						{header: 'Parent Schedule', width: 70, dataIndex: 'parent.schedule.label', hidden: true},
						{header: 'Job Runner', width: 150, dataIndex: 'systemBean.name', filter: {type: 'combo', searchFieldName: 'jobRunnerId', url: 'systemBeanListFind.json?groupName=Batch Job'}, hidden: true},
						{header: 'Run As', width: 30, dataIndex: 'runAsUser.label'},
						{header: 'Owner', dataIndex: 'ownerLabel', filter: {searchFieldName: 'ownerUserOrGroupName'}, sortable: false, width: 30},
						{header: 'Owner User', dataIndex: 'ownerUser.label', width: 50, idDataIndex: 'ownerUser.id', filter: {searchFieldName: 'ownerUserId', displayField: 'label', type: 'combo', url: 'securityUserListFind.json'}, hidden: true},
						{header: 'Owner Group', dataIndex: 'ownerGroup.label', width: 50, idDataIndex: 'ownerGroup.id', filter: {searchFieldName: 'ownerGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
						{header: 'Status', width: 40, dataIndex: 'status', renderer: Clifton.batch.job.renderJobStatus, filter: {type: 'list', options: Clifton.batch.job.StatusOptions}},
						{header: 'Retry Count', width: 20, dataIndex: 'retryCount', type: 'int', useNull: true},
						{header: 'Retry Delay Seconds', width: 40, dataIndex: 'retryDelayInSeconds', type: 'int', useNull: true, hidden: true},
						{header: 'Max Execution Time (ms)', width: 40, dataIndex: 'maxExecutionTimeMillis', type: 'int', useNull: true, hidden: true, qtip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
						{header: 'Step Order', width: 20, dataIndex: 'order', type: 'int', useNull: true, hidden: true},
						{header: 'Enabled', width: 20, dataIndex: 'enabled', type: 'boolean'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern',
								listeners: {
									specialkey: function(field, e) {
										if (e.getKey() === e.ENTER) {
											const grid = TCG.getParentByClass(field, Ext.Panel);
											TCG.getChildByName(grid.getTopToolbar(), 'displayType').setValue('ALL');
											grid.reload();
										}
									}
								}
							},
							{
								fieldLabel: 'Include Steps', xtype: 'toolbar-combo', name: 'displayType', width: 50, minListWidth: 50, mode: 'local', value: 'JOBS',
								store: {
									xtype: 'arraystore',
									data: [['JOBS', 'No', 'Display only main batch jobs (do not include job steps)'], ['ALL', 'Yes', 'Display both main jobs and corresponding job steps']]
								}
							},
							{
								fieldLabel: 'Category', xtype: 'toolbar-combo', width: 110, minListWidth: 150,
								url: 'batchJobCategoryListFind.json', linkedFilter: 'category.name'
							},
							{
								fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'status', width: 110, minListWidth: 150, forceSelection: true, mode: 'local', value: 'ALL',
								store: {
									xtype: 'arraystore',
									data: Clifton.batch.job.DisplayOptions
								}
							}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const toolBarInitialLoadParams = Clifton.batch.job.getToolbarDisplayFilterParamsAndSetFilter(this);
						const displayType = TCG.getChildByName(this.getTopToolbar(), 'displayType');
						if (displayType.getValue() !== 'ALL') {
							toolBarInitialLoadParams.rootNodesOnly = true;
						}

						return toolBarInitialLoadParams;
					},
					editor: {
						detailPageClass: 'Clifton.batch.job.JobWindow'
					}
				}]
			},


			{
				title: 'Job History',
				items: [{
					name: 'batchJobHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'History of runs for all batch jobs',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					additionalPropertiesToRequest: 'id|batchJob.maxExecutionTimeMillis',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Parent Job', width: 100, dataIndex: 'parent.batchJob.name', hidden: true},
						{
							header: 'Job Category', width: 50, dataIndex: 'batchJob.category.name', hidden: true,
							filter: {type: 'combo', searchFieldName: 'categoryId', displayField: 'name', url: 'batchJobCategoryListFind.json'}
						},
						{
							header: 'Job Name', width: 120, dataIndex: 'batchJob.name', filter: {searchFieldName: 'batchJobName'},
							renderer: function(value, metaData, record) {
								if (record.json.parent) {
									metaData.attr = 'style="PADDING-LEFT: 25px"';
								}
								return value;
							}
						},
						{header: 'Job Runner', width: 200, dataIndex: 'batchJob.systemBean.name', filter: {searchFieldName: 'jobRunnerId', type: 'combo', url: 'systemBeanListFind.json?groupName=Batch Job'}, hidden: true},
						{header: 'Scheduled', width: 50, dataIndex: 'scheduledDate', hidden: true},
						{header: 'Started', width: 50, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Ended', width: 50, dataIndex: 'endDate', hidden: true},
						{
							header: 'Duration', width: 55, dataIndex: 'executionTimeFormatted', filter: {type: 'int', sortFieldName: 'executionTimeInSeconds', searchFieldName: 'executionTimeInSeconds'},
							renderer: function(v, metaData, r) {
								return TCG.renderMaxSLA(v, TCG.getValue('executionTimeInMillis', r.json), TCG.getValue('batchJob.maxExecutionTimeMillis', r.json), metaData);
							}
						},
						{header: 'Duration In Seconds', width: 55, dataIndex: 'executionTimeInSeconds', type: 'int', hidden: true},
						{header: 'Duration In Millis', width: 50, dataIndex: 'executionTimeInMillis', type: 'int', hidden: true},
						{header: 'Max Time Millis', width: 40, dataIndex: 'batchJob.maxExecutionTimeMillis', type: 'int', useNull: true, filter: {searchFieldName: 'maxExecutionTimeMillis'}, hidden: true, tooltip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
						{header: 'Runner', width: 40, dataIndex: 'runnerUser.label', filter: {type: 'combo', searchFieldName: 'runnerUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true}},
						{header: 'Owner', dataIndex: 'batchJob.ownerLabel', filter: {searchFieldName: 'ownerUserOrGroupName'}, sortable: false, width: 40},
						{header: 'Owner User', dataIndex: 'batchJob.ownerUser.label', width: 40, idDataIndex: 'batchJob.ownerUser.id', filter: {searchFieldName: 'ownerUserId', displayField: 'label', type: 'combo', url: 'securityUserListFind.json'}, hidden: true},
						{header: 'Owner Group', dataIndex: 'batchJob.ownerGroup.label', width: 40, idDataIndex: 'batchJob.ownerGroup.id', filter: {searchFieldName: 'ownerGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
						{header: 'Status', width: 70, dataIndex: 'status', renderer: Clifton.batch.job.renderJobStatus, filter: {type: 'list', options: Clifton.batch.job.StatusOptions}},
						{header: 'Retry Attempt', width: 40, dataIndex: 'retryAttemptNumber', type: 'int', useNull: true},
						{header: 'Message', width: 170, dataIndex: 'description', renderer: TCG.stripTags}
					],
					getTopToolbarFilters: function(toolbar) {
						const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.apply(this, arguments);
						filters.push(
								{
									fieldLabel: 'Category', xtype: 'toolbar-combo', width: 110, minListWidth: 150,
									url: 'batchJobCategoryListFind.json', linkedFilter: 'batchJob.category.name'
								}
						);
						filters.push(
								{
									fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'status', width: 110, minListWidth: 150, forceSelection: true, mode: 'local', value: 'ALL',
									store: {
										xtype: 'arraystore',
										data: Clifton.batch.job.DisplayOptions
									}
								}
						);
						return filters;
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const toolBarInitialLoadParams = Clifton.batch.job.getToolbarDisplayFilterParamsAndSetFilter(this);
						if (firstLoad) {
							// default to last 10 days of runs
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -10)});
						}

						return toolBarInitialLoadParams;
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.batch.job.HistoryWindow'
					}
				}]
			},


			{
				title: 'Job Categories',
				items: [{
					name: 'batchJobCategoryListFind',
					xtype: 'gridpanel',
					instructions: 'Batch job categories are used to group batch jobs and assign common properties or conditions to their members.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Job Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyConditionName'}},
						{header: 'Job Execute Condition', width: 100, dataIndex: 'entityExecuteCondition.name', filter: {searchFieldName: 'entityExecuteConditionName'}},
						{header: 'Default Owner Group', width: 50, dataIndex: 'defaultOwnerSecurityGroup.label', idDataIndex: 'defaultOwnerSecurityGroup.id', filter: {searchFieldName: 'defaultOwnerSecurityGroupId', type: 'combo', url: 'securityGroupListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.batch.job.JobCategoryWindow'
					}
				}]
			},


			{
				title: 'Scheduled Runs',
				items: [{
					xtype: 'core-scheduled-runner-grid',
					typeName: 'BATCH'
				}]
			},


			{
				title: 'Invalid Jobs',
				items: [{
					name: 'batchJobListInvalid',
					xtype: 'gridpanel',
					instructions: 'The following jobs are in invalid state: RUNNING status but nothing is running. This is likely resulted from server restart during job execution. Use "Fix Job" button to mark the job as FAILED and allow future executions.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Job Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Schedule', width: 70, dataIndex: 'schedule.label'},
						{header: 'Run As', width: 40, dataIndex: 'runAsUser.label'},
						{header: 'Status', width: 60, dataIndex: 'status', renderer: Clifton.batch.job.renderJobStatus, filter: {type: 'list', options: Clifton.batch.job.StatusOptions}},
						{header: 'Enabled', width: 20, dataIndex: 'enabled', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.batch.job.JobWindow',
						addEditButtons: function(t) {
							t.add({
								text: 'Fix Job',
								tooltip: 'Mark selected batch job as FAILED',
								iconCls: 'tools',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a batch job to be fixed.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Fix Selected Job', 'Would you like to mark selected batch job as FAILED?', function(a) {
											if (a === 'yes') {
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Fixing...',
													params: {id: sm.getSelected().id},
													onLoad: function(record, conf) {
														grid.getStore().remove(sm.getSelected());
														grid.ownerCt.updateCount();
													}
												});
												loader.load('batchJobFix.json');
											}
										});
									}
								}
							});
							t.add('-');
						}
					}
				}]
			}
		]
	}]
});

Clifton.batch.job.getDisplayFilter = function(value) {
	let statusFilter = null;
	let ownerFilter = null;
	if (TCG.isEquals(value, 'FAILED')) {
		statusFilter = ['FAILED'];
	}
	else if (TCG.isEquals(value, 'COMPLETED')) {
		statusFilter = ['COMPLETED', 'COMPLETED_WITH_NO_RESULTS', 'COMPLETED_WITH_WARNINGS', 'COMPLETED_WITH_ERRORS'];
	}
	else if (TCG.isEquals(value, 'REQUIRES_ATTENTION')) {
		statusFilter = ['RUNNING', 'FAILED', 'COMPLETED_WITH_WARNINGS', 'COMPLETED_WITH_ERRORS'];
	}
	else if (TCG.isEquals(value, 'RUNNING')) {
		statusFilter = ['NONE', 'RUNNING'];
	}
	else if (TCG.isEquals(value, 'SKIPPED')) {
		statusFilter = ['SKIPPED'];
	}
	else if (TCG.isEquals(value, 'OWNED_BY_ME')) {
		const currentUser = TCG.getCurrentUser();
		ownerFilter = currentUser.id;
	}
	return {statusFilter: statusFilter, ownerFilter: ownerFilter};
};

Clifton.batch.job.getToolbarDisplayFilterParamsAndSetFilter = function(gp) {
	const toolBarInitialLoadParams = {};
	const t = gp.getTopToolbar();
	const status = TCG.getChildByName(t, 'status');
	const v = status.getValue();
	const displayFilter = Clifton.batch.job.getDisplayFilter(v);
	if (TCG.isNull(displayFilter.statusFilter)) {
		gp.clearFilter('status', true);
	}
	else {
		gp.setFilterValue('status', displayFilter.statusFilter, false, true);
	}
	if (TCG.isNotBlank(displayFilter.ownerFilter)) {
		toolBarInitialLoadParams.ownedByUserId = displayFilter.ownerFilter;
	}
	return toolBarInitialLoadParams;
};
