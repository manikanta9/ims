Clifton.batch.job.HistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Batch Job History',
	width: 1100,
	height: 550,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'History',
				items: [{
					xtype: 'formpanel',
					url: 'batchJobHistory.json',
					labelFieldName: 'batchJob.name',
					readOnly: true,

					items: [
						{fieldLabel: 'Batch Job', name: 'batchJob.name', xtype: 'linkfield', detailPageClass: 'Clifton.batch.job.JobWindow', detailIdField: 'batchJob.id'},
						{fieldLabel: 'Parent History', name: 'parent.batchJob.name', xtype: 'linkfield', detailPageClass: 'Clifton.batch.job.HistoryWindow', detailIdField: 'parent.id'},
						{fieldLabel: 'Status', name: 'status'},
						{fieldLabel: 'Runner User', name: 'runnerUser.label', detailIdField: 'runnerUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.security.user.UserWindow'},
						{fieldLabel: 'Scheduled Time', name: 'scheduledDate'},
						{fieldLabel: 'Start Time', name: 'startDate'},
						{fieldLabel: 'End Time', name: 'endDate'},
						{fieldLabel: 'Duration', name: 'executionTimeFormatted'},
						{fieldLabel: 'Retry Attempt', name: 'retryAttemptNumber'},
						{fieldLabel: 'Message', name: 'description', xtype: 'textarea', anchor: '-35 -240'}
					],

					listeners: {
						afterload: function(fp) {
							if (TCG.isBlank(fp.getFormValue('parent.batchJob.name'))) {
								fp.hideField('parent.batchJob.name');
							}
						}
					}
				}]
			},


			{
				title: 'Child Histories',
				items: [{
					name: 'batchJobHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'History of job steps for this historic run.',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					additionalPropertiesToRequest: 'id|batchJob.maxExecutionTimeMillis',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Job Name', width: 100, dataIndex: 'batchJob.name', filter: {searchFieldName: 'batchJobName'}},
						{header: 'Runner', width: 50, dataIndex: 'runnerUser.label', filter: {type: 'combo', searchFieldName: 'runnerUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true}},
						{header: 'Scheduled', width: 80, dataIndex: 'scheduledDate', hidden: true},
						{header: 'Started', width: 80, dataIndex: 'startDate', defaultSortColumn: true, hidden: true},
						{header: 'Ended', width: 80, dataIndex: 'endDate', hidden: true},
						{
							header: 'Duration', width: 55, dataIndex: 'executionTimeFormatted', filter: {type: 'int', sortFieldName: 'executionTimeInSeconds', searchFieldName: 'executionTimeInSeconds'},
							renderer: function(v, metaData, r) {
								return TCG.renderMaxSLA(v, TCG.getValue('executionTimeInMillis', r.json), TCG.getValue('batchJob.maxExecutionTimeMillis', r.json), metaData);
							}
						},
						{header: 'Duration In Millis', width: 50, dataIndex: 'executionTimeInMillis', type: 'int', hidden: true},
						{header: 'Retry', dataIndex: 'retryCountNumber', width: 60, type: 'int', title: 'The current retry for the job.', hidden: true, useNull: true},
						{header: 'Status', width: 80, dataIndex: 'status', renderer: Clifton.batch.job.renderJobStatus, filter: {type: 'list', options: Clifton.batch.job.StatusOptions}},
						{header: 'Message', width: 200, dataIndex: 'description', renderer: TCG.stripTags}
					],
					getTopToolbarInitialLoadParams: function() {
						return {parentId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.batch.job.HistoryWindow'
					}
				}]
			}
		]
	}]
});
