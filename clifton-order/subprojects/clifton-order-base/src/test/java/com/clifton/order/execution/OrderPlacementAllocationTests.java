package com.clifton.order.execution;

import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.builder.OrderAllocationBuilder;
import com.clifton.order.block.builder.OrderBlockBuilder;
import com.clifton.order.execution.builder.OrderPlacementBuilder;
import com.clifton.order.setup.builder.OrderDestinationBuilder;
import com.clifton.order.shared.builder.OrderCompanyBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author manderson
 */
public class OrderPlacementAllocationTests {

	@Test
	public void testGetLabel() {
		OrderPlacementAllocation placementAllocation = new OrderPlacementAllocation();
		placementAllocation.setId((long) 500);
		Assertions.assertEquals("500", placementAllocation.getLabel());

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewStockOrderAllocationGBP("BATS").build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(orderAllocation).build()).build();
		placementAllocation.setOrderAllocation(orderAllocation);
		placementAllocation.setOrderPlacement(orderPlacement);
		placementAllocation.setQuantity(orderAllocation.getQuantityIntended());
		Assertions.assertEquals("BUY 285 of BATS on 06/07/2021 for Test-123 (Test Custodian)", placementAllocation.getLabel());

		placementAllocation.getOrderPlacement().setOrderDestination(OrderDestinationBuilder.createFixExecutionVenue().build());
		placementAllocation.getOrderPlacement().setExecutingBrokerCompany(OrderCompanyBuilder.createExecutingBrokerCompany().build());
		Assertions.assertEquals("BUY 285 of BATS on 06/07/2021 [Test Broker] [Test FIX Session] for Test-123 (Test Custodian)", placementAllocation.getLabel());

		orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().sell().build();
		orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(orderAllocation).build()).build();
		placementAllocation.setOrderAllocation(orderAllocation);
		placementAllocation.setOrderPlacement(orderPlacement);
		placementAllocation.setQuantity(orderAllocation.getAccountingNotional());
		Assertions.assertEquals("SELL 10,000 of AUD on 06/07/2021 for Test-123 (Test Custodian)", placementAllocation.getLabel());
	}
}
