package com.clifton.order.allocation.upload;

import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.document.system.note.DocumentSystemNoteService;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.api.server.model.SecurityIdentifierTypesModel;
import com.clifton.order.api.server.model.SecurityRetrieverModel;
import com.clifton.order.group.OrderGroup;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataTestUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.NoSuchElementException;


/**
 * @author manderson
 */
@ContextConfiguration
public class OrderAllocationUploadServiceInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	private static final File testFile;


	static {
		try {
			testFile = new ClassPathResource("com/clifton/order/allocation/upload/OrderAllocationUploadTestFile.xls").getFile();
		}
		catch (IOException e) {
			throw new RuntimeException("Unable to instantiate test file ", e);
		}
	}


	@Resource
	private DocumentSystemNoteService documentSystemNoteService;

	@Resource
	private OrderAllocationService orderAllocationService;

	@Resource
	private OrderAllocationUploadServiceImpl orderAllocationUploadService;

	@Resource
	private OrderSetupService orderSetupService;

	@Resource
	private OrderSharedService orderSharedService;

	@Resource
	private OrderSharedMarketDataService orderSharedMarketDataService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderAllocationUpload_MissingFile() {
		testUploadWithExpectedError(populateCommand(OrderType.ORDER_TYPE_CURRENCY), null, ValidationException.class, "File selection is required to process an upload.");
	}


	@Test
	public void testOrderAllocationUpload_EmptyFile() {
		testUploadWithExpectedError(populateCommand(OrderType.ORDER_TYPE_CURRENCY), "EmptyTab", ValidationException.class, "No data available in the file to upload.  Please check your file.");
	}


	@Test
	public void testOrderAllocationUpload_MissingOrderType() {
		testUploadWithExpectedError(populateCommand(null), "EmptyTab", ValidationException.class, "Please specify the Order Type for the Order Allocations in the Simple Upload File.");
	}


	@Test
	public void testOrderAllocationUpload_WrongOrderType() {
		testUploadWithExpectedError(populateCommand(OrderType.ORDER_TYPE_CURRENCY), "Stocks", NoSuchElementException.class, "Cannot find Active Security on 08/17/2021 with Ticker [BATS] and Security Type [Currency].");
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Currency Order Import Tests                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderAllocationUpload_Currency_Successful() {
		DocumentSystemNoteService documentSystemNoteServiceSpy = Mockito.spy(this.documentSystemNoteService);
		this.orderAllocationUploadService.setDocumentSystemNoteService(documentSystemNoteServiceSpy);
		Mockito.doCallRealMethod().when(documentSystemNoteServiceSpy).uploadDocumentSystemNote(ArgumentMatchers.any(SystemNote.class), ArgumentMatchers.any(FileUploadWrapper.class));
		Mockito.doNothing().when(documentSystemNoteServiceSpy).saveDocumentSystemNoteAttachment(ArgumentMatchers.any(SystemNote.class), ArgumentMatchers.any(FileUploadWrapper.class));

		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.eq("CHF"), ArgumentMatchers.eq("USD"), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getCHFUSDExchangeRate());
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.eq("EUR"), ArgumentMatchers.eq("USD"), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getEURUSDExchangeRate());
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.eq("CAD"), ArgumentMatchers.eq("USD"), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getCADUSDExchangeRate());
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.eq("HKD"), ArgumentMatchers.eq("USD"), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getHKDUSDExchangeRate());
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.eq("JPY"), ArgumentMatchers.eq("USD"), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getJPYUSDExchangeRate());

		OrderAllocationUploadCommand uploadCommand = populateCommand(OrderType.ORDER_TYPE_CURRENCY);
		testUploadWithExpectedResultMessage(uploadCommand, "Currency", "[Order Allocation(s)]: 7 Records Inserted.There were no errors with the upload file.");

		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderGroupId(uploadCommand.getOrderGroup().getId());
		Assertions.assertEquals(7, CollectionUtils.getSize(this.orderAllocationService.getOrderAllocationList(searchForm)));

		//successful upload with order source
		OrderAllocationUploadCommand uploadCommand1 = populateCommand(OrderType.ORDER_TYPE_CURRENCY);
		testUploadWithExpectedResultMessage(uploadCommand1, "OrderSource", "[Order Allocation(s)]: 2 Records Inserted.There were no errors with the upload file.");
		OrderAllocationSearchForm searchForm1 = new OrderAllocationSearchForm();
		searchForm1.setOrderGroupId(uploadCommand1.getOrderGroup().getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(this.orderAllocationService.getOrderAllocationList(searchForm1)));
	}


	@Test
	public void testOrderAllocationUpload_Currency_NegativeAmount() {
		OrderAllocationUploadCommand uploadCommand = populateCommand(OrderType.ORDER_TYPE_CURRENCY);
		testUploadWithExpectedError(uploadCommand, "Currency Negative", ValidationException.class, "Accounting Notional must be greater than zero");

		uploadCommand.setPartialUploadAllowed(true);
		testUploadWithExpectedResultMessage(uploadCommand, "Currency Negative", "[Order Allocation(s)]: 0 Records Inserted.The following errors were encountered with the upload file: [Error_SELL -3,200 of EUR on 08/17/2021 for pimaig (State Street Bank)]: Accounting Notional must be greater than zero[Error_SELL -4,100 of CHF on 08/17/2021 for pimaig (State Street Bank)]: Accounting Notional must be greater than zero");
	}


	@Test
	public void testOrderAllocationUpload_OrderSource_Invalid() {
		OrderAllocationUploadCommand uploadCommand = populateCommand(OrderType.ORDER_TYPE_CURRENCY);
		testUploadWithExpectedError(uploadCommand, "OrderSource Invalid", ValidationException.class, "Cannot find Order Source with name TradeDeskInvalid");

		uploadCommand.setPartialUploadAllowed(false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Stock Order Import Tests                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderAllocationUpload_Stocks_Successful() {
		DocumentSystemNoteService documentSystemNoteServiceSpy = Mockito.spy(this.documentSystemNoteService);
		this.orderAllocationUploadService.setDocumentSystemNoteService(documentSystemNoteServiceSpy);
		Mockito.doCallRealMethod().when(documentSystemNoteServiceSpy).uploadDocumentSystemNote(ArgumentMatchers.any(SystemNote.class), ArgumentMatchers.any(FileUploadWrapper.class));
		Mockito.doNothing().when(documentSystemNoteServiceSpy).saveDocumentSystemNoteAttachment(ArgumentMatchers.any(SystemNote.class), ArgumentMatchers.any(FileUploadWrapper.class));
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataPriceFlexible(ArgumentMatchers.eq(getOrderSecurityIdForStockTicker("BATS")), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getPriceDataForPrice("2680", "26.8"));
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataPriceFlexible(ArgumentMatchers.eq(getOrderSecurityIdForStockTicker("STM")), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getPriceDataForPrice("36.535", "36.535"));
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataPriceFlexible(ArgumentMatchers.eq(getOrderSecurityIdForStockTicker("ULVR")), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getPriceDataForPrice("4130", "41.30"));

		OrderAllocationUploadCommand uploadCommand = populateCommand(OrderType.ORDER_TYPE_STOCKS);
		testUploadWithExpectedResultMessage(uploadCommand, "Stocks", "[Order Allocation(s)]: 8 Records Inserted.There were no errors with the upload file.");

		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderGroupId(uploadCommand.getOrderGroup().getId());
		Assertions.assertEquals(8, CollectionUtils.getSize(this.orderAllocationService.getOrderAllocationList(searchForm)));

		// Validate Note was created
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(OrderGroup.TABLE_NAME);
		noteSearchForm.setFkFieldId(uploadCommand.getOrderGroup().getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(this.systemNoteService.getSystemNoteList(noteSearchForm)));
	}


	@Test
	public void testOrderAllocationUpload_Stocks_NegativeAmount() {
		OrderAllocationUploadCommand uploadCommand = populateCommand(OrderType.ORDER_TYPE_STOCKS);
		testUploadWithExpectedError(uploadCommand, "Stocks Negative", ValidationException.class, "Quantity Intended must be greater than zero");

		uploadCommand.setPartialUploadAllowed(true);
		testUploadWithExpectedResultMessage(uploadCommand, "Stocks Negative", "[Order Allocation(s)]: 0 Records Inserted.The following errors were encountered with the upload file: [Error_SELL -282 of BATS on 08/17/2021 for pimfin (State Street Bank)]: Quantity Intended must be greater than zero[Error_SELL -1,689 of BATS on 08/17/2021 for clrintl (Northern Trust Securities, Inc.)]: Quantity Intended must be greater than zero[Error_SELL -1,420 of STM on 08/17/2021 for pimaig (State Street Bank)]: Quantity Intended must be greater than zero");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends Throwable> void testUploadWithExpectedError(OrderAllocationUploadCommand uploadCommand, String tabName, Class<T> expectedType, String expectedMessage) {
		populateFileOnCommand(uploadCommand, tabName);
		T e = Assertions.assertThrows(expectedType, () -> this.orderAllocationUploadService.uploadOrderAllocationUploadFile(uploadCommand));
		Assertions.assertEquals(expectedMessage, e.getMessage());
	}


	private void testUploadWithExpectedResultMessage(OrderAllocationUploadCommand uploadCommand, String tabName, String expectedResult) {
		populateFileOnCommand(uploadCommand, tabName);
		this.orderAllocationUploadService.uploadOrderAllocationUploadFile(uploadCommand);
		Assertions.assertEquals(expectedResult.trim(), StringUtils.removeAll(uploadCommand.getUploadResultsString().trim(), StringUtils.NEW_LINE));
	}


	private OrderAllocationUploadCommand populateCommand(String orderTypeName) {
		OrderAllocationUploadCommand uploadCommand = new OrderAllocationUploadCommand();
		uploadCommand.setSimple(true);
		uploadCommand.setSimpleOrderType(this.orderSetupService.getOrderTypeByName(orderTypeName));
		uploadCommand.setOrderGroup(new OrderGroup());
		uploadCommand.getOrderGroup().setTradeDate(DateUtils.toDate("08/17/2021"));
		uploadCommand.getOrderGroup().setOrderCreatorUser(this.securityUserService.getSecurityUserByName("imstestuser1"));
		return uploadCommand;
	}


	private void populateFileOnCommand(OrderAllocationUploadCommand uploadCommand, String tabName) {
		if (tabName != null) {
			Workbook workbook = ExcelFileUtils.createWorkbook(testFile);
			uploadCommand.setSheetIndex(workbook.getSheetIndex(tabName));
			uploadCommand.setFile(new MultipartFileImpl(testFile));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int getOrderSecurityIdForStockTicker(String ticker) {
		SecurityRetrieverModel retrieverModel = new SecurityRetrieverModel();
		retrieverModel.setIdentifierType(SecurityIdentifierTypesModel.TICKER);
		retrieverModel.setIdentifierValue(ticker);
		retrieverModel.securityType("Stocks");
		return this.orderSharedService.getOrderSecurityForRetriever(retrieverModel, false).getId();
	}
}
