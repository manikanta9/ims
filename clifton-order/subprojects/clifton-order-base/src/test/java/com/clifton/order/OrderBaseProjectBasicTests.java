package com.clifton.order;


import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderBaseProjectBasicTests extends OrderProjectBasicTests {


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("creation");
		approvedList.add("block");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.apache.poi.ss.usermodel.Workbook");
	}


	// Note: These are from shared - can we get around needing these?  Project Prefix is "order" for all clifton-order sub-projects
	@Override
	protected boolean isMethodSkipped(Method method) {
		return CollectionUtils.createHashSet(
				"saveOrderCompany",
				"deleteOrderCompany",
				"saveOrderSecurity",
				"deleteOrderSecurity",
				"saveOrderAccount",
				"deleteOrderAccount",
				"saveOrderSecurityExchange",
				"deleteOrderSecurityExchange",
				"deleteOrderDestinationConfiguration",
				"saveOrderGroup"
		).contains(method.getName());
	}
}


