package com.clifton.order.execution;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.exchangerate.api.client.model.ExchangeRateData;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.builder.OrderAllocationBuilder;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.block.builder.OrderBlockBuilder;
import com.clifton.order.execution.builder.OrderPlacementBuilder;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.builder.OrderCompanyBuilder;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataTestUtils;
import com.clifton.order.trade.OrderTrade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderExecutionHandlerTests {

	@Resource
	private OrderExecutionHandler orderExecutionHandler;

	@Resource
	private OrderSharedMarketDataService orderSharedMarketDataService;

	////////////////////////////////////////////////////////////////////////////
	////////           Test Validate Order Execution Price              ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidateOrderExecutionPrice() {
		ExchangeRateData exchangeRateData = OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate();

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		OrderBlock orderBlock = OrderBlockBuilder.createForOrderAllocation(orderAllocation).build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(orderBlock).build();

		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(exchangeRateData);
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		// Within 1 Percent
		BigDecimal onePercent_lookupRate = new BigDecimal("0.7820");
		// Within 5 Percent
		BigDecimal fivePercent_lookupRate = new BigDecimal("0.8121");
		// Bad Rate
		BigDecimal badRate = new BigDecimal("1.9971");
		this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, exchangeRateData.getExchangeRate(), BigDecimal.ONE);
		this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, onePercent_lookupRate, BigDecimal.ONE);

		UserIgnorableValidationException validationException = Assertions.assertThrows(UserIgnorableValidationException.class, () -> {
			this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, fivePercent_lookupRate, BigDecimal.ONE);
		});
		Assertions.assertEquals("Execution Price is 4.8547% different from latest available of 0.7745 which is over allowed threshold of +/- 1%.", validationException.getMessage());

		this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, fivePercent_lookupRate, new BigDecimal("5"));

		validationException = Assertions.assertThrows(UserIgnorableValidationException.class, () -> {
			this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, badRate, new BigDecimal("5"));
		});
		Assertions.assertEquals("Execution Price is 157.8567% different from latest available of 0.7745 which is over allowed threshold of +/- 5%.", validationException.getMessage());

		// No threshold
		this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, badRate, null);
	}


	@Test
	public void testValidateOrderExecutionPrice_NoRecentAvailable() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		OrderBlock orderBlock = OrderBlockBuilder.createForOrderAllocation(orderAllocation).build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(orderBlock).build();

		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(null);
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		BigDecimal executionPrice = new BigDecimal("0.7820");

		UserIgnorableValidationException validationException = Assertions.assertThrows(UserIgnorableValidationException.class, () -> {
			this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, executionPrice, BigDecimal.ONE);
		});
		Assertions.assertEquals("Placement [10: BUY 10,000 of AUD on 06/07/2021] Unable to validate execution price of 0.782.  Unable to locate a recent value to compare to.", validationException.getMessage());

		// No threshold - should still work without a value
		this.orderExecutionHandler.validateOrderExecutionPrice(orderPlacement, executionPrice, null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateOrderPlacementAllocationListForPlacement_Currency_OneAllocation_SinglePlacement_FullFilled() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		OrderBlock orderBlock = OrderBlockBuilder.createForOrderAllocation(orderAllocation).build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_Filled(orderBlock, OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate()).build();

		List<OrderPlacementAllocation> placementAllocationList = this.orderExecutionHandler.calculateOrderPlacementAllocationListForPlacement(orderPlacement, Collections.singletonList(orderAllocation), null);
		Assertions.assertEquals(1, placementAllocationList.size());

		OrderPlacementAllocation orderPlacementAllocation = placementAllocationList.get(0);
		validateOrderPlacementAllocation(orderPlacementAllocation, null, orderAllocation.getAccountingNotional(), orderPlacement.getAverageUnitPrice());

		// Set An ID on the Placement Allocation so we can confirm recalculate doesn't re-create
		orderPlacementAllocation.setId((long) 3);
		placementAllocationList = this.orderExecutionHandler.calculateOrderPlacementAllocationListForPlacement(orderPlacement, Collections.singletonList(orderAllocation), placementAllocationList);
		Assertions.assertEquals(1, placementAllocationList.size());

		orderPlacementAllocation = placementAllocationList.get(0);
		validateOrderPlacementAllocation(orderPlacementAllocation, (long) 3, orderAllocation.getAccountingNotional(), orderPlacement.getAverageUnitPrice());
	}


	@Test
	public void testCalculateOrderPlacementAllocationListForPlacement_Currency_TwoAllocations_SinglePlacement_FullFilled() {
		OrderAllocation orderAllocation1 = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		OrderAllocation orderAllocation2 = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD_Account2().build();
		List<OrderAllocation> orderAllocationList = CollectionUtils.createList(orderAllocation1, orderAllocation2);
		OrderBlock orderBlock = OrderBlockBuilder.createForOrderAllocationList(orderAllocationList).build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_Filled(orderBlock, OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate()).build();

		List<OrderPlacementAllocation> placementAllocationList = this.orderExecutionHandler.calculateOrderPlacementAllocationListForPlacement(orderPlacement, orderAllocationList, null);
		Assertions.assertEquals(2, placementAllocationList.size());

		for (OrderPlacementAllocation orderPlacementAllocation : placementAllocationList) {
			if (orderPlacementAllocation.getOrderAllocation().equals(orderAllocation1)) {
				validateOrderPlacementAllocation(orderPlacementAllocation, null, orderAllocation1.getAccountingNotional(), orderPlacement.getAverageUnitPrice());
				orderPlacementAllocation.setId((long) 100);
			}
			else {
				validateOrderPlacementAllocation(orderPlacementAllocation, null, orderAllocation2.getAccountingNotional(), orderPlacement.getAverageUnitPrice());
				orderPlacementAllocation.setId((long) 200);
			}
		}

		placementAllocationList = this.orderExecutionHandler.calculateOrderPlacementAllocationListForPlacement(orderPlacement, orderAllocationList, placementAllocationList);
		Assertions.assertEquals(2, placementAllocationList.size());

		for (OrderPlacementAllocation orderPlacementAllocation : placementAllocationList) {
			if (orderPlacementAllocation.getOrderAllocation().equals(orderAllocation1)) {
				validateOrderPlacementAllocation(orderPlacementAllocation, (long) 100, orderAllocation1.getAccountingNotional(), orderPlacement.getAverageUnitPrice());
			}
			else {
				validateOrderPlacementAllocation(orderPlacementAllocation, (long) 200, orderAllocation2.getAccountingNotional(), orderPlacement.getAverageUnitPrice());
			}
		}
	}


	@Test
	public void testCalculateOrderPlacementAllocationListForPlacement_Currency_OneAllocation_PartialFill() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		OrderBlock orderBlock = OrderBlockBuilder.createForOrderAllocation(orderAllocation).build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(orderBlock).quantityFilled(BigDecimal.valueOf(1000.0)).averagePrice(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate()).build();

		// Replace this test when we have partial fill support
		IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> this.orderExecutionHandler.calculateOrderPlacementAllocationListForPlacement(orderPlacement, Collections.singletonList(orderAllocation), null));
		Assertions.assertEquals("Not supported yet for partial fills or multiple placements", exception.getMessage());
	}


	@Test
	public void testCalculateOrderPlacementAllocationListForPlacement_Currency_OneAllocation_MultiplePlacements_FullyFilled() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		OrderBlock orderBlock = OrderBlockBuilder.createForOrderAllocation(orderAllocation).build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(orderBlock).quantityIntended(BigDecimal.valueOf(1000.0)).quantityFilled(BigDecimal.valueOf(1000.0)).averagePrice(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate()).build();

		// Replace this test when we have multiple placement support
		IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> this.orderExecutionHandler.calculateOrderPlacementAllocationListForPlacement(orderPlacement, Collections.singletonList(orderAllocation), null));
		Assertions.assertEquals("Not supported yet for partial fills or multiple placements", exception.getMessage());
	}


	@Test
	public void testCalculateOrderTradeListForPlacement_Currency_OneAllocation() {
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		OrderBlock orderBlock = OrderBlockBuilder.createForOrderAllocation(orderAllocation).build();
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_Filled(orderBlock, OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate()).executingBrokerCompany(OrderCompanyBuilder.createExecutingBrokerCompany().build()).build();

		List<OrderPlacementAllocation> placementAllocationList = this.orderExecutionHandler.calculateOrderPlacementAllocationListForPlacement(orderPlacement, Collections.singletonList(orderAllocation), null);

		List<OrderTrade> orderTradeList = this.orderExecutionHandler.calculateOrderTradeListForPlacement(orderPlacement, placementAllocationList, null);
		Assertions.assertEquals(1, orderTradeList.size());
		validateOrderTradeCurrency(orderTradeList.get(0), orderAllocation.getId(), null, orderPlacement.getExecutingBrokerCompany(), orderPlacement.getQuantityFilled(), orderPlacement.getAverageUnitPrice());
		orderTradeList.get(0).setId((long) 5);

		orderTradeList = this.orderExecutionHandler.calculateOrderTradeListForPlacement(orderPlacement, placementAllocationList, orderTradeList);
		Assertions.assertEquals(1, orderTradeList.size());
		validateOrderTradeCurrency(orderTradeList.get(0), orderAllocation.getId(), (long) 5, orderPlacement.getExecutingBrokerCompany(), orderPlacement.getQuantityFilled(), orderPlacement.getAverageUnitPrice());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateOrderPlacementAllocation(OrderPlacementAllocation placementAllocation, Long id, BigDecimal quantity, BigDecimal price) {
		Assertions.assertEquals(price, placementAllocation.getPrice());
		if (id != null) {
			Assertions.assertEquals(id, placementAllocation.getId());
		}
		Assertions.assertEquals(quantity, placementAllocation.getQuantity());
	}


	private void validateOrderTradeCurrency(OrderTrade orderTrade, Long orderAllocationId, Long id, OrderCompany executingBroker, BigDecimal notional, BigDecimal fxRate) {
		Assertions.assertEquals(orderAllocationId, orderTrade.getOrderAllocationIdentifier());
		if (id != null) {
			Assertions.assertEquals(id, orderTrade.getId());
		}
		Assertions.assertEquals(notional, orderTrade.getAccountingNotional());
		Assertions.assertNull(orderTrade.getQuantity());
		Assertions.assertEquals(CoreMathUtils.formatNumberDecimal(fxRate), CoreMathUtils.formatNumberDecimal(orderTrade.getExchangeRateToSettlementCurrency()));
		Assertions.assertNull(orderTrade.getPrice());
		Assertions.assertEquals(executingBroker, orderTrade.getExecutingBrokerCompany());
	}
}
