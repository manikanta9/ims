SELECT 'WorkflowTransition' AS EntityTableName, t.WorkflowTransitionID AS EntityID
FROM WorkflowTransition t
	 INNER JOIN WorkflowState ws ON t.EndWorkflowStateID = ws.WorkflowStateID
	 INNER JOIN Workflow w ON ws.WorkflowID = w.WorkflowID
WHERE w.WorkflowName = 'Order Allocation Workflow'
UNION
SELECT 'OrderAccount', OrderAccountID
FROM OrderAccount
WHERE AccountShortName IN ('DAVINTL', 'CALEGBL', 'PIMAIG', 'QANTGE', 'pimfin', 'clrintl', 'ucitdiv', 'crowintl', 'brandgl')
UNION
SELECT 'OrderType', OrderTypeID
FROM OrderType
UNION
SELECT 'OrderSecurity', OrderSecurityID
FROM OrderSecurity s
	 INNER JOIN SystemListItem li ON s.SecurityTypeID = li.SystemListItemID
WHERE SystemListItemText = 'Currency'
UNION
SELECT 'OrderSecurity', OrderSecurityID
FROM OrderSecurity s
	 INNER JOIN SystemListItem li ON s.SecurityTypeID = li.SystemListItemID
WHERE SystemListItemText = 'Stocks'
  AND Ticker IN ('BATS', 'ULVR', 'STM')
UNION
SELECT 'SystemTable', SystemTableID
FROM SystemTable
UNION
SELECT 'SecurityUser', SecurityUserID
FROM SecurityUser
WHERE SecurityUserName = 'imstestuser1'
UNION
SELECT 'OrderGroupType', OrderGroupTypeID
FROM OrderGroupType
UNION
SELECT 'SystemListItem', SystemListItemID
FROM SystemListItem sli
	 INNER JOIN SystemList l ON sli.SystemListID = l.SystemListID
WHERE l.SystemListName = 'Currency Priority (Currency Conventions)'
UNION
SELECT 'OrderOpenCloseType', OrderOpenCloseTypeID
FROM OrderOpenCloseType
UNION
SELECT 'SystemNoteType', SystemNoteTypeID
FROM SystemNoteType snt
	 INNER JOIN SystemTable tb ON snt.SystemTableID = tb.SystemTableID
WHERE tb.TableName = 'OrderGroup'
UNION
SELECT 'OrderSource', OrderSourceID
FROM OrderSource
WHERE OrderSourceName = 'Verona'
;

