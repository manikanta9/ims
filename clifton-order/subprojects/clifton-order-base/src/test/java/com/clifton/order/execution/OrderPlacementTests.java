package com.clifton.order.execution;

import com.clifton.order.allocation.builder.OrderAllocationBuilder;
import com.clifton.order.block.builder.OrderBlockBuilder;
import com.clifton.order.execution.builder.OrderPlacementBuilder;
import com.clifton.order.setup.builder.OrderDestinationBuilder;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.shared.builder.OrderCompanyBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author manderson
 */
public class OrderPlacementTests {


	@Test
	public void testGetLabelShort() {
		OrderPlacement placement = new OrderPlacement();
		Assertions.assertNull(placement.getLabelShort());

		placement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		Assertions.assertEquals("BUY 10,000 of AUD on 06/07/2021", placement.getLabelShort());

		placement.getOrderBlock().setBuy(false);
		Assertions.assertEquals("SELL 10,000 of AUD on 06/07/2021", placement.getLabelShort());

		placement.setExecutingBrokerCompany(OrderCompanyBuilder.createExecutingBrokerCompany().build());
		placement.setOrderDestination(OrderDestinationBuilder.createFileExecutionVenue().build());

		Assertions.assertEquals("SELL 10,000 of AUD on 06/07/2021 [Test Broker] [Test File Execution]", placement.getLabelShort());
	}


	@Test
	public void testIsOfDestinationCommunicationType() {
		OrderPlacement placement = new OrderPlacement();
		Assertions.assertFalse(placement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FIX));

		placement.setOrderDestination(OrderDestinationBuilder.createFileExecutionVenue().build());
		Assertions.assertFalse(placement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FIX));
		Assertions.assertTrue(placement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FILE));
	}
}
