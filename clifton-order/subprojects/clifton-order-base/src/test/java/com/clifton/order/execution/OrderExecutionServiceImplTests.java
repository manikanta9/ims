package com.clifton.order.execution;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.exchangerate.api.client.model.ExchangeRateData;
import com.clifton.order.allocation.builder.OrderAllocationBuilder;
import com.clifton.order.block.builder.OrderBlockBuilder;
import com.clifton.order.execution.builder.OrderPlacementBuilder;
import com.clifton.order.shared.builder.OrderCompanyBuilder;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataTestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "OrderExecutionHandlerTests-context.xml")
@ExtendWith(SpringExtension.class)
public class OrderExecutionServiceImplTests {

	@Resource
	private OrderExecutionService orderExecutionService;

	@Resource
	private OrderSharedMarketDataService orderSharedMarketDataService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderPlacement_MissingPrice() {
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		orderPlacement.setQuantityFilled(MathUtils.add(orderPlacement.getQuantityIntended(), BigDecimal.TEN));
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.orderExecutionService.saveOrderPlacementManualExecution(orderPlacement, false));
		Assertions.assertEquals("Exchange Rate is required.", exception.getMessage());
	}


	@Test
	public void testOrderPlacement_NegativePrice() {
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		orderPlacement.setAverageUnitPrice(MathUtils.negate(BigDecimal.ONE));
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.orderExecutionService.saveOrderPlacementManualExecution(orderPlacement, false));
		Assertions.assertEquals("Exchange Rate cannot be negative.", exception.getMessage());
	}


	@Test
	public void testOrderPlacement_Partial_Fill() {
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		orderPlacement.setAverageUnitPrice(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate());
		orderPlacement.setQuantityFilled(MathUtils.subtract(orderPlacement.getQuantityIntended(), BigDecimal.TEN));
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.orderExecutionService.saveOrderPlacementManualExecution(orderPlacement, false));
		Assertions.assertEquals("Partial fills are not currently supported for manual fills.  Please enter the fully filled amount of " + CoreMathUtils.formatNumberDecimal(orderPlacement.getQuantityIntended()), exception.getMessage());
	}


	@Test
	public void testOrderPlacement_Negative_Fill() {
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		orderPlacement.setAverageUnitPrice(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate());
		orderPlacement.setQuantityFilled(MathUtils.negate(BigDecimal.TEN));
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.orderExecutionService.saveOrderPlacementManualExecution(orderPlacement, false));
		Assertions.assertEquals("Filled Amount cannot be negative.", exception.getMessage());
	}


	@Test
	public void testOrderPlacement_Over_Fill() {
		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		orderPlacement.setAverageUnitPrice(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate());
		orderPlacement.setQuantityFilled(MathUtils.add(orderPlacement.getQuantityIntended(), BigDecimal.TEN));
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.orderExecutionService.saveOrderPlacementManualExecution(orderPlacement, false));
		Assertions.assertEquals("Selected filled amount is more than what was placed.  Please enter the fully filled amount of " + CoreMathUtils.formatNumberDecimal(orderPlacement.getQuantityIntended()), exception.getMessage());
	}


	@Test
	public void testOrderPlacement_Filled_MissingExecutingBroker() {
		ExchangeRateData exchangeRateData = OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate();
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(exchangeRateData);
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		orderPlacement.setAverageUnitPrice(exchangeRateData.getExchangeRate());
		orderPlacement.setQuantityFilled(orderPlacement.getQuantityIntended());

		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.orderExecutionService.saveOrderPlacementManualExecution(orderPlacement, false));
		Assertions.assertEquals("Executing Broker is required.", exception.getMessage());
	}


	@Test
	public void testOrderPlacement_Filled() {
		ExchangeRateData exchangeRateData = OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate();
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(exchangeRateData);
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		OrderPlacement orderPlacement = OrderPlacementBuilder.createForOrderBlock_New(OrderBlockBuilder.createForOrderAllocation(OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build()).build()).build();
		orderPlacement.setAverageUnitPrice(exchangeRateData.getExchangeRate());
		orderPlacement.setQuantityFilled(orderPlacement.getQuantityIntended());
		orderPlacement.setExecutingBrokerCompany(OrderCompanyBuilder.createExecutingBrokerCompany().build());
		this.orderExecutionService.saveOrderPlacementManualExecution(orderPlacement, false);
	}
}
