package com.clifton.order.note;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTableBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderNoteServiceImplTests {

	@Resource
	private OrderNoteService orderNoteService;

	@Resource
	private SystemNoteService systemNoteService;

	@Resource
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetOrderPlacementNoteLastForNoteType() {
		long placementId = 1;
		SystemNoteType testNoteType = getTestNoteType();

		this.orderNoteService.getOrderPlacementNoteLastForNoteType(placementId, testNoteType.getName());

		Mockito.verify(this.systemNoteService, Mockito.times(1)).getSystemNoteListForEntity(ArgumentMatchers.argThat(searchForm -> {
			if (OrderPlacement.TABLE_NAME.equals(searchForm.getEntityTableName())) {
				if (MathUtils.isEqual(placementId, searchForm.getFkFieldId())) {
					if (StringUtils.isEqual(testNoteType.getName(), searchForm.getNoteTypeName())) {
						if (StringUtils.isEqual("createDate:desc", searchForm.getOrderBy())) {
							if (1 == searchForm.getLimit()) {
								return true;
							}
						}
					}
				}
			}
			return false;
		}));
	}


	@Test
	public void testSaveOrderPlacementNote() {
		long placementId = 1;
		SystemNoteType testNoteType = getTestNoteType();
		String noteText = "This is a test.";

		Mockito.when(this.systemSchemaService.getSystemTableByName(ArgumentMatchers.eq(OrderPlacement.TABLE_NAME))).thenReturn(SystemTableBuilder.createTable((short) 5, OrderPlacement.TABLE_NAME).toSystemTable());
		Mockito.when(this.systemNoteService.getSystemNoteTypeByTableAndName(ArgumentMatchers.eq((short) 5), ArgumentMatchers.eq(getTestNoteType().getName()))).thenReturn(testNoteType);

		this.orderNoteService.saveOrderPlacementNote(placementId, getTestNoteType().getName(), noteText);

		Mockito.verify(this.systemNoteService, Mockito.times(1)).saveSystemNote(ArgumentMatchers.argThat(note -> {
			if (CompareUtils.isEqual(note.getNoteType(), testNoteType)) {
				if (StringUtils.isEqual(noteText, note.getText())) {
					if (MathUtils.isEqual(placementId, note.getFkFieldId())) {
						return true;
					}
				}
			}
			return false;
		}));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemNoteType getTestNoteType() {
		SystemNoteType testNoteType = new SystemNoteType();
		testNoteType.setId((short) 6);
		testNoteType.setName("Test Note Type");
		return testNoteType;
	}
}
