package com.clifton.order.allocation;

import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.allocation.builder.OrderAllocationBuilder;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.setup.builder.OrderTypeBuilder;
import com.clifton.order.shared.builder.OrderCompanyBuilder;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataTestUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.SystemTableBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.NoSuchElementException;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderAllocationServiceImplTests {

	@Resource
	private OrderAllocationService orderAllocationService;

	@Resource
	private OrderSharedMarketDataService orderSharedMarketDataService;


	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		Mockito.when(this.systemSchemaService.getSystemTableByName(ArgumentMatchers.anyString())).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			SystemTable table = new SystemTable();
			table.setId(MathUtils.SHORT_ONE);
			table.setName(OrderBlock.TABLE_NAME);
			return table;
		});
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testOrderAllocation_missingOrderType() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		orderAllocation.setOrderType(null);
		NoSuchElementException exception = Assertions.assertThrows(NoSuchElementException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Cannot find a valid order type for Security AUD", exception.getMessage());
	}


	@Test
	public void testOrderAllocation_invalidOrderType() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().build();
		orderAllocation.setOrderType(OrderTypeBuilder.createStocks().build());
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Order Type Stocks is not a valid selection for trading security AUD", exception.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Currency Order Allocations                      ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCurrencyOrderAllocation_withMissingExchangeRate() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).build();
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate());
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());
		orderAllocation = this.orderAllocationService.saveOrderAllocation(orderAllocation);
		Assertions.assertTrue(MathUtils.isEqual(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate(), orderAllocation.getExchangeRateToSettlementCurrency()));

		// Confirm Settlement Amount
		Assertions.assertEquals(MathUtils.round(MathUtils.multiply(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate().getExchangeRate(), orderAllocation.getAccountingNotional()), 2), orderAllocation.getSettlementAmount());
	}


	@Test
	public void testCurrencyOrderAllocation_withMissingExchangeRate_AndNoRecentRateAvailable() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).build();
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(false))).thenReturn(null);
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());
		orderAllocation = this.orderAllocationService.saveOrderAllocation(orderAllocation);
		// Defaults to 1 if nothing available or supplied
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ONE, orderAllocation.getExchangeRateToSettlementCurrency()));

		// Confirm Settlement Amount
		Assertions.assertEquals(orderAllocation.getAccountingNotional(), orderAllocation.getSettlementAmount());
	}


	@Test
	public void testCurrencyOrderAllocation_withExchangeRate() {
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).build();
		BigDecimal exchangeRate = new BigDecimal("0.77489547");
		orderAllocation.setExchangeRateToSettlementCurrency(exchangeRate);
		// Ensures we don't override the value if populated
		orderAllocation = this.orderAllocationService.saveOrderAllocation(orderAllocation);
		Assertions.assertTrue(MathUtils.isEqual(exchangeRate, orderAllocation.getExchangeRateToSettlementCurrency()));

		// Confirm Settlement Amount
		Assertions.assertEquals(MathUtils.round(MathUtils.multiply(exchangeRate, orderAllocation.getAccountingNotional()), 2), orderAllocation.getSettlementAmount());
	}


	@Test
	public void testCurrencyOrderAllocation_RequireHoldingAccountIssuer_Defaulted() {
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).build();
		orderAllocation.getOrderType().setExecutingBrokerHoldingAccountIssuer(true);
		BigDecimal exchangeRate = new BigDecimal("0.77489547");
		orderAllocation.setExchangeRateToSettlementCurrency(exchangeRate);
		orderAllocation = this.orderAllocationService.saveOrderAllocation(orderAllocation);
		Assertions.assertEquals(orderAllocation.getHoldingAccount().getIssuingCompany(), orderAllocation.getExecutingBrokerCompany());
	}


	@Test
	public void testCurrencyOrderAllocation_RequireHoldingAccountIssuer_Error() {
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).build();
		orderAllocation.getOrderType().setExecutingBrokerHoldingAccountIssuer(true);
		orderAllocation.setExecutingBrokerCompany(OrderCompanyBuilder.createExecutingBrokerCompany().build());
		BigDecimal exchangeRate = new BigDecimal("0.77489547");
		orderAllocation.setExchangeRateToSettlementCurrency(exchangeRate);
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Invalid Executing Broker selected.  Order Type Currency requires executing broker to be the holding account issuer Test Custodian", exception.getMessage());
	}


	@Test
	public void testCurrencyOrderAllocation_withMissingAmount() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).build();
		orderAllocation.setAccountingNotional(null);
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Given Amount is required", exception.getMessage());
	}


	@Test
	public void testCurrencyOrderAllocation_negativeAmount() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).build();
		orderAllocation.setAccountingNotional(MathUtils.negate(orderAllocation.getAccountingNotional()));
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataExchangeRateFlexible(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDExchangeRate());
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(OrderSharedMarketDataTestUtils.getAUDUSDCurrencyConvention());
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Given Amount must be greater than zero", exception.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Stock Order Allocations                         ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testStockOrderAllocation_withMissingQuantity() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewStockOrderAllocationGBP("BATS").id(null).build();
		orderAllocation.setQuantityIntended(null);
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Quantity Intended is required", exception.getMessage());
	}


	@Test
	public void testStockOrderAllocation_withMissingPrice_LookupFromMaster() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewStockOrderAllocationGBP("BATS").id(null).build();
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataPriceFlexible(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getStockPriceForGBPSecurity(true));
		orderAllocation = this.orderAllocationService.saveOrderAllocation(orderAllocation);
		Assertions.assertTrue(MathUtils.isEqual(OrderSharedMarketDataTestUtils.getStockPriceForGBPSecurity(true).getBasePrice(), orderAllocation.getExpectedUnitPrice()));

		// Confirm Accounting Notional
		Assertions.assertEquals(MathUtils.round(MathUtils.multiply(OrderSharedMarketDataTestUtils.getStockPriceForGBPSecurity(true).getBasePrice(), orderAllocation.getQuantityIntended()), 2), orderAllocation.getAccountingNotional());
		// No FX Rate needed (Settles in Trading CCY)
		Assertions.assertEquals(BigDecimal.ONE, orderAllocation.getExchangeRateToSettlementCurrency());
		Assertions.assertEquals(orderAllocation.getSettlementAmount(), orderAllocation.getAccountingNotional());
	}


	@Test
	public void testStockOrderAllocation_withMissingPrice_AndNoRecentAvailable() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewStockOrderAllocationGBP("BATS").id(null).build();
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataPriceFlexible(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenThrow(new ValidationException("No price found for 'BATS' on or before '06/07/2021'."));

		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("No price found for 'BATS' on or before '06/07/2021'.", exception.getMessage());
	}


	@Test
	public void testCurrencyOrderAllocation_negativeQuantity() {
		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewStockOrderAllocationGBP("BATS").id(null).build();
		orderAllocation.setQuantityIntended(MathUtils.negate(orderAllocation.getQuantityIntended()));
		Mockito.when(this.orderSharedMarketDataService.getOrderMarketDataPriceFlexible(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class), ArgumentMatchers.anyBoolean())).thenReturn(OrderSharedMarketDataTestUtils.getStockPriceForGBPSecurity(true));
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Quantity Intended must be greater than zero", exception.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTraderSelectedIsValid() {
		SystemTable orderBlockTable = SystemTableBuilder.createOrderBlock().toSystemTable();
		Mockito.doReturn(orderBlockTable).when(this.systemSchemaService).getSystemTableByName(OrderBlock.TABLE_NAME);
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin("validTrader")).thenReturn(false);
		Mockito.when(this.securityAuthorizationService.isSecurityAccessAllowed("validTrader", orderBlockTable.getSecurityResource().getName(), SecurityPermission.PERMISSION_WRITE)).thenReturn(true);

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).forTrader("validTrader").build();
		this.orderAllocationService.saveOrderAllocation(orderAllocation);
	}


	@Test
	public void testTraderSelectedIsNotValid() {
		SystemTable orderBlockTable = SystemTableBuilder.createOrderBlock().toSystemTable();
		Mockito.doReturn(orderBlockTable).when(this.systemSchemaService).getSystemTableByName(OrderBlock.TABLE_NAME);
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin("notValidTrader")).thenReturn(false);
		Mockito.when(this.securityAuthorizationService.isSecurityAccessAllowed("notValidTrader", orderBlockTable.getSecurityResource().getName(), SecurityPermission.PERMISSION_WRITE)).thenReturn(false);

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().forTrader("notValidTrader").build();

		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderAllocationService.saveOrderAllocation(orderAllocation);
		});
		Assertions.assertEquals("Selected trader [" + orderAllocation.getTraderUser().getLabel() + "] is not a valid selection. Only users with ability to execute orders can be selected as traders", exception.getMessage());
	}


	@Test
	public void testTraderSelectedIsAdmin() {
		Mockito.doReturn(SystemTableBuilder.createOrderBlock().toSystemTable()).when(this.systemSchemaService).getSystemTableByName(OrderBlock.TABLE_NAME);
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin("adminUser")).thenReturn(true);

		OrderAllocation orderAllocation = OrderAllocationBuilder.createNewCurrencyOrderAllocation_AUD_USD().id(null).forTrader("adminUser").build();
		this.orderAllocationService.saveOrderAllocation(orderAllocation);
	}
}
