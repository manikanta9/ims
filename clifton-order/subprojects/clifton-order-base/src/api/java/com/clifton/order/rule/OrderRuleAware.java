package com.clifton.order.rule;

import com.clifton.core.beans.IdentityObject;
import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;

import java.util.Date;


/**
 * @author AbhinayaM
 * <p>
 * The OrderRuleAware interface defines common methods for order entities that we run rules against.
 * This allows use to re-use rules for both pre-execution (OrderAllocations) and post-execution (OrderTrades).
 */
public interface OrderRuleAware extends IdentityObject {

	public OrderAccount getClientAccount();


	public OrderAccount getHoldingAccount();


	public OrderCompany getExecutingBrokerCompany();


	public OrderSecurity getSecurity();


	public OrderSecurity getSettlementCurrency();


	public OrderType getOrderType();


	public Date getTradeDate();


	public Date getSettlementDate();
}
