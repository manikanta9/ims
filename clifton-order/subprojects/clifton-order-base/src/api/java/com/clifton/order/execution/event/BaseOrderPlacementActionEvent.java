package com.clifton.order.execution.event;

import com.clifton.order.setup.OrderExecutionActions;
import lombok.Getter;


/**
 * The <code>BaseOrderPlacementActionEvent</code> is a base class that is extended by events that need to just trigger an action on the placement.
 *
 * @author manderson
 */
@Getter
public abstract class BaseOrderPlacementActionEvent extends BaseOrderPlacementEvent {


	private final OrderExecutionActions orderExecutionAction;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected BaseOrderPlacementActionEvent(String eventName, long orderPlacementId, OrderExecutionActions orderExecutionAction) {
		super(eventName, orderPlacementId);
		this.orderExecutionAction = orderExecutionAction;
	}
}
