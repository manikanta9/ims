package com.clifton.order.execution.event;

import com.clifton.core.util.event.EventObject;
import com.clifton.order.execution.OrderPlacement;
import lombok.Getter;


/**
 * @author manderson
 */
@Getter
public abstract class BaseOrderPlacementEvent extends EventObject<OrderPlacement, OrderPlacement> {

	private final long orderPlacementId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected BaseOrderPlacementEvent(String eventName, long orderPlacementId) {
		super(eventName);
		this.orderPlacementId = orderPlacementId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public long getOrderPlacementId() {
		return this.orderPlacementId;
	}
}
