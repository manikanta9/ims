package com.clifton.order.group.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderGroupSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "groupType.id", sortField = "groupType.name")
	private Short groupTypeId;

	@SearchField
	private Long groupFkFieldId;

	@SearchField(searchField = "groupType.roll")
	private Boolean roll;

	@SearchField(searchField = "groupType.orderImport")
	private Boolean orderImport;

	@SearchField
	private Date tradeDate;

	@SearchField(searchField = "orderCreatorUser.id", sortField = "orderCreatorUser.userName")
	private Short orderCreatorUserId;

	@SearchField(searchField = "security.id", sortField = "security.ticker")
	private Integer securityId;

	@SearchField(searchField = "secondarySecurity.id", sortField = "secondarySecurity.ticker")
	private Integer secondarySecurityId;

	@SearchField
	private String note;
}
