package com.clifton.order.group;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.group.search.OrderGroupSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderGroupService {


	////////////////////////////////////////////////////////////////////////////
	////////                Order Group Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderGroup getOrderGroup(long id, boolean populateAllocations);


	public List<OrderGroup> getOrderGroupList(OrderGroupSearchForm searchForm);


	public OrderGroup saveOrderGroup(OrderGroup orderGroup);


	////////////////////////////////////////////////////////////////////////////
	////////            Order Group Action Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes Validate transition on either a Order Group (all order allocations in the group) or an individual order allocation
	 * Called after ignoring warnings on order allocations to also immediately validate the trade(s).
	 */
	@SecureMethod(dtoClass = OrderAllocation.class)
	@RequestMapping("orderAllocationValidateActionExecute")
	public Status executeOrderAllocationValidateAction(Integer orderGroupId, Integer orderAllocationId);
}
