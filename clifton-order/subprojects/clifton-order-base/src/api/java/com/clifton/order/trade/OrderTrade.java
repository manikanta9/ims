package com.clifton.order.trade;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.setup.OrderOpenCloseType;
import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>OrderTrade</code> class represents a single trade for an order allocation.  Most order allocations are filled with a single Trade.
 * If same executing broker and average pricing is used, then there'll be only one Trade for the order allocation too.
 * <p/>
 * However, an order may have more than one trade if it gets filled by different lots at different prices with different executing brokers
 * NOTE: Trades (not order allocations) get booked to the General Ledger. It maybe necessary to book multiple trades from multiple order allocations
 * for the same account and security on the same day in a custom order in order to match the broker( lowest price first, etc.).
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderTrade extends BaseEntity<Long> implements OrderRuleAware {

	public static final String TABLE_NAME = "OrderTrade";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Long orderAllocationIdentifier;

	private OrderAccount clientAccount;

	private OrderAccount holdingAccount;

	private OrderType orderType;

	private OrderSecurity security;

	private OrderOpenCloseType openCloseType;

	private OrderCompany executingBrokerCompany;

	private Date tradeDate;

	private Date settlementDate;

	private OrderSecurity settlementCurrency;

	private BigDecimal quantity;

	private BigDecimal price;

	/**
	 * Exchange Rate in Standard CCY conventions to settlement ccy.
	 * i.e. JPY -> USD and USD -> JPY rates display the same way and we either multiply or divide to calculate the settlement amount
	 */
	private BigDecimal exchangeRateToSettlementCurrency;

	private BigDecimal accountingNotional;

	/**
	 * Accounting Notional in Settlement CCY
	 * this is either Accounting Notional * Exchange Rate to Settlement  or Accounting Notional / Exchange Rate to Settlement depending on the direction and the currency convention
	 * Auto calculated By the System during saves
	 */
	private BigDecimal settlementAmount;

	private BigDecimal accrualAmount1;

	private BigDecimal accrualAmount2;

	private BigDecimal commissionPerUnit;

	private BigDecimal commissionAmount;

	private BigDecimal feeAmount;

	/**
	 * Used to close a specific lot opened on a this date.
	 */
	private Date openingDate;

	/**
	 * Used to find the lots that were opened at this price on the opening date.
	 */
	private BigDecimal openingPrice;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabelShort() {
		StringBuilder sb = new StringBuilder(64);
		sb.append(isBuy() ? "BUY " : "SELL ");
		if (getSecurity() != null && getSecurity().isCurrency()) {
			sb.append(CoreMathUtils.formatNumberDecimal(getAccountingNotional()));
		}
		else {
			sb.append(CoreMathUtils.formatNumberDecimal(getQuantity()));
		}
		if (getSecurity() != null) {
			sb.append(" of ");
			sb.append(getSecurity().getTicker());
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}


	public String getLabel() {
		if (getClientAccount() == null) {
			return getLabelShort();
		}
		return getLabelShort() + " for " + getClientAccount().getLabel();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBuy() {
		return getOpenCloseType() != null && getOpenCloseType().isBuy();
	}


	public BigDecimal getAccrualAmount() {
		return MathUtils.add(getAccrualAmount1(), getAccrualAmount2());
	}
}
