package com.clifton.order.trade.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.order.batch.search.OrderBatchItemAwareSearchForm;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


/**
 *
 */
@Getter
@Setter
public class OrderTradeSearchForm extends BaseAuditableEntitySearchForm implements OrderBatchItemAwareSearchForm {

	@SearchField
	private Long id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Long[] idList;

	@SearchField(searchField = "clientCompany.id", searchFieldPath = "clientAccount", sortField = "clientCompany.name")
	private Integer clientCompanyId;

	@SearchField(searchField = "clientAccount.id", sortField = "clientAccount.accountNumber")
	private Integer clientAccountId;

	@SearchField(searchField = "holdingAccount.id", sortField = "holdingAccount.accountNumber")
	private Integer holdingAccountId;

	@SearchField(searchField = "clientAccount.id,holdingAccount.id")
	private Integer clientOrHoldingAccountId;


	@SearchField(searchFieldPath = "holdingAccount", searchField = "issuingCompany.id", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchFieldPath = "holdingAccount", searchField = "issuingCompany.id", sortField = "issuingCompany.name")
	private Integer[] holdingAccountIssuingCompanyIds;

	@SearchField(searchFieldPath = "clientAccount", searchField = "service.id", sortField = "service.name")
	private Short serviceId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "clientAccount.service.id,clientAccount.service.parent.id,clientAccount.service.parent.parent.id,clientAccount.service.parent.parent.parent.id", leftJoin = true, sortField = "clientAccount.service.name")
	private Short serviceOrParentId;

	@SearchField(searchFieldPath = "clientAccount", searchField = "teamSecurityGroup.id", sortField = "teamSecurityGroup.name")
	private Short teamSecurityGroupId;

	@SearchField(searchFieldPath = "clientAccount", searchField = "portfolioManagerUser.id", sortField = "portfolioManagerUser.displayName")
	private Short portfolioManagerUserId;


	@SearchField(searchField = "executingBrokerCompany.id", sortField = "executingBrokerCompany.name")
	private Integer executingBrokerCompanyId;

	@SearchField(searchField = "executingBrokerCompany.id", sortField = "executingBrokerCompany.name")
	private Integer[] executingBrokerCompanyIds;

	@SearchField(searchField = "security.id", sortField = "security.ticker")
	private Integer securityId;

	@SearchField(searchFieldPath = "security", searchField = "securityHierarchy.id", sortField = "securityHierarchy.name")
	private Short securityHierarchyId;

	// Using 6 levels deep for now....
	@SearchField(searchField = "security.securityHierarchy.id,security.securityHierarchy.parent.id,security.securityHierarchy.parent.parent.id,security.securityHierarchy.parent.parent.parent.id,security.securityHierarchy.parent.parent.parent.id,security.securityHierarchy.parent.parent.parent.id", leftJoin = true, sortField = "security.securityHierarchy.name")
	private Short securityHierarchyOrParentId;

	@SearchField(searchFieldPath = "security", searchField = "securityType.id", sortField = "securityType.text")
	private Integer securityTypeId;

	@SearchField(searchFieldPath = "security", searchField = "name")
	private String securityName;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "security", sortField = "underlyingSecurity.ticker")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "security.currencyCode", comparisonConditions = ComparisonConditions.EQUALS)
	private String securityCurrencyCode;

	@SearchField(searchField = "settlementCurrency.id", sortField = "settlementCurrency.ticker")
	private Integer settlementCurrencyId;

	@SearchField(searchField = "openCloseType.buy")
	private Boolean buy;

	@SearchField(searchField = "openCloseType.id", sortField = "openCloseType.name")
	private Short openCloseTypeId;

	@SearchField
	private Long orderAllocationIdentifier;

	@SearchField(searchField = "orderAllocationIdentifier")
	private Long[] orderAllocationIdentifiers;

	@SearchField(searchField = "orderType.id", sortField = "orderType.name")
	private Short orderTypeId;

	@SearchField(searchField = "orderType.id", sortField = "orderType.name")
	private Short[] orderTypeIds;

	@SearchField
	private Date tradeDate;

	@SearchField
	private Date settlementDate;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal price;

	@SearchField
	private BigDecimal exchangeRateToSettlementCurrency;

	@SearchField
	private BigDecimal accountingNotional;

	@SearchField
	private BigDecimal settlementAmount;

	@SearchField
	private BigDecimal accrualAmount1;

	@SearchField
	private BigDecimal accrualAmount2;

	@SearchField(searchField = "accrualAmount1,accrualAmount2", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal accrualAmount;

	@SearchField
	private BigDecimal commissionPerUnit;

	@SearchField
	private BigDecimal commissionAmount;

	@SearchField
	private BigDecimal feeAmount;

	@SearchField
	private Date openingDate;

	@SearchField
	private BigDecimal openingPrice;

	// Custom Search field applied in Order Batch based on interface OrderBatchItemAwareSearchForm
	// To find trades included in a specific batch
	private Long orderBatchId;

	// Custom Search field applied in Order Batch based on interface OrderBatchItemAwareSearchForm
	// To find trades NOT included in any batch for a specific destination
	private Short orderBatchForDestinationIdMissing;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}
}
