package com.clifton.order.block;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderBlockService {

	////////////////////////////////////////////////////////////////////////////
	////////                Order Block Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderBlock getOrderBlock(long id, boolean populateAllocations);


	public List<OrderBlock> getOrderBlockList(OrderBlockSearchForm searchForm);


	public OrderBlock saveOrderBlock(OrderBlock bean);
}
