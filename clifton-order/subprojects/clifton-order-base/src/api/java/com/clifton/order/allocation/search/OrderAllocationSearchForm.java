package com.clifton.order.allocation.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.grouping.GroupingAggregateProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingSearchForm;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderAllocationSearchForm extends BaseWorkflowAwareSearchForm implements GroupingSearchForm {

	@SearchField
	private Long id;

	@SearchField(searchField = "id")
	private Long[] ids;

	@SearchField(searchFieldPath = "clientAccount", searchField = "clientCompany.id", sortField = "clientCompany.name")
	private Integer clientCompanyId;

	@SearchField(searchField = "clientAccount.id", sortField = "clientAccount.accountNumber")
	private Integer clientAccountId;

	@SearchField(searchField = "clientAccount.id,holdingAccount.id")
	private Integer clientOrHoldingAccountId;

	@SearchField(searchFieldPath = "clientAccount", searchField = "service.id", sortField = "service.name")
	private Short serviceId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "clientAccount.service.id,clientAccount.service.parent.id,clientAccount.service.parent.parent.id,clientAccount.service.parent.parent.parent.id", leftJoin = true, sortField = "clientAccount.service.name")
	private Short serviceOrParentId;

	@SearchField(searchFieldPath = "clientAccount", searchField = "teamSecurityGroup.id", sortField = "teamSecurityGroup.name")
	private Short teamSecurityGroupId;

	@SearchField(searchFieldPath = "clientAccount", searchField = "portfolioManagerUser.id", sortField = "portfolioManagerUser.displayName")
	private Short portfolioManagerUserId;

	@SearchField(searchField = "holdingAccount.id", sortField = "holdingAccount.accountNumber")
	private Integer holdingAccountId;

	@SearchField(searchFieldPath = "holdingAccount", searchField = "issuingCompany.id", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchField = "orderType.id", sortField = "orderType.name")
	private Short orderTypeId;

	@SearchField(searchField = "orderGroup.id")
	private Long orderGroupId;

	@SearchField(searchFieldPath = "orderGroup", searchField = "groupType.id")
	private Short orderGroupTypeId;

	@SearchField(searchField = "security.id", sortField = "security.ticker")
	private Integer securityId;

	@SearchField(searchFieldPath = "security", searchField = "securityHierarchy.id", sortField = "securityHierarchy.name")
	private Short securityHierarchyId;

	// Using 6 levels deep for now....
	@SearchField(searchField = "security.securityHierarchy.id,security.securityHierarchy.parent.id,security.securityHierarchy.parent.parent.id,security.securityHierarchy.parent.parent.parent.id,security.securityHierarchy.parent.parent.parent.id,security.securityHierarchy.parent.parent.parent.id", leftJoin = true, sortField = "security.securityHierarchy.name")
	private Short securityHierarchyOrParentId;

	@SearchField(searchFieldPath = "security", searchField = "securityType.id", sortField = "securityType.text")
	private Integer securityTypeId;

	@SearchField(searchFieldPath = "security", searchField = "name")
	private String securityName;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "security", sortField = "underlyingSecurity.ticker")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "security.currencyCode", comparisonConditions = ComparisonConditions.EQUALS)
	private String securityCurrencyCode;

	@SearchField(searchField = "openCloseType.buy")
	private Boolean buy;

	@SearchField(searchField = "openCloseType.id", sortField = "openCloseType.name")
	private Short openCloseTypeId;

	@SearchField
	private BigDecimal quantityIntended;

	@SearchField
	private BigDecimal originalFace;

	@SearchField
	private BigDecimal accountingNotional;

	@SearchField(searchField = "openCloseType.buy,accountingNotional", formula = "CASE WHEN $1 = 1 THEN $2 ELSE -1 * $2 END")
	private BigDecimal accountingNotionalSigned;

	@SearchField
	private BigDecimal notionalMultiplier;

	@SearchField
	private BigDecimal currentFactor;

	@SearchField
	private Date tradeDate;

	@SearchField
	private Date settlementDate;

	@SearchField(searchField = "settlementCurrency.id", sortField = "settlementCurrency.ticker")
	private Integer settlementCurrencyId;

	@SearchField
	private BigDecimal settlementAmount;

	@SearchField
	private BigDecimal exchangeRateToSettlementCurrency;

	@SearchField
	private BigDecimal expectedUnitPrice;

	@SearchField
	private BigDecimal averageUnitPrice;

	@SearchField
	private BigDecimal limitPrice;

	@SearchField
	private BigDecimal accrualAmount1;

	@SearchField
	private BigDecimal accrualAmount2;

	@SearchField(searchField = "accrualAmount1,accrualAmount2", searchFieldCustomType = SearchFieldCustomTypes.MATH_ADD_NULLABLE)
	private BigDecimal accrualAmount;

	@SearchField
	private BigDecimal commissionPerUnit;

	@SearchField
	private BigDecimal commissionAmount;

	@SearchField
	private BigDecimal feeAmount;

	@SearchField(searchField = "orderCreatorUser.id", sortField = "orderCreatorUser.userName")
	private Short orderCreatorUserId;

	@SearchField(searchField = "groupList.id", searchFieldPath = "orderCreatorUser", comparisonConditions = {ComparisonConditions.EXISTS})
	private Short orderCreatorUserGroupId;

	@SearchField(searchField = "traderUser.id", sortField = "traderUser.userName")
	private Short traderUserId;

	@SearchField(searchField = "traderUser.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean traderUserIdMissing;

	@SearchField
	private String description;

	@SearchField(searchField = "executingBrokerCompany.id", sortField = "executingBrokerCompany.name")
	private Integer executingBrokerCompanyId;

	@SearchField(searchField = "settlementCompany.id", sortField = "settlementCompany.name")
	private Integer settlementCompanyId;

	@SearchField(searchField = "orderDestination.id", sortField = "orderDestination.name")
	private Short orderDestinationId;

	@SearchField(searchField = "orderDestination.id", sortField = "orderDestination.name", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short orderDestinationIdOrNull;

	@SearchField
	private Long orderBlockIdentifier;

	@SearchField(searchField = "orderBlockIdentifier")
	private Long[] orderBlockIdentifiers;

	@SearchField(searchField = "orderBlockIdentifier", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean orderBlockIdentifierIsNull;

	@SearchField(searchField = "orderSource.id")
	private Short orderSourceId;

	@SearchField
	private Long externalOrderIdentifier;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Long idNotEquals;

	@SearchField
	private String ruleViolationCodes;

	// Grouping Search Form Property
	private List<GroupingProperty> groupingPropertyList;

	// Grouping Aggregate Properties
	private List<GroupingAggregateProperty> groupingAggregatePropertyList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}
}
