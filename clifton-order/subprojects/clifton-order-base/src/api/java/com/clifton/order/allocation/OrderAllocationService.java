package com.clifton.order.allocation;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.grouping.GroupedEntityResult;
import com.clifton.core.dataaccess.search.grouping.GroupedHierarchicalResult;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public interface OrderAllocationService {

	////////////////////////////////////////////////////////////////////////////
	////////            Order Allocation Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAllocation getOrderAllocation(long id);


	public List<OrderAllocation> getOrderAllocationList(OrderAllocationSearchForm searchForm);


	@SecureMethod(dtoClass = OrderAllocation.class, namingConventionViolation = true)
	public List<GroupedHierarchicalResult> getOrderAllocationGroupedHierarchicalResultList(OrderAllocationSearchForm searchForm);


	@SecureMethod(dtoClass = OrderAllocation.class, namingConventionViolation = true)
	public List<GroupedEntityResult<OrderAllocation>> getOrderAllocationGroupedEntityResultList(OrderAllocationSearchForm searchForm);


	public OrderAllocation saveOrderAllocation(OrderAllocation orderAllocation);


	/**
	 * Gets the orderAllocation, updates the trade and settlement date and then calls the workflow transition service to execute the transition
	 * Needs to be done in a custom call as the transition may involve transitioning to the same state
	 */
	@UserIgnorableValidation
	public OrderAllocation saveOrderAllocationWithRevisedDates(long id, Date tradeDate, Date settlementDate, int workflowTransitionId, boolean ignoreValidation);


	/**
	 * Called internally when updating placement information post execution to save the allocations with the Amend Execution self referencing workflow transition
	 * Amended values are populated separately and then copied to the original order allocation
	 */
	@DoNotAddRequestMapping
	public OrderAllocation saveOrderAllocationWithAmendments(OrderAllocationAmendmentCommand orderAllocationAmendmentCommand, int workflowTransitionId);


	/**
	 * Bypass validation can be used for simple updates where we don't need to re-validate the entire order allocation
	 * i.e. changing the Trader User
	 */
	@DoNotAddRequestMapping
	public void saveOrderAllocationList(List<OrderAllocation> orderAllocationList, boolean bypassValidation);


	/**
	 * Uses a runner and 3 second delay system defined transition to move the Order Allocations to the selected end state name
	 */
	@DoNotAddRequestMapping
	public void scheduleOrderAllocationWorkflowTransitionSystemDefined(String runId, Long[] orderAllocationIds, String nextStateName, int secondsDelay);
}
