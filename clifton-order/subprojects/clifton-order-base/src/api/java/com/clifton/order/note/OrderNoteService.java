package com.clifton.order.note;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.system.note.SystemNote;


/**
 * @author manderson
 */
public interface OrderNoteService {

	////////////////////////////////////////////////////////////////////////////
	////////            Order Placement Notes                           ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderPlacement.class)
	public SystemNote getOrderPlacementNoteLastForNoteType(long orderPlacementId, String noteTypeName);


	@DoNotAddRequestMapping
	public SystemNote saveOrderPlacementNote(long orderPlacementId, String noteTypeName, String noteText);
}
