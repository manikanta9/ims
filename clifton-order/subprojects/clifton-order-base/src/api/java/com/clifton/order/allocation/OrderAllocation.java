package com.clifton.order.allocation;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.order.group.OrderGroup;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.setup.OrderExecutionType;
import com.clifton.order.setup.OrderOpenCloseType;
import com.clifton.order.setup.OrderSource;
import com.clifton.order.setup.OrderType;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.rule.violation.RuleViolationCodeAware;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */

@Getter
@Setter
public class OrderAllocation extends BaseEntity<Long> implements WorkflowAware, OrderRuleAware, RuleViolationCodeAware {

	public static final String TABLE_NAME = "OrderAllocation";
	public static final String WORKFLOW_NAME = "Order Allocation Workflow";

	public static final String WORKFLOW_STATE_FILLED = "Filled";
	public static final String WORKFLOW_STATE_ALLOCATED = "Allocated";
	public static final String WORKFLOW_STATE_CANCELED = "Canceled";
	public static final String WORKFLOW_STATE_REJECTED = "Rejected";
	public static final String WORKFLOW_STATE_PENDING = "Pending"; // Sent back to PM

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderAccount clientAccount;

	private OrderAccount holdingAccount;

	private OrderType orderType;

	private OrderGroup orderGroup;

	private OrderSecurity security;

	private OrderOpenCloseType openCloseType;

	private OrderExecutionType executionType;

	private WorkflowState workflowState;

	private WorkflowStatus workflowStatus;

	/**
	 * The business company used to determine the place of settlement business identifier code (BIC) when generating settlement instructions.
	 * This is populated on newly created trades by an observer and is based on investment specificity service calls.  This field will show
	 * as read-only on the trade window and is editable on the settlement tab.  For USD and most security instruction types, this is either
	 * 'Deposit Trust & Clearing Corporation' or 'Federal Reserve Bank of New York'.
	 */
	private OrderCompany settlementCompany;

	private BigDecimal quantityIntended;

	/**
	 * Usually the same as quantityIntended but for bonds that had factor changes prior to this trade (current factor <> 1),
	 * stores Original Face value while quantityIntended will store Purchase Face.
	 */
	private BigDecimal originalFace;

	private BigDecimal accountingNotional;

	/**
	 * Accounting Notional in Settlement CCY
	 * this is either Accounting Notional * Exchange Rate to Settlement  or Accounting Notional / Exchange Rate to Settlement depending on the direction and the currency convention
	 * Auto calculated By the System during saves
	 */
	private BigDecimal settlementAmount;

	/**
	 * Some securities may adjust accounting notional by this multiplier that is tied to something.
	 * TIPS use it to adjust for changes in inflation: Index Ratio.
	 */
	private BigDecimal notionalMultiplier;
	/**
	 * Purchase Factor that applies to bonds. Must be set to 1 for all other securities meaning no factor change.
	 */
	private BigDecimal currentFactor;

	private Date tradeDate;

	private Date settlementDate;

	private OrderSecurity settlementCurrency;

	/**
	 * Exchange Rate in Standard CCY conventions to settlement ccy.
	 * i.e. JPY -> USD and USD -> JPY rates display the same way and we either multiply or divide to calculate the settlement amount
	 */
	private BigDecimal exchangeRateToSettlementCurrency;

	private BigDecimal expectedUnitPrice;

	/**
	 * True average price based on prices of fills. Set after fills are known.
	 */
	private BigDecimal averageUnitPrice;
	/**
	 * The limit price at which the trade should be executed.
	 */
	private BigDecimal limitPrice;

	/**
	 * For securities that support accruals (bonds, swaps, etc.), specifies amount of Accrued Interest on the date of the trade.
	 */
	private BigDecimal accrualAmount1;
	/**
	 * For securities with two accrual values (Fixed and Floating Leg for IRS), the second accrual is used.
	 */
	private BigDecimal accrualAmount2;

	private BigDecimal commissionPerUnit; // commissionAmount/quantityThatCommissionWasChargedFor
	private BigDecimal commissionAmount; // total commission amount charged for this trade
	private BigDecimal feeAmount; // total fee for the trade


	private SecurityUser orderCreatorUser; // usually Portfolio Manager responsible for this order allocation

	private SecurityUser traderUser;

	private String description;

	private OrderCompany executingBrokerCompany;

	private OrderDestination orderDestination;

	// Note: Don't want a hard link?
	private Long orderBlockIdentifier;
	/**
	 * This  will track the source system of an order, can get the list of OrderAllocation that only came from particular source.
	 */
	private OrderSource orderSource;
	/**
	 * This is the master id of the order allocation of where it came
	 * from(defined by orderSource) or something to that effect.  i.e. for now using this to store IMS Trade ID
	 * so we can link it back
	 */
	private Long externalOrderIdentifier;


	private String ruleViolationCodes;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabelShort() {
		StringBuilder sb = new StringBuilder(64);
		if (getOpenCloseType() != null) {
			sb.append(isBuy() ? "BUY " : "SELL ");
		}
		if (getSecurity() != null && getSecurity().isCurrency()) {
			sb.append(CoreMathUtils.formatNumberDecimal(getAccountingNotional()));
		}
		else {
			sb.append(CoreMathUtils.formatNumberDecimal(getQuantityIntended()));
		}
		if (getSecurity() != null) {
			sb.append(" of ");
			sb.append(getSecurity().getTicker());
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}


	public String getLabel() {
		if (getClientAccount() == null) {
			return getLabelShort();
		}
		return getLabelShort() + " for " + getClientAccount().getLabel();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBuy() {
		return getOpenCloseType() != null && getOpenCloseType().isBuy();
	}


	public BigDecimal getAccrualAmount() {
		return MathUtils.add(getAccrualAmount1(), getAccrualAmount2());
	}


	public String getLocalCurrencyCode() {
		if (getSecurity() != null) {
			if (getSecurity().isCurrency()) {
				return getSecurity().getTicker();
			}
			return getSecurity().getCurrencyCode();
		}
		return null;
	}


	public String getOrderSourceWithIdentifier() {

		if (getOrderSource() != null) {
			return getOrderSource().getLabel() + " (" + getExternalOrderIdentifier() + ")";
		}
		return null;
	}
}
