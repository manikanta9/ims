package com.clifton.order.block;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderBlockSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField
	private Long id;

	@SearchField(searchField = "id")
	private Long[] ids;

	@SearchField(searchField = "orderType.id", sortField = "orderType.name")
	private Short orderTypeId;

	@SearchField(searchField = "traderUser.id", sortField = "traderUser.userName")
	private Short traderUserId;

	@SearchField(searchField = "security.id", sortField = "security.ticker")
	private Integer securityId;

	@SearchField(searchFieldPath = "security", searchField = "securityType.id", sortField = "securityType.text")
	private Integer securityTypeId;

	@SearchField(searchFieldPath = "security", searchField = "name")
	private String securityName;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "security", sortField = "underlyingSecurity.ticker")
	private Integer underlyingSecurityId;

	@SearchField(searchFieldPath = "security", searchField = "securityHierarchy.id", sortField = "securityHierarchy.name")
	private Short securityHierarchyId;

	// Using 6 levels deep for now....
	@SearchField(searchField = "security.securityHierarchy.id,security.securityHierarchy.parent.id,security.securityHierarchy.parent.parent.id,security.securityHierarchy.parent.parent.parent.id,security.securityHierarchy.parent.parent.parent.id,security.securityHierarchy.parent.parent.parent.id", leftJoin = true, sortField = "security.securityHierarchy")
	private Short securityHierarchyOrParentId;

	@SearchField(searchField = "settlementCurrency.id", sortField = "settlementCurrency.ticker")
	private Integer settlementCurrencyId;

	@SearchField
	private Boolean buy;

	@SearchField
	private Date tradeDate;

	@SearchField
	private Date settlementDate;

	@SearchField
	private BigDecimal quantityIntended;

	@SearchField
	private BigDecimal quantityPlaced;

	@SearchField
	private BigDecimal quantityFilled;


	@SearchField(searchField = "quantityIntended,quantityPlaced", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT_NULLABLE)
	private BigDecimal quantityUnplaced;


	@SearchField(searchField = "quantityIntended,quantityFilled", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT_NULLABLE)
	private BigDecimal quantityUnfilled;


	// Custom Search Field WHERE EXISTS IN OrderAllocation and OrderBlockIdentifier = OrderBlockID
	private Integer clientOrHoldingAccountId;
}
