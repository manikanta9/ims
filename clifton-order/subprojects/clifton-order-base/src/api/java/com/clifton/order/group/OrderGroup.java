package com.clifton.order.group;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.setup.OrderGroupType;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderGroup extends HierarchicalEntity<OrderGroup, Long> implements LabeledObject {

	public static final String TABLE_NAME = "OrderGroup";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderGroupType groupType;

	/**
	 * ID referring back to the source record for the group.
	 * i.e. Portfolio Run ID for submissions from IMS Portfolio Runs, Trade Session ID for submissions from Sextant Trade Sessions
	 */
	private Long groupFkFieldId;

	private Date tradeDate;

	/**
	 * The person that is creating the order.  Usually the PM
	 */
	private SecurityUser orderCreatorUser;

	private OrderSecurity security;
	private OrderSecurity secondarySecurity;

	private String note;

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;


	@NonPersistentField
	private List<OrderAllocation> orderAllocationList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getNote() != null) {
			return getNote();
		}

		StringBuilder sb = new StringBuilder(16);
		if (getSecurity() != null) {
			if (getGroupType() != null) {
				sb.append(getGroupType().getName());
				sb.append(": ");
			}

			sb.append(getSecondarySecurity().getTicker());
			if (getSecondarySecurity() != null) {
				sb.append(" - ");
				sb.append(getSecondarySecurity().getTicker());
			}
		}
		else {
			if (getGroupType() != null) {
				sb.append(getGroupType().getName());
			}
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}


	// Convenience Method
	public void addOrderAllocation(OrderAllocation orderAllocation) {
		if (this.orderAllocationList == null) {
			this.orderAllocationList = new ArrayList<>();
		}
		this.orderAllocationList.add(orderAllocation);
		if (getTradeDate() == null) {
			setTradeDate(orderAllocation.getTradeDate());
		}
		if (getOrderCreatorUser() == null) {
			setOrderCreatorUser(orderAllocation.getOrderCreatorUser());
		}
	}
}
