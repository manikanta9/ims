package com.clifton.order.execution.event;

import com.clifton.order.setup.OrderExecutionActions;


/**
 * The <code>OrderPlacementFilledEvent</code> is fired by the OrderPlacement dao observer when the placement is fully filled.
 *
 * @author manderson
 */
public class OrderPlacementFilledEvent extends BaseOrderPlacementActionEvent {

	public static final String ORDER_PLACEMENT_FILLED_EVENT_NAME = "OrderPlacementFilledEvent";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderPlacementFilledEvent(long orderPlacementId) {
		super(ORDER_PLACEMENT_FILLED_EVENT_NAME, orderPlacementId, OrderExecutionActions.ALLOCATE);
	}
}
