package com.clifton.order.execution;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.setup.OrderAllocationStatus;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import com.clifton.order.shared.OrderCompany;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderPlacement extends BaseEntity<Long> {

	public static final String TABLE_NAME = "OrderPlacement";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderBlock orderBlock;

	private OrderDestination orderDestination;

	/**
	 * Additional configuration option for the destination
	 */
	private OrderDestinationConfiguration orderDestinationConfiguration;

	private OrderCompany executingBrokerCompany;

	private BigDecimal quantityIntended;

	private BigDecimal quantityFilled;

	private BigDecimal averageUnitPrice;

	private OrderExecutionStatus executionStatus;

	private OrderAllocationStatus allocationStatus;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getQuantityUnfilled() {
		return MathUtils.subtract(getQuantityIntended(), getQuantityFilled());
	}


	public String getLabelShort() {
		if (getOrderBlock() == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder(64);
		if (getOrderBlock().isBuy()) {
			sb.append("BUY ");
		}
		else {
			sb.append("SELL ");
		}
		sb.append(CoreMathUtils.formatNumberDecimal(getQuantityIntended()));
		if (getOrderBlock().getSecurity() != null) {
			sb.append(" of ");
			sb.append(getOrderBlock().getSecurity().getTicker());
		}
		if (getOrderBlock().getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getOrderBlock().getTradeDate()));
		}
		if (getExecutingBrokerCompany() != null) {
			sb.append(" [").append(getExecutingBrokerCompany().getName()).append("]");
		}
		if (getOrderDestination() != null) {
			sb.append(" [").append(getOrderDestination().getName()).append("]");
		}
		return sb.toString();
	}


	public boolean isOfDestinationCommunicationType(DestinationCommunicationTypes communicationType) {
		if (getOrderDestination() != null) {
			return getOrderDestination().isOfDestinationCommunicationType(communicationType);
		}
		return false;
	}
}
