package com.clifton.order.block;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.security.user.SecurityUser;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>OrderBlock</code> represents one or more {@link OrderAllocation}s that can be grouped together for execution.
 * <p>
 * Blocking critieria can be more specific where necessary, but in general, block orders are required to be unique on:
 * 1. Order Security (inferred they are all the same OrderType - if at some point the same security can be on separate order types this should be updated to be included)
 * 2. Trader
 * 3. Settlement CCY (Note: Definitely necessary for CCY Orders.  Not sure if they would even apply to any other order type - Settlement CCY is not the same as the account base currency)
 * 4. Buy or Sell (Note: Currency Orders allow netting (property on the order type) and in that case a Currency Order Block can contain both).
 * 5. Trade Date and Settlement Date
 * 6. IF the orders can be executed together.  This involves looking up the available Executing Brokers and the Destinations.
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderBlock extends BaseEntity<Long> {

	public final static String TABLE_NAME = "OrderBlock";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderType orderType;

	private SecurityUser traderUser;

	private OrderSecurity security;

	private OrderSecurity settlementCurrency;

	private boolean buy;

	private Date tradeDate;

	private Date settlementDate;

	private BigDecimal quantityIntended;

	private BigDecimal quantityPlaced;

	private BigDecimal quantityFilled;


	@NonPersistentField
	private List<OrderAllocation> orderAllocationList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getQuantityUnplaced() {
		return MathUtils.subtract(getQuantityIntended(), getQuantityPlaced());
	}


	public BigDecimal getQuantityUnfilled() {
		return MathUtils.subtract(getQuantityIntended(), getQuantityFilled());
	}


	public String getLabelShort() {
		StringBuilder sb = new StringBuilder(64);
		if (isBuy()) {
			sb.append("BUY ");
		}
		else {
			sb.append("SELL ");
		}
		sb.append(CoreMathUtils.formatNumberDecimal(getQuantityIntended()));
		if (getSecurity() != null) {
			sb.append(" of ");
			sb.append(getSecurity().getTicker());
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}
}
