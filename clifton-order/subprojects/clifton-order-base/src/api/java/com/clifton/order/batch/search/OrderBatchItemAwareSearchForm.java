package com.clifton.order.batch.search;

/**
 * The <code>OrderBatchItemAwareSearchForm</code> interface is used to register custom search criteria for search forms that are OrderBatchItemAware (OrderPlacements and Trades)
 *
 * @author manderson
 */
public interface OrderBatchItemAwareSearchForm {

	/**
	 * To search for where the item in included in the selected batch
	 */
	public Long getOrderBatchId();


	/**
	 * To search for where the item is NOT included in any batch for the selected destination
	 */
	public Short getOrderBatchForDestinationIdMissing();
}
