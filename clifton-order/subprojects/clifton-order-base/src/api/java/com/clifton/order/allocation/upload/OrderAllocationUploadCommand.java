package com.clifton.order.allocation.upload;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.StringUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.group.OrderGroup;
import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.system.upload.SystemUploadCommand;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>OrderAllocationUploadCommand</code> extends the System Upload
 * however has some order allocation specific fields that will be applied
 * to the order allocations during the insert process.
 *
 * @author Mary Anderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
@Getter
@Setter
public class OrderAllocationUploadCommand extends SystemUploadCommand {

	/**
	 * Cache retrievals based on ticker
	 */
	private final Map<String, OrderSecurity> securityTickerMap = new HashMap<>();
	/**
	 * Order Allocations are always inserted as a group
	 */
	private OrderGroup orderGroup;
	/**
	 * If simple is true, this needs to be set to clarify
	 * security look-ups, et.c
	 */
	private OrderType simpleOrderType;
	/**
	 * Allow setting explicitly and not have the system calculate
	 * So For CCY Forwards users can make it the same as the Trade Date
	 */
	private Date simpleSettlementDate;
	/**
	 * Settlement CCY - If simple is true, this can optionally be set to set the paying security (settlement ccy) for all
	 * Order Allocations in the file.  Can also be set within the file itself - otherwise defaults to traded security's CCY denomination
	 */
	private OrderSecurity simpleSettlementCurrency;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return OrderAllocation.TABLE_NAME;
	}


	@Override
	public void applyUploadSpecificProperties(IdentityObject bean, DataRow row) {
		// Copy properties from TradeUploadCommand bean where null on trade
		OrderAllocation orderAllocation = (OrderAllocation) bean;
		// Clear Workflow State & Status (Shouldn't be set anyway, but just to be sure)
		orderAllocation.setWorkflowState(null);
		orderAllocation.setWorkflowStatus(null);

		if (orderAllocation.getOrderCreatorUser() == null && getOrderGroup() != null && getOrderGroup().getOrderCreatorUser() != null) {
			orderAllocation.setOrderCreatorUser(getOrderGroup().getOrderCreatorUser());
		}
		if (orderAllocation.getTradeDate() == null && getOrderGroup() != null && getOrderGroup().getTradeDate() != null) {
			orderAllocation.setTradeDate(getOrderGroup().getTradeDate());
		}

		// If missing on order allocations, and simple upload values set that are available on screen use them
		if (orderAllocation.getOrderType() == null && getSimpleOrderType() != null) {
			orderAllocation.setOrderType(getSimpleOrderType());
		}
		if (orderAllocation.getSettlementDate() == null && getSimpleSettlementDate() != null) {
			orderAllocation.setSettlementDate(getSimpleSettlementDate());
		}
		if (orderAllocation.getSettlementCurrency() == null && getSimpleSettlementCurrency() != null) {
			orderAllocation.setSettlementCurrency(getSimpleSettlementCurrency());
		}

		// If order allocation has no description, but order group note was set on screen - use it
		if (StringUtils.isEmpty(orderAllocation.getDescription()) && getOrderGroup() != null && !StringUtils.isEmpty(getOrderGroup().getNote())) {
			orderAllocation.setDescription(getOrderGroup().getNote());
		}
	}

	// Overrides - Order Allocation Import is all or nothing, do not insert fk beans, and always inserts, never updates
	// Temporarily allow partial uploads - until account master sync is available


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		// Note: Security inserts are handled independently through unmapped properties converter
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	@Override
	public void clearResults() {
		super.clearResults();
		this.securityTickerMap.clear();
	}
}
