package com.clifton.order.group;

import lombok.Getter;


/**
 * The <code>OrderGroupActions</code> enum is used to process an action on an Order Group
 *
 * @author manderson
 */
@Getter
public enum OrderGroupActions {

	VALIDATE("Pending");


	private final String stateName;
	// Similar to Trade Workflow - for "Approval" action we allow Read Access Only
	private final boolean readAccessSufficientToExecute;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	OrderGroupActions(String stateName) {
		this(stateName, false);
	}


	OrderGroupActions(String stateName, boolean readAccessSufficientToExecute) {
		this.stateName = stateName;
		this.readAccessSufficientToExecute = readAccessSufficientToExecute;
	}
}
