package com.clifton.order.trade;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.order.trade.search.OrderTradeSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderTradeService {


	////////////////////////////////////////////////////////////////////////////
	////////            Order Trade Methods                             ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderTrade getOrderTrade(long id);


	@DoNotAddRequestMapping
	public void saveOrderTradeList(List<OrderTrade> orderTradeList);


	public List<OrderTrade> getOrderTradeList(OrderTradeSearchForm searchForm);
}
