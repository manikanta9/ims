package com.clifton.order.allocation.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.allocation.OrderAllocation;


/**
 * @author manderson
 */
public interface OrderAllocationUploadService {

	/**
	 * Uploads Order Allocations into the system as one Order ALlocation Import group.
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dtoClass = OrderAllocation.class)
	public void uploadOrderAllocationUploadFile(OrderAllocationUploadCommand uploadCommand);
}
