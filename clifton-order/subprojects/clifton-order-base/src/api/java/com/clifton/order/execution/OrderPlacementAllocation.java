package com.clifton.order.execution;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.order.allocation.OrderAllocation;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


/**
 * The <code>OrderPlacementAllocation</code> class is used for cases where an {@link OrderAllocation} is placed with multiple destinations and brokers and results in multiple trades.
 * If the order is only placed with one executing broker the original order object can be updated with this information.
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderPlacementAllocation extends BaseEntity<Long> {

	private OrderPlacement orderPlacement;

	private OrderAllocation orderAllocation;

	private BigDecimal quantity;

	private BigDecimal price;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (getOrderPlacement() == null || getOrderAllocation() == null) {
			return "" + getId();
		}
		StringBuilder sb = new StringBuilder(64);
		if (getOrderAllocation().isBuy()) {
			sb.append("BUY ");
		}
		else {
			sb.append("SELL ");
		}
		sb.append(CoreMathUtils.formatNumberDecimal(getQuantity()));
		if (getOrderAllocation().getSecurity() != null) {
			sb.append(" of ");
			sb.append(getOrderAllocation().getSecurity().getTicker());
		}
		if (getOrderAllocation().getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getOrderAllocation().getTradeDate()));
		}
		if (getOrderPlacement().getExecutingBrokerCompany() != null) {
			sb.append(" [").append(getOrderPlacement().getExecutingBrokerCompany().getName()).append("]");
		}
		if (getOrderPlacement().getOrderDestination() != null) {
			sb.append(" [").append(getOrderPlacement().getOrderDestination().getName()).append("]");
		}
		if (getOrderAllocation().getClientAccount() != null) {
			sb.append(" for ").append(getOrderAllocation().getClientAccount().getLabel());
		}
		return sb.toString();
	}
}
