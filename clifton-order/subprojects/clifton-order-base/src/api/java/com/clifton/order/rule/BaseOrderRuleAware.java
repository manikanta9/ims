package com.clifton.order.rule;

import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>BaseOrderRuleAware</code> class is a base class that implements OrderRuleAware and provides properties necessary.
 * Can be extended by other classes, or used alone for rule evaluation previews
 *
 * @author manderson
 */
@Getter
@Setter
public class BaseOrderRuleAware implements OrderRuleAware {

	private OrderAccount clientAccount;
	private OrderAccount holdingAccount;

	private OrderCompany executingBrokerCompany;

	private OrderSecurity security;
	private OrderSecurity settlementCurrency;

	private OrderType orderType;

	private Date tradeDate;
	private Date settlementDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BaseOrderRuleAware forOrderRuleAware(OrderRuleAware orderRuleAware) {
		BaseOrderRuleAware baseOrderRuleAware = new BaseOrderRuleAware();
		baseOrderRuleAware.setClientAccount(orderRuleAware.getClientAccount());
		baseOrderRuleAware.setHoldingAccount((orderRuleAware.getHoldingAccount()));
		baseOrderRuleAware.setSecurity(orderRuleAware.getSecurity());
		baseOrderRuleAware.setSettlementCurrency(orderRuleAware.getSettlementCurrency());
		baseOrderRuleAware.setOrderType(orderRuleAware.getOrderType());
		baseOrderRuleAware.setExecutingBrokerCompany(orderRuleAware.getExecutingBrokerCompany());
		baseOrderRuleAware.setTradeDate(orderRuleAware.getTradeDate());
		baseOrderRuleAware.setSettlementDate(orderRuleAware.getSettlementDate());
		return baseOrderRuleAware;
	}


	public static BaseOrderRuleAware forOrderRuleAwareSettlementCurrency(OrderRuleAware orderRuleAware) {
		// If order rule aware is a currency order - will return Order Rule Aware object with the settlement currency as the security so we can evaluate rules.
		// If it's NOT a currency order, returns null (i.e. Does not apply)
		if (orderRuleAware.getSecurity().isCurrency()) {
			BaseOrderRuleAware baseOrderRuleAware = new BaseOrderRuleAware();
			baseOrderRuleAware.setClientAccount(orderRuleAware.getClientAccount());
			baseOrderRuleAware.setHoldingAccount((orderRuleAware.getHoldingAccount()));
			baseOrderRuleAware.setSecurity(orderRuleAware.getSettlementCurrency());
			baseOrderRuleAware.setOrderType(orderRuleAware.getOrderType());
			baseOrderRuleAware.setExecutingBrokerCompany(orderRuleAware.getExecutingBrokerCompany());
			baseOrderRuleAware.setTradeDate(orderRuleAware.getTradeDate());
			baseOrderRuleAware.setSettlementDate(orderRuleAware.getSettlementDate());
			return baseOrderRuleAware;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Serializable getIdentity() {
		return null;
	}


	@Override
	public boolean isNewBean() {
		return true;
	}
}
