package com.clifton.order.execution.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderPlacementAllocationSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "orderPlacement.id")
	private Long orderPlacementId;

	@SearchField(searchField = "orderAllocation.id")
	private Long orderAllocationId;

	@SearchField(searchField = "orderBlock.id", searchFieldPath = "orderPlacement")
	private Long orderBlockId;

	@SearchField(searchField = "orderType.id", searchFieldPath = "orderAllocation", sortField = "orderType.name")
	private Short orderTypeId;

	@SearchField(searchField = "clientCompany.id", searchFieldPath = "orderAllocation.clientAccount", sortField = "clientCompany.name")
	private Integer clientCompanyId;

	@SearchField(searchField = "clientAccount.id", searchFieldPath = "orderAllocation", sortField = "clientAccount.accountNumber")
	private Integer clientAccountId;

	@SearchField(searchField = "service.id", searchFieldPath = "orderAllocation.clientAccount", sortField = "service.name")
	private Short serviceId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "orderAllocation.clientAccount.service.id,orderAllocation.clientAccount.service.parent.id,orderAllocation.clientAccount.service.parent.parent.id,orderAllocation.clientAccount.service.parent.parent.parent.id", leftJoin = true, sortField = "orderAllocation.clientAccount.service.name")
	private Short serviceOrParentId;

	@SearchField(searchField = "teamSecurityGroup.id", searchFieldPath = "orderAllocation.clientAccount", sortField = "teamSecurityGroup.name")
	private Short teamSecurityGroupId;

	@SearchField(searchFieldPath = "orderAllocation.clientAccount", searchField = "portfolioManagerUser.id", sortField = "portfolioManagerUser.displayName")
	private Short portfolioManagerUserId;


	@SearchField(searchField = "holdingAccount.id", searchFieldPath = "orderAllocation", sortField = "holdingAccount.accountNumber")
	private Integer holdingAccountId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "orderAllocation.holdingAccount", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchFieldPath = "orderAllocation", searchField = "security.id", sortField = "security.ticker")
	private Integer securityId;

	@SearchField(searchFieldPath = "orderAllocation.security", searchField = "securityType.id", sortField = "securityType.text")
	private Integer securityTypeId;

	@SearchField(searchFieldPath = "orderAllocation.security", searchField = "name")
	private String securityName;

	@SearchField(searchFieldPath = "orderAllocation.security", searchField = "underlyingSecurity.id", sortField = "underlyingSecurity.ticker")
	private Integer underlyingSecurityId;

	@SearchField(searchFieldPath = "orderAllocation.security", searchField = "securityHierarchy.id", sortField = "securityHierarchy.name")
	private Short securityHierarchyId;

	// Using 6 levels deep for now....
	@SearchField(searchField = "orderAllocation.security.securityHierarchy.id,orderAllocation.security.securityHierarchy.parent.id,orderAllocation.security.securityHierarchy.parent.parent.id,orderAllocation.security.securityHierarchy.parent.parent.parent.id,orderAllocation.security.securityHierarchy.parent.parent.parent.id,orderAllocation.security.securityHierarchy.parent.parent.parent.id", leftJoin = true, sortField = "orderAllocation.security.securityHierarchy.name")
	private Short securityHierarchyOrParentId;

	@SearchField(searchField = "orderBlock.buy", searchFieldPath = "orderPlacement")
	private Boolean buy;

	@SearchField(searchField = "orderBlock.tradeDate", searchFieldPath = "orderPlacement")
	private Date tradeDate;

	@SearchField(searchField = "orderDestination.id", searchFieldPath = "orderPlacement", sortField = "orderDestination.name")
	private Short orderDestinationId;

	@SearchField(searchField = "executingBrokerCompany.id", searchFieldPath = "orderPlacement", sortField = "executingBrokerCompany.name")
	private Integer executingBrokerCompanyId;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal price;
}
