package com.clifton.order.execution.event;

import com.clifton.order.shared.OrderCompany;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderPlacementExecutionUpdateEvent extends BaseOrderPlacementEvent {

	public static final String ORDER_PLACEMENT_EXECUTION_UPDATE_EVENT_NAME = "OrderPlacementExecutionUpdateEvent";


	private BigDecimal quantityFilled;

	private BigDecimal averageUnitPrice;

	/**
	 * Used for Currencies, so we know how the price (a.k.a. exchange rate) was quoted
	 * in case we need to convert it to standard currency conventions
	 */
	private String ticker;

	private String newExecutionStatusName;

	/**
	 * If filled through FIX we'd have the code
	 * If filled manually, we'd have the company
	 */
	private String executingBrokerCode;
	private OrderCompany executingBrokerCompany;

	/**
	 * For Batch Items - the placement may be canceled but still be in an existing batch
	 * (if canceled after batch was moved out of open status).  If that is the case we don't want to move the placement out of canceled state
	 * but we also don't want the batch update to have to check the status
	 */
	private boolean skipUpdateIfCanceled;

	/**
	 * If in rejected state, this is the rejection reason
	 * Used to create a SystemNote on the placement with this information
	 */
	private String rejectionReason;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderPlacementExecutionUpdateEvent(long orderPlacementId) {
		super(ORDER_PLACEMENT_EXECUTION_UPDATE_EVENT_NAME, orderPlacementId);
	}
}
