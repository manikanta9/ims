package com.clifton.order.execution.event;

import com.clifton.order.setup.OrderExecutionActions;


/**
 * The <code>OrderPlacementAllocationCompleteEvent</code> is fired by the OrderPlacement dao observer when the placement is fully allocated.
 * <p>
 * This can trigger the creation of the Order Trades
 *
 * @author manderson
 */
public class OrderPlacementAllocationCompleteEvent extends BaseOrderPlacementActionEvent {

	public static final String ORDER_PLACEMENT_ALLOCATION_COMPLETE_EVENT_NAME = "OrderPlacementAllocationCompleteEvent";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderPlacementAllocationCompleteEvent(long orderPlacementId) {
		super(ORDER_PLACEMENT_ALLOCATION_COMPLETE_EVENT_NAME, orderPlacementId, OrderExecutionActions.COMPLETE);
	}
}
