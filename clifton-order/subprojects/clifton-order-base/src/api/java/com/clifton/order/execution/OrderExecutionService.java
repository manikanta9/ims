package com.clifton.order.execution;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.order.execution.search.OrderPlacementAllocationSearchForm;
import com.clifton.order.execution.search.OrderPlacementSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderExecutionService {

	////////////////////////////////////////////////////////////////////////////
	////////              Order Placement Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderPlacement getOrderPlacement(long id);


	public List<OrderPlacement> getOrderPlacementList(OrderPlacementSearchForm searchForm);


	public OrderPlacement saveOrderPlacement(OrderPlacement bean);


	/**
	 * Used when manually entering the fill information for the OrderPlacement.
	 * Validation by default will check if the price is within 5% of the most recent price to the trade date
	 */
	@UserIgnorableValidation
	public OrderPlacement saveOrderPlacementManualExecution(OrderPlacement bean, boolean ignoreValidation);


	@DoNotAddRequestMapping
	public OrderPlacement saveOrderPlacementWithAllocations(OrderPlacement orderPlacement, List<OrderPlacementAllocation> orderPlacementAllocationList);

	////////////////////////////////////////////////////////////////////////////
	////////           Order Placement Allocation Methods               ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderPlacementAllocation getOrderPlacementAllocation(long id);


	public List<OrderPlacementAllocation> getOrderPlacementAllocationList(OrderPlacementAllocationSearchForm searchForm);


	public List<OrderPlacementAllocation> previewOrderPlacementAllocationListForPlacement(long orderPlacementId);


	/**
	 * Used for batching to get the list of previewed placement allocations for all in the batch.
	 * The batch id is required on the search form (prevents attempting to pull too much data)
	 */
	public List<OrderPlacementAllocation> previewOrderPlacementAllocationListForBatch(OrderPlacementSearchForm searchForm);
}
