package com.clifton.order.allocation;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>OrderAllocationAmendment</code> class references an existing order allocation id and values for fields that can be amended post execution.
 * This is currently price/rate information and trade/settlement dates.  Eventually we can easily add executing broker, accruals, fees, commissions, etc.
 *
 * @author manderson
 */
@NonPersistentObject
@Getter
@Setter
public class OrderAllocationAmendmentCommand {

	private long orderAllocationId;

	private Date amendTradeDate;

	private Date amendSettlementDate;

	private BigDecimal amendAverageUnitPrice;

	private BigDecimal amendExchangeRateToSettlementCurrency;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static OrderAllocationAmendmentCommand forOrderAllocation(long orderAllocationId) {
		OrderAllocationAmendmentCommand orderAllocationAmendmentCommand = new OrderAllocationAmendmentCommand(orderAllocationId);
		return orderAllocationAmendmentCommand;
	}


	private OrderAllocationAmendmentCommand(long orderAllocationId) {
		this.orderAllocationId = orderAllocationId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void copyAmendedValuesToOrderAllocation(OrderAllocation orderAllocation) {
		if (getAmendTradeDate() != null) {
			orderAllocation.setTradeDate(getAmendTradeDate());
		}
		if (getAmendSettlementDate() != null) {
			orderAllocation.setSettlementDate(getAmendSettlementDate());
		}
		if (getAmendAverageUnitPrice() != null) {
			orderAllocation.setAverageUnitPrice(getAmendAverageUnitPrice());
		}
		if (getAmendExchangeRateToSettlementCurrency() != null) {
			orderAllocation.setExchangeRateToSettlementCurrency(getAmendExchangeRateToSettlementCurrency());
		}
	}
}
