package com.clifton.order.execution.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.order.batch.search.OrderBatchItemAwareSearchForm;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderPlacementSearchForm extends BaseAuditableEntitySearchForm implements OrderBatchItemAwareSearchForm {

	@SearchField
	private Long id;

	@SearchField(searchFieldPath = "orderBlock", searchField = "orderType.id", sortField = "orderType.name")
	private Short orderTypeId;

	@SearchField(searchField = "orderBlock.id")
	private Long orderBlockId;

	@SearchField(searchField = "orderDestination.id", sortField = "orderDestination.name")
	private Short orderDestinationId;

	@SearchField(searchFieldPath = "orderDestination", searchField = "destinationType.id", sortField = "destinationType.name")
	private Short orderDestinationTypeId;

	@SearchField(searchField = "orderDestinationConfiguration.id", sortField = "orderDestinationConfiguration.name")
	private Short orderDestinationConfigurationId;

	@SearchField(searchFieldPath = "orderDestination.destinationType", searchField = "destinationCommunicationType")
	private DestinationCommunicationTypes destinationCommunicationType;

	@SearchField(searchField = "executingBrokerCompany.id", sortField = "executingBrokerCompany.name")
	private Integer executingBrokerCompanyId;

	@SearchField(searchFieldPath = "orderBlock", searchField = "traderUser.id", sortField = "traderUser.userName")
	private Short traderUserId;

	@SearchField(searchFieldPath = "orderBlock", searchField = "security.id", sortField = "security.ticker")
	private Integer securityId;

	@SearchField(searchFieldPath = "orderBlock.security", searchField = "securityType.id", sortField = "securityType.text")
	private Integer securityTypeId;

	@SearchField(searchFieldPath = "orderBlock.security", searchField = "name")
	private String securityName;

	@SearchField(searchFieldPath = "orderBlock.security", searchField = "underlyingSecurity.id", sortField = "underlyingSecurity.ticker")
	private Integer underlyingSecurityId;

	@SearchField(searchFieldPath = "orderBlock.security", searchField = "securityHierarchy.id", sortField = "securityHierarchy.name")
	private Short securityHierarchyId;

	// Using 6 levels deep for now....
	@SearchField(searchField = "orderBlock.security.securityHierarchy.id,orderBlock.security.securityHierarchy.parent.id,orderBlock.security.securityHierarchy.parent.parent.id,orderBlock.security.securityHierarchy.parent.parent.parent.id,orderBlock.security.securityHierarchy.parent.parent.parent.id,orderBlock.security.securityHierarchy.parent.parent.parent.id", leftJoin = true, sortField = "orderBlock.security.securityHierarchy.name")
	private Short securityHierarchyOrParentId;

	@SearchField(searchFieldPath = "orderBlock", searchField = "settlementCurrency.id", sortField = "settlementCurrency.ticker")
	private Integer settlementCurrencyId;

	@SearchField(searchField = "orderBlock.buy")
	private Boolean buy;

	@SearchField(searchField = "orderBlock.tradeDate")
	private Date tradeDate;

	@SearchField(searchField = "orderBlock.settlementDate")
	private Date settlementDate;

	@SearchField
	private BigDecimal quantityIntended;

	@SearchField
	private BigDecimal quantityFilled;

	@SearchField(searchField = "quantityIntended,quantityFilled", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal quantityUnfilled;

	@SearchField
	private BigDecimal averageUnitPrice;


	@SearchField(searchField = "executionStatus.id", sortField = "executionStatus.name")
	private Short executionStatusId;

	@SearchField(searchField = "executionStatus.fillComplete", leftJoin = true)
	private Boolean executionStatusComplete;


	@SearchField(searchField = "allocationStatus.id", sortField = "allocationStatus.name")
	private Short allocationStatusId;

	@SearchField(searchField = "allocationStatus.allocationComplete")
	private Boolean allocationStatusComplete;

	// Custom search field execution status.canceled = false and (execution status.fill complete = false or null or allocation status.allocation complete = false or null)
	private Boolean activePlacementsOnly;

	// Custom Search field applied in Order Batch based on interface OrderBatchItemAwareSearchForm
	// To find placements included in a specific batch
	private Long orderBatchId;

	// Custom Search field applied in Order Batch based on interface OrderBatchItemAwareSearchForm
	// To find placements NOT included in any batch for a specific destination
	private Short orderBatchForDestinationIdMissing;


	// Custom Search Field WHERE EXISTS IN OrderAllocation and OrderBlockIdentifier = OrderBlockID
	private Integer clientOrHoldingAccountId;
}
