package com.clifton.order.allocation.builder;


import com.clifton.core.util.MathUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.setup.OrderOpenCloseType;
import com.clifton.order.setup.OrderType;
import com.clifton.order.setup.builder.OrderTypeBuilder;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.builder.OrderAccountBuilder;
import com.clifton.order.shared.builder.OrderSecurityBuilder;
import com.clifton.security.user.SecurityUser;
import lombok.Builder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;


public class OrderAllocationBuilder {

	public static final Lazy<Random> RANDOM_LAZY = new Lazy<>(Random::new);
	public static final OrderOpenCloseType BUY, BUY_TO_OPEN, BUY_TO_CLOSE, SELL, SELL_TO_OPEN, SELL_TO_CLOSE;
	public static final Date DEFAULT_TRADE_DATE = DateUtils.toDate("06/07/2021");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	static {
		BUY = new OrderOpenCloseType();
		BUY.setId((short) 1);
		BUY.setName("Buy");
		BUY.setBuy(true);
		BUY.setOpen(true);
		BUY.setClose(true);

		BUY_TO_OPEN = new OrderOpenCloseType();
		BUY_TO_OPEN.setId((short) 2);
		BUY_TO_OPEN.setName("Buy to Open");
		BUY_TO_OPEN.setBuy(true);
		BUY_TO_OPEN.setOpen(true);
		BUY_TO_OPEN.setClose(false);

		BUY_TO_CLOSE = new OrderOpenCloseType();
		BUY_TO_CLOSE.setId((short) 3);
		BUY_TO_CLOSE.setName("Buy to Close");
		BUY_TO_CLOSE.setBuy(true);
		BUY_TO_CLOSE.setOpen(false);
		BUY_TO_CLOSE.setClose(true);

		SELL = new OrderOpenCloseType();
		SELL.setId((short) 4);
		SELL.setName("Sell");
		SELL.setBuy(false);
		SELL.setOpen(true);
		SELL.setClose(true);

		SELL_TO_OPEN = new OrderOpenCloseType();
		SELL_TO_OPEN.setId((short) 5);
		SELL_TO_OPEN.setName("Sell to Open");
		SELL_TO_OPEN.setBuy(false);
		SELL_TO_OPEN.setOpen(true);
		SELL_TO_OPEN.setClose(false);

		SELL_TO_CLOSE = new OrderOpenCloseType();
		SELL_TO_CLOSE.setId((short) 6);
		SELL_TO_CLOSE.setName("Sell to Close");
		SELL_TO_CLOSE.setBuy(false);
		SELL_TO_CLOSE.setOpen(false);
		SELL_TO_CLOSE.setClose(true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Builder
	public static OrderAllocation build(
			Long id,
			OrderOpenCloseType openCloseType,
			OrderCompany executingBrokerCompany,
			BigDecimal quantityIntended,
			OrderSecurity orderSecurity,
			OrderAccount account,
			OrderAccount clientAccount,
			OrderAccount holdingAccount,
			String note,
			OrderSecurity settlementCurrency,
			String exchangeRate,
			String accrualAmount1,
			String accrualAmount2,
			String accountingNotional,
			String averageUnitPrice,
			String commissionPerUnit,
			String feeAmount,
			String totalCommission,
			OrderDestination orderDestination,
			Date onTradeDate,
			Date tradeDate,
			Date settlementDate,
			OrderType orderType,
			String forTrader,
			SecurityUser traderUser,
			String expectedUnitPrice
	) {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setId(id);
		orderAllocation.setOpenCloseType(openCloseType);
		orderAllocation.setQuantityIntended(quantityIntended);
		orderAllocation.setExecutingBrokerCompany(executingBrokerCompany);
		orderAllocation.setSecurity(orderSecurity);
		orderAllocation.setClientAccount(account);
		orderAllocation.setHoldingAccount(account);
		if (clientAccount != null) {
			orderAllocation.setClientAccount(clientAccount);
		}
		if (holdingAccount != null) {
			orderAllocation.setHoldingAccount(holdingAccount);
		}
		orderAllocation.setDescription(note);
		orderAllocation.setSettlementCurrency(settlementCurrency);
		orderAllocation.setExchangeRateToSettlementCurrency(MathUtils.getAsBigDecimal(exchangeRate));
		orderAllocation.setAccrualAmount1(MathUtils.getAsBigDecimal(accrualAmount1));
		orderAllocation.setAccrualAmount2(MathUtils.getAsBigDecimal(accrualAmount2));
		orderAllocation.setAccountingNotional(MathUtils.getAsBigDecimal(accountingNotional));
		orderAllocation.setAverageUnitPrice(MathUtils.getAsBigDecimal(averageUnitPrice));
		orderAllocation.setCommissionPerUnit(MathUtils.getAsBigDecimal(commissionPerUnit));
		orderAllocation.setFeeAmount(MathUtils.getAsBigDecimal(feeAmount));
		orderAllocation.setCommissionAmount(MathUtils.getAsBigDecimal(totalCommission));
		orderAllocation.setOrderDestination(orderDestination);
		orderAllocation.setTradeDate(tradeDate);
		orderAllocation.setSettlementDate(settlementDate);
		if (onTradeDate != null) {
			orderAllocation.setTradeDate(onTradeDate);
			if (orderAllocation.getSettlementDate() == null || orderAllocation.getSettlementDate().equals(DEFAULT_TRADE_DATE)) {
				orderAllocation.setSettlementDate(onTradeDate);
			}
		}
		orderAllocation.setOrderType(orderType);
		orderAllocation.setTraderUser(traderUser);
		if (forTrader != null) {
			SecurityUser securityUser = new SecurityUser();
			securityUser.setUserName(forTrader);
			securityUser.setId((short) 1);
			orderAllocation.setTraderUser(securityUser);
		}
		orderAllocation.setExpectedUnitPrice(MathUtils.getAsBigDecimal(expectedUnitPrice));
		return orderAllocation;
	}


	/**
	 * Creates a new order allocation builder with defaults populated for major fields: client/holding accounts, paying security, open close type (buy), etc.
	 */
	public static InnerBuilder createNewCurrencyOrderAllocation_AUD_USD() {
		return builder()
				.buy()
				.id((long) 10)
				.onTradeDate(DEFAULT_TRADE_DATE)
				.account(OrderAccountBuilder.createTestAccount1().build())
				.orderSecurity(OrderSecurityBuilder.newAUD())
				.orderType(OrderTypeBuilder.createCurrency().build())
				.settlementCurrency(OrderSecurityBuilder.newUSD())
				.accountingNotional("10000");
	}


	/**
	 * Creates a new order allocation builder with second defaults populated for major fields.  Uses a second test account and different amount to easily test blocking currency orders
	 */
	public static InnerBuilder createNewCurrencyOrderAllocation_AUD_USD_Account2() {
		return builder()
				.buy()
				.id((long) 20)
				.onTradeDate(DEFAULT_TRADE_DATE)
				.account(OrderAccountBuilder.createTestAccount2().build())
				.orderSecurity(OrderSecurityBuilder.newAUD())
				.orderType(OrderTypeBuilder.createCurrency().build())
				.settlementCurrency(OrderSecurityBuilder.newUSD())
				.accountingNotional("5000");
	}


	/**
	 * Creates a new order allocation builder with defaults populated for major fields: client/holding accounts, paying security, open close type (buy), etc. uses GBP stock (price multiplier of 0.01)
	 */
	public static InnerBuilder createNewStockOrderAllocationGBP(String ticker) {
		return builder()
				.buy()
				.id((long) 30)
				.onTradeDate(DEFAULT_TRADE_DATE)
				.account(OrderAccountBuilder.createTestAccount1().build())
				.orderSecurity(OrderSecurityBuilder.newGBPStock(ticker).id(30).build())
				.orderType(OrderTypeBuilder.createStocks().build())
				.settlementCurrency(OrderSecurityBuilder.newGBP())
				.quantityIntended("285");
	}


	public static class InnerBuilder {

		public InnerBuilder buy() {
			return openCloseType(BUY);
		}


		public InnerBuilder sell() {
			return openCloseType(SELL);
		}


		public InnerBuilder inUSD() {
			return settlementCurrency(OrderSecurityBuilder.newUSD());
		}


		public InnerBuilder inEUR() {
			return settlementCurrency(OrderSecurityBuilder.newEUR());
		}


		public InnerBuilder quantityIntended(String quantityIntended) {
			return quantityIntended(MathUtils.getAsBigDecimal(quantityIntended));
		}


		public InnerBuilder quantityIntended(BigDecimal quantityIntended) {
			this.quantityIntended = quantityIntended;
			return this;
		}
	}
}
