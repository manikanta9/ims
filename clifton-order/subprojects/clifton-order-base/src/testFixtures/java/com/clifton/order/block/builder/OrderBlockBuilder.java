package com.clifton.order.block.builder;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.security.user.SecurityUser;
import lombok.Builder;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class OrderBlockBuilder {

	@Builder
	public static OrderBlock build(
			Long id,
			boolean buy,
			OrderType orderType,
			SecurityUser traderUser,
			OrderSecurity security,
			OrderSecurity settlementCurrency,
			Date tradeDate,
			Date settlementDate,
			BigDecimal quantityIntended,
			BigDecimal quantityPlaced,
			BigDecimal quantityFilled
	) {
		OrderBlock orderBlock = new OrderBlock();
		orderBlock.setId(id);
		orderBlock.setBuy(buy);
		orderBlock.setOrderType(orderType);
		orderBlock.setTraderUser(traderUser);
		orderBlock.setSecurity(security);
		orderBlock.setSettlementCurrency(settlementCurrency);
		orderBlock.setTradeDate(tradeDate);
		orderBlock.setSettlementDate(settlementDate);
		orderBlock.setQuantityIntended(quantityIntended);
		orderBlock.setQuantityPlaced(quantityPlaced);
		orderBlock.setQuantityFilled(quantityFilled);
		return orderBlock;
	}


	public static InnerBuilder createForOrderAllocation(OrderAllocation orderAllocation) {
		return createForOrderAllocationList(Collections.singletonList(orderAllocation));
	}


	/**
	 * Note for the list we assume that all expected equal block properties already match, that is not validated here
	 */
	public static InnerBuilder createForOrderAllocationList(List<OrderAllocation> orderAllocationList) {
		OrderAllocation firstAllocation = orderAllocationList.get(0);
		boolean currency = firstAllocation.getSecurity().isCurrency();
		BigDecimal totalQuantity = CoreMathUtils.sumProperty(orderAllocationList, orderAllocation -> {
			BigDecimal allocationAmount = currency ? orderAllocation.getAccountingNotional() : orderAllocation.getQuantityIntended();
			allocationAmount = orderAllocation.isBuy() ? allocationAmount : MathUtils.negate(allocationAmount);
			return allocationAmount;
		});
		boolean buy = MathUtils.isGreaterThan(totalQuantity, BigDecimal.ZERO);
		totalQuantity = MathUtils.abs(totalQuantity);

		return builder()
				.buy(buy)
				.id(firstAllocation.getId())
				.orderType(firstAllocation.getOrderType())
				.security(firstAllocation.getSecurity())
				.settlementCurrency(firstAllocation.getSettlementCurrency())
				.quantityIntended(totalQuantity)
				.quantityPlaced(BigDecimal.ZERO)
				.quantityFilled(BigDecimal.ZERO)
				.tradeDate(firstAllocation.getTradeDate())
				.settlementDate(firstAllocation.getSettlementDate())
				.traderUser(firstAllocation.getTraderUser());
	}
}
