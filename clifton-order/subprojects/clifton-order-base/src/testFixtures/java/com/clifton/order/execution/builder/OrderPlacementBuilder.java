package com.clifton.order.execution.builder;

import com.clifton.order.block.OrderBlock;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.setup.OrderAllocationStatus;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderCompany;
import lombok.Builder;

import java.math.BigDecimal;


/**
 * @author manderson
 */
public class OrderPlacementBuilder {

	@Builder
	public static OrderPlacement build(
			Long id,
			OrderBlock orderBlock,
			OrderDestination orderDestination,
			OrderCompany executingBrokerCompany,
			BigDecimal quantityIntended,
			BigDecimal quantityFilled,
			BigDecimal averagePrice,
			String executionStatus,
			String allocationStatus
	) {
		OrderPlacement orderPlacement = new OrderPlacement();
		orderPlacement.setId(id);
		orderPlacement.setOrderBlock(orderBlock);
		orderPlacement.setOrderDestination(orderDestination);
		orderPlacement.setExecutingBrokerCompany(executingBrokerCompany);
		orderPlacement.setQuantityIntended(quantityIntended);
		orderPlacement.setQuantityFilled(quantityFilled);
		orderPlacement.setAverageUnitPrice(averagePrice);
		if (executionStatus != null) {
			OrderExecutionStatus status = new OrderExecutionStatus();
			status.setName(executionStatus);
			orderPlacement.setExecutionStatus(status);
		}
		if (allocationStatus != null) {
			OrderAllocationStatus status = new OrderAllocationStatus();
			status.setName(allocationStatus);
			orderPlacement.setAllocationStatus(status);
		}
		return orderPlacement;
	}


	public static InnerBuilder createForOrderBlock_New(OrderBlock orderBlock) {
		return builder()
				.orderBlock(orderBlock)
				.id(orderBlock.getId())
				.quantityIntended(orderBlock.getQuantityIntended())
				.quantityFilled(BigDecimal.ZERO)
				.executionStatus(OrderExecutionStatus.NEW)
				.allocationStatus(OrderAllocationStatus.ALLOCATION_INCOMPLETE);
	}


	public static InnerBuilder createForOrderBlock_Filled(OrderBlock orderBlock, BigDecimal executionPrice) {
		return builder()
				.orderBlock(orderBlock)
				.id(orderBlock.getId())
				.quantityIntended(orderBlock.getQuantityIntended())
				.quantityFilled(orderBlock.getQuantityIntended())
				.averagePrice(executionPrice)
				.executionStatus(OrderExecutionStatus.FILLED)
				.allocationStatus(OrderAllocationStatus.ALLOCATION_COMPLETE_NO_REPORT);
	}
}
