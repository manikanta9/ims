package com.clifton.order.note;

import com.clifton.core.util.CollectionUtils;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;


/**
 * The <code>OrderNoteService</code> provides simplified access to get and save notes associated with Order related objects.
 *
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderNoteServiceImpl implements OrderNoteService {

	private SystemNoteService systemNoteService;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemNote getOrderPlacementNoteLastForNoteType(long orderPlacementId, String noteTypeName) {
		SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
		searchForm.setEntityTableName(OrderPlacement.TABLE_NAME);
		searchForm.setFkFieldId(orderPlacementId);
		searchForm.setNoteTypeName(noteTypeName);
		searchForm.setOrderBy("createDate:desc"); // Update date?
		searchForm.setLimit(1);
		return CollectionUtils.getFirstElement(getSystemNoteService().getSystemNoteListForEntity(searchForm));
	}


	@Override
	public SystemNote saveOrderPlacementNote(long orderPlacementId, String noteTypeName, String noteText) {
		return saveSystemNote(OrderPlacement.TABLE_NAME, orderPlacementId, noteTypeName, noteText);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private SystemNote saveSystemNote(String tableName, long id, String noteTypeName, String noteText) {
		SystemNote note = new SystemNote();
		note.setNoteType(getSystemNoteService().getSystemNoteTypeByTableAndName(getSystemSchemaService().getSystemTableByName(tableName).getId(), noteTypeName));
		note.setText(noteText);
		note.setFkFieldId(id);
		return getSystemNoteService().saveSystemNote(note);
	}
}
