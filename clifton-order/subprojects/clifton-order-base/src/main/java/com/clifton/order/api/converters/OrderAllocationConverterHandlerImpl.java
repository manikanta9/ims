package com.clifton.order.api.converters;

import com.clifton.core.util.date.DateUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.api.server.model.BaseOrderAllocationModel;
import com.clifton.order.api.server.model.CurrencyOrderAllocationModel;
import com.clifton.order.api.server.model.OrderGroupModel;
import com.clifton.order.api.server.model.StockOrderAllocationModel;
import com.clifton.order.api.server.model.UserModel;
import com.clifton.order.group.OrderGroup;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Component
public class OrderAllocationConverterHandlerImpl implements OrderAllocationConverterHandler {

	////////////////////////////////////////////////////////////////////////////
	////////              OrderAllocationModel Converters               ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CurrencyOrderAllocationModel convertOrderAllocationToCurrencyOrderAllocation(OrderAllocation orderAllocation) {
		CurrencyOrderAllocationModel model = new CurrencyOrderAllocationModel();
		populateBaseOrderAllocationModelFields(orderAllocation, model);
		model.setGivenAmount(orderAllocation.getAccountingNotional());
		model.setGivenCurrencyCode(orderAllocation.getSecurity().getTicker());
		return model;
	}


	@Override
	public StockOrderAllocationModel convertOrderAllocationToStockOrderAllocation(OrderAllocation orderAllocation) {
		StockOrderAllocationModel model = new StockOrderAllocationModel();
		populateBaseOrderAllocationModelFields(orderAllocation, model);
		model.setQuantityIntended(orderAllocation.getQuantityIntended());
		model.setSecurity(orderAllocation.getSecurity().toModelObject());
		return model;
	}


	private BaseOrderAllocationModel convertOrderAllocationToOrderAllocationModel(OrderAllocation orderAllocation) {
		if (orderAllocation.getSecurity().isCurrency()) {
			return convertOrderAllocationToCurrencyOrderAllocation(orderAllocation);
		}
		return convertOrderAllocationToStockOrderAllocation(orderAllocation);
	}


	private <T extends BaseOrderAllocationModel> void populateBaseOrderAllocationModelFields(OrderAllocation orderAllocation, T model) {
		model.setId(orderAllocation.getId());
		model.setOrderType(orderAllocation.getOrderType().getName());
		model.setWorkflowState(orderAllocation.getWorkflowState().getName());
		model.setWorkflowStatus(orderAllocation.getWorkflowStatus().getName());
		model.setClientAccount(orderAllocation.getClientAccount().toModelObject());
		if (!orderAllocation.getClientAccount().equals(orderAllocation.getHoldingAccount())) {
			// only set the Holding Account if Client and Holding Accounts are different (reduces object/message size)
			model.setHoldingAccount(orderAllocation.getHoldingAccount().toModelObject());
		}
		model.setOpenCloseType(orderAllocation.getOpenCloseType().getName());
		model.setSettlementCurrencyCode(orderAllocation.getSettlementCurrency().getTicker());
		model.setSettlementAmount(orderAllocation.getSettlementAmount().floatValue());
		model.setExchangeRateToSettlementCurrency(orderAllocation.getExchangeRateToSettlementCurrency().floatValue());
		model.setTradeNote(orderAllocation.getDescription());
		model.setOrderCreatorUser(getUserModelForSecurityUser(orderAllocation.getOrderCreatorUser()));
		model.setCreateDate(orderAllocation.getCreateDate().toInstant().atOffset(ZoneOffset.UTC));
		model.setTraderUser(getUserModelForSecurityUser(orderAllocation.getTraderUser()));
		model.setTradeDate(DateUtils.asLocalDate(orderAllocation.getTradeDate()));
		model.setSettlementDate(DateUtils.asLocalDate(orderAllocation.getSettlementDate()));
		model.setOrderSource(orderAllocation.getOrderSource() == null ? null : orderAllocation.getOrderSource().getName());
		model.setOrderSourceId(orderAllocation.getExternalOrderIdentifier());
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Order Group Model Converter                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderGroupModel convertOrderGroupToOrderGroupModel(OrderGroup orderGroup) {
		OrderGroupModel groupModel = new OrderGroupModel();
		groupModel.setId(orderGroup.getId());
		groupModel.setGroupType(orderGroup.getGroupType().getName());
		groupModel.setGroupFkFieldId(orderGroup.getGroupFkFieldId());
		groupModel.setOrderCreatorUser(getUserModelForSecurityUser(orderGroup.getOrderCreatorUser()));
		groupModel.setTradeDate(DateUtils.asLocalDate(orderGroup.getTradeDate()));
		groupModel.setNote(orderGroup.getNote());

		List<BaseOrderAllocationModel> orderAllocationModelList = orderGroup.getOrderAllocationList().stream()
				.map(this::convertOrderAllocationToOrderAllocationModel)
				.collect(Collectors.toList());
		groupModel.setOrderAllocationList(orderAllocationModelList);
		return groupModel;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private UserModel getUserModelForSecurityUser(SecurityUser securityUser) {
		if (securityUser == null) {
			return null;
		}
		UserModel userModel = new UserModel();
		userModel.setUserName(securityUser.getUserName());
		userModel.setDisplayName(securityUser.getDisplayName());
		return userModel;
	}
}
