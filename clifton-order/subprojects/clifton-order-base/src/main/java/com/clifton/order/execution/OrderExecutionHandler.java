package com.clifton.order.execution;

import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.trade.OrderTrade;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author manderson
 */
public interface OrderExecutionHandler {


	/**
	 * Validates the execution price (average unit price) on the placement is within give percent of latest available
	 * throws a UserIgnorableValidation exception if outside of threshold (or no latest available to validate against) as in some cases the User can choose to bypass and continue
	 * If percentThreshold is null, that is considered NO validation
	 */
	public void validateOrderExecutionPrice(OrderPlacement orderPlacement, BigDecimal executionPrice, BigDecimal percentThreshold);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Ability to preview a list of OrderPlacementAllocations for a Placement and expected quantity filled.
	 */
	public List<OrderPlacementAllocation> previewOrderPlacementAllocationListForPlacement(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, BigDecimal quantityFilled);


	/**
	 * Returns the calculated list of OrderPlacementAllocations for the given placement.
	 * The OrderAllocations are passed in to avoid having to retrieve again
	 * Existing list would be included when applying amendments post execution
	 */
	public List<OrderPlacementAllocation> calculateOrderPlacementAllocationListForPlacement(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, List<OrderPlacementAllocation> existingList);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the calculated list of OrderTrades for the given placement.
	 * The OrderPlacementAllocations are passed in to avoid having to retrieve again
	 * Existing list would be included when applying amendments post execution
	 */
	public List<OrderTrade> calculateOrderTradeListForPlacement(OrderPlacement orderPlacement, List<OrderPlacementAllocation> placementAllocationList, List<OrderTrade> existingList);
}
