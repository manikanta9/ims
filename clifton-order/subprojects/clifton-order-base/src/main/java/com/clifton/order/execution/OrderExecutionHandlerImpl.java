package com.clifton.order.execution;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import com.clifton.exchangerate.api.client.model.ExchangeRateData;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.shared.OrderSharedUtilHandler;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.order.trade.OrderTrade;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderExecutionHandlerImpl implements OrderExecutionHandler {

	private OrderExecutionService orderExecutionService;

	private OrderSharedMarketDataService orderSharedMarketDataService;

	private OrderSharedUtilHandler orderSharedUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateOrderExecutionPrice(OrderPlacement orderPlacement, BigDecimal executionPrice, BigDecimal percentThreshold) {
		if (percentThreshold != null) {
			if (orderPlacement.getOrderBlock().getSecurity().isCurrency()) {
				ExchangeRateData latestAvailableRate = getOrderSharedMarketDataService().getOrderMarketDataExchangeRateFlexible(orderPlacement.getOrderBlock().getSecurity().getTicker(), orderPlacement.getOrderBlock().getSettlementCurrency().getTicker(), orderPlacement.getOrderBlock().getTradeDate(), false);
				if (latestAvailableRate == null) {
					throw new UserIgnorableValidationException("Placement [" + orderPlacement.getId() + ": " + orderPlacement.getLabelShort() + "] Unable to validate execution price of " + CoreMathUtils.formatNumberDecimal(executionPrice) + ".  Unable to locate a recent value to compare to.");
				}
				// Round to 4 decimals so it's easier to relay information to user
				BigDecimal percentChange = MathUtils.round(MathUtils.getPercentChange(latestAvailableRate.getExchangeRate(), executionPrice, true), 4);
				if (MathUtils.isGreaterThan(MathUtils.abs(percentChange), percentThreshold)) {
					throw new UserIgnorableValidationException("Execution Price is " + CoreMathUtils.formatNumberDecimal(percentChange) + "% different from latest available of " + CoreMathUtils.formatNumberDecimal(latestAvailableRate.getExchangeRate()) + " which is over allowed threshold of +/- " + CoreMathUtils.formatNumberDecimal(percentThreshold) + "%.");
				}
			}
			else {
				// NOTE: WILL NEED TO ADD PRICE LOOK UP HERE ONCE WE HAVE A USE CASE FOR IT AND SOMEWHERE TO RETRIEVE THAT DATA
				throw new UserIgnorableValidationException(" Placement [" + orderPlacement.getId() + ": " + orderPlacement.getLabelShort() + "] Unable to validate execution price of " + CoreMathUtils.formatNumberDecimal(executionPrice) + ".  Unable to locate a recent value to compare to.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderPlacementAllocation> previewOrderPlacementAllocationListForPlacement(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, BigDecimal quantityFilled) {
		return calculateOrderPlacementAllocationListForPlacementImpl(orderPlacement, orderPlacement.getQuantityIntended(), orderAllocationList, null);
	}


	@Override
	public List<OrderPlacementAllocation> calculateOrderPlacementAllocationListForPlacement(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, List<OrderPlacementAllocation> existingList) {
		return calculateOrderPlacementAllocationListForPlacementImpl(orderPlacement, orderPlacement.getQuantityFilled(), orderAllocationList, existingList);
	}


	private List<OrderPlacementAllocation> calculateOrderPlacementAllocationListForPlacementImpl(OrderPlacement orderPlacement, BigDecimal quantityFilled, List<OrderAllocation> orderAllocationList, List<OrderPlacementAllocation> existingList) {
		Map<Long, OrderPlacementAllocation> orderAllocationPlacementAllocationMap = BeanUtils.getBeanMap(existingList, orderPlacementAllocation -> orderPlacementAllocation.getOrderAllocation().getId());

		List<OrderPlacementAllocation> placementAllocationList = new ArrayList<>();
		boolean currency = orderPlacement.getOrderBlock().getSecurity().isCurrency();
		boolean singlePlacement = MathUtils.isEqual(orderPlacement.getQuantityIntended(), orderPlacement.getOrderBlock().getQuantityIntended());
		boolean fullyFilled = MathUtils.isEqual(quantityFilled, orderPlacement.getQuantityIntended());
		BigDecimal totalIntended = CoreMathUtils.sumProperty(orderAllocationList, orderAllocation -> currency ? orderAllocation.getAccountingNotional() : orderAllocation.getQuantityIntended());

		// Note this has not been vetted for partial fills or multiple placements for a block order, however will attempt to apply prorated
		for (OrderAllocation orderAllocation : orderAllocationList) {
			OrderPlacementAllocation placementAllocation = orderAllocationPlacementAllocationMap.getOrDefault(orderAllocation.getId(), new OrderPlacementAllocation());
			placementAllocation.setOrderPlacement(orderPlacement);
			placementAllocation.setOrderAllocation(orderAllocation);
			placementAllocation.setPrice(orderPlacement.getAverageUnitPrice());
			placementAllocation.setQuantity(calculateOrderPlacementAllocationQuantity(currency ? orderAllocation.getAccountingNotional() : orderAllocation.getQuantityIntended(), singlePlacement, fullyFilled, totalIntended, quantityFilled));
			placementAllocationList.add(placementAllocation);
		}
		adjustForRoundingErrors(placementAllocationList);
		return placementAllocationList;
	}


	private BigDecimal calculateOrderPlacementAllocationQuantity(BigDecimal intendedAmount, boolean singlePlacement, boolean fullyFilled, BigDecimal totalIntended, BigDecimal totalFilled) {
		if (singlePlacement && fullyFilled) {
			return intendedAmount;
		}
		throw new IllegalStateException("Not supported yet for partial fills or multiple placements");
	}


	private void adjustForRoundingErrors(List<OrderPlacementAllocation> orderPlacementAllocationList) {
		// DO NOTHING - NOT DOING ANY PARTIAL FILLS OR MULTIPLE PLACEMENTS YET
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderTrade> calculateOrderTradeListForPlacement(OrderPlacement orderPlacement, List<OrderPlacementAllocation> placementAllocationList, List<OrderTrade> existingList) {
		List<OrderTrade> tradeList = new ArrayList<>();
		AssertUtils.assertNotNull(orderPlacement.getExecutingBrokerCompany(), "Cannot complete the placement " + orderPlacement.getLabelShort() + ", with id " + orderPlacement.getId() + ", because the executing broker is missing.");

		Map<String, OrderTrade> existingTradeMap = BeanUtils.getBeanMap(existingList, orderTrade -> orderTrade.getOrderAllocationIdentifier() + "_" + orderTrade.getExecutingBrokerCompany().getId());

		Map<String, List<OrderPlacementAllocation>> placementAllocationListMap = BeanUtils.getBeansMap(placementAllocationList, orderPlacementAllocation -> orderPlacementAllocation.getOrderAllocation().getId() + "_" + orderPlacementAllocation.getOrderPlacement().getExecutingBrokerCompany().getId());
		for (Map.Entry<String, List<OrderPlacementAllocation>> placementAllocationEntryKey : placementAllocationListMap.entrySet()) {
			List<OrderPlacementAllocation> tradePlacementAllocationList = placementAllocationEntryKey.getValue();
			OrderPlacementAllocation firstPlacementAllocation = tradePlacementAllocationList.get(0);
			OrderAllocation orderAllocation = firstPlacementAllocation.getOrderAllocation();
			OrderTrade orderTrade = existingTradeMap.getOrDefault(placementAllocationEntryKey.getKey(), new OrderTrade());
			orderTrade.setOrderAllocationIdentifier(orderAllocation.getId());
			orderTrade.setClientAccount(orderAllocation.getClientAccount());
			orderTrade.setHoldingAccount(orderAllocation.getHoldingAccount());
			orderTrade.setOrderType(orderAllocation.getOrderType());
			orderTrade.setSecurity(orderAllocation.getSecurity());
			orderTrade.setOpenCloseType(orderAllocation.getOpenCloseType());
			orderTrade.setExecutingBrokerCompany(firstPlacementAllocation.getOrderPlacement().getExecutingBrokerCompany());
			orderTrade.setTradeDate(orderAllocation.getTradeDate());
			orderTrade.setSettlementDate(orderAllocation.getSettlementDate());
			orderTrade.setSettlementCurrency(orderAllocation.getSettlementCurrency());

			if (orderAllocation.getSecurity().isCurrency()) {
				orderTrade.setAccountingNotional(CoreMathUtils.sumProperty(tradePlacementAllocationList, OrderPlacementAllocation::getQuantity));
				orderTrade.setExchangeRateToSettlementCurrency(MathUtils.divide(CoreMathUtils.sumProperty(tradePlacementAllocationList, orderPlacementAllocation -> MathUtils.multiply(orderPlacementAllocation.getQuantity(), orderPlacementAllocation.getPrice())), orderTrade.getAccountingNotional()));
			}
			else {
				orderTrade.setQuantity(CoreMathUtils.sumProperty(tradePlacementAllocationList, OrderPlacementAllocation::getQuantity));
				orderTrade.setPrice(MathUtils.divide(CoreMathUtils.sumProperty(tradePlacementAllocationList, orderPlacementAllocation -> MathUtils.multiply(orderPlacementAllocation.getQuantity(), orderPlacementAllocation.getPrice())), orderTrade.getQuantity()));
				orderTrade.setExchangeRateToSettlementCurrency(orderAllocation.getExchangeRateToSettlementCurrency()); // Recalc or just pull it off the order allocation?
				orderTrade.setAccountingNotional(MathUtils.multiply(orderTrade.getQuantity(), orderTrade.getPrice()));
			}

			CurrencyConventionData currencyConventionData = getOrderSharedMarketDataService().getOrderMarketDataCurrencyConvention(orderAllocation.getLocalCurrencyCode(), orderTrade.getSettlementCurrency().getTicker());
			orderTrade.setSettlementAmount(getOrderSharedUtilHandler().roundCurrencyAmount(orderTrade.getSettlementCurrency(), BooleanUtils.isTrue(currencyConventionData.getMultiplyByExchangeRate()) ? MathUtils.multiply(orderTrade.getAccountingNotional(), orderTrade.getExchangeRateToSettlementCurrency()) : MathUtils.divide(orderTrade.getAccountingNotional(), orderTrade.getExchangeRateToSettlementCurrency())));
			// REVIEW OTHER PROPERTIES WHEN DOING MORE THAN JUST CURRENCY ORDERS
			tradeList.add(orderTrade);
		}
		return tradeList;
	}
}
