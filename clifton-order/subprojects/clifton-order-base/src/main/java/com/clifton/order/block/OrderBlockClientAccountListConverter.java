package com.clifton.order.block;

import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.util.StringUtils;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderBlockClientAccountListConverter</code> is used to display the list of client accounts short label for a particular block order
 * <p>
 * Used for Excel Exports of Placements or Blocks to include this additional information that isn't displayed on the grid.
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderBlockClientAccountListConverter implements ReversableConverter<String, Long> {

	private OrderAllocationService orderAllocationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Long convert(String from) {
		// Not used, only convert from a block order to a list of client accounts for exporting.
		return null;
	}


	@Override
	public String reverseConvert(Long orderBlockId) {
		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderBlockIdentifier(orderBlockId);
		return StringUtils.collectionToCommaDelimitedString(getOrderAllocationService().getOrderAllocationList(searchForm), orderAllocation -> orderAllocation.getClientAccount().getLabelShort());
	}
}
