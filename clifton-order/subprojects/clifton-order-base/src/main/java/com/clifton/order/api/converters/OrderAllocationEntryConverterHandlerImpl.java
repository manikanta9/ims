package com.clifton.order.api.converters;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.api.server.model.BaseOrderAllocationEntryModel;
import com.clifton.order.api.server.model.CurrencyOrderAllocationEntryModel;
import com.clifton.order.api.server.model.OrderGroupEntryModel;
import com.clifton.order.api.server.model.StockOrderAllocationEntryModel;
import com.clifton.order.group.OrderGroup;
import com.clifton.order.group.OrderGroupService;
import com.clifton.order.setup.OrderGroupType;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.OrderSource;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Component
public class OrderAllocationEntryConverterHandlerImpl implements OrderAllocationEntryConverterHandler {

	private OrderGroupService orderGroupService;

	private OrderSharedService orderSharedService;

	private OrderSetupService orderSetupService;

	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////            OrderAllocationEntryModel Converters            ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderAllocation convertCurrencyOrderAllocationEntryToOrderAllocation(CurrencyOrderAllocationEntryModel entryModel) {
		OrderAllocation orderAllocation = convertBaseOrderAllocationEntryToOrderAllocation(entryModel);
		orderAllocation.setSecurity(getOrderSharedService().getOrderSecurityForCurrencyTicker(entryModel.getGivenCurrencyCode()));
		orderAllocation.setAccountingNotional(entryModel.getGivenAmount());
		orderAllocation.setExchangeRateToSettlementCurrency(MathUtils.getNumberAsBigDecimal(entryModel.getExchangeRateToSettlementCurrency()));
		orderAllocation.setSettlementCurrency(getOrderSharedService().getOrderSecurityForCurrencyTicker(entryModel.getSettlementCurrencyCode()));
		return orderAllocation;
	}


	@Override
	public OrderAllocation convertStockOrderAllocationEntryToOrderAllocation(StockOrderAllocationEntryModel entryModel) {
		OrderAllocation orderAllocation = convertBaseOrderAllocationEntryToOrderAllocation(entryModel);
		orderAllocation.setSecurity(getOrderSharedService().getOrderSecurityForRetriever(entryModel.getSecurity(), false));
		orderAllocation.setQuantityIntended(entryModel.getQuantityIntended());
		orderAllocation.setSettlementCurrency(getOrderSharedService().getOrderSecurityForCurrencyTicker(orderAllocation.getSecurity().getCurrencyCode()));
		return orderAllocation;
	}


	private OrderAllocation convertOrderAllocationEntryToOrderAllocation(BaseOrderAllocationEntryModel entryModel) {
		if (entryModel instanceof CurrencyOrderAllocationEntryModel) {
			return convertCurrencyOrderAllocationEntryToOrderAllocation((CurrencyOrderAllocationEntryModel) entryModel);
		}
		if (entryModel instanceof StockOrderAllocationEntryModel) {
			return convertStockOrderAllocationEntryToOrderAllocation((StockOrderAllocationEntryModel) entryModel);
		}
		throw new ValidationException("Unable to determine conversion for entry model " + entryModel.getClass());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderAllocation convertBaseOrderAllocationEntryToOrderAllocation(BaseOrderAllocationEntryModel entryModel) {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setClientAccount(getOrderSharedService().getOrderAccountForClientAccountRetriever(entryModel.getClientAccount()));
		ValidationUtils.assertNotNull(orderAllocation.getClientAccount(), "Client Account is Required");
		if (orderAllocation.getClientAccount().isHoldingAccount() && entryModel.getHoldingAccount() == null) {
			orderAllocation.setHoldingAccount(orderAllocation.getClientAccount());
		}
		else {
			orderAllocation.setHoldingAccount(getOrderSharedService().getOrderAccountForHoldingAccountRetriever(entryModel.getHoldingAccount()));
		}
		ValidationUtils.assertNotNull(orderAllocation.getHoldingAccount(), "Holding Account is Required");
		orderAllocation.setOpenCloseType(getOrderSetupService().getOrderOpenCloseTypeByName(entryModel.getOpenCloseType()));
		orderAllocation.setTradeDate(DateUtils.asUtilDate(entryModel.getTradeDate()));
		orderAllocation.setSettlementDate(DateUtils.asUtilDate(entryModel.getSettlementDate()));
		orderAllocation.setDescription(entryModel.getTradeNote());
		if (entryModel.getOrderCreatorUserName() != null) {
			orderAllocation.setOrderCreatorUser(getSecurityUserService().getSecurityUserByName(entryModel.getOrderCreatorUserName()));
		}
		if (entryModel.getOrderSource() != null) {
			orderAllocation.setOrderSource(getOrderSetupService().getOrderSourceByName(entryModel.getOrderSource()));
		}
		orderAllocation.setExternalOrderIdentifier(entryModel.getOrderSourceId());
		return orderAllocation;
	}


	@Override
	public OrderGroup convertOrderGroupEntryToOrderGroup(OrderGroupEntryModel groupEntryModel) {
		if (groupEntryModel == null || CollectionUtils.isEmpty(groupEntryModel.getOrderAllocationList())) {
			throw new ValidationException("Missing Order Allocations for the group entry. At least one order allocation is required.");
		}
		List<OrderAllocation> orderAllocationList = groupEntryModel.getOrderAllocationList().stream()
				.map(this::convertOrderAllocationEntryToOrderAllocation)
				.collect(Collectors.toList());

		OrderGroup orderGroup = new OrderGroup();
		if (StringUtils.isEmpty(groupEntryModel.getGroupType())) {
			orderGroup.setGroupType(getOrderSetupService().getOrderGroupTypeByName(OrderGroupType.ORDER_GROUP_TYPE_MANUAL_NAME));
		}
		else {
			orderGroup.setGroupType(getOrderSetupService().getOrderGroupTypeByName(groupEntryModel.getGroupType()));
		}
		orderGroup.setGroupFkFieldId(groupEntryModel.getGroupFkFieldId());
		if (groupEntryModel.getOrderCreatorUserName() != null) {
			orderGroup.setOrderCreatorUser(getSecurityUserService().getSecurityUserByName(groupEntryModel.getOrderCreatorUserName()));
		}
		orderGroup.setTradeDate(DateUtils.clearTime(new Date()));

		final OrderSource orderSource = (!StringUtils.isEmpty(groupEntryModel.getOrderSource())) ? getOrderSetupService().getOrderSourceByName(groupEntryModel.getOrderSource()) : null;

		orderAllocationList.stream().forEach(orderAllocation -> {
			if (orderAllocation.getTradeDate() == null) {
				orderAllocation.setTradeDate(orderGroup.getTradeDate());
			}
			if (orderAllocation.getOrderCreatorUser() == null) {
				orderAllocation.setOrderCreatorUser(orderGroup.getOrderCreatorUser());
			}
			if (orderSource != null && orderAllocation.getOrderSource() == null) {
				orderAllocation.setOrderSource(orderSource);
			}
		});

		orderGroup.setOrderAllocationList(orderAllocationList);

		return orderGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderGroupService getOrderGroupService() {
		return this.orderGroupService;
	}


	public void setOrderGroupService(OrderGroupService orderGroupService) {
		this.orderGroupService = orderGroupService;
	}


	public OrderSharedService getOrderSharedService() {
		return this.orderSharedService;
	}


	public void setOrderSharedService(OrderSharedService orderSharedService) {
		this.orderSharedService = orderSharedService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public OrderSetupService getOrderSetupService() {
		return this.orderSetupService;
	}


	public void setOrderSetupService(OrderSetupService orderSetupService) {
		this.orderSetupService = orderSetupService;
	}
}
