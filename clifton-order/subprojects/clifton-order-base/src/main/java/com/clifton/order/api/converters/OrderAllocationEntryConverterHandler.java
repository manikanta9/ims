package com.clifton.order.api.converters;

import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.api.server.model.CurrencyOrderAllocationEntryModel;
import com.clifton.order.api.server.model.OrderGroupEntryModel;
import com.clifton.order.api.server.model.StockOrderAllocationEntryModel;
import com.clifton.order.group.OrderGroup;


/**
 * @author manderson
 */
public interface OrderAllocationEntryConverterHandler {

	////////////////////////////////////////////////////////////////////////////
	////////            OrderAllocationEntryModel Converters            ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAllocation convertCurrencyOrderAllocationEntryToOrderAllocation(CurrencyOrderAllocationEntryModel entryModel);


	public OrderAllocation convertStockOrderAllocationEntryToOrderAllocation(StockOrderAllocationEntryModel entryModel);

	////////////////////////////////////////////////////////////////////////////
	////////            Order Group Entry Converters                    ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderGroup convertOrderGroupEntryToOrderGroup(OrderGroupEntryModel groupEntryModel);
}
