package com.clifton.order.allocation.upload;


import com.clifton.order.allocation.OrderAllocation;


/**
 * The <code>OrderAllocationUploadDataPopulator</code> is used by simple order allocation uploads to populate bean properties with real objects based on the properties populated
 * Example: Uploads usually only require populating the Client Account Number or short name or Security Ticker, we can often try to determine the actual security based on ticker (and order type if not unique), or try to find the correct holding account if not specified
 *
 * @author manderson
 */
public interface OrderAllocationUploadDataPopulator {


	/**
	 * Based on the not fully populated OrderAllocation object and upload properties, will populate missing data on the OrderAllocation
	 * i.e. Client Account, Holding Account, Security, etc.
	 */
	public void populateOrderAllocation(OrderAllocation orderAllocation, OrderAllocationUploadCommand uploadCommand);
}
