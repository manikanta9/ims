package com.clifton.order.execution.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.block.OrderBlockService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.event.OrderPlacementAllocationCompleteEvent;
import com.clifton.order.execution.event.OrderPlacementFilledEvent;
import com.clifton.order.setup.OrderAllocationStatus;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


/**
 * The <code>OrderPlacementObserver</code> observes changes to {@link OrderPlacement} entities to update related objects.
 * For example, creating a new placement updates the placed quantity on an {@link com.clifton.order.block.OrderBlock}
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderPlacementObserver extends SelfRegisteringDaoObserver<OrderPlacement> {

	private OrderBlockService orderBlockService;
	private OrderSetupService orderSetupService;

	private EventHandler eventHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderPlacement> dao, DaoEventTypes event, OrderPlacement bean) {
		getOriginalBean(dao, bean);

		if (event.isInsert() || OrderDestinationUtils.isResetPlacementAllowed()) {
			if (bean.getExecutionStatus() == null) {
				if (bean.getOrderDestination().getDestinationType().isBatched()) {
					bean.setExecutionStatus(getOrderSetupService().getOrderExecutionStatusByName(OrderExecutionStatus.READY_FOR_BATCHING));
				}
				else {
					bean.setExecutionStatus(getOrderSetupService().getOrderExecutionStatusByName(OrderExecutionStatus.DRAFT));
				}
			}
			if (bean.getAllocationStatus() == null) {
				bean.setAllocationStatus(getOrderSetupService().getOrderAllocationStatusByName(OrderAllocationStatus.ALLOCATION_INCOMPLETE));
			}
		}
		else if (event.isUpdate()) {
			if (bean.getExecutionStatus().isCanceled()) {
				bean.setQuantityIntended(BigDecimal.ZERO);
				bean.setQuantityFilled(BigDecimal.ZERO);
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<OrderPlacement> dao, DaoEventTypes event, OrderPlacement bean, Throwable e) {
		if (e == null) {
			OrderBlock orderBlock = bean.getOrderBlock();
			BigDecimal placedChange;
			BigDecimal fillChange;
			if (event.isDelete()) {
				placedChange = MathUtils.negate(bean.getQuantityIntended());
				fillChange = MathUtils.negate(bean.getQuantityFilled());
			}
			else {
				if (event.isInsert()) {
					placedChange = bean.getQuantityIntended();
					fillChange = bean.getQuantityFilled();
				}
				else {
					OrderPlacement originalBean = getOriginalBean(dao, bean);
					placedChange = MathUtils.subtract(bean.getQuantityIntended(), originalBean.getQuantityIntended());
					fillChange = MathUtils.subtract(bean.getQuantityFilled(), originalBean.getQuantityFilled());
				}
			}
			if (!MathUtils.isNullOrZero(placedChange) || !MathUtils.isNullOrZero(fillChange)) {
				orderBlock = getOrderBlockService().getOrderBlock(orderBlock.getId(), false);
				orderBlock.setQuantityPlaced(MathUtils.add(orderBlock.getQuantityPlaced(), placedChange));
				orderBlock.setQuantityFilled(MathUtils.add(orderBlock.getQuantityFilled(), fillChange));
				getOrderBlockService().saveOrderBlock(orderBlock);
			}
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<OrderPlacement> dao, DaoEventTypes event, OrderPlacement bean, Throwable e) {
		if (e == null && event.isUpdate()) {
			OrderPlacement originalBean = getOriginalBean(dao, bean);
			getDaoEventContext().setOriginalBean(bean);
			if (!originalBean.getExecutionStatus().isFillComplete() && bean.getExecutionStatus().isFillComplete()) {
				getEventHandler().raiseEvent(new OrderPlacementFilledEvent(bean.getId()));
			}

			if (!originalBean.getAllocationStatus().isAllocationComplete() && bean.getAllocationStatus().isAllocationComplete()) {
				getEventHandler().raiseEvent(new OrderPlacementAllocationCompleteEvent(bean.getId()));
			}
		}
	}
}
