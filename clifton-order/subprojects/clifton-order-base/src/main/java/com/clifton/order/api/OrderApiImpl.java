package com.clifton.order.api;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.model.ModelAwareUtils;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.api.converters.OrderAllocationConverterHandler;
import com.clifton.order.api.server.OrdersApi;
import com.clifton.order.api.server.model.BaseOrderAllocationModel;
import com.clifton.order.api.server.model.CurrencyOrderAllocationPagingArrayListModel;
import com.clifton.order.api.server.model.DateComparisonConditionsModel;
import com.clifton.order.api.server.model.SecurityRetrieverModel;
import com.clifton.order.api.server.model.StockOrderAllocationPagingArrayListModel;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSharedService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;


/**
 * @author manderson
 */
@RestController
public class OrderApiImpl implements OrdersApi {

	private OrderAllocationService orderAllocationService;

	private OrderAllocationConverterHandler orderAllocationConverterHandler;

	private OrderSharedService orderSharedService;

	private OrderSetupService orderSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ResponseEntity<BaseOrderAllocationModel> getOrderAllocation(Long id) {
		OrderAllocation orderAllocation = getOrderAllocationService().getOrderAllocation(id);
		if (OrderType.ORDER_TYPE_CURRENCY.equalsIgnoreCase(orderAllocation.getOrderType().getName())) {
			return ResponseEntity.ok(getOrderAllocationConverterHandler().convertOrderAllocationToCurrencyOrderAllocation(orderAllocation));
		}
		return ResponseEntity.ok(getOrderAllocationConverterHandler().convertOrderAllocationToStockOrderAllocation(orderAllocation));
	}


	@SecureMethod(dtoClass = OrderAllocation.class)
	@Override
	public ResponseEntity<CurrencyOrderAllocationPagingArrayListModel> getCurrencyOrderAllocationList(Integer start, Integer pageSize, LocalDate tradeDate, DateComparisonConditionsModel tradeDateComparisonCondition, LocalDate settlementDate, DateComparisonConditionsModel settlementDateComparisonCondition, String givenCurrencyCode) {
		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderTypeId(getOrderSetupService().getOrderTypeByName(OrderType.ORDER_TYPE_CURRENCY).getId());
		searchForm.setLimit(pageSize);
		searchForm.setStart(start);
		if (tradeDate != null) {
			searchForm.addSearchRestriction("tradeDate", tradeDateComparisonCondition == null ? ComparisonConditions.EQUALS : ComparisonConditions.valueOf(tradeDateComparisonCondition.getValue()), DateUtils.asUtilDate(tradeDate));
		}
		if (settlementDate != null) {
			searchForm.addSearchRestriction("settlementDate", settlementDateComparisonCondition == null ? ComparisonConditions.EQUALS : ComparisonConditions.valueOf(settlementDateComparisonCondition.getValue()), DateUtils.asUtilDate(settlementDate));
		}

		if (!StringUtils.isEmpty(givenCurrencyCode)) {
			OrderSecurity orderSecurity = getOrderSharedService().getOrderSecurityForCurrencyTicker(givenCurrencyCode);
			if (orderSecurity != null) {
				searchForm.setSecurityId(orderSecurity.getId());
			}
		}

		List<OrderAllocation> orderAllocationList = getOrderAllocationService().getOrderAllocationList(searchForm);
		CurrencyOrderAllocationPagingArrayListModel pagingArrayListTemplateModel = new CurrencyOrderAllocationPagingArrayListModel();
		if (orderAllocationList instanceof PagingArrayList) {
			pagingArrayListTemplateModel.setFirstElementIndex(((PagingArrayList<OrderAllocation>) orderAllocationList).getFirstElementIndex());
			pagingArrayListTemplateModel.setPageSize(((PagingArrayList<OrderAllocation>) orderAllocationList).getPageSize());
			pagingArrayListTemplateModel.setTotalElementCount(((PagingArrayList<OrderAllocation>) orderAllocationList).getTotalElementCount());
		}
		else {
			pagingArrayListTemplateModel.setFirstElementIndex(searchForm.getStart());
			pagingArrayListTemplateModel.setPageSize(searchForm.getLimit());
			pagingArrayListTemplateModel.setTotalElementCount(CollectionUtils.getSize(orderAllocationList));
		}
		pagingArrayListTemplateModel.data(ModelAwareUtils.getModelObjectListUsingConverter(orderAllocationList, getOrderAllocationConverterHandler()::convertOrderAllocationToCurrencyOrderAllocation));
		return ResponseEntity.ok(Objects.requireNonNull(pagingArrayListTemplateModel));
	}


	@SecureMethod(dtoClass = OrderAllocation.class)
	@Override
	public ResponseEntity<StockOrderAllocationPagingArrayListModel> getStockOrderAllocationList(Integer start, Integer pageSize, LocalDate tradeDate, DateComparisonConditionsModel tradeDateComparisonCondition, LocalDate settlementDate, DateComparisonConditionsModel settlementDateComparisonCondition, SecurityRetrieverModel security) {
		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderTypeId(getOrderSetupService().getOrderTypeByName(OrderType.ORDER_TYPE_STOCKS).getId());
		searchForm.setLimit(pageSize);
		searchForm.setStart(start);
		if (tradeDate != null) {
			searchForm.addSearchRestriction("tradeDate", tradeDateComparisonCondition == null ? ComparisonConditions.EQUALS : ComparisonConditions.valueOf(tradeDateComparisonCondition.getValue()), DateUtils.asUtilDate(tradeDate));
		}
		if (settlementDate != null) {
			searchForm.addSearchRestriction("settlementDate", settlementDateComparisonCondition == null ? ComparisonConditions.EQUALS : ComparisonConditions.valueOf(settlementDateComparisonCondition.getValue()), DateUtils.asUtilDate(settlementDate));
		}
		if (security != null) {
			OrderSecurity orderSecurity = getOrderSharedService().getOrderSecurityForRetriever(security, true);
			if (orderSecurity != null) {
				searchForm.setSecurityId(orderSecurity.getId());
			}
		}

		List<OrderAllocation> orderAllocationList = getOrderAllocationService().getOrderAllocationList(searchForm);
		StockOrderAllocationPagingArrayListModel pagingArrayListModel = new StockOrderAllocationPagingArrayListModel();
		if (orderAllocationList instanceof PagingArrayList) {
			pagingArrayListModel.setFirstElementIndex(((PagingArrayList<OrderAllocation>) orderAllocationList).getFirstElementIndex());
			pagingArrayListModel.setPageSize(((PagingArrayList<OrderAllocation>) orderAllocationList).getPageSize());
			pagingArrayListModel.setTotalElementCount(((PagingArrayList<OrderAllocation>) orderAllocationList).getTotalElementCount());
		}
		else {
			pagingArrayListModel.setFirstElementIndex(searchForm.getStart());
			pagingArrayListModel.setPageSize(searchForm.getLimit());
			pagingArrayListModel.setTotalElementCount(CollectionUtils.getSize(orderAllocationList));
		}
		pagingArrayListModel.data(ModelAwareUtils.getModelObjectListUsingConverter(orderAllocationList, getOrderAllocationConverterHandler()::convertOrderAllocationToStockOrderAllocation));

		return ResponseEntity.ok(Objects.requireNonNull(pagingArrayListModel));
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAllocationService getOrderAllocationService() {
		return this.orderAllocationService;
	}


	public void setOrderAllocationService(OrderAllocationService orderAllocationService) {
		this.orderAllocationService = orderAllocationService;
	}


	public OrderAllocationConverterHandler getOrderAllocationConverterHandler() {
		return this.orderAllocationConverterHandler;
	}


	public void setOrderAllocationConverterHandler(OrderAllocationConverterHandler orderAllocationConverterHandler) {
		this.orderAllocationConverterHandler = orderAllocationConverterHandler;
	}


	public OrderSharedService getOrderSharedService() {
		return this.orderSharedService;
	}


	public void setOrderSharedService(OrderSharedService orderSharedService) {
		this.orderSharedService = orderSharedService;
	}


	public OrderSetupService getOrderSetupService() {
		return this.orderSetupService;
	}


	public void setOrderSetupService(OrderSetupService orderSetupService) {
		this.orderSetupService = orderSetupService;
	}
}
