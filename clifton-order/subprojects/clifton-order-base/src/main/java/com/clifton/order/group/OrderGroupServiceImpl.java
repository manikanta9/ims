package com.clifton.order.group;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusWithCounts;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.group.search.OrderGroupSearchForm;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderGroupServiceImpl implements OrderGroupService {

	private AdvancedUpdatableDAO<OrderGroup, Criteria> orderGroupDAO;

	private OrderAllocationService orderAllocationService;

	private WorkflowDefinitionService workflowDefinitionService;

	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////                Order Group Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderGroup getOrderGroup(long id, boolean populateAllocations) {
		OrderGroup group = getOrderGroupDAO().findByPrimaryKey(id);
		if (group != null && populateAllocations) {
			// Note: Should probably cache this list?
			OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
			searchForm.setOrderGroupId(id);
			group.setOrderAllocationList(getOrderAllocationService().getOrderAllocationList(searchForm));
		}
		return group;
	}


	@Override
	public List<OrderGroup> getOrderGroupList(OrderGroupSearchForm searchForm) {
		return getOrderGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public OrderGroup saveOrderGroup(OrderGroup orderGroup) {
		List<OrderAllocation> orderAllocationList = orderGroup.getOrderAllocationList();
		int size = CollectionUtils.getSize(orderAllocationList);
		ValidationUtils.assertFalse(size == 0, "Cannot create an order group without any order allocations.");

		// List to track saved OrderAllocations. The returned OrderGroup will be updated to this List so the OrderAllocations are in their persisted state.
		List<OrderAllocation> persistedOrderAllocationList = new ArrayList<>(size);

		// One order allocation in the group - don't create the group, just the trade (unless the group type explicitly allows one trade in the group)
		ValidationUtils.assertNotNull(orderGroup.getGroupType(), "Order Group Type is missing from the Order Group.");
		if (size == 1 && !orderGroup.getGroupType().isOneOrderAllocationAllowed()) {
			// Clear the Order Group and save the Order Allocation By Itself
			OrderAllocation orderAllocation = orderAllocationList.get(0);
			orderAllocation.setOrderGroup(null);
			persistedOrderAllocationList.add(getOrderAllocationService().saveOrderAllocation(orderAllocation));
			orderGroup.setOrderAllocationList(persistedOrderAllocationList);
			return orderGroup;
		}

		// Create the group and save all order allocations
		// If trade date is missing on the group - set it to the max
		if (orderGroup.getTradeDate() == null) {
			orderGroup.setTradeDate(CollectionUtils.getFirstElementStrict(BeanUtils.sortWithFunction(orderAllocationList, OrderAllocation::getTradeDate, false)).getTradeDate());
		}

		OrderGroup resultOrderGroup = orderGroup;
		// Insert new Groups Only - don't change existing
		if (orderGroup.isNewBean()) {
			resultOrderGroup = getOrderGroupDAO().save(orderGroup);
		}

		persistedOrderAllocationList.addAll(saveOrderGroupOrderAllocationList(resultOrderGroup, orderAllocationList));
		resultOrderGroup.setOrderAllocationList(persistedOrderAllocationList);
		return resultOrderGroup;
	}


	/**
	 * Save order allocations for an order group
	 *
	 * @param orderGroup          the order group
	 * @param orderAllocationList the list of order allocations  that are part of the order group
	 * @return the list of persisted order allocations
	 */
	@Transactional // open transaction if not present to improve performance and group updates
	protected List<OrderAllocation> saveOrderGroupOrderAllocationList(OrderGroup orderGroup, List<OrderAllocation> orderAllocationList) {
		List<OrderAllocation> persistedOrderAllocationList = new ArrayList<>(orderAllocationList.size());
		for (OrderAllocation orderAllocation : orderAllocationList) {
			orderAllocation.setOrderGroup(orderGroup);
			persistedOrderAllocationList.add(getOrderAllocationService().saveOrderAllocation(orderAllocation));
		}
		return persistedOrderAllocationList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Order Group Action Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status executeOrderAllocationValidateAction(Integer orderGroupId, Integer orderAllocationId) {
		if (orderGroupId == null && orderAllocationId == null) {
			return Status.ofMessage("No order allocations to transition.");
		}

		List<OrderAllocation> orderAllocationList;
		if (orderGroupId != null) {
			OrderGroup group = getOrderGroup(orderGroupId, true);
			orderAllocationList = group.getOrderAllocationList();
		}
		else {
			orderAllocationList = CollectionUtils.createList(getOrderAllocationService().getOrderAllocation(orderAllocationId));
		}
		return executeActionOnOrderAllocationList(OrderGroupActions.VALIDATE, orderAllocationList);
	}


	protected Status executeActionOnOrderAllocationList(OrderGroupActions action, List<OrderAllocation> orderAllocationList) {
		if (action == null) {
			throw new ValidationException("Unable to process action on Order Allocations.  No action set.");
		}

		String stateName = action.getStateName();
		if (StringUtils.isEmpty(stateName)) {
			return null; // No Workflow Transition for Fills only, or Delete Fills (i.e. action has no workflow state name)
		}

		// Put the order allocations in order by date, security, and price ascending
		orderAllocationList = sortOrderAllocationList(orderAllocationList);

		// Pull the First Order Allocation from the List to get the correct Workflow
		OrderAllocation orderAllocation = CollectionUtils.getFirstElementStrict(orderAllocationList);
		Workflow workflow = orderAllocation.getWorkflowState().getWorkflow();
		WorkflowState state = getWorkflowDefinitionService().getWorkflowStateByName(workflow.getId(), stateName);
		return executeOrderGroupWorkflowTransition(orderAllocationList, action, state);
	}


	private List<OrderAllocation> sortOrderAllocationList(List<OrderAllocation> orderAllocationList) {
		// Put the order allocations in order by date, security, and price ascending
		return BeanUtils.sortWithFunctions(orderAllocationList, CollectionUtils.createList(
						OrderAllocation::getTradeDate,
						orderAllocation -> orderAllocation.getSecurity().getId(),
						OrderAllocation::getAverageUnitPrice),
				CollectionUtils.createList(true, true, true));
	}


	private Status executeOrderGroupWorkflowTransition(List<OrderAllocation> orderAllocationList, OrderGroupActions action, WorkflowState state) {
		StatusWithCounts status = new StatusWithCounts();

		for (OrderAllocation orderAllocation : CollectionUtils.getIterable(orderAllocationList)) {
			try {
				// Is trade already in state
				if (orderAllocation.getWorkflowState().equals(state)) {
					status.incrementSkipCountWithMessage(orderAllocation.getLabel() + ": already in state " + state.getName());
				}
				else {
					// See if a valid transition for the orderAllocation, else skip it
					WorkflowTransition transition = getWorkflowTransitionService().getWorkflowTransitionForEntity(orderAllocation, orderAllocation.getWorkflowState(), state);
					if (transition != null) {
						getWorkflowTransitionService().executeWorkflowTransitionByTransition(OrderAllocation.TABLE_NAME, orderAllocation.getId(), transition.getId());
						status.incrementProcessCountWithMessage(orderAllocation.getLabel() + ": transitioned to state " + state.getName());
					}
					else {
						status.incrementSkipCountWithMessage(orderAllocation.getLabel() + ": transition not available to state " + state.getName());
					}
				}
			}
			catch (Exception e) {
				status.addError(orderAllocation.getLabel() + ": failed to transition to state " + state.getName() + ": " + e.getMessage());
				LogUtils.errorOrInfo(getClass(), "Error transitioning order allocation [" + orderAllocation.getLabel() + "] to Workflow State [" + state.getName() + "]: " + e.getMessage(), e);
			}
		}
		status.setMessage(status.getCountMessage("Processing Order Allocation(s) Workflow Transitions completed."));
		return status;
	}
}
