package com.clifton.order.allocation;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.grouping.AdvancedReadOnlyWithGroupingDAO;
import com.clifton.core.dataaccess.search.grouping.GroupedEntityResult;
import com.clifton.core.dataaccess.search.grouping.GroupedHierarchicalResult;
import com.clifton.core.dataaccess.search.grouping.SqlGroupedResultHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.exchangerate.api.client.model.ExchangeRateData;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.OrderType;
import com.clifton.order.setup.search.OrderTypeSearchForm;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.OrderSharedUtilHandler;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.price.api.client.model.PriceData;
import com.clifton.security.user.SecurityUserService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionCommand;
import com.clifton.workflow.transition.WorkflowTransitionService;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderAllocationServiceImpl implements OrderAllocationService {

	private AdvancedUpdatableDAO<OrderAllocation, Criteria> orderAllocationDAO;
	private SqlGroupedResultHandler sqlGroupedResultHandler;

	private OrderSharedMarketDataService orderSharedMarketDataService;
	private OrderSharedService orderSharedService;
	private OrderSharedUtilHandler orderSharedUtilHandler;
	private OrderSetupService orderSetupService;

	private RunnerHandler runnerHandler;

	private SecurityUserService securityUserService;

	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderAllocation getOrderAllocation(long id) {
		return getOrderAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderAllocation> getOrderAllocationList(OrderAllocationSearchForm searchForm) {
		return getOrderAllocationDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<GroupedHierarchicalResult> getOrderAllocationGroupedHierarchicalResultList(OrderAllocationSearchForm searchForm) {
		return getSqlGroupedResultHandler().getGroupedResult(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()), (AdvancedReadOnlyWithGroupingDAO<OrderAllocation>) getOrderAllocationDAO());
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<GroupedEntityResult<OrderAllocation>> getOrderAllocationGroupedEntityResultList(OrderAllocationSearchForm searchForm) {
		return getSqlGroupedResultHandler().getAggregateResult(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()), (AdvancedReadOnlyWithGroupingDAO<OrderAllocation>) getOrderAllocationDAO());
	}


	@Override
	public OrderAllocation saveOrderAllocation(OrderAllocation orderAllocation) {
		if (orderAllocation.getOrderCreatorUser() == null) {
			orderAllocation.setOrderCreatorUser(getSecurityUserService().getSecurityUserCurrent());
		}

		// set default values - this needs more = IMS had a lot of logic based on hierarchy flags, event types, etc.
		if (orderAllocation.getOrderType() == null) {
			orderAllocation.setOrderType(getOrderTypeForSecurity(orderAllocation.getSecurity()));
		}
		else {
			validateOrderTypeForSecurity(orderAllocation);
		}
		if (orderAllocation.getOrderType().isExecutingBrokerHoldingAccountIssuer()) {
			if (orderAllocation.getExecutingBrokerCompany() == null) {
				orderAllocation.setExecutingBrokerCompany(orderAllocation.getHoldingAccount().getIssuingCompany());
			}
			else if (orderAllocation.getExecutingBrokerCompany() != null && (!CompareUtils.isEqual(orderAllocation.getExecutingBrokerCompany(), orderAllocation.getHoldingAccount().getIssuingCompany()))) {
				throw new ValidationException("Invalid Executing Broker selected.  Order Type " + orderAllocation.getOrderType().getName() + " requires executing broker to be the holding account issuer " + orderAllocation.getHoldingAccount().getIssuingCompany().getName());
			}
		}
		if (orderAllocation.getCurrentFactor() == null) {
			orderAllocation.setCurrentFactor(BigDecimal.ONE);
		}
		if (orderAllocation.getNotionalMultiplier() == null) {
			orderAllocation.setNotionalMultiplier(BigDecimal.ONE);
		}
		if (orderAllocation.getSecurity().isCurrency()) {
			ValidationUtils.assertNotNull(orderAllocation.getAccountingNotional(), "Given Amount is required");
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(orderAllocation.getAccountingNotional(), BigDecimal.ZERO), "Given Amount must be greater than zero");
			orderAllocation.setAccountingNotional(getOrderSharedUtilHandler().roundCurrencyAmount(orderAllocation.getSecurity(), orderAllocation.getAccountingNotional()));

			if (orderAllocation.getExchangeRateToSettlementCurrency() == null) {
				ExchangeRateData exchangeRateData = getOrderSharedMarketDataService().getOrderMarketDataExchangeRateFlexible(orderAllocation.getSecurity().getTicker(), orderAllocation.getSettlementCurrency().getTicker(), orderAllocation.getTradeDate(), false);
				orderAllocation.setExchangeRateToSettlementCurrency(exchangeRateData == null ? BigDecimal.ONE : exchangeRateData.getExchangeRate());
			}
		}
		else {
			ValidationUtils.assertNotNull(orderAllocation.getQuantityIntended(), "Quantity Intended is required");
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(orderAllocation.getQuantityIntended(), BigDecimal.ZERO), "Quantity Intended must be greater than zero");
			if (orderAllocation.getExchangeRateToSettlementCurrency() == null) {
				if (!StringUtils.isEqual(orderAllocation.getSecurity().getCurrencyCode(), orderAllocation.getSettlementCurrency().getTicker())) {
					ExchangeRateData exchangeRateData = getOrderSharedMarketDataService().getOrderMarketDataExchangeRateFlexible(orderAllocation.getSecurity().getTicker(), orderAllocation.getSettlementCurrency().getTicker(), orderAllocation.getTradeDate(), false);
					orderAllocation.setExchangeRateToSettlementCurrency(exchangeRateData == null ? BigDecimal.ONE : exchangeRateData.getExchangeRate());
				}
				else {
					orderAllocation.setExchangeRateToSettlementCurrency(BigDecimal.ONE);
				}
			}

			// If both expected unit price and average unit price are missing expected to most recent so accounting notional will populate
			if (orderAllocation.getExpectedUnitPrice() == null && orderAllocation.getAverageUnitPrice() == null) {
				// Throws an exception if missing
				PriceData priceData = getOrderSharedMarketDataService().getOrderMarketDataPriceFlexible(orderAllocation.getSecurity().getId(), orderAllocation.getTradeDate(), true);
				orderAllocation.setExpectedUnitPrice(priceData.getBasePrice());
			}

			// Set Accounting Notional - For now we'll just use COALESCE(AVERAGE Price,EXPECTED PRICE) * Quantity
			OrderSecurity tradingCurrency = !StringUtils.isEqual(orderAllocation.getSecurity().getCurrencyCode(), orderAllocation.getSettlementCurrency().getTicker()) ? getOrderSharedService().getOrderSecurityForCurrencyTicker(orderAllocation.getSecurity().getCurrencyCode()) : orderAllocation.getSettlementCurrency();
			orderAllocation.setAccountingNotional(getOrderSharedUtilHandler().roundCurrencyAmount(tradingCurrency, MathUtils.multiply(orderAllocation.getQuantityIntended(), ObjectUtils.coalesce(orderAllocation.getAverageUnitPrice(), orderAllocation.getExpectedUnitPrice()))));
		}

		if (orderAllocation.getSettlementDate() == null) {
			orderAllocation.setSettlementDate(getOrderSharedUtilHandler().calculateSettlementDate(orderAllocation.getSecurity(), orderAllocation.getSettlementCurrency(), orderAllocation.getTradeDate()));
		}

		return getOrderAllocationDAO().save(orderAllocation);
	}


	@Override
	public OrderAllocation saveOrderAllocationWithRevisedDates(long id, Date tradeDate, Date settlementDate, int workflowTransitionId, boolean ignoreValidation) {
		OrderAllocation orderAllocation = getOrderAllocation(id);
		orderAllocation.setTradeDate(tradeDate);
		orderAllocation.setSettlementDate(settlementDate);

		getOrderSharedUtilHandler().validateTradeAndSettlementDateForSecurity(orderAllocation.getSecurity(), orderAllocation.getSettlementCurrency(), tradeDate, settlementDate, ignoreValidation);

		WorkflowTransition transition = getWorkflowTransitionService().getWorkflowTransition(workflowTransitionId);
		if (!transition.isSameStateTransition()) {
			orderAllocation.setWorkflowState(transition.getEndWorkflowState());
			orderAllocation.setWorkflowStatus(transition.getEndWorkflowState().getStatus());
			return getOrderAllocationDAO().save(orderAllocation);
		}
		return getWorkflowTransitionService().executeWorkflowAwareSaveWithSameStateTransition(orderAllocation, workflowTransitionId);
	}


	@Override
	public OrderAllocation saveOrderAllocationWithAmendments(OrderAllocationAmendmentCommand orderAllocationAmendmentCommand, int workflowTransitionId) {
		OrderAllocation orderAllocation = getOrderAllocation(orderAllocationAmendmentCommand.getOrderAllocationId());
		orderAllocationAmendmentCommand.copyAmendedValuesToOrderAllocation(orderAllocation);
		// Only use case is same state transition with system defined flag on
		return getWorkflowTransitionService().executeWorkflowAwareSaveWithSameStateTransitionSystemDefined(orderAllocation, workflowTransitionId);
	}


	@Transactional
	@Override
	public void saveOrderAllocationList(List<OrderAllocation> orderAllocationList, boolean bypassValidation) {
		if (bypassValidation) {
			getOrderAllocationDAO().saveList(orderAllocationList);
		}
		else {
			for (OrderAllocation orderAllocation : CollectionUtils.getIterable(orderAllocationList)) {
				saveOrderAllocation(orderAllocation);
			}
		}
	}


	@Override
	public void scheduleOrderAllocationWorkflowTransitionSystemDefined(String runId, Long[] orderAllocationIds, String nextStateName, int secondsDelay) {
		final Date scheduledDate = DateUtils.addSeconds(new Date(), secondsDelay);

		Runner runner = new AbstractStatusAwareRunner("ORDER-ALLOCATION-WORKFLOW-TRANSITION", runId, scheduledDate) {

			@Override
			public void run() {
				WorkflowTransitionCommand command = new WorkflowTransitionCommand();
				command.setAsynchronous(true);
				command.setAllowExecuteSystemDefined(true);
				command.setTableName(OrderAllocation.TABLE_NAME);
				command.setIds(orderAllocationIds);
				command.setAsynchronous(false);
				command.setNewWorkflowStateId(getWorkflowDefinitionService().getWorkflowStateByName(getWorkflowDefinitionService().getWorkflowByName(OrderAllocation.WORKFLOW_NAME).getId(), nextStateName).getId());
				getWorkflowTransitionService().executeWorkflowTransitionForCommand(command);
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////                 Helper Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////


	protected OrderType getOrderTypeForSecurity(OrderSecurity orderSecurity) {
		OrderTypeSearchForm searchForm = new OrderTypeSearchForm();
		searchForm.setSecurityTypeId(orderSecurity.getSecurityType().getId());
		List<OrderType> orderTypeList = getOrderSetupService().getOrderTypeList(searchForm);
		return CollectionUtils.getFirstElementStrict(orderTypeList, "Cannot find a valid order type for Security " + orderSecurity.getTicker(), "Found multiple valid order types for Security " + orderSecurity.getTicker() + ". Specific Order Type selection is required.");
	}


	protected void validateOrderTypeForSecurity(OrderAllocation orderAllocation) {
		ValidationUtils.assertEquals(orderAllocation.getOrderType().getSecurityType(), orderAllocation.getSecurity().getSecurityType(), "Order Type " + orderAllocation.getOrderType().getName() + " is not a valid selection for trading security " + orderAllocation.getSecurity().getTicker());
	}
}
