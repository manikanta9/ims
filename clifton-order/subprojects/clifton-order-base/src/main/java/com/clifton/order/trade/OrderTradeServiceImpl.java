package com.clifton.order.trade;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.order.trade.search.OrderTradeSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderTradeServiceImpl implements OrderTradeService {


	private AdvancedUpdatableDAO<OrderTrade, Criteria> orderTradeDAO;

	////////////////////////////////////////////////////////////////////////////
	////////            Order Trade Methods                             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderTrade getOrderTrade(long id) {
		return getOrderTradeDAO().findByPrimaryKey(id);
	}


	@Transactional
	@Override
	public void saveOrderTradeList(List<OrderTrade> orderTradeList) {
		getOrderTradeDAO().saveList(orderTradeList);
	}


	@Override
	public List<OrderTrade> getOrderTradeList(OrderTradeSearchForm searchForm) {
		return getOrderTradeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}
}
