package com.clifton.order.api;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.api.converters.OrderAllocationConverterHandler;
import com.clifton.order.api.converters.OrderAllocationEntryConverterHandler;
import com.clifton.order.api.server.OrderCreationApi;
import com.clifton.order.api.server.model.CurrencyOrderAllocationEntryModel;
import com.clifton.order.api.server.model.CurrencyOrderAllocationModel;
import com.clifton.order.api.server.model.OrderGroupEntryModel;
import com.clifton.order.api.server.model.OrderGroupModel;
import com.clifton.order.api.server.model.StockOrderAllocationEntryModel;
import com.clifton.order.api.server.model.StockOrderAllocationModel;
import lombok.Getter;
import lombok.Setter;
import com.clifton.order.group.OrderGroup;
import com.clifton.order.group.OrderGroupService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author manderson
 */
@RestController
@Getter
@Setter
public class OrderCreationApiImpl implements OrderCreationApi {

	private OrderAllocationService orderAllocationService;

	private OrderAllocationEntryConverterHandler orderAllocationEntryConverterHandler;

	private OrderAllocationConverterHandler orderAllocationConverterHandler;

	private OrderGroupService orderGroupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderAllocation.class)
	@Override
	public ResponseEntity<CurrencyOrderAllocationModel> saveCurrencyOrderAllocation(CurrencyOrderAllocationEntryModel currencyOrderAllocationEntryModel) {
		OrderAllocation result = getOrderAllocationService().saveOrderAllocation(getOrderAllocationEntryConverterHandler().convertCurrencyOrderAllocationEntryToOrderAllocation(currencyOrderAllocationEntryModel));
		return ResponseEntity.ok(getOrderAllocationConverterHandler().convertOrderAllocationToCurrencyOrderAllocation(result));
	}


	@SecureMethod(dtoClass = OrderAllocation.class)
	@Override
	public ResponseEntity<StockOrderAllocationModel> saveStockOrderAllocation(StockOrderAllocationEntryModel stockOrderAllocationEntryModel) {
		OrderAllocation result = getOrderAllocationService().saveOrderAllocation(getOrderAllocationEntryConverterHandler().convertStockOrderAllocationEntryToOrderAllocation(stockOrderAllocationEntryModel));
		return ResponseEntity.ok(getOrderAllocationConverterHandler().convertOrderAllocationToStockOrderAllocation(result));
	}


	@Override
	public ResponseEntity<OrderGroupModel> saveOrderGroupEntry(OrderGroupEntryModel orderGroupEntryModel) {
		OrderGroup orderGroup = getOrderAllocationEntryConverterHandler().convertOrderGroupEntryToOrderGroup(orderGroupEntryModel);
		orderGroup = getOrderGroupService().saveOrderGroup(orderGroup);
		return ResponseEntity.ok(getOrderAllocationConverterHandler().convertOrderGroupToOrderGroupModel(orderGroup));
	}


}
