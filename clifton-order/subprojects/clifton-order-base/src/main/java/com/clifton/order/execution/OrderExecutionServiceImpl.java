package com.clifton.order.execution;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.execution.event.OrderPlacementExecutionUpdateEvent;
import com.clifton.order.execution.search.OrderPlacementAllocationSearchForm;
import com.clifton.order.execution.search.OrderPlacementSearchForm;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.cache.OrderAllocationStatusIdsCache;
import com.clifton.order.setup.cache.OrderAllocationStatusIdsCacheImpl;
import com.clifton.order.setup.cache.OrderExecutionStatusIdsCache;
import com.clifton.order.setup.cache.OrderExecutionStatusIdsCacheImpl;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderExecutionServiceImpl implements OrderExecutionService {


	private static final BigDecimal MANUAL_EXECUTION_PRICE_THRESHOLD = BigDecimal.valueOf(5.0);
	private AdvancedUpdatableDAO<OrderPlacement, Criteria> orderPlacementDAO;
	private AdvancedUpdatableDAO<OrderPlacementAllocation, Criteria> orderPlacementAllocationDAO;
	private EventHandler eventHandler;
	private OrderAllocationStatusIdsCache orderAllocationStatusIdsCache;
	private OrderExecutionStatusIdsCache orderExecutionStatusIdsCache;
	private OrderAllocationService orderAllocationService;
	private OrderExecutionHandler orderExecutionHandler;
	private OrderSetupService orderSetupService;
	private OrderSharedMarketDataService orderSharedMarketDataService;

	////////////////////////////////////////////////////////////////////////////
	////////              Order Placement Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderPlacement getOrderPlacement(long id) {
		return getOrderPlacementDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderPlacement> getOrderPlacementList(OrderPlacementSearchForm searchForm) {
		return getOrderPlacementDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getClientOrHoldingAccountId() != null) {
					DetachedCriteria detachedCriteria = DetachedCriteria.forClass(OrderAllocation.class, "oa");
					detachedCriteria.setProjection(Projections.property("id"));
					detachedCriteria.add(Restrictions.or(Restrictions.eq("clientAccount.id", searchForm.getClientOrHoldingAccountId()), Restrictions.eq("holdingAccount.id", searchForm.getClientOrHoldingAccountId())));
					detachedCriteria.add(Restrictions.eqProperty("orderBlockIdentifier", criteria.getAlias() + ".orderBlock.id"));
					criteria.add(Subqueries.exists(detachedCriteria));
				}

				if (BooleanUtils.isTrue(searchForm.getActivePlacementsOnly())) {
					criteria.add(
							Restrictions.and(
									Restrictions.not(Restrictions.in("executionStatus.id", (Object[]) getOrderExecutionStatusIdsCache().getOrderExecutionStatusIds(OrderExecutionStatusIdsCacheImpl.OrderExecutionStatusIds.CANCELED_EXECUTION_STATUS))),
									Restrictions.or(
											Restrictions.in("executionStatus.id", (Object[]) getOrderExecutionStatusIdsCache().getOrderExecutionStatusIds(OrderExecutionStatusIdsCacheImpl.OrderExecutionStatusIds.ACTIVE_EXECUTION_STATUS)),
											Restrictions.in("allocationStatus.id", (Object[]) getOrderAllocationStatusIdsCache().getOrderAllocationStatusIds(OrderAllocationStatusIdsCacheImpl.OrderAllocationStatusIds.ALLOCATION_INCOMPLETE_STATUS))
									)
							)
					);
				}
			}
		});
	}


	@Override
	public OrderPlacement saveOrderPlacement(OrderPlacement bean) {
		return getOrderPlacementDAO().save(bean);
	}


	@Override
	public OrderPlacement saveOrderPlacementManualExecution(OrderPlacement bean, boolean ignoreValidation) {
		ValidationUtils.assertNotNull(bean.getAverageUnitPrice(), (bean.getOrderBlock().getSecurity().isCurrency() ? "Exchange Rate" : "Average price") + " is required.");
		ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getAverageUnitPrice(), BigDecimal.ZERO), (bean.getOrderBlock().getSecurity().isCurrency() ? "Exchange Rate" : "Average price") + " cannot be negative.");
		ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getQuantityFilled(), BigDecimal.ZERO), "Filled Amount cannot be negative.");
		ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getQuantityUnfilled(), BigDecimal.ZERO), "Selected filled amount is more than what was placed.  Please enter the fully filled amount of " + CoreMathUtils.formatNumberDecimal(bean.getQuantityIntended()));
		// Note: Currently only supporting fully filled for manual fills - will need to adjust this when we support asset classes with partial fills
		ValidationUtils.assertTrue(MathUtils.isEqual(bean.getQuantityUnfilled(), BigDecimal.ZERO), "Partial fills are not currently supported for manual fills.  Please enter the fully filled amount of " + CoreMathUtils.formatNumberDecimal(bean.getQuantityIntended()));
		ValidationUtils.assertNotNull(bean.getExecutingBrokerCompany(), "Executing Broker is required.");

		if (!ignoreValidation) {
			getOrderExecutionHandler().validateOrderExecutionPrice(bean, bean.getAverageUnitPrice(), MANUAL_EXECUTION_PRICE_THRESHOLD);
		}

		OrderPlacementExecutionUpdateEvent event = new OrderPlacementExecutionUpdateEvent(bean.getId());
		event.setQuantityFilled(bean.getQuantityFilled());
		if (MathUtils.isNullOrZero(bean.getQuantityUnfilled())) {
			event.setNewExecutionStatusName(OrderExecutionStatus.FILLED);
		}
		else if (!MathUtils.isNullOrZero(bean.getQuantityFilled())) {
			event.setNewExecutionStatusName(OrderExecutionStatus.PARTIALLY_FILLED);
		}
		event.setAverageUnitPrice(bean.getAverageUnitPrice());
		event.setExecutingBrokerCompany(bean.getExecutingBrokerCompany());
		// Raise the Event to Update the Placement
		getEventHandler().raiseEvent(event);
		return event.getResult();
	}


	@Transactional
	@Override
	public OrderPlacement saveOrderPlacementWithAllocations(OrderPlacement orderPlacement, List<OrderPlacementAllocation> orderPlacementAllocationList) {
		getOrderPlacementAllocationDAO().saveList(orderPlacementAllocationList);
		return saveOrderPlacement(orderPlacement);
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Order Placement Allocation Methods               ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderPlacementAllocation getOrderPlacementAllocation(long id) {
		return getOrderPlacementAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderPlacementAllocation> getOrderPlacementAllocationList(OrderPlacementAllocationSearchForm searchForm) {
		return getOrderPlacementAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<OrderPlacementAllocation> previewOrderPlacementAllocationListForPlacement(long orderPlacementId) {
		OrderPlacement orderPlacement = getOrderPlacement(orderPlacementId);
		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderBlockIdentifier(orderPlacement.getOrderBlock().getId());

		List<OrderAllocation> allocationList = getOrderAllocationService().getOrderAllocationList(searchForm);
		return getOrderExecutionHandler().previewOrderPlacementAllocationListForPlacement(orderPlacement, allocationList, orderPlacement.getQuantityIntended());
	}


	@Override
	public List<OrderPlacementAllocation> previewOrderPlacementAllocationListForBatch(OrderPlacementSearchForm searchForm) {
		ValidationUtils.assertNotNull(searchForm.getOrderBatchId(), "A specific batch id is required.");
		List<OrderPlacement> placementList = getOrderPlacementList(searchForm);
		List<OrderPlacementAllocation> placementAllocationList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(placementList)) {
			CollectionUtils.getStream(placementList).forEach(placement -> placementAllocationList.addAll(previewOrderPlacementAllocationListForPlacement(placement.getId())));
		}
		return placementAllocationList;
	}
}
