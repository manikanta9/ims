package com.clifton.order.block.observers;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.MathUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.block.OrderBlock;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>OrderBlockObserver</code> observes changes to {@link com.clifton.order.block.OrderBlock} entities.  When fully filled, the
 * List of {@link com.clifton.order.allocation.OrderAllocation} objects workflow state are updated.
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderBlockObserver extends SelfRegisteringDaoObserver<OrderBlock> {

	private OrderAllocationService orderAllocationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderBlock> dao, DaoEventTypes event, OrderBlock bean) {
		getOriginalBean(dao, bean);
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<OrderBlock> dao, DaoEventTypes event, OrderBlock bean, Throwable e) {
		if (e == null) {
			OrderBlock originalBean = getOriginalBean(dao, bean);
			if (MathUtils.isNullOrZero(bean.getQuantityUnfilled()) && !MathUtils.isNullOrZero(originalBean.getQuantityUnfilled())) {
				OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
				searchForm.setOrderBlockIdentifier(bean.getId());
				List<OrderAllocation> orderAllocationList = getOrderAllocationService().getOrderAllocationList(searchForm);
				// Call this with a scheduler and on a delay? - conflicts with auto execution on updating order allocation table....
				// asynchronous support
				getOrderAllocationService().scheduleOrderAllocationWorkflowTransitionSystemDefined("FILLED-BLOCK-ID" + bean.getId(), BeanUtils.getBeanIdentityArray(orderAllocationList, Long.class), OrderAllocation.WORKFLOW_STATE_FILLED, 1);
			}
		}
	}
}
