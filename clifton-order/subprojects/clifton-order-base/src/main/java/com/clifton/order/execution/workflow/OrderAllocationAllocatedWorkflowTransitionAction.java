package com.clifton.order.execution.workflow;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacementAllocation;
import com.clifton.order.execution.search.OrderPlacementAllocationSearchForm;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderCompany;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderAllocationAllocatedWorkflowTransitionAction implements WorkflowTransitionActionHandler<OrderAllocation> {

	private OrderExecutionService orderExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderAllocation processAction(OrderAllocation orderAllocation, WorkflowTransition transition) {
		OrderPlacementAllocationSearchForm searchForm = new OrderPlacementAllocationSearchForm();
		searchForm.setOrderAllocationId(orderAllocation.getId());

		List<OrderPlacementAllocation> placementAllocationList = getOrderExecutionService().getOrderPlacementAllocationList(searchForm);

		boolean currency = orderAllocation.getSecurity().isCurrency();
		if (CollectionUtils.getSize(placementAllocationList) == 1) {
			OrderPlacementAllocation orderPlacementAllocation = placementAllocationList.get(0);
			orderAllocation.setExecutingBrokerCompany(orderPlacementAllocation.getOrderPlacement().getExecutingBrokerCompany());
			orderAllocation.setOrderDestination(orderPlacementAllocation.getOrderPlacement().getOrderDestination());
			if (currency) {
				orderAllocation.setExchangeRateToSettlementCurrency(orderPlacementAllocation.getPrice());
			}
			else {
				orderAllocation.setAverageUnitPrice(orderPlacementAllocation.getPrice());
			}
		}
		else {
			BigDecimal totalFilled = CoreMathUtils.sumProperty(placementAllocationList, OrderPlacementAllocation::getQuantity);
			BigDecimal averagePrice = MathUtils.isNullOrZero(totalFilled) ? null : MathUtils.divide(CoreMathUtils.sumProperty(placementAllocationList, orderPlacementAllocation -> MathUtils.multiply(orderPlacementAllocation.getQuantity(), orderPlacementAllocation.getPrice())), totalFilled);
			OrderDestination[] orderDestinationList = BeanUtils.getPropertyValuesUniqueExcludeNull(placementAllocationList, orderPlacementAllocation -> orderPlacementAllocation.getOrderPlacement().getOrderDestination(), OrderDestination.class);
			OrderCompany[] executingBrokerCompanyList = BeanUtils.getPropertyValuesUniqueExcludeNull(placementAllocationList, orderPlacementAllocation -> orderPlacementAllocation.getOrderPlacement().getExecutingBrokerCompany(), OrderCompany.class);

			orderAllocation.setExecutingBrokerCompany((executingBrokerCompanyList.length == 1) ? executingBrokerCompanyList[0] : null);
			orderAllocation.setOrderDestination((orderDestinationList.length == 1) ? orderDestinationList[0] : null);
			if (currency) {
				orderAllocation.setExchangeRateToSettlementCurrency(averagePrice);
			}
			else {
				orderAllocation.setAverageUnitPrice(averagePrice);
			}
		}
		return orderAllocation;
	}
}
