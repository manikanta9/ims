package com.clifton.order.api.converters;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.api.server.model.BaseTradeModel;
import com.clifton.order.api.server.model.TradeEnhancedModel;
import com.clifton.order.api.server.model.TradeModel;
import com.clifton.order.api.server.model.UserModel;
import com.clifton.order.trade.OrderTrade;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Component;

import java.time.ZoneOffset;
import java.util.List;


/**
 * @author manderson
 */
@Component
public class OrderTradeConverterHandlerImpl implements OrderTradeConverterHandler {

	private OrderAllocationService orderAllocationService;

	private SecurityUserService securityUserService;

	private WorkflowHistoryService workflowHistoryService;

	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeModel convertOrderTradeToTrade(OrderTrade orderTrade) {
		TradeModel model = new TradeModel();
		populateBaseTradeModelFields(orderTrade, model);
		return model;
	}


	@Override
	public TradeEnhancedModel convertOrderTradeToTradeEnhanced(OrderTrade orderTrade) {
		TradeEnhancedModel model = new TradeEnhancedModel();
		populateBaseTradeModelFields(orderTrade, model);

		OrderAllocation orderAllocation = getOrderAllocationService().getOrderAllocation(orderTrade.getOrderAllocationIdentifier());
		if (orderAllocation != null) {
			model.setOrderAllocationIdentifier(orderTrade.getOrderAllocationIdentifier());
			model.setOrderBlockIdentifier(orderAllocation.getOrderBlockIdentifier());
			model.setQuantityIntended(orderAllocation.getQuantityIntended());
			model.setOrderCreatorUser(getUserModelForSecurityUser(orderAllocation.getOrderCreatorUser()));

			// Get last approval history
			WorkflowHistory approvalHistory = getLastWorkflowHistoryForTransition(orderAllocation, "Approve");
			if (approvalHistory != null) {
				SecurityUser approvedByUser = getSecurityUserService().getSecurityUser(approvalHistory.getCreateUserId());
				if (approvedByUser != null) {
					model.setApprovedByUser(getUserModelForSecurityUser(approvedByUser));
				}
				model.setApprovedOn(approvalHistory.getCreateDate().toInstant().atOffset(ZoneOffset.UTC));
			}

			model.setTraderUser(getUserModelForSecurityUser(orderAllocation.getTraderUser()));
		}

		// ? How to get the placement from the trade - are we missing a link  The original thought was you could have 2 placements, but could it still only result in one trade ?
		// model.setOrderPlacementIdentifier();
		//  OffsetDateTime executionTime; - this comes off the placement

		// Add Rule Violation List for Ignored Compliance Rule Violations, we don't have any of these yet and I'm guessing something like trade date in the future doesn't apply these are the "real" compliance rules
		return model;
	}


	private <T extends BaseTradeModel> void populateBaseTradeModelFields(OrderTrade orderTrade, T model) {
		model.setTradeSource("OMS");
		model.setId(orderTrade.getId());
		model.setOrderType(orderTrade.getOrderType().getName());
		model.setClientAccount(orderTrade.getClientAccount().toModelObject());
		if (!orderTrade.getClientAccount().equals(orderTrade.getHoldingAccount())) {
			// only set the Holding Account if Client and Holding Accounts are different (reduces object/message size)
			model.setHoldingAccount(orderTrade.getHoldingAccount().toModelObject());
		}
		model.setOpenCloseType(orderTrade.getOpenCloseType().getName());
		model.setSettlementCurrencyCode(orderTrade.getSettlementCurrency().getTicker());
		model.setAccountingNotional(orderTrade.getAccountingNotional().floatValue());
		model.setSettlementAmount(orderTrade.getSettlementAmount().floatValue());
		model.setQuantity(orderTrade.getQuantity());
		model.setPrice(orderTrade.getPrice() == null ? null : orderTrade.getPrice().floatValue());
		model.setExchangeRateToSettlementCurrency(orderTrade.getExchangeRateToSettlementCurrency().floatValue());
		model.setTradeCreateDate(orderTrade.getCreateDate().toInstant().atOffset(ZoneOffset.UTC));
		model.setTradeDate(DateUtils.asLocalDate(orderTrade.getTradeDate()));
		model.setSettlementDate(DateUtils.asLocalDate(orderTrade.getSettlementDate()));
		model.setExecutingBroker(orderTrade.getExecutingBrokerCompany().toModelObject());
		model.setCommissionAmount(orderTrade.getCommissionAmount() == null ? null : orderTrade.getCommissionAmount().floatValue());
		model.setFeeAmount(orderTrade.getFeeAmount() == null ? null : orderTrade.getFeeAmount().floatValue());
		// NET AMOUNT
		// ACCRUALS?

		OrderAllocation orderAllocation = getOrderAllocationService().getOrderAllocation(orderTrade.getOrderAllocationIdentifier());
		if (orderAllocation != null) {
			model.setOrderCreateDate(orderAllocation.getCreateDate().toInstant().atOffset(ZoneOffset.UTC));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private UserModel getUserModelForSecurityUser(SecurityUser securityUser) {
		if (securityUser == null) {
			return null;
		}
		UserModel userModel = new UserModel();
		userModel.setUserName(securityUser.getUserName());
		userModel.setDisplayName(securityUser.getDisplayName());
		return userModel;
	}


	public WorkflowHistory getLastWorkflowHistoryForTransition(OrderAllocation orderAllocation, String transitionName) {
		Workflow workflow = orderAllocation.getWorkflowState().getWorkflow();
		List<WorkflowHistory> historyList = getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(orderAllocation, false);

		List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionListByWorkflow(workflow.getId());
		transitionList = BeanUtils.filter(transitionList, WorkflowTransition::getName, transitionName);

		for (WorkflowHistory history : historyList) {
			for (WorkflowTransition transition : transitionList) {
				if (transition.getStartWorkflowState() != null && transition.getStartWorkflowState().equals(history.getStartWorkflowState())) {
					if (transition.getEndWorkflowState().equals(history.getEndWorkflowState())) {
						return history;
					}
				}
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAllocationService getOrderAllocationService() {
		return this.orderAllocationService;
	}


	public void setOrderAllocationService(OrderAllocationService orderAllocationService) {
		this.orderAllocationService = orderAllocationService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
