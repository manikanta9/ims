package com.clifton.order.api.converters;

import com.clifton.order.api.server.model.TradeEnhancedModel;
import com.clifton.order.api.server.model.TradeModel;
import com.clifton.order.trade.OrderTrade;


/**
 * @author manderson
 */
public interface OrderTradeConverterHandler {


	public TradeModel convertOrderTradeToTrade(OrderTrade orderTrade);


	public TradeEnhancedModel convertOrderTradeToTradeEnhanced(OrderTrade orderTrade);
}
