package com.clifton.order.allocation.upload;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.system.note.DocumentSystemNoteService;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.group.OrderGroup;
import com.clifton.order.group.OrderGroupService;
import com.clifton.order.setup.OrderGroupType;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.upload.SystemUploadHandler;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>OrderAllocationUploadService</code> ...
 *
 * @author Mary Anderson
 */
@Service
@Getter
@Setter
@SuppressWarnings("rawtypes")
public class OrderAllocationUploadServiceImpl implements OrderAllocationUploadService {

	private static final String ORDER_GROUP_IMPORT_FILE_NOTE_TYPE = "Import File";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderAllocationUploadDataPopulator orderAllocationUploadDataPopulator;
	private OrderGroupService orderGroupService;
	private OrderSetupService orderSetupService;

	private SystemUploadHandler systemUploadHandler;
	private FileUploadHandler fileUploadHandler;
	private SystemNoteService systemNoteService;

	private DocumentSystemNoteService documentSystemNoteService;

	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadOrderAllocationUploadFile(OrderAllocationUploadCommand uploadCommand) {
		OrderGroup orderGroup = uploadCommand.getOrderGroup() == null ? new OrderGroup() : uploadCommand.getOrderGroup();
		if (!uploadCommand.isSimple() || orderGroup.getGroupType() == null || orderGroup.getGroupType().getId() == null) {
			String groupTypeName = (orderGroup.getGroupType() == null || StringUtils.isEmpty(orderGroup.getGroupType().getName()))
					? OrderGroupType.ORDER_GROUP_TYPE_UPLOAD_NAME : orderGroup.getGroupType().getName();
			orderGroup.setGroupType(getOrderSetupService().getOrderGroupTypeByName(groupTypeName));
		}

		if (uploadCommand.isSimple()) {
			ValidationUtils.assertNotNull(uploadCommand.getSimpleOrderType(), "Please specify the Order Type for the Order Allocations in the Simple Upload File.");
		}

		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, !uploadCommand.isSimple());
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(beanList), "No data available in the file to upload.  Please check your file.");
		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			OrderAllocation orderAllocation = (OrderAllocation) obj;
			try {
				ValidationUtils.assertTrue(orderAllocation.isNewBean(), "Cannot update existing order allocations, only inserts are allowed.");

				getOrderAllocationUploadDataPopulator().populateOrderAllocation(orderAllocation, uploadCommand);

				orderAllocation.setOrderGroup(orderGroup);
				orderGroup.addOrderAllocation(orderAllocation);
			}
			catch (Throwable e) {
				if (!uploadCommand.isPartialUploadAllowed()) {
					throw e;
				}
				uploadCommand.getUploadResult().addError(orderAllocation.getLabel(), ExceptionUtils.getOriginalMessage(e));
				LogUtils.errorOrInfo(LogCommand.ofThrowable(getClass(), e));
			}
		}
		if (!CollectionUtils.isEmpty(orderGroup.getOrderAllocationList())) {
			saveOrderAllocationUploadResults(uploadCommand, orderGroup);
		}
		else {
			uploadCommand.getUploadResult().addUploadResults("Order Allocation(s)", 0, false);
		}
	}


	@Transactional
	protected void saveOrderGroupNote(OrderAllocationUploadCommand uploadCommand, OrderGroup group) {
		ValidationUtils.assertNotNull(group.getId(), "Group ID is Required.");

		SystemTable table = getSystemSchemaService().getSystemTableByName(OrderGroup.TABLE_NAME);
		SystemNoteType noteType = getSystemNoteService().getSystemNoteTypeByTableAndName(table.getId(), ORDER_GROUP_IMPORT_FILE_NOTE_TYPE);
		ValidationUtils.assertNotNull(noteType, "Note Type with name [" + ORDER_GROUP_IMPORT_FILE_NOTE_TYPE + "] does not exist for OrderGroup table.");

		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setFkFieldId(group.getId());
		note.setText(uploadCommand.getUploadResult().toString());
		FileUploadWrapper fileUploadWrapper = new FileUploadWrapper();
		fileUploadWrapper.setFile(uploadCommand.getFile());
		// Attach Document Appropriately
		getDocumentSystemNoteService().uploadDocumentSystemNote(note, fileUploadWrapper);
	}


	@Transactional
	protected void saveOrderAllocationUploadResults(OrderAllocationUploadCommand uploadCommand, OrderGroup orderGroup) {
		// Save order allocations
		getOrderGroupService().saveOrderGroup(orderGroup);
		uploadCommand.getUploadResult().addUploadResults("Order Allocation(s)", CollectionUtils.getSize(orderGroup.getOrderAllocationList()), false);
		//save order group note with  file attached
		saveOrderGroupNote(uploadCommand, orderGroup);
	}
}
