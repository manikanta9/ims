package com.clifton.order.api;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.model.ModelAwareUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.api.converters.OrderTradeConverterHandler;
import com.clifton.order.api.server.TradesApi;
import com.clifton.order.api.server.model.DateComparisonConditionsModel;
import com.clifton.order.api.server.model.SecurityRetrieverModel;
import com.clifton.order.api.server.model.TradeEnhancedPagingArrayListModel;
import com.clifton.order.api.server.model.TradePagingArrayListModel;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.trade.OrderTrade;
import com.clifton.order.trade.OrderTradeService;
import com.clifton.order.trade.search.OrderTradeSearchForm;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;


/**
 * @author manderson
 */
@RestController
public class TradesApiImpl implements TradesApi {

	private OrderSharedService orderSharedService;

	private OrderTradeConverterHandler orderTradeConverterHandler;

	private OrderTradeService orderTradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ResponseEntity<TradePagingArrayListModel> getTradeList(Integer start, Integer pageSize, LocalDate tradeDate, DateComparisonConditionsModel tradeDateComparisonCondition, LocalDate settlementDate, DateComparisonConditionsModel settlementDateComparisonCondition, SecurityRetrieverModel security) {
		OrderTradeSearchForm searchForm = new OrderTradeSearchForm();
		searchForm.setLimit(pageSize);
		searchForm.setStart(start);
		if (tradeDate != null) {
			searchForm.addSearchRestriction("tradeDate", tradeDateComparisonCondition == null ? ComparisonConditions.EQUALS : ComparisonConditions.valueOf(tradeDateComparisonCondition.getValue()), DateUtils.asUtilDate(tradeDate));
		}
		if (settlementDate != null) {
			searchForm.addSearchRestriction("settlementDate", settlementDateComparisonCondition == null ? ComparisonConditions.EQUALS : ComparisonConditions.valueOf(settlementDateComparisonCondition.getValue()), DateUtils.asUtilDate(settlementDate));
		}

		if (security != null) {
			OrderSecurity orderSecurity = getOrderSharedService().getOrderSecurityForRetriever(security, true);
			if (orderSecurity != null) {
				searchForm.setSecurityId(orderSecurity.getId());
			}
		}
		List<OrderTrade> orderTradeList = getOrderTradeService().getOrderTradeList(searchForm);
		TradePagingArrayListModel pagingArrayListModel = new TradePagingArrayListModel();
		if (orderTradeList instanceof PagingArrayList) {
			pagingArrayListModel.setFirstElementIndex(((PagingArrayList<OrderTrade>) orderTradeList).getFirstElementIndex());
			pagingArrayListModel.setPageSize(((PagingArrayList<OrderTrade>) orderTradeList).getPageSize());
			pagingArrayListModel.setTotalElementCount(((PagingArrayList<OrderTrade>) orderTradeList).getTotalElementCount());
		}
		else {
			pagingArrayListModel.setFirstElementIndex(searchForm.getStart());
			pagingArrayListModel.setPageSize(searchForm.getLimit());
			pagingArrayListModel.setTotalElementCount(CollectionUtils.getSize(orderTradeList));
		}
		pagingArrayListModel.data(ModelAwareUtils.getModelObjectListUsingConverter(orderTradeList, getOrderTradeConverterHandler()::convertOrderTradeToTrade));

		return ResponseEntity.ok(Objects.requireNonNull(pagingArrayListModel));
	}


	@Override
	public ResponseEntity<TradeEnhancedPagingArrayListModel> getTradeEnhancedList(Integer start, Integer pageSize, LocalDate tradeDate, DateComparisonConditionsModel tradeDateComparisonCondition) {
		OrderTradeSearchForm searchForm = new OrderTradeSearchForm();
		searchForm.setLimit(pageSize);
		searchForm.setStart(start);
		if (tradeDate != null) {
			searchForm.addSearchRestriction("tradeDate", tradeDateComparisonCondition == null ? ComparisonConditions.EQUALS : ComparisonConditions.valueOf(tradeDateComparisonCondition.getValue()), DateUtils.asUtilDate(tradeDate));
		}
		List<OrderTrade> orderTradeList = getOrderTradeService().getOrderTradeList(searchForm);
		TradeEnhancedPagingArrayListModel pagingArrayListModel = new TradeEnhancedPagingArrayListModel();
		if (orderTradeList instanceof PagingArrayList) {
			pagingArrayListModel.setFirstElementIndex(((PagingArrayList<OrderTrade>) orderTradeList).getFirstElementIndex());
			pagingArrayListModel.setPageSize(((PagingArrayList<OrderTrade>) orderTradeList).getPageSize());
			pagingArrayListModel.setTotalElementCount(((PagingArrayList<OrderTrade>) orderTradeList).getTotalElementCount());
		}
		else {
			pagingArrayListModel.setFirstElementIndex(searchForm.getStart());
			pagingArrayListModel.setPageSize(searchForm.getLimit());
			pagingArrayListModel.setTotalElementCount(CollectionUtils.getSize(orderTradeList));
		}
		pagingArrayListModel.data(ModelAwareUtils.getModelObjectListUsingConverter(orderTradeList, getOrderTradeConverterHandler()::convertOrderTradeToTradeEnhanced));

		return ResponseEntity.ok(Objects.requireNonNull(pagingArrayListModel));
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderSharedService getOrderSharedService() {
		return this.orderSharedService;
	}


	public void setOrderSharedService(OrderSharedService orderSharedService) {
		this.orderSharedService = orderSharedService;
	}


	public OrderTradeService getOrderTradeService() {
		return this.orderTradeService;
	}


	public void setOrderTradeService(OrderTradeService orderTradeService) {
		this.orderTradeService = orderTradeService;
	}


	public OrderTradeConverterHandler getOrderTradeConverterHandler() {
		return this.orderTradeConverterHandler;
	}


	public void setOrderTradeConverterHandler(OrderTradeConverterHandler orderTradeConverterHandler) {
		this.orderTradeConverterHandler = orderTradeConverterHandler;
	}
}
