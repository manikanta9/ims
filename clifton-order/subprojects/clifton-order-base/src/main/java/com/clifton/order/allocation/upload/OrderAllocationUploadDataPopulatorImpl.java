package com.clifton.order.allocation.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.OrderSource;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.search.OrderAccountSearchForm;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderAllocationUploadDataPopulatorImpl implements OrderAllocationUploadDataPopulator {

	private OrderSetupService orderSetupService;
	private OrderSharedService orderSharedService;

	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void populateOrderAllocation(OrderAllocation orderAllocation, OrderAllocationUploadCommand uploadCommand) {
		// Can apply to both simple and regular order allocation uploads as this information is no longer required on screen
		// and fields are allowed to be set in the file.
		ValidationUtils.assertNotNull(orderAllocation.getTradeDate(), "Trade Date is required for each order allocation in the file if not selected on the upload screen.");

		if (orderAllocation.getOrderCreatorUser() == null || (orderAllocation.getOrderCreatorUser().isNewBean() && StringUtils.isEmpty(orderAllocation.getOrderCreatorUser().getUserName()))) {
			throw new ValidationException("Order Creator User is required for each order allocation in the file if not selected on the upload screen.");
		}
		if (orderAllocation.getOrderCreatorUser().isNewBean()) {
			SecurityUser orderCreatorUser = getSecurityUserService().getSecurityUserByName(orderAllocation.getOrderCreatorUser().getUserName());
			if (orderCreatorUser == null) {
				throw new ValidationException("Cannot find user in the system with user name [" + orderAllocation.getOrderCreatorUser().getUserName() + "]");
			}
			orderAllocation.setOrderCreatorUser(orderCreatorUser);
		}

		// Process Simple Look ups
		if (uploadCommand.isSimple()) {
			populateAccountsForOrderAllocation(orderAllocation);
			populateSecurityForOrderAllocation(orderAllocation, uploadCommand);
		}
		ValidationUtils.assertFalse(orderAllocation.getSecurity().isNewBean(), "Security [" + orderAllocation.getSecurity().getLabel() + "] is missing in the database.");
		if (orderAllocation.getSecurity().isCurrency()) {
			if (orderAllocation.getAccountingNotional() != null) {
				ValidationUtils.assertTrue(MathUtils.isGreaterThan(orderAllocation.getAccountingNotional(), BigDecimal.ZERO), "Accounting Notional must be greater than zero");
			}
		}
		else if (orderAllocation.getQuantityIntended() != null) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(orderAllocation.getQuantityIntended(), BigDecimal.ZERO), "Quantity Intended must be greater than zero");
		}

		if (orderAllocation.getOpenCloseType() != null && orderAllocation.getOpenCloseType().isNewBean()) {
			populateOrderOpenCloseTypeForOrderAllocation(orderAllocation);
		}
		ValidationUtils.assertTrue(orderAllocation.getOpenCloseType() != null && !orderAllocation.getOpenCloseType().isNewBean(), "Open Close Type (i.e. Buy/Sell) is required for each order allocation.");

		// Settlement Currency
		if (orderAllocation.getSettlementCurrency() != null && orderAllocation.getSettlementCurrency().isNewBean() && !StringUtils.isEmpty(orderAllocation.getSettlementCurrency().getTicker())) {
			orderAllocation.setSettlementCurrency(getOrderSharedService().getOrderSecurityForCurrencyTicker(orderAllocation.getSettlementCurrency().getTicker()));
		}
		// If still not set - default it
		if (orderAllocation.getSettlementCurrency() == null || orderAllocation.getSettlementCurrency().isNewBean()) {
			orderAllocation.setSettlementCurrency(getOrderSharedService().getOrderSecurityForCurrencyTicker(orderAllocation.getSecurity().getCurrencyCode()));
		}

		// Trade Execution Type
		if (orderAllocation.getExecutionType() != null && orderAllocation.getExecutionType().isNewBean()) {
			orderAllocation.setExecutionType(getOrderSetupService().getOrderExecutionTypeByName(orderAllocation.getExecutionType().getName()));
		}
		if (orderAllocation.getOrderSource() != null && orderAllocation.getOrderSource().isNewBean()) {
			OrderSource orderSource = getOrderSetupService().getOrderSourceByName(orderAllocation.getOrderSource().getName());
			ValidationUtils.assertNotNull(orderSource, "Cannot find Order Source with name " + orderAllocation.getOrderSource().getName());
			orderAllocation.setOrderSource(orderSource);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Population Helper Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	private void populateOrderOpenCloseTypeForOrderAllocation(OrderAllocation orderAllocation) {
		orderAllocation.setOpenCloseType(getOrderSetupService().getOrderOpenCloseTypeByName(orderAllocation.getOpenCloseType().getName()));
	}


	private void populateAccountsForOrderAllocation(OrderAllocation orderAllocation) {
		OrderAccount clientAccount = orderAllocation.getClientAccount();
		OrderAccount holdingAccount = orderAllocation.getHoldingAccount();

		// Client Account Number or Short Name Specified - Use It
		if (clientAccount != null && (!StringUtils.isEmpty(clientAccount.getAccountNumber()) || !StringUtils.isEmpty(clientAccount.getAccountShortName()))) {
			// Will throw an exception if none or more than one found
			clientAccount = getAccountByNumberOrShortName(clientAccount.getAccountShortName(), clientAccount.getAccountNumber(), true, null);
			// Client And Holding Account are the same
			if (holdingAccount == null && clientAccount.isHoldingAccount()) {
				holdingAccount = clientAccount;
			}
			// Now - Look Up Holding Account Based on Client Account Selected (Unless Specified)
			else if (holdingAccount == null || StringUtils.isEmpty(holdingAccount.getAccountNumber())) {
				holdingAccount = getAccountByNumberOrShortName(null, null, false, clientAccount.getClientCompany());
			}
			else {
				// Will throw an exception if none or more than one found
				holdingAccount = getAccountByNumberOrShortName(holdingAccount.getAccountShortName(), holdingAccount.getAccountNumber(), false, clientAccount.getClientCompany());
			}
		}
		// Else - Holding Account Number must be specified and unique
		else if (holdingAccount != null && (!StringUtils.isEmpty(holdingAccount.getAccountNumber()) || !StringUtils.isEmpty(holdingAccount.getAccountShortName()))) {
			// Will throw an exception if none or more than one found
			holdingAccount = getAccountByNumberOrShortName(holdingAccount.getAccountShortName(), holdingAccount.getAccountNumber(), false, null);
			if (holdingAccount.isClientAccount()) {
				clientAccount = holdingAccount;
			}
			else {
				// Now - Look Up Client Account Based on Holding Account Selected (works if only one for the client)
				clientAccount = getAccountByNumberOrShortName(null, null, true, holdingAccount.getClientCompany());
			}
		}
		else {
			throw new ValidationException("At least one of Client Account Number/ShortName or Holding Account Number/ShortName is required.");
		}

		orderAllocation.setClientAccount(clientAccount);
		orderAllocation.setHoldingAccount(holdingAccount);
	}


	private void populateSecurityForOrderAllocation(OrderAllocation orderAllocation, OrderAllocationUploadCommand uploadCommand) {
		if (orderAllocation.getSecurity() == null || StringUtils.isEmpty(orderAllocation.getSecurity().getTicker())) {
			throw new ValidationException("Security Ticker is required for all Order Allocations.");
		}
		// First check if we've already retrieved this security by ticker
		OrderSecurity security = uploadCommand.getSecurityTickerMap().get(orderAllocation.getSecurity().getTicker());
		// If not - look it up
		if (security == null) {
			security = getSecurityForOrderAllocation(orderAllocation);
			uploadCommand.getSecurityTickerMap().put(security.getTicker(), security);
		}
		orderAllocation.setSecurity(security);
	}


	/**
	 * Uses {@link OrderSharedService#getOrderSecurityListByTicker(String)} to find the security for the provided OrderAllocation
	 * The found security is validated for being of the correct Security Type and active on the Order Allocation's trade date.
	 */
	private OrderSecurity getSecurityForOrderAllocation(OrderAllocation orderAllocation) {
		List<OrderSecurity> resultList = getOrderSharedService().getOrderSecurityListByTicker(orderAllocation.getSecurity().getTicker());
		resultList = BeanUtils.filter(resultList, security ->
				security.getSecurityType().getId().equals(orderAllocation.getOrderType().getSecurityType().getId()) && security.isActiveOn(orderAllocation.getTradeDate())
		);
		String msgSuffix = " on " + DateUtils.fromDateShort(orderAllocation.getTradeDate()) + " with Ticker [" + orderAllocation.getSecurity().getTicker() + "] and Security Type [" + orderAllocation.getOrderType().getSecurityType().getLabel() + "].";
		return CollectionUtils.getFirstElementStrict(resultList, "Cannot find Active Security" + msgSuffix, "Found multiple Active Securities " + msgSuffix);
	}

	////////////////////////////////////////////////////////////////////////
	//////////                  Helper Methods                    //////////
	////////////////////////////////////////////////////////////////////////


	private OrderAccount getAccountByNumberOrShortName(String accountShortName, String accountNumber, boolean ourAccount, OrderCompany clientCompany) {
		OrderAccountSearchForm searchForm = new OrderAccountSearchForm();
		if (ourAccount) {
			searchForm.setClientAccount(true);
		}
		else {
			searchForm.setHoldingAccount(true);
		}
		if (clientCompany != null) {
			searchForm.setClientCompanyId(clientCompany.getId());
		}
		if (!StringUtils.isEmpty(accountNumber)) {
			searchForm.addSearchRestriction(new SearchRestriction("accountNumber", ComparisonConditions.EQUALS, accountNumber));
		}
		else if (!StringUtils.isEmpty(accountShortName)) {
			searchForm.addSearchRestriction(new SearchRestriction("accountShortName", ComparisonConditions.EQUALS, accountShortName));
		}
		// TODO ADD WORKFLOW STATE NAME = ACTIVE

		List<OrderAccount> accountList = getOrderSharedService().getOrderAccountList(searchForm);
		if (CollectionUtils.isEmpty(accountList)) {
			throw new ValidationException("Cannot find " + (ourAccount ? "client" : "holding") + " account with " + (!StringUtils.isEmpty(accountNumber) ? "Account Number [" + accountNumber + "]" : "Account Short Name [" + accountShortName + "]") + (clientCompany != null ? " for Client ["
					+ clientCompany.getName() + "]" : ""));
		}
		else if (accountList.size() > 1) {
			throw new ValidationException("Multiple " + (ourAccount ? "client" : "holding") + " accounts with " + (!StringUtils.isEmpty(accountNumber) ? "Account Number [" + accountNumber + "]" : "Account Short Name [" + accountShortName + "]") + " exist in the system" + (clientCompany != null ? " for client ["
					+ clientCompany.getName() + "]" : ""));
		}
		return accountList.get(0);
	}
}
