package com.clifton.order.block;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderBlockServiceImpl implements OrderBlockService {

	private AdvancedUpdatableDAO<OrderBlock, Criteria> orderBlockDAO;

	private OrderAllocationService orderAllocationService;

	////////////////////////////////////////////////////////////////////////////
	////////                Order Block Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderBlock getOrderBlock(long id, boolean populateAllocations) {
		OrderBlock orderBlock = getOrderBlockDAO().findByPrimaryKey(id);
		if (orderBlock != null && populateAllocations) {
			// Note: Should probably cache this list?
			OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
			searchForm.setOrderBlockIdentifier(id);
			orderBlock.setOrderAllocationList(getOrderAllocationService().getOrderAllocationList(searchForm));
		}
		return orderBlock;
	}


	@Override
	public List<OrderBlock> getOrderBlockList(OrderBlockSearchForm searchForm) {

		return getOrderBlockDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getClientOrHoldingAccountId() != null) {
					DetachedCriteria detachedCriteria = DetachedCriteria.forClass(OrderAllocation.class, "oa");
					detachedCriteria.setProjection(Projections.property("id"));
					detachedCriteria.add(Restrictions.or(Restrictions.eq("clientAccount.id", searchForm.getClientOrHoldingAccountId()), Restrictions.eq("holdingAccount.id", searchForm.getClientOrHoldingAccountId())));
					detachedCriteria.add(Restrictions.eqProperty("orderBlockIdentifier", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.exists(detachedCriteria));
				}
			}
		});
	}


	@Transactional
	@Override
	public OrderBlock saveOrderBlock(OrderBlock bean) {
		List<OrderAllocation> orderAllocationList = bean.getOrderAllocationList();

		ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getQuantityIntended(), BigDecimal.ZERO), "Intended Amount cannot be negative.");
		if (bean.getQuantityPlaced() != null) {
			ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getQuantityPlaced(), BigDecimal.ZERO), "Placed Amount cannot be negative.");
			ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getQuantityUnplaced(), BigDecimal.ZERO), "Selected placed amount is more than what was requested.");
		}
		if (bean.getQuantityFilled() != null) {
			ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getQuantityFilled(), BigDecimal.ZERO), "Filled Amount cannot be negative.");
			ValidationUtils.assertFalse(MathUtils.isLessThan(bean.getQuantityUnfilled(), BigDecimal.ZERO), "Selected filled amount is more than what was requested.");
		}

		bean = getOrderBlockDAO().save(bean);

		if (!CollectionUtils.isEmpty(orderAllocationList)) {
			final long orderBlockId = bean.getId();
			orderAllocationList.forEach(orderAllocation -> orderAllocation.setOrderBlockIdentifier(orderBlockId));
			getOrderAllocationService().saveOrderAllocationList(orderAllocationList, true);
		}

		return bean;
	}
}
