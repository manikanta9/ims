package com.clifton.order.api.converters;

import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.api.server.model.CurrencyOrderAllocationModel;
import com.clifton.order.api.server.model.OrderGroupModel;
import com.clifton.order.api.server.model.StockOrderAllocationModel;
import com.clifton.order.group.OrderGroup;


/**
 * @author manderson
 */
public interface OrderAllocationConverterHandler {

	////////////////////////////////////////////////////////////////////////////
	////////              OrderAllocationModel Converters               ////////
	////////////////////////////////////////////////////////////////////////////


	public CurrencyOrderAllocationModel convertOrderAllocationToCurrencyOrderAllocation(OrderAllocation orderAllocation);


	public StockOrderAllocationModel convertOrderAllocationToStockOrderAllocation(OrderAllocation orderAllocation);

	////////////////////////////////////////////////////////////////////////////
	////////                Order Group Model Converter                 ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderGroupModel convertOrderGroupToOrderGroupModel(OrderGroup orderGroup);
}
