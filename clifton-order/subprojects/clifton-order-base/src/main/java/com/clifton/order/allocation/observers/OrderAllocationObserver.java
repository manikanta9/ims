package com.clifton.order.allocation.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.shared.OrderSharedUtilHandler;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


/**
 * The <code>OrderAllocationObserver</code> is used to observer changes to OrderAllocations to perform additional calculations.
 * i.e. if Accounting Notional OR Exchange Rate to Settlement Currency are changed, update Settlement Amount calculated value
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderAllocationObserver extends SelfRegisteringDaoObserver<OrderAllocation> {

	private OrderSharedMarketDataService orderSharedMarketDataService;
	private OrderSharedUtilHandler orderSharedUtilHandler;

	private SecurityAuthorizationService securityAuthorizationService;
	private SystemSchemaService systemSchemaService;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderAllocation> dao, DaoEventTypes event, OrderAllocation bean) {
		OrderAllocation originalBean = (event.isInsert() ? null : getOriginalBean(dao, bean));
		if (originalBean == null || (!CompareUtils.isEqual(originalBean.getAccountingNotional(), bean.getAccountingNotional()) || !CompareUtils.isEqual(originalBean.getExchangeRateToSettlementCurrency(), bean.getExchangeRateToSettlementCurrency()))) {
			// Recalculate Settlement Amount
			// If Accounting Notional is Null or Zero or the FX Rate is 1 just set it to the Accounting Notional
			if (MathUtils.isNullOrZero(bean.getAccountingNotional()) || MathUtils.isEqual(BigDecimal.ONE, bean.getExchangeRateToSettlementCurrency())) {
				bean.setSettlementAmount(bean.getAccountingNotional());
			}
			else {
				CurrencyConventionData currencyConventionData = getOrderSharedMarketDataService().getOrderMarketDataCurrencyConvention(bean.getLocalCurrencyCode(), bean.getSettlementCurrency().getTicker());
				bean.setSettlementAmount(getOrderSharedUtilHandler().roundCurrencyAmount(bean.getSettlementCurrency(), BooleanUtils.isTrue(currencyConventionData.getMultiplyByExchangeRate()) ? MathUtils.multiply(bean.getAccountingNotional(), bean.getExchangeRateToSettlementCurrency()) : MathUtils.divide(bean.getAccountingNotional(), bean.getExchangeRateToSettlementCurrency())));
			}
		}

		// Ensure if trader is newly selected, they have write access to Order Block table (i.e. can execute the orders)
		if (bean.getTraderUser() != null && ((originalBean == null) || !CompareUtils.isEqual(originalBean.getTraderUser(), bean.getTraderUser()))) {
			SystemTable table = getSystemSchemaService().getSystemTableByName(OrderBlock.TABLE_NAME);
			// If Trader is NOT an admin and doesn't have permissions - throw exception
			if (!getSecurityAuthorizationService().isSecurityUserAdmin(bean.getTraderUser().getUserName()) && !getSecurityAuthorizationService().isSecurityAccessAllowed(bean.getTraderUser().getUserName(), table.getSecurityResource().getName(), SecurityPermission.PERMISSION_WRITE)) {
				throw new ValidationException("Selected trader [" + bean.getTraderUser().getLabel() + "] is not a valid selection. Only users with ability to execute orders can be selected as traders");
			}
		}
	}
}
