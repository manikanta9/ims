Clifton.order.execution.OrderBlockListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderBlockListFind',
	xtype: 'gridpanel',
	wikiPage: 'IT/Order+Life+Cycle',
	instructions: 'A list of all block orders in the system. Orders (Block Orders) consist of one more multiple Orders for the same Trader, Security, Side (Buy/Sell), and Trade Date.  Each block order can be placed at multiple destinations and executed with different executing brokers.',
	viewNames: ['Default View', 'Currency View'],
	defaultDisplayFilter: 'ALL_TODAY',
	columns: [
		{header: 'ID', width: 24, dataIndex: 'id', hidden: true},
		{header: 'Order Type', width: 30, dataIndex: 'orderType.name', filter: {type: 'combo', searchFieldName: 'orderTypeId', url: 'orderTypeListFind.json'}, viewNames: ['Default View']},
		{header: 'Security Type', width: 30, dataIndex: 'security.securityType.text', hidden: true, filter: {searchFieldName: 'securityTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Security Type', valueField: 'id'}},
		{
			header: 'Security', width: 30, dataIndex: 'security.ticker', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'orderSecurityListFind.json'},
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given CCY'},
				{name: 'Default View', label: 'Security'}
			]
		},
		{header: 'Security Name', width: 45, dataIndex: 'security.name', filter: {searchFieldName: 'securityName'}, viewNames: ['Default View']},
		{header: 'Underlying Security', width: 40, dataIndex: 'security.underlyingSecurity.ticker', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'orderSecurityListFind.json'}, hidden: true},
		{header: 'Security Hierarchy', width: 80, dataIndex: 'security.securityHierarchy.nameExpanded', hidden: true, filter: {searchFieldName: 'securityHierarchyOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Security Hierarchy', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{
			header: 'Side', width: 12, dataIndex: 'buy', type: 'boolean', viewNames: ['Default View', 'Currency View'],
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Settle CCY', width: 20, dataIndex: 'settlementCurrency.ticker', filter: {type: 'combo', searchFieldName: 'settlementCurrencyId', displayField: 'label', url: 'orderSecurityListFind.json'}, viewNames: ['Default View', 'Currency View']},
		{
			header: 'Quantity', width: 20, dataIndex: 'quantityIntended', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given Amount'},
				{name: 'Default View', label: 'Quantity'}
			]
		},
		{
			header: 'Placed Quantity', width: 20, dataIndex: 'quantityPlaced', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Placed Amount'},
				{name: 'Default View', label: 'Placed Quantity'}
			]
		},
		{
			header: 'Unplaced Quantity', width: 22, dataIndex: 'quantityUnplaced', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Unplaced Amount'},
				{name: 'Default View', label: 'Unplaced Quantity'}
			]
		},
		{
			header: 'Filled Quantity', width: 20, dataIndex: 'quantityFilled', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Filled Amount'},
				{name: 'Default View', label: 'Filled Quantity'}
			]
		},
		{
			header: 'Unfilled Quantity', width: 22, dataIndex: 'quantityUnfilled', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Unfilled Amount'},
				{name: 'Default View', label: 'Unfilled Quantity'}
			]
		},
		{header: 'Trader', width: 24, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', displayField: 'label', url: 'securityUserListFind.json', allViews: true}},
		{header: 'Traded On', width: 19, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc', allViews: true},
		{header: 'Settled On', width: 19, dataIndex: 'settlementDate', allViews: true}
	],
	switchToViewBeforeReload: function(viewName) {
		// If no toolbar field than inside a detail window, i.e. block order and not necessary to switch this filter
		const otField = TCG.getChildByName(this.getTopToolbar(), 'orderType');
		if (otField) {
			if (viewName === 'Currency View') {
				TCG.data.getDataPromiseUsingCaching('orderTypeByName.json?requestedPropertiesRoot=data&requestedProperties=id|name&name=Currency', this, 'order.type.Currency')
					.then(function(data) {
						const record = {text: data.name, value: data.id};
						otField.setValue(record);
						otField.fireEvent('select', otField, record);
					});
				return true; // cancel reload: select event will reload
			}
			else {
				this.clearFilter('orderType.name', true);
				otField.clearValue();
			}
		}
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Account', xtype: 'toolbar-combo', width: 150, name: 'account', url: 'orderAccountListFind.json', displayField: 'label'});
		filters.push({fieldLabel: 'Order Type', xtype: 'toolbar-combo', width: 150, name: 'orderType', url: 'orderTypeListFind.json', linkedFilter: 'orderType.name', linkedField: 'orderType.name'});
		filters.push({
			fieldLabel: 'Display', xtype: 'combo', name: 'displayFilters', width: 175, minListWidth: 140, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: [
					['MY_ACTIVE', 'My Active Block Orders', 'Display all active block orders where I am the trader assigned.'],
					['ALL_ACTIVE', 'All Active Block Orders', 'Display all active block orders assigned to any trader.'],
					['MY_TODAY', 'My Today\'s Block Orders', 'Display all block orders where I am the trader assigned with a trade date of today.'],
					['ALL_TODAY', 'All Today\'s Block Orders', 'Display all block orders assigned to any trader with a trade date of today.'],
					['MY_ALL', 'My Block Orders', 'Display all block orders where I am the trader assigned.  Will default trade date to after 7 days ago to limit initial query.'],
					['ALL', 'All Block Orders', 'Display all block orders.  Will default trade date to after 7 days ago to limit initial query.']
				]
			}),
			listeners: {
				select: function(field) {
					const gp = TCG.getParentByClass(field, Ext.Panel);
					const v = field.getValue();
					const currentUser = TCG.getCurrentUser();
					if (TCG.contains(v, 'MY')) {
						gp.setFilterValue('traderUser.label', {value: currentUser.id, text: currentUser.label});
					}
					else {
						gp.clearFilter('traderUser.label', true);
					}
					if (TCG.contains(v, 'TODAY')) {
						gp.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
					}
					else if (!TCG.contains(v, 'ACTIVE')) {
						gp.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -8)});
					}

					if (TCG.contains(v, 'ACTIVE')) {
						gp.clearFilter('tradeDate', true);
						gp.setFilterValue('quantityUnfilled', {'gt': 0});
					}
					else {
						gp.clearFilter('quantityUnfilled', true);
					}
					gp.reload();
				}
			}
		});
		return filters;
	},
	getLoadParams: function(firstLoad) {
		const gridPanel = this;
		if (firstLoad) {
			const displayFilters = TCG.getChildByName(gridPanel.getTopToolbar(), 'displayFilters');
			if (TCG.isBlank(displayFilters.getValue())) {
				displayFilters.setValue(this.defaultDisplayFilter);
				displayFilters.fireEvent('select', displayFilters);
			}
		}
		const params = {readUncommittedRequested: true};
		const account = TCG.getChildByName(gridPanel.getTopToolbar(), 'account');
		if (TCG.isNotBlank(account.getValue())) {
			params.clientOrHoldingAccountId = account.getValue();
		}
		return params;
	},
	editor: {
		detailPageClass: 'Clifton.order.execution.OrderBlockWindow',
		drillDownOnly: true
	}
});
Ext.reg('order-block-list-grid', Clifton.order.execution.OrderBlockListGrid);
