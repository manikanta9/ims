Clifton.order.execution.OrderPlacementListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderPlacementListFind',
	xtype: 'gridpanel',
	wikiPage: 'IT/Order+Placements',
	instructions: 'A list of all placements in the system. Placements are a specified quantity of a block order placed with a particular executing broker/destination.',
	viewNames: ['Default View', 'Currency View'],
	defaultDisplayFilter: 'ALL_TODAY',
	enableOrderPlacementExecutionUploadDragAndDrop: false,

	// Dynamic Tooltips - show rejection reason(s) note for Rejected Placements
	useDynamicTooltipColumns: true,
	cacheDynamicTooltip: false,

	columns: [
		{header: 'ID', width: 24, dataIndex: 'id', hidden: true},
		{header: 'Account(s)', width: 60, exportDataIndex: 'orderBlock.id', exportColumnValueConverter: 'orderBlockClientAccountListConverter', hidden: true, exportOnly: true, hideable: false, tooltip: 'Available for Active Placements only in Currency View.  Accounts included in this placement.  Populated ONLY when exporting.'},
		{header: 'Execution Status', width: 40, dataIndex: 'executionStatus.name', filter: {type: 'combo', searchFieldName: 'executionStatusId', url: 'orderExecutionStatusListFind.json'}, viewNames: ['Default View', 'Currency View']},
		{
			header: '&nbsp;', dataIndex: 'rejectReasons', width: 10, filter: false, sortable: false, skipExport: true,
			renderer: function(v, metaData, r) {
				if (TCG.contains(TCG.getValue('executionStatus.name', r.json), 'Rejected')) {
					return TCG.renderDynamicTooltipColumn('cancel', 'Rejection Reason');
				}
				return '';
			}
		},
		{header: 'Allocation Status', width: 40, dataIndex: 'allocationStatus.name', filter: {type: 'combo', searchFieldName: 'allocationStatusId', url: 'orderAllocationStatusListFind.json'}, viewNames: ['Default View', 'Currency View']},
		{header: 'Order Type', width: 35, dataIndex: 'orderBlock.orderType.name', filter: {type: 'combo', searchFieldName: 'orderTypeId', url: 'orderTypeListFind.json'}, viewNames: ['Default View']},
		{header: 'Security Type', width: 35, dataIndex: 'orderBlock.security.securityType.text', hidden: true, filter: {searchFieldName: 'securityTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Security Type', valueField: 'id'}},
		{
			header: 'Security', width: 25, dataIndex: 'orderBlock.security.ticker', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'orderSecurityListFind.json'},
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given CCY'},
				{name: 'Default View', label: 'Security'}
			]
		},
		{header: 'Security Name', width: 45, dataIndex: 'orderBlock.security.name', filter: {searchFieldName: 'securityName'}, viewNames: ['Default View']},
		{header: 'Underlying Security', width: 40, dataIndex: 'orderBlock.security.underlyingSecurity.ticker', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'orderSecurityListFind.json'}, hidden: true},
		{header: 'Security Hierarchy', width: 80, dataIndex: 'orderBlock.security.securityHierarchy.nameExpanded', hidden: true, filter: {searchFieldName: 'securityHierarchyOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Security Hierarchy', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{header: 'Settle CCY', width: 25, dataIndex: 'orderBlock.settlementCurrency.ticker', hidden: true, filter: {searchFieldName: 'settlementCurrencyId', type: 'combo', displayField: 'label', url: 'orderSecurityListFind.json?securityType=Currency'}, viewNames: ['Currency View']},
		{
			header: 'Side', width: 12, dataIndex: 'orderBlock.buy', type: 'boolean', filter: {searchFieldName: 'buy'}, viewNames: ['Default View', 'Currency View'],
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Quantity', width: 30, dataIndex: 'quantityIntended', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given Amount'},
				{name: 'Default View', label: 'Quantity'}
			]
		},
		{
			header: 'Filled Qty', width: 30, dataIndex: 'quantityFilled', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Filled Amount'},
				{name: 'Default View', label: 'Filled Qty'}
			]
		},
		{
			header: 'Remaining Qty', width: 30, dataIndex: 'quantityUnfilled', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Remaining Amount'},
				{name: 'Default View', label: 'Remaining Qty'}
			]
		},
		{
			header: 'Avg Price', width: 25, dataIndex: 'averageUnitPrice', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'FX Rate'},
				{name: 'Default View', label: 'Avg Price'}
			]
		},
		{header: 'Order Destination', width: 34, dataIndex: 'orderDestination.name', filter: {type: 'combo', searchFieldName: 'orderDestinationId', url: 'orderDestinationListFind.json?executionVenue=true'}, allViews: true},
		{header: 'Destination Config', width: 34, dataIndex: 'orderDestinationConfiguration.name', filter: {type: 'combo', searchFieldName: 'orderDestinationConfigurationId', url: 'orderDestinationConfigurationListFind.json'}, allViews: true},
		{header: 'Destination Type', width: 34, dataIndex: 'orderDestination.destinationType.name', hidden: true, filter: {type: 'combo', searchFieldName: 'orderDestinationTypeId', url: 'orderDestinationTypeListFind.json'}},
		{header: 'Executing Broker', width: 50, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker'}, allViews: true},
		{header: 'Available Executing Broker(s)', width: 60, exportDataIndex: 'id', exportColumnValueConverter: 'orderManagementExecutionPlacementExecutingBrokerListConverter', hidden: true, exportOnly: true, hideable: false, tooltip: 'Available for Active Placement only. Executing Brokers available for this placement.  Populated ONLY when exporting.'},
		{header: 'Trader', width: 24, dataIndex: 'orderBlock.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', displayField: 'label', url: 'securityUserListFind.json'}, allViews: true},
		{header: 'Traded On', width: 26, dataIndex: 'orderBlock.tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc', filter: {searchFieldName: 'tradeDate'}, allViews: true},
		{header: 'Settled On', width: 24, dataIndex: 'orderBlock.settlementDate', filter: {searchFieldName: 'settlementDate'}, allViews: true}
	],
	switchToViewBeforeReload: function(viewName) {
		// If no toolbar field than inside a detail window, i.e. block order and not necessary to switch this filter
		const otField = TCG.getChildByName(this.getTopToolbar(), 'orderType');
		if (otField) {
			if (viewName === 'Currency View') {
				TCG.data.getDataPromiseUsingCaching('orderTypeByName.json?requestedPropertiesRoot=data&requestedProperties=id|name&name=Currency', this, 'order.type.Currency')
					.then(function(data) {
						const record = {text: data.name, value: data.id};
						otField.setValue(record);
						otField.fireEvent('select', otField, record);
					});
				return true; // cancel reload: select event will reload
			}

			else {
				this.clearFilter('orderBlock.orderType.name', true);
				otField.clearValue();
			}
		}
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];

		if (TCG.isTrue(this.enableOrderPlacementExecutionUploadDragAndDrop)) {
			const gridPanel = this;
			// Doesn't add an extra filter, but added the DD Icon/Message to the toolbar
			// Drag and Drop Support
			filters.push({
				xtype: 'drag-drop-container',
				layout: 'fit',
				allowMultiple: false,
				bindToFormLoad: false,
				cls: undefined,
				popupComponentName: 'Clifton.order.management.execution.upload.PlacementExecutionUploadWindow',
				message: '&nbsp;',
				tooltipMessage: 'Drag and drop a file to this grid to upload Placement execution information.',
				// Object to reload - i.e. gridPanel to reload after attaching new file
				getReloadObject: function() {
					return gridPanel;
				}
			});
			filters.push('-');
		}
		filters.push({fieldLabel: 'Account', xtype: 'toolbar-combo', width: 150, name: 'account', url: 'orderAccountListFind.json', displayField: 'label'});
		filters.push({fieldLabel: 'Order Type', xtype: 'toolbar-combo', width: 150, name: 'orderType', url: 'orderTypeListFind.json', linkedFilter: 'orderBlock.orderType.name', linkedField: 'orderBlock.orderType.name'});
		filters.push({fieldLabel: 'Destination Type', xtype: 'toolbar-combo', width: 150, name: 'destinationType', url: 'orderDestinationTypeListFind.json', linkedFilter: 'orderDestination.destinationType.name', linkedField: 'orderDestination.destinationType.name'});
		filters.push({
			fieldLabel: 'Display', xtype: 'combo', name: 'displayFilters', width: 175, minListWidth: 140, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: [
					['MY_ACTIVE', 'My Active Placements', 'Display all active placements where I am the trader assigned.'],
					['ALL_ACTIVE', 'All Active Placements', 'Display all active placements assigned to any trader.'],
					['MY_TODAY', 'My Today\'s Placements', 'Display all placements where I am the trader assigned with a trade date of today.'],
					['ALL_TODAY', 'All Today\'s Placements', 'Display all placements assigned to any trader with a trade date of today.'],
					['MY_ALL', 'My Placements', 'Display all placements where I am the trader assigned.  Will default trade date to after 7 days ago to limit initial query.'],
					['ALL', 'All Placements', 'Display all placements.  Will default trade date to after 7 days ago to limit initial query.']
				]
			}),
			listeners: {
				select: function(field) {
					const gp = TCG.getParentByClass(field, Ext.Panel);
					const v = field.getValue();
					const currentUser = TCG.getCurrentUser();
					if (TCG.contains(v, 'MY')) {
						gp.setFilterValue('orderBlock.traderUser.label', {value: currentUser.id, text: currentUser.label});
					}
					else {
						gp.clearFilter('orderBlock.traderUser.label', true);
					}
					if (TCG.contains(v, 'TODAY')) {
						gp.setFilterValue('orderBlock.tradeDate', {'after': new Date().add(Date.DAY, -1)});
					}
					else if (!TCG.contains(v, 'ACTIVE')) {
						gp.setFilterValue('orderBlock.tradeDate', {'after': new Date().add(Date.DAY, -8)});
					}

					if (TCG.contains(v, 'ACTIVE')) {
						gp.clearFilter('orderBlock.tradeDate', true);
						gp.activePlacementsOnly = true;
					}
					else {
						gp.activePlacementsOnly = false;
					}
					gp.reload();
				}
			}
		});
		return filters;
	},
	getLoadParams: function(firstLoad) {
		const gridPanel = this;
		if (firstLoad) {
			const displayFilters = TCG.getChildByName(gridPanel.getTopToolbar(), 'displayFilters');
			if (TCG.isBlank(displayFilters.getValue())) {
				displayFilters.setValue(this.defaultDisplayFilter);
				displayFilters.fireEvent('select', displayFilters);
			}
		}
		const lp = {readUncommittedRequested: true};
		if (TCG.isTrue(this.activePlacementsOnly)) {
			lp.activePlacementsOnly = true;
		}
		const account = TCG.getChildByName(gridPanel.getTopToolbar(), 'account');
		if (TCG.isNotBlank(account.getValue())) {
			lp.clientOrHoldingAccountId = account.getValue();
		}
		return lp;
	},
	editor: {
		detailPageClass: 'Clifton.order.execution.OrderPlacementWindow',
		drillDownOnly: true
	},

	// Custom Export functionality that does look ups for each row.  Allowed only on the active placements tabs to prevent pulling too much data
	// Accounts are only listed when in the CCY view
	isSkipExport: function(column) {
		if (TCG.isEquals('Account(s)', column.header) || TCG.isNotEquals('Currency View', this.currentViewName)) {
			return true;
		}
		return false;
	},


	getTooltipTextForRow: function(row, id, tooltipEventName) {
		// Don't cache this so we can always load the latest
		const note = TCG.data.getData('orderPlacementNoteLastForNoteType.json?orderPlacementId=' + id + '&noteTypeName=' + tooltipEventName, this);
		let noteText;
		if (TCG.isBlank(TCG.getValue('text', note))) {
			noteText = '<div style="WHITE-SPACE: normal; BORDER-TOP: 1px solid #909090; PADDING: 3px">No ' + tooltipEventName + ' available.</div>';
		}
		else {
			noteText = '<div style="WHITE-SPACE: normal; BORDER-TOP: 1px solid #909090; PADDING: 3px">' + TCG.getValue('text', note) + '</div>';
		}
		return '<br><br><b>Last ' + tooltipEventName + '</b><br>' + noteText;
	}


});
Ext.reg('order-placement-list-grid', Clifton.order.execution.OrderPlacementListGrid);
