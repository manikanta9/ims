Clifton.order.execution.OrderBlockWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Block Order',
	titlePrefixSeparator: ': ',
	width: 1100,
	height: 500,
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				layout: 'border',
				items: [
					{
						xtype: 'formpanel',
						region: 'north',
						height: 150,
						url: 'orderBlock.json',
						readOnly: true,
						labelFieldName: 'labelShort',

						listeners: {
							afterload: function(formPanel) {
								if (TCG.isTrue(formPanel.getFormValue('security.currency', false, false))) {
									formPanel.setFieldLabel('security.label', 'Given CCY:');
									formPanel.setFieldLabel('quantityIntended', 'Given Amount:');
									formPanel.setFieldLabel('quantityPlaced', ' Placed Amount:');
									formPanel.setFieldLabel('quantityFilled', ' Filled Amount:');
									formPanel.setFieldLabel('quantityUnplaced', ' Unplaced Amount:');
									formPanel.setFieldLabel('quantityUnfilled', ' Unfilled Amount:');
									const grid = TCG.getChildByName(this.getWindow(), 'orderAllocationListFind');
									grid.setDefaultView('Currency View'); // sets it as "checked"
									grid.switchToView('Currency View', true);
								}
							}
						},

						items: [{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [
								{
									rows: [
										{
											fieldLabel: 'Side', name: 'buy', xtype: 'displayfield', boxMaxWidth: 125,
											setRawValue: function(v) {
												this.el.addClass(TCG.isTrue(v) ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, TCG.isTrue(v) ? 'BUY' : 'SELL');
											}
										},
										{fieldLabel: 'Security', name: 'security.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'security.id'},
										{fieldLabel: 'Settle CCY', name: 'settlementCurrency.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'settlementCurrency.id'},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'}

									],
									config: {columnWidth: 0.5}
								},
								{
									rows: [
										{fieldLabel: 'Quantity', name: 'quantityIntended', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Placed Quantity', name: 'quantityPlaced', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Filled Quantity', name: 'quantityFilled', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
									],
									config: {columnWidth: 0.25}
								},
								{
									rows: [
										{fieldLabel: 'Block Order ID', name: 'id', xtype: 'integerfield'},
										{fieldLabel: 'Unplaced Qty', name: 'quantityUnplaced', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Unfilled Qty', name: 'quantityUnfilled', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true}
									],
									config: {columnWidth: 0.25}
								}
							]
						}]
					},

					{flex: 0.02},
					{
						title: 'Order Allocation(s)',
						xtype: 'order-allocation-list-grid',
						instructions: 'Selected Order was blocked from the following Order Allocation(s).',
						flex: 0.49,
						region: 'center',
						plugins: {ptype: 'gridsummary'},
						columnOverrides: [
							{dataIndex: 'orderType.name', hidden: true, viewNames: undefined},
							{dataIndex: 'buy', hidden: true, viewNames: ['Export Friendly', 'Currency View']},
							{dataIndex: 'openCloseType.label', hidden: false, viewNames: ['Default View', 'Export Friendly']},
							{dataIndex: 'security.ticker', hidden: true, viewNames: undefined},
							{dataIndex: 'security.name', hidden: true, viewNames: undefined},
							{dataIndex: 'averageUnitPrice', hidden: true, viewNames: undefined},
							{dataIndex: 'traderUser.label', hidden: true, viewNames: undefined},
							{dataIndex: 'tradeDate', hidden: true, viewNames: undefined},
							{dataIndex: 'settlementDate', hidden: true, viewNames: undefined},
							{dataIndex: 'orderDestination.name', hidden: true, viewNames: undefined}
						],
						getLoadParams: function(firstLoad) {
							return {orderBlockIdentifier: this.getWindow().getMainFormId()};
						},
						getTopToolbarFilters: function(toolbar) {
							return [
								{fieldLabel: 'PM Team', name: 'teamSecurityGroupId', width: 120, minListWidth: 120, xtype: 'toolbar-system-security-group-combo', groupTagName: 'PM Team', linkedFilter: 'clientAccount.teamSecurityGroup.name'}
							];
						}
					}
				]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'OrderBlock',
					defaultActiveFilter: false,
					// Set to true to see notes for related entities - i.e. on Placements also see block order and order allocation notes
					includeNotesForLinkedEntity: true,
					// Set to true to see notes where used as related entity - i.e. on security also see all trade notes for that security
					includeNotesAsLinkedEntity: true
				}]
			},

			{
				title: 'Placements',
				items: [{
					xtype: 'order-placement-list-grid',
					instructions: 'This Order was executed via the following Placement(s). Usually an Order is executed via a single Placement.  However, a large order maybe split into more than one Placement on different Order Destinations (Venues).',
					plugins: {ptype: 'gridsummary'},

					columnOverrides: [
						{dataIndex: 'orderBlock.orderType.name', hidden: true, viewNames: undefined},
						{dataIndex: 'orderBlock.security.ticker', hidden: true, viewNames: undefined},
						{dataIndex: 'orderBlock.security.name', hidden: true, viewNames: undefined},
						{dataIndex: 'orderBlock.traderUser.label', hidden: true, allViews: false},
						{dataIndex: 'orderBlock.tradeDate', hidden: true, defaultSortColumn: false, allViews: false},
						{dataIndex: 'orderBlock.settlementDate', hidden: true, allViews: false}
					],
					getTopToolbarFilters: function(toolbar) {
						return [];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							if (TCG.isTrue(this.getWindow().getMainFormPanel().getFormValue('security.currency', false, false))) {
								this.setDefaultView('Currency View'); // sets it as "checked"
								this.switchToView('Currency View', true);
							}
						}
						return {orderBlockId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.order.execution.OrderPlacementWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
