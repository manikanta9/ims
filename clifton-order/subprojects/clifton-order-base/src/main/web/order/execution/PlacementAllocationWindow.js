Clifton.order.execution.PlacementAllocationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Placement Allocation',
	titlePrefixSeparator: ': ',
	width: 900,
	height: 500,
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'orderPlacementAllocation.json',
					readOnly: true,
					labelFieldName: 'label',
					maxWindowTitleLength: 100,

					listeners: {
						afterload: function(formPanel) {
							if (TCG.isTrue(formPanel.getFormValue('orderAllocation.security.currency', false, false))) {
								formPanel.setFieldLabel('orderAllocation.security.label', 'Given CCY:');
								formPanel.setFieldLabel('quantity', 'Given Amount:');
								formPanel.setFieldLabel('price', 'Exchange Rate:');
							}
						}
					},

					items: [
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [
								{
									rows: [
										{
											fieldLabel: 'Side', name: 'orderAllocation.buy', xtype: 'displayfield', width: 125,
											setRawValue: function(v) {
												this.el.addClass(TCG.isTrue(v) ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, TCG.isTrue(v) ? 'BUY' : 'SELL');
											}
										},
										{fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Security', name: 'orderAllocation.security.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'orderAllocation.security.id'},
										{fieldLabel: 'Settle CCY', name: 'orderAllocation.settlementCurrency.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'orderAllocation.settlementCurrency.id'},
										{fieldLabel: 'Price', name: 'price', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Order Destination', name: 'orderPlacement.orderDestination.name', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', detailIdField: 'orderPlacement.orderDestination.id'},
										{fieldLabel: 'Executing Broker', name: 'orderPlacement.executingBrokerCompany.name', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', detailIdField: 'orderPlacement.executingBrokerCompany.id'}
									],
									config: {columnWidth: 0.5}
								},
								{
									rows: [
										{fieldLabel: 'Order Allocation ID', name: 'orderAllocation.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.allocation.OrderAllocationWindow', detailIdField: 'orderAllocation.id'},
										{fieldLabel: 'Block Order ID', name: 'orderAllocation.orderBlockIdentifier', xtype: 'linkfield', detailPageClass: 'Clifton.order.execution.OrderBlockWindow', detailIdField: 'orderAllocation.orderBlockIdentifier'},
										{fieldLabel: 'Placement ID', name: 'orderPlacement.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.execution.OrderPlacementWindow', detailIdField: 'orderPlacement.id'},
										{fieldLabel: 'Created By', name: 'orderAllocation.orderCreatorUser.label', xtype: 'linkfield', detailIdField: 'orderAllocation.orderCreatorUser.id', detailPageClass: 'Clifton.security.user.UserWindow', qtip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'},
										{fieldLabel: 'Trader', name: 'orderPlacement.orderBlock.traderUser.label', xtype: 'linkfield', detailIdField: 'orderPlacement.orderBlock.traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Trade Date', name: 'orderPlacement.orderBlock.tradeDate', xtype: 'datefield', readOnly: true},
										{fieldLabel: 'Settlement Date', name: 'orderPlacement.orderBlock.settlementDate', xtype: 'datefield', readOnly: true}
									],
									config: {columnWidth: 0.5}
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Client Account', name: 'orderAllocation.clientAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'orderAllocation.clientAccount.id'},
						{fieldLabel: 'Holding Account', name: 'orderAllocation.holdingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'orderAllocation.holdingAccount.id'},
						{xtype: 'label', html: '<hr/>'}
					]
				}]
			}
		]
	}]
})
;
