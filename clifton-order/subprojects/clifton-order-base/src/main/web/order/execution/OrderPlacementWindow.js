Clifton.order.execution.OrderPlacementWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderPlacement.json',

	getClassName: function(config, entity) {
		let clz = Clifton.order.execution.OrderPlacementWindowOverrides[entity.orderDestination.destinationType.name];
		if (TCG.isNull(clz)) {
			clz = 'Clifton.order.execution.BaseOrderPlacementWindow';
		}
		return clz;
	}
});
