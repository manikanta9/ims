Clifton.order.execution.PlacementAllocationListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderPlacementAllocationListFind',
	xtype: 'gridpanel',
	wikiPage: 'IT/Order+Life+Cycle',
	instructions: 'Order Placement Allocations are the post execution account allocated amounts for a placement.',
	viewNames: ['Default View', 'Currency View'],
	columns: [
		{header: 'ID', width: 24, dataIndex: 'id', hidden: true},
		{header: 'Order Type', width: 22, dataIndex: 'orderAllocation.orderType.name', filter: {type: 'combo', searchFieldName: 'orderTypeId', url: 'orderTypeListFind.json'}, viewNames: ['Default View']},
		{header: 'Client', width: 80, dataIndex: 'orderAllocation.clientAccount.clientCompany.name', hidden: true, filter: {searchFieldName: 'clientCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Client'}},
		{header: 'Client Account', width: 80, dataIndex: 'orderAllocation.clientAccount.label', filter: {type: 'combo', searchFieldName: 'clientAccountId', displayField: 'label', url: 'orderAccountListFind.json?clientAccount=true'}, viewNames: ['Default View', 'Currency View']},
		{header: 'Business Service', width: 80, dataIndex: 'orderAllocation.clientAccount.service.name', hidden: true, filter: {searchFieldName: 'serviceOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Account Service', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{header: 'Holding Account', width: 80, dataIndex: 'orderAllocation.holdingAccount.label', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'orderAccountListFind.json?holdingAccount=true'}},
		{header: 'Holding Account #', width: 30, dataIndex: 'orderAllocation.holdingAccount.accountNumber', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'orderAccountListFind.json?holdingAccount=true'}},
		{header: 'Holding Account Name', width: 50, dataIndex: 'orderAllocation.holdingAccount.labelShort', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'orderAccountListFind.json?ourAccount=false'}},
		{header: 'Holding Account Issuer', width: 60, dataIndex: 'orderAllocation.holdingAccount.issuingCompany.name', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'orderCompanyListFind.json?holdingAccountIssuer=true'}}, // TODO ADD FILTER: HOLDING ACCOUNT ISSUER = TRUE
		{
			header: 'Side', width: 15, dataIndex: 'orderAllocation.buy', type: 'boolean', filter: {searchFieldName: 'buy'}, allViews: true,
			exportColumnValueConverter: 'orderBuyColumnReversableConverter',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Quantity', width: 20, dataIndex: 'quantity', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given Amount'},
				{name: 'Default View', label: 'Quantity'}
			]
		},
		{header: 'Security Type', width: 45, dataIndex: 'orderAllocation.security.securityType.text', hidden: true, filter: {searchFieldName: 'securityTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Security Type', valueField: 'id'}},
		{
			header: 'Security', width: 45, dataIndex: 'orderAllocation.security.ticker', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'orderSecurityListFind.json'},
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given CCY'},
				{name: 'Default View', label: 'Security'}
			]
		},
		{header: 'Security Name', width: 45, dataIndex: 'orderAllocation.security.name', filter: {searchFieldName: 'securityName'}, viewNames: ['Default View']},
		{header: 'Underlying Security', width: 40, dataIndex: 'orderAllocation.security.underlyingSecurity.ticker', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'orderSecurityListFind.json'}, hidden: true},
		{header: 'Security Hierarchy', width: 80, dataIndex: 'orderAllocation.security.securityHierarchy.nameExpanded', hidden: true, filter: {searchFieldName: 'securityHierarchyOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Security Hierarchy', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{
			header: 'Price', width: 25, dataIndex: 'price', type: 'float', useNull: true,
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'FX Rate'},
				{name: 'Default View', label: 'Price'}
			]
		},
		{header: 'Order Destination', width: 40, dataIndex: 'orderPlacement.orderDestination.name', filter: {type: 'combo', searchFieldName: 'orderDestinationId', url: 'orderDestinationListFind.json?executionVenue=true'}, allViews: true},
		{header: 'Executing Broker', width: 60, dataIndex: 'orderPlacement.executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker'}, allViews: true},
		{header: 'Trader', width: 24, dataIndex: 'orderPlacement.orderBlock.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', displayField: 'label', url: 'securityUserListFind.json'}, allViews: true},
		{header: 'Traded On', width: 23, dataIndex: 'orderPlacement.orderBlock.tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc', filter: {searchFieldName: 'tradeDate'}, allViews: true},
		{header: 'Settled On', width: 23, dataIndex: 'orderPlacement.orderBlock.settlementDate', filter: {searchFieldName: 'settlementDate'}, allViews: true}
	],
	switchToViewBeforeReload: function(viewName) {
		// If no toolbar field than inside a detail window, i.e. block order and not necessary to switch this filter
		const otField = TCG.getChildByName(this.getTopToolbar(), 'orderType');
		if (otField) {
			if (viewName === 'Currency View') {
				TCG.data.getDataPromiseUsingCaching('orderTypeByName.json?requestedPropertiesRoot=data&requestedProperties=id|name&name=Currency', this, 'order.type.Currency')
					.then(function(data) {
						const record = {text: data.name, value: data.id};
						otField.setValue(record);
						otField.fireEvent('select', otField, record);
					});
				return true; // cancel reload: select event will reload
			}
			else {
				this.clearFilter('orderAllocation.orderType.name', true);
				otField.clearValue();
			}
		}
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Order Type', xtype: 'toolbar-combo', width: 150, name: 'orderType', url: 'orderTypeListFind.json', linkedFilter: 'orderAllocation.orderType.name', linkedField: 'orderAllocation.orderType.name'}
		];
	},
	getDefaultDaysBack: function() {
		return 8;
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to last 8 days of trades
			this.setFilterValue('orderPlacement.orderBlock.tradeDate', {'after': new Date().add(Date.DAY, -this.getDefaultDaysBack())});
		}
		return {readUncommittedRequested: true};
	},
	editor: {
		detailPageClass: 'Clifton.order.execution.PlacementAllocationWindow',
		drillDownOnly: true
	}

});
Ext.reg('order-placement-allocation-list-grid', Clifton.order.execution.PlacementAllocationListGrid);
