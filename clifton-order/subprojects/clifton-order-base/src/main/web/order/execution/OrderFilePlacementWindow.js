TCG.use('Clifton.order.execution.BaseOrderPlacementWindow');

Clifton.order.execution.OrderFilePlacementWindow = Ext.extend(Clifton.order.execution.BaseOrderPlacementWindow, {

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		tabs.add(this.batchesTab);
	},

	batchesTab: {
		title: 'Order Batches',
		items: [{
			xtype: 'order-batch-item-list-grid',
			tableName: 'OrderPlacement',
			instructions: 'A list of all order batches in the system this placement is included in. Order Batches are a batch of placements that are "batched" and then sent to a destination.  These can be used for execution where we need to send a file to the external party that does not support FIX connectivity.'
		}]
	}
});
