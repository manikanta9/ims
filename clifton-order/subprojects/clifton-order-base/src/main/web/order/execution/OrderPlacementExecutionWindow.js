// Used to enter the fill information for the Order Placement.  Eventually we can add more here for when placements are partially filled to handle how to allocate to accounts
// if they need manual adjustments.
Clifton.order.execution.OrderPlacementExecutionWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Order Placement',
	titlePrefixSeparator: ': ',
	width: 500,
	height: 300,
	iconCls: 'shopping-cart',

	defaultDataIsReal: true,
	items: [
		{
			xtype: 'formpanel',
			url: 'orderPlacement.json',
			labelFieldName: 'labelShort',
			maxWindowTitleLength: 100,
			getSaveURL: function() {
				return 'orderPlacementManualExecutionSave.json';
			},
			listeners: {
				afterload: function(formPanel) {
					if (TCG.isTrue(formPanel.getFormValue('orderBlock.security.currency', false, false))) {
						formPanel.setFieldLabel('orderBlock.security.label', 'Given CCY:');
						formPanel.setFieldLabel('quantityIntended', 'Given Amount:');
						formPanel.setFieldLabel('quantityFilled', ' Filled Amount:');
						formPanel.setFieldLabel('quantityUnfilled', ' Remaining Amount:');
						formPanel.setFieldLabel('averageUnitPrice', ' Exchange Rate:');
					}
				}
			},

			items: [
				{fieldLabel: 'Placement ID', name: 'id', readOnly: true, submitValue: false},
				{fieldLabel: 'Quantity', name: 'quantityIntended', xtype: 'floatfield', readOnly: true},
				{
					fieldLabel: 'Filled Qty', name: 'quantityFilled', xtype: 'floatfield', allowBlank: false,
					listeners: {
						'change': function(field, newValue, oldValue) {
							const fp = TCG.getParentFormPanel(field);
							fp.setFormValue('quantityUnfilled', TCG.subtractPrecise(fp.getFormValue('quantityIntended'), TCG.parseFloat(newValue)));
						}
					}
				},
				{fieldLabel: 'Unfilled Qty', name: 'quantityUnfilled', xtype: 'floatfield', readOnly: true},
				{fieldLabel: 'Avg Price', name: 'averageUnitPrice', xtype: 'floatfield', allowBlank: false},
				{
					fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', allowBlank: false, detailPageClass: 'Clifton.order.shared.company.CompanyWindow', url: 'orderManagementRuleExecutingBrokerCompanyListForOrderPlacement.json',
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.store.setBaseParam('orderPlacementId', TCG.getParentFormPanel(combo).getWindow().getMainFormId());
					}
				}
			]

		}
	]
});
