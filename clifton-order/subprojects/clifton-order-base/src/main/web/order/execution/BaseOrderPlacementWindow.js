Clifton.order.execution.BaseOrderPlacementWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Placement',
	titlePrefixSeparator: ': ',
	width: 1200,
	height: 700,
	iconCls: 'shopping-cart',
	enableRefreshWindow: true,

	reload: function() {
		this.refreshWindow();
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				layout: 'border',
				tbar: [
					{
						text: 'Manual Fill',
						iconCls: 'calculator',
						tooltip: 'Manually enter fill information',
						handler: function() {
							const fp = this.ownerCt.ownerCt.items.get(0);
							if (TCG.isTrue(fp.getFormValue('executionStatus.fillComplete'))) {
								TCG.showError('The fill for this placement is already completed.');
								return;
							}
							if (TCG.isEquals('FIX', fp.getFormValue('orderDestination.destinationType.destinationCommunicationType'))) {
								TCG.showError('FIX Orders cannot be manually filled');
								return;
							}
							const defaultData = fp.getForm().formValues;
							// Default to Fully Filled
							defaultData.quantityFilled = defaultData.quantityIntended;
							defaultData.quantityUnfilled = 0;
							const className = 'Clifton.order.execution.OrderPlacementExecutionWindow';
							const cmpId = TCG.getComponentId(className, defaultData.id);
							TCG.createComponent(className, {
								id: cmpId,
								defaultData: defaultData,
								openerCt: fp.getWindow()
							});
						}
					},
					'-',
					{
						text: 'Amend Execution',
						iconCls: 'shopping-cart',
						tooltip: 'Amend Execution Information',
						handler: function() {
							const fp = this.ownerCt.ownerCt.items.get(0);
							if (TCG.isFalse(fp.getFormValue('executionStatus.fillComplete'))) {
								TCG.showError('The fill for this placement is incomplete. Amendments can only be applied to fully executed placements');
								return;
							}
							const defaultData = fp.getForm().formValues;
							// Default to Fully Filled
							defaultData.overrideTradeDate = defaultData.orderBlock.tradeDate;
							defaultData.overrideSettlementDate = defaultData.orderBlock.settlementDate;
							defaultData.overrideAverageUnitPrice = defaultData.averageUnitPrice;
							defaultData.orderPlacementId = defaultData.id;
							const className = 'Clifton.order.management.execution.PlacementPostExecutionAmendWindow';
							const cmpId = TCG.getComponentId(className, defaultData.id);
							TCG.createComponent(className, {
								id: cmpId,
								defaultData: defaultData,
								openerCt: fp.getWindow()
							});
						}
					}
					, '-', {
						text: 'Help',
						iconCls: 'help',
						handler: function() {
							TCG.openWIKI('IT/Order+Placements', 'Order Placements');
						}
					}
				],

				items: [
					{
						xtype: 'formpanel',
						url: 'orderPlacement.json',
						readOnly: true,
						labelFieldName: 'labelShort',
						maxWindowTitleLength: 100,
						loadDefaultDataAfterRender: true,
						region: 'north',
						height: 240,
						labelWidth: 150,
						listeners: {
							afterload: function(formPanel) {
								formPanel.hideFieldIfBlank('orderDestinationConfiguration.name');
								if (TCG.isTrue(formPanel.getFormValue('orderBlock.security.currency', false, false))) {
									formPanel.setFieldLabel('orderBlock.security.label', 'Given CCY:');
									formPanel.setFieldLabel('quantityIntended', 'Given Amount:');
									formPanel.setFieldLabel('quantityFilled', ' Filled Amount:');
									formPanel.setFieldLabel('quantityUnfilled', ' Unfilled Amount:');
									formPanel.setFieldLabel('averageUnitPrice', ' Exchange Rate:');
								}
							}
						},

						items: [
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [{
									rows: [
										{
											fieldLabel: 'Side', name: 'orderBlock.buy', xtype: 'displayfield', width: 125,
											setRawValue: function(v) {
												if (this.el) {
													this.el.addClass(TCG.isTrue(v) ? 'buy' : 'sell');
												}
												TCG.form.DisplayField.superclass.setRawValue.call(this, TCG.isTrue(v) ? 'BUY' : 'SELL');
											}
										},
										{fieldLabel: 'Security', name: 'orderBlock.security.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'orderBlock.security.id'},
										{fieldLabel: 'Settle CCY', name: 'orderBlock.settlementCurrency.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'orderBlock.settlementCurrency.id'},
										{fieldLabel: 'Trader', name: 'orderBlock.traderUser.label', xtype: 'linkfield', detailIdField: 'orderBlock.traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'}
									],
									config: {columnWidth: 0.4}
								}, {
									rows: [
										{fieldLabel: 'Quantity', name: 'quantityIntended', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Filled Qty', name: 'quantityFilled', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Unfilled Qty', name: 'quantityUnfilled', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Avg Price', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true}
									],
									config: {columnWidth: 0.3}
								}, {
									rows: [
										{fieldLabel: 'Placement ID', name: 'id'},
										{fieldLabel: 'Block Order ID', name: 'orderBlock.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.execution.OrderBlockWindow', detailIdField: 'orderBlock.id'},
										{fieldLabel: 'Trade Date', name: 'orderBlock.tradeDate', xtype: 'datefield', readOnly: true},
										{fieldLabel: 'Settlement Date', name: 'orderBlock.settlementDate', xtype: 'datefield', readOnly: true}
									],
									config: {columnWidth: 0.3}
								}]
							},
							{xtype: 'label', html: '<hr/>'},
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [{
									rows: [
										{fieldLabel: 'Order Destination', name: 'orderDestination.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', detailIdField: 'orderDestination.id'},
										{fieldLabel: 'Destination Configuration', name: 'orderDestinationConfiguration.name', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.destination.DestinationConfigurationWindow', detailIdField: 'orderDestinationConfiguration.id'},
										{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', detailIdField: 'executingBrokerCompany.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow'}
									],
									config: {columnWidth: 0.5}
								}, {
									rows: [
										{fieldLabel: 'Execution Status', name: 'executionStatus.name', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.ExecutionStatusWindow', detailIdField: 'executionStatus.id'},
										{fieldLabel: 'Allocation Status', name: 'allocationStatus.name', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.AllocationStatusWindow', detailIdField: 'allocationStatus.id'}
									],
									config: {columnWidth: 0.5}
								}]
							},
							{xtype: 'order-executing-broker-list', url: 'orderManagementRuleExecutingBrokerCompanyListForOrderPlacement.json', idParamName: 'orderPlacementId'}
						]
					},

					{flex: 0.02},
					{
						title: 'Order Allocations',
						xtype: 'order-allocation-list-grid',
						flex: 0.49,
						region: 'center',
						instructions: 'The following Order Allocations are available for this Placement.',
						plugins: {ptype: 'gridsummary'},
						columnOverrides: [
							{dataIndex: 'orderType.name', hidden: true, viewNames: undefined},
							{dataIndex: 'buy', hidden: true, viewNames: ['Export Friendly', 'Currency View']},
							{dataIndex: 'openCloseType.label', hidden: false, viewNames: ['Default View', 'Export Friendly']},
							{dataIndex: 'security.name', hidden: true, viewNames: undefined},
							{dataIndex: 'averageUnitPrice', hidden: true, viewNames: undefined},
							{dataIndex: 'traderUser.label', hidden: true, viewNames: undefined},
							{dataIndex: 'tradeDate', hidden: true, viewNames: undefined},
							{dataIndex: 'settlementDate', hidden: true, viewNames: undefined},
							{dataIndex: 'orderDestination.name', hidden: true, viewNames: undefined}
						],
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								if (TCG.isTrue(this.getWindow().getMainFormPanel().getFormValue('orderBlock.security.currency', false, false))) {
									this.setDefaultView('Currency View'); // sets it as "checked"
									this.switchToView('Currency View', true);
								}
							}
							return {orderBlockIdentifier: TCG.getValue('orderBlock.id', this.getWindow().getMainForm().formValues)};
						}
					}
				]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'OrderPlacement',
					defaultActiveFilter: false,
					// Set to true to see notes for related entities - i.e. on Placements also see block order and order allocation notes
					includeNotesForLinkedEntity: true
				}]
			},
			{
				title: 'Placement Allocation(s)',
				xtype: 'order-placement-allocation-list-grid',
				instructions: 'This placement was allocated to the following Order Allocation(s).',
				plugins: {ptype: 'gridsummary'},
				getTopToolbarFilters: function(toolbar) {
					return [];
				},
				columnOverrides: [
					{dataIndex: 'orderAllocation.orderType.name', hidden: true},
					{dataIndex: 'orderAllocation.buy', hidden: true},
					{dataIndex: 'orderAllocation.security.ticker', hidden: true},
					{dataIndex: 'orderAllocation.security.name', hidden: true},
					{dataIndex: 'quantity', summaryType: 'sum'},
					{dataIndex: 'orderPlacement.orderDestination.name', hidden: true, allViews: false},
					{dataIndex: 'orderPlacement.executingBrokerCompany.name', hidden: true, allViews: false},
					{dataIndex: 'orderPlacement.orderBlock.tradeDate', hidden: true, allViews: false},
					{dataIndex: 'orderPlacement.orderBlock.settlementDate', hidden: true, allViews: false}
				],

				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						if (TCG.isTrue(this.getWindow().getMainFormPanel().getFormValue('orderBlock.security.currency', false, false))) {
							this.setDefaultView('Currency View'); // sets it as "checked"
							this.switchToView('Currency View', true);
						}
					}
					return {orderPlacementId: this.getWindow().getMainFormId()};
				},
				addToolbarButtons: function(toolbar, gridPanel) {
					toolbar.add({
						text: 'Preview',
						tooltip: 'Preview Calculated Allocations for Placement.  This can be useful for previewing the account allocations before the placement is filled and allocations are created',
						iconCls: 'preview',
						handler: function() {
							gridPanel.openPreviewWindow();
						}
					});
					toolbar.add('-');
				},
				openPreviewWindow: function() {
					const gridPanel = this;
					const w = gridPanel.getWindow();
					const id = w.getMainFormId();
					TCG.createComponent('TCG.app.CloseWindow', {
						title: 'Placement Allocation(s) Preview',
						iconCls: 'preview',
						height: 500,
						width: 1000,
						openerCt: gridPanel,
						items: [{
							xtype: 'order-placement-allocation-list-grid',
							getLoadParams: function(firstLoad) {
								return {orderPlacementId: id};
							},
							name: 'orderPlacementAllocationListForPlacementPreview',
							getTopToolbarFilters: function(toolbar) {
								return [];
							},
							editor: undefined,
							columnOverrides: [
								{dataIndex: 'orderAllocation.orderType.name', hidden: true},
								{dataIndex: 'orderAllocation.buy', hidden: false},
								{dataIndex: 'orderAllocation.security.ticker', hidden: true},
								{dataIndex: 'orderAllocation.security.name', hidden: true},
								{dataIndex: 'quantity', summaryType: 'sum'},
								{dataIndex: 'orderPlacement.orderDestination.name', hidden: true, allViews: false},
								{dataIndex: 'orderPlacement.executingBrokerCompany.name', hidden: true, allViews: false},
								{dataIndex: 'orderPlacement.orderBlock.tradeDate', hidden: true, allViews: false},
								{dataIndex: 'orderPlacement.orderBlock.settlementDate', hidden: true, allViews: false},
								{dataIndex: 'orderPlacement.orderBlock.traderUser.label', hidden: true}
							]
						}]
					});
				}
			}


		]
	}]
});
