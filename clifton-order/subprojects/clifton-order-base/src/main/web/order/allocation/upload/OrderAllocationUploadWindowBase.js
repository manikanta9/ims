Clifton.order.allocation.upload.OrderAllocationUploadWindowBase = Ext.extend(TCG.app.DetailWindow, {
	title: 'Order Allocation Import',
	iconCls: 'import',
	height: 600,
	width: 800,
	hideOKButton: true,
	saveTimeout: 240,
	applyDisabled: false,
	applyButtonText: 'Upload',
	applyButtonTooltip: 'Upload selected file',

	init: function() {
		// replace the first tab with the override
		const tabs = this.items[0].items;
		tabs[0] = this.importTab;
		Clifton.order.allocation.upload.OrderAllocationUploadWindowBase.superclass.init.apply(this, arguments);
	},

	importTab: {
		title: 'OVERRIDE ME',
		items: [{}]
	},

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{}, // first tab to be overridden
			{
				title: 'Import History',
				items: [{
					name: 'orderGroupListFind',
					xtype: 'gridpanel',
					instructions: 'The following order allocation groups have been imported into the system.',
					columns: [
						{header: 'Group #', width: 10, dataIndex: 'id'},
						{header: 'Workflow Status', width: 20, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Order Allocation'}, hidden: true},
						{header: 'Workflow State', width: 20, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, hidden: true},
						{header: 'Note', width: 70, dataIndex: 'note', filter: false},
						{header: 'Created By', width: 23, dataIndex: 'orderCreatorUser.label', filter: {type: 'combo', searchFieldName: 'orderCreatorUserId', displayField: 'label', url: 'securityUserListFind.json'}, tooltip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'},
						{header: 'Traded On', width: 18, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of trades
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -30)});
						}
						return {
							import: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.order.group.OrderGroupWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
