TCG.use('Clifton.order.allocation.BaseOrderAllocationWindow');
TCG.use('Clifton.order.allocation.BaseOrderAllocationForm');

Clifton.order.allocation.CurrencyOrderAllocationWindow = Ext.extend(Clifton.order.allocation.BaseOrderAllocationWindow, {
	titlePrefix: 'Currency Order Allocation',
	iconCls: 'currency',
	orderType: 'Currency',


	orderAllocationInfoTab: {
		title: 'Info',
		tbar: {
			xtype: 'order-allocation-workflow-toolbar'
		},
		items: [{
			xtype: 'order-allocation-form-base',
			labelWidth: 110,


			updateSettlementAmount: function(multiply) {
				const fp = this;
				const f = fp.getForm();
				const v = f.findField('accountingNotional').getNumericValue();
				const fx = f.findField('exchangeRateToSettlementCurrency').getNumericValue();
				if (TCG.isNumber(v) && TCG.isNumber(fx)) {
					if (TCG.isNotBlank(multiply)) {
						fp.setFormValue('settlementAmount', TCG.isTrue(multiply) ? v * fx : v / fx, true);
					}
					else {
						const fromCurrencyCode = f.findField('security.ticker').getValue();
						const toCurrencyCode = f.findField('settlementCurrency.ticker').getValue();
						TCG.data.getDataPromiseUsingCaching('orderMarketDataCurrencyConvention.json?fromCurrencyCode=' + fromCurrencyCode + '&toCurrencyCode=' + toCurrencyCode, this, 'currency.convention.' + fromCurrencyCode + toCurrencyCode)
							.then(function(currencyConvention) {
								if (currencyConvention) {
									multiply = TCG.isTrue(currencyConvention.multiplyByExchangeRate);
								}
								else {
									multiply = true;
								}
								fp.setFormValue('settlementAmount', TCG.isTrue(multiply) ? v * fx : v / fx, true);
							});
					}
				}
			},
			updateExchangeRate: async function() {
				const fp = this;
				const form = fp.getForm();
				const fxField = form.findField('exchangeRateToSettlementCurrency');
				const fromCurrencyCode = form.findField('security.ticker').getValue();
				const toCurrencyCode = form.findField('settlementCurrency.ticker').getValue();
				const rateDate = form.findField('tradeDate').value || form.findField('settlementDate').value;
				TCG.data.getDataPromise('orderMarketDataExchangeRateFlexible.json?fromCurrencyCode=' + fromCurrencyCode + '&toCurrencyCode=' + toCurrencyCode + '&date=' + rateDate + '&throwExceptionIfMissing=' + true, this)
					.then(function(exchangeRateData) {
						if (exchangeRateData) {
							fxField.setValue(exchangeRateData.exchangeRate);
							// Not necessary to look up the convention since we have it on the rate
							// If from on the rate = from on what we asked for then we multiply, else divide
							fp.updateSettlementAmount(TCG.isEquals(fromCurrencyCode, exchangeRateData.fromCurrencyCode));
						}
					});
			},


			items: [],

			updatableItems: [
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', defaults: {anchor: '-20'}},
					items: [
						{
							columnWidth: .71,
							items: [
								{xtype: 'hidden', name: 'clientAccount.clientCompany.id', submitValue: false},
								{
									fieldLabel: 'Client Account',
									name: 'clientAccount.label',
									hiddenName: 'clientAccount.id',
									displayField: 'label',
									xtype: 'combo',
									url: 'orderAccountListFind.json?clientAccount=true&workflowStatusNameEquals=Active',
									allowBlank: false,
									anchor: '-20',
									requestedProps: 'holdingAccount|baseCurrencyCode|clientCompany.id',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									listeners: {
										select: function(combo, record, index) {
											const fp = TCG.getParentFormPanel(combo);
											const currencyCode = record.json.baseCurrencyCode;
											if (currencyCode) {
												fp.setFormValue('clientAccount.baseCurrencyCode', currencyCode, true);
												TCG.data.getDataPromiseUsingCaching('orderSecurityForCurrencyTicker.json?ticker=' + currencyCode, fp, 'order.shared.security.currency.' + currencyCode)
													.then(function(curr) {
														fp.setFormValue('settlementCurrency.id', {value: curr.id, text: curr.label});
														fp.setFormValue('settlementCurrency.ticker', currencyCode, true);
													});
											}

											const clientCompanyId = record.json.clientCompany.id;
											fp.setFormValue('clientAccount.clientCompany.id', clientCompanyId);
											fp.updateHoldingAccountSelection(record);
										}
									}
								},
								{
									fieldLabel: 'Holding Account',
									name: 'holdingAccount.label',
									hiddenName: 'holdingAccount.id',
									displayField: 'label',
									xtype: 'combo',
									url: 'orderAccountListFind.json?holdingAccount=true',
									allowBlank: false,
									anchor: '-20',
									requiredFields: ['clientAccount.label'],
									requestedProps: 'baseCurrencyCode',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									beforeSelect: function(combo, record) {
										const fp = combo.getParentForm();
										const f = fp.getForm();
										const executingBrokerField = f.findField('executingBrokerCompany.label');
										const currencyCode = record.json.baseCurrencyCode;
										if (currencyCode) {
											fp.setFormValue('holdingAccount.baseCurrencyCode', currencyCode);
											TCG.data.getDataPromiseUsingCaching('orderSecurityForCurrencyTicker.json?ticker=' + currencyCode, fp, 'order.shared.security.currency.' + currencyCode)
												.then(function(curr) {
													fp.setFormValue('settlementCurrency.id', {value: curr.id, text: curr.label});
													fp.setFormValue('settlementCurrency.ticker', currencyCode);
												});
										}
										executingBrokerField.store.removeAll();
										executingBrokerField.lastQuery = null;
									},
									listeners: {
										select: function(combo, record) {
											combo.beforeSelect(combo, record);
										},
										beforequery: function(queryEvent) {
											const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
											const clientCompanyId = form.findField('clientAccount.clientCompany.id').value;
											queryEvent.combo.store.setBaseParam('clientCompanyId', clientCompanyId);
										}
									}
								}
							]
						},
						{
							columnWidth: .29,
							labelWidth: 60,
							items: [
								{
									fieldLabel: 'Base CCY',
									name: 'clientAccount.baseCurrencyCode',
									submitValue: false,
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow',
									useNameFieldValueForDetailIdFieldValue: true
								},
								{
									fieldLabel: 'Base CCY',
									name: 'holdingAccount.baseCurrencyCode',
									submitValue: false,
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow',
									useNameFieldValueForDetailIdFieldValue: true
								}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
					items: [
						{
							columnWidth: .31,
							labelWidth: 110,
							items: [
								{
									xtype: 'radiogroup',
									fieldLabel: 'Buy/Sell',
									columns: [60, 60],
									allowBlank: false,
									anchor: '-25',
									items: [
										{boxLabel: 'Buy', name: 'buy', inputValue: true},
										{boxLabel: 'Sell', name: 'buy', inputValue: false}
									],
									listeners: {
										change: function(radioGroup, checkedRadio) {
											const formPanel = TCG.getParentFormPanel(radioGroup);
											const params = {
												open: true,
												close: true,
												buy: checkedRadio.getRawValue()
											};
											TCG.data.getDataPromise('orderOpenCloseTypeListFind.json', radioGroup, {params: params})
												.then(function(openCloseType) {
													if (openCloseType && openCloseType.length === 1) {
														formPanel.setFormValue('openCloseType.id', openCloseType[0].id);
													}
												});
										}
									}
								},
								{xtype: 'hidden', name: 'openCloseType.id'},
								{xtype: 'hidden', name: 'notionalMultiplier', value: 1}, // TODO SERVER SIDE LOGIC?
								{name: 'security.ticker', xtype: 'hidden', submitValue: false},
								{
									fieldLabel: 'Given CCY',
									name: 'security.label',
									allowBlank: false,
									displayField: 'label',
									hiddenName: 'security.id',
									xtype: 'combo',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									url: 'orderSecurityListFind.json?securityType=currency',
									requiredFields: ['holdingAccount.label'],
									doNotClearIfRequiredSet: true,
									requestedProps: 'ticker',
									listeners: {
										beforequery: function(queryEvent) {
											const f = queryEvent.combo.getParentForm().getForm();
											queryEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value};
										},

										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											const form = fp.getForm();
											fp.setFormValue('security.ticker', TCG.getValue('ticker', record.json));

											Clifton.order.shared.security.getTradeDatePromise(record.id, null, combo)
												.then(function(tradeDate) {
													form.findField('tradeDate').setValue(tradeDate);
													return Clifton.order.shared.security.getSettlementDatePromise(record.id, tradeDate, false, form.findField('settlementCurrency.id').value, combo);
												})
												.then(function(settlementDate) {
													form.findField('settlementDate').setValue(settlementDate);
													fp.updateExchangeRate.call(fp);
												});

										}
									}
								},
								{
									fieldLabel: 'Given Amount', name: 'accountingNotional', xtype: 'currencyfield4', allowBlank: false, minValue: 0,
									listeners: {
										change: function(field, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(field);
											fp.updateExchangeRate.call(fp); // exchange rate update will update settlement amount
										}
									}
								},
								{name: 'settlementCurrency.ticker', xtype: 'hidden'},
								{
									fieldLabel: 'Settle CCY',
									name: 'settlementCurrency.label',
									hiddenName: 'settlementCurrency.id',
									allowBlank: false,
									displayField: 'label',
									xtype: 'combo',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									url: 'orderSecurityListFind.json?securityType=currency',
									requiredFields: ['holdingAccount.label'],
									doNotClearIfRequiredSet: true,
									requestedProps: 'ticker',
									listeners: {
										beforeQuery: function(qEvent) {
											const f = qEvent.combo.getParentForm().getForm();
											qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value};
										},
										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											const form = fp.getForm();
											fp.setFormValue('settlementCurrency.ticker', TCG.getValue('ticker', record.json));

											Clifton.order.shared.security.getSettlementDatePromise(form.findField('security.id').value, form.findField('tradeDate').value, false, record.id, combo)
												.then(function(settlementDate) {
													form.findField('settlementDate').setValue(settlementDate);
													fp.updateExchangeRate.call(fp);
												});

										}
									}
								},
								{fieldLabel: 'Settle Amount', name: 'settlementAmount', xtype: 'currencyfield4', readOnly: true}
							]
						},
						{
							columnWidth: .40,
							labelWidth: 100,
							items: [
								{
									fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo',
									requiredFields: ['holdingAccount.label'],
									displayField: 'label',
									detailPageClass: 'Clifton.order.shared.company.CompanyWindow',
									url: 'orderManagementRuleExecutingBrokerCompanyListFind.json',
									allowBlank: true,
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const fp = combo.getParentForm();

											combo.store.setBaseParam('command.clientAccountId', fp.getFormValue('clientAccount.id', true));
											combo.store.setBaseParam('command.holdingAccountId', fp.getFormValue('holdingAccount.id', true));
											let tradeDate = fp.getFormValue('tradeDate', true);
											if (TCG.isNotBlank(tradeDate)) {
												tradeDate = tradeDate.format('m/d/Y');
											}
											combo.store.setBaseParam('command.tradeDate', tradeDate);
											combo.store.setBaseParam('command.orderTypeId', fp.getFormValue('orderType.id'));
											combo.store.setBaseParam('command.orderSecurityId', fp.getFormValue('security.id', true));
											combo.store.setBaseParam('command.settlementCurrencyId', fp.getFormValue('settlementCurrency.id', true));
											combo.store.setBaseParam('orderDestinationId', fp.getFormValue('orderDestination.id', true));
										},
										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											// clear existing destination query (but leave value that is there selected)
											const od = fp.getForm().findField('orderDestination.label');
											od.resetStore();
										}

									}
								},
								{
									fieldLabel: 'Destination',
									name: 'orderDestination.label',
									hiddenName: 'orderDestination.id',
									xtype: 'combo',
									disableAddNewItem: true,
									detailPageClass: 'Clifton.order.setup.destination.DestinationWindow',
									url: 'orderManagementDestinationListForMapping.json',
									allowBlank: true,
									loadAll: true,
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const fp = combo.getParentForm();
											if (TCG.isNotBlank(fp.getFormValue('executingBrokerCompany.id', true))) {
												combo.store.setBaseParam('executingBrokerCompanyIdOrNull', fp.getFormValue('executingBrokerCompany.id', true));
											}
											combo.store.setBaseParam('orderTypeId', fp.getFormValue('orderType.id', true));
											let tradeDate = fp.getFormValue('tradeDate', true);
											if (TCG.isNotBlank(tradeDate)) {
												tradeDate = tradeDate.format('m/d/Y');
											}
											combo.store.setBaseParam('activeOnDate', tradeDate);
										},
										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											// clear existing executing broker query (but leave value that is there selected)
											const eb = fp.getForm().findField('executingBrokerCompany.label');
											eb.resetStore();
										}
									}

								},
								{
									fieldLabel: 'FX Rate',
									name: 'exchangeRateToSettlementCurrency',
									xtype: 'floatfield',
									allowBlank: true,
									tooltip: 'Currency exchange rates in the standard currency convention between the local currency and the settlement currency. For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency',
									listeners:
										{
											change: function(field) {
												const fp = TCG.getParentFormPanel(field);
												fp.updateSettlementAmount.call(fp);
											}
										}
								}
							]
						},
						{
							columnWidth: .29,
							labelWidth: 100,
							items: [
								{name: 'orderType.id', xtype: 'hidden'},
								{
									fieldLabel: 'Block Order ID',
									name: 'orderBlockIdentifier',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.execution.OrderBlockWindow',
									detailIdField: 'orderBlockIdentifier'
								},
								{
									fieldLabel: 'Order Group',
									name: 'orderGroup.groupType.name',
									xtype: 'linkfield',
									detailIdField: 'orderGroup.id',
									detailPageClass: 'Clifton.order.group.OrderGroupWindow'
								},
								{
									fieldLabel: 'Order Source',
									name: 'orderSourceWithIdentifier',
									xtype: 'linkfield',
									detailIdField: 'orderSource.id',
									detailPageClass: 'Clifton.order.setup.OrderSourceWindow',
									qtip: 'This field is the combination of Order Source and the Extrnal Identifier'
								},
								{
									fieldLabel: 'Created By',
									name: 'orderCreatorUser.label',
									xtype: 'linkfield',
									detailIdField: 'orderCreatorUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow',
									qtip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'
								},
								{
									fieldLabel: 'Trader',
									name: 'traderUser.label',
									xtype: 'linkfield',
									detailIdField: 'traderUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow'
								},
								{
									fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
									getParentFormPanel: function() {
										const parent = TCG.getParentFormPanel(this);
										return parent || this.ownerCt;
									},

									listeners: {
										change: function(field, newValue, oldValue) {
											const form = field.getParentFormPanel().getForm();
											const settlementDateField = form.findField('settlementDate');

											if (TCG.isBlank(newValue)) {
												settlementDateField.setValue('');
											}
											else {
												Clifton.order.shared.security.getSettlementDatePromise(form.findField('security.id').value, newValue, false, form.findField('settlementCurrency.id').value, field)
													.then(function(settlementDate) {
														form.findField('settlementDate').setValue(settlementDate);
													});
											}
										},
										select: function(field, date) {
											const form = field.getParentFormPanel().getForm();
											Clifton.order.shared.security.getSettlementDatePromise(form.findField('security.id').value, date, false, form.findField('settlementCurrency.id').value, field)
												.then(function(settlementDate) {
													form.findField('settlementDate').setValue(settlementDate);
												});
										}
									}

								},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', anchor: '-20'}
			],


			readOnlyItems: [
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', defaults: {anchor: '-20'}},
					items: [
						{
							columnWidth: .71,
							items: [
								{
									fieldLabel: 'Client Account',
									name: 'clientAccount.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									detailIdField: 'clientAccount.id'
								},
								{
									fieldLabel: 'Holding Account',
									name: 'holdingAccount.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									detailIdField: 'holdingAccount.id'
								}
							]
						},
						{
							columnWidth: .29,
							labelWidth: 60,
							items: [
								{
									fieldLabel: 'Base CCY',
									name: 'clientAccount.baseCurrencyCode',
									submitValue: false,
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow',
									useNameFieldValueForDetailIdFieldValue: true
								},
								{
									fieldLabel: 'Base CCY',
									name: 'holdingAccount.baseCurrencyCode',
									submitValue: false,
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow',
									useNameFieldValueForDetailIdFieldValue: true
								}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
					items: [
						{
							columnWidth: .31,
							labelWidth: 110,
							items: [
								{
									fieldLabel: 'Side', name: 'buy', xtype: 'displayfield', width: 140,
									setRawValue: function(v) {
										this.el.addClass(v ? 'buy' : 'sell');
										TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
									}
								},
								{
									fieldLabel: 'Given CCY',
									name: 'security.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									detailIdField: 'security.id'
								},
								{fieldLabel: 'Given Amount', name: 'accountingNotional', xtype: 'currencyfield4', readOnly: true},
								{
									fieldLabel: 'Settle CCY',
									name: 'settlementCurrency.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									detailIdField: 'settlementCurrency.id'
								},
								{fieldLabel: 'Settle Amount', name: 'settlementAmount', xtype: 'currencyfield4', readOnly: true}
							]
						},
						{
							columnWidth: .40,
							labelWidth: 100,
							items: [
								{
									fieldLabel: 'Executing Broker',
									name: 'executingBrokerCompany.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.company.CompanyWindow',
									detailIdField: 'executingBrokerCompany.id'
								},
								{
									fieldLabel: 'Destination',
									name: 'orderDestination.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.setup.destination.DestinationWindow',
									detailIdField: 'orderDestination.id'
								},
								{
									fieldLabel: 'FX Rate',
									name: 'exchangeRateToSettlementCurrency',
									xtype: 'floatfield',
									readOnly: true,
									tooltip: 'Currency exchange rates should always be the exchange rate between the dominant currency to the other, which is the industry standard convention. For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency'
								}
							]
						},
						{
							columnWidth: .29,
							labelWidth: 100,
							items: [
								{name: 'orderType.id', xtype: 'hidden'},
								{
									fieldLabel: 'Block Order ID',
									name: 'orderBlockIdentifier',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.execution.OrderBlockWindow',
									detailIdField: 'orderBlockIdentifier'
								},
								{
									fieldLabel: 'Order Group',
									name: 'orderGroup.groupType.name',
									xtype: 'linkfield',
									detailIdField: 'orderGroup.id',
									detailPageClass: 'Clifton.order.group.OrderGroupWindow'
								},
								{
									fieldLabel: 'Order Source',
									name: 'orderSourceWithIdentifier',
									xtype: 'linkfield',
									detailIdField: 'orderSource.id',
									detailPageClass: 'Clifton.order.setup.OrderSourceWindow',
									qtip: 'This field is the combination of Order Source and the Extrnal Identifier'
								},
								{
									fieldLabel: 'Created By',
									name: 'orderCreatorUser.label',
									xtype: 'linkfield',
									detailIdField: 'orderCreatorUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow',
									qtip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'
								},
								{
									fieldLabel: 'Trader',
									name: 'traderUser.label',
									xtype: 'linkfield',
									detailIdField: 'traderUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow'
								},
								{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', anchor: '-20'},
				{
					xtype: 'order-executing-broker-list', url: 'orderManagementRuleExecutingBrokerCompanyListFind.json',
					getLoadParams: function(fp) {
						const params = {};
						params['command.clientAccountId'] = fp.getFormValue('clientAccount.id', true);
						params['command.holdingAccountId'] = fp.getFormValue('holdingAccount.id', true);
						let tradeDate = fp.getFormValue('tradeDate', true);
						if (TCG.isNotBlank(tradeDate)) {
							tradeDate = tradeDate.format('m/d/Y');
						}
						params['command.tradeDate'] = tradeDate;
						params['command.orderTypeId'] = fp.getFormValue('orderType.id');
						params['command.orderSecurityId'] = fp.getFormValue('security.id', true);
						params['command.settlementCurrencyId'] = fp.getFormValue('settlementCurrency.id', true);
						params['orderDestinationId'] = fp.getFormValue('orderDestination.id', true);
						return params;
					}
				}
			]
		}
		]

	}
})
;
