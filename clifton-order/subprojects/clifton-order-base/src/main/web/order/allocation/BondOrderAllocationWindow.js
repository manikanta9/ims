TCG.use('Clifton.order.allocation.BaseOrderAllocationWindow');
TCG.use('Clifton.order.allocation.BaseOrderAllocationForm');

Clifton.order.allocation.BondOrderAllocationWindow = Ext.extend(Clifton.order.allocation.BaseOrderAllocationWindow, {
	titlePrefix: 'Bond Order Allocation',
	iconCls: 'bonds',


	orderAllocationInfoTab: {
		title: 'Info',
		tbar: {
			xtype: 'order-allocation-workflow-toolbar'
		},
		items: [{
			xtype: 'order-allocation-form-base',
			labelWidth: 125,

			listeners: {
				beforerender: function() {
					const status = TCG.getValue('workflowStatus.name', this.getWindow().defaultData);
					this.readOnlyMode = (status !== 'Draft');
					if (this.readOnlyMode) {
						this.readOnlyMode = (this.getLoadURL() !== false);
					}
					this.add(this.readOnlyMode ? this.readOnlyItems : this.updatableItems);
				},
				afterload: function(fp) {
					const ro = ('Draft' !== fp.getFormValue('workflowStatus.name'));
					fp.setReadOnlyMode(ro);

					fp.hideFieldIfBlank('orderGroup.groupType.name');
					fp.hideFieldIfBlank('settlementCompany.label');
					fp.hideFieldIfBlank('orderSourceWithIdentifier');

					fp.updateTotalPayment(); // TODO ONCE BELOW IS IMPLEMENTED THIS CAN BE REMOVED...THIS SPECIFIC FIELD DOESN'T HAVE DEPENDENCY SO IT CAN BE UPDATED
				}
			},


			readOnlyMode: true,
			setReadOnlyMode: function(ro) {
				if (TCG.isEquals(this.readOnlyMode, ro)) {
					return;
				}
				this.readOnlyMode = ro;
				this.removeAll(true);
				this.add(ro ? this.readOnlyItems : this.updatableItems);
				try {
					this.doLayout();
				}
				catch (e) {
					// strange error due to removal of elements with allowBlank = false
				}
				const f = this.getForm();
				f.setValues(f.formValues);
				this.loadValidationMetaData(true);
				f.isValid();
			},

			afterRenderApplyAdditionalDefaultData: async function(formPanel, formValues) {
				const hierarchy = TCG.getValue('security.securityHierarchy', formValues);
				if (hierarchy) {
					// set display field per security selection from default data
					await formPanel.activateBondType.call(formPanel, hierarchy);
				}
			},

			activateBondType: function(hierarchy) {
				/** TODO NEED CORPORATE ACTIONS
				const fp = this;
				const f = fp.getForm();
				let factor = false;
				let coupon = false;
				let cpi = false;

				return Promise.resolve()
						.then(function() {
							if (hierarchy.indexRatioAdjusted) {
								coupon = true;
								cpi = true;
							}
							else {
								return TCG.data.getDataPromiseUsingCaching('investmentSecurityEventTypeListByHierarchy.json?hierarchyId=' + hierarchy.id, fp, 'investment.hierarchy.eventTypeList.' + hierarchy.id)
										.then(function(events) {
											TCG.each(events, function(e) {
												if (e.name === 'Cash Coupon Payment') {
													coupon = true;
												}
												else if (e.name === 'Factor Change') {
													factor = true;
												}
											});
										});
							}
						})
						.then(function() {
							fp.activateField(f, 'currentFactor', factor);
							fp.activateField(f, 'quantityIntended', factor);
							fp.activateField(f, 'inflationAdjustedFace', cpi);
							fp.activateField(f, 'notionalMultiplier', cpi);
							fp.activateField(f, 'currentCoupon', coupon);
							fp.activateField(f, 'accrualDate', coupon);
							fp.activateField(f, 'accrualDays', coupon);
							fp.activateField(f, 'accrualAmount1', coupon);
							fp.activateField(f, 'totalPayment', coupon);
							fp.setFieldQtip('accrualAmount1', TCG.isTrue(fp.getFormValue('tradeType.amountsSignAccountsForBuy')) ? 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)' : 'Amount is expressed in absolute terms and payment direction is determined based on trade direction: BUY (pay) or SELL (receive)');
							try {
								fp.doLayout();
							}
							catch (e) {
								// strange error due to removal of elements with allowBlank = false
							}
						});
				 **/
			},

			activateField: function(form, field, show) {
				const f = form.findField(field);
				f.setVisible(show);
				if (f.readOnly === false) {
					f.allowBlank = !show;
				}
				if (!show) {
					f.setValue('');
					f.originalValue = f.getValue();
				}
			},

			updateCalculatedValues: async function(updatedField, newValue, oldValue) {
				/** TODO NEED NOTIONAL CALCULATOR CALL
				const fp = this;
				const f = fp.getForm();

				const securityId = f.findField('security.id').value;
				let orig = f.findField('originalFace').getNumericValue();
				if (Number.isNaN(orig)) {
					orig = 0;
				}

				const notionalMultiplierField = f.findField('notionalMultiplier');
				if (Ext.isNumber(orig)) {
					if (f.findField('currentFactor').isVisible()) {
						await fp.updateQuantityIntended(orig, updatedField);
					}
					else if (notionalMultiplierField.isVisible()) {
						// accounts for rounding: JPY, etc.
						await fp.updateInflationAdjusted(securityId, orig);
					}
				}

				const paymentFieldUpdated = updatedField === 'quantityIntended' || updatedField === 'currentFactor' || updatedField === 'originalFace' || updatedField === 'notionalMultiplier';
				if ((TCG.isNull(updatedField) && !f.findField('accrualAmount1').value) || paymentFieldUpdated) {
					await fp.updateAccruedInterest();
				}

				if (updatedField === 'averageUnitPrice' || paymentFieldUpdated) {
					const price = f.findField('averageUnitPrice').getNumericValue();
					const pf = fp.getPurchaseFace();
					if (Ext.isNumber(price) && Ext.isNumber(pf)) {
						// accounts for rounding: JPY, etc.
						let qty = pf;
						let multiplier = 1;
						if (notionalMultiplierField.isVisible()) {
							qty = orig;
							multiplier = notionalMultiplierField.getNumericValue();
						}
						const value = await TCG.data.getDataValuePromise(`investmentSecurityNotional.json?securityId=${securityId}&price=${price}&quantity=${qty}&notionalMultiplier=${multiplier}`, this);
						fp.setFormValue('accountingNotional', value);
						fp.updateTotalPayment();
					}
				}
				else {
					fp.updateTotalPayment();
				}

				if (!f.findField('currentCoupon').value) {
					await fp.updateCurrentCoupon();
					await fp.updateNumberOfAccrualDays();
				}
				 **/
			},

			updateQuantityIntended: async function(originalFace, updatedField) {
				const fp = this;
				const f = fp.getForm();
				let factor = f.findField('currentFactor').getNumericValue();
				const purchase = f.findField('quantityIntended').getNumericValue();

				if (!Ext.isNumber(factor) && Ext.isNumber(purchase)) {
					factor = purchase / originalFace;
					fp.setFormValue('currentFactor', factor, true);
				}

				if (Ext.isNumber(factor) && (!Ext.isNumber(purchase) || (updatedField === 'originalFace' || updatedField === 'currentFactor'))) {
					const amount = Math.round(originalFace * factor, 2);  // why does this need to be a server call ?await TCG.data.getDataValuePromise(`investmentMultiplyAndRound.json?value1=${originalFace}&value2=${factor}&scale=2`, this);
					fp.setFormValue('quantityIntended', amount, true); // need to round up, not truncate
				}
			},

			updateInflationAdjusted: async function(securityId, originalFace) {
				/** todo need notional calc
				const fp = this;
				const f = fp.getForm();
				const face = await TCG.data.getDataValuePromise(`investmentSecurityNotional.json?securityId=${securityId}&price=100&quantity=${originalFace}&notionalMultiplier=${f.findField('notionalMultiplier').getNumericValue()}`, this);
				fp.setFormValue('inflationAdjustedFace', face, true);
				 **/
			},

			updateTotalPayment: function() {
				const fp = this;
				const f = fp.getForm();
				const principal = f.findField('accountingNotional').getNumericValue();
				const interest = f.findField('accrualAmount1').getNumericValue();
				fp.setFormValue('totalPayment', principal + interest, true);
			},

			updateCurrentCoupon: async function(securityId) {
				/** TODO NEED CORPORATE ACTIONS
				const fp = this;
				const f = fp.getForm();
				if (f.findField('currentCoupon').isVisible()) {
					const params = {
						securityId: securityId || f.findField('security.id').value,
						typeName: 'Cash Coupon Payment',
						accrualEndDate: f.findField('settlementDate').value
					};
					let coupon = 0;
					let date = '';
					let event = await TCG.data.getDataPromise('investmentSecurityEventForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue|declareDate', this, {params: params});
					if (event) {
						coupon = event.afterEventValue;
						date = event.declareDate;
					}
					else {
						// lookup previous (assumed) coupon if current one is not available
						event = await TCG.data.getDataPromise('investmentSecurityEventPreviousForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue|declareDate', this, {params: params});
						if (event) {
							coupon = event.afterEventValue;
							date = event.declareDate;
						}
					}
					fp.setFormValue('currentCoupon', coupon, true);
					fp.setFormValue('accrualDate', date, true);
				}
				else {
					fp.setFormValue('notionalMultiplier', 1, true);
				}
				 **/
			},

			updateCurrentFactor: async function(securityId) {
				/** TODO NEED CORPORATE ACTIONS
				const f = this.getForm();
				const factorField = f.findField('currentFactor');
				if (factorField.isVisible()) {
					const params = {
						securityId: securityId,
						typeName: 'Factor Change',
						accrualEndDate: f.findField('settlementDate').value
					};
					// use previous because current factor may not be available
					let event = await TCG.data.getDataPromise('investmentSecurityEventPreviousForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue', this, {params: params});
					if (event) {
						factorField.setValue(event.afterEventValue);
					}
					else {
						// no previous factor found: try current
						event = await TCG.data.getDataPromise('investmentSecurityEventForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=beforeEventValue', this, {params: params});
						factorField.setValue(event ? event.beforeEventValue : 1);
					}
				}
				 **/
			},

			getPurchaseFace: function(adjustForIndexRatio) {
				/** TODO NEED ADDITIONAL PROPERTY ON SECURITY (OR CAN THIS BE BASED ON TYPE / SUB TYPE
				// quantityIntended if it's enabled or inflationAdjustedFace if it's enabled
				const f = this.getForm();
				let fieldName = 'originalFace';
				if (f.findField('quantityIntended').isVisible()) {
					fieldName = 'quantityIntended';
				}
				else if (f.findField('inflationAdjustedFace').isVisible() && (adjustForIndexRatio || this.getFormValue('investmentSecurity.instrument.hierarchy.notionalMultiplierForPriceNotUsed') !== true)) {
					fieldName = 'inflationAdjustedFace';
				}
				return f.findField(fieldName).getNumericValue();
				 **/
			},

			updateNumberOfAccrualDays: async function() {
				/** TODO ACCRUAL DATES
				const fp = this;
				const toDate = await TCG.data.getDataValuePromise('tradeAccrualUpToDate.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(fp.getForm().el.dom, true), this);
				const parsedToDate = TCG.parseDate(toDate);
				if (Ext.isDate(parsedToDate)) {
					const fromDate = TCG.parseDate(fp.getFormValue('accrualDate', true), 'm/d/Y');
					const days = Ext.isDate(fromDate) ? Math.round((parsedToDate.getTime() - fromDate.getTime()) / (24 * 60 * 60 * 1000)) : undefined;
					fp.setFormValue('accrualDays', days, true);
				}
				 **/
			},

			updateAccruedInterest: async function() {
				/** TODO ACCRUAL DATES
				const fp = this;
				const f = fp.getForm();
				const securityId = f.findField('investmentSecurity.id').value;
				let purchaseFace = fp.getPurchaseFace(true);
				if (f.findField('notionalMultiplier').isVisible()) {
					purchaseFace = f.findField('originalFace').getNumericValue();
				}
				const settlementDate = f.findField('settlementDate').value;
				if (settlementDate && TCG.isNotNull(securityId) && Ext.isNumber(purchaseFace) && f.findField('accrualAmount1').isVisible()) {
					const ai = await TCG.data.getDataValuePromise('tradeAccrualAmount.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(f.el.dom, true), this);
					if (TCG.isNotNull(ai)) { // no errors
						fp.setFormValue('accrualAmount1', ai, true);
					}
				}
				 **/
			},

			updateIndexRatio: async function(securityId, force) {
				/** TODO MARKET DATA
				const fp = this;
				const f = fp.getForm();
				if (securityId && (force || f.findField('notionalMultiplier').isVisible())) {
					const indexRatio = await TCG.data.getDataValuePromise(`marketDataIndexRatioForSecurity.json?securityId=${securityId}&date=${f.findField('settlementDate').getValue().format('m/d/Y')}`, this);
					fp.setFormValue('notionalMultiplier', indexRatio, true);
					// accounts for rounding: JPY, etc.
					const orig = f.findField('originalFace').getNumericValue();
					if (Ext.isNumber(orig)) {
						const value = await TCG.data.getDataValuePromise(`investmentSecurityNotional.json?securityId=${securityId}&price=100&quantity=${orig}&notionalMultiplier=${indexRatio}`, this);
						fp.setFormValue('inflationAdjustedFace', value, true);
					}
				}
				else {
					fp.setFormValue('notionalMultiplier', 1, true);
				}
				 **/
			},

			updateExecutingBrokerCompany: async function(providedOrderDestinationId) {
				/** ORDER DESTINATION MAPPINGS
				const f = this.getForm();
				const executingBroker = f.findField('executingBrokerCompany.label');
				if (executingBroker.isVisible()) {
					const orderDestinationId = providedTradeDestinationId || f.findField('orderDestination.name').getValue();
					const tradeDate = f.findField('tradeDate').getValue().format('m/d/Y');
					if (orderDestinationId) {
						const rec = await TCG.data.getDataPromise('tradeDestinationMappingByCommand.json', this, {
							params: {
								orderTypeId: f.findField('orderType.id').getValue(),
								orderDestinationId: orderDestinationId,
								activeOnDate: tradeDate
							}
						});

						if (rec && rec.executingBrokerCompany) {
							executingBroker.setValue({value: rec.executingBrokerCompany.id, text: rec.executingBrokerCompany.label});
							f.formValues.executingBrokerCompany = rec.executingBrokerCompany; // needs this in order to find the record when tabbing out when the store isn't initialized
						}
						else {
							executingBroker.reset();
							executingBroker.clearValue();
						}
					}
				}
				 **/
			},


			items: [],

			updatableItems: [
				// TODO ADDING READ ONLY MODE ONLY FOR NOW
			],


			readOnlyItems: [
				{
					xtype: 'panel',
					layout: 'column',
					items: [
						{
							columnWidth: .38,
							layout: 'form',
							labelWidth: 115,
							defaults: {xtype: 'textfield', width: 200},
							items: [
								{
									fieldLabel: 'Side', name: 'openCloseType.label', xtype: 'displayfield', width: 125,
									setRawValue: function(v) {
										this.el.addClass(v.includes('BUY') ? 'buy' : 'sell');
										TCG.form.DisplayField.superclass.setRawValue.call(this, v);
									}
								},
								{fieldLabel: 'Bond', name: 'security.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'security.id'},
								{fieldLabel: 'Bond Type', name: 'security.securityHierarchy.name', xtype: 'displayfield'},
								{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'},
								{fieldLabel: 'Destination', name: 'orderDestination.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', detailIdField: 'orderDestination.id'},
								//{boxLabel: 'Collateral trade', name: 'collateralTrade', xtype: 'checkbox', disabled: true},
								{fieldLabel: 'Block Order ID', name: 'orderBlockIdentifier', xtype: 'linkfield', detailPageClass: 'Clifton.order.execution.OrderBlockWindow', detailIdField: 'orderBlockIdentifier'},
								{fieldLabel: 'Order Group', name: 'orderGroup.groupType.name', xtype: 'linkfield', detailIdField: 'orderGroup.id', detailPageClass: 'Clifton.order.group.OrderGroupWindow'},
								{fieldLabel: 'Order Source', name: 'orderSourceWithIdentifier', xtype: 'linkfield', detailIdField: 'orderSource.id', detailPageClass: 'Clifton.order.setup.OrderSourceWindow', qtip: 'This field is the combination of Order Source and the Extrnal Identifier'}
							]
						},
						{
							columnWidth: .33,
							layout: 'form',
							labelWidth: 100,
							defaults: {xtype: 'textfield', width: 115},
							items: [
								{fieldLabel: 'Original Face', name: 'originalFace', xtype: 'currencyfield4', readOnly: true},
								{fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true},
								{fieldLabel: 'Factor', name: 'currentFactor', xtype: 'floatfield', readOnly: true, hidden: true},
								{fieldLabel: 'Purchase Face', name: 'quantityIntended', xtype: 'currencyfield4', readOnly: true},
								{
									fieldLabel: 'Index Ratio', name: 'notionalMultiplier', xtype: 'floatfield', readOnly: true, hidden: true,
									listeners: {
										change: function(field, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(field);
											fp.updateCalculatedValues.call(fp, 'notionalMultiplier', newValue, oldValue);
										}
									}
								},
								{fieldLabel: 'Inflation Adj Face', name: 'inflationAdjustedFace', xtype: 'currencyfield4', readOnly: true, hidden: true},
								{fieldLabel: 'Current Coupon', name: 'currentCoupon', xtype: 'floatfield', readOnly: true, hidden: true},
								{fieldLabel: 'Accrual Start', name: 'accrualDate', xtype: 'displayfield', type: 'date', readOnly: true, hidden: true, qtip: 'Accrual starts the day after this date: End Of Day industry convention'},
								{fieldLabel: 'Accrual Days', name: 'accrualDays', xtype: 'displayfield', readOnly: true, hidden: true, qtip: 'Calendar days between Accrual Start and End Dates (using accrual date calculator). Does not take into account security Day Count convention.'}
							]
						},
						{
							columnWidth: .29,
							layout: 'form',
							labelWidth: 100,
							defaults: {xtype: 'textfield', anchor: '-20'},
							items: [
								{fieldLabel: 'Principal', name: 'accountingNotional', xtype: 'currencyfield4', readOnly: true},
								{fieldLabel: 'Accrued Interest', name: 'accrualAmount1', xtype: 'currencyfield4', readOnly: true},
								{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'currencyfield4', qtip: 'Settlement Total = Principal + Accrued Interest', readOnly: true},
								//{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Settlement Total = Principal + Accrued Interest', hidden: true},
								{fieldLabel: 'Created By', name: 'orderCreatorUser.label', xtype: 'linkfield', detailIdField: 'orderCreatorUser.id', detailPageClass: 'Clifton.security.user.UserWindow', qtip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'},
								{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
								{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true}
								//{xtype: 'trade-bookingdate'}
							]
						}]
				},

				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Client Account', name: 'clientAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'clientAccount.id'},
				{fieldLabel: 'Holding Account', name: 'holdingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'holdingAccount.id'},
				{fieldLabel: 'Settlement Company', name: 'settlementCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', detailIdField: 'settlementCompany.id', qtip: 'Business company used to determine the place of settlement business identifier code (BIC) when generating settlement instructions.'},

				{xtype: 'label', html: '<hr/>'},
				//{xtype: 'trade-descriptionWithNoteDragDrop'}
				{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', anchor: '-20', readOnly: true}
			]
		}]

	}
});
