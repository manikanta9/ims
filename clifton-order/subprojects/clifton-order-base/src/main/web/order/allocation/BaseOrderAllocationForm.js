Clifton.order.allocation.BaseOrderAllocationForm = Ext.extend(TCG.form.FormPanel, {
	url: 'orderAllocation.json',
	labelFieldName: 'label',
	maxWindowTitleLength: 130,
	loadValidation: false,
	loadDefaultDataAfterRender: true,
	defaults: {anchor: '0'},

	readOnlyItems: [],
	updatableItems: [],

	reload: function() {
		const fp = this;
		TCG.data.getDataPromise('orderAllocation.json?requestedMaxDepth=6&UI_SOURCE=BaseOrderAllocationForm_reload&id=' + fp.getWindow().getMainFormId(), fp)
			.then(function(orderAllocation) {
				fp.getForm().setValues(orderAllocation, true);
				fp.fireEvent('afterload', fp);
			});
	},

	listeners: {
		beforerender: function() {
			const status = TCG.getValue('workflowStatus.name', this.getWindow().defaultData);
			this.readOnlyMode = (status !== 'Draft');
			if (this.readOnlyMode) {
				this.readOnlyMode = (this.getLoadURL() !== false);
			}
			this.add(this.readOnlyMode ? this.readOnlyItems : this.updatableItems);
		},
		afterload: function(fp) {
			const ro = ('Draft' !== fp.getFormValue('workflowStatus.name'));
			fp.setReadOnlyMode(ro);
			fp.hideFieldIfBlank('orderGroup.groupType.name');
			fp.hideFieldIfBlank('orderSourceWithIdentifier');

			if (TCG.isEquals(fp.getFormValue('clientAccount.id'), fp.getFormValue('holdingAccount.id'))) {
				fp.hideField('holdingAccount.label');
				fp.hideField('holdingAccount.baseCurrencyCode');
				fp.setFieldLabel('clientAccount.label', 'Account:');
			}
			fp.applyAdditionalAfterLoad(ro);
		}
	},

	applyAdditionalAfterLoad: function(readOnlyFlag) {
		// DO NOTHING - can be overridden
	},

	afterRenderPromise: async function(formPanel) {
		const form = formPanel.getForm();
		if (form.formValues && TCG.isBlank(form.formValues.id)) {
			if (TCG.isNotNull(form.formValues.security)) {
				// If the security was passed in with default data, set the record on the combo for form functions to use
				const security = form.formValues.security;
				const securityField = form.findField('security.label') || form.findField('security.ticker');
				if (securityField && securityField.xtype === 'combo') {
					const recordConstructor = securityField.getStore().recordType;
					const securityRecord = new recordConstructor(security);
					if (TCG.isNull(securityRecord.json)) {
						securityRecord.json = security;
						securityRecord.id = security.id;
					}
					await securityField.fireEvent('select', securityField, securityRecord);
				}
			}
			if (formPanel.afterRenderApplyAdditionalDefaultData) {
				await formPanel.afterRenderApplyAdditionalDefaultData.call(formPanel, formPanel, form.formValues);
			}

			if (TCG.isNotNull(form.formValues.buy)) {
				// check if form is valid to remove warning that buy/sell is not selected
				form.isValid();
			}
		}
	},


	getWarningMessage: function(form) {
		let msg = undefined;
		if (form.formValues && form.formValues.workflowState) {
			if (['Invalid'].includes(form.formValues.workflowState.name)) {
				msg = 'This order failed compliance check(s). See <b>Violations</b> tab for more information.';
			}
		}
		return msg;
	},

	getDefaultData: function(win) {
		const fp = this;
		const f = fp.getForm();
		let dd = Ext.apply(win.defaultData || {}, {});
		if (win.defaultDataIsReal) {
			return dd;
		}
		const orderTypeName = win.orderType;

		return TCG.data.getDataPromiseUsingCaching('orderTypeByName.json?name=' + orderTypeName, fp, 'order.type.' + orderTypeName)
			.then(function(orderType) {
				dd = Ext.apply({
					orderType: orderType,
					orderCreatorUser: TCG.getCurrentUser(),
					tradeDate: new Date().format('Y-m-d 00:00:00') // Default to Today
				}, dd);

				if (dd.clientAccount && !dd.holdingAccount) {
					// Need to trigger client account as selected so holding account selection is updated (and usually pre-selected when only one account applies)
					// The default data investmentSecurity is passed in to indicate that if the holding account can be auto selected, the current balance should be loaded
					f.addListener('afterRender', fp.updateHoldingAccountSelection.defer(300, fp, [dd.clientAccount]));
				}


				if (dd.security) {
					return Clifton.order.shared.getTradeDatePromise(dd.security.id, null, fp)
						.then(function(tradeDate) {
							dd.tradeDate = tradeDate.format('Y-m-d 00:00:00');
							if (dd.settlementCurrency) {
								return Clifton.order.shared.getSettlementDatePromise(dd.security.id, tradeDate.format('m/d/Y'), false, dd.settlementCurrency.id, fp)
									.then(function(settlementDate) {
										dd.settlementDate = settlementDate.format('Y-m-d 00:00:00');
										return dd;
									});
							}
							return dd;
						});
				}
				return dd;
			});
	},

	updateHoldingAccountSelection: function(clientAccountRecord) {
		const fp = this;
		const f = fp.getForm();
		const ha = f.findField('holdingAccount.label');

		ha.store.removeAll();
		ha.lastQuery = null;

		if (TCG.isTrue(clientAccountRecord.json.holdingAccount)) {
			fp.setFormValue('holdingAccount.id', {value: clientAccountRecord.json.id, text: clientAccountRecord.json.label});
			ha.beforeSelect(ha, clientAccountRecord);
			fp.hideField('holdingAccount.label');
			fp.hideField('holdingAccount.baseCurrencyCode');
			fp.setFieldLabel('clientAccount.label', 'Account:');
		}
		else {
			fp.showField('holdingAccount.label');
			fp.showField('holdingAccount.baseCurrencyCode');
			fp.setFieldLabel('clientAccount.label', 'Client Account:');
			// NEED TO LOOK IT UP NOT NEEDED UNTIL WE HAVE SEPARATE CLIENT AND HOLDING ACCOUNTS
		}

	},


	updatePrice: async function() {
		const fp = this;
		const form = fp.getForm();
		const orderSecurity = form.findField('security.label');
		const priceField = form.findField('expectedUnitPrice');
		const priceDate = form.findField('tradeDate').value || form.findField('settlementDate').value;
		TCG.data.getDataPromise('orderMarketDataPriceFlexible.json?orderSecurityId=' + orderSecurity.getValue() + '&date=' + priceDate + '&throwExceptionIfMissing=' + true, this)
			.then(function(priceData) {
				if (priceData) {
					priceField.setValue(priceData.basePrice);
				}
				else {
					priceField.setValue(1);
				}
				fp.updateAccountingNotional();
			});
		await fp.updateAccountingNotional();
	},

	updateAccountingNotional: async function() {
		// Assuming only price * qty for now
		const fp = this;
		const form = fp.getForm();
		const price = form.findField('expectedUnitPrice').getValue();
		const quantity = form.findField('quantityIntended').getValue();
		if (TCG.isNotBlank(price) && TCG.isNotBlank(quantity)) {
			fp.setFormValue('accountingNotional', price * quantity);
		}
	},


	readOnlyMode: true,
	setReadOnlyMode: function(ro) {
		if (TCG.isEquals(this.readOnlyMode, ro)) {
			return;
		}
		this.readOnlyMode = ro;
		this.removeAll(true);
		this.add(ro ? this.readOnlyItems : this.updatableItems);
		try {
			this.doLayout();
		}
		catch (e) {
			// strange error due to removal of elements with allowBlank = false
		}
		const f = this.getForm();
		f.setValues(f.formValues);
		this.loadValidationMetaData(true);
		f.isValid();
	}

});
Ext.reg('order-allocation-form-base', Clifton.order.allocation.BaseOrderAllocationForm);
