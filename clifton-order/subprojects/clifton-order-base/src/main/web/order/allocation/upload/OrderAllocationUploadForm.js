Clifton.order.allocation.upload.OrderAllocationUploadForm = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false,
	loadDefaultDataAfterRender: true,
	fileUpload: true,
	instructions: 'Order Allocation Uploads allow you to import order allocations from files directly into the system.  Use the options below to customize how to upload your data.',
	simpleOrderTypeName: undefined, // Override if form is for specific order type

	initComponent: function() {
		const currentItems = [];
		// If Specific Order Type - Put First as Link Field
		if (this.simpleOrderTypeName) {
			currentItems.push({fieldLabel: 'Order Type', name: 'simpleOrderType.name', hiddenName: 'simpleOrderType.id', detailIdField: 'simpleOrderType.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.OrderTypeWindow'});
		}
		// If window.ddFiles (Drag and drop file) - don't show the file selector
		if (this.getWindow().ddFiles) {
			currentItems.push({fieldLabel: 'File', name: 'fileName', xtype: 'displayfield', value: this.getWindow().ddFiles[0].name, submitValue: false});
		}
		else {
			currentItems.push({fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false});
		}

		// Then Common Order Allocation Upload Items
		Ext.each(this.commonItems, function(f) {
			currentItems.push(f);
		});

		// Then Screen Specific Simple Upload Options
		Ext.each(this.simpleItems, function(f) {
			currentItems.push(f);
		});

		// Then Import Results Area
		Ext.each(this.importResultItems, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;

		Clifton.order.allocation.upload.OrderAllocationUploadForm.superclass.initComponent.call(this);
	},


	saveWithDragAndDropFile: function(closeOnSuccess, forms, form, panel, params) {
		if (panel.getWindow().ddFiles) {
			const win = this;
			const files = win.ddFiles;
			TCG.file.enableDD.upload(files, 0, panel.getEl(), panel.getFormValuesFormatted(), win, panel.getSaveURL(), panel, panel.setFormValuesAfterDragAndDrop);
			return false;
		}
	},

	setFormValuesAfterDragAndDrop: function(data) {
		this.getForm().setValues(data, true);
	},


	getDefaultData: async function(win) {
		win.saveForm = win.saveForm.createInterceptor(this.saveWithDragAndDropFile);

		let dd = win.defaultData || {};
		if (win.defaultDataIsReal) {
			return dd;
		}
		const now = new Date().format('Y-m-d 00:00:00');
		if (this.simpleOrderTypeName) {
			const type = await TCG.data.getDataPromiseUsingCaching('orderTypeByName.json?name=' + this.simpleOrderTypeName, this, 'order.type.' + this.simpleOrderTypeName);
			dd = Ext.apply({
				simpleOrderType: type
			}, dd);
		}
		const orderGroupType = await TCG.data.getDataPromiseUsingCaching('orderGroupTypeByName.json?name=Order Allocation Import', this, 'order.group.type.Order Allocation Import');
		dd = Ext.apply({
			orderGroup: {
				orderCreatorUser: TCG.getCurrentUser(),
				tradeDate: now,
				orderGroupType: orderGroupType
			}
		}, dd);
		return dd;
	},
	commonItems: [
		{
			xtype: 'radiogroup', columns: 1, name: 'partialUploadAllowed',
			items: [
				{boxLabel: 'Fail this upload and don\'t load any data.', name: 'partialUploadAllowed', inputValue: 'false', checked: true},
				{boxLabel: 'Skip rows with invalid data but upload valid rows.', name: 'partialUploadAllowed', inputValue: 'true'}
			]
		},
		{
			fieldLabel: 'Created By', name: 'orderGroup.orderCreatorUser.label', hiddenName: 'orderGroup.orderCreatorUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false',
			qtip: 'Portfolio Manager or the person who was responsible for creation of these Order Allocation(s). If not selected, the order creator user is required for each order allocation in the file. For the group, the first order allocation\'s creator user will be set at the group level.'
		},
		{
			fieldLabel: 'Trade Date', name: 'orderGroup.tradeDate', xtype: 'datefield',
			qtip: 'If not selected, the trade date is required for each order allocation in the file.  For the group, the first order allocation\'s trade date will be set at the group level.'
		}
	],
	// Override with additional options
	simpleItems: [],
	importResultItems: [
		{
			xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
			items: [
				{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
			]
		}
	],

	getSaveURL: function() {
		return 'orderAllocationUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&requestedMaxDepth=2';
	}
});
Ext.reg('order-allocation-upload-form', Clifton.order.allocation.upload.OrderAllocationUploadForm);
