// not the actual window but a window selector based on 'orderType'

Clifton.order.allocation.OrderAllocationWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderAllocation.json?requestedMaxDepth=6&UI_SOURCE=OrderAllocationWindowSelector', // deep enough to get hierarchy attributes

	getClassName: function(config, entity) {
		let orderType = this.orderType;
		if (entity && entity.orderType) {
			orderType = entity.orderType.name;
		}
		let clazz = Clifton.order.allocation.OrderAllocationWindowOverrides[orderType];
		if (!clazz) {
			clazz = 'Clifton.order.allocation.DefaultOrderAllocationWindow';
		}
		return clazz;
	}
});
