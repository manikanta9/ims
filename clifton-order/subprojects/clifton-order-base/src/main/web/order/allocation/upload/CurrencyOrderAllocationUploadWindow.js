TCG.use('Clifton.order.allocation.upload.OrderAllocationUploadForm');
TCG.use('Clifton.order.allocation.upload.OrderAllocationUploadWindowBase');

Clifton.order.allocation.upload.CurrencyOrderAllocationUploadWindow = Ext.extend(Clifton.order.allocation.upload.OrderAllocationUploadWindowBase, {
	id: 'orderAllocationUploadCurrencyWindow',
	title: 'Currency Order Allocation Import',

	init: function() {
		this.simpleOrderTypeName = 'Currency';
		this.importTab.items[0].simpleOrderTypeName = this.simpleOrderTypeName;
		Clifton.order.allocation.upload.CurrencyOrderAllocationUploadWindow.superclass.init.apply(this, arguments);
	},

	importTab: {
		title: 'Order Allocation Import',
		tbar: [{
			text: 'Sample File',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File for Currency Order Allocation Uploads',
			handler: function() {
				TCG.openFile('order/allocation/upload/CurrencyOrderAllocationUploadFileSample.xls');
			}
		}],
		items: [{
			xtype: 'order-allocation-upload-form',
			simpleItems: [
				{fieldLabel: 'Settlement Date', name: 'simpleSettlementDate', xtype: 'datefield'},
				{xtype: 'label', html: 'Order Allocations without a specific description in the upload file will also have this note as the Trade Note.'},
				{fieldLabel: 'Notes', name: 'orderGroup.note', xtype: 'textarea'},
				{xtype: 'hidden', name: 'simple', value: 'true'}
			]
		}]
	}
});
