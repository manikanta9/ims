Clifton.order.allocation.OrderAllocationListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderAllocationListFind',
	xtype: 'gridpanel',
	wikiPage: 'IT/Order+Life+Cycle',
	instructions: 'A list of all order allocations in the system.  Order Allocations are a request to buy or sell a specific quantity of a security for a specified account.  Use filters to limit the order allocations to only those that you need.',
	viewNames: ['Default View', 'Export Friendly', 'Currency View'],
	defaultDisplayFilter: 'ALL',
	enableOrderAllocationUploadDragAndDrop: false,
	applyTraderFilter: false, // If true adds "my" options to the disply filter where trader = current user or null
	standardColumnViewNames: {'createDate': ['Export Friendly'], 'updateDate': ['Export Friendly']},
	columns: [
		{header: 'ID', width: 24, dataIndex: 'id', hidden: true, viewNames: ['Export Friendly']},
		{header: 'Block Order ID', width: 24, dataIndex: 'orderBlockIdentifier', hidden: true, viewNames: ['Export Friendly']},
		{header: 'Workflow Status', width: 26, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=OrderAllocation', showNotEquals: true}, allViews: true},
		{
			header: 'Workflow State', width: 26, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default View', 'Export Friendly', 'Currency View'],
			renderer: function(v, metaData, r) {
				if (TCG.isEquals('Invalid', v)) {
					metaData.css = 'ruleViolation';
				}
				return v;
			}
		},
		{
			header: 'Violations', width: 18, dataIndex: 'ruleViolationCodes', allViews: true, qtip: 'Rule Violation codes are the combination of the list of violations for a given order allocation. Each rule definition may have a Rule Violation Code (Optional) with a 1 or 2 digit character that represents that rule',
			renderer: function(ruleViolationCodes, args, r) {
				if (TCG.isNotBlank(ruleViolationCodes)) {
					return TCG.renderActionColumn('', ruleViolationCodes, 'View Violations for this order allocation');
				}
				return ruleViolationCodes;
			},
			eventListeners: {
				click: function(column, grid, rowIndex, event) {
					const row = grid.getStore().getAt(rowIndex);
					if (TCG.isNotBlank(row.json.ruleViolationCodes)) {
						const gridPanel = grid.ownerGridPanel;
						const clazz = gridPanel.editor.getDetailPageClass(grid, row);
						const id = gridPanel.editor.getDetailPageId(gridPanel, row);
						gridPanel.editor.openDetailPage(clazz, gridPanel, id, row, '', 'Violations');
					}
				}
			}
		},
		{header: 'Order Type', width: 22, dataIndex: 'orderType.name', filter: {type: 'combo', searchFieldName: 'orderTypeId', url: 'orderTypeListFind.json'}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Order Group Type', width: 30, hidden: true, dataIndex: 'orderGroup.groupType.name', filter: {type: 'combo', searchFieldName: 'orderGroupTypeId', url: 'orderGroupTypeListFind.json'}, viewNames: ['Export Friendly']},
		{header: 'Order Group ID', width: 20, hidden: true, dataIndex: 'orderGroup.id', filter: {type: 'combo', searchFieldName: 'orderGroupId'}, type: 'int', useNull: true},
		{header: 'Order Source', width: 30, dataIndex: 'orderSource.name', filter: {type: 'combo', searchFieldName: 'orderSourceId', url: 'orderSourceListFind.json'}, hidden: true, useNull: true},
		{header: 'External Identifier', width: 30, dataIndex: 'externalOrderIdentifier', filter: {searchFieldName: 'externalOrderIdentifier'}, hidden: true, useNull: true},
		{header: 'Client', width: 80, dataIndex: 'clientAccount.clientCompany.name', hidden: true, filter: {searchFieldName: 'clientCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Client'}},
		{
			header: 'Client Account', width: 30, dataIndex: 'clientAccount.labelShort', filter: {type: 'combo', searchFieldName: 'clientAccountId', displayField: 'labelShort', url: 'orderAccountListFind.json?clientAccount=true'}, viewNames: ['Default View', 'Export Friendly', 'Currency View'],
			renderer: function(v, metaData, r) {
				const note = r.data['description'];
				if (TCG.isNotBlank(note)) {
					const qtip = note;
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Client Account #', width: 30, dataIndex: 'clientAccount.accountNumber', hidden: true, filter: {type: 'combo', searchFieldName: 'clientAccountId', displayField: 'accountNumber', url: 'orderAccountListFind.json?clientAccount=true'}},
		{header: 'Client Account Name', width: 50, dataIndex: 'clientAccount.accountName', hidden: true, filter: {type: 'combo', searchFieldName: 'clientAccountId', displayField: 'accountName', url: 'orderAccountListFind.json?clientAccount=true'}},
		{header: 'Business Service', width: 80, dataIndex: 'clientAccount.service.name', hidden: true, filter: {searchFieldName: 'serviceOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Account Service', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{header: 'Holding Account', width: 80, dataIndex: 'holdingAccount.labelShort', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'labelShort', url: 'orderAccountListFind.json?holdingAccount=true'}, viewNames: ['Export Friendly']},
		{header: 'Holding Account #', width: 30, dataIndex: 'holdingAccount.accountNumber', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'accountNumber', url: 'orderAccountListFind.json?holdingAccount=true'}},
		{header: 'Holding Account Name', width: 50, dataIndex: 'holdingAccount.accountName', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'accountName', url: 'orderAccountListFind.json?ourAccount=false'}},
		{header: 'Holding Account Issuer', width: 40, dataIndex: 'holdingAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'orderCompanyListFind.json?holdingAccountIssuer=true'}, viewNames: ['Default View', 'Export Friendly', 'Currency View']},
		{header: 'Executing Broker', width: 40, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker'}, hidden: true, viewNames: ['Export Friendly']},
		//{header: 'Executing Sponsor', width: 30, dataIndex: 'executingSponsorCompany.name', filter: {type: 'combo', searchFieldName: 'executingSponsorCompanyId', url: 'orderCompanyListFind.json'}, hidden: true, viewNames: ['Export Friendly']},
		//{header: 'Settlement Company', width: 60, dataIndex: 'settlementCompany.name', filter: {type: 'combo', searchFieldName: 'settlementCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
		{header: 'Description', width: 100, dataIndex: 'description', hidden: true, viewNames: ['Export Friendly']},
		{
			header: 'Side', width: 12, dataIndex: 'buy', type: 'boolean', viewNames: ['Default View', 'Export Friendly', 'Currency View'],
			exportColumnValueConverter: 'orderBuyColumnReversableConverter',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'openCloseTypeId', url: 'orderOpenCloseTypeListFind.json', loadAll: true}, viewNames: ['Export Friendly'],
			renderer: function(v, metaData) {
				metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Original Face', width: 20, dataIndex: 'originalFace', type: 'float', useNull: true, hidden: true},
		{header: 'Quantity', width: 20, dataIndex: 'quantityIntended', type: 'float', useNull: true, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Notional Multiplier', width: 20, dataIndex: 'notionalMultiplier', type: 'float', useNull: true, hidden: true},
		{header: 'Price Multiplier', width: 30, dataIndex: 'security.priceMultiplier', type: 'float', hidden: true, filter: {searchFieldName: 'priceMultiplier'}},
		{
			header: 'Security', width: 40, dataIndex: 'security.ticker', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'orderSecurityListFind.json'}, viewNames: ['Default View', 'Export Friendly', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given CCY', widthMultiplier: 0.5},
				{name: 'Default View', label: 'Security', widthMultiplier: 1},
				{name: 'Export Friendly View', label: 'Security', widthMultiplier: 1}
			]
		},
		{header: 'Security Name', width: 45, dataIndex: 'security.name', filter: {searchFieldName: 'securityName'}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Underlying Security', width: 40, dataIndex: 'security.underlyingSecurity.ticker', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'orderSecurityListFind.json'}, hidden: true},
		{header: 'Security Type', width: 45, dataIndex: 'security.securityType.text', hidden: true, filter: {searchFieldName: 'securityTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Security Type', valueField: 'id'}},
		{header: 'Security Hierarchy', width: 80, dataIndex: 'security.securityHierarchy.nameExpanded', hidden: true, filter: {searchFieldName: 'securityHierarchyOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Security Hierarchy', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{header: 'CCY Denomination (Security)', width: 30, dataIndex: 'security.currencyCode', hidden: true, filter: {searchFieldName: 'securityCurrencyCode', type: 'combo', displayField: 'label', valueField: 'ticker', url: 'orderSecurityListFind.json?securityType=Currency'}},
		{header: 'Settle CCY', width: 20, dataIndex: 'settlementCurrency.ticker', hidden: true, filter: {searchFieldName: 'settlementCurrencyId', type: 'combo', displayField: 'label', url: 'orderSecurityListFind.json?securityType=Currency'}, viewNames: ['Currency View']},
		{header: 'FX Rate', width: 24, dataIndex: 'exchangeRateToSettlementCurrency', type: 'float', useNull: true, hidden: true, viewNames: ['Export Friendly', 'Currency View'], tooltip: 'Exchange rate to the settlement currency in the standard currency convention.'},
		{header: 'Avg Price', width: 22, dataIndex: 'averageUnitPrice', type: 'float', useNull: true, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Expected Price', width: 28, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
		{
			header: 'Notional', width: 28, dataIndex: 'accountingNotional', type: 'currency', useNull: true, tooltip: 'Accounting Notional', viewNames: ['Default View', 'Export Friendly', 'Currency View'],
			summaryCalculation: function(v, r, field, data, col) {
				return v + Clifton.order.calculateSignedAmount(TCG.getValue('accountingNotional', r.json), TCG.getValue('openCloseType.label', r.json));
			},
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given Amount'},
				{name: 'Default View', label: 'Notional'},
				{name: 'Export Friendly View', label: 'Notional'}
			]
		},
		{
			header: 'Settle Amount', width: 28, dataIndex: 'settlementAmount', type: 'currency', useNull: true, hidden: true, viewNames: ['Currency View'], tooltip: 'Accounting Notional in the Settlement Currency',
			summaryCalculation: function(v, r, field, data, col) {
				return v + Clifton.order.calculateSignedAmount(TCG.getValue('settlementAmount', r.json), TCG.getValue('openCloseType.label', r.json));
			}
		},
		{header: 'Accrual 1', width: 20, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Accrual 2', width: 20, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Accrual', width: 20, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Commission Per Unit', width: 20, dataIndex: 'commissionPerUnit', type: 'float', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Commission Amount', width: 20, dataIndex: 'commissionAmount', type: 'currency', hidden: true, useNull: true, tooltip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.'},
		{header: 'Fee', width: 20, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
		{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
		{header: 'PM Team', width: 22, dataIndex: 'clientAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', xtype: 'system-security-group-combo', groupTagName: 'PM Team'}, allViews: true},
		{header: 'Portfolio Manager', width: 50, dataIndex: 'clientAccount.portfolioManagerUser.displayName', filter: {searchFieldName: 'portfolioManagerUserId', type: 'combo', xtype: 'system-security-user-group-combo', groupTagName: 'PM Team'}, hidden: true, viewNames: ['Export Friendly'], tooltip: 'The Portfolio Manager currently assigned to the account in account setup, Not necessarily the PM that entered or approved the order allocation.'},
		{header: 'Created By', width: 24, dataIndex: 'orderCreatorUser.label', filter: {type: 'combo', searchFieldName: 'orderCreatorUserId', displayField: 'label', url: 'securityUserListFind.json'}, viewNames: ['Default View', 'Export Friendly', 'Currency View'], tooltip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'},
		{header: 'Trader', width: 24, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', orNull: true, displayField: 'label', url: 'securityUserListFind.json'}, viewNames: ['Default View', 'Export Friendly', 'Currency View']},
		{header: 'Order Destination', width: 28, dataIndex: 'orderDestination.name', idDataIndex: 'orderDestination.id', filter: {type: 'combo', searchFieldName: 'orderDestinationId', url: 'orderDestinationListFind.json?executionVenue=true'}, viewNames: ['Default View', 'Export Friendly', 'Currency View']},
		{header: 'Traded On', width: 23, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc', viewNames: ['Default View', 'Export Friendly', 'Currency View']},
		{header: 'Settled On', width: 23, dataIndex: 'settlementDate', viewNames: ['Default View', 'Export Friendly', 'Currency View']}
	],

	getDefaultDaysBack: function() {
		return 8;
	},

	// The grouped grid puts the filters in the top toolbar so we need to read from there not from the toolbar on the grid panel
	getTopToolbarWithFilters: function() {
		return this.getTopToolbar();
	},

	getLoadParams: async function(firstLoad) {
		if (firstLoad) {
			const gridPanel = this;
			const dd = this.getWindow().defaultData;
			if (dd && dd.forceReload) {
				this.clearFilters(true);
			}

			const displayFilters = TCG.getChildByName(gridPanel.getTopToolbarWithFilters(), 'displayFilters');
			if (TCG.isBlank(displayFilters.getValue())) {
				displayFilters.setValue(this.defaultDisplayFilter);
				displayFilters.fireEvent('select', displayFilters);
			}

			if (dd && (dd.fromDate || dd.toDate)) {
				const entryDate = {};
				if (dd.fromDate) {
					entryDate.after = dd.fromDate;
				}
				if (dd.toDate) {
					entryDate.before = dd.toDate.add(Date.DAY, 1); // include messages on toDate
				}
				this.setFilterValue('tradeDate', entryDate, false, true);
			}

			const team = await Clifton.system.security.getUserSecurityGroupForTag(this, 'Security Group Tags', 'PM Team');
			if (TCG.isNotNull(team)) {
				TCG.getChildByName(this.getTopToolbarWithFilters(), 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
				this.setFilterValue('clientAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
			}
		}
		const lp = {readUncommittedRequested: true};
		lp.workflowStatusNames = this.workflowStatusNames;
		lp.excludeWorkflowStatusNames = this.excludeWorkflowStatusNames;
		lp.excludeWorkflowStateNames = this.excludeWorkflowStateNames;

		if (this.getDefaultUserGroupName()) {
			if (TCG.getChildByName(this.getTopToolbar(), 'orderCreatorUserGroup') && TCG.getChildByName(this.getTopToolbar(), 'orderCreatorUserGroup').checked) {
				return TCG.data.getDataPromiseUsingCaching('securityGroupByName.json?name=' + this.getDefaultUserGroupName(), this, 'securityGroup.name.' + this.getDefaultUserGroupName())
					.then(group => {
						lp.orderCreatorUserGroupId = group.id;
						return lp;
					});
			}
		}
		return this.appendAdditionLoadParams(firstLoad, lp);
	},

	appendAdditionLoadParams: async function(firstLoad, params) {
		return params;
	},
	switchToViewBeforeReload: function(viewName) {
		// If no toolbar field than inside a detail window, i.e. block order and not necessary to switch this filter
		const otField = TCG.getChildByName(this.getTopToolbarWithFilters(), 'securityType');
		if (otField) {
			if (viewName === 'Currency View') {
				TCG.data.getDataPromiseUsingCaching('systemListItemByListAndText.json?listName=Security Type&text=Currency', this, 'security.type.Currency')
					.then(function(data) {
						const record = {text: data.text, value: data.id};
						otField.setValue(record);
						otField.fireEvent('select', otField, record);
					});
				return true; // cancel reload: select event will reload
			}
			else {
				this.clearFilter('security.securityType.text', true);
				otField.clearValue();
			}
		}
	},
	importTableName: [
		{table: 'CurrencyOrderAllocation', label: 'Currency Order Allocations', importComponentName: 'Clifton.order.allocation.upload.CurrencyOrderAllocationUploadWindow'}
	],
	getTopToolbarFilters: function(toolbar) {
		const gridPanel = this;

		const filters = [];
		if (TCG.isTrue(this.enableOrderAllocationUploadDragAndDrop)) {
			const gridPanel = this;
			// Doesn't add an extra filter, but added the DD Icon/Message to the toolbar
			// Drag and Drop Support
			filters.push({
				xtype: 'drag-drop-container',
				layout: 'fit',
				allowMultiple: false,
				bindToFormLoad: false,
				cls: undefined,
				popupComponentName: 'Clifton.order.allocation.upload.CurrencyOrderAllocationUploadWindow',
				message: '&nbsp;',
				tooltipMessage: 'Drag and drop a file to this grid to upload Currency Order Allocations.',
				// Object to reload - i.e. gridPanel to reload after attaching new file
				getReloadObject: function() {
					return gridPanel;
				}
			});
			filters.push('-');
		}
		if (this.getDefaultUserGroupName()) {
			filters.push({fieldLabel: '', boxLabel: 'Show ' + this.getDefaultUserGroupName() + ' Only', xtype: 'toolbar-checkbox', name: 'orderCreatorUserGroup', checked: true, qtip: 'Limit display to only those order allocations created by members of ' + this.getDefaultUserGroupName() + ' team.'});
			filters.push('-');
		}
		filters.push({fieldLabel: 'Security Type', xtype: 'toolbar-system-list-combo', width: 150, listName: 'Security Type', name: 'securityType', valueField: 'id', linkedFilter: 'security.securityType.text'});
		filters.push({fieldLabel: 'PM Team', name: 'teamSecurityGroupId', width: 120, xtype: 'toolbar-system-security-group-combo', groupTagName: 'PM Team', linkedFilter: 'clientAccount.teamSecurityGroup.name'});
		filters.push('-');

		const displayFilters = [];
		displayFilters.push(['ALL_DRAFT_PENDING', 'All Draft, Review or Pending (Not Approved for Trading)', 'Display order allocations with Workflow Status in Draft or Pending.']);
		displayFilters.push(['ALL_APPROVED', 'All Approved (Ready for Trading)', 'Display order allocations with Workflow Status = Approved, but are not being actively traded']);
		displayFilters.push(['ALL_ACTIVE', 'All Active (Working)', 'Display order allocations with Workflow Status = Active which means it is being actively traded.']);
		if (this.applyTraderFilter) {
			displayFilters.push(['MY_ACTIVE', 'My Active (Working)', 'Display order allocations with Workflow Status = Active which means it is being actively traded and I am the trader.']);
			displayFilters.push(['MY_TODAY', 'My Today', 'Display all order allocations with a trade date of today where I am the trader.']);
		}
		displayFilters.push(['ALL_TODAY_NOT_CANCELED', 'All Today (Exclude Canceled)', 'Display all order allocations with a trade date of today.']);
		displayFilters.push(['ALL_TODAY', 'All Today (Include Canceled)', 'Display all order allocations with a trade date of today.']);
		displayFilters.push(['ALL_NOT_CLOSED_CANCELED', 'All Not Closed or Canceled', 'Display all order allocations that have not been canceled or closed (fully allocated and trades generated).']);
		displayFilters.push(['ALL_NOT_CANCELED', 'All Not Canceled', 'Display all order allocations that have not been canceled. Will default trade date to after 7 days ago to limit initial query.']);
		displayFilters.push(['ALL', 'All', 'Display all order allocations.  Will default trade date to after 7 days ago to limit initial query.']);

		filters.push({
			fieldLabel: 'Display', xtype: 'combo', name: 'displayFilters', width: 250, minListWidth: 250, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: displayFilters
			}),
			listeners: {
				select: function(field) {
					const v = field.getValue();
					gridPanel.workflowStatusNames = [];
					gridPanel.excludeWorkflowStateNames = [];
					gridPanel.excludeWorkflowStatusNames = [];

					if (TCG.contains(v, 'TODAY')) {
						gridPanel.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)}, false, true);
					}
					else if (TCG.isEquals(v, 'ALL') || TCG.isEquals(v, 'ALL_NOT_CANCELED')) {
						gridPanel.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -8)}, false, true);
					}

					if (TCG.contains(v, 'MY')) {
						const trader = TCG.getCurrentUser();
						gridPanel.setFilterValue('traderUser.label', {value: trader.id, text: trader.label}, false, true);
					}
					else {
						gridPanel.clearFilter('traderUser.label', true);
					}


					if (TCG.contains(v, 'DRAFT_PENDING')) {
						gridPanel.clearFilter('tradeDate', true);
						gridPanel.workflowStatusNames = ['Draft', 'Review', 'Pending'];
					}
					else if (TCG.contains(v, 'APPROVED')) {
						gridPanel.clearFilter('tradeDate', true);
						gridPanel.workflowStatusNames = ['Approved'];
					}
					else if (TCG.contains(v, 'ACTIVE')) {
						gridPanel.clearFilter('tradeDate', true);
						gridPanel.workflowStatusNames = ['Active'];
					}
					else if (TCG.contains(v, 'NOT_CANCELED')) {
						gridPanel.excludeWorkflowStateNames = ['Canceled'];
					}
					else if (TCG.contains(v, 'NOT_CLOSED_CANCELED')) {
						gridPanel.clearFilter('tradeDate', true);
						gridPanel.excludeWorkflowStatusNames = ['Closed'];
					}
					gridPanel.reload();
				}
			}
		});
		return filters;
	},
	listeners: {
		afterrender: function(fp, isClosing) {
			const defaultView = this.getDefaultViewName();
			if (defaultView) {
				this.setDefaultView(defaultView);
			}
		}
	},
	getDefaultViewName: function() {
		return undefined;
	},
	getDefaultUserGroupName: function() {
		return undefined;
	},
	editor: {
		detailPageClass: 'Clifton.order.allocation.OrderAllocationWindow',
		drillDownOnly: true,
		addEditButtons: function(toolBar, gridPanel) {
			TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

			const gridEditor = this;

			toolBar.add({
				text: 'Add Note',
				tooltip: 'Add a note linked to selected order allocations (NOTE: Maximum of 50 order allocations can be selected)',
				iconCls: 'pencil',
				scope: this,
				handler: function() {
					const grid = gridPanel.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one order to add note to.', 'No Row(s) Selected');
					}
					else {
						const ut = sm.getSelections();
						gridEditor.addOrderAllocationNote(ut, gridEditor, gridPanel);
					}
				}
			});
			toolBar.add('-');
		},
		addOrderAllocationNote: function(rows, editor, gridPanel) {
			TCG.showError('not supported yet');
			//Clifton.order.addOrderAllocationNote(rows, editor, gridPanel, 'id', 'orderType.name', false, 50);
		}
	}
});
Ext.reg('order-allocation-list-grid', Clifton.order.allocation.OrderAllocationListGrid);


