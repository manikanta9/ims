Clifton.order.allocation.BaseOrderAllocationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Allocation',
	titlePrefixSeparator: ': ',
	width: 1000,
	height: 500,
	iconCls: 'shopping-cart',
	enableRefreshWindow: true,


	// Properties to Override in Extending Classes:
	orderAllocationInfoTab: 'OVERRIDE_ME',


	init: function () {
		// replace the first tab with the override
		const win = this;
		const tabs = win.items[0].items;
		tabs[0] = this.orderAllocationInfoTab;
		Clifton.order.allocation.BaseOrderAllocationWindow.superclass.init.apply(this, arguments);
	},


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,
		items: [

			{}, // first tab to be overridden with orderAllocationInfoTab


			{
				title: 'Violations',
				items: [{
					xtype: 'order-allocation-violations-grid'
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'OrderAllocation',
					defaultActiveFilter: false,
					// Set to true to see notes for related entities - i.e. on Order Allocations also see block order
					includeNotesForLinkedEntity: true
				}]
			},

			{
				title: 'Placement Allocations',
				items: [{
					xtype: 'order-placement-allocation-list-grid',
					instructions: 'This Order Allocation has been placed with the following destinations.',
					columnOverrides: [
						{dataIndex: 'orderAllocation.orderType.name', hidden: true},
						{dataIndex: 'orderAllocation.clientAccount.label', hidden: true},
						{dataIndex: 'orderAllocation.buy', hidden: true},
						{dataIndex: 'orderAllocation.security.ticker', hidden: true},
						{dataIndex: 'orderAllocation.security.name', hidden: true},
						{dataIndex: 'orderPlacement.orderBlock.traderUser.label', hidden: true},
						{dataIndex: 'orderPlacement.orderBlock.tradeDate', hidden: true}
					],
					getTopToolbarFilters: function (toolbar) {
						return [];
					},
					getLoadParams: function (firstLoad) {
						return {orderAllocationId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Trades',
				items: [{
					xtype: 'order-trade-list-grid',
					instructions: 'The following trades were generated as a result of an executed / filled / allocated order and placement. Each Order Allocation may result in multiple trades if it is filled via different executing brokers and / or whether or not average pricing can be used.',
					columnOverrides: [
						{dataIndex: 'orderAllocation.orderType.name', hidden: true},
						{dataIndex: 'orderAllocation.clientAccount.label', hidden: true},
						{dataIndex: 'openCloseType.buy', hidden: true},
						{dataIndex: 'orderAllocation.security.ticker', hidden: true},
						{dataIndex: 'orderAllocation.security.name', hidden: true}

					],
					getLoadParams: function (firstLoad) {
						return {orderAllocationIdentifier: this.getWindow().getMainFormId()};
					}
				}]

			},

			{
				title: 'Order Life Cycle',
				items: [{
					xtype: 'system-lifecycle-grid',
					tableName: 'OrderAllocation'
				}]

			}

		]
	}]
});
