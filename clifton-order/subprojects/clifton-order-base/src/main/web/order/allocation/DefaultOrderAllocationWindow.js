TCG.use('Clifton.order.allocation.BaseOrderAllocationWindow');
TCG.use('Clifton.order.allocation.BaseOrderAllocationForm');

Clifton.order.allocation.DefaultOrderAllocationWindow = Ext.extend(Clifton.order.allocation.BaseOrderAllocationWindow, {
	height: 600,

	orderAllocationInfoTab: {
		title: 'Info',
		tbar: {
			xtype: 'order-allocation-workflow-toolbar'
		},

		items: [{
			xtype: 'order-allocation-form-base',

			applyAdditionalAfterLoad: function(readOnlyFlag) {
				const fp = this;
				const f = this.getForm();
				const v = f.findField('accountingNotional').getNumericValue();

				let label = 'Notional';
				let value = v;
				if (['Stocks', 'Funds'].includes(this.getFormValue('orderType.securityType.text'))) {
					const buy = (fp.getFormValue('buy') === true);
					value = buy ? -v : v;
					value = value + (f.findField('feeAmount').getNumericValue() || 0) + (f.findField('commissionAmount').getNumericValue() || 0);

					label = (value < 0) ? 'Payment' : 'Proceeds';
					f.findField('accountingNotional').setFieldLabel(label + ':');
					const nn = f.findField('netNotional');
					nn.setFieldLabel('Net ' + label + ':');
					if (value < 0) {
						value = -value;
					}
					fp.setFormValue('netNotional', value, true);
					nn.show();
				}

				const type = this.getFormValue('orderType.securityType.text');
				if (type === 'Options' || type === 'Swaptions') {
					f.findField('accountingNotional').setFieldLabel('Premium:');
				}
			},

			items: [],

			updatableItems: [
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', defaults: {anchor: '-20'}},
					items: [
						{
							columnWidth: .71,
							items: [
								{xtype: 'hidden', name: 'clientAccount.clientCompany.id', submitValue: false},
								{
									fieldLabel: 'Client Account',
									name: 'clientAccount.label',
									hiddenName: 'clientAccount.id',
									displayField: 'label',
									xtype: 'combo',
									url: 'orderAccountListFind.json?clientAccount=true&workflowStatusNameEquals=Active',
									allowBlank: false,
									anchor: '-20',
									requestedProps: 'holdingAccount|baseCurrencyCode|clientCompany.id',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									listeners: {
										select: function(combo, record, index) {
											const fp = TCG.getParentFormPanel(combo);
											const currencyCode = record.json.baseCurrencyCode;
											if (currencyCode) {
												fp.setFormValue('clientAccount.baseCurrencyCode', currencyCode);
											}

											const clientCompanyId = record.json.clientCompany.id;
											fp.setFormValue('clientAccount.clientCompany.id', clientCompanyId);
											fp.updateHoldingAccountSelection(record);
										}
									}
								},
								{
									fieldLabel: 'Holding Account',
									name: 'holdingAccount.label',
									hiddenName: 'holdingAccount.id',
									displayField: 'label',
									xtype: 'combo',
									url: 'orderAccountListFind.json?holdingAccount=true',
									allowBlank: false,
									anchor: '-20',
									requiredFields: ['clientAccount.label'],
									requestedProps: 'baseCurrencyCode',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									beforeSelect: function(combo, record) {
										const fp = combo.getParentForm();
										const f = fp.getForm();
										const executingBrokerField = f.findField('executingBrokerCompany.label');
										const currencyCode = record.json.baseCurrencyCode;
										if (currencyCode) {
											fp.setFormValue('holdingAccount.baseCurrencyCode', currencyCode);
										}
										if (executingBrokerField) {
											executingBrokerField.store.removeAll();
											executingBrokerField.lastQuery = null;
										}
									},
									listeners: {
										select: function(combo, record) {
											combo.beforeSelect(combo, record);
										},
										beforequery: function(queryEvent) {
											const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
											const clientCompanyId = form.findField('clientAccount.clientCompany.id').value;
											queryEvent.combo.store.setBaseParam('clientCompanyId', clientCompanyId);
										}
									}
								}
							]
						},
						{
							columnWidth: .29,
							labelWidth: 60,
							items: [
								{
									fieldLabel: 'Base CCY',
									name: 'clientAccount.baseCurrencyCode',
									submitValue: false,
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow',
									useNameFieldValueForDetailIdFieldValue: true
								},
								{
									fieldLabel: 'Base CCY',
									name: 'holdingAccount.baseCurrencyCode',
									submitValue: false,
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow',
									useNameFieldValueForDetailIdFieldValue: true
								}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
					items: [
						{
							columnWidth: .31,
							labelWidth: 110,
							items: [
								{
									fieldLabel: 'Order Type',
									name: 'orderType.name',
									xtype: 'linkfield',
									detailIdField: 'orderType.id',
									detailPageClass: 'Clifton.order.setup.OrderTypeWindow'
								},
								{
									xtype: 'radiogroup',
									fieldLabel: 'Buy/Sell',
									columns: [60, 60],
									allowBlank: false,
									anchor: '-25',
									items: [
										{boxLabel: 'Buy', name: 'buy', inputValue: true},
										{boxLabel: 'Sell', name: 'buy', inputValue: false}
									],
									listeners: {
										change: function(radioGroup, checkedRadio) {
											const formPanel = TCG.getParentFormPanel(radioGroup);
											const params = {
												open: true,
												close: true,
												buy: checkedRadio.getRawValue()
											};
											TCG.data.getDataPromise('orderOpenCloseTypeListFind.json', radioGroup, {params: params})
												.then(function(openCloseType) {
													if (openCloseType && openCloseType.length === 1) {
														formPanel.setFormValue('openCloseType.id', openCloseType[0].id);
													}
												});
										}
									}
								},
								{xtype: 'hidden', name: 'openCloseType.id'},
								{xtype: 'hidden', name: 'notionalMultiplier', value: 1}, // TODO SERVER SIDE LOGIC?
								{
									fieldLabel: 'Security',
									name: 'security.label',
									allowBlank: false,
									displayField: 'label',
									hiddenName: 'security.id',
									xtype: 'combo',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									url: 'orderSecurityListFind.json',
									requiredFields: ['holdingAccount.label'],
									doNotClearIfRequiredSet: true,
									requestedProps: 'currencyCode',
									listeners: {
										beforequery: function(queryEvent) {
											const fp = queryEvent.combo.getParentForm();
											queryEvent.combo.store.setBaseParam('activeOnDate', fp.getForm().findField('tradeDate').value);
											queryEvent.combo.store.setBaseParam('securityType', fp.getFormValue('orderType.securityType.text'));

										},

										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											const form = fp.getForm();

											const currencyCode = record.json.currencyCode;
											if (currencyCode) {
												TCG.data.getDataPromiseUsingCaching('orderSecurityForCurrencyTicker.json?ticker=' + currencyCode, fp, 'order.shared.security.currency.' + currencyCode)
													.then(function(curr) {
														fp.setFormValue('settlementCurrency.label', curr.label);
														fp.setFormValue('settlementCurrency.id', curr.id);
														Clifton.order.shared.security.getTradeDatePromise(record.id, null, combo)
															.then(function(tradeDate) {
																form.findField('tradeDate').setValue(tradeDate);
																return Clifton.order.shared.security.getSettlementDatePromise(record.id, tradeDate, false, (curr ? curr.id : null), combo);
															})
															.then(function(settlementDate) {
																form.findField('settlementDate').setValue(settlementDate);
																fp.updatePrice.call(fp);
															});
													});
											}
										}
									}
								},
								{
									fieldLabel: 'Quantity',
									name: 'quantityIntended',
									xtype: 'spinnerfield',
									minValue: 0.00000001,
									maxValue: 10000000000,
									allowBlank: false,
									allowDecimals: true,
									decimalPrecision: 10,
									listeners: {
										change: function(field, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(field);
											fp.updateAccountingNotional(); // quantity update will update accounting notional
										}
									}

								},
								{
									fieldLabel: 'Settle CCY',
									name: 'settlementCurrency.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									detailIdField: 'settlementCurrency.id'
								}
							]
						},
						{
							columnWidth: .40,
							labelWidth: 100,
							items: [
								{
									fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo',
									requiredFields: ['holdingAccount.label'],
									displayField: 'label',
									detailPageClass: 'Clifton.order.shared.company.CompanyWindow',
									url: 'orderManagementRuleExecutingBrokerCompanyListFind.json',
									allowBlank: true,
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const fp = combo.getParentForm();

											combo.store.setBaseParam('command.clientAccountId', fp.getFormValue('clientAccount.id', true));
											combo.store.setBaseParam('command.holdingAccountId', fp.getFormValue('holdingAccount.id', true));
											let tradeDate = fp.getFormValue('tradeDate', true);
											if (TCG.isNotBlank(tradeDate)) {
												tradeDate = tradeDate.format('m/d/Y');
											}
											combo.store.setBaseParam('command.tradeDate', tradeDate);
											combo.store.setBaseParam('command.orderTypeId', fp.getFormValue('orderType.id'));
											combo.store.setBaseParam('command.orderSecurityId', fp.getFormValue('security.id', true));
											combo.store.setBaseParam('orderDestinationId', fp.getFormValue('orderDestination.id', true));
										},
										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											// clear existing destination query (but leave value that is there selected)
											const od = fp.getForm().findField('orderDestination.label');
											od.resetStore();
										}

									}
								},
								{
									fieldLabel: 'Destination',
									name: 'orderDestination.label',
									hiddenName: 'orderDestination.id',
									xtype: 'combo',
									disableAddNewItem: true,
									detailPageClass: 'Clifton.order.setup.destination.DestinationWindow',
									url: 'orderManagementDestinationListForMapping.json',
									allowBlank: true,
									loadAll: true,
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const fp = combo.getParentForm();
											if (TCG.isNotBlank(fp.getFormValue('executingBrokerCompany.id', true))) {
												combo.store.setBaseParam('executingBrokerCompanyIdOrNull', fp.getFormValue('executingBrokerCompany.id', true));
											}
											combo.store.setBaseParam('orderTypeId', fp.getFormValue('orderType.id', true));
											let tradeDate = fp.getFormValue('tradeDate', true);
											if (TCG.isNotBlank(tradeDate)) {
												tradeDate = tradeDate.format('m/d/Y');
											}
											combo.store.setBaseParam('activeOnDate', tradeDate);
										},
										select: function(combo, record, index) {
											const fp = combo.getParentForm();
											// clear existing executing broker query (but leave value that is there selected)
											const eb = fp.getForm().findField('executingBrokerCompany.label');
											eb.resetStore();
										}
									}

								},
								{xtype: 'hidden', name: 'exchangeRateToSettlementCurrency', value: 1}
							]
						},
						{
							columnWidth: .29,
							labelWidth: 100,
							items: [
								{
									fieldLabel: 'Block Order ID',
									name: 'orderBlockIdentifier',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.execution.OrderBlockWindow',
									detailIdField: 'orderBlockIdentifier'
								},
								{
									fieldLabel: 'Order Group',
									name: 'orderGroup.groupType.name',
									xtype: 'linkfield',
									detailIdField: 'orderGroup.id',
									detailPageClass: 'Clifton.order.group.OrderGroupWindow'
								},
								{
									fieldLabel: 'Order Source',
									name: 'orderSourceWithIdentifier',
									xtype: 'linkfield',
									detailIdField: 'orderSource.id',
									detailPageClass: 'Clifton.order.setup.OrderSourceWindow',
									qtip: 'This field is the combination of Order Source and the Extrnal Identifier'
								},
								{
									fieldLabel: 'Created By',
									name: 'orderCreatorUser.label',
									xtype: 'linkfield',
									detailIdField: 'orderCreatorUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow',
									qtip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'
								},
								{
									fieldLabel: 'Trader',
									name: 'traderUser.label',
									xtype: 'linkfield',
									detailIdField: 'traderUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow'
								},
								{
									fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
									getParentFormPanel: function() {
										const parent = TCG.getParentFormPanel(this);
										return parent || this.ownerCt;
									},

									listeners: {
										change: function(field, newValue, oldValue) {
											const form = field.getParentFormPanel().getForm();
											const settlementDateField = form.findField('settlementDate');

											if (TCG.isBlank(newValue)) {
												settlementDateField.setValue('');
											}
											else {
												Clifton.order.shared.security.getSettlementDatePromise(form.findField('security.id').value, newValue, false, form.findField('settlementCurrency.id').value, field)
													.then(function(settlementDate) {
														form.findField('settlementDate').setValue(settlementDate);
													});
											}
										},
										select: function(field, date) {
											const form = field.getParentFormPanel().getForm();
											Clifton.order.shared.security.getSettlementDatePromise(form.findField('security.id').value, date, false, form.findField('settlementCurrency.id').value, field)
												.then(function(settlementDate) {
													form.findField('settlementDate').setValue(settlementDate);
												});
										}
									}

								},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 120,
					items: [
						{
							columnWidth: .31,
							layout: 'form',
							defaults: {xtype: 'textfield'},
							items: [
								{
									fieldLabel: 'Expected Unit Price', name: 'expectedUnitPrice', xtype: 'pricefield',
									listeners: {
										change: function(field, newValue, oldValue) {
											const fp = TCG.getParentFormPanel(field);
											fp.updateAccountingNotional().call(fp); // price update will update accounting notional
										}
									}

								}
							]
						},
						{
							columnWidth: .40,
							layout: 'form',
							labelWidth: 120,
							defaults: {xtype: 'textfield'},
							items: [
								{
									fieldLabel: 'Commission Amount',
									name: 'commissionAmount',
									xtype: 'currencyfield4',
									readOnly: true,
									submitValue: false,
									qtip: 'Total Commission Amount for this order allocation. Negative amount means that the client pays commission.'
								},
								{fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield4', readOnly: true, qtip: 'Total Fees for this order allocation'}
							]
						},
						{
							columnWidth: .29,
							layout: 'form',
							labelWidth: 130,
							defaults: {xtype: 'textfield', anchor: '-20'},
							items: [
								{fieldLabel: 'Accounting Notional', name: 'accountingNotional', xtype: 'currencyfield4', readOnly: true},
								{fieldLabel: 'Net Notional', name: 'netNotional', xtype: 'currencyfield4', readOnly: true, hidden: true}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', anchor: '-20'}
			],

			readOnlyItems: [
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', defaults: {anchor: '-20'}},
					items: [
						{
							columnWidth: .71,
							items: [
								{
									fieldLabel: 'Client Account',
									name: 'clientAccount.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									detailIdField: 'clientAccount.id'
								},
								{
									fieldLabel: 'Holding Account',
									name: 'holdingAccount.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									detailIdField: 'holdingAccount.id'
								}
							]
						},
						{
							columnWidth: .29,
							labelWidth: 60,
							items: [
								{
									fieldLabel: 'Base CCY',
									name: 'clientAccount.baseCurrencyCode',
									submitValue: false,
									xtype: 'linkfield',
									detailIdField: 'clientAccount.baseCurrencyCode',
									detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow'
								},
								{fieldLabel: 'Base CCY', name: 'holdingAccount.baseCurrencyCode', xtype: 'displayfield'}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 120,
					items: [
						{
							columnWidth: .31,
							layout: 'form',
							defaults: {xtype: 'textfield'},
							items: [
								{
									fieldLabel: 'Order Type',
									name: 'orderType.name',
									xtype: 'linkfield',
									detailIdField: 'orderType.id',
									detailPageClass: 'Clifton.order.setup.OrderTypeWindow'
								},
								{
									fieldLabel: 'Side', name: 'openCloseType.label', xtype: 'displayfield', width: 125,
									setRawValue: function(v) {
										this.el.addClass(v.includes('BUY') ? 'buy' : 'sell');
										TCG.form.DisplayField.superclass.setRawValue.call(this, v);
									}
								},
								{
									fieldLabel: 'Quantity',
									name: 'quantityIntended',
									xtype: 'spinnerfield',
									minValue: 0.00000001,
									maxValue: 10000000000,
									width: 129,
									readOnly: true,
									allowBlank: false
								},
								{
									fieldLabel: 'Security',
									name: 'security.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									detailIdField: 'security.id'
								},
								{
									fieldLabel: 'Settle CCY',
									name: 'settlementCurrency.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
									detailIdField: 'settlementCurrency.id'
								}
							]
						},
						{
							columnWidth: .40,
							layout: 'form',
							labelWidth: 100,
							items: [
								{
									fieldLabel: 'Executing Broker',
									name: 'executingBrokerCompany.label',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.shared.company.CompanyWindow',
									detailIdField: 'executingBrokerCompany.id'
								},
								{
									fieldLabel: 'Order Destination',
									name: 'orderDestination.name',
									xtype: 'linkfield',
									detailIdField: 'orderDestination.id',
									detailPageClass: 'Clifton.order.setup.destination.DestinationWindow'
								}

							]
						},
						{
							columnWidth: .29,
							layout: 'form',
							labelWidth: 130,
							defaults: {xtype: 'textfield', anchor: '-20'},
							items: [
								{
									fieldLabel: 'Block Order ID',
									name: 'orderBlockIdentifier',
									xtype: 'linkfield',
									detailPageClass: 'Clifton.order.execution.OrderBlockWindow',
									detailIdField: 'orderBlockIdentifier'
								},
								{
									fieldLabel: 'Order Group',
									name: 'orderGroup.groupType.name',
									xtype: 'linkfield',
									detailIdField: 'orderGroup.id',
									detailPageClass: 'Clifton.order.group.OrderGroupWindow'
								},
								{
									fieldLabel: 'Order Source',
									name: 'orderSourceWithIdentifier',
									xtype: 'linkfield',
									detailIdField: 'orderSource.id',
									detailPageClass: 'Clifton.order.setup.OrderSourceWindow',
									qtip: 'This field is the combination of Order Source and the Extrnal Identifier'
								},
								{
									fieldLabel: 'Created By',
									name: 'orderCreatorUser.label',
									xtype: 'linkfield',
									detailIdField: 'orderCreatorUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow',
									qtip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'
								},
								{
									fieldLabel: 'Trader',
									name: 'traderUser.label',
									xtype: 'linkfield',
									detailIdField: 'traderUser.id',
									detailPageClass: 'Clifton.security.user.UserWindow'
								},
								{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
								{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true}
							]
						}
					]
				},

				{xtype: 'label', html: '<hr/>'},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 120,
					items: [
						{
							columnWidth: .35,
							layout: 'form',
							defaults: {xtype: 'textfield'},
							items: [
								{fieldLabel: 'Expected Price', name: 'expectedUnitPrice', xtype: 'floatfield', readOnly: true},
								{fieldLabel: 'Average Price', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true}
								//{fieldLabel: 'FX Rate', name: 'exchangeRateToSettlementCurrency', xtype: 'floatfield', readOnly: true}
							]
						},
						{
							columnWidth: .34,
							layout: 'form',
							labelWidth: 120,
							defaults: {xtype: 'textfield'},
							items: [
								{
									fieldLabel: 'Commission Amount',
									name: 'commissionAmount',
									xtype: 'currencyfield4',
									readOnly: true,
									submitValue: false,
									qtip: 'Total Commission Amount for this order allocation. Negative amount means that the client pays commission.'
								},
								{fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield4', readOnly: true, qtip: 'Total Fees for this order allocation.'}
							]
						},
						{
							columnWidth: .31,
							layout: 'form',
							labelWidth: 130,
							defaults: {xtype: 'textfield', anchor: '-20'},
							items: [
								{fieldLabel: 'Accounting Notional', name: 'accountingNotional', xtype: 'currencyfield4', readOnly: true},
								{fieldLabel: 'Net Notional', name: 'netNotional', xtype: 'currencyfield4', readOnly: true, hidden: true}
							]
						}
					]
				},
				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', anchor: '-20'}
			]
		}
		]

	}
});
