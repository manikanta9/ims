Clifton.order.allocation.ReviseDatesOrderAllocationTransitionEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Revise Order Allocation Dates',
	iconCls: 'calendar',
	height: 325,
	width: 700,
	modal: true,


	callBack: undefined, // When transition and close - call back can be used to close the original window - defined by the opener

	init: function() {
		// change defaultData to a method so can convert to each forms default data from the transition
		this.transitionParams = this.defaultData.transitionParams;
		const dd = this.defaultData;
		this.defaultData = this.getDefaultData(dd);

		Clifton.order.allocation.ReviseDatesOrderAllocationTransitionEntryWindow.superclass.init.apply(this, arguments);

		this.win.saveForm = this.win.saveForm.createInterceptor(this.executeTransition);
	},

	getDefaultData: function(dd) {
		const win = this;
		const fp = win.openerCt.getFormPanel();
		return {
			orderAllocation: fp.getForm().formValues,
			currentTradeDate: fp.getFormValue('tradeDate'),
			currentSettlementDate: fp.getFormValue('settlementDate'),
			tradeDate: fp.getFormValue('tradeDate'),
			settlementDate: fp.getFormValue('settlementDate')
		};
	},

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Enter the new trade and settlement dates for the Order Allocation. All rules will be re-ran against these new values.  You can manually enter new dates or use the quick options below.',
		items: [
			{
				xtype: 'columnpanel',
				columns: [
					{
						rows: [
							{xtype: 'sectionheaderfield', header: 'Current'},
							{fieldLabel: 'Trade Date', name: 'currentTradeDate', xtype: 'datefield', readOnly: true, submitValue: false},
							{fieldLabel: 'Settlement Date', name: 'currentSettlementDate', xtype: 'datefield', readOnly: true, submitValue: false}
						],
						config: {columnWidth: 0.44}
					},
					{
						rows: [
							{xtype: 'label', html: '&nbsp;'}
						],
						config: {columnWidth: 0.1}
					},
					{
						rows: [
							{xtype: 'sectionheaderfield', header: 'New'},
							{
								xtype: 'compositefield', fieldLabel: 'Trade Date',

								items: [
									{xtype: 'datefield', name: 'tradeDate', flex: 1, height: 22, allowBlank: false},
									{
										xtype: 'button', iconCls: 'calculator', width: 25, tooltip: 'Click to recalculate Trade Date as next available date from now.',
										handler: function(btn) {
											TCG.getParentFormPanel(btn.findParentByType('compositefield')).recalculateTradeDate(false);
										}
									},
									{
										xtype: 'button', iconCls: 'arrow-right-green', width: 25, tooltip: 'Click to move trade date to following trading day from selected trade date.',
										handler: function(btn) {
											TCG.getParentFormPanel(btn.findParentByType('compositefield')).moveTradeDateForward();
										}
									}
								]
							},
							{
								xtype: 'compositefield', fieldLabel: 'Settlement Date',

								items: [
									{xtype: 'datefield', name: 'settlementDate', flex: 1, height: 22, allowBlank: false},
									{
										xtype: 'button', iconCls: 'calculator', width: 25, tooltip: 'Click to recalculate Settlement Date based off of new trade date and days to settle.',
										handler: function(btn) {
											TCG.getParentFormPanel(btn.findParentByType('compositefield')).recalculateSettlementDate();
										}
									},
									{
										xtype: 'button', iconCls: 'arrow-right-green', width: 25, tooltip: 'Click to move settlement date to following settlement day from selected settlement date.',
										handler: function(btn) {
											TCG.getParentFormPanel(btn.findParentByType('compositefield')).moveSettlementDateForward();
										}
									}
								]
							}
						],
						config: {columnWidth: 0.46}
					}
				]
			}
		],

		getSecurityId: function() {
			const win = this.getWindow();
			return TCG.getValue('orderAllocation.security.id', win.defaultData);
		},

		getSettlementSecurityId: function() {
			const win = this.getWindow();
			if (TCG.isTrue(TCG.getValue('orderAllocation.security.currency', win.defaultData))) {
				return TCG.getValue('orderAllocation.settlementCurrency.id', win.defaultData);
			}
			return null;
		},

		moveTradeDateForward: function() {
			const p = this;
			if (TCG.isBlank(p.getFormValue('tradeDate', true, true))) {
				TCG.showError('Enter a trade date to move forward from.');
				return;
			}
			Clifton.order.shared.security.getTradeDateNextPromise(p.getSecurityId(), p.getFormValue('tradeDate', true, true), p)
				.then(function(tradeDate) {
					p.setFormValue('tradeDate', tradeDate);
				});
		},

		moveSettlementDateForward: function() {
			const p = this;
			if (TCG.isBlank(p.getFormValue('settlementDate', true, true))) {
				TCG.showError('Enter a settlement date to move forward from.');
				return;
			}
			Clifton.order.shared.security.getSettlementDateNextPromise(p.getSecurityId(), p.getFormValue('settlementDate', true, true), null, p.getSettlementSecurityId(), p)
				.then(function(settlementDate) {
					p.setFormValue('settlementDate', settlementDate);
				});
		},

		recalculateTradeDate: function() {
			const p = this;
			Clifton.order.shared.security.getTradeDatePromise(p.getSecurityId(), null, p)
				.then(function(tradeDate) {
					p.setFormValue('tradeDate', tradeDate);
				});
		},

		recalculateSettlementDate: function() {
			const p = this;
			if (TCG.isBlank(p.getFormValue('tradeDate', true, true))) {
				TCG.showError('Enter a trade date to calculate settlement date for.');
				return;
			}
			Clifton.order.shared.security.getSettlementDatePromise(p.getSecurityId(), p.getFormValue('tradeDate', true, true), null, p.getSettlementSecurityId(), p)
				.then(function(settlementDate) {
					p.setFormValue('settlementDate', settlementDate);
				});
		}
	}],

	executeTransition: function() {
		const win = this;
		const fp = this.getMainFormPanel();

		const transitionParams = {
			...this.transitionParams,
			tradeDate: fp.getForm().findField('tradeDate').value,
			settlementDate: fp.getForm().findField('settlementDate').value
		};

		const url = 'orderAllocationWithRevisedDatesSave.json';
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Executing Transition',
			waitTarget: fp,
			params: transitionParams,
			onLoad: function(record, conf) {
				win.openerCt.reloadAfterTransition(record);
				if (win.callBack) {
					win.callBack.call(fp);
				}
				win.closeWindow();

			}
		});
		loader.load(url);
		return false;
	}


});
