Clifton.order.OmsDashboardWindow = Ext.extend(TCG.app.Window, {
	id: 'omsDashboardWindow',
	title: 'OMS Dashboard',
	iconCls: 'chart-bar',
	enableRefreshWindow: true,
	width: 1500,
	height: 810,

	items: [{
		xtype: 'panel',
		layout: 'table',
		layoutConfig: {
			columns: 3,
			tableAttrs: {
				style: {'table-layout': 'fixed', width: '100%'},
				cellspacing: 3
			}
		},
		defaults: {height: 368},

		listeners: {
			afterrender: function(p) {
				TCG.getChildByName(p, 'tradeDate').updateDates('0'); // 'Today'
			}
		},

		getQueryParameters: function(includeGroupBy) {
			const params = {
				fromDate: TCG.getChildByName(this, 'fromDate').getValue().format('m/d/Y'),
				toDate: TCG.getChildByName(this, 'toDate').getValue().format('m/d/Y'),
				workflowFilter: TCG.getChildByName(this, 'workflowFilter').getValue()
			};
			let o = TCG.getChildByName(this, 'traderUser').getValue();
			if (TCG.isNotBlank(o)) {
				params.traderUserId = o;
			}
			o = TCG.getChildByName(this, 'orderDestination').getValue();
			if (TCG.isNotBlank(o)) {
				params.orderDestinationId = o;
			}
			if (includeGroupBy) {
				params.groupBy = TCG.getChildByName(this, 'groupBy').getValue();
			}
			return params;
		},

		items: [
			{
				xtype: 'panel',
				layout: 'hbox',
				bodyStyle: 'padding: 3px 0px 0px 3px',
				defaults: {},
				colspan: 3,
				height: 30,
				items: [
					{xtype: 'displayfield', value: 'Trade Date:', style: 'padding: 5px 5px 0px 5px;'},
					{
						name: 'tradeDate', xtype: 'combo', width: 105, minListWidth: 105, value: 'Today', mode: 'local', store: {xtype: 'arraystore', data: [['0', 'Today'], ['1', 'Last 2 Days'], ['7', 'Last Week'], ['30', 'Last Month'], ['180', 'Last 6 Months'], ['365', 'Last Year'], ['730', 'Last 2 Years'], ['14', 'Custom...']]},
						listeners: {
							beforeselect: function(combo, record) {
								combo.updateDates(record.data.id);
							}
						},
						updateDates: function(value) {
							const fromDate = new Date().add(Date.DAY, -parseInt(value));
							const toDate = new Date();
							const p = TCG.getParentByClass(this, Ext.Panel);
							TCG.getChildByName(p, 'fromDate').setValue(fromDate.format('m/d/Y'));
							TCG.getChildByName(p, 'toDate').setValue(toDate.format('m/d/Y'));
						}
					},
					{xtype: 'displayfield', value: 'From:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'fromDate', xtype: 'datefield', width: 95, allowBlank: false},
					{xtype: 'displayfield', value: 'To:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'toDate', xtype: 'datefield', width: 95, allowBlank: false},

					{xtype: 'displayfield', value: 'Trader:', style: 'padding: 5px 5px 0px 40px;'},
					{name: 'traderUser', xtype: 'combo', width: 170, minListWidth: 230, url: 'securityUserListFind.json', displayField: 'label'},
					{xtype: 'displayfield', value: 'Destination:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'orderDestination', xtype: 'combo', width: 170, minListWidth: 230, url: 'orderDestinationListFind.json'},
					{xtype: 'displayfield', value: 'Workflow:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'workflowFilter', xtype: 'combo', width: 100, minListWidth: 110, value: 'ALLOCATED', mode: 'local', store: {xtype: 'arraystore', data: [['ALL', 'All', 'Include all Order Allocations: allocated, cancelled, etc.'], ['ALLOCATED', 'Allocated', 'Limit to Order Allocations that have been Allocated.'], ['NOT_CANCELLED', 'Not Cancelled', 'Limit to Order Allocations that have NOT been Cancelled.'], ['CANCELLED', 'Cancelled', 'Limit to Order Allocations that have been Cancelled.']]}},
					{xtype: 'displayfield', value: 'Group By:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'groupBy', xtype: 'combo', width: 120, minListWidth: 120, value: 'TRADER', mode: 'local', store: {xtype: 'arraystore', data: [['TRADER', 'Trader', 'Group historical Order Allocation counts by Trader.'], ['DESTINATION', 'Order Destination', 'Group historical Order Allocation counts by Order Destination.'], ['EXECUTING_BROKER', 'Executing Broker', 'Group historical Order Allocation counts by Executing Broker.']]}},

					{
						xtype: 'button', text: 'Reload', iconCls: 'table-refresh', width: 80, style: 'margin-left: 35px;',
						handler: function(btn) {
							const items = TCG.getParentByClass(TCG.getParentByClass(btn, Ext.Panel), Ext.Panel).items;
							items.each(o => {
								if (o.reload) {
									o.reload();
								}
							});
						}
					}
				]
			},


			{
				xtype: 'system-query-doughnut-chart',
				title: 'Order Allocation Counts by Trader',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters();
				},
				onClickEvent: function(data, label, datasetLabel, fullData) {
					const params = this.queryParams();
					TCG.createComponent('Clifton.order.OrderHistoryWindow', {
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDate(params.fromDate, 'm/d/Y'),
							toDate: TCG.parseDate(params.toDate, 'm/d/Y'),
							traderUser: label
						}
					});
				}
			},
			{
				xtype: 'system-query-doughnut-chart',
				title: 'Order Allocation Counts by Order Destination',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters();
				},
				onClickEvent: function(data, label, datasetLabel) {
					const params = this.queryParams();
					TCG.createComponent('Clifton.order.OrderHistoryWindow', {
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDate(params.fromDate, 'm/d/Y'),
							toDate: TCG.parseDate(params.toDate, 'm/d/Y'),
							orderDestination: label
						}
					});
				}
			},
			{
				xtype: 'system-query-doughnut-chart',
				title: 'Order Allocation Counts by Executing Broker',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters();
				},
				onClickEvent: function(data, label, datasetLabel) {
					const params = this.queryParams();
					TCG.createComponent('Clifton.order.OrderHistoryWindow', {
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDate(params.fromDate, 'm/d/Y'),
							toDate: TCG.parseDate(params.toDate, 'm/d/Y'),
							executingBroker: label
						}
					});
				}
			},


			{
				xtype: 'system-query-stackedbar-chart',
				title: 'Historical Order Allocation Counts',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters(true);
				},
				colspan: 3,
				onClickEvent: function(data, label, datasetLabel) {
					// find the next label
					let toDate;
					const labels = this.chart.data.labels;
					for (let i = 0; i < labels.length; i++) {
						if (labels[i] === label) {
							i++;
							if (i < labels.length) {
								toDate = labels[i];
							}
							break;
						}
					}
					TCG.createComponent('Clifton.order.OrderHistoryWindow', {
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDateSmart(label),
							toDate: TCG.parseDateSmart(toDate || this.queryParams().toDate)
						}
					});
				}
			}
		]
	}]
});
