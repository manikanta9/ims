Ext.ns(
	'Clifton.order',
	'Clifton.order.allocation',
	'Clifton.order.allocation.activity',
	'Clifton.order.allocation.upload',
	'Clifton.order.execution',
	'Clifton.order.group',
	'Clifton.order.trade'
);


Clifton.order.allocation.OrderAllocationWindowOverrides = {
	'Currency': 'Clifton.order.allocation.CurrencyOrderAllocationWindow',
	'Bonds': 'Clifton.order.allocation.BondOrderAllocationWindow'
};

// Overridden in clifton-order-fix for Fix order placements
Clifton.order.execution.OrderPlacementWindowOverrides = {};
Clifton.order.execution.OrderPlacementWindowOverrides['File Order'] = 'Clifton.order.execution.OrderFilePlacementWindow';


Clifton.order.trade.OrderTradeWindowOverrides = {
	'Currency': 'Clifton.order.trade.CurrencyOrderTradeWindow'
};


TCG.use('Clifton.order.allocation.OrderAllocationListGrid');
TCG.use('Clifton.order.execution.OrderBlockListGrid');
TCG.use('Clifton.order.execution.OrderPlacementListGrid');
TCG.use('Clifton.order.execution.PlacementAllocationListGrid');
TCG.use('Clifton.order.trade.OrderTradeListGrid');
TCG.use('Clifton.order.group.OrderGroupListGrid');


// Grouping Properties
Clifton.order.allocation.OrderAllocationGroupingProperties = [
	{name: 'Security', label: 'Security', beanPropertyName: 'security.id', searchFieldName: 'securityId', emptyValueLabel: 'None', dataIndex: 'security.ticker'},
	{name: 'Settlement Currency', label: 'Settlement Currency', beanPropertyName: 'settlementCurrency.id', searchFieldName: 'settlementCurrencyId', emptyValueLabel: 'None', dataIndex: 'settlementCurrency.ticker'},
	{name: 'Order Destination', label: 'Order Destination', beanPropertyName: 'orderDestination.id', searchFieldName: 'orderDestinationId', emptyValueLabel: 'Not Specified', dataIndex: 'orderDestination.name'},
	{name: 'Executing Broker', label: 'Executing Broker', beanPropertyName: 'executingBrokerCompany.id', searchFieldName: 'executingBrokerCompanyId', emptyValueLabel: 'Not Specified', dataIndex: 'executingBrokerCompany.name'},
	{name: 'Buy / Sell', label: 'Buy / Sell', beanPropertyName: 'openCloseType.buy', searchFieldName: 'buy', emptyValueLabel: 'None', dataIndex: 'openCloseType.buy'},
	{name: 'Client Account', label: 'Client Account', beanPropertyName: 'clientAccount.id', searchFieldName: 'clientAccountId', emptyValueLabel: 'None', dataIndex: 'clientAccount.labelShort'},
	{name: 'Holding Account Issuer', label: 'Holding Account Issuer', beanPropertyName: 'holdingAccount.issuingCompany.id', searchFieldName: 'holdingAccountIssuingCompanyId', emptyValueLabel: 'None', dataIndex: 'holdingAccount.issuingCompany.name'},
	{name: 'Business Service', label: 'Business Service', beanPropertyName: 'clientAccount.service.id', searchFieldName: 'serviceId', emptyValueLabel: 'Not Specified', dataIndex: 'clientAccount.service.name'},
	{name: 'PM Team', label: 'PM Team', beanPropertyName: 'clientAccount.teamSecurityGroup.id', searchFieldName: 'teamSecurityGroupId', emptyValueLabel: 'Not Specified', dataIndex: 'clientAccount.teamSecurityGroup.name'},
	{name: 'Portfolio Manager', label: 'Portfolio Manager', beanPropertyName: 'clientAccount.portfolioManagerUser.id', searchFieldName: 'portfolioManagerUserId', emptyValueLabel: 'Not Specified', dataIndex: 'clientAccount.portfolioManagerUser.displayName'},
	{name: 'Order Creator', label: 'Order Creator', beanPropertyName: 'orderCreatorUser.id', searchFieldName: 'orderCreatorUserId', emptyValueLabel: 'Not Specified', dataIndex: 'orderCreatorUser.label'},
	{name: 'Trader', label: 'Trader', beanPropertyName: 'traderUser.id', searchFieldName: 'traderUserId', emptyValueLabel: 'Not Specified', dataIndex: 'traderUser.label'},
	{name: 'Order Type', label: 'Order Type', beanPropertyName: 'orderType.id', searchFieldName: 'orderTypeId', emptyValueLabel: 'None', dataIndex: 'orderType.name'},
	{name: 'Order Group Type', label: 'Order Group Type', beanPropertyName: 'orderGroup.groupType.id', searchFieldName: 'orderGroupTypeId', emptyValueLabel: 'None', dataIndex: 'orderGroup.groupType.name'},
	{name: 'Order Source', label: 'Order Source', beanPropertyName: 'orderSource.id', searchFieldName: 'orderSourceId', emptyValueLabel: 'None', dataIndex: 'orderSource.name'}
];

Clifton.order.allocation.OrderAllocationGroupingPropertiesArray = [];
Clifton.order.allocation.OrderAllocationGroupingProperties.forEach(value => Clifton.order.allocation.OrderAllocationGroupingPropertiesArray.push(value.name));


Clifton.order.allocation.OrderAllocationWorkflowToolbar = Ext.extend(Clifton.workflow.Toolbar, {
	tableName: 'OrderAllocation',
	finalState: 'Canceled',
	additionalSubmitParameters: {
		requestedMaxDepth: 6
	}
});
Ext.reg('order-allocation-workflow-toolbar', Clifton.order.allocation.OrderAllocationWorkflowToolbar);


Clifton.order.shared.account.OrderAccountWindowAdditionalTabs.push(
	{
		title: 'Order Allocations',
		xtype: 'order-allocation-list-grid',
		columnOverrides: [
			{dataIndex: 'clientAccount.labelShort', hidden: true},
			{dataIndex: 'holdingAccount.issuingCompany.name', hidden: true},
			{dataIndex: 'security.name', hidden: true},
			{dataIndex: 'orderDestination.name', hidden: true},
			{dataIndex: 'averageUnitPrice', hidden: true},
			{dataIndex: 'orderCreatorUser.label', hidden: true}
		],
		getLoadParams: function(firstLoad) {
			if (firstLoad) {
				// default to last 8 days of trades
				this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -30)});
			}
			return {clientOrHoldingAccountId: this.getWindow().getMainFormId()};
		}
	},
	{
		title: 'Trades',
		xtype: 'order-trade-list-grid',
		columnOverrides: [
			{dataIndex: 'clientAccount.label', hidden: true},
			{dataIndex: 'security.name', hidden: true}
		],
		getLoadParams: function(firstLoad) {
			if (firstLoad) {
				// default to last 8 days of trades
				this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -30)});
			}
			return {clientOrHoldingAccountId: this.getWindow().getMainFormId()};
		}
	}
);


Clifton.order.shared.security.OrderSecurityWindowAdditionalTabs.push(
	{
		title: 'Placements',
		items: [{

			xtype: 'order-placement-list-grid',
			instructions: 'A list of all placements in the system for this security. Placements are a specified quantity of a block order placed with a particular executing broker/destination.',

			columnOverrides: [
				{dataIndex: 'quantityUnfilled', hidden: true},
				{dataIndex: 'orderBlock.security.ticker', hidden: true},
				{dataIndex: 'orderBlock.security.name', hidden: true},
				{dataIndex: 'executionStatus.name', hidden: true},
				{dataIndex: 'allocationStatus.name', hidden: true}
			],
			getTopToolbarFilters: function(toolbar) {
				return [];
			},
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					// default to last 8 days of trades
					this.setFilterValue('orderBlock.tradeDate', {'after': new Date().add(Date.DAY, -30)});
				}
				return {securityId: this.getWindow().getMainFormId()};
			}
		}]
	});

Clifton.order.calculateSignedAmount = function(value, buy) {
	if (TCG.isNumber(value)) {
		if (TCG.isTrue(buy) || TCG.startsWith('BUY', buy)) {
			return value;
		}
		return -value;
	}
	return value;
};


Clifton.order.ExecutingBrokerListDisplayField = Ext.extend(Ext.form.Label, {
	// THE URL SHOULD CONTAIN the prefix of the id property name to include
	// I.E. orderManagementRuleExecutingBrokerCompanyListForOrderPlacement.json
	url: 'REPLACE_ME',
	idParamName: 'id', // Replace with actual param name if needed.  For example for above url, the id value is passed as orderPlacementId, OR overrie getLoadParams method
	html: 'Loading...',
	fieldLabel: 'Approved Broker(s)',
	xtype: 'label',
	qtip: 'The following executing broker(s) are approved to execute this order.',
	name: 'approvedExecutingBrokerList',

	listeners: {
		afterrender: function() {
			const field = this;
			const fp = TCG.getParentFormPanel(this);

			fp.on('afterload', function(fp) {
				field.setApprovedBrokers(fp);
			}, this);
		}
	},

	getLoadParams: function(formPanel) {
		const params = {};
		params[this.idParamName] = formPanel.getWindow().getMainFormId();
		return params;
	},

	setApprovedBrokers: function(formPanel) {
		const field = this;
		const params = Ext.apply({
			requestedPropertiesRoot: 'data',
			requestedProperties: 'id|name'
		}, this.getLoadParams(formPanel));

		TCG.data.getDataPromise(field.url, field, {params: params}).then(result => {
			let brokerNames = '';
			if (result && result.rows && result.rows.length > 0) {
				result.rows.forEach(company => (brokerNames += company.name + ', '));
				brokerNames = brokerNames.substring(0, brokerNames.length - 2);
			}
			else {
				brokerNames = 'No approved brokers found.';
			}
			field.update(brokerNames);
		});
	}
});
Ext.reg('order-executing-broker-list', Clifton.order.ExecutingBrokerListDisplayField);
