//TCG.use('Clifton.trade.group.TradeGroupNoteDragAndDropPanel');

Clifton.order.group.DefaultOrderGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Group',
	iconCls: 'shopping-cart',
	width: 1300,
	height: 630,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	saveTimeout: 200,

	items: [
		{
			xtype: 'tabpanel',
			items: [
				{
					title: 'Info',
					layout: 'border',
					items: [
						{
							xtype: 'formpanel',
							loadValidation: false,
							region: 'north',
							height: 125,
							url: 'orderGroup.json?populateAllocations=true&enableOpenSessionInView=true',

							listeners: {
								afterload: function(formPanel) {
									formPanel.hideFieldIfBlank('security.label');
									formPanel.hideFieldIfBlank('secondarySecurity.label');
									formPanel.hideFieldIfBlank('parent.id');
									formPanel.hideFieldIfBlank('workflowState.name');
									formPanel.hideFieldIfBlank('workflowStatus.name');
									formPanel.hideFieldIfBlank('groupFkFieldId');
								}
							},

							defaults: {anchor: '0'},
							items: [
								{
									xtype: 'panel',
									layout: 'column',
									items: [
										{
											columnWidth: .40,
											layout: 'form',
											defaults: {xtype: 'textfield'},
											items: [
												{fieldLabel: 'Group Type', name: 'groupType.name', xtype: 'linkfield', detailIdField: 'groupType.id', detailPageClass: 'Clifton.order.setup.GroupTypeWindow'},
												{fieldLabel: 'Group FKField ID', name: 'groupFkFieldId', xtype: 'displayfield', qtip: 'The group source FKFieldID - i.e. for Portfolio Runs, it is the Portfolio Run ID, for Sextant it is the Trade Session ID from which the group was submitted from'},
												{fieldLabel: 'Security', name: 'security.label', xtype: 'linkfield', detailIdField: 'security.id', detailPageClass: 'Clifton.order.shared.security.SecurityWindow'},
												{fieldLabel: 'Secondary Security', name: 'secondarySecurity.label', xtype: 'linkfield', detailIdField: 'secondarySecurity.id', detailPageClass: 'Clifton.order.shared.security.SecurityWindow'}
											]
										},
										{
											columnWidth: .30,
											layout: 'form',
											defaults: {xtype: 'textfield'},
											items: [
												{fieldLabel: 'Group ID', name: 'id', xtype: 'displayfield'},
												{fieldLabel: 'Parent Group', name: 'parent.id', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.order.group.OrderGroupWindow'},
												{fieldLabel: 'Created By', name: 'orderCreatorUser.label', xtype: 'displayfield', detailIdField: 'orderCreatorUser.id', qtip: 'Portfolio Manager or the person who was responsible for creation of this Order Allocation'}
											]
										},
										{
											columnWidth: .25,
											layout: 'form',
											defaults: {xtype: 'textfield', anchor: '-20'},
											items: [
												{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
												{fieldLabel: 'Workflow Status', name: 'workflowStatus.name', xtype: 'displayfield'},
												{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'displayfield', type: 'date'}
											]
										}]
								}
							]
						},


						{

							region: 'center',
							height: 400,
							xtype: 'order-management-allocation-workflow-list-grid',
							enableOrderAllocationUploadDragAndDrop: false,
							columnOverrides: [
								{dataIndex: 'orderCreatorUser.label', hidden: true},
								{dataIndex: 'tradeDate', hidden: true},
								{dataIndex: 'settlementDate', hidden: true}

							],
							transitionWorkflowStateList: [
								{stateName: 'Approved', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approve Selected Order Allocation(s) - Applies to Pending Order Allocations Only'},
								{stateName: 'Pending', iconCls: 'run', buttonText: 'Validate', buttonTooltip: 'Validate Selected Order Allocation(s) - Re-Runs Rule Violations.'},
								{stateName: 'Rejected', iconCls: 'undo', buttonText: 'Reject', buttonTooltip: 'Reject Selected Order Allocation(s).  Can make edits and and re-submit.'},
								{stateName: 'Canceled', iconCls: 'cancel', buttonText: 'Cancel', buttonTooltip: 'Cancel Selected Order Allocation(s)'}
							],

							getLoadParams: async function(firstLoad) {
								const lp = {readUncommittedRequested: true};
								lp.orderGroupId = this.getWindow().getMainFormId();
								return lp;
							},

							getTopToolbarFilters: function(toolbar) {
								return [];
							}
						}
					]
				},
				{
					title: 'Notes',
					items: [{
						xtype: 'document-system-note-grid',
						tableName: 'OrderGroup',
						showAttachmentInfo: true,
						showCreateDate: true,
						showInternalInfo: false,
						showPrivateInfo: false,
						showGlobalNoteMenu: true,
						defaultActiveFilter: false,
						showDisplayFilter: false
					}]
				}
			]
		}
	]


});
