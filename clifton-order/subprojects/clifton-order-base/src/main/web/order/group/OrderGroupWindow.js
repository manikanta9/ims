// not the actual window but a window selector based on 'orderGroupType'

Clifton.order.group.OrderGroupWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderGroup.json?populateAllocations=true&enableOpenSessionInView=true&requestedMaxDepth=5',

	// Copied from Default Order Group Window
	requestedProperties: 'id|workflowState.name|clientAccount.label|holdingAccount.label|buy|openCloseType.label|quantityIntended|security.ticker|security.name|exchangeRate|averageUnitPrice|commissionPerUnit|feeAmount|accountingNotional|clientAccount.teamSecurityGroup.name|portfolioManagerUser.displayName|traderUser.label|tradeDate|settlementDate|blockTrade|executingBrokerCompany.id|executingBrokerCompany.label|orderDestination.id|orderDestination.name|orderType.name',
	requestedPropertiesRoot: 'data.orderAllocationList',


	getClassName: function(config, entity) {
		let screenClass;
		if (entity && entity.groupType) {
			screenClass = entity.groupType.detailScreenClass;
		}
		if (!screenClass) {
			screenClass = 'Clifton.order.group.DefaultOrderGroupWindow';
		}
		return screenClass;
	},


	// OVERRIDE Still need the server call for Order Group Type = Roll because it does some special processing for retrieval
	doOpenEntityWindow: function(config, entity, className) {
		if (className !== false) {
			if (typeof className === 'object') {
				if (className.getEntity) {
					entity = className.getEntity(config, entity);
				}
				if (className.className) {
					className = className.className;
				}
			}
			if (entity) {
				// the entity was already retrieved: pass it to the window and instruct not to get it again
				// don't do this for cases where there is a detail screen class override (i.e. Trade Rolls or EFP - need to get the entity from service)
				if (TCG.isBlank(entity.groupType.detailScreenClass)) {
					config = Ext.apply(config, {
						defaultDataIsReal: true,
						defaultData: entity
					});
				}
			}
			TCG.createComponent(className, config);
		}
	}
});
