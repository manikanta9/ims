Clifton.order.group.OrderGroupListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderGroupListFind',
	xtype: 'gridpanel',
	instructions: 'Order Groups are collections of order allocations executed for a purpose, i.e. Multi-Client, PIOS, Rolls, Tails, or entered from a single source (i.e. same upload file).',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Group Type', width: 18, dataIndex: 'groupType.name', filter: {searchFieldName: 'groupTypeId', type: 'combo', url: 'orderGroupTypleListFind.json'}},
		{header: 'Group FK Field ID', width: 12, dataIndex: 'groupFkFieldId', tooltip: 'The group source FKFieldID - i.e. for Portfolio Runs, it is the Portfolio Run ID, for Sextant it is the Trade Session ID from which the group was submitted from'},
		{header: 'Group Note', width: 80, dataIndex: 'note'},
		{header: 'Security', width: 20, dataIndex: 'security.ticker', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'ticker', url: 'orderSecurityListFind.json'}},
		{header: '2nd Security', width: 20, dataIndex: 'secondarySecurity.ticker', filter: {type: 'combo', searchFieldName: 'secondarySecurityId', displayField: 'ticker', url: 'orderSecurityListFind.json'}},
		{header: 'Created By', width: 13, dataIndex: 'orderCreatorUser.label', filter: {type: 'combo', searchFieldName: 'orderCreatorUserId', displayField: 'label', url: 'securityUserListFind.json'}},
		{header: 'Traded On', width: 10, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
		{header: 'Roll', width: 8, dataIndex: 'groupType.roll', type: 'boolean', filter: {searchFieldName: 'roll'}},
		{header: 'Import', width: 8, dataIndex: 'groupType.orderImport', type: 'boolean', filter: {searchFieldName: 'orderImport'}}
	],
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to last 8 days of trades
			this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -8)});
		}
	},
	editor: {
		detailPageClass: 'Clifton.order.group.OrderGroupWindow',
		drillDownOnly: true
	}

});
Ext.reg('order-group-list-grid', Clifton.order.group.OrderGroupListGrid);
