Clifton.order.OrderHistoryWindow = Ext.extend(TCG.app.Window, {
	id: 'orderHistoryWindow',
	title: 'Order History',
	iconCls: 'shopping-cart',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Order Allocations',
				items: [{
					xtype: 'order-allocation-list-grid'
				}]
			},

			{
				title: 'Order Groups',
				items: [{
					xtype: 'order-group-list-grid'
				}]
			},


			{
				title: 'Block Orders',
				items: [{
					xtype: 'order-block-list-grid'
				}]
			},


			{
				title: 'Placements',
				items: [{
					xtype: 'order-placement-list-grid'
				}]
			},


			{
				title: 'Placement Allocations',
				items: [{
					xtype: 'order-placement-allocation-list-grid'

				}]
			},


			{
				title: 'Trades',
				items: [{
					xtype: 'order-trade-list-grid'
				}]
			}
		]
	}]
});

