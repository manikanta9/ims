Clifton.order.trade.BaseOrderTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 1000,
	height: 500,
	enableRefreshWindow: true,

	infoTab: {}, // Overridden in sub classes


	init: function() {
		// replace the first tab with the override
		const tabs = this.items[0].items;
		tabs[0] = this.infoTab;
		Clifton.order.trade.BaseOrderTradeWindow.superclass.init.apply(this, arguments);
	},


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info'
			},


			{
				title: 'Order Batches',
				items: [{
					xtype: 'order-batch-item-list-grid',
					tableName: 'OrderTrade',
					instructions: 'A list of all order batches in the system this trade is included in. Order Batches are a batch of Trades that are "batched" and then sent to a destination.  These can be used for Post Trade Files sent to Accounting system or some other external party.'
				}]
			},


			{
				title: 'Order Life Cycle',
				items: [{
					xtype: 'system-lifecycle-grid',
					tableName: 'OrderAllocation',
					getEntityId: function() {
						return TCG.getValue('orderAllocationIdentifier', this.getWindow().getMainForm().formValues);
					}
				}]
			}
		]
	}]
});
