// not the actual window but a window selector based on 'orderType'

Clifton.order.trade.OrderTradeWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderTrade.json?requestedMaxDepth=6&UI_SOURCE=OrderTradeWindowSelector', // deep enough to get hierarchy attributes

	getClassName: function(config, entity) {
		let orderType = this.orderType;
		if (entity && entity.orderType) {
			orderType = entity.orderType.name;
		}
		let clazz = Clifton.order.trade.OrderTradeWindowOverrides[orderType];
		if (!clazz) {
			clazz = 'Clifton.order.trade.DefaultOrderTradeWindow';
		}
		return clazz;
	}
});
