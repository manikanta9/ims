TCG.use('Clifton.order.trade.BaseOrderTradeWindow');

Clifton.order.trade.CurrencyOrderTradeWindow = Ext.extend(Clifton.order.trade.BaseOrderTradeWindow, {
	titlePrefix: 'Currency Trade',
	iconCls: 'currency',

	infoTab: {
		title: 'Info',
		items: [
			{
				xtype: 'formpanel',
				url: 'orderTrade.json',
				labelWidth: 110,
				loadDefaultDataAfterRender: true,

				listeners: {
					afterload: function(fp) {
						if (TCG.isEquals(fp.getFormValue('clientAccount.id'), fp.getFormValue('holdingAccount.id'))) {
							fp.hideField('holdingAccount.label');
							fp.hideField('holdingAccount.baseCurrencyCode');
							fp.setFieldLabel('clientAccount.label', 'Account:');
						}
					}
				},
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {layout: 'form', defaults: {anchor: '-20'}},
						items: [
							{
								columnWidth: .71,
								items: [
									{fieldLabel: 'Client Account', name: 'clientAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'clientAccount.id'},
									{fieldLabel: 'Holding Account', name: 'holdingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'holdingAccount.id'}
								]
							},
							{
								columnWidth: .29,
								labelWidth: 60,
								items: [
									{fieldLabel: 'Base CCY', name: 'clientAccount.baseCurrencyCode', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow', useNameFieldValueForDetailIdFieldValue: true},
									{fieldLabel: 'Base CCY', name: 'holdingAccount.baseCurrencyCode', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow', useNameFieldValueForDetailIdFieldValue: true}
								]
							}
						]
					},
					{xtype: 'label', html: '<hr/>'},
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
						items: [
							{
								columnWidth: .31,
								labelWidth: 110,
								items: [
									{
										fieldLabel: 'Side', name: 'buy', xtype: 'displayfield', width: 140,
										setRawValue: function(v) {
											if (this.el && this.el.addClass) {
												this.el.addClass(TCG.isTrue(v) ? 'buy' : 'sell');
											}
											TCG.form.DisplayField.superclass.setRawValue.call(this, TCG.isTrue(v) ? 'BUY' : 'SELL');
										}
									},
									{fieldLabel: 'Given CCY', name: 'security.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'security.id'},
									{fieldLabel: 'Given Amount', name: 'accountingNotional', xtype: 'currencyfield4', readOnly: true},
									{fieldLabel: 'Settle CCY', name: 'settlementCurrency.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'settlementCurrency.id'},
									{fieldLabel: 'Settle Amount', name: 'settlementAmount', xtype: 'currencyfield4', readOnly: true}
								]
							},
							{
								columnWidth: .40,
								labelWidth: 100,
								items: [
									{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'},
									{fieldLabel: 'FX Rate', name: 'exchangeRateToSettlementCurrency', xtype: 'floatfield', readOnly: true, tooltip: 'Currency exchange rates should always be the exchange rate between the dominant currency to the other, which is the industry standard convention. For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency'}
								]
							},
							{
								columnWidth: .29,
								labelWidth: 110,
								items: [
									{name: 'orderType.id', xtype: 'hidden'},
									{fieldLabel: 'Order Allocation ID', name: 'orderAllocationIdentifier', xtype: 'linkfield', detailPageClass: 'Clifton.order.allocation.OrderAllocationWindow', detailIdField: 'orderAllocationIdentifier'},
									{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
									{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true}
								]
							}
						]
					}
				]
			}
		]
	}
});
