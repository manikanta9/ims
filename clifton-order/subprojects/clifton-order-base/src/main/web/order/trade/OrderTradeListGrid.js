Clifton.order.trade.OrderTradeListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderTradeListFind',
	xtype: 'gridpanel',
	wikiPage: 'IT/Order+Life+Cycle',
	instructions: 'A list of all trades in the system.  Trades are generated as a result of an executed / filled / allocated order and placement. Each Order Allocation may result in multiple trades if it is filled via different executing brokers and / or whether or not average pricing can be used.',
	viewNames: ['Default View', 'Currency View'],
	columns: [
		{header: 'ID', width: 24, dataIndex: 'id', hidden: true},
		{header: 'Order Type', width: 22, dataIndex: 'orderType.name', filter: {type: 'combo', searchFieldName: 'orderTypeId', url: 'orderTypeListFind.json'}, viewNames: ['Default View']},
		{header: 'Client', width: 80, dataIndex: 'clientAccount.clientCompany.name', hidden: true, filter: {searchFieldName: 'clientCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Client'}},
		{header: 'Client Account', width: 80, dataIndex: 'clientAccount.label', filter: {type: 'combo', searchFieldName: 'clientAccountId', displayField: 'label', url: 'orderAccountListFind.json?clientAccount=true'}, allViews: true},
		{header: 'Business Service', width: 80, dataIndex: 'clientAccount.service.name', hidden: true, filter: {searchFieldName: 'serviceOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Account Service', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{header: 'Holding Account', width: 80, dataIndex: 'holdingAccount.label', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'orderAccountListFind.json?holdingAccount=true'}},
		{header: 'Holding Account #', width: 30, dataIndex: 'holdingAccount.accountNumber', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'orderAccountListFind.json?holdingAccount=true'}},
		{header: 'Holding Account Name', width: 50, dataIndex: 'holdingAccount.labelShort', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountId', displayField: 'label', url: 'orderAccountListFind.json?ourAccount=false'}},
		{header: 'Holding Account Issuer', width: 60, dataIndex: 'holdingAccount.issuingCompany.name', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'orderCompanyListFind.json?holdingAccountIssuer=true'}},
		{header: 'Executing Broker', width: 60, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker'}, allViews: true},
		{
			header: 'Side', width: 15, dataIndex: 'openCloseType.buy', type: 'boolean', allViews: true,
			exportColumnValueConverter: 'orderBuyColumnReversableConverter',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'openCloseTypeId', url: 'orderOpenCloseTypeListFind.json', loadAll: true},
			renderer: function(v, metaData) {
				metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Quantity', width: 20, dataIndex: 'quantity', type: 'float', useNull: true, viewNames: ['Default View']},
		{
			header: 'Security', width: 40, dataIndex: 'security.ticker', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'orderSecurityListFind.json'},
			viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given CCY', widthMultiplier: 0.75},
				{name: 'Default View', label: 'Security', widthMultiplier: 1}
			]
		},
		{header: 'Security Name', width: 45, dataIndex: 'security.name', filter: {searchFieldName: 'securityName'}, viewNames: ['Default View']},
		{header: 'Underlying Security', width: 40, dataIndex: 'security.underlyingSecurity.ticker', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'orderSecurityListFind.json'}, hidden: true},
		{header: 'Security Hierarchy', width: 80, dataIndex: 'security.securityHierarchy.nameExpanded', hidden: true, filter: {searchFieldName: 'securityHierarchyOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Security Hierarchy', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},
		{header: 'CCY Denomination (Security)', width: 30, dataIndex: 'security.currencyCode', hidden: true, filter: {searchFieldName: 'securityCurrencyCode', type: 'combo', displayField: 'label', valueField: 'ticker', url: 'orderSecurityListFind.json?securityType=Currency'}},
		{header: 'Settle CCY', width: 30, dataIndex: 'settlementCurrency.ticker', hidden: true, filter: {searchFieldName: 'settlementCurrencyId', type: 'combo', displayField: 'label', url: 'orderSecurityListFind.json?securityType=Currency'}, viewNames: ['Currency View']},
		{header: 'FX Rate', width: 24, dataIndex: 'exchangeRateToSettlementCurrency', type: 'float', useNull: true, hidden: true, viewNames: ['Currency View']},
		{header: 'Price', width: 25, dataIndex: 'price', type: 'float', useNull: true, viewNames: ['Default View']},
		{
			header: 'Notional', width: 30, dataIndex: 'accountingNotional', type: 'currency', useNull: true, tooltip: 'Accounting Notional', viewNames: ['Default View', 'Currency View'],
			viewNameHeaders: [
				{name: 'Currency View', label: 'Given Amount'},
				{name: 'Default View', label: 'Notional'}
			]
		},
		{header: 'Settle Amount', width: 30, dataIndex: 'settlementAmount', type: 'currency', useNull: true, viewNames: ['Currency View'], tooltip: 'Accounting Notional in the Settlement CCY'},
		{header: 'Accrual 1', width: 20, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual 2', width: 20, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual', width: 20, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Commission Per Unit', width: 20, dataIndex: 'commissionPerUnit', type: 'float', hidden: true, useNull: true},
		{header: 'Commission Amount', width: 20, dataIndex: 'commissionAmount', type: 'currency', hidden: true, useNull: true, tooltip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.'},
		{header: 'Fee', width: 20, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'PM Team', width: 24, dataIndex: 'clientAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', xtype: 'system-security-group-combo', groupTagName: 'PM Team'}, viewNames: ['Default View']},
		{header: 'Traded On', width: 23, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc', filter: {searchFieldName: 'tradeDate'}, allViews: true},
		{header: 'Settled On', width: 23, dataIndex: 'settlementDate', allViews: true}
	],

	switchToViewBeforeReload: function(viewName) {
		// If no toolbar field than inside a detail window, i.e. block order and not necessary to switch this filter
		const otField = TCG.getChildByName(this.getTopToolbar(), 'orderType');
		if (otField) {
			if (viewName === 'Currency View') {
				TCG.data.getDataPromiseUsingCaching('orderTypeByName.json?requestedPropertiesRoot=data&requestedProperties=id|name&name=Currency', this, 'order.type.Currency')
					.then(function(data) {
						const record = {text: data.name, value: data.id};
						otField.setValue(record);
						otField.fireEvent('select', otField, record);
					});
				return true; // cancel reload: select event will reload
			}
			else {
				this.clearFilter('orderType.name', true);
				otField.clearValue();
			}
		}
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'PM Team', name: 'teamSecurityGroupId', width: 120, minListWidth: 120, xtype: 'toolbar-system-security-group-combo', groupTagName: 'PM Team', linkedFilter: 'clientAccount.teamSecurityGroup.name'},
			{fieldLabel: 'Order Type', xtype: 'toolbar-combo', width: 150, name: 'orderType', url: 'orderTypeListFind.json', linkedFilter: 'orderType.name', linkedField: 'orderType.name'}
		];
	},
	getDefaultDaysBack: function() {
		return 8;
	},

	getLoadParams: async function(firstLoad) {
		if (firstLoad) {
			// default to last 8 days of trades
			this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -this.getDefaultDaysBack())});

			const team = await Clifton.system.security.getUserSecurityGroupForTag(this, 'Security Group Tags', 'PM Team');
			if (TCG.isNotNull(team)) {
				TCG.getChildByName(this.getTopToolbar(), 'teamSecurityGroupId').setValue({value: team.id, text: team.name});
				this.setFilterValue('clientAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
			}
		}
		return {readUncommittedRequested: true};
	},
	editor: {
		detailPageClass: 'Clifton.order.trade.OrderTradeWindow',
		drillDownOnly: true
	}


});
Ext.reg('order-trade-list-grid', Clifton.order.trade.OrderTradeListGrid);
