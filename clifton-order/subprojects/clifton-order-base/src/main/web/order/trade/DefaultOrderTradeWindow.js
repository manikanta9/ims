TCG.use('Clifton.order.trade.BaseOrderTradeWindow');

Clifton.order.trade.DefaultOrderTradeWindow = Ext.extend(Clifton.order.trade.BaseOrderTradeWindow, {
	titlePrefix: 'Trade',
	iconCls: 'shopping-cart',

	infoTab: {
		title: 'Info',
		items: [
			{
				xtype: 'formpanel',
				url: 'orderTrade.json',
				labelWidth: 110,
				loadDefaultDataAfterRender: true,

				listeners: {
					afterload: function(fp) {
						if (TCG.isEquals(fp.getFormValue('clientAccount.id'), fp.getFormValue('holdingAccount.id'))) {
							fp.hideField('holdingAccount.label');
							fp.hideField('holdingAccount.baseCurrencyCode');
							fp.setFieldLabel('clientAccount.label', 'Account:');
						}

						fp.hideFieldIfBlank('openingDate');
						fp.hideFieldIfBlank('openingPrice');
						fp.hideFieldIfBlank('accrualAmount');
						fp.hideFieldIfBlank('commissionAmount');
						fp.hideFieldIfBlank('feeAmount');

						const f = this.getForm();
						const v = f.findField('accountingNotional').getNumericValue();

						let label = 'Notional';
						if (['Stocks', 'Funds'].includes(this.getFormValue('orderType.securityType.text'))) {
							const buy = (fp.getFormValue('buy') === true);
							let value = buy ? -v : v;
							value = value + (f.findField('feeAmount').getNumericValue() || 0) + (f.findField('commissionAmount').getNumericValue() || 0);

							label = (value < 0) ? 'Payment' : 'Proceeds';
							f.findField('accountingNotional').setFieldLabel(label + ':');
							const nn = f.findField('netNotional');
							nn.setFieldLabel('Net ' + label + ':');
							if (value < 0) {
								value = -value;
							}
							fp.setFormValue('netNotional', value, true);
							nn.show();
						}

						const type = this.getFormValue('orderType.securityType.text');
						if (type === 'Options' || type === 'Swaptions') {
							f.findField('accountingNotional').setFieldLabel('Premium:');
						}
					}
				},
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {layout: 'form', defaults: {anchor: '-20'}},
						items: [
							{
								columnWidth: .71,
								items: [
									{fieldLabel: 'Client Account', name: 'clientAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'clientAccount.id'},
									{fieldLabel: 'Holding Account', name: 'holdingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.account.AccountWindow', detailIdField: 'holdingAccount.id'}
								]
							},
							{
								columnWidth: .29,
								labelWidth: 60,
								items: [
									{fieldLabel: 'Base CCY', name: 'clientAccount.baseCurrencyCode', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow', useNameFieldValueForDetailIdFieldValue: true},
									{fieldLabel: 'Base CCY', name: 'holdingAccount.baseCurrencyCode', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow', useNameFieldValueForDetailIdFieldValue: true}
								]
							}
						]
					},
					{xtype: 'label', html: '<hr/>'},
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
						items: [
							{
								columnWidth: .31,
								labelWidth: 110,
								items: [
									{fieldLabel: 'Order Type', name: 'orderType.name', xtype: 'linkfield', detailIdField: 'orderType.id', detailPageClass: 'Clifton.order.setup.OrderTypeWindow'},
									{
										fieldLabel: 'Side', name: 'openCloseType.label', xtype: 'displayfield', width: 125,
										setRawValue: function(v) {
											this.el.addClass(v.includes('BUY') ? 'buy' : 'sell');
											TCG.form.DisplayField.superclass.setRawValue.call(this, v);
										}
									}
								]
							},
							{
								columnWidth: .40,
								labelWidth: 120,
								items: [
									{fieldLabel: 'Security', name: 'security.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'security.id'},
									{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'}

								]
							},
							{
								columnWidth: .29,
								labelWidth: 110,
								items: [
									{fieldLabel: 'Order Allocation ID', name: 'orderAllocationIdentifier', xtype: 'linkfield', detailPageClass: 'Clifton.order.allocation.OrderAllocationWindow', detailIdField: 'orderAllocationIdentifier'},
									{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
									{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true},
									{fieldLabel: 'Opening Date', name: 'openingDate', xtype: 'datefield', readOnly: true, tooltip: 'Used to close a specific lot opened on the opening date and the opening price..'},
									{fieldLabel: 'Opening Price', name: 'openingPrice', xtype: 'floatfield', readOnly: true, tooltip: 'Used to close a specific lot opened on the opening date and the opening price.'}

								]
							}
						]
					},
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
						items: [
							{
								columnWidth: .31,
								labelWidth: 110,
								items: [
									{fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield', readOnly: true},
									{fieldLabel: 'Price', name: 'price', xtype: 'floatfield', readOnly: true}
								]
							},
							{
								columnWidth: .40,
								labelWidth: 120,
								items: [

									{fieldLabel: 'Accruals', name: 'accrualAmount', xtype: 'currencyfield4', readOnly: true},
									{fieldLabel: 'Commission', name: 'commissionAmount', xtype: 'currencyfield4', readOnly: true, qtip: 'Total Commission Amount for this trade. Negative amount means that the client pays commission.'},
									{fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield4', readOnly: true, qtip: 'Total Fees for this trade.'},
									{fieldLabel: 'Accounting Notional', name: 'accountingNotional', xtype: 'floatfield', readOnly: true},
									{fieldLabel: 'Net Notional', name: 'netNotional', xtype: 'floatfield', readOnly: true}
								]
							},
							{
								columnWidth: .29,
								labelWidth: 110,
								items: [
									{fieldLabel: 'Settle CCY', name: 'settlementCurrency.label', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow', detailIdField: 'settlementCurrency.id'},
									{fieldLabel: 'FX Rate (Settle)', name: 'exchangeRateToSettlementCurrency', xtype: 'floatfield', readOnly: true, tooltip: 'Currency exchange rates should always be the exchange rate between the dominant currency to the other, which is the industry standard convention. For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency'},
									{fieldLabel: 'Settlement Amount', name: 'settlementAmount', xtype: 'floatfield', readOnly: true}
								]
							}
						]
					}
				]
			}
		]
	}
});

