import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant

val apiVariant = registerVariant("api", upstreamForMain = true)

dependencies {

	////////////////////////////////////////////////////////////////////////////
	////////            Main Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	// Nothing yet

	////////////////////////////////////////////////////////////////////////////
	////////            API Dependencies                                ////////
	////////////////////////////////////////////////////////////////////////////

	apiVariant.api(project(":clifton-order:clifton-order-api")) { usingVariant("java-server") }
	apiVariant.api(project(":clifton-order:clifton-order-shared"))
	apiVariant.api(project(":clifton-order:clifton-order-setup"))
	apiVariant.api(project(":clifton-rule"))

	apiVariant.api(project(":clifton-security"))
	apiVariant.api(project(":clifton-workflow"))
	api(project(":clifton-document"))

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-system:clifton-system-field-mapping")))
	testFixturesApi(testFixtures(project(":clifton-order:clifton-order-shared")))
	testFixturesApi(testFixtures(project(":clifton-order:clifton-order-setup")))


}
