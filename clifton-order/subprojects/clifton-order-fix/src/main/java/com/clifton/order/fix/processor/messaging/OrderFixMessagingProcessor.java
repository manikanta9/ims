package com.clifton.order.fix.processor.messaging;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.order.fix.processor.OrderFixProcessor;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import lombok.Getter;
import lombok.Setter;


/**
 * The <code>OrderFixMessagingProcessor</code> processes incoming asynchronous JMS messages from the the FIX server.
 * <p>
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderFixMessagingProcessor implements AsynchronousProcessor<AsynchronousMessage> {


	private ContextHandler contextHandler;

	private OrderFixProcessor orderFixProcessor;

	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////


	/**
	 * A default user need to run the process so there is a user for database updates and inserts.
	 */
	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(AsynchronousMessage msg, @SuppressWarnings("unused") AsynchronousMessageHandler handler) {
		if (msg != null) {
			// set the user for database
			setRunAsUser();

			if (msg instanceof FixEntityMessagingMessage) {
				FixEntityMessagingMessage message = (FixEntityMessagingMessage) msg;
				try {
					FixEntity entity = message.getFixEntity();
					Long sourceSystemFkFieldId = message.getSourceSystemFkFieldId();
					if (sourceSystemFkFieldId == null) {
						// Note: We don't currently support Drop Copies
						throw new RuntimeException("Unable to process FIX Message because unable to determine the placement that it applies to: " + BeanUtils.describe(entity));
					}
					getOrderFixProcessor().process(sourceSystemFkFieldId, entity);
				}
				catch (Throwable e) {
					throw new RuntimeException(e);
				}
			}
		}
	}


	private void setRunAsUser() {
		if ((getContextHandler() != null) && getContextHandler().getBean(Context.USER_BEAN_NAME) == null) {
			SecurityUser result = getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
			getContextHandler().setBean(Context.USER_BEAN_NAME, result);
		}
	}
}
