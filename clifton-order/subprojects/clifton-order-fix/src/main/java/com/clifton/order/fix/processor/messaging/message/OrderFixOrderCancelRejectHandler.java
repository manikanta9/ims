package com.clifton.order.fix.processor.messaging.message;

import com.clifton.fix.order.FixOrderCancelReject;
import com.clifton.order.execution.event.OrderPlacementExecutionUpdateEvent;
import org.springframework.stereotype.Component;


/**
 * When an Cancel Request is rejected, this handler will raise an event to update OrderPlacement status
 *
 * @author manderson
 */
@Component
public class OrderFixOrderCancelRejectHandler extends AbstractOrderFixMessageHandler<FixOrderCancelReject> {


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	@Override
	public Class<FixOrderCancelReject> getMessageClass() {
		return FixOrderCancelReject.class;
	}


	@Override
	public void handleMessage(long orderPlacementId, FixOrderCancelReject reject) {
		OrderPlacementExecutionUpdateEvent event = new OrderPlacementExecutionUpdateEvent(orderPlacementId);
		event.setNewExecutionStatusName(getExecutionStatusNameForOrderStatus(reject.getStatus()));
		event.setRejectionReason(reject.getCancelRejectReason().toString());

		// Raise the Event to Update the Placement
		getEventHandler().raiseEvent(event);
	}
}
