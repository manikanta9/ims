package com.clifton.order.fix.configuration.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.fix.configuration.OrderFixDestinationConfigurationBean;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.util.List;


/**
 * The <code>OrderFixDestinationConfigurationRollupBean</code> allows grouping individual configuration beans into one for multiple changes
 *
 * @author manderson
 */
public class RollupOrderFixDestinationConfigurationBean implements OrderFixDestinationConfigurationBean, ValidationAware {


	private SystemBeanService systemBeanService;

	/**
	 * The list of individual configuration beans to apply
	 */
	private List<SystemBean> childConfigurationBeanList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(CollectionUtils.getSize(getChildConfigurationBeanList()) > 1, "You must select at least 2 configuration beans for this roll up configuration bean.");
		for (SystemBean childBean : getChildConfigurationBeanList()) {
			if (childBean.getType().getClassName().equalsIgnoreCase(this.getClass().getName())) {
				throw new ValidationException(childBean.getLabelShort() + " is invalid as a child because it is also a rollup.  Please add individual child calculators.");
			}
		}
	}


	@Override
	public <T> void applyConfiguration(OrderPlacement orderPlacement, T fixEntity) {
		validate(); // Confirm setup is correct

		// Apply configuration for each individually
		for (SystemBean bean : CollectionUtils.getIterable(getChildConfigurationBeanList())) {
			OrderFixDestinationConfigurationBean childConfigurer = (OrderFixDestinationConfigurationBean) getSystemBeanService().getBeanInstance(bean);
			childConfigurer.applyConfiguration(orderPlacement, fixEntity);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public List<SystemBean> getChildConfigurationBeanList() {
		return this.childConfigurationBeanList;
	}


	public void setChildConfigurationBeanList(List<SystemBean> childConfigurationBeanList) {
		this.childConfigurationBeanList = childConfigurationBeanList;
	}
}
