package com.clifton.order.fix.processor.messaging.message;

import com.clifton.fix.order.FixOrder;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderFixOrderHandler</code> is used to handle FixOrder messages received from FIX.
 * This could be drop copy orders (not supported yet).
 * FIX also, when submitted New Order singles to FIX, generates a FixIdentifier, applies it to the New Order and returns it. This could use that to save the updated identifier, however we are not using that in OMS so just ignore
 *
 * @author manderson
 */
@Component
public class OrderFixOrderHandler extends AbstractOrderFixMessageHandler<FixOrder> {


	@Override
	public Class<FixOrder> getMessageClass() {
		return FixOrder.class;
	}


	@Override
	public void handleMessage(long orderPlacementId, FixOrder fixOrder) {
		// DO NOTHING
		// This is used to return the FixIdentifier Unique String associated with the Fix Order.  Since we do not save these in OMS we don't need to do anything with them
		// In the future, we can update this to handle Drop Copies
	}
}
