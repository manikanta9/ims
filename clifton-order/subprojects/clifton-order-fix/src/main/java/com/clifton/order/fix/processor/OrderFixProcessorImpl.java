package com.clifton.order.fix.processor;


import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.fix.processor.messaging.message.OrderFixMessageHandler;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The <code>OrderFixProcessorImpl</code> processes incoming FIX objects, updates the TradeOrder object and then passes the resulting
 * TradeOrderProcessorParameters object to the TradeOrderProcessor which will execute the next action(s) on the Trade(s).
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderFixProcessorImpl implements OrderFixProcessor, InitializingBean, ApplicationContextAware {

	private OrderExecutionService orderExecutionService;


	private ApplicationContext applicationContext;

	/**
	 * The map of message handlers by message class name.
	 */
	private Map<String, OrderFixMessageHandler<?>> orderFixMessageHandlerMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		@SuppressWarnings("unchecked")
		Map<String, OrderFixMessageHandler<?>> fixMessageHandlerByName = (Map<String, OrderFixMessageHandler<?>>) (Map<?, ?>) getApplicationContext().getBeansOfType(OrderFixMessageHandler.class);
		Map<String, OrderFixMessageHandler<?>> fixMessageHandlerByMessageClassName = fixMessageHandlerByName.values().stream().collect(Collectors.toMap(k -> k.getMessageClass().getName(), Function.identity()));
		setOrderFixMessageHandlerMap(fixMessageHandlerByMessageClassName);
	}


	@Override
	public <T extends FixEntity> void process(Long orderPlacementId, T entity) {
		doProcess(orderPlacementId, entity, true);
	}


	@Override
	public <T extends FixEntity> void processDoNotSuppressErrors(Long orderPlacementId, T entity) {
		doProcess(orderPlacementId, entity, false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends FixEntity> void doProcess(Long orderPlacementId, T entity, boolean suppressErrors) {
		OrderFixMessageHandler<T> handler = getMessageHandler(entity);
		ValidationUtils.assertNotNull(handler, "Unable to find FixEntity handler for " + entity.getClass().getName());
		handler.handleMessage(orderPlacementId, entity);
	}


	@SuppressWarnings("unchecked")
	private <T extends FixEntity> OrderFixMessageHandler<T> getMessageHandler(T entity) {
		String className = entity.getClass().getName();
		if (getOrderFixMessageHandlerMap().containsKey(className)) {
			return (OrderFixMessageHandler<T>) getOrderFixMessageHandlerMap().get(className);
		}
		return null;
	}
}
