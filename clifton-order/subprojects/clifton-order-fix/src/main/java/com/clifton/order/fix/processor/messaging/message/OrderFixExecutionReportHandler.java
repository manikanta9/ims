package com.clifton.order.fix.processor.messaging.message;

import com.clifton.core.util.StringUtils;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.order.execution.event.OrderPlacementExecutionUpdateEvent;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


/**
 * When an ExecutionReport is received, this handler will raise an event to update OrderPlacement status, quantity filled, executing broker, and average price
 * from the execution report.
 *
 * @author manderson
 */
@Component
public class OrderFixExecutionReportHandler extends AbstractOrderFixMessageHandler<FixExecutionReport> {

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	@Override
	public Class<FixExecutionReport> getMessageClass() {
		return FixExecutionReport.class;
	}


	@Override
	public void handleMessage(long orderPlacementId, FixExecutionReport report) {
		OrderPlacementExecutionUpdateEvent event = new OrderPlacementExecutionUpdateEvent(orderPlacementId);
		event.setNewExecutionStatusName(getExecutionStatusNameForOrderStatus(report.getOrderStatus()));

		if (OrderStatuses.REJECTED == report.getOrderStatus()) {
			event.setRejectionReason(report.getText());
		}

		// Used for currencies so we know how the price/exchange rate was quoted
		event.setTicker(report.getSymbol());

		// update the order with the average price and the quantity
		event.setQuantityFilled(report.getCumulativeQuantity());
		BigDecimal price = report.getPriceType() != null ? report.getPriceType().calculateExecutionPrice(report) : report.getAveragePrice();
		event.setAverageUnitPrice(price);

		// Executing Broker
		if (report.getPartyList() != null) {
			FixParty broker = report.getPartyList().stream().filter((FixParty party) -> party.getPartyRole() == PartyRoles.EXECUTING_FIRM).findFirst().orElse(null);
			if (broker != null) {
				String brokerCode = broker.getPartyId();
				// For FIXSIM - returning original list and a random one as second option - so assuming actual sessions will return just the one
				if (StringUtils.contains(brokerCode, ";")) {
					brokerCode = StringUtils.substring(brokerCode, 0, brokerCode.indexOf(";"));
				}
				event.setExecutingBrokerCode(brokerCode);
			}
		}
		// Raise the Event to Update the Placement
		getEventHandler().raiseEvent(event);
	}
}
