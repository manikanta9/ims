package com.clifton.order.fix.converter;

import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderPlacement;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderFixConverterHandler {


	/**
	 * Generates the {@link FixOrder} object to represent the given order placement, allocation list and executing broker company list for a new order
	 */
	public FixOrder generateNewFixOrder(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, List<String> executingBrokerCompanyCodeList);


	/**
	 * Generates the {@link FixOrderCancelRequest} object to represent a request to cancel an order placement
	 */
	public FixOrderCancelRequest generateCancelRequestFixOrder(OrderPlacement orderPlacement);
}
