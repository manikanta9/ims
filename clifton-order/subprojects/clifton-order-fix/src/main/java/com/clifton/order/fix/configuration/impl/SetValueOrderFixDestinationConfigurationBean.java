package com.clifton.order.fix.configuration.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.fix.order.FixEntity;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.fix.configuration.OrderFixDestinationConfigurationBean;


/**
 * The <code>OrderFixDestinationConfigurationSetValueBeanImpl</code> allows setting a property on the fix entity bean
 * 1. Copy a value from the Placement
 * 2.
 *
 * @author manderson
 */
public class SetValueOrderFixDestinationConfigurationBean implements OrderFixDestinationConfigurationBean, ValidationAware {

	private static final String ORDER_PLACEMENT_TEMPLATE_KEY = "placement";
	private static final String FIX_ENTITY_TEMPLATE_KEY = "fixEntity";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The property on the fixEntity to set the value on
	 * This can also represent the additionalProperties map key
	 */
	private String fixEntityBeanPropertyName;

	/**
	 * If true, then we are adding to the additional properties map a value for the given key
	 * otherwise, it's a real property on the fix entity
	 */
	private boolean beanPropertyIsAdditionalPropertiesMapKey;

	/**
	 * If the change only applies to, for example a cancel request
	 * then the property wouldn't be on the FixOrder - so in that case just skip processing
	 */
	private boolean skipIfPropertyMissing;

	/**
	 * Static value - use this or the template
	 */
	private String value;


	/**
	 * Freemarker Template - to generate the new value to set if not using static value
	 */
	private String valueTemplate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertFalse(StringUtils.isEmpty(getFixEntityBeanPropertyName()), "Fix Entity Bean Property Name is required");
		if (StringUtils.isEmpty(getValue())) {
			ValidationUtils.assertFalse(StringUtils.isEmpty(getValueTemplate()), "A static value or value Freemarker template is required");
		}
		else {
			ValidationUtils.assertTrue(StringUtils.isEmpty(getValueTemplate()), "Either a static value or value Freemarker template can be supplied, but not both.");
		}
		if (isBeanPropertyIsAdditionalPropertiesMapKey()) {
			ValidationUtils.assertFalse(isSkipIfPropertyMissing(), "Cannot use skip option for the additional properties map.");
		}
	}


	@Override
	public <T> void applyConfiguration(OrderPlacement orderPlacement, T fixEntity) {
		ValidationUtils.assertNotNull(orderPlacement, "Order Placement is required.");
		ValidationUtils.assertNotNull(fixEntity, "Fix Entity is required.");
		validate(); // Confirm setup is correct

		if (isBeanPropertyIsAdditionalPropertiesMapKey()) {
			if (fixEntity instanceof FixEntity) {
				((FixEntity) fixEntity).setAdditionalParam(getFixEntityBeanPropertyName(), new ObjectToStringConverter().convert(getNewValue(orderPlacement, fixEntity)));
			}
			return;
		}

		if (!BeanUtils.isPropertyPresent(fixEntity.getClass(), getFixEntityBeanPropertyName())) {
			ValidationUtils.assertTrue(isSkipIfPropertyMissing(), fixEntity.getClass().getSimpleName() + " entity does not contain property with name " + getFixEntityBeanPropertyName() + " and option to skip if missing is not set.  Please confirm the field name or skip if missing.");
			return; // Skip if missing then nothing to set
		}

		// Question - Do we have to worry about  non-Strings? No use cases right now
		BeanUtils.setPropertyValue(fixEntity, getFixEntityBeanPropertyName(), getNewValue(orderPlacement, fixEntity));
	}


	private <T> Object getNewValue(OrderPlacement orderPlacement, T fixEntity) {
		if (!StringUtils.isEmpty(getValue())) {
			return getValue();
		}
		else {
			TemplateConfig config = new TemplateConfig(getValueTemplate());
			config.addBeanToContext(ORDER_PLACEMENT_TEMPLATE_KEY, orderPlacement);
			config.addBeanToContext(FIX_ENTITY_TEMPLATE_KEY, fixEntity);
			return getTemplateConverter().convert(config);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public String getFixEntityBeanPropertyName() {
		return this.fixEntityBeanPropertyName;
	}


	public void setFixEntityBeanPropertyName(String fixEntityBeanPropertyName) {
		this.fixEntityBeanPropertyName = fixEntityBeanPropertyName;
	}


	public boolean isBeanPropertyIsAdditionalPropertiesMapKey() {
		return this.beanPropertyIsAdditionalPropertiesMapKey;
	}


	public void setBeanPropertyIsAdditionalPropertiesMapKey(boolean beanPropertyIsAdditionalPropertiesMapKey) {
		this.beanPropertyIsAdditionalPropertiesMapKey = beanPropertyIsAdditionalPropertiesMapKey;
	}


	public boolean isSkipIfPropertyMissing() {
		return this.skipIfPropertyMissing;
	}


	public void setSkipIfPropertyMissing(boolean skipIfPropertyMissing) {
		this.skipIfPropertyMissing = skipIfPropertyMissing;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getValueTemplate() {
		return this.valueTemplate;
	}


	public void setValueTemplate(String valueTemplate) {
		this.valueTemplate = valueTemplate;
	}
}
