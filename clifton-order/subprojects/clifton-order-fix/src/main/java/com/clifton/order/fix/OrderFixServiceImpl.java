package com.clifton.order.fix;

import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.fix.configuration.OrderFixDestinationConfigurationBean;
import com.clifton.order.fix.converter.OrderFixConverterHandler;
import com.clifton.system.bean.SystemBeanService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderFixServiceImpl implements OrderFixService {

	private AsynchronousMessageHandler orderFixMessagingHandler;

	private OrderExecutionService orderExecutionService;

	private OrderFixConverterHandler orderFixConverterHandler;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void sendOrderPlacement(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, List<String> executingBrokerCompanyCodeList) {
		FixOrder fixOrder = getOrderFixConverterHandler().generateNewFixOrder(orderPlacement, orderAllocationList, executingBrokerCompanyCodeList);
		applyDestinationConfiguration(orderPlacement, fixOrder);
		sendFixEntity(fixOrder, orderPlacement.getId());
	}


	@Override
	public void sendCancelRequestOrderPlacement(OrderPlacement orderPlacement) {
		FixOrderCancelRequest fixOrderCancelRequest = getOrderFixConverterHandler().generateCancelRequestFixOrder(orderPlacement);
		applyDestinationConfiguration(orderPlacement, fixOrderCancelRequest);
		sendFixEntity(fixOrderCancelRequest, orderPlacement.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T> void applyDestinationConfiguration(OrderPlacement orderPlacement, T fixEntity) {
		if (orderPlacement.getOrderDestinationConfiguration() != null && orderPlacement.getOrderDestinationConfiguration().getConfigurationBean() != null) {
			OrderFixDestinationConfigurationBean configurationBean = (OrderFixDestinationConfigurationBean) getSystemBeanService().getBeanInstance(orderPlacement.getOrderDestinationConfiguration().getConfigurationBean());
			configurationBean.applyConfiguration(orderPlacement, fixEntity);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void sendFixEntity(FixEntity fixEntity, Long sourceFkFieldId) {
		// Generate message
		FixEntityMessagingMessage entityMessage = new FixEntityMessagingMessage();
		entityMessage.setFixEntity(fixEntity);
		entityMessage.setSourceSystemFkFieldId(sourceFkFieldId);
		entityMessage.getProperties().put(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME, MessageTypes.JSON.getContentType());

		// Send message
		getOrderFixMessagingHandler().send(entityMessage);
	}
}
