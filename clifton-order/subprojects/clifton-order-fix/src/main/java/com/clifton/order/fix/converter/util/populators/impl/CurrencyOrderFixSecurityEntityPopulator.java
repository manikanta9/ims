package com.clifton.order.fix.converter.util.populators.impl;

import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.fix.converter.util.populators.OrderFixSecurityEntityPopulator;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The <code>CurrencyOrderFixSecurityEntityPopulator</code> is used to populate security information on the FixSecurityEntity specific for Currency Orders.
 * For example, the symbol is actually a concatenation of the given and settlement currency
 */
@Component
@Getter
@Setter
public class CurrencyOrderFixSecurityEntityPopulator implements OrderFixSecurityEntityPopulator {

	private OrderSharedMarketDataService orderSharedMarketDataService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return 80;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(OrderPlacement orderPlacement) {
		return orderPlacement.getOrderBlock().getSecurity().isCurrency();
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, OrderPlacement orderPlacement) {
		String ccy1 = orderPlacement.getOrderBlock().getSecurity().getTicker();
		String ccy2 = orderPlacement.getOrderBlock().getSettlementCurrency().getTicker();
		CurrencyConventionData currencyConventionData = getOrderSharedMarketDataService().getOrderMarketDataCurrencyConvention(ccy1, ccy2);
		securityEntity.setSymbol(currencyConventionData.getDominantCurrencyCode() + "/" + currencyConventionData.getNonDominateCurrencyCode());
		securityEntity.setCurrency(ccy1);
		securityEntity.setSettlementCurrency(ccy2);
		securityEntity.setExDestination(null);
	}
}
