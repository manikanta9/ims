package com.clifton.order.fix.processor.messaging.message;

import com.clifton.fix.order.FixEntity;


/**
 * @author mwacker
 */
public interface OrderFixMessageHandler<T extends FixEntity> {

	public void handleMessage(long orderPlacementId, T fixEntity);


	public Class<T> getMessageClass();
}
