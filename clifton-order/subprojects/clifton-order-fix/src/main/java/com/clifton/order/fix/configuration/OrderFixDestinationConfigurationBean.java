package com.clifton.order.fix.configuration;

import com.clifton.order.execution.OrderPlacement;


/**
 *
 */
public interface OrderFixDestinationConfigurationBean {

	/**
	 * Applies the modifications, if any to the given fixEntity for the OrderPlacement just prior to sending to FIX
	 * The fix entity can be modified on various types - New Orders, Cancel Requests, etc.
	 * Most commonly used to add additional attributes to the object that are session specific (i.e. TradeSessionType)
	 * When we have limit orders, we can utilize this to apply the limit price.
	 * This can also be used to modify existing properties on the fixEntity.
	 */
	public <T> void applyConfiguration(OrderPlacement orderPlacement, T fixEntity);
}
