package com.clifton.order.fix.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.fix.lifecycle.BaseFixMessageLifeCycleRetriever;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>OrderFixLifeCycleRetriever</code> is applied to placements that utilize FIX for executions to include FIX messages
 * associated with the placement
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderFixLifeCycleRetriever extends BaseFixMessageLifeCycleRetriever {

	private FixMessageService fixMessageService;

	private SecurityImpersonationHandler securityImpersonationHandler;

	@Value("${fix.sourceSystemName:OMS}")
	private String fixSourceSystemName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		if (entity instanceof OrderPlacement) {
			OrderPlacement placement = (OrderPlacement) entity;
			return placement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FIX);
		}
		return false;
	}


	@Override
	public List<FixMessage> getFixMessageList(SystemLifeCycleEventContext lifeCycleEventContext) {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setSourceSystemName(getFixSourceSystemName());
		searchForm.setSourceSystemFkFieldId(BeanUtils.getIdentityAsLong(lifeCycleEventContext.getCurrentProcessingEntity()));
		return getSecurityImpersonationHandler().runAsSecurityUserNameAndReturn(SecurityUser.SYSTEM_USER, () -> getFixMessageService().getFixMessageList(searchForm));
	}
}
