package com.clifton.order.fix.processor.messaging.message;

import com.clifton.core.util.event.EventHandler;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.order.setup.OrderExecutionStatus;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public abstract class AbstractOrderFixMessageHandler<T extends FixEntity> implements OrderFixMessageHandler<T> {

	private EventHandler eventHandler;

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	protected String getExecutionStatusNameForOrderStatus(OrderStatuses orderStatus) {
		if (orderStatus == null) {
			return null;
		}
		switch (orderStatus) {
			case NEW:
				return OrderExecutionStatus.NEW;
			case PARTIALLY_FILLED:
				return OrderExecutionStatus.PARTIALLY_FILLED;
			case FILLED:
				return OrderExecutionStatus.FILLED;
			case DONE_FOR_DAY:
				return OrderExecutionStatus.DONE_FOR_DAY;
			case CANCELED:
				return OrderExecutionStatus.CANCELED_STATUS;
			case REJECTED:
				return OrderExecutionStatus.REJECTED;
			case REPLACED:
				return OrderExecutionStatus.REPLACED;
			case STOPPED:
			case PENDING_CANCEL:
			case PENDING_REPLACE:
			default:
				// TODO
				return null;
		}
	}
}
