package com.clifton.order.fix.processor;


import com.clifton.fix.order.FixEntity;


/**
 * The <code>OrderFixProcessor</code> defines method(s) used to process incoming FIX object.
 * <p>
 *
 * @author manderson
 */
public interface OrderFixProcessor {

	/**
	 * Processes the FIX message
	 */
	public <T extends FixEntity> void process(Long orderPlacementId, T entity);


	/**
	 * Processes the FIX message without suppressing errors.
	 */
	public <T extends FixEntity> void processDoNotSuppressErrors(Long orderPlacementId, T entity);
}
