package com.clifton.order.fix.converter.util.populators;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.order.execution.OrderPlacement;
import org.springframework.core.Ordered;


/**
 * The <code>OrderFixSecurityEntityPopulator</code> interface should be implemented by classes that can be used to populate the FixSecurityEntity information on FixEntities.
 */
public interface OrderFixSecurityEntityPopulator extends Ordered {

	/**
	 * A method used to determine if the populate action is applicable to the placement based on properties
	 * within the placement.  This method should be called before calling populateFixSecurityEntityFields(...)
	 * to determine if the population operation should be executed.
	 *
	 * @return True if populator is applicable to the placement, False if not.
	 */
	public boolean isApplicable(OrderPlacement orderPlacement);


	/**
	 * Populates the FixSecurityEntity fields based on data within the OrderPlacement. This
	 * method should only be called if the call to the isApplicable(OrderPlacement orderPlacement) method returns true.
	 */
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, OrderPlacement orderPlacement);
}
