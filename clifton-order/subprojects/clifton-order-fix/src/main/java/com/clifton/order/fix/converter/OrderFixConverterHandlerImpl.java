package com.clifton.order.fix.converter;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.order.beans.TimeInForceTypes;
import com.clifton.fix.util.FixUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.fix.converter.util.populators.OrderFixSecurityEntityPopulator;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import com.clifton.order.shared.OrderAccount;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderFixConverterHandlerImpl implements OrderFixConverterHandler {

	public static final String FIX_UNIQUE_PLACEMENT_ID_PREFIX = "P";
	public static final String FIX_UNIQUE_ALLOCATION_ID_PREFIX = "A";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Autowired
	private List<OrderFixSecurityEntityPopulator> orderFixSecurityEntityPopulatorList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrder generateNewFixOrder(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, List<String> executingBrokerCompanyCodeList) {
		FixOrder fixOrder = new FixOrder();
		BigDecimal orderQty = orderPlacement.getQuantityIntended();
		ValidationUtils.assertTrue(orderQty.doubleValue() > 0, "Cannot send a FIX order with a quantity less than or equal to '0'");

		fixOrder.setFixDestinationName(OrderDestinationUtils.getOrderDestinationFixDestinationName(orderPlacement.getOrderDestination()));

		setOrderDates(fixOrder, orderPlacement);

		//result.setText(getDescription(tradeOrder.getTradeList(), destinationMapping));

		// set the trader user
		fixOrder.setSenderUserName(orderPlacement.getOrderBlock().getTraderUser().getUserName());

		setAccountField(fixOrder, orderPlacement, orderAllocationList);

		// set the broker codes
		fixOrder.setPartyList(getFixPartyList(executingBrokerCompanyCodeList));

		if (orderPlacement.getOrderBlock().isBuy()) {
			fixOrder.setSide(OrderSides.BUY);
		}
		else {
			fixOrder.setSide(OrderSides.SELL);
		}
		fixOrder.setOrderQty(orderQty);
		fixOrder.setOrderType(OrderTypes.MARKET);

		// set the order times
		fixOrder.setTimeInForce(TimeInForceTypes.DAY);
		fixOrder.setTransactionTime(new Date());

		// add the security fields
		setSecurityFields(orderPlacement, fixOrder);

		// set the unique id of the order
		fixOrder.setClientOrderStringId(FixUtils.createUniqueId(orderPlacement, FIX_UNIQUE_PLACEMENT_ID_PREFIX));

		fixOrder.setHandlingInstructions(HandlingInstructions.valueOf(OrderDestinationUtils.getOrderDestinationFixHandlingInstructions(orderPlacement.getOrderDestination())));
		return fixOrder;
	}


	@Override
	public FixOrderCancelRequest generateCancelRequestFixOrder(OrderPlacement orderPlacement) {
		FixOrderCancelRequest fixOrderCancelRequest = new FixOrderCancelRequest();
		fixOrderCancelRequest.setFixDestinationName(OrderDestinationUtils.getOrderDestinationFixDestinationName(orderPlacement.getOrderDestination()));

		fixOrderCancelRequest.setClientOrderStringId(FixUtils.createUniqueIdForCancel(orderPlacement, FIX_UNIQUE_PLACEMENT_ID_PREFIX));
		fixOrderCancelRequest.setOriginalClientOrderStringId(FixUtils.createUniqueId(orderPlacement, FIX_UNIQUE_PLACEMENT_ID_PREFIX));
		fixOrderCancelRequest.setTransactionTime(new Date());
		setSecurityFields(orderPlacement, fixOrderCancelRequest);

		fixOrderCancelRequest.setHandlingInstructions(HandlingInstructions.valueOf(OrderDestinationUtils.getOrderDestinationFixHandlingInstructions(orderPlacement.getOrderDestination())));
		fixOrderCancelRequest.setSenderUserName(orderPlacement.getOrderBlock().getTraderUser().getUserName());
		fixOrderCancelRequest.setOrderQty(orderPlacement.getQuantityIntended());
		if (orderPlacement.getOrderBlock().isBuy()) {
			fixOrderCancelRequest.setSide(OrderSides.BUY);
		}
		else {
			fixOrderCancelRequest.setSide(OrderSides.SELL);
		}
		return fixOrderCancelRequest;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setSecurityFields(OrderPlacement orderPlacement, FixSecurityEntity entity) {
		// when set to true, indicates a populator was found for the entity
		boolean foundApplicablePopulator = false;

		if (orderPlacement.getOrderBlock().getSecurity().getPrimaryExchange() != null) {
			entity.setExDestination(orderPlacement.getOrderBlock().getSecurity().getPrimaryExchange().getExchangeCode());
		}

		for (OrderFixSecurityEntityPopulator populator : getOrderFixSecurityEntityPopulatorList()) {
			if (populator.isApplicable(orderPlacement)) {
				populator.populateFixSecurityEntityFields(entity, orderPlacement);
				foundApplicablePopulator = true;
				break;
			}
		}

		if (!foundApplicablePopulator) {
			throw new RuntimeException("'" + orderPlacement.getOrderBlock().getSecurity() + "' is not supported by FIX session [" + entity.getFixDestinationName() + "].");
		}
	}


	private List<FixParty> getFixPartyList(List<String> executingBrokerCompanyCodeList) {
		List<FixParty> result = new ArrayList<>();
		for (String companyCode : CollectionUtils.getIterable(executingBrokerCompanyCodeList)) {
			FixParty party = new FixParty();
			party.setPartyId(companyCode);
			party.setPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE);
			party.setPartyRole(executingBrokerCompanyCodeList.size() == 1 ? PartyRoles.EXECUTING_FIRM : PartyRoles.ACCEPTABLE_COUNTERPARTY);

			result.add(party);
		}
		return result;
	}


	private void setAccountField(FixOrder fixOrder, OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList) {
		// Fix only accepts one account, so we'll use the account tag and not the allocation detail tag
		if (orderPlacement.getOrderDestination().isForceBlockingByAccount()) {
			OrderAccount allocationAccount = orderAllocationList.get(0).getClientAccount();
			String allocationAccountNumber = StringUtils.coalesce(allocationAccount.getAccountShortName(), allocationAccount.getAccountNumber());
			fixOrder.setAccount(allocationAccountNumber);
		}
		else {
			List<FixAllocationDetail> allocationDetailList = new ArrayList<>();

			// TODO ASSUMING CURRENCY AND FULLY PLACED NEED TO REVIEW LOGIC IF WE ARE SPLITTING
			if (orderPlacement.getOrderBlock().getSecurity().isCurrency() && MathUtils.isEqual(orderPlacement.getQuantityIntended(), orderPlacement.getOrderBlock().getQuantityIntended())) {
				for (OrderAllocation orderAllocation : CollectionUtils.getIterable(orderAllocationList)) {
					FixAllocationDetail detail = new FixAllocationDetail();

					OrderAccount allocationAccount = orderAllocation.getClientAccount();
					String allocationAccountNumber = StringUtils.coalesce(allocationAccount.getAccountShortName(), allocationAccount.getAccountNumber());
					detail.setAllocationAccount(allocationAccountNumber);
					// ???
					//if (!allocationAccount.getIssuingCompany().equals(trade.getExecutingBrokerCompany())) {
					//	detail.setProcessCode(ProcessCodes.STEP_OUT);
					//}
					//else {
					//	detail.setProcessCode(ProcessCodes.REGULAR);
					//}

					// Since we can NET currency orders reverse the sign if opposing allocation
					detail.setAllocationShares((orderAllocation.isBuy() != orderPlacement.getOrderBlock().isBuy()) ? MathUtils.negate(orderAllocation.getAccountingNotional()) : orderAllocation.getAccountingNotional());
					// clearing firm? addAllocationDetailParties(trade, destinationMapping, detail);

					allocationDetailList.add(detail);
				}
				fixOrder.setFixAllocationDetailList(allocationDetailList);
			}
		}
	}


	private void setOrderDates(FixOrder order, OrderPlacement orderPlacement) {
		order.setTradeDate(orderPlacement.getOrderBlock().getTradeDate());
		order.setSettlementDate(orderPlacement.getOrderBlock().getSettlementDate());
	}

	/**
	 * private String getDescription(List<Trade> tradeList, TradeDestinationMapping destinationMapping) {
	 * StringBuilder sb = new StringBuilder();
	 * boolean allAreEqual = true;
	 * String description = "";
	 * for (Trade trade : CollectionUtils.getIterable(tradeList)) {
	 * if ((trade.getDescription() != null) && !trade.getDescription().isEmpty()) {
	 * sb.append(trade.getDescription());
	 * sb.append("/n");
	 * if ((description == null) || description.isEmpty()) {
	 * description = trade.getDescription();
	 * }
	 * else {
	 * allAreEqual = description.equals(trade.getDescription());
	 * }
	 * }
	 * }
	 * return (allAreEqual ? description : sb.toString()) + (destinationMapping.getFixGlobalTradeNote() != null ? destinationMapping.getFixGlobalTradeNote() : "");
	 * }
	 **/
}
