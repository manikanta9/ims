Ext.ns('Clifton.order.fix');


Clifton.order.execution.OrderPlacementWindowOverrides['FIX Order'] = 'Clifton.order.fix.OrderFixPlacementWindow';

// Add Re-Process Button
Ext.override(Clifton.fix.FixMessageGrid, {
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Reprocess Selected',
				tooltip: 'Reprocess selected messages',
				iconCls: 'run',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one message to be processed.', 'No Message(s) Selected');
					}
					else {
						const messages = sm.getSelections();
						const messageIdList = [];
						for (let i = 0; i < messages.length; i++) {
							messageIdList[i] = messages[i].id;
						}
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Processing Messages',
							waitTarget: gridPanel,
							params: {
								messageIdList: messageIdList
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('tradeOrderFixReprocessFixMessageList.json');
					}
				}
			}, '-', {
				xtype: 'fix-download-selected-message-button'
			}, '-');
		}
	}
});


Clifton.order.fix.OrderPlacementFixExecutionReportGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fix/openapi-webclient/fixExecutionReportListForSourceEntity',
	xtype: 'gridpanel',
	appendStandardColumns: false,
	instructions: 'The following execution reports have been received for selected FIX Order. These are the messages that we received from FIX provider about the order: confirm receipt, fills, cancellation, reject (duplicates), etc.',
	getLoadParams: function() {
		const win = this.getWindow();
		return {
			'sourceSystemName': win.sourceSystemName,
			'sourceSystemFkFieldId': win.getMainFormId(),
			requestedMaxDepth: 4,
			readUncommittedRequested: true
		};
	},
	isPagingEnabled: function() {
		return true;
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'fixMessageMetadata.id', hidden: true},
		{header: 'Order ID', width: 15, dataIndex: 'orderId', hidden: true},
		{header: 'Secondary Order ID', width: 15, dataIndex: 'secondaryOrderId', hidden: true},
		{header: 'Client Order ID', width: 15, dataIndex: 'clientOrderStringId', hidden: true},
		{header: 'Original Client Order ID', width: 15, dataIndex: 'originalClientOrderStringId', hidden: true},

		{header: 'Received', width: 60, dataIndex: 'fixMessageMetadata.messageDateAndTime', type: 'date'},
		{header: 'Type', width: 40, dataIndex: 'executionTransactionType'},
		{header: 'Exec. Type', width: 40, dataIndex: 'executionType', hidden: true},
		{header: 'Exec. ID', width: 40, dataIndex: 'executionId', hidden: true},
		{header: 'Ref Exec. ID', width: 40, dataIndex: 'executionReferenceId', hidden: true},
		{
			header: 'Status', width: 60, dataIndex: 'orderStatus', filter: {
				type: 'list', options: [
					['NEW', 'NEW'], ['PARTIALLY_FILLED', 'PARTIALLY_FILLED'],
					['FILLED', 'FILLED'], ['CANCELED', 'CANCELED'],
					['DONE_FOR_DAY', 'DONE_FOR_DAY'], ['REPLACED', 'REPLACED'],
					['PENDING_CANCEL', 'PENDING_CANCEL'], ['STOPPED', 'STOPPED'],
					['REJECTED', 'REJECTED'], ['PENDING_REPLACE', 'PENDING_REPLACE']]
			}
		},
		{header: 'Text', width: 70, dataIndex: 'text'},
		{
			header: 'Buy/Sell', width: 25, dataIndex: 'side', filter: {type: 'list', options: [['BUY', 'BUY'], ['SELL', 'SELL']]},
			renderer: function(v, metaData) {
				metaData.css = (v === 'BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Order Type', width: 40, dataIndex: 'orderType', hidden: true},
		{header: 'Quantity', width: 35, dataIndex: 'orderQuantity', type: 'float'},
		{header: 'Last Qty', width: 45, dataIndex: 'lastShares', type: 'float'},
		{header: 'Cumulative Qty', width: 50, dataIndex: 'cumulativeQuantity', type: 'float'},
		{header: 'Security', width: 40, dataIndex: 'fixSecurityData.securityId', hidden: true},
		{header: 'Security ID Source', width: 40, dataIndex: 'fixSecurityData.securityIdSource', hidden: true},
		{header: 'Security Type', width: 40, dataIndex: 'fixSecurityData.securityType', hidden: true},
		{header: 'Price', width: 45, dataIndex: 'price', type: 'float'},
		{header: 'Last Price', width: 45, dataIndex: 'lastPrice', type: 'float'},
		{header: 'Avg Price', width: 50, dataIndex: 'averagePrice', type: 'float'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.json.fixMessageMetadata.id;
		}
	}
});
Ext.reg('fix-executionReportGrid', Clifton.order.fix.OrderPlacementFixExecutionReportGrid);

Clifton.order.fix.OrderPlacementFixAllocationAcknowledgementGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fix/openapi-webclient/fixAllocationAcknowledgementListForSourceEntity',
	xtype: 'gridpanel',
	appendStandardColumns: false,
	instructions: 'The following allocation acknowledgements from FIX provider have been received for selected FIX Order. FIX provider first confirms that they received allocations from us: RECEIVED. After that, they process allocation and either accept or reject it.',
	getLoadParams: function() {
		// default position date to previous business day
		const win = this.getWindow();
		return {
			'sourceSystemName': win.sourceSystemName,
			'sourceSystemFkFieldId': win.getMainFormId(),
			requestedMaxDepth: 4,
			readUncommittedRequested: true
		};
	},
	isPagingEnabled: function() {
		return true;
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'fixMessageMetadata.id', hidden: true},
		{header: 'Received', width: 50, dataIndex: 'fixMessageMetadata.messageDateAndTime', type: 'date'},
		{header: 'Trade Date', width: 50, dataIndex: 'tradeDate', xtype: 'localdate'},
		{
			header: 'Status', width: 50, dataIndex: 'allocationStatus', filter: {
				type: 'list', options: [
					['ACCEPTED', 'ACCEPTED'],
					['BLOCK_LEVEL_REJECT', 'BLOCK_LEVEL_REJECT'],
					['ACCOUNT_LEVEL_REJECT', 'ACCOUNT_LEVEL_REJECT'],
					['RECEIVED', 'RECEIVED'],
					['INCOMPLETE', 'INCOMPLETE'],
					['REJECTED_BY_INTERMEDIARY', 'REJECTED_BY_INTERMEDIARY'],
					['ALLOCATION_PENDING', 'ALLOCATION_PENDING'],
					['REVERSED', 'REVERSED'],
					['CANCELED', 'CANCELED']]
			}
		},
		{header: 'Reject Code', width: 50, dataIndex: 'allocationRejectCode'},
		{header: 'Text', width: 100, dataIndex: 'text'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.json.fixMessageMetadata.id;
		}
	}
});
Ext.reg('fix-allocationAcknowledgementGrid', Clifton.order.fix.OrderPlacementFixAllocationAcknowledgementGrid);

Clifton.order.fix.OrderPlacementFixAllocationReportDetailGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fixAllocationReportDetailListForSourceEntity',
	xtype: 'gridpanel',
	instructions: 'Final message about the order received from FIX provider if allocation was accepted. It includes final prices used (average pricing) and how these were allocated to specific client account(s).',
	getLoadParams: function() {
		// default position date to previous business day
		const win = this.getWindow();
		return {
			'sourceSystemName': win.sourceSystemName,
			'sourceSystemFkFieldId': win.getMainFormId(),
			requestedMaxDepth: 4,
			readUncommittedRequested: true
		};
	},
	columns: [
		{header: 'Account', width: 50, dataIndex: 'allocationAccount'},
		{header: 'Process Code', width: 50, dataIndex: 'ProcessCode'},
		{header: 'Shares', width: 50, dataIndex: 'allocationShares', type: 'float'},
		{header: 'Average Price', width: 50, dataIndex: 'allocationAveragePrice', type: 'float'},
		{header: 'Allocation Price', width: 50, dataIndex: 'allocationPrice', type: 'float', useNull: true},
		{header: 'Net Money', width: 50, dataIndex: 'allocationNetMoney', type: 'currency'},
		{header: 'Gross Amount', width: 50, dataIndex: 'allocationGrossTradeAmount', type: 'currency', hidden: true},
		{header: 'Accrued Interest', width: 50, dataIndex: 'allocationAccruedInterestAmount', type: 'currency', hidden: true}
	]
});
Ext.reg('fix-allocationReportDetailGrid', Clifton.order.fix.OrderPlacementFixAllocationReportDetailGrid);


Clifton.order.fix.OrderPlacementFixMessageGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fixMessageListFind',
	xtype: 'gridpanel',
	instructions: 'FIX Messages received and processed by our system. Also includes messages that failed processing. After the underlying problem is resolved, you can reprocess failed messages.',
	appendStandardColumns: false,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Incoming', width: 20, dataIndex: 'incoming', type: 'boolean'},
		{header: 'FIX Session', width: 40, dataIndex: 'identifier.session.definition.name', filter: {searchField: 'sessionDefinitionName'}},
		{header: 'Message Type', width: 40, dataIndex: 'type.name'},
		{header: 'Received', width: 40, dataIndex: 'messageEntryDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Raw Message Text', width: 80, dataIndex: 'text'}
	],
	getLoadParams: function(firstLoad) {
		const win = this.getWindow();
		return {
			'sourceSystemName': win.sourceSystemName,
			'sourceSystemFkFieldId': win.getMainFormId(),
			requestedMaxDepth: 6,
			readUncommittedRequested: true
		};
	},
	isPagingEnabled: function() {
		return false;
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',

		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Reprocess selected',
				tooltip: 'Reprocess selected messages',
				iconCls: 'run',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					if (sm.getCount() !== 1) {
						TCG.showError('Please select one and only one message to run.', 'No Message(s) Selected');
					}
					else {
						const messages = sm.getSelections();
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Reprocessing Message',
							waitTarget: gridPanel,
							params: {
								entryId: messages[0].id
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('fixMessageEntryReprocess.json');
					}
				}
			}, '-');
		}
	}
});
Ext.reg('fix-orderMessageGrid', Clifton.order.fix.OrderPlacementFixMessageGrid);
