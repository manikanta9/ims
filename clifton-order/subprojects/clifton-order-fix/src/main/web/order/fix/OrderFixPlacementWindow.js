TCG.use('Clifton.order.execution.BaseOrderPlacementWindow');

Clifton.order.fix.OrderFixPlacementWindow = Ext.extend(Clifton.order.execution.BaseOrderPlacementWindow, {
	sourceSystemName: 'OMS',

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		tabs.add(this.executionReportsTab);
		tabs.add(this.allocationAcknowledgementsTab);
		tabs.add(this.allocationReportsTab);
		tabs.add(this.fixMessagesTab);
	},

	executionReportsTab: {
		title: 'Execution Reports',
		items: [{
			xtype: 'fix-executionReportGrid'
		}]
	},

	allocationAcknowledgementsTab: {
		title: 'Allocation Acknowledgements',
		items: [{
			xtype: 'fix-allocationAcknowledgementGrid',
			wikiPage: 'IT/FIX+Trouble+Shooting',
			editor: {
				drillDownOnly: true,
				detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
				getDetailPageId: function(gridPanel, row) {
					return row.json.fixMessageMetadata.id;
				}
				/** TODO
				addEditButtons: function(t, grid) {
					TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

					const win = this.getWindow();
					const orderId = win.params.id;

					t.add({
						text: 'Reallocate',
						xtype: 'button',
						iconCls: 'shopping-cart',
						scope: grid,
						handler: function() {
							const order = win.defaultData;
							if ((TCG.isFalse(order.status.partialFill) || order.tradeList.length === 1) && order.status.fillComplete) {
								const loader = new TCG.data.JsonLoader({
									waitTarget: grid,
									params: {tradeOrderId: orderId},
									onLoad: function(record, conf) {
										grid.reload();
									}
								});
								loader.load('tradeOrderFixAllocationInstructionResend.json');
							}
							else {

								const className = 'Clifton.trade.order.TradeOrderPartialFillAllocationWindow';
								const id = orderId;
								const params = id ? {id: id} : undefined;
								const cmpId = TCG.getComponentId(className, id);
								TCG.createComponent(className, {
									id: cmpId,
									params: params,
									openerCt: grid,
									defaultIconCls: win.iconCls
								});

							}
						}
					});
					t.add('-');
					t.add({
						text: 'Cancel Allocation',
						xtype: 'button',
						iconCls: 'shopping-cart',
						scope: grid,
						handler: function() {
							const loader = new TCG.data.JsonLoader({
								waitTarget: grid,
								params: {tradeOrderId: orderId},
								onLoad: function(record, conf) {
									grid.reload();
								}
							});
							loader.load('tradeOrderFixAllocationInstructionCancel.json');
						}
					});
					t.add('-');
				}
				 **/
			}
		}]
	},

	allocationReportsTab: {
		title: 'Allocation Report Details',
		items: [{
			xtype: 'fix-allocationReportDetailGrid'
		}]
	},

	fixMessagesTab: {
		title: 'FIX Messages',
		items: [{
			xtype: 'fix-orderMessageGrid'
		}]
	}
});
