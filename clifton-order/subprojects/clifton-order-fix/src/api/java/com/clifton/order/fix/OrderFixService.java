package com.clifton.order.fix;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderPlacement;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderFixService {


	/**
	 * Sends a placement to FIX which is the equivalent of a NewOrderSingle message
	 */
	@DoNotAddRequestMapping
	public void sendOrderPlacement(OrderPlacement orderPlacement, List<OrderAllocation> orderAllocationList, List<String> executingBrokerCompanyCodeList);


	/**
	 * Sends a cancel attempt to FIX
	 */
	@DoNotAddRequestMapping
	public void sendCancelRequestOrderPlacement(OrderPlacement orderPlacement);
}
