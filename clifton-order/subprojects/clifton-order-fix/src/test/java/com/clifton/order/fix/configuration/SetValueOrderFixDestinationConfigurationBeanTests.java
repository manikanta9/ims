package com.clifton.order.fix.configuration;


import com.clifton.core.util.validation.ValidationException;
import com.clifton.fix.order.FixOrder;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.fix.configuration.impl.SetValueOrderFixDestinationConfigurationBean;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class SetValueOrderFixDestinationConfigurationBeanTests extends BaseOrderFixDestinationConfigurationBeanTests {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SetValueOrderFixDestinationConfigurationBean getConfigurationBean() {
		SetValueOrderFixDestinationConfigurationBean configurationBean = new SetValueOrderFixDestinationConfigurationBean();
		return wireConfigurationBean(configurationBean);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidation_EmptyBean_Invalid() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		Exception ve = Assertions.assertThrows(ValidationException.class, bean::validate);
		Assertions.assertEquals("Fix Entity Bean Property Name is required", ve.getMessage());
	}


	@Test
	public void testValidation_NoValueOrTemplate_Invalid() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("test");
		Exception ve = Assertions.assertThrows(ValidationException.class, bean::validate);
		Assertions.assertEquals("A static value or value Freemarker template is required", ve.getMessage());
	}


	@Test
	public void testValidation_ValueAndTemplate_Invalid() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("test");
		bean.setValue("test");
		bean.setValueTemplate("test");
		Exception ve = Assertions.assertThrows(ValidationException.class, bean::validate);
		Assertions.assertEquals("Either a static value or value Freemarker template can be supplied, but not both.", ve.getMessage());
	}


	@Test
	public void testValidation_UseMapAndSkipIfMissing_Invalid() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("test");
		bean.setValue("test");
		bean.setBeanPropertyIsAdditionalPropertiesMapKey(true);
		bean.setSkipIfPropertyMissing(true);
		Exception ve = Assertions.assertThrows(ValidationException.class, bean::validate);
		Assertions.assertEquals("Cannot use skip option for the additional properties map.", ve.getMessage());
	}


	@Test
	public void test_MissingOrderPlacement() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> bean.applyConfiguration(null, null));
		Assertions.assertEquals("Order Placement is required.", ve.getMessage());
	}


	@Test
	public void test_MissingFixEntity() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		OrderPlacement orderPlacement = new OrderPlacement();
		Exception ve = Assertions.assertThrows(ValidationException.class, () -> bean.applyConfiguration(orderPlacement, null));
		Assertions.assertEquals("Fix Entity is required.", ve.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void test_SetUsingStaticValue() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("clientOrderStringId");
		bean.setValue("Test");

		OrderPlacement placement = new OrderPlacement();
		FixOrder fixOrder = new FixOrder();
		fixOrder.setClientOrderStringId("Original Value");

		bean.applyConfiguration(placement, fixOrder);
		Assertions.assertEquals("Test", fixOrder.getClientOrderStringId());
	}


	@Test
	public void test_PropertyMissing_Skip() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("clientOrderStringIdMissing");
		bean.setValue("Test");
		bean.setSkipIfPropertyMissing(true);

		OrderPlacement placement = new OrderPlacement();
		FixOrder fixOrder = new FixOrder();
		fixOrder.setClientOrderStringId("Original Value");

		bean.applyConfiguration(placement, fixOrder);
		Assertions.assertEquals("Original Value", fixOrder.getClientOrderStringId());
	}


	@Test
	public void test_PropertyMissing_Error() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("clientOrderStringIdMissing");
		bean.setValue("Test");

		OrderPlacement placement = new OrderPlacement();
		FixOrder fixOrder = new FixOrder();

		Exception ve = Assertions.assertThrows(ValidationException.class, () -> bean.applyConfiguration(placement, fixOrder));
		Assertions.assertEquals("FixOrder entity does not contain property with name clientOrderStringIdMissing and option to skip if missing is not set.  Please confirm the field name or skip if missing.", ve.getMessage());
	}


	@Test
	public void test_SetUsingFreemarkerValue() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("clientOrderStringId");
		bean.setValueTemplate("${fixEntity.clientOrderStringId?replace('_','')}");

		OrderPlacement placement = new OrderPlacement();
		FixOrder fixOrder = new FixOrder();
		fixOrder.setClientOrderStringId("P_1_20211001");

		bean.applyConfiguration(placement, fixOrder);
		Assertions.assertEquals("P120211001", fixOrder.getClientOrderStringId());
	}


	@Test
	public void test_UsingAdditionalPropertiesMap() {
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("6337");
		bean.setValue("RFS");
		bean.setBeanPropertyIsAdditionalPropertiesMapKey(true);

		OrderPlacement placement = new OrderPlacement();
		placement.setOrderDestinationConfiguration(new OrderDestinationConfiguration());
		placement.getOrderDestinationConfiguration().setName("RFS");

		FixOrder fixOrder = new FixOrder();
		fixOrder.setClientOrderStringId("P_1_20211001");

		bean.applyConfiguration(placement, fixOrder);
		Assertions.assertEquals("RFS", fixOrder.getAdditionalParams().get("6337"));
	}


	@Test
	public void test_UsingAdditionalPropertiesMap2() {
		//TradeSessType
		SetValueOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setFixEntityBeanPropertyName("6337");
		bean.setValue("RFS");
		bean.setBeanPropertyIsAdditionalPropertiesMapKey(true);

		//Commission
		SetValueOrderFixDestinationConfigurationBean bean2 = getConfigurationBean();
		bean2.setFixEntityBeanPropertyName("12");
		bean2.setValue("0.5");
		bean2.setBeanPropertyIsAdditionalPropertiesMapKey(true);

		//CommissionType
		SetValueOrderFixDestinationConfigurationBean bean3 = getConfigurationBean();
		bean3.setFixEntityBeanPropertyName("13");
		bean3.setValue("2"); //Percentage
		bean3.setBeanPropertyIsAdditionalPropertiesMapKey(true);

		OrderPlacement placement = new OrderPlacement();
		placement.setOrderDestinationConfiguration(new OrderDestinationConfiguration());
		placement.getOrderDestinationConfiguration().setName("Configuration-1");

		FixOrder fixOrder = new FixOrder();
		fixOrder.setClientOrderStringId("P_1_20211001");

		bean.applyConfiguration(placement, fixOrder);
		bean2.applyConfiguration(placement, fixOrder);
		bean3.applyConfiguration(placement, fixOrder);

		Assertions.assertEquals("RFS", fixOrder.getAdditionalParams().get("6337"));
		Assertions.assertEquals("0.5", fixOrder.getAdditionalParams().get("12"));
		Assertions.assertEquals("2", fixOrder.getAdditionalParams().get("13"));
	}
}
