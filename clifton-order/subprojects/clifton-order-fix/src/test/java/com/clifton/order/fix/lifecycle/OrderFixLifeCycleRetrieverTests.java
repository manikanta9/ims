package com.clifton.order.fix.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.lifecycle.BaseFixMessageLifeCycleRetrieverTests;
import com.clifton.fix.message.FixMessageService;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationType;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleTestExecutor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderFixLifeCycleRetrieverTests extends BaseFixMessageLifeCycleRetrieverTests {

	@Resource
	private FixMessageService fixMessageService;

	@Resource
	private OrderFixLifeCycleRetriever orderFixLifeCycleRetriever;

	@Resource
	private OrderExecutionService orderExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected SystemLifeCycleEventRetriever getSystemLifeCycleRetriever() {
		return this.orderFixLifeCycleRetriever;
	}


	@Override
	protected List<IdentityObject> getApplyToEntityFalseList() {
		OrderPlacement placement = new OrderPlacement();
		placement.setOrderDestination(new OrderDestination());
		placement.getOrderDestination().setDestinationType(new OrderDestinationType());
		placement.getOrderDestination().getDestinationType().setDestinationCommunicationType(DestinationCommunicationTypes.FILE);

		return CollectionUtils.createList(new OrderAllocation(), new OrderPlacement(), placement);
	}


	@Override
	protected List<IdentityObject> getApplyToEntityTrueList() {
		OrderPlacement placement = new OrderPlacement();
		placement.setOrderDestination(new OrderDestination());
		placement.getOrderDestination().setDestinationType(new OrderDestinationType());
		placement.getOrderDestination().getDestinationType().setDestinationCommunicationType(DestinationCommunicationTypes.FIX);
		return CollectionUtils.createList(placement);
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_NoResults() {
		Mockito.when(this.fixMessageService.getFixMessageList(ArgumentMatchers.any())).thenReturn(null);

		SystemLifeCycleTestExecutor.forTableAndEntityIdAndRetriever(OrderPlacement.TABLE_NAME, 1, getSystemLifeCycleRetriever())
				.withExpectedResults(new String[0])
				.execute();
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_Results() {
		Mockito.when(this.fixMessageService.getFixMessageList(ArgumentMatchers.any())).thenReturn(getTestFixMessageList());
		SystemLifeCycleTestExecutor.forTableAndEntityAndRetriever(OrderPlacement.TABLE_NAME, this.orderExecutionService.getOrderPlacement((long) 1), getSystemLifeCycleRetriever())
				.withExpectedResults("Source: N/A\tLink: OrderPlacement [1]\tEvent Source: Fix Message\tAction: New Order Single\tDate: 2021-07-29 10:00:00\tUser: Unknown\tDetails: Sent New Order Single to Test Session",
						"Source: N/A\tLink: OrderPlacement [1]\tEvent Source: Fix Message\tAction: Execution Report\tDate: 2021-07-29 10:00:00\tUser: Unknown\tDetails: Received Execution Report from Test Session",
						"Source: N/A\tLink: OrderPlacement [1]\tEvent Source: Fix Message\tAction: New Order Single\tDate: 2021-07-29 10:02:30\tUser: Unknown\tDetails: Sent New Order Single to Test Session")
				.execute();
	}
}
