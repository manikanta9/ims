package com.clifton.order.fix.configuration;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.fix.configuration.impl.RollupOrderFixDestinationConfigurationBean;
import com.clifton.order.fix.configuration.impl.SetValueOrderFixDestinationConfigurationBean;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBeanTypeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class RollupOrderFixDestinationConfigurationBeanTests extends BaseOrderFixDestinationConfigurationBeanTests {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private RollupOrderFixDestinationConfigurationBean getConfigurationBean() {
		RollupOrderFixDestinationConfigurationBean configurationBean = new RollupOrderFixDestinationConfigurationBean();
		return wireConfigurationBean(configurationBean);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidation_EmptyBean_Invalid() {
		RollupOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		Exception ve = Assertions.assertThrows(ValidationException.class, bean::validate);
		Assertions.assertEquals("You must select at least 2 configuration beans for this roll up configuration bean.", ve.getMessage());
	}


	@Test
	public void testValidation_OneChild_Invalid() {
		RollupOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setChildConfigurationBeanList(CollectionUtils.createList(createChildBean(SetValueOrderFixDestinationConfigurationBean.class.getName(), 1, "Test Child 1")));
		Exception ve = Assertions.assertThrows(ValidationException.class, bean::validate);
		Assertions.assertEquals("You must select at least 2 configuration beans for this roll up configuration bean.", ve.getMessage());
	}


	@Test
	public void testValidation_ToChildren_Valid() {
		RollupOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setChildConfigurationBeanList(CollectionUtils.createList(createChildBean(SetValueOrderFixDestinationConfigurationBean.class.getName(), 1, "Test Child 1"), createChildBean(SetValueOrderFixDestinationConfigurationBean.class.getName(), 2, "Test Child 2")));
		bean.validate();
	}


	@Test
	public void testValidation_ChildIsRollup_Invalid() {
		RollupOrderFixDestinationConfigurationBean bean = getConfigurationBean();
		bean.setChildConfigurationBeanList(CollectionUtils.createList(createChildBean(SetValueOrderFixDestinationConfigurationBean.class.getName(), 1, "Test Child 1"), createChildBean(RollupOrderFixDestinationConfigurationBean.class.getName(), 2, "Test Rollup")));
		Exception ve = Assertions.assertThrows(ValidationException.class, bean::validate);
		Assertions.assertEquals("Test Rollup is invalid as a child because it is also a rollup.  Please add individual child calculators.", ve.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemBean createChildBean(String className, int id, String beanName) {
		SystemBean bean = SystemBeanBuilder.createEmpty().withId(id).withName(beanName).toSystemBean();
		bean.setType(SystemBeanTypeBuilder.createWithNameAndClass(id, StringUtils.substringAfterLast(className, "."), className).toSystemBeanType());
		return bean;
	}
}
