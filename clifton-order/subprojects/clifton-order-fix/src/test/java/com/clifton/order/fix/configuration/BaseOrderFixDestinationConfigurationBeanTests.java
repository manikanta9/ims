package com.clifton.order.fix.configuration;


import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@ContextConfiguration("OrderFixDestinationConfigurationBeanTests-context.xml")
public abstract class BaseOrderFixDestinationConfigurationBeanTests implements ApplicationContextAware {


	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected <T> T wireConfigurationBean(T configurationBean) {
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(configurationBean, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return configurationBean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
