package com.clifton.order.fix;

import com.clifton.order.OrderProjectBasicTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderFixProjectBasicTests extends OrderProjectBasicTests {

	@Override
	public String getProjectPrefix() {
		return "order-fix";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		super.configureApprovedClassImports(imports);
		imports.add("org.springframework.core.Ordered");
		imports.add("org.springframework.beans.BeansException");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("configuration");
	}
}
