import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant

val apiVariant = registerVariant("api", upstreamForMain = true)

dependencies {

	////////////////////////////////////////////////////////////////////////////
	////////            Main Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////
	// Nothing yet

	////////////////////////////////////////////////////////////////////////////
	////////            External Dependencies                           ////////
	////////////////////////////////////////////////////////////////////////////

	api("javax.validation:validation-api:2.0.1.Final")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	////////            API Dependencies                                ////////
	////////////////////////////////////////////////////////////////////////////
	apiVariant.api(project(":clifton-system:clifton-system-group"))
	apiVariant.api(project(":clifton-investment")) { usingVariant("shared") }
	apiVariant.api(project(":clifton-order:clifton-order-api")) { usingVariant("java-server") }
	apiVariant.api(project(":clifton-marketdata:clifton-exchangerate-api")) { usingVariant("java-client") }
	apiVariant.api(project(":clifton-marketdata:clifton-price-api")) { usingVariant("java-client") }
	apiVariant.api(project(":clifton-core"))
	apiVariant.api(project(":clifton-system"))
	apiVariant.api(project(":clifton-system:clifton-system-query"))
	apiVariant.api(project(":clifton-workflow"))

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-system")))
	testFixturesApi(testFixtures(project(":clifton-order")))
	testFixturesApi(testFixtures(project(":clifton-workflow")))
}
