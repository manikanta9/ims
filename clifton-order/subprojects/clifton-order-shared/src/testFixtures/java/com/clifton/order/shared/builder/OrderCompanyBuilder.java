package com.clifton.order.shared.builder;

import com.clifton.order.shared.OrderCompany;
import lombok.Builder;


public class OrderCompanyBuilder {

	@Builder
	public static OrderCompany build(
			int id,
			String name,
			String description,
			OrderCompany parent
	) {
		OrderCompany orderCompany = new OrderCompany();
		orderCompany.setId(id);
		orderCompany.setName(name);
		orderCompany.setDescription(description);
		orderCompany.setParent(parent);
		return orderCompany;
	}


	public static InnerBuilder createClientCompany() {
		return builder()
				.name("Test Client")
				.id(123);
	}


	public static InnerBuilder createCustodianCompany() {
		return builder()
				.name("Test Custodian")
				.id(124);
	}


	public static InnerBuilder createExecutingBrokerCompany() {
		return builder()
				.name("Test Broker")
				.id(125);
	}
}
