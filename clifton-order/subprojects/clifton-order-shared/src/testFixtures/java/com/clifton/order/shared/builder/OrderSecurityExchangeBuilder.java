package com.clifton.order.shared.builder;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarTimeZone;
import com.clifton.core.util.date.Time;
import com.clifton.order.shared.OrderSecurityExchange;
import lombok.Builder;


/**
 * @author manderson
 */
public class OrderSecurityExchangeBuilder {

	@Builder(builderMethodName = "")
	public static OrderSecurityExchange createSecurityExchange(
			short id,
			String exchangeCode,
			String name,
			Calendar calendar,
			String timeZone,
			String openTime,
			String openTimePartialDay,
			String closeTime,
			String closeTimePartialDay,
			boolean dstOn
	) {
		OrderSecurityExchange orderSecurityExchange = new OrderSecurityExchange();
		orderSecurityExchange.setId(id);
		orderSecurityExchange.setExchangeCode(exchangeCode);
		orderSecurityExchange.setName(name);
		orderSecurityExchange.setCalendar(calendar);
		if (timeZone != null) {
			CalendarTimeZone tz = new CalendarTimeZone();
			tz.setName(timeZone);
			if (dstOn) {
				tz.setDstOn(dstOn);
			}
			orderSecurityExchange.setTimeZone(tz);
		}
		if (openTime != null) {
			orderSecurityExchange.setOpenTime(Time.parse(openTime));
		}
		if (openTimePartialDay != null) {
			orderSecurityExchange.setOpenTimePartialDay(Time.parse(openTimePartialDay));
		}
		if (closeTime != null) {
			orderSecurityExchange.setCloseTime(Time.parse(closeTime));
		}
		if (closeTimePartialDay != null) {
			orderSecurityExchange.setCloseTimePartialDay(Time.parse(closeTimePartialDay));
		}
		return orderSecurityExchange;
	}


	public static InnerBuilder forExchangeCode(short id, String exchangeCode) {
		InnerBuilder builder = new InnerBuilder();
		builder.id(id);
		builder.exchangeCode(exchangeCode);
		return builder;
	}


	public static InnerBuilder forTimeZone(String timeZone, boolean includingClosingTime) {
		InnerBuilder builder = new InnerBuilder();
		builder.timeZone(timeZone);
		if (includingClosingTime) {
			builder.closeTime("17:00:00"); // 5 pm local time
		}
		return builder;
	}


	public static InnerBuilder forTimeZoneWithDst(String timeZone, boolean includingClosingTime) {
		return forTimeZone(timeZone, includingClosingTime).dstOn(true);
	}
}
