package com.clifton.order.shared.builder;

import com.clifton.core.util.CollectionUtils;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSecurityExchange;
import com.clifton.order.shared.OrderSharedTestObjectFactory;
import com.clifton.system.list.SystemListItem;
import lombok.Builder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


public class OrderSecurityBuilder {

	public static final Set<String> CURRENCIES_WITHOUT_PENNIES = CollectionUtils.newUnmodifiableSet("IDR, JPY, KRW");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static OrderSecurity newAUD() {
		return OrderSecurityBuilder.ofType(263, "AUD", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newBRL() {
		return OrderSecurityBuilder.ofType(16284, "BRL", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newCAD() {
		return OrderSecurityBuilder.ofType(425, "CAD", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newCHF() {
		return OrderSecurityBuilder.ofType(458, "CHF", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newCNH() {
		return OrderSecurityBuilder.ofType(22632, "CNH", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newDKK() {
		return OrderSecurityBuilder.ofType(12375, "DKK", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newEUR() {
		return OrderSecurityBuilder.ofType(2875, "EUR", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newGBP() {
		return OrderSecurityBuilder.ofType(764, "GBP", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newHKD() {
		return OrderSecurityBuilder.ofType(850, "HKD", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newIDR() {
		return OrderSecurityBuilder.ofType(20260, "IDR", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newILS() {
		return OrderSecurityBuilder.ofType(23102, "ILS", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newINR() {
		return OrderSecurityBuilder.ofType(12455, "INR", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newJPY() {
		return OrderSecurityBuilder.ofType(948, "JPY", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newKRW() {
		return OrderSecurityBuilder.ofType(12377, "KRW", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newMXN() {
		return OrderSecurityBuilder.ofType(12453, "MXN", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newNOK() {
		return OrderSecurityBuilder.ofType(16310, "NOK", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newRUB() {
		return OrderSecurityBuilder.ofType(20263, "RUB", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newSEK() {
		return OrderSecurityBuilder.ofType(10476, "SEK", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newTHB() {
		return OrderSecurityBuilder.ofType(20256, "THB", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newTRY() {
		return OrderSecurityBuilder.ofType(20255, "TRY", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newTWD() {
		return OrderSecurityBuilder.ofType(12378, "TWD", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newUSD() {
		return OrderSecurityBuilder.ofType(1975, "USD", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}


	public static OrderSecurity newZAR() {
		return OrderSecurityBuilder.ofType(12379, "ZAR", OrderSharedTestObjectFactory.newCurrencySecurityType()).build();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Builder
	public static OrderSecurity build(
			Integer id,
			SystemListItem type,
			String name,
			String description,
			String ticker,
			Integer priceMultiplier,
			BigDecimal priceMultiplierBD,
			String currencyDenomination,
			String sedol,
			String cusip,
			String isin,
			Date startDate,
			Date endDate,
			OrderSecurityExchange primaryExchange,
			boolean active,
			boolean tradable
	) {
		OrderSecurity orderSecurity = new OrderSecurity();
		orderSecurity.setId(id);
		orderSecurity.setSecurityType(type);
		orderSecurity.setName(name);
		orderSecurity.setDescription(description);
		orderSecurity.setTicker(ticker);
		if (priceMultiplierBD != null) {
			orderSecurity.setPriceMultiplier(priceMultiplierBD);
		}
		else if (priceMultiplier != null) {
			orderSecurity.setPriceMultiplier(new BigDecimal(priceMultiplier));
		}
		orderSecurity.setCurrencyCode(currencyDenomination);
		orderSecurity.setSedol(sedol);
		orderSecurity.setCusip(cusip);
		orderSecurity.setIsin(isin);
		orderSecurity.setStartDate(startDate);
		orderSecurity.setEndDate(endDate);
		orderSecurity.setPrimaryExchange(primaryExchange);
		orderSecurity.setActive(active);
		orderSecurity.setTradable(tradable);
		return orderSecurity;
	}


	public static InnerBuilder newBond(String ticker) {
		return ofType(ticker, OrderSharedTestObjectFactory.newBondsSecurityType());
	}


	public static InnerBuilder newUSDStock(String ticker) {
		return ofType(ticker, OrderSharedTestObjectFactory.newStocksSecurityType()).currencyDenomination("USD");
	}


	/**
	 * Uses price multiplier of 0.01 for the pricing difference on London Stock Exchange
	 */
	public static InnerBuilder newGBPStock(String ticker) {
		return ofType(ticker, OrderSharedTestObjectFactory.newStocksSecurityType()).currencyDenomination("GBP").priceMultiplierBD(BigDecimal.valueOf(0.01));
	}


	public static InnerBuilder newForward(String ticker) {
		return ofType(ticker, OrderSharedTestObjectFactory.newForwardsSecurityType());
	}


	public static InnerBuilder newFuture(String ticker) {
		return ofType(ticker, OrderSharedTestObjectFactory.newFuturesSecurityType());
	}


	public static InnerBuilder ofType(String ticker, SystemListItem securityType) {
		return ofType(null, ticker, securityType);
	}


	public static InnerBuilder ofType(Integer id, String ticker, SystemListItem securityType) {
		InnerBuilder securityBuilder = builder()
				.id(id)
				.ticker(ticker)
				.type(securityType)
				.active(true)
				.tradable(true);

		if (securityBuilder.build().isCurrency()) {
			securityBuilder.currencyDenomination(ticker);
		}
		else {
			securityBuilder.currencyDenomination("USD");
		}
		return securityBuilder;
	}
}
