package com.clifton.order.shared;


import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.shared.builder.OrderSecurityBuilder;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListItemBuilder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>OrderSharedTestObjectFactory</code> class contains helper methods for creating various order shared
 * objects used in tests.
 *
 * @author manderson
 */
public class OrderSharedTestObjectFactory {

	////////////////////////////////////////////////////////////////////////////
	//////////                     Security Types                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public static SystemListItem newCurrencySecurityType() {
		return SystemListItemBuilder.createEmptySystemListItem().withText(OrderSharedUtils.SECURITY_TYPE_CURRENCY).withActive(true).withId(1).toSystemListItem();
	}


	public static SystemListItem newStocksSecurityType() {
		return SystemListItemBuilder.createEmptySystemListItem().withText(OrderSharedUtils.SECURITY_TYPE_STOCKS).withActive(true).withId(3).toSystemListItem();
	}


	public static SystemListItem newBondsSecurityType() {
		return SystemListItemBuilder.createEmptySystemListItem().withText(OrderSharedUtils.SECURITY_TYPE_BONDS).withActive(true).withId(3).toSystemListItem();
	}


	public static SystemListItem newForwardsSecurityType() {
		return SystemListItemBuilder.createEmptySystemListItem().withText(OrderSharedUtils.SECURITY_TYPE_FORWARDS).withActive(true).withId(3).toSystemListItem();
	}


	public static SystemListItem newFuturesSecurityType() {
		return SystemListItemBuilder.createEmptySystemListItem().withText(OrderSharedUtils.SECURITY_TYPE_FUTURES).withActive(true).withId(3).toSystemListItem();
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                     Order Security                   ////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("DuplicateBranchesInSwitch")
	public static OrderSecurity newOrderSecurity(int id, String ticker, String securityType, BigDecimal priceMultiplier, String currencyCode) {
		OrderSecurity security = new OrderSecurity();
		security.setId(id);
		security.setTicker(ticker);
		security.setActive(true);
		security.setTradable(true);

		SystemListItem securityTypeItem = new SystemListItem();
		securityTypeItem.setText(securityType);

		security.setPriceMultiplier(priceMultiplier);
		security.setCurrencyCode(currencyCode);

		switch (securityType) {
			case OrderSharedUtils.SECURITY_TYPE_BONDS:
				security.setPriceMultiplier(new BigDecimal("0.01"));
				security.setDaysToSettle((short) 3);
				break;
			case OrderSharedUtils.SECURITY_TYPE_FORWARDS:
				OrderSecurity underlyingCurrency = getCurrencyForTicker(ticker.substring(0, 3));
				security.setUnderlyingSecurity(underlyingCurrency);
				security.setPriceMultiplier(BigDecimal.ONE);
				break;
			case OrderSharedUtils.SECURITY_TYPE_FUTURES:
				security.setDaysToSettle((short) 1);
				break;
			case OrderSharedUtils.SECURITY_TYPE_STOCKS:
				security.setPriceMultiplier(BigDecimal.ONE);
				break;
			case OrderSharedUtils.SECURITY_TYPE_SWAPS:
				security.setDaysToSettle((short) 1);
				break;
		}
		return security;
	}


	public static OrderSecurity newOrderSecurity(int id, String ticker, String investmentType) {
		return newOrderSecurity(id, ticker, investmentType, null, "USD");
	}


	private static Map<String, OrderSecurity> currencyMap;


	private static OrderSecurity getCurrencyForTicker(String ticker) {
		if (currencyMap == null) {
			currencyMap = new HashMap<>();
			currencyMap.put("AUD", OrderSecurityBuilder.newAUD());
			currencyMap.put("BRL", OrderSecurityBuilder.newBRL());
			currencyMap.put("CAD", OrderSecurityBuilder.newCAD());
			currencyMap.put("CHF", OrderSecurityBuilder.newCHF());
			currencyMap.put("CNH", OrderSecurityBuilder.newCNH());
			currencyMap.put("DKK", OrderSecurityBuilder.newDKK());
			currencyMap.put("EUR", OrderSecurityBuilder.newEUR());
			currencyMap.put("GBP", OrderSecurityBuilder.newGBP());
			currencyMap.put("HKD", OrderSecurityBuilder.newHKD());
			currencyMap.put("IDR", OrderSecurityBuilder.newIDR());
			currencyMap.put("ILS", OrderSecurityBuilder.newILS());
			currencyMap.put("INR", OrderSecurityBuilder.newINR());
			currencyMap.put("JPY", OrderSecurityBuilder.newJPY());
			currencyMap.put("KRW", OrderSecurityBuilder.newKRW());
			currencyMap.put("MXN", OrderSecurityBuilder.newMXN());
			currencyMap.put("NOK", OrderSecurityBuilder.newNOK());
			currencyMap.put("RUB", OrderSecurityBuilder.newRUB());
			currencyMap.put("SEK", OrderSecurityBuilder.newSEK());
			currencyMap.put("THB", OrderSecurityBuilder.newTHB());
			currencyMap.put("TRY", OrderSecurityBuilder.newTRY());
			currencyMap.put("TWD", OrderSecurityBuilder.newTWD());
			currencyMap.put("USD", OrderSecurityBuilder.newUSD());
			currencyMap.put("ZAR", OrderSecurityBuilder.newZAR());
		}

		OrderSecurity result = currencyMap.get(ticker);
		ValidationUtils.assertNotNull(result, "Unsupported currency denomination: " + ticker);
		return result;
	}
}
