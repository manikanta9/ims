package com.clifton.order.shared.marketdata;

import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import com.clifton.exchangerate.api.client.model.ExchangeRateData;
import com.clifton.price.api.client.model.PriceData;

import java.math.BigDecimal;


/**
 * Helper methods for shared market data when using mockito
 *
 * @author manderson
 */
public class OrderSharedMarketDataTestUtils {


	public static ExchangeRateData getAUDUSDExchangeRate() {
		return getExchangeRateDataForValue("0.7745");
	}


	public static ExchangeRateData getCHFUSDExchangeRate() {
		return getExchangeRateDataForValue("0.90667");
	}


	public static ExchangeRateData getEURUSDExchangeRate() {
		return getExchangeRateDataForValue("1.1789");
	}


	public static ExchangeRateData getCADUSDExchangeRate() {
		return getExchangeRateDataForValue("1.25295");
	}


	public static ExchangeRateData getHKDUSDExchangeRate() {
		return getExchangeRateDataForValue("7.78145");
	}


	public static ExchangeRateData getJPYUSDExchangeRate() {
		return getExchangeRateDataForValue("110.6111");
	}


	public static ExchangeRateData getExchangeRateDataForValue(String rate) {
		ExchangeRateData exchangeRateData = new ExchangeRateData();
		exchangeRateData.setExchangeRate(new BigDecimal(rate));
		return exchangeRateData;
	}


	public static CurrencyConventionData getAUDUSDCurrencyConvention() {
		CurrencyConventionData currencyConventionData = new CurrencyConventionData();
		currencyConventionData.setDominantCurrencyCode("AUD");
		currencyConventionData.setNonDominateCurrencyCode("USD");
		currencyConventionData.setFromCurrencyCode("AUD");
		currencyConventionData.setToCurrencyCode("USD");
		currencyConventionData.setMultiplyByExchangeRate(true);
		return currencyConventionData;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static PriceData getStockPriceForGBPSecurity(boolean quotedInPence) {
		return getPriceDataForPrice("2860", "28.60");
	}


	public static PriceData getPriceDataForPrice(String quotedPrice, String basePrice) {
		PriceData priceData = new PriceData();
		priceData.setPrice(new BigDecimal(quotedPrice));
		priceData.setBasePrice(new BigDecimal(basePrice));
		return priceData;
	}
}
