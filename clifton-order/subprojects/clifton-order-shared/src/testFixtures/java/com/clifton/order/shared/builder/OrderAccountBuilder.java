package com.clifton.order.shared.builder;

import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import lombok.Builder;


public class OrderAccountBuilder {

	@Builder
	public static OrderAccount build(
			int id,
			String accountShortName,
			String accountName,
			String description, String accountNumber,
			OrderCompany clientCompany,
			OrderCompany issuingCompany,
			String baseCurrency,
			boolean workflowStateActive,
			boolean clientAccount,
			boolean holdingAccount
	) {
		OrderAccount orderAccount = new OrderAccount();
		orderAccount.setId(id);
		orderAccount.setAccountShortName(accountShortName);
		orderAccount.setAccountName(accountName);
		orderAccount.setDescription(description);
		orderAccount.setAccountNumber(accountNumber);
		orderAccount.setClientCompany(clientCompany);
		orderAccount.setIssuingCompany(issuingCompany);
		orderAccount.setBaseCurrencyCode(baseCurrency);
		if (workflowStateActive) {
			orderAccount.setWorkflowState(WorkflowStateBuilder.createActive().toWorkflowState());
		}
		orderAccount.setClientAccount(clientAccount);
		orderAccount.setHoldingAccount(holdingAccount);
		return orderAccount;
	}


	public static InnerBuilder createTestAccount1() {
		return builder().id(4073)

				.clientCompany(OrderCompanyBuilder.createClientCompany().build())
				.issuingCompany(OrderCompanyBuilder.createCustodianCompany().build())
				.accountName("Testing Account")
				.accountShortName("Test-123")
				.accountNumber("123")
				.baseCurrency(OrderSecurityBuilder.newUSD().getTicker())
				.workflowStateActive(true)
				.clientAccount(true)
				.holdingAccount(true);
	}


	public static InnerBuilder createTestAccount2() {
		return builder().id(4075)

				.clientCompany(OrderCompanyBuilder.createClientCompany().build())
				.issuingCompany(OrderCompanyBuilder.createCustodianCompany().build())
				.accountName("Testing Account 2")
				.accountShortName("Test-456")
				.accountNumber("456")
				.baseCurrency(OrderSecurityBuilder.newUSD().getTicker())
				.workflowStateActive(true)
				.clientAccount(true)
				.holdingAccount(true);
	}
}
