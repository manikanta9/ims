package com.clifton.order.shared;


import com.clifton.core.util.CollectionUtils;
import com.clifton.order.OrderProjectBasicTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderSharedProjectBasicTests extends OrderProjectBasicTests {


	@Override
	protected boolean isMethodSkipped(Method method) {
		return CollectionUtils.createHashSet(
				"saveOrderCompany",
				"deleteOrderCompany",
				"saveOrderSecurity",
				"deleteOrderSecurity",
				"saveOrderAccount",
				"deleteOrderAccount",
				"saveOrderSecurityExchange",
				"deleteOrderSecurityExchange"
		).contains(method.getName());
	}
}
