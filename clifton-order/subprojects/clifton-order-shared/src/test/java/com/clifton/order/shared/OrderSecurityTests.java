package com.clifton.order.shared;

import com.clifton.core.util.date.DateUtils;
import com.clifton.order.shared.builder.OrderSecurityBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * @author manderson
 */
public class OrderSecurityTests {

	@Test
	public void testGetLabel() {
		OrderSecurity currencySecurity = OrderSecurityBuilder.newAUD();
		currencySecurity.setName("Australian Dollar");
		Assertions.assertEquals("AUD (Australian Dollar)", currencySecurity.getLabel());

		// No name
		currencySecurity.setName(null);
		Assertions.assertEquals("AUD", currencySecurity.getLabel());

		// Put name back, but no symbol
		currencySecurity.setName("AUD");
		currencySecurity.setTicker(null);
		Assertions.assertEquals("AUD", currencySecurity.getLabel());

		OrderSecurity stockSecurity = OrderSecurityBuilder.newUSDStock("GOOG").name("Google Stock").build();
		Assertions.assertEquals("GOOG (Google Stock)", stockSecurity.getLabel());

		// same name and symbol should only show it once
		stockSecurity.setName(stockSecurity.getTicker());
		Assertions.assertEquals("GOOG", stockSecurity.getLabel());

		// inactive security
		stockSecurity.setEndDate(DateUtils.toDate("05/01/2021"));
		stockSecurity.setActive(false);
		Assertions.assertEquals("GOOG [INACTIVE]", stockSecurity.getLabel());

		// end date in future
		stockSecurity.setEndDate(DateUtils.addDays(new Date(), 5));
		stockSecurity.setActive(true);
		Assertions.assertEquals("GOOG", stockSecurity.getLabel());
	}


	@Test
	public void testIsActiveOn() {
		OrderSecurity stockSecurity = OrderSecurityBuilder.newUSDStock("GOOG").build();
		Assertions.assertTrue(stockSecurity.isActive());
		Assertions.assertTrue(stockSecurity.isActiveOn(new Date()));

		stockSecurity.setEndDate(DateUtils.toDate("05/01/2021"));
		Assertions.assertFalse(stockSecurity.isActiveOn(new Date()));

		Assertions.assertTrue(stockSecurity.isActiveOn(stockSecurity.getEndDate()));
		Assertions.assertTrue(stockSecurity.isActiveOn(DateUtils.addDays(stockSecurity.getEndDate(), -5)));
	}


	@Test
	public void testIsCurrency() {
		OrderSecurity orderSecurity = new OrderSecurity();
		Assertions.assertFalse(orderSecurity.isCurrency());

		OrderSecurity currencySecurity = OrderSecurityBuilder.newAUD();
		Assertions.assertTrue(currencySecurity.isCurrency());

		OrderSecurity stockSecurity = OrderSecurityBuilder.newUSDStock("GOOG").build();
		Assertions.assertFalse(stockSecurity.isCurrency());
	}


	@Test
	public void testToModelObject() {
		OrderSecurity currencySecurity = OrderSecurityBuilder.newAUD();
		Assertions.assertEquals("class SecurityModel {\n" +
				"    id: 263\n" +
				"    masterIdentifier: null\n" +
				"    ticker: AUD\n" +
				"    cusip: null\n" +
				"    isin: null\n" +
				"    sedol: null\n" +
				"    occSymbol: null\n" +
				"    bloombergGlobalIdentifier: null\n" +
				"    name: null\n" +
				"    description: null\n" +
				"    currencyCode: AUD\n" +
				"    underlyingSecurity: null\n" +
				"    company: null\n" +
				"    primaryExchangeCode: null\n" +
				"    compositeExchangeCode: null\n" +
				"    securityHierarchy: null\n" +
				"    securityType: Currency\n" +
				"    securityTypeSubType: null\n" +
				"    securityTypeSubType2: null\n" +
				"    countryOfRisk: null\n" +
				"    startDate: null\n" +
				"    endDate: null\n" +
				"}", currencySecurity.toModelObject().toString());
	}
}
