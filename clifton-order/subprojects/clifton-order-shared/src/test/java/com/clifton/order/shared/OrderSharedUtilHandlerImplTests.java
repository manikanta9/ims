package com.clifton.order.shared;

import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.shared.builder.OrderSecurityBuilder;
import com.clifton.order.shared.builder.OrderSecurityExchangeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderSharedUtilHandlerImplTests {

	@Resource
	private OrderSharedUtilHandler orderSharedUtilHandler;


	@Resource
	private CalendarSetupService calendarSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////            Settlement Date Tests                           ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateSettlementDate() {
		Date transactionDate = DateUtils.toDate("3/12/2012");
		OrderSecurity security = OrderSharedTestObjectFactory.newOrderSecurity(1, "IBM", OrderSharedUtils.SECURITY_TYPE_STOCKS);

		Date settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/12/2012"), settlementDate);

		security.setDaysToSettle((short) 1);
		settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/13/2012"), settlementDate);

		security.setDaysToSettle((short) 2);
		settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/14/2012"), settlementDate);

		security.setDaysToSettle((short) 4);
		settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/16/2012"), settlementDate);

		security.setDaysToSettle((short) 5);
		settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/19/2012"), settlementDate);

		security.setSettlementCalendar(setupCalendar((short) 1));
		settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/19/2012"), settlementDate);

		security.setSettlementCalendar(setupCalendar((short) 2)); // 19th is a holiday
		settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/20/2012"), settlementDate);
		security.setSettlementCalendar(null);

		// cannot settle before Issue Date for security (common for corporate bonds)
		security.setStartDate(DateUtils.toDate("3/20/2012"));
		settlementDate = this.orderSharedUtilHandler.calculateSettlementDate(security, null, transactionDate);
		Assertions.assertEquals(DateUtils.toDate("3/20/2012"), settlementDate);
	}


	@Test
	public void testSettlementDateNext() {
		Date settlementDate = DateUtils.toDate("3/14/2012");
		OrderSecurity security = OrderSharedTestObjectFactory.newOrderSecurity(1, "IBM", OrderSharedUtils.SECURITY_TYPE_STOCKS);

		settlementDate = this.orderSharedUtilHandler.getOrderSecuritySettlementDateNext(security, null, settlementDate);
		Assertions.assertEquals(DateUtils.toDate("3/15/2012"), settlementDate);

		security.setSettlementCalendar(setupCalendar((short) 1));
		settlementDate = this.orderSharedUtilHandler.getOrderSecuritySettlementDateNext(security, null, settlementDate);
		Assertions.assertEquals(DateUtils.toDate("3/16/2012"), settlementDate);

		settlementDate = this.orderSharedUtilHandler.getOrderSecuritySettlementDateNext(security, null, settlementDate);
		Assertions.assertEquals(DateUtils.toDate("3/19/2012"), settlementDate);

		OrderSharedUtilHandler mockUtilHandler = Mockito.spy(this.orderSharedUtilHandler);
		Mockito.when(mockUtilHandler.isValidSettlementDate(ArgumentMatchers.eq(security), ArgumentMatchers.nullable(OrderSecurity.class), ArgumentMatchers.any(Date.class))).thenReturn(false);
		Exception e = Assertions.assertThrows(IllegalStateException.class, () -> mockUtilHandler.getOrderSecuritySettlementDateNext(security, null, DateUtils.toDate("03/11/2012")));
		Assertions.assertEquals("Cannot determine a valid next settlement date after attempting to move through 100 days for security [IBM] and original settlement date [03/11/2012].  Last date checked: 06/20/2012.", e.getMessage());
	}


	@Test
	public void testValidSettlementDate_Currency() {
		OrderSecurity aud = OrderSecurityBuilder.newAUD();
		OrderSecurity usd = OrderSecurityBuilder.newUSD();

		// No calendars - as long as not a weekend
		Assertions.assertTrue(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/19/2012")));
		Assertions.assertFalse(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/18/2012")));

		Calendar calendar1 = setupCalendar(((short) 1));
		Calendar calendar2 = setupCalendar(((short) 2));

		aud.setSettlementCalendar(calendar1);
		usd.setSettlementCalendar(calendar2); // 19th is a holiday

		Assertions.assertFalse(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/19/2012")));
		Assertions.assertFalse(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/18/2012")));
		Assertions.assertTrue(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/20/2012")));

		// Switch the calendars
		aud.setSettlementCalendar(calendar2); // 19th is a holiday
		usd.setSettlementCalendar(calendar1);

		// Same Results
		Assertions.assertFalse(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/19/2012")));
		Assertions.assertFalse(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/18/2012")));
		Assertions.assertTrue(this.orderSharedUtilHandler.isValidSettlementDate(aud, usd, DateUtils.toDate("3/20/2012")));

		// Test calc settlement date
		Assertions.assertEquals(DateUtils.toDate("3/20/2012"), this.orderSharedUtilHandler.calculateSettlementDate(aud, usd, DateUtils.toDate("03/19/2012")));
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Trade Date Tests                             ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateTradeDate_NoCalendar() {
		OrderSecurity security = setupSecurity(null);
		for (Date date : getTestDates()) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, date);
			Assertions.assertEquals(DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}

		security = setupSecurity(new OrderSecurityExchange());
		for (Date date : getTestDates()) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, date);
			Assertions.assertEquals(DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_NoTimeZone() {
		Calendar calendar = setupCalendar((short) 1);
		OrderSecurityExchange exchange = new OrderSecurityExchange();
		exchange.setCalendar(calendar);
		OrderSecurity security = setupSecurity(exchange);
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testTradeDateNext() {
		Calendar calendar = setupCalendar((short) 1);
		OrderSecurityExchange exchange = new OrderSecurityExchange();
		exchange.setCalendar(calendar);
		OrderSecurity security = setupSecurity(exchange);
		security.setId(1);
		security.setTicker("test");

		Date tradeDate = DateUtils.toDate("3/14/2012");

		tradeDate = this.orderSharedUtilHandler.getOrderSecurityTradeDateNext(security, tradeDate);
		Assertions.assertEquals(DateUtils.toDate("3/15/2012"), tradeDate);

		tradeDate = this.orderSharedUtilHandler.getOrderSecurityTradeDateNext(security, tradeDate);
		Assertions.assertEquals(DateUtils.toDate("3/16/2012"), tradeDate);

		tradeDate = this.orderSharedUtilHandler.getOrderSecurityTradeDateNext(security, tradeDate);
		Assertions.assertEquals(DateUtils.toDate("3/19/2012"), tradeDate);

		OrderSharedUtilHandler mockUtilHandler = Mockito.spy(this.orderSharedUtilHandler);
		Mockito.when(mockUtilHandler.isValidTradeDate(ArgumentMatchers.eq(security), ArgumentMatchers.any(Date.class))).thenReturn(false);
		Exception e = Assertions.assertThrows(IllegalStateException.class, () -> mockUtilHandler.getOrderSecurityTradeDateNext(security, DateUtils.toDate("03/11/2012")));
		Assertions.assertEquals("Cannot determine a valid next trade date after attempting to move through 100 days for security [test] and original trade date [03/11/2012].  Last date checked: 06/20/2012.", e.getMessage());
	}

	/////////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateTradeDate_WithoutTimeLA() {
		OrderSecurity security = setupSecurity(getExchange("America/Los_Angeles", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeChicago() {
		OrderSecurity security = setupSecurity(getExchange("America/Chicago", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeNewYork() {
		OrderSecurity security = setupSecurity(getExchange("America/New_York", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeSydney() {
		OrderSecurity security = setupSecurity(getExchange("Australia/Sydney", false));
		String[] result = new String[]{"10/10/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeJapan() {
		OrderSecurity security = setupSecurity(getExchange("Japan", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithoutTimeLondon() {
		OrderSecurity security = setupSecurity(getExchange("Europe/London", false));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}

	//////////////////////////////////////////////////////////////////////////


	@Test
	public void testCalculateTradeDate_WithTimeLA() {
		OrderSecurity security = setupSecurity(getExchange("America/Los_Angeles", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeChicago() {
		OrderSecurity security = setupSecurity(getExchange("America/Chicago", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeNewYork() {
		OrderSecurity security = setupSecurity(getExchange("America/New_York", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeSydney() {
		OrderSecurity security = setupSecurity(getExchange("Australia/Sydney", true));
		String[] result = new String[]{"10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeJapan() {
		OrderSecurity security = setupSecurity(getExchange("Japan", true));
		String[] result = new String[]{"10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}


	@Test
	public void testCalculateTradeDate_WithTimeLondon() {
		OrderSecurity security = setupSecurity(getExchange("Europe/London", true));
		String[] result = new String[]{"10/10/2011", "10/10/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/11/2011", "10/17/2011"};
		for (int i = 0; i < getTestDates().length; i++) {
			Date tradeDate = this.orderSharedUtilHandler.calculateTradeDate(security, getTestDates()[i]);
			Assertions.assertEquals(result[i], DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Validate Trade & Settlment Dates                ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidateTradeAndSettlementDate_SettleBeforeTrade() {
		OrderSecurity security = OrderSharedTestObjectFactory.newOrderSecurity(1, "IBM", OrderSharedUtils.SECURITY_TYPE_STOCKS);

		Date tradeDate = DateUtils.toDate("07/05/2021");
		Date settlementDate = DateUtils.toDate("07/02/2021");
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderSharedUtilHandler.validateTradeAndSettlementDateForSecurity(security, null, tradeDate, settlementDate, false);
		});
		Assertions.assertEquals("Settlement Date cannot be before trade date.", validationException.getMessage());

		// Try again while ignoring validation (should still error because this check is a hard requirement
		validationException = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderSharedUtilHandler.validateTradeAndSettlementDateForSecurity(security, null, tradeDate, settlementDate, true);
		});
		Assertions.assertEquals("Settlement Date cannot be before trade date.", validationException.getMessage());
	}


	@Test
	public void testValidateTradeAndSettlementDate() {
		OrderSecurity aud = OrderSecurityBuilder.newAUD();
		OrderSecurity usd = OrderSecurityBuilder.newUSD();

		Calendar calendar1 = setupCalendar(((short) 1));
		Calendar calendar2 = setupCalendar(((short) 2));

		aud.setSettlementCalendar(calendar1);
		usd.setSettlementCalendar(calendar2); // 19th is a holiday

		Date tradeDate = DateUtils.toDate("03/19/2012"); // ok to trade currency on a holiday, but not settle
		Date settlementDate = DateUtils.toDate("03/20/2012");

		this.orderSharedUtilHandler.validateTradeAndSettlementDateForSecurity(aud, usd, tradeDate, settlementDate, false);

		UserIgnorableValidationException validationException = Assertions.assertThrows(UserIgnorableValidationException.class, () -> {
			this.orderSharedUtilHandler.validateTradeAndSettlementDateForSecurity(aud, usd, tradeDate, tradeDate, false);
		});
		Assertions.assertEquals("Settlement Date [03/19/2012] is not a valid settlement date for order security AUD and settlement currency USD", validationException.getMessage());

		// Test again but ignore validation - should be OK
		this.orderSharedUtilHandler.validateTradeAndSettlementDateForSecurity(aud, usd, tradeDate, tradeDate, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Date[] testDates;


	private Date[] getTestDates() {
		if (this.testDates != null) {
			return this.testDates;
		}
		this.testDates = new Date[7];
		String localTimeZone = "America/Chicago";
		java.util.Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(localTimeZone));
		cal.set(java.util.Calendar.YEAR, 2011);
		cal.set(java.util.Calendar.MONTH, 9);
		cal.set(java.util.Calendar.DAY_OF_MONTH, 10);
		cal.set(java.util.Calendar.MINUTE, 0);
		cal.set(java.util.Calendar.SECOND, 0);
		cal.set(java.util.Calendar.MILLISECOND, 0);

		// 4 Hour increments from 4 am on 10/10 to 12 midnight 11/11
		cal.set(java.util.Calendar.HOUR_OF_DAY, 4);
		this.testDates[0] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 8);
		this.testDates[1] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 12);
		this.testDates[2] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 16);
		this.testDates[3] = cal.getTime();

		cal.set(java.util.Calendar.HOUR_OF_DAY, 20);
		this.testDates[4] = cal.getTime();

		cal.set(java.util.Calendar.DAY_OF_MONTH, 11);
		cal.set(java.util.Calendar.HOUR_OF_DAY, 0);
		this.testDates[5] = cal.getTime();

		// Saturday
		cal.set(java.util.Calendar.DAY_OF_MONTH, 15);
		cal.set(java.util.Calendar.HOUR_OF_DAY, 9);
		this.testDates[6] = cal.getTime();
		return this.testDates;
	}


	public Calendar setupCalendar(short calendarId) {
		short yr = 2011;
		if (this.calendarSetupService.getCalendarYear(yr) == null) {
			this.calendarSetupService.saveCalendarYearForYear(yr);
		}
		return this.calendarSetupService.getCalendar(calendarId);
	}


	private OrderSecurity setupSecurity(OrderSecurityExchange exchange) {
		return OrderSecurityBuilder.builder().primaryExchange(exchange).active(true).tradable(true).build();
	}


	private OrderSecurityExchange getExchange(String timeZone, boolean includeClosingTime) {
		Calendar calendar = setupCalendar((short) 1);
		return OrderSecurityExchangeBuilder.forTimeZone(timeZone, includeClosingTime).calendar(calendar).build();
	}
}
