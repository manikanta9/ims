package com.clifton.order.shared;

import com.clifton.order.shared.builder.OrderCompanyBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author manderson
 */
public class OrderCompanyTests {

	@Test
	public void testToModelObject() {
		OrderCompany testCustodian = OrderCompanyBuilder.createCustodianCompany().build();
		Assertions.assertEquals("class CompanyModel {\n" +
				"    masterIdentifier: null\n" +
				"    bloombergCompanyIdentifier: null\n" +
				"    businessIdentifierCode: null\n" +
				"    legalEntityIdentifier: null\n" +
				"    name: Test Custodian\n" +
				"    abbreviation: null\n" +
				"    alias: null\n" +
				"}", testCustodian.toModelObject().toString());
	}


	@Test
	public void testGetParentObligorCompany() {
		OrderCompany orderCompany1 = OrderCompanyBuilder.builder().id(1).name("Obligor").build();
		OrderCompany orderCompany2 = OrderCompanyBuilder.builder().id(2).name("Grand Parent Company").build();
		orderCompany2.setObligorCompany(orderCompany1);

		OrderCompany orderCompany3 = OrderCompanyBuilder.builder().id(3).name("Parent Company").build();
		orderCompany3.setParent(orderCompany2);

		OrderCompany orderCompany4 = OrderCompanyBuilder.builder().id(4).name("Company").build();
		orderCompany4.setParent(orderCompany3);

		Assertions.assertNull(orderCompany1.getObligorCompany());
		Assertions.assertEquals(orderCompany1, orderCompany2.getObligorCompany());
		Assertions.assertEquals(orderCompany1, orderCompany2.getParentObligorCompany());
		Assertions.assertEquals(orderCompany1, orderCompany3.getParentObligorCompany());
		Assertions.assertEquals(orderCompany1, orderCompany4.getParentObligorCompany());

		// Remove the Obligor Company and do recursive calls again
		orderCompany2.setObligorCompany(null);
		Assertions.assertNull(orderCompany1.getObligorCompany());
		Assertions.assertNull(orderCompany2.getObligorCompany());
		Assertions.assertNull(orderCompany2.getParentObligorCompany());
		Assertions.assertNull(orderCompany3.getParentObligorCompany());
		Assertions.assertNull(orderCompany4.getParentObligorCompany());
	}
}
