package com.clifton.order.shared;

import com.clifton.order.api.server.model.AccountModel;
import com.clifton.order.shared.builder.OrderAccountBuilder;
import com.clifton.order.shared.builder.OrderCompanyBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author manderson
 */
public class OrderAccountTests {


	@Test
	public void testGetLabelShort() {
		OrderAccount orderAccount = OrderAccountBuilder.builder().build();
		Assertions.assertNull(orderAccount.getLabelShort());

		orderAccount = OrderAccountBuilder.createTestAccount1().build();

		Assertions.assertEquals(orderAccount.getAccountShortName(), orderAccount.getLabelShort());

		orderAccount.setAccountShortName(null);
		Assertions.assertEquals(orderAccount.getAccountNumber(), orderAccount.getLabelShort());
	}


	@Test
	public void testGetLabel() {
		// Client Account, not a Holding Account
		OrderAccount orderAccount = OrderAccountBuilder.builder().clientAccount(true).accountShortName("TEST-123").accountName("Test 123 Account").build();
		Assertions.assertEquals("TEST-123: Test 123 Account", orderAccount.getLabel());

		// Client Account and Holding Account (But Missing Issuer)
		orderAccount.setHoldingAccount(true);
		Assertions.assertEquals("TEST-123", orderAccount.getLabel());

		// Client and Holding Account with Issuer
		orderAccount.setIssuingCompany(OrderCompanyBuilder.createCustodianCompany().build());
		Assertions.assertEquals("TEST-123 (Test Custodian)", orderAccount.getLabel());
	}


	@Test
	public void testToModelObject() {
		OrderAccount emptyAccount = OrderAccountBuilder.builder().build();
		// Ensure no NPE
		AccountModel emptyAccountModel = emptyAccount.toModelObject();
		Assertions.assertNotNull(emptyAccountModel);

		OrderAccount orderAccount = OrderAccountBuilder.createTestAccount1().build();
		AccountModel accountModel = orderAccount.toModelObject();
		Assertions.assertNotNull(accountModel);

		Assertions.assertEquals("class AccountModel {\n" +
				"    masterIdentifier: null\n" +
				"    clientName: Test Client\n" +
				"    accountNumber: 123\n" +
				"    accountNumber2: null\n" +
				"    accountName: Testing Account\n" +
				"    accountShortName: Test-123\n" +
				"    baseCurrencyCode: USD\n" +
				"    workflowState: Active\n" +
				"    inceptionDate: null\n" +
				"    issuingCompany: class CompanyModel {\n" +
				"        masterIdentifier: null\n" +
				"        bloombergCompanyIdentifier: null\n" +
				"        businessIdentifierCode: null\n" +
				"        legalEntityIdentifier: null\n" +
				"        name: Test Custodian\n" +
				"        abbreviation: null\n" +
				"        alias: null\n" +
				"    }\n" +
				"    businessService: null\n" +
				"    accountServiceTeam: null\n" +
				"}", orderAccount.toModelObject().toString());
	}
}
