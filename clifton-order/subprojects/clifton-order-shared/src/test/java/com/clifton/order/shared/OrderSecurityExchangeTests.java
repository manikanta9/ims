package com.clifton.order.shared;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.order.shared.builder.OrderSecurityExchangeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * @author manderson
 */
public class OrderSecurityExchangeTests {


	@Test
	public void testGetLabel() {
		OrderSecurityExchange exchange = OrderSecurityExchangeBuilder.forExchangeCode((short) 1, "NYSE").name("New York Stock Exchange").build();
		Assertions.assertEquals("New York Stock Exchange (NYSE)", exchange.getLabel());

		exchange.setExchangeCode(null);
		Assertions.assertEquals("New York Stock Exchange", exchange.getLabel());
	}


	@Test
	public void testGetLocalTimeZoneLabel() {
		java.util.Calendar now = java.util.Calendar.getInstance();
		String localTimeZone = now.getTimeZone().getID();
		OrderSecurityExchange exchange = new OrderSecurityExchange();
		Assertions.assertEquals("Local Time (" + localTimeZone + ")", exchange.getLocalTimeZoneLabel());
	}


	@Test
	public void testGetLocalTime() {
		java.util.Calendar now = java.util.Calendar.getInstance();
		String localTimeZone = now.getTimeZone().getID();

		Time time = Time.parse("08:00:00");
		OrderSecurityExchange exchange = OrderSecurityExchangeBuilder.forTimeZoneWithDst(localTimeZone, false).build();

		Assertions.assertNull(exchange.getOpenTimeLocal());
		Assertions.assertNull(exchange.getCloseTimeLocal());
		Assertions.assertNull(exchange.getOpenTimePartialDayLocal());
		Assertions.assertNull(exchange.getCloseTimePartialDayLocal());
		Assertions.assertFalse(exchange.isPartialDayHoursDefinedAndDifferent());

		exchange.setOpenTime(time);
		exchange.setCloseTime(time);
		exchange.setOpenTimePartialDay(time);
		exchange.setCloseTimePartialDay(time);

		Assertions.assertEquals(exchange.getOpenTime(), exchange.getOpenTimeLocal());
		Assertions.assertEquals(exchange.getCloseTime(), exchange.getCloseTimeLocal());
		Assertions.assertEquals(exchange.getOpenTimePartialDay(), exchange.getOpenTimePartialDayLocal());
		Assertions.assertEquals(exchange.getCloseTime(), exchange.getCloseTimePartialDayLocal());
		Assertions.assertFalse(exchange.isPartialDayHoursDefinedAndDifferent());
	}


	@Test
	public void testIsOpenClose() {
		java.util.Calendar now = java.util.Calendar.getInstance();
		String localTimeZone = now.getTimeZone().getID();
		OrderSecurityExchange exchange = OrderSecurityExchangeBuilder.forTimeZoneWithDst(localTimeZone, false).build();

		Assertions.assertTrue(exchange.isOpen());
		Assertions.assertTrue(exchange.isOpenPartialDay());
		Assertions.assertFalse(exchange.isPartialDayHoursDefinedAndDifferent());

		Date currentDate = now.getTime();
		Time twentyMinutesAgo = Time.valueOf(DateUtils.addMinutes(currentDate, -20));
		Time tenMinutesAgo = Time.valueOf(DateUtils.addMinutes(currentDate, -10));
		Time tenMinutesAhead = Time.valueOf(DateUtils.addMinutes(currentDate, 10));

		// Set Open Time to 10 minutes ago
		exchange.setOpenTime(tenMinutesAgo);
		Assertions.assertTrue(exchange.isOpen());
		Assertions.assertTrue(exchange.isOpenPartialDay());

		// Set Close Time to 10 minutes ahead
		exchange.setCloseTime(tenMinutesAhead);
		Assertions.assertTrue(exchange.isOpen());
		Assertions.assertTrue(exchange.isOpenPartialDay());

		// Set open and close time to earlier in the day
		exchange.setOpenTime(twentyMinutesAgo);
		exchange.setCloseTime(tenMinutesAgo);
		Assertions.assertFalse(exchange.isOpen());
		Assertions.assertFalse(exchange.isOpenPartialDay());

		// Set close time partial day to the past time, and close time in the future
		exchange.setOpenTimePartialDay(twentyMinutesAgo);
		exchange.setCloseTime(tenMinutesAhead);
		exchange.setCloseTimePartialDay(tenMinutesAgo);
		Assertions.assertTrue(exchange.isOpen());
		Assertions.assertFalse(exchange.isOpenPartialDay());
		Assertions.assertTrue(exchange.isPartialDayHoursDefinedAndDifferent());

		// Set open Time after close time
		exchange.setOpenTime(tenMinutesAhead);
		exchange.setOpenTimePartialDay(tenMinutesAhead);
		exchange.setCloseTime(tenMinutesAgo);
		exchange.setCloseTimePartialDay(twentyMinutesAgo);
		Assertions.assertFalse(exchange.isOpen());
		Assertions.assertFalse(exchange.isOpenPartialDay());
		Assertions.assertTrue(exchange.isPartialDayHoursDefinedAndDifferent());
	}
}
