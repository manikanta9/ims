package com.clifton.order.shared.marketdata;

import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderSharedMarketDataServiceImplTests {

	@Resource
	private OrderSharedMarketDataService orderSharedMarketDataService;

	////////////////////////////////////////////////////////////////////////////
	////////            Currency Convention Tests                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetCurrencyConvention_Success() {
		validateCurrencyConventionResult("JPY", "USD", "USD");
		validateCurrencyConventionResult("USD", "JPY", "USD");
		validateCurrencyConventionResult("TRY", "CAD", "CAD");
		validateCurrencyConventionResult("CAD", "TRY", "CAD");
	}


	@Test
	public void testGetCurrencyConvention_SameCurrency() {
		validateCurrencyConventionResult("USD", "USD", "USD");
		validateCurrencyConventionResult("JPY", "JPY", "JPY");
	}


	@Test
	public void testGetCurrencyConvention_MissingTicker() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention("ABC", "USD");
		});
		Assertions.assertEquals("From Currency Code [ABC] is not specified in the list [Currency Priority (Currency Conventions)].  Unable to determine currency convention for currency pair ABC/USD", validationException.getMessage());

		validationException = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention("USD", "ABC");
		});
		Assertions.assertEquals("To Currency Code [ABC] is not specified in the list [Currency Priority (Currency Conventions)].  Unable to determine currency convention for currency pair USD/ABC", validationException.getMessage());
	}


	private void validateCurrencyConventionResult(String fromCurrencyCode, String toCurrencyCode, String expectedDominantCurrencyCode) {
		String currencyPairString = fromCurrencyCode + "/" + toCurrencyCode;
		CurrencyConventionData currencyConventionData = this.orderSharedMarketDataService.getOrderMarketDataCurrencyConvention(fromCurrencyCode, toCurrencyCode);
		ValidationUtils.assertNotNull(currencyConventionData, "Missing expected CCY Convention data for currency pair " + currencyPairString);
		Assertions.assertEquals(expectedDominantCurrencyCode, currencyConventionData.getDominantCurrencyCode());
		if (fromCurrencyCode.equalsIgnoreCase(expectedDominantCurrencyCode)) {
			Assertions.assertTrue(currencyConventionData.getMultiplyByExchangeRate(), "Expected rate to be multiply by exchange rate when going FROM the dominant currency");
		}
		else {
			Assertions.assertFalse(currencyConventionData.getMultiplyByExchangeRate(), "Expected rate NOT to be multiply by exchange rate when going TO the dominant currency");
		}
	}
}
