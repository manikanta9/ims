package com.clifton.order.shared.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.shared.OrderSecurity;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderSecurityListByTickerCache</code> class caches the list of {@link OrderSecurity} objects by security ticker field.
 * This is very common use case that will reduce the number of database hits.  Most of the time, security tickers are globally unique.
 * In IMS - Out of 100K securities, we have 50 cases when the same ticker is used exactly twice for different securities.
 * May need to revisit this use case with Security Master
 *
 * @author manderson
 */
@Component
public class OrderSecurityListByTickerCache extends SelfRegisteringSingleKeyDaoListCache<OrderSecurity, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "ticker";
	}


	@Override
	protected String getBeanKeyValue(OrderSecurity security) {
		return security.getTicker();
	}


	/**
	 * Returns the upper case string representation of the super implementation.
	 */
	@Override
	protected String getBeanKeySegmentForProperty(Object keyProperty) {
		return super.getBeanKeySegmentForProperty(keyProperty).toUpperCase();
	}
}
