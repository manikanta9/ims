package com.clifton.order.shared.group.rebuild;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.search.OrderAccountSearchForm;
import com.clifton.system.bean.SystemBeanGroup;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.group.SystemGroup;
import com.clifton.system.group.SystemGroupItem;
import com.clifton.system.group.SystemGroupService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author AbhinayaM
 * The OrderAccountGroupSearchRebuildExecutor allows for the creation of system defined
 * Order Account groups using properties from the Order Account via OrderAccountSearchForm.
 */
@Getter
@Setter
public class OrderAccountGroupSearchRebuildExecutor implements OrderAccountGroupRebuildExecutor {

	private OrderSharedService orderSharedService;
	private SystemGroupService systemGroupService;


	private Integer issuingCompanyId;
	private Short workflowStateId;
	private Short workflowStatusId;
	private Short teamSecurityGroupId;
	private Short portfolioManagerUserId;
	private Short serviceOrParentId;
	private String baseCurrencyCode;
	private Integer companyPlatformId;


	@Transactional
	@Override
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status) {
		SystemGroup systemGroup = (SystemGroup) groupEntity;

		List<OrderAccount> orderAccountList = getRebuildOrderAccountListFromSearchCriteria();
		List<SystemGroupItem> systemGroupItemList = orderAccountList.stream().map(this::convert).collect(Collectors.toList());
		getSystemGroupService().saveSystemGroupItemList(systemGroup, systemGroupItemList);

		status.setMessage("Completed rebuild of Account Group [%s]; Total Items: [%d]", ((SystemGroup) groupEntity).getName(), CollectionUtils.getSize(orderAccountList));
		return status;
	}


	private SystemGroupItem convert(final OrderAccount orderAccount) {
		SystemGroupItem systemGroupItem = new SystemGroupItem();
		//Setting OrderAccount Id
		systemGroupItem.setFkFieldId(BeanUtils.getIdentityAsLong(orderAccount));
		return systemGroupItem;
	}


	@Override
	public void validate(EntityGroupRebuildAware groupEntity) {
		ValidationUtils.assertEquals("OrderAccount", groupEntity.getRebuildTableName(), "Rebuild executor applies to Order Accounts only");
		ValidationUtils.assertTrue(groupEntity instanceof SystemBeanGroup, "Rebuild executor applies to SystemBeanGroup only.");
	}


	private List<OrderAccount> getRebuildOrderAccountListFromSearchCriteria() {
		List<OrderAccount> rebuildOrderAccountList;
		OrderAccountSearchForm orderAccountSearchForm = new OrderAccountSearchForm();
		orderAccountSearchForm.setIssuingCompanyId(getIssuingCompanyId());
		orderAccountSearchForm.setWorkflowStateId(getWorkflowStateId());
		orderAccountSearchForm.setWorkflowStatusId(getWorkflowStatusId());
		orderAccountSearchForm.setTeamSecurityGroupId(getTeamSecurityGroupId());
		orderAccountSearchForm.setPortfolioManagerUserId(getPortfolioManagerUserId());
		orderAccountSearchForm.setServiceOrParentId(getServiceOrParentId());
		orderAccountSearchForm.setBaseCurrencyCode(getBaseCurrencyCode());
		orderAccountSearchForm.setCompanyPlatformId(getCompanyPlatformId());
		rebuildOrderAccountList = getOrderSharedService().getOrderAccountList(orderAccountSearchForm);
		return CollectionUtils.asNonNullList(rebuildOrderAccountList);
	}
}
