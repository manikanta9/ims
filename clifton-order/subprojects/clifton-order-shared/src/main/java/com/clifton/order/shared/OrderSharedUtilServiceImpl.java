package com.clifton.order.shared;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Date;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderSharedUtilServiceImpl implements OrderSharedUtilService {

	private OrderSharedService orderSharedService;
	private OrderSharedUtilHandler orderSharedUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getOrderSecurityTradeDate(int securityId) {
		return getOrderSharedUtilHandler().calculateTradeDate(getOrderSharedService().getOrderSecurity(securityId), null);
	}


	@Override
	public Date getOrderSecurityTradeDateNext(int securityId, Date tradeDate) {
		OrderSecurity security = getOrderSharedService().getOrderSecurity(securityId);
		return getOrderSharedUtilHandler().getOrderSecurityTradeDateNext(security, tradeDate);
	}


	@Override
	public Date getOrderSecuritySettlementDate(int securityId, Integer settlementCurrencyId, Date transactionDate) {
		return getOrderSharedUtilHandler().calculateSettlementDate(getOrderSharedService().getOrderSecurity(securityId), settlementCurrencyId != null ? getOrderSharedService().getOrderSecurity(settlementCurrencyId) : null, transactionDate);
	}


	@Override
	public Date getOrderSecuritySettlementDateNext(int securityId, Integer settlementCurrencyId, Date settlementDate) {
		OrderSecurity security = getOrderSharedService().getOrderSecurity(securityId);
		OrderSecurity settlementSecurity = (settlementCurrencyId == null ? null : getOrderSharedService().getOrderSecurity(settlementCurrencyId));
		return getOrderSharedUtilHandler().getOrderSecuritySettlementDateNext(security, settlementSecurity, settlementDate);
	}
}
