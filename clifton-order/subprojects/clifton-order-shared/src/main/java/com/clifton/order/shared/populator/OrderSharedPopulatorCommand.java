package com.clifton.order.shared.populator;

import com.clifton.core.util.status.StatusWithCounts;
import lombok.Getter;
import lombok.Setter;


/**
 * The <code>OrderSharedPopulatorCommand</code> is a basic command object that can be used for defining what data needs to be synced.
 * It contains options for all relevant sections so everything can be run at once or in pieces.
 * Each section kicks off a different process sequentially
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderSharedPopulatorCommand {

	/**
	 * If blank, will do a full sync.  Otherwise, will limit results to those updated since weekdays back.
	 * If not a full sync, will only pull those updated on or after weekdays back.
	 * i.e. 1 means UpdateDate >= Previous Weekday.
	 */
	private Integer weekdaysBack;

	/**
	 * The shared look up data can be common across accounts, companies, and securities.
	 * When running all syncs in order, we only need to run this for the first one so we can optionally turn this off internally to prevent.
	 */
	private boolean skipSharedLookupData;


	/**
	 * Option to populate Account Data
	 * accountMasterIdentifier and accountShortName can be used to look up a specific account
	 * Otherwise will do full sync (optionally limited on last updated date for weekdays back)
	 */
	private boolean populateAccountData;
	private Integer accountMasterIdentifier;
	private String accountShortName;

	/**
	 * Option to populate Company Data
	 * companyMasterIdentifier and companyName can be used to look up a specific company
	 * Otherwise will do full sync (optionally limited on last updated date for weekdays back)
	 */
	private boolean populateCompanyData;
	private Integer companyMasterIdentifier;
	private String companyName;

	/**
	 * Option to populate Security Data
	 * securityMasterIdentifier and securityTicker can be used to look up a specific security
	 * Otherwise will do full sync (optionally limited on last updated date for weekdays back)
	 */
	private boolean populateSecurityData;
	private Integer securityMasterIdentifier;
	private String securityTicker;


	private StatusWithCounts status;

	private boolean synchronous = false;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLookupDataRunId() {
		if (isSkipSharedLookupData()) {
			return "LOOKUP_DATA_NONE";
		}
		if (getWeekdaysBack() != null) {
			return "LOOKUP_DATA_ALL_UPDATED_DAYS_" + getWeekdaysBack();
		}
		return "LOOKUP_DATA_ALL";
	}


	public String getAccountRunId() {
		if (!isPopulateAccountData()) {
			return "ACCOUNTS_NONE";
		}
		if (getAccountMasterIdentifier() != null) {
			return "ACCOUNT_ID_" + getAccountRunId();
		}
		if (getAccountShortName() != null) {
			return "ACCOUNT_" + getAccountShortName();
		}
		if (getWeekdaysBack() != null) {
			return "ACCOUNTS_ALL_UPDATED_DAYS_" + getWeekdaysBack();
		}
		return "ACCOUNTS_ALL";
	}


	public String getCompanyRunId() {
		if (!isPopulateCompanyData()) {
			return "COMPANIES_NONE";
		}

		if (getCompanyMasterIdentifier() != null) {
			return "COMPANY_ID_" + getCompanyMasterIdentifier();
		}
		if (getCompanyName() != null) {
			return "COMPANY_" + getCompanyName();
		}
		if (getWeekdaysBack() != null) {
			return "COMPANIES_ALL_UPDATED_DAYS_" + getWeekdaysBack();
		}
		return "COMPANIES_ALL";
	}


	public String getSecurityRunId() {
		if (!isPopulateSecurityData()) {
			return "SECURITIES_NONE";
		}
		if (getSecurityMasterIdentifier() != null) {
			return "SECURITY_ID_" + getSecurityMasterIdentifier();
		}
		if (getSecurityTicker() != null) {
			return "SECURITY_" + getSecurityTicker();
		}
		if (getWeekdaysBack() != null) {
			return "SECURITIES_ALL_UPDATED_DAYS_" + getWeekdaysBack();
		}
		return "SECURITIES_ALL";
	}
}
