package com.clifton.order.shared.marketdata.cache;

import com.clifton.core.cache.CustomCache;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The PriceCache caches the price value for the give security (identified by master identifier)
 * Since we only use this for flexible lookups the cache is only for flexible retrievals
 *
 * @author manderson
 */
public interface OrderSharedPriceCache extends CustomCache<String, BigDecimal> {

	public BigDecimal getPriceFromCache(int masterSecurityIdentifier, Date date);


	public void setPriceInCache(int masterSecurityIdentifier, Date date, BigDecimal price);
}
