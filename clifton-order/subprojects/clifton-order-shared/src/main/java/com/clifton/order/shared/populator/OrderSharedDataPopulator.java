package com.clifton.order.shared.populator;

import java.util.List;


/**
 * The <code>OrderSharedDataPopulator</code> interface defines the method to get a list of objects for saving.
 * This only handles retrieving the data from the master system and does not handle merging/saving in the system.
 * Implementation is based on how the master system is set up.  Since at this point we are using a batch job with direct sql access the implementation will in OMS
 * <p>
 * Currently used for Companies, Accounts, and Securities.
 * Each implementation is reponsible for a specific object type <T>
 * The more generic look up data uses the {@link OrderSharedLookupDataPopulator} interface which does handle the entire upload / save process
 *
 * @author manderson
 */
public interface OrderSharedDataPopulator<T> {

	public List<T> populateOrderSharedDataList(OrderSharedPopulatorCommand command);
}
