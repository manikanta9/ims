package com.clifton.order.shared;

import com.clifton.calendar.setup.Calendar;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public interface OrderSharedUtilHandler {


	/**
	 * Returns trade date based on the specified transactionDate (with time) (uses current time if null) for the specified security.
	 * Depends on the primary exchange the security is traded in and it's operating hours and business days
	 * <p>
	 * Uses TRADE business days - If it's a partial day trade holiday, the primary exchanges partial day hours (if supplied) will be used
	 */
	public Date calculateTradeDate(OrderSecurity security, Date transactionDate);


	/**
	 * Returns the next available trade date from the given trade date.
	 * i.e. if Trade Date = today, will check tomorrow's date, if a valid trading business day will return tomorrow, if not will move again to the next calendar day
	 * This method does not use time check if the exchange is currently closed
	 */
	public Date getOrderSecurityTradeDateNext(OrderSecurity security, Date tradeDate);


	/**
	 * Returns true if the given date is a valid trade date for the security.
	 * <p>
	 * If there is no primary exchange supplied on the security, then the trade date is always considered to be valid
	 */
	public boolean isValidTradeDate(OrderSecurity security, Date tradeDate);


	/**
	 * Returns settlement date based on the specified transaction date (trade date) for the specified security.
	 * Optional "settlementCurrency" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 * <p>
	 * Uses SETTLEMENT business days
	 */
	public Date calculateSettlementDate(OrderSecurity security, OrderSecurity settlementCurrency, Date transactionDate);


	/**
	 * Returns the next available settlement date from the given settlement date.
	 * i.e. if Settlement Date = tomorrow, will check the following day's date.  If a valid settlement business day will return that date, if not will move again to the next calendar day
	 * Optional "settlementCurrencyId" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 */
	public Date getOrderSecuritySettlementDateNext(OrderSecurity security, OrderSecurity settlementCurrency, Date settlementDate);


	/**
	 * Returns true if the given date is a valid settlement date for the security.
	 * Optional "settlementCurrency" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 */
	public boolean isValidSettlementDate(OrderSecurity security, OrderSecurity settlementCurrency, Date settlementDate);


	/**
	 * Returns the calendar(s) used for settlement date calculations.  Useful for rule violations
	 */
	public Calendar[] getSettlementCalendarsForSecurityAndSettlementCurrency(OrderSecurity security, OrderSecurity settlementCurrency);


	/**
	 * Used internally whenever trade and settlement dates can be changes to do a quick fail check.  Rules are used for actual validation.
	 * Included in this is that Settlement Date cannot be before Trade Date - that is a hard requirement, the business day checks can be ignored
	 */
	public void validateTradeAndSettlementDateForSecurity(OrderSecurity security, OrderSecurity settlementCurrency, Date tradeDate, Date settlementDate, boolean ignoreValidation);


	public BigDecimal roundCurrencyAmount(OrderSecurity currency, BigDecimal amount);
}
