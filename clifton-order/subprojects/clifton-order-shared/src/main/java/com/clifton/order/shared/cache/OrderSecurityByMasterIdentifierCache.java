package com.clifton.order.shared.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.order.shared.OrderSecurity;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderSecurityByMasterIdentifierCache</code> caches the OrderSecurity by masterIdentifier
 *
 * @author manderson
 */
@Component
public class OrderSecurityByMasterIdentifierCache extends SelfRegisteringSingleKeyDaoCache<OrderSecurity, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "masterIdentifier";
	}


	@Override
	protected Integer getBeanKeyValue(OrderSecurity bean) {
		return bean.getMasterIdentifier();
	}
}
