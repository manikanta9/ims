package com.clifton.order.shared.populator.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.order.shared.OrderCompany;
import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>OrderCompanyPopulatorUploadCommand</code> allows mapping CompanyTags column to actually link companies to their tags.
 *
 * @author manderson
 */
public class OrderCompanyPopulatorUploadCommand extends SystemUploadCommand {

	@Override
	public void applyUploadSpecificProperties(IdentityObject bean, DataRow row) {
		((OrderCompany) bean).setCompanyTags((String) row.getValue("CompanyTags"));
	}
}
