package com.clifton.order.shared.populator;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.order.shared.OrderAccount;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>OrderSharedPopulatorService</code> defines/exposes the methods used to synchronize various shared data points.
 * Some of these can be called on demand or via a batch job
 *
 * @author manderson
 */
public interface OrderSharedPopulatorService {


	////////////////////////////////////////////////////////////////////////////
	////////            Order Shared Object Populators                  ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Used by migrations for the first sync to sync all of the shared look up data (i.e. SystemListItems for States, Countries, Account Types, etc.)
	 * Then all of the Companies
	 * Then all of the accounts
	 * <p>
	 * Runs Synchronously
	 */
	@DoNotAddRequestMapping
	public void populateOrderSharedDataAll();


	@RequestMapping("orderSharedDataListForCommandPopulate.json")
	@SecureMethod(dtoClass = OrderAccount.class, permissions = SecurityPermission.PERMISSION_READ)
	public Status populateOrderSharedDataListForCommand(OrderSharedPopulatorCommand command);
}
