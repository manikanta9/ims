package com.clifton.order.shared.marketdata.cache;

import com.clifton.core.cache.CustomCache;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The ExchangeRateCache caches the Exchange Rate value for the from/to currencies and date.
 * Since we only use this for flexible lookups the cache is only for flexible retrievals
 *
 * @author AbhinayaM
 */
public interface OrderSharedExchangeRateCache extends CustomCache<String, BigDecimal> {

	public BigDecimal getExchangeRateFromCache(String ccyFrom, String ccyTo, Date date);


	public void setExchangeRateInCache(String ccyFrom, String ccyTo, Date date, BigDecimal fxRate);
}
