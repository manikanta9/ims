package com.clifton.order.shared.marketdata.cache;

import com.clifton.core.cache.CustomCache;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;


/**
 * @author manderson
 */
public interface OrderSharedCurrencyConventionCache extends CustomCache<String, CurrencyConventionData> {


	public CurrencyConventionData getCurrencyConventionDataFromCache(String fromCurrencyCode, String toCurrencyCode);


	public void setCurrencyConventionDataInCache(CurrencyConventionData currencyConventionData);
}
