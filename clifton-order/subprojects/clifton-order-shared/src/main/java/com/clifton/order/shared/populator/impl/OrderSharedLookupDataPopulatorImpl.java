package com.clifton.order.shared.populator.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.shared.populator.OrderSharedLookupDataPopulator;
import com.clifton.order.shared.populator.OrderSharedPopulatorCommand;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.upload.SystemUploadCommand;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>OrderSharedLookupDataPopulatorImpl</code> implements the {@link OrderSharedLookupDataPopulator} interface
 * and uploads the shared look up data based on query results.  Note: This is used for more generic tables like System List Items, System Hierarchies.
 * <p>
 * The query results are expected to contain a column in the results under column header "ImportTableName".  Although returned for each row, only the first one is used.
 *
 * @author manderson
 */
@Component
public class OrderSharedLookupDataPopulatorImpl extends BaseOrderSharedPopulatorImpl<IdentityObject> implements OrderSharedLookupDataPopulator {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadOrderSharedLookupData(OrderSharedPopulatorCommand command) {
		List<SystemQuery> queryList = getSystemQueryListByTag(false, false);
		for (SystemQuery query : CollectionUtils.getIterable(queryList)) {
			try {
				command.getStatus().setMessage("Processing Results for Query: " + query.getName());
				DataTable results = getQueryResults(query, command);
				if (results == null || results.getTotalRowCount() == 0) {
					command.getStatus().addMessage(query.getName() + " No Results to process");
					continue;
				}
				SystemUploadCommand uploadCommand = new SystemUploadCommand();
				buildSystemUploadCommand(uploadCommand, command, false, results);
				ValidationUtils.assertNotEmpty(uploadCommand.getTableName(), "Cannot find table name to use for the import for query " + query.getName() + ". Make sure the query results include a column for ImportTableName.");
				uploadCommand.setSmartSave(true);
				if ("SystemHierarchy".equalsIgnoreCase(uploadCommand.getTableName())) {
					uploadCommand.setOverrideTableNaturalKeyBeanProperties(new String[]{"category", "parent", "value"});
				}
				getSystemUploadHandler().uploadSystemUploadFile(uploadCommand);
				if (uploadCommand.getUploadResult().isError()) {
					command.getStatus().addError(query.getName() + " " + uploadCommand.getTableName() + " Results: " + uploadCommand.getUploadResultsString());
				}
				else {
					command.getStatus().addMessage(query.getName() + " " + uploadCommand.getTableName() + " Results: " + uploadCommand.getUploadResultsString());
				}
			}
			catch (Throwable e) {
				command.getStatus().addError("Error importing data for query " + query.getName() + ": " + ExceptionUtils.getDetailedMessage(e));
				LogUtils.errorOrInfo(getClass(), "Error importing data for query " + query.getName() + ": ", e);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getQueryTagName() {
		return "Order Shared Lookup Data Import";
	}


	@Override
	protected String getTableName(DataTable results) {
		if (results != null && results.getTotalRowCount() > 0) {
			return (String) results.getRow(0).getValue("ImportTableName");
		}
		return null;
	}


	@Override
	protected List<SystemQueryParameterValue> populateSystemQueryParameterValueList(OrderSharedPopulatorCommand command, List<SystemQueryParameter> parameterList) {
		List<SystemQueryParameterValue> parameterValueList = new ArrayList<>();
		for (SystemQueryParameter parameter : CollectionUtils.getIterable(parameterList)) {
			String parameterStringValue = null;
			switch (parameter.getName()) {
				case "lastUpdatedAfterDate":
					if (command.getWeekdaysBack() != null) {
						parameterStringValue = DateUtils.fromDateShort(DateUtils.addWeekDays(DateUtils.clearTime(new Date()), -command.getWeekdaysBack()));
					}
					break;
			}
			if (parameterStringValue != null) {
				parameterValueList.add(populateSystemQueryParameterValue(parameter, parameterStringValue));
			}
		}
		return parameterValueList;
	}
}
