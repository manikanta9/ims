package com.clifton.order.shared;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.ObjectWrapperWithBooleanResult;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.api.server.model.*;
import com.clifton.order.shared.search.OrderAccountSearchForm;
import com.clifton.order.shared.search.OrderCompanySearchForm;
import com.clifton.order.shared.search.OrderSecurityExchangeSearchForm;
import com.clifton.order.shared.search.OrderSecuritySearchForm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Getter
@Setter
public class OrderSharedServiceImpl implements OrderSharedService {

	private AdvancedUpdatableDAO<OrderAccount, Criteria> orderAccountDAO;
	private AdvancedUpdatableDAO<OrderCompany, Criteria> orderCompanyDAO;
	private AdvancedUpdatableDAO<OrderSecurity, Criteria> orderSecurityDAO;
	private AdvancedUpdatableDAO<OrderSecurityExchange, Criteria> orderSecurityExchangeDAO;

	private DaoSingleKeyCache<OrderAccount, Integer> orderAccountByMasterIdentifierCache;
	private DaoSingleKeyCache<OrderCompany, Integer> orderCompanyByMasterIdentifierCache;
	private DaoSingleKeyCache<OrderSecurity, Integer> orderSecurityByMasterIdentifierCache;
	private DaoSingleKeyCache<OrderSecurityExchange, String> orderSecurityExchangeByExchangeCodeCache;

	private DaoSingleKeyListCache<OrderSecurity, String> orderSecurityListByTickerCache;

	////////////////////////////////////////////////////////////////////////////
	////////            Order Company Methods                           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderCompany getOrderCompany(int id) {
		return getOrderCompanyDAO().findByPrimaryKey(id);
	}


	private OrderCompany getOrderCompanyByMasterIdentifier(int masterIdentifier) {
		return getOrderCompanyByMasterIdentifierCache().getBeanForKeyValue(getOrderCompanyDAO(), masterIdentifier);
	}


	@Override
	public OrderCompany getOrderCompanyForRetriever(CompanyRetrieverModel retrieverModel) {
		OrderCompany company = null;
		if (retrieverModel != null) {
			if (retrieverModel.getMasterIdentifier() != null) {
				company = getOrderCompanyByMasterIdentifier(retrieverModel.getMasterIdentifier());
				if (company == null) {
					// lOOK IT UP FROM MASTER SYSTEM AND ADD TO OMS
					ValidationUtils.fail("Missing company with master identifier [" + retrieverModel.getMasterIdentifier() + "]");
				}
			}
			else {
				boolean searchApplied = false;
				OrderCompanySearchForm searchForm = new OrderCompanySearchForm();
				if (!StringUtils.isEmpty(retrieverModel.getName())) {
					searchForm.setNameEquals(retrieverModel.getName());
					searchApplied = true;
				}
				if (!StringUtils.isEmpty(retrieverModel.getLegalEntityIdentifier())) {
					searchForm.setLegalEntityIdentifierEquals(retrieverModel.getLegalEntityIdentifier());
					searchApplied = true;
				}
				if (retrieverModel.getBloombergCompanyIdentifier() != null) {
					searchForm.setBloombergCompanyIdentifier(retrieverModel.getBloombergCompanyIdentifier());
					searchApplied = true;
				}
				ValidationUtils.assertTrue(searchApplied, "At least one search property is required to look up the company.");
				List<OrderCompany> companyList = getOrderCompanyList(searchForm);
				company = CollectionUtils.getOnlyElement(companyList);
			}
		}
		return company;
	}


	@Override
	public List<OrderCompany> getOrderCompanyList(OrderCompanySearchForm searchForm) {
		return getOrderCompanyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ObjectWrapperWithBooleanResult<OrderCompany> saveOrderCompany(OrderCompany bean) {
		return getOrderCompanyDAO().saveSmart(bean);
	}


	@Override
	public void deleteOrderCompany(int id) {
		getOrderCompanyDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Order Security Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderSecurity getOrderSecurity(int id) {
		return getOrderSecurityDAO().findByPrimaryKey(id);
	}


	private OrderSecurity getOrderSecurityByMasterIdentifier(int masterIdentifier) {
		return getOrderSecurityByMasterIdentifierCache().getBeanForKeyValue(getOrderSecurityDAO(), masterIdentifier);
	}


	@Override
	public OrderSecurity getOrderSecurityForCurrencyTicker(String ticker) {
		ValidationUtils.assertNotNull(ticker, "Currency ticker cannot be null. It is required in order to look up security.");
		OrderSecurity result = null;
		List<OrderSecurity> resultList = getOrderSecurityListByTicker(ticker);
		if (!CollectionUtils.isEmpty(resultList)) {
			resultList = BeanUtils.filter(resultList, OrderSecurity::isCurrency);
			if (resultList.size() > 1) {
				throw new ValidationException("Found more than one Currency Security for ticker: '" + ticker + "'. Results: " + CollectionUtils.toString(resultList, 5));
			}
			return resultList.get(0);
		}
		return result;
	}


	@Override
	public OrderSecurity getOrderSecurityForRetriever(SecurityRetrieverModel retrieverModel, boolean ignoreIfEmpty) {
		OrderSecurity security = null;
		if (retrieverModel != null) {
			if (retrieverModel.getMasterIdentifier() != null) {
				security = getOrderSecurityByMasterIdentifier(retrieverModel.getMasterIdentifier());
				if (security == null) {
					// lOOK IT UP FROM MASTER SYSTEM AND ADD TO OMS
					ValidationUtils.fail("Missing security with master identifier [" + retrieverModel.getMasterIdentifier() + "]");
				}
			}
			else {
				boolean searchApplied = false;
				OrderSecuritySearchForm searchForm = new OrderSecuritySearchForm();
				if (retrieverModel.getIdentifierType() != null && !StringUtils.isEmpty(retrieverModel.getIdentifierValue())) {
					addSearchRestrictionForSecurityIdentifier(searchForm, retrieverModel.getIdentifierType(), retrieverModel.getIdentifierValue());
					searchApplied = true;
				}
				if (retrieverModel.getIdentifierType2() != null && !StringUtils.isEmpty(retrieverModel.getIdentifierValue2())) {
					addSearchRestrictionForSecurityIdentifier(searchForm, retrieverModel.getIdentifierType2(), retrieverModel.getIdentifierValue2());
					searchApplied = true;
				}
				if (!searchApplied && ignoreIfEmpty) {
					return null;
				}
				ValidationUtils.assertTrue(searchApplied, "At least one identifier value is required to look up the security when not using master identifier.");
				if (!StringUtils.isEmpty(retrieverModel.getSecurityType())) {
					searchForm.setSecurityType(retrieverModel.getSecurityType());
				}
				if (!StringUtils.isEmpty(retrieverModel.getPrimaryExchangeCode())) {
					searchForm.setPrimaryExchangeId(getOrderSecurityExchangeForExchangeCode(retrieverModel.getPrimaryExchangeCode()).getId());
				}

				List<OrderSecurity> securityList = getOrderSecurityList(searchForm);
				security = CollectionUtils.getOnlyElement(securityList);
			}
		}
		ValidationUtils.assertNotNull(security, "Cannot find security for retriever " + retrieverModel);
		return security;
	}


	private void addSearchRestrictionForSecurityIdentifier(OrderSecuritySearchForm searchForm, SecurityIdentifierTypesModel identifierType, String identifierValue) {
		String propertyName;
		switch (identifierType) {
			case BLOOMBERG_GLOBAL_ID:
				propertyName = "bloombergGlobalIdentifier";
				break;
			case ISIN:
				propertyName = "isin";
				break;
			case CUSIP:
				propertyName = "cusip";
				break;
			case SEDOL:
				propertyName = "sedol";
				break;
			case TICKER:
				propertyName = "ticker";
				break;
			case OCC_SYMBOL:
				propertyName = "occSymbol";
				break;
			default:
				throw new ValidationException("Missing property name mapping for identifier type " + identifierType.name());
		}
		searchForm.addSearchRestriction(propertyName, ComparisonConditions.EQUALS, identifierValue);
	}


	@Override
	public List<OrderSecurity> getOrderSecurityList(OrderSecuritySearchForm searchForm) {
		return getOrderSecurityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<OrderSecurity> getOrderSecurityListByTicker(String ticker) {
		return getOrderSecurityListByTickerCache().getBeanListForKeyValue(getOrderSecurityDAO(), ticker);
	}


	@Override
	public ObjectWrapperWithBooleanResult<OrderSecurity> saveOrderSecurity(OrderSecurity bean) {
		return getOrderSecurityDAO().saveSmart(bean);
	}


	@Override
	public void deleteOrderSecurity(int id) {
		// Do Nothing for now
	}

	////////////////////////////////////////////////////////////////////////////
	///////        Order Security Exchange Business Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderSecurityExchange getOrderSecurityExchange(short id) {
		return getOrderSecurityExchangeDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderSecurityExchange getOrderSecurityExchangeForExchangeCode(String exchangeCode) {
		return getOrderSecurityExchangeByExchangeCodeCache().getBeanForKeyValue(getOrderSecurityExchangeDAO(), exchangeCode);
	}


	@Override
	public List<OrderSecurityExchange> getOrderSecurityExchangeList(OrderSecurityExchangeSearchForm searchForm) {
		return getOrderSecurityExchangeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ObjectWrapperWithBooleanResult<OrderSecurityExchange> saveOrderSecurityExchange(OrderSecurityExchange bean) {
		return getOrderSecurityExchangeDAO().saveSmart(bean);
	}


	@Override
	public void deleteOrderSecurityExchange(short id) {
		// DO NOTHING FOR NOW
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Order Account Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderAccount getOrderAccount(int id) {
		return getOrderAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderAccount getOrderAccountByMasterIdentifier(int masterIdentifier) {
		return getOrderAccountByMasterIdentifierCache().getBeanForKeyValue(getOrderAccountDAO(), masterIdentifier);
	}


	@Override
	public OrderAccount getOrderAccountForClientAccountRetriever(ClientAccountRetrieverModel retrieverModel) {
		OrderAccount account = null;
		if (retrieverModel != null) {
			if (retrieverModel.getMasterIdentifier() != null) {
				account = getOrderAccountByMasterIdentifier(retrieverModel.getMasterIdentifier());
				if (account == null) {
					// lOOK IT UP FROM MASTER SYSTEM AND ADD TO OMS
					ValidationUtils.fail("Missing account with master identifier [" + retrieverModel.getMasterIdentifier() + "]");
				}
			}
			else {
				boolean searchApplied = false;
				OrderAccountSearchForm searchForm = new OrderAccountSearchForm();
				searchForm.setClientAccount(true);
				if (!StringUtils.isEmpty(retrieverModel.getAccountNumber())) {
					searchForm.setAccountNumberEquals(retrieverModel.getAccountNumber());
					searchApplied = true;
				}
				if (!StringUtils.isEmpty(retrieverModel.getAccountShortName())) {
					searchForm.setAccountShortNameEquals(retrieverModel.getAccountShortName());
					searchApplied = true;
				}
				ValidationUtils.assertTrue(searchApplied, "At least one search property is required to look up the client account.");
				List<OrderAccount> accountList = getOrderAccountList(searchForm);
				account = CollectionUtils.getOnlyElement(accountList);
			}

			if (account != null) {
				ValidationUtils.assertTrue(account.isClientAccount(), "Invalid Client Account Options selected. Selected account is not a client account.");
			}
		}
		return account;
	}


	@Override
	public OrderAccount getOrderAccountForHoldingAccountRetriever(HoldingAccountRetrieverModel retrieverModel) {
		OrderAccount account = null;
		if (retrieverModel != null) {
			if (retrieverModel.getMasterIdentifier() != null) {
				account = getOrderAccountByMasterIdentifier(retrieverModel.getMasterIdentifier());
				if (account == null) {
					// lOOK IT UP FROM MASTER SYSTEM AND ADD TO OMS
					ValidationUtils.fail("Missing account with master identifier [" + retrieverModel.getMasterIdentifier() + "]");
				}
			}
			else {
				boolean searchApplied = false;
				OrderAccountSearchForm searchForm = new OrderAccountSearchForm();
				searchForm.setHoldingAccount(true);
				if (!StringUtils.isEmpty(retrieverModel.getAccountNumber())) {
					searchForm.setAccountNumberEquals(retrieverModel.getAccountNumber());
					searchApplied = true;
				}
				// Issuing Company
				ValidationUtils.assertTrue(searchApplied, "At least one search property is required to look up the holding account.");
				List<OrderAccount> accountList = getOrderAccountList(searchForm);
				account = CollectionUtils.getOnlyElement(accountList);
			}

			if (account != null) {
				ValidationUtils.assertTrue(account.isHoldingAccount(), "Invalid Holding Account Options selected. Selected account is not a holding account.");
			}
		}
		return account;
	}


	@Override
	public List<OrderAccount> getOrderAccountList(OrderAccountSearchForm searchForm) {
		// Add Default Sorting if not specified
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("accountShortName:asc#accountNumber:asc");
		}
		return getOrderAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ObjectWrapperWithBooleanResult<OrderAccount> saveOrderAccount(OrderAccount bean) {
		return getOrderAccountDAO().saveSmart(bean);
	}


	@Override
	public void deleteOrderAccount(int id) {
		getOrderAccountDAO().delete(id);
	}
}
