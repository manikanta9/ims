package com.clifton.order.shared.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.order.shared.OrderCompany;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderCompanyByMasterIdentifierCache</code> caches the OrderCompany by masterIdentifier
 *
 * @author manderson
 */
@Component
public class OrderCompanyByMasterIdentifierCache extends SelfRegisteringSingleKeyDaoCache<OrderCompany, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "masterIdentifier";
	}


	@Override
	protected Integer getBeanKeyValue(OrderCompany bean) {
		return bean.getMasterIdentifier();
	}
}
