package com.clifton.order.shared.populator.jobs;

import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.status.StatusWithCounts;
import com.clifton.order.shared.populator.OrderSharedPopulatorCommand;
import com.clifton.order.shared.populator.OrderSharedPopulatorService;

import java.util.Map;


/**
 * The Order Shared Populator job that run the synchronization of mastered shared data from master systems to the OMS.
 *
 * @author AbhinayaM
 */
public class OrderSharedPopulatorJob implements Task, StatusHolderObjectAware<StatusWithCounts> {

	private StatusHolderObject<StatusWithCounts> statusHolder;

	private OrderSharedPopulatorService orderSharedPopulatorService;

	private Integer weekdaysBack;

	private boolean skipSharedLookupData;

	private boolean populateAccountData;

	private boolean populateCompanyData;

	private boolean populateSecurityData;


	@Override
	public void setStatusHolderObject(StatusHolderObject<StatusWithCounts> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		OrderSharedPopulatorCommand command = new OrderSharedPopulatorCommand();
		Status status = this.statusHolder.getStatus();
		command.setWeekdaysBack(getWeekdaysBack());
		command.setSkipSharedLookupData(isSkipSharedLookupData());
		command.setPopulateAccountData(isPopulateAccountData());
		command.setPopulateCompanyData(isPopulateCompanyData());
		command.setPopulateSecurityData(isPopulateSecurityData());
		command.setSynchronous(true);
		command.setStatus(StatusWithCounts.ofMessage(status.getMessage()));

		return getOrderSharedPopulatorService().populateOrderSharedDataListForCommand(command);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getters and Setters                             ////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getWeekdaysBack() {
		return this.weekdaysBack;
	}


	public void setWeekdaysBack(Integer weekdaysBack) {
		this.weekdaysBack = weekdaysBack;
	}


	public boolean isSkipSharedLookupData() {
		return this.skipSharedLookupData;
	}


	public void setSkipSharedLookupData(boolean skipSharedLookupData) {
		this.skipSharedLookupData = skipSharedLookupData;
	}


	public boolean isPopulateAccountData() {
		return this.populateAccountData;
	}


	public void setPopulateAccountData(boolean populateAccountData) {
		this.populateAccountData = populateAccountData;
	}


	public boolean isPopulateCompanyData() {
		return this.populateCompanyData;
	}


	public void setPopulateCompanyData(boolean populateCompanyData) {
		this.populateCompanyData = populateCompanyData;
	}


	public boolean isPopulateSecurityData() {
		return this.populateSecurityData;
	}


	public void setPopulateSecurityData(boolean populateSecurityData) {
		this.populateSecurityData = populateSecurityData;
	}


	public StatusHolderObject<StatusWithCounts> getStatusHolder() {
		return this.statusHolder;
	}


	public void setStatusHolder(StatusHolderObject<StatusWithCounts> statusHolder) {
		this.statusHolder = statusHolder;
	}


	public OrderSharedPopulatorService getOrderSharedPopulatorService() {
		return this.orderSharedPopulatorService;
	}


	public void setOrderSharedPopulatorService(OrderSharedPopulatorService orderSharedPopulatorService) {
		this.orderSharedPopulatorService = orderSharedPopulatorService;
	}
}
