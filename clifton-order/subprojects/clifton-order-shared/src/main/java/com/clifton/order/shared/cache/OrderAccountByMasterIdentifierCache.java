package com.clifton.order.shared.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.order.shared.OrderAccount;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderAccountByMasterIdentifierCache</code> caches the OrderAccount by masterIdentifier
 *
 * @author manderson
 */
@Component
public class OrderAccountByMasterIdentifierCache extends SelfRegisteringSingleKeyDaoCache<OrderAccount, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "masterIdentifier";
	}


	@Override
	protected Integer getBeanKeyValue(OrderAccount bean) {
		return bean.getMasterIdentifier();
	}
}
