package com.clifton.order.shared.marketdata;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import com.clifton.exchangerate.api.client.model.ExchangeRateData;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.price.api.client.model.PriceData;

import java.util.Date;


/**
 * @author AbhinayaM
 */
public interface OrderSharedMarketDataService {

	////////////////////////////////////////////////////////////////////////////
	////////           Currency Convention Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderSecurity.class)
	public CurrencyConventionData getOrderMarketDataCurrencyConvention(String fromCurrencyCode, String toCurrencyCode);

	////////////////////////////////////////////////////////////////////////////
	////////               Exchange Rate Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderSecurity.class)
	public ExchangeRateData getOrderMarketDataExchangeRateFlexible(String fromCurrencyCode, String toCurrencyCode, Date date, boolean exceptionIfMissing);

	////////////////////////////////////////////////////////////////////////////
	////////                   Price Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderSecurity.class)
	public PriceData getOrderMarketDataPriceFlexible(int orderSecurityId, Date date, boolean exceptionIfMissing);
}
