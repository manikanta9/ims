package com.clifton.order.shared.group.rebuild;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.search.OrderSecuritySearchForm;
import com.clifton.system.bean.SystemBeanGroup;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.group.SystemGroup;
import com.clifton.system.group.SystemGroupItem;
import com.clifton.system.group.SystemGroupService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author AbhinayaM
 * * The OrderSecurityGroupSearchRebuildExecutor allows for the creation of system defined
 * * Order Security groups using properties from the Order Security via OrderSecuritySearchForm.
 */
@Getter
@Setter
public class OrderSecurityGroupSearchRebuildExecutor implements OrderSecurityGroupRebuildExecutor {

	private OrderSharedService orderSharedService;
	private SystemGroupService systemGroupService;


	private String currencyCode;
	private Integer underlyingSecurityId;
	private Integer companyId;
	private Short primaryOrCompositeExchangeId;
	private Short securityHierarchyOrParentId;
	private Integer securityTypeId;
	private Integer securityTypeSubTypeId;
	private Integer securityTypeSubType2Id;


	@Transactional
	@Override
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status) {

		SystemGroup systemGroup = (SystemGroup) groupEntity;

		List<OrderSecurity> orderSecurities = getRebuildOrderSecurityList();
		List<SystemGroupItem> systemGroupItemList = orderSecurities.stream().map(this::convert).collect(Collectors.toList());
		getSystemGroupService().saveSystemGroupItemList(systemGroup, systemGroupItemList);

		status.setMessage("Completed rebuild of Security Group [%s]; Total Items: [%d]", ((SystemGroup) groupEntity).getName(), CollectionUtils.getSize(orderSecurities));
		return status;
	}


	private SystemGroupItem convert(final OrderSecurity orderSecurity) {
		SystemGroupItem systemGroupItem = new SystemGroupItem();
		// Setting Order Security Id
		systemGroupItem.setFkFieldId(BeanUtils.getIdentityAsLong(orderSecurity));
		return systemGroupItem;
	}


	@Override
	public void validate(EntityGroupRebuildAware groupEntity) {
		ValidationUtils.assertEquals("OrderSecurity", groupEntity.getRebuildTableName(), "Rebuild executor applies to Order Securities only");
		ValidationUtils.assertTrue(groupEntity instanceof SystemBeanGroup, "Rebuild executor applies to SystemBeanGroup only.");
	}


	private List<OrderSecurity> getRebuildOrderSecurityList() {
		List<OrderSecurity> rebuildOrderSecurityList;
		OrderSecuritySearchForm orderSecuritySearchForm = new OrderSecuritySearchForm();
		orderSecuritySearchForm.setCurrencyCode(getCurrencyCode());
		orderSecuritySearchForm.setUnderlyingSecurityId(getUnderlyingSecurityId());
		orderSecuritySearchForm.setCompanyId(getCompanyId());
		orderSecuritySearchForm.setPrimaryOrCompositeExchangeId(getPrimaryOrCompositeExchangeId());
		orderSecuritySearchForm.setSecurityHierarchyOrParentId(getSecurityHierarchyOrParentId());
		orderSecuritySearchForm.setSecurityTypeId(getSecurityTypeId());
		orderSecuritySearchForm.setSecurityTypeSubTypeId(getSecurityTypeSubTypeId());
		orderSecuritySearchForm.setSecurityTypeSubType2Id(getSecurityTypeSubType2Id());
		rebuildOrderSecurityList = getOrderSharedService().getOrderSecurityList(orderSecuritySearchForm);
		return CollectionUtils.asNonNullList(rebuildOrderSecurityList);
	}
}
