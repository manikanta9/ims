package com.clifton.order.shared.group.rebuild;

import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;


/**
 * @author AbhinayaM
 */
public interface OrderSecurityGroupRebuildExecutor extends EntityGroupRebuildAwareExecutor {
}
