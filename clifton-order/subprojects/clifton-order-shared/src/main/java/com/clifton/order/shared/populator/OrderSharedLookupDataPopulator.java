package com.clifton.order.shared.populator;

/**
 * The <code>OrderSharedLookupDataPopulator</code> interface is used for shared lookup tables that can upload data directly
 * into OMS. These are generic tables (i.e. System Lists, System Hierarchy)
 * Note: There is only one and it's expected to handle all look up data.  Should be re-evaluated once we have actual mastered data.
 *
 * @author manderson
 */
public interface OrderSharedLookupDataPopulator {

	public void uploadOrderSharedLookupData(OrderSharedPopulatorCommand command);
}
