package com.clifton.order.shared;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarTimeZone;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;


/**
 * @author manderson
 */
@Component
public class OrderSharedUtilHandlerImpl implements OrderSharedUtilHandler {

	private CalendarBusinessDayService calendarBusinessDayService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Note: Changes to this method should also be reviewed/applied to InvestmentCalculatorImpl.calculateTradeDate
	 * Applies the same logic, however uses different security objects/structure
	 */
	@Override
	public Date calculateTradeDate(OrderSecurity security, Date transactionDate) {
		if (transactionDate == null) {
			transactionDate = new Date();
		}
		Date tradeDate = transactionDate;
		// No exchange - Use today
		OrderSecurityExchange exchange = security.getPrimaryExchange();
		if (exchange != null) {
			Calendar calendar = exchange.getCalendar();
			if (calendar != null) {
				CalendarTimeZone timeZone = exchange.getTimeZone();
				if (timeZone != null) {
					// Find out what day it is on the exchange
					tradeDate = DateUtils.getDateInTimeZone(tradeDate, timeZone.getName());
				}
				// Is "today" a business day?
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forTrade(tradeDate, calendar.getId()))) {
					// If not, return next business day
					tradeDate = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forTrade(tradeDate, calendar.getId()));
				}
				else {
					// Otherwise check against time
					Time closingTime = exchange.getCloseTime();

					// If it is a business day - is it a PARTIAL business day?
					// Only call this if the exchange has different hours for partial or full day
					if (exchange.isPartialDayHoursDefinedAndDifferent() && getCalendarBusinessDayService().isBusinessDayPartial(CalendarBusinessDayCommand.forTrade(tradeDate, calendar.getId()))) {
						closingTime = exchange.getCloseTimePartialDay();
					}

					// Otherwise check against time
					if (closingTime != null) {
						int currentTime = DateUtils.getMillisecondsFromMidnight(tradeDate);
						// If after closing time - move to next business day
						if (closingTime.getMilliseconds() < currentTime) {
							tradeDate = getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(tradeDate, calendar.getId()));
						}
					}
				}
			}
		}
		return DateUtils.clearTime(tradeDate);
	}


	@Override
	public Date getOrderSecurityTradeDateNext(OrderSecurity security, Date tradeDate) {
		Date newTradeDate = DateUtils.addDays(tradeDate, 1);
		int counter = 0;
		while (counter < 100 && !isValidTradeDate(security, newTradeDate)) {
			newTradeDate = DateUtils.addDays(newTradeDate, 1);
			counter++;
		}

		if (counter >= 100) {
			throw new IllegalStateException("Cannot determine a valid next trade date after attempting to move through 100 days for security [" + security.getLabel() + "] and original trade date [" + DateUtils.fromDateShort(tradeDate) + "].  Last date checked: " + DateUtils.fromDateShort(newTradeDate) + ".");
		}

		return newTradeDate;
	}


	@Override
	public boolean isValidTradeDate(OrderSecurity security, Date tradeDate) {
		OrderSecurityExchange exchange = (security != null ? security.getPrimaryExchange() : null);
		if (exchange != null) {
			Calendar calendar = exchange.getCalendar();
			if (calendar != null) {
				return getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forTrade(tradeDate, calendar.getId()));
			}
		}
		// No exchange or calendar, then is a valid trade date
		return true;
	}


	/**
	 * Note: Changes to this method should also be reviewed/applied to InvestmentCalculatorImpl.calculateSettlementDate
	 * Applies the same logic, however uses different security objects/structure
	 */
	@Override
	public Date calculateSettlementDate(OrderSecurity security, OrderSecurity settlementCurrency, Date transactionDate) {
		ValidationUtils.assertNotNull(transactionDate, "Transaction date is required from which settlement date is calculated", "tradeDate");
		short daysToSettle = security.getDaysToSettle() == null ? 0 : security.getDaysToSettle();

		Calendar[] calendars = getSettlementCalendarsForSecurityAndSettlementCurrency(security, settlementCurrency);
		// If days to settle = 0 and date is NOT a business day - need to move forward an extra day
		// This happens if a security can be traded today, but couldn't settle today
		if (daysToSettle == 0 && !ArrayUtils.isEmpty(calendars)) {
			for (Calendar cal : calendars) {
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forSettlement(transactionDate, cal.getId()))) {
					daysToSettle++;
					break;
				}
			}
		}

		Date result = transactionDate;
		if (ArrayUtils.isEmpty(calendars)) {
			result = DateUtils.addWeekDays(result, daysToSettle);
		}
		else if (calendars.length == 1) {
			result = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forSettlement(transactionDate, calendars[0].getId()), daysToSettle);
		}
		else {
			result = getCalendarBusinessDayService().getBusinessDayFrom(transactionDate, daysToSettle, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, calendars);
		}

		// cannot settle a trade before Issue Date for a bond
		if (security.getStartDate() != null && DateUtils.compare(security.getStartDate(), result, false) > 0) {
			result = security.getStartDate();
		}
		return result;
	}


	@Override
	public Date getOrderSecuritySettlementDateNext(OrderSecurity security, OrderSecurity settlementCurrency, Date settlementDate) {
		Date newSettlementDate = DateUtils.addDays(settlementDate, 1);
		int counter = 0;
		while (counter < 100 && !isValidSettlementDate(security, settlementCurrency, newSettlementDate)) {
			newSettlementDate = DateUtils.addDays(newSettlementDate, 1);
			counter++;
		}
		if (counter >= 100) {
			throw new IllegalStateException("Cannot determine a valid next settlement date after attempting to move through 100 days for security [" + security.getLabel() + "]" + (settlementCurrency == null ? "" : " and settlement currency [" + settlementCurrency.getLabel() + "]") + " and original settlement date [" + DateUtils.fromDateShort(settlementDate) + "].  Last date checked: " + DateUtils.fromDateShort(newSettlementDate) + ".");
		}
		return newSettlementDate;
	}


	@Override
	public boolean isValidSettlementDate(OrderSecurity security, OrderSecurity settlementCurrency, Date settlementDate) {
		Calendar[] calendars = getSettlementCalendarsForSecurityAndSettlementCurrency(security, settlementCurrency);
		if (!ArrayUtils.isEmpty(calendars)) {
			for (Calendar cal : calendars) {
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forSettlement(settlementDate, cal.getId()))) {
					return false;
				}
			}
		}
		return DateUtils.isWeekday(settlementDate);
	}


	@Override
	public Calendar[] getSettlementCalendarsForSecurityAndSettlementCurrency(OrderSecurity security, OrderSecurity settlementCurrency) {
		Calendar calendar = (security != null) ? security.getSettlementCalendar() : null;
		Calendar settlementCalendar = (settlementCurrency != null) ? settlementCurrency.getSettlementCalendar() : null;
		return ArrayUtils.getStream(calendar, settlementCalendar).filter(Objects::nonNull).toArray(Calendar[]::new);
	}


	@Override
	public void validateTradeAndSettlementDateForSecurity(OrderSecurity security, OrderSecurity settlementCurrency, Date tradeDate, Date settlementDate, boolean ignoreValidation) {
		// Should not be user ignorable
		if (DateUtils.isDateBefore(settlementDate, tradeDate, false)) {
			throw new ValidationException("Settlement Date cannot be before trade date.");
		}

		if (!ignoreValidation) {
			if (!isValidTradeDate(security, tradeDate)) {
				throw new UserIgnorableValidationException("Trade Date [" + DateUtils.fromDateShort(tradeDate) + "] is not a valid trade date for security " + security.getLabel());
			}
			if (!isValidSettlementDate(security, settlementCurrency, settlementDate)) {
				throw new UserIgnorableValidationException("Settlement Date [" + DateUtils.fromDateShort(settlementDate) + "] is not a valid settlement date for order security " + security.getLabel() + (settlementCurrency != null ? " and settlement currency " + settlementCurrency.getLabel() : ""));
			}
		}
	}


	@Override
	public BigDecimal roundCurrencyAmount(OrderSecurity currency, BigDecimal amount) {
		InvestmentNotionalCalculatorTypes calculator = ObjectUtils.coalesce(currency.getCostCalculator(), InvestmentNotionalCalculatorTypes.getDefaultCalculatorType());
		return calculator.round(amount);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
