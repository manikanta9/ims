package com.clifton.order.shared.marketdata.cache;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.util.StringUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author AbhinayaM
 */
@Component
@Getter
@Setter
public class OrderSharedExchangeRateCacheImpl implements OrderSharedExchangeRateCache {


	private CacheHandler<String, BigDecimal> cacheHandler;


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getExchangeRateFromCache(String ccyFrom, String ccyTo, Date date) {
		return getCacheHandler().get(getCacheName(), getBeanKey(ccyFrom, ccyTo, date));
	}


	@Override
	public void setExchangeRateInCache(String ccyFrom, String ccyTo, Date date, BigDecimal fxRate) {
		getCacheHandler().put(getCacheName(), getBeanKey(ccyFrom, ccyTo, date), fxRate);
	}


	private String getBeanKey(String ccyFrom, String ccyTo, Date date) {
		return StringUtils.generateKey(ccyFrom, ccyTo, date);
	}
}
