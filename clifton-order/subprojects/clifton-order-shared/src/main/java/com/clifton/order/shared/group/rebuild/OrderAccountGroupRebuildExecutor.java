package com.clifton.order.shared.group.rebuild;

import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;


/**
 * @author AbhinayaM
 * /**
 * * The <code>OrderAccountGroupRebuildExecutor</code> interface defines the methods for rebuilding {@link com.clifton.order.shared.OrderAccount}
 * *
 */
public interface OrderAccountGroupRebuildExecutor extends EntityGroupRebuildAwareExecutor {

}
