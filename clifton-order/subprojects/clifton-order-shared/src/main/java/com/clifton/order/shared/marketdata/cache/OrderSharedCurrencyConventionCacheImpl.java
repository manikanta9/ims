package com.clifton.order.shared.marketdata.cache;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.util.StringUtils;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderSharedCurrencyConventionCacheImpl implements OrderSharedCurrencyConventionCache {


	private CacheHandler<String, CurrencyConventionData> cacheHandler;


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CurrencyConventionData getCurrencyConventionDataFromCache(String fromCurrencyCode, String toCurrencyCode) {
		return getCacheHandler().get(getCacheName(), getBeanKey(fromCurrencyCode, toCurrencyCode));
	}


	@Override
	public void setCurrencyConventionDataInCache(CurrencyConventionData currencyConventionData) {
		getCacheHandler().put(getCacheName(), getBeanKey(currencyConventionData.getFromCurrencyCode(), currencyConventionData.getToCurrencyCode()), currencyConventionData);
	}


	private String getBeanKey(String fromCurrencyCode, String toCurrencyCode) {
		return StringUtils.generateKey(fromCurrencyCode, toCurrencyCode);
	}
}
