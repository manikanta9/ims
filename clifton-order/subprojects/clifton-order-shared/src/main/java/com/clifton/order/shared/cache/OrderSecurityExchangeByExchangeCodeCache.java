package com.clifton.order.shared.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.order.shared.OrderSecurityExchange;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderSecurityExchangeByExchangeCodeCache</code> caches the OrderSecurityExchange by exchangeCode
 *
 * @author manderson
 */
@Component
public class OrderSecurityExchangeByExchangeCodeCache extends SelfRegisteringSingleKeyDaoCache<OrderSecurityExchange, String> {

	@Override
	protected String getBeanKeyProperty() {
		return "exchangeCode";
	}


	@Override
	protected String getBeanKeyValue(OrderSecurityExchange bean) {
		return bean.getExchangeCode();
	}
}
