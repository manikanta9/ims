package com.clifton.order.shared.populator.impl;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.populator.OrderSharedDataPopulator;
import com.clifton.order.shared.populator.OrderSharedPopulatorCommand;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>OrderCompanyPopulator</code> implements the {@link OrderSharedDataPopulator} interface
 * and creates {@link com.clifton.order.shared.OrderCompany} objects based on company data from a query execution
 * which the calling method can review and save
 *
 * @author manderson
 */
@Component
public class OrderCompanyPopulatorImpl extends BaseOrderSharedPopulatorImpl<OrderCompany> implements OrderSharedDataPopulator<OrderCompany> {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderCompany> populateOrderSharedDataList(OrderSharedPopulatorCommand command) {
		SystemQuery query = CollectionUtils.getFirstElementStrict(getSystemQueryListByTag(true, true));
		command.getStatus().setMessage("Processing Results for Query: " + query.getName());
		DataTable results = getQueryResults(query, command);
		OrderCompanyPopulatorUploadCommand uploadCommand = new OrderCompanyPopulatorUploadCommand();
		buildSystemUploadCommand(uploadCommand, command, true, results);
		uploadCommand.setOverrideTableNaturalKeyBeanProperties(new String[]{"masterIdentifier"});
		return convertDataTableToBeanList(command, uploadCommand);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getQueryTagName() {
		return "Order Company Data Import";
	}


	@Override
	protected String getTableName(DataTable results) {
		return "OrderCompany";
	}


	@Override
	protected List<SystemQueryParameterValue> populateSystemQueryParameterValueList(OrderSharedPopulatorCommand command, List<SystemQueryParameter> parameterList) {
		List<SystemQueryParameterValue> parameterValueList = new ArrayList<>();
		for (SystemQueryParameter parameter : CollectionUtils.getIterable(parameterList)) {
			String parameterStringValue = null;
			switch (parameter.getName()) {
				case "masterIdentifier":
					parameterStringValue = (command.getCompanyMasterIdentifier() == null ? null : command.getCompanyMasterIdentifier() + "");
					break;
				case "companyName":
					parameterStringValue = command.getCompanyName();
					break;
				case "lastUpdatedAfterDate":
					if (StringUtils.isEmpty(command.getCompanyName()) && command.getCompanyMasterIdentifier() == null && command.getWeekdaysBack() != null) {
						parameterStringValue = DateUtils.fromDateShort(DateUtils.addWeekDays(DateUtils.clearTime(new Date()), -command.getWeekdaysBack()));
					}
					break;
			}
			if (parameterStringValue != null) {
				parameterValueList.add(populateSystemQueryParameterValue(parameter, parameterStringValue));
			}
		}
		return parameterValueList;
	}
}
