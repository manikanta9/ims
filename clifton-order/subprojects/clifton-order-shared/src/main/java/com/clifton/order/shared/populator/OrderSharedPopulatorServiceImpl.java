package com.clifton.order.shared.populator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.ObjectWrapperWithBooleanResult;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.status.StatusWithCounts;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.system.hierarchy.definition.search.SystemHierarchySearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderSharedPopulatorServiceImpl implements OrderSharedPopulatorService {

	private static final String RUNNER_TYPE = "ORDER-SHARED-DATA-POPULATE";

	private OrderSharedDataPopulator<OrderAccount> orderAccountPopulator;
	private OrderSharedDataPopulator<OrderCompany> orderCompanyPopulator;
	private OrderSharedDataPopulator<OrderSecurity> orderSecurityPopulator;
	private OrderSharedLookupDataPopulator orderSharedLookupDataPopulator;

	private OrderSharedService orderSharedService;

	private SecurityImpersonationHandler securityImpersonationHandler;

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////            Order Shared Object Populators                  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void populateOrderSharedDataAll() {
		OrderSharedPopulatorCommand command = new OrderSharedPopulatorCommand();
		command.setPopulateAccountData(true);
		command.setPopulateCompanyData(true);
		command.setPopulateSecurityData(true);
		command.setStatus(new StatusWithCounts());

		populateOrderSharedLookupData(command);
		System.out.println(command.getStatus().getMessageWithErrors());

		command.setStatus(new StatusWithCounts());
		populateOrderCompanyData(command);
		System.out.println(command.getStatus().getMessageWithErrors());

		command.setStatus(new StatusWithCounts());
		populateOrderAccountData(command);
		System.out.println(command.getStatus().getMessageWithErrors());

		command.setStatus(new StatusWithCounts());
		populateOrderSecurityData(command);
		System.out.println(command.getStatus().getMessageWithErrors());
	}


	@Override
	public Status populateOrderSharedDataListForCommand(OrderSharedPopulatorCommand command) {
		if (command.isSynchronous()) {
			Status fullStatus = Status.ofMessage("Starting processing");
			populateOrderSharedLookupData(command);
			fullStatus.setMessage(command.getStatus().getMessage() + StringUtils.NEW_LINE);
			populateOrderCompanyData(command);
			fullStatus.setMessage(fullStatus.getMessage() + StringUtils.NEW_LINE + "Order Company Status: " + command.getStatus().getCountMessage("Companies") + StringUtils.NEW_LINE);
			populateOrderAccountData(command);
			fullStatus.setMessage(fullStatus.getMessage() + StringUtils.NEW_LINE + "Order Account Status: " + command.getStatus().getCountMessage("Accounts") + StringUtils.NEW_LINE);
			populateOrderSecurityData(command);
			fullStatus.setMessage(fullStatus.getMessage() + StringUtils.NEW_LINE + "Order Security Status: " + command.getStatus().getCountMessage("Securities") + StringUtils.NEW_LINE);
			return fullStatus;
		}

		scheduleOrderSharedLookupDataPopulation(command);
		return Status.ofMessage("Started Order Shared Data Sync. Processing will be completed shortly.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////        Order Shared Lookup Data Populator Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	protected void scheduleOrderSharedLookupDataPopulation(OrderSharedPopulatorCommand command) {
		if (command.isSkipSharedLookupData()) {
			scheduleOrderCompanyDataPopulation(command);
			return;
		}
		// asynchronous run support
		final String runId = command.getLookupDataRunId();
		final Date now = new Date();

		if (command.getStatus() == null) {
			command.setStatus(StatusWithCounts.ofMessage("Starting lookup data sync."));
		}

		final StatusHolder statusHolder = new StatusHolder(command.getStatus());
		Runner runner = new AbstractStatusAwareRunner(RUNNER_TYPE, runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					populateOrderSharedLookupData(command);
					command.setStatus(new StatusWithCounts());
					scheduleOrderCompanyDataPopulation(command);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error synchronizing lookup data for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error synchronizing lookup data for " + runId, e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}


	protected void populateOrderSharedLookupData(OrderSharedPopulatorCommand command) {
		if (!command.isSkipSharedLookupData()) {
			getSecurityImpersonationHandler().runAsSecurityUserName(SecurityUser.SYSTEM_USER, () ->
					getOrderSharedLookupDataPopulator().uploadOrderSharedLookupData(command));
			command.getStatus().setMessageWithErrors("Shared Look up Data Processing Complete. Errors: " + command.getStatus().getErrorCount(), 5);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Order Company Data Populator Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	protected void scheduleOrderCompanyDataPopulation(OrderSharedPopulatorCommand command) {
		if (!command.isPopulateCompanyData()) {
			scheduleOrderAccountDataPopulation(command);
			return;
		}
		// asynchronous run support
		final String runId = command.getCompanyRunId();
		final Date now = new Date();

		if (command.getStatus() == null) {
			command.setStatus(StatusWithCounts.ofMessage("Starting company data sync."));
		}

		final StatusHolder statusHolder = new StatusHolder(command.getStatus());
		Runner runner = new AbstractStatusAwareRunner(RUNNER_TYPE, runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					populateOrderCompanyData(command);
					command.setStatus(new StatusWithCounts());
					scheduleOrderAccountDataPopulation(command);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error synchronizing company data for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error synchronizing company data for " + runId, e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}


	protected void populateOrderCompanyData(OrderSharedPopulatorCommand command) {
		if (!command.isPopulateCompanyData()) {
			command.getStatus().setMessage("Skipping.  Option to sync Company Data is not selected.");
			return;
		}

		getSecurityImpersonationHandler().runAsSecurityUserName(SecurityUser.SYSTEM_USER, () -> {
			List<OrderCompany> orderCompanyList = getOrderCompanyPopulator().populateOrderSharedDataList(command);

			SystemHierarchySearchForm searchForm = new SystemHierarchySearchForm();
			searchForm.setCategoryName(OrderCompany.COMPANY_TAGS_HIERARCHY_CATEGORY_NAME);
			List<SystemHierarchy> companyTagList = getSystemHierarchyDefinitionService().getSystemHierarchyList(searchForm);

			int totalCount = CollectionUtils.getSize(orderCompanyList);

			// Dependencies due to parents, obligor and parent obligor so need to do these in proper order
			Map<Integer, OrderCompany> masterIdentifierCompanyMap = BeanUtils.getBeanMap(orderCompanyList, OrderCompany::getMasterIdentifier);
			Set<Integer> masterIdentifierSaveSet = new HashSet<>();

			for (OrderCompany orderCompany : CollectionUtils.getIterable(orderCompanyList)) {
				try {
					command.getStatus().incrementProcessCount();
					command.getStatus().setMessage("Processing " + command.getStatus().getProcessCount() + " of " + totalCount + " Companies.");

					// Already Saved
					if (masterIdentifierSaveSet.contains(orderCompany.getMasterIdentifier())) {
						continue;
					}

					saveOrderCompanyImpl(orderCompany, command, masterIdentifierCompanyMap, masterIdentifierSaveSet, companyTagList);
				}
				catch (Throwable e) {
					String errorMessage = "Error processing company " + orderCompany.getLabel() + ": " + ExceptionUtils.getOriginalMessage(e);
					command.getStatus().addError(errorMessage);
					LogUtils.errorOrInfo(getClass(), errorMessage, e);
				}
			}
			command.getStatus().setActionPerformed(command.getStatus().getInsertCount() > 0 || command.getStatus().getUpdateCount() > 0);
			command.getStatus().setMessageWithErrors("Processing Complete. " + command.getStatus().getCountMessage("Companies "), 5);
		});
	}


	private OrderCompany saveOrderCompanyImpl(OrderCompany orderCompany, OrderSharedPopulatorCommand command, Map<Integer, OrderCompany> masterIdentifierCompanyMap, Set<Integer> masterIdentifierSaveSet, List<SystemHierarchy> companyTagList) {
		if (orderCompany.getParent() != null && orderCompany.getParent().isNewBean()) {
			if (masterIdentifierSaveSet.contains(orderCompany.getParent().getMasterIdentifier())) {
				orderCompany.setParent(masterIdentifierCompanyMap.get(orderCompany.getParent().getMasterIdentifier()));
			}
			else {
				orderCompany.setParent(saveOrderCompanyImpl(masterIdentifierCompanyMap.get(orderCompany.getParent().getMasterIdentifier()), command, masterIdentifierCompanyMap, masterIdentifierSaveSet, companyTagList));
			}
		}
		// Note: these are not currently included in the upload file as IMS currently has some circular dependencies between parents and obligors
		if (orderCompany.getObligorCompany() != null && orderCompany.getObligorCompany().isNewBean()) {
			if (masterIdentifierSaveSet.contains(orderCompany.getObligorCompany().getMasterIdentifier())) {
				orderCompany.setObligorCompany(masterIdentifierCompanyMap.get(orderCompany.getObligorCompany().getMasterIdentifier()));
			}
			else {
				orderCompany.setObligorCompany(saveOrderCompanyImpl(masterIdentifierCompanyMap.get(orderCompany.getObligorCompany().getMasterIdentifier()), command, masterIdentifierCompanyMap, masterIdentifierSaveSet, companyTagList));
			}
		}
		masterIdentifierSaveSet.add(orderCompany.getMasterIdentifier());

		String companyTags = orderCompany.getCompanyTags();
		// Clear so we don't trigger a save
		orderCompany.setCompanyTags(null);
		List<String> tags = CollectionUtils.asNonNullList(StringUtils.isEmpty(companyTags) ? null : StringUtils.delimitedStringToList(companyTags, ","));

		boolean insert = orderCompany.isNewBean();
		ObjectWrapperWithBooleanResult<OrderCompany> result = getOrderSharedService().saveOrderCompany(orderCompany);
		masterIdentifierCompanyMap.put(result.getObject().getMasterIdentifier(), result.getObject());

		boolean saveTags = false;
		List<SystemHierarchyLink> existingTags = (insert ? null : getSystemHierarchyAssignmentService().getSystemHierarchyLinkList(OrderCompany.TABLE_NAME, result.getObject().getId(), OrderCompany.COMPANY_TAGS_HIERARCHY_CATEGORY_NAME));
		if (!CollectionUtils.isEmpty(tags)) {
			List<String> saveTagList = new ArrayList<>();
			for (SystemHierarchyLink link : CollectionUtils.getIterable(existingTags)) {
				if (tags.contains(link.getHierarchy().getName())) {
					saveTagList.add(link.getHierarchy().getName());
					tags.remove(link.getHierarchy().getName());
					continue;
				}
				saveTags = true;
			}
			saveTagList.addAll(tags);
			saveTags = saveTags ? saveTags : !CollectionUtils.isEmpty(tags);
			if (saveTags) {
				getSystemHierarchyAssignmentService().saveSystemHierarchyLinkList(OrderCompany.TABLE_NAME, result.getObject().getId(), StringUtils.collectionToDelimitedString(BeanUtils.getBeanIdentityList(BeanUtils.filter(companyTagList, companyTag -> saveTagList.contains(companyTag.getName()))), ","), OrderCompany.COMPANY_TAGS_HIERARCHY_CATEGORY_NAME);
			}
		}
		else if (!CollectionUtils.isEmpty(existingTags)) {
			saveTags = true;
			CollectionUtils.getStream(existingTags).forEach(existingTag -> getSystemHierarchyAssignmentService().deleteSystemHierarchyLink(existingTag.getId()));
		}

		if (result.isResult() || saveTags) {
			command.getStatus().incrementSaveCountWithMessage(insert, (insert ? "Inserted Company: " : "Updated Company: ") + result.getObject().getLabel() + (saveTags ? "; Updated tags: " + StringUtils.coalesce(true, companyTags, "None") : ""));
		}
		else {
			command.getStatus().incrementSkipCount();
		}
		return result.getObject();
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Order Account Data Populator Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	protected void scheduleOrderAccountDataPopulation(OrderSharedPopulatorCommand command) {
		if (!command.isPopulateAccountData()) {
			scheduleOrderSecurityDataPopulation(command);
			return;
		}
		// asynchronous run support
		final String runId = command.getAccountRunId();
		final Date now = new Date();

		if (command.getStatus() == null) {
			command.setStatus(StatusWithCounts.ofMessage("Starting account data sync."));
		}

		final StatusHolder statusHolder = new StatusHolder(command.getStatus());
		Runner runner = new AbstractStatusAwareRunner(RUNNER_TYPE, runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					populateOrderAccountData(command);
					command.setStatus(new StatusWithCounts());
					scheduleOrderSecurityDataPopulation(command);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error synchronizing account data for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error synchronizing account data for " + runId, e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}


	protected void populateOrderAccountData(OrderSharedPopulatorCommand command) {
		if (!command.isPopulateAccountData()) {
			command.getStatus().setMessage(("Skipping.  Option to sync Account Data is not selected."));
			return;
		}

		getSecurityImpersonationHandler().runAsSecurityUserName(SecurityUser.SYSTEM_USER, () -> {
			List<OrderAccount> orderAccountList = getOrderAccountPopulator().populateOrderSharedDataList(command);
			SystemHierarchy clientTag = getSystemHierarchyDefinitionService().getSystemHierarchyByCategoryAndNameExpanded(OrderCompany.COMPANY_TAGS_HIERARCHY_CATEGORY_NAME, OrderCompany.COMPANY_TAGS_CLIENT_HIERARCHY_NAME);
			int totalCount = CollectionUtils.getSize(orderAccountList);

			for (OrderAccount orderAccount : CollectionUtils.getIterable(orderAccountList)) {
				try {
					command.getStatus().incrementProcessCount();
					command.getStatus().setMessage("Processing " + command.getStatus().getProcessCount() + " of " + totalCount + " Accounts.");
					if (orderAccount.getIssuingCompany() == null || orderAccount.getIssuingCompany().isNewBean()) {
						throw new ValidationException("Unable to find correct issuing company for account " + orderAccount.getLabelShort() + ". Please confirm mapping.");
					}
					if (orderAccount.getPortfolioManagerUser() != null && orderAccount.getPortfolioManagerUser().isNewBean()) {
						// Still include the account but add a warning so the user can be added (or PM updated)
						command.getStatus().addWarning("Account " + orderAccount.getAccountShortName() + ": Missing Portfolio Manager User [" + orderAccount.getPortfolioManagerUser().getUserName() + "].");
						orderAccount.setPortfolioManagerUser(null);
					}
					boolean insert = orderAccount.isNewBean();
					OrderCompany clientCompany = orderAccount.getClientCompany();
					if (!insert && clientCompany.isNewBean()) {
						// Name change (so comes in as new bean), just copy id and rv to new value so we update name and properties
						OrderAccount existingAccount = getOrderSharedService().getOrderAccount(orderAccount.getId());
						OrderCompany company = existingAccount.getClientCompany();
						clientCompany.setId(company.getId());
						clientCompany.setRv(company.getRv());
					}
					boolean addTag = clientCompany.isNewBean();
					clientCompany = getOrderSharedService().saveOrderCompany(clientCompany).getObject();
					if (addTag && clientTag != null) {
						getSystemHierarchyAssignmentService().saveSystemHierarchyLink(OrderCompany.TABLE_NAME, clientCompany.getId(), clientTag.getId());
					}
					orderAccount.setClientCompany(clientCompany);
					ObjectWrapperWithBooleanResult<OrderAccount> saveResult = getOrderSharedService().saveOrderAccount(orderAccount);
					orderAccount = saveResult.getObject();
					if (saveResult.isResult()) {
						command.getStatus().incrementSaveCountWithMessage(insert, (insert ? "Inserted Account: " : "Updated Account: ") + orderAccount.getLabelShort());
					}
					else {
						command.getStatus().incrementSkipCount();
					}
				}
				catch (Throwable e) {
					String errorMessage = "Error processing account " + orderAccount.getLabelShort() + ": " + ExceptionUtils.getOriginalMessage(e);
					command.getStatus().addError(errorMessage);
					LogUtils.errorOrInfo(getClass(), errorMessage, e);
				}
			}
			command.getStatus().setActionPerformed(command.getStatus().getInsertCount() > 0 || command.getStatus().getUpdateCount() > 0);
			command.getStatus().setMessageWithErrors("Processing Complete. " + command.getStatus().getCountMessage("Accounts "), 5);
		});
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Order Security Data Populator Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	protected void scheduleOrderSecurityDataPopulation(OrderSharedPopulatorCommand command) {
		if (!command.isPopulateSecurityData()) {
			return;
		}
		// asynchronous run support
		final String runId = command.getSecurityRunId();
		final Date now = new Date();

		if (command.getStatus() == null) {
			command.setStatus(StatusWithCounts.ofMessage("Starting security data sync."));
		}

		final StatusHolder statusHolder = new StatusHolder(command.getStatus());
		Runner runner = new AbstractStatusAwareRunner(RUNNER_TYPE, runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					populateOrderSecurityData(command);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error synchronizing security data for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error synchronizing security data for " + runId, e);
				}
			}
		};
		getRunnerHandler().rescheduleRunner(runner);
	}


	protected void populateOrderSecurityData(OrderSharedPopulatorCommand command) {
		if (!command.isPopulateSecurityData()) {
			command.getStatus().setMessage("Skipping.  Option to sync Security Data is not selected.");
			return;
		}

		getSecurityImpersonationHandler().runAsSecurityUserName(SecurityUser.SYSTEM_USER, () -> {
			List<OrderSecurity> orderSecurityList = getOrderSecurityPopulator().populateOrderSharedDataList(command);

			// Dependencies due to underlying, so need to do these in proper order
			Map<Integer, OrderSecurity> masterIdentifierSecurityMap = BeanUtils.getBeanMap(orderSecurityList, OrderSecurity::getMasterIdentifier);
			Set<Integer> masterIdentifierSaveSet = new HashSet<>();

			int totalCount = CollectionUtils.getSize(orderSecurityList);

			for (OrderSecurity orderSecurity : CollectionUtils.getIterable(orderSecurityList)) {
				try {
					command.getStatus().incrementProcessCount();
					command.getStatus().setMessage("Processing " + command.getStatus().getProcessCount() + " of " + totalCount + " Securities.");

					// Already Saved
					if (masterIdentifierSaveSet.contains(orderSecurity.getMasterIdentifier())) {
						continue;
					}

					saveOrderSecurityImpl(orderSecurity, command, masterIdentifierSecurityMap, masterIdentifierSaveSet);
				}
				catch (Throwable e) {
					String errorMessage = "Error processing security " + orderSecurity.getLabel() + ": " + ExceptionUtils.getOriginalMessage(e);
					command.getStatus().addError(errorMessage);
					LogUtils.errorOrInfo(getClass(), errorMessage, e);
				}
			}
			command.getStatus().setActionPerformed(command.getStatus().getInsertCount() > 0 || command.getStatus().getUpdateCount() > 0);
			command.getStatus().setMessageWithErrors("Processing Complete. " + command.getStatus().getCountMessage("Securities "), 5);
		});
	}


	private OrderSecurity saveOrderSecurityImpl(OrderSecurity orderSecurity, OrderSharedPopulatorCommand command, Map<Integer, OrderSecurity> masterIdentifierSecurityMap, Set<Integer> masterIdentifierSaveSet) {
		if (orderSecurity.getUnderlyingSecurity() != null && orderSecurity.getUnderlyingSecurity().isNewBean()) {
			orderSecurity.setUnderlyingSecurity(saveOrderSecurityImpl(masterIdentifierSecurityMap.get(orderSecurity.getUnderlyingSecurity().getMasterIdentifier()), command, masterIdentifierSecurityMap, masterIdentifierSaveSet));
		}
		masterIdentifierSaveSet.add(orderSecurity.getMasterIdentifier());
		boolean insert = orderSecurity.isNewBean();
		ObjectWrapperWithBooleanResult<OrderSecurity> result = getOrderSharedService().saveOrderSecurity(orderSecurity);
		if (result.isResult()) {
			if (insert) {
				command.getStatus().incrementInsertCount();
			}
			else {
				command.getStatus().incrementUpdateCount();
			}
		}
		else {
			command.getStatus().incrementSkipCount();
		}
		return result.getObject();
	}
}
