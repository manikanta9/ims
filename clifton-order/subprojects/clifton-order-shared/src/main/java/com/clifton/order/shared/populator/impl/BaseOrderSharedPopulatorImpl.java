package com.clifton.order.shared.populator.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.order.shared.populator.OrderSharedPopulatorCommand;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import com.clifton.system.query.search.SystemQuerySearchForm;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.system.upload.SystemUploadHandler;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BaseOrderSharedPopulatorImpl</code> is an abstract implementation that leverages system queries and system uploads to convert the query results into a list of beans
 * that can then be uploaded into the system, or doing the full upload itself
 *
 * @author manderson
 */
@Getter
@Setter
public abstract class BaseOrderSharedPopulatorImpl<T extends IdentityObject> {

	private SystemQueryService systemQueryService;
	private SystemQueryExecutionService systemQueryExecutionService;

	private SystemUploadHandler systemUploadHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The tag to use to look for the query to run to get the results.
	 * Currently only allow one query with the tag to exist, if multiple will throw an error
	 */
	protected abstract String getQueryTagName();


	/**
	 * The table name the entities are being uploaded to. i.e. OrderAccount
	 * Usually just one and doesn't use the results, but can if needs to be dynamic
	 */
	protected abstract String getTableName(DataTable results);


	/**
	 * Return the list of parameter values to use to execute the query.  These will often be dependant on options selected for the command.
	 */
	protected abstract List<SystemQueryParameterValue> populateSystemQueryParameterValueList(OrderSharedPopulatorCommand command, List<SystemQueryParameter> parameterList);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void buildSystemUploadCommand(SystemUploadCommand uploadCommand, OrderSharedPopulatorCommand command, boolean simple, DataTable queryResults) {
		uploadCommand.setSimple(simple);
		uploadCommand.setTableName(getTableName(queryResults));
		uploadCommand.setPartialUploadAllowed(true);
		uploadCommand.setExistingBeans(FileUploadExistingBeanActions.UPDATE);

		DataTableToExcelFileConverter dataFileConverter = new DataTableToExcelFileConverter("xls");
		DataTableFileConfig fileConfig = new DataTableFileConfig(queryResults);
		try {
			uploadCommand.setFile(new MultipartFileImpl(dataFileConverter.convert(fileConfig)));
		}
		catch (Throwable e) {
			throw new RuntimeException("Error setting upload file: " + e.getMessage(), e);
		}
	}


	protected DataTable getQueryResults(SystemQuery query, OrderSharedPopulatorCommand command) {
		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
		List<SystemQueryParameterValue> parameterValueList = populateSystemQueryParameterValueList(command, parameterList);
		query.setParameterValueList(parameterValueList);
		return getSystemQueryExecutionService().getSystemQueryResult(query);
	}


	protected SystemQueryParameterValue populateSystemQueryParameterValue(SystemQueryParameter parameter, String value) {
		SystemQueryParameterValue parameterValue = new SystemQueryParameterValue();
		parameterValue.setParameter(parameter);
		parameterValue.setValue(value);
		parameterValue.setText(value);
		return parameterValue;
	}


	protected List<SystemQuery> getSystemQueryListByTag(boolean onlyOneAllowed, boolean exceptionIfMissing) {
		SystemQuerySearchForm searchForm = new SystemQuerySearchForm();
		searchForm.setCategoryName("System Query Tags");

		searchForm.setCategoryHierarchyName(getQueryTagName());
		searchForm.setCategoryIncludeAllIfNull(false);
		searchForm.setOrderBy("name:asc");
		List<SystemQuery> queryList = getSystemQueryService().getSystemQueryList(searchForm);
		if (onlyOneAllowed && CollectionUtils.getSize(queryList) > 1) {
			throw new ValidationException("Expected only one query to exist that uses tag " + getQueryTagName() + " but found " + CollectionUtils.getSize(queryList) + ": " + StringUtils.collectionToCommaDelimitedString(queryList, SystemQuery::getName));
		}
		if (exceptionIfMissing && CollectionUtils.isEmpty(queryList)) {
			throw new ValidationException("Did not find any query that uses tag " + getQueryTagName() + ".");
		}
		return queryList;
	}


	@SuppressWarnings("unchecked")
	protected List<T> convertDataTableToBeanList(OrderSharedPopulatorCommand command, SystemUploadCommand uploadCommand) {
		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, true);
		if (uploadCommand.isError()) {
			command.getStatus().addError(uploadCommand.getUploadResultsString());
		}
		List<T> entityList = new ArrayList<>();
		CollectionUtils.getStream(beanList).forEach(bean -> entityList.add((T) bean));
		return entityList;
	}
}
