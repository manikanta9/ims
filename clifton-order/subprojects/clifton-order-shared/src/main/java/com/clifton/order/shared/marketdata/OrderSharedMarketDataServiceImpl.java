package com.clifton.order.shared.marketdata;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import com.clifton.exchangerate.api.client.model.ExchangeRateData;
import com.clifton.exchangerate.api.client.model.ExchangeRateRequest;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.marketdata.cache.OrderSharedCurrencyConventionCache;
import com.clifton.order.shared.marketdata.cache.OrderSharedExchangeRateCache;
import com.clifton.order.shared.marketdata.cache.OrderSharedPriceCache;
import com.clifton.price.api.client.model.PriceData;
import com.clifton.price.api.client.model.PriceRequest;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author AbhinayaM
 */
@Service
@Getter
@Setter
public class OrderSharedMarketDataServiceImpl implements OrderSharedMarketDataService {

	private static final String EXCHANGE_RATE_LOOKUP_QUERY_NAME = "Exchange Rate Lookup";
	private static final String PRICE_LOOKUP_QUERY_NAME = "Price Lookup";
	private static final String CURRENCY_CONVENTION_LIST_NAME = "Currency Priority (Currency Conventions)";


	private OrderSharedCurrencyConventionCache orderSharedCurrencyConventionCache;
	private OrderSharedExchangeRateCache orderSharedExchangeRateCache;
	private OrderSharedPriceCache orderSharedPriceCache;

	private OrderSharedService orderSharedService;

	private SystemListService<SystemList> systemListService;
	private SystemQueryService systemQueryService;
	private SystemQueryExecutionService systemQueryExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////           Currency Convention Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CurrencyConventionData getOrderMarketDataCurrencyConvention(String fromCurrencyCode, String toCurrencyCode) {
		ValidationUtils.assertNotEmpty(fromCurrencyCode, "From Currency Code is Required.");
		ValidationUtils.assertNotEmpty(toCurrencyCode, "To Currency Code is Required.");

		CurrencyConventionData currencyConventionData = getOrderSharedCurrencyConventionCache().getCurrencyConventionDataFromCache(fromCurrencyCode, toCurrencyCode);
		if (currencyConventionData == null) {
			currencyConventionData = new CurrencyConventionData();
			currencyConventionData.setFromCurrencyCode(fromCurrencyCode);
			currencyConventionData.setToCurrencyCode(toCurrencyCode);
			String dominantCurrencyCode = getDominantCurrencyCodeForPair(fromCurrencyCode, toCurrencyCode);
			ValidationUtils.assertNotEmpty(dominantCurrencyCode, "Unable to determine Currency Convention for Currency Pair " + fromCurrencyCode + ", " + toCurrencyCode);
			currencyConventionData.setDominantCurrencyCode(dominantCurrencyCode);
			currencyConventionData.setNonDominateCurrencyCode((StringUtils.isEqualIgnoreCase(fromCurrencyCode, dominantCurrencyCode) ? toCurrencyCode : fromCurrencyCode));
			currencyConventionData.setMultiplyByExchangeRate(StringUtils.isEqualIgnoreCase(fromCurrencyCode, currencyConventionData.getDominantCurrencyCode()));
			getOrderSharedCurrencyConventionCache().setCurrencyConventionDataInCache(currencyConventionData);
		}
		return currencyConventionData;
	}


	protected String getDominantCurrencyCodeForPair(String fromCurrencyCode, String toCurrencyCode) {
		// Not going to error out if requesting same currency on both sides - FX rate would be 1 anyway
		if (StringUtils.isEqualIgnoreCase(fromCurrencyCode, toCurrencyCode)) {
			return fromCurrencyCode;
		}
		SystemListItem fromCurrencyItem = getSystemListService().getSystemListItemByListAndValue(CURRENCY_CONVENTION_LIST_NAME, fromCurrencyCode);
		ValidationUtils.assertNotNull(fromCurrencyItem, "From Currency Code [" + fromCurrencyCode + "] is not specified in the list [" + CURRENCY_CONVENTION_LIST_NAME + "].  Unable to determine currency convention for currency pair " + fromCurrencyCode + "/" + toCurrencyCode);
		SystemListItem toCurrencyItem = getSystemListService().getSystemListItemByListAndValue(CURRENCY_CONVENTION_LIST_NAME, toCurrencyCode);
		ValidationUtils.assertNotNull(toCurrencyItem, "To Currency Code [" + toCurrencyCode + "] is not specified in the list [" + CURRENCY_CONVENTION_LIST_NAME + "].  Unable to determine currency convention for currency pair " + fromCurrencyCode + "/" + toCurrencyCode);

		if (MathUtils.isLessThan(fromCurrencyItem.getOrder(), toCurrencyItem.getOrder())) {
			return fromCurrencyCode;
		}
		return toCurrencyCode;
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Exchange Rate Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExchangeRateData getOrderMarketDataExchangeRateFlexible(String fromCurrencyCode, String toCurrencyCode, Date date, boolean exceptionIfMissing) {
		ValidationUtils.assertNotEmpty(fromCurrencyCode, "From Currency Code is Required.");
		ValidationUtils.assertNotEmpty(toCurrencyCode, "To Currency Code is Required.");
		if (date == null) {
			date = DateUtils.clearTime(new Date());
		}

		ExchangeRateRequest exchangeRateRequest = new ExchangeRateRequest();
		exchangeRateRequest.setFromCurrencyCode(fromCurrencyCode);
		exchangeRateRequest.setToCurrencyCode(toCurrencyCode);
		exchangeRateRequest.setExchangeRateDate(DateUtils.asLocalDate(date));

		// Our interim solution will use currency conventions and a flexible 7 days back search - so set the properties to it matches what we are caching
		exchangeRateRequest.setFlexible(true);
		exchangeRateRequest.setFlexibleMaximumDaysBack(7);
		exchangeRateRequest.setDoNotUseCurrencyConvention(false);

		ExchangeRateData exchangeRateData = getExchangeRateDataForRequest(exchangeRateRequest);
		if (exchangeRateData == null && exceptionIfMissing) {
			throw new ValidationException("No exchange rate found between '" + fromCurrencyCode + "' and '" + toCurrencyCode + "' within 7 days of '" + DateUtils.fromDateShort(date) + "'.");
		}
		return exchangeRateData;
	}


	protected ExchangeRateData getExchangeRateDataForRequest(ExchangeRateRequest exchangeRateRequest) {
		CurrencyConventionData currencyConventionData = getOrderMarketDataCurrencyConvention(exchangeRateRequest.getFromCurrencyCode(), exchangeRateRequest.getToCurrencyCode());
		String lookupFromCurrency = currencyConventionData.getDominantCurrencyCode();
		String lookupToCurrency = currencyConventionData.getNonDominateCurrencyCode();
		// Also Uses a cache look up
		BigDecimal exchangeRate = lookupExchangeRateFlexible(lookupFromCurrency, lookupToCurrency, DateUtils.asUtilDate(exchangeRateRequest.getExchangeRateDate()));

		if (exchangeRate != null) {
			ExchangeRateData exchangeRateData = new ExchangeRateData();
			exchangeRateData.setFromCurrencyCode(lookupFromCurrency);
			exchangeRateData.setToCurrencyCode(lookupToCurrency);
			exchangeRateData.setExchangeRateDate(exchangeRateRequest.getExchangeRateDate());
			exchangeRateData.setExchangeRate(exchangeRate);
			return exchangeRateData;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the Exchange Rate as explicitly requested.  For currency conventions, we'll first determine the convention and then request the rate in that convention
	 */
	protected BigDecimal lookupExchangeRateFlexible(String fromCurrencyCode, String toCurrencyCode, Date date) {
		if (fromCurrencyCode.equals(toCurrencyCode)) {
			return BigDecimal.ONE;
		}

		// Try to get from cache first
		BigDecimal cachedExchangeRate = getOrderSharedExchangeRateCache().getExchangeRateFromCache(fromCurrencyCode, toCurrencyCode, date);
		if (cachedExchangeRate != null) {
			return cachedExchangeRate;
		}

		SystemQuery query = getSystemQueryService().getSystemQueryByName(EXCHANGE_RATE_LOOKUP_QUERY_NAME);
		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
		List<SystemQueryParameterValue> parameterValueList = new ArrayList<>();
		for (SystemQueryParameter parameter : CollectionUtils.getIterable(parameterList)) {
			SystemQueryParameterValue parameterValue = new SystemQueryParameterValue();
			parameterValue.setParameter(parameter);
			switch (parameter.getName()) {
				case "fromCurrencyCode":
					parameterValue.setValue(fromCurrencyCode);
					break;

				case "toCurrencyCode":
					parameterValue.setValue(toCurrencyCode);
					break;
				case "Date":
					parameterValue.setValue(DateUtils.fromDateShort(date));
					break;
			}
			if (!StringUtils.isEmpty(parameterValue.getValue())) {
				parameterValueList.add(parameterValue);
			}
		}
		query.setParameterValueList(parameterValueList);
		DataTable dataTable = getSystemQueryExecutionService().getSystemQueryResult(query);
		BigDecimal exchangeRate = (dataTable == null || dataTable.getTotalRowCount() == 0) ? null : (BigDecimal) dataTable.getRow(0).getValue(0);

		if (exchangeRate != null) {
			// Put it in the cache
			getOrderSharedExchangeRateCache().setExchangeRateInCache(fromCurrencyCode, toCurrencyCode, date, exchangeRate);
		}
		return exchangeRate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Price Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PriceData getOrderMarketDataPriceFlexible(int orderSecurityId, Date date, boolean exceptionIfMissing) {
		if (date == null) {
			date = DateUtils.clearTime(new Date());
		}

		OrderSecurity orderSecurity = getOrderSharedService().getOrderSecurity(orderSecurityId);

		PriceRequest priceRequest = new PriceRequest();
		priceRequest.setIdentifierType("masterIdentifier");
		priceRequest.setIdentifierValue(orderSecurity.getMasterIdentifier() + "");
		priceRequest.setPriceDate(DateUtils.asLocalDate(date));

		// Our interim solution will use a flexible (with no limit on days back search) - so set the properties to it matches what we are caching
		// This solution will not work if there are any splits or other type of events between requested date and price date
		priceRequest.setFlexible(true);

		PriceData priceData = getPriceDataForRequest(priceRequest, orderSecurity);
		if (priceData == null && exceptionIfMissing) {
			throw new ValidationException("No price found for '" + orderSecurity.getLabel() + "' on or before '" + DateUtils.fromDateShort(date) + "'.");
		}
		return priceData;
	}


	protected PriceData getPriceDataForRequest(PriceRequest priceRequest, OrderSecurity orderSecurity) {
		// Also Uses a cache look up
		ValidationUtils.assertTrue(StringUtils.isEqualIgnoreCase("masterIdentifier", priceRequest.getIdentifierType()), "Price lookup only implemented for master identifier at this time.");
		BigDecimal price = lookupPriceFlexible(Integer.parseInt(priceRequest.getIdentifierValue()), DateUtils.asUtilDate(priceRequest.getPriceDate()));

		if (price != null) {
			PriceData priceData = new PriceData();
			priceData.setIdentifierType(priceRequest.getIdentifierType());
			priceData.setIdentifierValue(priceRequest.getIdentifierValue());
			priceData.setPrice(price);
			// NOTE: THIS WILL NEED TO BE CONVERTED TO CHECK QUOTED CURRENCY ONCE SECURITY MASTER IS USED AND PRICE MULTIPLIER DOES NOT APPLY TO EQUITIES
			priceData.setBasePrice(MathUtils.multiply(price, orderSecurity.getPriceMultiplier()));
			priceData.setPriceDate(priceRequest.getPriceDate());
			return priceData;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the Price as explicitly requested.
	 */
	protected BigDecimal lookupPriceFlexible(int masterSecurityIdentifier, Date date) {
		// Try to get from cache first
		BigDecimal price = getOrderSharedPriceCache().getPriceFromCache(masterSecurityIdentifier, date);
		if (price != null) {
			return price;
		}

		SystemQuery query = getSystemQueryService().getSystemQueryByName(PRICE_LOOKUP_QUERY_NAME);
		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
		List<SystemQueryParameterValue> parameterValueList = new ArrayList<>();
		for (SystemQueryParameter parameter : CollectionUtils.getIterable(parameterList)) {
			SystemQueryParameterValue parameterValue = new SystemQueryParameterValue();
			parameterValue.setParameter(parameter);
			switch (parameter.getName()) {
				case "masterIdentifier":
					parameterValue.setValue(masterSecurityIdentifier + "");
					break;
				case "Date":
					parameterValue.setValue(DateUtils.fromDateShort(date));
					break;
			}
			if (!StringUtils.isEmpty(parameterValue.getValue())) {
				parameterValueList.add(parameterValue);
			}
		}
		query.setParameterValueList(parameterValueList);
		DataTable dataTable = getSystemQueryExecutionService().getSystemQueryResult(query);
		price = (dataTable == null || dataTable.getTotalRowCount() == 0) ? null : (BigDecimal) dataTable.getRow(0).getValue(0);

		if (price != null) {
			// Put it in the cache
			getOrderSharedPriceCache().setPriceInCache(masterSecurityIdentifier, date, price);
		}
		return price;
	}
}
