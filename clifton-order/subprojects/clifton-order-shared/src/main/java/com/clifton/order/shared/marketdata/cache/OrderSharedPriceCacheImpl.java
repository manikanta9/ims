package com.clifton.order.shared.marketdata.cache;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.util.StringUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderSharedPriceCacheImpl implements OrderSharedPriceCache {


	private CacheHandler<String, BigDecimal> cacheHandler;


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getPriceFromCache(int masterSecurityIdentifier, Date date) {
		return getCacheHandler().get(getCacheName(), getBeanKey(masterSecurityIdentifier, date));
	}


	@Override
	public void setPriceInCache(int masterSecurityIdentifier, Date date, BigDecimal price) {
		getCacheHandler().put(getCacheName(), getBeanKey(masterSecurityIdentifier, date), price);
	}


	private String getBeanKey(int masterSecurityIdentifier, Date date) {
		return StringUtils.generateKey(masterSecurityIdentifier, date);
	}
}
