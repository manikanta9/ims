package com.clifton.order.shared.populator.impl;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.populator.OrderSharedDataPopulator;
import com.clifton.order.shared.populator.OrderSharedPopulatorCommand;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.upload.SystemUploadCommand;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>OrderSecurityPopulator</code> implements the {@link OrderSharedDataPopulator} interface
 * and returns {@link OrderSecurity} objects to later be merged/saved based on security data from a query execution
 * which the calling method can review and save
 *
 * @author manderson
 */
@Component
public class OrderSecurityPopulatorImpl extends BaseOrderSharedPopulatorImpl<OrderSecurity> implements OrderSharedDataPopulator<OrderSecurity> {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderSecurity> populateOrderSharedDataList(OrderSharedPopulatorCommand command) {
		SystemQuery query = CollectionUtils.getFirstElementStrict(getSystemQueryListByTag(true, true));
		command.getStatus().setMessage("Processing Results for Query: " + query.getName());
		DataTable results = getQueryResults(query, command);
		SystemUploadCommand uploadCommand = new SystemUploadCommand();
		buildSystemUploadCommand(uploadCommand, command, true, results);
		uploadCommand.setOverrideTableNaturalKeyBeanProperties(new String[]{"masterIdentifier"});
		return convertDataTableToBeanList(command, uploadCommand);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getQueryTagName() {
		return "Order Security Data Import";
	}


	@Override
	protected String getTableName(DataTable results) {
		return "OrderSecurity";
	}


	@Override
	protected List<SystemQueryParameterValue> populateSystemQueryParameterValueList(OrderSharedPopulatorCommand command, List<SystemQueryParameter> parameterList) {
		List<SystemQueryParameterValue> parameterValueList = new ArrayList<>();
		for (SystemQueryParameter parameter : CollectionUtils.getIterable(parameterList)) {
			String parameterStringValue = null;
			switch (parameter.getName()) {
				case "masterIdentifier":
					parameterStringValue = (command.getSecurityMasterIdentifier() == null ? null : command.getSecurityMasterIdentifier() + "");
					break;
				case "ticker":
					parameterStringValue = command.getSecurityTicker();
					break;
				case "lastUpdatedAfterDate":
					if (StringUtils.isEmpty(command.getSecurityTicker()) && command.getSecurityMasterIdentifier() == null && command.getWeekdaysBack() != null) {
						parameterStringValue = DateUtils.fromDateShort(DateUtils.addWeekDays(DateUtils.clearTime(new Date()), -command.getWeekdaysBack()));
					}
					break;
			}
			if (parameterStringValue != null) {
				parameterValueList.add(populateSystemQueryParameterValue(parameter, parameterStringValue));
			}
		}
		return parameterValueList;
	}
}
