Ext.ns(
	'Clifton.order',
	'Clifton.order.shared',
	'Clifton.order.shared.account',
	'Clifton.order.shared.company',
	'Clifton.order.shared.security'
);

Clifton.order.shared.security.OrderSecurityWindowAdditionalTabs = [];
Clifton.order.shared.account.OrderAccountWindowAdditionalTabs = [];
Clifton.order.shared.company.OrderCompanyWindowAdditionalTabs = [];


Clifton.order.shared.security.CurrencySecurityByTickerWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderSecurityForCurrencyTicker.json',

	getClassName: function() {
		return 'Clifton.order.shared.security.SecurityWindow';
	},

	getLoadParams: function(config) {
		const params = {currencyCode: config.params.id};
		if (this.requestedPropertiesRoot) {
			params.requestedPropertiesRoot = this.requestedPropertiesRoot;
		}
		if (this.requestedProperties) {
			params.requestedProperties = this.requestedProperties;
		}
		return params;
	}
});

Clifton.order.shared.security.getTradeDatePromise = function(securityId, returnString, componentScope) {
	return new Promise(function(resolveCallback) {
		if (!securityId) {
			resolveCallback(null);
		}
		else {
			TCG.data.getDataValue('orderSecurityTradeDate.json?securityId=' + securityId, componentScope, function(result) {
				const parsedResult = TCG.parseDate(result);
				const today = new Date().clearTime();
				if (parsedResult.clearTime() > today) {
					/*
									 * The Security Trade Date service uses the Security's Exchange to lookup the Trade Date. If the result is greater
									 * than today, an exchange was found to be closed. Get the security so we have all the information we need for the message.
									 */
					TCG.data.getDataPromise('orderSecurity.json?id=' + securityId, componentScope, 'security.id.' + securityId) //check this
						.then(function(sec) {
							const exchange = sec.primaryExchange;
							const exchangeMessage = exchange ? 'Investment Exchange: [' + exchange.name + '] is currently closed.  ' : '';
							TCG.showInfo(exchangeMessage + 'Trade Date for Security: [' + sec.ticker + '] will be moved forward to the next available Trade Date of [' + parsedResult.format('m/d/Y') + '].', 'Trade Date');
							resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
						});
				}
				else {
					resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
				}
			});
		}
	});
};

// Returns the next viable trading business day from the given trade date
Clifton.order.shared.security.getTradeDateNextPromise = function(securityId, tradeDate, returnString, componentScope) {
	return new Promise(function(resolveCallback) {
		if (!securityId) {
			resolveCallback(null);
		}
		else {
			let dateStr = tradeDate;
			if (typeof dateStr !== 'string') {
				dateStr = dateStr.format('m/d/Y');
			}
			TCG.data.getDataValue('orderSecurityTradeDateNext.json?securityId=' + securityId + '&tradeDate=' + dateStr, componentScope, function(result) {
				const parsedResult = TCG.parseDate(result);
				resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
			});
		}
	});
};


// settlementCurrencyId is needed for physical currencies that have settlement convention
Clifton.order.shared.security.getSettlementDatePromise = function(securityId, transactionDate, returnString, settlementCurrencyId, componentScope) {
	return new Promise(function(resolveCallback) {
		if (!securityId) {
			resolveCallback(null);
		}
		else {
			let dateStr = transactionDate;
			if (typeof dateStr !== 'string') {
				dateStr = dateStr.format('m/d/Y');
			}
			TCG.data.getDataValue('orderSecuritySettlementDate.json?securityId=' + securityId + '&transactionDate=' + dateStr + (settlementCurrencyId ? '&settlementCurrencyId=' + settlementCurrencyId : ''), componentScope, function(result) {
				const parsedResult = TCG.parseDate(result);
				resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
			});
		}
	});
};

// Returns the next viable settlement business day from the given settlement date
// settlementCurrencyId is needed for physical currencies that have settlement convention
Clifton.order.shared.security.getSettlementDateNextPromise = function(securityId, settlementDate, returnString, settlementCurrencyId, componentScope) {
	return new Promise(function(resolveCallback) {
		if (!securityId) {
			resolveCallback(null);
		}
		else {
			let dateStr = settlementDate;
			if (typeof dateStr !== 'string') {
				dateStr = dateStr.format('m/d/Y');
			}
			TCG.data.getDataValue('orderSecuritySettlementDateNext.json?securityId=' + securityId + '&settlementDate=' + dateStr + (settlementCurrencyId ? '&settlementCurrencyId=' + settlementCurrencyId : ''), componentScope, function(result) {
				const parsedResult = TCG.parseDate(result);
				resolveCallback(returnString ? parsedResult.format('m/d/Y') : parsedResult);
			});
		}
	});
};


Clifton.order.shared.company.CompanyListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderCompanyListFind',
	xtype: 'gridpanel',
	instructions: 'This section allows browsing and filtering all companies (Brokers, Custodians, Counterparties, Security Issuers, ...) defined in the system.',
	topToolbarSearchParameter: 'searchPattern',

	categoryHierarchyName: undefined, // Override to Hide the Tag Toolbar filter and always pass value for Company Tags filter.

	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Master ID', width: 30, hidden: true, dataIndex: 'masterIdentifier', type: 'int', doNotFormat: true, useNull: true},
		{header: 'Bloomberg ID', width: 35, dataIndex: 'bloombergCompanyIdentifier', type: 'int', doNotFormat: true, useNull: true, qtip: 'Bloomberg Company Identifier'},

		{
			header: 'Relationship to Parent',
			width: 80,
			dataIndex: 'parentRelationshipType.text',
			filter: {type: 'combo', searchFieldName: 'parentRelationshipTypeId', xtype: 'system-list-combo', listName: 'Company Parent Relationship Type', valueField: 'id'},
			hidden: true
		},

		{header: 'Is Ultimate Parent', width: 70, dataIndex: 'ultimateParent', type: 'boolean', hidden: true},
		{header: 'Is Private Company', width: 70, dataIndex: 'privateCompany', type: 'boolean', hidden: true},
		{header: 'Acquired By Parent', width: 70, dataIndex: 'acquiredByParent', type: 'boolean', hidden: true},

		{header: 'Parent Company', width: 70, dataIndex: 'parent.name', filter: {type: 'combo', searchFieldName: 'parentId', url: 'orderCompanyListFind.json'}, hidden: true},
		{
			header: 'Obligor Company',
			width: 70,
			dataIndex: 'obligorCompany.name',
			filter: {type: 'combo', searchFieldName: 'obligorCompanyId', url: 'orderCompanyListFind.json'},
			hidden: true
		},
		{
			header: 'Parent Obligor Company',
			width: 100,
			dataIndex: 'parentObligorCompany.name',
			filter: {type: 'combo', searchFieldName: 'parentObligorCompanyId', url: 'orderCompanyListFind.json'},
			hidden: true
		},


		{header: 'Company Name', width: 100, dataIndex: 'name', defaultSortColumn: true},  // Uses Toolbar Filter
		{header: 'Alias', width: 60, dataIndex: 'alias', filter: {searchFieldName: 'alias'}},
		{header: 'Abbreviation', width: 40, dataIndex: 'abbreviation', filter: {searchFieldName: 'abbreviation'}},
		{header: 'FED Mnemonic', width: 50, dataIndex: 'fedMnemonic', hidden: true},
		{header: 'Business Identifier Code', width: 75, dataIndex: 'businessIdentifierCode', hidden: true},
		{header: 'DTC Number', width: 50, dataIndex: 'dtcNumber'},

		{header: 'Legal Entity Identifier (LEI)', width: 60, dataIndex: 'legalEntityIdentifier'},
		{header: 'LEI Entity Status', width: 60, dataIndex: 'leiEntityStatus', hidden: true},
		{header: 'LEI Entity Start Date', width: 75, dataIndex: 'leiEntityStartDate', hidden: true},
		{header: 'LEI Entity End Date', width: 75, dataIndex: 'leiEntityEndDate', hidden: true},

		{header: 'Company Corporate Ticker', width: 80, dataIndex: 'companyCorporateTicker', hidden: true},
		{header: 'Ultimate Parent Equity Ticker', width: 80, dataIndex: 'ultimateParentEquityTicker', hidden: true},

		{header: 'State Of Incorporation', width: 50, dataIndex: 'stateOfIncorporation.text', filter: {searchFieldName: 'stateOfIncorporation'}},
		{
			header: 'Country Of Incorporation',
			width: 50,
			dataIndex: 'countryOfIncorporation.text',
			filter: {searchFieldName: 'countryOfIncorporationId', type: 'combo', xtype: 'system-list-combo', listName: 'Countries', valueField: 'id'}
		},
		{
			header: 'Country Of Risk',
			width: 45,
			dataIndex: 'countryOfRisk.text',
			filter: {searchFieldName: 'countryOfRiskId', type: 'combo', xtype: 'system-list-combo', listName: 'Countries', valueField: 'id'}
		}
	],

	getTopToolbarFilters: function(toolbar) {
		if (TCG.isNull(this.categoryHierarchyName)) {
			return [
				{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Company Tags'},
				{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
			];
		}
		return [
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (TCG.isNull(this.categoryHierarchyName)) {
			const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
			if (TCG.isNotBlank(tag.getValue())) {
				return {
					categoryName: 'Company Tags',
					categoryTableName: 'OrderCompany',
					categoryHierarchyId: tag.getValue()
				};
			}
		}
		else {
			return {
				categoryName: 'Company Tags',
				categoryTableName: 'OrderCompany',
				categoryHierarchyName: this.categoryHierarchyName
			};
		}
	},
	editor: {
		drillDownOnly: true, // allow deletes or adds?
		detailPageClass: 'Clifton.order.shared.company.CompanyWindow'
	}
});
Ext.reg('order-company-list-grid', Clifton.order.shared.company.CompanyListGrid);

Clifton.order.shared.account.AccountListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderAccountListFind',
	xtype: 'gridpanel',
	instructions: 'This section allows browsing and filtering all accounts (Client and Holding Accounts) defined in the system.',
	topToolbarSearchParameter: 'searchPattern',
	//groupField: 'clientCompany.name',
	//groupTextTpl: '{values.text}: {[values.rs.length]} {[values.rs.length > 1 ? "Accounts" : "Account"]}',
	viewNames: ['Client Accounts', 'Holding Accounts', 'All Accounts'],
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Master ID', width: 30, hidden: true, dataIndex: 'masterIdentifier', type: 'int', doNotFormat: true, useNull: true},
		{
			header: 'Client Name',
			width: 100,
			dataIndex: 'clientCompany.name',
			hidden: true,
			filter: {searchFieldName: 'clientCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Client'}
		},
		{header: 'Account #', width: 50, dataIndex: 'accountNumber', allViews: true},
		{header: 'Account # 2', width: 50, dataIndex: 'accountNumber2', hidden: true},
		{header: 'Account Short Name', width: 75, dataIndex: 'accountShortName', viewNames: ['Client Accounts']},
		{header: 'Account Name', width: 125, dataIndex: 'accountName', allViews: true},

		{
			header: 'Account Type',
			width: 50,
			dataIndex: 'accountType.text',
			filter: {searchFieldName: 'accountTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Account Type', valueField: 'id'},
			viewNames: ['All Accounts', 'Holding Accounts']
		},
		{
			header: 'Issuing Company',
			width: 75,
			dataIndex: 'issuingCompany.name',
			filter: {searchFieldName: 'issuingCompanyId', type: 'combo', url: 'orderCompanyListFind.json'},
			allViews: true
		},

		{
			header: 'Workflow State',
			width: 50,
			dataIndex: 'workflowState.name',
			allViews: true,
			filter: {searchFieldName: 'workflowStateId', type: 'combo', url: 'workflowStateListFind.json?workflowName=Account Status'}
		},
		{
			header: 'Workflow Status',
			width: 50,
			dataIndex: 'workflowState.status.name',
			hidden: true,
			filter: {searchFieldName: 'workflowStatusId', type: 'combo', url: 'workflowStateListFind.json?assignmentTableName=OrderAccount'}
		},

		{
			header: 'PM Team',
			width: 50,
			dataIndex: 'teamSecurityGroup.name',
			viewNames: ['Client Accounts'],
			filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', xtype: 'system-security-group-combo', groupTagName: 'PM Team'}
		},
		{
			header: 'Portfolio Manager',
			width: 50,
			dataIndex: 'portfolioManagerUser.displayName',
			viewNames: ['Client Accounts'],
			filter: {searchFieldName: 'portfolioManagerUserId', type: 'combo', xtype: 'system-security-user-group-combo', groupTagName: 'PM Team'}
		},
		{
			header: 'Business Service',
			width: 80,
			dataIndex: 'service.name',
			hidden: true,
			filter: {
				searchFieldName: 'serviceOrParentId',
				type: 'combo',
				url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Account Service',
				displayField: 'nameExpanded',
				queryParam: 'nameExpanded',
				listWidth: 500
			},
			viewNames: ['Client Accounts']
		},

		{header: 'Inception Date', width: 50, dataIndex: 'inceptionDate', viewNames: ['Client Accounts']},

		{header: 'Base CCY', width: 35, dataIndex: 'baseCurrencyCode', allViews: true},
		{
			header: 'FX Source',
			width: 100,
			dataIndex: 'defaultExchangeRateSourceCompany.name',
			hidden: true,
			filter: {searchFieldName: 'defaultExchangeRateSourceCompanyId', type: 'combo', url: 'orderCompanyListFind.json'}
		},


		{header: 'Client Directed', width: 50, dataIndex: 'clientDirected', type: 'boolean', hidden: true},
		{
			header: 'Platform',
			width: 50,
			dataIndex: 'companyPlatform.text',
			hidden: true,
			filter: {searchFieldName: 'companyPlatformId', type: 'combo', xtype: 'system-list-combo', listName: 'Account Platform', valueField: 'id'}
		},
		{header: 'Wrap Account', width: 50, dataIndex: 'wrapAccount', type: 'boolean', hidden: true},


		{header: 'Client Account', width: 50, dataIndex: 'clientAccount', type: 'boolean', viewNames: ['All Accounts']},
		{header: 'Holding Account', width: 50, dataIndex: 'holdingAccount', type: 'boolean', viewNames: ['All Accounts']}

	],

	listeners: {
		afterRender: function() {
			this.setDefaultView('Client Accounts');
		}
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		const params = {};
		if (tag && TCG.isNotBlank(tag.getValue())) {
			params.categoryName = 'Account Tags';
			params.categoryHierarchyId = tag.getValue();
		}
		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{
				fieldLabel: 'Client',
				xtype: 'toolbar-combo',
				width: 250,
				url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Client',
				linkedFilter: 'clientCompany.name'
			}, // TODO ADD CLIENT TAG FILTER
			{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Account Tags'},
			{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},

	switchToViewBeforeReload: function(viewName) {
		if (viewName === 'Client Accounts') {
			this.setFilterValue('clientAccount', true);
			this.clearFilter('holdingAccount', true);
		}
		else if (viewName === 'Holding Accounts') {
			this.setFilterValue('holdingAccount', true);
			this.clearFilter('clientAccount', true);
		}
		else {
			this.clearFilter('holdingAccount', true);
			this.clearFilter('clientAccount', true);
		}
	},
	editor: {
		drillDownOnly: true, // allow deletes or adds?
		detailPageClass: 'Clifton.order.shared.account.AccountWindow'
	}
});
Ext.reg('order-account-list-grid', Clifton.order.shared.account.AccountListGrid);

Clifton.order.shared.account.SecurityListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderSecurityListFind',
	xtype: 'gridpanel',
	instructions: 'This section allows browsing and filtering all securities defined in the system.',
	topToolbarSearchParameter: 'searchPattern',
	groupField: 'securityHierarchy.nameExpanded',
	groupTextTpl: '{values.text}: {[values.rs.length]} {[values.rs.length > 1 ? "Securities" : "Security"]}',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Master ID', width: 30, hidden: true, dataIndex: 'masterIdentifier', type: 'int', doNotFormat: true, useNull: true},

		{header: 'Security Type', width: 75, dataIndex: 'securityType.text', hidden: true, filter: {searchFieldName: 'securityTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Security Type', valueField: 'id'}},
		{header: 'Bloomberg Security Type', width: 75, dataIndex: 'bloombergSecurityType.text', hidden: true, filter: {searchFieldName: 'bloombergSecurityTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Bloomberg Security Type', valueField: 'id'}},
		{header: 'Security Sub Type', width: 100, dataIndex: 'securityTypeSubType.text', hidden: true, filter: {searchFieldName: 'securityTypeSubTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Security Sub Type', valueField: 'id'}},
		{header: 'Security Sub Type 2', width: 100, dataIndex: 'securityTypeSubType2.text', hidden: true, filter: {searchFieldName: 'securityTypeSubType2Id', type: 'combo', xtype: 'system-list-combo', listName: 'Security Sub Type 2', valueField: 'id'}},
		{header: 'Security Hierarchy', width: 80, dataIndex: 'securityHierarchy.nameExpanded', hidden: true, filter: {searchFieldName: 'securityHierarchyOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Security Hierarchy', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}},

		{header: 'Ticker', width: 75, dataIndex: 'ticker', defaultSortColumn: true},
		{header: 'CUSIP', width: 55, dataIndex: 'cusip'},
		{header: 'ISIN', width: 55, dataIndex: 'isin'},
		{header: 'SEDOL', width: 55, dataIndex: 'sedol'},
		{header: 'SEDOL 2', width: 55, dataIndex: 'sedol2', hidden: true},
		{header: 'OCC Symbol', width: 55, dataIndex: 'occSymbol', hidden: true},
		{header: 'CINS', width: 55, dataIndex: 'cins', hidden: true},
		{header: 'APIR', width: 55, dataIndex: 'apir', hidden: true},
		{header: 'BBG Global Identifier', width: 55, dataIndex: 'bloombergGlobalIdentifier', hidden: true},
		{header: 'Composite BBG Global Identifier', width: 55, dataIndex: 'compositeBloombergGlobalIdentifier', hidden: true},
		{header: 'Security Name', width: 100, dataIndex: 'name'},
		{header: 'Security Description', width: 175, dataIndex: 'description', hidden: true},

		{header: 'Underlying Security', width: 100, dataIndex: 'underlyingSecurity.label', filter: {searchFieldName: 'underlyingSecurityId', type: 'combo', url: 'orderSecurityListFind.json'}},
		{header: 'Counterparty or Issuer', width: 100, dataIndex: 'company.name', filter: {searchFieldName: 'companyId', type: 'combo', url: 'orderCompanyListFind.json'}},
		{header: 'Primary Exchange', width: 100, dataIndex: 'primaryExchange.name', filter: {searchFieldName: 'primaryExchangeId', type: 'combo', url: 'orderSecurityExchangeListFind.json'}},
		{header: 'Composite Exchange', width: 100, dataIndex: 'compositeExchange.name', filter: {searchFieldName: 'compositeExchangeId', type: 'combo', url: 'orderSecurityExchangeListFind.json'}, hidden: true},
		{header: 'Country Of Risk', width: 60, dataIndex: 'countryOfRisk.text', filter: {searchFieldName: 'countryOfRiskId', type: 'combo', xtype: 'system-list-combo', listName: 'Countries', valueField: 'id'}},
		{header: 'CCY Denomination', width: 70, dataIndex: 'currencyCode', filter: {type: 'combo', url: 'orderSecurityListFind.json?securityTypeName=CURRENCY', valueField: 'ticker', displayField: 'ticker'}},
		{header: 'Price Multiplier', width: 70, dataIndex: 'priceMultiplier', type: 'float', hidden: true},


		{header: 'Days To Settle', width: 70, hidden: true, dataIndex: 'daysToSettle', type: 'int', useNull: true},
		{header: 'Settlement Calendar', width: 100, hidden: true, dataIndex: 'settlementCalendar.name', filter: {type: 'combo', searchFieldName: 'settlementCalendarId', url: 'calendarListFind.json'}},
		{header: 'Cost Calculator', width: 50, hidden: true, dataIndex: 'costCalculator'},
		{header: 'Start Date', width: 50, dataIndex: 'startDate', filter: {orNull: true}, hidden: true},
		{header: 'End Date', width: 50, dataIndex: 'endDate', filter: {orNull: true}, hidden: true},
		{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean'},
		{header: 'Tradable', width: 40, dataIndex: 'tradable', type: 'boolean'},

		{header: 'Master Create Date', width: 50, dataIndex: 'masterRecordCreateDate', hidden: true},
		{header: 'Master Update Date', width: 50, dataIndex: 'masterRecordUpdateDate', hidden: true}

	],
	editor: {
		drillDownOnly: true, // allow deletes or adds?
		detailPageClass: 'Clifton.order.shared.security.SecurityWindow'
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Security Type', xtype: 'toolbar-system-list-combo', width: 150, listName: 'Security Type', valueField: 'id', linkedFilter: 'securityType.text'},
			{
				fieldLabel: 'Security Sub Type',
				width: 150,
				xtype: 'toolbar-system-list-combo',
				listName: 'Security Sub Type',
				valueField: 'id',
				linkedFilter: 'securityTypeSubType.text'
			},
			{
				fieldLabel: 'Security Sub Type 2',
				width: 150,
				xtype: 'toolbar-system-list-combo',
				listName: 'Security Sub Type 2',
				valueField: 'id',
				linkedFilter: 'securityTypeSubType2.text'
			},
			{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('active', true);
		}
	}
});
Ext.reg('order-security-list-grid', Clifton.order.shared.account.SecurityListGrid);





