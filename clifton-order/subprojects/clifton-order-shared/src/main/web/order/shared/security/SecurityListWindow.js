Clifton.order.shared.security.SecurityListWindow = Ext.extend(TCG.app.Window, {
	id: 'orderSecurityListWindow',
	title: 'Investment Securities',
	iconCls: 'stock-chart',
	width: 1500,
	height: 700,


	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Securities',
				xtype: 'order-security-list-grid',
				instructions: 'This section allows browsing and filtering all securities defined in the system.'
			},


			{
				title: 'Security Exchanges',
				items: [{
					name: 'orderSecurityExchangeListFind',
					xtype: 'gridpanel',
					instructions: 'An exchange is a marketplace in which financial instruments are traded. Exchanges give companies, governments and other groups a platform to sell securities to the investing public and provides regulation to ensure fair and orderly trading, as well as efficient dissemination of price information for any securities trading on that exchange.  It may or may not be a physical location, but could also be an electronic platform.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Exchange Name', width: 130, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Code', width: 50, dataIndex: 'exchangeCode', filter: {comparison: 'EQUALS'}},
						{header: 'MIC', width: 50, dataIndex: 'marketIdentifierCode', filter: {comparison: 'EQUALS'}, hidden: true},
						{header: 'Composite', width: 40, dataIndex: 'compositeExchange', type: 'boolean'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Parent Exchange', width: 120, dataIndex: 'parent.name', filter: {searchFieldName: 'parentId', type: 'combo', url: 'orderSecurityExchangeListFind.json'}},
						{header: 'Base Currency', width: 60, dataIndex: 'baseCurrency.name', filter: {searchFieldName: 'baseCurrencyCode'}},
						{header: 'Country', width: 70, dataIndex: 'country.text', filter: {searchFieldName: 'country'}},
						{header: 'Calendar', width: 100, dataIndex: 'calendar.name', filter: {searchFieldName: 'calendarId', type: 'combo', url: 'calendarListFind.json'}},
						{header: 'Time Zone', width: 120, dataIndex: 'timeZone.label', filter: false, sortable: false},
						{header: 'Open Time', width: 50, dataIndex: 'openTime', hidden: true},
						{header: 'Close Time', width: 50, dataIndex: 'closeTime', hidden: true},
						{header: 'Local Open Time', width: 50, dataIndex: 'openTimeLocal', hidden: true},
						{header: 'Local Close Time', width: 50, dataIndex: 'closeTimeLocal', hidden: true},
						{header: 'Open', width: 40, dataIndex: 'open', type: 'boolean', filter: false, sortable: false},
						{header: 'Partial Day Open Time', width: 50, dataIndex: 'openTimePartialDay', hidden: true},
						{header: 'Partial Day Close Time', width: 50, dataIndex: 'closeTimePartialDay', hidden: true},
						{header: 'Partial Day Local Open Time', width: 50, dataIndex: 'openTimePartialDayLocal', hidden: true},
						{header: 'Partial Day Local Close Time', width: 50, dataIndex: 'closeTimePartialDayLocal', hidden: true},
						{header: 'Partial Day Open', width: 40, dataIndex: 'openPartialDay', type: 'boolean', filter: false, sortable: false, hidden: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.order.shared.security.SecurityExchangeWindow'
					}
				}]
			},


			{
				title: 'Security Types',
				items: [
					{
						xtype: 'system-list-item-grid',
						instructions: 'Security Type defines a type of a security: Stock, Future, Bond, etc. This is the highest level of classification for investment securities.',
						columnOverrides: [
							{dataIndex: 'value', hidden: true, header: 'Master Identifier'},
							{dataIndex: 'text', defaultSortColumn: true, defaultSortDirection: 'ASC', header: 'Security Type Name'},
							{dataIndex: 'tooltip', header: 'Description', width: 300},
							{dataIndex: 'order', hidden: true}
						],

						getSystemList: function() {
							return TCG.data.getData('systemListByName.json?name=Security Type', this, 'system.list.Security Type');
						}
					}
				]
			},


			{
				title: 'Security Sub Types',
				layout: {
					type: 'border',
					align: 'stretch'
				},
				items: [
					{
						region: 'north',
						flex: 1,
						height: 250,
						title: 'Security Sub Type',
						xtype: 'system-list-item-grid',
						instructions: 'Security Sub Type is an additional classification of a security for a security type.',
						columnOverrides: [
							{dataIndex: 'value', hidden: true, header: 'Master Identifier'},
							{dataIndex: 'text', defaultSortColumn: true, defaultSortDirection: 'ASC', header: 'Security Sub Type Name'},
							{dataIndex: 'tooltip', header: 'Description', width: 300},
							{dataIndex: 'order', hidden: true}
						],

						getSystemList: function() {
							return TCG.data.getData('systemListByName.json?name=Security Sub Type', this, 'system.list.Security Sub Type');
						}
					},
					{
						region: 'center',
						flex: 1,
						height: 200,
						title: 'Security Sub Type 2',
						xtype: 'system-list-item-grid',
						instructions: 'Security Sub Type 2 is an additional classification of a security for a security type.',
						columnOverrides: [
							{dataIndex: 'value', hidden: true, header: 'Master Identifier'},
							{dataIndex: 'text', defaultSortColumn: true, defaultSortDirection: 'ASC', header: 'Security Sub Type 2 Name'},
							{dataIndex: 'tooltip', header: 'Description', width: 300},
							{dataIndex: 'order', hidden: true}
						],

						getSystemList: function() {
							return TCG.data.getData('systemListByName.json?name=Security Sub Type 2', this, 'system.list.Security Sub Type 2');
						}
					}
				]
			},


			{
				title: 'Security Hierarchies',
				items: [{
					xtype: 'system-hierarchy-grid',
					instructions: ' Securities are organized into hierarchy nodes that group them as well as define their behavior.',

					columnOverrides: [
						{dataIndex: 'value', hidden: true, header: 'Master Identifier'},
						{dataIndex: 'name', header: 'Security Hierarchy Name'}
					],

					getHierarchyCategory: function() {
						return TCG.data.getData('systemHierarchyCategoryByName.json?name=Security Hierarchy', this, 'system.hierarchy.category.Security Hierarchy');
					}
				}]
			},


			{
				title: 'Security Groups',
				items: [
					{
						xtype: 'system-group-list-grid',
						groupItemTableName: 'OrderSecurity'
					}
				]
			}
		]
	}]
});

