Clifton.order.shared.account.AccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Account',
	width: 1200,
	height: 600,
	iconCls: 'account',


	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.order.shared.account.OrderAccountWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Account Info',
				items: [{
					xtype: 'formpanel',
					url: 'orderAccount.json',

					labelWidth: 120,

					listeners: {
						afterload: function(formPanel) {

							if (TCG.isFalse(formPanel.getFormValue('clientAccount'))) {
								formPanel.hideField('service.nameExpanded');
								formPanel.hideField('teamSecurityGroup.name');
								formPanel.hideField('inceptionDate');
							}
							if (TCG.isFalse(formPanel.getFormValue('holdingAccount'))) {
								formPanel.hideFieldIfBlank('accountShortName');
								formPanel.hideField('clientDirected');
								formPanel.hideField('wrapAccount');
								formPanel.hideField('companyPlatform.text');
							}
							if (TCG.isBlank(formPanel.getFormValue('companyPlatform.text'))) {
								formPanel.hideField('companyPlatform.text');
								formPanel.hideField('wrapAccount');
							}
							formPanel.hideFieldIfBlank('accountNumber2');
							formPanel.hideFieldIfBlank('defaultExchangeRateSourceCompany.name');
							formPanel.setReadOnlyFields(true);


						}
					},

					setReadOnlyFields: function(readOnly) {
						this.setReadOnly(readOnly, ['systemHierarchyList']);
					},


					items: [
						{fieldLabel: 'Client', name: 'clientCompany.name', detailIdField: 'clientCompany.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow'},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Account Type', name: 'accountType.text', xtype: 'linkfield', detailIdField: 'accountType.id', detailPageClass: 'Clifton.system.list.ItemWindow'},
									{fieldLabel: 'Account Number', name: 'accountNumber'},
									{fieldLabel: 'Account Number 2', name: 'accountNumber2'},
									{fieldLabel: 'Base Currency', name: 'baseCurrencyCode', xtype: 'linkfield', detailIdField: 'baseCurrencyCode', detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow'},
									{fieldLabel: 'FX Source', name: 'defaultExchangeRateSourceCompany.name', detailIdField: 'defaultExchangeRateSourceCompany.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow'},
									{fieldLabel: 'Workflow State', name: 'workflowState.name', detailIdField: 'workflowState.id', xtype: 'linkfield', detailPageClass: 'Clifton.workflow.definition.StateWindow'},
									{fieldLabel: 'Positions On Date', name: 'inceptionDate', xtype: 'datefield'}
								],
								config: {columnWidth: 0.4}
							}, {
								rows: [
									{fieldLabel: 'Master ID', name: 'masterIdentifier', qtip: 'Account identifier from central Account Master that is unique to this account across all of our systems.'},
									{fieldLabel: 'Account Name', name: 'accountName'},
									{fieldLabel: 'Short Name', name: 'accountShortName'},
									{fieldLabel: 'Issued By', name: 'issuingCompany.name', detailIdField: 'issuingCompany.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow'},
									{fieldLabel: 'PM Team', name: 'teamSecurityGroup.name', detailIdField: 'teamSecurityGroup.id', xtype: 'linkfield', detailPageClass: 'Clifton.security.user.GroupWindow', qtip: 'Portfolio Management team responsible for this account'},
									{fieldLabel: 'Portfolio Manager', name: 'portfolioManagerUser.displayName', detailIdField: 'portfolioManagerUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.security.user.UserWindow', qtip: 'Individual Portfolio Manager responsible for this account.'}
								],
								config: {columnWidth: 0.6}
							}]
						},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Business Service', name: 'service.nameExpanded', xtype: 'linkfield', detailIdField: 'service.id', detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow'},

						{fieldLabel: 'Platform', name: 'companyPlatform.text', qtip: 'A company platform is a Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\' strategies. Each platform is unique to a particular company, and the only selections allowed are those associated with the issuing company of this account.'},
						{boxLabel: 'Wrap Account', name: 'wrapAccount', xtype: 'checkbox', qtip: 'Applies only if a platform is selected. Differentiates accounts where the client pays a periodic account fee rather than trade commissions.  Is tied to ‘Platform’ in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature: Retail clients generally do while Institutional clients generally do not participate in the WRAP feature/account structure on a platform.'},
						{boxLabel: 'Trading for this account is Directed by the Client', xtype: 'checkbox', name: 'clientDirected'},

						{xtype: 'label', html: '<hr />'},

						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							name: 'account-tags',
							tableName: 'OrderAccount',
							hierarchyCategoryName: 'Account Tags'
						}

					]
				}]
			},

			{
				title: 'Related Accounts',
				items: [{
					name: 'orderAccountListFind',
					xtype: 'gridpanel',
					instructions: 'Related accounts are all accounts in the system for the same client company. (Includes this account)',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Master ID', width: 30, hidden: true, dataIndex: 'masterIdentifier', type: 'int', doNotFormat: true, useNull: true},
						{header: 'Client Name', width: 100, dataIndex: 'clientCompany.name', hidden: true, filter: {searchFieldName: 'clientCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Client'}},
						{header: 'Account #', width: 50, dataIndex: 'accountNumber'},
						{header: 'Account # 2', width: 50, dataIndex: 'accountNumber2', hidden: true},
						{header: 'Account Short Name', width: 65, dataIndex: 'accountShortName'},
						{header: 'Account Name', width: 135, dataIndex: 'accountName'},

						{header: 'Account Type', width: 50, dataIndex: 'accountType.text', filter: {searchFieldName: 'accountTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Account Type', valueField: 'id'}},
						{header: 'Issuing Company', width: 75, dataIndex: 'issuingCompany.name', filter: {searchFieldName: 'issuingCompanyId', type: 'combo', url: 'orderCompanyListFind.json'}},

						{header: 'Workflow State', width: 50, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateId', type: 'combo', url: 'workflowStateListFind.json?workflowName=Account Status'}},
						{header: 'Workflow Status', width: 50, dataIndex: 'workflowState.status.name', hidden: true, filter: {searchFieldName: 'workflowStatusId', type: 'combo', url: 'workflowStateListFind.json?assignmentTableName=OrderAccount'}},
						{header: 'PM Team', width: 50, dataIndex: 'teamSecurityGroup.name', hidden: true, filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', xtype: 'system-security-group-combo', groupTagName: 'PM Team'}},
						{header: 'Portfolio Manager', width: 50, dataIndex: 'portfolioManagerUser.displayName', filter: {searchFieldName: 'portfolioManagerUserId', type: 'combo', xtype: 'system-security-user-group-combo', groupTagName: 'PM Team'}, hidden: true},
						{
							header: 'Business Service', width: 80, dataIndex: 'service.name', hidden: true,
							filter: {
								searchFieldName: 'serviceOrParentId',
								type: 'combo',
								url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Account Service',
								displayField: 'nameExpanded',
								queryParam: 'nameExpanded',
								listWidth: 500
							},
							viewNames: ['Client Accounts']
						},

						{header: 'Inception Date', width: 50, dataIndex: 'inceptionDate', hidden: true},

						{header: 'Base CCY', width: 35, dataIndex: 'baseCurrencyCode'},
						{header: 'FX Source', width: 100, dataIndex: 'defaultExchangeRateSourceCompany.name', hidden: true, filter: {searchFieldName: 'defaultExchangeRateSourceCompanyId', type: 'combo', url: 'orderCompanyListFind.json'}},

						{header: 'Client Directed', width: 50, dataIndex: 'clientDirected', type: 'boolean', hidden: true},
						{header: 'Platform', width: 50, dataIndex: 'companyPlatform.text', hidden: true, filter: {searchFieldName: 'companyPlatformId', type: 'combo', xtype: 'system-list-combo', listName: 'Account Platform', valueField: 'id'}},
						{header: 'Wrap Account', width: 50, dataIndex: 'wrapAccount', type: 'boolean', hidden: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {clientCompanyId: TCG.getValue('clientCompany.id', this.getWindow().getMainForm().formValues)};

					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.order.shared.account.AccountWindow'
					}
				}]
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'system-note-grid',
					tableName: 'OrderAccount',
					showInternalInfo: false,
					showPrivateInfo: false,
					defaultActiveFilter: false,
					showDisplayFilter: false
				}]
			},

			{
				title: 'Account Groups',
				xtype: 'system-group-list-grid',
				groupItemTableName: 'OrderAccount',
				getGroupItemFkFieldId: function() {
					return this.getWindow().getMainFormId();
				}
			}

		]
	}],

	saveWindow: function(closeOnSuccess, forceSubmit) {
		const win = this;
		const panel = this.getMainFormPanel();
		const tags = TCG.getChildByName(panel, 'account-tags');
		if (tags && tags.saveTags()) {
			tags.saveTags();
			win.savedSinceOpen = true;
		}
		if (closeOnSuccess === true) {
			win.doNotWarnOnCloseModified = true;
			win.closeWindow();
		}
	}
});
