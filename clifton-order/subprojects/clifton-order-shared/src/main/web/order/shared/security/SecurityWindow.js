Clifton.order.shared.security.SecurityWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security',
	width: 900,
	height: 550,
	iconCls: 'stock-chart',

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.order.shared.security.OrderSecurityWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					//instructions: 'A security ...',
					url: 'orderSecurity.json',
					readOnly: true,
					labelWidth: 140,

					listeners: {
						afterload: function(formPanel) {
							formPanel.hideFieldIfBlank('underlyingSecurity.label');
						}
					},

					items: [
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Security Identifier', name: 'id', qtip: 'Uniquely identifies security in OMS system only.  See "Master Identifier" for global identifier across all of our systems.'},
									{fieldLabel: 'Master Identifier', name: 'masterIdentifier', qtip: 'Security identifier from central Security Master that is unique to this security across all of our systems.'}
								],
								config: {columnWidth: 0.5}
							}, {
								rows: [
									{fieldLabel: 'Master Record Created', name: 'masterRecordCreateDate', xtype: 'displayfield', type: 'date'},
									{fieldLabel: 'Master Record Updated', name: 'masterRecordUpdateDate', xtype: 'displayfield', type: 'date'}
								],
								config: {columnWidth: 0.5}
							}]
						},

						{xtype: 'label', html: '<hr />'},
						{fieldLabel: 'Security Hierarchy', name: 'securityHierarchy.nameExpanded', xtype: 'linkfield', detailIdField: 'securityHierarchy.id', detailPageClass: 'Clifton.system.hierarchy.HierarchyWindow'},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Security Type', name: 'securityType.text', xtype: 'linkfield', detailIdField: 'securityType.id', detailPageClass: 'Clifton.system.list.ItemWindow'},
									{fieldLabel: 'Security Sub Type', name: 'securityTypeSubType.text', xtype: 'linkfield', detailIdField: 'securityTypeSubType.id', detailPageClass: 'Clifton.system.list.ItemWindow'},
									{fieldLabel: 'Security Sub Type 2', name: 'securityTypeSubType2.text', xtype: 'linkfield', detailIdField: 'securityTypeSubType2.id', detailPageClass: 'Clifton.system.list.ItemWindow'},
									{fieldLabel: 'BBG Security Type', name: 'bloombergSecurityType.text', xtype: 'linkfield', detailIdField: 'bloombergSecurityType.id', detailPageClass: 'Clifton.system.list.ItemWindow', qtip: 'Bloomberg security type'}
								],
								config: {columnWidth: 0.5}
							}, {
								rows: [
									{fieldLabel: 'Counterparty / Issuer', name: 'company.name', detailIdField: 'company.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow'},
									{fieldLabel: 'Primary Exchange', name: 'primaryExchange.label', detailIdField: 'primaryExchange.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityExchangeWindow', qtip: 'The main stock exchange where a publicly traded company\'s stock is bought and sold.'},
									{fieldLabel: 'Composite Exchange', name: 'compositeExchange.label', detailIdField: 'compositeExchange.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityExchangeWindow', qtip: 'Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades.'}
								],
								config: {columnWidth: 0.5}
							}]
						},

						{xtype: 'label', html: '<hr />'},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Ticker', name: 'ticker'},
									{fieldLabel: 'CUSIP', name: 'cusip'},
									{fieldLabel: 'ISIN', name: 'isin', qtip: 'International Securities Identification Number is a 12-digit alphanumeric code that uniquely identifies a specific security.'},
									{fieldLabel: 'CINS', name: 'cins', qtip: 'A CINS number if an international extension of the CUSIP numbering system. It consists of 9 characters.'},
									{fieldLabel: 'APIR', name: 'apir', qtip: 'The Asia Pacific Investment Register (APIR) Code is a unique identifier for many financial products issued by APIR to participants and products within the Financial Services Industry. It is widely used in some countries: Australia...'}
								],
								config: {columnWidth: 0.5}
							}, {
								rows: [
									{fieldLabel: 'SEDOL', name: 'sedol'},
									{fieldLabel: 'SEDOL 2', name: 'sedol2'},
									{fieldLabel: 'OCC Symbol', name: 'occSymbol'},
									{fieldLabel: 'BBG Global Identifier', name: 'bloombergGlobalIdentifier', qtip: 'Bloomberg Global Identifier (BBGID), also know as the Financial Instrument Global Identifier (FIGI) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'},
									{fieldLabel: 'Composite BBG Global Identifier', name: 'compositeBloombergGlobalIdentifier', qtip: 'Bloomberg\'s Twelve character, alphanumeric identifier. The Composite level of assignment is provided in cases where there are multiple trading venues for the instrument within a single country or market. The Composite Financial Instrument Global Identifier (FIGI) enables users to link multiple FIGIs at the trading venue-level within the same country or market in order to obtain an aggregated view for that instrument within that country or market.'}
								],
								config: {columnWidth: 0.5, labelWidth: 175}
							}]
						},
						{fieldLabel: 'Security Name', name: 'name'},
						{fieldLabel: 'Security Description', name: 'description', xtype: 'textarea', height: 50},

						{xtype: 'label', html: '<hr />'},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Underlying Security', name: 'underlyingSecurity.label', detailIdField: 'underlyingSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.security.SecurityWindow'},
									{fieldLabel: 'Country of Risk', name: 'countryOfRisk.labelLong', xtype: 'linkfield', detailIdField: 'countryOfRisk.id', detailPageClass: 'Clifton.system.list.ItemWindow'},
									{fieldLabel: 'CCY Denomination', name: 'currencyCode', xtype: 'linkfield', detailIdField: 'currencyCode', detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow'},
									{fieldLabel: 'Settlement Calendar', name: 'settlementCalendar.name', detailIdField: 'settlementCalendar.id', xtype: 'linkfield', detailPageClass: 'Clifton.calendar.setup.CalendarWindow'},
									{fieldLabel: 'Days to Settle', name: 'daysToSettle', xtype: 'integerfield'}

								], config: {columnWidth: 0.5}
							}, {
								rows: [
									{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
									{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
									{boxLabel: 'Active', name: 'active', xtype: 'checkbox'},
									{boxLabel: 'Tradable', name: 'tradable', xtype: 'checkbox'},
									{fieldLabel: 'Price Multiplier', name: 'priceMultiplier', xtype: 'floatfield'},
									{fieldLabel: 'Cost Calculator', name: 'costCalculator'}
								],
								config: {columnWidth: 0.5}
							}]
						}
					]
				}]
			},
			{
				title: 'Security Groups',
				items: [
					{
						xtype: 'system-group-list-grid',
						groupItemTableName: 'OrderSecurity',
						getGroupItemFkFieldId: function() {
							return this.getWindow().getMainFormId();
						}
					}
				]
			}
		]
	}]
});
