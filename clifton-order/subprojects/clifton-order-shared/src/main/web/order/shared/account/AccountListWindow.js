Clifton.order.shared.account.AccountListWindow = Ext.extend(TCG.app.Window, {
	id: 'orderAccountListWindow',
	title: 'Accounts',
	iconCls: 'account',
	width: 1500,
	height: 700,


	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Accounts',
				xtype: 'order-account-list-grid',
				instructions: 'This section allows browsing and filtering all accounts (Client and Holding Accounts) defined in the system.'
			},


			{
				title: 'Clients',
				items: [{
					xtype: 'order-company-list-grid',
					categoryHierarchyName: 'Client',
					instructions: 'This section allows browsing and filtering all client companies defined in the system.',
					columnOverrides: [
						{dataIndex: 'bloombergCompanyIdentifier', hidden: true},
						{dataIndex: 'dtcNumber', hidden: true},
						{dataIndex: 'legalEntityIdentifier', hidden: true},
						{dataIndex: 'countryOfRisk.text', hidden: true}
					]
				}]
			},


			{
				title: 'Account Types',
				items: [{
					xtype: 'system-list-item-grid',
					instructions: 'Account Types are used to classify Accounts. Examples: Client, Custodian, Futures Broker, OTC ISDA, REPO Account, etc.',
					columnOverrides: [
						{dataIndex: 'value', hidden: true, header: 'Master Identifier', width: 50},
						{dataIndex: 'text', defaultSortColumn: true, defaultSortDirection: 'ASC', header: 'Account Type Name', width: 100},
						{dataIndex: 'tooltip', header: 'Description', width: 300},
						{dataIndex: 'order', hidden: true}
					],
					getSystemList: function () {
						return TCG.data.getData('systemListByName.json?name=Account Type', this, 'system.list.Account Type');
					}
				}]
			},


			{
				title: 'Platforms',
				items: [{
					xtype: 'system-list-item-grid',
					instructions: 'Account platforms are the Broker Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\' strategies.',
					columnOverrides: [
						{dataIndex: 'value', hidden: true, header: 'Master Identifier'},
						{dataIndex: 'text', defaultSortColumn: true, defaultSortDirection: 'ASC', header: 'Platform Name'},
						{dataIndex: 'tooltip', header: 'Description', width: 300},
						{dataIndex: 'order', hidden: true}
					],

					getSystemList: function () {
						return TCG.data.getData('systemListByName.json?name=Account Platform', this, 'system.list.Account Platform');
					}
				}]
			},


			{
				title: 'Business Services',
				items: [{
					xtype: 'system-hierarchy-grid',
					instructions: ' A service represents a product or strategy that we offer our clients. Services are assigned to accounts.',

					columnOverrides: [
						{dataIndex: 'value', hidden: true, header: 'Master Identifier'},
						{dataIndex: 'name', header: 'Service Name'}
					],

					getHierarchyCategory: function () {
						return TCG.data.getData('systemHierarchyCategoryByName.json?name=Account Service', this, 'system.hierarchy.category.Account Service');
					}
				}]
			},

			{
				title: 'Account Groups',
				items: [
					{
						xtype: 'system-group-list-grid',
						groupItemTableName: 'OrderAccount'
					}]
			}


		]
	}]
});

