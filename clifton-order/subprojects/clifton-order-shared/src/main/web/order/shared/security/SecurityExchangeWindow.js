Clifton.order.shared.security.SecurityExchangeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Exchange',
	iconCls: 'stock-chart',
	width: 800,
	height: 650,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Exchange',
				items: [{
					xtype: 'formpanel',
					labelWidth: 130,
					instructions: 'An exchange is a marketplace in which financial instruments are traded. Exchanges give companies, governments and other groups a platform to sell securities to the investing public and provides regulation to ensure fair and orderly trading, as well as efficient dissemination of price information for any securities trading on that exchange.  It may or may not be a physical location, but could also be an electronic platform.',
					url: 'orderSecurityExchange.json',
					readOnly: true,

					getWarningMessage: function (form) {
						let msg = undefined;
						if (TCG.isFalse(form.formValues.open)) {
							msg = 'This exchange is currently closed.';
						}
						return msg;
					},

					items: [
						{
							fieldLabel: 'Master ID',
							name: 'masterIdentifier',
							qtip: 'Exchange identifier from central Security Master that is unique to this exchange across all of our systems.'
						},
						{
							fieldLabel: 'Parent Exchange',
							name: 'parent.name',
							hiddenName: 'parent.id',
							xtype: 'combo',
							url: 'orderSecurityExchangeListFind.json',
							detailPageClass: 'Clifton.order.shared.security.SecurityExchangeWindow'
						},
						{fieldLabel: 'Exchange Name', name: 'name'},
						{fieldLabel: 'Exchange Code', name: 'exchangeCode'},
						{fieldLabel: 'Market Identifier Code', name: 'marketIdentifierCode', qtip: 'ISO 10383 Market Identifier Code (MIC)'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Composite Exchange',
							name: 'compositeExchange',
							xtype: 'checkbox',
							boxLabel: 'Used to calculate average or standardized prices for a security across all exchanges where it trades.'
						},
						{
							fieldLabel: 'Base Currency',
							name: 'baseCurrencyCode',
							xtype: 'linkfield',
							detailIdField: 'baseCurrencyCode',
							detailPageClass: 'Clifton.order.shared.security.CurrencySecurityByTickerWindow'
						},
						{
							fieldLabel: 'Country',
							name: 'country.text',
							hiddenName: 'country.id',
							xtype: 'combo',
							displayField: 'text',
							tooltipField: 'tooltip',
							url: 'systemListItemListFind.json?listName=Countries'
						},
						{
							fieldLabel: 'Calendar',
							name: 'calendar.name',
							hiddenName: 'calendar.id',
							xtype: 'combo',
							url: 'calendarListFind.json',
							detailPageClass: 'Clifton.calendar.setup.CalendarWindow'
						},
						{fieldLabel: 'Time Zone', name: 'timeZone.label', hiddenName: 'timeZone.id', displayField: 'label', xtype: 'combo', url: 'calendarTimeZoneListFind.json'},

						{
							fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
							defaults: {
								xtype: 'displayfield',
								flex: 1
							},
							items: [
								{value: 'Exchange Time Zone'},
								{name: 'localTimeZoneLabel'}
							]
						},
						{
							fieldLabel: 'Open Time',
							xtype: 'compositefield',
							qtip: 'The time when this Exchange opens for a full day of work: a day that is not full or partial holiday.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'openTime'},
								{name: 'openTimeLocal', submitValue: false, disabled: true}
							]
						},
						{
							fieldLabel: 'Close Time',
							xtype: 'compositefield',
							qtip: 'The time when this Exchange closes for a full day of work: a day that is not full or partial holiday.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'closeTime'},
								{name: 'closeTimeLocal', submitValue: false, disabled: true}
							]
						},
						{
							fieldLabel: 'Partial Day Open Time',
							xtype: 'compositefield',
							qtip: 'The time when this Exchange opens on a Partial Holiday: corresponding Calendar has a holiday on this day and the Holiday is NOT marked as Full Day.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'openTimePartialDay'},
								{name: 'openTimePartialDayLocal', submitValue: false, disabled: true}
							]
						},
						{
							fieldLabel: 'Partial Day Close Time',
							xtype: 'compositefield',
							qtip: 'The time when this Exchange closes on a Partial Holiday: corresponding Calendar has a holiday on this day and the Holiday is NOT marked as Full Day.',
							defaults: {
								xtype: 'timefield',
								flex: 1
							},
							items: [
								{name: 'closeTimePartialDay'},
								{name: 'closeTimePartialDayLocal', submitValue: false, disabled: true}
							]
						}
					]
				}]
			},


			{
				title: 'Securities',
				items: [{
					name: 'orderSecurityListFind',
					xtype: 'gridpanel',
					instructions: 'The following securities are traded on selected exchange.',
					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{
							header: 'Security Hierarchy',
							width: 80,
							dataIndex: 'securityHierarchy.nameExpanded',
							hidden: true,
							filter: {
								searchFieldName: 'securityHierarchyOrParentId',
								type: 'combo',
								url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Security Hierarchy',
								displayField: 'nameExpanded',
								queryParam: 'nameExpanded',
								listWidth: 500
							}
						},
						{header: 'Security Name', width: 100, dataIndex: 'name'},
						{header: 'Ticker', width: 75, dataIndex: 'ticker', defaultSortColumn: true},
						{
							header: 'Underlying Security',
							width: 100,
							dataIndex: 'underlyingSecurity.label',
							filter: {searchFieldName: 'underlyingSecurityId', type: 'combo', url: 'orderSecurityListFind.json'}
						},
						{header: 'Security Description', width: 200, dataIndex: 'description', hidden: true},
						{
							header: 'CCY Denomination',
							width: 70,
							dataIndex: 'currencyCode',
							filter: {type: 'combo', url: 'orderSecurityListFind.json?securityTypeName=CURRENCY', valueField: 'ticker', displayField: 'ticker'}
						},
						{header: 'Multiplier', width: 50, dataIndex: 'priceMultiplier', type: 'float'}
					],
					editor: {
						detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
						drillDownOnly: true
					},
					getLoadParams: function () {
						return {
							primaryOrCompositeExchangeId: this.getWindow().getMainFormId()
						};
					}
				}]
			},


			{
				title: 'Holidays',
				items: [{
					name: 'calendarHolidayDayListFind',
					xtype: 'gridpanel',
					instructions: 'The following holidays are defined for this Calendar of this Exchange.',
					importTableName: 'CalendarHolidayDay',
					getLoadParams: function (firstLoad) {
						const calendarId = this.getWindow().getMainFormPanel().getFormValue('calendar.id');
						if (TCG.isBlank(calendarId)) {
							return false;
						}
						if (firstLoad) {
							this.setFilterValue('day.startDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {'calendarId': calendarId};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holiday', width: 150, dataIndex: 'holiday.name'},
						{header: 'Day', width: 100, dataIndex: 'day.startDate', filter: {searchFieldName: 'startDate'}, defaultSortColumn: true},
						{header: 'Weekday', width: 100, dataIndex: 'day.weekday.name'},
						{header: 'Year', width: 70, dataIndex: 'day.year.year', type: 'int', doNotFormat: true},
						{
							header: 'Trade',
							width: 70,
							dataIndex: 'tradeHoliday',
							type: 'boolean',
							tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a trading day holiday.'
						},
						{
							header: 'Settle',
							width: 70,
							dataIndex: 'settlementHoliday',
							type: 'boolean',
							tooltip: 'Most holidays are just holidays. However, investment management industry can differentiate between Trading and Settlement holidays: <li>Trading: the specified market is closed for trading; therefore, price settlement data are not published. Note: in cases where electronic trading schedules are supplied separately from floor trading, the latter should be used to determine business and settlement data holidays)</li><li>Settlement: relates to cash settlement and clearing of trades; settlement of trades may not be possible because banks are closed, clearing agency is closed, specified market is close, or some combination of the above.</li><p>When applicable, use this field to determine if it is a settlement day holiday.'
						},
						{
							header: 'Full Day',
							width: 70,
							dataIndex: 'fullDayHoliday',
							type: 'boolean',
							tooltip: 'Most holidays are full day holidays.  However, some investment exchanges may have partial day holidays. In case of a partial day holiday, partial day start/end times should be used in order to determine if the exchange is open.'
						}
					]
				}]
			}
		]
	}]
});
