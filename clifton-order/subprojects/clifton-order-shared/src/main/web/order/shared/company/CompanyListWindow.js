Clifton.order.shared.company.CompanyListWindow = Ext.extend(TCG.app.Window, {
	id: 'orderCompanyListWindow',
	title: 'Companies',
	iconCls: 'business',
	width: 1500,
	height: 700,


	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Companies',
				items: [{
					xtype: 'order-company-list-grid',
					instructions: 'This section allows browsing and filtering all companies (Brokers, Custodians, Counterparties, Security Issuers, Clients...) defined in the system.'
				}]
			},


			{
				title: 'Parent Relationship Types',
				items: [{
					xtype: 'system-list-item-grid',
					instructions: 'Used to indicate for a company, what type of relationship it has to its parent.',
					columnOverrides: [
						{dataIndex: 'value', hidden: true, header: 'Master Identifier'},
						{dataIndex: 'text', defaultSortColumn: true, defaultSortDirection: 'ASC', header: 'Relationship Type Name'},
						{dataIndex: 'tooltip', header: 'Description', width: 300},
						{dataIndex: 'order', hidden: true}
					],

					getSystemList: function() {
						return TCG.data.getData('systemListByName.json?name=Company Parent Relationship Type', this, 'system.list.Company Parent Relationship Type');
					}
				}]
			}

		]
	}]
});

