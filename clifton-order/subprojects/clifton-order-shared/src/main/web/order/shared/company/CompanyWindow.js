Clifton.order.shared.company.CompanyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Company',
	width: 900,
	height: 700,
	iconCls: 'business',
	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.order.shared.company.OrderCompanyWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'orderCompany.json',
					readOnly: true,
					labelWidth: 160,

					listeners: {
						afterload: function(formPanel) {
							if (TCG.isBlank(formPanel.getFormValue('parent.name'))) {
								formPanel.hideField('nameExpanded');
								formPanel.hideField('parent.name');
								formPanel.hideField('parentRelationshipType.text');
							}
							formPanel.hideFieldIfBlank('obligorCompany.name');
							formPanel.hideFieldIfBlank('parentObligorCompany.name');
						}
					},

					items: [
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Bloomberg Company ID', name: 'bloombergCompanyIdentifier', qtip: 'Bloomberg Company Identifier that uniquely identifies each company on Bloomberg and can be used to link our companies to Bloomberg data.'}
								],
								config: {columnWidth: 0.5}
							}, {
								rows: [
									{fieldLabel: 'Master Identifier', name: 'masterIdentifier', qtip: 'Company identifier from central Company Master that is unique to this company across all of our systems.'}
								],
								config: {columnWidth: 0.5}
							}]
						},
						{xtype: 'label', html: '<hr />'},

						{fieldLabel: 'Expanded Name', name: 'nameExpanded', disabled: true, qtip: 'Full corporate structure all the way up to the ultimate parent.'},
						{fieldLabel: 'Parent Company', name: 'parent.name', detailIdField: 'parent.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow'},
						{fieldLabel: 'Relationship to Parent', name: 'parentRelationshipType.text'},
						{fieldLabel: 'Obligor Company', name: 'obligorCompany.name', detailIdField: 'obligorCompany.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', qtip: 'Bloomberg number that identifies the obligor, which is the immediate resource of payment for a company.'},
						{fieldLabel: 'Parent Obligor Company', name: 'parentObligorCompany.name', detailIdField: 'parentObligorCompany.id', xtype: 'linkfield', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', submitValue: false},
						{fieldLabel: 'Company Name', name: 'name', qtip: 'The official long name of the company, index, or other entity.'},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Alias', name: 'alias', qtip: '"Friendly" company name instead of legal name. For example, "GS - REDI Stocks Only" for "Goldman Sachs Execution & Clearing".'},
									{fieldLabel: 'Abbreviation', name: 'abbreviation', qtip: 'Abbreviations are used to standardize account names, security names (Swaps), etc.', maxLength: 4}
								]
							}, {
								rows: [
									{fieldLabel: 'Company Corporate Ticker', name: 'companyCorporateTicker', qtip: '\'Corp\' ticker (fixed income) for publicly traded companies.', anchor: '-19'},
									{fieldLabel: 'Ultimate Parent Equity Ticker', name: 'ultimateParentEquityTicker', qtip: '\'Equity\' Ticker and exchange of the ultimate parent company (highest level in organization chain).', anchor: '-19'}
								]
							}]
						},
						{boxLabel: 'Ultimate Parent (top most) for all child companies/subsidiaries', name: 'ultimateParent', xtype: 'checkbox'},
						{boxLabel: 'Private Company (not traded on a public stock exchange)', name: 'privateCompany', xtype: 'checkbox'},
						{boxLabel: 'Acquired By Parent Company', name: 'acquiredByParent', xtype: 'checkbox'},

						{xtype: 'label', html: '<hr />'},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Legal Entity Identifier (LEI)', name: 'legalEntityIdentifier', qtip: 'A 20-character, alpha-numeric code that uniquely identifies legally distinct entities that engage in financial transactions.'},
									{fieldLabel: 'LEI Entity Start Date', name: 'leiEntityStartDate', xtype: 'datefield', allowBlank: true, qtip: 'Date on which the legal entity identifier (LEI) was first assigned by the registration authority.'}
								]
							}, {
								rows: [
									{fieldLabel: 'LEI Entity Status', name: 'leiEntityStatus', qtip: 'Current status of the entity for the legal entity identifier (LEI), either \'ACTIVE\' or \'INACTIVE\'.', anchor: '-19'},
									{fieldLabel: 'LEI Entity End Date', name: 'leiEntityEndDate', xtype: 'datefield', allowBlank: true, qtip: 'Date on which the legal entity identifier (LEI) entered a disabled status.', anchor: '-19'}
								]
							}]
						},

						{xtype: 'label', html: '<hr />'},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'State Of Incorporation', name: 'stateOfIncorporation.labelLong', qtip: 'The State Code of the company\'s state of incorporation for Australia, Canada, Japan, and the United States.'},
									{fieldLabel: 'Country Of Incorporation', name: 'countryOfIncorporation.labelLong', qtip: 'Specifies the ISO (International Organization for Standardization) country code of where a company is incorporated. Supranational (SNAT) will be returned for entities formed by multiple governments and without a single country of incorporation. Multinational (MULT) will be returned for companies incorporated in multiple jurisdictions (countries). Multinational companies that are physically incorporated in a single country will return the ISO country code of that country. Multi-issuers will return \'MULT\' when the individual entities that make up the Multi-issuer are incorporated in different countries.'},
									{fieldLabel: 'Country Of Risk', name: 'countryOfRisk.labelLong', qtip: 'International Organization for Standardization (ISO) country code of the issuer\'s country of risk. Methodology consists of four factors listed in order of importance: management location, country of primary listing, country of revenue and reporting currency of the issuer. Management location is defined by country of domicile unless location of such key players as Chief Executive Officer (CEO), Chief Financial Officer (CFO), Chief Operating Officer (COO), and/or General Counsel is proven to be otherwise.'}
								]
							}, {
								rows: [
									{fieldLabel: 'Business Identifier Code', name: 'businessIdentifierCode', qtip: 'A BIC is composed of a 4-character bank code, 2-character country code 2-character location code and optional 3 character branch code.  It is used for addressing messages, routing business transactions, and identifying business parties.', anchor: '-19'},
									{fieldLabel: 'DTC Number', name: 'dtcNumber', qtip: 'The DTC number is a number that is issued in connection with Depository Trust Company for settlement services.', anchor: '-19'},
									{fieldLabel: 'FED Mnemonic', name: 'fedMnemonic', qtip: 'A company\'s standardized account at the Fed.', anchor: '-19'}
								]
							}]
						},

						{xtype: 'label', html: '<hr />'},

						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'OrderCompany',
							hierarchyCategoryName: 'Company Tags'
						}
					]
				}]
			},


			{
				title: 'Subsidiaries',
				items: [{
					name: 'orderCompanyListFind',
					xtype: 'gridpanel',
					instructions: 'A list of child companies for this parent company.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Master ID', width: 30, hidden: true, dataIndex: 'masterIdentifier', type: 'int', doNotFormat: true, useNull: true},
						{header: 'Bloomberg ID', width: 35, hidden: true, dataIndex: 'bloombergCompanyIdentifier', type: 'int', doNotFormat: true, useNull: true, qtip: 'Bloomberg Company Identifier'},

						{header: 'Relationship to Parent', width: 80, dataIndex: 'parentRelationshipType.text', filter: {type: 'combo', searchFieldName: 'parentRelationshipTypeId', xtype: 'system-list-combo', listName: 'Company Parent Relationship Type', valueField: 'id'}, hidden: true},

						{header: 'Is Ultimate Parent', width: 70, dataIndex: 'ultimateParent', type: 'boolean', hidden: true},
						{header: 'Is Private Company', width: 70, dataIndex: 'privateCompany', type: 'boolean', hidden: true},
						{header: 'Acquired By Parent', width: 70, dataIndex: 'acquiredByParent', type: 'boolean', hidden: true},

						{header: 'Parent Company', width: 70, dataIndex: 'parent.name', filter: {type: 'combo', searchFieldName: 'parentId', url: 'orderCompanyListFind.json'}, hidden: true},
						{header: 'Obligor Company', width: 70, dataIndex: 'obligorCompany.name', filter: {type: 'combo', searchFieldName: 'obligorCompanyId', url: 'orderCompanyListFind.json'}, hidden: true},
						{header: 'Parent Obligor Company', width: 100, dataIndex: 'parentObligorCompany.name', filter: {type: 'combo', searchFieldName: 'parentObligorCompanyId', url: 'orderCompanyListFind.json'}, hidden: true},

						{header: 'Company Name', width: 100, dataIndex: 'name', defaultSortColumn: true},  // Uses Toolbar Filter
						{header: 'Alias', width: 60, dataIndex: 'alias', filter: {searchFieldName: 'alias'}, hidden: true},
						{header: 'Abbreviation', width: 40, dataIndex: 'abbreviation', filter: {searchFieldName: 'abbreviation'}},
						{header: 'FED Mnemonic', width: 50, dataIndex: 'fedMnemonic', hidden: true},
						{header: 'Business Identifier Code', width: 75, dataIndex: 'businessIdentifierCode', hidden: true},
						{header: 'DTC Number', width: 50, dataIndex: 'dtcNumber'},

						{header: 'Legal Entity Identifier (LEI)', width: 60, dataIndex: 'legalEntityIdentifier'},
						{header: 'LEI Entity Status', width: 60, dataIndex: 'leiEntityStatus', hidden: true},
						{header: 'LEI Entity Start Date', width: 75, dataIndex: 'leiEntityStartDate', hidden: true},
						{header: 'LEI Entity End Date', width: 75, dataIndex: 'leiEntityEndDate', hidden: true},

						{header: 'Company Corporate Ticker', width: 80, dataIndex: 'companyCorporateTicker', hidden: true},
						{header: 'Ultimate Parent Equity Ticker', width: 80, dataIndex: 'ultimateParentEquityTicker', hidden: true},

						{header: 'State Of Incorporation', width: 50, dataIndex: 'stateOfIncorporation.text', filter: {searchFieldName: 'stateOfIncorporation'}, hidden: true},
						{header: 'Country Of Incorporation', width: 50, dataIndex: 'countryOfIncorporation.text', filter: {searchFieldName: 'countryOfIncorporationId', type: 'combo', xtype: 'system-list-combo', listName: 'Countries', valueField: 'id'}, hidden: true},
						{header: 'Country Of Risk', width: 45, dataIndex: 'countryOfRisk.text', filter: {searchFieldName: 'countryOfRiskId', type: 'combo', xtype: 'system-list-combo', listName: 'Countries', valueField: 'id'}}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {parentId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.order.shared.company.CompanyWindow'
					}
				}]
			},


			{
				title: 'Accounts',
				items: [{
					name: 'orderAccountListFind',
					xtype: 'gridpanel',
					instructions: 'A list of accounts where the company either issued the account or is the client company of the account.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Master ID', width: 30, hidden: true, dataIndex: 'masterIdentifier', type: 'int', doNotFormat: true, useNull: true},
						{header: 'Client Name', width: 100, dataIndex: 'clientCompany.name', hidden: true, filter: {searchFieldName: 'clientCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Client'}},
						{header: 'Account #', width: 50, dataIndex: 'accountNumber'},
						{header: 'Account # 2', width: 50, dataIndex: 'accountNumber2', hidden: true},
						{header: 'Account Short Name', width: 75, dataIndex: 'accountShortName'},
						{header: 'Account Name', width: 125, dataIndex: 'accountName'},

						{header: 'Account Type', width: 50, dataIndex: 'accountType.text', filter: {searchFieldName: 'accountTypeId', type: 'combo', xtype: 'system-list-combo', listName: 'Account Type', valueField: 'id'}, hidden: true},
						{header: 'Issuing Company', width: 75, dataIndex: 'issuingCompany.name', filter: {searchFieldName: 'issuingCompanyId', type: 'combo', url: 'orderCompanyListFind.json'}, hidden: true},

						{header: 'Workflow State', width: 50, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateId', type: 'combo', url: 'workflowStateListFind.json?workflowName=Account Status'}},
						{header: 'Workflow Status', width: 50, dataIndex: 'workflowState.status.name', hidden: true, filter: {searchFieldName: 'workflowStatusId', type: 'combo', url: 'workflowStateListFind.json?assignmentTableName=OrderAccount'}},

						{header: 'PM Team', width: 50, dataIndex: 'teamSecurityGroup.name', hidden: true, filter: {searchFieldName: 'teamSecurityGroupId', type: 'combo', xtype: 'system-security-group-combo', groupTagName: 'PM Team'}},
						{header: 'Portfolio Manager', width: 50, dataIndex: 'portfolioManagerUser.displayName', filter: {searchFieldName: 'portfolioManagerUserId', type: 'combo', xtype: 'system-security-user-group-combo', groupTagName: 'PM Team'}, hidden: true},
						{header: 'Business Service', width: 80, dataIndex: 'service.name', hidden: true, filter: {searchFieldName: 'serviceOrParentId', type: 'combo', url: 'systemHierarchyListFind.json?ignoreNullParent=true&categoryName=Account Service', displayField: 'nameExpanded', queryParam: 'nameExpanded', listWidth: 500}, viewNames: ['Client Accounts']},

						{header: 'Inception Date', width: 50, dataIndex: 'inceptionDate', hidden: true},

						{header: 'Base CCY', width: 35, dataIndex: 'baseCurrencyCode'},
						{header: 'FX Source', width: 100, dataIndex: 'defaultExchangeRateSourceCompany.name', hidden: true, filter: {searchFieldName: 'defaultExchangeRateSourceCompanyId', type: 'combo', url: 'orderCompanyListFind.json'}},


						{header: 'Client Directed', width: 50, dataIndex: 'clientDirected', type: 'boolean', hidden: true},
						{header: 'Platform', width: 50, dataIndex: 'companyPlatform.text', hidden: true, filter: {searchFieldName: 'companyPlatformId', type: 'combo', xtype: 'system-list-combo', listName: 'Account Platform', valueField: 'id'}},
						{header: 'Wrap Account', width: 50, dataIndex: 'wrapAccount', type: 'boolean', hidden: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {clientCompanyOrIssuingCompanyId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true, // allow deletes or adds?
						detailPageClass: 'Clifton.order.shared.account.AccountWindow'
					}
				}]
			}
		]
	}]
});
