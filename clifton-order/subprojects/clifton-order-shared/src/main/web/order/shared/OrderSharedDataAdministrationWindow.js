Clifton.order.shared.OrderSharedDataAdministrationWindow = Ext.extend(TCG.app.Window, {
	id: 'orderSharedDataAdministrationWindow',
	title: 'Master Data Sync Administration',
	iconCls: 'stock-chart',
	width: 1500,
	height: 700,
	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 140,
						loadValidation: false, // using the form only to get background color/padding
						height: 230,
						buttonAlign: 'right',
						instructions: 'Use this section to import or synchronize shared data from master system(s). Optionally select criteria below to limit what data to synchronize.',
						items: [
							{
								xtype: 'columnpanel',
								columns: [
									{
										rows: [
											{fieldLabel: 'Weekdays Back', xtype: 'spinnerfield', name: 'weekdaysBack', minValue: 1, maxValue: 90, qtip: 'This option allows filtering data to sync to those updated in the master system on or after selected weekdays back.  Enter as a positive value.'},
											{boxLabel: 'Skip Shared Look up Data', name: 'skipSharedLookupData', xtype: 'checkbox', qtip: 'By default, prior to all syncs, we also look for any updates to shared look up data (security types, account types, etc.)'}
										],
										config: {columnWidth: 0.30}
									},
									{
										rows: [
											{boxLabel: 'Process Company Data', name: 'populateCompanyData', xtype: 'checkbox', qtip: 'Check to include synchronization of Order Company data.'},
											{fieldLabel: 'Company Name', name: 'companyName', xtype: 'textfield', qtip: 'If populated, will limit to a single company with given name.', requiredFields: ['populateCompanyData']},
											{boxLabel: 'Process Account Data', name: 'populateAccountData', xtype: 'checkbox', qtip: 'Check to include synchronization of Order Account data.'},
											{fieldLabel: 'Account Short Name', name: 'accountShortName', xtype: 'textfield', qtip: 'If populated, will limit to a single account with given short name.', requiredFields: ['populateAccountData']},
											{boxLabel: 'Process Security Data', name: 'populateSecurityData', xtype: 'checkbox', qtip: 'Check to include synchronization of Order Security data.'},
											{fieldLabel: 'Security Ticker', name: 'securityTicker', xtype: 'textfield', qtip: 'If populated, will limit to a single security with given ticker.', requiredFields: ['populateSecurityData']}
										],
										config: {columnWidth: 0.70}
									}
								]
							}
						],

						buttons: [{
							text: 'Run Data Sync',
							iconCls: 'run',
							width: 150,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'orderSharedDataListForCommandPopulate.json',
									waitMsg: 'Processing...',
									success: function(form, action) {
										Ext.Msg.alert('Processing Started', action.result.status.message, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload.defer(300, grid);
										});
									}
								}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
							}
						}]
					},
					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'ORDER-SHARED-DATA-POPULATE',
						instantRunner: true,
						instructions: 'The following data syncs are being processed right now, or having recently completed.',
						title: 'Data Synchronization',
						columnOverrides: [{dataIndex: 'type', hidden: true}],
						flex: 1
					}
				]
			},

			{
				title: 'Import Queries',
				xtype: 'system-query-grid',
				getTopToolbarInitialLoadParams: function(firstLoad) {
					const returnData = {
						category2Name: 'System Query Tags',
						category2HierarchyName: 'Order Shared Import Queries'
					};
					const tag = TCG.getChildByName(this.getTopToolbar(), 'queryTagHierarchyId');
					if (TCG.isNotBlank(tag.getValue())) {
						returnData.categoryName = 'System Query Tags';
						returnData.categoryHierarchyId = tag.getValue();

					}
					return returnData;
				}
			}
		]
	}]
});
