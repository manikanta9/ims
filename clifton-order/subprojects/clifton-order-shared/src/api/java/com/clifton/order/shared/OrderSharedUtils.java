package com.clifton.order.shared;

/**
 * @author manderson
 */
public class OrderSharedUtils {

	public static final String SECURITY_TYPE_BONDS = "Bonds";
	public static final String SECURITY_TYPE_CURRENCY = "Currency";
	public static final String SECURITY_TYPE_FORWARDS = "Forwards";
	public static final String SECURITY_TYPE_FUTURES = "Futures";
	public static final String SECURITY_TYPE_BENCHMARKS = "Benchmarks";
	public static final String SECURITY_TYPE_OPTIONS = "Options";
	public static final String SECURITY_TYPE_STOCKS = "Stocks";
	public static final String SECURITY_TYPE_FUNDS = "Funds";
	public static final String SECURITY_TYPE_NOTES = "Notes";
	public static final String SECURITY_TYPE_SWAPS = "Swaps";
	public static final String SECURITY_TYPE_SWAPTIONS = "Swaptions";
	public static final String SECURITY_TYPE_INDEX = "Index";
	public static final String SECURITY_TYPE_CASH = "Cash";
}
