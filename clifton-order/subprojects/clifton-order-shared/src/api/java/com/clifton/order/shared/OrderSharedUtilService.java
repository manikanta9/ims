package com.clifton.order.shared;

import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;


/**
 * The <code>OrderSharedUtilService</code> defines utility methods that can be used for Order Shared objects.
 * <p>
 *
 * @author manderson
 */
public interface OrderSharedUtilService {


	/**
	 * Returns today if the exchange is currently open. If it's currently closed, it will return the next TRADE business day from today using exchange's calendar.
	 * <p>
	 * Uses TRADE Business Days
	 */
	@ResponseBody
	@SecureMethod(dtoClass = OrderSecurity.class)
	public Date getOrderSecurityTradeDate(int securityId);


	/**
	 * Returns the next available trade date from the given trade date.
	 * i.e. if Trade Date = today, will check tomorrow's date, if a valid trading business day will return tomorrow, if not will move again to the next calendar day
	 * This method does not use time check if the exchange is currently closed
	 */
	@ResponseBody
	@SecureMethod(dtoClass = OrderSecurity.class)
	public Date getOrderSecurityTradeDateNext(int securityId, Date tradeDate);


	/**
	 * Returns settlement date based on the specified transaction date (trade date) for the specified security.
	 * Optional "settlementCurrencyId" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 * <p>
	 * Uses SETTLEMENT Business Days
	 */
	@ResponseBody
	@SecureMethod(dtoClass = OrderSecurity.class)
	public Date getOrderSecuritySettlementDate(int securityId, Integer settlementCurrencyId, Date transactionDate);


	/**
	 * Returns the next available settlement date from the given settlement date.
	 * i.e. if Settlement Date = tomorrow, will check the following day's date.  If a valid settlement business day will return that date, if not will move again to the next calendar day
	 * Optional "settlementCurrencyId" parameter is required for spot currency trades that have settlement cycle
	 * defined per currency pair.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = OrderSecurity.class)
	public Date getOrderSecuritySettlementDateNext(int securityId, Integer settlementCurrencyId, Date settlementDate);
}
