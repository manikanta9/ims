package com.clifton.order.shared.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.system.group.search.SystemGroupAwareSearchForm;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
public class OrderSecuritySearchForm extends BaseAuditableEntitySearchForm implements SystemGroupAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] idList;

	@SearchField
	private Integer masterIdentifier;

	@SearchField(searchField = "currencyCode", comparisonConditions = ComparisonConditions.EQUALS)
	private String currencyCode;

	@SearchField(searchField = "name,ticker,cusip,isin,sedol,sedol2,occSymbol,cins,apir,bloombergGlobalIdentifier,compositeBloombergGlobalIdentifier")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String ticker;

	@SearchField(searchField = "ticker", comparisonConditions = ComparisonConditions.EQUALS)
	private String tickerEquals;

	@SearchField
	private String cusip;

	@SearchField
	private String isin;

	@SearchField
	private String sedol;

	@SearchField
	private String sedol2;

	@SearchField
	private String occSymbol;

	@SearchField
	private String cins;

	@SearchField
	private String apir;

	@SearchField
	private String bloombergGlobalIdentifier;

	@SearchField
	private String compositeBloombergGlobalIdentifier;

	@SearchField
	private BigDecimal priceMultiplier;

	@SearchField(searchField = "underlyingSecurity.id", sortField = "underlyingSecurity.ticker")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "company.id", sortField = "company.name")
	private Integer companyId;

	@SearchField(searchField = "primaryExchange.id", sortField = "primaryExchange.name")
	private Short primaryExchangeId;

	@SearchField(searchField = "compositeExchange.id", sortField = "compositeExchange.name")
	private Short compositeExchangeId;

	@SearchField(searchField = "primaryExchange.id,compositeExchange.id")
	private Short primaryOrCompositeExchangeId;

	@SearchField(searchField = "securityHierarchy.id", sortField = "securityHierarchy.name")
	private Short securityHierarchyId;

	// Using 6 levels deep for now....
	@SearchField(searchField = "securityHierarchy.id,securityHierarchy.parent.id,securityHierarchy.parent.parent.id,securityHierarchy.parent.parent.parent.id,securityHierarchy.parent.parent.parent.id,securityHierarchy.parent.parent.parent.id", leftJoin = true, sortField = "name")
	private Short securityHierarchyOrParentId;

	@SearchField(searchField = "securityType.id", sortField = "securityType.text")
	private Integer securityTypeId;

	@SearchField(searchField = "bloombergSecurityType.id", sortField = "bloombergSecurityType.text")
	private Integer bloombergSecurityTypeId;

	@SearchField(searchField = "securityTypeSubType.id", sortField = "securityTypeSubType.text")
	private Integer securityTypeSubTypeId;

	@SearchField(searchField = "securityTypeSubType2.id", sortField = "securityTypeSubType2.text")
	private Integer securityTypeSubType2Id;

	@SearchField(searchField = "countryOfRisk.id", sortField = "countryOfRisk.text")
	private Integer countryOfRiskId;

	@SearchField(searchField = "securityType.text", comparisonConditions = ComparisonConditions.EQUALS)
	private String securityType;

	@SearchField(searchField = "securityType.text", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeSecurityType;

	@SearchField(searchField = "settlementCalendar.id", sortField = "settlementCalendar.name")
	private Short settlementCalendarId;

	@SearchField
	private Short daysToSettle;

	@SearchField
	private Boolean active;

	@SearchField
	private Boolean tradable;

	@SearchField
	private Date startDate;

	@SearchField
	private Date endDate;

	@SearchField
	private Date masterRecordCreateDate;

	@SearchField
	private Date masterRecordUpdateDate;

	// Custom Search fields applied in SystemGroup based on interface SystemGroupAwareSearchForm
	private Integer systemGroupId;

	private Integer excludeSystemGroupId;
}
