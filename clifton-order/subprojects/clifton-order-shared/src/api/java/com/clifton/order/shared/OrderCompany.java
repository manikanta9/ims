package com.clifton.order.shared;

import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.model.ModelAware;
import com.clifton.order.api.server.model.CompanyModel;
import com.clifton.system.list.SystemListItem;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
public class OrderCompany extends NamedHierarchicalEntity<OrderCompany, Integer> implements ModelAware<CompanyModel> {

	public static final String TABLE_NAME = "OrderCompany";

	public static final String COMPANY_TAGS_HIERARCHY_CATEGORY_NAME = "Company Tags";
	public static final String COMPANY_TAGS_CLIENT_HIERARCHY_NAME = "Client";
	public static final String COMPANY_TAGS_CUSTODIAN_HIERARCHY_NAME = "Custodian";
	public static final String COMPANY_TAGS_BROKER_HIERARCHY_NAME = "Broker";


	/**
	 * Company identifier from central Company Master that is unique to this company across all of our systems.
	 */
	private Integer masterIdentifier;

	/**
	 * Bloomberg Company Identifier that uniquely identifies each company on Bloomberg and can be used to link our companies to Bloomberg data.
	 */
	private Integer bloombergCompanyIdentifier;


	private SystemListItem parentRelationshipType;

	/**
	 * Explicitly designated Obligor.  Bloomberg number that identifies the obligor, which is the immediate resource of payment for a company.
	 */
	private OrderCompany obligorCompany;


	/**
	 * "Friendly" company name instead of legal name. For example, "GS - REDI Stocks Only" for "Goldman Sachs Execution & Clearing".
	 */
	private String alias;


	/**
	 * Abbreviation for the company, i.e. BAR for Barclays
	 * These abbreviations can be used to standardize account names, security names (swaps) etc.
	 */
	private String abbreviation;


	/**
	 * A BIC is composed of a 4-character bank code, 2-character country code 2-character location code and optional 3 character branch code.
	 */
	private String businessIdentifierCode;

	/**
	 * The DTC number is a number that is issued in connection with Depository Trust Company for settlement services.
	 */
	private String dtcNumber;

	/**
	 * A company's standardized account at the Fed.
	 */
	private String fedMnemonic;


	/**
	 * Indicates if the company has a parent.  Returns 'Y' if the company is the ultimate parent, 'N' if the company has a parent or 'N.A.' if the parent
	 * information has not been verified.
	 */
	private boolean ultimateParent;

	/**
	 * Indicates if the company is presently a private entity, not publicly traded.
	 */
	private boolean privateCompany;

	private boolean acquiredByParent;


	/**
	 * We currently use it to identify our SEF sponsor when sending Cleared CDS orders via FIX.
	 * Legal entity identifier (LEI) displayed as a 20-character, alpha-numeric code that uniquely identifies legally distinct entities that engage in financial transactions.
	 * Used for reporting and other regulatory purposes. This was first mandated by the Dodd-Frank Act.
	 */
	private String legalEntityIdentifier;

	/**
	 * Current status of the entity for the legal entity identifier (LEI), either 'ACTIVE' or 'INACTIVE'.
	 * An active status represents an entity with an independent legal status which includes in administration, in receivership, bankruptcy, and other legal statuses.
	 * An inactive status represents an entity which no longer has an independent legal status as a result of dissolution.
	 */
	private String leiEntityStatus;

	/**
	 * Date on which the legal entity identifier (LEI) was first assigned by the registration authority.
	 */
	private Date leiEntityStartDate;

	/**
	 * Date on which the legal entity identifier (LEI) entered a disabled status.
	 */
	private Date leiEntityEndDate;


	private SystemListItem stateOfIncorporation;
	private SystemListItem countryOfIncorporation;
	private SystemListItem countryOfRisk;


	/**
	 * 'Corp' ticker (fixed income) for publicly traded companies.
	 */
	private String companyCorporateTicker;
	/**
	 * Ticker and exchange of the ultimate parent company (highest level in organization chain).
	 */
	private String ultimateParentEquityTicker;


	/**
	 * Used during import process so we know which tags to apply to the company
	 */
	@NonPersistentField
	private String companyTags;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CompanyModel toModelObject() {
		CompanyModel companyModel = new CompanyModel();
		companyModel.setMasterIdentifier(getMasterIdentifier());
		companyModel.setBloombergCompanyIdentifier(getBloombergCompanyIdentifier());
		companyModel.setBusinessIdentifierCode(getBusinessIdentifierCode());
		companyModel.setLegalEntityIdentifier(getLegalEntityIdentifier());
		companyModel.setName(this.getName());
		companyModel.setAbbreviation(getAbbreviation());
		companyModel.setAlias(getAlias());
		return companyModel;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculated-Inherited Obligor; the obligor of the nearest ancestor.
	 */
	public OrderCompany getParentObligorCompany() {
		if (getObligorCompany() != null) {
			return getObligorCompany();
		}
		return doRecurseParentObligor(getParent());
	}


	private OrderCompany doRecurseParentObligor(OrderCompany company) {
		if (company != null) {
			if (company.getObligorCompany() != null) {
				return company.getObligorCompany();
			}
			else {
				return doRecurseParentObligor(company.getParent());
			}
		}
		return null;
	}
}
