package com.clifton.order.shared;


import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.model.ModelAware;
import com.clifton.core.model.ModelAwareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.order.api.server.model.SecurityModel;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.list.SystemListItem;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
public class OrderSecurity extends BaseEntity<Integer> implements ModelAware<SecurityModel>, LabeledObject {


	/**
	 * Master Identifier - int? String ? Do we need to define a master system or should we have source system name and source system id
	 */
	private Integer masterIdentifier;


	private String ticker;
	private String cusip;
	/**
	 * International Securities Identification Number is a 12-digit alphanumeric code that uniquely identifies a specific security.
	 */
	private String isin;
	private String sedol;
	private String sedol2;
	/**
	 * A CINS number if an international extension of the CUSIP numbering system. It consists of 9 characters.
	 */
	private String cins;
	/**
	 * The Asia Pacific Investment Register (APIR) Code is a unique identifier for many financial products issued by APIR to participants and products within the Financial Services Industry. It is widely used in some countries: Australia...
	 */
	private String apir;
	private String occSymbol;
	private String bloombergGlobalIdentifier;
	private String compositeBloombergGlobalIdentifier;

	private String name;
	private String description;

	private BigDecimal priceMultiplier;


	private String currencyCode;

	private OrderSecurity underlyingSecurity;

	/**
	 * Issuer or Counterparty
	 */
	private OrderCompany company;


	private OrderSecurityExchange primaryExchange;
	private OrderSecurityExchange compositeExchange;

	private SystemHierarchy securityHierarchy;
	private SystemListItem securityType;
	private SystemListItem securityTypeSubType;
	private SystemListItem securityTypeSubType2;

	private SystemListItem bloombergSecurityType;

	private SystemListItem countryOfRisk;

	private Date startDate;
	private Date endDate;

	private Calendar settlementCalendar;
	private Short daysToSettle;

	private InvestmentNotionalCalculatorTypes costCalculator;

	private boolean active;
	private boolean tradable;

	private Date masterRecordCreateDate;
	private Date masterRecordUpdateDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityModel toModelObject() {
		SecurityModel model = new SecurityModel();
		model.setId(getId());
		model.setMasterIdentifier(getMasterIdentifier());
		model.setTicker(getTicker());
		model.setCusip(getCusip());
		model.setIsin(getIsin());
		model.setSedol(getSedol());
		model.setOccSymbol(getOccSymbol());
		model.setBloombergGlobalIdentifier(getBloombergGlobalIdentifier());
		model.setName(getName());
		model.setDescription(getDescription());
		model.setCurrencyCode(getCurrencyCode());
		model.setUnderlyingSecurity(ModelAwareUtils.getModelObject(getUnderlyingSecurity()));
		model.setCompany(ModelAwareUtils.getModelObject(getCompany()));
		model.setPrimaryExchangeCode(getPrimaryExchange() != null ? getPrimaryExchange().getExchangeCode() : null);
		model.setCompositeExchangeCode(getCompositeExchange() != null ? getCompositeExchange().getExchangeCode() : null);
		model.setSecurityHierarchy(getSecurityHierarchy() != null ? getSecurityHierarchy().getNameExpanded() : null);
		model.setSecurityType(getSecurityType() != null ? getSecurityType().getText() : null);
		model.setSecurityTypeSubType(getSecurityTypeSubType() != null ? getSecurityTypeSubType().getText() : null);
		model.setSecurityTypeSubType2(getSecurityTypeSubType2() != null ? getSecurityTypeSubType2().getText() : null);
		model.setCountryOfRisk(getCountryOfRisk() != null ? getCountryOfRisk().getText() : null);
		model.setStartDate(DateUtils.asLocalDate(getStartDate()));
		model.setEndDate(DateUtils.asLocalDate(getEndDate()));
		return model;
	}


	@Override
	public String getLabel() {
		String result = getName();
		if (this.ticker != null) {
			if (result == null) {
				result = this.ticker;
			}
			else if (!result.equalsIgnoreCase(this.ticker)) {
				result = this.ticker + " (" + result + ')';
			}
		}

		if (!isActive()) {
			result += " [INACTIVE]";
		}

		return result;
	}


	public boolean isCurrency() {
		return getSecurityType() != null && OrderSharedUtils.SECURITY_TYPE_CURRENCY.equalsIgnoreCase(getSecurityType().getText());
	}


	/**
	 * Sec Master has active flag, but in enhanced version will also have end date (at least) so keeping this method if end date is populated check that, else rely on active flag only
	 */
	public boolean isActiveOn(Date date) {
		if (getEndDate() != null) {
			return DateUtils.isDateBeforeOrEqual(date, getEndDate(), false);
		}
		return isActive();
	}
}
