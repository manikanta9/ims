package com.clifton.order.shared.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class OrderSecurityExchangeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,exchangeCode,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Integer masterIdentifier;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String exchangeCode;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String marketIdentifierCode;

	@SearchField(searchField = "marketIdentifierCode", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean marketIdentifierCodeNotNull;

	@SearchField
	private Boolean compositeExchange;

	@SearchField(searchField = "parent.id", sortField = "parent.name")
	private Short parentId;

	@SearchField
	private String baseCurrencyCode;

	@SearchField(searchField = "calendar.id", sortField = "calendar.name")
	private Short calendarId;

	@SearchField(searchField = "country.id")
	private Integer countryId;

	@SearchField(searchFieldPath = "country", searchField = "text")
	private String country;
}
