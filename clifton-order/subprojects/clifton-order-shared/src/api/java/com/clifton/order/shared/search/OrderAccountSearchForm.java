package com.clifton.order.shared.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.order.shared.OrderAccount;
import com.clifton.system.group.search.SystemGroupAwareSearchForm;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderAccountSearchForm extends BaseAuditableSystemHierarchyItemSearchForm implements SystemGroupAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] idList;

	@SearchField
	private Integer masterIdentifier;

	@SearchField(searchField = "accountNumber,accountNumber2,accountName,accountShortName")
	private String searchPattern;

	@SearchField(searchField = "accountShortName,accountNumber", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String labelShort;

	@SearchField(searchField = "clientCompany.id", sortField = "clientCompany.name")
	private Integer clientCompanyId;

	@SearchField(searchField = "clientCompany.id,issuingCompany.id", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Integer clientCompanyOrIssuingCompanyId;

	@SearchField
	private String accountNumber;

	@SearchField(searchField = "accountNumber", comparisonConditions = ComparisonConditions.EQUALS)
	private String accountNumberEquals;

	@SearchField
	private String accountNumber2;

	@SearchField
	private String accountName;

	@SearchField
	private String accountShortName;

	@SearchField(searchField = "accountShortName", comparisonConditions = ComparisonConditions.EQUALS)
	private String accountShortNameEquals;

	@SearchField
	private Boolean clientAccount;

	@SearchField
	private Boolean holdingAccount;

	@SearchField(searchField = "accountType.id", sortField = "accountType.text")
	private Integer accountTypeId;

	@SearchField(searchField = "issuingCompany.id", sortField = "issuingCompany.name")
	private Integer issuingCompanyId;

	@SearchField(searchField = "workflowState.id", sortField = "workflowState.name")
	private Short workflowStateId;

	@SearchField(searchFieldPath = "workflowState", searchField = "status.id", sortField = "status.name")
	private Short workflowStatusId;

	@SearchField(searchField = "teamSecurityGroup.id", sortField = "teamSecurityGroup.name")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "portfolioManagerUser.id", sortField = "portfolioManagerUser.displayName")
	private Short portfolioManagerUserId;

	@SearchField(searchField = "service.id", sortField = "service.name")
	private Short serviceId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "service.id,service.parent.id,service.parent.parent.id,service.parent.parent.parent.id", leftJoin = true, sortField = "service.name")
	private Short serviceOrParentId;

	@SearchField
	private Date inceptionDate;

	@SearchField
	private String baseCurrencyCode;

	@SearchField(searchField = "defaultExchangeRateSourceCompany.id", sortField = "defaultExchangeRateSourceCompany.name")
	private Integer defaultExchangeRateSourceCompanyId;

	@SearchField
	private Boolean clientDirected;

	@SearchField(searchField = "companyPlatform.id", sortField = "companyPlatform.text")
	private Integer companyPlatformId;

	@SearchField
	private Boolean wrapAccount;

	// Custom Search fields applied in SystemGroup based on interface SystemGroupAwareSearchForm
	private Integer systemGroupId;

	private Integer excludeSystemGroupId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return OrderAccount.TABLE_NAME;
	}
}
