package com.clifton.order.shared.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.order.shared.OrderCompany;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
public class OrderCompanySearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] idList;

	@SearchField
	private Integer masterIdentifier;

	@SearchField(searchField = "parent.id", sortField = "parent.name")
	private Integer parentId;

	@SearchField(searchField = "parentRelationshipType.id", sortField = "parentRelationshipType.text")
	private Integer parentRelationshipTypeId;

	@SearchField(searchField = "name,alias")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String abbreviation;

	@SearchField
	private String alias;

	@SearchField
	private String businessIdentifierCode;

	@SearchField
	private String dtcNumber;

	@SearchField(searchField = "dtcNumber", comparisonConditions = ComparisonConditions.EQUALS)
	private String dtcNumberEquals;

	@SearchField
	private String fedMnemonic;

	@SearchField
	private String legalEntityIdentifier;

	@SearchField(searchField = "legalEntityIdentifier", comparisonConditions = ComparisonConditions.EQUALS)
	private String legalEntityIdentifierEquals;

	@SearchField
	private Integer bloombergCompanyIdentifier;


	@SearchField(searchField = "obligorCompany.id", sortField = "obligorCompany.name")
	private Integer obligorCompanyId;


	@SearchField
	private String leiEntityStatus;

	@SearchField
	private Date leiEntityStartDate;


	@SearchField
	private Date leiEntityEndDate;


	@SearchField
	private Boolean ultimateParent;

	@SearchField
	private Boolean privateCompany;

	@SearchField
	private Boolean acquiredByParent;


	// Can be US States or Canadian Provinces, so just use the text search
	@SearchField(searchFieldPath = "stateOfIncorporation", searchField = "value,text", sortField = "value")
	private String stateOfIncorporation;

	@SearchField(searchField = "countryOfIncorporation.id", sortField = "countryOfIncorporation.text")
	private Integer countryOfIncorporationId;

	@SearchField(searchField = "countryOfRisk.id", sortField = "countryOfRisk.text")
	private Integer countryOfRiskId;


	@SearchField
	private String companyCorporateTicker;

	@SearchField
	private String ultimateParentEquityTicker;

	@SearchField(searchField = "accountList.holdingAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Boolean holdingAccountIssuer;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return OrderCompany.TABLE_NAME;
	}
}
