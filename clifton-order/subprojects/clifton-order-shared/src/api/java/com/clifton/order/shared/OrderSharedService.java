package com.clifton.order.shared;

import com.clifton.core.beans.ObjectWrapperWithBooleanResult;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.order.api.server.model.ClientAccountRetrieverModel;
import com.clifton.order.api.server.model.CompanyRetrieverModel;
import com.clifton.order.api.server.model.HoldingAccountRetrieverModel;
import com.clifton.order.api.server.model.SecurityRetrieverModel;
import com.clifton.order.shared.search.OrderAccountSearchForm;
import com.clifton.order.shared.search.OrderCompanySearchForm;
import com.clifton.order.shared.search.OrderSecurityExchangeSearchForm;
import com.clifton.order.shared.search.OrderSecuritySearchForm;

import java.util.List;


public interface OrderSharedService {

	////////////////////////////////////////////////////////////////////////////
	////////            Order Company Methods                           ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderCompany getOrderCompany(int id);


	public OrderCompany getOrderCompanyForRetriever(CompanyRetrieverModel retrieverModel);


	public List<OrderCompany> getOrderCompanyList(OrderCompanySearchForm searchForm);


	/**
	 * Called internally ONLY by Account / Company Syncs
	 **/
	@DoNotAddRequestMapping
	public ObjectWrapperWithBooleanResult<OrderCompany> saveOrderCompany(OrderCompany bean);


	/**
	 * Not used yet, but should only be called by Account / Company Syncs
	 **/
	@DoNotAddRequestMapping
	public void deleteOrderCompany(int id);

	////////////////////////////////////////////////////////////////////////////
	////////            Order Security Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderSecurity getOrderSecurity(int id);


	/**
	 * If retrieverModel is not empty, will throw an exception if cannot find a single security that matches
	 *
	 * @param ignoreIfEmpty - used for search commands where we are passed the object but all properties are empty
	 */
	public OrderSecurity getOrderSecurityForRetriever(SecurityRetrieverModel retrieverModel, boolean ignoreIfEmpty);


	/**
	 * Similar to getOrderSecurityListByTicker, however it filters to just CCY and expects only one result
	 */
	public OrderSecurity getOrderSecurityForCurrencyTicker(String ticker);


	public List<OrderSecurity> getOrderSecurityList(OrderSecuritySearchForm searchForm);


	/**
	 * Uses caching to speed up the lookup and avoid DB access.  Most of the time only one {@link OrderSecurity} will be returned.
	 */
	public List<OrderSecurity> getOrderSecurityListByTicker(String ticker);


	@DoNotAddRequestMapping
	public ObjectWrapperWithBooleanResult<OrderSecurity> saveOrderSecurity(OrderSecurity bean);


	public void deleteOrderSecurity(int id);

	////////////////////////////////////////////////////////////////////////////
	///////        Order Security Exchange Business Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderSecurityExchange getOrderSecurityExchange(short id);


	public OrderSecurityExchange getOrderSecurityExchangeForExchangeCode(String exchangeCode);


	public List<OrderSecurityExchange> getOrderSecurityExchangeList(OrderSecurityExchangeSearchForm searchForm);


	@DoNotAddRequestMapping
	public ObjectWrapperWithBooleanResult<OrderSecurityExchange> saveOrderSecurityExchange(OrderSecurityExchange bean);


	public void deleteOrderSecurityExchange(short id);

	////////////////////////////////////////////////////////////////////////////
	////////               Order Account Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderAccount getOrderAccount(int id);


	public OrderAccount getOrderAccountByMasterIdentifier(int masterIdentifier);


	public OrderAccount getOrderAccountForClientAccountRetriever(ClientAccountRetrieverModel retrieverModel);


	public OrderAccount getOrderAccountForHoldingAccountRetriever(HoldingAccountRetrieverModel retrieverModel);


	public List<OrderAccount> getOrderAccountList(OrderAccountSearchForm searchForm);


	/**
	 * Called internally ONLY by Account Sync
	 **/
	@DoNotAddRequestMapping
	public ObjectWrapperWithBooleanResult<OrderAccount> saveOrderAccount(OrderAccount bean);


	/**
	 * Not used yet, but should only be called by Account Sync
	 **/
	@DoNotAddRequestMapping
	public void deleteOrderAccount(int id);
}
