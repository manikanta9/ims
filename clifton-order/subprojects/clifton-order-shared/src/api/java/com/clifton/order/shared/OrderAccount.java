package com.clifton.order.shared;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.model.ModelAware;
import com.clifton.core.model.ModelAwareUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.api.server.model.AccountModel;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.list.SystemListItem;
import com.clifton.workflow.definition.WorkflowState;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
public class OrderAccount extends NamedHierarchicalEntity<OrderAccount, Integer> implements ModelAware<AccountModel>, LabeledObject {

	public static final String TABLE_NAME = "OrderAccount";
	private Integer masterIdentifier;
	private boolean clientAccount;
	private boolean holdingAccount;
	private OrderCompany clientCompany;
	/**
	 * Unique issuer account number assigned to this account by the issuer.
	 */
	private String accountNumber;
	private String accountNumber2;
	private String accountName;
	private String accountShortName;
	private SystemListItem accountType;
	private OrderCompany issuingCompany;
	private WorkflowState workflowState;
	/**
	 * The Portfolio Management team associated with this account
	 */
	private SecurityGroup teamSecurityGroup;
	/**
	 * The individual portfolio manager associated with this account
	 */
	private SecurityUser portfolioManagerUser;
	private SystemHierarchy service;
	private Date inceptionDate;
	private String baseCurrencyCode;
	/**
	 * The default company (uses parent name if applies) to use for retrieving FX rates.
	 * Applies to both Client and Holding Accounts and can be used to override default behaviour.
	 * <p>
	 * Whenever possible, holding account FX Source is used: COALESCE(Holding Account FX Source, Holding Account Issuer, Default: Goldman Sachs)
	 */
	private OrderCompany defaultExchangeRateSourceCompany;
	/**
	 * Used for Holding Accounts to indicate that Trading for the account is directed by the Client
	 */
	private boolean clientDirected;
	/**
	 * Applies to Holding Accounts Only - company on the platform must be the same as the issuing company of the account
	 * Broker Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers’ strategies.
	 */
	private SystemListItem companyPlatform;
	/**
	 * Applies to Holding Accounts Only - Differentiates accounts where the client pays a periodic account fee rather than trade commissions.
	 * Is tied to "Platform" in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature
	 * (retail clients generally do (Institutional clients generally do not) participate in the WRAP feature/account structure on a platform.
	 */
	private boolean wrapAccount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountModel toModelObject() {
		AccountModel model = new AccountModel();
		model.setMasterIdentifier(getMasterIdentifier());
		model.setClientName(getClientCompany() != null ? getClientCompany().getName() : null);
		model.setAccountNumber(getAccountNumber());
		model.setAccountNumber2(getAccountNumber2());
		model.setAccountName(getAccountName());
		model.setAccountShortName(getAccountShortName());
		model.setBaseCurrencyCode(getBaseCurrencyCode());
		model.setWorkflowState(getWorkflowState() != null ? getWorkflowState().getName() : null);
		model.setInceptionDate(DateUtils.asLocalDate(getInceptionDate()));
		model.setIssuingCompany(ModelAwareUtils.getModelObject(getIssuingCompany()));
		model.setBusinessService(getService() != null ? getService().getNameExpanded() : null);
		model.setAccountServiceTeam(getTeamSecurityGroup() != null ? getTeamSecurityGroup().getName() : null);
		return model;
	}


	@Override
	public String getLabel() {
		if (!isHoldingAccount()) {
			return getLabelShort() + ": " + getAccountName();
		}
		if (getIssuingCompany() != null) {
			return getLabelShort() + " (" + getIssuingCompany().getName() + ")";
		}
		return getLabelShort();
	}


	public String getLabelShort() {
		return StringUtils.coalesce(getAccountShortName(), getAccountNumber());
	}
}
