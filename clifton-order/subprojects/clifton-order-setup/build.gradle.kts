import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant


val apiVariant = registerVariant("api", upstreamForMain = true)

dependencies {

	////////////////////////////////////////////////////////////////////////////
	////////            Main Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	// Nothing yet

	////////////////////////////////////////////////////////////////////////////
	////////            API Dependencies                                ////////
	////////////////////////////////////////////////////////////////////////////

	apiVariant.api(project(":clifton-order:clifton-order-api")) { usingVariant("java-server") }
	apiVariant.api(project(":clifton-core"))
	apiVariant.api(project(":clifton-system"))
	apiVariant.api(project(":clifton-system:clifton-system-field-mapping"))
	apiVariant.api(project(":clifton-order:clifton-order-shared"))


	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-system")))
	testFixturesApi(testFixtures(project(":clifton-order")))
	testFixturesApi(testFixtures(project(":clifton-order:clifton-order-shared")))
}
