package com.clifton.order.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderAllocationStatus extends NamedEntity<Short> {

	public static final String ALLOCATION_INCOMPLETE = "Allocation Incomplete";
	public static final String ALLOCATION_COMPLETE_NO_REPORT = "Allocation Complete without Allocation Report";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The allocation report with the actual fills has been received.
	 */
	private boolean allocationFillReceived;

	/**
	 * Indicates that all allocation data has been received.
	 */
	private boolean allocationComplete;

	/**
	 * Indicates whether this allocation has been rejected
	 */
	private boolean rejected;
}
