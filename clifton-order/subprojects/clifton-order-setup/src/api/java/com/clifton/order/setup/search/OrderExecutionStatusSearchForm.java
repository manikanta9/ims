package com.clifton.order.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderExecutionStatusSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Boolean cancelIfNotFilled;

	@SearchField
	private Boolean partialFill;

	@SearchField
	private Boolean fillComplete;

	@SearchField
	private Boolean canceled;
}
