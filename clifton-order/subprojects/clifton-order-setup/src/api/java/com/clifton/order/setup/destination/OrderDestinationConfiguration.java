package com.clifton.order.setup.destination;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;


public class OrderDestinationConfiguration extends NamedEntity<Short> {


	/**
	 * The destination this configuration applies to
	 */
	private OrderDestination orderDestination;


	/**
	 * The main destination configuration - only applies if the configuration is for a backup destination
	 * All properties for the configuration are driven by the main destination (default, name, disabled), except for the configuration bean.
	 * For example, for FX Connect via fix, we have a configuration for RFS.  If we have to switch to the backup file, we can have a corresponding file configuration that handles that.
	 */
	private OrderDestinationConfiguration mainDestinationConfiguration;


	/**
	 * There can only be one default for each destination.  If the default it is automatically selected when no configuration is selected
	 */
	private boolean defaultConfiguration;

	/**
	 * Since we can't delete a configuration once it's used, we can disable it to prevent it from being used in the future
	 */
	private boolean disabled;

	/**
	 * The system bean the applies the configuration.  The bean must below to the bean group as defined by the Order Destination Type
	 * For example, the application of the configuration would apply differently for FIX Orders vs Files.
	 */
	private SystemBean configurationBean;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getOrderDestination() != null) {
			return getOrderDestination().getLabel() + ": " + getName();
		}
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderDestinationConfiguration getMainDestinationConfiguration() {
		return this.mainDestinationConfiguration;
	}


	public void setMainDestinationConfiguration(OrderDestinationConfiguration mainDestinationConfiguration) {
		this.mainDestinationConfiguration = mainDestinationConfiguration;
	}


	public OrderDestination getOrderDestination() {
		return this.orderDestination;
	}


	public void setOrderDestination(OrderDestination orderDestination) {
		this.orderDestination = orderDestination;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public boolean isDefaultConfiguration() {
		return this.defaultConfiguration;
	}


	public void setDefaultConfiguration(boolean defaultConfiguration) {
		this.defaultConfiguration = defaultConfiguration;
	}


	public SystemBean getConfigurationBean() {
		return this.configurationBean;
	}


	public void setConfigurationBean(SystemBean configurationBean) {
		this.configurationBean = configurationBean;
	}
}
