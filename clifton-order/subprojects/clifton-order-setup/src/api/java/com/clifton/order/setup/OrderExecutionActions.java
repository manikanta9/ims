package com.clifton.order.setup;

import com.clifton.core.util.CollectionUtils;

import java.util.Set;


/**
 * @author manderson
 */
public enum OrderExecutionActions {

	SEND(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.DRAFT, OrderExecutionStatus.REJECTED).withNextExecutionStatusName(OrderExecutionStatus.SENT).withRequireCurrentUserTraderOrAdmin()),
	FORCE_RESEND(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.SENT).withNextExecutionStatusName(OrderExecutionStatus.SENT).withRequireCurrentUserTraderOrAdmin()),
	CANCEL(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.DRAFT, OrderExecutionStatus.SENT, OrderExecutionStatus.READY_FOR_BATCHING, OrderExecutionStatus.BATCHED, OrderExecutionStatus.ERROR).withNextExecutionStatusName(OrderExecutionStatus.CANCELED_STATUS)),
	CANCEL_REQUEST(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.NEW, OrderExecutionStatus.REJECTED).withNextExecutionStatusName(OrderExecutionStatus.SENT)),
	REJECT_CANCEL(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.REJECTED).withNextExecutionStatusName(OrderExecutionStatus.REJECTED_CANCELED)),
	REJECT_CANCEL_SWITCH_DESTINATION_CONFIGURATION(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.REJECTED)),
	AMEND_EXECUTION(OrderExecutionActionsBuilder.forRequiredExecutionStatusName(OrderExecutionStatus.FILLED)), // ?
	ALLOCATE(), // CREATE PLACEMENT ALLOCATIONS
	COMPLETE(), // CREATE TRADES FROM PLACEMENT ALLOCATIONS

	// Additional Checks are done within the switch to determine if the placement must be canceled
	SWITCH_DESTINATION(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.DRAFT, OrderExecutionStatus.SENT, OrderExecutionStatus.READY_FOR_BATCHING, OrderExecutionStatus.BATCHED, OrderExecutionStatus.ERROR)),
	SWITCH_TO_MANUAL(OrderExecutionActionsBuilder.forRequiredExecutionStatusNames(OrderExecutionStatus.DRAFT, OrderExecutionStatus.SENT, OrderExecutionStatus.READY_FOR_BATCHING, OrderExecutionStatus.BATCHED, OrderExecutionStatus.ERROR));

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The execution status the OrderPlacement should be in in order to execute the action
	 */
	private final Set<String> requiredExecutionStatusNames;


	/**
	 * Upon successful completion of the action, the execution status to move the order to
	 */
	private final String nextExecutionStatusName;


	/**
	 * Once we have SystemActions this can be a system condition,
	 * but for now we'll just use a boolean check
	 */
	private final boolean requireCurrentUserTraderOrAdmin;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	OrderExecutionActions() {
		this.requiredExecutionStatusNames = null;
		this.nextExecutionStatusName = null;
		this.requireCurrentUserTraderOrAdmin = false;
	}


	OrderExecutionActions(OrderExecutionActionsBuilder builder) {
		this.requiredExecutionStatusNames = builder.requiredExecutionStatusNames;
		this.nextExecutionStatusName = builder.nextExecutionStatusName;
		this.requireCurrentUserTraderOrAdmin = builder.requireCurrentUserTraderOrAdmin;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class OrderExecutionActionsBuilder {


		private Set<String> requiredExecutionStatusNames;


		private String nextExecutionStatusName;
		private boolean requireCurrentUserTraderOrAdmin;


		private OrderExecutionActionsBuilder() {
		}


		public static OrderExecutionActions.OrderExecutionActionsBuilder forRequiredExecutionStatusName(String requiredExecutionStatusName) {
			OrderExecutionActions.OrderExecutionActionsBuilder result = new OrderExecutionActions.OrderExecutionActionsBuilder();
			result.requiredExecutionStatusNames = CollectionUtils.createHashSet(requiredExecutionStatusName);
			return result;
		}


		public static OrderExecutionActions.OrderExecutionActionsBuilder forRequiredExecutionStatusNames(String... requiredExecutionStatusNames) {
			OrderExecutionActions.OrderExecutionActionsBuilder result = new OrderExecutionActions.OrderExecutionActionsBuilder();
			result.requiredExecutionStatusNames = CollectionUtils.createHashSet(requiredExecutionStatusNames);
			return result;
		}


		public OrderExecutionActions.OrderExecutionActionsBuilder withNextExecutionStatusName(String nextExecutionStatusName) {
			this.nextExecutionStatusName = nextExecutionStatusName;
			return this;
		}


		public OrderExecutionActions.OrderExecutionActionsBuilder withRequireCurrentUserTraderOrAdmin() {
			this.requireCurrentUserTraderOrAdmin = true;
			return this;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Set<String> getRequiredExecutionStatusNames() {
		return this.requiredExecutionStatusNames;
	}


	public String getNextExecutionStatusName() {
		return this.nextExecutionStatusName;
	}


	public boolean isRequireCurrentUserTraderOrAdmin() {
		return this.requireCurrentUserTraderOrAdmin;
	}
}
