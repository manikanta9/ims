package com.clifton.order.setup.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "securityType.id", sortField = "securityType.text")
	private Integer securityTypeId;

	@SearchField(searchFieldPath = "securityType", searchField = "text", comparisonConditions = ComparisonConditions.EQUALS)
	private String securityType;

	@SearchField
	private Boolean executingBrokerHoldingAccountIssuer;

	@SearchField
	private Boolean externallyExecuted;
}
