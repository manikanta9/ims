package com.clifton.order.setup;

import com.clifton.order.setup.search.OrderAllocationStatusSearchForm;
import com.clifton.order.setup.search.OrderExecutionStatusSearchForm;
import com.clifton.order.setup.search.OrderExecutionTypeSearchForm;
import com.clifton.order.setup.search.OrderGroupTypeSearchForm;
import com.clifton.order.setup.search.OrderOpenCloseTypeSearchForm;
import com.clifton.order.setup.search.OrderSourceSearchForm;
import com.clifton.order.setup.search.OrderTypeSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderSetupService {

	////////////////////////////////////////////////////////////////////////////
	////////            Order Type Methods                              ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderType getOrderType(short id);


	public OrderType getOrderTypeByName(String name);


	public List<OrderType> getOrderTypeList(OrderTypeSearchForm searchForm);


	public OrderType saveOrderType(OrderType bean);


	public void deleteOrderType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////            Order Group Type Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderGroupType getOrderGroupType(short id);


	public OrderGroupType getOrderGroupTypeByName(String name);


	public List<OrderGroupType> getOrderGroupTypeList(OrderGroupTypeSearchForm searchForm);


	/**
	 * public OrderGroupType saveOrderGroupType(OrderGroupType bean);
	 * <p>
	 * <p>
	 * public void deleteOrderGroupType(short id);
	 **/

	////////////////////////////////////////////////////////////////////////////
	////////         Order Open Close Type Methods                      ////////
	////////////////////////////////////////////////////////////////////////////
	public OrderOpenCloseType getOrderOpenCloseType(short id);


	public OrderOpenCloseType getOrderOpenCloseTypeByName(String name);


	public List<OrderOpenCloseType> getOrderOpenCloseTypeList(OrderOpenCloseTypeSearchForm searchForm);


	public OrderOpenCloseType saveOrderOpenCloseType(OrderOpenCloseType bean);


	public void deleteOrderOpenCloseType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////          Order Execution Type Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderExecutionType getOrderExecutionType(short id);


	public OrderExecutionType getOrderExecutionTypeByName(String name);


	public List<OrderExecutionType> getOrderExecutionTypeList(OrderExecutionTypeSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////          Order Execution Status Methods                    ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderExecutionStatus getOrderExecutionStatus(short id);


	public OrderExecutionStatus getOrderExecutionStatusByName(String name);


	public List<OrderExecutionStatus> getOrderExecutionStatusList(OrderExecutionStatusSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////          Order Allocation Status Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	public OrderAllocationStatus getOrderAllocationStatus(short id);


	public OrderAllocationStatus getOrderAllocationStatusByName(String name);


	public List<OrderAllocationStatus> getOrderAllocationStatusList(OrderAllocationStatusSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////            Order Source Methods                             ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderSource getOrderSource(short id);


	public OrderSource getOrderSourceByName(String name);


	public List<OrderSource> getOrderSourceList(OrderSourceSearchForm searchForm);


	public OrderSource saveOrderSource(OrderSource bean);


	public void deleteOrderSource(short id);
}
