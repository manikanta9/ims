package com.clifton.order.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * Order Source table will track the source system of an order. This is useful in  cases like PM systems that want to get a list of Order Allocations
 * that only came from their system so they can update status (or other information).
 *
 * @author AbhinayaM
 */

@CacheByName
public class OrderSource extends NamedEntity<Short> {

	private boolean disabled;

	////////////////////////////////////////////////////////////////////////////
	////////            Getters and Setters                             ////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
