package com.clifton.order.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderExecutionStatus extends NamedEntity<Short> {

	public static final String DRAFT = "Draft";
	public static final String SENT = "Sent";
	public static final String NEW = "New Order";
	public static final String PARTIALLY_FILLED = "Partially Filled";
	public static final String FILLED = "Filled";
	public static final String DONE_FOR_DAY = "Done for Day";
	public static final String CANCELED_STATUS = "Canceled";
	public static final String REPLACED = "Replaced";
	public static final String REJECTED = "Rejected";
	public static final String REJECTED_CANCELED = "Canceled (Rejected)";

	public static final String READY_FOR_BATCHING = "Ready for Batching";
	public static final String BATCHED = "Batched";
	public static final String ERROR = "Error";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If no fills are received and the order is cancelled, then cancel the underlying trades.
	 */
	private boolean cancelIfNotFilled;
	/**
	 * The order has been partially filled.
	 */
	private boolean partialFill;
	/**
	 * The order fill is complete, either FILLED or PARTIALLY_FILLED_CANCELED
	 */
	private boolean fillComplete;

	/**
	 * If execution status is considered canceled.  No longer shown as an active placement
	 * and placement quantity is cleared out.
	 */
	private boolean canceled;
}
