package com.clifton.order.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderGroupType extends NamedEntity<Short> {

	public static final String ORDER_GROUP_TYPE_UPLOAD_NAME = "Order Allocation Import";

	public static final String ORDER_GROUP_TYPE_MANUAL_NAME = "Order Allocation Manual Submission";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If all order allocations belong to a specific security (note secondary security can also be used)
	 * i.e. For Rolls the security is the security we are rolling OUT of
	 */
	private boolean securityRequired;

	/**
	 * If all order allocations belong to the security OR secondary security
	 * i.e. For Rolls, the secondary security is the security we are rolling IN to
	 */
	private boolean secondarySecurityRequired;

	/**
	 * If false, order groups created with one order allocation will actually just create the order allocation and won't group it
	 */
	private boolean oneOrderAllocationAllowed;

	/**
	 * If true, requires the Order Group to have a parent Order Group
	 * i.e. Tail Groups require a parent roll group
	 */
	private boolean parentGroupRequired;

	/**
	 * If true, the order allocations of the group are rolls where an existing position is closed/rolled to a new position with a different security
	 */
	private boolean roll;

	/**
	 * If true, the order allocations of the group are from an imported file (e.g. excel files)
	 */
	private boolean orderImport;

	/**
	 * Some groups display the data differently, and a generic list of trades in the group is not helpful
	 * i.e. Roll Orders are good to see the 2 orders (roll out of and roll into) on the same line
	 */
	private String detailScreenClass;
}
