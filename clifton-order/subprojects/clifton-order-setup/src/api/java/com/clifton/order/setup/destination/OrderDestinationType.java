package com.clifton.order.setup.destination;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBeanGroup;
import com.clifton.system.schema.SystemTable;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderDestinationType extends NamedEntity<Short> {

	/**
	 * How we communicate with the selected destination: FIX, File, API, Manual.
	 */
	private DestinationCommunicationTypes destinationCommunicationType;


	/**
	 * If true, then the destinations under this type are used to place orders with a specific destination/execution venue
	 * If false, then the destination is used for some other type of routing for Order related entities (i.e. Sending Trades to Accounting System)
	 * <p>
	 * Execution Venues also require mappings in Order Destination Mappings to exist for proper routing filtering (i.e. can't send equity trades to FX Connect)
	 */
	private boolean executionVenue;

	/**
	 * Only currently allowed for FILES and required to be true for FILES
	 * If true, then the entities that are sent are batched together, else they are sent one at a time.
	 */
	private boolean batched;

	/**
	 * The source table of the entities we are batching for sending.
	 * If executionVenue = true, then the table must be OrderPlacement
	 * The only other known viable option here is OrderTrade table
	 */
	private SystemTable batchSourceSystemTable;

	/**
	 * If selected, then destination configurations for destinations of this type can select a configuration bean
	 * and those configuration beans must be of this type. i.e. FIX vs FILEs have different implementations
	 */
	private SystemBeanGroup destinationConfigurationBeanGroup;
}
