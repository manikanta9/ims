package com.clifton.order.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderOpenCloseTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Boolean buy;

	@SearchField
	private Boolean open;

	@SearchField
	private Boolean close;
}
