package com.clifton.order.setup.destination;

/**
 * The <code>DestinationCommunicationTypes</code> is an enum which defines the communication protocols used for a destination type.
 * i.e. FIX - via FIX application, File we use batches and send a file, Manual, API
 *
 * @author manderson
 */
public enum DestinationCommunicationTypes {

	// TODO ADD API AS AN OPTION ONCE WE HAVE A DESTINATION THAT WILL USE IT
	FILE(true, true, false, false),
	FIX(false, false, true, false),
	MANUAL(false, false, true, true);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Supports Order Batches (currently only used for files)
	 */
	private final boolean batchingSupported;

	/**
	 * Requires Order Batches (currently only used for files)
	 */
	private final boolean batchingRequired;

	/**
	 * If true, only supported for execution venues
	 */
	private final boolean executionVenueOnly;

	/**
	 * Requires ONLY one active destination for this communication type
	 */
	private final boolean oneDestinationAllowed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	DestinationCommunicationTypes(boolean batchingSupported, boolean batchingRequired, boolean executionVenueOnly, boolean oneDestinationAllowed) {
		this.batchingSupported = batchingSupported;
		this.batchingRequired = batchingRequired;
		this.executionVenueOnly = executionVenueOnly;
		this.oneDestinationAllowed = oneDestinationAllowed;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBatchingSupported() {
		return this.batchingSupported;
	}


	public boolean isBatchingRequired() {
		return this.batchingRequired;
	}


	public boolean isExecutionVenueOnly() {
		return this.executionVenueOnly;
	}


	public boolean isOneDestinationAllowed() {
		return this.oneDestinationAllowed;
	}
}
