package com.clifton.order.setup.destination.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderDestinationSearchForm extends BaseAuditableEntityNamedSearchForm implements CustomJsonStringAwareSearchForm {

	@SearchField(searchField = "destinationType.id", sortField = "destinationType.name")
	private Short destinationTypeId;

	@SearchField(searchField = "mainOrderDestination.id")
	private Short mainOrderDestinationId;

	@SearchField(searchFieldPath = "destinationType", searchField = "destinationCommunicationType")
	private DestinationCommunicationTypes destinationCommunicationType;

	@SearchField(searchField = "destinationType.executionVenue")
	private Boolean executionVenue;

	@SearchField
	private Boolean forceBlockingByAccount;

	@SearchField(searchField = "orderCompanyMapping.id")
	private Short orderCompanyMappingId;

	@SearchField(searchField = "destinationType.batched")
	private Boolean batched;

	@SearchField(searchField = "mainOrderDestination.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean backupDestination;

	@SearchField(searchField = "disabled")
	private Boolean disabled;
}
