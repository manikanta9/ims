package com.clifton.order.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * @author AbhinayaM
 */
public class OrderSourceSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private String label;

	@SearchField
	private Boolean disabled;

	////////////////////////////////////////////////////////////////////////////
	////////            Getters and Setters                             ////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
}
