package com.clifton.order.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * @author manderson
 */
@CacheByName
public class OrderExecutionType extends NamedEntity<Short> {

}
