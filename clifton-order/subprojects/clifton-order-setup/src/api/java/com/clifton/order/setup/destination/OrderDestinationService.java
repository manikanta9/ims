package com.clifton.order.setup.destination;

import com.clifton.order.setup.destination.search.OrderDestinationConfigurationSearchForm;
import com.clifton.order.setup.destination.search.OrderDestinationSearchForm;
import com.clifton.order.setup.destination.search.OrderDestinationTypeSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderDestinationService {

	////////////////////////////////////////////////////////////////////////////
	////////           Order Destination Type Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderDestinationType getOrderDestinationType(short id);


	public OrderDestinationType getOrderDestinationTypeByName(String name);


	public List<OrderDestinationType> getOrderDestinationTypeList(OrderDestinationTypeSearchForm searchForm);


	public OrderDestinationType saveOrderDestinationType(OrderDestinationType bean);


	public void deleteOrderDestinationType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////               Order Destination Methods                    ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderDestination getOrderDestination(short id);


	public OrderDestination getOrderDestinationByName(String name);


	public OrderDestination getOrderDestinationManual();


	public List<OrderDestination> getOrderDestinationList(OrderDestinationSearchForm searchForm);


	/**
	 * Note: Also filters out disabled destinations
	 */
	public List<OrderDestination> getOrderDestinationListForBatching();


	/**
	 * Uses Cache and returns the list of back ups for an order destination
	 * ONLY returns active destinations
	 */
	public List<OrderDestination> getOrderDestinationBackupListForDestination(short orderDestinationId);


	public OrderDestination saveOrderDestination(OrderDestination bean);


	public void deleteOrderDestination(short id);

	////////////////////////////////////////////////////////////////////////////
	////////           Order Destination Configuration Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderDestinationConfiguration getOrderDestinationConfiguration(short id);


	public OrderDestinationConfiguration getOrderDestinationConfigurationDefaultForDestination(short orderDestinationId);


	public List<OrderDestinationConfiguration> getOrderDestinationConfigurationListForDestination(short orderDestinationId, boolean activeOnly);


	public List<OrderDestinationConfiguration> getOrderDestinationConfigurationListForMainConfiguration(short mainOrderDestinationConfigurationId);


	public List<OrderDestinationConfiguration> getOrderDestinationConfigurationList(OrderDestinationConfigurationSearchForm searchForm);


	public OrderDestinationConfiguration saveOrderDestinationConfiguration(OrderDestinationConfiguration bean);


	public void deleteOrderDestinationConfiguration(short id);
}
