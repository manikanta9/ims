package com.clifton.order.setup.destination;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.system.fieldmapping.SystemFieldMapping;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderDestination extends NamedEntity<Short> implements SystemColumnCustomJsonAware {


	private OrderDestinationType destinationType;

	/**
	 * If populated then this order destination is considered to be a back up of the main order destination.
	 * Back ups are useful for things like FIX destinations where the FIX line can come down and we can easily switch
	 * to a file execution.
	 * Back up destinations do not use destination mappings and inherit various properties from their main destination.
	 * Back up and main can be different destination types (i.e. FIX vs File), however the back up and main destination much match on the executionVenue flag, and if both set the batch table
	 */
	private OrderDestination mainOrderDestination;

	/**
	 * For some destinations we must block by account and cannot group across accounts
	 * Only applies to destinationTypes where IsExecutionVenue = true
	 * i.e. Pershing FX emails, Street FX
	 */
	private boolean forceBlockingByAccount;

	/**
	 * Order Company Mapping is used to associate the Executing Broker company with a specific broker code value for the destination.
	 * If not set, or no company mapping exists for the Executing Broker Company, will default to use the company name.
	 * For example, for Goldman Sachs trading with Bloomberg we send the code 3788 in the FIX message which is what represents GS as the broker.
	 */
	private SystemFieldMapping orderCompanyMapping;

	/**
	 * Order destinations that are no longer valid, but were used historically should be disabled.
	 * Disabled order destinations cannot have any active destination mappings (applies to execution venues only)
	 */
	private boolean disabled;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * A List of custom column values for this order destination (field are assigned and vary by destination type)
	 */
	@NonPersistentField
	private String columnGroupName;

	/**
	 * JSON String value representing the custom column values
	 */
	private CustomJsonString customColumns;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBackupDestination() {
		return getMainOrderDestination() != null;
	}


	public boolean isOfDestinationCommunicationType(DestinationCommunicationTypes communicationType) {
		if (getDestinationType() != null) {
			return communicationType == getDestinationType().getDestinationCommunicationType();
		}
		return false;
	}
}
