package com.clifton.order.setup.destination.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class OrderDestinationConfigurationSearchForm extends BaseAuditableEntityNamedSearchForm {


	@SearchField(searchField = "mainDestinationConfiguration.id", sortField = "mainDestinationConfiguration.name")
	private Short mainDestinationConfigurationId;

	@SearchField(searchField = "orderDestination.id", sortField = "orderDestination.name")
	private Short orderDestinationId;

	@SearchField
	private Boolean disabled;

	@SearchField
	private Boolean defaultConfiguration;

	@SearchField(searchField = "configurationBean.id", sortField = "configurationBean.name")
	private Integer configurationBeanId;

	@SearchField(searchField = "configurationBean.name")
	private String configurationBeanName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getMainDestinationConfigurationId() {
		return this.mainDestinationConfigurationId;
	}


	public void setMainDestinationConfigurationId(Short mainDestinationConfigurationId) {
		this.mainDestinationConfigurationId = mainDestinationConfigurationId;
	}


	public Short getOrderDestinationId() {
		return this.orderDestinationId;
	}


	public void setOrderDestinationId(Short orderDestinationId) {
		this.orderDestinationId = orderDestinationId;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Boolean getDefaultConfiguration() {
		return this.defaultConfiguration;
	}


	public void setDefaultConfiguration(Boolean defaultConfiguration) {
		this.defaultConfiguration = defaultConfiguration;
	}


	public Integer getConfigurationBeanId() {
		return this.configurationBeanId;
	}


	public void setConfigurationBeanId(Integer configurationBeanId) {
		this.configurationBeanId = configurationBeanId;
	}


	public String getConfigurationBeanName() {
		return this.configurationBeanName;
	}


	public void setConfigurationBeanName(String configurationBeanName) {
		this.configurationBeanName = configurationBeanName;
	}
}
