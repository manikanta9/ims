package com.clifton.order.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import lombok.Getter;
import lombok.Setter;


/**
 * Represents the Buy/Sell and Open/Close for an order.  There are only 6 entries in the table 'Buy', 'Buy To Open', 'Buy To Close', 'Sell', 'Sell To Open' and 'Sell To Close'.
 * Some positions we explicitly say if we are opening or closing the position.
 *
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderOpenCloseType extends NamedEntity<Short> {

	/**
	 * Indicates if the order is a buy or sell.  True is buy, False is sell.
	 */
	private boolean buy;

	/**
	 * Indicates if the order is to open a position
	 */
	private boolean open;

	/**
	 * Indicates if the order is to close (or partially close) a position
	 */
	private boolean close;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isExplicitOpenClose() {
		return isOpen() != isClose();
	}
}
