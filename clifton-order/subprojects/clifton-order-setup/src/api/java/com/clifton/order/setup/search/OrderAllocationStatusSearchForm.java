package com.clifton.order.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderAllocationStatusSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Boolean allocationFillReceived;

	@SearchField
	private Boolean allocationComplete;

	@SearchField
	private Boolean rejected;
}
