package com.clifton.order.setup.search;

import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * @author manderson
 */
public class OrderExecutionTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	// empty for now
}
