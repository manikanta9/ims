package com.clifton.order.setup.destination.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderDestinationTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private DestinationCommunicationTypes destinationCommunicationType;


	@SearchField
	private Boolean executionVenue;

	@SearchField
	private Boolean batched;

	@SearchField(searchField = "batchSourceSystemTable.id", sortField = "batchSourceSystemTable.name")
	private Short batchSourceSystemTableId;


	@SearchField(searchField = "destinationConfigurationBeanGroup.id", sortField = "destinationConfigurationBeanGroup.name")
	private Short destinationConfigurationBeanGroupId;
}
