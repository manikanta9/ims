package com.clifton.order.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderGroupTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Boolean securityRequired;

	@SearchField
	private Boolean secondarySecurityRequired;

	@SearchField
	private Boolean oneOrderAllocationAllowed;

	@SearchField
	private Boolean parentGroupRequired;

	@SearchField
	private Boolean roll;

	@SearchField
	private Boolean orderImport;

	@SearchField
	private String detailScreenClass;
}



