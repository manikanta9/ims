package com.clifton.order.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.list.SystemListItem;
import lombok.Getter;
import lombok.Setter;


@CacheByName
@Getter
@Setter
public class OrderType extends NamedEntity<Short> {

	public static final String ORDER_TYPE_CURRENCY = "Currency";
	public static final String ORDER_TYPE_STOCKS = "Stocks";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemListItem securityType;

	/**
	 * In general this should always be false, however for Currency
	 * we can allow block orders to net Buys and Sells.  The Block Order itself will be a Buy or a Sell based on the net total
	 */
	private boolean allowOrderBlockNetting;

	/**
	 * If checked, then the executing broker for this order type is required to be the holding account's issuing company.
	 * This overrides any Executing Broker rules defined for the account and security and is always considered to be valid option
	 */
	private boolean executingBrokerHoldingAccountIssuer;


	/**
	 * If checked, the order allocation is externally executed and not executed by our trading team.
	 * This is rare and the Order Allocations themselves are often estimates where the holding account issuer executes the actual transaction
	 */
	private boolean externallyExecuted;
}
