Clifton.order.setup.OpenCloseTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Open/Close Type',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'orderOpenCloseType.json',
		readOnly: true,
		instructions: 'When possible, more specific open/close type should be used. This will result in additional validation during order booking.',

		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Type Label', name: 'label'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},

			{xtype: 'label', html: '<hr/>'},

			{boxLabel: 'Buy', name: 'buy', xtype: 'checkbox'},
			{boxLabel: 'Open', name: 'open', xtype: 'checkbox'},
			{boxLabel: 'Close', name: 'close', xtype: 'checkbox'}
		]
	}]
});
