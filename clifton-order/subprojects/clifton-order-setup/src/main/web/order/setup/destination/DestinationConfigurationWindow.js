Clifton.order.setup.destination.DestinationConfigurationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Destination Configuration',
	width: 800,
	height: 450,
	iconCls: 'repo',
	enableRefreshWindow: true,

	items: [{

		xtype: 'formpanel',
		url: 'orderDestinationConfiguration.json',
		labelWidth: 170,
		instructions: 'A destination configuration specifies how the placement for the order should be executed. Destination configurations are specific for a destination and can apply additional information with the placement when being routed to the destination. If the configuration is for a back up destination, then there is a required main destination configuration to reference and some proprties (name, disabled, default) are automatically synchronized to the main destination configuration.',

		listeners: {
			afterRender: function() {
				const fp = this.getWindow().getMainFormPanel();
				if (fp.getWindow().defaultData) {
					fp.resetBeanCombo(fp);
					fp.resetReadOnlyFields(fp, TCG.isNotBlank(TCG.getValue('orderDestination.mainOrderDestination.id', fp.getWindow().defaultData)));
				}
			},

			afterload: function(fp) {
				fp.resetBeanCombo(fp);
				fp.resetReadOnlyFields(fp, TCG.isNotBlank(fp.getFormValue('orderDestination.mainOrderDestination.id')));
			}
		},

		resetBeanCombo: function(fp, beanGroupName) {
			const combo = fp.getForm().findField('configurationBean.name');
			if (TCG.isBlank(beanGroupName)) {
				beanGroupName = this.getFormValue('orderDestination.destinationType.destinationConfigurationBeanGroup.name');
			}
			combo.url = 'systemBeanListFind.json?groupName=' + beanGroupName;
			combo.store.proxy.setUrl(combo.url, true);
		},

		resetReadOnlyFields: function(fp, readOnly) {
			fp.setReadOnlyField('name', readOnly);
			fp.setReadOnlyField('disabled', readOnly);
			fp.setReadOnlyField('defaultConfiguration', readOnly);
			if (TCG.isTrue(readOnly)) {
				fp.showField('orderDestination.mainOrderDestination.name');
			}
			else {
				fp.hideField('orderDestination.mainOrderDestination.name');
			}
		},

		items: [
			{xtype: 'hidden', name: 'orderDestination.destinationType.destinationConfigurationBeanGroup.name', submitValue: false},
			{xtype: 'hidden', name: 'orderDestination.mainOrderDestination.id', submitValue: false},
			{
				fieldLabel: 'Destination', name: 'orderDestination.name', hiddenName: 'orderDestination.id', xtype: 'combo', url: 'orderDestinationListFind.json', detailPageClass: 'Clifton.order.setup.destination.DestinationWindow',
				requestedProps: 'destinationType.destinationConfigurationBeanGroup.id|mainOrderDestination.id|mainOrderDestination.name',
				listeners: {
					select: function(combo, record, index) {
						const fp = TCG.getParentFormPanel(combo);
						fp.setFormValue('orderDestination.destinationType.destinationConfigurationBeanGroup.name', TCG.getValue('destinationType.destinationConfigurationBeanGroup.name', record.json));
						fp.setFormValue('orderDestination.mainOrderDestination.id', TCG.getValue('mainOrderDestination.id', record.json));
						fp.setFormValue('orderDestination.mainOrderDestination.name', TCG.getValue('mainOrderDestination.name', record.json));
						fp.resetBeanCombo(fp, TCG.getValue('destinationType.destinationConfigurationBeanGroup.name', record.json));
						fp.resetReadOnlyFields(fp, TCG.isNotBlank(TCG.getValue('mainOrderDestination.id', record.json)));
					}
				}
			},
			{
				fieldLabel: 'Main Order Destination', name: 'orderDestination.mainOrderDestination.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', detailIdField: 'orderDestination.mainOrderDestination.id', submitDetailField: false,
				qtip: 'If the selected destination is a back up, this is the main destination it is a back up for.  When adding configuration as a back up there must be a corresponding entry from the main destination to configuration options for.  All properties except for description and bean configuration are read only.'
			},
			{
				fieldLabel: 'Main Destination Configuration', name: 'mainDestinationConfiguration.name', hiddenName: 'mainDestinationConfiguration.id', xtype: 'combo', url: 'orderDestinationConfigurationListFind.json', detailPageClass: 'Clifton.order.setup.destination.DestinationConfigurationWindow',
				requiredFields: ['orderDestination.mainOrderDestination.name'], setVisibilityOnRequired: true,
				requestedProps: 'name|disabled|defaultConfiguration',
				listeners: {
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm();
						queryEvent.combo.store.setBaseParam('orderDestinationId', f.getFormValue('orderDestination.mainOrderDestination.id', true));
					},
					select: function(combo, record, index) {
						const fp = TCG.getParentFormPanel(combo);
						fp.setFormValue('name', record.json.name);
						fp.setFormValue('defaultConfiguration', record.json.defaultConfiguration);
						fp.setFormValue('disabled', record.json.disabled);
					},
					change: function(combo, newValue, oldValue) {
						const fp = TCG.getParentFormPanel(combo);
						fp.resetReadOnlyFields(fp, TCG.isNotBlank(newValue));
					}
				}
			},
			{fieldLabel: 'Configuration Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
			{fieldLabel: '', name: 'disabled', xtype: 'checkbox', boxLabel: 'Disabled'},
			{fieldLabel: '', name: 'defaultConfiguration', xtype: 'checkbox', boxLabel: 'Default Configuration'},

			{
				fieldLabel: 'Configuration Bean', name: 'configurationBean.name', hiddenName: 'configurationBean.id', xtype: 'combo', url: 'systemBeanListFind.json', detailPageClass: 'Clifton.system.bean.BeanWindow', requiredFields: ['orderDestination.destinationType.destinationConfigurationBeanGroup.name'],
				getDefaultData: function() {
					const f = this.getParentForm();
					return {type: {group: {name: f.getFormValue('orderDestination.destinationType.destinationConfigurationBeanGroup.name')}}};
				}
			},

			{xtype: 'label', html: '<hr />'}
		]
	}]
});

