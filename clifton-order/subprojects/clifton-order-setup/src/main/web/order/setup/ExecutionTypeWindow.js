Clifton.order.setup.ExecutionTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Execution Type',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'orderExecutionType.json',
		readOnly: true,
		labelWidth: 125,

		items: [
			{fieldLabel: 'Execution Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50}

		]
	}]
});
