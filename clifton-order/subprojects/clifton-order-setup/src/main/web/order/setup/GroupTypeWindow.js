Clifton.order.setup.GroupTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Group Type',
	iconCls: 'shopping-cart',
	width: 700,
	height: 500,

	items: [
		{
			xtype: 'formpanel',
			url: 'orderGroupType.json',
			readOnly: true,
			labelWidth: 150,

			items: [
				{fieldLabel: 'Group Type Name', xtype: 'displayfield', name: 'name'},
				{fieldLabel: 'Description', xtype: 'textarea', name: 'description', readOnly: true},
				{
					fieldLabel: 'Detail Screen', xtype: 'displayfield', name: 'detailScreenClass',
					qtip: 'An optional window used for viewing and managing an order group. If undefined, the default order group window is used.'
				},
				{xtype: 'label', html: '<hr/>'},
				{
					boxLabel: 'Security Required', xtype: 'checkbox', name: 'securityRequired', disabled: true,
					qtip: 'If checked, the orders of this group will all use the same security unless secondary security is also required, thus, all orders of a group will use either the defined primary or secondary security.'
				},
				{
					boxLabel: 'Secondary Security Required', xtype: 'checkbox', name: 'secondarySecurityRequired', disabled: true,
					qtip: 'If checked, the orders of this group will consist of an order group\'s primary or secondary security. An example would be a group of trades rolling from one security to a new one of the same instrument and later maturity/expiration date.'
				},
				{
					boxLabel: 'One Order Allocation Allowed', xtype: 'checkbox', name: 'oneOrderAllocationAllowed', disabled: true,
					qtip: 'If checked, a group can be created with a single order allocation. If unchecked, an order group saved with only one order allocation will result in a single saved order allocation without the group.'
				},
				{
					boxLabel: 'Parent Group Required', xtype: 'checkbox', name: 'parentGroupRequired', disabled: true,
					qtip: 'If checked, the order group will contain order allocations that are related to the defined parent order group\'s trades. An example is the tail orders when rolling future orders (where the parent group contains closing orders and orders of equal quantity for the rolled to security; the tail group contains orders for the rolled to security with additional quantity).'
				},
				{
					boxLabel: 'Order Import', xtype: 'checkbox', name: 'orderImport', disabled: true,
					qtip: 'If checked, the order allocations of this group were generated from imported files.'
				},
				{
					boxLabel: 'Roll', xtype: 'checkbox', name: 'roll', disabled: true,
					qtip: 'If checked, the orders of this group are associated with roll orders (rolling an existing position to a new position for a different security).'
				}
			]
		}
	]
});
