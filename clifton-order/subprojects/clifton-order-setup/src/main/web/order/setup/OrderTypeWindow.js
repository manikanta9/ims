Clifton.order.setup.OrderTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Type',
	width: 700,
	height: 400,
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'orderType.json',
					instructions: 'Each order type is associated with a specific security type (Stocks, Bonds, Currency, Swaps, etc.).  Each order type may support different attributes and have a different behavior.',
					readOnly: true,

					items: [
						{fieldLabel: 'Order Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
						{fieldLabel: 'Security Type', name: 'securityType.text', hiddenName: 'securityType.id', xtype: 'system-list-combo', listName: 'Security Type', valueField: 'id', qtip: 'Limits securities managed by this Order Type to those of the specified Security Type.'},
						{boxLabel: 'Allow Order Allocations to Net for Block Orders', name: 'allowOrderBlockNetting', xtype: 'checkbox', qtip: 'Rare and should only be used for Currency.  In general order blocks separate buys from sells.  For currency, we can net and only execute the total, so an order block can contain both buys and sells.'},
						{boxLabel: 'Require Execution with Holding Account Issuer', name: 'executingBrokerHoldingAccountIssuer', xtype: 'checkbox', qtip: 'If checked, then the executing broker for this order type is required to be the holding accounts issuing company.This overrides any Executing Broker rules defined for the account and security and is always considered to be valid option'},
						{boxLabel: 'Externally Executed (Not traded internally)', name: 'externallyExecuted', xtype: 'checkbox', qtip: ' If checked, the order allocation is externally executed and not executed by our trading team. This is rare and the Order Allocations themselves are often estimates where the holding account issuer executes the actual transaction'}
					]
				}]
			}
		]
	}]
});
