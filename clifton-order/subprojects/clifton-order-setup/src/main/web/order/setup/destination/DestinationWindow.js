Clifton.order.setup.destination.DestinationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Destination',
	width: 800,
	height: 600,
	iconCls: 'repo',

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.order.setup.destination.DestinationWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel-custom-json-fields',
					columnGroupName: 'Order Destination Custom Fields',
					url: 'orderDestination.json',
					labelWidth: 170,
					instructions: 'A destination specifies where and how the placement for the order should be routed.  Each order destination is associated with a specific destination type.',

					items: [
						{xtype: 'hidden', name: 'destinationType.executionVenue', submitValue: false},
						{
							fieldLabel: 'Destination Type', name: 'destinationType.name', hiddenName: 'destinationType.id', xtype: 'combo', url: 'orderDestinationTypeListFind.json', detailPageClass: 'Clifton.order.setup.destination.DestinationTypeWindow',
							requestedProps: 'executionVenue',
							listeners: {
								select: function(combo, record, index) {
									const fp = TCG.getParentFormPanel(combo);
									fp.setFormValue('destinationType.executionVenue', record.json.executionVenue);
								}
							}
						},
						{
							fieldLabel: 'Main Destination', name: 'mainOrderDestination.name', hiddenName: 'mainOrderDestination.id', xtype: 'combo', url: 'orderDestinationListFind.json', detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', requiredFields: ['destinationType.name'],
							qtip: 'If selected, then this destination is a back up of the selected main destination.  In order to be a valid back up, this destination cannot be used as a main destination for another back up.  Also, the main and back up destinations much match on destination type properties for execution venue and batch table (if supplied on both destination types).',
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm();
								queryEvent.combo.store.setBaseParam('backupDestination', false);
								queryEvent.combo.store.setBaseParam('executionVenue', f.getFormValue('destinationType.executionVenue', true));
							}
						},
						{fieldLabel: 'Destination Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
						{fieldLabel: '', name: 'disabled', xtype: 'checkbox', boxLabel: 'Disabled'},
						{fieldLabel: '', name: 'forceBlockingByAccount', xtype: 'checkbox', boxLabel: 'Force Blocking By Account', qtip: 'Used for Execution Venues only, if true blocking logic requires blocking by account and multiple account allocations is not supported.'},
						{fieldLabel: 'Broker Mapping', name: 'orderCompanyMapping.name', hiddenName: 'orderCompanyMapping.id', xtype: 'combo', url: 'systemFieldMappingListFind.json?fieldMappingDefinitionName=Broker Mappings', detailPageClass: 'Clifton.system.fieldmapping.SystemFieldMappingWindow'},

						{xtype: 'label', html: '<hr />'}
					]
				}]
			},
			{
				title: 'Destination Configurations',
				items: [{
					xtype: 'order-destination-configuration-list-grid',
					instructions: 'A list of destination configurations for this destination.  A destination configuration specifies how the placement for the order should be executed. Destination configurations are specific for a destination and can apply additional information with the placement when being routed to the destination.',
					columnOverrides: [
						{dataIndex: 'orderDestination.name', hidden: true},
						{dataIndex: 'name', defaultSortColumn: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {orderDestinationId: this.getWindow().getMainFormId()};
					},
					getDefaultData: function(gridPanel) {
						return {
							orderDestination: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}]
			}
		]
	}]
});
