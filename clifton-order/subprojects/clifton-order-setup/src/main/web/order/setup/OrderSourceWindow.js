Clifton.order.setup.OrderSourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Source',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'orderSource.json',
		instructions: 'An order source is a source system from which the order originated from. This can be used to track the order allocation back to the requesting system.',

		items: [
			{fieldLabel: 'Source Name', name: 'name'},
			{fieldLabel: 'Source Label', name: 'label'},
			{fieldLabel: 'Source Description', name: 'description', xtype: 'textarea', height: 50},

			{xtype: 'label', html: '<hr/>'},

			{boxLabel: 'Disabled', name: 'disabled', xtype: 'checkbox'}
		]
	}]
});
