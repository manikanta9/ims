Clifton.order.setup.AllocationStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Allocation Status',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'orderAllocationStatus.json',
		labelWidth: 125,
		readOnly: true,
		items: [
			{fieldLabel: 'Allocation Status Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'Allocation Fill Received', name: 'allocationFillReceived', xtype: 'checkbox'},
			{boxLabel: 'Allocation Complete', name: 'allocationComplete', xtype: 'checkbox'},
			{boxLabel: 'Allocation Rejected', name: 'rejected', xtype: 'checkbox'}
		]
	}]
});
