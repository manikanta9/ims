Clifton.order.setup.ExecutionStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Execution Status',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'orderExecutionStatus.json',
		labelWidth: 125,
		readOnly: true,
		items: [
			{fieldLabel: 'Execution Status Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'Cancel If Not Filled', name: 'cancelIfNotFilled', xtype: 'checkbox'},
			{boxLabel: 'Partial Fill', name: 'partialFill', xtype: 'checkbox'},
			{boxLabel: 'Fill Complete', name: 'fillComplete', xtype: 'checkbox'},
			{boxLabel: 'Canceled (No longer an active placement)', name: 'canceled', xtype: 'checkbox'}
		]
	}]
});
