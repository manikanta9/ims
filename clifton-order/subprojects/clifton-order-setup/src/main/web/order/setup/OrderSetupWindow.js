Clifton.order.setup.OrderSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'orderSetupWindow',
	title: 'Order Setup',
	iconCls: 'tools',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Order Types',
				items: [{
					name: 'orderTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all order types.  Each order type is associated with a specific security type (Stocks, Bonds, Currency, Swaps, etc.).  Each order type may support different attributes and have a different behavior.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Order Type Name', width: 40, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Order Type Description', width: 200, dataIndex: 'description'},
						{header: 'Security Type', width: 35, dataIndex: 'securityType.text', tooltip: 'Limits securities managed by this Order Type to those of the specified Security Type.'},
						{header: 'Allow Netting', width: 30, dataIndex: 'allowOrderBlockNetting', type: 'boolean'},
						{header: 'Enable Holding Account Issuer', width: 30, dataIndex: 'executingBrokerHoldingAccountIssuer', type: 'boolean'},
						{header: 'Enable External Execution', width: 30, dataIndex: 'externallyExecuted', type: 'boolean'}

					],
					editor: {
						detailPageClass: 'Clifton.order.setup.OrderTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Order Group Types',
				items: [{
					name: 'orderGroupTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all order group types.  Order Allocations can be grouped together to allow common operations or just to identify their relationship.',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 45, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Group Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Detail Page', width: 100, dataIndex: 'detailScreenClass', tooltip: 'When set, the trade group uses a custom window'},
						{header: 'Security Required', width: 30, dataIndex: 'securityRequired', type: 'boolean'},
						{header: 'Second Security Required', width: 35, dataIndex: 'secondarySecurityRequired', type: 'boolean'},
						{header: 'One Order Allocation Allowed', width: 30, dataIndex: 'oneOrderAllocationAllowed', type: 'boolean'},
						{header: 'Parent Group Required', width: 30, dataIndex: 'parentGroupRequired', type: 'boolean'},
						{header: 'Roll', width: 20, dataIndex: 'roll', type: 'boolean', tooltip: 'If checked, an order group of this type contains roll orders, rolling an existing position to a new position of a different security'},
						{header: 'Import', width: 20, dataIndex: 'orderImport', type: 'boolean', tooltip: 'If checked, an order group of this type is the result of an imported file'}
					],
					editor: {
						detailPageClass: 'Clifton.order.setup.GroupTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Order Execution Types',
				items: [{
					name: 'orderExecutionTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Execution Type Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 150, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.order.setup.ExecutionTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Order Execution Status',
				items: [
					{
						xtype: 'gridpanel',
						name: 'orderExecutionStatusListFind',
						instructions: 'List of available order execution statuses for orders',
						topToolbarSearchParameter: 'searchPattern',
						columns: [
							{header: 'ID', dataIndex: 'id', width: 25, hidden: true, type: 'int', doNotFormat: true},
							{header: 'Execution Status Name', dataIndex: 'name', width: 75, defaultSortColumn: true},
							{header: 'Description', dataIndex: 'description', width: 175},
							{header: 'Cancel If Not Filled', dataIndex: 'cancelIfNotFilled', width: 50, type: 'boolean', tooltip: 'If no fills are received and the order is canceled, then cancel the underlying trades.'},
							{header: 'Partial Fill', dataIndex: 'partialFill', width: 50, type: 'boolean', tooltip: 'The order has been partially filled.'},
							{header: 'Fill Complete', dataIndex: 'fillComplete', width: 50, type: 'boolean', tooltip: 'The order fill is complete. (Filled or Partially Filled Canceled)'},
							{header: 'Canceled', dataIndex: 'canceled', width: 50, type: 'boolean', tooltip: 'No longer an active placement'}
						],
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.order.setup.ExecutionStatusWindow'
						}
					}
				]
			},


			{
				title: 'Order Allocation Status',
				items: [
					{
						xtype: 'gridpanel',
						name: 'orderAllocationStatusListFind',
						instructions: 'List of available order allocation statuses for orders.',
						topToolbarSearchParameter: 'searchPattern',
						columns: [
							{header: 'ID', dataIndex: 'id', width: 25, hidden: true, type: 'int', doNotFormat: true},
							{header: 'Allocation Status Name', dataIndex: 'name', width: 75, defaultSortColumn: true},
							{header: 'Description', dataIndex: 'description', width: 175},
							{header: 'Allocation Fill Received', dataIndex: 'allocationFillReceived', width: 50, type: 'boolean', tooltip: 'The allocation report with the actual fills has been received.'},
							{header: 'Allocation Complete', dataIndex: 'allocationComplete', width: 50, type: 'boolean', tooltip: 'The allocation data has been received.'},
							{header: 'Allocation Rejected', dataIndex: 'rejected', width: 50, type: 'boolean', tooltip: 'The allocation has been rejected'}
						],
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.order.setup.AllocationStatusWindow'
						}
					}
				]

			},


			{
				title: 'Order Open/Close Types',
				items: [{
					name: 'orderOpenCloseTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all possible order open/close types. When possible, more specific open/close type should be used. This will result in additional validation during trade booking.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Open Close Type Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Open Close Type Label', width: 50, dataIndex: 'label'},
						{header: 'Description', width: 150, dataIndex: 'description'},
						{header: 'Buy', width: 30, dataIndex: 'buy', type: 'boolean'},
						{header: 'Open', width: 30, dataIndex: 'open', type: 'boolean'},
						{header: 'Close', width: 30, dataIndex: 'close', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.order.setup.OpenCloseTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Order Sources',
				items: [
					{
						xtype: 'gridpanel',
						name: 'orderSourceListFind',
						instructions: 'A list of all Order Sources: source systems which the Order Allocation originated from. Order sources can be used to track the Order Allocation back to the requesting system thus retracing its full life cycle.',
						topToolbarSearchParameter: 'searchPattern',
						columns: [
							{header: 'ID', dataIndex: 'id', width: 25, hidden: true, type: 'int', doNotFormat: true},
							{header: 'Source Name', dataIndex: 'name', width: 75, defaultSortColumn: true},
							{header: 'Source Label', dataIndex: 'label', width: 75},
							{header: 'Source Description', dataIndex: 'description', width: 300},
							{header: 'Disabled', dataIndex: 'disabled', type: 'boolean', width: 40}

						],
						editor: {
							detailPageClass: 'Clifton.order.setup.OrderSourceWindow'
						}
					}
				]
			}
		]
	}]
});

