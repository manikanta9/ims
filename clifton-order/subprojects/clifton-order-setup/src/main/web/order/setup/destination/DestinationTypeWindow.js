Clifton.order.setup.destination.DestinationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Destination Type',
	width: 750,
	height: 400,
	iconCls: 'repo',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'orderDestinationType.json',
					instructions: 'Destination Type classifies different placement routing types.  These can be used to limit/customize selections on destinations of each type.',
					labelWidth: 150,
					readOnly: true,

					items: [
						{fieldLabel: 'Destination Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
						{boxLabel: 'Execution Venue', name: 'executionVenue', xtype: 'checkbox', qtip: 'If checked, then destinations under this type are used to place orders with a specific destination/execution venue.  If unchecked, then the destination is used for some other type of routing for Order related entities (i.e. sending Trades to accounting system)'},
						{fieldLabel: 'Communication Type', name: 'destinationCommunicationType', xtype: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.order.setup.destination.DESTINATION_COMMUNICATION_TYPES}, tooltip: 'The communication type for destinations under this type.'},
						{fieldLabel: 'Configuration Bean Group', name: 'destinationConfigurationBeanGroup.name', hiddenName: 'destinationConfigurationBeanGroup.id', xtype: 'combo', url: 'systemBeanGroupListFind.json', qtip: 'If selected, then destination configurations for destinations of this type can select a configuration bean and those configuration beans must be of this type. i.e. FIX vs FILEs have different implementations'},
						{boxLabel: 'Batch Entities (Send multiple together in a batch)', name: 'batched', xtype: 'checkbox', qtip: 'Currently only allowed for Files and required for files.  All placements for destinations under this type are batched and sent via a file based on a schedule.'},
						{fieldLabel: 'Batch Source Table', name: 'batchSourceSystemTable.name', xtype: 'linkfield', detailIdField: 'batchSourceSystemTable.id', detailPageClass: 'Clifton.system.schema.TableWindow'}
					]
				}]
			},


			{
				title: 'Order Destinations',
				items: [{
					name: 'orderDestinationListFind',
					xtype: 'gridpanel',
					instructions: 'A list of destinations for this destination type.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Destination Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Destination Description', width: 200, dataIndex: 'description'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {destinationTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.order.setup.destination.DestinationWindow'
					}
				}]
			},


			{
				title: 'Custom Destination Fields',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with Order Destinations of this Destination Type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 150, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 40, dataIndex: 'order', type: 'int', defaultSortColumn: true},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'Global', width: 50, dataIndex: 'linkedToAllRows', type: 'boolean', tooltip: 'Global custom fields are not hierarchy specific: apply to all rows'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: 'Order Destination Custom Fields',
							includeNullLinkedValue: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						addFromTemplateURL: 'systemColumn.json',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							const grp = TCG.data.getData('systemColumnGroupByName.json?name=Order Destination Custom Fields', gridPanel, 'system.schema.group.Order Destination Custom Fields');
							const fVal = gridPanel.getWindow().getMainForm().formValues;
							const lbl = TCG.getValue('name', fVal);
							return {
								columnGroup: grp,
								linkedValue: TCG.getValue('id', fVal),
								linkedLabel: lbl,
								table: grp.table
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			}
		]
	}]
});
