Ext.ns(
	'Clifton.order',
	'Clifton.order.setup',
	'Clifton.order.setup.destination'
);


Clifton.order.setup.destination.DESTINATION_COMMUNICATION_TYPES = [
	['FIX', 'FIX', 'Uses FIX Application for exchanging FIX Messages with external party'],
	['FILE', 'FILE', 'Uses files to send information to external party.'],
	['MANUAL', 'MANUAL', 'No systematic way to exchange information.  Can be done via phone, etc. and manually communicated and input into the system.']
];

Clifton.order.setup.destination.DestinationWindowAdditionalTabs = [];

Clifton.order.shared.company.OrderCompanyWindowAdditionalTabs.push({

	title: 'Field Mapping Entry',
	xtype: 'system-field-mapping-entry-grid',

	columnOverrides: [{dataIndex: 'fkFieldLabel', hidden: true}],

	getTopToolbarFilters: () => ([{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}]),
	getTopToolbarInitialLoadParams: function(firstLoad) {
		return {
			fkFieldId: this.getWindow().getMainFormId()
		};
	}

});


Clifton.order.setup.destination.DestinationConfigurationListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderDestinationConfigurationListFind',
	xtype: 'gridpanel',
	instructions: 'A destination configuration specifies how the placement for the order should be executed. Destination configurations are specific for a destination and can apply additional information with the placement when being routed to the destination.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},

		{header: 'Destination Name', width: 50, dataIndex: 'orderDestination.name', filter: {searchFieldName: 'orderDestinationId', url: 'orderDestinationListFind.json'}},
		{header: 'Configuration Name', width: 50, dataIndex: 'name'},
		{header: 'Description', width: 100, dataIndex: 'description'},

		{header: 'Default', width: 20, dataIndex: 'defaultConfiguration', type: 'boolean'},
		{header: 'Disabled', width: 20, dataIndex: 'disabled', type: 'boolean'},

		{header: 'Configuration Bean', width: 50, dataIndex: 'configurationBean.name', filter: {searchFieldName: 'configurationBeanName'}}

	],
	editor: {
		detailPageClass: 'Clifton.order.setup.destination.DestinationConfigurationWindow',
		deleteConfirmMessage: 'Would you like to delete the selected row?<br><br>Note: If the configuration is referenced by a back up destination, the back up destination configuration will also be deleted.',
		getDefaultData: function(gridPanel) {
			if (gridPanel.getDefaultData) {
				return gridPanel.getDefaultData(gridPanel);
			}
		}
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('disabled', false);
		}
	}
});
Ext.reg('order-destination-configuration-list-grid', Clifton.order.setup.destination.DestinationConfigurationListGrid);



