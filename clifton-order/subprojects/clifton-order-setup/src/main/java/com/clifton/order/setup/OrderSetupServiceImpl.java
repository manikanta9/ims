package com.clifton.order.setup;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.order.setup.search.OrderAllocationStatusSearchForm;
import com.clifton.order.setup.search.OrderExecutionStatusSearchForm;
import com.clifton.order.setup.search.OrderExecutionTypeSearchForm;
import com.clifton.order.setup.search.OrderGroupTypeSearchForm;
import com.clifton.order.setup.search.OrderOpenCloseTypeSearchForm;
import com.clifton.order.setup.search.OrderSourceSearchForm;
import com.clifton.order.setup.search.OrderTypeSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderSetupServiceImpl implements OrderSetupService {

	private AdvancedUpdatableDAO<OrderType, Criteria> orderTypeDAO;
	private AdvancedUpdatableDAO<OrderGroupType, Criteria> orderGroupTypeDAO;
	private AdvancedUpdatableDAO<OrderOpenCloseType, Criteria> orderOpenCloseTypeDAO;
	private AdvancedUpdatableDAO<OrderExecutionType, Criteria> orderExecutionTypeDAO;
	private AdvancedUpdatableDAO<OrderExecutionStatus, Criteria> orderExecutionStatusDAO;
	private AdvancedUpdatableDAO<OrderAllocationStatus, Criteria> orderAllocationStatusDAO;
	private AdvancedUpdatableDAO<OrderSource, Criteria> orderSourceDAO;


	private DaoNamedEntityCache<OrderType> orderTypeCache;
	private DaoNamedEntityCache<OrderGroupType> orderGroupTypeCache;
	private DaoNamedEntityCache<OrderOpenCloseType> orderOpenCloseTypeCache;
	private DaoNamedEntityCache<OrderExecutionType> orderExecutionTypeCache;
	private DaoNamedEntityCache<OrderExecutionStatus> orderExecutionStatusCache;
	private DaoNamedEntityCache<OrderAllocationStatus> orderAllocationStatusCache;
	private DaoNamedEntityCache<OrderSource> orderSourceCache;

	////////////////////////////////////////////////////////////////////////////
	////////                 Order Type Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderType getOrderType(short id) {
		return getOrderTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderType getOrderTypeByName(String name) {
		return getOrderTypeCache().getBeanForKeyValue(getOrderTypeDAO(), name);
	}


	@Override
	public List<OrderType> getOrderTypeList(OrderTypeSearchForm searchForm) {
		return getOrderTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderType saveOrderType(OrderType bean) {
		return getOrderTypeDAO().save(bean);
	}


	@Override
	public void deleteOrderType(short id) {
		getOrderTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Order Group Type Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderGroupType getOrderGroupType(short id) {
		return getOrderGroupTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderGroupType getOrderGroupTypeByName(String name) {
		return getOrderGroupTypeCache().getBeanForKeyValue(getOrderGroupTypeDAO(), name);
	}


	@Override
	public List<OrderGroupType> getOrderGroupTypeList(OrderGroupTypeSearchForm searchForm) {
		return getOrderGroupTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Order Open Close Type Methods                      ////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public OrderOpenCloseType getOrderOpenCloseType(short id) {
		return getOrderOpenCloseTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderOpenCloseType getOrderOpenCloseTypeByName(String name) {
		return getOrderOpenCloseTypeCache().getBeanForKeyValue(getOrderOpenCloseTypeDAO(), name);
	}


	@Override
	public List<OrderOpenCloseType> getOrderOpenCloseTypeList(OrderOpenCloseTypeSearchForm searchForm) {
		return getOrderOpenCloseTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderOpenCloseType saveOrderOpenCloseType(OrderOpenCloseType bean) {
		return getOrderOpenCloseTypeDAO().save(bean);
	}


	@Override
	public void deleteOrderOpenCloseType(short id) {
		getOrderOpenCloseTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Order Execution Type Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderExecutionType getOrderExecutionType(short id) {
		return getOrderExecutionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderExecutionType getOrderExecutionTypeByName(String name) {
		return getOrderExecutionTypeCache().getBeanForKeyValue(getOrderExecutionTypeDAO(), name);
	}


	@Override
	public List<OrderExecutionType> getOrderExecutionTypeList(OrderExecutionTypeSearchForm searchForm) {
		return getOrderExecutionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Order Execution Status Methods                    ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderExecutionStatus getOrderExecutionStatus(short id) {
		return getOrderExecutionStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderExecutionStatus getOrderExecutionStatusByName(String name) {
		return getOrderExecutionStatusCache().getBeanForKeyValue(getOrderExecutionStatusDAO(), name);
	}


	@Override
	public List<OrderExecutionStatus> getOrderExecutionStatusList(OrderExecutionStatusSearchForm searchForm) {
		return getOrderExecutionStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Order Allocation Status Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderAllocationStatus getOrderAllocationStatus(short id) {
		return getOrderAllocationStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderAllocationStatus getOrderAllocationStatusByName(String name) {
		return getOrderAllocationStatusCache().getBeanForKeyValue(getOrderAllocationStatusDAO(), name);
	}


	@Override
	public List<OrderAllocationStatus> getOrderAllocationStatusList(OrderAllocationStatusSearchForm searchForm) {
		return getOrderAllocationStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Order Source Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderSource getOrderSource(short id) {
		return getOrderSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderSource getOrderSourceByName(String name) {
		return getOrderSourceCache().getBeanForKeyValue(getOrderSourceDAO(), name);
	}


	@Override
	public List<OrderSource> getOrderSourceList(OrderSourceSearchForm searchForm) {
		return getOrderSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderSource saveOrderSource(OrderSource bean) {
		return getOrderSourceDAO().save(bean);
	}


	@Override
	public void deleteOrderSource(short id) {
		getOrderSourceDAO().delete(id);
	}
}
