package com.clifton.order.setup.destination.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.setup.destination.OrderDestination;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class OrderDestinationListByMainOrderDestinationCache extends SelfRegisteringSingleKeyDaoListCache<OrderDestination, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "mainOrderDestination.id";
	}


	@Override
	protected Short getBeanKeyValue(OrderDestination bean) {
		if (bean != null && bean.getMainOrderDestination() != null) {
			return bean.getMainOrderDestination().getId();
		}
		return null;
	}
}
