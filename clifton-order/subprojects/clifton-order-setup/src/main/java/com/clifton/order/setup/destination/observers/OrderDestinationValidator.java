package com.clifton.order.setup.destination.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.setup.destination.OrderDestination;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>OrderDestinationValidate</code> validates saves or order destinations, specifically for main/back ups.
 * There is additional validation done with the Order Management project to confirm changes don't affect the destination mapping configurations
 *
 * @author manderson
 */
@Component
public class OrderDestinationValidator extends SelfRegisteringDaoValidator<OrderDestination> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(OrderDestination bean, DaoEventTypes config) throws ValidationException {
		// do nothing , use dao method instead
	}


	@Override
	public void validate(OrderDestination bean, DaoEventTypes config, ReadOnlyDAO<OrderDestination> dao) throws ValidationException {
		if (!bean.getDestinationType().isExecutionVenue()) {
			ValidationUtils.assertFalse(bean.isForceBlockingByAccount(), "Force Blocking by Account only applies to execution venues.");
		}
		OrderDestination originalBean = config.isUpdate() ? getOriginalBean(bean) : null;
		// If it's a new destination that is a back up, or an existing destination switching to be a backup, confirm it's a valid back up for the main destination
		if ((originalBean == null || !originalBean.isBackupDestination()) && bean.isBackupDestination()) {
			validateBackUpDestinationPropertiesForMain(bean);
		}
		// If it's an update and switching to be a back up, make sure not a main for another back up
		if (originalBean != null && !originalBean.isBackupDestination() && bean.isBackupDestination()) {
			// If switching to be a back up, make sure not a main for another back up
			ValidationUtils.assertTrue(CollectionUtils.isEmpty(dao.findByField("mainOrderDestination.id", bean.getId())), () -> bean.getName() + " cannot be switched to a back up because it's already being used as a main destination with back ups.");
		}

		if (originalBean != null && !originalBean.isDisabled() && bean.isDisabled() && !bean.isBackupDestination()) {
			// If disabling and NOT a back up, make sure all back ups are also disabled
			ValidationUtils.assertTrue(CollectionUtils.isEmpty(dao.findByFields(new String[]{"mainOrderDestination.id", "disabled"}, new Object[]{bean.getId(), false})), () -> bean.getName() + " cannot be disabled because there are back ups for this destination that are still enabled. Please disable all back up destinations for this destination first.");
		}

		if (!bean.isDisabled() && bean.getDestinationType().getDestinationCommunicationType().isOneDestinationAllowed()) {
			if (originalBean == null || (originalBean.getDestinationType() != bean.getDestinationType() || originalBean.isDisabled())) {
				List<OrderDestination> existingList = dao.findByFields(new String[]{"destinationType.destinationCommunicationType", "disabled"}, new Object[]{bean.getDestinationType().getDestinationCommunicationType(), false});
				// Nothing exists, or only one existing which equals current bean
				ValidationUtils.assertTrue(CollectionUtils.isEmpty(existingList) || (existingList.size() == 1 && CompareUtils.isEqual(existingList.get(0), bean)), "There already exists an active destination for " + bean.getDestinationType().getDestinationCommunicationType().name() + " and only one destination is allowed.");
			}
		}
	}


	/**
	 * Validates that the given destination is a valid back up for the main destination
	 */
	private void validateBackUpDestinationPropertiesForMain(OrderDestination destination) {
		// Both main and back up must match on execution venue flag
		ValidationUtils.assertTrue((destination.getDestinationType().isExecutionVenue() == destination.getMainOrderDestination().getDestinationType().isExecutionVenue()), () -> String.format("%s is not a valid back up for %s because both destinations must match on if they are execution venues. Main destination %s %s an execution venue, but %s %s.", destination.getName(), destination.getMainOrderDestination().getName(), destination.getMainOrderDestination().getName(), destination.getMainOrderDestination().getDestinationType().isExecutionVenue() ? "is" : "is not", destination.getName(), destination.getDestinationType().isExecutionVenue() ? "is" : "is not"));
		// If both populated, both must match on the batch table.
		if (destination.getDestinationType().getBatchSourceSystemTable() != null && destination.getMainOrderDestination().getDestinationType().getBatchSourceSystemTable() != null) {
			ValidationUtils.assertEquals(destination.getDestinationType().getBatchSourceSystemTable(), destination.getMainOrderDestination().getDestinationType().getBatchSourceSystemTable(), () -> String.format("%s is not a valid back up for %s because both destinations use batching on different tables. Main destination %s batches %s, but %s batches %s.", destination.getName(), destination.getMainOrderDestination().getName(), destination.getMainOrderDestination().getName(), destination.getMainOrderDestination().getDestinationType().getBatchSourceSystemTable().getLabel(), destination.getName(), destination.getDestinationType().getBatchSourceSystemTable().getLabel()));
		}
	}
}
