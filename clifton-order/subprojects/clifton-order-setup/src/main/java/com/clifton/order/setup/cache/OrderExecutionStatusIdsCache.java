package com.clifton.order.setup.cache;

/**
 * @author manderson
 */
public interface OrderExecutionStatusIdsCache {


	/**
	 * Returns an array of OrderExecutionStatus id's for the specified key. Use constants on this class for available key names.
	 * NOTE: use to improve performance by avoiding extra joins on large tables (OrderPlacement, etc.)
	 */
	public Short[] getOrderExecutionStatusIds(OrderExecutionStatusIdsCacheImpl.OrderExecutionStatusIds statusKey);
}
