package com.clifton.order.setup.api;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.model.ModelAwareUtils;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.api.server.OrderSetupApi;
import com.clifton.order.api.server.model.NamedEntityModel;
import com.clifton.order.api.server.model.OrderSetupLookupTypesModel;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.search.OrderExecutionTypeSearchForm;
import com.clifton.order.setup.search.OrderGroupTypeSearchForm;
import com.clifton.order.setup.search.OrderOpenCloseTypeSearchForm;
import com.clifton.order.setup.search.OrderSourceSearchForm;
import com.clifton.order.setup.search.OrderTypeSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


/**
 * @author manderson
 */
@RestController
@Getter
@Setter
public class OrderSetupApiImpl implements OrderSetupApi {

	private OrderSetupService orderSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(securityResource = "Order Setup")
	@Override
	public ResponseEntity<NamedEntityModel> getOrderSetupNamedEntityForName(OrderSetupLookupTypesModel lookupType, String entityName) {
		NamedEntity<Short> result;
		switch (lookupType) {
			case TYPE:
				result = getOrderSetupService().getOrderTypeByName(entityName);
				break;
			case SOURCE:
				result = getOrderSetupService().getOrderSourceByName(entityName);
				break;
			case GROUP_TYPE:
				result = getOrderSetupService().getOrderGroupTypeByName(entityName);
				break;
			case OPEN_CLOSE_TYPE:
				result = getOrderSetupService().getOrderOpenCloseTypeByName(entityName);
				break;
			case EXECUTION_TYPE:
				result = getOrderSetupService().getOrderExecutionTypeByName(entityName);
				break;
			default:
				throw new ValidationException("Lookup not implemented for " + lookupType.name());
		}
		return ResponseEntity.of(result == null ? Optional.empty() : Optional.of(ModelAwareUtils.getModelObjectUsingConverter(result, this::convertToNamedEntityModel)));
	}


	@SecureMethod(securityResource = "Order Setup")
	@Override
	public ResponseEntity<List<NamedEntityModel>> getOrderSetupNamedEntityList(OrderSetupLookupTypesModel lookupType) {
		List<? extends NamedEntity<Short>> result;
		switch (lookupType) {
			case TYPE:
				result = getOrderSetupService().getOrderTypeList(new OrderTypeSearchForm());
				break;
			case SOURCE:
				result = getOrderSetupService().getOrderSourceList(new OrderSourceSearchForm());
				break;
			case GROUP_TYPE:
				result = getOrderSetupService().getOrderGroupTypeList(new OrderGroupTypeSearchForm());
				break;
			case OPEN_CLOSE_TYPE:
				result = getOrderSetupService().getOrderOpenCloseTypeList(new OrderOpenCloseTypeSearchForm());
				break;
			case EXECUTION_TYPE:
				result = getOrderSetupService().getOrderExecutionTypeList(new OrderExecutionTypeSearchForm());
				break;
			default:
				throw new ValidationException("Lookup not implemented for " + lookupType.name());
		}
		return ResponseEntity.ok(Objects.requireNonNull(ModelAwareUtils.getModelObjectListUsingConverter(result, this::convertToNamedEntityModel)));
	}


	private <T extends NamedEntity<Short>> NamedEntityModel convertToNamedEntityModel(T entity) {
		NamedEntityModel model = new NamedEntityModel();
		model.setName(entity.getName());
		model.setDescription(entity.getDescription());
		return model;
	}
}
