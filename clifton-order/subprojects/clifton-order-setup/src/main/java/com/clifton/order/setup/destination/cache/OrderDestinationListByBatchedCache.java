package com.clifton.order.setup.destination.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.setup.destination.OrderDestination;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class OrderDestinationListByBatchedCache extends SelfRegisteringSingleKeyDaoListCache<OrderDestination, Boolean> {

	@Override
	protected String getBeanKeyProperty() {
		return "destinationType.batched";
	}


	@Override
	protected Boolean getBeanKeyValue(OrderDestination bean) {
		if (bean != null && bean.getDestinationType() != null) {
			return bean.getDestinationType().isBatched();
		}
		return false;
	}
}
