package com.clifton.order.setup.destination.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.setup.destination.OrderDestination;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class OrderDestinationListByCommunicationTypeCache extends SelfRegisteringSingleKeyDaoListCache<OrderDestination, DestinationCommunicationTypes> {

	@Override
	protected String getBeanKeyProperty() {
		return "destinationType.destinationCommunicationType";
	}


	@Override
	protected DestinationCommunicationTypes getBeanKeyValue(OrderDestination bean) {
		if (bean != null && bean.getDestinationType() != null) {
			return bean.getDestinationType().getDestinationCommunicationType();
		}
		return null;
	}
}
