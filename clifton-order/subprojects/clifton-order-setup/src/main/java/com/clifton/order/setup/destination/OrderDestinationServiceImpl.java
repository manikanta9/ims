package com.clifton.order.setup.destination;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.setup.destination.search.OrderDestinationConfigurationSearchForm;
import com.clifton.order.setup.destination.search.OrderDestinationSearchForm;
import com.clifton.order.setup.destination.search.OrderDestinationTypeSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderDestinationServiceImpl implements OrderDestinationService {

	private AdvancedUpdatableDAO<OrderDestinationType, Criteria> orderDestinationTypeDAO;
	private AdvancedUpdatableDAO<OrderDestination, Criteria> orderDestinationDAO;
	private AdvancedUpdatableDAO<OrderDestinationConfiguration, Criteria> orderDestinationConfigurationDAO;

	private DaoNamedEntityCache<OrderDestinationType> orderDestinationTypeCache;
	private DaoNamedEntityCache<OrderDestination> orderDestinationCache;

	private DaoSingleKeyListCache<OrderDestination, Boolean> orderDestinationListByBatchedCache;
	private DaoSingleKeyListCache<OrderDestination, DestinationCommunicationTypes> orderDestinationListByCommunicationTypeCache;
	private DaoSingleKeyListCache<OrderDestination, Short> orderDestinationListByMainOrderDestinationCache;
	private DaoSingleKeyListCache<OrderDestinationConfiguration, Short> orderDestinationConfigurationListByDestinationCache;
	private DaoSingleKeyListCache<OrderDestinationConfiguration, Short> orderDestinationConfigurationListByMainConfigurationCache;

	////////////////////////////////////////////////////////////////////////////
	////////           Order Destination Type Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderDestinationType getOrderDestinationType(short id) {
		return getOrderDestinationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderDestinationType getOrderDestinationTypeByName(String name) {
		return getOrderDestinationTypeCache().getBeanForKeyValue(getOrderDestinationTypeDAO(), name);
	}


	@Override
	public List<OrderDestinationType> getOrderDestinationTypeList(OrderDestinationTypeSearchForm searchForm) {
		return getOrderDestinationTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderDestinationType saveOrderDestinationType(OrderDestinationType bean) {
		return getOrderDestinationTypeDAO().save(bean);
	}


	@Override
	public void deleteOrderDestinationType(short id) {
		getOrderDestinationTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Order Destination Methods                    ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderDestination getOrderDestination(short id) {
		return getOrderDestinationDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderDestination getOrderDestinationByName(String name) {
		return getOrderDestinationCache().getBeanForKeyValue(getOrderDestinationDAO(), name);
	}


	@Override
	public OrderDestination getOrderDestinationManual() {
		List<OrderDestination> orderDestinationList = BeanUtils.filter(getOrderDestinationListByCommunicationTypeCache().getBeanListForKeyValue(getOrderDestinationDAO(), DestinationCommunicationTypes.MANUAL), orderDestination -> !orderDestination.isDisabled());
		if (CollectionUtils.isEmpty(orderDestinationList)) {
			throw new ValidationException("Cannot find an active MANUAL Order Destination.");
		}
		else if (orderDestinationList.size() > 1) {
			throw new ValidationException("Found " + orderDestinationList.size() + " active MANUAL Order Destinations, but there should only be one.");
		}
		return orderDestinationList.get(0);
	}


	@Override
	public List<OrderDestination> getOrderDestinationList(OrderDestinationSearchForm searchForm) {
		return getOrderDestinationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<OrderDestination> getOrderDestinationListForBatching() {
		return BeanUtils.filter(getOrderDestinationListByBatchedCache().getBeanListForKeyValue(getOrderDestinationDAO(), true), orderDestination -> !orderDestination.isDisabled());
	}


	@Override
	public List<OrderDestination> getOrderDestinationBackupListForDestination(short orderDestinationId) {
		return BeanUtils.filter(getOrderDestinationListByMainOrderDestinationCache().getBeanListForKeyValue(getOrderDestinationDAO(), orderDestinationId), orderDestination -> !orderDestination.isDisabled());
	}


	@Override
	public OrderDestination saveOrderDestination(OrderDestination bean) {
		return getOrderDestinationDAO().save(bean);
	}


	@Override
	public void deleteOrderDestination(short id) {
		getOrderDestinationDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Order Destination Configuration Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderDestinationConfiguration getOrderDestinationConfiguration(short id) {
		return getOrderDestinationConfigurationDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderDestinationConfiguration getOrderDestinationConfigurationDefaultForDestination(short orderDestinationId) {
		return CollectionUtils.getOnlyElement(CollectionUtils.getStream(getOrderDestinationConfigurationListForDestination(orderDestinationId, true)).filter(OrderDestinationConfiguration::isDefaultConfiguration).collect(Collectors.toList()), "Found more than one default configuration for destination " + getOrderDestination(orderDestinationId).getName());
	}


	@Override
	public List<OrderDestinationConfiguration> getOrderDestinationConfigurationListForDestination(short orderDestinationId, boolean activeOnly) {
		List<OrderDestinationConfiguration> result = getOrderDestinationConfigurationListByDestinationCache().getBeanListForKeyValue(getOrderDestinationConfigurationDAO(), orderDestinationId);
		if (activeOnly) {
			return CollectionUtils.getStream(result).filter(orderDestinationConfiguration -> !orderDestinationConfiguration.isDisabled()).collect(Collectors.toList());
		}
		return result;
	}


	@Override
	public List<OrderDestinationConfiguration> getOrderDestinationConfigurationListForMainConfiguration(short mainOrderDestinationConfigurationId) {
		return getOrderDestinationConfigurationListByMainConfigurationCache().getBeanListForKeyValue(getOrderDestinationConfigurationDAO(), mainOrderDestinationConfigurationId);
	}


	@Override
	public List<OrderDestinationConfiguration> getOrderDestinationConfigurationList(OrderDestinationConfigurationSearchForm searchForm) {
		return getOrderDestinationConfigurationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderDestinationConfiguration saveOrderDestinationConfiguration(OrderDestinationConfiguration bean) {
		return getOrderDestinationConfigurationDAO().save(bean);
	}


	@Transactional
	@Override
	public void deleteOrderDestinationConfiguration(short id) {
		OrderDestinationConfiguration destinationConfiguration = getOrderDestinationConfiguration(id);
		if (!destinationConfiguration.getOrderDestination().isBackupDestination()) {
			// Delete all of the backup destination configurations referencing this main config first
			getOrderDestinationConfigurationDAO().deleteList(getOrderDestinationConfigurationListForMainConfiguration(id));
		}
		getOrderDestinationConfigurationDAO().delete(id);
	}
}
