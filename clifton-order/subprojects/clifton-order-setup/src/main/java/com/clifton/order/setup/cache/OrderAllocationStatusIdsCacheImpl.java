package com.clifton.order.setup.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.setup.OrderAllocationStatus;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.search.OrderAllocationStatusSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;


/**
 * The <code>OrderAllocationStatusIdsCacheImpl</code> class is a cache of OrderAllocationStatus id's of specific types:
 * ACTIVE_ALLOCATION_STATUS
 * <p>
 * It can be used to improve system performance by avoiding joins on large tables: OrderPlacement, etc.
 * Instead, get corresponding AllocationStatus id's and use them in "IN" clause.
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderAllocationStatusIdsCacheImpl extends SelfRegisteringSimpleDaoCache<OrderAllocationStatus, String, Short[]> implements OrderAllocationStatusIdsCache {

	private OrderSetupService orderSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Short[] getOrderAllocationStatusIds(OrderAllocationStatusIdsCacheImpl.OrderAllocationStatusIds statusKey) {
		return getFromCacheOrUpdate(statusKey.name(), () -> {
			OrderAllocationStatusSearchForm searchForm = new OrderAllocationStatusSearchForm();
			searchForm.setAllocationComplete(statusKey.getAllocationComplete());
			return searchForm;
		});
	}


	private Short[] getFromCacheOrUpdate(String cacheKey, Supplier<OrderAllocationStatusSearchForm> searchFormSupplierIfMissing) {
		Short[] result = getCacheHandler().get(getCacheName(), cacheKey);
		if (result == null) {
			// not in cache: retrieve from service and store in cache for future calls
			OrderAllocationStatusSearchForm searchForm = searchFormSupplierIfMissing.get();
			List<OrderAllocationStatus> list = getOrderSetupService().getOrderAllocationStatusList(searchForm);

			int size = CollectionUtils.getSize(list);
			result = new Short[size];
			for (int i = 0; i < size; i++) {
				result[i] = list.get(i).getId();
			}
			getCacheHandler().put(getCacheName(), cacheKey, result);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The <code>OrderAllocationStatusIds</code> enum defines various options for OrderAllocationStatus searches.
	 */
	@Getter
	public enum OrderAllocationStatusIds {
		ALLOCATION_COMPLETE_STATUS(new IdsBuilder().withAllocationComplete(true)),
		ALLOCATION_INCOMPLETE_STATUS(new IdsBuilder().withAllocationComplete(false));


		private final Boolean allocationComplete;

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		OrderAllocationStatusIds(IdsBuilder builder) {
			this.allocationComplete = builder.allocationComplete;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////

		private static class IdsBuilder {

			private Boolean allocationComplete;


			public IdsBuilder withAllocationComplete(boolean allocationComplete) {
				this.allocationComplete = allocationComplete;
				return this;
			}
		}
	}
}
