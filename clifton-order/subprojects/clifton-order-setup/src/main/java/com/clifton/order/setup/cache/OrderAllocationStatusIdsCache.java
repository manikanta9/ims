package com.clifton.order.setup.cache;

/**
 * @author manderson
 */
public interface OrderAllocationStatusIdsCache {


	/**
	 * Returns an array of OrderAllocationStatus id's for the specified key. Use constants on this class for available key names.
	 * NOTE: used to improve performance by avoiding extra joins on large tables (OrderPlacement, etc.)
	 */
	public Short[] getOrderAllocationStatusIds(OrderAllocationStatusIdsCacheImpl.OrderAllocationStatusIds statusKey);
}
