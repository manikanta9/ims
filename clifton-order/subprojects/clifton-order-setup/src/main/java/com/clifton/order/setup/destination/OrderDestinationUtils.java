package com.clifton.order.setup.destination;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.json.custom.CustomJsonStringUtils;


/**
 * @author manderson
 */
public class OrderDestinationUtils {

	/**
	 * When switching destinations for a placement, in some cases we'll need to cancel the placement
	 * and create a new one, in others we'll need to reset the existing placement with the new destination
	 */
	private static final ThreadLocal<Boolean> allowResetPlacement = new ThreadLocal<>();

	private static final String FIX_DESTINATION_CUSTOM_COLUMN_NAME = "FIX Destination Name";
	private static final String FIX_HANDLING_INSTRUCTIONS_CUSTOM_COLUMN_NAME = "FIX Handling Instructions";

	private static final String FILE_SYSTEM_QUERY_COLUMN_NAME = "File System Query";
	private static final String FILE_BATCH_DATATABLE_FILE_CONVERTER_BEAN_COLUMN_NAME = "DataTable File Converter Bean";
	private static final String FILE_BATCH_SENDING_SYSTEM_BEAN_COLUMN_NAME = "Batch Sending System Bean";
	private static final String FILE_BATCH_PROCESSOR_SYSTEM_BEAN_COLUMN_NAME = "Batch Processor System Bean";
	private static final String FILE_BATCH_CALENDAR_SCHEDULE_COLUMN_NAME = "Batching Schedule";

	////////////////////////////////////////////////////////////////////////////////
	//////////             System Defined Workflow Transitions            //////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if execution of same state transition is occurring
	 * Without this same state transitions will NOT be triggered
	 */
	public static boolean isResetPlacementAllowed() {
		return Boolean.TRUE.equals(allowResetPlacement.get());
	}


	private static void allowResetPlacement() {
		allowResetPlacement.set(Boolean.TRUE);
	}


	private static void disallowResetPlacement() {
		allowResetPlacement.remove();
	}


	/**
	 * Executes the specified callback in the mode that allows system defined transitions. Guarantees to allow execution of system
	 * defined transitions before callback execution and disallowing after.
	 */
	public static <T extends IdentityObject> void executeResetPlacement(DaoUtils.DaoCallback<T> callback) {
		try {
			allowResetPlacement();
			callback.execute();
		}
		finally {
			disallowResetPlacement();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            FIX Custom Fields                               ////////
	////////////////////////////////////////////////////////////////////////////


	public static String getOrderDestinationFixDestinationName(OrderDestination orderDestination) {
		return CustomJsonStringUtils.getColumnValueAsString(orderDestination.getCustomColumns(), FIX_DESTINATION_CUSTOM_COLUMN_NAME);
	}


	public static String getOrderDestinationFixHandlingInstructions(OrderDestination orderDestination) {
		return CustomJsonStringUtils.getColumnValueAsString(orderDestination.getCustomColumns(), FIX_HANDLING_INSTRUCTIONS_CUSTOM_COLUMN_NAME);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            FILE Custom Fields                              ////////
	////////////////////////////////////////////////////////////////////////////


	public static Integer getOrderDestinationFileSystemQueryId(OrderDestination orderDestination) {
		return CustomJsonStringUtils.getColumnValueAsInteger(orderDestination.getCustomColumns(), FILE_SYSTEM_QUERY_COLUMN_NAME);
	}


	public static Integer getOrderDestinationDataTableToFileConverterBeanId(OrderDestination orderDestination) {
		return CustomJsonStringUtils.getColumnValueAsInteger(orderDestination.getCustomColumns(), FILE_BATCH_DATATABLE_FILE_CONVERTER_BEAN_COLUMN_NAME);
	}


	public static Integer getOrderDestinationFileBatchProcessorBeanId(OrderDestination orderDestination) {
		return CustomJsonStringUtils.getColumnValueAsInteger(orderDestination.getCustomColumns(), FILE_BATCH_PROCESSOR_SYSTEM_BEAN_COLUMN_NAME);
	}


	public static Integer getOrderDestinationFileBatchSendingBeanId(OrderDestination orderDestination) {
		return CustomJsonStringUtils.getColumnValueAsInteger(orderDestination.getCustomColumns(), FILE_BATCH_SENDING_SYSTEM_BEAN_COLUMN_NAME);
	}


	public static Integer getOrderDestinationFileBatchScheduleId(OrderDestination orderDestination) {
		return CustomJsonStringUtils.getColumnValueAsInteger(orderDestination.getCustomColumns(), FILE_BATCH_CALENDAR_SCHEDULE_COLUMN_NAME);
	}
}
