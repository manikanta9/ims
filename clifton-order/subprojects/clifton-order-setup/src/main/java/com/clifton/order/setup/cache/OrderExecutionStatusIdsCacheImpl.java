package com.clifton.order.setup.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.search.OrderExecutionStatusSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;


/**
 * The <code>OrderExecutionStatusIdsCacheImpl</code> class is a cache of OrderExecutionStatus id's of specific types:
 * ACTIVE_EXECUTION_STATUS, CANCELED_EXECUTION_STATUS
 * <p>
 * It can be used to improve system performance by avoiding joins on large tables: OrderPlacement, etc.
 * Instead, get corresponding ExecutionStatus id's and use them in "IN" clause.
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderExecutionStatusIdsCacheImpl extends SelfRegisteringSimpleDaoCache<OrderExecutionStatus, String, Short[]> implements OrderExecutionStatusIdsCache {

	private OrderSetupService orderSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Short[] getOrderExecutionStatusIds(OrderExecutionStatusIdsCacheImpl.OrderExecutionStatusIds statusKey) {
		return getFromCacheOrUpdate(statusKey.name(), () -> {
			OrderExecutionStatusSearchForm searchForm = new OrderExecutionStatusSearchForm();
			searchForm.setCanceled(statusKey.getCanceled());
			searchForm.setFillComplete(statusKey.getFillComplete());
			return searchForm;
		});
	}


	private Short[] getFromCacheOrUpdate(String cacheKey, Supplier<OrderExecutionStatusSearchForm> searchFormSupplierIfMissing) {
		Short[] result = getCacheHandler().get(getCacheName(), cacheKey);
		if (result == null) {
			// not in cache: retrieve from service and store in cache for future calls
			OrderExecutionStatusSearchForm searchForm = searchFormSupplierIfMissing.get();
			List<OrderExecutionStatus> list = getOrderSetupService().getOrderExecutionStatusList(searchForm);

			int size = CollectionUtils.getSize(list);
			result = new Short[size];
			for (int i = 0; i < size; i++) {
				result[i] = list.get(i).getId();
			}
			getCacheHandler().put(getCacheName(), cacheKey, result);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The <code>OrderExecutionStatusIds</code> enum defines various options for OrderExecutionStatus searches.
	 */
	@Getter
	public enum OrderExecutionStatusIds {
		CANCELED_EXECUTION_STATUS(new IdsBuilder().withCanceled(true)),
		ACTIVE_EXECUTION_STATUS(new IdsBuilder().withCanceled(false).withFillComplete(false));


		private final Boolean fillComplete;
		private final Boolean canceled;

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		OrderExecutionStatusIds(IdsBuilder builder) {
			this.fillComplete = builder.fillComplete;
			this.canceled = builder.canceled;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////

		private static class IdsBuilder {

			private Boolean fillComplete;
			private Boolean canceled;


			public IdsBuilder withFillComplete(boolean fillComplete) {
				this.fillComplete = fillComplete;
				return this;
			}


			public IdsBuilder withCanceled(boolean canceled) {
				this.canceled = canceled;
				return this;
			}
		}
	}
}
