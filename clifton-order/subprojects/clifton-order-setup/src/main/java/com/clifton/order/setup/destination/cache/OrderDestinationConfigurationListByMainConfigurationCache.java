package com.clifton.order.setup.destination.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import org.springframework.stereotype.Component;


@Component
public class OrderDestinationConfigurationListByMainConfigurationCache extends SelfRegisteringSingleKeyDaoListCache<OrderDestinationConfiguration, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "mainDestinationConfiguration.id";
	}


	@Override
	protected Short getBeanKeyValue(OrderDestinationConfiguration bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getMainDestinationConfiguration());
		}
		return null;
	}
}
