package com.clifton.order.setup.destination.observers;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import com.clifton.order.setup.destination.OrderDestinationService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>OrderDestinationConfigurationObserver</code> validates saves of order destination configurations, and also applies any automatic changes applied from main to back up destination configurations
 *
 * @author manderson
 */
@Component
public class OrderDestinationConfigurationObserver extends SelfRegisteringDaoObserver<OrderDestinationConfiguration> {

	private OrderDestinationService orderDestinationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderDestinationConfiguration> dao, DaoEventTypes event, OrderDestinationConfiguration bean) {
		if (event.isInsert() || event.isUpdate()) {
			ValidationUtils.assertNotNull(bean.getOrderDestination(), "Order Destination is required.");

			if (!bean.getOrderDestination().isBackupDestination()) {
				OrderDestinationConfiguration originalBean = (event.isInsert() ? null : getOriginalBean(dao, bean));
				ValidationUtils.assertFalse(bean.isDisabled() && bean.isDefaultConfiguration(), "Default configuration cannot be disabled.  Please uncheck default if this configuration should be disabled.");
				ValidationUtils.assertNull(bean.getMainDestinationConfiguration(), "Configuration is for a main destination.  Main Destination Configuration options only apply to back up destinations.");
				if (bean.isDefaultConfiguration() && (originalBean == null || !originalBean.isDefaultConfiguration())) {
					OrderDestinationConfiguration defaultConfig = this.orderDestinationService.getOrderDestinationConfigurationDefaultForDestination(bean.getOrderDestination().getId());
					if (defaultConfig != null && !CompareUtils.isEqual(defaultConfig, bean)) {
						throw new ValidationException("Configuration " + defaultConfig.getName() + " for destination " + bean.getOrderDestination().getName() + " is already flagged as the default.  Please clear the default on that configuration in order to make this configuration the new default");
					}
				}
			}
			else {
				ValidationUtils.assertNotNull(bean.getMainDestinationConfiguration(), "Configurations for back up destinations require the main configuration from the main destination to be selected");
				ValidationUtils.assertEquals(bean.getMainDestinationConfiguration().getOrderDestination(), bean.getOrderDestination().getMainOrderDestination(), "Selected main destination configuration does not apply to the main order destination " + bean.getOrderDestination().getMainOrderDestination().getName());
				populateChildPropertiesWithMain(bean, bean.getMainDestinationConfiguration());
			}
			if (bean.getOrderDestination().getDestinationType().getDestinationConfigurationBeanGroup() == null) {
				ValidationUtils.assertNull(bean.getConfigurationBean(), "Configuration Beans are not supported for destinations of type " + bean.getOrderDestination().getDestinationType().getName());
			}
			else if (bean.getConfigurationBean() != null) {
				ValidationUtils.assertEquals(bean.getConfigurationBean().getType().getGroup(), bean.getOrderDestination().getDestinationType().getDestinationConfigurationBeanGroup(), "Selected configuration bean is not of the correct bean group.  Destination Type " + bean.getOrderDestination().getDestinationType().getName() + " allows beans of group " + bean.getOrderDestination().getDestinationType().getDestinationConfigurationBeanGroup().getName() + " only");
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<OrderDestinationConfiguration> dao, DaoEventTypes event, OrderDestinationConfiguration bean, Throwable e) {
		// If it's not an insert (i.e. an update or delete, and is a main config used by back ups - apply the same changes
		if (e == null && event.isUpdate()) {
			if (!bean.getOrderDestination().isBackupDestination()) {
				List<OrderDestinationConfiguration> children = getOrderDestinationConfigurationListForMainConfiguration(bean.getId());
				for (OrderDestinationConfiguration child : CollectionUtils.getIterable(children)) {
					populateChildPropertiesWithMain(child, bean);
					this.orderDestinationService.saveOrderDestinationConfiguration(child);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private List<OrderDestinationConfiguration> getOrderDestinationConfigurationListForMainConfiguration(short mainConfigurationId) {
		return getOrderDestinationService().getOrderDestinationConfigurationListForMainConfiguration(mainConfigurationId);
	}


	private void populateChildPropertiesWithMain(OrderDestinationConfiguration childConfig, OrderDestinationConfiguration mainConfig) {
		childConfig.setDefaultConfiguration(mainConfig.isDefaultConfiguration());
		childConfig.setDisabled(mainConfig.isDisabled());
		childConfig.setName(mainConfig.getName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderDestinationService getOrderDestinationService() {
		return this.orderDestinationService;
	}


	public void setOrderDestinationService(OrderDestinationService orderDestinationService) {
		this.orderDestinationService = orderDestinationService;
	}
}
