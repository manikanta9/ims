package com.clifton.order.setup.builder;

import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationType;
import lombok.Builder;


/**
 * @author manderson
 */
public class OrderDestinationBuilder {

	@Builder
	public static OrderDestination build(
			short id,
			String name,
			String description,
			OrderDestinationType destinationType,
			boolean forceBlockingByAccount,
			String customColumns
	) {
		OrderDestination orderDestination = new OrderDestination();
		orderDestination.setId(id);
		orderDestination.setName(name);
		orderDestination.setDescription(description);
		orderDestination.setDestinationType(destinationType);
		orderDestination.setForceBlockingByAccount(forceBlockingByAccount);
		if (customColumns != null) {
			orderDestination.setCustomColumns(new CustomJsonString(customColumns));
		}
		return orderDestination;
	}


	public static InnerBuilder createManualExecutionVenue() {
		return builder()
				.id((short) 1)
				.destinationType(OrderDestinationTypeBuilder.createManualOrder().build());
	}


	public static InnerBuilder createFixExecutionVenue() {
		return builder()
				.id((short) 2)
				.destinationType(OrderDestinationTypeBuilder.createFixExecutionVenue().build())
				.name("Test FIX Session");
	}


	public static InnerBuilder createFileExecutionVenue() {
		return builder()
				.id((short) 3)
				.destinationType(OrderDestinationTypeBuilder.createFileOrder().build())
				.name("Test File Execution");
	}
}
