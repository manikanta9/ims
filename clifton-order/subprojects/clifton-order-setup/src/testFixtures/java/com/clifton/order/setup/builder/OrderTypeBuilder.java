package com.clifton.order.setup.builder;

import com.clifton.order.setup.OrderType;
import com.clifton.order.shared.OrderSharedTestObjectFactory;
import com.clifton.system.list.SystemListItem;
import lombok.Builder;


public class OrderTypeBuilder {

	@Builder
	public static OrderType build(
			short id,
			String name,
			String description,
			SystemListItem securityType,
			boolean allowOrderBlockNetting
	) {
		OrderType orderType = new OrderType();
		orderType.setId(id);
		orderType.setName(name);
		orderType.setDescription(description);
		orderType.setSecurityType(securityType);
		orderType.setAllowOrderBlockNetting(allowOrderBlockNetting);
		return orderType;
	}


	public static InnerBuilder createCurrency() {
		return builder()
				.id((short) 1)
				.name(OrderType.ORDER_TYPE_CURRENCY)
				.description("Spot Currency Orders")
				.securityType(OrderSharedTestObjectFactory.newCurrencySecurityType())
				.allowOrderBlockNetting(true);
	}


	public static InnerBuilder createStocks() {
		return builder()
				.id((short) 3)
				.name(OrderType.ORDER_TYPE_STOCKS)
				.description("Stock Orders")
				.securityType(OrderSharedTestObjectFactory.newStocksSecurityType());
	}
}
