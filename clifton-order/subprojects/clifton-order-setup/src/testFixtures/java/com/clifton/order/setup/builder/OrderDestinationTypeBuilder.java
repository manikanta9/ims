package com.clifton.order.setup.builder;

import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.setup.destination.OrderDestinationType;
import lombok.Builder;


public class OrderDestinationTypeBuilder {

	@Builder
	public static OrderDestinationType build(
			short id,
			String name,
			String description,
			boolean executionVenue,
			DestinationCommunicationTypes destinationCommunicationType,
			boolean batched
	) {
		OrderDestinationType orderDestinationType = new OrderDestinationType();
		orderDestinationType.setId(id);
		orderDestinationType.setName(name);
		orderDestinationType.setDescription(description);
		orderDestinationType.setDestinationCommunicationType(destinationCommunicationType);
		orderDestinationType.setBatched(batched);
		return orderDestinationType;
	}


	public static InnerBuilder createManualOrder() {
		return builder()
				.id((short) 1)
				.name("Manual Order")
				.executionVenue(true);
	}


	public static InnerBuilder createFixExecutionVenue() {
		return builder()
				.id((short) 2)
				.name("FIX Order")
				.executionVenue(true)
				.destinationCommunicationType(DestinationCommunicationTypes.FIX);
	}


	public static InnerBuilder createFileOrder() {
		return builder()
				.id((short) 3)
				.name("File Order")
				.executionVenue(true)
				.destinationCommunicationType(DestinationCommunicationTypes.FILE)
				.batched(true);
	}


	public static InnerBuilder createFileTrade() {
		return builder()
				.id((short) 4)
				.name("File Order")
				.destinationCommunicationType(DestinationCommunicationTypes.FILE)
				.batched(true);
	}
}
