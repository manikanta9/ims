package com.clifton.order.setup.destination;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class OrderDestinationServiceImplTests {

	@Resource
	private OrderDestinationService orderDestinationService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private ReadOnlyDAO<OrderDestination> orderDestinationDAO;


	private static final short FX_CONNECT_EXECUTION_VENUE = 2;
	private static final short PERSHING_FX_FILE_EXECUTION_VENUE = 4;
	private static final short CURRENCY_APX_TO_POST_FILE_EXECUTION_VENUE = 5;

	private static final int FIX_DESTINATION_CONFIG_BEAN = 1;
	private static final int FILE_DESTINATION_CONFIG_BEAN = 2;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetOrderDestinationListForBatching() {
		this.orderDestinationDAO = Mockito.spy(this.orderDestinationDAO);
		BeanUtils.setPropertyValue(this.orderDestinationService, "orderDestinationDAO", this.orderDestinationDAO, true);

		List<OrderDestination> batchingDestinationList = this.orderDestinationService.getOrderDestinationListForBatching();
		Assertions.assertEquals(2, CollectionUtils.getSize(batchingDestinationList));

		// Call it again - should return the same results but this time use the cache
		batchingDestinationList = this.orderDestinationService.getOrderDestinationListForBatching();
		Assertions.assertEquals(2, CollectionUtils.getSize(batchingDestinationList));

		// Ensures the cache is used and we only hit the db once
		Mockito.verify(this.orderDestinationDAO, Mockito.times(1)).findByFields(ArgumentMatchers.any(), ArgumentMatchers.any());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveOrderDestination_BackUp_Error_ExecutionVenue() {
		OrderDestination tradeFile = this.orderDestinationService.getOrderDestination(CURRENCY_APX_TO_POST_FILE_EXECUTION_VENUE);
		OrderDestination fxConnect = this.orderDestinationService.getOrderDestination(FX_CONNECT_EXECUTION_VENUE);

		tradeFile.setMainOrderDestination(fxConnect);
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(tradeFile));
		Assertions.assertEquals("APX To Post File is not a valid back up for FX Connect (Seattle) because both destinations must match on if they are execution venues. Main destination FX Connect (Seattle) is an execution venue, but APX To Post File is not.", e.getMessage());

		tradeFile.setMainOrderDestination(null);
		fxConnect.setMainOrderDestination(tradeFile);
		e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(fxConnect));
		Assertions.assertEquals("FX Connect (Seattle) is not a valid back up for APX To Post File because both destinations must match on if they are execution venues. Main destination APX To Post File is not an execution venue, but FX Connect (Seattle) is.", e.getMessage());
	}


	@Test
	public void testSaveOrderDestination_BackUp_Error_AlreadyMainWithBackups() {
		OrderDestination fxConnect = this.orderDestinationService.getOrderDestination(FX_CONNECT_EXECUTION_VENUE);
		OrderDestination pershingFile = this.orderDestinationService.getOrderDestination(PERSHING_FX_FILE_EXECUTION_VENUE);

		pershingFile.setMainOrderDestination(fxConnect);
		// Should save fine (technically OK because we aren't validating destination mappings here)
		this.orderDestinationService.saveOrderDestination(pershingFile);
		// Now try to make fx connect the back up of pershing
		fxConnect.setMainOrderDestination(pershingFile);
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(fxConnect));
		Assertions.assertEquals("FX Connect (Seattle) cannot be switched to a back up because it's already being used as a main destination with back ups.", e.getMessage());

		// switch pershing back to a main destination
		pershingFile.setMainOrderDestination(null);
		this.orderDestinationService.saveOrderDestination(pershingFile);
	}


	@Test
	public void testSaveOrderDestination_ForceBlockingByAccount_Error_NotExecutionVenue() {
		OrderDestination tradeFile = this.orderDestinationService.getOrderDestination(CURRENCY_APX_TO_POST_FILE_EXECUTION_VENUE);
		tradeFile.setForceBlockingByAccount(true);
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(tradeFile));
		Assertions.assertEquals("Force Blocking by Account only applies to execution venues.", e.getMessage());
	}


	@Test
	public void testSaveOrderDestination_Disabled_Error_ActiveBackups() {
		OrderDestination fxConnect = this.orderDestinationService.getOrderDestination(FX_CONNECT_EXECUTION_VENUE);
		OrderDestination pershingFile = this.orderDestinationService.getOrderDestination(PERSHING_FX_FILE_EXECUTION_VENUE);

		pershingFile.setMainOrderDestination(fxConnect);
		// Should save fine (technically OK because we aren't validating destination mappings here)
		this.orderDestinationService.saveOrderDestination(pershingFile);

		fxConnect.setDisabled(true);
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(fxConnect));
		Assertions.assertEquals("FX Connect (Seattle) cannot be disabled because there are back ups for this destination that are still enabled. Please disable all back up destinations for this destination first.", e.getMessage());

		// Clean up: Remove the back up
		pershingFile.setMainOrderDestination(null);
		this.orderDestinationService.saveOrderDestination(pershingFile);
	}


	@Test
	public void testSaveOrderDestination_Manual_Error_Exists() {
		OrderDestination duplicateManual = new OrderDestination();
		duplicateManual.setName("Manual Duplicate Test");
		duplicateManual.setDestinationType(this.orderDestinationService.getOrderDestinationType((short) 1));

		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(duplicateManual));
		Assertions.assertEquals("There already exists an active destination for MANUAL and only one destination is allowed.", e.getMessage());
	}


	@Test
	public void testSaveOrderDestination_Manual_Disable_Enable() {
		// Pull the existing one and disable
		OrderDestination manual = this.orderDestinationService.getOrderDestinationManual();
		ValidationUtils.assertNotNull(manual, "Expected a manual destination");
		manual.setDisabled(true);
		this.orderDestinationService.saveOrderDestination(manual);

		// Re-enable and save
		manual.setDisabled(false);
		this.orderDestinationService.saveOrderDestination(manual);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderDestinationMappingConfiguration_Valid() {
		OrderDestinationConfiguration destinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		destinationConfiguration = validateSaveOrderDestinationConfiguration(destinationConfiguration, null);

		destinationConfiguration.setDefaultConfiguration(true);
		destinationConfiguration = validateSaveOrderDestinationConfiguration(destinationConfiguration, null);
	}


	@Test
	public void testOrderDestinationMappingConfiguration_DisabledAndDefault() {
		OrderDestinationConfiguration destinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		destinationConfiguration.setDisabled(true);
		destinationConfiguration.setDefaultConfiguration(true);

		destinationConfiguration = validateSaveOrderDestinationConfiguration(destinationConfiguration, "Default configuration cannot be disabled.  Please uncheck default if this configuration should be disabled.");

		destinationConfiguration.setDisabled(false);
		destinationConfiguration = validateSaveOrderDestinationConfiguration(destinationConfiguration, null);

		destinationConfiguration.setDisabled(true);
		validateSaveOrderDestinationConfiguration(destinationConfiguration, "Default configuration cannot be disabled.  Please uncheck default if this configuration should be disabled.");
	}


	@Test
	public void testOrderDestinationMappingConfiguration_MainDestination_InvalidMainConfigSelected() {
		OrderDestinationConfiguration destinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		destinationConfiguration.setDefaultConfiguration(true);

		destinationConfiguration = validateSaveOrderDestinationConfiguration(destinationConfiguration, null);

		OrderDestinationConfiguration destinationConfiguration2 = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "RFS");
		destinationConfiguration2.setMainDestinationConfiguration(destinationConfiguration);

		validateSaveOrderDestinationConfiguration(destinationConfiguration2, "Configuration is for a main destination.  Main Destination Configuration options only apply to back up destinations.");
	}


	@Test
	public void testOrderDestinationMappingConfiguration_MultipleDefaults() {
		OrderDestinationConfiguration destinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		destinationConfiguration.setDefaultConfiguration(true);

		destinationConfiguration = validateSaveOrderDestinationConfiguration(destinationConfiguration, null);

		OrderDestinationConfiguration destinationConfiguration2 = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "RFS");
		destinationConfiguration2.setDefaultConfiguration(true);
		validateSaveOrderDestinationConfiguration(destinationConfiguration2, "Configuration Manager for destination FX Connect (Seattle) is already flagged as the default.  Please clear the default on that configuration in order to make this configuration the new default");
	}


	@Test
	public void testOrderDestinationMappingConfiguration_WithConfigBean() {
		OrderDestinationConfiguration destinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		destinationConfiguration.setConfigurationBean(this.systemBeanService.getSystemBean(FIX_DESTINATION_CONFIG_BEAN));
		validateSaveOrderDestinationConfiguration(destinationConfiguration, null);
	}


	@Test
	public void testOrderDestinationMappingConfiguration_WrongBean() {
		OrderDestinationConfiguration destinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		destinationConfiguration.setConfigurationBean(this.systemBeanService.getSystemBean(FILE_DESTINATION_CONFIG_BEAN));
		validateSaveOrderDestinationConfiguration(destinationConfiguration, "Selected configuration bean is not of the correct bean group.  Destination Type FIX Order allows beans of group Fix Destination Configurer only");
	}


	@Test
	public void testOrderDestinationMappingConfiguration_BackupDestination_WithoutMainConfig() {
		OrderDestination backupDestination = createBackUpDestination(FX_CONNECT_EXECUTION_VENUE, "FX Connect Placement File");

		OrderDestinationConfiguration destinationConfiguration = setupConfiguration(backupDestination.getId(), "Test");
		validateSaveOrderDestinationConfiguration(destinationConfiguration, "Configurations for back up destinations require the main configuration from the main destination to be selected");
	}


	@Test
	public void testOrderDestinationMappingConfiguration_BackupDestination_WrongBean() {
		OrderDestination backupDestination = createBackUpDestination(FX_CONNECT_EXECUTION_VENUE, "FX Connect Placement File");

		OrderDestinationConfiguration mainDestinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		mainDestinationConfiguration = validateSaveOrderDestinationConfiguration(mainDestinationConfiguration, null);

		OrderDestinationConfiguration backupDestinationConfiguration = setupConfiguration(backupDestination.getId(), null);
		backupDestinationConfiguration.setMainDestinationConfiguration(mainDestinationConfiguration);
		backupDestinationConfiguration.setConfigurationBean(this.systemBeanService.getSystemBean(FIX_DESTINATION_CONFIG_BEAN));
		validateSaveOrderDestinationConfiguration(backupDestinationConfiguration, "Selected configuration bean is not of the correct bean group.  Destination Type File Order allows beans of group File Destination Configurer only");
	}


	@Test
	public void testOrderDestinationMappingConfiguration_BackupDestination_Auto_Manage() {
		OrderDestination backupDestination = createBackUpDestination(FX_CONNECT_EXECUTION_VENUE, "FX Connect Placement File");

		OrderDestinationConfiguration mainDestinationConfiguration = setupConfiguration(FX_CONNECT_EXECUTION_VENUE, "Manager");
		mainDestinationConfiguration.setDefaultConfiguration(true);
		mainDestinationConfiguration = validateSaveOrderDestinationConfiguration(mainDestinationConfiguration, null);

		OrderDestinationConfiguration backupDestinationConfiguration = setupConfiguration(backupDestination.getId(), null);
		backupDestinationConfiguration.setMainDestinationConfiguration(mainDestinationConfiguration);

		backupDestinationConfiguration = validateSaveOrderDestinationConfiguration(backupDestinationConfiguration, null);
		validateMainAndBackupConfigurationMatch(backupDestinationConfiguration.getId());

		mainDestinationConfiguration.setName("Manager - Update");
		mainDestinationConfiguration.setDisabled(true);
		mainDestinationConfiguration.setDefaultConfiguration(false);

		validateSaveOrderDestinationConfiguration(mainDestinationConfiguration, null);

		// Properties should have updated on the back up
		validateMainAndBackupConfigurationMatch(backupDestinationConfiguration.getId());

		// Delete the parent - should also delete the children
		this.orderDestinationService.deleteOrderDestinationConfiguration(mainDestinationConfiguration.getId());
		Assertions.assertNull(this.orderDestinationService.getOrderDestinationConfiguration(backupDestinationConfiguration.getId()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderDestinationConfiguration setupConfiguration(short destinationId, String name) {
		// TODO SWITCH TO USE LOMBOK WHEN AVAILABLE
		OrderDestinationConfiguration configuration = new OrderDestinationConfiguration();
		configuration.setOrderDestination(this.orderDestinationService.getOrderDestination(destinationId));
		configuration.setName(name);
		return configuration;
	}


	private OrderDestinationConfiguration validateSaveOrderDestinationConfiguration(OrderDestinationConfiguration destinationConfiguration, String expectedErrorMessage) {
		if (StringUtils.isEmpty(expectedErrorMessage)) {
			return this.orderDestinationService.saveOrderDestinationConfiguration(destinationConfiguration);
		}
		else {
			ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestinationConfiguration(destinationConfiguration));
			Assertions.assertEquals(expectedErrorMessage, e.getMessage());
		}
		return destinationConfiguration;
	}


	private OrderDestination createBackUpDestination(short mainDestinationId, String name) {
		OrderDestination destination = new OrderDestination();
		destination.setMainOrderDestination(this.orderDestinationService.getOrderDestination(mainDestinationId));
		destination.setDestinationType(this.orderDestinationService.getOrderDestinationTypeByName("File Order"));
		return this.orderDestinationService.saveOrderDestination(destination);
	}


	private void validateMainAndBackupConfigurationMatch(short backupDestinationConfigurationId) {
		OrderDestinationConfiguration backupDestinationConfiguration = this.orderDestinationService.getOrderDestinationConfiguration(backupDestinationConfigurationId);
		Assertions.assertEquals(backupDestinationConfiguration.getMainDestinationConfiguration().getName(), backupDestinationConfiguration.getName());
		Assertions.assertEquals(backupDestinationConfiguration.getMainDestinationConfiguration().isDefaultConfiguration(), backupDestinationConfiguration.isDefaultConfiguration());
		Assertions.assertEquals(backupDestinationConfiguration.getMainDestinationConfiguration().isDisabled(), backupDestinationConfiguration.isDisabled());
	}
}
