package com.clifton.order.setup.destination;

import com.clifton.order.setup.builder.OrderDestinationBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ContextConfiguration("classpath:com/clifton/order/setup/OrderSetupProjectBasicTests-context.xml")
@ExtendWith(SpringExtension.class)
public class OrderDestinationUtilsTests {

	private static final String fixCustomColumnValue = "{\"FIX Destination Name\":\"FX Connect (Seattle)\",\"FIX Handling Instructions\":\"MANUAL\"}";
	private static final String fileCustomColumnValue = "{\"File System Query\":{\"value\":18,\"text\":\"Pershing FX Execution File\"},\"DataTable File Converter Bean\":{\"value\":66,\"text\":\"Pershing FX Execution File Converter\"},\"Batch Processor System Bean\":{\"value\":50,\"text\":\"Default Order Batching Placement Processor\"},\"Batching Schedule\":{\"value\":10,\"text\":\"Custom One Time Batch Schedule\"},\"Batch Sending System Bean\":{\"value\":33,\"text\":\"Do Nothing Order Batch Sender (Success)\"}}";


	@Test
	public void test_FixDestinationCustomFieldValues() {
		OrderDestination orderDestination = OrderDestinationBuilder.createFixExecutionVenue().customColumns(fixCustomColumnValue).build();
		Assertions.assertEquals("FX Connect (Seattle)", OrderDestinationUtils.getOrderDestinationFixDestinationName(orderDestination));
		Assertions.assertEquals("MANUAL", OrderDestinationUtils.getOrderDestinationFixHandlingInstructions(orderDestination));

		OrderDestination emptyOrderDestination = OrderDestinationBuilder.createFixExecutionVenue().build();
		Assertions.assertNull(OrderDestinationUtils.getOrderDestinationFixDestinationName(emptyOrderDestination));
		Assertions.assertNull(OrderDestinationUtils.getOrderDestinationFixHandlingInstructions(emptyOrderDestination));
	}


	@Test
	public void test_FileDestinationCustomFieldValues() {
		OrderDestination orderDestination = OrderDestinationBuilder.createFileExecutionVenue().customColumns(fileCustomColumnValue).build();
		Assertions.assertEquals(18, OrderDestinationUtils.getOrderDestinationFileSystemQueryId(orderDestination));
		Assertions.assertEquals(66, OrderDestinationUtils.getOrderDestinationDataTableToFileConverterBeanId(orderDestination));
		Assertions.assertEquals(50, OrderDestinationUtils.getOrderDestinationFileBatchProcessorBeanId(orderDestination));
		Assertions.assertEquals(33, OrderDestinationUtils.getOrderDestinationFileBatchSendingBeanId(orderDestination));
		Assertions.assertEquals(10, OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(orderDestination));

		OrderDestination emptyOrderDestination = OrderDestinationBuilder.createFixExecutionVenue().build();
		Assertions.assertNull(OrderDestinationUtils.getOrderDestinationFileSystemQueryId(emptyOrderDestination));
		Assertions.assertNull(OrderDestinationUtils.getOrderDestinationDataTableToFileConverterBeanId(emptyOrderDestination));
		Assertions.assertNull(OrderDestinationUtils.getOrderDestinationFileBatchProcessorBeanId(emptyOrderDestination));
		Assertions.assertNull(OrderDestinationUtils.getOrderDestinationFileBatchSendingBeanId(emptyOrderDestination));
		Assertions.assertNull(OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(emptyOrderDestination));
	}
}
