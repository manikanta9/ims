package com.clifton.order.setup.cache;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderSetupCacheTests {

	@Resource
	private OrderAllocationStatusIdsCache orderAllocationStatusIdsCache;


	@Resource
	private OrderExecutionStatusIdsCache orderExecutionStatusIdsCache;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderAllocationStatusIdsCache() {
		Short[] statusIds = this.orderAllocationStatusIdsCache.getOrderAllocationStatusIds(OrderAllocationStatusIdsCacheImpl.OrderAllocationStatusIds.ALLOCATION_COMPLETE_STATUS);
		// Only have 2 allocation complete statuses: Allocation Complete without Allocation Report and Allocation Complete
		Assertions.assertEquals(2, statusIds.length);

		statusIds = this.orderAllocationStatusIdsCache.getOrderAllocationStatusIds(OrderAllocationStatusIdsCacheImpl.OrderAllocationStatusIds.ALLOCATION_INCOMPLETE_STATUS);
		// The rest (11 records are incomplete)
		Assertions.assertEquals(11, statusIds.length);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderExecutionStatusIdsCache() {
		Short[] statusIds = this.orderExecutionStatusIdsCache.getOrderExecutionStatusIds(OrderExecutionStatusIdsCacheImpl.OrderExecutionStatusIds.ACTIVE_EXECUTION_STATUS);
		// 14 Not complete and not conceled
		Assertions.assertEquals(14, statusIds.length);

		statusIds = this.orderExecutionStatusIdsCache.getOrderExecutionStatusIds(OrderExecutionStatusIdsCacheImpl.OrderExecutionStatusIds.CANCELED_EXECUTION_STATUS);
		// 2 that are conceled
		Assertions.assertEquals(2, statusIds.length);
	}
}
