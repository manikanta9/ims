Ext.ns(
	'Clifton.order',
	'Clifton.order.management',
	'Clifton.order.management.activity',
	'Clifton.order.management.activity.block',
	'Clifton.order.management.blotter',
	'Clifton.order.management.execution',
	'Clifton.order.management.execution.upload',
	'Clifton.order.management.setup',
	'Clifton.order.management.setup.destination',
	'Clifton.order.management.setup.rule',
	'Clifton.order.management.setup.rule.preview'
);


Clifton.order.management.setup.rule.RuleAssignmentListGrid = Ext.extend(Clifton.system.schema.GridPanelWithCustomJsonFields, {
	name: 'orderManagementRuleAssignmentListFind',
	xtype: 'gridpanel-custom-json-fields',
	tableName: 'OrderManagementRuleDefinition',
	instructions: 'Rule Assignments allow assigning rules to accounts or globally, optionally for a specified period of time.  Rules can also be explicitly excluded in cases where necessary.  Rule assignments of rollup rules is the equivalent of assigning all of the child rules.',
	groupField: 'scopeLabel',
	additionalPropertiesToRequest: 'ruleDefinition.id|rollupRuleDefinition.id|ruleDefinition.ruleType.ruleCategory.previewRuleTypeWindowClass|clientAccount.id|holdingAccount.id|holdingAccountIssuingCompany.id|ruleScope|coalesceHoldingAccountIssuingCompany.id',
	topToolbarSearchParameter: 'searchPattern',
	viewNames: [{text: 'Default View', url: 'orderManagementRuleAssignmentListFind.json'}, {text: 'Expanded View', url: 'orderManagementRuleAssignmentExtendedListFind.json', tooltip: 'Expand rollup rules and show all of the children.'}],
	defaultHidden: true,
	remoteSort: true,
	getJsonValueBeanPath: function() {
		return 'ruleDefinition.';
	},

	columnOverrides: [
		{dataIndex: 'ruleDefinition.customColumns.Security Type', hidden: false},
		{dataIndex: 'ruleDefinition.customColumns.Security', hidden: false},
		{dataIndex: 'ruleDefinition.customColumns.Executing Broker', hidden: false}
	],

	nonCustomColumns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true, useNull: true, expandedFilter: false, defaultFilter: {searchFieldName: 'id'}}, // Default View
		{header: 'Rule Assignment ID', width: 15, dataIndex: 'ruleAssignmentId', hidden: true, useNull: true, expandedFilter: {searchFieldName: 'ruleAssignmentId'}, defaultFilter: false}, // Expanded View
		{
			header: 'Rule Scope', width: 80, dataIndex: 'scopeLabel', filter: false, sortable: false, hidden: true,
			renderer: function(v, p, r) {
				if (TCG.isEquals('GLOBAL', r.json.ruleScope)) {
					p.css = 'amountPositive';
				}
				else if (TCG.isEquals('HOLDING_ACCOUNT_ISSUER', r.json.ruleScope)) {
					p.css = 'amountAdjusted';
				}
				return v;
			},
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const record = grid.getStore().getAt(rowIndex);
					event.preventDefault();
					const contextMenuItems = await grid.ownerGridPanel.getGridRowContextMenuItems(grid.ownerGridPanel, rowIndex, record);
					const menu = new Ext.menu.Menu({
						items: contextMenuItems
					});
					menu.showAt(event.getXY());
				}
			}
		},
		// Expanded View
		{header: 'Rollup Rule Definition', width: 75, dataIndex: 'rollupRuleDefinition.name', hidden: true, viewNames: ['Expanded View'], filter: {searchFieldName: 'rollupRuleDefinitionId', type: 'combo', url: 'orderManagementRuleDefinitionListFind.json?rollup=true'}},

		{header: 'Rule Type', width: 40, dataIndex: 'ruleDefinition.ruleType.name', hidden: true, filter: {searchFieldName: 'ruleTypeId', type: 'combo', url: 'orderManagementRuleTypeListFind.json'}},
		{
			header: 'Rule Definition', width: 75, dataIndex: 'ruleDefinition.name', allView: true, defaultFilter: {searchFieldName: 'ruleDefinitionId', type: 'combo', url: 'orderManagementRuleDefinitionListFind.json'},
			expandedFilter: {searchFieldName: 'ruleDefinitionId', type: 'combo', url: 'orderManagementRuleDefinitionListFind.json?rollup=false'}
		},

		{
			header: 'Scope', width: 30, dataIndex: 'scopeLabelShort', filter: {
				type: 'combo', displayField: 'value', valueField: 'value', mode: 'local',
				store: new Ext.data.ArrayStore({
					fields: ['value', 'description'],
					data: [['Global', 'Global rules apply globally to all accounts.'], ['Issuer', 'Issuer rules apply to selected holding account issuer.'], ['Account', 'Account rules apply to a client/holding account combination.']]
				})
			}
		},
		{header: 'Account', width: 50, dataIndex: 'clientAccount.labelShort', hidden: true, filter: {searchFieldName: 'clientAccountId', type: 'combo', url: 'orderAccountListFind.json?clientAccount=true', displayField: 'label'}},
		{header: 'Client Account', width: 50, dataIndex: 'clientAccount.label', hidden: true, filter: {searchFieldName: 'clientAccountId', type: 'combo', url: 'orderAccountListFind.json?clientAccount=true', displayField: 'label'}},
		{header: 'Holding Account', width: 50, dataIndex: 'holdingAccount.label', hidden: true, filter: {searchFieldName: 'holdingAccountId', type: 'combo', url: 'orderAccountListFind.json?holdingAccount=true', displayField: 'label'}},
		{header: 'Holding Account Issuer', width: 50, dataIndex: 'coalesceHoldingAccountIssuingCompany.name', hidden: true, filter: {searchFieldName: 'coalesceHoldingAccountIssuingCompanyId', type: 'combo', url: 'orderCompanyListFind.json?holdingAccountIssuer=true'}},


		{header: 'Modify Condition', width: 50, dataIndex: 'entityModifyCondition.name', hidden: true, filter: {searchFieldName: 'entityModifyConditionId', type: 'combo', url: 'systemConditionListFind.json?optionalTableName=OrderManagementRuleAssignment'}},
		{header: 'Assignment Note', width: 100, dataIndex: 'note', allViews: true},
		{header: 'Excluded', width: 25, dataIndex: 'excluded', type: 'boolean', allViews: true},
		{header: 'Active', width: 25, dataIndex: 'active', type: 'boolean', allViews: true},
		{header: 'Start Date', width: 25, dataIndex: 'startDate', hidden: true},
		{header: 'End Date', width: 25, dataIndex: 'endDate', hidden: true}
	],

	listeners: {
		afterrender: function() {
			const gp = this;
			gp.switchToViewBeforeReload('Default View');
			// Note: Because the URL Changes Based on the View Need to make sure URL is updated appropriately
			// Otherwise when clicking on a column to sort it reverts back to the original default run view url
			const store = this.ds;
			store.on('beforeload', function() {
				store.url = encodeURI(gp.getLoadURL());
				store.proxy.setUrl(store.url);
			});
		}
	},

	onBeforeReturnFilterData: function(filters) {
		for (let i = 0; i < filters.length; i++) {
			const filter = filters[i];
			if (filter && TCG.isEquals('scopeLabelShort', filter.field)) {
				if (TCG.isEquals('Global', filter.data.value)) {
					filter.field = 'globalRuleAssignment';
				}
				else if (TCG.isEquals('Issuer', filter.data.value)) {
					filter.field = 'holdingAccountIssuerAssignment';
				}
				else {
					filter.field = 'accountAssignment';
				}
				filter.data.comparison = 'EQUALS';
				filter.data.value = 'true';
				filter.dataTypeName = 'BOOLEAN';
				break;
			}
		}
	},

	getLoadURL: function() {
		let url = 'orderManagementRuleAssignmentListFind.json';
		if (this.currentViewName === 'Expanded View') {
			url = 'orderManagementRuleAssignmentExtendedListFind.json';
		}
		return url;
	},

	// Exclude rollup column if not in expanded view during export (cause NPE since not a real column)
	isSkipExport: function(column) {
		return (TCG.isEquals('rollupRuleDefinition.name', column.dataIndex) && TCG.isNotEquals('Expanded View', this.currentViewName));
	},


	switchToViewBeforeReload: function(viewName) {
		// Update Filtering/sorting on parent/child based on default or expanded view
		const cm = this.getColumnModel();
		const l = cm.getColumnCount();
		const offset = (this.rowSelectionModel && this.rowSelectionModel.width) ? 1 : 0; // checkbox selector
		for (let i = offset; i < l; i++) {
			const c = cm.getColumnById(i);
			if (viewName === 'Default View') {
				if (TCG.isNotBlank(c.defaultFilter)) {
					c.filter = c.defaultFilter;
					c.sortable = true;
					if (TCG.isFalse(c.defaultFilter)) {
						c.sortable = false;
						// Clear if currently being filtered
						if (this.isFilterValueSet(c.dataIndex)) {
							this.clearFilter(c.dataIndex, true); // Suppress Events
						}
					}
				}
			}
			else {
				if (TCG.isNotBlank(c.expandedFilter)) {
					c.filter = c.expandedFilter;
					c.sortable = true;
					if (TCG.isFalse(c.expandedFilter)) {
						c.sortable = false;
						// Clear if currently being filtered
						if (this.isFilterValueSet(c.dataIndex)) {
							this.clearFilter(c.dataIndex, true); // Suppress Events
						}
					}
				}
			}
		}
		// This is necessary so the Filters will properly update (listener on the filters that listens to this event from the grid)
		this.grid.fireEvent('reconfigure');
	},

	editor: {
		detailPageClass: 'Clifton.order.management.setup.rule.RuleAssignmentWindow',
		getDetailPageId: function(gridPanel, row) {
			return TCG.isNotBlank(row.json.ruleAssignmentId) ? row.json.ruleAssignmentId : row.json.id;
		},
		getDefaultData: function(gridPanel) { // defaults account(s) for the detail page
			let dd = {};
			if (gridPanel.getDefaultData) {
				dd = gridPanel.getDefaultData(gridPanel);
			}
			dd.clientAccount = gridPanel.getDefaultClientAccount();
			dd.holdingAccount = gridPanel.getDefaultHoldingAccount();
			return dd;
		}
	},
	addToolbarButtons: function(toolbar, gridPanel) {
		toolbar.add({
			text: 'Preview',
			tooltip: 'Preview Rule evaluation for supported categories and account selection.',
			iconCls: 'preview',
			handler: function() {
				const sm = gridPanel.grid.getSelectionModel();
				if (sm.getCount() === 1) {
					gridPanel.openPreviewWindow(TCG.getValue('ruleDefinition.ruleType.ruleCategory.previewRuleTypeWindowClass', sm.getSelected().json), TCG.getValue('clientAccount', sm.getSelected().json), TCG.getValue('holdingAccount', sm.getSelected().json));
				}
				else {
					gridPanel.openPreviewWindow();
				}
			}
		});
		toolbar.add('-');
	},


	openPreviewWindow: function(previewWindowClass, clientAccount, holdingAccount) {
		const gridPanel = this;
		clientAccount = TCG.isBlank(clientAccount) ? this.getDefaultClientAccount() : clientAccount;
		holdingAccount = TCG.isBlank(holdingAccount) ? this.getDefaultHoldingAccount() : holdingAccount;
		if (TCG.isBlank(previewWindowClass)) {
			TCG.createComponent('TCG.app.CloseWindow', {
				title: 'Select Rule Category to Preview',
				iconCls: 'preview',
				height: 185,
				width: 600,
				openerCt: gridPanel,
				items: [{
					xtype: 'formpanel',
					instructions: 'Preview results for a specific rule category (where supported).  For example, you can preview the list of available executing brokers available based on selected options.  Otherwise, you can preview the rule execution results across all categories.',
					labelWidth: 140,
					items: [
						{
							fieldLabel: 'Rule Category', name: 'name', hiddenName: 'id', requestedProps: 'previewRuleTypeWindowClass', xtype: 'combo', url: 'orderManagementRuleCategoryListFind.json?previewAllowed=true',
							listeners: {
								select: function(combo, record, index) {
									const windowClass = TCG.getValue('previewRuleTypeWindowClass', record.json);
									if (TCG.isNotBlank(windowClass)) {
										TCG.createComponent(windowClass, {
											id: TCG.getComponentId(windowClass),
											defaultData: {clientAccount: clientAccount, holdingAccount: holdingAccount},
											openerCt: gridPanel
										});
										this.getParentForm().getWindow().closeWindow();
									}
								}
							}
						},
						{
							xtype: 'checkbox', boxLabel: 'Preview Execution Results for All Categories', name: 'allCategories',
							listeners: {
								check: function(field) {
									const windowClass = 'Clifton.order.management.setup.rule.preview.RuleExecutionResultPreviewWindow';
									TCG.createComponent(windowClass, {
										id: TCG.getComponentId(windowClass),
										defaultData: {clientAccount: clientAccount, holdingAccount: holdingAccount},
										openerCt: gridPanel
									});
									TCG.getParentFormPanel(field).getWindow().closeWindow();
								}
							}
						}
					]
				}]
			});
			return;
		}
		const clazz = previewWindowClass;
		const cmpId = TCG.getComponentId(clazz);
		TCG.createComponent(clazz, {
			id: cmpId,
			defaultData: {clientAccount: clientAccount, holdingAccount: holdingAccount},
			openerCt: this
		});
	},

	getDefaultClientAccount: function() {
		return undefined;
	},

	getDefaultHoldingAccount: function() {
		return undefined;
	},


	getGridRowContextMenuItems: async function(grid, rowIndex, record) {
		const items = [
			{
				text: 'Open Rule Assignment',
				iconCls: 'rule',
				handler: async () => {
					const clazz = 'Clifton.order.management.setup.rule.RuleAssignmentWindow';
					const id = TCG.getValue('id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			},
			{
				text: 'Open Rule Definition',
				iconCls: 'rule',
				handler: async () => {
					const clazz = 'Clifton.order.management.setup.rule.RuleDefinitionWindow';
					const id = TCG.getValue('ruleDefinition.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}
		];

		if (TCG.isNotBlank(record.json.rollupRuleDefinition)) {
			items.push({
				text: 'Open Rollup Rule Definition',
				iconCls: 'rule',
				handler: async () => {
					const clazz = 'Clifton.order.management.setup.rule.RuleDefinitionWindow';
					const id = TCG.getValue('rollupRuleDefinition.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			});
		}

		if (TCG.isNotBlank(record.json.clientAccount)) {
			items.push('-');
			items.push({
				text: 'Open ' + record.json.clientAccount.label + ' Account Window',
				iconCls: 'account',
				handler: async () => {
					const clazz = 'Clifton.order.shared.account.AccountWindow';
					const id = TCG.getValue('clientAccount.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			});
		}
		if (TCG.isNotBlank(record.json.coalesceHoldingAccountIssuingCompany)) {
			items.push('-');
			items.push({
				text: 'Open ' + record.json.coalesceHoldingAccountIssuingCompany.name + ' Company Window',
				iconCls: 'company',
				handler: async () => {
					const clazz = 'Clifton.order.shared.company.CompanyWindow';
					const id = TCG.getValue('coalesceHoldingAccountIssuingCompany.id', record.json);
					await TCG.createComponent(clazz, {
						id: TCG.getComponentId(clazz, id),
						params: {id: id},
						openerCt: grid
					});
				}
			});
		}
		return items;
	}
});
Ext.reg('order-management-rule-assignment-list-grid', Clifton.order.management.setup.rule.RuleAssignmentListGrid);


Clifton.order.shared.account.OrderAccountWindowAdditionalTabs.push(
	{
		title: 'Account Rules',
		items: [{
			xtype: 'rule-assignment-grid-forAdditionalScope',
			scopeTableName: 'OrderAccount',
			defaultCategoryName: 'Order Allocation Rules',
			defaultViewName: 'Global and Overrides'
		}]
	},

	{
		title: 'OMS Account Rules',
		xtype: 'order-management-rule-assignment-list-grid',

		getTopToolbarFilters: function(toolbar) {
			return [
				{boxLabel: 'Include Global Assignments&nbsp;', xtype: 'toolbar-checkbox', name: 'includeGlobalAssignments'},
				{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
			];
		},


		getLoadParams: function(firstLoad) {
			const t = this.getTopToolbar();
			const includeGlobal = TCG.getChildByName(t, 'includeGlobalAssignments').getValue();
			const params = {assignedAccountId: this.getWindow().getMainFormId()};
			if (TCG.isTrue(includeGlobal)) {
				params.accountIncludeGlobal = true;
			}
			return params;
		},

		getDefaultClientAccount: function() {
			return TCG.isTrue(TCG.getValue('clientAccount', this.getWindow().getMainForm().formValues)) ? this.getWindow().getMainForm().formValues : undefined;
		},
		getDefaultHoldingAccount: function() {
			return TCG.isTrue(TCG.getValue('holdingAccount', this.getWindow().getMainForm().formValues)) ? this.getWindow().getMainForm().formValues : undefined;
		}
	}
);

Clifton.order.shared.company.OrderCompanyWindowAdditionalTabs.push(
	{
		title: 'Issuer Rules',
		xtype: 'order-management-rule-assignment-list-grid',

		getTopToolbarFilters: function(toolbar) {
			return [
				{boxLabel: 'Include Global Assignments&nbsp;', xtype: 'toolbar-checkbox', name: 'includeGlobalAssignments'},
				{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
			];
		},


		getLoadParams: function(firstLoad) {
			const t = this.getTopToolbar();
			const includeGlobal = TCG.getChildByName(t, 'includeGlobalAssignments').getValue();
			const params = {holdingAccountIssuingCompanyId: this.getWindow().getMainFormId()};
			if (TCG.isTrue(includeGlobal)) {
				params.accountIncludeGlobal = true;
			}
			return params;
		}
	}
);


Clifton.order.allocation.OrderAllocationViolationGridPanel = Ext.extend(Clifton.rule.violation.ViolationGridPanel, {
	previewAllowed: false, //Custom preview button defined below
	tableName: 'OrderAllocation',
	batchAlwaysShowStatus: false, // Only show workflow transition results after ignore if there are errors
	showViolationCode: true,
	// OPTIONALLY ADD ORDER GROUP IGNORE VIOLATIONS BUTTON
	addCustomIgnoreButtons: function(toolBar) {
		const gridPanel = this;
		const orderGroupId = gridPanel.getWindow().getMainFormPanel().getFormValue('orderGroup.id');
		if (!TCG.isBlank(orderGroupId)) {
			return [{
				text: 'Ignore Selected (For All Order Allocations In Group)',
				tooltip: 'Mark selected violation as ignored (applies only if description is not required) and will ignore all violations of that type for all Order Allocations in the Order Group',
				iconCls: 'cancel',
				scope: gridPanel,
				handler: function() {
					const grid = this.grid;
					const sm = grid.getSelectionModel();

					if (sm.getCount() === 0) {
						TCG.showError('Please select a violation to mark as ignored.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection ignores are not supported.  Please select one row or click the Ignore All button to mark all as ignored.', 'NOT SUPPORTED');
					}
					else {
						gridPanel.ignoreViolations(null, 'orderGroup.id');
					}
				}
			}];
		}
		return [];
	},
	addCustomEditButtons: function(toolBar) {
		const gridPanel = this;
		toolBar.add({
			text: 'Preview',
			xtype: 'splitbutton',
			tooltip: 'Preview rule options',
			iconCls: 'preview',
			scope: gridPanel,
			isClickOnArrow: function(e) {
				return true;
			},
			menu: new Ext.menu.Menu({
				items: [{
					text: 'All Rules',
					tooltip: 'Preview all violations including those that passed.',
					iconCls: 'rule',
					scope: gridPanel,
					handler: function() {
						gridPanel.previewHandler();
					}
				}, {
					text: 'Order Management Rules',
					tooltip: 'Preview details of each Order Management Rule that applies and if rule execution passed or failed.',
					iconCls: 'verify',
					scope: gridPanel,
					handler: function() {
						const t = this.getWindow().getMainForm().formValues;
						const tradeDate = TCG.getChildByName(this.getWindow(), 'tradeDate').getValue().format('m/d/Y');
						const defaultData = {
							clientAccount: TCG.getValue('clientAccount', t),
							holdingAccount: TCG.getValue('holdingAccount', t),
							tradeDate: tradeDate,
							orderType: TCG.getValue('orderType', t),
							security: TCG.getValue('security', t),
							settlementCurrency: TCG.getValue('settlementCurrency', t),
							executingBrokerCompany: TCG.getValue('executingBrokerCompany', t)
						};

						const className = 'Clifton.order.management.setup.rule.preview.RuleExecutionResultPreviewWindow';
						const cmpId = TCG.getComponentId(className, TCG.getValue('id', t));
						TCG.createComponent(className, {
							id: cmpId,
							defaultData: defaultData,
							openerCt: this
						});
					}
				}]
			})
		});
		toolBar.add('-');
	},
	// Order Allocations after ignored will be automatically validated
	afterIgnoreSuccessAction: function(includeIgnoreRelatedByField) {
		// Specified Violation Id means that we are only ignoring one violation, otherwise, marking all as ignored
		const gridPanel = this;
		const params = {};
		const url = 'orderAllocationValidateActionExecute.json';
		if (includeIgnoreRelatedByField) {
			params['orderGroupId'] = gridPanel.getWindow().getMainFormPanel().getFormValue('orderGroup.id');
		}
		// Always pass this order allocation id so we can refresh main tab with updated values
		params['orderAllocationId'] = gridPanel.getEntityId();

		const loader = new TCG.data.JsonLoader({
			waitTarget: gridPanel,
			waitMsg: 'Validating...',
			params: params,
			conf: params,
			onLoad: function(status, conf) {
				if (status && (gridPanel.batchAlwaysShowStatus || (status.errorList && status.errorList.length > 0))) {
					// show status window only if there are errors to show or it is defined to show always
					TCG.createComponent('Clifton.core.StatusWindow', {
						title: 'Batch Order Allocation Transition Status',
						defaultData: {status: status}
					});
				}
				gridPanel.refreshAfterValidate();
			},
			onFailure: function() {
				gridPanel.refreshAfterValidate();
			}
		});
		loader.load(url);
	},
	refreshAfterValidate: function() {
		this.reload();
		this.getWindow().savedSinceOpen = true;
	}
});
Ext.reg('order-allocation-violations-grid', Clifton.order.allocation.OrderAllocationViolationGridPanel);


Clifton.order.management.OrderAllocationWorkflowListGrid = Ext.extend(Clifton.order.allocation.OrderAllocationListGrid, {
	transitionWorkflowStateList: UNDEFINED, // In addition to right-click functionality
	rowSelectionModel: 'checkbox',
	enableOrderAllocationUploadDragAndDrop: true,
	editor: {
		detailPageClass: 'Clifton.order.allocation.OrderAllocationWindow',
		drillDownOnly: true,
		ptype: 'workflowAwareEntity-grideditor',
		executeMultipleInBatch: true,
		// ignore all response data, which is ignored to speed up each row's transition request
		requestedPropertiesToExclude: 'workflowTransitionCommand',
		tableName: 'OrderAllocation'
	},

	getGridRowContextMenuItems: async function(grid, rowIndex, record) {
		const gridPanel = grid.ownerGridPanel;

		const sm = grid.getSelectionModel();
		// Make sure current row is selected
		sm.selectRow(rowIndex, true);

		const ut = sm.getSelections();
		let allWorkflowItems;
		let allActionItems;

		for (let i = 0; i < ut.length; i++) {
			// add multiple details from the same journal only once
			const thisRecord = ut[i].json;
			const workflowItems = await this.getGridRowContextMenuWorkflowItemsForRecord(gridPanel, thisRecord);
			if (TCG.isNull(allWorkflowItems)) {
				allWorkflowItems = workflowItems;
			}
			else {
				allWorkflowItems = allWorkflowItems.filter(item => {
					for (let j = 0; j < workflowItems.length; j++) {
						const thisItem = workflowItems[j];
						if (TCG.isEquals(item.name, thisItem.name)) {
							return true;
						}
					}
					return false;
				});
			}

			const actionItems = await this.getGridRowContextMenuActionItemsForRecord(gridPanel, thisRecord);

			if (TCG.isNull(allActionItems)) {
				allActionItems = actionItems;
			}
			else {
				allActionItems = allActionItems.filter(item => {
					for (let j = 0; j < actionItems.length; j++) {
						const thisItem = actionItems[j];
						if (TCG.isEquals(item.name, thisItem.name)) {
							return true;
						}
					}
					return false;
				});
			}

		}

		if (TCG.isNotNull(allWorkflowItems) && allWorkflowItems.length > 0) {
			if (TCG.isNotNull(allActionItems) && allActionItems.length > 0) {
				allWorkflowItems.push('-');
				return allWorkflowItems.concat(allActionItems);
			}
			else {
				return allWorkflowItems;
			}
		}
		return allActionItems;
	},

	getGridRowContextMenuActionItemsForRecord: async function(gridPanel, record) {
		const rowItems = [];
		if (gridPanel.isReviseDatesAllowed(record)) {
			rowItems.push(this.getReviseDatesRowMenuItem(gridPanel));
		}
		return rowItems;
	},

	getReviseDatesRowMenuItem: function(gridPanel) {
		return {
			text: 'Revise Dates',
			name: 'Revise Dates',
			iconCls: 'calculator',
			handler: async () => gridPanel.handleReviseDates()
		};
	},

	getGridRowContextMenuWorkflowItemsForRecord: async function(gridPanel, record) {
		const rowItems = [];

		// Probably better to make this dynamic and ask the server for what transitions are available, but we batch, so the ones that don't work would just pop up with an error on the rows that it didn't apply to
		if (TCG.isEquals('Draft', record.workflowStatus.name) || TCG.isEquals('Pending', record.workflowStatus.name) || TCG.isEquals('Review', record.workflowStatus.name)) {
			if (TCG.isEquals('Pending Ops Approval', record.workflowState.name)) {
				rowItems.push(gridPanel.getWorkflowTransitionMenuItem(gridPanel, 'Approve', 'Ops Approve', 'row_reconciled', 'Pending'));
			}
			else if (TCG.isEquals('Pending', record.workflowState.name)) {
				rowItems.push(gridPanel.getWorkflowTransitionMenuItem(gridPanel, 'Approve', 'PM Approve', 'row_reconciled', 'Approved'));
			}
			else {
				rowItems.push(gridPanel.getWorkflowTransitionMenuItem(gridPanel, 'Validate', 'Validate', 'run', 'Pending'));
			}

			rowItems.push(gridPanel.getWorkflowTransitionMenuItem(gridPanel, 'Reject', 'Reject', 'undo', 'Rejected'));
			rowItems.push(gridPanel.getWorkflowTransitionMenuItem(gridPanel, 'Cancel', 'Cancel', 'cancel', 'Canceled'));
		}
		else if (TCG.isEquals('Approved', record.workflowStatus.name)) {
			rowItems.push(gridPanel.getWorkflowTransitionMenuItem(gridPanel, 'Reject', 'Reject', 'undo', 'Rejected'));
		}
		return rowItems;
	},

	getWorkflowTransitionMenuItem(gridPanel, text, name, iconClass, newWorkflowState) {
		return {
			text: text,
			name: name,
			iconCls: iconClass,
			handler: async () => gridPanel.handleWorkflowTransition(newWorkflowState)
		};
	},

	handleReviseDates: function() {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const sm = grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select at least one order allocation.', 'No Row(s) Selected');
		}
		else {
			const rows = sm.getSelections();
			const recordIds = [];
			let invalidSelection = false;
			for (let i = 0; i < rows.length; i++) {
				if (gridPanel.isReviseDatesAllowed(rows[i].json)) {
					recordIds.push(rows[i].json.id);
				}
				else {
					invalidSelection = true;
				}
			}
			if (TCG.isTrue(invalidSelection) && recordIds.length === 0) {
				TCG.showError('You did not select any viable Order Allocations where the trade or settlement date can be revised.  Revising Dates is allowed prior to the order execution only.', 'No Valid Order Allocations Selected');
			}
			else if (TCG.isTrue(invalidSelection)) {
				if ('yes' === TCG.showConfirm('There are some order allocations selected that are not available for Trade or Settlement Date edits. Revising Dates is allowed prior to the order execution only. Would you like to continue with the order allocations that are available for revisions?', 'Revise Dates for Sub-Selection')) {
					TCG.createComponent('Clifton.order.management.activity.OrderManagementReviseDatesWindow', {
						defaultDataIsReal: true,
						defaultData: {orderAllocationIds: recordIds},
						openerCt: gridPanel
					});
				}
			}
			else {
				TCG.createComponent('Clifton.order.management.activity.OrderManagementReviseDatesWindow', {
					defaultDataIsReal: true,
					defaultData: {orderAllocationIds: recordIds},
					openerCt: gridPanel
				});
			}
		}
	},

	isReviseDatesAllowed(record) {
		if (record && record.workflowStatus) {
			if (TCG.isEquals('Approved', record.workflowStatus.name)) {
				return true;
			}
			if (TCG.isEquals('Pending', record.workflowStatus.name)) {
				return true;
			}
			if (TCG.isEquals('Review', record.workflowStatus.name)) {
				return true;
			}
			// This does not do a workflow transition, but we can still edit dates
			if (TCG.isEquals('Draft', record.workflowStatus.name)) {
				return true;
			}
		}
		return false;
	},

	handleWorkflowTransition: function(newWorkflowStateName) {
		const gridPanel = this;
		const editor = gridPanel.editor;
		const grid = gridPanel.grid;
		const sm = grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select a row to transition.', 'No Row(s) Selected');
		}
		else {
			const ut = sm.getSelections();
			editor.transitionList(ut, newWorkflowStateName, editor, gridPanel);
		}
	}


});
Ext.reg('order-management-allocation-workflow-list-grid', Clifton.order.management.OrderAllocationWorkflowListGrid);


Clifton.order.management.OrderAllocationTradingBlotterGrid = Ext.extend(Clifton.order.management.OrderAllocationWorkflowListGrid, {

	applyTraderFilter: true,
	defaultDisplayFilter: 'ALL_APPROVED',
	transitionWorkflowStateList: [
		{stateName: 'Pending', iconCls: 'arrow-left-green', buttonText: 'Return to PM', buttonTooltip: 'Return Selected Order Allocation(s) to PM.  Moves order allocation back to Pending.'},
		{stateName: 'Rejected', iconCls: 'undo', buttonText: 'Reject', buttonTooltip: 'Reject Selected Order Allocation(s).  Can make edits and and re-submit.'}
	],
	additionalPropertiesToRequest: 'clientAccount.id|holdingAccount.id|settlementCurrency.id|orderDestination.destinationType.batched|orderDestination.destinationType.destinationCommunicationType|orderDestination.id|security.id|orderType.id|executingBrokerCompany.id',
	addEditButtons: function(toolBar, gridPanel) {
		toolBar.add({
			text: 'Actions',
			tooltip: 'Assign Trader / Block Orders',
			iconCls: 'blotter',
			scope: this,
			menu: {
				items: [
					{
						text: 'Assign Trader for Selected to Me (Overwrite)',
						iconCls: 'user',
						tooltip: 'Assign Trader for selected Order Allocations to Current User, overwriting Trader if already assigned to another Trader.',
						handler: function() {
							gridPanel.handleActivity('Assign Trader Current User (Overwrite)');
						}
					},
					{
						text: 'Assign Trader for Selected to Me',
						iconCls: 'user',
						tooltip: 'Assign Trader for selected Order Allocations to Current User where not assigned.',
						handler: function() {
							gridPanel.handleActivity('Assign Trader Current User');
						}
					},
					'-',
					{
						text: 'Advanced Options',
						iconCls: 'blotter',
						tooltip: 'Assign Trader / Block Orders',
						handler: function() {
							gridPanel.handleActivity('Advanced Activity Window');
						}
					}
				]
			}
		});
		toolBar.add('-');
	},

	getGridRowContextMenuActionItemsForRecord: async function(gridPanel, record) {
		// If a destination is pre-selected on the order allocation
		// As we expand beyond currency and limited use case set this will need to be more dynamic....
		// If a file (we assume we will batch and block by account) change this to use properties on the destination once batching is added...
		const rowItems = [];
		if (gridPanel.isReviseDatesAllowed(record)) {
			rowItems.push(this.getReviseDatesRowMenuItem(gridPanel));
		}
		if (TCG.isEquals('Approved', record.workflowStatus.name)) {
			// If destination is already supplied,
			if (TCG.isNotNull(record.orderDestination)) {
				return this.appendRowItemsForDestination(record.orderDestination, gridPanel, rowItems);
			}

			// Else if executing broker is selected, use it to look up the destination(s)
			else if (TCG.isNotNull(record.executingBrokerCompany)) {
				const destinationList = await TCG.data.getDataPromise('orderManagementDestinationListForMapping.json?executingBrokerCompanyIdOrNull=' + record.executingBrokerCompany.id + '&orderTypeId=' + record.orderType.id + '&activeOnDate=' + record.tradeDate, gridPanel);
				if (destinationList != null && destinationList.length !== 0) {
					for (let i = 0; i < destinationList.length; i++) {
						const destination = destinationList[i];
						await gridPanel.appendRowItemsForDestination(destination, gridPanel, rowItems);
					}
				}
			}
			// Else look up the applicable executing brokers and find applicable destinations for those brokers
			else {
				// Then get destinations associated with executing broker ids or null
				// for each destination, apply row items
				const brokerList = await TCG.data.getDataPromise('orderManagementRuleExecutingBrokerCompanyListFind.json?command.clientAccountId=' + record.clientAccount.id + '&command.holdingAccountId=' + record.holdingAccount.id + '&command.orderTypeId=' + record.orderType.id + '&command.tradeDate=' + record.tradeDate + '&command.orderSecurityId=' + record.security.id + '&command.settlementCurrencyId=' + record.settlementCurrency.id, gridPanel);

				if (brokerList != null && brokerList.rows && brokerList.rows.length !== 0) {
					const brokerIdResult = [];
					brokerList.rows.forEach(broker => brokerIdResult.push(broker.id));
					const destinationList = await TCG.data.getDataPromise('orderManagementDestinationListForMapping.json?executingBrokerCompanyIdsOrNull=' + brokerIdResult + '&orderTypeId=' + record.orderType.id + '&activeOnDate=' + record.tradeDate, gridPanel);
					if (destinationList != null && destinationList.length !== 0) {
						for (let i = 0; i < destinationList.length; i++) {
							const destination = destinationList[i];
							await gridPanel.appendRowItemsForDestination(destination, gridPanel, rowItems);
						}
					}
				}
			}
		}
		return rowItems;
	},

	appendRowItemsForDestination: async function(orderDestination, gridPanel, rowItems) {
		return TCG.data.getDataPromiseUsingCaching('orderDestinationConfigurationListForDestination.json?orderDestinationId=' + orderDestination.id + '&activeOnly=true', gridPanel, 'DESTINATION_CONFIG_' + orderDestination.id)
			.then(function(result) {
					if (result == null || result.length === 0) {
						result = [{id: null}];
					}
					result.forEach(destinationConfiguration => {
						const destinationConfigLabel = destinationConfiguration.name ? ' - ' + destinationConfiguration.name : '';
						const destinationConfigTooltip = destinationConfiguration.name ? 'Uses ' + destinationConfiguration.name + ' destination configuration' : '';
						if (TCG.isTrue(orderDestination.destinationType.batched)) {
							rowItems.push({
								text: orderDestination.name + ': Block Place and Batch for Sending' + destinationConfigLabel,
								tooltip: 'Block, Place, and Batch for sending to ' + orderDestination.name + '. Blocks by account. ' + destinationConfigTooltip,
								name: 'block-place-send-' + orderDestination.name + '-' + destinationConfiguration.id,
								iconCls: 'email',
								handler: async () => gridPanel.handleActivity('Block (Account) Place Send', orderDestination.id, orderDestination.name, destinationConfiguration.id)
							});
						}
						else if (TCG.isEquals('FIX', orderDestination.destinationType.destinationCommunicationType)) {
							rowItems.push({
								text: orderDestination.name + ': Block Place and Send' + destinationConfigLabel,
								tooltip: 'Block, Place, and send to ' + orderDestination.name + '. Does NOT force blocking by account. ' + destinationConfigTooltip,
								name: 'block-place-send-' + orderDestination.name + '-' + destinationConfiguration.id,
								iconCls: 'shopping-cart-blue',
								handler: async () => gridPanel.handleActivity('Block Place Send', orderDestination.id, orderDestination.name, destinationConfiguration.id)
							});
						}
					});
					return rowItems;
				}
			);
	},

	handleActivity: function(activityName, orderDestinationId, orderDestinationLabel, destinationConfigurationId) {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const sm = grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select at least one order allocation.', 'No Row(s) Selected');
		}
		else {
			const rows = sm.getSelections();
			const recordIds = [];
			for (let i = 0; i < rows.length; i++) {
				recordIds.push(rows[i].json.id);
			}

			// If only one row selected (but more than one in the full list) or all rows selected (but multiple pages) pop open a modal window for additional selections
			if ((rows.length === 1 || rows.length === gridPanel.pageSize) && (rows.length !== grid.getStore().getTotalCount())) {
				const applyActionOptions = [];
				applyActionOptions.push({boxLabel: 'Selected Order Allocation(s) Only', xtype: 'radio', name: 'applyActionOption', inputValue: 'SELECTED', checked: true});
				if (TCG.isNotBlank(orderDestinationId)) {
					applyActionOptions.push({boxLabel: 'All applicable <b>' + orderDestinationLabel + '</b> Order Allocations for Current Filters', xtype: 'radio', name: 'applyActionOption', inputValue: 'ALL_DESTINATION'});
					if (sm.getCount() === 1) {
						applyActionOptions.push({boxLabel: 'All applicable <b>[' + rows[0].json.security.ticker + '] ' + orderDestinationLabel + '</b> Order Allocations for Current Filters', xtype: 'radio', name: 'applyActionOption', inputValue: 'ALL_DESTINATION_SECURITY'});
					}
					applyActionOptions.push({xtype: 'label', html: '<hr/>'});
					applyActionOptions.push({boxLabel: 'Include Order Allocations without a pre-defined destination, but ' + orderDestinationLabel + ' is a valid destination.', name: 'includeEmptyDestinationInFilter', xtype: 'checkbox', hidden: true});
				}
				else {
					applyActionOptions.push({boxLabel: 'All applicable Order Allocations for Current Filters', xtype: 'radio', name: 'applyActionOption', inputValue: 'ALL'});
				}

				TCG.createComponent('TCG.app.OKCancelWindow', {
					title: 'Apply Action to',
					iconCls: 'shopping-cart',
					height: 225,
					width: 700,
					modal: true,

					items: [{
						xtype: 'formpanel',
						loadValidation: false,
						instructions: 'How do you want to apply the selected activity?',
						items: [
							{
								xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'applyActionOption',
								items: applyActionOptions,
								listeners: {
									change: function(radioGroup, checkedRadio) {
										const formPanel = TCG.getParentFormPanel(radioGroup);
										if (!TCG.startsWith(checkedRadio.getRawValue(), 'ALL_DESTINATION')) {
											formPanel.setFormValue('includeEmptyDestinationInFilter', false);
											formPanel.hideField('includeEmptyDestinationInFilter');
										}
										else {
											formPanel.showField('includeEmptyDestinationInFilter');
										}
									}
								}
							}
						]
					}],
					saveWindow: function(closeOnSuccess, forceSubmit) {
						const fp = this.getMainFormPanel();
						const aa = fp.getForm().findField('applyActionOption').getValue().inputValue;
						if (TCG.isEquals('SELECTED', aa)) {
							gridPanel.handleActivityForRecords(activityName, orderDestinationId, destinationConfigurationId, recordIds);
						}
						else {
							const additionalFilters = {};
							const includeEmpty = fp.getForm().findField('includeEmptyDestinationInFilter').getValue();
							if (TCG.isEquals('ALL_DESTINATION', aa) || TCG.isEquals('ALL_DESTINATION_SECURITY', aa)) {
								if (TCG.isTrue(includeEmpty)) {
									additionalFilters['orderAllocationSearchCommand.orderDestinationIdOrNull'] = orderDestinationId;
								}
								else {
									additionalFilters['orderAllocationSearchCommand.orderDestinationId'] = orderDestinationId;
								}
							}
							if (TCG.isEquals('ALL_DESTINATION_SECURITY', aa)) {
								additionalFilters['orderAllocationSearchCommand.securityId'] = rows[0].json.security.id;
							}

							const options = {};
							const params = gridPanel.getExportGridParams(options);
							Promise.resolve(params).then(function(params) {
								// options.params does not get defined if there are no filters and Ext.apply will do nothing if options.params is undefined
								if (!options.params) {
									options.params = {};
								}
								Ext.apply(options.params, params);
								gridPanel.grid.loadMask.onLoad();

								for (const param in options.params) {
									if (options.params.hasOwnProperty(param)) {
										const newParamName = 'orderAllocationSearchCommand.' + param;
										if (!TCG.isEquals('restrictionList', param)) {
											additionalFilters[newParamName] = options.params[param];
										}
										else {
											// Cannot filter on the same field in 2 ways, so if we are adding the filter directly to the field based on above selections, then remove it from the restriction list
											const restrictionList = Ext.util.JSON.decode(options.params[param]);
											if (Ext.isArray(restrictionList)) {
												const newRestrictionList = [];
												for (let i = 0, len = restrictionList.length; i < len; i++) {
													const restriction = restrictionList[i];
													if (TCG.isNull(additionalFilters['orderAllocationSearchCommand.' + restriction.field])) {
														newRestrictionList.push(restriction);
													}
												}
												if (newRestrictionList.length > 0) {
													additionalFilters[newParamName] = Ext.util.JSON.encode(newRestrictionList);
												}
											}
										}
									}
								}
								gridPanel.handleActivityForRecords(activityName, orderDestinationId, destinationConfigurationId, null, additionalFilters);
							});
						}
						this.closeWindow();
					},
					openerCt: this.openerCt
				});
			}
			else {
				gridPanel.handleActivityForRecords(activityName, orderDestinationId, destinationConfigurationId, recordIds);
			}
		}
	},

	handleActivityForRecords: function(activityName, orderDestinationId, destinationConfigurationId, recordIds, additionalFilters) {
		const params = {};
		Ext.apply(params, additionalFilters);
		params.orderAllocationIds = recordIds;

		const gridPanel = this;
		if (activityName === 'Advanced Activity Window') {
			TCG.createComponent('Clifton.order.management.activity.OrderManagementActivityWindow', {
				defaultDataIsReal: true,
				defaultData: {assignTrader: true, assignTraderUserLabel: TCG.getCurrentUser().label, assignTraderUserId: TCG.getCurrentUser().id},
				additionalSubmitParameters: params,
				openerCt: gridPanel
			});
		}
		else if (activityName === 'Assign Trader Current User' || activityName === 'Assign Trader Current User (Overwrite)') {
			params.assignTrader = true;
			params.assignTraderUserId = TCG.getCurrentUser().id;
			params.reassignTraderIfPopulated = (activityName === 'Assign Trader Current User (Overwrite)');

			const loader = new TCG.data.JsonLoader({
				waitTarget: gridPanel,
				waitMsg: 'Assigning...',
				params: params,
				onLoad: function(record, conf) {
					gridPanel.reload();
				}
			});
			loader.load('orderManagementActivityForOrderAllocationsProcess.json?enableValidatingBinding=true');
		}
		else if (activityName === 'Block Place Send' || activityName === 'Block (Account) Place Send') {
			params.assignTrader = true;
			params.assignTraderUserId = TCG.getCurrentUser().id;
			params.createBlockOrders = true;
			params.orderBlockAccount = (activityName === 'Block (Account) Place Send');
			params.createPlacements = true;
			params.sendPlacements = true;
			params.orderDestinationId = orderDestinationId;
			params.destinationConfigurationId = destinationConfigurationId;
			const loader = new TCG.data.JsonLoader({
				waitTarget: gridPanel,
				waitMsg: 'Processing...',
				params: params,
				onLoad: function(record, conf) {
					gridPanel.reload();
				}
			});
			loader.load('orderManagementActivityForOrderAllocationsProcess.json?enableValidatingBinding=true');
		}
	}
});
Ext.reg('order-management-allocation-trading-blotter-grid', Clifton.order.management.OrderAllocationTradingBlotterGrid);


Clifton.order.setup.destination.DestinationWindowAdditionalTabs.push(
	{
		title: 'Destination Mappings',
		items: [{
			xtype: 'gridpanel',
			name: 'orderManagementDestinationMappingListFind',
			instructions: 'The following Destination Mappings are associated with the selected Destination.',
			topToolbarSearchParameter: 'searchPattern',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Order Type', width: 40, dataIndex: 'orderType.name', filter: {searchFieldName: 'orderTypeId', type: 'combo', url: 'orderTypeListFind.json'}, defaultSortColumn: true},
				{header: 'Order Destination', width: 40, dataIndex: 'orderDestination.name', filter: {searchFieldName: 'orderDestinationId', type: 'combo', url: 'orderDestinationListFind.json?executionVenue=true'}, hidden: true},
				{header: 'Executing Broker', width: 75, dataIndex: 'executingBrokerCompany.name', filter: {searchFieldName: 'executingBrokerCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker'}},
				{header: 'Active', width: 23, dataIndex: 'active', type: 'boolean'},
				{header: 'Start Date', width: 25, dataIndex: 'startDate', hidden: true},
				{header: 'End Date', width: 25, dataIndex: 'endDate', hidden: true}
			],
			editor: {
				detailPageClass: 'Clifton.order.management.setup.destination.DestinationMappingWindow'
			},
			getLoadParams: function(firstLoad) {
				return {'orderDestinationId': this.getWindow().getMainFormId()};
			}
		}]
	}
);
