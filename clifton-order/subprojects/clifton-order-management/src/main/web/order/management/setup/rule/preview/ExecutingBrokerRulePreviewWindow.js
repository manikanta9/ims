Clifton.order.management.setup.rule.preview.ExecutingBrokerRulePreviewWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Executing Broker Rules Preview',
	id: 'orderManagementExecutingBrokerRulePreviewWindow',
	iconCls: 'preview',
	width: 800,
	height: 800,

	defaultDataIsReal: true,
	doNotWarnOnCloseModified: true,

	layout: 'border',
	items: [
		{
			xtype: 'formpanel',
			instructions: 'Executing Broker Rule Preview window allows to preview the list of executing brokers eligible to trade for an account and security.',
			region: 'north',
			height: 250,
			labelWidth: 120,

			loadDefaultDataAfterRender: true,

			listeners: {
				afterload: function(fp) {
					if (TCG.isEquals(fp.getFormValue('clientAccount.id'), fp.getFormValue('holdingAccount.id'))) {
						fp.hideField('holdingAccount.label');
						fp.setFieldLabel('clientAccount.label', 'Account:');
					}
				}
			},
			items: [
				{xtype: 'hidden', name: 'clientAccount.clientCompany.id', submitValue: false},
				{
					fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', url: 'orderAccountListFind.json?clientAccount=true', displayField: 'label', allowBlank: false, requestedProps: 'holdingAccount|clientCompany.id|baseCurrencyCode', detailPageClass: 'Clifton.order.shared.account.AccountWindow',
					listeners: {
						select: function(combo, record, index) {
							const clientCompanyId = record.json.clientCompany.id;
							const fp = TCG.getParentFormPanel(combo);
							fp.setFormValue('clientAccount.clientCompany.id', clientCompanyId);
							const holdingAccount = record.json.holdingAccount;
							if (TCG.isTrue(holdingAccount)) {
								fp.setFormValue('holdingAccount.id', {value: record.json.id, text: record.json.label});
								fp.hideField('holdingAccount.label');
								fp.setFieldLabel('clientAccount.label', 'Account:');
							}
							else {
								fp.showField('holdingAccount.label');
								fp.setFieldLabel('clientAccount.label', 'Client Account:');
							}
							const currencyCode = record.json.baseCurrencyCode;
							if (currencyCode) {
								TCG.data.getDataPromiseUsingCaching('orderSecurityForCurrencyTicker.json?ticker=' + currencyCode, fp, 'order.shared.security.currency.' + currencyCode)
									.then(function(curr) {
										fp.setFormValue('settlementCurrency.id', {value: curr.id, text: curr.label});
									});
							}
						}
					}
				},
				{
					fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', xtype: 'combo', url: 'orderAccountListFind.json?holdingAccount=true', displayField: 'label', requiredFields: ['clientAccount.label'], allowBlank: false, detailPageClass: 'Clifton.order.shared.account.AccountWindow',
					requestedProps: 'baseCurrencyCode',
					listeners: {
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							const clientCompanyId = form.findField('clientAccount.clientCompany.id').value;
							queryEvent.combo.store.setBaseParam('clientCompanyId', clientCompanyId);
						},
						select: function(combo, record, index) {
							const currencyCode = record.json.baseCurrencyCode;
							if (currencyCode) {
								TCG.data.getDataPromiseUsingCaching('orderSecurityForCurrencyTicker.json?ticker=' + currencyCode, fp, 'order.shared.security.currency.' + currencyCode)
									.then(function(curr) {
										fp.setFormValue('settlementCurrency.id', {value: curr.id, text: curr.label});
									});
							}
						}
					}

				},
				{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false, value: new Date().format('m/d/Y')},
				{xtype: 'hidden', name: 'securityType.id', submitValue: false, disabled: true},
				{
					fieldLabel: 'Order Type', name: 'orderType.name', hiddenName: 'orderType.id', xtype: 'combo', url: 'orderTypeListFind.json', requestedProps: 'securityType.id', allowBlank: false, detailPageClass: 'Clifton.order.setup.OrderTypeWindow',
					listeners: {
						select: function(combo, record, index) {
							const securityTypeId = record.json.securityType.id;
							const fp = TCG.getParentFormPanel(combo);
							fp.setFormValue('securityType.id', securityTypeId);
						}
					}

				},
				{
					fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', url: 'orderSecurityListFind.json', displayField: 'label', allowBlank: false, requiredFields: ['orderType.name', 'tradeDate'], detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
					listeners: {
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							queryEvent.combo.store.setBaseParam('activeOnDate', form.findField('tradeDate').value);

							const securityTypeId = form.findField('securityType.id').value;
							queryEvent.combo.store.setBaseParam('securityTypeId', securityTypeId);
						}
					}
				},
				{fieldLabel: 'Settlement Currency', name: 'settlementCurrency.label', hiddenName: 'settlementCurrency.id', xtype: 'combo', url: 'orderSecurityListFind.json?securityType=Currency', displayField: 'label', allowBlank: false, detailPageClass: 'Clifton.order.shared.security.SecurityWindow'},
				{xtype: 'label', html: 'Executing brokers available are limited based on destination mappings as they exist for the order type. Can optionally select a specific destination as well to further limit results.'},
				{
					fieldLabel: 'Order Destination', name: 'orderDestination.name', hiddenName: 'orderDestination.id', xtype: 'combo', url: 'orderManagementDestinationListForMapping.json', requiredFields: ['orderType.name', 'tradeDate'], loadAll: true,
					beforequery: function(queryEvent) {
						const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
						queryEvent.combo.store.setBaseParam('activeOnDate', form.findField('tradeDate').value);
						const orderTypeId = form.findField('orderType.id').value;
						queryEvent.combo.store.setBaseParam('orderTypeId', orderTypeId);
					}
				}
			]
		},
		{
			xtype: 'gridpanel',
			region: 'center',
			height: 400,
			title: 'Preview Results',
			name: 'orderManagementRuleExecutingBrokerCompanyListFind',

			viewConfig: {emptyText: 'No valid executing brokers available to trade.', deferEmptyText: false},
			columns: [
				{header: 'Company ID', hidden: true, width: 25, dataIndex: 'id'},
				{header: 'Executing Broker', width: 500, dataIndex: 'label', filter: {searchFieldName: 'searchPattern'}}
			],
			editor: {
				detailPageClass: 'Clifton.order.shared.company.CompanyWindow',
				drillDownOnly: true
			},
			getLoadParams: function(firstLoad) {
				const fp = this.getWindow().getMainFormPanel();
				const o = fp.getFirstInValidField();
				if (TCG.isNull(o)) {
					const formParams = fp.getFormValuesFormatted();
					const params = {};
					params['command.clientAccountId'] = formParams['clientAccount.id'];
					params['command.holdingAccountId'] = formParams['holdingAccount.id'];
					params['command.tradeDate'] = formParams['tradeDate'];
					params['command.orderTypeId'] = formParams['orderType.id'];
					params['command.orderSecurityId'] = formParams['security.id'];
					params['command.settlementCurrencyId'] = formParams['settlementCurrency.id'];
					params['orderDestinationId'] = formParams['orderDestination.id'];
					//params['command.executingBrokerCompanyId'] = formParams['executingBrokerCompany.id'];
					return params;
				}
				else if (!firstLoad) {
					TCG.showError('Please select required fields above.');
				}
				return false;
			}
		}

	]
});
