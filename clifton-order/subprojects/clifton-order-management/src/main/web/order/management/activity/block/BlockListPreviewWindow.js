Clifton.order.management.activity.block.BlockListPreviewWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Block Order Preview',
	iconCls: 'blotter',
	width: 1300,
	height: 700,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	saveTimeout: 200,
	doNotWarnOnCloseModified: true,

	setDoNotWarnOnCloseModified: function(newVal) {
		this.doNotWarnOnCloseModified = newVal;
	},

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		loadDefaultDataAfterRender: true,
		getSaveURL: function() {
			return 'orderManagementBlockListSave.json';
		},
		instructions: 'Use the following screen to apply Blocking rules and block order allocations.',

		defaults: {anchor: '0'},
		items: [
			//Need to add id as a hidden field and submitValue = false so that id is not double submitted
			{name: 'id', xtype: 'hidden', submitValue: false},
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .34,
						layout: 'form',
						items: [
							{fieldLabel: 'TODO'}
						]
					},

					{columnWidth: .03, items: [{html: '&nbsp;'}]},

					{
						columnWidth: .34,
						layout: 'form',
						items: [
							{fieldLabel: 'TODO'}
						]
					},

					{columnWidth: .03, items: [{html: '&nbsp;'}]},

					{
						columnWidth: .26,
						layout: 'form',
						items: [
							{fieldLabel: 'TODO'}
						]
					}
				]
			}
		]
	}]
});


