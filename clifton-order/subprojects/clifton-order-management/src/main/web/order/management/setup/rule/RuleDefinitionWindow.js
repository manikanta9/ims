Clifton.order.management.setup.rule.RuleDefinitionWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderManagementRuleDefinition.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.order.management.setup.rule.StandardRuleDefinitionWindow';
		if ((entity && entity.rollup) || (config && config.defaultData && config.defaultData.rollup)) {
			className = 'Clifton.order.management.setup.rule.RollupRuleDefinitionWindow';
		}
		return className;
	}
});
