Clifton.order.management.blotter.OrderPortfolioManagerBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'orderManagementPortfolioManagementBlotterWindow',
	title: 'PM Order Blotter',
	iconCls: 'shopping-cart',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Order Allocations',
				items: [{
					xtype: 'order-management-allocation-workflow-list-grid',
					defaultDisplayFilter: 'ALL_DRAFT_PENDING',
					transitionWorkflowStateList: [
						{stateName: 'Approved', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approve Selected Order Allocation(s) - Applies to Pending Order Allocations Only'},
						{stateName: 'Pending', iconCls: 'run', buttonText: 'Validate', buttonTooltip: 'Validate Selected Order Allocation(s) - Re-Runs Rule Violations.'},
						{stateName: 'Rejected', iconCls: 'undo', buttonText: 'Reject', buttonTooltip: 'Reject Selected Order Allocation(s).  Can make edits and and re-submit.'},
						{stateName: 'Canceled', iconCls: 'cancel', buttonText: 'Cancel', buttonTooltip: 'Cancel Selected Order Allocation(s)'}
					]
				}]
			},


			{
				title: 'Trades',
				items: [{
					xtype: 'order-trade-list-grid'
				}]
			}

		]
	}]
});

