Clifton.order.management.execution.upload.PlacementExecutionUploadWindow = Ext.extend(TCG.file.DragAndDropPopupWindow, {
	title: 'Upload Placement Execution File',
	iconCls: 'attach',
	height: 400,
	width: 600,
	allowOpenFromModal: true,

	okButtonTooltip: 'Upload',

	saveUrl: 'orderPlacementExecutionFileUpload.json',
	formItems: [
		{
			fieldLabel: 'Upload Converter', beanName: 'orderExecutionUploadConverterBean', xtype: 'system-bean-combo', groupName: 'Order Placement Execution Upload Converter', allowBlank: false,
			qtip: 'The upload converter is used to map columns in the file to the system.  This is generally somewhat unique per broker that sends us a file'

		},
		{fieldLabel: 'Price Threshold', name: 'warnPriceThreshold', xtype: 'currencyfield', decimalPrecision: 4, minValue: 0, value: 5, qtip: 'Percent threshold for difference from execution price to latest available. Leave blank to not run this check'},
		{fieldLabel: '', boxLabel: 'Expect all Placements to be fully filled', xtype: 'checkbox', name: 'warnIfNotFullyFilled', qtip: 'Leave checked to verify each placement in the upload file is fully filled. If any found that are not fully filled, they will be skipped and a warning added to the results.', checked: true},
		{fieldLabel: '', boxLabel: 'Fail Upload If Any Warnings or Errors', xtype: 'checkbox', name: 'failUploadIfAnyWarningsOrErrors', qtip: 'Leave checked to not upload any rows if any pre-checks fail for errors or warnings (quantity, price)', checked: true},
		{xtype: 'sectionheaderfield', header: 'Attach Upload File as Note to Placement(s)'},
		{name: 'columnGroupName', xtype: 'hidden', value: 'System Note Custom Fields'}, // I think because of items dynamically being added this doesn't appear to populate, so manually setting for now
		{
			fieldLabel: 'Note Type', name: 'note.noteType.name', hiddenName: 'note.noteType.id', xtype: 'combo', detailIdField: 'note.noteType.id', detailPageClass: 'Clifton.system.note.TypeWindow', url: 'systemNoteTypeListFind.json?excludeAttachmentSupportedType=NONE&tableName=OrderPlacement',
			requestedProps: 'orderSupported|internal|dateRangeAllowed|endDateResolutionDate|defaultNoteTextPopulated',
			// reset document related fields based on selection
			selectHandler: function(combo, record, index) {
				combo.ownerCt.getWindow().resetNoteTypeFields(combo.ownerCt, record.json);
			}
		},
		{fieldLabel: 'Note Text', name: 'note.text', xtype: 'textarea'},
		{fieldLabel: 'Order', name: 'note.order', xtype: 'spinnerfield', allowNegative: false},
		{name: 'note.privateNote', xtype: 'checkbox', boxLabel: 'Private Note (Should be viewed by internal employees only)'},
		{fieldLabel: 'Start Date', xtype: 'datefield', name: 'note.startDate'},
		{fieldLabel: 'End Date', xtype: 'datefield', name: 'note.endDate'},
		{xtype: 'label', html: '<hr/>'}
	],

	saveCallBackFunction: function(data) {
		if (data) { //} && (gridPanel.batchAlwaysShowStatus || (status.errorList && status.errorList.length > 0))) {
			TCG.createComponent('Clifton.core.StatusWindow', {
				title: 'Placement Execution Update Status',
				defaultData: {status: data}
			});
		}
	},

	getDefaultData: function(win, formPanel) {
		const dd = Ext.apply(win.defaultData || {}, {});
		const fileName = win.ddFiles[0].name;
		return TCG.data.getDataPromise('orderExecutionUploadConverterBeanForFileName.json?fileName= ' + fileName, this)
			.then(function(systemBean) {
				if (TCG.isNotNull(systemBean)) {
					// set orderExecutionUploadConverterBean value/text
					dd.orderExecutionUploadConverterBean = systemBean;
				}
				return TCG.data.getDataPromiseUsingCaching('systemTableByName.json?tableName=OrderPlacement', this, 'system.table.OrderPlacement')
					.then(function(tbl) {
						if (tbl) {
							return TCG.data.getDataPromiseUsingCaching('systemNoteTypeByTableAndName.json?tableId=' + tbl.id + '&noteTypeName=Execution File', this, 'system.note.type.OrderPlacement.Execution File')
								.then(function(noteType) {
									if (noteType) {
										dd.note = {'noteType': noteType};
										win.resetNoteTypeFields(formPanel, noteType);
									}
									return dd;
								});
						}
						else {
							return dd;
						}
					});
			});

	},

	resetNoteTypeFields: function(fp, record) {
		if (record) {
			const f = fp.getForm();
			const orderField = f.findField('note.order');
			const orderSupported = record.orderSupported;
			orderField.setDisabled(!orderSupported);
			orderField.setVisible(orderSupported);
			if (!orderSupported) {
				orderField.setValue('');
			}

			const privateField = f.findField('note.privateNote');
			if (record.internal === true) {
				privateField.setValue(true);
				privateField.setDisabled(true);
				privateField.setVisible(false);
			}
			else {
				privateField.setDisabled(false);
			}

			const startDateField = f.findField('note.startDate');
			startDateField.setDisabled(!record.dateRangeAllowed);
			startDateField.setVisible(record.dateRangeAllowed);

			const endDateField = f.findField('note.endDate');
			if (record.dateRangeAllowed === true || record.endDateResolutionDate === true) {
				endDateField.setDisabled(false);
				endDateField.setVisible(true);
				let lbl = 'End Date:';
				if (record.endDateResolutionDate === true) {
					lbl = 'Resolution Date: ';
				}
				endDateField.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(lbl);
			}
			else {
				endDateField.setValue('');
				endDateField.setDisabled(true);
				endDateField.setVisible(false);
			}

			const noteTextField = f.findField('note.text');
			if (record.defaultNoteTextPopulated === true && noteTextField.getValue() === '') {
				noteTextField.setValue(record.defaultNoteText);
			}
			noteTextField.allowBlank = (record.blankNoteAllowed === true);
		}
	}
});
