Clifton.order.management.setup.destination.DestinationSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'orderManagementDestinationSetupWindow',
	title: 'Order Destination Setup',
	iconCls: 'repo',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Destination Mappings',
				items: [{
					name: 'orderManagementDestinationMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Defines allowed order destinations and executing brokers for each order type.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Order Type', width: 40, dataIndex: 'orderType.name', filter: {searchFieldName: 'orderTypeId', type: 'combo', url: 'orderTypeListFind.json'}, defaultSortColumn: true},
						{header: 'Order Destination', width: 40, dataIndex: 'orderDestination.name', filter: {searchFieldName: 'orderDestinationId', type: 'combo', url: 'orderDestinationListFind.json?executionVenue=true'}},
						{header: 'Executing Broker', width: 75, dataIndex: 'executingBrokerCompany.name', filter: {searchFieldName: 'executingBrokerCompanyId', type: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker'}},
						{header: 'Active', width: 23, dataIndex: 'active', type: 'boolean'},
						{header: 'Start Date', width: 25, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 25, dataIndex: 'endDate', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.order.management.setup.destination.DestinationMappingWindow'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						return {};
					}
				}]
			},


			{
				title: 'Order Destinations',
				items: [{
					name: 'orderDestinationListFind',
					xtype: 'gridpanel-custom-json-fields',
					defaultHidden: true,
					tableName: 'OrderDestination',
					instructions: 'A list of all order destinations.  A destination specifies where and how the placement for the order should be routed.  Each order destination is associated with a specific destination type.',
					topToolbarSearchParameter: 'searchPattern',
					nonCustomColumns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Destination Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Destination Type', width: 50, dataIndex: 'destinationType.name', filter: {searchFieldName: 'destinationTypeId', type: 'combo', url: 'orderDestinationTypeListFind.json'}},
						{header: 'Destination Description', width: 200, dataIndex: 'description'},
						{header: 'Back Up Destination', width: 40, dataIndex: 'backupDestination', type: 'boolean', filter: {searchFieldName: 'backupDestination'}, hidden: true},
						{header: 'Main Destination', width: 50, dataIndex: 'mainOrderDestination.name', filter: {searchFieldName: 'mainOrderDestinationId', type: 'combo', url: 'orderDestinationListFind.json?backupDestination=false'}},
						{header: 'Execution Venue', width: 40, dataIndex: 'destinationType.executionVenue', type: 'boolean', filter: {searchFieldName: 'executionVenue'}},
						{header: 'Company Mappings', width: 50, dataIndex: 'orderCompanyMapping.name', filter: {searchFieldName: 'orderCompanyMapping.id', type: 'combo', url: 'systemFieldMappingListFind.json?fieldMappingDefinitionName=Broker Mappings'}},
						{header: 'Force Blocking By Account', dataIndex: 'forceBlockingByAccount', type: 'boolean', tooltip: 'Used for Execution Venues only, if true blocking logic requires blocking by account and multiple account allocations is not supported.', hidden: true},
						{header: 'Disabled', width: 30, dataIndex: 'disabled', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.order.setup.destination.DestinationWindow'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('disabled', false);
						}
						return {};
					}
				}]
			},


			{
				title: 'Destination Configurations',
				items: [{
					xtype: 'order-destination-configuration-list-grid'
				}]
			},


			{
				title: 'Order Destination Types',
				items: [{
					name: 'orderDestinationTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all order destination types.  Destination Type classifies different placement routing types.  These can be used to limit/customize selections on destinations of each type.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Destination Type Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Destination Type Description', width: 150, dataIndex: 'description'},
						{header: 'Execution Venue', width: 30, dataIndex: 'executionVenue', type: 'boolean', tooltip: 'If checked, then destinations under this type are used to place orders with a specific destiination/execution venue.  If unchecked, then the destination is used for some other type of routing for Order related entities (i.e. sending Trades to accounting system)'},
						{header: 'Communication Type', width: 35, dataIndex: 'destinationCommunicationType', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.order.setup.destination.DESTINATION_COMMUNICATION_TYPES}}, qtip: 'The communication type for destinations under this type.'},
						{header: 'Configuration Bean Group', width: 50, dataIndex: 'destinationConfigurationBeanGroup.name', filter: {searchFieldName: 'destinationConfigurationBeanGroupId', type: 'combo', url: 'systemBeanGroupListFind.json'}},
						{header: 'Batched', width: 25, dataIndex: 'batched', type: 'boolean', tooltip: 'Currently only allowed for Files and required for files.  All placements for destinations under this type are batched and sent via a file based on a schedule.'},
						{header: 'Batch Source Table', width: 50, dataIndex: 'batchSourceSystemTable.name', filter: {searchFieldName: 'batchSourceSystemTableId', type: 'combo', url: 'systemTableListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.order.setup.destination.DestinationTypeWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});

