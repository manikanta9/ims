Clifton.order.management.setup.destination.DestinationMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Destination Mapping',
	iconCls: 'shopping-cart',
	width: 800,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Defines allowed order destinations and executing brokers for each order type.',
					url: 'orderManagementDestinationMapping.json',
					labelWidth: 220,
					items: [
						{fieldLabel: 'Order Type', name: 'orderType.name', hiddenName: 'orderType.id', xtype: 'combo', url: 'orderTypeListFind.json', detailPageClass: 'Clifton.order.setup.OrderTypeWindow'},
						{fieldLabel: 'Order Destination', name: 'orderDestination.name', hiddenName: 'orderDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', url: 'orderDestinationListFind.json?executionVenue=true&backupDestination=false&disabled=false'},
						{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.name', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker'},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
					]
				}]
			}
		]
	}]
});
