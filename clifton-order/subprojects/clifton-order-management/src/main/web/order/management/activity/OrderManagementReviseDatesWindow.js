Clifton.order.management.activity.OrderManagementReviseDatesWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Order Management - Revise Dates',
	iconCls: 'calculator',
	height: 500,
	width: 800,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Use the following selections to revise trade and or settlement date changes to the selected list of Order Allocations.  Any order allocations that result in no change will not actually be attempted to be adjusted',
		items: [
			{xtype: 'sectionheaderfield', header: 'Trade Date Adjustments'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'tradeDateAdjustment',
				items: [
					{boxLabel: 'Do Not Adjust Trade Date', xtype: 'radio', name: 'tradeDateAdjustment', inputValue: 'NONE', checked: true, qtip: 'Do not change trade date of selected order allocations.'},
					{boxLabel: 'Recalculate Trade Date to Next Available', xtype: 'radio', name: 'tradeDateAdjustment', inputValue: 'RECALCULATE', qtip: 'Recalculate trade date to next available as of now.'},
					{boxLabel: 'Set Trade Date to T+1', xtype: 'radio', name: 'tradeDateAdjustment', inputValue: 'T_PLUS_ONE', qtip: 'Move trade date forward one day (uses trading business days)'},
					{boxLabel: 'Set Trade Date to:', xtype: 'radio', name: 'tradeDateAdjustment', inputValue: 'OVERRIDE', qtip: 'Manually enter the new Trade Date'}
				],
				listeners: {
					change: (radioGroup, checkedItem) => {
						const formPanel = TCG.getParentFormPanel(radioGroup);
						// Show override field if applies
						switch (checkedItem && checkedItem.inputValue) {
							case 'OVERRIDE':
								formPanel.showHideFormFragment('overrideTradeDateFragment', true);
								formPanel.getForm().findField('overrideTradeDate').allowBlank = false;
								break;
							default:
								formPanel.showHideFormFragment('overrideTradeDateFragment', false);
								formPanel.getForm().findField('overrideTradeDate').allowBlank = true;
						}
					}
				}
			},
			{
				xtype: 'formfragment',
				frame: false,
				name: 'overrideTradeDateFragment',
				hidden: true,
				items: [
					{fieldLabel: 'Trade Date', name: 'overrideTradeDate', xtype: 'datefield'}
				]
			},
			{xtype: 'sectionheaderfield', header: 'Settlement Date Adjustments'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'settlementDateAdjustment',
				items: [
					{boxLabel: 'Do Not Adjust Settlement Date', xtype: 'radio', name: 'settlementDateAdjustment', inputValue: 'NONE', checked: true, qtip: 'Do not change settlement date of selected order allocations'},
					{boxLabel: 'Same Day Settlement', xtype: 'radio', name: 'settlementDateAdjustment', inputValue: 'SAME_DAY', qtip: 'Make settlement date = trade date'},
					{boxLabel: 'Recalculate Settlement Date based on Trade Date and Default Days to Settle', xtype: 'radio', name: 'settlementDateAdjustment', inputValue: 'RECALCULATE', qtip: 'Recalculate Settlement Date based on trade date and default days to settle.'},
					{boxLabel: 'Move Settlement Date forward one business day', xtype: 'radio', name: 'settlementDateAdjustment', inputValue: 'T_PLUS_ONE', qtip: 'Move settlement date forward one business day.'},
					{boxLabel: 'Set Settlement Date to:', xtype: 'radio', name: 'settlementDateAdjustment', inputValue: 'OVERRIDE', qtip: 'Manually enter the new Settlement Date'}
				],
				listeners: {
					change: (radioGroup, checkedItem) => {
						const formPanel = TCG.getParentFormPanel(radioGroup);
						// Show override field if applies
						switch (checkedItem && checkedItem.inputValue) {
							case 'OVERRIDE':
								formPanel.showHideFormFragment('overrideSettlementDateFragment', true);
								formPanel.getForm().findField('overrideSettlementDate').allowBlank = false;
								break;
							default:
								formPanel.showHideFormFragment('overrideSettlementDateFragment', false);
								formPanel.getForm().findField('overrideSettlementDate').allowBlank = true;
						}
					}
				}
			},
			{
				xtype: 'formfragment',
				frame: false,
				name: 'overrideSettlementDateFragment',
				hidden: true,
				items: [
					{fieldLabel: 'Settlement Date', name: 'overrideSettlementDate', xtype: 'datefield'}
				]
			},
			{xtype: 'sectionheaderfield', header: 'Business Day Adjustments'},
			{xtype: 'checkbox', name: 'autoAdjustReviseDatesForBusinessDays', boxLabel: 'Auto adjust all dates forward based on trading/settlement date business days', checked: true, qtip: 'For example, if same day settlement is selected but that date is not a valid settlement date automatically adjust settlement date forward one business day.'}
		],

		showHideFormFragment: function(formFragmentName, show) {
			const formPanel = this;
			TCG.getChildByName(formPanel, formFragmentName).setVisible(show);
			TCG.getChildByName(formPanel, formFragmentName).items.items.forEach(function(fld) {
				// Don't auto enable if it uses required fields
				if (!show || TCG.isBlank(fld.requiredFields)) {
					fld.setDisabled(!show);
				}
			});
		},

		getSaveURL: function() {
			return 'orderManagementReviseDatesForOrderAllocationsProcess.json?enableValidatingBinding=true';
		},
		getSubmitParams: function() {
			const data = this.getWindow().defaultData;
			return {orderAllocationIds: data.orderAllocationIds, reviseDates: true};
		}
	}
	]
})
;
