Clifton.order.management.execution.PlacementPostExecutionAmendWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Amend Order Placement',
	iconCls: 'shopping-cart',
	titlePrefixSeparator: ': ',
	height: 500,
	width: 800,
	modal: true,
	defaultDataIsReal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelFieldName: 'labelShort',
		maxWindowTitleLength: 100,
		instructions: 'Use the following selections to revise trade and or settlement date changes to the selected list of Order Allocations.  Any order allocations that result in no change will not actually be attempted to be adjusted.  If the placement is only a partial placement for the block order than dates cannot be adjusted here.',
		url: 'orderPlacement.json',
		getSaveURL: function() {
			return 'orderManagementExecutionActionForCommandProcess.json';
		},

		listeners: {
			afterload: function(formPanel) {
				if (TCG.isTrue(formPanel.getFormValue('orderBlock.security.currency', false, false))) {
					formPanel.setFieldLabel('orderBlock.security.label', 'Given CCY:');
					formPanel.setFieldLabel('averageUnitPrice', ' Exchange Rate:');
					formPanel.setFieldLabel('newAverageUnitPrice', ' Exchange Rate:');

					// If 1 placement for the block order then allow trade/settlement dates to be changed
				}
			}
		},

		items: [
			{xtype: 'hidden', name: 'orderPlacementId', type: 'int'},
			{xtype: 'hidden', name: 'orderExecutionAction', value: 'AMEND_EXECUTION'},
			{
				xtype: 'columnpanel',
				columns: [
					{
						rows: [
							{xtype: 'sectionheaderfield', header: 'Current'},
							{fieldLabel: 'Trade Date', name: 'orderBlock.tradeDate', xtype: 'datefield', readOnly: true, submitValue: false},
							{fieldLabel: 'Settlement Date', name: 'orderBlock.settlementDate', xtype: 'datefield', readOnly: true, submitValue: false},
							{fieldLabel: 'Avg Price', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true, submitValue: false}
						],
						config: {columnWidth: 0.44}
					},
					{
						rows: [
							{xtype: 'label', html: '&nbsp;'}
						],
						config: {columnWidth: 0.1}
					},
					{
						rows: [
							{xtype: 'sectionheaderfield', header: 'New'},
							{fieldLabel: 'Trade Date', name: 'overrideTradeDate', xtype: 'datefield'},
							{fieldLabel: 'Settlement Date', name: 'overrideSettlementDate', xtype: 'datefield'},
							{fieldLabel: 'Avg Price', name: 'overrideAverageUnitPrice', xtype: 'floatfield'}
						],
						config: {columnWidth: 0.46}
					}
				]
			},
			{xtype: 'label', html: '&nbsp;'},
			{fieldLabel: 'Price Threshold', name: 'warnPriceThreshold', xtype: 'currencyfield', decimalPrecision: 4, minValue: 0, value: 5, qtip: 'Percent threshold for difference from execution price to latest available. Leave blank to not run this check'},
			{xtype: 'sectionheaderfield', header: 'Reason for Amendment'},
			{fieldLabel: '', xtype: 'textarea', name: 'amendmentReasonNote', allowBlank: false, height: 50, grow: true, qtip: 'A reason for amending the execution information is required.'}
		]
	}]
});
