Clifton.order.management.blotter.OrderTradingBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'orderManagementTradingBlotterWindow',
	title: 'Trading Blotter',
	iconCls: 'shopping-cart',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Order Allocations',
				items: [{
					xtype: 'order-management-allocation-trading-blotter-grid'
				}]
			},


			{
				title: 'Order Allocations (Grouped)',
				xtype: 'panel',
				name: 'order-allocation-grouped-panel',
				layout: 'border',
				defaultView: 'Currency View',

				listeners: {
					render: function() {
						const panel = this;
						this.generateToolbar(panel);

						this.setDefaultView('Currency View'); // sets it as "checked"

					},
					afterrender: function() {
						this.switchToView('Currency View');
					}

				},

				tbar: {
					xtype: 'toolbar',
					name: 'order-allocation-top-toolbar',
					items: []
				},

				viewNames: ['Default View', 'Currency View'],
				generateToolbar: function(panel) {
					const t = panel.getTopToolbar();
					const menu = new Ext.menu.Menu();
					let i;
					for (i = 0; i < this.viewNames.length; i++) {
						const v = this.viewNames[i];
						menu.add({
							text: v.text || v,
							xtype: 'menucheckitem',
							tooltip: v.tooltip || void 0,
							checked: (i === 0),
							group: 'view' + panel.id,
							handler: function() {
								panel.switchToView(this.text);
							}
						});
					}
					t.add({
						text: 'Views',
						iconCls: 'views',
						menu: menu
					});
					t.add('-');
					t.add({
						text: 'Reload',
						tooltip: 'Reload latest grid data',
						iconCls: 'table-refresh',
						handler: function() {
							panel.reload();
						}
					});
					t.add('-');
					t.add('->');

					this.addToolbarFilters(t, this);
				},

				reload: function() {
					const detailGrid = TCG.getChildByName(this, 'orderAllocationGroupDetailGrid');
					detailGrid.reload();

					const groupedGrid = TCG.getChildByName(this, 'orderAllocationGroupedHierarchicalResultListFind');
					groupedGrid.reload();

					const aggregateGrid = TCG.getChildByName(this, 'orderAllocationAggregateGrid');
					aggregateGrid.reload();
				},

				addToolbarFilters: function(toolbar, gridPanel) {
					const detailGrid = TCG.getChildByName(this, 'orderAllocationGroupDetailGrid');
					const filters = detailGrid.getTopToolbarFilters();
					if (TCG.isNotNull(toolbar) && TCG.isNotNull(filters)) {
						toolbar.add('->');
						for (let i = 0; i < filters.length; i++) {
							let filter = filters[i];
							// labels are not displayed by default in the toolbar
							if (filter.fieldLabel) {
								const labelSeparator = (filter.labelSeparator !== undefined) ? filter.labelSeparator : ':';
								const tabLabelField = {xtype: 'tbtext', text: '&nbsp;' + filter.fieldLabel + labelSeparator + '&nbsp;', hidden: filter.hidden};
								if (filter.tooltip) {
									tabLabelField.qtip = filter.tooltip;
								}
								toolbar.add(tabLabelField);
							}
							if (filter.linkedFilter) {
								// on toolbar filter change, update menu filter
								let ls = filter.listeners;
								if (!ls) {
									ls = {};
									filter.listeners = ls;
								}
								if (!ls.select) {
									ls.select = function(combo, record) {
										detailGrid.setFilterValue(combo.linkedFilter, {value: combo.getValue(), text: combo.getRawValue()}, !combo.linkedFilterCancelReload);
									};
								}
								if (!ls.blur) {
									ls.blur = function(combo) {
										if (combo.getValue() === '') { // clear on clear
											detailGrid.setFilterValue(combo.linkedFilter, {value: '', text: ''}, !combo.linkedFilterCancelReload);
										}
									};
								}
							}
							filter = toolbar.add(filter);

							if (filter.linkedFilter) {
								const linkedColumnFilter = detailGrid.grid.filters.getFilter(filter.linkedFilter);
								if (TCG.isNotNull(linkedColumnFilter) && TCG.isNotNull(linkedColumnFilter.combo)) {
									// on menu filter change update toolbar filter
									const mf = linkedColumnFilter.combo;
									mf.linkedCombo = filter;
									mf.addListener('select', function(combo, record) {
										const filter = combo.linkedCombo;
										filter.lastSelectionText = combo.getRawValue();
										Ext.form.ComboBox.superclass.setValue.call(filter, filter.lastSelectionText);
										filter.value = combo.getValue();
									});
								}
							}
						}
					}
				},

				setFilterValue: function(dataIndex, value, doNotCancelReload, activate, customComparison) {
					const detailGrid = TCG.getChildByName(this, 'orderAllocationGroupDetailGrid');
					const filters = detailGrid.grid.filters;
					const currentFilter = filters.getFilter(dataIndex);

					// if this value is already set, activate the filter
					if (currentFilter.getValue() === value && currentFilter.active === false) {
						currentFilter.setActive(true);
					}
					else {
						currentFilter.setValue(value);
					}
					if (customComparison && currentFilter.combo) {
						currentFilter.combo.setValue(customComparison);
						currentFilter.comparison = customComparison;
					}
					// cancel reloading triggered by setting the filter
					if (!doNotCancelReload) {
						filters.deferredUpdate.cancel();
					}
					if (activate === true) {
						currentFilter.setActive(currentFilter.isActivatable(), true);
					}
				},

				clearFilter: function(dataIndex, suppressEvent) {
					const detailGrid = TCG.getChildByName(this, 'orderAllocationGroupDetailGrid');
					const filters = detailGrid.grid.filters;
					const currentFilter = filters.getFilter(dataIndex);
					currentFilter.setActive(false, suppressEvent);
				},

				clearFilters: function(suppressEvent) {
					const detailGrid = TCG.getChildByName(this, 'orderAllocationGroupDetailGrid');
					detailGrid.grid.filters.filters.each(function(filter) {
						filter.setActive(false, suppressEvent);
					});
				},

				// Can be called after render to default the view to something else before loading the first time
				// Will also update the menu item so it's set as checked
				// But doesn't call "reload" since grid hasn't been loaded yet.
				setDefaultView: function(viewName) {
					const tb = this.getTopToolbar();
					// Increment By two because of the | separators
					for (let i = 0; i < tb.items.length; i = i + 2) {
						const button = tb.items.items[i];
						if (button.text === 'Views') {
							for (let j = 0; j < button.menu.items.length; j++) {
								const mi = button.menu.items.items[j];
								if (mi.text === viewName) {
									// true to suppress checked event so it doesn't try to switch it again
									mi.setChecked(true, true);
									break;
								}
							}
							break;
						}
					}
				},

				switchToView: function(viewName, cancelReload) {
					const detailGrid = TCG.getChildByName(this, 'orderAllocationGroupDetailGrid');
					detailGrid.switchToView(viewName, true);

					const aggregateGrid = TCG.getChildByName(this, 'orderAllocationAggregateGrid');
					aggregateGrid.switchToView(viewName, true);


					if (!TCG.isTrue(cancelReload)) {
						this.reload();
					}
				},


				items: [
					{
						layout: 'border',
						region: 'north',
						height: 300,
						collapsible: true,
						items: [
							{
								region: 'west',
								split: true,
								xtype: 'formpanel',
								labelWidth: 5,
								collapsible: true,
								width: 500,
								title: 'Groupings',
								items: [
									{
										xtype: 'treegrid',
										name: 'orderAllocationGroupedHierarchicalResultListFind',
										instructions: 'Use the grouping fields in the toolbar to define how to display grouped results',
										appendStandardColumns: false,
										readOnly: true,
										useArrows: true,
										showExpandCollapseShortcut: true,
										layout: 'fit',

										addTopToolbarItems: function(toolbar) {
											const treeGrid = this;
											toolbar.add('-');
											toolbar.add({
												xtype: 'listfield',
												name: 'groupingFields',
												defaultItems: ['Order Destination'],
												allowBlank: true,
												controlConfig: {
													displayField: 'name', valueField: 'name', xtype: 'string-combo',
													stringArray: Clifton.order.allocation.OrderAllocationGroupingPropertiesArray
												},
												listeners: {
													'change': function(field, newValue, oldValue) {
														treeGrid.reload();
													}
												}
											});
										},

										columns: [
											{
												header: '&nbsp;', width: 375, dataIndex: 'groupValue',
												renderer: function(v, nodeData) {
													let displayValue = TCG.getValue('groupProperty.label', nodeData) + ': ';
													if (TCG.isBlank(v)) {
														displayValue += TCG.getValue('groupProperty.emptyValueLabel', nodeData);
													}
													else {
														displayValue += (TCG.isBlank(nodeData.groupLabel) ? nodeData.groupValue : nodeData.groupLabel);
													}
													displayValue += ' (' + nodeData.totalCount + ')';
													return displayValue;
												}
											}
										],


										listeners: {
											'click': function(node, e) {
												this.applyGroupingFiltersToDetailGrid(node);
											}
										},


										applyGroupingFiltersToDetailGrid: function(node) {
											this.setRemoveGroupingFiltersFromDetailGrid(node, false);
										},

										removeGroupingFiltersFromDetailGrid: function() {
											this.setRemoveGroupingFiltersFromDetailGrid(null, false);
										},

										detailAdditionalRestrictions: [],
										setRemoveGroupingFiltersFromDetailGrid: function(node, suppressReload) {
											this.detailAdditionalRestrictions = [];
											const detailGrid = TCG.getChildByName(TCG.getParentTabPanel(this), 'orderAllocationGroupDetailGrid');
											const groupingProperties = this.getGroupingPropertyArray();
											if (TCG.isNotNull(groupingProperties)) {
												groupingProperties.forEach(groupingProperty => {
													const gp = Clifton.order.allocation.OrderAllocationGroupingProperties.find(function(item) {
														if (item.name === groupingProperty) {
															return true;
														}
														return false;
													});
													const filters = detailGrid.grid.filters;
													if (TCG.isNotNull(filters.getFilter(gp.dataIndex))) {
														detailGrid.clearFilter(gp.dataIndex, true);
													}
													while (node) {
														const nodeData = node.attributes;
														if (TCG.isNotBlank(TCG.getValue('groupProperty.searchFieldName', nodeData))) {
															if (TCG.isBlank(nodeData.groupValue)) {
																this.detailAdditionalRestrictions.push({
																	field: TCG.getValue('groupProperty.searchFieldName', nodeData),
																	data: {comparison: 'IS_NULL'}
																});
															}
															else {
																this.detailAdditionalRestrictions.push({
																	field: TCG.getValue('groupProperty.searchFieldName', nodeData),
																	data: {comparison: 'EQUALS', value: nodeData.groupValue}
																});
															}
														}
														node = node.parentNode;
													}
												});

												if (TCG.isFalse(suppressReload)) {
													detailGrid.reload();

													const aggregateGrid = TCG.getChildByName(TCG.getParentTabPanel(this), 'orderAllocationAggregateGrid');
													aggregateGrid.reload();
												}
											}
										},


										getLoadParams: function(firstLoad) {
											const grid = this;

											if (TCG.isTrue(firstLoad) || TCG.isNull(this.getGroupingPropertyList())) {
												return false;
											}
											this.removeGroupingFiltersFromDetailGrid();

											const gd = TCG.getChildByName(TCG.getParentTabPanel(this), 'orderAllocationGroupDetailGrid');
											const params = this.getDetailGridLoadParamsWithoutGroupedPropertyFilters(gd);
											return Promise.resolve(params).then(function(params) {
												params = Ext.apply(params, {
													requestedPropertiesRoot: 'data',
													requestedProperties: 'id|groupProperty.beanPropertyName|groupProperty.searchFieldName|groupProperty.label|groupProperty.emptyValueLabel|groupValue|groupLabel|leaf|children|totalCount',
													groupingPropertyList: Ext.util.JSON.encode(grid.getGroupingPropertyList())
												});
												return params;
											});
										},


										getDetailGridLoadParamsWithoutGroupedPropertyFilters: function(gridPanel) {
											const groupingProperties = this.getGroupingPropertyArray();
											const options = {};
											const params = gridPanel.getExportGridParams(options);

											return Promise.resolve(params).then(function(params) {
												// options.params does not get defined if there are no filters and Ext.apply will do nothing if options.params is undefined
												if (!options.params) {
													options.params = {};
												}
												Ext.apply(options.params, params);
												gridPanel.grid.loadMask.onLoad();

												const additionalFilters = {};
												for (const param in options.params) {
													if (options.params.hasOwnProperty(param)) {
														if (!TCG.isEquals('restrictionList', param)) {
															additionalFilters[param] = options.params[param];
														}
														else {
															// Remove the filter for any field that we are grouping on
															const restrictionList = Ext.util.JSON.decode(options.params[param]);
															if (Ext.isArray(restrictionList)) {
																const newRestrictionList = [];
																for (let i = 0, len = restrictionList.length; i < len; i++) {
																	const restriction = restrictionList[i];
																	if (groupingProperties.indexOf(restriction.field) < 0) {
																		newRestrictionList.push(restriction);
																	}
																}
																if (newRestrictionList.length > 0) {
																	additionalFilters[param] = Ext.util.JSON.encode(newRestrictionList);
																}
															}
														}
													}
												}
												return additionalFilters;
											});
										},

										getGroupingPropertyArray: function() {
											const groupingListString = TCG.getChildByName(this.getTopToolbar(), 'groupingFields').getValue();
											if (TCG.isBlank(groupingListString)) {
												return null;
											}
											return groupingListString.split('::');
										},

										getGroupingPropertyList: function() {
											const groupingList = this.getGroupingPropertyArray();

											if (TCG.isNull(groupingList)) {
												return null;
											}
											const groupingPropertyList = [];
											groupingList.forEach(group => {
												const groupItem = Clifton.order.allocation.OrderAllocationGroupingProperties.find(function(item) {
													if (item.name === group) {
														return true;
													}
													return false;
												});
												groupingPropertyList.push({
													class: 'com.clifton.core.dataaccess.search.grouping.GroupingProperty',
													label: groupItem.label,
													searchFieldName: groupItem.searchFieldName,
													beanPropertyName: groupItem.beanPropertyName,
													emptyValueLabel: groupItem.emptyValueLabel
												});
											});
											return groupingPropertyList;
										}
									}

								]
							},
							{
								region: 'center',
								title: 'Aggregate View',

								xtype: 'gridpanel',
								name: 'orderAllocationAggregateGrid',
								id: 'orderAllocationAggregateGrid',
								getLoadURL: function() {
									return 'orderAllocationGroupedEntityResultListFind.json';
								},

								instructions: 'Results aggregated based on security and side',
								//defaults: {anchor: '-35 -35'}, // leave room for error icon
								appendStandardColumns: false,
								readOnly: true,
								additionalPropertiesToRequest: 'id|totalAggregateValueMapJson|entity.security.id|entity.settlementCurrency.id',
								viewNames: ['Default View', 'Currency View'],
								columns: [
									{
										header: 'Security',
										width: 50,
										dataIndex: 'entity.security.ticker',
										groupingProperties: 'security.id|securityId|Security|None',
										allViews: true,
										viewNameHeaders: [
											{name: 'Currency View', label: 'Given CCY'},
											{name: 'Default View', label: 'Security'}
										]
									},
									{
										header: 'Settle CCY',
										width: 50,
										dataIndex: 'entity.settlementCurrency.ticker',
										groupingProperties: 'settlementCurrency.id|settlementCurrencyId|Settlement CCY|None',
										allViews: true
									},
									{
										header: 'Side',
										width: 25,
										dataIndex: 'entity.openCloseType.buy',
										groupingProperties: 'openCloseType.buy|buy|Side|None',
										allViews: true,
										renderer: function(v, metaData, r) {
											let buy = TCG.isTrue(v);
											if (TCG.isNull(r.json.entity.openCloseType)) {
												buy = r.json.totalAggregateValueMapJson['accountingNotionalSigned.SUM'] > 0;
											}
											metaData.css = buy ? 'buy-light' : 'sell-light';
											return buy ? 'BUY' : 'SELL';
										}
									},
									{header: 'Count', width: 25, dataIndex: 'totalCount', type: 'int'},
									{
										header: 'Notional',
										width: 25,
										dataIndex: 'totalAggregateValueMapJson.accountingNotional.SUM',
										type: 'currency',
										viewNames: ['Default View'],
										renderer: function(v, metaData, r) {
											return TCG.renderAmount(r.json.totalAggregateValueMapJson['accountingNotional.SUM'], false, '0,000.00');
										}
									},
									{
										header: 'Given Amount',
										width: 25,
										dataIndex: 'totalAggregateValueMapJson.accountingNotionalSigned.SUM',
										type: 'currency',
										viewNames: ['Currency View'],
										renderer: function(v, metaData, r) {
											return TCG.renderAmount(Math.abs(r.json.totalAggregateValueMapJson['accountingNotionalSigned.SUM']), false, '0,000.00');
										}
									},
									{
										header: 'Quantity', width: 25, dataIndex: 'totalAggregateValueMapJson.quantityIntended.SUM', type: 'int', viewNames: ['Default View'],
										renderer: function(v, metaData, r) {
											return TCG.renderAmount(r.json.totalAggregateValueMapJson['quantityIntended.SUM'], false, '0,000');
										}
									}
								],

								getRowSelectionModel: function() {
									if (typeof this.rowSelectionModel != 'object') {
										this.singleSelect = this.rowSelectionModel !== 'multiple';
										this.rowSelectionModel = new Ext.grid.RowSelectionModel({
											singleSelect: this.rowSelectionModel !== 'multiple',
											listeners: {
												'rowselect': function(model, rowIndex, record) {
													// update Detail grid filters
													const gridpanel = model.grid.ownerCt;
													gridpanel.applyGroupingFiltersToDetailGrid(rowIndex);
												},
												'rowdeselect': function(model, rowIndex, record) {
													// update the grand totals if applicable
													const gridpanel = model.grid.ownerCt;
													gridpanel.removeGroupingFiltersFromDetailGrid();
												}
											}
										});
									}
									return this.rowSelectionModel;
								},

								applyGroupingFiltersToDetailGrid: function(rowIndex) {
									this.setRemoveGroupingFiltersFromDetailGrid(rowIndex, false);
								},

								removeGroupingFiltersFromDetailGrid: function() {
									this.setRemoveGroupingFiltersFromDetailGrid(null, false);
								},

								detailAdditionalRestrictions: [],
								setRemoveGroupingFiltersFromDetailGrid: function(selectedRowIndex, suppressReload) {
									this.detailAdditionalRestrictions = [];
									const record = this.grid.getStore().getAt(selectedRowIndex);
									const detailGrid = TCG.getChildByName(TCG.getParentTabPanel(this), 'orderAllocationGroupDetailGrid');
									const groupingProperties = this.getGroupingPropertyList();
									groupingProperties.forEach(groupingProperty => {
										const gp = Clifton.order.allocation.OrderAllocationGroupingProperties.find(function(item) {
											if (item.name === groupingProperty.label) {
												return true;
											}
											return false;
										});
										const filters = detailGrid.grid.filters;
										if (TCG.isNotNull(filters.getFilter(gp.dataIndex))) {
											detailGrid.clearFilter(gp.dataIndex, true);
										}
										if (record) {
											if (TCG.isBlank(TCG.getValue('entity.' + gp.beanPropertyName, record.json))) {
												this.detailAdditionalRestrictions.push({
													field: gp.searchFieldName,
													data: {comparison: 'IS_NULL'}
												});
											}
											else {
												this.detailAdditionalRestrictions.push({
													field: gp.searchFieldName,
													data: {comparison: 'EQUALS', value: TCG.getValue('entity.' + gp.beanPropertyName, record.json)}
												});
											}
										}
									});
									if (TCG.isFalse(suppressReload)) {
										detailGrid.reload();
									}
								},


								getLoadParams: function(firstLoad) {
									const grid = this;
									this.removeGroupingFiltersFromDetailGrid();

									const gd = TCG.getChildByName(TCG.getParentTabPanel(this), 'orderAllocationGroupDetailGrid');
									const params = this.getDetailGridLoadParamsWithoutGroupedPropertyFilters(gd);
									return Promise.resolve(params).then(function(params) {
										params = Ext.apply(params, {
											requestedPropertiesRoot: 'data',
											requestedProperties: 'id|groupProperty.beanPropertyName|groupProperty.searchFieldName|groupProperty.label|groupProperty.emptyValueLabel|groupValue|groupLabel|leaf|children|totalAggregateValueMapJson',
											groupingPropertyList: Ext.util.JSON.encode(grid.getGroupingPropertyList()),
											groupingAggregatePropertyList: Ext.util.JSON.encode(grid.groupingAggregatePropertyList)
										});
										return params;
									});
								},

								getDetailGridLoadParamsWithoutGroupedPropertyFilters: function(gridPanel) {
									const groupingProperties = ['securityId', 'settlementCurrencyId', 'buy'];
									const options = {};
									const params = gridPanel.getExportGridParams(options);

									return Promise.resolve(params).then(function(params) {

										// options.params does not get defined if there are no filters and Ext.apply will do nothing if options.params is undefined
										if (!options.params) {
											options.params = {};
										}
										Ext.apply(options.params, params);
										gridPanel.grid.loadMask.onLoad();

										const additionalFilters = {};
										for (const param in options.params) {
											if (options.params.hasOwnProperty(param)) {
												if (!TCG.isEquals('restrictionList', param)) {
													additionalFilters[param] = options.params[param];
												}
												else {
													// Remove the filter for any field that we are grouping on
													const restrictionList = Ext.util.JSON.decode(options.params[param]);
													if (Ext.isArray(restrictionList)) {
														const newRestrictionList = [];
														for (let i = 0, len = restrictionList.length; i < len; i++) {
															const restriction = restrictionList[i];
															if (groupingProperties.indexOf(restriction.field) < 0) {
																newRestrictionList.push(restriction);
															}
														}
														if (newRestrictionList.length > 0) {
															additionalFilters[param] = Ext.util.JSON.encode(newRestrictionList);
														}
													}
												}
											}
										}

										return additionalFilters;
									});
								},

								groupingAggregatePropertyList: [],

								getGroupingPropertyList: function() {
									return this.groupingPropertyList;
								},
								groupingPropertyList: [],

								beforeSwitchToView: function(viewName) {
									this.groupingPropertyList = [];
									this.groupingPropertyList.push({
										class: 'com.clifton.core.dataaccess.search.grouping.GroupingProperty',
										beanPropertyName: 'security.id',
										searchFieldName: 'securityId',
										label: 'Security',
										emptyValueLabel: 'None'
									});
									this.groupingPropertyList.push({
										class: 'com.clifton.core.dataaccess.search.grouping.GroupingProperty',
										beanPropertyName: 'settlementCurrency.id',
										searchFieldName: 'settlementCurrency',
										label: 'Settlement Currency',
										emptyValueLabel: 'None'
									});
									if (TCG.isNotEquals('Currency View', viewName)) {
										this.groupingPropertyList.push({
											class: 'com.clifton.core.dataaccess.search.grouping.GroupingProperty',
											beanPropertyName: 'openCloseType.buy',
											searchFieldName: 'buy',
											label: 'Buy / Sell',
											emptyValueLabel: 'None'
										});
									}

									this.groupingAggregatePropertyList = [];
									if (TCG.isEquals('Currency View', viewName)) {
										this.groupingAggregatePropertyList.push({
											class: 'com.clifton.core.dataaccess.search.grouping.GroupingAggregateProperty',
											aggregateType: 'SUM',
											searchFormFieldName: 'accountingNotionalSigned',
											aggregateLabel: 'Notional'
										});
									}
									else {
										this.groupingAggregatePropertyList.push({
											class: 'com.clifton.core.dataaccess.search.grouping.GroupingAggregateProperty',
											aggregateType: 'SUM',
											beanPropertyName: 'accountingNotional',
											aggregateLabel: 'Notional'
										});
										this.groupingAggregatePropertyList.push({
											class: 'com.clifton.core.dataaccess.search.grouping.GroupingAggregateProperty',
											aggregateType: 'SUM',
											beanPropertyName: 'quantityIntended',
											aggregateLabel: 'Quantity'
										});
									}
								}
							}
						]
					},
					{
						region: 'center',
						layout: 'fit',
						title: 'Order Allocations',
						xtype: 'order-management-allocation-trading-blotter-grid',
						name: 'orderAllocationGroupDetailGrid',
						loadURL: 'orderAllocationListFind.json',

						isPagingEnabled: function() {
							return true;
						},

						gridConfig: {
							plugins: [{ptype: 'gridsummary'}]
						},

						onBeforeReturnFilterData: function(filters) {
							const groupedGrid = TCG.getChildByName(TCG.getParentTabPanel(this.gridPanel), 'orderAllocationGroupedHierarchicalResultListFind');

							if (groupedGrid.detailAdditionalRestrictions && groupedGrid.detailAdditionalRestrictions.length > 0) {
								groupedGrid.detailAdditionalRestrictions.forEach(ar => filters.push(ar));
							}

							const aggregateGrid = TCG.getChildByName(TCG.getParentTabPanel(this.gridPanel), 'orderAllocationAggregateGrid');
							if (aggregateGrid.detailAdditionalRestrictions && aggregateGrid.detailAdditionalRestrictions.length > 0) {
								aggregateGrid.detailAdditionalRestrictions.forEach(ar => filters.push(ar));
							}

						},

						addToolbarFilters: function(toolbar, gridPanel) {
							// do nothing added to top toolbar
						},

						// Return the top level tool bar to read the filters from
						getTopToolbarWithFilters: function() {
							return TCG.getChildByName(TCG.getParentTabPanel(this), 'order-allocation-grouped-panel').getTopToolbar();

						}

					}
				]


			},


			{
				title: 'Block Orders',
				items: [{
					xtype: 'order-block-list-grid',
					defaultDisplayFilter: 'MY_ACTIVE',
					rowSelectionModel: 'checkbox',

					addFirstToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Actions',
							tooltip: 'Assign Trader / Block Orders',
							iconCls: 'blotter',
							scope: this,
							menu: {
								items: [
									{
										text: 'Assign Trader for Selected to Me',
										iconCls: 'user',
										tooltip: 'Assign Trader for selected Block Order and corresponding Order Allocations to Current User.  Warning: This should only be done if the original trader is not available to complete trading.',
										handler: function() {
											gridPanel.handleActivity('Assign Trader Current User (Overwrite)');
										}
									},
									'-',
									{
										text: 'Place and Send',
										iconCls: 'arrow-right-green',
										menu: new Ext.menu.Menu({
											layout: 'fit',
											style: {overflow: 'visible'},
											width: 200,
											items: [
												{
													xtype: 'combo', name: 'orderDestinationId', url: 'orderDestinationListFind.json?executionVenue=true',
													getListParent: function() {
														return this.el.up('.x-menu');
													},
													listeners: {
														'select': function(combo) {
															const orderDestinationId = combo.getValue();
															combo.reset();
															gridPanel.handleActivity('Place Send', orderDestinationId);
														}
													}
												}
											]
										})
									},
									'-',
									{
										text: 'Unblock Block Order (Unblock Order Allocations)',
										iconCls: 'undo',
										tooltip: 'Cancel the block order, remove all order allocations from this block order and put the Order Allocations back into Ready for Trading.',
										handler: function() {
											gridPanel.handleActivity('Unblock');
										}
									},
									{
										text: 'Unblock Block Order (Return to PM Order Allocations)',
										iconCls: 'cancel',
										tooltip: 'Cancel the block order, remove all order allocations from this block order and put the Order Allocations into Pending state.',
										handler: function() {
											gridPanel.handleActivity('Unblock Return');
										}
									},
									{
										text: 'Unblock Block Order (Reject Order Allocations)',
										iconCls: 'cancel',
										tooltip: 'Cancel the block order, remove all order allocations from this block order and put the Order Allocations into Rejected state.',
										handler: function() {
											gridPanel.handleActivity('Unblock Reject');
										}
									}

								]
							}
						});
						toolBar.add('-');
					},


					handleActivity: function(activityName, orderDestinationId, destinationConfigurationId) {
						const gridPanel = this;
						const grid = gridPanel.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select at least one block order.', 'No Row(s) Selected');
						}
						else {
							const rows = sm.getSelections();
							const recordIds = [];
							for (let i = 0; i < rows.length; i++) {
								recordIds.push(rows[i].json.id);
							}
							if (activityName === 'Assign Trader Current User (Overwrite)') {
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Assigning...',
									params: {orderBlockIds: recordIds, assignTrader: true, 'assignTraderUserId': TCG.getCurrentUser().id, reassignTraderIfPopulated: true},
									onLoad: function(record, conf) {
										gridPanel.reload();
									}
								});
								loader.load('orderManagementActivityForOrderBlocksProcess.json?enableValidatingBinding=true');
							}
							else if (activityName === 'Unblock' || activityName === 'Unblock Reject' || activityName === 'Unblock Return') {
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Unblocking...',
									params: {
										orderBlockIds: recordIds,
										unblockOrderAllocations: true,
										rejectUnblockedOrderAllocations: (activityName === 'Unblock Reject'),
										returnUnblockedOrderAllocations: (activityName === 'Unblock Return')
									},
									onLoad: function(record, conf) {
										gridPanel.reload();
									}
								});
								loader.load('orderManagementActivityForOrderBlocksProcess.json?enableValidatingBinding=true');
							}
							else if (activityName === 'Place Send') {
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Processing...',
									params: {
										orderBlockIds: recordIds,
										createPlacements: true,
										sendPlacements: true,
										orderDestinationId: orderDestinationId,
										destinationConfigurationId: destinationConfigurationId
									},
									onLoad: function(record, conf) {
										gridPanel.reload();
									}
								});
								loader.load('orderManagementActivityForOrderBlocksProcess.json?enableValidatingBinding=true');
							}
						}
					}
				}]
			},


			{
				title: 'Order Placements',
				items: [{
					xtype: 'order-placement-list-grid',
					instructions: 'The following placements orders are actively being worked and have either not been fully filled or have not been fully allocated.',
					rowSelectionModel: 'checkbox',
					defaultDisplayFilter: 'MY_ACTIVE',
					enableOrderPlacementExecutionUploadDragAndDrop: true,
					additionalPropertiesToRequest: 'orderDestination.id|orderDestination.destinationType.destinationCommunicationType|orderDestination.mainOrderDestination.name|orderDestination.mainOrderDestination.id|orderDestination.backupDestination|executionStatus.canceled|executionStatus.fillComplete|labelShort|executingBrokerCompany.id|executingBrokerCompany.label|orderBlock.security.currency|orderBlock.security.id|orderDestinationConfiguration.id',

					getGridRowContextMenuItems: async function(grid, rowIndex, record) {
						const gridPanel = grid.ownerGridPanel;

						const sm = grid.getSelectionModel();
						// Make sure current row is selected
						sm.selectRow(rowIndex, true);

						const ut = sm.getSelections();
						let allItems;

						for (let i = 0; i < ut.length; i++) {
							// add multiple details from the same journal only once
							const thisRecord = ut[i].json;
							const items = await this.getGridRowContextMenuItemsForRecord(gridPanel, thisRecord);
							if (TCG.isNull(allItems)) {
								allItems = items;
							}
							else {
								allItems = allItems.filter(item => {
									for (let j = 0; j < items.length; j++) {
										const thisItem = items[j];
										if (TCG.isEquals(item.name, thisItem.name)) {
											return true;
										}
									}
									return false;
								});
							}
						}
						return allItems;
					},

					getGridRowContextMenuItemsForRecord: async function(gridPanel, record) {
						const rowItems = [];
						const manualDestination = TCG.isEquals('Manual', record.orderDestination.name);
						if (TCG.isEquals('Draft', record.executionStatus.name)) {
							if (TCG.isFalse(manualDestination)) {
								rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Send', 'Send', 'run', 'Send placements to destination', 'SEND'));
							}
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Cancel', 'Cancel', 'cancel', 'Cancel Placement. Will open the block order up for cancellation or new placements', 'CANCEL'));
						}
						else if (TCG.isEquals('Rejected', record.executionStatus.name)) {
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Send', 'Send', 'run', 'Send placements to destination', 'SEND'));
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Send Cancel Request', 'Send Cancel Request', 'cancel', 'Send cancellation request', 'CANCEL_REQUEST'));
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Confirm Rejection', 'Confirm Rejection', 'reject', 'Confirm Rejection of the placement and cancel.  Will open the block order up for cancellation or new placements ', 'REJECT_CANCEL'));

							const destinationConfigurations = await TCG.data.getDataPromiseUsingCaching('orderDestinationConfigurationListForDestination.json?orderDestinationId=' + record.orderDestination.id + '&activeOnly=true', gridPanel, 'DESTINATION_CONFIG_' + record.orderDestination.id);
							if (destinationConfigurations && destinationConfigurations.length > 1) {
								destinationConfigurations.forEach(destinationConfig => {
									if (TCG.isNotEquals(record.orderDestinationConfiguration.id, destinationConfig.id)) {
										rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Confirm Rejection / Re-Send - ' + destinationConfig.name, 'Confirm Rejection / Re-send to ' + destinationConfig.name, 'run', 'Confirm Rejection of the placement and cancel.  Auto create a new placement to the same destination for the selected destination configuration.', 'REJECT_CANCEL_SWITCH_DESTINATION_CONFIGURATION', {switchToDestinationConfigurationId: destinationConfig.id}));
									}
								});
							}
						}
						else if (TCG.isEquals('New Order', record.executionStatus.name)) {
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Send Cancel Request', 'Send Cancel Request', 'cancel', 'Send cancellation request', 'CANCEL_REQUEST'));
						}
						else if (TCG.isEquals('Ready for Batching', record.executionStatus.name)) {
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Process Batching for ' + record.orderDestination.name, 'Batch ' + record.orderDestination.id, 'run', 'Process Batching of ALL Ready for Batching placements for ' + record.orderDestination.name + ', but do not immediately send.', 'BATCH'));
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Process Batching & Send for ' + record.orderDestination.name, 'Batch and Send ' + record.orderDestination.id, 'email', 'Process Batching of ALL Ready for Batching placements for ' + record.orderDestination.name + ', and send all batches.', 'BATCH_SEND'));
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Cancel', 'Cancel', 'cancel', 'Cancel Placement. Will open the block order up for cancellation or new placements', 'CANCEL'));
						}
						else if (TCG.isEquals('Batched', record.executionStatus.name)) {
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Cancel', 'Cancel', 'cancel', 'Cancel Placement. Will open the block order up for cancellation or new placements.  Will remove the placement from the unsent batch.', 'CANCEL'));
						}
						else if (TCG.isEquals('Sent', record.executionStatus.name)) {
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Cancel', 'Cancel', 'cancel', 'Cancel Placement. Will open the block order up for cancellation or new placements', 'CANCEL'));
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Re-Send', 'Re-send', 'run', 'Re-Send Placement. Should only be used if the initial message did not make it to external party', 'FORCE_RESEND'));
						}
						else if (TCG.isEquals('Error', record.executionStatus.name)) {
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Cancel', 'Cancel', 'cancel', 'Cancel Placement. Will open the block order up for cancellation or new placements', 'CANCEL'));
						}
						// DRAFT, SENT, READY_FOR_BATCHING, BATCHED, ERROR
						const switchDestinationStatuses = ['Draft', 'Sent', 'Ready for Batching', 'Batched', 'Error'];
						if (switchDestinationStatuses.indexOf(record.executionStatus.name) >= 0 && TCG.isFalse(manualDestination)) {
							rowItems.push('-');
							rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Switch to Manual', 'Switch To Manual', 'doctor', 'Switch Placement to manual to manually enter execution details', 'SWITCH_TO_MANUAL'));
							await TCG.data.getDataPromise('orderDestinationBackupListForDestination.json?orderDestinationId=' + record.orderDestination.id)
								.then(function(backupList) {
										if (TCG.isNotNull(backupList) && backupList.length > 0) {
											backupList.forEach(backup => rowItems.push(gridPanel.getActionMenuItem(gridPanel, 'Switch to Back Up: ' + backup.name, 'Switch To Back Up ' + backup.id, 'doctor', 'Switch Placement to Use the Back up Destination of ' + backup.name, 'SWITCH_DESTINATION', {'switchToDestinationId': backup.id})));
										}
									}
								);
						}

						// Add manual fill option if not fully filled and not a FIX Order
						if (TCG.isFalse(record.executionStatus.fillComplete) && TCG.isNotEquals('FIX', record.orderDestination.destinationType.destinationCommunicationType)) {
							rowItems.push('-');
							rowItems.push({
								text: 'Manual Fill',
								name: 'Manual Fill ' + record.id, // Unique per row, if multiple selected won't display
								tooltip: 'Manually enter fill information',
								iconCls: 'calculator',
								handler: async () => {
									const defaultData = record;
									// Default to Fully Filled
									defaultData.quantityFilled = defaultData.quantityIntended;
									defaultData.quantityUnfilled = 0;
									const className = 'Clifton.order.execution.OrderPlacementExecutionWindow';
									const cmpId = TCG.getComponentId(className, defaultData.id);
									TCG.createComponent(className, {
										id: cmpId,
										defaultData: defaultData,
										openerCt: gridPanel
									});
								}
							});
						}
						return rowItems;
					},

					getActionMenuItem(gridPanel, text, name, iconClass, tooltip, actionName, additionalParams) {
						return {
							text: text,
							name: name,
							tooltip: tooltip,
							iconCls: iconClass,
							handler: async () => gridPanel.handleActivity(actionName, additionalParams)
						};
					},


					handleActivity: function(activityName, additionalParams) {
						const gridPanel = this;
						const grid = gridPanel.grid;
						const sm = grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select at least one placement.', 'No Row(s) Selected');
						}
						else {
							const rows = sm.getSelections();
							const actionCount = 0;
							gridPanel.handleActivityForRow(rows, actionCount, activityName, additionalParams);
						}
					},
					handleActivityForRow: function(rows, actionCount, activityName, additionalParams) {
						const gridPanel = this;
						const rowId = rows[actionCount].json.id;
						const params = Ext.apply({}, additionalParams);
						// Bulk Action based on Destination
						if (activityName.startsWith('BATCH')) {
							params.orderDestinationId = rows[actionCount].json.orderDestination.id;
							params.send = activityName.endsWith('SEND');
							const loader = new TCG.data.JsonLoader({
								waitMsg: 'Processing',
								waitTarget: gridPanel,
								params: params,
								onLoad: function(record, conf) {
									// All based on destination, so just reload
									gridPanel.reload();
								}
							});
							const url = 'orderBatchingForDestinationProcess.json';
							loader.load(url);
							return;
						}

						let url = 'orderManagementExecutionActionProcess.json';
						params.orderPlacementId = rowId;
						params.orderExecutionAction = activityName;

						if (TCG.contains(activityName, 'SWITCH')) {
							url = 'orderManagementExecutionActionForCommandProcess.json';
						}

						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Processing',
							waitTarget: gridPanel,
							params: params,
							onLoad: function(record, conf) {
								actionCount++;
								if (actionCount === rows.length) { // refresh after all rows were processed
									gridPanel.reload();
								}
								else {
									gridPanel.handleActivityForRow(rows, actionCount, activityName, additionalParams);
								}
							}
						});

						loader.load(url);
					},

					// Custom Export functionality that does look ups for each row.  Allowed only on the active placements tabs to prevent pulling too much data
					isSkipExport: function(column) {
						return (TCG.isEquals('Account(s)', column.header) && TCG.isNotEquals('Currency View', this.currentViewName));
					}
				}]
			},


			{
				title: 'Placement Allocations',
				items: [{xtype: 'order-placement-allocation-list-grid'}]
			},


			{
				title: 'Trades',
				items: [{xtype: 'order-trade-list-grid'}]
			},


			{
				title: 'Order Batches',
				items: [{xtype: 'order-batch-list-grid'}]
			}
		]
	}]
});

