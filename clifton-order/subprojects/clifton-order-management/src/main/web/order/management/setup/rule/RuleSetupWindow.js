Clifton.order.management.setup.rule.RuleSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'orderManagementRuleSetupWindow',
	title: 'Order Rule Setup',
	iconCls: 'rule',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Rule Assignments',
				items: [{
					xtype: 'order-management-rule-assignment-list-grid'
				}]
			},


			{
				title: 'Rule Library',
				items: [{
					name: 'orderManagementRuleDefinitionListFind',
					xtype: 'gridpanel-custom-json-fields',
					tableName: 'OrderManagementRuleDefinition',
					instructions: 'The following are all of the rules available in the system. Rules can be used (based on rule types) for various purposes such as limited what securities can be traded for an account, what executing brokers are allowed, etc.  Rules can be grouped under a rollup rule for easier assignment to accounts that share similar characteristics.',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'ruleType.name',
					defaultHidden: false,

					nonCustomColumns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Rule Category', width: 40, dataIndex: 'ruleType.ruleCategory.name', hidden: true, filter: {searchFieldName: 'ruleCategoryId', type: 'combo', url: 'orderManagementRuleCategoryListFind.json'}},
						{header: 'Rule Type', width: 40, dataIndex: 'ruleType.name', hidden: true, filter: {searchFieldName: 'ruleTypeId', type: 'combo', url: 'orderManagementRuleTypeListFind.json'}, defaultSortColumn: true},
						{header: 'Rule Definition', width: 75, dataIndex: 'name'},
						{header: 'Rule Definition Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Violation Note Template', width: 100, dataIndex: 'violationNoteTemplateOverride', hidden: true},
						{header: 'Violation Code Template', width: 100, dataIndex: 'violationCodeTemplate', hidden: true},
						{header: 'Rollup', width: 25, hidden: true, dataIndex: 'rollup', tooltip: 'Rollup rules are a grouping of rule definitions.', type: 'boolean', filter: {searchFieldName: 'rollup'}},
						{header: 'Definition Modify Condition', width: 75, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'coalesceOverrideRuleDefinitionModifyConditionId', type: 'combo', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagmentRuleDefinition'}, tooltip: 'This Field represents either the overridden definition modify condition or the inherited definition modify condition from the rule type. The overridden takes precedent. In other words: this is the applicable condition', hidden: true},
						{header: 'Assignment Modify Condition', width: 75, dataIndex: 'coalesceAssignmentEntityModifyCondition.name', filter: {searchFieldName: 'coalesceOverrideRuleAssignmentModifyConditionId', type: 'combo', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagementRuleAssignment'}, tooltip: 'This Field represents either the overridden assignment modify condition or the inherited assignment modify condition from the rule type. The overridden takes precedent. In other words: this is the applicable condition', hidden: true}

					],

					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Rule Category', width: 180, xtype: 'toolbar-combo', url: 'orderManagementRuleCategoryListFind.json', linkedFilter: 'ruleType.ruleCategory.name'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=OMS Rule Tags'},
							{
								fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 150, minListWidth: 150, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value', value: 'ALL',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name', 'description'],
									data: [
										['ALL', 'All', 'Display all Rule Definitions'],
										['ROLLUP', 'Rollup Only', 'Display only rollup rules'],
										['STANDARD', 'Standard Only (Non-Rollup)', 'Display standard (a.k.a non rollup rules only)']
									]
								})
							},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						const displayTypeField = TCG.getChildByName(t, 'displayType');
						if (displayTypeField) {
							const displayType = displayTypeField.getValue();
							if (displayType === 'ROLLUP') {
								this.setFilterValue('rollup', true);
							}
							else if (displayType === 'STANDARD') {
								this.setFilterValue('rollup', false);
							}
							else {
								this.clearFilter('rollup', true);
							}
						}

						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						const params = {};
						if (TCG.isNotBlank(tag.getValue())) {

							params.categoryName = 'OMS Rule Tags';
							params.categoryTableName = 'OrderManagementRuleDefinition';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					},
					editor: {
						copyURL: 'orderManagementRuleDefinitionCopy.json',
						copyHandler: function(gridPanel) {
							const editor = this;

							const grid = this.grid;
							const sm = grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select an existing rule to copy.', 'No Row(s) Selected');
								return;
							}
							else if (sm.getCount() !== 1) {
								TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
								return;
							}
							const id = sm.getSelected().id;
							editor.openDetailPage('Clifton.order.management.setup.rule.RuleDefinitionCopyWindow', editor.getGridPanel(), id);
						},
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('rollup');
								return (t) ? 'Rollup Rule' : 'Standard Rule';
							},
							items: [
								{text: 'Standard Rule', iconCls: 'verify', className: 'Clifton.order.management.setup.rule.StandardRuleDefinitionWindow'},
								{text: 'Rollup Rule', iconCls: 'hierarchy', className: 'Clifton.order.management.setup.rule.RollupRuleDefinitionWindow'}
							]
						}
					}
				}]
			},


			{
				title: 'Rule Types',
				items: [{
					name: 'orderManagementRuleTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Rule Types define a specific rule executor (bean) for a category (except for rollups which are just containers of child rules).  Each rule type can support custom fields for that rule type that the executor can use during processing. For example, Allow Trading for a Specific Security Type, where Security Type is a custom field selection, however an Executing Broker rule would allow selecting a specific Executing Broker.',
					topToolbarSearchParameter: 'searchPattern',
					groupField: 'ruleCategory.name',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Rule Category', width: 40, dataIndex: 'ruleCategory.name', hidden: true, filter: {searchFieldName: 'ruleCategoryId', type: 'combo', url: 'orderManagementRuleCategoryListFind.json'}},
						{header: 'Rule Type Name', width: 40, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Rule Type Description', width: 80, dataIndex: 'description'},
						{
							header: 'Rule Name Template', width: 150, dataIndex: 'ruleDefinitionNameTemplate', hidden: true,
							tooltip: 'Freemarker template used to generate rule definition names under this rule type.  Helps to build the names dynamically and provide consistency for large rule sets.  Rule Definition names are read only when this is set and cannot be changed manually.'
						},
						{header: 'Assignment Prohibited', width: 30, dataIndex: 'assignmentProhibited', type: 'boolean'},
						{header: 'Rollup Rule Type', width: 40, dataIndex: 'rollupRuleType.name', filter: {searchFieldName: 'rollupRuleTypeId', type: 'combo', url: 'orderManagementRuleTypeListFind.json?rollup=true'}},
						{header: 'Executor Bean', width: 40, dataIndex: 'ruleExecutorBean.name', filter: {searchFieldName: 'ruleExecutorBeanName'}},
						{header: 'Specificity Filter Bean', width: 40, dataIndex: 'ruleSpecificityFilterBean.name', filter: {searchFieldName: 'ruleSpecificityFilterBeanName'}, tooltip: 'This bean is used to take all pre-filtered rules the use the same filter bean and apply specificity filtering within that list to find the most specific if more than one apply. For example: Rule that prohibits CCY trading, and a second rule that allows AUD.  They both apply, but the AUD rule would win when validating against an AUD order allocation.'},
						{
							header: 'Definition Modify Condition', width: 50, dataIndex: 'ruleDefinitionModifyCondition.name', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagmentRuleDefinition',
							tooltip: 'When set, rule definitions of this type must pass the selected modify condition in order to be modified.  The modify condition can be overridden for a specific rule definition.  If rule definition is a rollup, it is also used to restrict adding children to the rollup rule definition.'
						},
						{
							header: 'Assignment Modify Condition', width: 50, dataIndex: 'ruleAssignmentModifyCondition.name', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagementRuleAssignment',
							tooltip: 'When set, rule assignments for rule definitions under this type must pass the selected modify condition in order to be modified. The assignment modify condition can be overridden for a specific rule definition. Does not apply if assignments are not allowed.'
						}
					],

					editor: {
						detailPageClass: 'Clifton.order.management.setup.rule.RuleTypeWindow'
					}

				}]
			},


			{
				title: 'Rule Categories',
				items: [
					{
						name: 'orderManagementRuleCategoryListFind',
						xtype: 'gridpanel',
						instructions: 'Rule Categories are used to categorize rules that can perform a specific purpose and must implement a specific interface (Executor Bean Group).  For example, Security Rules are used to not only validate a security is allowed to trade for an account, but can also be used to limit combo box selections.  Similarly, Executing Broker rules will limit the selections - in cases where there is only one allowed it can be pre-selected for you.',
						topToolbarSearchParameter: 'searchPattern',
						columns: [
							{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
							{header: 'Rule Category Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
							{header: 'Rule Category Description', width: 150, dataIndex: 'description'},
							{header: 'Executor Bean Group', width: 50, dataIndex: 'ruleExecutorBeanGroup.name', filter: {searchFieldName: 'ruleExecutorBeanGroupName'}, tooltip: 'The System Bean Group identifies the interface the rules under this category must implement'},
							{header: 'Preview Window Class', width: 50, dataIndex: 'previewRuleTypeWindowClass', hidden: true, tooltip: 'If supplied a preview feature is supported to test the results of rule execution of the category for a specific account across all rules under the category.'},
							{header: 'Rollup', width: 20, dataIndex: 'rollup', type: 'boolean', tooltip: 'Rollup category is a generic rule category for rollup rules and do not define an interface (executor bean group).  Rollups can contain children across other rule categories and are just a container to ease management of rule assignments.'},
							{
								header: 'Preview', width: 12, dataIndex: 'previewRuleTypeWindowClass', align: 'center', tooltip: 'Available if preview feature is supported to test the results of rule execution of the category for a specific account across all rules under the category.',
								renderer: function(clz, args, r) {
									if (TCG.isNotBlank(clz)) {
										return TCG.renderActionColumn('preview', '', 'Preview results for this category for a specific account', 'Preview');
									}
									return '';
								},
								eventListeners: {
									'click': function(column, grid, rowIndex, event) {
										const clz = TCG.getValue('previewRuleTypeWindowClass', grid.getStore().getAt(rowIndex).json);
										if (TCG.isNotBlank(clz)) {
											grid.ownerGridPanel.editor.openDetailPage(clz, grid.ownerGridPanel);
										}
									}
								}
							}
						],

						editor: {
							detailPageClass: 'Clifton.order.management.setup.rule.RuleCategoryWindow'
						}
					}
				]
			}

		]
	}]
});

