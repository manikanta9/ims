Clifton.order.management.setup.rule.RuleAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Rule Assignment',
	iconCls: 'rule',
	width: 800,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel-custom-json-fields',
					dynamicFieldFormFragment: 'ruleAssignmentCustomFields',
					columnGroupName: 'Rule Assignment Custom Fields',
					instructions: 'Rule Assignments allow assigning rules to accounts or globally, optionally for a specified period of time.  Rules can also be explicitly excluded in cases where necessary.  Rule assignments of rollup rules is the equivalent of assigning all of the child rules.',
					url: 'orderManagementRuleAssignment.json',
					labelWidth: 130,

					listeners: {
						afterload: function(panel) {
							const ruleScope = TCG.getValue('ruleScope', panel.getForm().formValues);
							panel.resetRuleScopeView(ruleScope);
						}
					},

					items: [
						{fieldLabel: 'Rule Definition', name: 'ruleDefinition.name', hiddenName: 'ruleDefinition.id', xtype: 'combo', url: 'orderManagementRuleDefinitionListFind.json?assignmentProhibited=false', detailPageClass: 'Clifton.order.management.setup.rule.RuleDefinitionWindow'},
						{
							fieldLabel: 'Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
							qtip: 'This Field represents either the overridden assignment modify condition from the rule definition or the inherited assignment modify condition from the rule type. The overridden takes precedent. In other words: this is the applicable condition to modify this assignment.'
						},
						{fieldLabel: '', boxLabel: 'Exclude Rule for this assignment', name: 'excluded', xtype: 'checkbox'},
						{xtype: 'sectionheaderfield', header: 'Rule Scope'},
						{
							xtype: 'radiogroup', columns: 3, fieldLabel: '', name: 'ruleScope', allowBlank: false, submitValue: false,
							items: [
								{boxLabel: 'Global Rule Assignment', xtype: 'radio', name: 'ruleScope', inputValue: 'GLOBAL', qtip: 'Global Rules applies to ALL accounts'},
								{boxLabel: 'Holding Account Issuer', xtype: 'radio', name: 'ruleScope', inputValue: 'HOLDING_ACCOUNT_ISSUER', qtip: 'Applies to all holding accounts with the selected issuer (a.k.a. Custodian)'},
								{boxLabel: 'Account', xtype: 'radio', name: 'ruleScope', inputValue: 'ACCOUNT', qtip: 'Rule applies to the client / holding account combination'}
							],
							listeners: {
								change: (radioGroup, checkedItem) => {
									const formPanel = TCG.getParentFormPanel(radioGroup);
									if (checkedItem && checkedItem.inputValue) {
										formPanel.resetRuleScopeView(checkedItem.inputValue);

									}
								}
							}
						},
						{
							xtype: 'formfragment',
							frame: false,
							name: 'account',
							hidden: true,
							labelWidth: 130,
							items: [
								{xtype: 'hidden', name: 'clientAccount.clientCompany.id', submitValue: false},
								{
									fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', url: 'orderAccountListFind.json?clientAccount=true', displayField: 'label', allowBlank: false, requestedProps: 'holdingAccount|clientCompany.id', detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									listeners: {
										select: function(combo, record, index) {
											const clientCompanyId = record.json.clientCompany.id;
											const fp = TCG.getParentFormPanel(combo);
											fp.setFormValue('clientAccount.clientCompany.id', clientCompanyId);
											const holdingAccount = record.json.holdingAccount;
											if (TCG.isTrue(holdingAccount)) {
												fp.setFormValue('holdingAccount.id', {value: record.json.id, text: record.json.label});
											}
										}
									}
								},
								{
									fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', xtype: 'combo', url: 'orderAccountListFind.json?holdingAccount=true', displayField: 'label', requiredFields: ['clientAccount.label'], allowBlank: false, detailPageClass: 'Clifton.order.shared.account.AccountWindow',
									listeners: {
										beforequery: function(queryEvent) {
											const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
											const clientCompanyId = form.findField('clientAccount.clientCompany.id').value;
											queryEvent.combo.store.setBaseParam('clientCompanyId', clientCompanyId);
										}
									}

								}
							]
						},
						{
							xtype: 'formfragment',
							frame: false,
							name: 'holdingAccountIssuer',
							hidden: true,
							labelWidth: 130,
							items: [
								{fieldLabel: 'Holding Account Issuer', name: 'holdingAccountIssuingCompany.name', hiddenName: 'holdingAccountIssuingCompany.id', xtype: 'combo', url: 'orderCompanyListFind.json?holdingAccountIssuer=true', allowBlank: false, detailPageClass: 'Clifton.order.shared.company.CompanyWindow'}
							]
						},
						{
							xtype: 'formfragment',
							frame: false,
							labelWidth: 135,
							name: 'ruleAssignmentCustomFields',
							items: []
						},
						{xtype: 'sectionheaderfield', header: 'Active Date Range'},
						{fieldLabel: 'Start Date', xtype: 'datefield', name: 'startDate'},
						{fieldLabel: 'End Date', xtype: 'datefield', name: 'endDate'},
						{xtype: 'sectionheaderfield', header: 'Note'},
						{name: 'note', xtype: 'textarea'}
					],

					resetRuleScopeView: function(ruleScope) {
						const formPanel = this;
						// Show correct fields
						switch (ruleScope) {
							case 'GLOBAL':
								formPanel.showHideFormFragment('holdingAccountIssuer', false);
								formPanel.showHideFormFragment('account', false);
								// Must clear existing selection
								formPanel.getForm().findField('clientAccount.label').clearAndReset();
								formPanel.getForm().findField('holdingAccount.label').clearAndReset();
								formPanel.getForm().findField('holdingAccountIssuingCompany.name').clearAndReset();
								break;
							case 'HOLDING_ACCOUNT_ISSUER':
								formPanel.showHideFormFragment('holdingAccountIssuer', true);
								formPanel.showHideFormFragment('account', false);
								// Must clear existing selection
								formPanel.getForm().findField('clientAccount.label').clearAndReset();
								formPanel.getForm().findField('holdingAccount.label').clearAndReset();
								break;
							case 'ACCOUNT':
								formPanel.showHideFormFragment('holdingAccountIssuer', false);
								formPanel.showHideFormFragment('account', true);
								// Must clear existing selection
								formPanel.getForm().findField('holdingAccountIssuingCompany.name').clearAndReset();
								break;
							default:
								// Do nothing
						}
					},

					showHideFormFragment: function(formFragmentName, show) {
						const formPanel = this;
						const formFragment = TCG.getChildByName(formPanel, formFragmentName);
						if (!formFragment) {
							TCG.showError('Cannot find Form Fragment with name: ' + formFragmentName);
						}
						formFragment.setVisible(show);
						const items = formFragment.items;
						if (items) {
							formPanel.enableDisableRequiredFields(formPanel, show, items);
						}
					},
					enableDisableRequiredFields: function(fp, show, items) {
						if (!items) {
							return;
						}
						items.each(function(fld) {
							// Don't auto enable if it uses required fields
							if (!show || TCG.isBlank(fld.requiredFields)) {
								fld.setDisabled(!show);
							}
							if (fld.items) {
								fp.enableDisableRequiredFields(fp, show, fld.items);
							}
						});
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'system-note-grid',
					tableName: 'OrderManagementRuleAssignment',
					showInternalInfo: false,
					showPrivateInfo: false,
					defaultActiveFilter: false,
					showDisplayFilter: false
				}]
			}
		]
	}]
});
