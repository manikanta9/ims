Clifton.order.management.setup.rule.RuleCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Rule Category',
	iconCls: 'rule',
	width: 800,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Rule Categories are used to categorize rules that can perform a specific purpose and must implement a specific interface (Executor Bean Group).  For example, Security Rules are used to not only validate a security is allowed to trade for an account, but can also be used to limit combo box selections.  Similarly, Executing Broker rules will limit the selections - in cases where there is only one allowed it can be pre-selected for you.',
					url: 'orderManagementRuleCategory.json',
					readOnly: true,
					labelWidth: 160,
					items: [
						{fieldLabel: 'Rule Category Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Executor Bean Group', name: 'ruleExecutorBeanGroup.name', hiddenName: 'ruleExecutorBeanGroup.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.system.bean.GroupWindow', url: 'systemBeanGroupListFind.json', mutuallyExclusiveFields: ['rollup'],
							qtip: 'Rollup category is a generic rule category for rollup rules and do not define an interface (executor bean group).  Rollups can contain children across other rule categories and are just a container to ease management of rule assignments.'
						},
						{fieldLabel: 'Preview Window Class', name: 'previewRuleTypeWindowClass', requiredFields: ['ruleExecutorBeanGroup.name'], qtip: 'If supplied a preview feature is supported to test the results of rule execution of the category for a specific account across all rules under the category.'},
						{boxLabel: 'Rollup Rule (used to simplify assignments of multiple rules)', xtype: 'checkbox', name: 'rollup', mutuallyExclusiveFields: ['ruleExecutorBeanGroup.name'], qtip: 'Rollup category is a generic rule category for rollup rules and do not define an interface (executor bean group).  Rollups can contain children across other rule categories and are just a container to ease management of rule assignments.'}
					]
				}]
			},


			{
				title: 'Rule Types',
				items: [{
					name: 'orderManagementRuleTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The following rule types exist for selected category.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Rule Type Name', width: 40, dataIndex: 'name'},
						{header: 'Rule Type Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Executor Bean', width: 40, dataIndex: 'ruleExecutorBeanGroup.name', filter: {searchFieldName: 'ruleExecutorBeanName'}},
						{header: 'Specificity Filter Bean', width: 40, dataIndex: 'ruleSpecificityFilterBean.name', filter: {searchFieldName: 'ruleSpecificityFilterBeanName'}}

					],

					getLoadParams: function(firstLoad) {
						return {ruleCategoryId: this.getWindow().getMainFormId()};
					},

					editor: {
						detailPageClass: 'Clifton.order.management.setup.rule.RuleTypeWindow'
					}

				}]
			}


		]
	}]
});
