Clifton.order.management.setup.rule.RuleTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Rule Type',
	iconCls: 'rule',
	width: 900,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Rule Types define a specific rule executor (bean) for a category (except for rollups which are just containers of child rules).  Each rule type can support custom fields for that rule type that the executor can use during processing. For example, Allow Trading for a Specific Security Type, where Security Type is a custom field selection, however an Executing Broker rule would allow selecting a specific Executing Broker.',
					url: 'orderManagementRuleType.json',
					labelWidth: 170,
					listeners: {
						afterload: function(fp) {
							fp.resetBeanCombo(fp);
						}
					},

					resetBeanCombo: function(fp, beanGroupName, rollup) {
						const combo = fp.getForm().findField('ruleExecutorBean.name');
						if (TCG.isBlank(beanGroupName)) {
							beanGroupName = this.getFormValue('ruleCategory.ruleExecutorBeanGroup.name');
						}
						combo.url = 'systemBeanListFind.json?groupName=' + beanGroupName;
						combo.store.proxy.setUrl(combo.url, true);

						if (TCG.isBlank(rollup)) {
							rollup = this.getFormValue('ruleCategory.rollup');
						}
						if (TCG.isTrue(rollup)) {
							fp.setFormValue('assignmentProhibited', false);
							fp.disableField('assignmentProhibited');
							fp.getForm().findField('rollupRuleType.name').clearAndReset();
							fp.disableField('rollupRuleType.name');
						}
					},

					items: [
						{xtype: 'hidden', name: 'ruleCategory.ruleExecutorBeanGroup.name', submitValue: false},
						{
							fieldLabel: 'Rule Category', name: 'ruleCategory.name', hiddenName: 'ruleCategory.id', xtype: 'combo', url: 'orderManagementRuleCategoryListFind.json', requestedProps: 'ruleExecutorBeanGroup.id|ruleExecutorBeanGroup.name|rollup',
							listeners: {
								select: function(combo, record, index) {
									const groupName = record.json.ruleCategory.ruleExecutorBeanGroup.name;
									const fp = TCG.getParentFormPanel(combo);
									fp.setFormValue('ruleCategory.ruleExecutorBeanGroup.name', groupName);
									fp.resetBeanCombo(fp, groupName, record.json.ruleCategory.rollup);
								}
							}
						},
						{fieldLabel: 'Rule Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: '', boxLabel: 'Prohibit Rule Assignments (Rules are assigned through specified rollup rule type)', xtype: 'checkbox', name: 'assignmentProhibited', mutuallyExclusiveFields: ['ruleAssignmentModifyCondition.name']},
						{
							fieldLabel: 'Rule Name Template', name: 'ruleDefinitionNameTemplate', xtype: 'textarea', grow: true, height: 35,
							qtip: 'Freemarker template used to generate rule definition names under this rule type.  Helps to build the names dynamically and provide consistency for large rule sets.  Rule Definition names are read only when this is set and cannot be changed manually.'
						},
						{fieldLabel: 'Rollup Rule Type', name: 'rollupRuleType.name', hiddenName: 'rollupRuleType.id', xtype: 'combo', url: 'orderManagementRuleTypeListFind.json?rollup=true', qtip: 'If selected, the rule can only be a child of selected rollup rule type. This usually means that the rollup rule may have additional attributes required during rule execution of this rule.'},
						{
							fieldLabel: 'Executor Bean', name: 'ruleExecutorBean.name', hiddenName: 'ruleExecutorBean.id', xtype: 'combo', requiredFields: ['ruleCategory.name'], url: 'systemBeanListFind.json', detailPageClass: 'Clifton.system.bean.BeanWindow',
							getDefaultData: function() {
								return {type: {group: {name: TCG.getParentFormPanel(this).getFormValue('ruleCategory.ruleExecutorBeanGroup.name')}}};
							}
						},
						{
							fieldLabel: 'Specificity Filter Bean', beanName: 'ruleSpecificityFilterBean', xtype: 'system-bean-combo', groupName: 'Order Management Rules Specificity Filter',
							qtip: 'This bean is used to take all pre-filtered rules the use the same filter bean and apply specificity filtering within that list to find the most specific if more than one apply. For example: Rule that prohibits CCY trading, and a second rule that allows AUD.  They both apply, but the AUD rule would win when validating against an AUD order allocation.'
						},
						{
							fieldLabel: 'Definition Modify Condition', xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: false, name: 'ruleDefinitionModifyCondition.name', hiddenName: 'ruleDefinitionModifyCondition.id', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagementRuleDefinition',
							optionalConditionForNonAdminQtip: 'When set, rule definitions of this type must pass the selected modify condition in order to be modified.  The modify condition can be overridden for a specific rule definition.  If rule definition is a rollup, it is also used to restrict adding children to the rollup rule definition.'
						},
						{
							fieldLabel: 'Assignment Modify Condition', xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: false, name: 'ruleAssignmentModifyCondition.name', hiddenName: 'ruleAssignmentModifyCondition.id', mutuallyExclusiveFields: ['assignmentProhibited'], url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagementRuleAssignment',
							optionalConditionForNonAdminQtip: 'When set, rule assignments for rule definitions under this type must pass the selected modify condition in order to be modified. The assignment modify condition can be overridden for a specific rule definition. Does not apply if assignments are not allowed.'
						}
					]
				}]
			},


			{
				title: 'Rules',
				items: [{
					name: 'orderManagementRuleDefinitionListFind',
					xtype: 'gridpanel-custom-json-fields',
					tableName: 'OrderManagementRuleDefinition',
					instructions: 'The following are all of the rules available in the system for the selected type.',
					topToolbarSearchParameter: 'searchPattern',
					rowSelectionModel: 'checkbox',
					defaultHidden: false,
					getLinkedValueFilter: function() {
						if (this.getWindow().params) {
							return this.getWindow().params.id;
						}
						return -1;
					},

					nonCustomColumns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Rule Name', width: 50, dataIndex: 'name'},
						{header: 'Rule Description', width: 100, hidden: true, dataIndex: 'description'}
					],
					editor: {
						getDetailPageClass: function(grid, row) {
							if (TCG.isTrue(TCG.getValue('ruleCategory.rollup', this.getWindow().getMainForm().formValues))) {
								return 'Clifton.order.management.setup.rule.RollupRuleDefinitionWindow';
							}
							return 'Clifton.order.management.setup.rule.StandardRuleDefinitionWindow';
						},
						getDefaultData: function(gridPanel) {
							return {ruleType: gridPanel.getWindow().getMainForm().formValues};
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {ruleTypeId: this.getWindow().getMainFormId()};
					},
					addToolbarButtons: function(toolbar, gridPanel) {
						toolbar.add({
							text: 'Preview Rule Name',
							iconCls: 'preview',
							width: 120,
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select a Rule  to preview.', 'No Row(s) Selected');
								}
								else if (sm.getCount() !== 1) {
									TCG.showError('Multi-selection previews are not supported.  Please select one Rule.', 'NOT SUPPORTED');
								}
								else {
									const ruleDefinition = sm.getSelected().json;
									TCG.data.getDataValuePromise('orderManagementRuleDefinitionForName.json?ruleDefinitionId=' + sm.getSelected().id, this)
										.then(function(previewRule) {
											TCG.createComponent('TCG.app.OKCancelWindow', {
												doNotWarnOnCloseModified: true,
												okButtonText: 'Update Rule Name',
												okButtonTooltip: 'Save Rule Name  with newly added display name',
												title: 'Preview Rule Name',
												iconCls: 'preview',
												height: 300,
												width: 600,
												openerCt: gridPanel,

												items: [{
													xtype: 'formpanel',
													instructions: 'Preview results for a rule definition name .',
													labelWidth: 140,
													items: [
														{xtype: 'sectionheaderfield', header: 'Display Name'},
														{fieldLabel: 'Current Name', xtype: 'displayfield', value: ruleDefinition.name},
														{fieldLabel: 'New Name', xtype: 'displayfield', value: previewRule}
													]
												}],
												saveWindow: function(closeOnSuccess, forceSubmit) {
													gridPanel.updateRuleName([sm.getSelected().id]);
													this.closeWindow();
												}
											});
										});
								}
							}
						});

						toolbar.add('-');
						toolbar.add({
							text: 'Update',
							tooltip: 'Update Rule Definition Name.',
							iconCls: 'config',
							handler: function() {

								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									Ext.Msg.confirm('Update Rule Definition Name', 'Are you sure you want to update  name  for <b>ALL</b>  Rules?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Updating...',
												params: {ruleTypeId: gridPanel.getWindow().getMainFormId()},
												onLoad: function(record, conf) {
													gridPanel.reload();
												}
											});
											loader.load('orderManagementRuleDefinitionGenerate.json');

										}
									});
								}
								else {
									const ids = [];
									const ut = sm.getSelections();
									for (let i = 0; i < ut.length; i++) {
										ids.push(ut[i].json.id);
									}
									gridPanel.updateRuleName(ids);

								}
							}
						});
						toolbar.add('-');
					},
					updateRuleName: function(ruleDefinitionIds) {
						const gridPanel = this;
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							waitMsg: 'Updating...',
							params: {ruleDefinitionIds: ruleDefinitionIds},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('orderManagementRuleDefinitionGenerate.json');
					}

				}]
			},


			{
				title: 'Custom Rule Fields',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with rule definitions under this rule type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 150, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 40, dataIndex: 'order', type: 'int', defaultSortColumn: true},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'Global', width: 50, dataIndex: 'linkedToAllRows', type: 'boolean', tooltip: 'Global custom fields are not hierarchy specific: apply to all rows'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: 'Rule Definition Custom Fields',
							includeNullLinkedValue: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						addFromTemplateURL: 'systemColumn.json',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							const grp = TCG.data.getData('systemColumnGroupByName.json?name=Rule Definition Custom Fields', gridPanel, 'system.schema.group.Rule Definition Custom Fields');
							const fVal = gridPanel.getWindow().getMainForm().formValues;
							const lbl = TCG.getValue('name', fVal);
							return {
								columnGroup: grp,
								linkedValue: TCG.getValue('id', fVal),
								linkedLabel: lbl,
								table: grp.table
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			}


		]
	}]
});
