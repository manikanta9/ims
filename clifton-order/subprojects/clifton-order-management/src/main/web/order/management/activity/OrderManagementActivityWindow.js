Clifton.order.management.activity.OrderManagementActivityWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Order Management Activity',
	iconCls: 'blotter',
	height: 500,
	width: 900,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Use the following selections to apply actions to the selected list of Order Allocations.',
		labelWidth: 150,
		items: [
			{
				xtype: 'fieldset-checkbox',
				title: 'Trader Assignment',
				checkboxName: 'assignTrader',
				instructions: 'Use the following options to assign or re-assign the trader for selected order allocations.',
				onCheckClick: function() {
					TCG.form.FieldSetCheckbox.prototype.onCheckClick.call(this, arguments);
					const checked = this.checkbox.dom.checked;
					const fp = TCG.getParentFormPanel(this);
					const field = fp.getForm().findField('assignTraderUserLabel');

					if (!checked) {
						field.clearAndReset();
						field.allowBlank = true;
					}
					else {
						const currentUser = TCG.getCurrentUser();
						field.setValue({value: currentUser.id, text: currentUser.label});
						field.allowBlank = false;
					}
				},
				items: [
					{fieldLabel: 'Trader', name: 'assignTraderUserLabel', hiddenName: 'assignTraderUserId', xtype: 'combo', url: 'securityUserListFind.json', displayField: 'label'},
					{boxLabel: 'Force re-assign (leave unchecked to only populate trader if blank on selected order allocations)', xtype: 'checkbox', name: 'reassignTraderIfPopulated'}
				]
			},
			{
				xtype: 'fieldset',
				title: 'Order Blocking / Routing',
				instructions: 'Preview or Create Block Orders for the selected Order Allocations.  Order Allocations are always grouped based on Security, Side (Buy/Sell) and Trader, but can optionally include the below options in the grouping. All unassigned Order Allocations will automatically be assigned to you (if Trader is not selected above and not adding to an existing Order Allocation already assigned to another Trader). The option to add to existing uplaced Block Orders allows the system to add Order Allocations to an existing Block Order if criteria are met.',
				items: [
					{boxLabel: 'Create Block Order(s) from selected Order Allocations', xtype: 'checkbox', name: 'createBlockOrders'},
					// TODO {boxLabel: 'Add to Existing Unplaced Orders', xtype: 'checkbox', name: 'useExistingOrders', requiredFields: ['createBlockOrders'], setVisibilityOnRequired: true},
					{
						xtype: 'checkboxgroup',
						fieldLabel: 'Block Order Criteria',
						requiredFields: ['createBlockOrders'], setVisibilityOnRequired: true,
						columns: 1,
						items: [
							{boxLabel: 'Account', name: 'orderBlockAccount'}
							// TODO - EXECUTION TYPE? WHAT ELSE WOULD WE WANT TO GROUP ON
						]
					},
					{boxLabel: 'Create Placements (if a Destination is selected below or pre-defined on the Allocation any Block Order that can be grouped and executed with that destination will automatically be placed)', xtype: 'checkbox', name: 'createPlacements'},
					{
						fieldLabel: 'Default Destination', name: 'orderDestinationLabel', hiddenName: 'orderDestinationId', xtype: 'combo', requiredFields: ['createPlacements'], disableAddNewItem: true, detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', url: 'orderDestinationListFind.json?executionVenue=true&backupDestination=false&disabled=false', allowBlank: true,
						qtip: 'Any order that can be assigned to the selected destination will be assigned and placed.'
					},
					{
						fieldLabel: 'Destination Configuration', name: 'destinationConfigurationLabel', hiddenName: 'destinationConfigurationId', xtype: 'combo', requiredFields: ['orderDestinationId'], disableAddNewItem: true, detailPageClass: 'Clifton.order.setup.destination.DestinationConfigurationWindow', url: 'orderDestinationConfigurationListFind.json?disabled=false', allowBlank: true,
						qtip: 'For the selected destination, optionally select a specific configuration to use.',
						listeners: {
							beforequery: function(queryEvent) {
								const f = queryEvent.combo.getParentForm();
								queryEvent.combo.store.setBaseParam('orderDestinationId', f.getFormValue('orderDestinationId', true));
							}
						}
					}

				],
				buttons: [
					{
						text: 'Preview',
						xtype: 'button',
						iconCls: 'preview',
						width: 120,
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							f.previewBlockOrders();
						}
					}
				]
			}
		],

		previewBlockOrders: function() {
			//let formParams = this.getFormValuesFormatted();
			//formParams = Ext.apply(formParams, this.getSubmitParams());
			//TCG.data.getDataPromise('orderManagementBlockListPreview.json', this, Ext.encode(formParams));
			TCG.showError('Not Implemented Yet');
		},

		getSaveURL: function() {
			return 'orderManagementActivityForOrderAllocationsProcess.json?enableValidatingBinding=true';
		},
		getSubmitParams: function() {
			return this.getWindow().additionalSubmitParameters;
		}
	}]
});
