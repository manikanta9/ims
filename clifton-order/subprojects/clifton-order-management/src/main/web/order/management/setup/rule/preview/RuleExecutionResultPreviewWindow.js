Clifton.order.management.setup.rule.preview.RuleExecutionResultPreviewWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Rule Execution Preview',
	id: 'orderManagementExecutionRulePreviewWindow',
	iconCls: 'preview',
	width: 800,
	height: 800,

	doNotWarnOnCloseModified: true,
	defaultDataIsReal: true,

	layout: 'border',
	items: [
		{
			xtype: 'formpanel',
			instructions: 'Rule preview allows you to view each rule definition that is processed for the selected options and whether or not the rule passes or fails.  When these rules are run against an actual order allocation, every rule definition must pass.',
			region: 'north',
			height: 250,
			labelWidth: 120,
			loadDefaultDataAfterRender: true,

			listeners: {
				afterload: function(fp) {
					if (TCG.isEquals(fp.getFormValue('clientAccount.id'), fp.getFormValue('holdingAccount.id'))) {
						fp.hideField('holdingAccount.label');
						fp.setFieldLabel('clientAccount.label', 'Account:');
					}
				}
			},
			items: [
				{xtype: 'hidden', name: 'clientAccount.clientCompany.id', submitValue: false},
				{
					fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', url: 'orderAccountListFind.json?clientAccount=true', displayField: 'label', allowBlank: false, requestedProps: 'holdingAccount|clientCompany.id|baseCurrencyCode', detailPageClass: 'Clifton.order.shared.account.AccountWindow',
					listeners: {
						select: function(combo, record, index) {
							const clientCompanyId = record.json.clientCompany.id;
							const fp = TCG.getParentFormPanel(combo);
							fp.setFormValue('clientAccount.clientCompany.id', clientCompanyId);
							const holdingAccount = record.json.holdingAccount;
							if (TCG.isTrue(holdingAccount)) {
								fp.setFormValue('holdingAccount.id', {value: record.json.id, text: record.json.label});
								fp.hideField('holdingAccount.label');
								fp.setFieldLabel('clientAccount.label', 'Account:');
							}
							else {
								fp.showField('holdingAccount.label');
								fp.setFieldLabel('clientAccount.label', 'Client Account:');
							}
							const currencyCode = record.json.baseCurrencyCode;
							if (currencyCode) {
								TCG.data.getDataPromiseUsingCaching('orderSecurityForCurrencyTicker.json?ticker=' + currencyCode, fp, 'order.shared.security.currency.' + currencyCode)
									.then(function(curr) {
										fp.setFormValue('settlementCurrency.id', {value: curr.id, text: curr.label});
									});
							}
						}
					}
				},
				{
					fieldLabel: 'Holding Account', name: 'holdingAccount.label', hiddenName: 'holdingAccount.id', xtype: 'combo', url: 'orderAccountListFind.json?holdingAccount=true', displayField: 'label', requiredFields: ['clientAccount.label'], allowBlank: false, detailPageClass: 'Clifton.order.shared.account.AccountWindow',
					requestedProps: 'baseCurrencyCode',
					listeners: {
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							const clientCompanyId = form.findField('clientAccount.clientCompany.id').value;
							queryEvent.combo.store.setBaseParam('clientCompanyId', clientCompanyId);
						},
						select: function(combo, record, index) {
							const currencyCode = record.json.baseCurrencyCode;
							if (currencyCode) {
								TCG.data.getDataPromiseUsingCaching('orderSecurityForCurrencyTicker.json?ticker=' + currencyCode, fp, 'order.shared.security.currency.' + currencyCode)
									.then(function(curr) {
										fp.setFormValue('settlementCurrency.id', {value: curr.id, text: curr.label});
									});
							}
						}
					}

				},
				{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false, value: new Date().format('m/d/Y')},
				{xtype: 'hidden', name: 'securityType.id', submitValue: false, disabled: true},
				{
					fieldLabel: 'Order Type', name: 'orderType.name', hiddenName: 'orderType.id', xtype: 'combo', url: 'orderTypeListFind.json', requestedProps: 'securityType.id', allowBlank: false, detailPageClass: 'Clifton.order.setup.OrderTypeWindow',
					listeners: {
						select: function(combo, record, index) {
							const securityTypeId = record.json.securityType.id;
							const fp = TCG.getParentFormPanel(combo);
							fp.setFormValue('securityType.id', securityTypeId);
						}
					}

				},
				{
					fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', url: 'orderSecurityListFind.json', displayField: 'label', allowBlank: false, requiredFields: ['orderType.name', 'tradeDate'], detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
					listeners: {
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							queryEvent.combo.store.setBaseParam('activeOnDate', form.findField('tradeDate').value);

							const securityTypeId = form.findField('securityType.id').value;
							queryEvent.combo.store.setBaseParam('securityTypeId', securityTypeId);
						}
					}
				},
				{
					fieldLabel: 'Settlement Currency', name: 'settlementCurrency.label', hiddenName: 'settlementCurrency.id', xtype: 'combo', url: 'orderSecurityListFind.json?securityType=Currency', displayField: 'label', allowBlank: true, requiredFields: ['tradeDate'], detailPageClass: 'Clifton.order.shared.security.SecurityWindow',
					listeners: {
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							queryEvent.combo.store.setBaseParam('activeOnDate', form.findField('tradeDate').value);
						}
					}
				},
				{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.name', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', url: 'orderCompanyListFind.json?categoryName=Company Tags&categoryHierarchyName=Broker', detailPageClass: 'Clifton.order.shared.company.CompanyWindow', allowBlank: true}
			]
		},
		{
			xtype: 'gridpanel',
			region: 'center',
			height: 400,
			title: 'Preview Results',
			name: 'orderManagementRuleExecutionResultList',

			viewConfig: {emptyText: 'No rules found to execute.', deferEmptyText: false},
			columns: [
				{header: 'Rule Definition ID', hidden: true, width: 25, dataIndex: 'id'},
				{header: 'Rule Category', width: 100, dataIndex: 'ruleDefinition.ruleType.ruleCategory.name'},
				{header: 'Rule Definition', width: 200, dataIndex: 'ruleDefinition.name'},
				{
					header: 'Pass / Fail', width: 50, dataIndex: 'executionResult', align: 'center',
					renderer: function(v, metaData, r) {
						if (TCG.isTrue(v)) {
							return TCG.renderIconWithTooltip('row_checked', 'Passed');
						}
						return TCG.renderIconWithTooltip('stop', 'Failed');
					}
				}
			],
			editor: {
				detailPageClass: 'Clifton.order.management.setup.rule.RuleDefinitionWindow',
				drillDownOnly: true
			},
			getLoadParams: function(firstLoad) {
				const fp = this.getWindow().getMainFormPanel();
				const o = fp.getFirstInValidField();
				if (TCG.isNull(o)) {
					const formParams = fp.getFormValuesFormatted();
					const params = {};
					params['clientAccountId'] = formParams['clientAccount.id'];
					params['holdingAccountId'] = formParams['holdingAccount.id'];
					params['tradeDate'] = formParams['tradeDate'];
					params['orderTypeId'] = formParams['orderType.id'];
					params['orderSecurityId'] = formParams['security.id'];
					params['settlementCurrencyId'] = formParams['settlementCurrency.id'];
					params['executingBrokerCompanyId'] = formParams['executingBrokerCompany.id'];
					return params;
				}
				else if (!firstLoad) {
					TCG.showError('Please select required fields above.');
				}
				return false;
			}
		}

	]
});
