Clifton.order.management.setup.rule.RuleDefinitionCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Copy Rule Definition',
	modal: true,

	//defaultDataIsReal: true,
	doNotWarnOnCloseModified: true,
	okButtonText: 'Copy',

	items: [{
		xtype: 'formpanel-custom-json-fields',
		columnGroupName: 'Rule Definition Custom Fields',
		loadValidation: false,
		labelWidth: 150,
		url: 'orderManagementRuleDefinition.json',
		instructions: 'To copy an existing rule enter the new name (if not auto-generated), optional description override and new properties (if available) listed below. After saving, the new rule window will be opened.  If the rule is a rollup rule, all child rules will also be copied to the new rule.',
		listeners: {
			afterload: function(panel, closeOnSuccess) {
				// Don't allow changing rollup rule type after saved
				if (TCG.isNotBlank(this.getFormValue('ruleType.ruleDefinitionNameTemplate'))) {
					this.setNameReadOnly(true);
				}
				if (TCG.isTrue(closeOnSuccess)) {
					// the entity was already retrieved: pass it to the window and instruct not to get it again
					const config = {
						defaultDataIsReal: true,
						defaultData: panel.getForm().formValues,
						openerCt: panel.ownerCt.openerCt
					};
					TCG.createComponent('Clifton.order.management.setup.rule.RuleDefinitionWindow', config);
				}
			}
		},
		setNameReadOnly: function(readOnly) {
			this.setReadOnlyField('name', readOnly);
			const nameField = this.getForm().findField('name');
			nameField.allowBlank = readOnly;
			this.setFieldQtip('name', readOnly ? 'Rule Name will be dynamically generated for you by the rule options.' : '');
		},
		items: [
			{fieldLabel: 'Copy Rule', name: 'label', xtype: 'displayfield', submitValue: false},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'New Rule Name', name: 'name', xtype: 'textfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		],
		getSaveURL: function() {
			return 'orderManagementRuleDefinitionCopy.json';
		}
	}]
});
