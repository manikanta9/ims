Clifton.order.management.setup.rule.RollupRuleDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rollup Order Rule Definition',
	iconCls: 'rule',
	width: 1200,
	height: 900,
	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				layout: 'border',
				items: [
					{
						xtype: 'formpanel-custom-json-fields',
						columnGroupName: 'Rule Definition Custom Fields',
						dynamicFieldFormFragment: 'ruleDefinitionCustomFields',
						region: 'north',
						height: 325,
						labelWidth: 220,
						url: 'orderManagementRuleDefinition.json',
						instructions: 'A rollup rule is a collection of other rules. Assigning a rollup rule to an account is equivalent to assigning every single child rule to that account. Rollup rules are used to simplify management of similar rule sets.',

						listeners: {
							afterload: function(panel) {
								// Don't allow changing rollup rule type after saved
								this.setReadOnlyField('ruleType.name', true);
								if (TCG.isNotBlank(this.getFormValue('ruleType.ruleDefinitionNameTemplate'))) {
									this.setNameReadOnly(true);
								}
								if (TCG.isTrue(this.getFormValue('ruleType.assignmentProhibited'))) {
									this.setReadOnlyField('overrideRuleAssignmentModifyCondition.name', true);
								}
							}
						},
						setNameReadOnly: function(readOnly) {
							this.setReadOnlyField('name', readOnly);
							const nameField = this.getForm().findField('name');
							nameField.allowBlank = readOnly;
							this.setFieldQtip('name', readOnly ? 'Rule Name will be dynamically generated for you by the rule options.' : '');
						},
						getWarningMessage: function(f) {
							this.loadRuleUsedByWarningMessage(f);
						},
						ruleUsedWarningMessage: undefined, // loaded by loadRuleUsedByWarningMessage
						confirmBeforeSaveMsgTitle: 'Edit Actively Used Rule',
						getConfirmBeforeSaveMsg: function() {
							if (TCG.isNotBlank(this.ruleUsedWarningMessage)) {
								return this.ruleUsedWarningMessage + '<br/><br/>Are you sure you want to continue and edit this rule?';
							}
							return undefined;
						},
						loadRuleUsedByWarningMessage: function(f) {
							const fp = this;
							const formValues = fp.getForm().formValues;
							let msg = undefined;

							TCG.data.getDataPromise('orderManagementRuleAssignmentListFind.json?requestedPropertiesRoot=data&requestedProperties=id&active=true&ruleDefinitionIdOrChildId=' + formValues.id, this)
								.then(activeAssignments => {
									if (activeAssignments.length > 0) {
										msg = '<b>Warning</b>: This rule definition is actively being used by <b><u>' + activeAssignments.length + '</u></b> rule assignments. See rule assignments tab for more detail.';
										if (fp.ruleUsedWarningMessage !== msg) {
											fp.ruleUsedWarningMessage = msg;
											const ruleUsedWarnId = fp.id + '-ruleUsedWarn';
											const ruleUsedWarn = Ext.get(ruleUsedWarnId);
											if (TCG.isNotNull(ruleUsedWarn)) {
												fp.remove(ruleUsedWarn);
												Ext.destroy(ruleUsedWarn);
												fp.doLayout();
											}
											const html = msg;
											msg = [{xtype: 'label', html: html}];
											fp.insert(0, {xtype: 'container', layout: 'hbox', id: ruleUsedWarnId, autoEl: 'div', cls: 'warning-msg', items: msg});
											fp.doLayout();
										}
									}
								});
						},
						items: [
							{
								fieldLabel: 'Rule Type', name: 'ruleType.name', hiddenName: 'ruleType.id', xtype: 'combo', url: 'orderManagementRuleTypeListFind.json?rollup=true', detailPageClass: 'Clifton.order.management.setup.rule.RuleTypeWindow',
								requestedProps: 'ruleDefinitionNameTemplate|assignmentProhibited',
								listeners: {
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										fp.setNameReadOnly(TCG.isNotBlank(record.json.ruleDefinitionNameTemplate));
										if (TCG.isTrue(record.json.assignmentProhibited)) {
											fp.setReadOnlyField('overrideRuleAssignmentModifyCondition.name', true);

										}
									}
								}
							},
							{fieldLabel: 'Rule Name', name: 'name', xtype: 'textfield'},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
							{
								xtype: 'columnpanel',
								columns: [
									{
										rows: [
											{
												fieldLabel: 'Definition Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
												qtip: 'This Field represents either the overridden definition modify condition or the inherited definition modify condition from the rule type. The overridden takes precedent. In other words: this is the applicable condition'
											},
											{fieldLabel: 'Override Definition Modify Condition', name: 'overrideRuleDefinitionModifyCondition.name', hiddenName: 'overrideRuleDefinitionModifyCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagmentRuleDefinition', qtip: 'Optionally override the modify condition on the rule type. Used to restrict if this rule definition can be modified. If rule definition is a rollup, it\'s also used to restrict adding children rule definitions to this rollup rule definition. For example, we can prohibit rules used globally to only be modified by administrators'}
										]
									},
									{
										rows: [
											{
												fieldLabel: 'Assignment Modify Condition', name: 'coalesceAssignmentEntityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'coalesceAssignmentEntityModifyCondition.id', submitDetailField: false,
												qtip: 'This Field represents either the overridden assignment modify condition or the inherited assignment modify condition from the rule type. The overridden takes precedent. In other words: this is the applicable condition'
											},
											{fieldLabel: 'Override Assignment Modify Condition', name: 'overrideRuleAssignmentModifyCondition.name', hiddenName: 'overrideRuleAssignmentModifyCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagementRuleAssignment', qtip: 'Optionally override the modify condition on the rule type. Used to restrict if this rule definition can be assigned. If rule definition is a rollup, it\'s also used to restrict adding children rule definitions to this rollup rule definition. For example, we may restrict assignments for Investor ID rules to only be modified by Settlements group'}
										]
									}
								]
							},
							{xtype: 'label', html: '<hr />'},
							{
								xtype: 'formfragment',
								frame: false,
								labelWidth: 220,
								name: 'ruleDefinitionCustomFields',
								items: []
							},
							{
								xtype: 'system-tags-fieldset',
								title: 'Tags',
								name: 'oms-rule-tags',
								tableName: 'OrderManagementRuleDefinition',
								hierarchyCategoryName: 'OMS Rule Tags'
							}
						]
					},
					{
						region: 'center',
						title: 'Child Rules',
						name: 'orderManagementRuleDefinitionListFind',
						xtype: 'gridpanel-custom-json-fields',
						tableName: 'OrderManagementRuleDefinition',
						instructions: 'The following are all of the rules available in the system. Rules can be used (based on rule types) for various purposes such as limited what securities can be traded for an account, what executing brokers are allowed, etc.  Rules can be grouped under a rollup rule for easier assignment to accounts that share similar characteristics.',
						topToolbarSearchParameter: 'searchPattern',
						groupField: 'ruleType.name',

						listeners: {
							afterRender: function() {
								const gp = this;
								const fp = this.getWindow().getMainFormPanel();
								fp.on('afterload', function(fp) {
									const ruleTypeName = fp.getFormValue('ruleType.name');
									if (TCG.contains(ruleTypeName, 'Currency')) {
										gp.setColumnHidden('customColumns.Security Type', TCG.contains(ruleTypeName, 'Currency'));
										gp.setColumnHidden('customColumns.Executing Broker', TCG.contains(ruleTypeName, 'Currency'));
										gp.setColumnHidden('customColumns.Require 11A', !TCG.contains(ruleTypeName, 'Currency'));
										gp.setColumnHidden('customColumns.Can Net Through 11A NO', !TCG.contains(ruleTypeName, 'Currency'));
										gp.setColumnHidden('customColumns.Can Use Local Accounting Balance', !TCG.contains(ruleTypeName, 'Currency'));
									}
								}, this);
							}
						},

						columnOverrides: [
							{dataIndex: 'customColumns.Security Type', hidden: false},
							{dataIndex: 'customColumns.Security', hidden: false},
							{dataIndex: 'customColumns.Executing Broker', hidden: false}
						],


						nonCustomColumns: [
							{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
							{header: 'Rule Category', width: 40, dataIndex: 'ruleType.ruleCategory.name', hidden: true, filter: {searchFieldName: 'ruleCategoryId', type: 'combo', url: 'orderManagementRuleCategoryListFind.json'}},
							{header: 'Rule Type', width: 40, dataIndex: 'ruleType.name', hidden: true, filter: {searchFieldName: 'ruleTypeId', type: 'combo', url: 'orderManagementRuleTypeListFind.json'}},
							{header: 'Rule Definition', width: 75, dataIndex: 'name'},
							{header: 'Rule Definition Description', width: 100, dataIndex: 'description', hidden: true},
							{header: 'Rollup', width: 25, hidden: true, dataIndex: 'rollup', tooltip: 'Rollup rules are a grouping of rule definitions.', type: 'boolean', filter: {searchFieldName: 'rollup'}}
						],

						getTopToolbarInitialLoadParams: function() {
							const w = this.getWindow();
							if (w.isMainFormSaved()) {
								return {parentId: w.getMainFormId()};
							}
							return false;
						},
						editor: {
							detailPageClass: 'Clifton.order.management.setup.rule.StandardRuleDefinitionWindow',
							deleteURL: 'orderManagementRuleDefinitionRollupUnlink.json',
							allowToDeleteMultiple: true,
							reloadGridAfterDelete: true,
							getDeleteParams: function(selectionModel) {
								return {
									parentRuleDefinitionId: this.getWindow().getMainFormId(),
									childRuleDefinitionId: selectionModel.getSelected().id
								};
							},
							addToolbarAddButton: function(toolBar) {
								const gridPanel = this.getGridPanel();
								toolBar.add(new TCG.form.ComboBox({
									name: 'childRule', url: 'orderManagementRuleDefinitionListFind.json?rollup=false', width: 225, listWidth: 300,
									listeners: {
										beforequery: function(queryEvent) {
											queryEvent.combo.store.setBaseParam('rollupRuleTypeIdOrNull', gridPanel.getWindow().getMainFormPanel().getFormValue('ruleType.id'));
										}
									}
								}));
								toolBar.add({
									text: 'Add',
									tooltip: 'Add an existing rule as a child of this rollup.',
									iconCls: 'add',
									scope: this,
									handler: function() {
										const w = gridPanel.getWindow();
										if (!w.isMainFormSaved()) {
											TCG.showError('Please save the window before adding children.');
											return false;
										}
										const childRule = TCG.getChildByName(toolBar, 'childRule');
										const childRuleId = childRule.getValue();
										if (TCG.isBlank(childRuleId)) {
											TCG.showError('You must first select desired Child Rule from the list.');
										}
										else {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Linking...',
												params: {parentRuleDefinitionId: w.getMainFormId(), childRuleDefinitionId: childRuleId},
												onLoad: function(record, conf) {
													gridPanel.reload();
													TCG.getChildByName(toolBar, 'childRule').reset();
												}
											});
											loader.load('orderManagementRuleDefinitionRollupLink.json');
										}
									}
								});
								toolBar.add('-');
							}
						}
					}
				]
			},
			{
				title: 'Rule Assignments',
				items: [{
					xtype: 'order-management-rule-assignment-list-grid',
					groupField: undefined,
					columnOverrides: [
						{dataIndex: 'ruleDefinition.name', hidden: true},
						{dataIndex: 'clientAccount.labelShort', hidden: false},
						{dataIndex: 'coalesceHoldingAccountIssuingCompany.name', hidden: false}
					],
					getDefaultData: function(gridPanel) { // defaults rule definition for the assignment page
						return {
							ruleDefinition: gridPanel.getWindow().getMainForm().formValues
						};
					},

					getLoadParams: function(firstLoad) {
						return {ruleDefinitionId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
