Clifton.order.management.setup.rule.StandardRuleDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Rule Definition',
	iconCls: 'rule',
	width: 1200,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel-custom-json-fields',
					columnGroupName: 'Rule Definition Custom Fields',
					dynamicFieldFormFragment: 'ruleDefinitionCustomFields',
					instructions: 'A Rule Definition is a specific rule in the system.  Available options for each rule is dependent on the rule type selected.',
					url: 'orderManagementRuleDefinition.json',
					labelWidth: 210,
					listeners: {
						afterload: function(panel) {
							if (TCG.isNotBlank(this.getFormValue('ruleType.ruleDefinitionNameTemplate'))) {
								this.setNameReadOnly(true);
							}
							if (TCG.isTrue(this.getFormValue('ruleType.assignmentProhibited'))) {
								this.setReadOnlyField('overrideRuleAssignmentModifyCondition.name', true);
							}
						}
					},
					setNameReadOnly: function(readOnly) {
						this.setReadOnlyField('name', readOnly);
						const nameField = this.getForm().findField('name');
						nameField.allowBlank = readOnly;
						this.setFieldQtip('name', readOnly ? 'Rule Name will be dynamically generated for you by the rule options.' : '');
					},
					getWarningMessage: function(f) {
						this.loadRuleUsedByWarningMessage(f);
					},
					ruleUsedWarningMessage: undefined, // loaded by loadRuleUsedByWarningMessage
					confirmBeforeSaveMsgTitle: 'Edit Actively Used Rule',
					getConfirmBeforeSaveMsg: function() {
						if (TCG.isNotBlank(this.ruleUsedWarningMessage)) {
							return this.ruleUsedWarningMessage + '<br/><br/>Are you sure you want to continue and edit this rule?';
						}
						return undefined;
					},
					loadRuleUsedByWarningMessage: function(f) {
						const fp = this;
						const formValues = fp.getForm().formValues;
						let msg = undefined;

						TCG.data.getDataPromise('orderManagementRuleAssignmentListFind.json?requestedPropertiesRoot=data&requestedProperties=id&active=true&ruleDefinitionIdOrChildId=' + formValues.id, this)
							.then(activeAssignments => {
								if (activeAssignments.length > 0) {
									msg = '<b>Warning</b>: This rule definition is actively being used by <b><u>' + activeAssignments.length + '</u></b> rule assignments. See rule assignments tab for more detail.';
									if (fp.ruleUsedWarningMessage !== msg) {
										fp.ruleUsedWarningMessage = msg;
										const ruleUsedWarnId = fp.id + '-ruleUsedWarn';
										const ruleUsedWarn = Ext.get(ruleUsedWarnId);
										if (TCG.isNotNull(ruleUsedWarn)) {
											fp.remove(ruleUsedWarn);
											Ext.destroy(ruleUsedWarn);
											fp.doLayout();
										}
										const html = msg;
										msg = [{xtype: 'label', html: html}];
										fp.insert(0, {xtype: 'container', layout: 'hbox', id: ruleUsedWarnId, autoEl: 'div', cls: 'warning-msg', items: msg});
										fp.doLayout();
									}
								}
							});
					},

					items: [
						{
							fieldLabel: 'Rule Type', name: 'ruleType.name', hiddenName: 'ruleType.id', xtype: 'combo', url: 'orderManagementRuleTypeListFind.json?rollup=false', detailPageClass: 'Clifton.order.management.setup.rule.RuleTypeWindow',
							requestedProps: 'ruleDefinitionNameTemplate|assignmentProhibited',
							listeners: {
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									fp.setNameReadOnly(TCG.isNotBlank(record.json.ruleDefinitionNameTemplate));
									if (TCG.isTrue(record.json.assignmentProhibited)) {
										fp.setReadOnlyField('overrideRuleAssignmentModifyCondition.name', true);

									}
								}
							}
						},
						{fieldLabel: 'Rule Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50, grow: true},
						{fieldLabel: 'Violation Note Template', name: 'violationNoteTemplateOverride', xtype: 'textarea', height: 50, grow: true, qtip: 'When running this rule through the rule engine we can override the violation note (currently defaults to the rule definition name) to be more specific.'},
						{fieldLabel: 'Violation Code Template', name: 'violationCodeTemplate', xtype: 'textarea', height: 50, grow: true, qtip: 'For Rule Engine rule violations, if the rule fails, we can customize the message. The linkedEntity (OrderRuleAware object) and causeEntity (this rule definition) are available'},

						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{
											fieldLabel: 'Definition Modify Condition', name: 'entityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'entityModifyCondition.id', submitDetailField: false,
											qtip: 'This Field represents either the overridden definition modify condition or the inherited definition modify condition from the rule type. The overridden takes precedent. In other words: this is the applicable condition'
										},
										{fieldLabel: 'Override Definition Modify Condition', name: 'overrideRuleDefinitionModifyCondition.name', hiddenName: 'overrideRuleDefinitionModifyCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagmentRuleDefinition', qtip: 'Optionally override the modify condition on the rule type. Used to restrict if this rule definition can be modified. If rule definition is a rollup, it\'s also used to restrict adding children rule definitions to this rollup rule definition. For example, we can prohibit rules used globally to only be modified by administrators'}
									]
								},
								{
									rows: [
										{
											fieldLabel: 'Assignment Modify Condition', name: 'coalesceAssignmentEntityModifyCondition.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.condition.ConditionWindow', detailIdField: 'coalesceAssignmentEntityModifyCondition.id', submitDetailField: false,
											qtip: 'This Field represents either the overridden assignment modify condition or the inherited assignment modify condition from the rule type. The overridden takes precedent. In other words: this is the applicable condition'
										},
										{fieldLabel: 'Override Assignment Modify Condition', name: 'overrideRuleAssignmentModifyCondition.name', hiddenName: 'overrideRuleAssignmentModifyCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=OrderManagementRuleAssignment', qtip: 'Optionally override the modify condition on the rule type. Used to restrict if this rule definition can be assigned. If rule definition is a rollup, it\'s also used to restrict adding children rule definitions to this rollup rule definition. For example, we may restrict assignments for Investor ID rules to only be modified by Settlements group'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr />'},
						{
							xtype: 'formfragment',
							frame: false,
							labelWidth: 135,
							name: 'ruleDefinitionCustomFields',
							items: []
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							name: 'oms-rule-tags',
							tableName: 'OrderManagementRuleDefinition',
							hierarchyCategoryName: 'OMS Rule Tags'
						}
					]
				}]
			},


			{
				title: 'Rule Assignments',
				items: [{
					xtype: 'order-management-rule-assignment-list-grid',
					groupField: undefined,
					columnOverrides: [
						{dataIndex: 'ruleDefinition.name', hidden: true},
						{dataIndex: 'clientAccount.labelShort', hidden: false},
						{dataIndex: 'coalesceHoldingAccountIssuingCompany.name', hidden: false}
					],
					getLoadParams: function(firstLoad) {
						if (TCG.isTrue(TCG.getChildByName(this.getTopToolbar(), 'includeChildAssignment').getValue())) {
							return {ruleDefinitionIdOrChildId: this.getWindow().getMainFormId()};
						}
						return {ruleDefinitionId: this.getWindow().getMainFormId()};
					},
					getDefaultData: function(gridPanel) { // defaults rule definition for the assignment page
						return {
							ruleDefinition: gridPanel.getWindow().getMainForm().formValues
						};
					},
					getTopToolbarFilters: function(toolbar) {
						return [{boxLabel: 'Include As a Child Assignment&nbsp;&nbsp;', xtype: 'toolbar-checkbox', name: 'includeChildAssignment', checked: true, qtip: 'If checked will include assignments where this rule is a child of a rollup rule that is assigned.'}];
					}
				}]
			},


			{
				title: 'Rollups',
				name: 'orderManagementRuleDefinitionListFind',
				xtype: 'gridpanel',
				instructions: 'The following are all of the rollup rules this rule is a child of',
				columns: [
					{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
					{header: 'Parent Rule Definition', width: 40, dataIndex: 'name'},
					{header: 'Parent Rule Definition Description', width: 100, dataIndex: 'description'}
				],
				getLoadParams: function(firstLoad) {
					return {childId: this.getWindow().getMainFormId()};
				},
				editor: {
					detailPageClass: 'Clifton.order.management.setup.rule.RollupRuleDefinitionWindow'
				}
			},


			{
				title: 'Custom Assignment Fields',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with rule assignments for this rule definition.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 150, dataIndex: 'name'},
						{header: 'Column Label', width: 150, dataIndex: 'label'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 40, dataIndex: 'order', type: 'int', defaultSortColumn: true},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'Global', width: 50, dataIndex: 'linkedToAllRows', type: 'boolean', tooltip: 'Global custom fields are not hierarchy specific: apply to all rows'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: 'Rule Assignment Custom Fields',
							includeNullLinkedValue: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						addFromTemplateURL: 'systemColumn.json',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							const grp = TCG.data.getData('systemColumnGroupByName.json?name=Rule Assignment Custom Fields', gridPanel, 'system.schema.group.Rule Assignment Custom Fields');
							const fVal = gridPanel.getWindow().getMainForm().formValues;
							const lbl = TCG.getValue('name', fVal);
							return {
								columnGroup: grp,
								linkedValue: TCG.getValue('id', fVal),
								linkedLabel: lbl,
								table: grp.table
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			}

		]
	}]
});
