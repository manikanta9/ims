package com.clifton.order.management.execution.action.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationAmendmentCommand;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.block.OrderBlockService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.OrderPlacementAllocation;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.note.OrderNoteService;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.shared.OrderSharedUtilHandler;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.order.trade.OrderTrade;
import com.clifton.order.trade.OrderTradeService;
import com.clifton.order.trade.search.OrderTradeSearchForm;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionSearchCommand;
import com.clifton.workflow.transition.WorkflowTransitionService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementExecutionAmendPlacementActionProcessor extends BaseOrderManagementExecutionActionProcessor {

	private final static String AMENDMENT_REASON_NOTE_TYPE_NAME = "Amendment Reason";
	private OrderBlockService orderBlockService;
	private OrderSharedMarketDataService orderSharedMarketDataService;
	private OrderSharedUtilHandler orderSharedUtilHandler;
	private OrderTradeService orderTradeService;
	private OrderNoteService orderNoteService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.AMEND_EXECUTION);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		Date overrideTradeDate = getValueOverride(orderPlacement.getOrderBlock().getTradeDate(), command.getOverrideTradeDate());
		Date overrideSettlementDate = getValueOverride(orderPlacement.getOrderBlock().getSettlementDate(), command.getOverrideSettlementDate());
		BigDecimal overrideExecutionPrice = getValueOverride(orderPlacement.getAverageUnitPrice(), command.getOverrideAverageUnitPrice());

		// If no actual changes, just return
		if (overrideTradeDate == null && overrideSettlementDate == null && overrideExecutionPrice == null) {
			if (!command.isSkipIfNoChanges()) {
				throw new ValidationException("No changes supplied apply to the execution - no updates made.");
			}
			return;
		}

		List<OrderAllocation> orderAllocationList = getOrderAllocationListForPlacement(orderPlacement);
		Map<OrderAllocation, OrderAllocationAmendmentCommand> orderAllocationAmendmentMap = new HashMap<>();
		orderAllocationList.forEach(orderAllocation -> orderAllocationAmendmentMap.put(orderAllocation, OrderAllocationAmendmentCommand.forOrderAllocation(orderAllocation.getId())));
		OrderBlock orderBlock = getOrderBlockService().getOrderBlock(orderPlacement.getOrderBlock().getId(), false);
		List<OrderPlacementAllocation> placementAllocationList = getOrderPlacementAllocationListForPlacement(orderPlacement);
		List<OrderTrade> orderTradeList = getOrderTradeListForOrderAllocationList(orderAllocationList);

		// There are some fields (trade date) that should not be modified for multiple placements within a block order.  If that is the case then we need to modify from the block order level
		boolean singlePlacement = MathUtils.isEqual(orderPlacement.getQuantityIntended(), orderPlacement.getOrderBlock().getQuantityIntended());
		boolean fullyFilled = MathUtils.isEqual(orderPlacement.getQuantityFilled(), orderPlacement.getQuantityIntended());
		ValidationUtils.assertTrue(singlePlacement && fullyFilled, "Logic was only implemented for single placements that are fully filled");

		// Track which entities we are actually updating - Order Allocations and Order Trades are affected by any of these changes, so we don't need to track
		boolean updateBlockOrder = false;
		boolean updatePlacement = false;

		if (overrideTradeDate != null || overrideSettlementDate != null) {
			updateBlockOrder = true;
			if (overrideTradeDate != null) {
				orderAllocationAmendmentMap.values().forEach(orderAllocationAmendmentCommand -> orderAllocationAmendmentCommand.setAmendTradeDate(overrideTradeDate));
				orderBlock.setTradeDate(overrideTradeDate);
				orderTradeList.forEach(orderTrade -> orderTrade.setTradeDate(overrideTradeDate));
			}
			if (overrideSettlementDate != null) {
				orderAllocationAmendmentMap.values().forEach(orderAllocationAmendmentCommand -> orderAllocationAmendmentCommand.setAmendSettlementDate(overrideSettlementDate));
				orderBlock.setSettlementDate(overrideSettlementDate);
				orderTradeList.forEach(orderTrade -> orderTrade.setSettlementDate(overrideSettlementDate));
			}
		}

		if (overrideExecutionPrice != null) {
			updatePlacement = true;
			orderPlacement.setAverageUnitPrice(overrideExecutionPrice);
			placementAllocationList.forEach(orderPlacementAllocation -> orderPlacementAllocation.setPrice(overrideExecutionPrice));
			if (orderBlock.getSecurity().isCurrency()) {
				orderAllocationAmendmentMap.values().forEach(orderAllocationAmendmentCommand -> orderAllocationAmendmentCommand.setAmendExchangeRateToSettlementCurrency(overrideExecutionPrice));
			}
			else {
				orderAllocationAmendmentMap.values().forEach(orderAllocationAmendmentCommand -> orderAllocationAmendmentCommand.setAmendAverageUnitPrice(overrideExecutionPrice));
			}

			// Since we are assuming only one trade (single placement and fully filled), we can just update this.
			// Will need to recalculate later with support of multiple placements
			orderTradeList = getOrderExecutionHandler().calculateOrderTradeListForPlacement(orderPlacement, placementAllocationList, orderTradeList);
		}

		// Save the updates
		if (!CollectionUtils.isEmpty(orderAllocationAmendmentMap)) {
			Map<Short, WorkflowTransition> amendExecutionWorkflowTransitionMap = getAmendExecutionWorkflowTransitionMap();
			for (Map.Entry<OrderAllocation, OrderAllocationAmendmentCommand> orderAllocationAmendmentEntry : orderAllocationAmendmentMap.entrySet()) {
				WorkflowTransition transition = amendExecutionWorkflowTransitionMap.get(orderAllocationAmendmentEntry.getKey().getWorkflowState().getId());
				ValidationUtils.assertNotNull(transition, "Missing transition for Amend Execution from workflow state " + orderAllocationAmendmentEntry.getKey().getWorkflowState().getName());
				getOrderAllocationService().saveOrderAllocationWithAmendments(orderAllocationAmendmentEntry.getValue(), transition.getId());
			}
		}
		// Would only need to update the block order if the dates change
		if (updateBlockOrder) {
			getOrderBlockService().saveOrderBlock(orderBlock);
		}
		if (updatePlacement) {
			getOrderExecutionService().saveOrderPlacementWithAllocations(orderPlacement, placementAllocationList);
		}
		getOrderTradeService().saveOrderTradeList(orderTradeList);
		getOrderNoteService().saveOrderPlacementNote(orderPlacement.getId(), AMENDMENT_REASON_NOTE_TYPE_NAME, command.getAmendmentReasonNote());

		// Option to resend batches affected?
	}


	@Override
	protected void validateCommand(OrderManagementExecutionActionCommand command, OrderPlacement orderPlacement) {
		ValidationUtils.assertNotEmpty(command.getAmendmentReasonNote(), "Amendment Reason is required.");

		Date overrideTradeDate = getValueOverride(orderPlacement.getOrderBlock().getTradeDate(), command.getOverrideTradeDate());
		Date overrideSettleDate = getValueOverride(orderPlacement.getOrderBlock().getSettlementDate(), command.getOverrideSettlementDate());
		// Capture the list of exceptions to return all - this way by ignoring the date exception, we don't accidentally also ignore the price validation
		UserIgnorableValidationException dateException = null;
		if (overrideTradeDate != null || overrideSettleDate != null) {
			overrideTradeDate = ObjectUtils.coalesce(overrideTradeDate, orderPlacement.getOrderBlock().getTradeDate());
			overrideSettleDate = ObjectUtils.coalesce(overrideSettleDate, orderPlacement.getOrderBlock().getSettlementDate());
			try {
				getOrderSharedUtilHandler().validateTradeAndSettlementDateForSecurity(orderPlacement.getOrderBlock().getSecurity(), orderPlacement.getOrderBlock().getSettlementCurrency(), overrideTradeDate, overrideSettleDate, command.isIgnoreValidation());
			}
			catch (UserIgnorableValidationException e) {
				dateException = e;
			}
		}
		if (!command.isIgnoreValidation() && getValueOverride(orderPlacement.getAverageUnitPrice(), command.getOverrideAverageUnitPrice()) != null) {
			try {
				getOrderExecutionHandler().validateOrderExecutionPrice(orderPlacement, getValueOverride(orderPlacement.getAverageUnitPrice(), command.getWarnPriceThreshold()), command.getWarnPriceThreshold());
			}
			catch (UserIgnorableValidationException e) {
				if (dateException != null) {
					e.setConfirmationMessage(dateException.getMessage() + "; AND " + e.getMessage());
				}
				throw e;
			}
		}
		if (dateException != null) {
			throw dateException;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected <T> T getValueOverride(T currentValue, T overrideValue) {
		if (overrideValue != null && !CompareUtils.isEqualWithSmartLogic(currentValue, overrideValue)) {
			return overrideValue;
		}
		return null;
	}


	protected List<OrderTrade> getOrderTradeListForOrderAllocationList(List<OrderAllocation> orderAllocationList) {
		OrderTradeSearchForm orderTradeSearchForm = new OrderTradeSearchForm();
		orderTradeSearchForm.setOrderAllocationIdentifiers(BeanUtils.getBeanIdentityArray(orderAllocationList, Long.class));
		return getOrderTradeService().getOrderTradeList(orderTradeSearchForm);
	}


	protected Map<Short, WorkflowTransition> getAmendExecutionWorkflowTransitionMap() {
		WorkflowTransitionSearchCommand cmd = WorkflowTransitionSearchCommand.of(OrderAllocation.WORKFLOW_NAME, "Amend Execution");
		return BeanUtils.getBeanMap(getWorkflowTransitionService().getWorkflowTransitionListForCommand(cmd), workflowTransition -> workflowTransition.getStartWorkflowState().getId());
	}
}
