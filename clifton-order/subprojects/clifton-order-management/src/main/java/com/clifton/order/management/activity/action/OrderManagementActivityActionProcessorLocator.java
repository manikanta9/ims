package com.clifton.order.management.activity.action;


public interface OrderManagementActivityActionProcessorLocator {

	/**
	 * Returns OrderManagementActivityAction for the specified orderManagementActivityAction
	 */
	public OrderManagementActivityActionProcessor locate(OrderManagementActivityActions orderManagementActivityAction);
}
