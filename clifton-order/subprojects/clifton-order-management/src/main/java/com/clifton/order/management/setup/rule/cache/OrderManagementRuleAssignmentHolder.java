package com.clifton.order.management.setup.rule.cache;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.order.management.setup.rule.OrderManagementRuleAssignment;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>OrderManagementRuleAssignmentHolder</code> is used to cache rules assignments assigned to accounts.  It's a lightweight object that is used to build out the actual extended assignments from the cache.
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleAssignmentHolder extends BaseSimpleEntity<Integer> implements Serializable {

	private Integer clientAccountId;

	private Integer holdingAccountId;

	private Integer holdingAccountIssuingCompanyId;

	private Short ruleCategoryId;

	private boolean rollup;

	private Date startDate;

	private Date endDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static OrderManagementRuleAssignmentHolder forRuleAssignment(OrderManagementRuleAssignment ruleAssignment) {
		OrderManagementRuleAssignmentHolder ruleAssignmentHolder = new OrderManagementRuleAssignmentHolder();
		ruleAssignmentHolder.setId(BeanUtils.getBeanIdentity(ruleAssignment));
		ruleAssignmentHolder.setClientAccountId(BeanUtils.getBeanIdentity(ruleAssignment.getClientAccount()));
		ruleAssignmentHolder.setHoldingAccountId(BeanUtils.getBeanIdentity(ruleAssignment.getHoldingAccount()));
		ruleAssignmentHolder.setHoldingAccountIssuingCompanyId(BeanUtils.getBeanIdentity(ruleAssignment.getHoldingAccountIssuingCompany()));
		ruleAssignmentHolder.setRuleCategoryId(BeanUtils.getBeanIdentity(ruleAssignment.getRuleDefinition().getRuleType().getRuleCategory()));
		ruleAssignmentHolder.setRollup(ruleAssignment.getRuleDefinition().isRollup());
		ruleAssignmentHolder.setStartDate(ruleAssignment.getStartDate());
		ruleAssignmentHolder.setEndDate(ruleAssignment.getEndDate());
		return ruleAssignmentHolder;
	}
}
