package com.clifton.order.management.setup.rule.cache;

import java.util.Date;
import java.util.Set;


/**
 * The <code>OrderManagementRuleAssignmentHolderCache</code> is a cache of a OrderManagementRuleAssignmentHolder objects which are lightweight copies of the {@link com.clifton.order.management.setup.rule.OrderManagementRuleAssignment} object
 * This cache is used during rule execution only to build and return the list of applicable assignments we need for rule executing for a given account/
 * <p>
 * Note: The cache utilizes cache keys based on rule assignment scope and when returning includes all for the associated scopes.
 * Global Cache Key + Holding Account Issuer Key + Client / Holding Account Key
 *
 * @author manderson
 */
public interface OrderManagementRuleAssignmentHolderCache {

	public Set<OrderManagementRuleAssignmentHolder> getOrderManagementRuleAssignmentHolderSetForAccount(int clientAccountId, int holdingAccountId, int holdingAccountIssuingCompanyId, Date activeOnDate);
}
