package com.clifton.order.management.setup.rule.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinitionRollup;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class OrderManagementRuleDefinitionRollupListByParentCache extends SelfRegisteringSingleKeyDaoListCache<OrderManagementRuleDefinitionRollup, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Integer getBeanKeyValue(OrderManagementRuleDefinitionRollup bean) {
		if (bean.getReferenceOne() != null) {
			return bean.getReferenceOne().getId();
		}
		return null;
	}
}
