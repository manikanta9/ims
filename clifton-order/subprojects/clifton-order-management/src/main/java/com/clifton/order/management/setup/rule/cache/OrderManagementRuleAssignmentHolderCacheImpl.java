package com.clifton.order.management.setup.rule.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.management.setup.rule.OrderManagementRuleAssignment;
import com.clifton.order.management.setup.rule.OrderManagementRuleAssignmentScopes;
import com.clifton.order.management.setup.rule.OrderManagementRuleService;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleAssignmentSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementRuleAssignmentHolderCacheImpl extends SelfRegisteringSimpleDaoCache<OrderManagementRuleAssignment, String, Set<OrderManagementRuleAssignmentHolder>> implements OrderManagementRuleAssignmentHolderCache {

	private OrderManagementRuleService orderManagementRuleService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<OrderManagementRuleAssignmentHolder> getOrderManagementRuleAssignmentHolderSetForAccount(int clientAccountId, int holdingAccountId, int holdingAccountIssuingCompanyId, Date activeOnDate) {
		Set<OrderManagementRuleAssignmentHolder> ruleAssignmentHolderSet = new HashSet<>();
		// Global Assignments
		ruleAssignmentHolderSet.addAll(getRuleAssignmentHolderSet(OrderManagementRuleAssignmentScopes.GLOBAL, clientAccountId, holdingAccountId, holdingAccountIssuingCompanyId));
		// Add all Holding Account Issuer Assignments
		ruleAssignmentHolderSet.addAll(getRuleAssignmentHolderSet(OrderManagementRuleAssignmentScopes.HOLDING_ACCOUNT_ISSUER, clientAccountId, holdingAccountId, holdingAccountIssuingCompanyId));
		// Add all Account Assignments
		ruleAssignmentHolderSet.addAll(getRuleAssignmentHolderSet(OrderManagementRuleAssignmentScopes.ACCOUNT, clientAccountId, holdingAccountId, holdingAccountIssuingCompanyId));

		// Filter based on category and active on date
		return CollectionUtils.getStream(ruleAssignmentHolderSet).filter(ruleAssignmentHolder -> {
			if (DateUtils.isDateBetween(activeOnDate, ruleAssignmentHolder.getStartDate(), ruleAssignmentHolder.getEndDate(), false)) {
				return true;
			}
			return false;
		}).collect(Collectors.toSet());
	}


	protected Set<OrderManagementRuleAssignmentHolder> getRuleAssignmentHolderSet(OrderManagementRuleAssignmentScopes ruleScope, int clientAccountId, int holdingAccountId, int holdingAccountIssuingCompanyId) {
		String key = getCacheKey(ruleScope, clientAccountId, holdingAccountId, holdingAccountIssuingCompanyId);
		Set<OrderManagementRuleAssignmentHolder> result = getCacheHandler().get(getCacheName(), key);
		if (result == null) {
			OrderManagementRuleAssignmentSearchForm searchForm = new OrderManagementRuleAssignmentSearchForm();
			switch (ruleScope) {
				case GLOBAL:
					searchForm.setGlobalRuleAssignment(true);
					break;
				case HOLDING_ACCOUNT_ISSUER:
					searchForm.setHoldingAccountIssuingCompanyId(holdingAccountIssuingCompanyId);
					break;
				case ACCOUNT:
					searchForm.setClientAccountId(clientAccountId);
					searchForm.setHoldingAccountId(holdingAccountId);
					break;
				default:
					throw new IllegalStateException("Unknown search form configuration for rule scope " + ruleScope.name());
			}

			List<OrderManagementRuleAssignment> assignmentList = getOrderManagementRuleService().getOrderManagementRuleAssignmentList(searchForm);
			Set<OrderManagementRuleAssignmentHolder> finalResult = CollectionUtils.getStream(assignmentList).map(OrderManagementRuleAssignmentHolder::forRuleAssignment).collect(Collectors.toSet());
			getCacheHandler().put(getCacheName(), key, finalResult);
			return finalResult;
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderManagementRuleAssignment> dao, DaoEventTypes event, OrderManagementRuleAssignment bean) {
		if (event.isUpdate()) {
			// Get original bean so we have it for after update cache clear check
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<OrderManagementRuleAssignment> dao, DaoEventTypes event, OrderManagementRuleAssignment bean, Throwable e) {
		if (e == null) {
			// If it's an insert or a delete - clear the cache for the key
			if (event.isInsert() || event.isDelete()) {
				getCacheHandler().remove(getCacheName(), getCacheKey(bean));
			}
			else {
				OrderManagementRuleAssignment originalBean = getOriginalBean(dao, bean);
				if (originalBean != null) {
					OrderManagementRuleAssignmentHolder originalHolder = OrderManagementRuleAssignmentHolder.forRuleAssignment(originalBean);
					OrderManagementRuleAssignmentHolder newHolder = OrderManagementRuleAssignmentHolder.forRuleAssignment(bean);
					// If any of the fields we track on the holder object changed - clear the cache for old and new keys - otherwise we can keep the cache as is
					if (!CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(originalHolder, newHolder, false))) {
						getCacheHandler().remove(getCacheName(), getCacheKey(originalBean));
						getCacheHandler().remove(getCacheName(), getCacheKey(bean));
					}
				}
			}
		}
	}


	private String getCacheKey(OrderManagementRuleAssignment ruleAssignment) {
		return getCacheKey(ruleAssignment.getRuleScope(), BeanUtils.getBeanIdentity(ruleAssignment.getClientAccount()), BeanUtils.getBeanIdentity(ruleAssignment.getHoldingAccount()), BeanUtils.getBeanIdentity(ruleAssignment.getHoldingAccountIssuingCompany()));
	}


	private String getCacheKey(OrderManagementRuleAssignmentScopes ruleScope, Integer clientAccountId, Integer holdingAccountId, Integer holdingAccountIssuingCompanyId) {
		switch (ruleScope) {
			case GLOBAL:
				return OrderManagementRuleAssignmentScopes.GLOBAL.name();
			case HOLDING_ACCOUNT_ISSUER:
				return StringUtils.generateKey(OrderManagementRuleAssignmentScopes.HOLDING_ACCOUNT_ISSUER.name(), holdingAccountIssuingCompanyId);
			case ACCOUNT:
				return StringUtils.generateKey(OrderManagementRuleAssignmentScopes.ACCOUNT.name(), clientAccountId, holdingAccountId);
			default:
				throw new IllegalStateException("Unknown cache key for rule scope " + ruleScope.name());
		}
	}
}

