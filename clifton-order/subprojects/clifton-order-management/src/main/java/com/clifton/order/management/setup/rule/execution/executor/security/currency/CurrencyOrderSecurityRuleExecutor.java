package com.clifton.order.management.setup.rule.execution.executor.security.currency;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionUtils;
import com.clifton.order.management.setup.rule.execution.executor.security.BasicOrderSecurityRuleExecutor;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class CurrencyOrderSecurityRuleExecutor extends BasicOrderSecurityRuleExecutor implements OrderManagementCurrencyRuleExecutor {

	private static final String CURRENCY_11A_REQUIRED_COLUMN_NAME = "Require 11A";
	//private static final String CURRENCY_CAN_NET_THROUGH_11A_N0 = "Can Net Through 11A NO";
	//private static final String CURRENCY_CAN_USE_LOCAL_BALANCE = "Can Use Local Accounting Balance";


	private Short currency11ARequiredOrderTypeId;


	@Override
	public boolean executeRule(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		if (isProhibited()) {
			return super.executeRule(ruleAssignment, orderRuleAware, context);
		}

		// If it's not already a currency 11A order type - confirm that 11A isn't required - if it is - fail
		if (!MathUtils.isEqual(getCurrency11ARequiredOrderTypeId(), BeanUtils.getBeanIdentity(orderRuleAware.getOrderType()))) {
			if (isCurrency11ARequired(ruleAssignment, orderRuleAware, context)) {
				return false;
			}
		}
		return true;
	}


	@Override
	public boolean isCurrency11ARequired(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		return BooleanUtils.isTrue(OrderManagementRuleExecutionUtils.getRuleAssignmentCustomFieldValue(ruleAssignment, CURRENCY_11A_REQUIRED_COLUMN_NAME));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected boolean isCurrencyRule() {
		return true;
	}
}
