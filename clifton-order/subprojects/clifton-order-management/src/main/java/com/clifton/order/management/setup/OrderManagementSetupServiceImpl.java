package com.clifton.order.management.setup;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchCommand;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchForm;
import com.clifton.order.setup.destination.OrderDestination;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderManagementSetupServiceImpl implements OrderManagementSetupService {

	private AdvancedUpdatableDAO<OrderManagementDestinationMapping, Criteria> orderManagementDestinationMappingDAO;

	private DaoSingleKeyListCache<OrderManagementDestinationMapping, Short> orderManagementDestinationMappingByOrderTypeCache;

	////////////////////////////////////////////////////////////////////////////
	////////           Order Management Destination Mapping             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderManagementDestinationMapping getOrderManagementDestinationMapping(int id) {
		return getOrderManagementDestinationMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderManagementDestinationMapping> getOrderManagementDestinationMappingList(OrderManagementDestinationMappingSearchForm searchForm) {
		return getOrderManagementDestinationMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<OrderManagementDestinationMapping> getOrderManagementDestinationMappingListForCommand(OrderManagementDestinationMappingSearchCommand command) {

		ValidationUtils.assertNotNull(command, "Command object is null.");
		ValidationUtils.assertNotNull(command.getOrderTypeId(), "Order Type is required.");
		List<OrderManagementDestinationMapping> orderManagementDestinationMappings = getOrderManagementDestinationMappingByOrderTypeCache().getBeanListForKeyValue(getOrderManagementDestinationMappingDAO(), command.getOrderTypeId());
		//filtering for active on date, executing broker and destination to further narrow the list during look ups
		return Optional.ofNullable(orderManagementDestinationMappings)
				.orElseGet(Collections::emptyList).stream()
				.filter(orderManagementDestinationMapping -> command.getActiveOnDate() == null || DateUtils.isDateBetween(command.getActiveOnDate(), orderManagementDestinationMapping.getStartDate(), orderManagementDestinationMapping.getEndDate(), false))
				.filter(orderManagementDestinationMapping -> command.getOrderDestinationId() == null || CompareUtils.isEqual(orderManagementDestinationMapping.getOrderDestination().getId(), command.getOrderDestinationId()))
				.filter(orderManagementDestinationMapping -> command.getExecutingBrokerCompanyIdOrNull() == null || orderManagementDestinationMapping.getExecutingBrokerCompany() == null || CompareUtils.isEqual(orderManagementDestinationMapping.getExecutingBrokerCompany().getId(), command.getExecutingBrokerCompanyIdOrNull()))
				.filter(orderManagementDestinationMapping -> command.getExecutingBrokerCompanyIdsOrNull() == null || orderManagementDestinationMapping.getExecutingBrokerCompany() == null || ArrayUtils.contains(command.getExecutingBrokerCompanyIdsOrNull(), orderManagementDestinationMapping.getExecutingBrokerCompany().getId()))
				.filter(orderManagementDestinationMapping -> command.getExecutingBrokerCompanyName() == null || (orderManagementDestinationMapping.getExecutingBrokerCompany() != null && StringUtils.isEqualIgnoreCase(orderManagementDestinationMapping.getExecutingBrokerCompany().getName(), command.getExecutingBrokerCompanyName())))
				.collect(Collectors.toList());
	}


	/**
	 * @see com.clifton.order.management.setup.validation.OrderManagementDestinationMappingValidator
	 */
	@Override
	public OrderManagementDestinationMapping saveOrderManagementDestinationMapping(OrderManagementDestinationMapping bean) {
		// TODO CAN WE VALIDATE THE EXECUTING BROKER COMPANY, IF SELECTED HAS THE TAG FOR BROKER ?
		return getOrderManagementDestinationMappingDAO().save(bean);
	}


	@Override
	public void deleteOrderManagementDestinationMapping(int id) {
		// TODO ADD VALIDATION IF EVER USED, MUST END IT?  OR NEVER ALL DELETING, AND ONLY ENDING?
		getOrderManagementDestinationMappingDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderDestination> getOrderManagementDestinationListForMapping(OrderManagementDestinationMappingSearchCommand command) {
		// Active On Date (Trade Date), Order Type, and Executing Broker or Brokers (Optional) - get from cache
		List<OrderManagementDestinationMapping> destinationMappingList = getOrderManagementDestinationMappingListForCommand(command);
		return Arrays.asList(BeanUtils.getPropertyValuesUniqueExcludeNull(destinationMappingList, OrderManagementDestinationMapping::getOrderDestination, OrderDestination.class));
	}
}
