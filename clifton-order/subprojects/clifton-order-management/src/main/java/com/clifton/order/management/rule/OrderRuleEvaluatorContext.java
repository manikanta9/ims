package com.clifton.order.management.rule;

import com.clifton.order.management.setup.rule.OrderManagementRuleDefinition;
import com.clifton.order.management.setup.rule.execution.executor.OrderManagementRuleExecutor;
import com.clifton.order.management.setup.rule.specificity.OrderManagementSpecificityRuleFilter;
import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.util.HashMap;
import java.util.Map;


/**
 * The <code>OrderRuleEvaluatorContext</code> class is used by all {@link com.clifton.order.rule.OrderRuleAware} Rule evaluation and contains common information (easily stored/retrieved from context) and re-used across various rule evaluators
 *
 * @author manderson
 */
public class OrderRuleEvaluatorContext extends BaseRuleEvaluatorContext {


	/**
	 * Map of SystemBeanID -> Bean Instance of {@Link OrderManagementRuleExecutor}
	 */
	private final Map<Integer, OrderManagementRuleExecutor> ruleExecutorBeanInstanceMap = new HashMap<>();

	/**
	 * Map of SystemBeanID -> Bean Instance of {@Link OrderManagementSpecificityRuleFilter}
	 */
	private final Map<Integer, OrderManagementSpecificityRuleFilter> ruleSpecificityBeanInstanceMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderManagementRuleExecutor getOrderManagementRuleExecutor(OrderManagementRuleDefinition ruleDefinition, SystemBeanService systemBeanService) {
		return this.ruleExecutorBeanInstanceMap.computeIfAbsent(ruleDefinition.getRuleType().getRuleExecutorBean().getId(), key -> (OrderManagementRuleExecutor) systemBeanService.getBeanInstance(ruleDefinition.getRuleType().getRuleExecutorBean()));
	}


	public OrderManagementSpecificityRuleFilter getOrderManagementRuleFilter(SystemBean systemBean, SystemBeanService systemBeanService) {
		return this.ruleSpecificityBeanInstanceMap.computeIfAbsent(systemBean.getId(), key -> (OrderManagementSpecificityRuleFilter) systemBeanService.getBeanInstance(systemBean));
	}
}
