package com.clifton.order.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.block.OrderBlockService;
import com.clifton.order.trade.OrderTradeService;
import com.clifton.order.trade.search.OrderTradeSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderAllocationRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private OrderBlockService orderBlockService;

	private OrderTradeService orderTradeService;

	private RuleViolationService ruleViolationService;

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof OrderAllocation;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		OrderAllocation orderAllocation = (OrderAllocation) entity;
		if (orderAllocation != null && orderAllocation.getOrderBlockIdentifier() != null) {
			relatedEntityList.add(getOrderBlockService().getOrderBlock(orderAllocation.getOrderBlockIdentifier(), false));
			// Only makes sense to look up trades if the block is set
			// Note this is the only link to trades and it's a soft link
			OrderTradeSearchForm tradeSearchForm = new OrderTradeSearchForm();
			tradeSearchForm.setOrderAllocationIdentifier(orderAllocation.getId());
			relatedEntityList.addAll(getOrderTradeService().getOrderTradeList(tradeSearchForm));
		}

		// Add in Rule Violations - should be a more generic way to do this (check tables for rule categories?)
		AssertUtils.assertNotNull(orderAllocation, "Unexpected state. Null pointer when dereferencing variable [orderAllocation].");
		List<RuleViolation> ruleViolations = getRuleViolationService().getRuleViolationListByLinkedEntity(OrderAllocation.TABLE_NAME, orderAllocation.getId());
		if (!CollectionUtils.isEmpty(ruleViolations)) {
			relatedEntityList.addAll(ruleViolations);
		}

		// If part of a group, add it
		if (orderAllocation.getOrderGroup() != null) {
			relatedEntityList.add(orderAllocation.getOrderGroup());
		}

		// Notes - should be a more generic way to do this (check tables for note types?)
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(OrderAllocation.TABLE_NAME);
		noteSearchForm.setFkFieldId(orderAllocation.getId());
		// Not adding for linked entities - could be messy if we have notes on accounts
		relatedEntityList.addAll(getSystemNoteService().getSystemNoteListForEntity(noteSearchForm));

		return relatedEntityList;
	}
}
