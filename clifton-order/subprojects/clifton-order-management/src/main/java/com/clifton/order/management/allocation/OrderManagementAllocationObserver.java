package com.clifton.order.management.allocation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.management.setup.OrderManagementSetupService;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionService;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchCommand;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderCompany;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>OrderManagementAllocationObserver</code> observes inserts of new {@link OrderAllocation} entries.
 * If executing broker and/or order destination are missing, and only one is allowed will default it automatically.
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementAllocationObserver extends SelfRegisteringDaoObserver<OrderAllocation> {

	private OrderManagementRuleExecutionService orderManagementRuleExecutionService;
	private OrderManagementSetupService orderManagementSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.INSERT;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderAllocation> dao, DaoEventTypes event, OrderAllocation bean) {
		if (event.isInsert()) {
			List<OrderCompany> executingBrokerCompanyList = null;
			if (bean.getExecutingBrokerCompany() == null) {
				executingBrokerCompanyList = getOrderManagementRuleExecutionService().getOrderManagementRuleExecutingBrokerCompanyListForOrderRuleAware(bean, BeanUtils.getBeanIdentity(bean.getOrderDestination()));
				if (CollectionUtils.getSize(executingBrokerCompanyList) == 1) {
					bean.setExecutingBrokerCompany(executingBrokerCompanyList.get(0));
				}
			}
			if (bean.getOrderDestination() == null && (bean.getExecutingBrokerCompany() != null || !CollectionUtils.isEmpty(executingBrokerCompanyList))) {
				OrderManagementDestinationMappingSearchCommand command = OrderManagementDestinationMappingSearchCommand.forOrderTypeAndExecutingBrokerIdsOrNullAndActiveOnDate(bean.getOrderType().getId(), BeanUtils.getBeanIdentity(bean.getExecutingBrokerCompany()), BeanUtils.getBeanIdentityArray(executingBrokerCompanyList, Integer.class), bean.getTradeDate());
				List<OrderDestination> destinationList = getOrderManagementSetupService().getOrderManagementDestinationListForMapping(command);
				if (CollectionUtils.getSize(destinationList) == 1) {
					bean.setOrderDestination(destinationList.get(0));
				}
			}
		}
	}
}
