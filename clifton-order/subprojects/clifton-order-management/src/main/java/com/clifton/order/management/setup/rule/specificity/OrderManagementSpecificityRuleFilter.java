package com.clifton.order.management.setup.rule.specificity;

import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;

import java.util.List;


/**
 * The <code>OrderManagementSpecificityRuleFilter</code> interface defines the method to apply specificity filtering for a list of rule assignments
 * when more than one applies to find the most specific one.
 * <p>
 * Filters are applied to the list that use the same specificity filter bean and are evaluated against the specific properties that are available.
 * For example: Security Allowed and Prohibited rule types use the same specificity filter.  If there is a rule that applies to all CCY, and a second rule that applies just to AUD, when
 * evaluating rules for AUD - we can discard the all CCY rule because the AUD specific rule will win.
 *
 * @author manderson
 */
public interface OrderManagementSpecificityRuleFilter {


	public List<OrderManagementRuleAssignmentExtended> applySpecificityFiltering(List<OrderManagementRuleAssignmentExtended> assignmentExtendedList, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context);
}
