package com.clifton.order.management.activity.action;

import com.clifton.order.management.activity.OrderManagementActivityCommand;


/**
 * @author manderson
 */
public interface OrderManagementActivityActionProcessor {


	public OrderManagementActivityActions getOrderManagementActivityAction();


	public void processOrderAllocationActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context);


	public void processOrderBlockActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context);
}
