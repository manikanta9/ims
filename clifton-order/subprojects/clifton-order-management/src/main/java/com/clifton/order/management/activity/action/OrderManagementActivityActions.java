package com.clifton.order.management.activity.action;

/**
 * @author manderson
 */
public enum OrderManagementActivityActions {


	ASSIGN_TRADER,
	REVISE_DATES,
	CREATE_BLOCK_ORDERS,
	UNBLOCK_ORDERS,
	CREATE_PLACEMENTS,
	SEND_PLACEMENTS
}
