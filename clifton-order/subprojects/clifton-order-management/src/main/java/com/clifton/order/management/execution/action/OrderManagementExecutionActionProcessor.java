package com.clifton.order.management.execution.action;

import com.clifton.order.setup.OrderExecutionActions;

import java.util.Set;


/**
 * @author manderson
 */
public interface OrderManagementExecutionActionProcessor {

	public Set<OrderExecutionActions> getOrderExecutionActionSet();


	public void processExecutionAction(Long orderPlacementId, OrderManagementExecutionActionCommand command);
}
