package com.clifton.order.management.execution.action.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import org.springframework.stereotype.Component;

import java.util.Set;


/**
 * @author manderson
 */
@Component
public class OrderManagementExecutionSendCancelRequestActionProcessor extends BaseOrderManagementExecutionActionProcessor {


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.CANCEL_REQUEST);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		if (orderPlacement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FIX)) {
			getOrderFixService().sendCancelRequestOrderPlacement(orderPlacement);
		}
		else {
			throw new ValidationException("Sending Order Placement Cancel Requests for " + orderPlacement.getOrderDestination().getDestinationType().getName() + " is not currently supported");
		}
	}
}
