package com.clifton.order.management.setup.rule.execution;

import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinition;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;


/**
 * @author manderson
 */
public class OrderManagementRuleExecutionUtils {

	private static final String SECURITY_TYPE_CUSTOM_FIELD_NAME = "Security Type";
	private static final String SECURITY_HIERARCHY_CUSTOM_FIELD_NAME = "Security Hierarchy";
	private static final String PRIMARY_EXCHANGE_CUSTOM_FIELD_NAME = "Primary Exchange";
	private static final String PRIMARY_EXCHANGE_COUNTRY_CUSTOM_FIELD_NAME = "Primary Exchange Country";
	private static final String SECURITY_CUSTOM_FIELD_NAME = "Security";
	private static final String EXECUTING_BROKER_CUSTOM_FIELD_NAME = "Executing Broker";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static Integer getRuleAssignmentSecurityType(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		return getValueAsInteger(getRuleAssignmentCustomFieldValue(ruleAssignmentExtended, SECURITY_TYPE_CUSTOM_FIELD_NAME));
	}


	public static Short getRuleAssignmentSecurityHierarchy(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		return getValueAsShort(getRuleAssignmentCustomFieldValue(ruleAssignmentExtended, SECURITY_HIERARCHY_CUSTOM_FIELD_NAME));
	}


	public static Integer getRuleAssignmentPrimaryExchange(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		return getValueAsInteger(getRuleAssignmentCustomFieldValue(ruleAssignmentExtended, PRIMARY_EXCHANGE_CUSTOM_FIELD_NAME));
	}


	public static Integer getRuleAssignmentPrimaryExchangeCountry(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		return getValueAsInteger(getRuleAssignmentCustomFieldValue(ruleAssignmentExtended, PRIMARY_EXCHANGE_COUNTRY_CUSTOM_FIELD_NAME));
	}


	public static Integer getRuleAssignmentSecurity(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		return getValueAsInteger(getRuleAssignmentCustomFieldValue(ruleAssignmentExtended, SECURITY_CUSTOM_FIELD_NAME));
	}


	public static Integer getRuleAssignmentExecutingBroker(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		return getValueAsInteger(getRuleAssignmentCustomFieldValue(ruleAssignmentExtended, EXECUTING_BROKER_CUSTOM_FIELD_NAME));
	}


	public static Object getRuleAssignmentCustomFieldValue(OrderManagementRuleAssignmentExtended ruleAssignmentExtended, String customFieldName) {
		OrderManagementRuleDefinition ruleDefinition = ruleAssignmentExtended.getRuleDefinition();
		Object value = getRuleDefinitionCustomFieldValue(ruleDefinition, customFieldName);
		if (value == null && ruleAssignmentExtended.getRollupRuleDefinition() != null) {
			value = getRuleDefinitionCustomFieldValue(ruleAssignmentExtended.getRollupRuleDefinition(), customFieldName);
		}
		return value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static Object getRuleDefinitionCustomFieldValue(OrderManagementRuleDefinition ruleDefinition, String customFieldName) {
		return CustomJsonStringUtils.getColumnValue(ruleDefinition.getCustomColumns(), customFieldName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private static Short getValueAsShort(Object value) {
		if (value != null) {
			if (value instanceof String) {
				return Short.parseShort((String) value);
			}
			else if (value instanceof Number) {
				return ((Number) value).shortValue();
			}
		}
		return null;
	}


	private static Integer getValueAsInteger(Object value) {
		if (value != null) {
			if (value instanceof String) {
				return Integer.parseInt((String) value);
			}
			else if (value instanceof Number) {
				return ((Number) value).intValue();
			}
		}
		return null;
	}
}
