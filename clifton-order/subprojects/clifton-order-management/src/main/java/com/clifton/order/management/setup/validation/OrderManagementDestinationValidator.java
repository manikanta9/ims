package com.clifton.order.management.setup.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.management.setup.OrderManagementSetupService;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchForm;
import com.clifton.order.setup.destination.OrderDestination;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementDestinationValidator extends SelfRegisteringDaoValidator<OrderDestination> {

	private OrderManagementSetupService orderManagementSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	public void validate(OrderDestination bean, DaoEventTypes config) throws ValidationException {
		OrderDestination originalBean = config.isUpdate() ? getOriginalBean(bean) : null;
		if (originalBean != null) {
			if (!originalBean.isBackupDestination() && bean.isBackupDestination()) {
				ValidationUtils.assertFalse(isMappingsForDestinationExist(bean, false), () -> "Cannot change " + bean.getName() + " to a back up because there are destination mappings associated with it.  Destination mappings can only be applied to main destinations that are executing venues.");
			}

			if (originalBean.getDestinationType().isExecutionVenue() && !bean.getDestinationType().isExecutionVenue()) {
				ValidationUtils.assertFalse(isMappingsForDestinationExist(bean, false), () -> "Cannot change " + bean.getName() + " to from an execution venue because there are destination mappings associated with it.  Destination mappings can only be applied to main destinations that are executing venues.");
			}

			if (!originalBean.isDisabled() && bean.isDisabled() && bean.getDestinationType().isExecutionVenue() && !bean.isBackupDestination()) {
				ValidationUtils.assertFalse(isMappingsForDestinationExist(bean, true), () -> "Cannot disable " + bean.getName() + " because there are still active destination mappings associated with it.");
			}
		}
	}


	private boolean isMappingsForDestinationExist(OrderDestination bean, boolean activeOnly) {
		OrderManagementDestinationMappingSearchForm searchForm = new OrderManagementDestinationMappingSearchForm();
		searchForm.setOrderDestinationId(bean.getId());
		if (activeOnly) {
			searchForm.setActive(true);
		}
		return !CollectionUtils.isEmpty(getOrderManagementSetupService().getOrderManagementDestinationMappingList(searchForm));
	}
}
