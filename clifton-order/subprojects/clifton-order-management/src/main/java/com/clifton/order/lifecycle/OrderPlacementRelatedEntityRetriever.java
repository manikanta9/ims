package com.clifton.order.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.order.batch.OrderBatchService;
import com.clifton.order.batch.search.OrderBatchItemSearchForm;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.search.OrderPlacementAllocationSearchForm;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderPlacementRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private OrderBatchService orderBatchService;

	private OrderExecutionService orderExecutionService;

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof OrderPlacement;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		OrderPlacement orderPlacement = (OrderPlacement) entity;

		// Add Order Placement Allocations
		OrderPlacementAllocationSearchForm searchForm = new OrderPlacementAllocationSearchForm();
		searchForm.setOrderPlacementId(orderPlacement.getId());
		List<IdentityObject> relatedEntityList = new ArrayList<>(getOrderExecutionService().getOrderPlacementAllocationList(searchForm));

		// If the placement uses batching, include the batches
		if (orderPlacement.getOrderDestination().getDestinationType().isBatched()) {
			OrderBatchItemSearchForm batchItemSearchForm = new OrderBatchItemSearchForm();
			batchItemSearchForm.setTableName(OrderPlacement.TABLE_NAME);
			batchItemSearchForm.setFkFieldId(orderPlacement.getId());
			relatedEntityList.addAll(getOrderBatchService().getOrderBatchItemList(batchItemSearchForm));
		}

		// Notes - should be a more generic way to do this (check tables for note types?)
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(OrderPlacement.TABLE_NAME);
		noteSearchForm.setFkFieldId(orderPlacement.getId());
		// Not adding for linked entities - could be messy if we have notes on accounts
		relatedEntityList.addAll(getSystemNoteService().getSystemNoteListForEntity(noteSearchForm));

		return relatedEntityList;
	}
}
