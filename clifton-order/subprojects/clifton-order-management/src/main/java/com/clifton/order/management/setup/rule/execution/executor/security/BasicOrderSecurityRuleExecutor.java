package com.clifton.order.management.setup.rule.execution.executor.security;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionUtils;
import com.clifton.order.management.setup.rule.execution.executor.BaseOrderManagementRuleExecutor;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class BasicOrderSecurityRuleExecutor extends BaseOrderManagementRuleExecutor {

	/**
	 * Used to flag if the filtering should apply as allowed or prohibited
	 * i.e. Security Type = Currency, Prohibited = true or false
	 */
	private boolean prohibited;


	/**
	 * Optionally used for additional grouping.  i.e. Allow vs. Prohibit Trading for Stocks, but for Restricted Countries we need additional input
	 * so these rules are applied in addition to any standard security allowed/prohibited from trading
	 */
	private String assignmentSpecificityScopeKeyPrefix;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getAssignmentSpecificityScopeKey(OrderManagementRuleAssignmentExtended ruleAssignment) {
		return StringUtils.generateKey(getAssignmentSpecificityScopeKeyPrefix(), getClass().getName(), ruleAssignment.getRuleDefinition().getCustomColumns() == null ? "" : StringUtils.removeAll(ruleAssignment.getRuleDefinition().getCustomColumns().toString(), "\""));
	}


	@Override
	public boolean isAssignmentApplicable(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		if (orderRuleAware.getSecurity() != null) {
			if (isCurrencyRule() != orderRuleAware.getSecurity().isCurrency()) {
				return false;
			}
			if (isRuleValueNotApplyToContext(orderRuleAware.getSecurity().getId(), OrderManagementRuleExecutionUtils.getRuleAssignmentSecurity(ruleAssignment))) {
				return false;
			}
			if (isRuleValueNotApplyToContext(BeanUtils.getBeanIdentity(orderRuleAware.getSecurity().getSecurityHierarchy()), OrderManagementRuleExecutionUtils.getRuleAssignmentSecurityHierarchy(ruleAssignment))) {
				return false;
			}
			if (isRuleValueNotApplyToContext(BeanUtils.getBeanIdentity(orderRuleAware.getSecurity().getPrimaryExchange()), OrderManagementRuleExecutionUtils.getRuleAssignmentPrimaryExchange(ruleAssignment))) {
				return false;
			}
			if (isRuleValueNotApplyToContext(orderRuleAware.getSecurity().getPrimaryExchange() == null ? null : BeanUtils.getBeanIdentity(orderRuleAware.getSecurity().getPrimaryExchange().getCountry()), OrderManagementRuleExecutionUtils.getRuleAssignmentPrimaryExchangeCountry(ruleAssignment))) {
				return false;
			}
			if (isRuleValueNotApplyToContext(BeanUtils.getBeanIdentity(orderRuleAware.getSecurity().getSecurityType()), OrderManagementRuleExecutionUtils.getRuleAssignmentSecurityType(ruleAssignment))) {
				return false;
			}
		}
		else {
			if (isRuleValueNotApplyToContext(BeanUtils.getBeanIdentity(orderRuleAware.getOrderType().getSecurityType()), OrderManagementRuleExecutionUtils.getRuleAssignmentSecurityType(ruleAssignment))) {
				return false;
			}
		}
		return true;
	}


	@Override
	public boolean executeRule(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		return !isProhibited();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Overridden in the Currency Rule executor to return true.
	 */
	protected boolean isCurrencyRule() {
		return false;
	}
}
