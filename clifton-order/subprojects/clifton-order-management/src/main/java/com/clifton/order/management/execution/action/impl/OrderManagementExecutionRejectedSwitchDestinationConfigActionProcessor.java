package com.clifton.order.management.execution.action.impl;

import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import com.clifton.order.setup.destination.OrderDestinationService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Set;


/**
 * The <code>OrderManagementExecutionRejectedSwitchDestinationConfigActionProcessor</code> is used to resend a rejected placement to the same destination but using a different destination configuration.
 * The placement is canceled internally and a new one is generated to send to destination
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementExecutionRejectedSwitchDestinationConfigActionProcessor extends OrderManagementExecutionCancelActionProcessor {


	private ContextHandler contextHandler;

	private OrderDestinationService orderDestinationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.REJECT_CANCEL_SWITCH_DESTINATION_CONFIGURATION);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		OrderDestinationConfiguration newDestinationConfiguration = getOrderDestinationConfiguration(command);

		// If not already in a canceled state, cancel it
		if (!orderPlacement.getExecutionStatus().isCanceled()) {
			orderPlacement.setExecutionStatus(getOrderSetupService().getOrderExecutionStatusByName(OrderExecutionStatus.REJECTED_CANCELED));
		}

		// Create the new Placement, but don't save it until after the canceled one is saved
		OrderPlacement newOrderPlacement = new OrderPlacement();
		newOrderPlacement.setOrderBlock(orderPlacement.getOrderBlock());
		newOrderPlacement.setOrderDestination(orderPlacement.getOrderDestination());
		newOrderPlacement.setOrderDestinationConfiguration(newDestinationConfiguration);
		newOrderPlacement.setExecutingBrokerCompany(orderPlacement.getExecutingBrokerCompany());
		newOrderPlacement.setQuantityIntended(orderPlacement.getQuantityIntended());

		// Cancel the existing placement
		processCancelBatchItems(orderPlacement);
		getOrderExecutionService().saveOrderPlacement(orderPlacement);

		// Save the new Placement
		newOrderPlacement = getOrderExecutionService().saveOrderPlacement(newOrderPlacement);

		// If FIX - automatically send the placement
		if (newOrderPlacement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FIX)) {
			getOrderFixService().sendOrderPlacement(newOrderPlacement, getOrderAllocationListForPlacement(newOrderPlacement), getExecutingBrokerCompanyCodeListForPlacement(newOrderPlacement));
			newOrderPlacement.setExecutionStatus(getOrderSetupService().getOrderExecutionStatusByName(OrderExecutionStatus.SENT));
			getOrderExecutionService().saveOrderPlacement(newOrderPlacement);
		}
	}


	@Override
	protected void validateCommand(OrderManagementExecutionActionCommand command, OrderPlacement orderPlacement) {
		ValidationUtils.assertNotNull(command.getSwitchToDestinationConfigurationId(), "A new destination configuration must be selected to switch to.");
		OrderDestinationConfiguration newDestinationConfiguration = getOrderDestinationConfiguration(command);

		ValidationUtils.assertNotNull(newDestinationConfiguration, "A new destination configuration selection for " + orderPlacement.getOrderDestination().getName() + " is required to switch to.");
		ValidationUtils.assertEquals(newDestinationConfiguration.getOrderDestination(), orderPlacement.getOrderDestination(), "Selected destination configuration to switch to does not apply to " + orderPlacement.getOrderDestination().getName());
		ValidationUtils.assertNotEquals(newDestinationConfiguration, orderPlacement.getOrderDestinationConfiguration(), "Selected destination configuration is already selected on the placement.  Nothing to switch to.");
	}


	private OrderDestinationConfiguration getOrderDestinationConfiguration(OrderManagementExecutionActionCommand command) {
		return getOrderDestinationService().getOrderDestinationConfiguration(command.getSwitchToDestinationConfigurationId());
	}
}
