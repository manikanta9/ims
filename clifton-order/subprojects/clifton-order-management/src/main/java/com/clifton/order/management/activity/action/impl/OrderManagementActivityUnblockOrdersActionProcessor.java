package com.clifton.order.management.activity.action.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.management.activity.OrderManagementActivityCommand;
import com.clifton.order.management.activity.action.OrderManagementActivityActionContext;
import com.clifton.order.management.activity.action.OrderManagementActivityActions;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementActivityUnblockOrdersActionProcessor extends BaseOrderManagementActivityActionProcessor {

	private WorkflowDefinitionService workflowDefinitionService;


	@Override
	public OrderManagementActivityActions getOrderManagementActivityAction() {
		return OrderManagementActivityActions.UNBLOCK_ORDERS;
	}


	@Transactional
	@Override
	public void processOrderAllocationActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		if (ArrayUtils.isEmpty(command.getOrderAllocationIds())) {
			throw new ValidationException("No order allocations selected to perform activity on.");
		}

		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setIds(command.getOrderAllocationIds());
		processUnblockOrderAllocations(command, getOrderAllocationService().getOrderAllocationList(searchForm));
	}


	@Transactional
	@Override
	public void processOrderBlockActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		if (ArrayUtils.isEmpty(command.getOrderBlockIds())) {
			throw new ValidationException("No block orders selected to perform activity on.");
		}

		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderBlockIdentifiers(command.getOrderBlockIds());
		processUnblockOrderAllocations(command, getOrderAllocationService().getOrderAllocationList(searchForm));
	}


	private void processUnblockOrderAllocations(OrderManagementActivityCommand command, List<OrderAllocation> orderAllocationList) {
		// If a block order has been partially placed, track the amount we want to take out to ensure we don't remove more than is placed
		Map<Long, List<OrderAllocation>> orderBlockAllocationUnblockListMap = BeanUtils.getBeansMap(orderAllocationList, OrderAllocation::getOrderBlockIdentifier);
		WorkflowState rejectedState = getRejectedWorkflowState(orderAllocationList.get(0));
		WorkflowState pendingState = getPendingWorkflowState(orderAllocationList.get(0));
		for (Map.Entry<Long, List<OrderAllocation>> orderBlockEntry : CollectionUtils.getIterable(orderBlockAllocationUnblockListMap.entrySet())) {
			OrderBlock orderBlock = getOrderBlockService().getOrderBlock(orderBlockEntry.getKey(), false);
			BigDecimal quantityToUnblock = getQuantityToUnblock(orderBlock.isBuy(), orderBlockEntry.getValue());
			if (MathUtils.isNullOrZero(orderBlock.getQuantityPlaced()) || MathUtils.isGreaterThanOrEqual(orderBlock.getQuantityUnplaced(), quantityToUnblock)) {
				orderBlockEntry.getValue().forEach(orderAllocation -> {
					orderAllocation.setOrderBlockIdentifier(null);
					if (command.isRejectUnblockedOrderAllocations()) {
						orderAllocation.setWorkflowState(rejectedState);
						orderAllocation.setWorkflowStatus(rejectedState.getStatus());
					}
					else if (command.isReturnUnblockedOrderAllocations()) {
						orderAllocation.setWorkflowState(pendingState);
						orderAllocation.setWorkflowStatus(pendingState.getStatus());
					}
					getOrderAllocationService().saveOrderAllocation(orderAllocation);
				});
				// For Currency, could switch from a BUY to a SELL - if nothing placed - allow this...
				BigDecimal newQuantity = MathUtils.subtract(orderBlock.getQuantityIntended(), quantityToUnblock);
				if (MathUtils.isLessThan(newQuantity, BigDecimal.ZERO)) {
					newQuantity = MathUtils.abs(newQuantity);
					orderBlock.setBuy(!orderBlock.isBuy());
				}
				orderBlock.setQuantityIntended(newQuantity);
				getOrderBlockService().saveOrderBlock(orderBlock);
			}
			else {
				throw new ValidationException("Cannot unblock order allocations.  Not enough unplaced quantity to remove selected order allocations.");
			}
		}
	}


	private BigDecimal getQuantityToUnblock(boolean buy, List<OrderAllocation> allocationList) {
		return CoreMathUtils.sumProperty(allocationList, orderAllocation -> {
			BigDecimal amount = orderAllocation.getSecurity().isCurrency() ? orderAllocation.getAccountingNotional() : orderAllocation.getQuantityIntended();
			return (buy == orderAllocation.isBuy() ? amount : MathUtils.negate(amount));
		});
	}


	private WorkflowState getRejectedWorkflowState(OrderAllocation orderAllocation) {
		WorkflowState workflowState = getWorkflowDefinitionService().getWorkflowStateByName(orderAllocation.getWorkflowState().getWorkflow().getId(), OrderAllocation.WORKFLOW_STATE_REJECTED);
		ValidationUtils.assertTrue(workflowState != null, "Missing Workflow State with Name Rejected on Workflow " + orderAllocation.getWorkflowState().getWorkflow().getName());
		return workflowState;
	}


	private WorkflowState getPendingWorkflowState(OrderAllocation orderAllocation) {
		WorkflowState workflowState = getWorkflowDefinitionService().getWorkflowStateByName(orderAllocation.getWorkflowState().getWorkflow().getId(), OrderAllocation.WORKFLOW_STATE_PENDING);
		ValidationUtils.assertTrue(workflowState != null, "Missing Workflow State with Name Pending on Workflow " + orderAllocation.getWorkflowState().getWorkflow().getName());
		return workflowState;
	}
}
