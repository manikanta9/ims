package com.clifton.order.management.activity.action.impl;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.block.OrderBlockSearchForm;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.activity.OrderManagementActivityCommand;
import com.clifton.order.management.activity.action.OrderManagementActivityActionContext;
import com.clifton.order.management.activity.action.OrderManagementActivityActions;
import com.clifton.order.management.block.OrderManagementBlock;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import com.clifton.order.shared.OrderCompany;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@Component
public class OrderManagementActivityCreatePlacementsActionProcessor extends BaseOrderManagementActivityActionProcessor {


	@Override
	public OrderManagementActivityActions getOrderManagementActivityAction() {
		return OrderManagementActivityActions.CREATE_PLACEMENTS;
	}


	@Transactional
	@Override
	public void processOrderAllocationActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		processOrderActivityActionImpl(command, context);
	}


	@Transactional
	@Override
	public void processOrderBlockActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		if (command.isCreatePlacements() && command.getOrderDestinationId() != null) {
			OrderBlockSearchForm searchForm = new OrderBlockSearchForm();
			searchForm.setIds(command.getOrderBlockIds());
			searchForm.addSearchRestriction("quantityUnplaced", ComparisonConditions.GREATER_THAN, BigDecimal.ZERO);

			for (OrderBlock orderBlock : CollectionUtils.getIterable(getOrderBlockService().getOrderBlockList(searchForm))) {
				List<OrderCompany> executingBrokerList = getOrderManagementRuleExecutionService().getOrderManagementRuleExecutingBrokerCompanyListForOrderBlockAndDestination(orderBlock.getId(), command.getOrderDestinationId());
				if (!CollectionUtils.isEmpty(executingBrokerList)) {
					context.addOrderBlock(OrderManagementBlock.of(orderBlock, executingBrokerList, getOrderDestinationService().getOrderDestination(command.getOrderDestinationId())));
				}
			}
			processOrderActivityActionImpl(command, context);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void processOrderActivityActionImpl(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		OrderDestinationConfiguration destinationConfiguration = (command.getDestinationConfigurationId() != null ? getOrderDestinationService().getOrderDestinationConfiguration(command.getDestinationConfigurationId()) : null);
		if (destinationConfiguration != null) {
			ValidationUtils.assertFalse(destinationConfiguration.isDisabled(), "Selected Destination Configuration " + destinationConfiguration.getName() + " is disabled.");
		}

		Set<OrderManagementBlock> blockOrderSet = context.getOrderBlockSet();

		for (OrderManagementBlock orderManagementBlock : CollectionUtils.getIterable(blockOrderSet)) {
			if (command.isCreatePlacements() && orderManagementBlock.getOrderDestination() != null) {
				OrderPlacement orderPlacement = new OrderPlacement();
				orderPlacement.setOrderBlock(orderManagementBlock.getOrderBlock());
				orderPlacement.setOrderDestination(orderManagementBlock.getOrderDestination());
				if (destinationConfiguration != null && CompareUtils.isEqual(orderManagementBlock.getOrderDestination(), destinationConfiguration.getOrderDestination())) {
					orderPlacement.setOrderDestinationConfiguration(destinationConfiguration);
				}
				else {
					orderPlacement.setOrderDestinationConfiguration(context.getOrderDestinationConfigurationDefaultForDestination(orderPlacement.getOrderDestination().getId(), getOrderDestinationService()));
				}

				if (CollectionUtils.getSize(orderManagementBlock.getExecutingBrokerCompanyList()) == 1) {
					orderPlacement.setExecutingBrokerCompany(CollectionUtils.getFirstElement(orderManagementBlock.getExecutingBrokerCompanyList()));
				}
				orderPlacement.setQuantityIntended(orderManagementBlock.getOrderBlock().getQuantityUnplaced());
				orderPlacement = getOrderExecutionService().saveOrderPlacement(orderPlacement);
				context.addOrderPlacement(orderPlacement);
			}
		}
	}
}
