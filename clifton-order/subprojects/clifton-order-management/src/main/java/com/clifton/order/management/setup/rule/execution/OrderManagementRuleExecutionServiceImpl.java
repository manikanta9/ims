package com.clifton.order.management.setup.rule.execution;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.block.OrderBlockService;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.OrderManagementRuleCategory;
import com.clifton.order.management.setup.rule.OrderManagementRuleService;
import com.clifton.order.management.setup.rule.execution.search.OrderManagementRuleExecutingBrokerCompanySearchForm;
import com.clifton.order.rule.BaseOrderRuleAware;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationService;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSharedService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderManagementRuleExecutionServiceImpl implements OrderManagementRuleExecutionService {

	private OrderAllocationService orderAllocationService;

	private OrderBlockService orderBlockService;

	private OrderDestinationService orderDestinationService;

	private OrderExecutionService orderExecutionService;

	private OrderManagementRuleService orderManagementRuleService;

	private OrderManagementRuleExecutionHandler orderManagementRuleExecutionHandler;

	private OrderSharedService orderSharedService;

	private OrderSetupService orderSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////            Rule Category Preview                           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderManagementRuleExecutionResult> getOrderManagementRuleExecutionResultList(OrderManagementRuleExecutionCommand command) {
		BaseOrderRuleAware orderRuleAware = validateOrderManagementRulePreviewCommand(command);
		OrderRuleEvaluatorContext context = new OrderRuleEvaluatorContext();
		if (command.getRuleCategoryId() != null) {
			OrderManagementRuleCategory category = getOrderManagementRuleService().getOrderManagementRuleCategory(command.getRuleCategoryId());
			return getOrderManagementRuleExecutionHandler().getOrderManagementRuleExecutionResultListForCategory(orderRuleAware, category, context, false);
		}
		return getOrderManagementRuleExecutionHandler().getOrderManagementRuleExecutionResultList(orderRuleAware, context, false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////           Executing Broker Rule Preview                    ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyList(OrderManagementRuleExecutingBrokerCompanySearchForm searchForm) {
		BaseOrderRuleAware orderRuleAware = validateOrderManagementRulePreviewCommand(searchForm.getCommand());
		return getOrderManagementRuleExecutingBrokerCompanyListImpl(orderRuleAware, searchForm);
	}


	@Override
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyListForOrderRuleAware(OrderRuleAware orderRuleAware, Short orderDestinationId) {
		OrderManagementRuleExecutingBrokerCompanySearchForm searchForm = new OrderManagementRuleExecutingBrokerCompanySearchForm();
		searchForm.setOrderDestinationId(orderDestinationId);
		return getOrderManagementRuleExecutingBrokerCompanyListImpl(orderRuleAware, searchForm);
	}


	@Override
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyListForOrderPlacement(long orderPlacementId) {
		OrderPlacement placement = getOrderExecutionService().getOrderPlacement(orderPlacementId);
		return getOrderManagementRuleExecutingBrokerCompanyListForOrderBlockAndDestination(placement.getOrderBlock().getId(), placement.getOrderDestination().getId());
	}


	@Override
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyListForOrderBlockAndDestination(long orderBlockId, short orderDestinationId) {
		OrderBlock orderBlock = getOrderBlockService().getOrderBlock(orderBlockId, true);
		return getOrderManagementRuleExecutionBrokerCompanyListForOrderAllocationList(orderBlock.getOrderAllocationList(), orderDestinationId);
	}


	private List<OrderCompany> getOrderManagementRuleExecutionBrokerCompanyListForOrderAllocationList(List<OrderAllocation> orderAllocationList, Short orderDestinationId) {
		List<OrderCompany> companyList = null;

		// If destination is manual then consider it to be null and include all approved brokers
		OrderDestination orderDestination = (orderDestinationId == null ? null : getOrderDestinationService().getOrderDestination(orderDestinationId));
		if (orderDestination != null && orderDestination.isOfDestinationCommunicationType(DestinationCommunicationTypes.MANUAL)) {
			orderDestinationId = null;
		}

		for (OrderAllocation orderAllocation : CollectionUtils.getIterable(orderAllocationList)) {
			if (companyList == null) {
				companyList = getOrderManagementRuleExecutingBrokerCompanyListForOrderRuleAware(orderAllocation, orderDestinationId);
			}
			else {
				companyList = CollectionUtils.getIntersection(companyList, getOrderManagementRuleExecutingBrokerCompanyListForOrderRuleAware(orderAllocation, orderDestinationId));
			}
		}
		return (companyList == null) ? null : new PagingArrayList<>(companyList);
	}


	private List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyListImpl(OrderRuleAware orderRuleAware, OrderManagementRuleExecutingBrokerCompanySearchForm searchForm) {
		OrderRuleEvaluatorContext context = new OrderRuleEvaluatorContext();
		return getOrderManagementRuleExecutionHandler().getOrderManagementRuleExecutingBrokerList(orderRuleAware, context, searchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	private BaseOrderRuleAware validateOrderManagementRulePreviewCommand(OrderManagementRuleExecutionCommand command) {
		ValidationUtils.assertNotNull(command, "Command object is null.");
		ValidationUtils.assertNotNull(command.getClientAccountId(), "Client Account selection is required.");
		ValidationUtils.assertNotNull(command.getHoldingAccountId(), "Holding Account selection is required.");
		ValidationUtils.assertNotNull(command.getTradeDate(), "Trade Date is required to find which date rules are active on.");
		ValidationUtils.assertNotNull(command.getOrderSecurityId(), "Security selection is required.");

		BaseOrderRuleAware orderRuleAware = new BaseOrderRuleAware();
		orderRuleAware.setClientAccount(getOrderSharedService().getOrderAccount(command.getClientAccountId()));
		orderRuleAware.setHoldingAccount(getOrderSharedService().getOrderAccount(command.getHoldingAccountId()));
		orderRuleAware.setSecurity(command.getOrderSecurityId() == null ? null : getOrderSharedService().getOrderSecurity(command.getOrderSecurityId()));
		if (orderRuleAware.getSecurity().isCurrency()) {
			ValidationUtils.assertNotNull(command.getSettlementCurrencyId(), "Settlement Currency is required for Currency Order Allocations.");
		}
		orderRuleAware.setSettlementCurrency(command.getSettlementCurrencyId() == null ? null : getOrderSharedService().getOrderSecurity(command.getSettlementCurrencyId()));
		orderRuleAware.setOrderType(command.getOrderTypeId() == null ? null : getOrderSetupService().getOrderType(command.getOrderTypeId()));
		orderRuleAware.setTradeDate(command.getTradeDate());
		orderRuleAware.setExecutingBrokerCompany(command.getExecutingBrokerCompanyId() == null ? null : getOrderSharedService().getOrderCompany(command.getExecutingBrokerCompanyId()));
		return orderRuleAware;
	}
}
