package com.clifton.order.management.execution.action.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.OrderPlacementAllocation;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.trade.OrderTrade;
import com.clifton.order.trade.OrderTradeService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementExecutionCompletePlacementActionProcessor extends BaseOrderManagementExecutionActionProcessor {


	private OrderTradeService orderTradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.COMPLETE);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		List<OrderPlacementAllocation> placementAllocationList = getOrderPlacementAllocationListForPlacement(orderPlacement);
		List<OrderTrade> tradeList = getOrderExecutionHandler().calculateOrderTradeListForPlacement(orderPlacement, placementAllocationList, null);
		getOrderTradeService().saveOrderTradeList(tradeList);
		getOrderAllocationService().scheduleOrderAllocationWorkflowTransitionSystemDefined("TRADES-PLACEMENT-ID" + orderPlacement.getId(), BeanUtils.getPropertyValuesExcludeNull(tradeList, OrderTrade::getOrderAllocationIdentifier, Long.class), OrderAllocation.WORKFLOW_STATE_ALLOCATED, 5);
	}
}
