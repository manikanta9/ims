package com.clifton.order.management.setup.rule.execution;

import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.OrderManagementRuleCategory;
import com.clifton.order.management.setup.rule.execution.search.OrderManagementRuleExecutingBrokerCompanySearchForm;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.shared.OrderCompany;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderManagementRuleExecutionHandler {


	public List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleDefinitionApplyList(OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context, short ruleCategoryId);


	////////////////////////////////////////////////////////////////////////////
	////////            Rule Category Preview                           ////////
	////////////////////////////////////////////////////////////////////////////


	public List<OrderManagementRuleExecutionResult> getOrderManagementRuleExecutionResultList(OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context, boolean returnFailedOnly);


	public List<OrderManagementRuleExecutionResult> getOrderManagementRuleExecutionResultListForCategory(OrderRuleAware orderRuleAware, OrderManagementRuleCategory ruleCategory, OrderRuleEvaluatorContext context, boolean returnFailedOnly);


	////////////////////////////////////////////////////////////////////////////
	////////          Executing Broker Rule Custom Handling             ////////
	////////////////////////////////////////////////////////////////////////////


	public List<OrderCompany> getOrderManagementRuleExecutingBrokerList(OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context, OrderManagementRuleExecutingBrokerCompanySearchForm searchForm);
}
