package com.clifton.order.management.activity.action.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.management.activity.OrderManagementActivityCommand;
import com.clifton.order.management.activity.action.OrderManagementActivityActionContext;
import com.clifton.order.management.activity.action.OrderManagementActivityActions;
import com.clifton.order.management.block.OrderManagementBlock;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderCompany;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
public class OrderManagementActivityCreateBlockOrdersActionProcessor extends BaseOrderManagementActivityActionProcessor {

	@Override
	public OrderManagementActivityActions getOrderManagementActivityAction() {
		return OrderManagementActivityActions.CREATE_BLOCK_ORDERS;
	}


	@Transactional
	@Override
	public void processOrderAllocationActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		List<OrderManagementBlock> blockOrderList = generateOrderManagementBlockList(command);
		for (OrderManagementBlock orderManagementBlock : CollectionUtils.getIterable(blockOrderList)) {
			orderManagementBlock.setOrderBlock(getOrderBlockService().saveOrderBlock(orderManagementBlock.getOrderBlock()));
			context.addOrderBlock(orderManagementBlock);
		}
	}


	@Override
	public void processOrderBlockActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		throw new ValidationException("Creating block order option is not supported for existing block orders.");
	}


	private List<OrderManagementBlock> generateOrderManagementBlockList(OrderManagementActivityCommand command) {
		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setIds(command.getOrderAllocationIds());
		searchForm.setOrderBlockIdentifierIsNull(true);

		List<OrderAllocation> orderAllocationList = getOrderAllocationService().getOrderAllocationList(searchForm);

		Map<String, OrderBlock> orderBlockMap = new HashMap<>();
		Map<String, OrderDestination> orderBlockDestinationMap = new HashMap<>();
		Map<String, List<OrderCompany>> orderBlockExecutingBrokerListMap = new HashMap<>();

		OrderDestination defaultDestination = (command.getOrderDestinationId() != null ? getOrderDestinationService().getOrderDestination(command.getOrderDestinationId()) : null);

		for (OrderAllocation orderAllocation : CollectionUtils.getIterable(orderAllocationList)) {
			ValidationUtils.assertTrue(StringUtils.isEqualIgnoreCase(WorkflowStatus.STATUS_APPROVED, orderAllocation.getWorkflowStatus().getName()), "You can only block order allocations that are in Approved status.");
			List<OrderCompany> executingBrokerList = (orderAllocation.getExecutingBrokerCompany() != null ? CollectionUtils.createList(orderAllocation.getExecutingBrokerCompany()) : getOrderManagementRuleExecutionService().getOrderManagementRuleExecutingBrokerCompanyListForOrderRuleAware(orderAllocation, ObjectUtils.coalesce(BeanUtils.getBeanIdentity(orderAllocation.getOrderDestination()), command.getOrderDestinationId())));
			// Do not continue processing if the selected destination doesn't apply to the order allocation i.e. no available executing brokers
			if (CollectionUtils.isEmpty(executingBrokerList)) {
				continue;
			}
			String key = getOrderAllocationBlockKey(orderAllocation, command, defaultDestination, executingBrokerList);
			if (!orderBlockMap.containsKey(key)) {
				OrderBlock orderBlock = new OrderBlock();
				orderBlock.setOrderType(orderAllocation.getOrderType());
				orderBlock.setSecurity(orderAllocation.getSecurity());
				orderBlock.setSettlementCurrency(orderAllocation.getSettlementCurrency());
				orderBlock.setQuantityIntended(BigDecimal.ZERO);
				orderBlock.setTraderUser(orderAllocation.getTraderUser());
				orderBlock.setTradeDate(orderAllocation.getTradeDate());
				orderBlock.setSettlementDate(orderAllocation.getSettlementDate());
				orderBlock.setOrderAllocationList(new ArrayList<>());
				orderBlockMap.put(key, orderBlock);
				orderBlockExecutingBrokerListMap.put(key, executingBrokerList);
				orderBlockDestinationMap.put(key, ObjectUtils.coalesce(orderAllocation.getOrderDestination(), defaultDestination));
			}
			OrderBlock orderBlock = orderBlockMap.get(key);
			BigDecimal allocationQuantity = orderAllocation.getSecurity().isCurrency() ? orderAllocation.getAccountingNotional() : orderAllocation.getQuantityIntended();
			if (!orderAllocation.isBuy()) {
				allocationQuantity = MathUtils.negate(allocationQuantity);
			}
			orderBlock.setQuantityIntended(MathUtils.add(orderBlock.getQuantityIntended(), allocationQuantity));
			orderBlock.getOrderAllocationList().add(orderAllocation);
		}

		List<OrderManagementBlock> orderManagementBlockList = new ArrayList<>();
		for (Map.Entry<String, OrderBlock> blockEntry : orderBlockMap.entrySet()) {
			OrderBlock block = blockEntry.getValue();
			if (MathUtils.isLessThan(block.getQuantityIntended(), BigDecimal.ZERO)) {
				block.setBuy(false);
				block.setQuantityIntended(MathUtils.abs(block.getQuantityIntended()));
			}
			else {
				block.setBuy(true);
			}
			orderManagementBlockList.add(OrderManagementBlock.of(block, orderBlockExecutingBrokerListMap.get(blockEntry.getKey()), orderBlockDestinationMap.get(blockEntry.getKey())));
		}

		return orderManagementBlockList;
	}


	private String getOrderAllocationBlockKey(OrderAllocation orderAllocation, OrderManagementActivityCommand command, OrderDestination defaultDestination, List<OrderCompany> executingBrokerList) {
		String key = StringUtils.generateKey(orderAllocation.getSecurity().getId(), orderAllocation.getSettlementCurrency().getId(), (orderAllocation.getOrderType().isAllowOrderBlockNetting() ? "-" : orderAllocation.getOpenCloseType().isBuy() ? "B" : "S"), DateUtils.fromDateShort(orderAllocation.getTradeDate()), DateUtils.fromDateShort(orderAllocation.getSettlementDate()), BeanUtils.getIdentityAsLong(orderAllocation.getTraderUser()));
		if (isBlockByAccount(orderAllocation, command, defaultDestination)) {
			key = StringUtils.generateKey(key, BeanUtils.getIdentityAsLong(orderAllocation.getHoldingAccount()));
		}
		if (!CollectionUtils.isEmpty(executingBrokerList)) {
			key = StringUtils.generateKey(key, "EB_", StringUtils.collectionToCommaDelimitedString(BeanUtils.sortWithFunction(executingBrokerList, OrderCompany::getId, true), OrderCompany::getId));
		}
		if (orderAllocation.getOrderDestination() != null) {
			key = StringUtils.generateKey(key, "OD_", orderAllocation.getOrderDestination().getId());
		}
		return key;
	}


	private boolean isBlockByAccount(OrderAllocation orderAllocation, OrderManagementActivityCommand command, OrderDestination defaultDestination) {
		if (command.isOrderBlockAccount()) {
			return true;
		}
		if (orderAllocation.getOrderDestination() != null && orderAllocation.getOrderDestination().isForceBlockingByAccount()) {
			return true;
		}
		if (defaultDestination != null && defaultDestination.isForceBlockingByAccount()) {
			return true;
		}
		return false;
	}
}
