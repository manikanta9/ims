package com.clifton.order.management.activity.action.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.management.activity.OrderManagementActivityCommand;
import com.clifton.order.management.activity.OrderManagementDateRevisionTypes;
import com.clifton.order.management.activity.action.OrderManagementActivityActionContext;
import com.clifton.order.management.activity.action.OrderManagementActivityActions;
import com.clifton.order.shared.OrderSharedUtilHandler;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionSearchCommand;
import com.clifton.workflow.transition.WorkflowTransitionService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementActivityReviseDatesActionProcessor extends BaseOrderManagementActivityActionProcessor {


	private OrderSharedUtilHandler orderSharedUtilHandler;

	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderManagementActivityActions getOrderManagementActivityAction() {
		return OrderManagementActivityActions.REVISE_DATES;
	}


	@Transactional
	@Override
	public void processOrderAllocationActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		if (ArrayUtils.isEmpty(command.getOrderAllocationIds())) {
			throw new ValidationException("No order allocations selected to perform activity on.");
		}

		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setIds(command.getOrderAllocationIds());
		searchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CLOSED);

		List<OrderAllocation> allocationList = getOrderAllocationService().getOrderAllocationList(searchForm);

		if (!CollectionUtils.isEmpty(allocationList)) {
			Map<Short, WorkflowTransition> reviseDatesWorkflowTransitionMap = getReviseDatesWorkflowTransitionMap();
			for (OrderAllocation orderAllocation : allocationList) {
				Date newTradeDate = getNewTradeDateForOrderAllocation(orderAllocation, command);
				Date newSettlementDate = getNewSettlementDateForOrderAllocation(orderAllocation, newTradeDate, command);
				if (!DateUtils.isEqual(newTradeDate, orderAllocation.getTradeDate()) || !DateUtils.isEqual(newSettlementDate, orderAllocation.getSettlementDate())) {
					WorkflowTransition transition = reviseDatesWorkflowTransitionMap.get(orderAllocation.getWorkflowState().getId());
					// If there is no "Revise Dates" transition, then consider assume we can just save the update (in a Draft status)
					if (transition == null) {
						orderAllocation.setTradeDate(newTradeDate);
						orderAllocation.setSettlementDate(newSettlementDate);
						getOrderAllocationService().saveOrderAllocation(orderAllocation);
					}
					else {
						getOrderAllocationService().saveOrderAllocationWithRevisedDates(orderAllocation.getId(), newTradeDate, newSettlementDate, transition.getId(), true);
					}
				}
			}
		}
	}


	@Override
	public void processOrderBlockActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		throw new ValidationException("Revise Dates not supports on Block Orders"); // Note - could be used for "amending" post execution
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	protected void validateReviseDateOptions(OrderManagementActivityCommand command) {
		if (command.getTradeDateAdjustment() == null) {
			command.setTradeDateAdjustment(OrderManagementDateRevisionTypes.NONE);
		}
		if (command.getSettlementDateAdjustment() == null) {
			command.setSettlementDateAdjustment(OrderManagementDateRevisionTypes.NONE);
		}
		ValidationUtils.assertTrue(command.getTradeDateAdjustment().isValidTradeDateAdjustment(), "Selected Trade Date Adjustment is not a valid selection.");
		ValidationUtils.assertTrue(command.getSettlementDateAdjustment().isValidSettlementDateAdjustment(), "Selected Settlement Date Adjustment is not a valid selection.");

		if (command.getTradeDateAdjustment() == OrderManagementDateRevisionTypes.OVERRIDE) {
			ValidationUtils.assertNotNull(command.getOverrideTradeDate(), "Trade Date Adjustment type of override requires an explicit trade date to be entered.");
		}
		if (command.getSettlementDateAdjustment() == OrderManagementDateRevisionTypes.OVERRIDE) {
			ValidationUtils.assertNotNull(command.getOverrideSettlementDate(), "Settlement Date Adjustment type of override requires an explicit trade date to be entered.");
		}
	}


	protected Date getNewTradeDateForOrderAllocation(OrderAllocation orderAllocation, OrderManagementActivityCommand command) {
		Date newTradeDate = orderAllocation.getTradeDate();
		switch (command.getTradeDateAdjustment()) {
			case OVERRIDE:
				newTradeDate = command.getOverrideTradeDate();
				break;
			case RECALCULATE:
				newTradeDate = getOrderSharedUtilHandler().calculateTradeDate(orderAllocation.getSecurity(), null);
				break;
			case T_PLUS_ONE:
				newTradeDate = getOrderSharedUtilHandler().getOrderSecurityTradeDateNext(orderAllocation.getSecurity(), newTradeDate);
				break;
		}
		if (command.isAutoAdjustReviseDatesForBusinessDays()) {
			if (!getOrderSharedUtilHandler().isValidTradeDate(orderAllocation.getSecurity(), newTradeDate)) {
				newTradeDate = getOrderSharedUtilHandler().getOrderSecurityTradeDateNext(orderAllocation.getSecurity(), newTradeDate);
			}
		}
		return newTradeDate;
	}


	protected Date getNewSettlementDateForOrderAllocation(OrderAllocation orderAllocation, Date newTradeDate, OrderManagementActivityCommand command) {
		Date newSettlementDate = orderAllocation.getSettlementDate();
		switch (command.getSettlementDateAdjustment()) {
			case OVERRIDE:
				newSettlementDate = command.getOverrideSettlementDate();
				break;
			case RECALCULATE:
				newSettlementDate = getOrderSharedUtilHandler().calculateSettlementDate(orderAllocation.getSecurity(), orderAllocation.getSettlementCurrency(), newTradeDate);
				break;
			case T_PLUS_ONE:
				newSettlementDate = getOrderSharedUtilHandler().getOrderSecuritySettlementDateNext(orderAllocation.getSecurity(), orderAllocation.getSettlementCurrency(), newSettlementDate);
				break;
			case SAME_DAY:
				newSettlementDate = newTradeDate;
		}
		if (command.isAutoAdjustReviseDatesForBusinessDays()) {
			if (!getOrderSharedUtilHandler().isValidSettlementDate(orderAllocation.getSecurity(), orderAllocation.getSettlementCurrency(), newSettlementDate)) {
				newSettlementDate = getOrderSharedUtilHandler().getOrderSecuritySettlementDateNext(orderAllocation.getSecurity(), orderAllocation.getSettlementCurrency(), newSettlementDate);
			}
		}
		return newSettlementDate;
	}


	protected Map<Short, WorkflowTransition> getReviseDatesWorkflowTransitionMap() {
		WorkflowTransitionSearchCommand cmd = WorkflowTransitionSearchCommand.of(OrderAllocation.WORKFLOW_NAME, "Revise Dates");
		return BeanUtils.getBeanMap(getWorkflowTransitionService().getWorkflowTransitionListForCommand(cmd), workflowTransition -> workflowTransition.getStartWorkflowState().getId());
	}
}
