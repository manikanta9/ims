package com.clifton.order.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.order.batch.OrderBatchService;
import com.clifton.order.batch.search.OrderBatchItemSearchForm;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.trade.OrderTrade;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderTradeRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private OrderBatchService orderBatchService;

	private OrderExecutionService orderExecutionService;

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof OrderTrade;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		OrderTrade orderTrade = (OrderTrade) entity;
		List<IdentityObject> relatedEntityList = new ArrayList<>();

		// Include the batches
		OrderBatchItemSearchForm batchItemSearchForm = new OrderBatchItemSearchForm();
		batchItemSearchForm.setTableName(OrderTrade.TABLE_NAME);
		batchItemSearchForm.setFkFieldId(orderTrade.getId());
		relatedEntityList.addAll(getOrderBatchService().getOrderBatchItemList(batchItemSearchForm));

		// Notes - should be a more generic way to do this (check tables for note types?)
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(OrderTrade.TABLE_NAME);
		noteSearchForm.setFkFieldId(orderTrade.getId());
		// Not adding for linked entities - could be messy if we have notes on accounts
		relatedEntityList.addAll(getSystemNoteService().getSystemNoteListForEntity(noteSearchForm));

		return relatedEntityList;
	}
}
