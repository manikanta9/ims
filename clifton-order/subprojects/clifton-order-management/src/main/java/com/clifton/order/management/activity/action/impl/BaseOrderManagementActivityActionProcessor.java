package com.clifton.order.management.activity.action.impl;

import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.block.OrderBlockService;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.management.activity.action.OrderManagementActivityActionProcessor;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionService;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionService;
import com.clifton.order.setup.destination.OrderDestinationService;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public abstract class BaseOrderManagementActivityActionProcessor implements OrderManagementActivityActionProcessor {

	private OrderAllocationService orderAllocationService;
	private OrderBlockService orderBlockService;
	private OrderDestinationService orderDestinationService;
	private OrderExecutionService orderExecutionService;

	private OrderManagementExecutionActionService orderManagementExecutionActionService;
	private OrderManagementRuleExecutionService orderManagementRuleExecutionService;
}
