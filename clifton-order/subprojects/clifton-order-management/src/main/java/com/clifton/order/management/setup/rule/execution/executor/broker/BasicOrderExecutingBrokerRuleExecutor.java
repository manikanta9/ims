package com.clifton.order.management.setup.rule.execution.executor.broker;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionUtils;
import com.clifton.order.management.setup.rule.execution.executor.BaseOrderManagementRuleExecutor;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class BasicOrderExecutingBrokerRuleExecutor extends BaseOrderManagementRuleExecutor {

	/**
	 * Used to flag if the filtering should apply as allowed or prohibited
	 * i.e. Security Type = Currency, Prohibited = true or false
	 */
	private boolean prohibited;

	/**
	 * If allowed, can also be required
	 * So, even if rules allow another executing broker this rule overrides
	 * and requires that only this broker is allowed.
	 */
	private boolean required;

	/**
	 * If required, the requirement can be applied in ADDITION to the broker also being approved
	 */
	private boolean requiredNotApproved;

	/**
	 * If the rule is executing broker specific, or applies to all
	 */
	private boolean applyToAllExecutingBrokers;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getAssignmentSpecificityScopeKey(OrderManagementRuleAssignmentExtended ruleAssignment) {
		return StringUtils.generateKey(getClass().getName(), isRequired(), isRequiredNotApproved(), BeanUtils.getBeanIdentity(ruleAssignment.getRollupRuleDefinition()), StringUtils.removeAll(ruleAssignment.getRuleDefinition().getCustomColumns().toString(), "\""));
	}


	@Override
	public boolean isAssignmentApplicable(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		if (orderRuleAware.getExecutingBrokerCompany() != null && !isApplyToAllExecutingBrokers()) {
			// If the rule is requiring a specific executing broker - it will always apply (based on security scope below)
			if (!isRequired() && isRuleValueNotApplyToContext(orderRuleAware.getExecutingBrokerCompany().getId(), OrderManagementRuleExecutionUtils.getRuleAssignmentExecutingBroker(ruleAssignment))) {
				return false;
			}
		}

		if (orderRuleAware.getSecurity() != null) {
			if (isRuleValueNotApplyToContext(orderRuleAware.getSecurity().getId(), OrderManagementRuleExecutionUtils.getRuleAssignmentSecurity(ruleAssignment))) {
				return false;
			}
			if (isRuleValueNotApplyToContext(BeanUtils.getBeanIdentity(orderRuleAware.getSecurity().getSecurityType()), OrderManagementRuleExecutionUtils.getRuleAssignmentSecurityType(ruleAssignment))) {
				return false;
			}
		}
		else {
			if (isRuleValueNotApplyToContext(BeanUtils.getBeanIdentity(orderRuleAware.getOrderType().getSecurityType()), OrderManagementRuleExecutionUtils.getRuleAssignmentSecurityType(ruleAssignment))) {
				return false;
			}
		}
		return true;
	}


	@Override
	public boolean executeRule(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		if (isProhibited() && isApplyToAllExecutingBrokers()) {
			return false;
		}
		else if (orderRuleAware.getExecutingBrokerCompany() == null) {
			return true;
		}
		boolean allow = !isProhibited();
		if (allow && isRequired()) {
			return CompareUtils.isEqual(orderRuleAware.getExecutingBrokerCompany().getId(), OrderManagementRuleExecutionUtils.getRuleAssignmentExecutingBroker(ruleAssignment));
		}
		return allow;
	}
}
