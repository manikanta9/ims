package com.clifton.order.management.batch.send;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import lombok.Getter;
import lombok.Setter;


/**
 * Copies the batch file to a network file path
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderBatchNetworkFileSender extends BaseOrderBatchSender {

	private static final String FILE_NAME_WRITING_EXTENSION = "write";


	/**
	 * The path to the network folder where the file will be exported to.
	 */
	private String destinationNetworkPath;

	/**
	 * Whether or not the destination file should be overwritten if a file with the same name already exists
	 */
	private boolean overwriteFile;

	/**
	 * Whether or not a .write extension will be added to the file while being copied.
	 */
	private boolean writeExtension;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void sendOrderBatch(OrderBatch orderBatch, FileWrapper fileWrapper) {
		FileContainer networkPath = FileContainerFactory.getFileContainer(getDestinationNetworkPath());
		ValidationUtils.assertTrue(networkPath.exists(), "Cannot send files [" + networkPath + "] because it does not exist.");

		String targetFileName = fileWrapper.getFileName();
		String targetAbsolutePath = FileUtils.combinePaths(networkPath.getPath(), targetFileName);
		FileContainer targetFileContainer = FileContainerFactory.getFileContainer(targetAbsolutePath);

		FileContainer fullFilePath = FileContainerFactory.getFileContainer(fileWrapper.getFile());

		if (!isOverwriteFile() && targetFileContainer.exists()) {
			throw new ValidationException("Target path " + targetFileContainer.getAbsolutePath() + "already exists. Enable Overwrite File to process this batch.");
		}

		try {

			if (isWriteExtension()) {
				String targetWriteAbsolutePath = FileUtils.combinePaths(networkPath.getPath(), targetFileName + "." + FILE_NAME_WRITING_EXTENSION);
				FileContainer targetWriteFileContainer = FileContainerFactory.getFileContainer(targetWriteAbsolutePath);
				FileUtils.copyFileOverwrite(fullFilePath, targetWriteFileContainer);
				boolean successful;
				if (isOverwriteFile() && targetFileContainer.exists()) {
					// Need to create a temp file (Path: Parent/NanoTime_FileName) if target already exists. Windows doesn't allow rename to overwrite.
					String tempAbsolutePath = FileUtils.combinePaths(targetFileContainer.getParent(), (System.nanoTime() + "_" + targetFileContainer.getName()));
					FileContainer tempFileContainer = FileContainerFactory.getFileContainer(tempAbsolutePath);
					// Rename the original file to the temp file
					successful = targetFileContainer.renameToFile(tempFileContainer);
					if (successful) {
						// Rename the write extension file to the original file
						successful = targetWriteFileContainer.renameToFile(targetFileContainer);
						if (successful) {
							// Delete the original file
							tempFileContainer.deleteFile();
						}
						else {
							// Reset the original file
							tempFileContainer.renameToFile(targetFileContainer);
						}
					}
				}
				else {
					successful = targetWriteFileContainer.renameToFile(targetFileContainer);
				}
				if (!successful) {
					throw new ValidationException("Could not rename " + targetWriteFileContainer.getAbsolutePath() + " to " + targetFileContainer.getAbsolutePath());
				}
			}
			else if (isOverwriteFile()) {
				FileUtils.copyFileOverwrite(fullFilePath, FileContainerFactory.getFileContainer(FileUtils.combinePath(networkPath.getPath(), targetFileName)));
			}
			else {
				FileUtils.copyFile(fullFilePath, FileContainerFactory.getFileContainer(FileUtils.combinePath(networkPath.getPath(), targetFileName)));
			}
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
}
