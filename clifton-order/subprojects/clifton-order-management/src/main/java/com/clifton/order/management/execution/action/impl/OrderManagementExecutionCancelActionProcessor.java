package com.clifton.order.management.execution.action.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.order.batch.OrderBatchItem;
import com.clifton.order.batch.OrderBatchService;
import com.clifton.order.batch.search.OrderBatchItemSearchForm;
import com.clifton.order.batch.setup.OrderBatchSetupService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderExecutionActions;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;


/**
 * The <code>OrderManagementExecutionCancelActionProcessor</code> really just changes the status to canceled (handled by the base class).  If the placement is already batched, but not sent, will remove it from the batch
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementExecutionCancelActionProcessor extends BaseOrderManagementExecutionActionProcessor {


	private OrderBatchService orderBatchService;
	private OrderBatchSetupService orderBatchSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.CANCEL);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		processCancelBatchItems(orderPlacement);
	}


	protected void processCancelBatchItems(OrderPlacement orderPlacement) {
		// If the placement could be in a batch, we'll remove it from the batch ONLY if the batch is in Batched status.  Otherwise it was sent and we'll be able to track this based on the Placement Execution Status
		// If re-sending the batch, then canceled placements should be excluded (but that is up to the query the batch uses to exclude them).
		if (orderPlacement.getOrderDestination().getDestinationType().isBatched()) {
			OrderBatchItemSearchForm searchForm = new OrderBatchItemSearchForm();
			searchForm.setOrderDestinationId(orderPlacement.getOrderDestination().getId());
			searchForm.setFkFieldId(orderPlacement.getId());
			searchForm.setOpen(true);
			List<OrderBatchItem> itemList = getOrderBatchService().getOrderBatchItemList(searchForm);
			CollectionUtils.getStream(itemList).forEach(batchItem -> getOrderBatchService().deleteOrderBatchItem(batchItem.getId()));
		}
	}
}
