package com.clifton.order.management.execution.action.impl;

import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationService;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Set;


/**
 * The <code>OrderManagementExecutionSwitchDestinationActionProcessor</code> is used to switch a placement to a different destination
 * Either From / TO Main to Back up Destination
 * Or From / To Manual
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementExecutionSwitchDestinationActionProcessor extends OrderManagementExecutionCancelActionProcessor {


	private static final Set<String> BATCHED_PLACEMENT_REQUIRE_CANCEL = CollectionUtils.createHashSet(OrderExecutionStatus.SENT, OrderExecutionStatus.BATCHED, OrderExecutionStatus.ERROR);
	// Nothing for FIX yet as we don't allow switching once it's beyond SENT (which means we didn't get acknowledgement from external system)

	private ContextHandler contextHandler;

	private OrderDestinationService orderDestinationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.SWITCH_DESTINATION, OrderExecutionActions.SWITCH_TO_MANUAL);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		OrderDestination newDestination = getOrderDestination(command);

		if (!isPlacementRequiredToBeCanceled(orderPlacement)) {
			orderPlacement.setOrderDestination(newDestination);
			orderPlacement.setExecutionStatus(null);
			orderPlacement.setAllocationStatus(null);
			orderPlacement.setQuantityFilled(null);
			orderPlacement.setAverageUnitPrice(null);
			OrderDestinationUtils.executeResetPlacement(() -> getOrderExecutionService().saveOrderPlacement(orderPlacement));
		}
		else {
			// Create the new Placement, but don't save it until after the canceled one is saved
			OrderPlacement newOrderPlacement = new OrderPlacement();
			newOrderPlacement.setOrderBlock(orderPlacement.getOrderBlock());
			newOrderPlacement.setOrderDestination(newDestination);
			newOrderPlacement.setExecutingBrokerCompany(orderPlacement.getExecutingBrokerCompany());
			newOrderPlacement.setQuantityIntended(orderPlacement.getQuantityIntended());

			// Cancel the existing placement
			processCancelBatchItems(orderPlacement);
			orderPlacement.setExecutionStatus(getOrderSetupService().getOrderExecutionStatusByName(OrderExecutionStatus.CANCELED_STATUS));
			getOrderExecutionService().saveOrderPlacement(orderPlacement);

			// Save the new Placement
			getOrderExecutionService().saveOrderPlacement(newOrderPlacement);
		}
	}


	private boolean isPlacementRequiredToBeCanceled(OrderPlacement orderPlacement) {
		if (orderPlacement.getOrderDestination().getDestinationType().isBatched()) {
			return (BATCHED_PLACEMENT_REQUIRE_CANCEL.contains(orderPlacement.getExecutionStatus().getName()));
		}
		return false;
	}


	@Override
	protected void validateCommand(OrderManagementExecutionActionCommand command, OrderPlacement orderPlacement) {
		if (OrderExecutionActions.SWITCH_DESTINATION == command.getOrderExecutionAction()) {
			ValidationUtils.assertNotNull(command.getSwitchToDestinationId(), "A new destination must be selected to switch to.");
			OrderDestination destination = getOrderDestinationService().getOrderDestination(command.getSwitchToDestinationId());
			// Will only allow switching to the back up for now
			ValidationUtils.assertTrue(destination.isBackupDestination() && CompareUtils.isEqual(destination.getMainOrderDestination(), orderPlacement.getOrderDestination()), "Selected destination is not a valid back up for current destination " + orderPlacement.getOrderDestination().getName());
		}
		else {
			ValidationUtils.assertFalse(orderPlacement.isOfDestinationCommunicationType(DestinationCommunicationTypes.MANUAL), "Selected placement is already using Manual destination.");
		}
	}


	private OrderDestination getOrderDestination(OrderManagementExecutionActionCommand command) {
		if (OrderExecutionActions.SWITCH_DESTINATION == command.getOrderExecutionAction()) {
			return getOrderDestinationService().getOrderDestination(command.getSwitchToDestinationId());
		}
		return getOrderDestinationService().getOrderDestinationManual();
	}
}
