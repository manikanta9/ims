package com.clifton.order.management.setup.rule.retriever;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.order.management.setup.rule.BaseOrderManagementRuleAssignmentSearchFormConfigurer;
import com.clifton.order.management.setup.rule.OrderManagementRuleAssignment;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinitionRollup;
import com.clifton.order.management.setup.rule.OrderManagementRuleService;
import com.clifton.order.management.setup.rule.cache.OrderManagementRuleAssignmentHolder;
import com.clifton.order.management.setup.rule.cache.OrderManagementRuleAssignmentHolderCache;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.shared.OrderSharedService;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderManagementRuleRetrieverServiceImpl implements OrderManagementRuleRetrieverService {


	private AdvancedReadOnlyDAO<OrderManagementRuleAssignmentExtended, Criteria> orderManagementRuleAssignmentExtendedDAO;

	private OrderManagementRuleAssignmentHolderCache orderManagementRuleAssignmentHolderCache;

	private OrderManagementRuleService orderManagementRuleService;

	private OrderSharedService orderSharedService;

	////////////////////////////////////////////////////////////////////////////
	////////      Order Management Rule Assignment Extended Methods     ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleAssignmentExtendedList(OrderManagementRuleAssignmentExtendedSearchForm searchForm) {
		return getOrderManagementRuleAssignmentExtendedDAO().findBySearchCriteria(new BaseOrderManagementRuleAssignmentSearchFormConfigurer(searchForm, getOrderSharedService()));
	}


	@Override
	public List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleAssignmentExtendedListForAccountAndCategory(OrderRuleAware orderRuleAware, short ruleCategoryId) {
		Set<OrderManagementRuleAssignmentHolder> ruleAssignmentHolderSet = getOrderManagementRuleAssignmentHolderCache().getOrderManagementRuleAssignmentHolderSetForAccount(orderRuleAware.getClientAccount().getId(), orderRuleAware.getHoldingAccount().getId(), orderRuleAware.getHoldingAccount().getIssuingCompany().getId(), orderRuleAware.getTradeDate());
		List<OrderManagementRuleAssignmentExtended> extendedList = new ArrayList<>();

		CollectionUtils.getStream(ruleAssignmentHolderSet).forEach(ruleAssignmentHolder -> {
			if (ruleAssignmentHolder.isRollup()) {
				// If it is a rollup, then we need to get all child rules and only include them if they are for the same category
				OrderManagementRuleAssignment ruleAssignment = getOrderManagementRuleService().getOrderManagementRuleAssignment(ruleAssignmentHolder.getId());
				List<OrderManagementRuleDefinitionRollup> rollupList = getOrderManagementRuleService().getOrderManagementRuleDefinitionRollupListByParent(ruleAssignment.getRuleDefinition().getId());
				CollectionUtils.getStream(rollupList).forEach(rollup -> {
					if (MathUtils.isEqual(ruleCategoryId, rollup.getReferenceTwo().getRuleType().getRuleCategory().getId())) {
						extendedList.add(OrderManagementRuleAssignmentExtended.forRollupRuleAssignmentWithChild(ruleAssignment, rollup.getReferenceTwo()));
					}
				});
			}
			else {
				// If not a rollup, only include if it's for the same category
				if (MathUtils.isEqual(ruleAssignmentHolder.getRuleCategoryId(), ruleCategoryId)) {
					extendedList.add(OrderManagementRuleAssignmentExtended.forNonRollupRuleAssignment(getOrderManagementRuleService().getOrderManagementRuleAssignment(ruleAssignmentHolder.getId())));
				}
			}
		});
		return extendedList;
	}
}
