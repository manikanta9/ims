package com.clifton.order.management.activity.action;


import com.clifton.core.util.AssertUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
@Getter
@Setter
public class OrderManagementActivityActionProcessorLocatorInSpringContext implements OrderManagementActivityActionProcessorLocator, InitializingBean, ApplicationContextAware {

	private final Map<OrderManagementActivityActions, OrderManagementActivityActionProcessor> processorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, OrderManagementActivityActionProcessor> beanMap = getApplicationContext().getBeansOfType(OrderManagementActivityActionProcessor.class);

		// need a map with InvestmentSecurityAllocationTypes as keys instead of bean names
		for (Map.Entry<String, OrderManagementActivityActionProcessor> stringOrderManagementActivityActionProcessorEntry : beanMap.entrySet()) {
			OrderManagementActivityActionProcessor processor = stringOrderManagementActivityActionProcessorEntry.getValue();
			if (getProcessorMap().containsKey(processor.getOrderManagementActivityAction())) {
				throw new RuntimeException("Cannot register '" + stringOrderManagementActivityActionProcessorEntry.getKey() + "' as a processor for order management activity action  '" + processor.getOrderManagementActivityAction()
						+ "' because this activity action type already has a registered processor.");
			}
			getProcessorMap().put(processor.getOrderManagementActivityAction(), processor);
		}
	}


	@Override
	public OrderManagementActivityActionProcessor locate(OrderManagementActivityActions activityAction) {
		AssertUtils.assertNotNull(activityAction, "Required activity action cannot be null.");
		OrderManagementActivityActionProcessor result = getProcessorMap().get(activityAction);
		AssertUtils.assertNotNull(result, "Cannot locate OrderManagementActivityActionProcessor for '%1s' activity action.", activityAction);
		return result;
	}
}
