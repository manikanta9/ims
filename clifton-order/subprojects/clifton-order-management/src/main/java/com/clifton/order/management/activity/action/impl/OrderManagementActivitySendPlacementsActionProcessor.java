package com.clifton.order.management.activity.action.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.activity.OrderManagementActivityCommand;
import com.clifton.order.management.activity.action.OrderManagementActivityActionContext;
import com.clifton.order.management.activity.action.OrderManagementActivityActions;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import org.springframework.stereotype.Component;


/**
 * This action is explicitly non-transactional because it queues up messages for placements and cannot be rolled back
 *
 * @author manderson
 */
@Component
public class OrderManagementActivitySendPlacementsActionProcessor extends BaseOrderManagementActivityActionProcessor {


	@Override
	public OrderManagementActivityActions getOrderManagementActivityAction() {
		return OrderManagementActivityActions.SEND_PLACEMENTS;
	}


	@Override
	public void processOrderAllocationActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		processActivityActionImpl(command, context);
	}


	@Override
	public void processOrderBlockActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		processActivityActionImpl(command, context);
	}


	private void processActivityActionImpl(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		for (OrderPlacement orderPlacement : CollectionUtils.getIterable(context.getOrderPlacementSet())) {
			if (orderPlacement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FIX)) {
				// make this raise event ?  put in queue to pick up for processing?
				getOrderManagementExecutionActionService().processOrderManagementExecutionAction(orderPlacement.getId(), OrderExecutionActions.SEND);
			}
		}
	}
}
