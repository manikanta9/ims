package com.clifton.order.management.activity.action.impl;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.block.OrderBlockSearchForm;
import com.clifton.order.management.activity.OrderManagementActivityCommand;
import com.clifton.order.management.activity.action.OrderManagementActivityActionContext;
import com.clifton.order.management.activity.action.OrderManagementActivityActions;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.workflow.definition.WorkflowStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementActivityAssignTraderActionProcessor extends BaseOrderManagementActivityActionProcessor {

	private SecurityUserService securityUserService;


	@Override
	public OrderManagementActivityActions getOrderManagementActivityAction() {
		return OrderManagementActivityActions.ASSIGN_TRADER;
	}


	@Transactional
	@Override
	public void processOrderAllocationActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		if (ArrayUtils.isEmpty(command.getOrderAllocationIds())) {
			throw new ValidationException("No order allocations selected to perform activity on.");
		}

		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setIds(command.getOrderAllocationIds());
		searchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CLOSED);
		if (!command.isReassignTraderIfPopulated()) {
			searchForm.setTraderUserIdMissing(true);
		}
		// Ignore where already assigned to the trader
		else {
			searchForm.addSearchRestriction(new SearchRestriction("traderUserId", ComparisonConditions.NOT_EQUALS, command.getAssignTraderUserId()));
		}
		List<OrderAllocation> allocationList = getOrderAllocationService().getOrderAllocationList(searchForm);
		if (!CollectionUtils.isEmpty(allocationList)) {
			SecurityUser traderUser = getSecurityUserService().getSecurityUser(command.getAssignTraderUserId());
			allocationList.forEach(orderAllocation -> orderAllocation.setTraderUser(traderUser));
			getOrderAllocationService().saveOrderAllocationList(allocationList, true);
		}
	}


	@Transactional
	@Override
	public void processOrderBlockActivityAction(OrderManagementActivityCommand command, OrderManagementActivityActionContext context) {
		if (ArrayUtils.isEmpty(command.getOrderBlockIds())) {
			throw new ValidationException("No block orders selected to perform activity on.");
		}

		OrderBlockSearchForm searchForm = new OrderBlockSearchForm();
		searchForm.setIds(command.getOrderBlockIds());
		// Can change Trader as long as Quantity Unfilled > 0?
		searchForm.addSearchRestriction("quantityUnfilled", ComparisonConditions.GREATER_THAN, BigDecimal.ZERO);
		// Ignore where already assigned to the trader
		searchForm.addSearchRestriction(new SearchRestriction("traderUserId", ComparisonConditions.NOT_EQUALS, command.getAssignTraderUserId()));

		List<OrderBlock> orderBlockList = getOrderBlockService().getOrderBlockList(searchForm);
		if (!CollectionUtils.isEmpty(orderBlockList)) {
			SecurityUser traderUser = getSecurityUserService().getSecurityUser(command.getAssignTraderUserId());
			for (OrderBlock orderBlock : orderBlockList) {
				orderBlock = getOrderBlockService().getOrderBlock(orderBlock.getId(), true);
				orderBlock.setTraderUser(traderUser);
				orderBlock.getOrderAllocationList().forEach(orderAllocation -> orderAllocation.setTraderUser(traderUser));
				getOrderBlockService().saveOrderBlock(orderBlock);
			}
		}
	}
}
