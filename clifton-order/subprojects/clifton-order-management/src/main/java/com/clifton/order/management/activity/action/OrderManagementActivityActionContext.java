package com.clifton.order.management.activity.action;

import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.block.OrderManagementBlock;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import com.clifton.order.setup.destination.OrderDestinationService;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementActivityActionContext {


	private final Set<OrderManagementBlock> orderBlockSet = new HashSet<>();


	private final Set<OrderPlacement> orderPlacementSet = new HashSet<>();

	private final Map<Short, OrderDestinationConfiguration> defaultDestinationConfigurationMap = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addOrderBlock(OrderManagementBlock orderBlock) {
		this.orderBlockSet.add(orderBlock);
	}


	public void addOrderPlacement(OrderPlacement orderPlacement) {
		this.orderPlacementSet.add(orderPlacement);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderDestinationConfiguration getOrderDestinationConfigurationDefaultForDestination(short orderDestinationId, OrderDestinationService orderDestinationService) {
		return this.defaultDestinationConfigurationMap.putIfAbsent(orderDestinationId, orderDestinationService.getOrderDestinationConfigurationDefaultForDestination(orderDestinationId));
	}
}
