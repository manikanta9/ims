package com.clifton.order.management.execution.event;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.order.execution.event.BaseOrderPlacementActionEvent;
import com.clifton.order.execution.event.OrderPlacementAllocationCompleteEvent;
import com.clifton.order.execution.event.OrderPlacementFilledEvent;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderPlacementActionEventListener<T extends BaseOrderPlacementActionEvent> extends BaseEventListener<T> {

	private OrderManagementExecutionActionService orderManagementExecutionActionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(OrderPlacementFilledEvent.ORDER_PLACEMENT_FILLED_EVENT_NAME, OrderPlacementAllocationCompleteEvent.ORDER_PLACEMENT_ALLOCATION_COMPLETE_EVENT_NAME);
	}


	@Override
	public void onEvent(T event) {
		getOrderManagementExecutionActionService().processOrderManagementExecutionAction(event.getOrderPlacementId(), event.getOrderExecutionAction());
	}
}
