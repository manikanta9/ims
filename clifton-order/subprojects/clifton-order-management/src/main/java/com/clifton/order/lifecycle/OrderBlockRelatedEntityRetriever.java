package com.clifton.order.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.search.OrderPlacementSearchForm;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderBlockRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private OrderExecutionService orderExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof OrderBlock;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		OrderPlacementSearchForm orderPlacementSearchForm = new OrderPlacementSearchForm();
		orderPlacementSearchForm.setOrderBlockId(BeanUtils.getIdentityAsLong(entity));
		return CollectionUtils.getStream(getOrderExecutionService().getOrderPlacementList(orderPlacementSearchForm)).collect(Collectors.toList());
	}
}
