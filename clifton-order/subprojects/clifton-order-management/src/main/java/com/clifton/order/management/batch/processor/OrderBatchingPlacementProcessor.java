package com.clifton.order.management.batch.processor;

import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.batch.runner.processor.BaseOrderBatchingProcessor;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.search.OrderPlacementSearchForm;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * The <code>OrderBatchingPlacementProcessor</code> is a generic processor for batching placements for sending.  By default it looks for any placements for the selected destination in a Ready for Batching execution status.
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderBatchingPlacementProcessor extends BaseOrderBatchingProcessor<OrderPlacement> {


	private OrderExecutionService orderExecutionService;

	private OrderSetupService orderSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateOrderDestination(OrderDestination orderDestination) {
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(orderDestination.getCustomColumns());
		ValidationUtils.assertTrue(orderDestination.getDestinationType().isBatched(), "Batching processor can only be selected if the order destination type uses batching.");
		ValidationUtils.assertEquals(OrderPlacement.TABLE_NAME, orderDestination.getDestinationType().getBatchSourceSystemTable(), "Selected batching processor can only be used for Order Placements.");
		ValidationUtils.assertTrue(orderDestination.getDestinationType().isExecutionVenue(), "Selected batching processor can only be used for execution venues.");
		ValidationUtils.assertNotNull(OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(orderDestination), "Missing required calendar schedule to process batching.");
	}


	@Override
	public List<OrderBatch> processOrderBatching(OrderDestination orderDestination) {
		ValidationUtils.assertTrue(orderDestination.getDestinationType().isBatched(), "Selected destination " + orderDestination.getName() + " does not support batching");
		return processOrderBatchingImpl(orderDestination);
	}


	@Transactional
	protected List<OrderBatch> processOrderBatchingImpl(OrderDestination orderDestination) {
		OrderPlacementSearchForm orderPlacementSearchForm = new OrderPlacementSearchForm();
		orderPlacementSearchForm.setOrderDestinationId(orderDestination.getId());
		orderPlacementSearchForm.setExecutionStatusId(getOrderSetupService().getOrderExecutionStatusByName(OrderExecutionStatus.READY_FOR_BATCHING).getId());
		List<OrderPlacement> placementList = getOrderExecutionService().getOrderPlacementList(orderPlacementSearchForm);
		if (!CollectionUtils.isEmpty(placementList)) {
			OrderBatch batch = getOrCreateOrderBatch(orderDestination);
			populateOrderBatchWithItems(batch, placementList);
			return CollectionUtils.createList(getOrderBatchService().saveOrderBatch(batch));
		}
		return null;
	}
}
