package com.clifton.order.management.setup.rule.execution.executor;

import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;


/**
 * Generic Rule Executor interface
 *
 * @author manderson
 */
public interface OrderManagementRuleExecutor {


	/**
	 * Returns the assignment specificity scope key
	 * This is usually the Rule Type + Rule ID, however it could be customized for specific rule types.
	 * For example Investor ID required rules, we use Rule Type + Exchange Code (required custom field value on the rule definition)
	 * <p>
	 * More than one rule type could return the same key (related rules) in order to allow grouping rules for specificity.
	 * When the key is the same, the most specific (Account > Account Issuer > Global) assignments win.  There could be more than 1 winner as long as they are at the same level of specificity.
	 */
	public String getAssignmentSpecificityScopeKey(OrderManagementRuleAssignmentExtended ruleAssignment);


	/**
	 * Returns true/false if the rule assignment applies to the {@link OrderRuleAware} object.
	 * <p>
	 * Applies filters only if corresponding entity field is populated: if security is populated, apply security filter, otherwise keep it and keep the assignment.
	 * If executing broker is populated, apply the filter.
	 */
	public boolean isAssignmentApplicable(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context);


	/**
	 * Returns true if the rule passes, false if it fails
	 */
	public boolean executeRule(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context);
}
