package com.clifton.order.management.batch.send;

import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.runner.send.OrderBatchSender;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryService;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public abstract class BaseOrderBatchSender implements OrderBatchSender {

	private SystemQueryService systemQueryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateOrderDestination(OrderDestination orderDestination) {
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(orderDestination.getCustomColumns());
		Integer systemQueryId = OrderDestinationUtils.getOrderDestinationFileSystemQueryId(orderDestination);
		ValidationUtils.assertNotNull(systemQueryId, "Missing a system query to execute to generate the file to send for order destination " + orderDestination.getName());
		SystemQuery query = getSystemQueryService().getSystemQuery(systemQueryId);
		ValidationUtils.assertEquals(orderDestination.getDestinationType().getBatchSourceSystemTable(), query.getTable(), "The System Query [" + query.getName() + "] is invalid for destination [" + orderDestination.getName() + "].  System query is required to use main table [" + orderDestination.getDestinationType().getBatchSourceSystemTable().getName() + "].");
	}
}
