package com.clifton.order.management.setup.rule.execution;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.OrderManagementDestinationMapping;
import com.clifton.order.management.setup.OrderManagementSetupService;
import com.clifton.order.management.setup.rule.OrderManagementRuleCategory;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinition;
import com.clifton.order.management.setup.rule.OrderManagementRuleService;
import com.clifton.order.management.setup.rule.execution.executor.OrderManagementRuleExecutor;
import com.clifton.order.management.setup.rule.execution.search.OrderManagementRuleExecutingBrokerCompanySearchForm;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleRetrieverService;
import com.clifton.order.management.setup.rule.specificity.OrderManagementSpecificityRuleFilter;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchCommand;
import com.clifton.order.rule.BaseOrderRuleAware;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementRuleExecutionHandlerImpl implements OrderManagementRuleExecutionHandler {

	private OrderManagementSetupService orderManagementSetupService;

	private OrderManagementRuleRetrieverService orderManagementRuleRetrieverService;
	private OrderManagementRuleService orderManagementRuleService;

	private OrderSharedService orderSharedService;

	private SystemBeanService systemBeanService;


	/**
	 * Used for rule evaluation to see which rules actually apply after exclusions for a category
	 */
	@Override
	public List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleDefinitionApplyList(OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context, short ruleCategoryId) {
		// Step 1: Get all Rule Assignments that apply to a particular OrderRuleAware entity (Order Allocation)
		// Only uses Client/Holding accounts fields on the entity to limit the list of assignments to those applicable.
		// Returns the List of Global, Holding Account Issuer, and Account specific assignments for the entity.
		List<OrderManagementRuleAssignmentExtended> ruleAssignmentExtendedList = getOrderManagementRuleAssignmentExtendedListForOrderRuleAwareAndCategory(orderRuleAware, ruleCategoryId);
		return getOrderManagementRuleDefinitionApplyListImpl(ruleAssignmentExtendedList, orderRuleAware, context);
	}


	protected List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleDefinitionApplyListImpl(List<OrderManagementRuleAssignmentExtended> ruleAssignmentExtendedList, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		// Taking the List from Step one - begins filtering that full list.  Applied separately so we can get the full list once and then keep filtering it over and over for various use cases
		Map<String, List<OrderManagementRuleAssignmentExtended>> ruleDefinitionAssignmentListMap = BeanUtils.getBeansMap(ruleAssignmentExtendedList, orderManagementRuleAssignmentExtended -> {
			// If there is a required parent include that in the key
			if (orderManagementRuleAssignmentExtended.getRuleDefinition().getRuleType().isAssignmentProhibited() && orderManagementRuleAssignmentExtended.getRuleDefinition().getRuleType().getRollupRuleType() != null) {
				return StringUtils.generateKey(BeanUtils.getBeanIdentity(orderManagementRuleAssignmentExtended.getRollupRuleDefinition()), BeanUtils.getBeanIdentity(orderManagementRuleAssignmentExtended.getRuleDefinition()));
			}
			return orderManagementRuleAssignmentExtended.getRuleDefinition().getId() + "";
		});

		// Remove dupes (ranking) and anything excluded
		List<OrderManagementRuleAssignmentExtended> filteredList = new ArrayList<>();
		for (Map.Entry<String, List<OrderManagementRuleAssignmentExtended>> ruleDefinitionEntry : ruleDefinitionAssignmentListMap.entrySet()) {
			List<OrderManagementRuleAssignmentExtended> ruleDefinitionAssignmentList = ruleDefinitionEntry.getValue();
			// Find the Lowest Rank (Highest Precedence) Sorted by Excluded = true
			OrderManagementRuleAssignmentExtended extendedAssignment = CollectionUtils.getFirstElement(CollectionUtils.sort(ruleDefinitionAssignmentList, (o1, o2) -> {
				// Ranking
				if (o1.getRuleScope().getRanking() < o2.getRuleScope().getRanking()) {
					return -1;
				}
				if (o1.getRuleScope().getRanking() > o2.getRuleScope().getRanking()) {
					return 1;
				}
				// If same ranking, then if one is excluded that wins
				if (o1.isExcluded()) {
					return -1;
				}
				return 0;
			}));

			// Step 2: Do not include if assignment doesn't apply to the Order Rule Aware object.
			// Applies filters only if corresponding entity field is populated: if security is populated, apply security filter, otherwise keep it and keep the assignment.
			// If executing broker is populated, apply the filter.
			if (extendedAssignment != null && !extendedAssignment.isExcluded()) {
				OrderManagementRuleDefinition ruleDefinition = extendedAssignment.getRuleDefinition();
				OrderManagementRuleExecutor ruleExecutor = context.getOrderManagementRuleExecutor(ruleDefinition, getSystemBeanService());
				if (ruleExecutor.isAssignmentApplicable(extendedAssignment, orderRuleAware, context)) {
					filteredList.add(extendedAssignment);
				}
			}
		}

		// Step 3: Group this list into smaller lists by Assignment Specificity Scope Key
		Map<String, List<OrderManagementRuleAssignmentExtended>> assignmentSpecificityScopeMap = BeanUtils.getBeansMap(filteredList, assignmentExtended -> {
			OrderManagementRuleExecutor ruleExecutor = context.getOrderManagementRuleExecutor(assignmentExtended.getRuleDefinition(), getSystemBeanService());
			return ruleExecutor.getAssignmentSpecificityScopeKey(assignmentExtended);
		});
		// For each sub list that has more than one assignment using specificity: remove less specific assignments and keep most specific assignments. Could be more than one.
		filteredList = new ArrayList<>();
		for (Map.Entry<String, List<OrderManagementRuleAssignmentExtended>> assignmentSpecificityScopeEntry : assignmentSpecificityScopeMap.entrySet()) {
			if (CollectionUtils.getSize(assignmentSpecificityScopeEntry.getValue()) == 1) {
				filteredList.addAll(assignmentSpecificityScopeEntry.getValue());
			}
			else {
				// Find the Lowest Rank (Highest Precedence)
				OrderManagementRuleAssignmentExtended topRanked = CollectionUtils.getFirstElement(CollectionUtils.sort(assignmentSpecificityScopeEntry.getValue(),
						Comparator.comparingInt(o -> o.getRuleScope().getRanking())));
				// Include all for the first ranking
				AssertUtils.assertNotNull(topRanked, "Unexpected state. Null pointer when dereferencing variable [topRanked]");
				filteredList.addAll(BeanUtils.filter(assignmentSpecificityScopeEntry.getValue(), assignmentExtended -> assignmentExtended.getRuleScope().getRanking(), topRanked.getRuleScope().getRanking()));
			}
		}

		// Step 4: More filtering for Specificity within the rule (i.e. Security Type -> Hierarchy -> Security) where most specific one should win

		Map<SystemBean, List<OrderManagementRuleAssignmentExtended>> specificityFilterListMap = BeanUtils.getBeansMap(BeanUtils.filter(filteredList, orderManagementRuleAssignmentExtended -> orderManagementRuleAssignmentExtended.getRuleDefinition().getRuleType().getRuleSpecificityFilterBean() != null), orderManagementRuleAssignmentExtended -> orderManagementRuleAssignmentExtended.getRuleDefinition().getRuleType().getRuleSpecificityFilterBean());
		// Add all that don't apply additional specificity scope
		List<OrderManagementRuleAssignmentExtended> finalList = new ArrayList<>(BeanUtils.filter(filteredList, orderManagementRuleAssignmentExtended -> orderManagementRuleAssignmentExtended.getRuleDefinition().getRuleType().getRuleSpecificityFilterBean() == null));
		for (Map.Entry<SystemBean, List<OrderManagementRuleAssignmentExtended>> specificityFilterMapEntry : specificityFilterListMap.entrySet()) {
			OrderManagementSpecificityRuleFilter ruleFilter = context.getOrderManagementRuleFilter(specificityFilterMapEntry.getKey(), getSystemBeanService());
			finalList.addAll(ruleFilter.applySpecificityFiltering(specificityFilterMapEntry.getValue(), orderRuleAware, context));
		}

		return finalList;
	}


	private List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleAssignmentExtendedListForOrderRuleAwareAndCategory(OrderRuleAware orderRuleAware, short ruleCategoryId) {
		return getOrderManagementRuleRetrieverService().getOrderManagementRuleAssignmentExtendedListForAccountAndCategory(orderRuleAware, ruleCategoryId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Rule Category Preview                           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderManagementRuleExecutionResult> getOrderManagementRuleExecutionResultList(OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context, boolean returnFailedOnly) {
		List<OrderManagementRuleExecutionResult> resultList = new ArrayList<>();
		List<OrderManagementRuleCategory> categoryList = getOrderManagementRuleService().getOrderManagementRuleCategoryListByRollup(false);
		for (OrderManagementRuleCategory ruleCategory : categoryList) {
			resultList.addAll(getOrderManagementRuleExecutionResultListForCategory(orderRuleAware, ruleCategory, context, returnFailedOnly));
		}
		return resultList;
	}


	@Override
	public List<OrderManagementRuleExecutionResult> getOrderManagementRuleExecutionResultListForCategory(OrderRuleAware orderRuleAware, OrderManagementRuleCategory ruleCategory, OrderRuleEvaluatorContext context, boolean returnFailedOnly) {
		// For Executing Broker Rules: If none selected on the OrderRuleAware object, we skip execution
		if (OrderManagementRuleCategory.EXECUTING_BROKER_RULES.equalsIgnoreCase(ruleCategory.getName())) {
			if (orderRuleAware.getExecutingBrokerCompany() == null) {
				return new ArrayList<>();
			}
		}

		List<OrderManagementRuleAssignmentExtended> assignmentExtendedList = getOrderManagementRuleDefinitionApplyList(orderRuleAware, context, ruleCategory.getId());
		List<OrderManagementRuleExecutionResult> resultList = getOrderManagementRuleExecutionResultListImpl(orderRuleAware, assignmentExtendedList, context, returnFailedOnly);

		// If it's a currency order, then we need to evaluate rules against both currencies
		BaseOrderRuleAware settlementCurrencyOrderRuleAware = BaseOrderRuleAware.forOrderRuleAwareSettlementCurrency(orderRuleAware);
		if (settlementCurrencyOrderRuleAware != null) {
			assignmentExtendedList = getOrderManagementRuleDefinitionApplyList(settlementCurrencyOrderRuleAware, context, ruleCategory.getId());
			List<OrderManagementRuleExecutionResult> settlementCurrencyResultList = getOrderManagementRuleExecutionResultListImpl(settlementCurrencyOrderRuleAware, assignmentExtendedList, context, returnFailedOnly);
			return CollectionUtils.combineCollections(resultList, settlementCurrencyResultList);
		}
		return resultList;
	}


	private List<OrderManagementRuleExecutionResult> getOrderManagementRuleExecutionResultListImpl(OrderRuleAware orderRuleAware, List<OrderManagementRuleAssignmentExtended> assignmentExtendedList, OrderRuleEvaluatorContext context, boolean returnFailedOnly) {
		List<OrderManagementRuleExecutionResult> resultList = new ArrayList<>();
		for (OrderManagementRuleAssignmentExtended assignmentExtended : assignmentExtendedList) {
			OrderManagementRuleExecutor ruleExecutor = context.getOrderManagementRuleExecutor(assignmentExtended.getRuleDefinition(), getSystemBeanService());
			boolean allowed = ruleExecutor.executeRule(assignmentExtended, orderRuleAware, context);
			if (!allowed || !returnFailedOnly) {
				resultList.add(new OrderManagementRuleExecutionResult(assignmentExtended.getRuleDefinition(), allowed));
			}
		}
		return resultList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////          Executing Broker Rule Custom Handling             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerList(OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context, OrderManagementRuleExecutingBrokerCompanySearchForm searchForm) {
		validateOrderAllocationSelections(orderRuleAware, true);

		OrderManagementRuleCategory ruleCategory = getOrderManagementRuleService().getOrderManagementRuleCategoryByName(OrderManagementRuleCategory.EXECUTING_BROKER_RULES);
		List<OrderManagementRuleAssignmentExtended> assignmentExtendedList = getOrderManagementRuleAssignmentExtendedListForOrderRuleAwareAndCategory(orderRuleAware, ruleCategory.getId());

		BaseOrderRuleAware settlementCurrencyOrderRuleAware = BaseOrderRuleAware.forOrderRuleAwareSettlementCurrency(orderRuleAware);
		List<OrderManagementRuleAssignmentExtended> settlementCurrencyAssignmentExtendedList = (settlementCurrencyOrderRuleAware == null ? null : getOrderManagementRuleAssignmentExtendedListForOrderRuleAwareAndCategory(settlementCurrencyOrderRuleAware, ruleCategory.getId()));

		// What is available based on Destination mappings
		OrderManagementDestinationMappingSearchCommand command = OrderManagementDestinationMappingSearchCommand.forOrderTypeAndActiveOnDateAndDestination(orderRuleAware.getOrderType().getId(), orderRuleAware.getTradeDate(), searchForm.getOrderDestinationId());

		List<OrderManagementDestinationMapping> mappingList = getOrderManagementSetupService().getOrderManagementDestinationMappingListForCommand(command);
		if (CollectionUtils.isEmpty(mappingList)) {
			return Collections.emptyList();
		}

		OrderCompany[] availableCompanies = BeanUtils.getPropertyValuesUniqueExcludeNull(mappingList, OrderManagementDestinationMapping::getExecutingBrokerCompany, OrderCompany.class);

		// If no executing broker supplied, then loop through all available (via destination mappings) and determine if they are allowed for the account
		if (orderRuleAware.getExecutingBrokerCompany() == null) {
			List<OrderCompany> filteredList = new ArrayList<>();
			for (OrderCompany orderCompany : availableCompanies) {
				BaseOrderRuleAware companyOrderRuleAware = BaseOrderRuleAware.forOrderRuleAware(orderRuleAware);
				companyOrderRuleAware.setExecutingBrokerCompany(orderCompany);
				List<OrderManagementRuleAssignmentExtended> filteredAssignmentExtendedList = getOrderManagementRuleDefinitionApplyListImpl(assignmentExtendedList, companyOrderRuleAware, context);
				if (CollectionUtils.isEmpty(getOrderManagementRuleExecutionResultListImpl(companyOrderRuleAware, filteredAssignmentExtendedList, context, true))) {
					// If passes - check settlement currency rules (if applies)
					if (settlementCurrencyOrderRuleAware != null) {
						companyOrderRuleAware = BaseOrderRuleAware.forOrderRuleAware(settlementCurrencyOrderRuleAware);
						companyOrderRuleAware.setExecutingBrokerCompany(orderCompany);
						filteredAssignmentExtendedList = getOrderManagementRuleDefinitionApplyListImpl(settlementCurrencyAssignmentExtendedList, companyOrderRuleAware, context);
						if (CollectionUtils.isEmpty(getOrderManagementRuleExecutionResultListImpl(companyOrderRuleAware, filteredAssignmentExtendedList, context, true))) {
							filteredList.add(orderCompany);
						}
					}
					else {
						filteredList.add(orderCompany);
					}
				}
			}
			return new PagingArrayList<>(BeanUtils.sortWithFunction(filteredList, OrderCompany::getName, true));
		}
		// Otherwise - validate selected is valid and exists in the company list
		if (ArrayUtils.contains(availableCompanies, orderRuleAware.getExecutingBrokerCompany())) {
			List<OrderManagementRuleAssignmentExtended> filteredAssignmentExtendedList = getOrderManagementRuleDefinitionApplyListImpl(assignmentExtendedList, orderRuleAware, context);
			if (CollectionUtils.isEmpty(getOrderManagementRuleExecutionResultListImpl(orderRuleAware, filteredAssignmentExtendedList, context, true))) {
				// If passes - check settlement currency rules (if applies)
				if (settlementCurrencyOrderRuleAware != null) {
					filteredAssignmentExtendedList = getOrderManagementRuleDefinitionApplyListImpl(settlementCurrencyAssignmentExtendedList, settlementCurrencyOrderRuleAware, context);
					if (CollectionUtils.isEmpty(getOrderManagementRuleExecutionResultListImpl(settlementCurrencyOrderRuleAware, filteredAssignmentExtendedList, context, true))) {
						return new PagingArrayList<>(CollectionUtils.createList(orderRuleAware.getExecutingBrokerCompany()));
					}
				}
				else {
					return new PagingArrayList<>(CollectionUtils.createList(orderRuleAware.getExecutingBrokerCompany()));
				}
			}
		}
		// Nothing applies - return null
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	private void validateOrderAllocationSelections(OrderRuleAware orderRuleAware, boolean requireSecurity) {
		ValidationUtils.assertNotNull(orderRuleAware, " OrderRuleAware object is null.");
		ValidationUtils.assertNotNull(orderRuleAware.getClientAccount(), "Client Account selection is required.");
		ValidationUtils.assertNotNull(orderRuleAware.getHoldingAccount(), "Holding Account selection is required.");
		ValidationUtils.assertNotNull(orderRuleAware.getTradeDate(), "Trade Date is required to find which date rules are active on.");
		ValidationUtils.assertNotNull(orderRuleAware.getOrderType(), "Order Type is required.");
		if (requireSecurity) {
			ValidationUtils.assertNotNull(orderRuleAware.getSecurity(), "Order Security is required.");
		}
	}
}
