package com.clifton.order.management.rule;

import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


/**
 * Will generate rule violations for an Order Allocation when there exists at least one other not canceled order allocation for the same client account, holding account, trade date, security, and settlement currency.
 *
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderRuleAwareDuplicateRuleEvaluator implements RuleEvaluator<OrderAllocation, OrderRuleEvaluatorContext> {

	private OrderAllocationService orderAllocationService;

	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(OrderAllocation orderAllocation, RuleConfig ruleConfig, OrderRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {

			OrderAllocationSearchForm orderAllocationSearchForm = new OrderAllocationSearchForm();
			orderAllocationSearchForm.setClientAccountId(orderAllocation.getClientAccount().getId());
			orderAllocationSearchForm.setHoldingAccountId(orderAllocation.getHoldingAccount().getId());
			orderAllocationSearchForm.setTradeDate(orderAllocation.getTradeDate());
			orderAllocationSearchForm.setSecurityId(orderAllocation.getSecurity().getId());
			orderAllocationSearchForm.setSettlementCurrencyId(orderAllocation.getSettlementCurrency().getId());
			orderAllocationSearchForm.setExcludeWorkflowStateName(OrderAllocation.WORKFLOW_STATE_CANCELED);
			orderAllocationSearchForm.setIdNotEquals(orderAllocation.getId());
			List<OrderAllocation> orderAllocationList = getOrderAllocationService().getOrderAllocationList(orderAllocationSearchForm);

			if (!(orderAllocationList.isEmpty())) {
				orderAllocationList.forEach(duplicateOrderAllocation -> ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, orderAllocation.getIdentity(), duplicateOrderAllocation.getId())));
			}
		}
		return ruleViolationList;
	}
}
