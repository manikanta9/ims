package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleAssignmentSearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleCategorySearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleDefinitionRollupSearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleDefinitionSearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleTypeSearchForm;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderManagementRuleServiceImpl implements OrderManagementRuleService {


	private AdvancedUpdatableDAO<OrderManagementRuleCategory, Criteria> orderManagementRuleCategoryDAO;
	private AdvancedUpdatableDAO<OrderManagementRuleType, Criteria> orderManagementRuleTypeDAO;
	private AdvancedUpdatableDAO<OrderManagementRuleDefinition, Criteria> orderManagementRuleDefinitionDAO;
	private AdvancedUpdatableDAO<OrderManagementRuleDefinitionRollup, Criteria> orderManagementRuleDefinitionRollupDAO;
	private AdvancedUpdatableDAO<OrderManagementRuleAssignment, Criteria> orderManagementRuleAssignmentDAO;

	private DaoNamedEntityCache<OrderManagementRuleCategory> orderManagementRuleCategoryCache;
	private DaoSingleKeyListCache<OrderManagementRuleCategory, Boolean> orderManagementRuleCategoryByRollupCache;
	private DaoNamedEntityCache<OrderManagementRuleType> orderManagementRuleTypeCache;
	private DaoSingleKeyListCache<OrderManagementRuleDefinitionRollup, Integer> orderManagementRuleDefinitionRollupListByParentCache;

	private OrderSharedService orderSharedService;

	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderManagementRuleCategory getOrderManagementRuleCategory(short id) {
		return getOrderManagementRuleCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderManagementRuleCategory getOrderManagementRuleCategoryByName(String name) {
		return getOrderManagementRuleCategoryCache().getBeanForKeyValue(getOrderManagementRuleCategoryDAO(), name);
	}


	@Override
	public List<OrderManagementRuleCategory> getOrderManagementRuleCategoryList(OrderManagementRuleCategorySearchForm searchForm) {
		return getOrderManagementRuleCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<OrderManagementRuleCategory> getOrderManagementRuleCategoryListByRollup(boolean rollup) {
		return getOrderManagementRuleCategoryByRollupCache().getBeanListForKeyValue(getOrderManagementRuleCategoryDAO(), rollup);
	}


	@Override
	public OrderManagementRuleCategory saveOrderManagementRuleCategory(OrderManagementRuleCategory bean) {
		return getOrderManagementRuleCategoryDAO().save(bean);
	}


	@Override
	public void deleteOrderManagementRuleCategory(short id) {
		getOrderManagementRuleCategoryDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderManagementRuleType getOrderManagementRuleType(short id) {
		return getOrderManagementRuleTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderManagementRuleType getOrderManagementRuleTypeByName(String name) {
		return getOrderManagementRuleTypeCache().getBeanForKeyValue(getOrderManagementRuleTypeDAO(), name);
	}


	@Override
	public List<OrderManagementRuleType> getOrderManagementRuleTypeList(OrderManagementRuleTypeSearchForm searchForm) {
		return getOrderManagementRuleTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderManagementRuleType saveOrderManagementRuleType(OrderManagementRuleType bean) {
		return getOrderManagementRuleTypeDAO().save(bean);
	}


	@Override
	public void deleteOrderManagementRuleType(short id) {
		getOrderManagementRuleTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderManagementRuleDefinition getOrderManagementRuleDefinition(int id) {
		return getOrderManagementRuleDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderManagementRuleDefinition> getOrderManagementRuleDefinitionList(OrderManagementRuleDefinitionSearchForm searchForm) {
		return getOrderManagementRuleDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Transactional
	@Override
	public OrderManagementRuleDefinition saveOrderManagementRuleDefinition(OrderManagementRuleDefinition bean) {
		boolean regenerateName = false;
		if (!StringUtils.isEmpty(bean.getRuleType().getRuleDefinitionNameTemplate())) {
			if (bean.isNewBean()) {
				regenerateName = true;
				// Custom Columns doesn't work well to generate name until it's saved
				bean.setName(UUID.randomUUID().toString());
			}
			else {
				bean.setName(previewOrderManagementRuleDefinitionNameImpl(bean));
			}
		}
		bean = getOrderManagementRuleDefinitionDAO().save(bean);
		if (regenerateName) {
			bean.setName(previewOrderManagementRuleDefinitionNameImpl(bean));
			OrderManagementRuleDefinition finalBean = bean;
			// Save without auditing since we are changing from system generated name to actual name
			return SystemAuditDaoUtils.executeWithAuditingDisabled(new String[]{"name"}, () -> getOrderManagementRuleDefinitionDAO().save(finalBean));
		}
		return bean;
	}


	@Transactional
	@Override
	public OrderManagementRuleDefinition copyOrderManagementRuleDefinition(OrderManagementRuleDefinition bean) {
		OrderManagementRuleDefinition existingRuleDefinition = getOrderManagementRuleDefinition(bean.getId());
		OrderManagementRuleDefinition newRuleDefinition = BeanUtils.cloneBean(existingRuleDefinition, false, false);
		newRuleDefinition.setName(bean.getName());
		newRuleDefinition.setDescription(bean.getDescription());
		newRuleDefinition.setCustomColumns(bean.getCustomColumns());
		newRuleDefinition = saveOrderManagementRuleDefinition(newRuleDefinition);
		if (existingRuleDefinition.isRollup()) {
			List<OrderManagementRuleDefinitionRollup> rollupList = getOrderManagementRuleDefinitionRollupListByParent(existingRuleDefinition.getId());
			if (!CollectionUtils.isEmpty(rollupList)) {
				List<OrderManagementRuleDefinitionRollup> newRollupList = new ArrayList<>();
				for (OrderManagementRuleDefinitionRollup existingRollup : rollupList) {
					OrderManagementRuleDefinitionRollup newRollup = BeanUtils.cloneBean(existingRollup, false, false);
					newRollup.setReferenceOne(newRuleDefinition);
					newRollupList.add(newRollup);
				}
				getOrderManagementRuleDefinitionRollupDAO().saveList(newRollupList);
			}
		}
		return newRuleDefinition;
	}


	@Transactional
	@Override
	public void deleteOrderManagementRuleDefinition(int id) {
		OrderManagementRuleDefinition ruleDefinition = getOrderManagementRuleDefinition(id);
		if (ruleDefinition.isRollup()) {
			getOrderManagementRuleDefinitionRollupDAO().deleteList(getOrderManagementRuleDefinitionRollupListByParent(id));
		}
		getOrderManagementRuleDefinitionDAO().delete(id);
	}


	@Override
	public String getOrderManagementRuleDefinitionForName(int ruleDefinitionId) {
		return previewOrderManagementRuleDefinitionNameImpl(getOrderManagementRuleDefinition(ruleDefinitionId));
	}


	protected String previewOrderManagementRuleDefinitionNameImpl(OrderManagementRuleDefinition ruleDefinition) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(ruleDefinition.getRuleType().getRuleDefinitionNameTemplate()), "Rule Definition Template Name is required to Preview");
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(ruleDefinition.getCustomColumns());
		TemplateConfig templateConfig = new TemplateConfig(ruleDefinition.getRuleType().getRuleDefinitionNameTemplate());
		templateConfig.addBeanToContext("bean", ruleDefinition);
		String result = getTemplateConverter().convert(templateConfig);
		return result;
	}


	@Override
	@Transactional
	public void generateOrderManagementRuleDefinition(Integer[] ruleDefinitionIds) {
		// Get the list of rule definitions
		//iterate through the list - get the updated name for each and set it and save it.
		generateOrderManagementRuleDefinitionNameImpl(getOrderManagementRuleDefinitionDAO().findByPrimaryKeys(ruleDefinitionIds));
	}


	protected void generateOrderManagementRuleDefinitionNameImpl(List<OrderManagementRuleDefinition> ruleDefinitionList) {
		// Get the list of rule definitions
		//iterate through the list - get the updated name for each and set it and save it.

		List<OrderManagementRuleDefinition> saveRuleDefinitionList = new ArrayList<>();
		for (OrderManagementRuleDefinition orderManagementRuleDefinition : CollectionUtils.getIterable(ruleDefinitionList)) {

			String name = previewOrderManagementRuleDefinitionNameImpl(orderManagementRuleDefinition);
			if (!StringUtils.isEqual(orderManagementRuleDefinition.getName(), name)) {
				orderManagementRuleDefinition.setName(name);
				saveRuleDefinitionList.add(orderManagementRuleDefinition);
			}
		}
		getOrderManagementRuleDefinitionDAO().saveList(saveRuleDefinitionList);
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderManagementRuleDefinitionRollup> getOrderManagementRuleDefinitionRollupListByParent(int parentId) {
		return getOrderManagementRuleDefinitionRollupListByParentCache().getBeanListForKeyValue(getOrderManagementRuleDefinitionRollupDAO(), parentId);
	}


	@Override
	public List<OrderManagementRuleDefinitionRollup> getOrderManagementRuleDefinitionRollupList(OrderManagementRuleDefinitionRollupSearchForm searchForm) {
		return getOrderManagementRuleDefinitionRollupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderManagementRuleDefinitionRollup linkOrderManagementRuleDefinitionRollup(int parentRuleDefinitionId, int childRuleDefinitionId) {
		OrderManagementRuleDefinition parent = getOrderManagementRuleDefinition(parentRuleDefinitionId);
		ValidationUtils.assertTrue(parent.isRollup(), "Parent Rule Definition is not a Rollup.");

		OrderManagementRuleDefinition child = getOrderManagementRuleDefinition(childRuleDefinitionId);
		ValidationUtils.assertFalse(child.isRollup(), "Child Rule Definition is a rollup.  A rollup rule cannot contain other rollup rules.");

		if (child.getRuleType().getRollupRuleType() != null) {
			ValidationUtils.assertEquals(parent.getRuleType(), child.getRuleType().getRollupRuleType(), "Child Rule " + child.getName() + "] can only be a child of " + child.getRuleType().getRollupRuleType().getName() + " rollup rules.");
		}

		OrderManagementRuleDefinitionRollup rollup = new OrderManagementRuleDefinitionRollup();
		rollup.setReferenceOne(parent);
		rollup.setReferenceTwo(child);
		return getOrderManagementRuleDefinitionRollupDAO().save(rollup);
	}


	@Override
	public void unlinkOrderManagementRuleDefinitionRollup(int parentRuleDefinitionId, int childRuleDefinitionId) {
		OrderManagementRuleDefinitionRollup rollup = getOrderManagementRuleDefinitionRollupDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{parentRuleDefinitionId, childRuleDefinitionId});
		if (rollup != null) {
			getOrderManagementRuleDefinitionRollupDAO().delete(rollup);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderManagementRuleAssignment getOrderManagementRuleAssignment(int id) {
		return getOrderManagementRuleAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderManagementRuleAssignment> getOrderManagementRuleAssignmentList(OrderManagementRuleAssignmentSearchForm searchForm) {

		return getOrderManagementRuleAssignmentDAO().findBySearchCriteria(new BaseOrderManagementRuleAssignmentSearchFormConfigurer(searchForm, getOrderSharedService()) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getRuleDefinitionIdOrChildId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(OrderManagementRuleDefinitionRollup.class, "rollup");
					sub.setProjection(Projections.property("id"));
					String ruleDefinitionAlias = getPathAlias("ruleDefinition", criteria);
					sub.add(Restrictions.eqProperty("rollup.referenceOne.id", ruleDefinitionAlias + ".id"));
					sub.add(Restrictions.eq("rollup.referenceTwo.id", searchForm.getRuleDefinitionIdOrChildId()));

					criteria.add(Restrictions.or(Restrictions.eq("ruleDefinition.id", searchForm.getRuleDefinitionIdOrChildId()), Subqueries.exists(sub)));
				}
			}
		});
	}


	@Override
	public OrderManagementRuleAssignment saveOrderManagementRuleAssignment(OrderManagementRuleAssignment bean) {
		ValidationUtils.assertFalse(bean.getRuleDefinition().getRuleType().isAssignmentProhibited(), "Rule Assignments are prohibited for rule type " + bean.getRuleDefinition().getRuleType().getName());
		if (OrderManagementRuleAssignmentScopes.ACCOUNT == bean.getRuleScope()) {
			ValidationUtils.assertNotNull(bean.getClientAccount(), "Client Account is required");
			ValidationUtils.assertNotNull(bean.getHoldingAccount(), "Holding Account is required");
			ValidationUtils.assertTrue(CompareUtils.isEqual(bean.getClientAccount().getClientCompany(), bean.getHoldingAccount().getClientCompany()), "Client Account and Holding Account selection must both belong to the same client company.");
		}
		return getOrderManagementRuleAssignmentDAO().save(bean);
	}


	@Override
	public void deleteOrderManagementRuleAssignment(int id) {
		getOrderManagementRuleAssignmentDAO().delete(id);
	}
}
