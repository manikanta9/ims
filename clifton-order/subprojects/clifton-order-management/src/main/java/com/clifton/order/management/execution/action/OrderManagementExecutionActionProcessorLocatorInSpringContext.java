package com.clifton.order.management.execution.action;


import com.clifton.core.util.AssertUtils;
import com.clifton.order.setup.OrderExecutionActions;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
@Getter
@Setter
public class OrderManagementExecutionActionProcessorLocatorInSpringContext implements OrderManagementExecutionActionProcessorLocator, InitializingBean, ApplicationContextAware {

	private final Map<OrderExecutionActions, OrderManagementExecutionActionProcessor> processorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, OrderManagementExecutionActionProcessor> beanMap = getApplicationContext().getBeansOfType(OrderManagementExecutionActionProcessor.class);

		// need a map with InvestmentSecurityAllocationTypes as keys instead of bean names
		for (Map.Entry<String, OrderManagementExecutionActionProcessor> stringOrderManagementExecutionActionProcessorEntry : beanMap.entrySet()) {
			OrderManagementExecutionActionProcessor processor = stringOrderManagementExecutionActionProcessorEntry.getValue();
			for (OrderExecutionActions executionAction : processor.getOrderExecutionActionSet()) {
				if (getProcessorMap().containsKey(executionAction)) {
					throw new RuntimeException("Cannot register '" + stringOrderManagementExecutionActionProcessorEntry.getKey() + "' as a processor for order management execution action  '" + executionAction
							+ "' because this activity action type already has a registered processor.");
				}
				getProcessorMap().put(executionAction, processor);
			}
		}
	}


	@Override
	public OrderManagementExecutionActionProcessor locate(OrderExecutionActions executionAction) {
		AssertUtils.assertNotNull(executionAction, "Required execution action cannot be null.");
		OrderManagementExecutionActionProcessor result = getProcessorMap().get(executionAction);
		AssertUtils.assertNotNull(result, "Cannot locate OrderManagementExecutionActionProcessor for '%1s' execution action.", executionAction);
		return result;
	}
}
