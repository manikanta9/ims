package com.clifton.order.management.activity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.management.activity.action.OrderManagementActivityActionContext;
import com.clifton.order.management.activity.action.OrderManagementActivityActionProcessorLocator;
import com.clifton.order.management.activity.action.OrderManagementActivityActions;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderManagementActivityServiceImpl implements OrderManagementActivityService {


	private OrderAllocationService orderAllocationService;

	private OrderManagementActivityActionProcessorLocator orderManagementActivityActionProcessorLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processOrderManagementActivityForOrderAllocations(OrderManagementActivityCommand command) {
		if (ArrayUtils.isEmpty(command.getOrderAllocationIds())) {
			if (command.getOrderAllocationSearchCommand() == null) {
				throw new ValidationException("No order allocations selected to perform activity on.");
			}
			else {
				// Make sure at least one of the following search fields is being filtered on:
				if (!command.getOrderAllocationSearchCommand().isSearchRestrictionSetAny("orderDestinationId", "orderDestinationIdOrNull", "securityId", "workflowStatusName", "tradeDate", "workflowStatusNames")) {
					throw new ValidationException("No order allocations selected to perform activity on and not enough search criteria supplied to use to find applicable order allocations.  Required to filter on at least Order Destination, Security, Workflow Status, and/or Trade Date.");
				}
				List<OrderAllocation> orderAllocationList = getOrderAllocationService().getOrderAllocationList(command.getOrderAllocationSearchCommand());
				ValidationUtils.assertFalse(CollectionUtils.isEmpty(orderAllocationList), "No order allocations available for selected filters to perform activity on.");
				command.setOrderAllocationIds(BeanUtils.getBeanIdentityArray(orderAllocationList, Long.class));
			}
		}
		OrderManagementActivityActionContext context = new OrderManagementActivityActionContext();
		for (OrderManagementActivityActions activityAction : CollectionUtils.getIterable(getActionListForCommand(command))) {
			getOrderManagementActivityActionProcessorLocator().locate(activityAction).processOrderAllocationActivityAction(command, context);
		}
	}


	@Override
	public void processOrderManagementReviseDatesForOrderAllocations(OrderManagementActivityCommand command) {
		processOrderManagementActivityForOrderAllocations(command);
	}


	@Override
	public void processOrderManagementActivityForOrderBlocks(OrderManagementActivityCommand command) {
		if (ArrayUtils.isEmpty(command.getOrderBlockIds())) {
			throw new ValidationException("No block orders selected to perform activity on.");
		}
		OrderManagementActivityActionContext context = new OrderManagementActivityActionContext();
		for (OrderManagementActivityActions activityAction : CollectionUtils.getIterable(getActionListForCommand(command))) {
			getOrderManagementActivityActionProcessorLocator().locate(activityAction).processOrderBlockActivityAction(command, context);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private List<OrderManagementActivityActions> getActionListForCommand(OrderManagementActivityCommand command) {
		List<OrderManagementActivityActions> activityActions = new ArrayList<>();
		if (command.isReviseDates()) {
			activityActions.add((OrderManagementActivityActions.REVISE_DATES));
		}
		if (command.getAssignTraderUserId() != null) {
			activityActions.add(OrderManagementActivityActions.ASSIGN_TRADER);
		}
		if (command.isCreateBlockOrders()) {
			activityActions.add(OrderManagementActivityActions.CREATE_BLOCK_ORDERS);
		}
		if (command.isCreatePlacements()) {
			activityActions.add(OrderManagementActivityActions.CREATE_PLACEMENTS);
		}
		if (command.isSendPlacements()) {
			activityActions.add(OrderManagementActivityActions.SEND_PLACEMENTS);
		}
		if (command.isUnblockOrderAllocations()) {
			activityActions.add(OrderManagementActivityActions.UNBLOCK_ORDERS);
		}
		return activityActions;
	}
}
