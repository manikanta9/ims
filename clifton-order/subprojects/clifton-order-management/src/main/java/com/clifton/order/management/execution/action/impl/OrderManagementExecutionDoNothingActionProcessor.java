package com.clifton.order.management.execution.action.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderExecutionActions;
import org.springframework.stereotype.Component;

import java.util.Set;


/**
 * The <code>OrderManagementExecutionStatusChangeActionProcessor</code> does nothing, just used to change the status based on next execution status
 * which is handled by the base class
 *
 * @author manderson
 */
@Component
public class OrderManagementExecutionDoNothingActionProcessor extends BaseOrderManagementExecutionActionProcessor {


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.REJECT_CANCEL);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		// DO NOTHING
	}
}
