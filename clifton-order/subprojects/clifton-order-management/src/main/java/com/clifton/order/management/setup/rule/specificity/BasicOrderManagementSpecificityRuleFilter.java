package com.clifton.order.management.setup.rule.specificity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionUtils;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.core.util.MathUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BasicOrderManagementSpecificityRuleFilter</code> applies specificity logic based on a set of defined field levels
 * The HIGHEST level will win.
 * Optional ability to group on another field value (i.e. Executing Broker security specificity applies to each executing broker individually)
 *
 * @author manderson
 */
public class BasicOrderManagementSpecificityRuleFilter implements OrderManagementSpecificityRuleFilter {

	public static final int SPECIFICITY_DEFAULT = 1;
	public static final int SPECIFICITY_5 = 2;
	public static final int SPECIFICITY_4 = 4;
	public static final int SPECIFICITY_3 = 8;
	public static final int SPECIFICITY_2 = 16;
	public static final int SPECIFICITY_1 = 32;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If groupingPropertyCustomColumn:
	 * The system column that is used to group like restrictions together
	 * For example, for ExecutingBrokerRules if we are searching for a list, we need to
	 * evaluate rules that apply to a specific executing broker together and not in combination with rules for other executing brokers.
	 * Since we use JSON for custom values, we only need the name of the column to pull the value
	 * <p>
	 * Otherwise if not grouping by a system column value, then we can group on a bean property.
	 * i.e. if the value is on the assignment....
	 * This is evaluated against the OrderManagementRuleAssignmentExtended object.
	 */
	private String groupingPropertyName;

	private boolean groupingPropertyCustomColumn;

	/**
	 * The following defines the specificity columns/properties
	 * where level 1 is most specific.
	 * Again specificityLevelXPropertyIsCustomColumn is used to look up custom column value vs. bean property value on the OrderManagementRuleAssignmentExtended object.
	 */
	private String specificityLevel1PropertyName;
	private boolean specificityLevel1CustomColumn;

	private String specificityLevel2PropertyName;
	private boolean specificityLevel2CustomColumn;


	private String specificityLevel3PropertyName;
	private boolean specificityLevel3CustomColumn;

	private String specificityLevel4PropertyName;
	private boolean specificityLevel4CustomColumn;

	private String specificityLevel5PropertyName;
	private boolean specificityLevel5CustomColumn;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<OrderManagementRuleAssignmentExtended> applySpecificityFiltering(List<OrderManagementRuleAssignmentExtended> assignmentExtendedList, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		Map<Object, List<OrderManagementRuleAssignmentExtended>> groupingSpecificityListMap = new HashMap<>();
		Map<Object, Integer> groupingSpecificityLevelMap = new HashMap<>();

		for (OrderManagementRuleAssignmentExtended ruleAssignmentExtended : CollectionUtils.getIterable(assignmentExtendedList)) {
			Object groupingValue = getGroupingValue(ruleAssignmentExtended);
			Integer specificityLevel = getSpecificityLevel(ruleAssignmentExtended);

			Integer currentSpecificityLevel = groupingSpecificityLevelMap.getOrDefault(groupingValue, null);
			if (currentSpecificityLevel == null || specificityLevel > currentSpecificityLevel) {
				groupingSpecificityLevelMap.put(groupingValue, specificityLevel);
				groupingSpecificityListMap.put(groupingValue, CollectionUtils.createList(ruleAssignmentExtended));
			}
			else if (MathUtils.isEqual(currentSpecificityLevel, specificityLevel)) {
				groupingSpecificityListMap.get(groupingValue).add(ruleAssignmentExtended);
			}
		}

		// Only include NONE if there is nothing else defined...
		if (groupingSpecificityListMap.containsKey("NONE") && groupingSpecificityListMap.size() > 1) {
			groupingSpecificityListMap.remove("NONE");
		}

		return CollectionUtils.combineCollectionOfCollections(groupingSpecificityListMap.values());
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Helper Methods                               ////////
	////////////////////////////////////////////////////////////////////////////


	protected Object getGroupingValue(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		Object groupingValue = getValue(ruleAssignmentExtended, getGroupingPropertyName(), isGroupingPropertyCustomColumn());
		if (groupingValue == null) {
			return "NONE";
		}
		return groupingValue;
	}


	protected Integer getSpecificityLevel(OrderManagementRuleAssignmentExtended ruleAssignmentExtended) {
		int specificityLevel = 0;
		Object level1Value = getValue(ruleAssignmentExtended, getSpecificityLevel1PropertyName(), isSpecificityLevel1CustomColumn());
		specificityLevel = (level1Value != null) ? specificityLevel | SPECIFICITY_1 : specificityLevel;
		Object level2Value = getValue(ruleAssignmentExtended, getSpecificityLevel2PropertyName(), isSpecificityLevel2CustomColumn());
		specificityLevel = (level2Value != null) ? specificityLevel | SPECIFICITY_2 : specificityLevel;
		Object level3Value = getValue(ruleAssignmentExtended, getSpecificityLevel3PropertyName(), isSpecificityLevel3CustomColumn());
		specificityLevel = (level3Value != null) ? specificityLevel | SPECIFICITY_3 : specificityLevel;
		Object level4Value = getValue(ruleAssignmentExtended, getSpecificityLevel4PropertyName(), isSpecificityLevel4CustomColumn());
		specificityLevel = (level4Value != null) ? specificityLevel | SPECIFICITY_4 : specificityLevel;
		Object level5Value = getValue(ruleAssignmentExtended, getSpecificityLevel5PropertyName(), isSpecificityLevel5CustomColumn());
		specificityLevel = (level5Value != null) ? specificityLevel | SPECIFICITY_5 : specificityLevel;
		return specificityLevel;
	}


	private Object getValue(OrderManagementRuleAssignmentExtended ruleAssignmentExtended, String propertyName, boolean customColumn) {
		if (StringUtils.isEmpty(propertyName)) {
			return null;
		}
		if (customColumn) {
			return OrderManagementRuleExecutionUtils.getRuleAssignmentCustomFieldValue(ruleAssignmentExtended, propertyName);
		}
		return BeanUtils.getPropertyValue(ruleAssignmentExtended, propertyName);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setters                          ////////
	////////////////////////////////////////////////////////////////////////////


	public String getGroupingPropertyName() {
		return this.groupingPropertyName;
	}


	public void setGroupingPropertyName(String groupingPropertyName) {
		this.groupingPropertyName = groupingPropertyName;
	}


	public boolean isGroupingPropertyCustomColumn() {
		return this.groupingPropertyCustomColumn;
	}


	public void setGroupingPropertyCustomColumn(boolean groupingPropertyCustomColumn) {
		this.groupingPropertyCustomColumn = groupingPropertyCustomColumn;
	}


	public String getSpecificityLevel1PropertyName() {
		return this.specificityLevel1PropertyName;
	}


	public void setSpecificityLevel1PropertyName(String specificityLevel1PropertyName) {
		this.specificityLevel1PropertyName = specificityLevel1PropertyName;
	}


	public boolean isSpecificityLevel1CustomColumn() {
		return this.specificityLevel1CustomColumn;
	}


	public void setSpecificityLevel1CustomColumn(boolean specificityLevel1CustomColumn) {
		this.specificityLevel1CustomColumn = specificityLevel1CustomColumn;
	}


	public String getSpecificityLevel2PropertyName() {
		return this.specificityLevel2PropertyName;
	}


	public void setSpecificityLevel2PropertyName(String specificityLevel2PropertyName) {
		this.specificityLevel2PropertyName = specificityLevel2PropertyName;
	}


	public boolean isSpecificityLevel2CustomColumn() {
		return this.specificityLevel2CustomColumn;
	}


	public void setSpecificityLevel2CustomColumn(boolean specificityLevel2CustomColumn) {
		this.specificityLevel2CustomColumn = specificityLevel2CustomColumn;
	}


	public String getSpecificityLevel3PropertyName() {
		return this.specificityLevel3PropertyName;
	}


	public void setSpecificityLevel3PropertyName(String specificityLevel3PropertyName) {
		this.specificityLevel3PropertyName = specificityLevel3PropertyName;
	}


	public boolean isSpecificityLevel3CustomColumn() {
		return this.specificityLevel3CustomColumn;
	}


	public void setSpecificityLevel3CustomColumn(boolean specificityLevel3CustomColumn) {
		this.specificityLevel3CustomColumn = specificityLevel3CustomColumn;
	}


	public String getSpecificityLevel4PropertyName() {
		return this.specificityLevel4PropertyName;
	}


	public void setSpecificityLevel4PropertyName(String specificityLevel4PropertyName) {
		this.specificityLevel4PropertyName = specificityLevel4PropertyName;
	}


	public boolean isSpecificityLevel4CustomColumn() {
		return this.specificityLevel4CustomColumn;
	}


	public void setSpecificityLevel4CustomColumn(boolean specificityLevel4CustomColumn) {
		this.specificityLevel4CustomColumn = specificityLevel4CustomColumn;
	}


	public String getSpecificityLevel5PropertyName() {
		return this.specificityLevel5PropertyName;
	}


	public void setSpecificityLevel5PropertyName(String specificityLevel5PropertyName) {
		this.specificityLevel5PropertyName = specificityLevel5PropertyName;
	}


	public boolean isSpecificityLevel5CustomColumn() {
		return this.specificityLevel5CustomColumn;
	}


	public void setSpecificityLevel5CustomColumn(boolean specificityLevel5CustomColumn) {
		this.specificityLevel5CustomColumn = specificityLevel5CustomColumn;
	}
}
