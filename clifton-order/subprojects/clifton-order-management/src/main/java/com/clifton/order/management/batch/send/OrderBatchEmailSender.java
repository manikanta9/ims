package com.clifton.order.management.batch.send;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreStringUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.email.Email;
import com.clifton.core.util.email.EmailHandler;
import com.clifton.core.util.email.MimeContentTypes;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * The <code>OrderBatchEmailSender</code> is used to send the batch file via email to specified email addresses
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderBatchEmailSender extends BaseOrderBatchSender implements ValidationAware {


	private EmailHandler emailHandler;

	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<String> toEmailAddressList;
	private List<String> ccEmailAddressList;
	private List<String> bccEmailAddressList;

	private String fromEmailAddress;


	/**
	 * Freemarker template to generate the subject of the email
	 * accepts the orderBatch as a context property
	 */
	private String subjectTemplate;

	/**
	 * Freemarker template to generate the body of the email
	 * accepts the orderBatch as a context property
	 */
	private String textTemplate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getToEmailAddressList(), "At least one email address to send the email to is required");
		if (!StringUtils.isEmpty(getFromEmailAddress())) {
			ValidationUtils.assertTrue(CoreStringUtils.isEmailValid(getFromEmailAddress()), "Email Address " + getFromEmailAddress() + " is not valid.");
		}
		validateEmailAddressList(getToEmailAddressList());
		validateEmailAddressList(getCcEmailAddressList());
		validateEmailAddressList(getBccEmailAddressList());
		ValidationUtils.assertFalse(StringUtils.isEmpty(getSubjectTemplate()), "Email Subject is Required.");
	}


	private void validateEmailAddressList(List<String> emailAddressList) {
		if (!CollectionUtils.isEmpty(emailAddressList)) {
			emailAddressList.forEach(emailAddress -> ValidationUtils.assertTrue(CoreStringUtils.isEmailValid(emailAddress), "Email Address " + emailAddress + " is not valid."));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void sendOrderBatch(OrderBatch orderBatch, FileWrapper fileWrapper) {
		validate();
		ValidationUtils.assertNotNull(fileWrapper, "Missing a file for the batch to email.");

		String subject = getStringFromTemplate(getSubjectTemplate(), orderBatch);
		String body = getStringFromTemplate(getTextTemplate(), orderBatch);

		Email email = new Email(getFromEmailAddress(), getEmailAddressesAsArray(getToEmailAddressList()), getEmailAddressesAsArray(getCcEmailAddressList()), getEmailAddressesAsArray(getBccEmailAddressList()), subject, body, MimeContentTypes.TEXT_HTML, fileWrapper);
		getEmailHandler().send(email);
	}


	protected String[] getEmailAddressesAsArray(List<String> emailAddressList) {
		if (CollectionUtils.isEmpty(emailAddressList)) {
			return null;
		}
		return CollectionUtils.toArray(emailAddressList, String.class);
	}


	protected String getStringFromTemplate(String template, OrderBatch orderBatch) {
		if (StringUtils.isEmpty(template)) {
			return null;
		}
		TemplateConfig templateConfig = new TemplateConfig(template);
		templateConfig.addBeanToContext("orderBatch", orderBatch);
		return getTemplateConverter().convert(templateConfig);
	}
}
