package com.clifton.order.management.execution.upload.converter;

import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;


/**
 * @author manderson
 */
@Getter
@Setter
public class BasicOrderExecutionUploadConverter implements OrderExecutionUploadConverter {

	private OrderExecutionService orderExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Required: Used to look up the placement by id
	 */
	private String orderPlacementIdColumnName;


	/**
	 * Not required, we assume it would be fully filled so we just use the quantity intended
	 */
	private String fillQuantityColumnName;

	/**
	 * Required - for FX Orders this is the Exchange rate
	 */
	private String averageUnitPriceColumnName;

	/**
	 * File name pattern regex.
	 */
	private String fileNameRegex; //^PershingFX.*

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderPlacement getOrderPlacementForRow(DataRow row) {
		Long orderPlacementId = MathUtils.getNumberAsLong((BigDecimal) row.getValue(getOrderPlacementIdColumnName()));
		if (orderPlacementId != null) {
			return getOrderExecutionService().getOrderPlacement(orderPlacementId);
		}
		return null;
	}


	@Override
	public BigDecimal getFillQuantityForRow(OrderPlacement orderPlacement, DataRow row) {
		if (!StringUtils.isEmpty(getFillQuantityColumnName())) {
			BigDecimal filledQuantity = (BigDecimal) row.getValue(getFillQuantityColumnName());
			if (filledQuantity != null) {
				return filledQuantity;
			}
		}
		return orderPlacement.getQuantityIntended();
	}


	@Override
	public BigDecimal getAverageUnitPriceForRow(OrderPlacement orderPlacement, DataRow row) {
		return (BigDecimal) row.getValue(getAverageUnitPriceColumnName());
	}


	@Override
	public boolean isSupportedForFileName(String fileName) {
		if (!StringUtils.isEmpty(fileName) && !StringUtils.isEmpty(getFileNameRegex())) {
			return fileName.matches(getFileNameRegex());
		}
		return false;
	}
}
