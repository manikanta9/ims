package com.clifton.order.management.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


/**
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderRuleAwareAccountWorkflowStateRuleEvaluator implements RuleEvaluator<OrderRuleAware, OrderRuleEvaluatorContext>, ValidationAware {

	private RuleViolationService ruleViolationService;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<String> allowedWorkflowStateNames;


	@Override
	public void validate() throws ValidationException {

		ValidationUtils.assertNotEmpty(getAllowedWorkflowStateNames(), "Allowed workflow state cannot be empty");
	}


	@Override
	public List<RuleViolation> evaluateRule(OrderRuleAware orderRuleAware, RuleConfig ruleConfig, OrderRuleEvaluatorContext context) {

		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {

			if (CompareUtils.isEqual(orderRuleAware.getClientAccount(), orderRuleAware.getHoldingAccount())) {

				if ((!CollectionUtils.isEmpty(getAllowedWorkflowStateNames()) && !getAllowedWorkflowStateNames().contains(orderRuleAware.getClientAccount().getWorkflowState().getName()))) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, orderRuleAware.getIdentity(), orderRuleAware.getClientAccount().getId(), null));
				}
			}
			else {
				if ((!CollectionUtils.isEmpty(getAllowedWorkflowStateNames()) && !getAllowedWorkflowStateNames().contains(orderRuleAware.getHoldingAccount().getWorkflowState().getName()))) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, orderRuleAware.getIdentity(), orderRuleAware.getHoldingAccount().getId(), null));
				}
				if ((!CollectionUtils.isEmpty(getAllowedWorkflowStateNames()) && !getAllowedWorkflowStateNames().contains(orderRuleAware.getClientAccount().getWorkflowState().getName()))) {
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, orderRuleAware.getIdentity(), orderRuleAware.getClientAccount().getId(), null));
				}
			}
		}
		return ruleViolationList;
	}
}
