package com.clifton.order.management.rule;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarBusinessDayTypes;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.shared.OrderSharedUtilHandler;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>OrderRuleAwareDateRuleEvaluator</code> class can be used to run rules against the Trade Date or Settlement Date property of OrderRuleAware beans.
 * <p>
 * These can be configured to run together or separately for business day logic, date in past, present, or future checks.
 * <p>
 * Cause table on this rule violations is the Security (for drill down to more easily get to the calendar or exchange)
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderRuleAwareDateRuleEvaluator implements RuleEvaluator<OrderRuleAware, OrderRuleEvaluatorContext>, ValidationAware {

	private CalendarBusinessDayService calendarBusinessDayService;

	private OrderSharedUtilHandler orderSharedUtilHandler;

	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If false, this is a settlement date check
	 */
	private boolean validateTradeDate;

	/**
	 * If true, then checking that the date is on a valid business
	 * If violated, this is often a hard stop
	 * If validating settlement date, there is an additional check that settlement date is at least on or after trade date
	 */
	private boolean violateIfNotBusinessDay;

	/**
	 * If validating settlement date in the past or the future, compare it to the trade date vs. today
	 * i.e. warning if more than 2 business days from trade date, vs. 2 days from today.
	 */
	private boolean compareSettlementDatePastFutureToTradeDate;

	/**
	 * These can be configured and usually result in soft warnings
	 */
	private boolean violateIfDateInPast;

	/**
	 * For violateIfDateInPast, can limit to only if > max business days in past
	 */
	private int maxBusinessDaysInPast;

	private boolean violateIfDateToday;

	private boolean violateIfDateInFuture;

	/**
	 * For violateIfDateInFuture, can limit to only if > max business days in future
	 */
	private int maxBusinessDaysInFuture;

	/**
	 * The following options can be used to tailor a rule to a specific order type (or exclude an order type)
	 */
	private List<Short> includeOrderTypeIds;

	private List<Short> excludeOrderTypeIds;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(isViolateIfDateInFuture() || isViolateIfDateToday() || isViolateIfDateInPast() || isViolateIfNotBusinessDay(), "At least one violation option must be checked.");
	}


	@Override
	public List<RuleViolation> evaluateRule(OrderRuleAware ruleBean, RuleConfig ruleConfig, OrderRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			if (isApplyToOrderType(ruleBean)) {
				// If any violations, they are put into the map - then we know to generate a rule violation...
				Map<String, Object> ruleViolationTemplateContext = new HashMap<>();
				if (isValidateTradeDate()) {
					if (isViolateIfNotBusinessDay()) {
						if (!getOrderSharedUtilHandler().isValidTradeDate(ruleBean.getSecurity(), ruleBean.getTradeDate())) {
							ruleViolationTemplateContext.put("notBusinessDay", true);
						}
					}
					evaluateDateInPastTodayAndFuture(ruleBean, ruleBean.getTradeDate(), null, ruleViolationTemplateContext);
				}
				else {
					// Always validate settlement date is not before trade date
					if (DateUtils.isDateBefore(ruleBean.getSettlementDate(), ruleBean.getTradeDate(), false)) {
						ruleViolationTemplateContext.put("customMessage", "Settlement Date cannot be before Trade Date.");
					}
					if (isViolateIfNotBusinessDay()) {
						if (!getOrderSharedUtilHandler().isValidSettlementDate(ruleBean.getSecurity(), ruleBean.getSettlementCurrency(), ruleBean.getSettlementDate())) {
							ruleViolationTemplateContext.put("notBusinessDay", true);
						}
					}
					evaluateDateInPastTodayAndFuture(ruleBean, ruleBean.getSettlementDate(), (isCompareSettlementDatePastFutureToTradeDate() ? ruleBean.getTradeDate() : null), ruleViolationTemplateContext);
				}

				if (!CollectionUtils.isEmpty(ruleViolationTemplateContext)) {
					ruleViolationTemplateContext.put("dateField", isValidateTradeDate() ? "Trade Date" : "Settlement Date");
					ruleViolationTemplateContext.put("maxBusinessDaysInPast", getMaxBusinessDaysInPast());
					ruleViolationTemplateContext.put("maxBusinessDaysInFuture", getMaxBusinessDaysInFuture());
					ruleViolationTemplateContext.put("compareSettleToTrade", isCompareSettlementDatePastFutureToTradeDate());
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, ruleBean.getIdentity(), ruleBean.getSecurity().getId(), null, ruleViolationTemplateContext));
				}
			}
		}
		return ruleViolationList;
	}


	private boolean isApplyToOrderType(OrderRuleAware orderRuleAware) {
		if (getIncludeOrderTypeIds() != null && CollectionUtils.contains(getIncludeOrderTypeIds(), orderRuleAware.getOrderType().getId())) {
			return true;
		}
		if (getExcludeOrderTypeIds() != null && CollectionUtils.contains(getExcludeOrderTypeIds(), orderRuleAware.getOrderType().getId())) {
			return false;
		}
		return true;
	}


	private void evaluateDateInPastTodayAndFuture(OrderRuleAware orderRuleAware, Date date, Date compareToDate, Map<String, Object> ruleViolationTemplateContext) {
		compareToDate = (compareToDate == null ? DateUtils.clearTime(new Date()) : compareToDate);

		if (isViolateIfDateInPast()) {
			Date pastDate = getMaxBusinessDaysInPast() == 0 ? compareToDate : getBusinessDaysFrom(orderRuleAware, compareToDate, -getMaxBusinessDaysInPast());
			if (DateUtils.isDateBefore(date, pastDate, false)) {
				ruleViolationTemplateContext.put("dateInPast", true);
			}
		}
		if (isViolateIfDateToday()) {
			if (DateUtils.isEqualWithoutTime(date, new Date())) {
				ruleViolationTemplateContext.put("dateToday", true);
			}
		}
		if (isViolateIfDateInFuture()) {
			Date futureDate = getMaxBusinessDaysInFuture() == 0 ? compareToDate : getBusinessDaysFrom(orderRuleAware, compareToDate, getMaxBusinessDaysInFuture());
			if (DateUtils.isDateAfter(date, futureDate)) {
				ruleViolationTemplateContext.put("dateInFuture", true);
			}
		}
	}


	private Date getBusinessDaysFrom(OrderRuleAware orderRuleAware, Date date, int businessDays) {
		if (isValidateTradeDate()) {
			if (orderRuleAware.getSecurity() != null && orderRuleAware.getSecurity().getPrimaryExchange() != null && orderRuleAware.getSecurity().getPrimaryExchange().getCalendar() != null) {
				return getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTrade(date, orderRuleAware.getSecurity().getPrimaryExchange().getCalendar().getId()), businessDays);
			}
		}
		else {
			Calendar[] calendars = getOrderSharedUtilHandler().getSettlementCalendarsForSecurityAndSettlementCurrency(orderRuleAware.getSecurity(), orderRuleAware.getSettlementCurrency());
			if (!ArrayUtils.isEmpty(calendars)) {
				return getCalendarBusinessDayService().getBusinessDayFrom(date, businessDays, CalendarBusinessDayTypes.SETTLEMENT_BUSINESS_DAY, calendars);
			}
		}
		return DateUtils.addDays(date, businessDays);
	}
}
