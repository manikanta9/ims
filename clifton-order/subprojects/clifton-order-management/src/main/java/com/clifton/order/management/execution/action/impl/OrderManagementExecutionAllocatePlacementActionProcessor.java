package com.clifton.order.management.execution.action.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.OrderPlacementAllocation;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderAllocationStatus;
import com.clifton.order.setup.OrderExecutionActions;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
@Component
public class OrderManagementExecutionAllocatePlacementActionProcessor extends BaseOrderManagementExecutionActionProcessor {

	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.ALLOCATE);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		// Assuming placements are fully filled, fill all with average price and we do not send allocation instructions.  Just build the OrderPlacementAllocations table
		List<OrderAllocation> allocationList = getOrderAllocationListForPlacement(orderPlacement);
		List<OrderPlacementAllocation> placementAllocationList = getOrderExecutionHandler().calculateOrderPlacementAllocationListForPlacement(orderPlacement, allocationList, null);
		orderPlacement.setAllocationStatus(getOrderSetupService().getOrderAllocationStatusByName(OrderAllocationStatus.ALLOCATION_COMPLETE_NO_REPORT));
		getOrderExecutionService().saveOrderPlacementWithAllocations(orderPlacement, placementAllocationList);
	}
}
