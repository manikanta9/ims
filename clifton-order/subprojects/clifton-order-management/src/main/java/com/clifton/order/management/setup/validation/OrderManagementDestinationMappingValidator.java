package com.clifton.order.management.setup.validation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.management.setup.OrderManagementDestinationMapping;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class OrderManagementDestinationMappingValidator extends SelfRegisteringDaoValidator<OrderManagementDestinationMapping> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(OrderManagementDestinationMapping bean, DaoEventTypes config) throws ValidationException {
		// Uses DAO method
	}


	@Override
	public void validate(OrderManagementDestinationMapping bean, DaoEventTypes config, ReadOnlyDAO<OrderManagementDestinationMapping> dao) throws ValidationException {
		ValidationUtils.assertTrue(bean.getOrderDestination().getDestinationType().isExecutionVenue(), "Destination mappings can only apply to main execution venue destination types.");
		ValidationUtils.assertFalse(bean.getOrderDestination().isBackupDestination(), "Destination mappings cannot be applies to back up destinations");
		ValidationUtils.assertFalse(bean.getOrderDestination().isOfDestinationCommunicationType(DestinationCommunicationTypes.MANUAL), "Destination mappings are not valid for manual destinations");

		// Confirm not a duplicate for overlapping date range
		List<OrderManagementDestinationMapping> mappingList = dao.findByFields(new String[]{"orderType.id", "orderDestination.id", "executingBrokerCompany.id"}, new Object[]{BeanUtils.getBeanIdentity(bean.getOrderType()), BeanUtils.getBeanIdentity(bean.getOrderDestination()), BeanUtils.getBeanIdentity(bean.getExecutingBrokerCompany())});
		for (OrderManagementDestinationMapping existingMapping : CollectionUtils.getIterable(mappingList)) {
			if (!CompareUtils.isEqual(bean, existingMapping) && DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), existingMapping.getStartDate(), existingMapping.getEndDate())) {
				throw new ValidationException("There already exists an order destination mapping for the same options with an overlapping date range: " + existingMapping.getLabel() + " " + DateUtils.fromDateRange(existingMapping.getStartDate(), existingMapping.getEndDate(), true, false));
			}
		}
	}
}
