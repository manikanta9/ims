package com.clifton.order.management.execution.action.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.allocation.OrderAllocationService;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import com.clifton.order.execution.OrderExecutionHandler;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.OrderPlacementAllocation;
import com.clifton.order.execution.search.OrderPlacementAllocationSearchForm;
import com.clifton.order.fix.OrderFixService;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionProcessor;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionService;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.shared.OrderCompany;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.fieldmapping.SystemFieldMappingEntry;
import com.clifton.system.fieldmapping.SystemFieldMappingService;
import com.clifton.system.fieldmapping.search.SystemFieldMappingEntrySearchForm;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Getter
@Setter
public abstract class BaseOrderManagementExecutionActionProcessor implements OrderManagementExecutionActionProcessor {

	private OrderAllocationService orderAllocationService;

	private OrderExecutionHandler orderExecutionHandler;

	private OrderExecutionService orderExecutionService;

	private OrderFixService orderFixService;

	private OrderManagementRuleExecutionService orderManagementRuleExecutionService;

	private OrderSetupService orderSetupService;

	private SecurityAuthorizationService securityAuthorizationService;

	private SecurityUserService securityUserService;

	private SystemFieldMappingService systemFieldMappingService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final void processExecutionAction(Long orderPlacementId, OrderManagementExecutionActionCommand command) {
		OrderExecutionActions orderExecutionAction = command.getOrderExecutionAction();
		OrderPlacement orderPlacement = getOrderExecutionService().getOrderPlacement(orderPlacementId);

		validateCommand(command, orderPlacement);

		if (!CollectionUtils.isEmpty(orderExecutionAction.getRequiredExecutionStatusNames())) {
			ValidationUtils.assertTrue(orderExecutionAction.getRequiredExecutionStatusNames().contains(orderPlacement.getExecutionStatus().getName()), "Can only " + orderExecutionAction.name() + " placements in " + StringUtils.collectionToCommaDelimitedString(orderExecutionAction.getRequiredExecutionStatusNames()) + " Statuses.");
		}

		if (orderExecutionAction.isRequireCurrentUserTraderOrAdmin()) {
			SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
			if (!CompareUtils.isEqual(currentUser, orderPlacement.getOrderBlock().getTraderUser())) {
				ValidationUtils.assertTrue(getSecurityAuthorizationService().isSecurityUserAdmin(), "Only the trader of the selected placement (or admin) can " + orderExecutionAction.name() + " selected placement");
			}
		}

		processExecutionActionImpl(orderPlacement, command);

		if (!StringUtils.isEmpty(orderExecutionAction.getNextExecutionStatusName())) {
			orderPlacement.setExecutionStatus(getOrderSetupService().getOrderExecutionStatusByName(orderExecutionAction.getNextExecutionStatusName()));
			getOrderExecutionService().saveOrderPlacement(orderPlacement);
		}
	}


	protected abstract void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command);


	protected void validateCommand(OrderManagementExecutionActionCommand command, OrderPlacement orderPlacement) {
		// By default do nothing.  Should only be overridden for those actions that require a command object for additional details
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected List<OrderAllocation> getOrderAllocationListForPlacement(OrderPlacement orderPlacement) {
		OrderAllocationSearchForm searchForm = new OrderAllocationSearchForm();
		searchForm.setOrderBlockIdentifier(orderPlacement.getOrderBlock().getId());
		return getOrderAllocationService().getOrderAllocationList(searchForm);
	}


	protected List<OrderPlacementAllocation> getOrderPlacementAllocationListForPlacement(OrderPlacement orderPlacement) {
		OrderPlacementAllocationSearchForm searchForm = new OrderPlacementAllocationSearchForm();
		searchForm.setOrderPlacementId(orderPlacement.getId());
		return getOrderExecutionService().getOrderPlacementAllocationList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected List<String> getExecutingBrokerCompanyCodeListForPlacement(OrderPlacement orderPlacement) {
		List<OrderCompany> companyList = orderPlacement.getExecutingBrokerCompany() != null ? CollectionUtils.createList(orderPlacement.getExecutingBrokerCompany()) : getOrderManagementRuleExecutionService().getOrderManagementRuleExecutingBrokerCompanyListForOrderPlacement(orderPlacement.getId());

		if ((orderPlacement.getOrderDestination().getOrderCompanyMapping()) != null) {
			SystemFieldMappingEntrySearchForm searchForm = new SystemFieldMappingEntrySearchForm();
			searchForm.setSystemFieldMappingId(orderPlacement.getOrderDestination().getOrderCompanyMapping().getId());
			Map<Long, SystemFieldMappingEntry> executingBrokerDestinationMappingMap = BeanUtils.getBeanMap(getSystemFieldMappingService().getSystemFieldMappingEntryList(searchForm), SystemFieldMappingEntry::getFkFieldId);

			List<String> executingBrokerCodeList = new ArrayList<>();
			for (OrderCompany company : CollectionUtils.getIterable(companyList)) {
				String executingBrokerCode = null;
				if (executingBrokerDestinationMappingMap.containsKey(MathUtils.getNumberAsLong(company.getId()))) {
					executingBrokerCode = executingBrokerDestinationMappingMap.get(MathUtils.getNumberAsLong(company.getId())).getMappingValue();
				}

				executingBrokerCodeList.add(StringUtils.coalesce(executingBrokerCode, company.getName()));
			}
			return executingBrokerCodeList;
		}
		else {
			return Arrays.asList(BeanUtils.getPropertyValues(companyList, OrderCompany::getName, String.class));
		}
	}
}
