package com.clifton.order.management.execution.action;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.setup.OrderExecutionActions;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderManagementExecutionActionServiceImpl implements OrderManagementExecutionActionService {


	private OrderManagementExecutionActionProcessorLocator orderManagementExecutionActionProcessorLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Transactional
	@Override
	public void processOrderManagementExecutionAction(long orderPlacementId, OrderExecutionActions orderExecutionAction) {
		ValidationUtils.assertNotNull(orderExecutionAction, "Execution Action is required to perform on Placement " + orderPlacementId);
		OrderManagementExecutionActionCommand command = OrderManagementExecutionActionCommand.forExecutionAction(orderExecutionAction);
		processOrderManagementExecutionActionForCommand(orderPlacementId, command, false);
	}


	@Transactional
	@Override
	public void processOrderManagementExecutionActionForCommand(long orderPlacementId, OrderManagementExecutionActionCommand command, boolean ignoreValidation) {
		ValidationUtils.assertNotNull(command.getOrderExecutionAction(), "Execution Action is required to perform on Placement " + orderPlacementId);
		getOrderManagementExecutionActionProcessorLocator().locate(command.getOrderExecutionAction()).processExecutionAction(orderPlacementId, command);
	}
}
