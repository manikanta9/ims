package com.clifton.order.management.execution.upload;

import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.ExcelFileToDataTableConverter;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusWithCounts;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.system.note.DocumentSystemNoteService;
import com.clifton.order.execution.OrderExecutionHandler;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.event.OrderPlacementExecutionUpdateEvent;
import com.clifton.order.management.execution.upload.converter.OrderExecutionUploadConverter;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanGroup;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderExecutionUploadServiceImpl implements OrderExecutionUploadService {

	private static final String ORDER_PLACEMENT_EXECUTION_UPLOAD_BEAN_GROUP_NAME = "Order Placement Execution Upload Converter";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DocumentSystemNoteService documentSystemNoteService;

	private EventHandler eventHandler;

	private OrderExecutionHandler orderExecutionHandler;

	private OrderExecutionService orderExecutionService;

	private OrderSharedMarketDataService orderSharedMarketDataService;

	private SystemBeanService systemBeanService;

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status uploadOrderPlacementExecutionFile(OrderPlacementExecutionUploadCommand uploadCommand) {
		ValidationUtils.assertNotNull(uploadCommand.getOrderExecutionUploadConverterBean(), "Upload Converter Bean is required");
		ValidationUtils.assertNotNull(uploadCommand.getFile(), "Missing upload file.");

		// First: Read Excel file or pre existing Workbook and convert it into a DataTable
		ExcelFileToDataTableConverter excelConverter = new ExcelFileToDataTableConverter();
		excelConverter.setSheetIndex(uploadCommand.getSheetIndex());
		DataTable dataTable = excelConverter.convert(uploadCommand.getFile());

		if (dataTable == null || dataTable.getTotalRowCount() == 0 || dataTable.getColumnCount() == 0) {
			throw new ValidationException("No data available to import.");
		}

		OrderExecutionUploadConverter converter = (OrderExecutionUploadConverter) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(uploadCommand.getOrderExecutionUploadConverterBean().getId()));

		StatusWithCounts statusWithCounts = new StatusWithCounts();
		Set<OrderPlacement> checkedPlacements = new HashSet<>();
		Map<Long, OrderPlacementExecutionUpdateEvent> placementUpdateMap = new HashMap<>();
		int rowNumber = 1; // Header Row
		for (DataRow row : dataTable.getRowList()) {
			rowNumber++;
			OrderPlacement orderPlacement = converter.getOrderPlacementForRow(row);
			if (orderPlacement == null) {
				statusWithCounts.addError("Cannot find a placement for row " + rowNumber);
				continue;
			}
			if (checkedPlacements.contains(orderPlacement)) {
				placementUpdateMap.remove(orderPlacement.getId());
				statusWithCounts.addError("Error Processing row " + rowNumber + ": The same placement [" + orderPlacement.getId() + ": " + orderPlacement.getLabelShort() + "] was found for multiple rows in the file.  Placements should be unique per row.");
				continue;
			}
			checkedPlacements.add(orderPlacement);
			if (orderPlacement.getExecutionStatus().isFillComplete()) {
				statusWithCounts.addSkipped("Skipping Placement [" + orderPlacement.getId() + ": " + orderPlacement.getLabelShort() + "].  Placement is already fully filled.  Use Amend Execution feature to apply changes.");
				continue;
			}
			BigDecimal quantityFilled = converter.getFillQuantityForRow(orderPlacement, row);
			BigDecimal averageUnitPrice = converter.getAverageUnitPriceForRow(orderPlacement, row);
			if (averageUnitPrice == null) {
				statusWithCounts.addError("Error Processing row " + rowNumber + ": Missing Price for placement [" + orderPlacement.getId() + ": " + orderPlacement.getLabelShort() + "].");
				continue;
			}
			if (!MathUtils.isEqual(quantityFilled, orderPlacement.getQuantityFilled()) || !MathUtils.isEqual(averageUnitPrice, orderPlacement.getAverageUnitPrice())) {
				OrderPlacementExecutionUpdateEvent updateEvent = new OrderPlacementExecutionUpdateEvent(orderPlacement.getId());
				updateEvent.setQuantityFilled(quantityFilled);
				updateEvent.setAverageUnitPrice(averageUnitPrice);
				if (MathUtils.isEqual(orderPlacement.getQuantityIntended(), quantityFilled)) {
					updateEvent.setNewExecutionStatusName(OrderExecutionStatus.FILLED);
				}
				else if (!MathUtils.isNullOrZero(quantityFilled)) {
					updateEvent.setNewExecutionStatusName(OrderExecutionStatus.PARTIALLY_FILLED);
				}
				if (!validateRowForWarnings(uploadCommand, statusWithCounts, orderPlacement, updateEvent)) {
					continue;
				}
				placementUpdateMap.put(orderPlacement.getId(), updateEvent);
			}
			else {
				statusWithCounts.addSkipped("Skipping Placement [" + orderPlacement.getId() + ": " + orderPlacement.getLabelShort() + "].  No changes to apply.");
			}
		}
		if (uploadCommand.isFailUploadIfAnyWarningsOrErrors() && (statusWithCounts.getErrorCount() > 0 || statusWithCounts.getWarningCount() > 0)) {
			statusWithCounts.setMessageWithErrors("Errors and/or Warnings found.  No Placements updated. " + statusWithCounts.getCountMessage("Placements"), 5);
			return statusWithCounts;
		}
		return saveOrderPlacementExecutionList(uploadCommand, placementUpdateMap, statusWithCounts);
	}


	private boolean validateRowForWarnings(OrderPlacementExecutionUploadCommand uploadCommand, StatusWithCounts statusWithCounts, OrderPlacement orderPlacement, OrderPlacementExecutionUpdateEvent updateEvent) {
		if (uploadCommand.isWarnIfNotFullyFilled() && !MathUtils.isEqual(updateEvent.getQuantityFilled(), orderPlacement.getQuantityIntended())) {
			statusWithCounts.addWarning(" Placement [" + orderPlacement.getId() + ": " + orderPlacement.getLabelShort() + "] is not fully filled.  File Execution has fill quantity of " + CoreMathUtils.formatNumberDecimal(updateEvent.getQuantityFilled()));
			return false;
		}
		if (uploadCommand.getWarnPriceThreshold() != null) {
			try {
				getOrderExecutionHandler().validateOrderExecutionPrice(orderPlacement, orderPlacement.getAverageUnitPrice(), uploadCommand.getWarnPriceThreshold());
			}
			catch (UserIgnorableValidationException e) {
				statusWithCounts.addWarning(e.getMessage());
			}
		}
		return true;
	}


	@Transactional
	protected StatusWithCounts saveOrderPlacementExecutionList(OrderPlacementExecutionUploadCommand uploadCommand, Map<Long, OrderPlacementExecutionUpdateEvent> placementUpdateMap, StatusWithCounts statusWithCounts) {
		if (!placementUpdateMap.isEmpty()) {
			ValidationUtils.assertTrue(uploadCommand.getNote() != null && uploadCommand.getNote().getNoteType() != null, "Note type is required for execution uploads.");
			if (uploadCommand.getNote().getNoteType() != null) {
				SystemNote uploadNote = uploadCommand.getNote();
				uploadNote.setNoteType(getSystemNoteService().getSystemNoteType(uploadNote.getNoteType().getId()));
				uploadNote.setFkFieldId(CollectionUtils.getFirstElement(placementUpdateMap.keySet())); // Needs to be associated with one and then linked to all
				getSystemNoteService().saveSystemNoteWithLinks(uploadNote, placementUpdateMap.keySet().toArray(new Long[0]));
				// Attach Document Appropriately
				FileUploadWrapper fileUploadWrapper = new FileUploadWrapper();
				fileUploadWrapper.setFile(uploadCommand.getFile());
				getDocumentSystemNoteService().saveDocumentSystemNoteAttachment(uploadNote, fileUploadWrapper);
				statusWithCounts.addMessage(uploadNote.getNoteType().getName() + " Note with attachment saved and associated with updated placements.");
			}
			for (Map.Entry<Long, OrderPlacementExecutionUpdateEvent> updateEventEntry : placementUpdateMap.entrySet()) {
				getEventHandler().raiseEvent(updateEventEntry.getValue());
				statusWithCounts.incrementProcessCountWithMessage("Updated execution for Placement ID " + updateEventEntry.getValue().getOrderPlacementId());
			}
		}
		statusWithCounts.setMessageWithErrors("Placement Execution Upload Processing Complete. " + (MathUtils.isNullOrZero(statusWithCounts.getProcessCount()) ? "No Placements Updated." : statusWithCounts.getCountMessage("Placements")), 5);
		return statusWithCounts;
	}


	/**
	 * Loops through all beans of given System Bean Group and validate the file name -
	 * if none or more than one found - return null - if only one found return that one
	 */
	@Override
	public SystemBean getOrderExecutionUploadConverterBeanForFileName(String fileName) {

		List<SystemBean> systemBeanList = getOrderPlacementExecutionUploadConverterSystemBeanList();
		// loop through list of system beans and check if the fileName is valid
		List<SystemBean> result1 = CollectionUtils.getFiltered(systemBeanList, systemBean -> ((OrderExecutionUploadConverter) getSystemBeanService().getBeanInstance(systemBean)).isSupportedForFileName(fileName));
		int size = CollectionUtils.getSize(result1);
		if (size == 1) {
			return result1.get(0);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<SystemBean> getOrderPlacementExecutionUploadConverterSystemBeanList() {
		SystemBeanSearchForm systemBeanSearchForm = new SystemBeanSearchForm();
		SystemBeanGroup systemBeanGroup = getSystemBeanService().getSystemBeanGroupByName(ORDER_PLACEMENT_EXECUTION_UPLOAD_BEAN_GROUP_NAME);
		if (ObjectUtils.isNotNullPresent(systemBeanGroup)) {
			systemBeanSearchForm.setGroupId(systemBeanGroup.getId());

			return getSystemBeanService().getSystemBeanList(systemBeanSearchForm);
		}
		else {
			return Collections.emptyList();
		}
	}
}
