package com.clifton.order.management.execution.event;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.exchangerate.api.client.model.CurrencyConventionData;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.event.OrderPlacementExecutionUpdateEvent;
import com.clifton.order.management.setup.OrderManagementDestinationMapping;
import com.clifton.order.management.setup.OrderManagementSetupService;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchCommand;
import com.clifton.order.note.OrderNoteService;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.marketdata.OrderSharedMarketDataService;
import com.clifton.system.fieldmapping.SystemFieldMappingEntry;
import com.clifton.system.fieldmapping.SystemFieldMappingService;
import com.clifton.system.fieldmapping.search.SystemFieldMappingEntrySearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


/**
 * The <code>OrderPlacementExecutionUpdateEventListener</code> listens for events that update placements during execution process (usually the receipt of ExecutionReports via FIX messages) and updates the placements as necessary.
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderPlacementExecutionUpdateEventListener extends BaseEventListener<OrderPlacementExecutionUpdateEvent> {

	private final static String REJECTION_REASON_NOTE_TYPE_NAME = "Rejection Reason";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderExecutionService orderExecutionService;
	private OrderSetupService orderSetupService;
	private OrderSharedMarketDataService orderSharedMarketDataService;
	private OrderSharedService orderSharedService;
	private SystemFieldMappingService systemFieldMappingService;
	private OrderManagementSetupService orderManagementSetupService;
	private OrderNoteService orderNoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return OrderPlacementExecutionUpdateEvent.ORDER_PLACEMENT_EXECUTION_UPDATE_EVENT_NAME;
	}


	@Override
	public void onEvent(OrderPlacementExecutionUpdateEvent event) {
		OrderPlacement orderPlacement = getOrderExecutionService().getOrderPlacement(event.getOrderPlacementId());
		ValidationUtils.assertNotNull(orderPlacement, "Cannot find order placement with ID " + event.getOrderPlacementId());
		if (event.isSkipUpdateIfCanceled() && orderPlacement.getExecutionStatus().isCanceled()) {
			return;
		}
		if (!StringUtils.isEmpty(event.getNewExecutionStatusName())) {
			orderPlacement.setExecutionStatus(getOrderSetupService().getOrderExecutionStatusByName(event.getNewExecutionStatusName()));
		}
		if ((event.getQuantityFilled() != null) && ((orderPlacement.getQuantityFilled() == null) || event.getQuantityFilled().compareTo(orderPlacement.getQuantityFilled()) > 0)) {
			orderPlacement.setQuantityFilled(event.getQuantityFilled());

			if (orderPlacement.getOrderBlock().getSecurity().isCurrency()) {
				// If we are returned an execution report that priced the currency in the opposite direction of what we expect, we have to reverse the (1/value) the price so settlement calculations will work correctly
				boolean reverse = false;
				if (!StringUtils.isEmpty(event.getTicker())) {
					CurrencyConventionData currencyConventionData = getOrderSharedMarketDataService().getOrderMarketDataCurrencyConvention(orderPlacement.getOrderBlock().getSecurity().getTicker(), orderPlacement.getOrderBlock().getSettlementCurrency().getTicker());
					if (!event.getTicker().startsWith(currencyConventionData.getDominantCurrencyCode())) {
						reverse = true;
					}
				}
				orderPlacement.setAverageUnitPrice((reverse ? MathUtils.divide(BigDecimal.ONE, event.getAverageUnitPrice()) : event.getAverageUnitPrice()));
			}
			else {
				orderPlacement.setAverageUnitPrice(event.getAverageUnitPrice());
			}
		}

		if (event.getExecutingBrokerCompany() != null) {
			orderPlacement.setExecutingBrokerCompany(event.getExecutingBrokerCompany());
		}
		else if (!StringUtils.isEmpty(event.getExecutingBrokerCode())) {
			if ((orderPlacement.getOrderDestination().getOrderCompanyMapping() != null)) {
				SystemFieldMappingEntrySearchForm searchForm = new SystemFieldMappingEntrySearchForm();
				searchForm.setMappingValue(event.getExecutingBrokerCode());
				searchForm.setSystemFieldMappingId(orderPlacement.getOrderDestination().getOrderCompanyMapping().getId());
				SystemFieldMappingEntry mapping = CollectionUtils.getFirstElement(getSystemFieldMappingService().getSystemFieldMappingEntryList(searchForm));
				if (mapping != null) {
					orderPlacement.setExecutingBrokerCompany(getOrderSharedService().getOrderCompany(MathUtils.getNumberAsInteger(mapping.getFkFieldId())));
				}
			}
			if (orderPlacement.getExecutingBrokerCompany() == null) {
				ValidationUtils.assertNotNull(orderPlacement.getOrderBlock().getOrderType().getId(), "Order Type is required.");
				OrderManagementDestinationMappingSearchCommand command = OrderManagementDestinationMappingSearchCommand.forOrderTypeAndExecutingBrokerCompanyNameAndDestination(orderPlacement.getOrderBlock().getOrderType().getId(), event.getExecutingBrokerCode(), orderPlacement.getOrderDestination().getId());
				OrderManagementDestinationMapping mapping = CollectionUtils.getFirstElement(getOrderManagementSetupService().getOrderManagementDestinationMappingListForCommand(command));
				if (mapping != null) {
					orderPlacement.setExecutingBrokerCompany(mapping.getExecutingBrokerCompany());
				}
			}
		}
		orderPlacement = getOrderExecutionService().saveOrderPlacement(orderPlacement);

		if (!StringUtils.isEmpty(event.getRejectionReason())) {
			getOrderNoteService().saveOrderPlacementNote(orderPlacement.getId(), REJECTION_REASON_NOTE_TYPE_NAME, event.getRejectionReason());
		}
	}
}
