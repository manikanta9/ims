package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.order.management.setup.rule.search.BaseOrderManagementRuleAssignmentSearchForm;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderSharedService;
import com.clifton.order.shared.search.OrderAccountSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.List;


/**
 * @author manderson
 */
public class BaseOrderManagementRuleAssignmentSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final OrderSharedService orderSharedService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseOrderManagementRuleAssignmentSearchFormConfigurer(BaseEntitySearchForm searchForm, OrderSharedService orderSharedService) {
		super(searchForm);
		this.orderSharedService = orderSharedService;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		BaseOrderManagementRuleAssignmentSearchForm searchForm = (BaseOrderManagementRuleAssignmentSearchForm) getSortableSearchForm();

		if (searchForm.getGlobalRuleAssignment() != null) {
			if (BooleanUtils.isTrue(searchForm.getGlobalRuleAssignment())) {
				criteria.add(Restrictions.and(Restrictions.isNull("clientAccount.id"), Restrictions.isNull("holdingAccountIssuingCompany.id")));
			}
			else {
				criteria.add(Restrictions.or(Restrictions.isNotNull("clientAccount.id"), Restrictions.isNotNull("holdingAccountIssuingCompany.id")));
			}
		}

		if (searchForm.getAssignedAccountId() != null || searchForm.getAssignedClientAccountId() != null || searchForm.getAssignedHoldingAccountId() != null) {
			Disjunction disjunction = Restrictions.disjunction();

			if (searchForm.getAssignedAccountId() != null || searchForm.getAssignedClientAccountId() == null || searchForm.getAssignedHoldingAccountId() == null) {
				Integer accountId = ObjectUtils.coalesce(searchForm.getAssignedAccountId(), searchForm.getAssignedClientAccountId(), searchForm.getAssignedHoldingAccountId());
				OrderAccount orderAccount = this.orderSharedService.getOrderAccount(accountId);
				if (orderAccount.isClientAccount()) {
					disjunction.add(Restrictions.eq("clientAccount.id", orderAccount.getId()));
				}
				else {
					// TODO CACHE THIS - DOES NOT CURRENTLY APPLY TO SEATTLE ACCOUNTS THAT ARE 1:1
					OrderAccountSearchForm clientAccountSearchForm = new OrderAccountSearchForm();
					clientAccountSearchForm.setClientCompanyId(orderAccount.getClientCompany().getId());
					clientAccountSearchForm.setClientAccount(true);
					List<OrderAccount> accountList = this.orderSharedService.getOrderAccountList(clientAccountSearchForm);
					if (!CollectionUtils.isEmpty(accountList)) {
						disjunction.add(accountList.size() == 1 ? (Restrictions.eq("clientAccount.id", accountList.get(0).getId())) : (Restrictions.in("clientAccount.id", BeanUtils.getBeanIdentityArray(accountList, Integer.class))));
					}
				}
				if (orderAccount.isHoldingAccount()) {
					disjunction.add(Restrictions.eq("holdingAccount.id", orderAccount.getId()));
					disjunction.add(Restrictions.eq("holdingAccountIssuingCompany.id", orderAccount.getIssuingCompany().getId()));
				}
				else {
					// TODO CACHE THIS - DOES NOT CURRENTLY APPLY TO SEATTLE ACCOUNTS THAT ARE 1:1
					OrderAccountSearchForm holdingAccountSearchForm = new OrderAccountSearchForm();
					holdingAccountSearchForm.setClientCompanyId(orderAccount.getClientCompany().getId());
					holdingAccountSearchForm.setHoldingAccount(true);
					List<OrderAccount> accountList = this.orderSharedService.getOrderAccountList(holdingAccountSearchForm);
					if (!CollectionUtils.isEmpty(accountList)) {
						disjunction.add(accountList.size() == 1 ? (Restrictions.eq("holdingAccount.id", accountList.get(0).getId())) : (Restrictions.in("holdingAccount.id", BeanUtils.getBeanIdentityArray(accountList, Integer.class))));
					}
					Integer[] issuingCompanyIds = BeanUtils.getPropertyValuesUniqueExcludeNull(accountList, account -> account.getIssuingCompany().getId(), Integer.class);
					disjunction.add(issuingCompanyIds.length == 1 ? (Restrictions.eq("holdingAccountIssuingCompany.id", issuingCompanyIds[0])) : (Restrictions.in("holdingAccountIssuingCompany.id", issuingCompanyIds)));
				}
			}
			else {
				disjunction.add(Restrictions.and(Restrictions.eq("clientAccount.id", searchForm.getAssignedClientAccountId()), Restrictions.eq("holdingAccount.id", searchForm.getAssignedHoldingAccountId())));
				OrderAccount holdingAccount = this.orderSharedService.getOrderAccount(searchForm.getAssignedHoldingAccountId());
				disjunction.add(Restrictions.eq("holdingAccountIssuingCompany.id", holdingAccount.getIssuingCompany().getId()));
			}
			if (BooleanUtils.isTrue(searchForm.getAccountIncludeGlobal())) {
				disjunction.add(Restrictions.and(Restrictions.isNull("clientAccount.id"), Restrictions.isNull("holdingAccountIssuingCompany.id")));
			}
			criteria.add(disjunction);
		}
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(getSortableSearchForm().getOrderBy());
		if (!CollectionUtils.isEmpty(orderByList)) {
			return super.configureOrderBy(criteria);
		}
		// Default Sorting - Global, then Company, Then Account
		criteria.addOrder(Order.asc(getPathAlias("clientAccount", criteria, JoinType.LEFT_OUTER_JOIN) + ".accountShortName"));
		criteria.addOrder(Order.asc(getPathAlias("holdingAccount", criteria, JoinType.LEFT_OUTER_JOIN) + ".accountName"));
		criteria.addOrder(Order.asc(getPathAlias("holdingAccountIssuingCompany", criteria, JoinType.LEFT_OUTER_JOIN) + ".name"));
		return true;
	}
}
