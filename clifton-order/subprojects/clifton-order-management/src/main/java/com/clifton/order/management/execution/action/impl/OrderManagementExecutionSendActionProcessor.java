package com.clifton.order.management.execution.action.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.management.execution.action.OrderManagementExecutionActionCommand;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.setup.destination.DestinationCommunicationTypes;
import org.springframework.stereotype.Component;

import java.util.Set;


/**
 * @author manderson
 */
@Component
public class OrderManagementExecutionSendActionProcessor extends BaseOrderManagementExecutionActionProcessor {


	@Override
	public Set<OrderExecutionActions> getOrderExecutionActionSet() {
		return CollectionUtils.createHashSet(OrderExecutionActions.SEND, OrderExecutionActions.FORCE_RESEND);
	}


	@Override
	protected void processExecutionActionImpl(OrderPlacement orderPlacement, OrderManagementExecutionActionCommand command) {
		if (orderPlacement.isOfDestinationCommunicationType(DestinationCommunicationTypes.FIX)) {
			getOrderFixService().sendOrderPlacement(orderPlacement, getOrderAllocationListForPlacement(orderPlacement), getExecutingBrokerCompanyCodeListForPlacement(orderPlacement));
		}
		else {
			throw new ValidationException("Sending Order Placements for " + orderPlacement.getOrderDestination().getDestinationType().getName() + " is not currently supported");
		}
	}
}
