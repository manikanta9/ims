package com.clifton.order.management.setup.rule.execution.executor;


import com.clifton.core.util.MathUtils;


/**
 * @author manderson
 */
public abstract class BaseOrderManagementRuleExecutor implements OrderManagementRuleExecutor {


	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                           ////////
	////////////////////////////////////////////////////////////////////////////


	protected boolean isRuleValueNotApplyToContext(Number contextValue, Number ruleValue) {
		if (ruleValue != null && contextValue != null) {
			return !MathUtils.isEqual(contextValue, ruleValue);
		}
		return false;
	}
}
