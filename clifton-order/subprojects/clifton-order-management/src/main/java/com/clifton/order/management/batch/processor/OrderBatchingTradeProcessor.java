package com.clifton.order.management.batch.processor;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.batch.runner.processor.BaseOrderBatchingProcessor;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import com.clifton.order.trade.OrderTrade;
import com.clifton.order.trade.OrderTradeService;
import com.clifton.order.trade.search.OrderTradeSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>OrderBatchingTradeProcessor</code> is a generic processor for batching trades for sending.
 * <p>
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderBatchingTradeProcessor extends BaseOrderBatchingProcessor<OrderTrade> implements ValidationAware {


	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	private OrderTradeService orderTradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Required.  Used to limit the trade date to on or after calculated start date
	 */
	private DateGenerationOptions startDateGenerationOption;

	/**
	 * Used by the startDateGenerationOption which may require a value
	 */
	private Integer startDateDayCount;


	/**
	 * Required.  Used to limit the trade date to on or before calculated end date
	 */
	private DateGenerationOptions endDateGenerationOption;

	/**
	 * Used by the endDateGenerationOption which may require a value
	 */
	private Integer endDateDayCount;

	/**
	 * Optionally limit to one or more orderTypes
	 */
	private List<Short> orderTypeIds;

	/**
	 * Optionally limit to one or more executing broker companies
	 */
	private List<Integer> executingBrokerCompanyIds;


	/**
	 * Optionally limit to one or more holding account issuers
	 */
	private List<Integer> holdingAccountIssuingCompanyIds;


	/**
	 * Likely rarely used - if false then the trade can only exist in one batch for the destination (can exist in batches for other destinations)
	 * If true, then could be included in multiple batches for the same destination.  This could be if we send a file out and it includes all cumulative trades for the trade date
	 * i.e. 10 am file, noon file includes all from 10am file + new trades since then, etc.
	 */
	private boolean allowMultipleBatchesPerDestination;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateOrderDestination(OrderDestination orderDestination) {
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(orderDestination.getCustomColumns());
		ValidationUtils.assertTrue(orderDestination.getDestinationType().isBatched(), "Batching processor can only be selected if the order destination type uses batching.");
		ValidationUtils.assertEquals(OrderTrade.TABLE_NAME, orderDestination.getDestinationType().getBatchSourceSystemTable(), "Selected batching processor can only be used for Order Trades.");
		ValidationUtils.assertFalse(orderDestination.getDestinationType().isExecutionVenue(), "Selected batching processor can not be used for execution venues.");
		ValidationUtils.assertNotNull(OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(orderDestination), "Missing required calendar schedule to process batching.");
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getStartDateGenerationOption(), "Start Date Generation Option is Required");
		if (getStartDateGenerationOption().isDayCountRequired()) {
			ValidationUtils.assertNotNull(getStartDateDayCount(), "Start Date Generation Option " + getStartDateGenerationOption().getLabel() + " requires start date day count.");
		}
		else {
			ValidationUtils.assertNull(getStartDateDayCount(), "Start Date Generation Option " + getStartDateGenerationOption().getLabel() + " does not use start date day count. Please clear the value.");
		}
		ValidationUtils.assertNotNull(getEndDateGenerationOption(), "End Date Generation Option is Required");
		if (getEndDateGenerationOption().isDayCountRequired()) {
			ValidationUtils.assertNotNull(getEndDateDayCount(), "End Date Generation Option " + getEndDateGenerationOption().getLabel() + " requires end date day count.");
		}
		else {
			ValidationUtils.assertNull(getEndDateDayCount(), "End Date Generation Option " + getEndDateGenerationOption().getLabel() + " does not use end date day count. Please clear the value.");
		}
	}


	@Override
	public List<OrderBatch> processOrderBatching(OrderDestination orderDestination) {
		ValidationUtils.assertTrue(orderDestination.getDestinationType().isBatched(), "Selected destination " + orderDestination.getName() + " does not support batching");
		return processOrderBatchingImpl(orderDestination);
	}


	@Transactional
	protected List<OrderBatch> processOrderBatchingImpl(OrderDestination orderDestination) {
		OrderTradeSearchForm orderTradeSearchForm = configureSearchForm(orderDestination);
		List<OrderTrade> tradeList = getOrderTradeService().getOrderTradeList(orderTradeSearchForm);
		if (!CollectionUtils.isEmpty(tradeList)) {
			OrderBatch batch = getOrCreateOrderBatch(orderDestination);
			populateOrderBatchWithItems(batch, tradeList);
			return CollectionUtils.createList(getOrderBatchService().saveOrderBatch(batch));
		}
		return null;
	}


	protected OrderTradeSearchForm configureSearchForm(OrderDestination orderDestination) {
		OrderTradeSearchForm orderTradeSearchForm = new OrderTradeSearchForm();
		if (!isAllowMultipleBatchesPerDestination()) {
			orderTradeSearchForm.setOrderBatchForDestinationIdMissing(orderDestination.getId());
		}
		Date startDate = getCalendarDateGenerationHandler().generateDate(getStartDateGenerationOption(), getStartDateDayCount());
		Date endDate = getCalendarDateGenerationHandler().generateDate(getEndDateGenerationOption(), getEndDateDayCount());
		orderTradeSearchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		orderTradeSearchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));

		if (!CollectionUtils.isEmpty(getOrderTypeIds())) {
			orderTradeSearchForm.setOrderTypeIds(CollectionUtils.toArray(getOrderTypeIds(), Short.class));
		}
		if (!CollectionUtils.isEmpty(getExecutingBrokerCompanyIds())) {
			orderTradeSearchForm.setExecutingBrokerCompanyIds(CollectionUtils.toArray(getExecutingBrokerCompanyIds(), Integer.class));
		}
		if (!CollectionUtils.isEmpty(getHoldingAccountIssuingCompanyIds())) {
			orderTradeSearchForm.setExecutingBrokerCompanyIds(CollectionUtils.toArray(getHoldingAccountIssuingCompanyIds(), Integer.class));
		}
		return orderTradeSearchForm;
	}
}
