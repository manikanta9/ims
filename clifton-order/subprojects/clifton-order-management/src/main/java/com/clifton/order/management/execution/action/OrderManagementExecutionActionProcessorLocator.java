package com.clifton.order.management.execution.action;


import com.clifton.order.setup.OrderExecutionActions;


public interface OrderManagementExecutionActionProcessorLocator {

	/**
	 * Returns OrderManagementExecutionActionProcessor for the specified orderExecutionAction
	 */
	public OrderManagementExecutionActionProcessor locate(OrderExecutionActions orderExecutionAction);
}
