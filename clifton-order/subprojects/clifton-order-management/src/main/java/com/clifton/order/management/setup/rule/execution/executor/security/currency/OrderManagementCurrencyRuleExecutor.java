package com.clifton.order.management.setup.rule.execution.executor.security.currency;

import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.execution.executor.OrderManagementRuleExecutor;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;


/**
 * Currency Rule Executor interface.  This is a separate interface since currency rules also validate if a currency is required to be 11A which would mean it needs to use a different order type
 *
 * @author manderson
 */
public interface OrderManagementCurrencyRuleExecutor extends OrderManagementRuleExecutor {

	/**
	 * Returns true if the 11A is required for the selected orderRuleAware entity.
	 * Returns false if not required or doesn't apply.
	 */
	public boolean isCurrency11ARequired(OrderManagementRuleAssignmentExtended ruleAssignment, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context);
}
