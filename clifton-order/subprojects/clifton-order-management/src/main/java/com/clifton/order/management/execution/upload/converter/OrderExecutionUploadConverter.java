package com.clifton.order.management.execution.upload.converter;

import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.order.execution.OrderPlacement;

import java.math.BigDecimal;


/**
 * The <code>OrderExecutionUploadConverter</code> is a bean interface used to define the methods for uploading execution information into the system.
 * Allows configuring columns in the file to actual data in the system.
 *
 * @author manderson
 */
public interface OrderExecutionUploadConverter {


	public OrderPlacement getOrderPlacementForRow(DataRow row);


	public BigDecimal getFillQuantityForRow(OrderPlacement orderPlacement, DataRow row);


	public BigDecimal getAverageUnitPriceForRow(OrderPlacement orderPlacement, DataRow row);


	public boolean isSupportedForFileName(String fileName);
}
