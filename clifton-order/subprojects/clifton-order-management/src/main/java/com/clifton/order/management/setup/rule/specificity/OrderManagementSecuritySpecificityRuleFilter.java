package com.clifton.order.management.setup.rule.specificity;

import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.rule.OrderRuleAware;

import java.util.List;


/**
 * @author manderson
 */
public class OrderManagementSecuritySpecificityRuleFilter extends BasicOrderManagementSpecificityRuleFilter {


	@Override
	public List<OrderManagementRuleAssignmentExtended> applySpecificityFiltering(List<OrderManagementRuleAssignmentExtended> assignmentExtendedList, OrderRuleAware orderRuleAware, OrderRuleEvaluatorContext context) {
		// If no security, then can't apply specificity filtering so just return the full list back
		if (orderRuleAware.getSecurity() == null) {
			return assignmentExtendedList;
		}

		return super.applySpecificityFiltering(assignmentExtendedList, orderRuleAware, context);
	}
}
