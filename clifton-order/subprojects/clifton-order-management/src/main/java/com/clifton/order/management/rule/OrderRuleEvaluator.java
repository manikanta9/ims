package com.clifton.order.management.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.order.management.setup.rule.OrderManagementRuleCategory;
import com.clifton.order.management.setup.rule.OrderManagementRuleService;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionHandler;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionResult;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionService;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.shared.OrderCompany;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.SystemSchemaService;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>OrderManagementRuleEvaluator</code> evaluates all of the applicable {@link com.clifton.order.management.setup.rule.OrderManagementRuleAssignment}
 * and creates {@link RuleViolation} where rules are violated.
 * <p>
 * For cases like Executing Broker Rules, if the Order Allocation does not have an executing broker selected, those rules are skipped and expected to be validated
 * on another entity (i.e. the Block Order or Placement)
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderRuleEvaluator implements RuleEvaluator<OrderRuleAware, OrderRuleEvaluatorContext> {

	private OrderManagementRuleExecutionHandler orderManagementRuleExecutionHandler;

	private OrderManagementRuleExecutionService orderManagementRuleExecutionService;

	private OrderManagementRuleService orderManagementRuleService;

	private RuleViolationService ruleViolationService;

	private SystemBeanService systemBeanService;

	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(OrderRuleAware orderRuleAware, RuleConfig ruleConfig, OrderRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<OrderManagementRuleCategory> categoryList = getOrderManagementRuleService().getOrderManagementRuleCategoryListByRollup(false);
			for (OrderManagementRuleCategory ruleCategory : categoryList) {
				// For Executing Broker Rules: If none selected on the OrderRuleAware object, we can skip evaluation, however still try to retrieve the list of available, if not add a Rule Violation
				if (OrderManagementRuleCategory.EXECUTING_BROKER_RULES.equalsIgnoreCase(ruleCategory.getName())) {
					if (orderRuleAware.getExecutingBrokerCompany() == null) {
						List<OrderCompany> executingBrokerCompanyList = getOrderManagementRuleExecutionService().getOrderManagementRuleExecutingBrokerCompanyListForOrderRuleAware(orderRuleAware, null);
						if (CollectionUtils.isEmpty(executingBrokerCompanyList)) {
							ruleViolationList.add(getRuleViolationService().createRuleViolationWithNoteTemplate(entityConfig, orderRuleAware.getIdentity(), null, null, null, null, "There are no available executing brokers that apply to this Order Allocation.", null));
						}
						continue;
					}
					if (orderRuleAware.getOrderType().isExecutingBrokerHoldingAccountIssuer()) {
						if (CompareUtils.isEqual(orderRuleAware.getExecutingBrokerCompany(), orderRuleAware.getHoldingAccount().getIssuingCompany())) {
							ruleViolationList.add(getRuleViolationService().createRuleViolationWithNoteTemplate(entityConfig, orderRuleAware.getIdentity(), null, null, null, null, "Invalid Executing Broker selected. Order Type " + orderRuleAware.getOrderType().getName() + " requires orders to be executed with the holding account issuer.", null));
						}
						continue;
					}
				}

				List<OrderManagementRuleExecutionResult> resultList = getOrderManagementRuleExecutionHandler().getOrderManagementRuleExecutionResultListForCategory(orderRuleAware, ruleCategory, context, true);
				for (OrderManagementRuleExecutionResult result : resultList) {
					if (!StringUtils.isEmpty(result.getRuleDefinition().getViolationNoteTemplateOverride()) || !StringUtils.isEmpty(result.getRuleDefinition().getViolationCodeTemplate())) {
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithNoteTemplate(entityConfig, orderRuleAware.getIdentity(), null, result.getRuleDefinition().getId(), null, null, result.getRuleDefinition().getViolationNoteTemplateOverride(), result.getRuleDefinition().getViolationCodeTemplate()));
					}
					else {
						ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, orderRuleAware.getIdentity(), result.getRuleDefinition().getId()));
					}
				}
			}
		}
		return ruleViolationList;
	}
}
