package com.clifton.order.management.setup.rule.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinition;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleDefinitionSearchForm extends BaseAuditableSystemHierarchyItemSearchForm implements CustomJsonStringAwareSearchForm {


	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String violationNoteTemplateOverride;

	@SearchField(searchField = "ruleType.ruleCategory.rollup")
	private Boolean rollup;

	@SearchField(searchField = "ruleType.rollupRuleType.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short rollupRuleTypeIdOrNull;

	@SearchField(searchField = "ruleType.assignmentProhibited")
	private Boolean assignmentProhibited;

	@SearchField(searchFieldPath = "ruleType", searchField = "ruleCategory.id", sortField = "ruleCategory.name")
	private Short ruleCategoryId;

	@SearchField(searchField = "ruleType.id", sortField = "ruleType.name")
	private Short ruleTypeId;

	@SearchField(searchField = "ruleExecutorBean.id", searchFieldPath = "ruleType", sortField = "ruleExecutorBean.name")
	private Integer ruleExecutorBeanId;

	@SearchField(searchField = "ruleExecutorBean.name", searchFieldPath = "ruleType")
	private String ruleExecutorBeanName;

	@SearchField(searchField = "parentRuleList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer parentId;

	@SearchField(searchField = "childRuleList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer childId;

	@SearchField
	private String violationCodeTemplate;

	@SearchField(searchField = "overrideRuleDefinitionModifyCondition.id")
	private Integer overrideRuleDefinitionModifyConditionId;

	@SearchField(searchField = "overrideRuleDefinitionModifyCondition.id,ruleType.ruleDefinitionModifyCondition.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer coalesceOverrideRuleDefinitionModifyConditionId;

	@SearchField(searchField = "overrideRuleAssignmentModifyCondition.id,ruleType.ruleAssignmentModifyCondition.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer coalesceOverrideRuleAssignmentModifyConditionId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return OrderManagementRuleDefinition.TABLE_NAME;
	}
}
