package com.clifton.order.management.setup.rule.search;

import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleAssignmentSearchForm extends BaseOrderManagementRuleAssignmentSearchForm {


	// Custom Search Field ruleDefinitionId = or Exists ruleDefinition.childList.id =
	private Integer ruleDefinitionIdOrChildId;
}
