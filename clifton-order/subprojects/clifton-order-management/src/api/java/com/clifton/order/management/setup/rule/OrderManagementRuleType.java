package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import lombok.Getter;
import lombok.Setter;


/**
 * The <code>OrderManagementRuleType</code> class defines a specific rule executor (bean) for a category (except for rollups).  Each rule type can support custom fields for that rule type that the executor can use during processing.
 * For example, Allow Trading for a Specific Security Type, where Security Type is a custom field selection, however an Executing Broker rule would allow selecting a specific Executing Broker.
 * <p>
 * Note that each Rule Type bean implementation must be aware of the custom fields supported and thus are expected to be defined by Dev
 *
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderManagementRuleType extends NamedEntity<Short> {

	private OrderManagementRuleCategory ruleCategory;

	/**
	 * If these rule types are not allowed to be directly assigned.
	 * If true, then a requiredRollupRuleTypeId is required
	 * since that is where the assignments are done and the rollup rule will likely have additional options.
	 */
	private boolean assignmentProhibited;

	/**
	 * If populated, rules can only be children of selected rollup rule type
	 * Note: Additional rules can also be children of the rollup rule if they have no requirement
	 */
	private OrderManagementRuleType rollupRuleType;

	private SystemBean ruleExecutorBean;

	/**
	 * This bean is used to take all pre-filtered rules the use the same filter bean
	 * and apply specificity filtering within that list to find the most specific if more than one apply
	 * For example: Rule that prohibits CCY trading, and a second rule that allows AUD
	 * When executing the rules without a known CCY, both rules would be valid, but if executing rules on AUD only the AUD one applies
	 * If not supplied, no additional filtering is applied
	 */
	private SystemBean ruleSpecificityFilterBean;

	/**
	 *Freemarker template used to generate rule definition names under this rule type.  Helps to build the names dynamically and provide consistency for large rule sets.  Rule Definition names are
	 * 	read only when this is set and cannot be changed manually
	 */
	private String ruleDefinitionNameTemplate;

	/**
	 * Applies to all rule definitions of this type (unless overridden by a specific rule definition).
	 * Used to restrict if this rule definition can be modified. If rule definition is a rollup, it's also used to restrict adding children rule definitions to this rollup rule definition.
	 * For example, we can prohibit rules used globally to only be modified by administrators.
	 */
	private SystemCondition ruleDefinitionModifyCondition;

	/**
	 * Applies to all rule definitions of this type (unless overridden by a specific rule definition).
	 * Used to restrict if this rule definition can be assigned. If rule definition is a rollup, it's also used to restrict adding children rule definitions to this rollup rule definition.
	 * For example, we may restrict assignments for Investor ID rules to only be modified by Settlements group.
	 * Does not apply if the rules under this type cannot be directly assigned
	 */
	private SystemCondition ruleAssignmentModifyCondition;
}
