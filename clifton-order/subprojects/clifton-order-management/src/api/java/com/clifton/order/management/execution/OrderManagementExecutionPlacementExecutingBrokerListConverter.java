package com.clifton.order.management.execution;

import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.util.StringUtils;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionService;
import com.clifton.order.shared.OrderCompany;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderManagementExecutionPlacementExecutingBrokerListConverter</code> is used to display the list of available executing brokers for a particular placement
 * <p>
 * Used for Excel Exports of Placements to include this additional information that isn't displayed on the grid.
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderManagementExecutionPlacementExecutingBrokerListConverter implements ReversableConverter<String, Long> {

	private OrderManagementRuleExecutionService orderManagementRuleExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Long convert(String from) {
		// Not used, only convert from a placement to a list of brokers for exporting.
		return null;
	}


	@Override
	public String reverseConvert(Long placementId) {
		return StringUtils.collectionToCommaDelimitedString(getOrderManagementRuleExecutionService().getOrderManagementRuleExecutingBrokerCompanyListForOrderPlacement(placementId), OrderCompany::getName);
	}
}
