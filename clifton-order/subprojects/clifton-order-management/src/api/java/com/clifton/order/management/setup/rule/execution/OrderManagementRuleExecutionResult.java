package com.clifton.order.management.setup.rule.execution;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinition;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@NonPersistentObject
@Getter
@Setter
public class OrderManagementRuleExecutionResult {

	private OrderManagementRuleDefinition ruleDefinition;

	private boolean executionResult;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderManagementRuleExecutionResult() {
		super();
	}


	public OrderManagementRuleExecutionResult(OrderManagementRuleDefinition ruleDefinition, boolean executionResult) {
		this();
		this.ruleDefinition = ruleDefinition;
		this.executionResult = executionResult;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return getRuleDefinition() != null ? getRuleDefinition().getId() : null;
	}
}
