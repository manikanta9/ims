package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBeanGroup;
import lombok.Getter;
import lombok.Setter;


/**
 * A <code>OrderManagementRuleCategory</code> is used to categorize rules that can perform a specific purpose and must implement a specific interface (ruleExecutorBeanGroup)
 * <p>
 * For example, Security Rules are also used to limit combo box selections on Order Entry screens.  Executing Broker Rules are also used to limit combo box selections for the Executing Broker Selections.
 * For some, a preview support functionality may be supported, if so the detail window class is defined.
 *
 * @author manderson
 */
@CacheByName
@Getter
@Setter
public class OrderManagementRuleCategory extends NamedEntity<Short> {

	public static final String SECURITY_RULES = "Security Rules";

	public static final String EXECUTING_BROKER_RULES = "Executing Broker Rules";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, then the rule is a rollup which is used to group rule definitions together and does not actually handle any of it's own logic.
	 * There only needs to be one rollup rule category as it's just to hold other rules.  Rollup rules can contain child rules across types and categories.
	 */
	private boolean rollup;


	/**
	 * SystemBeanGroup / Interface responsible for executing this rule
	 * may involve additional features (i.e. Security Rules can allow getting a list of securities available for an account to trade)
	 */
	private SystemBeanGroup ruleExecutorBeanGroup;

	/**
	 * UI window to easily show users preview results for a rule type
	 * Executes ALL rules for a given account under this type
	 */
	private String previewRuleTypeWindowClass;
}
