package com.clifton.order.management.setup.search;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * The <code>OrderManagementDestinationMappingSearchCommand</code> defines a custom search command.  This is used to search the
 * OrderManagementDestinationMappingCache.
 *
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderManagementDestinationMappingSearchCommand {

	private Short orderTypeId;

	private Date activeOnDate;

	private Short orderDestinationId;

	private Integer executingBrokerCompanyId;

	private Integer executingBrokerCompanyIdOrNull;

	private Integer[] executingBrokerCompanyIdsOrNull;

	private String executingBrokerCompanyName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static OrderManagementDestinationMappingSearchCommand forOrderTypeAndExecutingBrokerCompanyNameAndDestination(Short orderTypeId, String executingBrokerCompanyName, Short orderDestinationId) {
		//Added this to reference non-static variable from static context
		OrderManagementDestinationMappingSearchCommand orderManagementDestinationMappingSearchCommand = new OrderManagementDestinationMappingSearchCommand();
		orderManagementDestinationMappingSearchCommand.setOrderTypeId(orderTypeId);
		orderManagementDestinationMappingSearchCommand.setExecutingBrokerCompanyName(executingBrokerCompanyName);
		orderManagementDestinationMappingSearchCommand.setOrderDestinationId(orderDestinationId);
		return orderManagementDestinationMappingSearchCommand;
	}


	public static OrderManagementDestinationMappingSearchCommand forOrderTypeAndActiveOnDateAndDestination(Short orderTypeId, Date activeOnDate, Short orderDestinationId) {
		//Added this to reference non-static variable from static context
		OrderManagementDestinationMappingSearchCommand orderManagementDestinationMappingSearchCommand = new OrderManagementDestinationMappingSearchCommand();
		orderManagementDestinationMappingSearchCommand.setOrderTypeId(orderTypeId);
		orderManagementDestinationMappingSearchCommand.setActiveOnDate(activeOnDate);
		orderManagementDestinationMappingSearchCommand.setOrderDestinationId(orderDestinationId);
		return orderManagementDestinationMappingSearchCommand;
	}


	public static OrderManagementDestinationMappingSearchCommand forOrderTypeAndExecutingBrokerIdsOrNullAndActiveOnDate(Short orderTypeId, Integer executingBrokerCompanyIdOrNull, Integer[] executingBrokerCompanyIdsOrNull, Date activeOnDate) {
		//Added this to reference non-static variable from static context
		OrderManagementDestinationMappingSearchCommand orderManagementDestinationMappingSearchCommand = new OrderManagementDestinationMappingSearchCommand();
		orderManagementDestinationMappingSearchCommand.setOrderTypeId(orderTypeId);
		orderManagementDestinationMappingSearchCommand.setExecutingBrokerCompanyIdOrNull(executingBrokerCompanyIdOrNull);
		orderManagementDestinationMappingSearchCommand.setExecutingBrokerCompanyIdsOrNull(executingBrokerCompanyIdsOrNull);
		orderManagementDestinationMappingSearchCommand.setActiveOnDate(activeOnDate);
		return orderManagementDestinationMappingSearchCommand;
	}
}
