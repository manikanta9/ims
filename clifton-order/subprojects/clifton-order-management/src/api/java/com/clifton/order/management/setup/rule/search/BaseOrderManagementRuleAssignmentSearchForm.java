package com.clifton.order.management.setup.rule.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * The <code>BaseOrderManagementRuleAssignmentSearchForm</code> contains the commons search form properties for both the {@link OrderManagementRuleAssignmentSearchForm}
 * and {@link com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtendedSearchForm}
 *
 * @author manderson
 */
@Getter
@Setter
public abstract class BaseOrderManagementRuleAssignmentSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm implements CustomJsonStringAwareSearchForm {

	@SearchField(searchField = "ruleDefinition.name,clientAccount.accountNumber,clientAccount.accountShortName,clientAccount.accountName,holdingAccount.accountNumber,holdingAccount.accountName", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField(searchField = "clientAccount.id,holdingAccount.id")
	private Integer clientOrHoldingAccountId;

	@SearchField(searchField = "holdingAccountIssuingCompany.id")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchField = "holdingAccount.issuingCompany.id,holdingAccountIssuingCompany.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer coalesceHoldingAccountIssuingCompanyId;

	@SearchField
	private Boolean excluded;

	// Can't use coalesce with integers to search for is NULL.  So, instead this will add custom criteria if true (clientAccountId is null and holdingAccountIssuingCompanyId is null) else (clientAccountId is not null or holdingAccountIssuingCompanyId is not null)
	private Boolean globalRuleAssignment;

	@SearchField(searchField = "holdingAccountIssuingCompany.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean holdingAccountIssuerAssignment;

	@SearchField(searchField = "clientAccount.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean accountAssignment;

	@SearchField(searchFieldPath = "ruleDefinition.ruleType", searchField = "ruleCategory.id")
	private Short ruleCategoryId;

	@SearchField(searchFieldPath = "ruleDefinition", searchField = "ruleType.id")
	private Short ruleTypeId;

	@SearchField(searchField = "ruleDefinition.id")
	private Integer ruleDefinitionId;

	@SearchField(searchField = "ruleDefinition.overrideRuleAssignmentModifyCondition.id,ruleDefinition.ruleType.ruleAssignmentModifyCondition.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer entityModifyConditionId;

	@SearchField
	private String note;

	// Custom search field to include any rule "assigned" to the account (at any level, but global inclusions determined by accountIncludeGlobal)
	// Handled as either a client account or holding account or both
	private Integer assignedAccountId;

	// Custom search field to include any rule "assigned" to the CLIENT account (at any level, but global inclusions determined by accountIncludeGlobal)
	// Should be used in conjunction with the assignedHoldingAccountId
	private Integer assignedClientAccountId;

	// Custom search field to include any rule "assigned" to the HOLDING account (at any level, but global inclusions determined by accountIncludeGlobal)
	// Should be used in conjunction with the assignedHoldingAccountId
	private Integer assignedHoldingAccountId;

	// Custom search field used with "assigned..." account id values to determine whether or not to include global rules
	private Boolean accountIncludeGlobal;
}
