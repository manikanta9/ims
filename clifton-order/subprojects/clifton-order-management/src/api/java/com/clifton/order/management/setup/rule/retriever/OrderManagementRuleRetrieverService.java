package com.clifton.order.management.setup.rule.retriever;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.management.setup.rule.OrderManagementRuleAssignment;
import com.clifton.order.rule.OrderRuleAware;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderManagementRuleRetrieverService {


	////////////////////////////////////////////////////////////////////////////
	////////      Order Management Rule Assignment Extended Methods     ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderManagementRuleAssignment.class)
	public List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleAssignmentExtendedList(OrderManagementRuleAssignmentExtendedSearchForm searchForm);


	/**
	 * Returns the rule assignment extended objects that apply to that account (global, holding account issuer, account) and category
	 * Should be used for rule execution as it takes advantage of caching to find all application rule assignments for an account
	 */
	@DoNotAddRequestMapping
	public List<OrderManagementRuleAssignmentExtended> getOrderManagementRuleAssignmentExtendedListForAccountAndCategory(OrderRuleAware orderRuleAware, short ruleCategoryId);
}
