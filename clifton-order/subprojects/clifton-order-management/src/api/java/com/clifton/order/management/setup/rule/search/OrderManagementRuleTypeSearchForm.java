package com.clifton.order.management.setup.rule.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "ruleCategory.id", sortField = "ruleCategory.name")
	private Short ruleCategoryId;

	@SearchField(searchField = "ruleCategory.rollup")
	private Boolean rollup;

	@SearchField
	private Boolean assignmentProhibited;

	@SearchField(searchField = "rollupRuleType.id", sortField = "rollupRuleType.name")
	private Short rollupRuleTypeId;

	@SearchField(searchField = "ruleExecutorBean.id", sortField = "ruleExecutorBean.name")
	private Integer ruleExecutorBeanId;

	@SearchField(searchField = "ruleExecutorBean.name")
	private String ruleExecutorBeanName;

	@SearchField(searchField = "ruleSpecificityFilterBean.id", sortField = "ruleSpecificityFilterBean.name")
	private Integer ruleSpecificityFilterBeanId;

	@SearchField(searchField = "ruleSpecificityFilterBean.name")
	private String ruleSpecificityFilterBeanName;

	@SearchField
	private String ruleDefinitionNameTemplate;

	@SearchField(searchField = "ruleDefinitionModifyCondition.id", sortField = "ruleDefinitionModifyCondition.name")
	private Integer ruleDefinitionModifyConditionId;

	@SearchField(searchField = "ruleAssignmentModifyCondition.id", sortField = "ruleAssignmentModifyCondition.name")
	private Integer ruleAssignmentModifyConditionId;
}
