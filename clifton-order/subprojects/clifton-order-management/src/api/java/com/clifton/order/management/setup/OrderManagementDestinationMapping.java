package com.clifton.order.management.setup;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.setup.OrderType;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderCompany;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * The <code>OrderManagementDestinationMapping</code> class maps order types to corresponding destinations by broker.
 * <p>
 * For example, if 6 brokers are allowed for "REDI" destination for "Futures" trades, then 6 mapping rows will be created.
 * If only 1 broker is used for trade tickets (Goldman Sachs), then only one row will be for "Trade Ticket" destination.
 * If any broker can be used for bonds, then executingBrokerCompany can be null to allow all.
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementDestinationMapping extends BaseEntity<Integer> implements LabeledObject {

	private OrderType orderType;

	private OrderDestination orderDestination;

	private OrderCompany executingBrokerCompany;


	private Date startDate;

	private Date endDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		if (getOrderType() != null) {
			result.append(getOrderType().getName());
		}
		if (getOrderDestination() != null) {
			result.append(": ").append(getOrderDestination().getName());
		}
		if (getExecutingBrokerCompany() != null) {
			result.append(": ").append(getExecutingBrokerCompany().getName());
		}
		return result.toString();
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}
}
