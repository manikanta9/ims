package com.clifton.order.management.setup.rule;

import lombok.Getter;


/**
 * The <code>OrderManagementRuleAssignmentScopes</code> are used to define the scopes and their rankings for each level
 * The LOWER the ranking value the higher the precendence.  i.e. Global rules can be overridden by holding account issuer rules, and account rules override them all.
 *
 * @author manderson
 */
@Getter
public enum OrderManagementRuleAssignmentScopes {

	GLOBAL((short) 100),

	HOLDING_ACCOUNT_ISSUER((short) 10),

	ACCOUNT((short) 1);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final short ranking;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	OrderManagementRuleAssignmentScopes(short ranking) {
		this.ranking = ranking;
	}
}
