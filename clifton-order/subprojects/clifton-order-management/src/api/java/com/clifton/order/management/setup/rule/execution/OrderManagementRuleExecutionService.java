package com.clifton.order.management.setup.rule.execution;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.management.setup.rule.OrderManagementRuleCategory;
import com.clifton.order.management.setup.rule.execution.search.OrderManagementRuleExecutingBrokerCompanySearchForm;
import com.clifton.order.rule.OrderRuleAware;
import com.clifton.order.shared.OrderCompany;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderManagementRuleExecutionService {


	////////////////////////////////////////////////////////////////////////////
	////////            Rule Category Preview                           ////////
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	@SecureMethod(dtoClass = OrderManagementRuleCategory.class)
	public List<OrderManagementRuleExecutionResult> getOrderManagementRuleExecutionResultList(OrderManagementRuleExecutionCommand command);


	////////////////////////////////////////////////////////////////////////////
	////////          Executing Broker Rule Preview                     ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderManagementRuleCategory.class)
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyList(OrderManagementRuleExecutingBrokerCompanySearchForm searchForm);


	/**
	 * Optional orderDestinationId can be used to limit results of valid executing brokers
	 */
	@DoNotAddRequestMapping
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyListForOrderRuleAware(OrderRuleAware orderRuleAware, Short orderDestinationId);


	/**
	 * Iterates through all order allocations for the block order and the placement destination to find the common available executing brokers
	 */
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyListForOrderPlacement(long orderPlacementId);


	/**
	 * Iterates through all order allocations for the block order and the placement destination to find the common available executing brokers for the given destination
	 */
	public List<OrderCompany> getOrderManagementRuleExecutingBrokerCompanyListForOrderBlockAndDestination(long orderBlockId, short orderDestinationId);
}
