package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>OrderManagementRuleDefinitionRollup</code> class allows for grouping {@link OrderManagementRuleDefinition} together into a single rollup to be assigned
 * This allows for common rules to be defined as a group and then assigned to an account
 *
 * @author manderson
 */
public class OrderManagementRuleDefinitionRollup extends ManyToManyEntity<OrderManagementRuleDefinition, OrderManagementRuleDefinition, Integer> implements SystemEntityModifyConditionAware {

	/**
	 * When modifying a rollup rule definition you are adding/removing child rule definitions from the parent.
	 * Required to have access to edit the parent rule definition in order to add/remove child rule definitions.
	 */
	@Override
	public SystemCondition getEntityModifyCondition() {
		return getReferenceOne().getEntityModifyCondition();
	}
}
