package com.clifton.order.management.setup;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchCommand;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchForm;
import com.clifton.order.setup.destination.OrderDestination;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderManagementSetupService {

	public OrderManagementDestinationMapping getOrderManagementDestinationMapping(int id);


	public List<OrderManagementDestinationMapping> getOrderManagementDestinationMappingList(OrderManagementDestinationMappingSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<OrderManagementDestinationMapping> getOrderManagementDestinationMappingListForCommand(OrderManagementDestinationMappingSearchCommand command);


	public OrderManagementDestinationMapping saveOrderManagementDestinationMapping(OrderManagementDestinationMapping bean);


	public void deleteOrderManagementDestinationMapping(int id);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	public List<OrderDestination> getOrderManagementDestinationListForMapping(OrderManagementDestinationMappingSearchCommand command);
}
