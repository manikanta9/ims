package com.clifton.order.management.activity;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.block.OrderBlock;


/**
 * @author manderson
 */
public interface OrderManagementActivityService {


	@SecureMethod(dtoClass = OrderBlock.class)
	public void processOrderManagementActivityForOrderAllocations(OrderManagementActivityCommand command);


	/**
	 * Applies the Trade and/or Settlement Date adjustments on the selected order allocations.  This includes the Revise Dates workflow transitions necessary where appropriate
	 * separate method/url as it uses a separate screen and only requires access to Order Allocations, not Order Blocks
	 */
	@SecureMethod(dtoClass = OrderAllocation.class)
	public void processOrderManagementReviseDatesForOrderAllocations(OrderManagementActivityCommand command);


	@SecureMethod(dtoClass = OrderBlock.class)
	public void processOrderManagementActivityForOrderBlocks(OrderManagementActivityCommand command);
}
