package com.clifton.order.management.execution.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>OrderExecutionUploadService</code> is used to handle processing of upload files related to executions.  i.e. Fill information for File Placements
 *
 * @author manderson
 */
public interface OrderExecutionUploadService {


	@SecureMethod(dtoClass = OrderPlacement.class)
	public Status uploadOrderPlacementExecutionFile(OrderPlacementExecutionUploadCommand uploadCommand);


	/**
	 * Returns the Order Execution Upload Converter bean
	 * found that matches based on file name if only one found that matches
	 */
	public SystemBean getOrderExecutionUploadConverterBeanForFileName(String fileName);
}
