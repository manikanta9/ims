package com.clifton.order.management.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.management.setup.rule.OrderManagementRuleCategory;
import org.springframework.stereotype.Component;


@Component
public class OrderManagementRuleCategoryByRollupCache extends SelfRegisteringSingleKeyDaoListCache<OrderManagementRuleCategory, Boolean> {

	@Override
	protected String getBeanKeyProperty() {
		return "rollup";
	}


	@Override
	protected Boolean getBeanKeyValue(OrderManagementRuleCategory bean) {

		if (bean.isRollup()) {
			return bean.isRollup();
		}

		return false;
	}
}


