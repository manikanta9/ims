package com.clifton.order.management.setup.rule;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleAssignmentSearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleCategorySearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleDefinitionRollupSearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleDefinitionSearchForm;
import com.clifton.order.management.setup.rule.search.OrderManagementRuleTypeSearchForm;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * @author manderson
 */
public interface OrderManagementRuleService {


	public OrderManagementRuleCategory getOrderManagementRuleCategory(short id);


	public OrderManagementRuleCategory getOrderManagementRuleCategoryByName(String name);


	public List<OrderManagementRuleCategory> getOrderManagementRuleCategoryList(OrderManagementRuleCategorySearchForm searchForm);


	public List<OrderManagementRuleCategory> getOrderManagementRuleCategoryListByRollup(boolean rollup);


	public OrderManagementRuleCategory saveOrderManagementRuleCategory(OrderManagementRuleCategory bean);


	public void deleteOrderManagementRuleCategory(short id);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderManagementRuleType getOrderManagementRuleType(short id);


	public OrderManagementRuleType getOrderManagementRuleTypeByName(String name);


	public List<OrderManagementRuleType> getOrderManagementRuleTypeList(OrderManagementRuleTypeSearchForm searchForm);


	public OrderManagementRuleType saveOrderManagementRuleType(OrderManagementRuleType bean);


	public void deleteOrderManagementRuleType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderManagementRuleDefinition getOrderManagementRuleDefinition(int id);


	public List<OrderManagementRuleDefinition> getOrderManagementRuleDefinitionList(OrderManagementRuleDefinitionSearchForm searchForm);


	public OrderManagementRuleDefinition saveOrderManagementRuleDefinition(OrderManagementRuleDefinition bean);


	/**
	 * Creates a copy of the bean with updated properties
	 * (name, description, and custom field values can be changed)
	 */
	public OrderManagementRuleDefinition copyOrderManagementRuleDefinition(OrderManagementRuleDefinition bean);


	public void deleteOrderManagementRuleDefinition(int id);


	@ResponseBody
	public String getOrderManagementRuleDefinitionForName(int ruleDefinitionId);


	public void generateOrderManagementRuleDefinition(Integer[] ruleDefinitionIds);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<OrderManagementRuleDefinitionRollup> getOrderManagementRuleDefinitionRollupList(OrderManagementRuleDefinitionRollupSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<OrderManagementRuleDefinitionRollup> getOrderManagementRuleDefinitionRollupListByParent(int parentId);


	public OrderManagementRuleDefinitionRollup linkOrderManagementRuleDefinitionRollup(int parentRuleDefinitionId, int childRuleDefinitionId);


	public void unlinkOrderManagementRuleDefinitionRollup(int parentRuleDefinitionId, int childRuleDefinitionId);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderManagementRuleAssignment getOrderManagementRuleAssignment(int id);


	public List<OrderManagementRuleAssignment> getOrderManagementRuleAssignmentList(OrderManagementRuleAssignmentSearchForm searchForm);


	public OrderManagementRuleAssignment saveOrderManagementRuleAssignment(OrderManagementRuleAssignment bean);


	public void deleteOrderManagementRuleAssignment(int id);
}
