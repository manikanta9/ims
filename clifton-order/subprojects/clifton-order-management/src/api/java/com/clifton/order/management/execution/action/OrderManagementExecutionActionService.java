package com.clifton.order.management.execution.action;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.setup.OrderExecutionActions;


/**
 * @author manderson
 */
public interface OrderManagementExecutionActionService {


	/**
	 * Handles the processing of the OrderPlacement by validating and then executing the action.
	 * i.e. SEND If it's a FIX execution and in draft will send to FIX
	 */
	@SecureMethod(dtoClass = OrderPlacement.class)
	public void processOrderManagementExecutionAction(long orderPlacementId, OrderExecutionActions orderExecutionAction);


	/**
	 * Handles the processing of the OrderPlacement by using the given command object. Command object is required for some execution actions because we need additional data.
	 * i.e. Amend placement (block, order allocation, trade) after execution
	 * Switch to Destination (include the destination to switch to)
	 */
	@UserIgnorableValidation
	@SecureMethod(dtoClass = OrderPlacement.class)
	public void processOrderManagementExecutionActionForCommand(long orderPlacementId, OrderManagementExecutionActionCommand command, boolean ignoreValidation);
}
