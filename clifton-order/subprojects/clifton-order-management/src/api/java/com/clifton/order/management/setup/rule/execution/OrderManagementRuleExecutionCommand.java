package com.clifton.order.management.setup.rule.execution;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleExecutionCommand implements Serializable {

	private Short ruleCategoryId;

	private Integer clientAccountId;

	private Integer holdingAccountId;

	private Date tradeDate;

	private Short orderTypeId;

	private Integer orderSecurityId;

	private Integer settlementCurrencyId;

	private Integer executingBrokerCompanyId;
}
