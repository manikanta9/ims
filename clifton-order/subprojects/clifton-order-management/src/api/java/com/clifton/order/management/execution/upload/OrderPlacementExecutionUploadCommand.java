package com.clifton.order.management.execution.upload;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.note.SystemNote;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;


/**
 * @author manderson
 */
@NonPersistentObject
@Getter
@Setter
public class OrderPlacementExecutionUploadCommand {

	/**
	 * Handles the conversion of each row to an OrderPlacement and execution information
	 */
	private SystemBean orderExecutionUploadConverterBean;

	/**
	 * Percent from latest available that is considered OK.  Otherwise a warning is generated
	 * entered as a percent - i.e. 5 = 5% which means +/- %
	 */
	private BigDecimal warnPriceThreshold;

	/**
	 * Skips and Warns of any row where the filled amount is not the same as the quantity intended
	 */
	private boolean warnIfNotFullyFilled;

	/**
	 * If true, will NOT save any of the executions if any rows produce errors (duplicates or bad rows) or warnings (percentThreshold)
	 */
	private boolean failUploadIfAnyWarningsOrErrors;

	/**
	 * If a note type is selected on a note, then we intend to create a note associated with the placements
	 * in the upload file that are affected. The upload file will be attached to the note for historical tracking
	 */
	private SystemNote note;


	/**
	 * The file to upload (and attach to the note) is automatically bound by Spring
	 */
	private MultipartFile file;

	/**
	 * If working with Excel files with multiple sheets - options
	 * to set the sheet index that contains the data to be uploaded.
	 * NOTE: This is not on UI yet, but implemented for Excel Based test cases
	 */
	private int sheetIndex = 0;
}
