package com.clifton.order.management.execution.action;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.order.setup.OrderExecutionActions;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
@Getter
@Setter
public class OrderManagementExecutionActionCommand implements Serializable {


	private OrderExecutionActions orderExecutionAction;

	/**
	 * AMENDING EXECUTION OPTIONS BELOW
	 */

	/**
	 * We can silently just skip if no changes
	 * Useful for bulk feature if needed
	 */
	private boolean skipIfNoChanges;

	private Date overrideTradeDate;

	private Date overrideSettlementDate;

	private BigDecimal overrideAverageUnitPrice;

	/**
	 * Will be attached as a note for type Amendment Reason
	 */
	private String amendmentReasonNote;

	/**
	 * We'll do some initial checks to confirm trade/settlement dates (if updated) pass initial checks,
	 * Price/Rate information is reasonable.  Some of these can be skipped via User Ignorable Exception
	 * Some exceptions, can't be ignored (i.e. Trade Date MUST be on or before Settlement Date)
	 */
	private boolean ignoreValidation;

	private BigDecimal warnPriceThreshold;


	/**
	 * SWITCH DESTINATION OPTION
	 */
	private Short switchToDestinationId;

	/**
	 * We can optionally resend to the same destination with a different destination configuration
	 * Original placement is canceled and a new one is created for the selected configuration
	 */
	private Short switchToDestinationConfigurationId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderManagementExecutionActionCommand() {
		super();
	}


	public static OrderManagementExecutionActionCommand forExecutionAction(OrderExecutionActions orderExecutionAction) {
		OrderManagementExecutionActionCommand command = new OrderManagementExecutionActionCommand();
		command.setOrderExecutionAction(orderExecutionAction);
		return command;
	}
}
