package com.clifton.order.management.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.management.setup.OrderManagementDestinationMapping;
import org.springframework.stereotype.Component;


/**
 * @author AbhinayaM
 */

@Component
public class OrderManagementDestinationMappingByOrderTypeCache extends SelfRegisteringSingleKeyDaoListCache<OrderManagementDestinationMapping, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "orderType.id";
	}


	@Override
	protected Short getBeanKeyValue(OrderManagementDestinationMapping bean) {
		if (bean.getOrderType() != null) {
			return bean.getOrderType().getId();
		}
		return null;
	}
}
