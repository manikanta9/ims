package com.clifton.order.management.activity;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.order.allocation.search.OrderAllocationSearchForm;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>OrderManagementActivityCommand</code> is used to define actions or activities to be applied to a list of order allocations, block orders, placements, etc..
 * This can be assigning a trader, creating block orders, etc.
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
@Getter
@Setter
public class OrderManagementActivityCommand implements Serializable {

	private Long[] orderAllocationIds;

	/**
	 * Instead of specific allocations ids, can supply filters
	 * Each specific action may add more filters to further limit scope for that specific action
	 */
	private OrderAllocationSearchForm orderAllocationSearchCommand;

	private Long[] orderBlockIds;


	/**
	 * If populated, we will assign the trader to the selected order allocations
	 * Will only do this if the trader field is empty unless reassignTraderIfPopulated is true
	 */
	private Short assignTraderUserId;

	private boolean reassignTraderIfPopulated;

	/**
	 * Create OrderBlock(s) for selected Order Allocations
	 */
	private boolean createBlockOrders;


	/**
	 * Create OrderBlock(s) for selected Order Allocations
	 */
	private boolean unblockOrderAllocations;

	/**
	 * If unblocking, can also reject the order allocations
	 */
	private boolean rejectUnblockedOrderAllocations;

	/**
	 * If unblocking, can also return the order allocations to the PM
	 */
	private boolean returnUnblockedOrderAllocations;

	/**
	 * If populated, will block orders according to what is available based on this selected destination (if not pre-defined on the order allocation)
	 * If no brokers are available for the destination and order allocation, it is not blocked
	 */
	private Short orderDestinationId;

	/**
	 * If populated, and destination matches the order destination will also set the configuration option on the placement when creating it
	 */
	private Short destinationConfigurationId;

	/**
	 * Separate block order creation by account
	 */
	private boolean orderBlockAccount;

	/**
	 * If true, and destination is selected and applies
	 * will immediately place the orders for execution
	 */
	private boolean createPlacements;

	/**
	 * Any created placements will immediately try to send
	 */
	private boolean sendPlacements;

	/**
	 * If true, then revise dates of selected order allocations with the below options
	 */
	private boolean reviseDates;

	private OrderManagementDateRevisionTypes tradeDateAdjustment;

	private Date overrideTradeDate;


	private OrderManagementDateRevisionTypes settlementDateAdjustment;

	private Date overrideSettlementDate;

	/**
	 * After applying above command logic if trade date or settlement date is not validate, moves it forward to the next viable date.
	 */
	private boolean autoAdjustReviseDatesForBusinessDays;
}
