package com.clifton.order.management.setup.rule.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleCategorySearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Boolean rollup;

	@SearchField(searchField = "ruleExecutorBeanGroup.id", sortField = "ruleExecutorBeanGroup.name")
	private Short ruleExecutorBeanGroupId;

	@SearchField(searchField = "ruleExecutorBeanGroup.name")
	private String ruleExecutorBeanGroupName;

	@SearchField
	private String previewRuleTypeWindowClass;

	@SearchField(searchField = "previewRuleTypeWindowClass", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean previewAllowed;
}
