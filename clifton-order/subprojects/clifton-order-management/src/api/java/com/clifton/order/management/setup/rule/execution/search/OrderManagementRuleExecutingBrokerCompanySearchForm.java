package com.clifton.order.management.setup.rule.execution.search;

import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.order.management.setup.rule.execution.OrderManagementRuleExecutionCommand;
import com.clifton.order.shared.search.OrderCompanySearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@SearchForm(hasOrmDtoClass = false)
@Getter
@Setter
public class OrderManagementRuleExecutingBrokerCompanySearchForm extends OrderCompanySearchForm {

	private OrderManagementRuleExecutionCommand command;


	/**
	 * The list of executing brokers allowed is ALWAYS limited based on destination mappings for the order type
	 * However, setting this will also limit that list based on a specific destination and it's mappings.
	 * <p>
	 * For example, if an account can execute with Pershing Email (Pershing Only) or FX Connect (State Street)
	 * if no destination is selected, then they both would be viable executing brokers, however if FX Connect is selected, then only State Street is a viable selection
	 */
	private Short orderDestinationId;
}
