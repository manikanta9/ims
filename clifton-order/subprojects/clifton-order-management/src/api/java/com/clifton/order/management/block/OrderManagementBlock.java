package com.clifton.order.management.block;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.order.block.OrderBlock;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderCompany;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * The <code>OrderManagementBlock</code> is the result of a calculated or populated {@link OrderBlock} but includes information for a specified
 * order destination and for that order destination and accounts in the OrderBlock the common executing broker list available.
 * <p>
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
@Getter
@Setter
public class OrderManagementBlock extends BaseSimpleEntity<Long> {

	private OrderBlock orderBlock;

	private OrderDestination orderDestination;

	private List<OrderCompany> executingBrokerCompanyList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static OrderManagementBlock of(OrderBlock orderBlock, List<OrderCompany> executingBrokerCompanyList, OrderDestination orderDestination) {
		OrderManagementBlock blockPreview = new OrderManagementBlock();
		blockPreview.setOrderBlock(orderBlock);
		blockPreview.setExecutingBrokerCompanyList(executingBrokerCompanyList);
		blockPreview.setOrderDestination(orderDestination);
		return blockPreview;
	}


	/**
	 * Using the block ids so they are unique for equals comparison when adding to the set.
	 */
	@Override
	public Long getIdentity() {
		if (getOrderBlock() != null) {
			return getOrderBlock().getIdentity();
		}
		return null;
	}
}
