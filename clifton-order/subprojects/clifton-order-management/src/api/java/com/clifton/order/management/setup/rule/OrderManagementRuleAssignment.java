package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderCompany;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;


/**
 * The <code>OrderManagementRuleAssignment</code> is used to assign rules to accounts.
 * Rules can either be
 * 1. Global
 * 2. Holding Account Issuing Company
 * 3. Apply to a Client / Holding Account Pair (or one account that represents both sides of the pair)
 * <p>
 * Rules can also be explicitly excluded in cases where necessary.  Rule assignments of rollup rules is the equivalent of assigning all of the child rules.
 * Account Rules are the most specific, so if the same rule is assigned at multiple levels, the account rule will win - this allows for exclusions
 * Holding Account Issuing company rules are less specific but can apply to all accounts where the holding account issuing company matches.  These override global, but not account rules
 * Global Rules are the most generic and should be used only to apply blanket rules (i.e. Investor ID rules) that should be enforced across all accounts
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleAssignment extends BaseEntity<Integer> implements LabeledObject, SystemColumnCustomJsonAware, SystemEntityModifyConditionAware {

	private OrderManagementRuleDefinition ruleDefinition;

	/**
	 * Account Specific Rule - Most Specific
	 * If selected, holding account must also be selected (rules apply to the pair) - If the account represents both the client and holding account both of the account selections will match
	 * If blank, then can be an issuer rule (holdingAccountIssuingCompany) or it's considered to be a global rule
	 */
	private OrderAccount clientAccount;

	private OrderAccount holdingAccount;

	/**
	 * Holding Account Issuing Company Rule
	 * If accounts and this field is null, then the rule is a global rule
	 */
	private OrderCompany holdingAccountIssuingCompany;

	private Date startDate;

	private Date endDate;

	/**
	 * Provides the ability to exclude a rule.  For example, a global rule or a rollup rule is assigned, but a specific account can exclude a particular rule.
	 */
	private boolean excluded;

	private String note;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * A List of custom column values for this rule definition (field are assigned and vary by rule type)
	 */
	@NonPersistentField
	private String columnGroupName;

	/**
	 * JSON String value representing the custom column values
	 */
	private CustomJsonString customColumns;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isGlobalRuleAssignment() {
		return getRuleScope() == OrderManagementRuleAssignmentScopes.GLOBAL;
	}


	public boolean isHoldingAccountIssuerAssignment() {
		return getRuleScope() == OrderManagementRuleAssignmentScopes.HOLDING_ACCOUNT_ISSUER;
	}


	public boolean isAccountAssignment() {
		return getRuleScope() == OrderManagementRuleAssignmentScopes.ACCOUNT;
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), true);
	}


	public OrderManagementRuleAssignmentScopes getRuleScope() {
		if (getClientAccount() == null) {
			if (getHoldingAccountIssuingCompany() == null) {
				return OrderManagementRuleAssignmentScopes.GLOBAL;
			}
			return OrderManagementRuleAssignmentScopes.HOLDING_ACCOUNT_ISSUER;
		}
		return OrderManagementRuleAssignmentScopes.ACCOUNT;
	}


	public String getScopeLabelShort() {
		switch (getRuleScope()) {
			case GLOBAL:
				return "Global";
			case HOLDING_ACCOUNT_ISSUER:
				return "Issuer";
			case ACCOUNT:
				return "Account";
			default:
				throw new ValidationException("Rule Scope Label missing for " + getRuleScope());
		}
	}


	public String getScopeLabel() {
		switch (getRuleScope()) {
			case GLOBAL:
				return "Global Rule";
			case HOLDING_ACCOUNT_ISSUER:
				return "Holding Account Issuer: " + getHoldingAccountIssuingCompany().getName();
			case ACCOUNT:
				if (CompareUtils.isEqual(getClientAccount(), getHoldingAccount())) {
					return "Account: " + getClientAccount().getLabelShort();
				}
				else {
					return "Client Account: " + getClientAccount().getLabelShort() + ", " + " / Holding Account: " + getHoldingAccount().getAccountNumber();
				}
			default:
				throw new ValidationException("Rule Scope Label missing for " + getRuleScope());
		}
	}


	@Override
	public String getLabel() {
		return getRuleDefinition().getName() + (isExcluded() ? " (Excluded) - " : " - ") + getScopeLabel();
	}


	public OrderCompany getCoalesceHoldingAccountIssuingCompany() {
		if (getHoldingAccount() != null) {
			return getHoldingAccount().getIssuingCompany();
		}
		return getHoldingAccountIssuingCompany();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getRuleDefinition() != null) {
			return getRuleDefinition().getCoalesceAssignmentEntityModifyCondition();
		}
		return null;
	}
}
