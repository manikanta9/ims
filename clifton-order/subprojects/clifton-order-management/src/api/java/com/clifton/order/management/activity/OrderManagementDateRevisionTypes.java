package com.clifton.order.management.activity;

/**
 * @author manderson
 */
public enum OrderManagementDateRevisionTypes {

	NONE(true, true),
	RECALCULATE(true, true),
	T_PLUS_ONE(true, true),
	OVERRIDE(true, true),
	SAME_DAY(false, true);


	private final boolean validTradeDateAdjustment;
	private final boolean validSettlementDateAdjustment;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	OrderManagementDateRevisionTypes(boolean validTradeDateAdjustment, boolean validSettlementDateAdjustment) {
		this.validTradeDateAdjustment = validTradeDateAdjustment;
		this.validSettlementDateAdjustment = validSettlementDateAdjustment;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isValidTradeDateAdjustment() {
		return this.validTradeDateAdjustment;
	}


	public boolean isValidSettlementDateAdjustment() {
		return this.validSettlementDateAdjustment;
	}
}
