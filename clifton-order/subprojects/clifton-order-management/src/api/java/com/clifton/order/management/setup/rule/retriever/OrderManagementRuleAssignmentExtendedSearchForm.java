package com.clifton.order.management.setup.rule.retriever;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.order.management.setup.rule.search.BaseOrderManagementRuleAssignmentSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleAssignmentExtendedSearchForm extends BaseOrderManagementRuleAssignmentSearchForm {

	@SearchField
	private Integer ruleAssignmentId;

	@SearchField(searchField = "rollupRuleDefinition.id")
	private Integer rollupRuleDefinitionId;
}
