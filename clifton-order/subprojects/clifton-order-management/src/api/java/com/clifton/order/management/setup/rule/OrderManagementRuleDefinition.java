package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.ObjectUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.column.json.SystemColumnCustomJsonAware;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import lombok.Getter;
import lombok.Setter;


/**
 * The <code>OrderManagementRuleDefinition</code> class defines a specific rule.  If a rollup rule, then it contains a list of child rules is just a container.  If it's not a rollup rule, then the custom columns available are dependent on the rule type.
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleDefinition extends NamedEntity<Integer> implements SystemColumnCustomJsonAware, SystemEntityModifyConditionAware {

	public static final String TABLE_NAME = "OrderManagementRuleDefinition";

	private OrderManagementRuleType ruleType;

	/**
	 * For Rule Engine rule violations, if the rule fails, we can customize the message
	 * The linkedEntity (OrderRuleAware object) and causeEntity (this rule definition) are available
	 */
	private String violationNoteTemplateOverride;

	/**
	 * For Rule Engine rule violations, if the rule fails, we can customize the violation code
	 * The linkedEntity (OrderRuleAware object) and causeEntity (this rule definition) are available
	 */
	private String violationCodeTemplate;
	/**
	 * Optionally override to modify condition on the rule type. Used to restrict if this rule definition can be modified.
	 * If rule definition is a rollup, it's also used to restrict adding children rule definitions to this rollup rule definition.
	 * For example, we can prohibit rules used globally to only be modified by administrators.
	 */
	private SystemCondition overrideRuleDefinitionModifyCondition;
	/**
	 * Optionally override to modify condition on the rule type. Used to restrict if this rule definition can be assigned.
	 * If rule definition is a rollup, it's also used to restrict adding children rule definitions to this rollup rule definition.
	 * For example, we may restrict assignments for Investor ID rules to only be modified by Settlements group
	 */
	private SystemCondition overrideRuleAssignmentModifyCondition;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * A List of custom column values for this rule definition (field are assigned and vary by rule type)
	 */
	@NonPersistentField
	private String columnGroupName;

	/**
	 * JSON String value representing the custom column values
	 */
	private CustomJsonString customColumns;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isRollup() {
		if (getRuleType() != null && getRuleType().getRuleCategory() != null) {
			return getRuleType().getRuleCategory().isRollup();
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getRuleType() != null) {
			return ObjectUtils.coalesce(getOverrideRuleDefinitionModifyCondition(), getRuleType().getRuleDefinitionModifyCondition());
		}
		return getOverrideRuleDefinitionModifyCondition();
	}


	public SystemCondition getCoalesceAssignmentEntityModifyCondition() {
		if (getRuleType() != null) {
			return ObjectUtils.coalesce(getOverrideRuleAssignmentModifyCondition(), getRuleType().getRuleAssignmentModifyCondition());
		}
		return getOverrideRuleAssignmentModifyCondition();
	}
}
