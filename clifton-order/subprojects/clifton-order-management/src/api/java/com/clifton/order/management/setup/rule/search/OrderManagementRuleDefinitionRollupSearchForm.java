package com.clifton.order.management.setup.rule.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleDefinitionRollupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer parentRuleId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer childRuleId;
}

