package com.clifton.order.management.setup.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementDestinationMappingSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {


	@SearchField(searchField = "orderType.name,executingBrokerCompany.name,orderDestination.name", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "orderType.id", sortField = "orderType.name")
	private Short orderTypeId;

	@SearchField(searchField = "executingBrokerCompany.id", sortField = "executingBrokerCompany.name")
	private Integer executingBrokerCompanyId;

	@SearchField(searchField = "executingBrokerCompany.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL, sortField = "executingBrokerCompany.name")
	private Integer executingBrokerCompanyIdOrNull;

	@SearchField(searchField = "executingBrokerCompany.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Integer[] executingBrokerCompanyIdsOrNull;


	@SearchField(searchField = "orderDestination.id", sortField = "orderDestination.name")
	private Short orderDestinationId;

	@SearchField(searchField = "name,alias", searchFieldPath = "executingBrokerCompany", sortField = "name")
	private String executingBrokerCompanyName;
}
