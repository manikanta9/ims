package com.clifton.order.management.setup.rule.retriever;

import com.clifton.order.management.setup.rule.OrderManagementRuleAssignment;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinition;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;


/**
 * The <code>OrderManagementRuleAssignmentExtended</code> object is used to expand rollup rules into detailed assignments for viewing and execution at the child/standard rule level
 * <p>
 * When expanded, the OrderManagementRuleAssignment.RuleDefinition property will contain the child rule definition and the rollup rule definition is pulled into a separate property here
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderManagementRuleAssignmentExtended extends OrderManagementRuleAssignment {
	
	/**
	 * Unique identifier - since each rollup assignment can apply to multiple results (needed for UI to display each separately)
	 */
	private String uuid;
	/**
	 * Can't use "id" here - UI is merging records together by id
	 */
	private Integer ruleAssignmentId;
	/**
	 * If the assignment is for a rollup rule, there is a record for that assignment and each child within that rule
	 * Otherwise the child rule definition is the rule definition on the assignment
	 */
	public OrderManagementRuleDefinition rollupRuleDefinition;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static OrderManagementRuleAssignmentExtended forNonRollupRuleAssignment(OrderManagementRuleAssignment ruleAssignment) {
		return OrderManagementRuleAssignmentExtended.forRuleAssignmentImpl(ruleAssignment, null);
	}


	public static OrderManagementRuleAssignmentExtended forRollupRuleAssignmentWithChild(OrderManagementRuleAssignment ruleAssignment, OrderManagementRuleDefinition childRuleDefinition) {
		return OrderManagementRuleAssignmentExtended.forRuleAssignmentImpl(ruleAssignment, childRuleDefinition);
	}


	private static OrderManagementRuleAssignmentExtended forRuleAssignmentImpl(OrderManagementRuleAssignment ruleAssignment, OrderManagementRuleDefinition childRuleDefinition) {
		OrderManagementRuleAssignmentExtended ruleAssignmentExtended = new OrderManagementRuleAssignmentExtended();
		if (childRuleDefinition != null) {
			ruleAssignmentExtended.setRollupRuleDefinition(ruleAssignment.getRuleDefinition());
			ruleAssignmentExtended.setRuleDefinition(childRuleDefinition);
		}
		else {
			ruleAssignmentExtended.setRuleDefinition(ruleAssignment.getRuleDefinition());
		}
		ruleAssignmentExtended.setUuid(UUID.randomUUID().toString());
		ruleAssignmentExtended.setRuleAssignmentId(ruleAssignment.getId());
		ruleAssignmentExtended.setClientAccount(ruleAssignment.getClientAccount());
		ruleAssignmentExtended.setHoldingAccount(ruleAssignment.getHoldingAccount());
		ruleAssignmentExtended.setHoldingAccountIssuingCompany(ruleAssignment.getHoldingAccountIssuingCompany());
		ruleAssignmentExtended.setStartDate(ruleAssignment.getStartDate());
		ruleAssignmentExtended.setEndDate(ruleAssignment.getEndDate());
		ruleAssignmentExtended.setExcluded(ruleAssignment.isExcluded());
		ruleAssignmentExtended.setNote(ruleAssignment.getNote());
		return ruleAssignmentExtended;
	}
}
