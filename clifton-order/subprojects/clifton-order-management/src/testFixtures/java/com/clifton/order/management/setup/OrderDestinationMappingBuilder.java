package com.clifton.order.management.setup;

import com.clifton.core.util.date.DateUtils;
import com.clifton.order.setup.OrderType;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.shared.OrderCompany;
import com.clifton.order.shared.OrderSharedTestObjectFactory;
import lombok.Builder;

import java.util.Date;


/**
 * @author AbhinayaM
 */
public class OrderDestinationMappingBuilder {


	protected static final Date START_DATE = DateUtils.toDate("06/07/2021");

	protected static final Date END_DATE = DateUtils.toDate("06/30/2021");

	protected static final String DESTINATION = "FX Connect (Seattle)";

	protected static final String EXECUTION_BROKER = "Brown Brothers Harriman & Co";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Builder
	public static OrderManagementDestinationMapping build(
			int id,
			Date startDate,
			Date endDate,
			OrderType orderType,
			OrderDestination orderDestinationLabel,
			OrderCompany executingBrokerCompany
	) {
		OrderManagementDestinationMapping orderDestinationMapping = new OrderManagementDestinationMapping();
		orderDestinationMapping.setId(id);
		orderDestinationMapping.setStartDate(startDate);
		orderDestinationMapping.setEndDate(endDate);
		orderDestinationMapping.setOrderType(orderType);
		orderDestinationMapping.setOrderDestination(orderDestinationLabel);
		orderDestinationMapping.setExecutingBrokerCompany(executingBrokerCompany);
		return orderDestinationMapping;
	}


	public static InnerBuilder forFxConnectWithBBH() {
		//get OrderType
		OrderType orderType = new OrderType();
		orderType.setId((short) 1);
		orderType.setName(OrderType.ORDER_TYPE_CURRENCY);
		orderType.setDescription("Spot Currency Orders");
		orderType.setSecurityType(OrderSharedTestObjectFactory.newCurrencySecurityType());
		orderType.setAllowOrderBlockNetting(true);

		//get destination
		OrderDestination destination = new OrderDestination();
		destination.setId((short) 2);
		destination.setLabel(DESTINATION);
		destination.setName(DESTINATION);

		//get executingBroker
		OrderCompany executingBrokerCompany = new OrderCompany();
		executingBrokerCompany.setId(1178);
		executingBrokerCompany.setLabel(EXECUTION_BROKER);
		executingBrokerCompany.setName(EXECUTION_BROKER);
		executingBrokerCompany.setAbbreviation("BBHC");

		return builder()
				.id(1)
				.orderType(orderType)
				.orderDestinationLabel(destination)
				.executingBrokerCompany(executingBrokerCompany)
				.startDate(START_DATE)
				.endDate(END_DATE);
	}
}

