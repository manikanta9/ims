package com.clifton.order.management.execution.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.execution.OrderExecutionService;
import com.clifton.order.execution.OrderPlacement;
import com.clifton.order.execution.search.OrderPlacementSearchForm;
import com.clifton.order.setup.OrderExecutionActions;
import com.clifton.order.setup.OrderExecutionStatus;
import com.clifton.order.setup.destination.OrderDestinationConfiguration;
import com.clifton.order.setup.destination.OrderDestinationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
public class OrderManagementExecutionActionServiceInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private OrderDestinationService orderDestinationService;

	@Resource
	private OrderExecutionService orderExecutionService;

	@Resource
	private OrderManagementExecutionActionService orderManagementExecutionActionService;


	private static final long FILE_PLACEMENT_ID_READY_FOR_BATCHING = 136;
	private static final long FIX_PLACEMENT_ID_SENT = 137;
	private static final long FILE_PLACEMENT_ID_SENT = 130; // 131 & 132 ALSO SENT IN THE SAME BATCH

	private static final long FILE_PLACEMENT_ID_COMPLETED = 122;
	private static final long FIX_PLACEMENT_ID_COMPLETED = 115;

	private static final long FIX_PLACEMENT_ID_REJECTED = 133;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFileSwitchToManual_NoCancel() {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(FILE_PLACEMENT_ID_READY_FOR_BATCHING);
		Assertions.assertEquals("Ready for Batching", placement.getExecutionStatus().getName());

		switchToManual(placement.getId(), null);
		// Should now be in Draft status and Manual Destination
		validateManualPlacement(placement.getId());
	}


	@Test
	public void testFixSwitchToManual_NoCancel() {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(FIX_PLACEMENT_ID_SENT);
		Assertions.assertEquals("Sent", placement.getExecutionStatus().getName());
		switchToManual(placement.getId(), null);
		// Should now be in Draft status and Manual Destination
		validateManualPlacement(placement.getId());
	}


	@Test
	public void testFileSwitchToManual_Cancel() {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(FILE_PLACEMENT_ID_SENT);
		Assertions.assertEquals("Sent", placement.getExecutionStatus().getName());
		switchToManual(placement.getId(), null);
		// Should now be in Canceled status
		validateCanceledPlacement(placement.getId());
		// New placement should have been created in Manual/Draft
		validateManualPlacement(getActivePlacementIdForCanceledPlacement(placement.getId()));
	}


	@Test
	public void testFileSwitchToManual_Error_AlreadyFilled() {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(FILE_PLACEMENT_ID_COMPLETED);
		Assertions.assertEquals("Filled", placement.getExecutionStatus().getName());
		switchToManual(placement.getId(), "Can only SWITCH_TO_MANUAL placements in Draft, Error, Batched, Ready for Batching, Sent Statuses.");
	}


	@Test
	public void testFixSwitchToManual_Error_AlreadyFilled() {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(FIX_PLACEMENT_ID_COMPLETED);
		Assertions.assertEquals("Filled", placement.getExecutionStatus().getName());
		switchToManual(placement.getId(), "Can only SWITCH_TO_MANUAL placements in Draft, Error, Batched, Ready for Batching, Sent Statuses.");
	}


	@Test
	public void testFix_ResendRejected_SwitchDestinationConfiguration() {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(FIX_PLACEMENT_ID_REJECTED);
		Assertions.assertEquals(OrderExecutionStatus.REJECTED, placement.getExecutionStatus().getName());
		OrderDestinationConfiguration destinationConfiguration = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(this.orderDestinationService.getOrderDestinationConfigurationListForDestination(placement.getOrderDestination().getId(), true), OrderDestinationConfiguration::getName, "RFS"));
		OrderManagementExecutionActionCommand command = OrderManagementExecutionActionCommand.forExecutionAction(OrderExecutionActions.REJECT_CANCEL_SWITCH_DESTINATION_CONFIGURATION);
		command.setSwitchToDestinationConfigurationId(destinationConfiguration.getId());
		this.orderManagementExecutionActionService.processOrderManagementExecutionActionForCommand(placement.getId(), command, false);

		validateCanceledPlacement(placement.getId(), OrderExecutionStatus.REJECTED_CANCELED);

		OrderPlacement newPlacement = this.orderExecutionService.getOrderPlacement(getActivePlacementIdForCanceledPlacement(placement.getId()));
		Assertions.assertEquals(placement.getOrderDestination(), newPlacement.getOrderDestination());
		Assertions.assertEquals(destinationConfiguration, newPlacement.getOrderDestinationConfiguration());
		Assertions.assertEquals(OrderExecutionStatus.SENT, newPlacement.getExecutionStatus().getName());
		Assertions.assertEquals(placement.getOrderBlock().getQuantityPlaced(), placement.getQuantityIntended());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void switchToManual(long placementId, String expectedErrorMessage) {
		OrderManagementExecutionActionCommand command = OrderManagementExecutionActionCommand.forExecutionAction(OrderExecutionActions.SWITCH_TO_MANUAL);
		if (!StringUtils.isEmpty(expectedErrorMessage)) {
			ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderManagementExecutionActionService.processOrderManagementExecutionActionForCommand(placementId, command, false));
			Assertions.assertEquals(expectedErrorMessage, e.getMessage());
		}
		else {
			Assertions.assertDoesNotThrow(() -> this.orderManagementExecutionActionService.processOrderManagementExecutionActionForCommand(placementId, command, false));
		}
	}


	private void validateManualPlacement(long placementId) {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(placementId);
		Assertions.assertEquals("Manual", placement.getOrderDestination().getName());
		Assertions.assertEquals("Draft", placement.getExecutionStatus().getName());
		Assertions.assertEquals(placement.getOrderBlock().getQuantityPlaced(), placement.getQuantityIntended());
	}


	private void validateCanceledPlacement(long placementId) {
		validateCanceledPlacement(placementId, OrderExecutionStatus.CANCELED_STATUS);
	}


	private void validateCanceledPlacement(long placementId, String canceledStatusName) {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(placementId);
		Assertions.assertEquals(canceledStatusName, placement.getExecutionStatus().getName());
	}


	private long getActivePlacementIdForCanceledPlacement(long placementId) {
		OrderPlacement placement = this.orderExecutionService.getOrderPlacement(placementId);

		OrderPlacementSearchForm searchForm = new OrderPlacementSearchForm();
		searchForm.setOrderBlockId(placement.getOrderBlock().getId());
		searchForm.setActivePlacementsOnly(true);
		return CollectionUtils.getOnlyElementStrict(this.orderExecutionService.getOrderPlacementList(searchForm)).getId();
	}
}
