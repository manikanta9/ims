package com.clifton.order.management.execution.upload;

import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.system.bean.SystemBeanService;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;


/**
 * @author manderson
 */
@ContextConfiguration
public class OrderExecutionUploadServiceInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	private static final File testFile;


	static {
		try {
			testFile = new ClassPathResource("com/clifton/order/management/execution/upload/ExecutionUploadTestFile.xlsx").getFile();
		}
		catch (IOException e) {
			throw new RuntimeException("Unable to instantiate test file ", e);
		}
	}


	@Resource
	private OrderExecutionUploadService orderExecutionUploadService;

	@Resource
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testUpload_MissingFile() {
		testUploadWithExpectedError(null, ValidationException.class, "Missing upload file.");
	}


	@Test
	public void testUpload_MissingData_Error() {
		testUploadWithExpectedError("EmptyTab", ValidationException.class, "No data available to import.");
	}


	@Test
	public void testUpload_PlacementCompleted_Skipped() {
		Status result = testUploadWithExpectedResultMessage("PlacementCompleted", "Placement Execution Upload Processing Complete. No Placements Updated.");
		Assertions.assertEquals(12, result.getSkippedCount());
	}


	@Test
	public void testUpload_InvalidPlacementId_Error() {
		testUploadWithExpectedResultMessage("InvalidPlacement", "Placement Execution Upload Processing Complete. No Placements Updated. Error: Cannot find a placement for row 2; ");
	}


	@Test
	public void testUpload_MissingPlacementIdColumn_Error() {
		testUploadWithExpectedResultMessage("MissingPlacementIDColumn", "Placement Execution Upload Processing Complete. No Placements Updated. Error: Cannot find a placement for row 2; ");
	}


	@Test
	public void testUpload_MissingPriceColumn_Error() {
		testUploadWithExpectedResultMessage("MissingPriceColumn", "Placement Execution Upload Processing Complete. No Placements Updated. Error: Error Processing row 2: Missing Price for placement [136: SELL 2,900 of GBP on 06/22/2021 [Pershing Advisor Solutions LLC] [Pershing FX Email]].; ");
	}


	@Test
	public void testUpload_MissingPrice_Error() {
		testUploadWithExpectedResultMessage("MissingPrice", "Placement Execution Upload Processing Complete. No Placements Updated. Error: Error Processing row 2: Missing Price for placement [136: SELL 2,900 of GBP on 06/22/2021 [Pershing Advisor Solutions LLC] [Pershing FX Email]].; ");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends Throwable> void testUploadWithExpectedError(String tabName, Class<T> expectedType, String expectedMessage) {
		OrderPlacementExecutionUploadCommand command = populateCommand(tabName);
		T e = Assertions.assertThrows(expectedType, () -> this.orderExecutionUploadService.uploadOrderPlacementExecutionFile(command));
		Assertions.assertEquals(expectedMessage, e.getMessage());
	}


	private Status testUploadWithExpectedResultMessage(String tabName, String expectedResult) {
		OrderPlacementExecutionUploadCommand command = populateCommand(tabName);
		Status result = this.orderExecutionUploadService.uploadOrderPlacementExecutionFile(command);
		Assertions.assertEquals(expectedResult, result.getMessage());
		return result;
	}


	private OrderPlacementExecutionUploadCommand populateCommand(String tabName) {
		OrderPlacementExecutionUploadCommand uploadCommand = new OrderPlacementExecutionUploadCommand();
		uploadCommand.setOrderExecutionUploadConverterBean(this.systemBeanService.getSystemBeanByName("Pershing FX Execution Upload Converter"));
		if (tabName != null) {
			Workbook workbook = ExcelFileUtils.createWorkbook(testFile);
			uploadCommand.setSheetIndex(workbook.getSheetIndex(tabName));
			uploadCommand.setFile(new MultipartFileImpl(testFile));
		}
		return uploadCommand;
	}
}
