package com.clifton.order.management;


import com.clifton.core.util.CollectionUtils;
import com.clifton.order.OrderProjectBasicTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderManagementProjectBasicTests extends OrderProjectBasicTests {

	@Override
	public String getProjectPrefix() {
		return "order-management";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("activity");
		approvedList.add("block");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.apache.poi.ss.usermodel.Workbook");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		return CollectionUtils.createHashSet(
				"getOrderManagementRuleDefinition",
				"deleteOrderManagementRuleDefinition",
				"getOrderManagementRuleAssignmentExtendedList",
				"saveOrderManagementRuleAssignment"
		).contains(method.getName());
	}
}

