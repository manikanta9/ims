package com.clifton.order.management.setup.rule.destination;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.management.setup.OrderManagementDestinationMapping;
import com.clifton.order.management.setup.OrderManagementSetupService;
import com.clifton.order.management.setup.search.OrderManagementDestinationMappingSearchCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
public class OrderManagementDestinationMappingCacheInMemoryDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private OrderManagementSetupService orderManagementSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderManagementDestinationMappingListForCommand_NoOrderType() {
		OrderManagementDestinationMappingSearchCommand command = destinationMappingSearchCommand(null, (short) 10, 110);
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> this.orderManagementSetupService.getOrderManagementDestinationMappingListForCommand(command));
		Assertions.assertEquals("Order Type is required.", exception.getMessage());
	}


	@Test
	public void testOrderManagementDestinationMappingListForCommand_with_OrderType() {
		OrderManagementDestinationMappingSearchCommand command = destinationMappingSearchCommand((short) 1, null, null);
		List<OrderManagementDestinationMapping> orderManagementDestinationMappingList = this.orderManagementSetupService.getOrderManagementDestinationMappingListForCommand(command);
		Assertions.assertEquals(11, CollectionUtils.getSize(orderManagementDestinationMappingList));
	}


	@Test
	public void testOrderManagementDestinationMappingListForCommand_with_OrderType_Destination() {
		OrderManagementDestinationMappingSearchCommand command = destinationMappingSearchCommand((short) 1, (short) 10, null);
		List<OrderManagementDestinationMapping> orderManagementDestinationMappingList = this.orderManagementSetupService.getOrderManagementDestinationMappingListForCommand(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(orderManagementDestinationMappingList));
	}


	@Test
	public void testOrderManagementDestinationMappingListForCommand_with_OrderType_Destination_Broker() {
		OrderManagementDestinationMappingSearchCommand command = destinationMappingSearchCommand((short) 1, (short) 10, 110);
		List<OrderManagementDestinationMapping> orderManagementDestinationMappingList = this.orderManagementSetupService.getOrderManagementDestinationMappingListForCommand(command);
		Assertions.assertEquals(0, CollectionUtils.getSize(orderManagementDestinationMappingList));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private OrderManagementDestinationMappingSearchCommand destinationMappingSearchCommand(Short orderTypeId, Short destinationId, Integer executingBrokerId) {
		OrderManagementDestinationMappingSearchCommand command = new OrderManagementDestinationMappingSearchCommand();
		command.setOrderTypeId(orderTypeId);
		command.setOrderDestinationId(destinationId);
		command.setExecutingBrokerCompanyId(executingBrokerId);
		return command;
	}
}
