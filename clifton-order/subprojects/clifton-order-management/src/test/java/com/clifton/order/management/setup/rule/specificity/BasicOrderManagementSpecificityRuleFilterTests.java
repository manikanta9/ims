package com.clifton.order.management.setup.rule.specificity;

import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.management.rule.OrderRuleEvaluatorContext;
import com.clifton.order.management.setup.rule.OrderManagementRuleDefinition;
import com.clifton.order.management.setup.rule.retriever.OrderManagementRuleAssignmentExtended;
import com.clifton.order.shared.OrderSharedUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration(("classpath:com/clifton/order/management/OrderManagementProjectBasicTests-context.xml"))
@ExtendWith(SpringExtension.class)
public class BasicOrderManagementSpecificityRuleFilterTests {

	private static final String SECURITY_TYPE_COLUMN_NAME = "SecurityType";
	private static final String SECURITY_COLUMN_NAME = "Security";


	@Test
	public void testSpecificitySecurityType_Security() {
		BasicOrderManagementSpecificityRuleFilter filter = new BasicOrderManagementSpecificityRuleFilter();
		filter.setSpecificityLevel1PropertyName(SECURITY_COLUMN_NAME);
		filter.setSpecificityLevel1CustomColumn(true);

		filter.setSpecificityLevel2PropertyName(SECURITY_TYPE_COLUMN_NAME);
		filter.setSpecificityLevel2CustomColumn(true);


		List<OrderManagementRuleAssignmentExtended> ruleAssignmentExtendedList = CollectionUtils.createList(createSecurityRuleAssignment(1, OrderSharedUtils.SECURITY_TYPE_CURRENCY, null), createSecurityRuleAssignment(2, null, "AUD"));

		List<OrderManagementRuleAssignmentExtended> result = filter.applySpecificityFiltering(ruleAssignmentExtendedList, new OrderAllocation(), new OrderRuleEvaluatorContext());
		Assertions.assertEquals(1, CollectionUtils.getSize(result));
		Assertions.assertEquals(2, result.get(0).getId(), "Expected security specific rule only to be returned from specificity filtering");
	}


	@Test
	public void testGetSpecificityLevel() {
		BasicOrderManagementSpecificityRuleFilter filter = new BasicOrderManagementSpecificityRuleFilter();
		filter.setSpecificityLevel1PropertyName(SECURITY_COLUMN_NAME);
		filter.setSpecificityLevel1CustomColumn(true);

		filter.setSpecificityLevel2PropertyName(SECURITY_TYPE_COLUMN_NAME);
		filter.setSpecificityLevel2CustomColumn(true);

		Assertions.assertEquals(16, filter.getSpecificityLevel(createSecurityRuleAssignment(1, OrderSharedUtils.SECURITY_TYPE_CURRENCY, null)));
		Assertions.assertEquals(32, filter.getSpecificityLevel(createSecurityRuleAssignment(2, null, "AUD")));
	}


	////////////////////////////////////////////////////////////////////////////
	////////                 Helper Methods                             ////////
	////////////////////////////////////////////////////////////////////////////


	private OrderManagementRuleAssignmentExtended createSecurityRuleAssignment(int id, String securityType, String securitySymbol) {
		OrderManagementRuleAssignmentExtended ruleAssignmentExtended = new OrderManagementRuleAssignmentExtended();
		ruleAssignmentExtended.setId(id);
		OrderManagementRuleDefinition ruleDefinition = new OrderManagementRuleDefinition();
		CustomJsonString jsonString = new CustomJsonString();
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(jsonString);

		setCustomJsonStringValue(jsonString, SECURITY_TYPE_COLUMN_NAME, securityType);
		setCustomJsonStringValue(jsonString, SECURITY_COLUMN_NAME, securitySymbol);
		CustomJsonStringUtils.resetCustomJsonStringFromJsonValueMap(jsonString);
		ruleDefinition.setCustomColumns(jsonString);
		ruleAssignmentExtended.setRuleDefinition(ruleDefinition);
		return ruleAssignmentExtended;
	}


	private void setCustomJsonStringValue(CustomJsonString customJsonString, String columnName, String value) {
		if (value != null) {
			customJsonString.setColumnValue(columnName, value, value);
		}
	}
}
