package com.clifton.order.management.setup.rule.cache;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.management.setup.rule.OrderManagementRuleAssignment;
import com.clifton.order.management.setup.rule.OrderManagementRuleService;
import com.clifton.order.shared.OrderAccount;
import com.clifton.order.shared.OrderSharedService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.Set;


/**
 * This test is used to ensure we are properly clearing the cache when rule assignments are changed and avoid clearing the cache for irrelevant changes.
 *
 * @author manderson
 */
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
@ContextConfiguration
public class OrderManagementRuleAssignmentHolderCacheTests {

	private static final Integer TEST_ACCOUNT_ID = 1;
	private static final Integer TEST_GLOBAL_ASSIGNMENT_ID = 1;
	private static final Integer TEST_HOLDING_ACCOUNT_ASSIGNMENT_ID = 6;
	private static final Integer TEST_ACCOUNT_ASSIGNMENT_ID = 574;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Resource
	private CacheHandler<String, Set<OrderManagementRuleAssignmentHolder>> cacheHandler;

	@Resource
	private OrderManagementRuleAssignmentHolderCacheImpl orderManagementRuleAssignmentHolderCache;

	@Resource
	private OrderSharedService orderSharedService;

	@Resource
	private OrderManagementRuleService orderManagementRuleService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCacheAccess() {
		// Cache should be empty
		validateCacheSize(0);

		this.orderManagementRuleService = Mockito.spy(this.orderManagementRuleService);
		this.orderManagementRuleAssignmentHolderCache.setOrderManagementRuleService(this.orderManagementRuleService);

		// Pull the Rules for an account and Currency Rules
		OrderAccount testAccount = this.orderSharedService.getOrderAccount(TEST_ACCOUNT_ID);
		this.orderManagementRuleAssignmentHolderCache.getOrderManagementRuleAssignmentHolderSetForAccount(TEST_ACCOUNT_ID, TEST_ACCOUNT_ID, testAccount.getIssuingCompany().getId(), new Date());
		Mockito.verify(this.orderManagementRuleService, Mockito.times(3)).getOrderManagementRuleAssignmentList(Mockito.any());
		// Check Cache Stats again - should be a size of 3, with the following keys
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);


		// Pull again - still should be a size of 3 all from cache
		Mockito.clearInvocations(this.orderManagementRuleService);
		this.orderManagementRuleAssignmentHolderCache.getOrderManagementRuleAssignmentHolderSetForAccount(TEST_ACCOUNT_ID, TEST_ACCOUNT_ID, testAccount.getIssuingCompany().getId(), new Date());
		Mockito.verify(this.orderManagementRuleService, Mockito.times(0)).getOrderManagementRuleAssignmentList(Mockito.any());
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);
	}


	@Test
	public void testCacheClearOnRuleAssignmentDelete() {
		OrderAccount testAccount = this.orderSharedService.getOrderAccount(TEST_ACCOUNT_ID);
		this.orderManagementRuleAssignmentHolderCache.getOrderManagementRuleAssignmentHolderSetForAccount(TEST_ACCOUNT_ID, TEST_ACCOUNT_ID, testAccount.getIssuingCompany().getId(), new Date());

		// Check Cache Stats - should be a size of 3, with the following keys
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// DELETE GLOBAL RULE ASSIGNMENT
		this.orderManagementRuleService.deleteOrderManagementRuleAssignment(TEST_GLOBAL_ASSIGNMENT_ID);

		// Check Cache Stats - should be a size of 2, with the following keys
		validateCacheSize(2, "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// DELETE HOLDING ACCOUNT ISSUER ASSIGNMENT
		this.orderManagementRuleService.deleteOrderManagementRuleAssignment(TEST_HOLDING_ACCOUNT_ASSIGNMENT_ID);

		// Check Cache Stats - should be a size of 1, with the following key
		validateCacheSize(1, "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// DELETE ACCOUNT ASSIGNMENT
		this.orderManagementRuleService.deleteOrderManagementRuleAssignment(TEST_ACCOUNT_ASSIGNMENT_ID);

		// Cache should now be empty
		validateCacheSize(0);
	}


	@Test
	public void testCacheClearOnRuleAssignmentInsert() {
		OrderAccount testAccount = this.orderSharedService.getOrderAccount(TEST_ACCOUNT_ID);
		this.orderManagementRuleAssignmentHolderCache.getOrderManagementRuleAssignmentHolderSetForAccount(TEST_ACCOUNT_ID, TEST_ACCOUNT_ID, testAccount.getIssuingCompany().getId(), new Date());

		// Check Cache Stats - should be a size of 3, with the following keys
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		OrderManagementRuleAssignment globalTestAssignment = this.orderManagementRuleService.getOrderManagementRuleAssignment(TEST_GLOBAL_ASSIGNMENT_ID);
		OrderManagementRuleAssignment accountTestAssignment = this.orderManagementRuleService.getOrderManagementRuleAssignment(TEST_ACCOUNT_ASSIGNMENT_ID);

		// ADD GLOBAL RULE ASSIGNMENT
		OrderManagementRuleAssignment newGlobalAssignment = new OrderManagementRuleAssignment();
		newGlobalAssignment.setRuleDefinition(accountTestAssignment.getRuleDefinition());
		newGlobalAssignment.setNote("Test");
		this.orderManagementRuleService.saveOrderManagementRuleAssignment(newGlobalAssignment);

		// Check Cache Stats - should be a size of 2, with the following keys
		validateCacheSize(2, "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// ADD HOLDING ACCOUNT ISSUER ASSIGNMENT
		OrderManagementRuleAssignment newIssuerAssignment = new OrderManagementRuleAssignment();
		newIssuerAssignment.setHoldingAccountIssuingCompany(testAccount.getIssuingCompany());
		newIssuerAssignment.setRuleDefinition(accountTestAssignment.getRuleDefinition());
		newIssuerAssignment.setNote("Test");
		this.orderManagementRuleService.saveOrderManagementRuleAssignment(newIssuerAssignment);

		// Check Cache Stats - should be a size of 1, with the following key
		validateCacheSize(1, "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// ADD ACCOUNT ASSIGNMENT
		OrderManagementRuleAssignment newAccountAssignment = new OrderManagementRuleAssignment();
		newAccountAssignment.setClientAccount(testAccount);
		newAccountAssignment.setHoldingAccount(testAccount);
		newAccountAssignment.setRuleDefinition(globalTestAssignment.getRuleDefinition());
		newAccountAssignment.setExcluded(true);
		newAccountAssignment.setNote("Test");
		this.orderManagementRuleService.saveOrderManagementRuleAssignment(newAccountAssignment);

		// Cache should now be empty
		validateCacheSize(0);
	}


	@Test
	public void testCacheClearOnRuleAssignmentUpdate() {
		OrderAccount testAccount = this.orderSharedService.getOrderAccount(TEST_ACCOUNT_ID);
		this.orderManagementRuleAssignmentHolderCache.getOrderManagementRuleAssignmentHolderSetForAccount(TEST_ACCOUNT_ID, TEST_ACCOUNT_ID, testAccount.getIssuingCompany().getId(), new Date());

		// Check Cache Stats - should be a size of 3, with the following keys
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		OrderManagementRuleAssignment holdingAccountIssuerTestAssignment = this.orderManagementRuleService.getOrderManagementRuleAssignment(TEST_HOLDING_ACCOUNT_ASSIGNMENT_ID);

		// Add a note
		holdingAccountIssuerTestAssignment.setNote("test update");
		holdingAccountIssuerTestAssignment = this.orderManagementRuleService.saveOrderManagementRuleAssignment(holdingAccountIssuerTestAssignment);

		// Check Cache Stats - should be a size of 3, the cache should NOT have cleared
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// Change excluded flag
		holdingAccountIssuerTestAssignment.setExcluded(true);
		holdingAccountIssuerTestAssignment = this.orderManagementRuleService.saveOrderManagementRuleAssignment(holdingAccountIssuerTestAssignment);

		// Check Cache Stats - should be a size of 3, the cache should NOT have cleared
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// Change rule assignment to be global
		holdingAccountIssuerTestAssignment.setHoldingAccountIssuingCompany(null);
		holdingAccountIssuerTestAssignment = this.orderManagementRuleService.saveOrderManagementRuleAssignment(holdingAccountIssuerTestAssignment);

		// Cache should have cleared both Global and Issuer keys
		validateCacheSize(1, "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// Switch it back - no cache changes
		holdingAccountIssuerTestAssignment.setHoldingAccountIssuingCompany(testAccount.getIssuingCompany());
		holdingAccountIssuerTestAssignment = this.orderManagementRuleService.saveOrderManagementRuleAssignment(holdingAccountIssuerTestAssignment);
		validateCacheSize(1, "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// Pull full list again so it caches
		this.orderManagementRuleAssignmentHolderCache.getOrderManagementRuleAssignmentHolderSetForAccount(TEST_ACCOUNT_ID, TEST_ACCOUNT_ID, testAccount.getIssuingCompany().getId(), new Date());
		validateCacheSize(3, "GLOBAL", "HOLDING_ACCOUNT_ISSUER:" + testAccount.getIssuingCompany().getId(), "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);

		// Set an end date on the assignment
		holdingAccountIssuerTestAssignment.setEndDate(new Date());
		holdingAccountIssuerTestAssignment = this.orderManagementRuleService.saveOrderManagementRuleAssignment(holdingAccountIssuerTestAssignment);

		// Check Cache Stats - should be a size of 2, the cache should have cleared
		validateCacheSize(2, "GLOBAL", "ACCOUNT:" + TEST_ACCOUNT_ID + ":" + TEST_ACCOUNT_ID);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateCacheSize(int expectedSize, String... expectedKeys) {
		Collection<String> keys = this.cacheHandler.getKeys(OrderManagementRuleAssignmentHolderCacheImpl.class.getName());
		Assertions.assertEquals(expectedSize, CollectionUtils.getSize(keys));
		if (expectedKeys != null) {
			for (String expectedKey : expectedKeys) {
				Assertions.assertTrue(keys.contains(expectedKey));
			}
		}
	}
}
