SELECT 'OrderPlacement' AS EntityTableName, p.OrderPlacementID AS EntityID
FROM OrderPlacement p
	 INNER JOIN OrderDestination d ON p.OrderDestinationID = d.OrderDestinationID
	 INNER JOIN OrderBlock b ON p.OrderBlockID = b.OrderBlockID
WHERE b.TradeDate IN ('06/21/2021', '06/22/2021')
UNION
SELECT 'OrderBatchItem', bi.OrderBatchItemID
FROM OrderBatchItem bi
	 INNER JOIN OrderBatch b ON bi.OrderBatchID = b.OrderBatchID
	 INNER JOIN OrderDestination d ON b.OrderDestinationID = d.OrderDestinationID
	 INNER JOIN OrderDestinationType dt ON d.OrderDestinationTypeID = dt.OrderDestinationTypeID
	 INNER JOIN SystemTable tb ON dt.BatchSourceSystemTableID = tb.SystemTableID
WHERE tb.TableName = 'OrderPlacement'
  AND bi.FKFieldID IN (130, 131, 132)
UNION
SELECT 'SystemBeanProperty', SystemBeanPropertyID
FROM SystemBeanProperty p
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
WHERE b.SystemBeanName = 'Pershing FX Execution Upload Converter'
UNION
SELECT 'OrderExecutionStatus', OrderExecutionStatusID
FROM OrderExecutionStatus
UNION
SELECT 'OrderAllocationStatus', OrderAllocationStatusID
FROM OrderAllocationStatus
UNION
SELECT 'OrderDestination', OrderDestinationID
FROM OrderDestination
UNION
SELECT 'OrderDestinationConfiguration', OrderDestinationConfigurationID
FROM OrderDestinationConfiguration
UNION
SELECT 'SystemBeanProperty', SystemBeanPropertyID
FROM SystemBeanProperty p
INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
INNER JOIN OrderDestinationConfiguration c ON b.SystemBeanID = c.ConfigurationBeanID
UNION
SELECT 'SystemListItem', SystemListItemID
FROM SystemListItem i
INNER JOIN SystemList l ON i.SystemListID = l.SystemListID
WHERE l.SystemListName = 'Currency Priority (Currency Conventions)';

