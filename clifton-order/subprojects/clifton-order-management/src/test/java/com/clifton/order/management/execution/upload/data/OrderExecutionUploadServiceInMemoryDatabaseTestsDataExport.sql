SELECT 'OrderPlacement' AS EntityTableName, p.OrderPlacementID AS EntityID
FROM OrderPlacement p
	 INNER JOIN OrderDestination d ON p.OrderDestinationID = d.OrderDestinationID
	 INNER JOIN OrderBlock b ON p.OrderBlockID = b.OrderBlockID
WHERE DestinationName = 'Pershing FX Email'
  AND b.TradeDate IN ('06/21/2021', '06/22/2021')
UNION
SELECT 'SystemBeanProperty', SystemBeanPropertyID
FROM SystemBeanProperty p
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
WHERE b.SystemBeanName = 'Pershing FX Execution Upload Converter'
UNION
SELECT 'OrderExecutionStatus', OrderExecutionStatusID
FROM OrderExecutionStatus
WHERE OrderExecutionStatusID = 1
UNION
SELECT 'OrderAllocationStatus', OrderAllocationStatusID
FROM OrderAllocationStatus
WHERE OrderAllocationStatusID = 9;
