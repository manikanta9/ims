package com.clifton.order.management.rule;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberIntegerMethod;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberMoneyMethod;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberPercentMethod;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberPreciseMethod;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.order.allocation.OrderAllocation;
import com.clifton.order.shared.OrderSecurity;
import com.clifton.order.shared.builder.OrderSecurityBuilder;
import com.clifton.order.shared.OrderSharedUtilHandler;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;


/**
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("OrderRuleAwareRuleEvaluatorTests-context.xml")
public class OrderRuleAwareDateRuleEvaluatorTests {

	@Mock
	private OrderSharedUtilHandler orderSharedUtilHandler;

	@Mock
	private RuleViolationService ruleViolationService;

	@Resource
	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testTradeDateBusinessDay_Pass() {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setId((long) 1);
		orderAllocation.setSecurity(OrderSecurityBuilder.newAUD());
		orderAllocation.setSettlementCurrency(OrderSecurityBuilder.newUSD());
		orderAllocation.setTradeDate(DateUtils.toDate("02/26/2021"));

		OrderRuleAwareDateRuleEvaluator evaluator = createRuleEvaluatorBeanForTradeDates(true);
		evaluator.setViolateIfNotBusinessDay(true);

		validateRuleViolationPass(evaluator, orderAllocation);
	}


	@Test
	public void testTradeDateBusinessDay_Fail() {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setId((long) 1);
		orderAllocation.setSecurity(OrderSecurityBuilder.newAUD());
		orderAllocation.setSettlementCurrency(OrderSecurityBuilder.newUSD());
		orderAllocation.setTradeDate(DateUtils.toDate("02/26/2021"));

		String ruleViolationNoteTemplate = "${dateField} [${linkedEntity.tradeDate?string[\"MM/dd/yyyy\"]}] is not a valid trading business day for security [${linkedEntity.security.ticker}]";

		OrderRuleAwareDateRuleEvaluator evaluator = createRuleEvaluatorBeanForTradeDates(false);
		evaluator.setViolateIfNotBusinessDay(true);

		validateRuleViolation(evaluator, orderAllocation, ruleViolationNoteTemplate, "Trade Date [02/26/2021] is not a valid trading business day for security [AUD]");
	}


	@Test
	public void testSettlementDateBusinessDay_Pass() {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setId((long) 1);
		orderAllocation.setSecurity(OrderSecurityBuilder.newAUD());
		orderAllocation.setSettlementCurrency(OrderSecurityBuilder.newUSD());
		orderAllocation.setTradeDate(DateUtils.toDate("02/26/2021"));
		orderAllocation.setSettlementDate(DateUtils.toDate("03/01/2021"));

		OrderRuleAwareDateRuleEvaluator evaluator = createRuleEvaluatorBeanForSettlementDates(true);
		evaluator.setViolateIfNotBusinessDay(true);

		validateRuleViolationPass(evaluator, orderAllocation);
	}


	@Test
	public void testSettlementDateBusinessDay_Fail() {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setId((long) 1);
		orderAllocation.setSecurity(OrderSecurityBuilder.newAUD());
		orderAllocation.setSettlementCurrency(OrderSecurityBuilder.newUSD());
		orderAllocation.setTradeDate(DateUtils.toDate("02/26/2021"));
		orderAllocation.setSettlementDate(DateUtils.toDate("02/27/2021"));

		OrderRuleAwareDateRuleEvaluator evaluator = createRuleEvaluatorBeanForSettlementDates(false);
		evaluator.setViolateIfNotBusinessDay(true);

		String ruleViolationNoteTemplate = "${dateField} [${linkedEntity.settlementDate?string[\"MM/dd/yyyy\"]}] is not a valid settlement date for security [${linkedEntity.security.ticker}]";
		validateRuleViolation(evaluator, orderAllocation, ruleViolationNoteTemplate, "Settlement Date [02/27/2021] is not a valid settlement date for security [AUD]");
	}


	@Test
	public void testSettlementDateBeforeTradeDate_Fail() {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setId((long) 1);
		orderAllocation.setSecurity(OrderSecurityBuilder.newAUD());
		orderAllocation.setSettlementCurrency(OrderSecurityBuilder.newUSD());
		orderAllocation.setTradeDate(DateUtils.toDate("02/26/2021"));
		orderAllocation.setSettlementDate(DateUtils.toDate("02/25/2021"));

		OrderRuleAwareDateRuleEvaluator evaluator = createRuleEvaluatorBeanForSettlementDates(true);
		evaluator.setViolateIfNotBusinessDay(true);

		String ruleViolationNoteTemplate = "${customMessage}";
		validateRuleViolation(evaluator, orderAllocation, ruleViolationNoteTemplate, "Settlement Date cannot be before Trade Date.");
	}


	@Test
	public void testSettlementDateTooFarInFuture_Fail() {
		OrderAllocation orderAllocation = new OrderAllocation();
		orderAllocation.setId((long) 1);
		orderAllocation.setSecurity(OrderSecurityBuilder.newAUD());
		orderAllocation.setSettlementCurrency(OrderSecurityBuilder.newUSD());
		orderAllocation.setTradeDate(DateUtils.toDate("02/26/2021"));
		orderAllocation.setSettlementDate(DateUtils.toDate("03/05/2021"));

		OrderRuleAwareDateRuleEvaluator evaluator = createRuleEvaluatorBeanForSettlementDates(true);
		evaluator.setViolateIfNotBusinessDay(false);
		evaluator.setViolateIfDateInFuture(true);
		evaluator.setMaxBusinessDaysInFuture(2);
		evaluator.setCompareSettlementDatePastFutureToTradeDate(true);

		String ruleViolationNoteTemplate = "${dateField} [${linkedEntity.settlementDate?string[\"MM/dd/yyyy\"]}] is more than ${maxBusinessDaysInFuture} weekdays from ${compareSettleToTrade?string('Trade Date', 'today')}.";
		validateRuleViolation(evaluator, orderAllocation, ruleViolationNoteTemplate, "Settlement Date [03/05/2021] is more than 2 weekdays from Trade Date.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////                  Helper Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	private OrderRuleAwareDateRuleEvaluator createRuleEvaluatorBeanForTradeDates(boolean isBusinessDay) {
		OrderRuleAwareDateRuleEvaluator evaluator = createOrderRuleAwareDateRuleEvaluatorBean(isBusinessDay);
		evaluator.setValidateTradeDate(true);
		return evaluator;
	}


	private OrderRuleAwareDateRuleEvaluator createRuleEvaluatorBeanForSettlementDates(boolean isBusinessDay) {
		OrderRuleAwareDateRuleEvaluator evaluator = createOrderRuleAwareDateRuleEvaluatorBean(isBusinessDay);
		evaluator.setValidateTradeDate(false);
		return evaluator;
	}


	private OrderRuleAwareDateRuleEvaluator createOrderRuleAwareDateRuleEvaluatorBean(boolean isBusinessDay) {
		OrderRuleAwareDateRuleEvaluator evaluator = new OrderRuleAwareDateRuleEvaluator();
		Mockito.when(this.orderSharedUtilHandler.isValidTradeDate(ArgumentMatchers.any(OrderSecurity.class), ArgumentMatchers.any(Date.class))).thenReturn(isBusinessDay);
		Mockito.when(this.orderSharedUtilHandler.isValidSettlementDate(ArgumentMatchers.any(OrderSecurity.class), ArgumentMatchers.any(OrderSecurity.class), ArgumentMatchers.any(Date.class))).thenReturn(isBusinessDay);
		evaluator.setOrderSharedUtilHandler(this.orderSharedUtilHandler);
		evaluator.setRuleViolationService(this.ruleViolationService);
		return evaluator;
	}


	private RuleViolation createRuleViolation(OrderAllocation orderAllocation, String violationNoteTemplate, Map<String, Object> templateContextValuesMap) {
		RuleViolation ruleViolation = new RuleViolation();

		//Add objects to context for template evaluation
		templateContextValuesMap.put(RuleEvaluator.CAUSE_ENTITY_FREEMARKER_TEMPLATE_KEY, orderAllocation);

		TemplateConfig config = new TemplateConfig(violationNoteTemplate);
		for (Map.Entry<String, Object> templateContextEntry : CollectionUtils.getIterable(templateContextValuesMap.entrySet())) {
			Object templateContextValue = templateContextEntry.getValue();
			if (templateContextValue != null) {
				config.addBeanToContext(templateContextEntry.getKey(), templateContextValue);
			}
		}
		config.addBeanToContext("linkedEntity", orderAllocation);
		config.addBeanToContext("formatNumberInteger", new FreeMarkerFormatNumberIntegerMethod());
		config.addBeanToContext("formatNumberMoney", new FreeMarkerFormatNumberMoneyMethod());
		config.addBeanToContext("formatNumberPercent", new FreeMarkerFormatNumberPercentMethod());
		config.addBeanToContext("formatNumberPrecise", new FreeMarkerFormatNumberPreciseMethod());

		//generate violation note using freemarker template.
		ruleViolation.setViolationNote(this.templateConverter.convert(config));
		return ruleViolation;
	}


	private void validateRuleViolationPass(OrderRuleAwareDateRuleEvaluator evaluator, OrderAllocation orderAllocation) {
		validateRuleViolation(evaluator, orderAllocation, "No expected violation", null);
	}


	private void validateRuleViolation(OrderRuleAwareDateRuleEvaluator evaluator, OrderAllocation orderAllocation, String ruleViolationNoteTemplate, String expectedMessage) {
		Mockito.when(this.ruleViolationService.createRuleViolationWithCause(ArgumentMatchers.any(EntityConfig.class), ArgumentMatchers.any(Long.class), ArgumentMatchers.any(Integer.class), ArgumentMatchers.nullable(SystemTable.class), ArgumentMatchers.anyMap())).thenAnswer(invocation -> {
			return createRuleViolation(orderAllocation, ruleViolationNoteTemplate, invocation.getArgument(4));
		});

		RuleDefinition ruleDefinition = new RuleDefinition();
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setRuleDefinition(ruleDefinition);

		RuleConfig ruleConfig = new RuleConfig(ruleAssignment);
		EntityConfig entityConfig = new EntityConfig(ruleAssignment);
		ruleConfig.addEntityConfig(entityConfig);

		RuleViolation ruleViolation = CollectionUtils.getFirstElement(evaluator.evaluateRule(orderAllocation, ruleConfig, new OrderRuleEvaluatorContext()));
		if (StringUtils.isEmpty(expectedMessage)) {
			Assertions.assertNull(ruleViolation, () -> String.format("Did not expect a violation to generate, but instead received the following message %s", (ruleViolation == null ? "EMPTY MESSAGE?" : ruleViolation.getViolationNote())));
		}
		else {
			Assertions.assertNotNull(ruleViolation, "Expected rule violation with message " + expectedMessage + ", but did not get any violations");
			Assertions.assertEquals(expectedMessage, ruleViolation.getViolationNote());
		}
	}
}
