package com.clifton.order.management.setup.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.order.management.setup.OrderManagementDestinationMapping;
import com.clifton.order.management.setup.OrderManagementSetupService;
import com.clifton.order.setup.OrderSetupService;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Set;


/**
 * @author AbhinayaM
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class})
public class OrderManagementSetupServiceImplTests {

	@Resource
	private OrderManagementSetupService orderManagementSetupService;

	@Resource
	private OrderDestinationService orderDestinationService;

	@Resource
	private OrderSetupService orderSetupService;


	@Resource
	private CacheHandler<String, Set<OrderManagementDestinationMapping>> cacheHandler;

	private static final short MANUAL_EXECUTION_VENUE = 1;
	private static final short FX_CONNECT_EXECUTION_VENUE = 2;
	private static final short STREET_FX_EXECUTION_VENUE = 3;
	private static final short PERSHING_FX_FILE_EXECUTION_VENUE = 4;
	private static final short CURRENCY_APX_TO_POST_FILE_EXECUTION_VENUE = 5;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveOrderDestination_Error_SwitchToBackUp_WithMappings() {
		OrderDestination pershingDestination = this.orderDestinationService.getOrderDestination(PERSHING_FX_FILE_EXECUTION_VENUE);
		OrderDestination fxConnectDestination = this.orderDestinationService.getOrderDestination(FX_CONNECT_EXECUTION_VENUE);

		pershingDestination.setMainOrderDestination(fxConnectDestination);
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(pershingDestination));
		Assertions.assertEquals("Cannot change Pershing FX Email to a back up because there are destination mappings associated with it.  Destination mappings can only be applied to main destinations that are executing venues.", e.getMessage());
	}


	@Test
	public void testSaveOrderManagementDestinationMapping_Error_NotAnExecutionVenue() {
		OrderManagementDestinationMapping destinationMapping = new OrderManagementDestinationMapping();
		destinationMapping.setOrderDestination(this.orderDestinationService.getOrderDestination(CURRENCY_APX_TO_POST_FILE_EXECUTION_VENUE));
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderManagementSetupService.saveOrderManagementDestinationMapping(destinationMapping));
		Assertions.assertEquals("Destination mappings can only apply to main execution venue destination types.", e.getMessage());
	}


	@Test
	public void testSaveOrderManagementDestinationMapping_Error_ManualDestination() {
		OrderManagementDestinationMapping destinationMapping = new OrderManagementDestinationMapping();
		destinationMapping.setOrderDestination(this.orderDestinationService.getOrderDestination(MANUAL_EXECUTION_VENUE));
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderManagementSetupService.saveOrderManagementDestinationMapping(destinationMapping));
		Assertions.assertEquals("Destination mappings are not valid for manual destinations", e.getMessage());
	}


	@Test
	public void testSaveOrderManagementDestinationMapping_Error_Overlapping() {
		OrderManagementDestinationMapping destinationMapping = this.orderManagementSetupService.getOrderManagementDestinationMapping(1);
		OrderManagementDestinationMapping dupe = BeanUtils.cloneBean(destinationMapping, false, false);
		dupe.setId(null);
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderManagementSetupService.saveOrderManagementDestinationMapping(dupe));
		Assertions.assertEquals("There already exists an order destination mapping for the same options with an overlapping date range: Currency: FX Connect (Seattle): State Street ( - )", e.getMessage());

		destinationMapping.setEndDate(DateUtils.toDate("07/31/2021"));
		this.orderManagementSetupService.saveOrderManagementDestinationMapping(destinationMapping);

		dupe.setStartDate(DateUtils.toDate("08/01/2021"));
		this.orderManagementSetupService.saveOrderManagementDestinationMapping(dupe);

		destinationMapping.setEndDate(DateUtils.toDate("08/15/2021"));
		e = Assertions.assertThrows(ValidationException.class, () -> this.orderManagementSetupService.saveOrderManagementDestinationMapping(destinationMapping));
		Assertions.assertEquals("There already exists an order destination mapping for the same options with an overlapping date range: Currency: FX Connect (Seattle): State Street (08/01/2021 - )", e.getMessage());

		dupe.setStartDate(DateUtils.toDate("07/01/2021"));
		e = Assertions.assertThrows(ValidationException.class, () -> this.orderManagementSetupService.saveOrderManagementDestinationMapping(dupe));
		Assertions.assertEquals("There already exists an order destination mapping for the same options with an overlapping date range: Currency: FX Connect (Seattle): State Street ( - 07/31/2021)", e.getMessage());

		this.orderManagementSetupService.deleteOrderManagementDestinationMapping(dupe.getId());
		destinationMapping.setEndDate(null);
		this.orderManagementSetupService.saveOrderManagementDestinationMapping(destinationMapping);
	}


	@Test
	public void testSaveOrderDestination_Disabled_Error_ActiveMappings() {
		OrderDestination destination = this.orderDestinationService.getOrderDestination(STREET_FX_EXECUTION_VENUE);
		destination.setDisabled(true);
		ValidationException e = Assertions.assertThrows(ValidationException.class, () -> this.orderDestinationService.saveOrderDestination(destination));
		Assertions.assertEquals("Cannot disable Street FX because there are still active destination mappings associated with it.", e.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	//TODO MAKE THE BELOW TESTS PASS AND ADD MORE UNDER OMS-89

/*
	@Test
	public void testOrderManagementDestinationMapping() {

		OrderManagementDestinationMapping orderManagementDestinationMapping = OrderDestinationMappingBuilder.forFxConnectWithBBH().build();
		orderManagementDestinationMapping.setStartDate(DateUtils.toDate("06/07/2021"));
		orderManagementDestinationMapping.setEndDate(DateUtils.toDate("06/30/2021"));
		ValidationException exception = Assertions.assertThrows(ValidationException.class, () -> {
			this.orderManagementSetupService.saveOrderManagementDestinationMapping(orderManagementDestinationMapping);
		});
		Assertions.assertEquals("There already exists an order destination mapping for the same options with an overlapping date range:", exception.getMessage());
	}


	@Test
	public void getOrderManagementDestinationMappingList() {
		OrderManagementDestinationMapping orderManagementDestinationMapping1 = this.saveOrderManagementDestinationMapping((short) 1, (short) 2, "06/09/2021", "06/30/2020");

		OrderManagementDestinationMappingSearchForm searchForm = new OrderManagementDestinationMappingSearchForm();
		searchForm.setOrderTypeId(Short.valueOf("1"));
		searchForm.setOrderDestinationId((short) 2);
		searchForm.setExecutingBrokerCompanyId(1178);
		searchForm.setExecutingBrokerCompanyName("Brown Brothers Harriman & Co");

		List<OrderManagementDestinationMapping> orderManagementDestinationMapping = this.orderManagementSetupService.getOrderManagementDestinationMappingList(searchForm);
		Assertions.assertEquals(orderManagementDestinationMapping1.getId(), orderManagementDestinationMapping.get(0).getId());
	}


	@Test
	public void getOrderManagementDestinationMappingListForCommand() {

		OrderManagementDestinationMappingSearchCommand searchForm = new OrderManagementDestinationMappingSearchCommand();
		searchForm.setOrderTypeId(Short.valueOf("1"));
		searchForm.setOrderDestinationId((short) 2);
		searchForm.setExecutingBrokerCompanyId(1178);
		searchForm.setExecutingBrokerCompanyIdOrNull(1178);
		searchForm.setExecutingBrokerCompanyName("Brown Brothers Harriman & Co");

		List<OrderManagementDestinationMapping> orderManagementDestinationMapping = this.orderManagementSetupService.getOrderManagementDestinationMappingListForCommand(searchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(orderManagementDestinationMapping));
	}


	@Test
	public void testCacheAccess() {
		// Cache should be empty

	}
	////////////////////////////////////////////////////////////////////////////
	////////            Helper methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private OrderManagementDestinationMapping saveOrderManagementDestinationMapping(Short id, Short id1, String start, String end) {
		OrderManagementDestinationMapping orderManagementDestinationMapping = new OrderManagementDestinationMapping();
		orderManagementDestinationMapping.setOrderType(this.orderSetupService.getOrderType(id));
		orderManagementDestinationMapping.setOrderDestination(this.orderDestinationService.getOrderDestination(id1));
		orderManagementDestinationMapping.setStartDate(DateUtils.toDate(start));
		orderManagementDestinationMapping.setEndDate(DateUtils.toDate(end));
		return this.orderManagementSetupService.saveOrderManagementDestinationMapping(orderManagementDestinationMapping);
	}


	//check cache size
	//TODO-ADD test for caching list
	private void validateCacheSize(int expectedSize, String... expectedKeys) {
		Collection<String> keys = this.cacheHandler.getKeys(OrderManagementDestinationMappingByOrderTypeCache.class.getName());
		Assertions.assertEquals(expectedSize, CollectionUtils.getSize(keys));
		if (expectedKeys != null) {
			for (String expectedKey : expectedKeys) {
				Assertions.assertTrue(keys.contains(expectedKey));
			}
		}
	}*/
}
