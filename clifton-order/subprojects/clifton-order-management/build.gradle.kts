import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant

val apiVariant = registerVariant("api", upstreamForMain = true)

dependencies {

	////////////////////////////////////////////////////////////////////////////
	////////            Main Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	// Nothing yet

	////////////////////////////////////////////////////////////////////////////
	////////            API Dependencies                                ////////
	////////////////////////////////////////////////////////////////////////////

	apiVariant.api(project(":clifton-order:clifton-order-shared"))
	apiVariant.api(project(":clifton-order:clifton-order-setup"))
	apiVariant.api(project(":clifton-order:clifton-order-base"))
	apiVariant.api(project(":clifton-order:clifton-order-batch"))
	apiVariant.api(project(":clifton-order:clifton-order-batch")) { usingVariant("api") }

	api(project(":clifton-rule"))

	api(project(":clifton-order:clifton-order-fix"))

	implementation(project(":clifton-document"))

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-system:clifton-system-field-mapping")))
	testFixturesApi(testFixtures(project(":clifton-order:clifton-order-shared")))
	testFixturesApi(testFixtures(project(":clifton-order:clifton-order-setup")))
	testFixturesApi(testFixtures(project(":clifton-order:clifton-order-base")))
	testFixturesApi(testFixtures(project(":clifton-order:clifton-order-batch")))

}
