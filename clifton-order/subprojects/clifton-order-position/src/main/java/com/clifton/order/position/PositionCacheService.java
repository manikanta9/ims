package com.clifton.order.position;


import com.clifton.order.position.dgs.types.ResponsePositions;


/**
 * The {@link PositionCacheService} defines methods for making requests to the Position Cache Service using GraphQL
 *
 * @author lnaylor
 */
public interface PositionCacheService {

	public ResponsePositions getCurrencyPositions(PositionsQueryRequest request);
}
