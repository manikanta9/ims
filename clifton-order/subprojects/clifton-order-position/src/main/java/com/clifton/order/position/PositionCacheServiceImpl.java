package com.clifton.order.position;


import com.clifton.order.position.dgs.client.PositionsGraphQLQuery;
import com.clifton.order.position.dgs.client.PositionsProjectionRoot;
import com.clifton.order.position.dgs.client.Positions_ResultSetProjection;
import com.clifton.order.position.dgs.client.Positions_ResultSet_PortfolioPositionProjection;
import com.clifton.order.position.dgs.types.ResponsePositions;
import com.netflix.graphql.dgs.client.CustomGraphQLClient;
import com.netflix.graphql.dgs.client.GraphQLClient;
import com.netflix.graphql.dgs.client.GraphQLResponse;
import com.netflix.graphql.dgs.client.HttpResponse;
import com.netflix.graphql.dgs.client.codegen.GraphQLQueryRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


/**
 * @author lnaylor
 */
@Service
public class PositionCacheServiceImpl implements PositionCacheService {

	private RestTemplate restTemplate;


	@Value("${positionservice.url:}")
	private String positionServiceUrl;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ResponsePositions getCurrencyPositions(PositionsQueryRequest request) {
		CustomGraphQLClient dgsClient = GraphQLClient.createCustom(getPositionServiceUrl(), (url, headers, body) -> {
			HttpHeaders httpHeaders = new HttpHeaders();
			headers.forEach(httpHeaders::addAll);
			ResponseEntity<String> exchange = getRestTemplate().exchange(url, HttpMethod.POST, new HttpEntity<>(body, httpHeaders), String.class);
			return new HttpResponse(exchange.getStatusCodeValue(), exchange.getBody());
		});

		GraphQLQueryRequest dgsRequest = new GraphQLQueryRequest(PositionsGraphQLQuery.newRequest()
				.requestType(request.getRequestType())
				.portfolioCode(request.getPortfolioCode())
				.filter(request.getFilter()).build(),
				getResponseProjection());

		GraphQLResponse dgsResponse = dgsClient.executeQuery(dgsRequest.serialize());
		if (dgsResponse.getData().containsKey("positions")) {
			return dgsResponse.extractValueAsObject("data.positions", ResponsePositions.class);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Positions_ResultSet_PortfolioPositionProjection getResponseProjection() {
		Positions_ResultSetProjection resultSetProjection = new PositionsProjectionRoot()
				.taskID()
				.elapsedTime()
				.responseCode()
				.responseMessage()
				.resultSet();
		return resultSetProjection
				.onPortfolioPosition()
				.accountNumber()
				.asofDate()
				.portfolioCode()
				.marketValueLocal()
				.localCurrencyISO();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public RestTemplate getRestTemplate() {
		return this.restTemplate;
	}


	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}


	public String getPositionServiceUrl() {
		return this.positionServiceUrl;
	}


	public void setPositionServiceUrl(String positionServiceUrl) {
		this.positionServiceUrl = positionServiceUrl;
	}
}
