package com.clifton.order.position;

import java.time.LocalDate;
import java.util.List;


/**
 * @author lnaylor
 */
public class PositionsQueryRequest {

	private String requestType;
	private LocalDate asOfDate;
	private List<String> portfolioCode;
	private String filter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRequestType() {
		return this.requestType;
	}


	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}


	public LocalDate getAsOfDate() {
		return this.asOfDate;
	}


	public void setAsOfDate(LocalDate asOfDate) {
		this.asOfDate = asOfDate;
	}


	public List<String> getPortfolioCode() {
		return this.portfolioCode;
	}


	public void setPortfolioCode(List<String> portfolioCode) {
		this.portfolioCode = portfolioCode;
	}


	public String getFilter() {
		return this.filter;
	}


	public void setFilter(String filter) {
		this.filter = filter;
	}
}
