package com.clifton.order.position;


import com.clifton.order.position.dgs.types.PortfolioPosition;
import com.clifton.order.position.dgs.types.ResponsePositions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;


/**
 * @author lnaylor
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:/com/clifton/order/position/OrderPositionProjectBasicTests-context.xml")
public class PositionCacheServiceImplTests {

	public static String REQUEST_BODY = "{\"query\":\"query {positions(requestType: \\\"A\\\", portfolioCode: [\\\"C\\\", \\\"D\\\"], filter: \\\"B\\\"){ taskID elapsedTime responseCode responseMessage resultSet   { ... on PortfolioPosition { __typename accountNumber asofDate portfolioCode marketValueLocal localCurrencyISO } } } }\",\"variables\":{},\"operationName\":null}";
	public static String RESPONSE_JSON = "{\"data\":{\"positions\":{\"taskID\":\"TASKID\",\"elapsedTime\":1,\"responseCode\":\"RESPONSECODE\",\"responseMessage\":\"RESPONSEMESSAGE\",\"resultSet\":[{\"__typename\":\"PortfolioPosition\",\"accountNumber\":\"ACCOUNT_NUMBER\",\"asofDate\":\"2021-10-12\",\"portfolioCode\":\"PORTFOLIOCODE\",\"marketValueLocal\":15,\"localCurrencyISO\":\"ISO\"}]}}}";
	public static String RESPONSE_JSON_EMPTY = "{\"data\":{}}";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Mock
	private RestTemplate restTemplate;

	@Resource
	@InjectMocks
	private PositionCacheService positionQueryService;

	private PositionsQueryRequest positionsQueryRequest;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void init() {
		PositionsQueryRequest positionsQueryRequest = new PositionsQueryRequest();
		positionsQueryRequest.setRequestType("A");
		positionsQueryRequest.setFilter("B");
		positionsQueryRequest.setPortfolioCode(Arrays.asList("C", "D"));
		setPositionsQueryRequest(positionsQueryRequest);

		MockitoAnnotations.initMocks(this);
		Mockito.when(getRestTemplate().exchange(Mockito.eq("http://test"), Mockito.eq(HttpMethod.POST), Mockito.argThat(httpEntity -> Objects.equals(httpEntity.getBody(), REQUEST_BODY)), Mockito.eq(String.class))).thenReturn(new ResponseEntity<>(RESPONSE_JSON, HttpStatus.OK));
		Mockito.when(getRestTemplate().exchange(Mockito.eq("http://testPositionsDataNull"), Mockito.eq(HttpMethod.POST), Mockito.argThat(httpEntity -> Objects.equals(httpEntity.getBody(), REQUEST_BODY)), Mockito.eq(String.class))).thenReturn(new ResponseEntity<>(RESPONSE_JSON_EMPTY, HttpStatus.OK));
	}


	@Test
	public void testGetPositions() {
		ReflectionTestUtils.setField(getPositionQueryService(), "positionServiceUrl", "http://test");
		ResponsePositions responsePositions = getPositionQueryService().getCurrencyPositions(getPositionsQueryRequest());

		Assertions.assertEquals("TASKID", responsePositions.getTaskID());
		Assertions.assertEquals(1, responsePositions.getElapsedTime());
		Assertions.assertEquals("RESPONSECODE", responsePositions.getResponseCode());
		Assertions.assertEquals("RESPONSEMESSAGE", responsePositions.getResponseMessage());
		Assertions.assertEquals(1, responsePositions.getResultSet().size());

		PortfolioPosition portfolioPosition = (PortfolioPosition) responsePositions.getResultSet().get(0);
		Assertions.assertEquals(LocalDate.of(2021, 10, 12), portfolioPosition.getAsofDate());
		Assertions.assertEquals("PORTFOLIOCODE", portfolioPosition.getPortfolioCode());
		Assertions.assertEquals(BigDecimal.valueOf(15), portfolioPosition.getMarketValueLocal());
		Assertions.assertEquals("ISO", portfolioPosition.getLocalCurrencyISO());
	}


	@Test
	public void testGetPositionsPositionsDataNull() {
		ReflectionTestUtils.setField(getPositionQueryService(), "positionServiceUrl", "http://testPositionsDataNull");
		Assertions.assertNull(getPositionQueryService().getCurrencyPositions(getPositionsQueryRequest()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PositionCacheService getPositionQueryService() {
		return this.positionQueryService;
	}


	public void setPositionQueryService(PositionCacheService positionQueryService) {
		this.positionQueryService = positionQueryService;
	}


	public RestTemplate getRestTemplate() {
		return this.restTemplate;
	}


	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}


	public PositionsQueryRequest getPositionsQueryRequest() {
		return this.positionsQueryRequest;
	}


	public void setPositionsQueryRequest(PositionsQueryRequest positionsQueryRequest) {
		this.positionsQueryRequest = positionsQueryRequest;
	}
}
