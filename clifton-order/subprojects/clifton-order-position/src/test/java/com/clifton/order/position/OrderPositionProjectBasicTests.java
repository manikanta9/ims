package com.clifton.order.position;

import com.clifton.order.OrderProjectBasicTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderPositionProjectBasicTests extends OrderProjectBasicTests {

	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.http.");
		imports.add("org.springframework.web.");
		imports.add("com.netflix.graphql.dgs.client.");
	}
}
