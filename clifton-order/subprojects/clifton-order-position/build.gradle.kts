import com.netflix.graphql.dgs.codegen.gradle.GenerateJavaTask

plugins {
	id("com.netflix.dgs.codegen") version ("5.1.16")
}

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	implementation("com.netflix.graphql.dgs:graphql-dgs-client:4.9.17")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-core"))
	api(project(":clifton-security"))

	////////////////////////////////////////////////////////////////////////////
	////////            Test Dependencies                               ////////
	////////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-order")))
}

tasks.named<GenerateJavaTask>("generateJava") {
	schemaPaths = mutableListOf("${projectDir}/src/main/resources/dgs") // List of directories containing schema files
	packageName = "com.clifton.order.position.dgs" // The package name to use to generate sources
	generateClient = true // Enable generating the type safe query API
	typeMapping = mutableMapOf(
			Pair("Float", "java.math.BigDecimal"))
}



