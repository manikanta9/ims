package com.clifton.order.batch.observers;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.runner.RunnerUtils;
import com.clifton.order.batch.runner.OrderBatchRunnerProvider;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderDestinationBatchObserver</code> class is a {@link DaoEventObserver} that
 * handles any rescheduling for batched order destinations.
 * A reschedule is deemed necessary during Order Destination
 * Inserts and Deletes (only if the destination type is batched)
 * Or Updates to the schedule (custom column)
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderDestinationBatchObserver extends SelfRegisteringDaoObserver<OrderDestination> {

	private OrderBatchRunnerProvider orderBatchRunnerProvider;
	private RunnerHandler runnerHandler;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<OrderDestination> dao, DaoEventTypes event, OrderDestination bean, Throwable e) {
		if (e == null) {
			if (isRescheduleNecessary(dao, event, bean)) {
				Integer scheduleId = OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(bean);
				RunnerUtils.rescheduleRunnersForEntity(bean, d -> d.getDestinationType().isBatched() && scheduleId != null, Integer.toString(bean.getId()), getOrderBatchRunnerProvider(), getRunnerHandler());
			}
		}
	}


	private boolean isRescheduleNecessary(ReadOnlyDAO<OrderDestination> dao, DaoEventTypes event, OrderDestination bean) {
		// Reschedule for Inserts/Deletes only if the destination is batched (and not disabled)
		if (event.isInsert() || event.isDelete()) {
			return !bean.isDisabled() && bean.getDestinationType().isBatched();
		}

		// Updates
		OrderDestination originalBean = getOriginalBean(dao, bean);

		// Batched flag switched
		if (originalBean.getDestinationType().isBatched() != bean.getDestinationType().isBatched()) {
			return true;
		}

		// Not batched (and wasn't batched previously) - nothing to reschedule
		if (!bean.getDestinationType().isBatched()) {
			return false;
		}
		// If Disabled flag switched
		if (originalBean.isDisabled() != bean.isDisabled()) {
			return true;
		}

		// Otherwise - see if the schedule changed
		return (!MathUtils.isEqual(OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(originalBean), OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(bean)));
	}
}
