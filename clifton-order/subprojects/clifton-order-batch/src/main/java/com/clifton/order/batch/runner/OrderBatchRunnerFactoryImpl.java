package com.clifton.order.batch.runner;


import com.clifton.core.context.ApplicationContextService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
@Getter
@Setter
public class OrderBatchRunnerFactoryImpl implements OrderBatchRunnerFactory {

	private ApplicationContextService applicationContextService;


	@Override
	public OrderBatchRunner createOrderBatchRunner(Date runDate, short orderDestinationId, boolean send) {
		OrderBatchRunner runner = new OrderBatchRunner(runDate, orderDestinationId, send);
		return createOrderBatchRunner(runner);
	}


	/**
	 * Auto wire the runner and return it
	 */
	private OrderBatchRunner createOrderBatchRunner(OrderBatchRunner runner) {
		getApplicationContextService().autowireBean(runner);
		return runner;
	}
}
