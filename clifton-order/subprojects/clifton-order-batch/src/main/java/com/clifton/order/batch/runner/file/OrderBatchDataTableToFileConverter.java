package com.clifton.order.batch.runner.file;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.order.batch.OrderBatch;


/**
 * The <code>OrderBatchDataTableToFileConverter</code> class is used to take the result of the query used to generate the file for the batch
 * and apply any modifications to it (i.e. remove header row), name the file, etc.
 *
 * @author manderson
 */
public interface OrderBatchDataTableToFileConverter {


	public FileWrapper convertOrderBatchDataTableResult(OrderBatch orderBatch, DataTable dataTable);
}
