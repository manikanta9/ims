package com.clifton.order.batch;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.cache.OrderBatchStatusIdsCache;
import com.clifton.order.batch.search.OrderBatchItemSearchForm;
import com.clifton.order.batch.search.OrderBatchSearchForm;
import com.clifton.order.batch.setup.OrderBatchSetupService;
import com.clifton.order.batch.setup.OrderBatchStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author AbhinayaM
 */
@Service
@Getter
@Setter
public class OrderBatchServiceImpl implements OrderBatchService {

	private AdvancedUpdatableDAO<OrderBatch, Criteria> orderBatchDAO;
	private AdvancedUpdatableDAO<OrderBatchItem, Criteria> orderBatchItemDAO;

	private DaoSingleKeyListCache<OrderBatchItem, Long> orderBatchItemListByBatchCache;
	private DaoCompositeKeyListCache<OrderBatch, Short, Boolean> orderBatchListByDestinationAndOpenCache;

	private EventHandler eventHandler;

	private OrderBatchSetupService orderBatchSetupService;

	private OrderBatchStatusIdsCache orderBatchStatusIdsCache;

	////////////////////////////////////////////////////////////////////////////
	////////              Order Batch Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderBatch getOrderBatch(long id) {
		return getOrderBatchDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderBatch> getOrderBatchOpenListForDestination(short orderDestinationId) {
		return getOrderBatchListByDestinationAndOpenCache().getBeanListForKeyValues(getOrderBatchDAO(), orderDestinationId, true);
	}


	@Override
	public List<OrderBatch> getOrderBatchList(OrderBatchSearchForm searchForm) {
		return getOrderBatchDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getOrderBatchStatusIds() != null) {
					criteria.add(

							Restrictions.in("orderBatchStatus.id", (Object[]) getOrderBatchStatusIdsCache().getOrderBatchStatusIds(searchForm.getOrderBatchStatusIds()))

					);
				}
			}
		});
	}


	@Override
	public OrderBatch saveOrderBatch(OrderBatch bean) {
		ValidationUtils.assertTrue(bean.isNewBean() || bean.getOrderBatchStatus().isOpen(), "Order Batches can only be modifid if in an open status.");
		ValidationUtils.assertNotEmpty(bean.getItemList(), "Item list is required for open Order Batches.");

		List<OrderBatchItem> originalList = (bean.isNewBean() ? null : getOrderBatchItemListForBatch(bean.getId()));
		List<OrderBatchItem> itemList = bean.getItemList();
		if (bean.isNewBean()) {
			bean.setOrderBatchStatus(getOrderBatchSetupService().getOrderBatchStatusByName(OrderBatchStatus.BATCHED_STATUS_NAME));
		}

		OrderBatch savedBean = getOrderBatchDAO().save(bean);
		itemList.forEach(orderBatchItem -> orderBatchItem.setOrderBatch(savedBean));
		getOrderBatchItemDAO().saveList(itemList, originalList);
		return savedBean;
	}


	@Override
	public void updateOrderBatchReadyToSend(long orderBatchId, boolean forceResend) {
		OrderBatch orderBatch = getOrderBatch(orderBatchId);
		ValidationUtils.assertTrue(forceResend || orderBatch.getOrderBatchStatus().isOpen(), "Order Batch " + orderBatch.getId() + " is not in a status available for sending (" + orderBatch.getOrderBatchStatus().getName() + ").  Please confirm the status of the batch or use the force resend option.");
		orderBatch.setOrderBatchStatus(getOrderBatchSetupService().getOrderBatchStatusByName(OrderBatchStatus.READY_TO_SEND_STATUS_NAME));
		getOrderBatchDAO().save(orderBatch);
	}


	@Override
	public void updateOrderBatchError(long orderBatchId, String errorMessage) {
		OrderBatch orderBatch = getOrderBatch(orderBatchId);
		orderBatch.setLastErrorMessage(errorMessage);
		updateOrderBatchStatus(orderBatch, OrderBatchStatus.ERROR_STATUS_NAME);
	}


	/**
	 * Updates the status only of the order batch.
	 * Note: Internal method call only and assumes the transition to the new status, if necessary, is validated by the calling method
	 */
	@Override
	public OrderBatch updateOrderBatchStatus(OrderBatch orderBatch, String batchStatusName) {
		OrderBatchStatus newStatus = getOrderBatchSetupService().getOrderBatchStatusByName(batchStatusName);
		ValidationUtils.assertNotNull(newStatus, "Unable to find Order Batch Status with name " + batchStatusName);
		orderBatch.setOrderBatchStatus(newStatus);
		return getOrderBatchDAO().save(orderBatch);
	}

	////////////////////////////////////////////////////////////////////////////
	////////             Order Batch Item Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderBatchItem getOrderBatchItem(long id) {
		return getOrderBatchItemDAO().findByPrimaryKey(id);
	}


	@Override
	public List<OrderBatchItem> getOrderBatchItemListForBatch(long orderBatchId) {
		return getOrderBatchItemListByBatchCache().getBeanListForKeyValue(getOrderBatchItemDAO(), orderBatchId);
	}


	@Override
	public List<OrderBatchItem> getOrderBatchItemList(OrderBatchItemSearchForm searchForm) {
		return getOrderBatchItemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public void deleteOrderBatchItem(long id) {
		getOrderBatchItemDAO().delete(id);
	}
}
