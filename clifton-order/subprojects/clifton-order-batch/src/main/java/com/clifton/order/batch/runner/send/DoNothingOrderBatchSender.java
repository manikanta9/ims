package com.clifton.order.batch.runner.send;

import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.setup.destination.OrderDestination;
import lombok.Getter;
import lombok.Setter;


/**
 * The <code>DoNothingOrderBatchSender</code> does nothing and just marks the batch as sent.
 * Can be useful in lower testing environments so we don't actually send files but still work through the workflow of batch sending.
 * <p>
 * Optionally allows always forcing an error as well for testing
 *
 * @author manderson
 */
@Getter
@Setter
public class DoNothingOrderBatchSender implements OrderBatchSender {

	private boolean forceError;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateOrderDestination(OrderDestination orderDestination) {
		// nothing to validate
	}


	@Override
	public void sendOrderBatch(OrderBatch orderBatch, FileWrapper fileWrapper) {
		ValidationUtils.assertFalse(isForceError(), "Forcing error on Do Nothing Order Batching Sending.");
	}
}
