package com.clifton.order.batch.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatchItem;
import com.clifton.order.batch.event.OrderBatchStatusUpdateEvent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderBatchItemObserver</code> is used to
 * 1. Validate that items are only added/removed IF the batch is in open status
 * 2. On Inserts, we raise an event to update the status of the source entity (i.e. Placement status is updated to Batched)
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderBatchItemObserver extends SelfRegisteringDaoObserver<OrderBatchItem> {

	private EventHandler eventHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderBatchItem> dao, DaoEventTypes event, OrderBatchItem bean) {
		ValidationUtils.assertTrue(bean.getOrderBatch().getOrderBatchStatus().isOpen(), "Batch Items can only be modified on open batches.  Batch is currently in " + bean.getOrderBatch().getOrderBatchStatus().getName() + " status.");
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<OrderBatchItem> dao, DaoEventTypes event, OrderBatchItem bean, Throwable e) {
		if (e == null && event.isInsert()) {
			getEventHandler().raiseEvent(OrderBatchStatusUpdateEvent.ofOrderBatchItem(bean));
		}
	}
}
