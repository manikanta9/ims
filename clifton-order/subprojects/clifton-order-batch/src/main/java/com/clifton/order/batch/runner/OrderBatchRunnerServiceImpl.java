package com.clifton.order.batch.runner;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.batch.OrderBatchService;
import com.clifton.order.batch.runner.file.OrderBatchDataTableToFileConverter;
import com.clifton.order.batch.runner.send.OrderBatchSender;
import com.clifton.order.batch.setup.OrderBatchSetupService;
import com.clifton.order.batch.setup.OrderBatchStatus;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@Service
@Getter
@Setter
public class OrderBatchRunnerServiceImpl implements OrderBatchRunnerService {

	private static final String SEND_RUNNER_TYPE = "ORDER-BATCH-SEND";


	private OrderBatchRunnerFactory orderBatchRunnerFactory;

	private OrderBatchService orderBatchService;
	private OrderBatchSetupService orderBatchSetupService;

	private RunnerHandler runnerHandler;

	private SystemBeanService systemBeanService;
	private SystemQueryService systemQueryService;
	private SystemQueryExecutionService systemQueryExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processOrderBatchingForDestination(short orderDestinationId, boolean send) {
		OrderBatchRunner orderBatchRunner = getOrderBatchRunnerFactory().createOrderBatchRunner(new Date(), orderDestinationId, send);
		getRunnerHandler().runNow(orderBatchRunner);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Order Batch Files                           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable getOrderBatchSystemQueryResult(long orderBatchId) {
		OrderBatch orderBatch = getOrderBatchService().getOrderBatch(orderBatchId);
		ValidationUtils.assertNotNull(orderBatch, "Cannot find order batch with id " + orderBatchId);

		Integer queryId = OrderDestinationUtils.getOrderDestinationFileSystemQueryId(orderBatch.getOrderDestination());
		ValidationUtils.assertNotNull(queryId, "There is no query associated with Order Destination " + orderBatch.getOrderDestination().getName());

		SystemQuery query = getSystemQueryService().getSystemQuery(queryId);
		ValidationUtils.assertNotNull(query, "Cannot find query with id " + queryId);

		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
		ValidationUtils.assertTrue(CollectionUtils.getSize(parameterList) == 1, "System Query " + query.getName() + " should have ONE parameter for OrderBatchID");
		ValidationUtils.assertTrue("OrderBatchID".equalsIgnoreCase(parameterList.get(0).getName()), "System Query " + query.getName() + " should have ONE parameter for OrderBatchID");

		SystemQueryParameterValue pv = new SystemQueryParameterValue();
		pv.setParameter(parameterList.get(0));
		pv.setValue(orderBatchId + "");

		query.setParameterValueList(CollectionUtils.createList(pv));
		return getSystemQueryExecutionService().getSystemQueryResult(query);
	}


	/**
	 * Executes the query for the OrderBatch, but also converts it into the same file and format that would be sent
	 */
	@Override
	public FileWrapper downloadOrderBatchFile(long orderBatchId) {
		OrderBatch orderBatch = getOrderBatchService().getOrderBatch(orderBatchId);
		ValidationUtils.assertNotNull(orderBatch, "Cannot find order batch with id " + orderBatchId);

		Integer converterBeanId = OrderDestinationUtils.getOrderDestinationDataTableToFileConverterBeanId(orderBatch.getOrderDestination());
		ValidationUtils.assertNotNull(converterBeanId, "There is no data table to file converter bean associated with Order Destination " + orderBatch.getOrderDestination().getName());

		DataTable dataTable = getOrderBatchSystemQueryResult(orderBatchId);
		OrderBatchDataTableToFileConverter converter = (OrderBatchDataTableToFileConverter) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(converterBeanId));
		return converter.convertOrderBatchDataTableResult(orderBatch, dataTable);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Order Batch Sending                         ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Send the OrderBatch.  Confirms current status of the batch and if valid
	 * will move it to Ready to Send status which raises an event to being the sending
	 * <p>
	 * forceResend can be used to force the triggering of Ready to Send event in case something happened while it was in Ready To Send state.
	 * forceResend option can also be used if the status is in completed state, but it needs to be re-sent
	 */
	@Override
	public void sendOrderBatch(long orderBatchId) {
		// asynchronous run support
		final String runId = orderBatchId + "";
		final Date now = new Date();
		Runner runner = new AbstractStatusAwareRunner(SEND_RUNNER_TYPE, runId, now) {

			@Override
			public void run() {
				try {
					OrderBatch orderBatch = getOrderBatchService().getOrderBatch(orderBatchId);
					OrderBatchStatus currentStatus = orderBatch.getOrderBatchStatus();
					if (StringUtils.isEqual(OrderBatchStatus.SENDING_STATUS_NAME, currentStatus.getName())) {
						getStatus().setMessage("Skipping send of Batch # " + orderBatchId + ". Sending is already in progress.");
						return;
					}

					// Confirm in Ready to Send status
					ValidationUtils.assertTrue(StringUtils.isEqual(OrderBatchStatus.READY_TO_SEND_STATUS_NAME, currentStatus.getName()), "Can only send batches in Ready to Send status. Batch # " + orderBatch.getId() + " is currently in " + currentStatus.getName() + " status.");
					orderBatch = getOrderBatchService().updateOrderBatchStatus(orderBatch, OrderBatchStatus.SENDING_STATUS_NAME);

					getStatus().setMessage("Sending Order Batch # " + orderBatchId);

					FileWrapper fileWrapper = downloadOrderBatchFile(orderBatchId);

					Integer senderBeanId = OrderDestinationUtils.getOrderDestinationFileBatchSendingBeanId(orderBatch.getOrderDestination());
					ValidationUtils.assertNotNull(senderBeanId, "Order Batch # " + orderBatch.getId() + " for destination " + orderBatch.getOrderDestination().getName() + " is missing a sender bean.");
					OrderBatchSender sender = (OrderBatchSender) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(senderBeanId));
					sender.sendOrderBatch(orderBatch, fileWrapper);

					// MARK AS SENT
					getOrderBatchService().updateOrderBatchStatus(orderBatch, OrderBatchStatus.SENT_STATUS_NAME);

					getStatus().setMessage("Completed sending Order Batch # " + orderBatchId);
				}
				catch (Throwable e) {
					String errorMessage = "Error sending Order Batch # " + orderBatchId + ": " + ExceptionUtils.getDetailedMessage(e);
					getOrderBatchService().updateOrderBatchError(orderBatchId, errorMessage);
					getStatus().setMessage(errorMessage);
					LogUtils.errorOrInfo(getClass(), errorMessage, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
	}


	/**
	 * Fixes Order Batch in Sending status that are not actually Sending (server restart)
	 */
	@Override
	public void fixOrderBatch(long orderBatchId) {
		// validate current order batch status to make sure we're not fixing a good batch
		OrderBatch orderBatch = getOrderBatchService().getOrderBatch(orderBatchId);
		ValidationUtils.assertNotNull(orderBatch, "Cannot find order batch with id = " + orderBatchId);
		ValidationUtils.assertTrue(StringUtils.isEqual(OrderBatchStatus.SENDING_STATUS_NAME, orderBatch.getOrderBatchStatus().getName()), "Cannot fix order batch that is not in SENDING status: " + orderBatch.getOrderBatchStatus().getName());
		ValidationUtils.assertFalse(isBatchSending(orderBatch), "Cannot fix the order batch with id = " + orderBatchId + " because it maybe currently sending.");

		getOrderBatchService().updateOrderBatchError(orderBatchId, "Manually fixing invalid batch stuck in Sending status");
	}


	/**
	 * Fix Batch Status that  was in Error State and sent outside System manually and process is Complete
	 */
	@Override
	public void fixOrderBatchManuallySent(long orderBatchId) {
		// validate current order batch status
		OrderBatch orderBatch = getOrderBatchService().getOrderBatch(orderBatchId);
		ValidationUtils.assertNotNull(orderBatch, "Cannot find order batch with id = " + orderBatchId);
		ValidationUtils.assertTrue(StringUtils.isEqual(OrderBatchStatus.ERROR_STATUS_NAME, orderBatch.getOrderBatchStatus().getName()), "Batch should be in Error state to mark it Manually(Sent): " + orderBatch.getOrderBatchStatus().getName());

		getOrderBatchService().updateOrderBatchStatus(orderBatch, OrderBatchStatus.MANUAL_SENT_STATUS_NAME);
	}


	private boolean isBatchSending(OrderBatch orderBatch) {
		return getRunnerHandler().isRunnerWithTypeAndIdActive(SEND_RUNNER_TYPE, Long.toString(orderBatch.getId()));
	}
}
