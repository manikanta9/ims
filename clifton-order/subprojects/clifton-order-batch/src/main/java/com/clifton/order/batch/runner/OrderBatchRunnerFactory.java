package com.clifton.order.batch.runner;


import java.util.Date;


/**
 * The <code>OrderBatchRunnerFactory</code> interface defines methods for creating OrderBatchRunner instances.
 *
 * @author Mary Anderson
 */
public interface OrderBatchRunnerFactory {


	public OrderBatchRunner createOrderBatchRunner(Date runDate, short orderDestinationId, boolean send);
}
