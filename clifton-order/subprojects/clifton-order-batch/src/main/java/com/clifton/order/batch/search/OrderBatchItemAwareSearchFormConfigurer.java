package com.clifton.order.batch.search;

import com.clifton.core.dataaccess.search.SearchFormCustomConfigurer;
import com.clifton.order.batch.OrderBatchItem;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author manderson
 */
@Component
public class OrderBatchItemAwareSearchFormConfigurer implements SearchFormCustomConfigurer<OrderBatchItemAwareSearchForm> {


	@Override
	public Class<OrderBatchItemAwareSearchForm> getSearchFormInterfaceClassName() {
		return OrderBatchItemAwareSearchForm.class;
	}


	@Override
	public void configureCriteria(Criteria criteria, OrderBatchItemAwareSearchForm searchForm, Map<String, Criteria> processedPathToCriteria) {
		if (searchForm.getOrderBatchId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(OrderBatchItem.class, "bi");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eq("orderBatch.id", searchForm.getOrderBatchId()));
			sub.add(Restrictions.eqProperty("fkFieldId", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getOrderBatchForDestinationIdMissing() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(OrderBatchItem.class, "bi");
			sub.setProjection(Projections.property("id"));
			sub.createCriteria("orderBatch", "b");
			sub.add(Restrictions.eq("b.orderDestination.id", searchForm.getOrderBatchForDestinationIdMissing()));
			sub.add(Restrictions.eqProperty("fkFieldId", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.notExists(sub));
		}
	}
}
