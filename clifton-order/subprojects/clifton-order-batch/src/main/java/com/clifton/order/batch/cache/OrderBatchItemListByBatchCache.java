package com.clifton.order.batch.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.order.batch.OrderBatchItem;
import org.springframework.stereotype.Component;


/**
 * Cache the list of Items for a batch
 *
 * @author manderson
 */
@Component
public class OrderBatchItemListByBatchCache extends SelfRegisteringSingleKeyDaoListCache<OrderBatchItem, Long> {


	@Override
	protected String getBeanKeyProperty() {
		return "orderBatch.id";
	}


	@Override
	protected Long getBeanKeyValue(OrderBatchItem bean) {
		if (bean != null && bean.getOrderBatch() != null) {
			return bean.getOrderBatch().getId();
		}
		return null;
	}
}
