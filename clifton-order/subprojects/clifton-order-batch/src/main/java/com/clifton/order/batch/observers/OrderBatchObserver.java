package com.clifton.order.batch.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.batch.event.OrderBatchStatusUpdateEvent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderBatchObserver</code> is used to validate changes to an Order Batch
 * It's also used on status changes to update the status of the source entities for it's items (i.e. placements)
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderBatchObserver extends SelfRegisteringDaoObserver<OrderBatch> {

	private EventHandler eventHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<OrderBatch> dao, DaoEventTypes event, OrderBatch bean) {
		if (event.isDelete()) {
			ValidationUtils.fail("Order batches cannot be deleted, only canceled.");
		}
		else if (event.isInsert()) {
			ValidationUtils.assertTrue(bean.getOrderBatchStatus().isOpen(), "New order batches must be in an open status.");
			ValidationUtils.assertTrue(bean.getOrderDestination().getDestinationType().isBatched(), "Order batches can only be created for batched order destination types.  Order Destination [" + bean.getOrderDestination().getName() + "] does not support batching.");
		}
		else {
			OrderBatch originalBean = getOriginalBean(dao, bean);
			ValidationUtils.assertTrue(bean.getOrderDestination().equals(originalBean.getOrderDestination()), "You cannot change the order destination of an existing order batch.");
			// Note: Batch Status change is validated in the service method to prevent attempt to update from Sending to Sending (i.e. can't send if already in the process of sending)

		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<OrderBatch> dao, DaoEventTypes event, OrderBatch bean, Throwable e) {
		if (e == null && event.isUpdate()) {
			if (!CompareUtils.isEqual(getOriginalBean(dao, bean).getOrderBatchStatus(), bean.getOrderBatchStatus())) {
				getEventHandler().raiseEvent(OrderBatchStatusUpdateEvent.ofOrderBatch(bean));
			}
		}
	}
}
