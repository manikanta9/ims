package com.clifton.order.batch.runner.processor;

import com.clifton.order.batch.OrderBatch;
import com.clifton.order.setup.destination.OrderDestination;

import java.util.List;


/**
 * The <code>OrderBatchingProcessor</code> interface is defines the method to create a batch with items based on any specific logic.
 * <p>
 * For example, for File Execution Venue Order Destinations, this is usually batching together all placements in a Ready for Batching execution status.
 *
 * @author manderson
 */
public interface OrderBatchingProcessor {


	/**
	 * When saving the order destination, used to validate the selected processor is configured correctly for the order destination.
	 * For example, cannot use the placement batching processor on trades.
	 */
	public void validateOrderDestination(OrderDestination orderDestination);


	/**
	 * Returns the list (often only 1, but potentially could have batching put things into separate groups) of batched entities and items.
	 */
	public List<OrderBatch> processOrderBatching(OrderDestination orderDestination);
}
