package com.clifton.order.batch.runner.processor;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.batch.OrderBatchItem;
import com.clifton.order.batch.OrderBatchService;
import com.clifton.order.setup.destination.OrderDestination;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Getter
@Setter
public abstract class BaseOrderBatchingProcessor<T extends IdentityObject> implements OrderBatchingProcessor {

	private OrderBatchService orderBatchService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If there is an existing batch that is in Batched status, let's add items to it.  Otherwise create a new batch
	 */
	protected OrderBatch getOrCreateOrderBatch(OrderDestination orderDestination) {
		OrderBatch batch = CollectionUtils.getFirstElement(getOrderBatchService().getOrderBatchOpenListForDestination(orderDestination.getId()));
		if (batch == null) {
			batch = new OrderBatch();
			batch.setOrderDestination(orderDestination);
			batch.setItemList(new ArrayList<>());
		}
		else {
			batch.setItemList(CollectionUtils.asNonNullList(getOrderBatchService().getOrderBatchItemListForBatch(batch.getId())));
		}
		return batch;
	}


	protected void populateOrderBatchWithItems(OrderBatch orderBatch, List<T> sourceItems) {
		sourceItems.forEach(sourceItem -> {
			OrderBatchItem item = new OrderBatchItem();
			item.setOrderBatch(orderBatch);
			item.setFkFieldId(BeanUtils.getIdentityAsLong(sourceItem));
			orderBatch.getItemList().add(item);
		});
	}
}
