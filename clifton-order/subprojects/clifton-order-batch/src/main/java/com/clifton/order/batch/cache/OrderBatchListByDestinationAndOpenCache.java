package com.clifton.order.batch.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.order.batch.OrderBatch;
import org.springframework.stereotype.Component;


/**
 * Cache the list of open batches for a destination
 *
 * @author manderson
 */
@Component
public class OrderBatchListByDestinationAndOpenCache extends SelfRegisteringCompositeKeyDaoListCache<OrderBatch, Short, Boolean> {


	@Override
	protected String getBeanKey1Property() {
		return "orderDestination.id";
	}


	@Override
	protected Short getBeanKey1Value(OrderBatch bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getOrderDestination());
		}
		return null;
	}


	@Override
	protected String getBeanKey2Property() {
		return "orderBatchStatus.open";
	}


	@Override
	protected Boolean getBeanKey2Value(OrderBatch bean) {
		if (bean != null && bean.getOrderBatchStatus() != null) {
			return bean.getOrderBatchStatus().isOpen();
		}
		return false;
	}
}
