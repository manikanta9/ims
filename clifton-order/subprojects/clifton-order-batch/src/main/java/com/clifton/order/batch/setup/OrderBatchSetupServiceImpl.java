package com.clifton.order.batch.setup;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.order.batch.setup.search.OrderBatchStatusSearchForm;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author AbhinayaM
 */
@Service
@Getter
@Setter
public class OrderBatchSetupServiceImpl implements OrderBatchSetupService {

	private AdvancedUpdatableDAO<OrderBatchStatus, Criteria> orderBatchStatusDAO;

	private DaoNamedEntityCache<OrderBatchStatus> orderBatchStatusCache;

	////////////////////////////////////////////////////////////////////////////
	////////            Order Batch Status Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderBatchStatus getOrderBatchStatus(short id) {
		return getOrderBatchStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public OrderBatchStatus getOrderBatchStatusByName(String name) {
		return getOrderBatchStatusCache().getBeanForKeyValue(getOrderBatchStatusDAO(), name);
	}


	@Override
	public List<OrderBatchStatus> getOrderBatchStatusList(OrderBatchStatusSearchForm searchForm) {

		return getOrderBatchStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public OrderBatchStatus saveOrderBatchStatus(OrderBatchStatus bean) {
		return getOrderBatchStatusDAO().save(bean);
	}


	@Override
	public void deleteOrderBatchStatus(short id) {
		getOrderBatchStatusDAO().delete(id);
	}
}
