package com.clifton.order.batch.runner.send;

import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.setup.destination.OrderDestination;


/**
 * @author manderson
 */
public interface OrderBatchSender {

	/**
	 * When saving the order destination, used to validate the selected sender is configured correctly for the order destination.
	 * For example, the file system query table must match the destination type batching source table
	 */
	public void validateOrderDestination(OrderDestination orderDestination);


	/**
	 * Sends the items in the OrderBatch (i.e. via email a file, drop a file to a network share, etc.)
	 */
	public void sendOrderBatch(OrderBatch orderBatch, FileWrapper fileWrapper);
}
