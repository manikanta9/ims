package com.clifton.order.batch.event;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.order.batch.runner.OrderBatchRunnerService;
import com.clifton.order.batch.setup.OrderBatchStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;


/**
 * The <code>OrderBatchReadyToSendEventListener</code> listens for events raised when OrderBatch is moved to Ready to Send status and schedules the sending of the batch
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderBatchReadyToSendEventListener extends BaseEventListener<OrderBatchStatusUpdateEvent> {


	private OrderBatchRunnerService orderBatchRunnerService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return OrderBatchStatusUpdateEvent.EVENT_NAME;
	}


	@Override
	public void onEvent(OrderBatchStatusUpdateEvent event) {
		if (StringUtils.isEqual(OrderBatchStatus.READY_TO_SEND_STATUS_NAME, event.getNewBatchStatusName())) {
			getOrderBatchRunnerService().sendOrderBatch(event.getTarget());
		}
	}
}
