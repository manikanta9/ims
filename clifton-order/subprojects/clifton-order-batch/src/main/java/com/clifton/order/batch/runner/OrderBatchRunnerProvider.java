package com.clifton.order.batch.runner;


import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerProvider;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationService;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * The <code>OrderBatchRunnerProvider</code> class returns OrderBatch Runner instances that are scheduled for the specified time period.
 *
 * @author Mary Anderson
 */
@Component
@Getter
@Setter
public class OrderBatchRunnerProvider implements RunnerProvider<OrderDestination> {

	private OrderDestinationService orderDestinationService;
	private OrderBatchRunnerFactory orderBatchRunnerFactory;

	private ScheduleApiService scheduleApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		long startMillis = System.currentTimeMillis();
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Starting getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE)));
		List<OrderDestination> orderDestinationListBatched = getOrderDestinationService().getOrderDestinationListForBatching();
		List<Runner> results = new ArrayList<>();
		for (OrderDestination orderDestination : orderDestinationListBatched) {
			try {
				ValidationUtils.assertTrue(orderDestination.getDestinationType().isBatched(), "Cannot schedule batching for Order Destination " + orderDestination.getName() + " does not support batching");
				Integer processorBeanId = OrderDestinationUtils.getOrderDestinationFileBatchProcessorBeanId(orderDestination);
				ValidationUtils.assertNotNull(processorBeanId, "Cannot schedule batching for Order Destination " + orderDestination.getName() + ".  Missing a processor bean.");
				Integer scheduleId = OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(orderDestination);
				ValidationUtils.assertNotNull(scheduleId, "Cannot schedule batching for Order Destination " + orderDestination.getName() + ".  Missing a schedule.");
				List<Runner> batchRunners = getOccurrencesBetweenForEntity(orderDestination, startDateTime, endDateTime);
				if (!CollectionUtils.isEmpty(batchRunners)) {
					results.addAll(batchRunners);
				}
			}
			catch (Throwable e) {
				LogUtils.warn(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Unable to schedule batching for order destination " + orderDestination.getName()));
			}
		}
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Ending getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
		return results;
	}


	@Override
	public Runner createRunnerForEntityAndDate(OrderDestination destination, Date runnerDate) {
		return getOrderBatchRunnerFactory().createOrderBatchRunner(runnerDate, destination.getId(), true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Runner> getOccurrencesBetweenForEntity(OrderDestination orderDestination, Date startDateTime, Date endDateTime) {
		long startMillis = System.currentTimeMillis();
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Starting getOccurrencesBetweenForEntity at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE)));
		List<Runner> results = new ArrayList<>();
		Integer scheduleId = OrderDestinationUtils.getOrderDestinationFileBatchScheduleId(orderDestination);
		if (scheduleId != null) {
			try {
				// Convert to CalendarSchedule for usage
				Schedule schedule = getScheduleApiService().getSchedule(scheduleId);
				List<Date> occurrences = getScheduleOccurrences(orderDestination, schedule, startDateTime, endDateTime);
				LogUtils.trace(LogCommand.ofMessage(getClass(), "Date occurrence list compiled at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
				for (Date date : occurrences) {
					Runner runner = createRunnerForEntityAndDate(orderDestination, date);
					results.add(runner);
				}
				LogUtils.trace(LogCommand.ofMessage(getClass(), "Finished creating runners for date occurrences at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
			}
			catch (Throwable e) {
				LogUtils.error(getClass(), "Failed to get schedule occurrences of " + orderDestination.getClass() + " for [" + orderDestination.getName() + "] between [" + DateUtils.fromDate(startDateTime) + "] and [" + DateUtils.fromDate(endDateTime) + "].", e);
			}
			finally {
				LogUtils.trace(LogCommand.ofMessage(getClass(), "Completed getOccurrencesBetweenForEntity at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
			}
		}
		return results;
	}


	// handle any exceptions without rolling back the inherited transaction
	@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
	protected List<Date> getScheduleOccurrences(OrderDestination orderDestination, Schedule schedule, Date startDateTime, Date endDateTime) {
		try {
			return getScheduleApiService().getScheduleOccurrences(ScheduleOccurrenceCommand.forOccurrencesBetween(schedule, startDateTime, endDateTime));
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to get schedule occurrences of " + orderDestination.getClass() + " for [" + orderDestination.getName() + "] between [" + DateUtils.fromDate(startDateTime) + "] and [" + DateUtils.fromDate(endDateTime) + "].", e);
		}
		return Collections.emptyList();
	}
}
