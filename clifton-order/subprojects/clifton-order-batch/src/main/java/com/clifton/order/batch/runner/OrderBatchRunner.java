package com.clifton.order.batch.runner;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.batch.OrderBatchService;
import com.clifton.order.batch.runner.processor.OrderBatchingProcessor;
import com.clifton.order.batch.setup.OrderBatchSetupService;
import com.clifton.order.setup.destination.OrderDestination;
import com.clifton.order.setup.destination.OrderDestinationService;
import com.clifton.order.setup.destination.OrderDestinationUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBeanService;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;


/**
 * The <code>OrderBatchRunner</code> class is run based on it configured schedule see {@link OrderBatchRunnerProvider}.
 * <p>
 * When the run method is called, a {@link com.clifton.order.setup.destination.OrderDestination} bean is obtained and the batch processor for that destination bean is executed.
 *
 * @author Mary Anderson
 */
@Getter
@Setter
public class OrderBatchRunner extends AbstractStatusAwareRunner {

	private final short orderDestinationId;

	private final boolean send;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ContextHandler contextHandler;

	private OrderBatchService orderBatchService;

	private OrderBatchSetupService orderBatchSetupService;

	private OrderDestinationService orderDestinationService;

	private SecurityUserService securityUserService;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public OrderBatchRunner(Date runDate, short orderDestinationId, boolean send) {
		super("ORDER-BATCH", Short.toString(orderDestinationId), runDate);
		this.orderDestinationId = orderDestinationId;
		this.send = send;
		getStatus().setMessage("Scheduled for " + DateUtils.fromDate(runDate));
	}


	@Override
	public void run() {
		SecurityUser runAsUser = getRunAsUser();
		if (runAsUser == null) {
			LogUtils.error(getClass(), "Cannot run order batch.  No current user is set, and the default user [" + SecurityUser.SYSTEM_USER + "] is missing.");
			return;
		}

		// save the current user
		Object currentUser = getContextHandler().getBean(Context.USER_BEAN_NAME);
		try {
			// set the Run As User as current user
			getContextHandler().setBean(Context.USER_BEAN_NAME, runAsUser);

			getStatus().setMessage("Started on " + DateUtils.fromDate(new Date()));

			OrderDestination orderDestination = getOrderDestinationService().getOrderDestination(this.orderDestinationId);
			ValidationUtils.assertTrue(orderDestination.getDestinationType().isBatched(), "Selected Order Destination " + orderDestination.getName() + " does not support batching");
			Integer processorBeanId = OrderDestinationUtils.getOrderDestinationFileBatchProcessorBeanId(orderDestination);
			ValidationUtils.assertNotNull(processorBeanId, "Selected Order Destination " + orderDestination.getName() + " is missing a processor bean.");
			OrderBatchingProcessor processor = (OrderBatchingProcessor) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(processorBeanId));
			processor.processOrderBatching(orderDestination);
			getStatus().setMessage("Finished on " + DateUtils.fromDate(new Date()));

			// If sending, find all open batches and send them, not just those that had updates
			if (this.send) {
				List<OrderBatch> sendBatchList = getOrderBatchService().getOrderBatchOpenListForDestination(this.orderDestinationId);
				if (!CollectionUtils.isEmpty(sendBatchList)) {
					sendBatchList.forEach(orderBatch -> getOrderBatchService().updateOrderBatchReadyToSend(orderBatch.getId(), false));
				}
			}
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error generating order batch(es) for destination [" + this.orderDestinationId + "]: " + e.getMessage(), e);
		}
		finally {
			// restore the current user
			if (currentUser == null) {
				getContextHandler().removeBean(Context.USER_BEAN_NAME);
			}
			else {
				getContextHandler().setBean(Context.USER_BEAN_NAME, currentUser);
			}
		}
	}


	/**
	 * Returns the run as user specified for the batch job or the default run as user if one is not set.
	 */
	private SecurityUser getRunAsUser() {
		SecurityUser result = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
		if (result == null) {
			result = getSecurityUserService().getSecurityUserByName(SecurityUser.SYSTEM_USER);
		}
		return result;
	}
}
