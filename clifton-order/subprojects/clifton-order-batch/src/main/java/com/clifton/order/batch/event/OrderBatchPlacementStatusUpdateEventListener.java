package com.clifton.order.batch.event;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.EventHandler;
import com.clifton.order.batch.OrderBatchItem;
import com.clifton.order.batch.OrderBatchService;
import com.clifton.order.execution.event.OrderPlacementExecutionUpdateEvent;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>OrderBatchPlacementStatusUpdateEventListener</code> listens to events raised when the OrderBatch status is updated OR an OrderBatchItem is inserted
 * This is used to update the status on the placement (unless the placement is already canceled).
 *
 * @author manderson
 */
@Component
@Getter
@Setter
public class OrderBatchPlacementStatusUpdateEventListener extends BaseEventListener<OrderBatchStatusUpdateEvent> {

	private EventHandler eventHandler;

	private OrderBatchService orderBatchService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public int getOrder() {
		return 50;
	}


	@Override
	public String getEventName() {
		return OrderBatchStatusUpdateEvent.EVENT_NAME;
	}


	@Override
	public void onEvent(OrderBatchStatusUpdateEvent event) {
		if (event.isExecutionVenue()) {
			// If only for one batch item
			if (event.getFkFieldId() != null) {
				OrderPlacementExecutionUpdateEvent placementEvent = new OrderPlacementExecutionUpdateEvent(event.getFkFieldId());
				placementEvent.setSkipUpdateIfCanceled(true);
				placementEvent.setNewExecutionStatusName(event.getNewBatchStatusName());
				getEventHandler().raiseEvent(placementEvent);
			}
			// Update the entire batch
			else {
				List<OrderBatchItem> batchItemList = getOrderBatchService().getOrderBatchItemListForBatch(event.getTarget());
				CollectionUtils.getStream(batchItemList).forEach(batchItem -> {
							OrderPlacementExecutionUpdateEvent placementEvent = new OrderPlacementExecutionUpdateEvent(batchItem.getFkFieldId());
							placementEvent.setSkipUpdateIfCanceled(true);
							placementEvent.setNewExecutionStatusName(event.getNewBatchStatusName());
							getEventHandler().raiseEvent(placementEvent);
						}
				);
			}
		}
	}
}
