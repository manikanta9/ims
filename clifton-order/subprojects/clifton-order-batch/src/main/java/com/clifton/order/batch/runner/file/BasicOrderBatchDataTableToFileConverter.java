package com.clifton.order.batch.runner.file;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.order.batch.OrderBatch;
import lombok.Getter;
import lombok.Setter;

import java.io.File;


/**
 * The <code>BasicOrderBatchDataTableToFileConverter</code> is used to do simple conversions of a DataTable to a file
 * Options include setting the file name, file formatting, etc.
 *
 * @author manderson
 */
@Getter
@Setter
public class BasicOrderBatchDataTableToFileConverter implements OrderBatchDataTableToFileConverter, ValidationAware {

	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Freemarker template to generate the file name
	 * accepts the orderBatch in the context
	 */
	private String fileNameTemplate;

	/**
	 * Optionally remove the header row
	 */
	private boolean suppressHeaderRow;

	/**
	 * File format of the file
	 */
	private FileFormats fileFormat;

	/**
	 * Optionally allow overriding the file extension
	 */
	private String overrideExtension;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertFalse(StringUtils.isEmpty(getFileNameTemplate()), "File Name Template is Required");
		ValidationUtils.assertNotNull(getFileFormat(), "File Format is required.");
		ValidationUtils.assertNotNull(getFileFormat().getDataTableConverter(), "File Format " + getFileFormat().name() + " does not have a file converter.");
	}


	@Override
	public FileWrapper convertOrderBatchDataTableResult(OrderBatch orderBatch, DataTable dataTable) {
		validate();
		ValidationUtils.assertFalse(dataTable.getTotalRowCount() == 0, "No data in the query result to add to the file.");
		DataTableFileConfig dataTableFileConfig = new DataTableFileConfig(dataTable);
		dataTableFileConfig.setSuppressHeaderRow(isSuppressHeaderRow());
		File file = getFileFormat().getDataTableConverter().convert(dataTableFileConfig);

		TemplateConfig fileNameConfig = new TemplateConfig(getFileNameTemplate());
		fileNameConfig.addBeanToContext("orderBatch", orderBatch);
		String fileName = getTemplateConverter().convert(fileNameConfig);
		fileName = fileName + "." + StringUtils.coalesce(getOverrideExtension(), FileUtils.getFileExtension(file.getName()));

		return new FileWrapper(file, fileName, false);
	}
}
