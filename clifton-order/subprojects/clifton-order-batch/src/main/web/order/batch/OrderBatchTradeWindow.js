TCG.use('Clifton.order.batch.BaseOrderBatchWindow');

Clifton.order.batch.OrderBatchTradeWindow = Ext.extend(Clifton.order.batch.BaseOrderBatchWindow, {


	batchItemGrid: {
		title: 'Trades in this Batch',
		region: 'center',
		flex: 0.75,
		xtype: 'order-trade-list-grid',
		instructions: 'This Order Batch includes the following trades.',
		columnOverrides: [
			{dataIndex: 'security.name', hidden: true, viewNames: []}
		],
		getTopToolbarFilters: function(toolbar) {
			return [];
		},
		getLoadParams: function(firstLoad) {
			const lp = {readUncommittedRequested: true};
			lp.orderBatchId = this.getWindow().getMainFormId();
			return lp;
		}
	}

});
