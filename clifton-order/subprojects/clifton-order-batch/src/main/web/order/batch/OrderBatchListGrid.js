Clifton.order.batch.OrderBatchListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderBatchListFind',
	xtype: 'gridpanel',
	instructions: 'A list of all order batches in the system. Order Batches are a batch of Order Placements or Trades that are "batched" and then sent to a destination.  These can be used for File Executions, or Post Trade Files sent to Accounting system or some other external party.',
	defaultDisplayFilter: 'ALL_TODAY',
	additionalPropertiesToRequest: 'orderBatchStatus.open',

	columns: [
		{header: 'Batch #', width: 20, dataIndex: 'id', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Destination', width: 65, dataIndex: 'orderDestination.name', filter: {type: 'combo', searchFieldName: 'orderDestinationId', url: 'orderDestinationListFind.json'}},
		{header: 'Execution Venue', width: 30, dataIndex: 'orderDestination.destinationType.executionVenue', type: 'boolean', filter: {searchFieldName: 'executionVenue'}},
		{
			header: 'Batch Status', width: 30, dataIndex: 'orderBatchStatus.name', filter: {type: 'combo', searchFieldName: 'orderBatchStatusId', url: 'orderBatchStatusListFind.json'},
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.json.orderBatchStatus.error)) {
					metaData.css = 'amountNegative';
					metaData.attr = TCG.renderQtip(r.json.lastErrorMessage);
				}
				else if (TCG.isTrue(r.json.orderBatchStatus.completed)) {
					metaData.css = 'amountPositive';
				}
				else if (TCG.isFalse(r.json.orderBatchStatus.open)) {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{header: 'Last Error Message', width: 200, dataIndex: 'lastErrorMessage', renderer: TCG.renderValueWithTooltip},
		{header: 'Error', width: 25, dataIndex: 'orderBatchStatus.error', type: 'boolean', filter: {searchFieldName: 'error'}, hidden: true},
		{header: 'Canceled', width: 25, dataIndex: 'orderBatchStatus.canceled', type: 'boolean', filter: {searchFieldName: 'canceled'}, hidden: true},
		{header: 'Completed', width: 25, dataIndex: 'orderBatchStatus.completed', type: 'boolean', filter: {searchFieldName: 'completed'}, hidden: true},
		{
			header: 'Created By', width: 30, dataIndex: 'createUserId', filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label'},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Created On', width: 40, dataIndex: 'createDate'}
	],

	addFirstToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add(new TCG.form.ComboBox({
			name: 'batchOrderDestinationId',
			url: 'orderDestinationListFind.json?batched=true&disabled=false',
			emptyText: '< Select Destination >',
			instructions: 'Select a destination to process batching for.  All eligible entities will be added to appropriate batches.',
			disableAddNewItem: true,
			width: 150,
			listWidth: 230
		}));
		toolBar.add(' ');
		toolBar.add({
			text: 'Process Batching',
			tooltip: 'Process Batching',
			iconCls: 'run',
			handler: function() {
				gridPanel.processBatching(false);
			}
		});
		toolBar.add(' ');
		toolBar.add({
			text: 'Process Batching & Send',
			tooltip: 'Process Batching & Immediately Send the File',
			iconCls: 'email',
			handler: function() {
				gridPanel.processBatching(true);
			}
		});
		toolBar.add('-');
	},

	processBatching: function(send) {
		const gridPanel = this;
		const toolBar = gridPanel.getTopToolbar();
		const resetTask = new Ext.util.DelayedTask(function() {
			TCG.getChildByName(toolBar, 'batchOrderDestinationId').reset();
		});
		const batchOrderDestinationId = TCG.getChildByName(toolBar, 'batchOrderDestinationId').getValue();
		if (batchOrderDestinationId === '') {
			TCG.getChildByName(toolBar, 'batchOrderDestinationId').markInvalid('Select an Order Destination');
			resetTask.delay(1000);
		}
		else {
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Processing',
				waitTarget: gridPanel,
				params: {
					orderDestinationId: batchOrderDestinationId,
					send: send
				},
				onLoad: function(record, conf) {
					// All based on destination, so just reload
					gridPanel.reload();
				}
			});
			const url = 'orderBatchingForDestinationProcess.json';
			loader.load(url);
		}
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = [];

		filters.push({
			fieldLabel: 'Display', xtype: 'combo', name: 'displayFilters', width: 175, minListWidth: 140, forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name', 'description'],
				data: [
					['ALL_ACTIVE', 'All Active Batches', 'Display all active batches.  Active batches are those that are not completed or canceled.'],
					['ALL_TODAY', 'All Today\'s Batches', 'Display all batches created today.'],
					['ALL', 'All Batches', 'Display all batches.  Will default create date to after 7 days ago to limit initial query.']
				]
			}),
			listeners: {
				select: function(field) {
					const gp = TCG.getParentByClass(field, Ext.Panel);
					const v = field.getValue();
					gp.orderBatchStatusIds = [];
					if (TCG.contains(v, 'TODAY')) {
						gp.setFilterValue('createDate', {'after': new Date()}, false, true);
					}
					else if (TCG.contains(v, 'ACTIVE')) {
						gp.clearFilter('createDate', true);
					}
					else {
						gp.setFilterValue('createDate', {'after': new Date().add(Date.DAY, -8)}, false, true);
					}

					if (TCG.contains(v, 'ACTIVE')) {
						gp.orderBatchStatusIds = 'ACTIVE_STATUS';
					}
					else {
						gp.orderBatchStatusIds = null;
					}
					gp.reload();
				}
			}
		});
		return filters;
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			const gridPanel = this;
			const displayFilters = TCG.getChildByName(gridPanel.getTopToolbar(), 'displayFilters');
			if (TCG.isBlank(displayFilters.getValue())) {
				displayFilters.setValue(this.defaultDisplayFilter);
				displayFilters.fireEvent('select', displayFilters);
			}
		}
		const lp = {readUncommittedRequested: true};
		lp.orderBatchStatusIds = this.orderBatchStatusIds;

		return lp;

	},
	editor: {
		detailPageClass: 'Clifton.order.batch.OrderBatchWindow',
		drillDownOnly: true
	},

	getGridRowContextMenuItems: function(grid, rowIndex, record) {
		const rowItems = [];

		if (TCG.isTrue(record.json.orderBatchStatus.open)) {
			rowItems.push({
				text: 'Send Batch File', iconCls: 'email', handler: function() {
					Clifton.order.batch.sendOrderBatch(record.json, false, grid);
				}
			});
		}
		else if (TCG.isTrue(record.json.orderBatchStatus.error)) {
			rowItems.push({
				text: 'Mark as Sent (Manual)', tooltip: 'Manually Fix  Batch Status to Sent that  is in Error State and was processed outside the system ', iconCls: 'send', handler: function() {
					Clifton.order.batch.markOrderBatchSent(record.json, grid);
				}
			});
		}

		else if (TCG.isEquals('Sending', record.json.orderBatchStatus.name)) {
			rowItems.push({
				text: 'Fix Batch Status (Stuck in Sending)', iconCls: 'tools', handler: function() {
					Clifton.order.batch.fixOrderBatch(record.json, grid);
				}
			});
		}
		else {
			rowItems.push({
				text: 'Force Re-Send Batch File', iconCls: 'email', handler: function() {
					Clifton.order.batch.sendOrderBatch(record.json, true, grid);
				}
			});
		}

		rowItems.push('-');
		rowItems.push({
			text: 'Preview Batch File', iconCls: 'disk', handler: function() {
				Clifton.order.batch.previewOrderBatchQueryResult(record.json);
			}
		});
		return rowItems;
	}
});
Ext.reg('order-batch-list-grid', Clifton.order.batch.OrderBatchListGrid);
