Clifton.order.batch.BaseOrderBatchWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Batch',
	titlePrefixSeparator: ': ',
	width: 1400,
	height: 700,
	iconCls: 'list',
	enableRefreshWindow: true,

	reload: function() {
		this.refreshWindow();
	},

	// Should be Overridden in Extended Class
	batchItemGrid: {},

	exportOrderBatchQueryResult: function(exportFormat) {
		const fp = this.getMainFormPanel();
		Clifton.order.batch.previewOrderBatchQueryResult(fp.getForm().formValues, exportFormat);
	},

	initComponent: function() {
		const mainItem = {
			layout: 'border',
			tbar: [
				{
					text: 'Preview Batch File (Original Format)',
					iconCls: 'disk',
					xtype: 'splitbutton',
					tooltip: 'Downloads the file as it would be sent to external party.  You can also download directly using the following options',
					handler: function() {
						const fp = TCG.getParentByClass(TCG.getParentByClass(this, Ext.Toolbar), Ext.Window).getMainFormPanel();
						fp.getWindow().exportOrderBatchQueryResult();
					},
					menu: {
						items: [
							{
								text: 'Excel (*.xlsx)',
								iconCls: 'excel_open_xml',
								handler: function() {
									const fp = TCG.getParentByClass(TCG.getParentByClass(this, Ext.Toolbar), Ext.Window).getMainFormPanel();
									fp.getWindow().exportOrderBatchQueryResult('xlsx');
								}
							},
							{
								text: 'Excel (97-2003)',
								iconCls: 'excel',
								handler: function() {
									const fp = TCG.getParentByClass(TCG.getParentByClass(this, Ext.Toolbar), Ext.Window).getMainFormPanel();
									fp.getWindow().exportOrderBatchQueryResult('xls');
								}
							}, '-', {
								text: 'PDF',
								iconCls: 'pdf',
								handler: function() {
									const fp = TCG.getParentByClass(TCG.getParentByClass(this, Ext.Toolbar), Ext.Window).getMainFormPanel();
									fp.getWindow().exportOrderBatchQueryResult('pdf');
								}
							}, '-', {
								text: 'CSV',
								iconCls: 'csv',
								handler: function() {
									const fp = TCG.getParentByClass(TCG.getParentByClass(this, Ext.Toolbar), Ext.Window).getMainFormPanel();
									fp.getWindow().exportOrderBatchQueryResult('csv');
								}
							}, {
								text: 'CSV Minimal',
								iconCls: 'csv',
								handler: function() {
									const fp = TCG.getParentByClass(TCG.getParentByClass(this, Ext.Toolbar), Ext.Window).getMainFormPanel();
									fp.getWindow().exportOrderBatchQueryResult('csv_min');
								}
							}, {
								text: 'CSV Pipe',
								iconCls: 'csv',
								handler: function() {
									const fp = TCG.getParentByClass(TCG.getParentByClass(this, Ext.Toolbar), Ext.Window).getMainFormPanel();
									fp.getWindow().exportOrderBatchQueryResult('csv_pipe');
								}
							}
						]
					}
				}
			]
		};
		mainItem.items = [];
		mainItem.items.push(this.formItem);
		mainItem.items.push({flex: 0.02});
		mainItem.items.push(this.batchItemGrid);
		this.items = [mainItem];

		if (this.enableRefreshWindow) {
			this.addTool({
				id: 'refresh',
				qtip: 'Refreshes the main form of this window.  If you have any unsaved changes they will be lost.',
				handler: this.refreshWindow.createDelegate(this, [])
			});
		}
		if (this.enableShowInfo) {
			this.addTool({
				id: 'help',
				handler: this.showInfo.createDelegate(this, [])
			});
			this.addTool({
				id: 'left',
				qtip: 'Close window trail: go back to first detail window',
				handler: this.doCloseWindowTrail.createDelegate(this, [true])
			});
		}
		TCG.Window.superclass.initComponent.apply(this, arguments);
	},

	formItem: {
		xtype: 'formpanel',
		url: 'orderBatch.json',
		readOnly: true,
		labelFieldName: 'id',
		loadDefaultDataAfterRender: true,
		region: 'north',
		height: 125,

		items: [
			{
				xtype: 'columnpanel',
				defaults: {xtype: 'textfield'},
				columns: [{
					rows: [
						{fieldLabel: 'Batch #', name: 'id'}
					],
					config: {columnWidth: 0.3}
				}, {
					rows: [
						{fieldLabel: 'Destination', name: 'orderDestination.name', xtype: 'linkfield', detailPageClass: 'Clifton.order.setup.destination.DestinationWindow', detailIdField: 'orderDestination.id'}
					],
					config: {columnWidth: 0.4}
				}, {
					rows: [
						{fieldLabel: 'Batch Status', name: 'orderBatchStatus.name', xtype: 'linkfield', detailPageClass: 'Clifton.order.batch.setup.BatchStatusWindow', detailIdField: 'orderBatchStatus.id'}
					],
					config: {columnWidth: 0.3}
				}]
			},
			{fieldLabel: 'Last Error Message', name: 'lastErrorMessage', xtype: 'textarea', height: 75}
		]
	}

});
