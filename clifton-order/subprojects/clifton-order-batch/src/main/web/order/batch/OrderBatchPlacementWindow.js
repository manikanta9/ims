TCG.use('Clifton.order.batch.BaseOrderBatchWindow');

Clifton.order.batch.OrderBatchPlacementWindow = Ext.extend(Clifton.order.batch.BaseOrderBatchWindow, {


	batchItemGrid: {
		title: 'Placements',
		region: 'center',
		flex: 0.75,
		xtype: 'order-placement-list-grid',
		instructions: 'This Order Batch includes the following placements.',
		columnOverrides: [
			{dataIndex: 'orderDestination.name', hidden: true, viewNames: undefined},
			{dataIndex: 'rejectReasons', hidden: true, viewNames: undefined}
		],
		getTopToolbarFilters: function(toolbar) {
			return [];
		},
		getLoadParams: function(firstLoad) {
			const lp = {readUncommittedRequested: true};
			lp.orderBatchId = this.getWindow().getMainFormId();
			return lp;
		}
	}

});
