// not the actual window but looks up the batch for the item and opens that window
Clifton.order.batch.OrderBatchItemWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderBatchItem.json',

	openEntityWindow: function(config, entity) {
		const className = 'Clifton.order.batch.OrderBatchWindow';
		if (entity) {
			config.params.id = entity.orderBatch.id;
			entity = TCG.data.getData('orderBatch.json?id=' + entity.orderBatch.id, this);
		}
		this.doOpenEntityWindow(config, entity, className);
	}
});
