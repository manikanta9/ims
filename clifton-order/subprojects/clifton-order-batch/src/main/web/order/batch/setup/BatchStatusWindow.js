Clifton.order.batch.setup.BatchStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Order Batch Status',
	iconCls: 'shopping-cart',
	width: 600,
	height: 400,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Shows the status of the batched Entities',
					url: 'orderBatchStatus.json',
					labelWidth: 150,
					items: [
						{fieldLabel: 'Batch Status Name', name: 'name'},
						{fieldLabel: 'Batch Status Description', name: 'description', xtype: 'textarea'},
						{boxLabel: 'Open', name: 'open', xtype: 'checkbox', qtip: 'Open statuses are those that have not been sent and batch items can be added or removed from them.'},
						{boxLabel: 'Error', name: 'error', xtype: 'checkbox', qtip: 'Known error during batch sending (some things like email bounce backs we may not be aware of, but if a known issue we can put into this state)'},
						{boxLabel: 'Completed', name: 'completed', xtype: 'checkbox', qtip: 'Batch has been sent and is considered complete.'},
						{boxLabel: 'Canceled', name: 'canceled', xtype: 'checkbox', qtip: 'Cancel the batch.'}
					]
				}]
			}
		]
	}]
});
