Clifton.order.batch.OrderBatchListWindow = Ext.extend(TCG.app.Window, {
	id: 'orderBatchListWindow',
	title: 'Order Batches',
	iconCls: 'list',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Order Batches',
				items: [{
					xtype: 'order-batch-list-grid'
				}]

			},


			{
				title: 'Order Batch Status',
				items: [{
					name: 'orderBatchStatusListFind',
					xtype: 'gridpanel',
					instructions: 'A list of  batch entities from various Order tables to send out.  Most commonly this will be some kind of "file" execution, or could be batching and posting Trades to an accounting system, or batching and sending files to some other party (custodian) with the trade information.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Batch Status Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Batch Status Description', width: 200, dataIndex: 'description'},
						{header: 'Open', width: 30, dataIndex: 'open', type: 'boolean', tooltip: 'Open batches have not been sent and are available to add/remove items from.'},
						{header: 'Error', width: 30, dataIndex: 'error', type: 'boolean'},
						{header: 'Canceled', width: 30, dataIndex: 'canceled', type: 'boolean'},
						{header: 'Completed', width: 30, dataIndex: 'completed', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.order.batch.setup.BatchStatusWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
