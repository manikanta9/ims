Clifton.order.batch.OrderBatchItemListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'orderBatchItemListFind',
	xtype: 'gridpanel',
	instructions: 'OVERRIDE_ME',
	tableName: 'OVERRIDE_ME',
	additionalPropertiesToRequest: 'id|orderBatch.lastErrorMessage|orderBatch.orderBatchStatus.error|orderBatch.orderBatchStatus.canceled|orderBatch.orderBatchStatus.completed',
	columns: [
		{header: 'Batch #', width: 20, dataIndex: 'orderBatch.id', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Destination', width: 65, dataIndex: 'orderBatch.orderDestination.name', filter: {type: 'combo', searchFieldName: 'orderDestinationId', url: 'orderDestinationListFind.json'}},
		{
			header: 'Batch Status', width: 30, dataIndex: 'orderBatch.orderBatchStatus.name', filter: {type: 'combo', searchFieldName: 'orderBatchStatusId', url: 'orderBatchStatusListFind.json'},
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.json.orderBatch.orderBatchStatus.error)) {
					metaData.css = 'amountNegative';
					metaData.attr = TCG.renderQtip(r.json.orderBatch.lastErrorMessage);
				}
				else if (TCG.isTrue(r.json.orderBatch.orderBatchStatus.completed)) {
					metaData.css = 'amountPositive';
				}
				else if (TCG.isFalse(r.json.orderBatch.orderBatchStatus.open)) {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		}
	],

	getLoadParams: function(firstLoad) {
		return {tableName: this.tableName, fkFieldId: this.getWindow().getMainFormId(), readUncommittedRequested: true};
	},
	editor: {
		detailPageClass: 'Clifton.order.batch.OrderBatchItemWindow',
		drillDownOnly: true
	}
});
Ext.reg('order-batch-item-list-grid', Clifton.order.batch.OrderBatchItemListGrid);
