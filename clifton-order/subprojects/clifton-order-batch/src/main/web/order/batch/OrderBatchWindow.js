Clifton.order.batch.OrderBatchWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'orderBatch.json',

	getClassName: function(config, entity) {
		if (TCG.isEquals('OrderPlacement', entity.orderDestination.destinationType.batchSourceSystemTable.name)) {
			return 'Clifton.order.batch.OrderBatchPlacementWindow';
		}
		return 'Clifton.order.batch.OrderBatchTradeWindow';
	}
});
