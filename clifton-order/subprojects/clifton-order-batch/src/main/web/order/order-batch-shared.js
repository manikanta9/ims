Ext.ns(
	'Clifton.order',
	'Clifton.order.batch',
	'Clifton.order.batch.setup'
);

TCG.use('Clifton.order.batch.OrderBatchListGrid');
TCG.use('Clifton.order.batch.OrderBatchItemListGrid');


Clifton.order.batch.previewOrderBatchQueryResult = function(orderBatch, outputFormat) {
	if (TCG.isBlank(outputFormat)) {
		const params = {
			orderBatchId: orderBatch.id
		};
		TCG.downloadFile('orderBatchFileDownload.json', params);
	}
	else {
		const params = {
			orderBatchId: orderBatch.id,
			fileName: TCG.getValue('orderDestination.name', orderBatch) + ' ' + TCG.parseDate(orderBatch.createDate).format('Y.m.d') + ' Batch # ' + orderBatch.id,
			outputFormat: outputFormat
		};
		TCG.downloadFile('orderBatchSystemQueryResult.json', params);
	}
};


Clifton.order.batch.sendOrderBatch = function(orderBatch, forceResend, reloadObject) {
	const loader = new TCG.data.JsonLoader({
		waitMsg: 'Scheduling Sending...',
		waitTarget: reloadObject,
		params: {
			orderBatchId: orderBatch.id,
			forceResend: forceResend
		},
		onLoad: function(record, conf) {
			if (reloadObject && reloadObject.reload) {
				reloadObject.reload();
			}
		}
	});
	const url = 'orderBatchReadyToSendUpdate.json';
	loader.load(url);
};

Clifton.order.batch.markOrderBatchSent = function(orderBatch, reloadObject) {
	const loader = new TCG.data.JsonLoader({
		waitMsg: 'Marking as Sent...',
		waitTarget: reloadObject,
		params: {
			orderBatchId: orderBatch.id
		},
		onLoad: function(record, conf) {
			if (reloadObject && reloadObject.reload) {
				reloadObject.reload();
			}
		}
	});
	const url = 'orderBatchManuallySentFix.json';
	loader.load(url);
};

Clifton.order.batch.fixOrderBatch = function(orderBatch, reloadObject) {
	const loader = new TCG.data.JsonLoader({
		waitMsg: 'Fixing Order Batch...',
		waitTarget: reloadObject,
		params: {
			orderBatchId: orderBatch.id
		},
		onLoad: function(record, conf) {
			if (reloadObject && reloadObject.reload) {
				reloadObject.reload();
			}
		}
	});
	const url = 'orderBatchFix.json';
	loader.load(url);
};
