package com.clifton.order.batch.cache;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderBatchStatusCacheTests {

	@Resource
	private OrderBatchStatusIdsCache orderBatchStatusIdsCache;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOrderBatchStatusIdsCache() {
		Short[] statusIds = this.orderBatchStatusIdsCache.getOrderBatchStatusIds(OrderBatchStatusIdsCacheImpl.OrderBatchStatusIds.ACTIVE_STATUS);
		Assertions.assertEquals(5, statusIds.length);

		statusIds = this.orderBatchStatusIdsCache.getOrderBatchStatusIds(OrderBatchStatusIdsCacheImpl.OrderBatchStatusIds.OPEN_STATUS);
		Assertions.assertEquals(1, statusIds.length);

		statusIds = this.orderBatchStatusIdsCache.getOrderBatchStatusIds(OrderBatchStatusIdsCacheImpl.OrderBatchStatusIds.ERROR_STATUS);
		Assertions.assertEquals(1, statusIds.length);

		statusIds = this.orderBatchStatusIdsCache.getOrderBatchStatusIds(OrderBatchStatusIdsCacheImpl.OrderBatchStatusIds.COMPLETED_STATUS);
		Assertions.assertEquals(2, statusIds.length);
	}
}
