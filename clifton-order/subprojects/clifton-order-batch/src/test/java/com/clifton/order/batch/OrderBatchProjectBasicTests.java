package com.clifton.order.batch;

import com.clifton.core.util.CollectionUtils;
import com.clifton.order.OrderProjectBasicTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;


/**
 * @author AbhinayaM
 */

@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class OrderBatchProjectBasicTests extends OrderProjectBasicTests {

	@Override
	public String getProjectPrefix() {
		return "order-batch";
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		return CollectionUtils.createHashSet(
				"saveOrderBatch"
		).contains(method.getName());
	}
}
