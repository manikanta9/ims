package com.clifton.order.batch.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderBatchStatusSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Boolean open;

	@SearchField
	private Boolean error;

	@SearchField
	private Boolean completed;

	@SearchField
	private Boolean canceled;
}
