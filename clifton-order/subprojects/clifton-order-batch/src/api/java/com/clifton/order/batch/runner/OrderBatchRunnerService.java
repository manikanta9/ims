package com.clifton.order.batch.runner;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.batch.OrderBatch;


/**
 * @author manderson
 */
public interface OrderBatchRunnerService {

	////////////////////////////////////////////////////////////////////////////
	////////                Order Batch Processing                      ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Manually run the order batch processing for the selected destination.
	 * When manually running you can choose to immediately send or hold in a batched state
	 * in which more items could later be added until it's updated to be sent.
	 * Sending is performed asynchronously, and the batch itself is just set to Ready to Send which raises an event to schedule the sending
	 */
	@SecureMethod(dtoClass = OrderBatch.class)
	public void processOrderBatchingForDestination(short orderDestinationId, boolean send);

	////////////////////////////////////////////////////////////////////////////
	////////                Order Batch Files                           ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = OrderBatch.class)
	public DataTable getOrderBatchSystemQueryResult(long orderBatchId);


	/**
	 * Executes the query for the OrderBatch, but also converts it into the same file and format that would be sent
	 */
	@SecureMethod(dtoClass = OrderBatch.class)
	public FileWrapper downloadOrderBatchFile(long orderBatchId);

	////////////////////////////////////////////////////////////////////////////
	////////                Order Batch Sending                         ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sends the OrderBatch.  Confirms current status of the batch is in Ready for Sending state
	 */
	@DoNotAddRequestMapping
	public void sendOrderBatch(long orderBatchId);


	/**
	 * Fixes Order Batch in Sending status that are not actually Sending (server restart)
	 */
	public void fixOrderBatch(long orderBatchId);

	/**
	 * Manually mark an orderBatch to Sent Status
	 */
	@SecureMethod(dtoClass = OrderBatch.class)
	public void fixOrderBatchManuallySent(long orderBatchId);
}
