package com.clifton.order.batch.cache;

/**
 * @author AbhinayaM
 */
public interface OrderBatchStatusIdsCache {

	/**
	 * Returns an array of OrderBatchStatus id's for the specified key. Use constants on this class for available key names.
	 * NOTE: used to improve performance by avoiding extra joins on large tables (OrderBatch, etc.)
	 */
	public Short[] getOrderBatchStatusIds(OrderBatchStatusIdsCacheImpl.OrderBatchStatusIds statusKey);
}
