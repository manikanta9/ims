package com.clifton.order.batch.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import lombok.Getter;
import lombok.Setter;


/**
 * @author AbhinayaM
 */

@CacheByName
@Getter
@Setter
public class OrderBatchStatus extends NamedEntity<Short> {

	public static final String BATCHED_STATUS_NAME = "Batched";
	public static final String READY_TO_SEND_STATUS_NAME = "Ready to Send";
	public static final String SENDING_STATUS_NAME = "Sending";
	public static final String SENT_STATUS_NAME = "Sent";
	public static final String MANUAL_SENT_STATUS_NAME = "Sent (Manual)";
	public static final String ERROR_STATUS_NAME = "Error";
	public static final String CANCELED_STATUS_NAME = "Canceled";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Open indicates that the batch has not been sent and batch items can still be added or removed
	 * Past this status, items can still be canceled, however that cancellation is tracked on the source entity
	 * i.e. OrderExecutionStatus for Placements.
	 */
	private boolean open;

	private boolean error;

	private boolean completed;

	private boolean canceled;
}
