package com.clifton.order.batch.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import lombok.Getter;
import lombok.Setter;


/**
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderBatchItemSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Long id;


	@SearchField(searchField = "orderBatch.id")
	private Long orderBatchId;

	@SearchField(searchFieldPath = "orderBatch", searchField = "orderBatchStatus.id")
	private Short orderBatchStatusId;

	@SearchField(searchFieldPath = "orderBatch", searchField = "orderDestination.id")
	private Short orderDestinationId;

	@SearchField(searchFieldPath = "orderBatch.orderBatchStatus", searchField = "open")
	private Boolean open;

	@SearchField(searchFieldPath = "orderBatch.orderBatchStatus", searchField = "canceled")
	private Boolean canceled;

	@SearchField(searchFieldPath = "orderBatch.orderDestination.destinationType.batchSourceSystemTable", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String tableName;

	@SearchField
	private Long fkFieldId;
}
