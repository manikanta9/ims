package com.clifton.order.batch;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.usedby.softlink.SoftLinkField;
import lombok.Getter;
import lombok.Setter;


/**
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderBatchItem extends BaseEntity<Long> {

	private OrderBatch orderBatch;


	@SoftLinkField(tableBeanPropertyName = "orderBatch.orderDestination.destinationType.batchSourceSystemTable")
	private Long fkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabelShort() {
		StringBuilder result = new StringBuilder(64);
		result.append("Item ");
		result.append(getId());
		if (getOrderBatch() != null) {
			result.append(' ');
			result.append(getOrderBatch().getLabelShort());
		}
		return result.toString();
	}
}
