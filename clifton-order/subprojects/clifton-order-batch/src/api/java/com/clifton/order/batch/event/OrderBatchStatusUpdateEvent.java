package com.clifton.order.batch.event;

import com.clifton.core.util.event.EventObject;
import com.clifton.order.batch.OrderBatch;
import com.clifton.order.batch.OrderBatchItem;
import lombok.Getter;
import lombok.Setter;


/**
 * THe <code>OrderBatchStatusUpdateEvent</code> represents an Event that is raised with the status
 * of the OrderBatch is updated
 * 1. If moved to Ready To Send - triggers the sending of the batch
 * 2. If batch is for destination where execution venue = true, update the status on all of the placements
 *
 * @author manderson
 */
@Getter
@Setter
public class OrderBatchStatusUpdateEvent extends EventObject<Long, Object> {

	public static final String EVENT_NAME = "Order Batch Status Update";


	/**
	 * For the batch, so we know if it's an execution venue then we need to update the placements
	 */
	private boolean executionVenue;

	/**
	 * Execution venue this will be placements, other case currently is OrderTrade
	 */
	private String orderBatchSystemTableName;

	private String newBatchStatusName;

	/**
	 * If set, then ONLY the specific item needs to update
	 * This is used on insert of batch items so we update the placement for each item insert
	 */
	private Long fkFieldId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private OrderBatchStatusUpdateEvent(Long orderBatchId, boolean executionVenue, String orderBatchSystemTableName, String newBatchStatusName, Long fkFieldId) {
		super(EVENT_NAME);
		setTarget(orderBatchId);
		setExecutionVenue(executionVenue);
		setOrderBatchSystemTableName(orderBatchSystemTableName);
		setNewBatchStatusName(newBatchStatusName);
		setFkFieldId(fkFieldId);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static OrderBatchStatusUpdateEvent ofOrderBatch(OrderBatch orderBatch) {
		return new OrderBatchStatusUpdateEvent(orderBatch.getId(), orderBatch.getOrderDestination().getDestinationType().isExecutionVenue(), orderBatch.getOrderDestination().getDestinationType().getBatchSourceSystemTable().getName(), orderBatch.getOrderBatchStatus().getName(), null);
	}


	public static OrderBatchStatusUpdateEvent ofOrderBatchItem(OrderBatchItem orderBatchItem) {
		OrderBatch orderBatch = orderBatchItem.getOrderBatch();
		return new OrderBatchStatusUpdateEvent(orderBatch.getId(), orderBatch.getOrderDestination().getDestinationType().isExecutionVenue(), orderBatch.getOrderDestination().getDestinationType().getBatchSourceSystemTable().getName(), orderBatch.getOrderBatchStatus().getName(), orderBatchItem.getFkFieldId());
	}
}
