package com.clifton.order.batch.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.order.batch.cache.OrderBatchStatusIdsCacheImpl;
import lombok.Getter;
import lombok.Setter;


/**
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderBatchSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Long id;

	@SearchField(searchField = "orderDestination.id", sortField = "orderDestination.name")
	private Short orderDestinationId;

	@SearchField(searchFieldPath = "orderDestination.destinationType", searchField = "executionVenue")
	private Boolean executionVenue;

	@SearchField(searchField = "orderBatchStatus.id", sortField = "orderBatchStatus.name")
	private Short orderBatchStatusId;

	@SearchField(searchField = "orderBatchStatus.open")
	private Boolean open;

	@SearchField(searchField = "orderBatchStatus.error")
	private Boolean error;

	@SearchField(searchField = "orderBatchStatus.canceled")
	private Boolean canceled;

	@SearchField(searchField = "orderBatchStatus.completed")
	private Boolean completed;

	@SearchField
	private String lastErrorMessage;

	private OrderBatchStatusIdsCacheImpl.OrderBatchStatusIds orderBatchStatusIds;
}
