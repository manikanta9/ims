package com.clifton.order.batch.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.order.batch.setup.OrderBatchSetupService;
import com.clifton.order.batch.setup.OrderBatchStatus;
import com.clifton.order.batch.setup.search.OrderBatchStatusSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;


/**
 * @author AbhinayaM
 */
@Component
public class OrderBatchStatusIdsCacheImpl extends SelfRegisteringSimpleDaoCache<OrderBatchStatus, String, Short[]> implements OrderBatchStatusIdsCache {

	private OrderBatchSetupService orderBatchSetupService;


	@Override
	public Short[] getOrderBatchStatusIds(OrderBatchStatusIdsCacheImpl.OrderBatchStatusIds statusKey) {
		return getFromCacheOrUpdate(statusKey.name(), () -> {
			OrderBatchStatusSearchForm searchForm = new OrderBatchStatusSearchForm();
			searchForm.setCompleted(statusKey.getComplete());
			searchForm.setOpen(statusKey.getOpen());
			searchForm.setError(statusKey.getError());
			return searchForm;
		});
	}


	private Short[] getFromCacheOrUpdate(String cacheKey, Supplier<OrderBatchStatusSearchForm> searchFormSupplierIfMissing) {
		Short[] result = getCacheHandler().get(getCacheName(), cacheKey);
		if (result == null) {
			// not in cache: retrieve from service and store in cache for future calls
			OrderBatchStatusSearchForm searchForm = searchFormSupplierIfMissing.get();
			List<OrderBatchStatus> list = getOrderBatchSetupService().getOrderBatchStatusList(searchForm);

			int size = CollectionUtils.getSize(list);
			result = new Short[size];
			for (int i = 0; i < size; i++) {
				result[i] = list.get(i).getId();
			}
			getCacheHandler().put(getCacheName(), cacheKey, result);
		}
		return result;
	}


	/**
	 * The <code>OrderBatchStatus</code> enum defines various options for OrderBatchStatus searches.
	 */
	public enum OrderBatchStatusIds {
		OPEN_STATUS(new IdsBuilder().withOpen(true)),
		ACTIVE_STATUS(new IdsBuilder().withCompleted(false)),
		COMPLETED_STATUS(new IdsBuilder().withCompleted(true)),
		ERROR_STATUS(new IdsBuilder().withError(true));


		private final Boolean open;
		private final Boolean completed;
		private final Boolean error;


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////
		OrderBatchStatusIds(IdsBuilder builder) {
			this.completed = builder.completed;
			this.open = builder.open;
			this.error = builder.error;
		}


		public Boolean getComplete() {
			return this.completed;
		}


		public Boolean getOpen() {
			return this.open;
		}


		public Boolean getError() {
			return this.error;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private static class IdsBuilder {

			private Boolean open;
			private Boolean completed;
			private Boolean error;


			public IdsBuilder withCompleted(boolean completed) {
				this.completed = completed;
				return this;
			}


			public IdsBuilder withOpen(boolean open) {
				this.open = open;
				return this;
			}


			public IdsBuilder withError(boolean error) {
				this.error = error;
				return this;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getters and Setters                             ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderBatchSetupService getOrderBatchSetupService() {
		return this.orderBatchSetupService;
	}


	public void setOrderBatchSetupService(OrderBatchSetupService orderBatchSetupService) {
		this.orderBatchSetupService = orderBatchSetupService;
	}
}
