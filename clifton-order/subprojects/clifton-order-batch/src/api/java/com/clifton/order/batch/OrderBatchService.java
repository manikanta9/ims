package com.clifton.order.batch;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.order.batch.search.OrderBatchItemSearchForm;
import com.clifton.order.batch.search.OrderBatchSearchForm;

import java.util.List;


/**
 * @author AbhinayaM
 */
public interface OrderBatchService {

	////////////////////////////////////////////////////////////////////////////
	////////                Order Batch Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderBatch getOrderBatch(long id);


	/**
	 * Returns the list of open batches for a destination.  The list is cached as there usually would not be any or only one
	 * so we can easily add items to the open batch.
	 */
	public List<OrderBatch> getOrderBatchOpenListForDestination(short orderDestinationId);


	public List<OrderBatch> getOrderBatchList(OrderBatchSearchForm searchForm);


	/**
	 * Saves the OrderBatch.  Can ONLY be used for new or open OrderBatches
	 * Also save the list of items.
	 */
	@DoNotAddRequestMapping
	public OrderBatch saveOrderBatch(OrderBatch bean);


	/**
	 * Triggers the sending of the Order Batch by moving it to Ready to Send status and raises an event to begin the sending
	 * <p>
	 * forceResend can be used to force the triggering of Ready to Send event in case something happened while it was in Ready To Send state.
	 * forceResend option can also be used if the status is in completed state, but it needs to be re-sent
	 **/
	@SecureMethod(dtoClass = OrderBatch.class)
	public void updateOrderBatchReadyToSend(long orderBatchId, boolean forceResend);


	@SecureMethod(dtoClass = OrderBatch.class)
	public void updateOrderBatchError(long orderBatchId, String errorMessage);

	@DoNotAddRequestMapping
	public OrderBatch updateOrderBatchStatus(OrderBatch orderBatch, String batchStatusName);

	////////////////////////////////////////////////////////////////////////////
	////////             Order Batch Item Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	public OrderBatchItem getOrderBatchItem(long id);


	public List<OrderBatchItem> getOrderBatchItemListForBatch(long orderBatchId);


	public List<OrderBatchItem> getOrderBatchItemList(OrderBatchItemSearchForm searchForm);


	@DoNotAddRequestMapping
	public void deleteOrderBatchItem(long id);
}
