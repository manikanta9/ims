package com.clifton.order.batch.setup;

import com.clifton.order.batch.setup.search.OrderBatchStatusSearchForm;

import java.util.List;


/**
 * @author AbhinayaM
 */
public interface OrderBatchSetupService {


	////////////////////////////////////////////////////////////////////////////
	////////            Order Batch Status Methods                      ////////
	////////////////////////////////////////////////////////////////////////////
	public OrderBatchStatus getOrderBatchStatus(short id);


	public OrderBatchStatus getOrderBatchStatusByName(String name);


	public List<OrderBatchStatus> getOrderBatchStatusList(OrderBatchStatusSearchForm searchForm);


	public OrderBatchStatus saveOrderBatchStatus(OrderBatchStatus bean);


	public void deleteOrderBatchStatus(short id);
}
