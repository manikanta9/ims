package com.clifton.order.batch;

import com.clifton.core.beans.BaseEntity;
import com.clifton.order.batch.setup.OrderBatchStatus;
import com.clifton.order.setup.destination.OrderDestination;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * The OrderBatch class represents a grouping of batched items (Placements, Trades) that are intended to be sent together to the specified destination.
 *
 * @author AbhinayaM
 */
@Getter
@Setter
public class OrderBatch extends BaseEntity<Long> {


	private OrderDestination orderDestination;

	private OrderBatchStatus orderBatchStatus;

	/**
	 * If the batch ever makes it into an error state, this is the last error message that was received
	 */
	private String lastErrorMessage;


	private List<OrderBatchItem> itemList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabelShort() {
		StringBuilder result = new StringBuilder(64);
		result.append("Batch ");
		result.append(getId());
		if (getOrderBatchStatus() != null) {
			result.append(' ');
			result.append(getOrderBatchStatus().getName());
		}
		if (getOrderDestination() != null) {
			result.append(" to ");
			result.append(getOrderDestination().getName());
		}
		return result.toString();
	}
}
