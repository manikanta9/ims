package com.clifton.order;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public abstract class OrderProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "order";
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		// Auto generated code - move to core?
		imports.add("org.springframework.http.ResponseEntity");
		imports.add("javax.validation.Valid");
		imports.add("lombok.Getter");
		imports.add("lombok.Setter");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("lombok.Builder");
	}


	// TODO MOVE TO CORE IF THIS WILL BE MORE COMMON WITH OPEN API?
	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> suffixList = super.getAllowedContextManagedBeanSuffixNames();
		suffixList.add("Api");
		suffixList.add("ApiClient");
		return suffixList;
	}
}
