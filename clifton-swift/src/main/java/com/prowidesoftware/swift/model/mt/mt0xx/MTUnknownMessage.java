package com.prowidesoftware.swift.model.mt.mt0xx;

import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;


public class MTUnknownMessage extends AbstractMT {

	public MTUnknownMessage(SwiftMessage swiftMessage) {
		super(swiftMessage);
	}


	@Override
	public String getMessageType() {
		return "Unknown";
	}
}
