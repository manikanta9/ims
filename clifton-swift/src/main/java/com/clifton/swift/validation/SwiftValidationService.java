package com.clifton.swift.validation;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.swift.message.SwiftMessageFormats;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * Defines methods used to validate SWIFT messages.
 *
 * @author mwacker
 */
public interface SwiftValidationService {

	@ModelAttribute("data")
	@SecureMethod(securityResource = "SWIFT")
	public String validateSwiftMessage(String message, SwiftMessageFormats messageType);
}
