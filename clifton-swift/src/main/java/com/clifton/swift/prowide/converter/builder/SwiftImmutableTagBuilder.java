package com.clifton.swift.prowide.converter.builder;

import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * <code>SwiftImmutableTagBuilder</code> delay construction of an immutable block until
 * all its tags are added, only tags can be added to this immutable block.
 */
public class SwiftImmutableTagBuilder implements SwiftBlockBuilder {

	private Function<Tag[], SwiftTagListBlock> blockSupplier;
	private List<Tag> tags = new ArrayList<>();

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private SwiftImmutableTagBuilder(Function<Tag[], SwiftTagListBlock> blockSupplier) {
		this.blockSupplier = blockSupplier;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public static SwiftImmutableTagBuilder create(Function<Tag[], SwiftTagListBlock> blockSupplier) {
		return new SwiftImmutableTagBuilder(blockSupplier);
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public SwiftImmutableTagBuilder withTag(Supplier<Tag> componentSupplier) {
		this.tags.add(componentSupplier.get());
		return this;
	}


	@Override
	public SwiftTagListBlock toBlock() {
		return this.blockSupplier.apply(this.tags.toArray(new Tag[this.tags.size()]));
	}
}
