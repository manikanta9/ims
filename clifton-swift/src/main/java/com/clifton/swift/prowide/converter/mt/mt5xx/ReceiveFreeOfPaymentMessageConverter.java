package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.clifton.instruction.messages.transfers.ReceiveFreeAgainstPayment;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.prowide.converter.mt.AbstractSwiftMtMessageConverter;
import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.mt.mt5xx.MT542;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Stream;


@Component
public class ReceiveFreeOfPaymentMessageConverter extends AbstractSwiftMtMessageConverter<ReceiveFreeAgainstPayment, MT542> {

	@Override
	public Class<?> getInternalMessageClass() {
		return ReceiveFreeAgainstPayment.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MT542.class;
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	@Override
	public MT542 toSwiftEntity(ReceiveFreeAgainstPayment entity) {
		MT542 message = new MT542();
		AgainstPaymentMessageBuilder.buildSwiftMessage(message, entity, getAgainstPaymentBlockProvider());
		return message;
	}


	@Override
	public ReceiveFreeAgainstPayment fromSwiftEntity(MT542 message) {
		ReceiveFreeAgainstPayment receiveFreeAgainstPayment = new ReceiveFreeAgainstPayment();
		receiveFreeAgainstPayment.setSenderBIC(message.getSender());
		receiveFreeAgainstPayment.setReceiverBIC(message.getReceiver());
		receiveFreeAgainstPayment.setBuy(false);
		Stream.of(
				getSequenceBlockFields(message.getSequenceA(), MT542.SequenceA.START_END_16RS),
				getSequenceBlockFields(message.getSequenceB(), MT542.SequenceB.START_END_16RS),
				getSequenceBlockFields(message.getSequenceC(), MT542.SequenceC.START_END_16RS),
				getSequenceBlockFields(message.getSequenceD(), MT542.SequenceD.START_END_16RS),
				getSequenceBlockFields(message.getSequenceE(), MT542.SequenceE.START_END_16RS)
		)
				.flatMap(Function.identity())
				.forEach(b -> AgainstPaymentMessageBuilder.Setters.forEach(s -> s.accept(b, receiveFreeAgainstPayment)));
		return receiveFreeAgainstPayment;
	}


	public AgainstPaymentBlockProvider getAgainstPaymentBlockProvider() {
		return new AgainstPaymentBlockProvider() {
			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA() {
				return MT542.SequenceA::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA1() {
				return MT542.SequenceA1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB() {
				return MT542.SequenceB::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB1() {
				return MT542.SequenceB1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceC() {
				return MT542.SequenceC::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceD() {
				return MT542.SequenceD::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE() {
				return MT542.SequenceE::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE1() {
				return MT542.SequenceE1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE3() {
				return MT542.SequenceE3::newInstance;
			}
		};
	}
}
