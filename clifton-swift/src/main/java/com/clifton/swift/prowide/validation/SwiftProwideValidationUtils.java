package com.clifton.swift.prowide.validation;

import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.validator.ValidationProblem;

import java.util.List;


/**
 * @author mwacker
 */
public class SwiftProwideValidationUtils {

	public static final String B1 = "{1:F01FOOSEDR0AXXX0000000000}";
	public static final String B2_103 = "{2:I103FOORECV0XXXXN}";


	/**
	 * Dumps validation problems in standard out
	 */
	public static String getValidationProblemsAsString(List<ValidationProblem> problems) {
		return getValidationProblemsAsString(problems, (SwiftMessage) null);
	}


	/**
	 * Dumps validation problems in standard out
	 */
	public static String getValidationProblemsAsString(List<ValidationProblem> problems, AbstractMT mt) {
		return getValidationProblemsAsString(problems, mt.getSwiftMessage());
	}


	/**
	 * Dumps validation problems in standard out
	 */
	public static String getValidationProblemsAsString(List<ValidationProblem> problems, SwiftMessage m) {
		StringBuilder sb = new StringBuilder();
		if (problems.isEmpty()) {
			sb.append("MESSAGE OK. NO VALIDATION PROBLEMS FOUND.");
		}
		else {
			sb.append("MALFORMED MESSAGE, ").append(problems.size()).append(" VALIDATION PROBLEM").append(problems.size() == 1 ? "" : "S").append(" FOUND.\n");
			int k = 1;
			for (final ValidationProblem p : problems) {
				String tagName = "";
				if (m != null && p.getTagIndex() != null && p.getTagIndex() < m.getBlock4().getTags().size()) {
					final Tag t = m.getBlock4().getTag(p.getTagIndex());
					tagName = " " + t.getName();
				}
				StringBuilder s = new StringBuilder();
				s.append(k).append("/").append(problems.size()).append(tagName).append(": ").append(p.getErrorKey());
				if (p.getTagIndex() != null) {
					s.append(" tag index ").append(p.getTagIndex());
				}
				s.append(" --> ").append(p.getMessage());
				sb.append(s.toString()).append("\n");
				k++;
			}
		}
		return sb.toString();
	}
}
