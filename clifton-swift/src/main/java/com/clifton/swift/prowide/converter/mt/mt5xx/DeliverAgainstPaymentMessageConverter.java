package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.prowide.converter.mt.AbstractSwiftMtMessageConverter;
import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.mt.mt5xx.MT541;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Stream;


@Component
public class DeliverAgainstPaymentMessageConverter extends AbstractSwiftMtMessageConverter<DeliverAgainstPayment, MT541> {

	@Override
	public Class<?> getInternalMessageClass() {
		return DeliverAgainstPayment.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MT541.class;
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	@Override
	public DeliverAgainstPayment fromSwiftEntity(MT541 message) {
		DeliverAgainstPayment deliverAgainstPayment = new DeliverAgainstPayment();
		deliverAgainstPayment.setSenderBIC(message.getSender());
		deliverAgainstPayment.setReceiverBIC(message.getReceiver());
		deliverAgainstPayment.setBuy(true);
		Stream.of(
				getSequenceBlockFields(message.getSequenceA(), MT541.SequenceA.START_END_16RS),
				getSequenceBlockFields(message.getSequenceB(), MT541.SequenceB.START_END_16RS),
				getSequenceBlockFields(message.getSequenceC(), MT541.SequenceC.START_END_16RS),
				getSequenceBlockFields(message.getSequenceD(), MT541.SequenceD.START_END_16RS),
				getSequenceBlockFields(message.getSequenceE(), MT541.SequenceE.START_END_16RS)
		)
				.flatMap(Function.identity())
				.forEach(b -> AgainstPaymentMessageBuilder.Setters.forEach(s -> s.accept(b, deliverAgainstPayment)));
		return deliverAgainstPayment;
	}


	@Override
	public MT541 toSwiftEntity(DeliverAgainstPayment entity) {
		MT541 message = new MT541();
		AgainstPaymentMessageBuilder.buildSwiftMessage(message, entity, getAgainstPaymentBlockProvider());
		return message;
	}


	public AgainstPaymentBlockProvider getAgainstPaymentBlockProvider() {
		return new AgainstPaymentBlockProvider() {
			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA() {
				return MT541.SequenceA::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA1() {
				return MT541.SequenceA1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB() {
				return MT541.SequenceB::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB1() {
				return MT541.SequenceB1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceC() {
				return MT541.SequenceC::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceD() {
				return MT541.SequenceD::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE() {
				return MT541.SequenceE::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE1() {
				return MT541.SequenceE1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE3() {
				return MT541.SequenceE3::newInstance;
			}
		};
	}
}
