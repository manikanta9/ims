package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.prowide.converter.mt.AbstractSwiftMtMessageConverter;
import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.mt.mt5xx.MT543;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Stream;


@Component
public class ReceiveAgainstPaymentMessageConverter extends AbstractSwiftMtMessageConverter<ReceiveAgainstPayment, MT543> {

	@Override
	public Class<?> getInternalMessageClass() {
		return ReceiveAgainstPayment.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MT543.class;
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	@Override
	public ReceiveAgainstPayment fromSwiftEntity(MT543 message) {
		ReceiveAgainstPayment receiveAgainstPayment = new ReceiveAgainstPayment();
		receiveAgainstPayment.setSenderBIC(message.getSender());
		receiveAgainstPayment.setReceiverBIC(message.getReceiver());
		receiveAgainstPayment.setBuy(false);
		Stream.of(
				getSequenceBlockFields(message.getSequenceA(), MT543.SequenceA.START_END_16RS),
				getSequenceBlockFields(message.getSequenceB(), MT543.SequenceB.START_END_16RS),
				getSequenceBlockFields(message.getSequenceC(), MT543.SequenceC.START_END_16RS),
				getSequenceBlockFields(message.getSequenceD(), MT543.SequenceD.START_END_16RS),
				getSequenceBlockFields(message.getSequenceE(), MT543.SequenceE.START_END_16RS)
		)
				.flatMap(Function.identity())
				.forEach(b -> AgainstPaymentMessageBuilder.Setters.forEach(s -> s.accept(b, receiveAgainstPayment)));
		return receiveAgainstPayment;
	}


	@Override
	public MT543 toSwiftEntity(ReceiveAgainstPayment entity) {
		MT543 message = new MT543();
		AgainstPaymentMessageBuilder.buildSwiftMessage(message, entity, getAgainstPaymentBlockProvider());
		return message;
	}


	public AgainstPaymentBlockProvider getAgainstPaymentBlockProvider() {
		return new AgainstPaymentBlockProvider() {
			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA() {
				return MT543.SequenceA::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA1() {
				return MT543.SequenceA1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB() {
				return MT543.SequenceB::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB1() {
				return MT543.SequenceB1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceC() {
				return MT543.SequenceC::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceD() {
				return MT543.SequenceD::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE() {
				return MT543.SequenceE::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE1() {
				return MT543.SequenceE1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE3() {
				return MT543.SequenceE3::newInstance;
			}
		};
	}
}
