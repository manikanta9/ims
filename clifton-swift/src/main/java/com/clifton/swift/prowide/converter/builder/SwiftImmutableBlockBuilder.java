package com.clifton.swift.prowide.converter.builder;

import com.prowidesoftware.swift.model.SwiftTagListBlock;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * <code>SwiftImmutableBlockBuilder</code> delay construction of an immutable block until
 * all its blocks are added, only blocks can be added to this immutable block.
 */
public class SwiftImmutableBlockBuilder implements SwiftBlockBuilder {

	private Function<SwiftTagListBlock[], SwiftTagListBlock> blockSupplier;
	private List<SwiftTagListBlock> blocks = new ArrayList<>();

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private SwiftImmutableBlockBuilder(Function<SwiftTagListBlock[], SwiftTagListBlock> blockSupplier) {
		this.blockSupplier = blockSupplier;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public static SwiftImmutableBlockBuilder create(Function<SwiftTagListBlock[], SwiftTagListBlock> blockSupplier) {
		return new SwiftImmutableBlockBuilder(blockSupplier);
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public SwiftImmutableBlockBuilder withBlock(Supplier<SwiftTagListBlock> componentSupplier) {
		this.blocks.add(componentSupplier.get());
		return this;
	}


	@Override
	public SwiftTagListBlock toBlock() {
		return this.blockSupplier.apply(this.blocks.toArray(new SwiftTagListBlock[this.blocks.size()]));
	}
}
