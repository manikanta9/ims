package com.clifton.swift.prowide.converter.mt.mt2xx;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.transfers.AbstractTransferMessage;
import com.clifton.instruction.messages.transfers.TransferCancellationMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverter;
import com.clifton.swift.message.converter.SwiftMessageConverterLocator;
import com.clifton.core.util.MathUtils;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field11S;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.field.Field21;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt2xx.MT292;
import com.prowidesoftware.swift.scheme.Scheme;
import com.prowidesoftware.swift.scheme.SchemeField;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * <code>FinancialInstitutionTransferMessageToMT292Converter</code> generate cancellation messages for category 2, financial institution
 * transfer messages.
 * <p>
 * Cancellation messages MT292, include mandatory fields from the original message:
 * MT210: 20,30,21,32B
 * MT202: 20,21,32A,58A
 * <p>
 * Typical message format:
 * 20:      Transaction Reference Number
 * 21:      Related Reference (Field 20 of the message that is being cancelled)
 * 11S:     MT# and date of original message
 * 79:      Optional narrative
 * copy of mandatory fields from original message (see above)
 */
@Component
public class FinancialInstitutionTransferMessageToMT292Converter<M extends AbstractMT> implements SwiftMessageConverter<TransferCancellationMessage, MT292> {

	public static final String PROWIDE_SCHEME_PACKAGE = "com.prowidesoftware.swift.scheme.Scheme";

	private SwiftMessageConverterLocator swiftMessageConverterLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getInternalMessageClass() {
		return TransferCancellationMessage.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MT292.class;
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	@Override
	public String toSwiftMessage(TransferCancellationMessage entity) {
		LogUtils.info(getClass(), "Converting Swift Message from Instruction Entity using converter implementation:  " + this.getClass().getName());
		AbstractMT abstractMT = toSwiftEntity(entity);
		return abstractMT.message();
	}


	@Override
	public TransferCancellationMessage fromSwiftEntity(MT292 swiftEntity) {
		throw new UnsupportedOperationException("From Swift Entity messages are not supported.");
	}


	@Override
	public MT292 toSwiftEntity(TransferCancellationMessage cancellationMessage) {
		MT292 message = new MT292();

		AbstractTransferMessage originalTransferMessage = cancellationMessage.getOriginalMessage();

		message.setSender(originalTransferMessage.getSenderBIC());
		message.setReceiver(originalTransferMessage.getReceiverBIC());

		message.addField(new Field20(originalTransferMessage.getTransactionReferenceNumber()));
		message.addField(new Field21(originalTransferMessage.getMessageReferenceNumber().toString()));

		@SuppressWarnings("unchecked")
		SwiftMessageConverter<AbstractTransferMessage, M> converter = getSwiftMessageConverterLocator().locate(originalTransferMessage, getSupportedMessageFormat());
		M originalMtMsg = converter.toSwiftEntity(originalTransferMessage);

		String messageNumber = Scheme.detectSchemeName(originalMtMsg.getSwiftMessage());
		AssertUtils.assertTrue(MathUtils.isNumber(messageNumber), () -> String.format("Expected the message to have the format MT2## [%s].", originalMtMsg.getClass().getSimpleName()));

		Field11S field11S = new Field11S();
		field11S.setMT(messageNumber);
		field11S.setDate(DateUtils.fromDate(originalTransferMessage.getValueDate(), "yyMMdd"));
		message.addField(field11S);

		Set<String> mandatoryFields = getMandatoryFields(originalMtMsg);

		Stream.of(originalMtMsg.getSwiftMessage())
				.map(SwiftMessage::getBlock4)
				.flatMap(s -> Arrays.stream(s.asTagArray()))
				.map(Tag::asField)
				.filter(f -> mandatoryFields.contains(f.getName()))
				.forEach(f -> {
					if (StringUtils.isEqual(f.getName(), Field20.NAME)) {
						message.addField(new Field20(originalTransferMessage.getMessageReferenceNumber().toString()));
					}
					else {
						message.addField(f);
					}
				});

		return message;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Uses the Prowide Integration validation package to determine the list of mandatory message fields.
	 */
	private Set<String> getMandatoryFields(M message) {
		String number = Scheme.detectSchemeName(message.getSwiftMessage());
		String schemeClass = PROWIDE_SCHEME_PACKAGE + number;

		try {
			Scheme scheme = (Scheme) Class.forName(schemeClass).newInstance();
			List<SchemeField> schemeFields = scheme.getAllFields();
			return schemeFields.stream()
					.filter(SchemeField::isField)
					.filter(sf -> sf.getMinRepetitions() > 0)
					.flatMap(sf -> sf.getLetterOptionsCount() > 0 ? sf.getLetterOptions().stream().map(l -> sf.getName() + l) : Stream.of(sf.getName()))
					.collect(Collectors.toSet());
		}
		catch (Exception e) {
			AssertUtils.fail(() -> String.format("Could not create Scheme for class name [%s]", schemeClass));
		}
		return Collections.emptySet();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageConverterLocator getSwiftMessageConverterLocator() {
		return this.swiftMessageConverterLocator;
	}


	public void setSwiftMessageConverterLocator(SwiftMessageConverterLocator swiftMessageConverterLocator) {
		this.swiftMessageConverterLocator = swiftMessageConverterLocator;
	}
}
