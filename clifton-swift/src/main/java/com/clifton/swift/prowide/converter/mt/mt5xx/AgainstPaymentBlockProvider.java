package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.prowidesoftware.swift.model.SwiftTagListBlock;

import java.util.function.Function;


public interface AgainstPaymentBlockProvider {

	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA1();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB1();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceC();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceD();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE1();


	public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE3();
}
