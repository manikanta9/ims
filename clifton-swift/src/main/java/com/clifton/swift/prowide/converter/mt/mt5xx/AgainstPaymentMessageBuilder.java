package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.instruction.messages.beans.RepoRateTypes;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.instruction.messages.transfers.ExposureTypes;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;
import com.clifton.swift.prowide.converter.builder.SwiftMessageBlockBuilder;
import com.clifton.swift.prowide.converter.mt.AbstractSwiftMtMessageConverter;
import com.clifton.swift.util.SwiftUtils;
import com.prowidesoftware.swift.model.field.Field11A;
import com.prowidesoftware.swift.model.field.Field12A;
import com.prowidesoftware.swift.model.field.Field12B;
import com.prowidesoftware.swift.model.field.Field19A;
import com.prowidesoftware.swift.model.field.Field20C;
import com.prowidesoftware.swift.model.field.Field22F;
import com.prowidesoftware.swift.model.field.Field23G;
import com.prowidesoftware.swift.model.field.Field35B;
import com.prowidesoftware.swift.model.field.Field36B;
import com.prowidesoftware.swift.model.field.Field90A;
import com.prowidesoftware.swift.model.field.Field90B;
import com.prowidesoftware.swift.model.field.Field92A;
import com.prowidesoftware.swift.model.field.Field94B;
import com.prowidesoftware.swift.model.field.Field95P;
import com.prowidesoftware.swift.model.field.Field95Q;
import com.prowidesoftware.swift.model.field.Field95R;
import com.prowidesoftware.swift.model.field.Field97A;
import com.prowidesoftware.swift.model.field.Field98A;
import com.prowidesoftware.swift.model.field.Field99B;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt5xx.MT541;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;


/**
 * <code>AgainstPaymentMessageBuilder</code> Builds a base message, represents BOTH Equities and Futures.
 * The builder is NULL-DRIVEN, by the existence or absence of data values in the provided Against Payment Entity.
 */
public class AgainstPaymentMessageBuilder {

	public static final String TAG_SHOULD_ONLY_APPEAR_ONCE = "Tag should only appear once ";
	protected static final List<BiConsumer<AbstractSwiftMtMessageConverter.BlockField, AbstractTradeMessage>> Setters = Arrays.asList(
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field23G) {
					deliverAgainstPayment.setMessageFunctions(MessageFunctions.findByCode(block.getField().getValue()));
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field20C) {
					Field20C field20C = (Field20C) block.getField();
					if (Objects.equals("SEME", field20C.getQualifier())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getTransactionReferenceNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field20C.getName());
						deliverAgainstPayment.setTransactionReferenceNumber(field20C.getReference());
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field20C) {
					Field20C field20C = (Field20C) block.getField();
					if (Objects.equals("PREV", field20C.getQualifier())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getMessageReferenceNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field20C.getName());
						try {
							deliverAgainstPayment.setMessageReferenceNumber(Long.parseLong(field20C.getReference()));
						}
						catch (NumberFormatException nfe) {
							// ignore
						}
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field94B) {
					Field94B field94B = (Field94B) block.getField();
					if (Objects.equals(field94B.getQualifier(), "TRAD") && Objects.equals(field94B.getPlaceCode(), "EXCH")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getSecurityExchangeCode(), TAG_SHOULD_ONLY_APPEAR_ONCE + field94B.getName());
						deliverAgainstPayment.setSecurityExchangeCode(field94B.getNarrative());
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field98A) {
					Field98A field98A = (Field98A) block.getField();
					if (Objects.equals(field98A.getQualifier(), "SETT") && !StringUtils.isEmpty(field98A.getDate())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getSettlementDate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field98A.getName());
						Date settlementDate = DateUtils.toDate(field98A.getDate(), DateUtils.FIX_DATE_FORMAT_INPUT);
						deliverAgainstPayment.setSettlementDate(settlementDate);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field98A) {
					Field98A field98A = (Field98A) block.getField();
					if (Objects.equals(field98A.getQualifier(), "TRAD") && !StringUtils.isEmpty(field98A.getDate())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getTradeDate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field98A.getName());
						Date tradeDate = DateUtils.toDate(field98A.getDate(), DateUtils.FIX_DATE_FORMAT_INPUT);
						deliverAgainstPayment.setTradeDate(tradeDate);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field90B) {
					Field90B field90B = (Field90B) block.getField();
					if (Objects.equals(field90B.getQualifier(), "DEAL")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getPriceType(), TAG_SHOULD_ONLY_APPEAR_ONCE + field90B.getName());
						TradeMessagePriceTypes type = TradeMessagePriceTypes.findByCode(field90B.getAmountTypeCode());
						deliverAgainstPayment.setPriceType(type);
						deliverAgainstPayment.setSecurityCurrency(field90B.getCurrency());
						deliverAgainstPayment.setPrice(new BigDecimal(field90B.getPriceAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field90A) {
					Field90A field90A = (Field90A) block.getField();
					if (Objects.equals(field90A.getQualifier(), "DEAL")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getPriceType(), TAG_SHOULD_ONLY_APPEAR_ONCE + field90A.getName());
						TradeMessagePriceTypes type = TradeMessagePriceTypes.findByCode(field90A.getPercentageTypeCode());
						deliverAgainstPayment.setPriceType(type);
						deliverAgainstPayment.setPrice(new BigDecimal(field90A.getPriceAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field35B) {
					Field35B field35B = (Field35B) block.getField();
					if (field35B.getComponents().contains("ISIN")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getIsin(), TAG_SHOULD_ONLY_APPEAR_ONCE + field35B.getName());
						deliverAgainstPayment.setIsin(field35B.getISIN());
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field35B) {
					Field35B field35B = (Field35B) block.getField();
					Optional<String> symbol = field35B.getComponents().stream().filter(Objects::nonNull).filter(s -> s.startsWith("/TS/")).findFirst();
					if (symbol.isPresent()) {
						ValidationUtils.assertNull(deliverAgainstPayment.getSecuritySymbol(), TAG_SHOULD_ONLY_APPEAR_ONCE + field35B.getName());
						String securitySymbol = symbol.get().substring("/TS/".length());
						deliverAgainstPayment.setSecuritySymbol(securitySymbol);
						Optional<String> description = field35B.getComponents().stream().filter(Objects::nonNull).filter(s -> !s.startsWith("/TS/")).findFirst();
						deliverAgainstPayment.setSecurityDescription(description.orElse(null));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field12A) {
					Field12A field12A = (Field12A) block.getField();
					if (Objects.equals("CLAS", field12A.getComponent1()) && Objects.equals("ISIT", field12A.getDataSourceScheme())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getIsitcCode(), TAG_SHOULD_ONLY_APPEAR_ONCE + field12A.getName());
						deliverAgainstPayment.setIsitcCode(Arrays.stream(ISITCCodes.values()).filter(e -> Objects.equals(field12A.getInstrumentCode(), e.name())).findFirst().orElse(null));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field12B) {
					Field12B field12B = (Field12B) block.getField();
					if (Objects.equals("OPST", field12B.getQualifier())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getOptionStyle(), TAG_SHOULD_ONLY_APPEAR_ONCE + field12B.getName());
						deliverAgainstPayment.setOptionStyle(OptionStyleTypes.findByCode(field12B.getComponent3()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field12B) {
					Field12B field12B = (Field12B) block.getField();
					if (Objects.equals("OPTI", field12B.getQualifier())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getOptionType(), TAG_SHOULD_ONLY_APPEAR_ONCE + field12B.getName());
						String optionType = field12B.getComponent3();
						ValidationUtils.assertNotEmpty(optionType, "Options Type cannot be empty for field " + field12B.getName() + " value " + field12B.getValueDisplay());
						ValidationUtils.assertTrue(Arrays.asList("PUTO", "CALL").contains(optionType), "Option type should be 'PUTO' or 'CALL' but was " + optionType);
						deliverAgainstPayment.setOptionType("PUTO".equalsIgnoreCase(optionType) ? "PUT" : "CALL");
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field11A) {
					Field11A field11A = (Field11A) block.getField();
					if (Objects.equals(field11A.getQualifier(), "DENO")) {
						deliverAgainstPayment.setSecurityCurrency(field11A.getCurrency());
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field98A) {
					Field98A field98A = (Field98A) block.getField();
					if (Objects.equals(field98A.getQualifier(), "EXPI") && !StringUtils.isEmpty(field98A.getDate())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getSecurityExpirationDate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field98A.getName());
						Date expireDate = DateUtils.toDate(field98A.getDate(), DateUtils.FIX_DATE_FORMAT_INPUT);
						deliverAgainstPayment.setSecurityExpirationDate(expireDate);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field98A) {
					Field98A field98A = (Field98A) block.getField();
					if (Objects.equals(field98A.getQualifier(), "MATU") && !StringUtils.isEmpty(field98A.getDate())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getMaturityDate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field98A.getName());
						Date matureDate = DateUtils.toDate(field98A.getDate(), DateUtils.FIX_DATE_FORMAT_INPUT);
						deliverAgainstPayment.setMaturityDate(matureDate);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field98A) {
					Field98A field98A = (Field98A) block.getField();
					if (Objects.equals(field98A.getQualifier(), "TERM") && !StringUtils.isEmpty(field98A.getDate())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getRepoClosingDate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field98A.getName());
						if (!StringUtils.isEqual("OPEN", field98A.getDate())) {
							Date closingDate = DateUtils.toDate(field98A.getDate(), DateUtils.FIX_DATE_FORMAT_INPUT);
							deliverAgainstPayment.setRepoClosingDate(closingDate);
						}
						deliverAgainstPayment.setRepurchaseAgreement(true);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field22F) {
					Field22F field22F = (Field22F) block.getField();
					if (Objects.equals(field22F.getQualifier(), "RERT")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getRepoRateType(), TAG_SHOULD_ONLY_APPEAR_ONCE + field22F.getName());
						deliverAgainstPayment.setRepoRateType(RepoRateTypes.findByCode(field22F.getIndicator()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field92A) {
					Field92A field92A = (Field92A) block.getField();
					if (Objects.equals(field92A.getQualifier(), "REPO")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getRepoInterestRate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field92A.getName());
						deliverAgainstPayment.setRepoInterestRate(new BigDecimal(field92A.getRateAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field92A) {
					Field92A field92A = (Field92A) block.getField();
					if (Objects.equals(field92A.getQualifier(), "SHAI")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getRepoHaircutPercentage(), TAG_SHOULD_ONLY_APPEAR_ONCE + field92A.getName());
						deliverAgainstPayment.setRepoHaircutPercentage(new BigDecimal(field92A.getRateAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field19A) {
					Field19A field19A = (Field19A) block.getField();
					if (Objects.equals(field19A.getQualifier(), "TRTE")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getRepoNetCashAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field19A.getName());
						deliverAgainstPayment.setSecurityCurrency(field19A.getCurrency());
						deliverAgainstPayment.setRepoNetCashAmount(new BigDecimal(field19A.getAmountAsNumber().toString()));
						deliverAgainstPayment.setRepurchaseAgreement(true);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field19A && StringUtils.isEqual(block.getBlock(), MT541.SequenceD.START_END_16RS)) {
					Field19A field19A = (Field19A) block.getField();
					if (Objects.equals(field19A.getQualifier(), "ACRU") && Objects.nonNull(field19A.getAmountAsNumber())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getRepoAccruedInterestAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field19A.getName());
						deliverAgainstPayment.setSecurityCurrency(field19A.getCurrency());
						deliverAgainstPayment.setRepoAccruedInterestAmount(new BigDecimal(field19A.getAmountAsNumber().toString()));
						deliverAgainstPayment.setRepurchaseAgreement(true);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field98A) {
					Field98A field98A = (Field98A) block.getField();
					if (Objects.equals(field98A.getQualifier(), "ISSU") && !StringUtils.isEmpty(field98A.getDate())) {
						ValidationUtils.assertNull(deliverAgainstPayment.getIssueDate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field98A.getName());
						Date issueDate = DateUtils.toDate(field98A.getDate(), DateUtils.FIX_DATE_FORMAT_INPUT);
						deliverAgainstPayment.setIssueDate(issueDate);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field92A) {
					Field92A field92A = (Field92A) block.getField();
					if (Objects.equals(field92A.getQualifier(), "INTR")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getInterestRate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field92A.getName());
						deliverAgainstPayment.setInterestRate(new BigDecimal(field92A.getRateAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field90B) {
					Field90B field90B = (Field90B) block.getField();
					if (Objects.equals(field90B.getQualifier(), "EXER")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getStrikePrice(), TAG_SHOULD_ONLY_APPEAR_ONCE + field90B.getName());
						deliverAgainstPayment.setStrikePrice(new BigDecimal(field90B.getPriceAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field36B) {
					Field36B field36B = (Field36B) block.getField();
					if (Objects.equals(field36B.getQualifier(), "SIZE") && Objects.equals(field36B.getQuantityTypeCode(), "UNIT")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getPriceMultiplier(), TAG_SHOULD_ONLY_APPEAR_ONCE + field36B.getName());
						deliverAgainstPayment.setPriceMultiplier(new BigDecimal(field36B.getQuantityAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field22F) {
					Field22F field22F = (Field22F) block.getField();
					if (Objects.equals(field22F.getQualifier(), "PROC")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getOpen(), TAG_SHOULD_ONLY_APPEAR_ONCE + field22F.getName());
						boolean isOpen = Objects.equals("OPEP", field22F.getIndicator());
						deliverAgainstPayment.setOpen(isOpen);
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field36B) {
					Field36B field36B = (Field36B) block.getField();
					if (Objects.equals(field36B.getQualifier(), "SETT") && Objects.equals(field36B.getQuantityTypeCode(), "UNIT")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getQuantity(), TAG_SHOULD_ONLY_APPEAR_ONCE + field36B.getName());
						deliverAgainstPayment.setQuantity(new BigDecimal(field36B.getQuantityAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field36B) {
					Field36B field36B = (Field36B) block.getField();
					if (Objects.equals(field36B.getQualifier(), "SETT") && Objects.equals(field36B.getQuantityTypeCode(), "AMOR")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getAmortizedFaceAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field36B.getName());
						deliverAgainstPayment.setAmortizedFaceAmount(new BigDecimal(field36B.getQuantityAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field36B) {
					Field36B field36B = (Field36B) block.getField();
					if (Objects.equals(field36B.getQualifier(), "SETT") && Objects.equals(field36B.getQuantityTypeCode(), "FAMT")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getOriginalFaceAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field36B.getName());
						deliverAgainstPayment.setOriginalFaceAmount(new BigDecimal(field36B.getQuantityAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field97A && StringUtils.isEqual(block.getBlock(), MT541.SequenceC.START_END_16RS)) {
					Field97A field97A = (Field97A) block.getField();
					if (Objects.equals(field97A.getQualifier(), "SAFE")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getCustodyAccountNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field97A.getName());
						deliverAgainstPayment.setCustodyAccountNumber(field97A.getAccount());
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field97A && StringUtils.isEqual(block.getBlock(), MT541.SequenceE.START_END_16RS)) {
					Field97A field97A = (Field97A) block.getField();
					// duplicate group and field on REPOs
					if (Objects.equals(field97A.getQualifier(), "SAFE")) {
						if (Objects.isNull(deliverAgainstPayment.getClearingSafekeepingAccount())) {
							deliverAgainstPayment.setClearingSafekeepingAccount(field97A.getAccount());
						}
						deliverAgainstPayment.setSafekeepingAccount(field97A.getAccount());
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field95Q) {
					Field95Q field95Q = (Field95Q) block.getField();
					if (Objects.equals(field95Q.getValue(), ":PSET//XX")) {
						deliverAgainstPayment.setPlaceOfSettlement("XX");
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field95P) {
					Field95P field95P = (Field95P) block.getField();
					if (Objects.equals(field95P.getQualifier(), "PSET")) {
						deliverAgainstPayment.setPlaceOfSettlement(field95P.getBIC());
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field95P) {
					Field95P field95P = (Field95P) block.getField();
					if (Objects.equals(field95P.getQualifier(), "DEAG") || Objects.equals(field95P.getQualifier(), "REAG")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getClearingBrokerIdentifier(), TAG_SHOULD_ONLY_APPEAR_ONCE + field95P.getName());
						deliverAgainstPayment.setBuy(Objects.equals(field95P.getQualifier(), "DEAG"));
						deliverAgainstPayment.setClearingBrokerIdentifier(new PartyIdentifier(field95P.getBIC(), MessagePartyIdentifierTypes.BIC));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field95P) {
					Field95P field95P = (Field95P) block.getField();
					if (Objects.equals(field95P.getQualifier(), "SELL") || Objects.equals(field95P.getQualifier(), "BUYR")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getExecutingBrokerIdentifier(), TAG_SHOULD_ONLY_APPEAR_ONCE + field95P.getName());
						deliverAgainstPayment.setBuy(Objects.equals(field95P.getQualifier(), "DEAG"));
						deliverAgainstPayment.setExecutingBrokerIdentifier(new PartyIdentifier(field95P.getBIC(), MessagePartyIdentifierTypes.BIC));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field95R) {
					Field95R field95R = (Field95R) block.getField();
					if (Objects.equals(field95R.getQualifier(), "DEAG") || Objects.equals(field95R.getQualifier(), "REAG")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getClearingBrokerIdentifier(), TAG_SHOULD_ONLY_APPEAR_ONCE + field95R.getName());
						deliverAgainstPayment.setBuy(Objects.equals(field95R.getQualifier(), "DEAG"));
						deliverAgainstPayment.setClearingBrokerIdentifier(new PartyIdentifier(field95R.getProprietaryCode(),
								MessagePartyIdentifierTypes.findByCode(field95R.getDataSourceScheme())));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field95R) {
					Field95R field95R = (Field95R) block.getField();
					if (Objects.equals(field95R.getQualifier(), "SELL") || Objects.equals(field95R.getQualifier(), "BUYR")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getExecutingBrokerIdentifier(), TAG_SHOULD_ONLY_APPEAR_ONCE + field95R.getName());
						deliverAgainstPayment.setBuy(Objects.equals(field95R.getQualifier(), "DEAG"));
						deliverAgainstPayment.setExecutingBrokerIdentifier(new PartyIdentifier(field95R.getProprietaryCode(), MessagePartyIdentifierTypes.DTC));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field19A) {
					Field19A field19A = (Field19A) block.getField();
					if (Objects.equals(field19A.getQualifier(), "SETT")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getNetSettlementAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field19A.getName());
						deliverAgainstPayment.setSecurityCurrency(field19A.getCurrency());
						deliverAgainstPayment.setNetSettlementAmount(new BigDecimal(field19A.getAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field19A) {
					Field19A field19A = (Field19A) block.getField();
					if (Objects.equals(field19A.getQualifier(), "DEAL")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getDealAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field19A.getName());
						deliverAgainstPayment.setSecurityCurrency(field19A.getCurrency());
						deliverAgainstPayment.setDealAmount(new BigDecimal(field19A.getAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field19A) {
					Field19A field19A = (Field19A) block.getField();
					if (Objects.equals(field19A.getQualifier(), "EXEC")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getCommissionAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field19A.getName());
						deliverAgainstPayment.setSecurityCurrency(field19A.getCurrency());
						deliverAgainstPayment.setCommissionAmount(new BigDecimal(field19A.getAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field19A) {
					Field19A field19A = (Field19A) block.getField();
					if (Objects.equals(field19A.getQualifier(), "REGF")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getRegulatoryAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field19A.getName());
						deliverAgainstPayment.setSecurityCurrency(field19A.getCurrency());
						deliverAgainstPayment.setRegulatoryAmount(new BigDecimal(field19A.getAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field19A && StringUtils.isEqual(block.getBlock(), MT541.SequenceE.START_END_16RS)) {
					Field19A field19A = (Field19A) block.getField();
					if (Objects.equals(field19A.getQualifier(), "ACRU")) {
						ValidationUtils.assertNull(deliverAgainstPayment.getAccruedInterest(), TAG_SHOULD_ONLY_APPEAR_ONCE + field19A.getName());
						deliverAgainstPayment.setSecurityCurrency(field19A.getCurrency());
						deliverAgainstPayment.setAccruedInterest(new BigDecimal(field19A.getAmountAsNumber().toString()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field22F) {
					Field22F field22F = (Field22F) block.getField();
					if (Objects.equals(field22F.getQualifier(), "SETR")) {
						deliverAgainstPayment.setSettlementTransactionType(SettlementTransactionTypes.findByCode(field22F.getIndicator()));
					}
				}
			},
			(block, deliverAgainstPayment) -> {
				if (block.getField() instanceof Field22F) {
					Field22F field22F = (Field22F) block.getField();
					if (Objects.equals(field22F.getQualifier(), "COLA")) {
						deliverAgainstPayment.setExposureType(ExposureTypes.findByCode(field22F.getIndicator()));
					}
				}
			}
	);


	private AgainstPaymentMessageBuilder() {
		// static builder class.
	}


	public static <M extends AbstractMT, E extends AbstractTradeMessage> void buildSwiftMessage(M message, E entity, AgainstPaymentBlockProvider blockProvider) {
		// Set sender and receiver BIC codes
		message.setSender(entity.getSenderBIC());
		message.setReceiver(entity.getReceiverBIC());

		// GENL: General Information
		message.append(
				SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceA())
						.withBlock(
								() -> SwiftMessageBlockBuilder.createMutableBlock()
										// reference
										.withField(() -> new Field20C().setQualifier("SEME").setReference(entity.getTransactionReferenceNumber()))
										// function
										.withField(() -> new Field23G().setFunction(entity.getMessageFunctions().getCode()))
										// A1 only applies to cancels
										.withBlock(
												() -> entity.getMessageFunctions() == MessageFunctions.CANCEL, () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceA1())
														.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
																.withField(() -> new Field20C().setQualifier("PREV").setReference(entity.getMessageReferenceNumber().toString()))
																.toBlock())
														.toBlock()
										)
										.toBlock()
						)
						.toBlock()
		);

		// TRADDET:  Trade Details
		message.append(
				SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceB())
						.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
								// Place -  Place of Trade.
								.withField(() -> Objects.nonNull(entity.getSecurityExchangeCode()), () -> new Field94B().setQualifier("TRAD").setPlaceCode("EXCH").setNarrative(entity.getSecurityExchangeCode()))
								// Date/Time - Settlement Date/Time
								.withField(() -> Objects.nonNull(entity.getSettlementDate()), () -> new Field98A().setQualifier("SETT").setDate(DateUtils.fromDate(entity.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT)))
								// Date/Time - Trade Date/Time
								.withField(() -> Objects.nonNull(entity.getTradeDate()), () -> new Field98A().setQualifier("TRAD").setDate(DateUtils.fromDate(entity.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT)))
								// Price - Deal Price - all price types accept percent.
								.withField(() -> Objects.nonNull(entity.getPrice()) && entity.getPriceType() != TradeMessagePriceTypes.PERCENTAGE, () -> new Field90B().setQualifier("DEAL").setAmountTypeCode(entity.getPriceType().getCode()).setCurrency(entity.getSecurityCurrency()).setPrice(entity.getPrice()))
								// Price - Deal Price - percent price type.
								.withField(() -> Objects.nonNull(entity.getPrice()) && entity.getPriceType() == TradeMessagePriceTypes.PERCENTAGE, () -> new Field90A().setQualifier("DEAL").setPercentageTypeCode(entity.getPriceType().getCode()).setPrice(entity.getPrice()))
								// ISIN - International Securities Identification Number
								.withField(() -> Objects.nonNull(entity.getIsin()), () -> new Field35B().setComponent1("ISIN").setISIN(entity.getIsin()))
								//  Identification of the Financial Instrument - Ticker
								.withField(() -> Objects.nonNull(entity.getSecuritySymbol()), () -> new Field35B().setComponent1("/TS/" + entity.getSecuritySymbol())
										.setDescription(StringUtils.isEmpty(entity.getSecurityDescription()) ? null : StringUtils.left(SwiftUtils.removeInvalidMessageCharacters(entity.getSecurityDescription()), 35)))
								// FIA:  Financial Instrument Attributes
								.withBlock(() -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceB1())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												// Type of Financial Instrument - Classification Type
												.withField(() -> Objects.nonNull(entity.getIsitcCode()), () -> new Field12A().setComponent1("CLAS").setDataSourceScheme("ISIT").setInstrumentCode(entity.getIsitcCode().name()))
												// Options - Option Style
												.withField(() -> Objects.nonNull(entity.getOptionStyle()), () -> new Field12B().setComponent1("OPST").setComponent3(entity.getOptionStyle().getCode()))
												// Options - Option Type
												.withField(() -> Objects.nonNull(entity.getOptionType()), () -> new Field12B().setComponent1("OPTI").setComponent3("PUT".equalsIgnoreCase(entity.getOptionType()) ? "PUTO" : "CALL"))
												// Currency - Currency of Denomination
												.withField(() -> Objects.nonNull(entity.getSecurityCurrency()), () -> new Field11A().setQualifier("DENO").setCurrency(entity.getSecurityCurrency()))
												// Date - Expiry Date
												.withField(() -> Objects.nonNull(entity.getSecurityExpirationDate()), () -> new Field98A().setQualifier("EXPI").setDate(DateUtils.fromDate(entity.getSecurityExpirationDate(), DateUtils.FIX_DATE_FORMAT_INPUT)))
												// Date - Maturity Date
												.withField(() -> Objects.nonNull(entity.getMaturityDate()), () -> new Field98A().setQualifier("MATU").setDate(DateUtils.fromDate(entity.getMaturityDate(), DateUtils.FIX_DATE_FORMAT_INPUT)))
												// Date - Issue Date
												.withField(() -> Objects.nonNull(entity.getIssueDate()), () -> new Field98A().setQualifier("ISSU").setDate(DateUtils.fromDate(entity.getIssueDate(), DateUtils.FIX_DATE_FORMAT_INPUT)))
												// Rate - Interest Rate
												.withField(() -> Objects.nonNull(entity.getInterestRate()), () -> new Field92A().setQualifier("INTR").setRateAmount(entity.getInterestRate()))
												// Options - exercise or strike price
												.withField(() -> Objects.nonNull(entity.getStrikePrice()), () -> new Field90B().setQualifier("EXER").setAmountTypeCode("ACTU").setCurrency(entity.getSecurityCurrency()).setPrice(entity.getStrikePrice()))
												// Quantity of Financial Instrument - Contract Size
												.withField(() -> Objects.nonNull(entity.getPriceMultiplier()), () -> new Field36B().setQualifier("SIZE").setQuantityTypeCode("UNIT").setQuantity(entity.getPriceMultiplier()))
												.toBlock())
										.toBlock()
								)
								// Indicator - Processing Indicator - Open / Close Position.
								.withField(() -> Objects.nonNull(entity.getOpen()), () -> new Field22F().setQualifier("PROC").setIndicator(entity.getOpen() ? "OPEP" : "CLOP"))
								.toBlock()
						)
						.toBlock()
		);

		// FIAC:  Financial Instrument/Account
		message.append(
				SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceC())
						.withBlock(
								() -> SwiftMessageBlockBuilder.createMutableBlock()
										// Quantity of Financial Instrument - Quantity expressed as a number, for example, a number of shares.
										.withField(() -> Objects.nonNull(entity.getQuantity()), () -> new Field36B().setQualifier("SETT").setQuantityTypeCode("UNIT").setQuantity(entity.getQuantity()))
										// bonds - amortized face amount
										.withField(() -> Objects.nonNull(entity.getAmortizedFaceAmount()), () -> new Field36B().setQualifier("SETT").setQuantityTypeCode("AMOR").setQuantity(entity.getAmortizedFaceAmount()))
										// bonds - face amount.
										.withField(() -> Objects.nonNull(entity.getOriginalFaceAmount()), () -> new Field36B().setQualifier("SETT").setQuantityTypeCode("FAMT").setQuantity(entity.getOriginalFaceAmount()))
										.withField(() -> Objects.nonNull(entity.getCustodyAccountNumber()), () -> new Field97A().setQualifier("SAFE").setAccount(entity.getCustodyAccountNumber()))
										.toBlock()
						)
						.toBlock()
		);

		// REPO:  Repo instrument
		if (entity.isRepurchaseAgreement()) {
			message.append(
					SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceD())
							.withBlock(
									() -> SwiftMessageBlockBuilder.createMutableBlock()
											// Date - Closing Date
											.withField(() -> Objects.isNull(entity.getRepoClosingDate()) ? new Field98A().setQualifier("TERM").setDate("OPEN") : new Field98A().setQualifier("TERM").setDate(DateUtils.fromDate(entity.getRepoClosingDate(), DateUtils.FIX_DATE_FORMAT_INPUT)))
											// Bond Interest Rate Type
											.withField(() -> Objects.nonNull(entity.getRepoRateType()), () -> new Field22F().setQualifier("RERT").setIndicator(entity.getRepoRateType().getCode()))
											// REPO interest rate
											.withField(() -> Objects.nonNull(entity.getRepoInterestRate()), () -> new Field92A().setQualifier("REPO").setRateAmount(entity.getRepoInterestRate()))
											// REPO Securities haircut
											.withField(() -> Objects.nonNull(entity.getRepoHaircutPercentage()), () -> new Field92A().setQualifier("SHAI").setRateAmount(entity.getRepoHaircutPercentage()))
											// Total number of collateral instructions involved in the transaction
											.withField(() -> new Field99B().setQualifier("TOCO").setNumber("001"))
											// Re-purchase Amount
											.withField(() -> Objects.nonNull(entity.getRepoNetCashAmount()), () -> new Field19A().setQualifier("TRTE").setCurrency(entity.getSecurityCurrency()).setAmount(MathUtils.abs(entity.getRepoNetCashAmount())))
											// Accrued Interest Amount
											.withField(() -> Objects.nonNull(entity.getRepoAccruedInterestAmount()), () -> new Field19A().setQualifier("ACRU").setCurrency(entity.getSecurityCurrency()).setAmount(MathUtils.abs(entity.getRepoAccruedInterestAmount())))
											.toBlock()
							)
							.toBlock()
			);
		}

		// SETDET:  Settlement Details
		message.append(
				SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE())
						.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
								// Type of Settlement Transaction Indicator
								.withField(() -> Objects.nonNull(entity.getSettlementTransactionType()), () -> new Field22F().setQualifier("SETR").setIndicator(entity.getSettlementTransactionType().getCode()))
								// Exposure Type Indicator
								.withField(() -> Objects.nonNull(entity.getExposureType()), () -> new Field22F().setQualifier("COLA").setIndicator(entity.getExposureType().getCode()))
								// SETPRTY:  Settlement Parties
								.withBlock(() -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE1())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												.withTag(() -> StringUtils.isEqual("XX", entity.getPlaceOfSettlement()), () -> Field95Q.tag(":PSET//XX"))
												.withField(() -> !StringUtils.isEqual("XX", entity.getPlaceOfSettlement()) && Objects.nonNull(entity.getPlaceOfSettlement()), () -> new Field95P().setQualifier("PSET").setBIC(entity.getPlaceOfSettlement()))
												.toBlock())
										.toBlock()
								)
								// Clearing Broker - Qualifier, Identifier Code (CLEARING AGENT)
								.withBlock(() -> Objects.nonNull(entity.getClearingBrokerIdentifier()), () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE1())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												.withField(() -> entity.getClearingBrokerIdentifier().getType() == MessagePartyIdentifierTypes.BIC, () -> new Field95P().setQualifier(entity.isBuy() ? "DEAG" : "REAG").setBIC(entity.getClearingBrokerIdentifier().getIdentifier()))
												.withField(() -> entity.getClearingBrokerIdentifier().getType() == MessagePartyIdentifierTypes.DTC, () -> new Field95R().setQualifier(entity.isBuy() ? "DEAG" : "REAG").setDataSourceScheme(MessagePartyIdentifierTypes.DTC.getCode()).setProprietaryCode(entity.getClearingBrokerIdentifier().getIdentifier()))
												.withField(() -> entity.getClearingBrokerIdentifier().getType() == MessagePartyIdentifierTypes.USFW, () -> new Field95R().setQualifier(entity.isBuy() ? "DEAG" : "REAG").setDataSourceScheme(MessagePartyIdentifierTypes.USFW.getCode()).setProprietaryCode(entity.getClearingBrokerIdentifier().getIdentifier()))
												.withField(() -> Objects.nonNull(entity.getClearingSafekeepingAccount()), () -> new Field97A().setQualifier("SAFE").setAccount(entity.getClearingSafekeepingAccount()))
												.toBlock())
										.toBlock()
								)
								// Executing Broker - Qualifier, Identifier Code (EXECUTING BROKER)
								.withBlock(() -> Objects.nonNull(entity.getExecutingBrokerIdentifier()), () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE1())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												.withField(() -> entity.getExecutingBrokerIdentifier().getType() == MessagePartyIdentifierTypes.BIC, () -> new Field95P().setQualifier(entity.isBuy() ? "SELL" : "BUYR").setBIC(entity.getExecutingBrokerIdentifier().getIdentifier()))
												.withField(() -> entity.getExecutingBrokerIdentifier().getType() == MessagePartyIdentifierTypes.DTC, () -> new Field95R().setQualifier(entity.isBuy() ? "SELL" : "BUYR").setDataSourceScheme(MessagePartyIdentifierTypes.DTC.getCode()).setProprietaryCode(entity.getExecutingBrokerIdentifier().getIdentifier()))
												.withField(() -> Objects.nonNull(entity.getSafekeepingAccount()), () -> new Field97A().setQualifier("SAFE").setAccount(entity.getSafekeepingAccount()))
												.toBlock())
										.toBlock()
								)
								// Settlement Amount
								.withBlock(() -> Objects.nonNull(entity.getNetSettlementAmount()), () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE3())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												.withField(() -> new Field19A().setQualifier("SETT").setCurrency(entity.getSecurityCurrency()).setAmount(MathUtils.abs(entity.getNetSettlementAmount())))
												.toBlock())
										.toBlock()
								)
								// Trade Amount
								.withBlock(() -> Objects.nonNull(entity.getDealAmount()), () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE3())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												// Trade Amount:  Deal amount is O for futures
												.withField(() -> new Field19A().setQualifier("DEAL").setCurrency(entity.getSecurityCurrency()).setAmount(MathUtils.abs(entity.getDealAmount())))
												.toBlock())
										.toBlock()
								)
								// Accrued interest amount
								.withBlock(() -> Objects.nonNull(entity.getAccruedInterest()), () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE3())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												// Trade Amount:  Deal amount is O for futures
												.withField(() -> new Field19A().setQualifier("ACRU").setCurrency(entity.getSecurityCurrency()).setAmount(MathUtils.abs(entity.getAccruedInterest())))
												.toBlock())
										.toBlock()
								)
								// Executing Broker's Amount
								.withBlock(() -> Objects.nonNull(entity.getCommissionAmount()), () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE3())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												.withField(() -> new Field19A().setQualifier("EXEC").setCurrency(entity.getSecurityCurrency()).setAmount(MathUtils.abs(entity.getCommissionAmount())))
												.toBlock())
										.toBlock()
								)
								// Regulatory Amount
								.withBlock(() -> Objects.nonNull(entity.getRegulatoryAmount()), () -> SwiftMessageBlockBuilder.createImmutableBlockFromBlocks(blockProvider.getSequenceE3())
										.withBlock(() -> SwiftMessageBlockBuilder.createMutableBlock()
												.withField(() -> new Field19A().setQualifier("REGF").setCurrency(entity.getSecurityCurrency()).setAmount(MathUtils.abs(entity.getRegulatoryAmount())))
												.toBlock())
										.toBlock()
								)
								.toBlock()
						)
						.toBlock()
		);
	}
}
