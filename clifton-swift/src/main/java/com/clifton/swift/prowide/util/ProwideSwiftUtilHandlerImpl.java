package com.clifton.swift.prowide.util;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.util.SwiftUtilHandler;
import com.prowidesoftware.swift.io.printout.PrintoutWriter;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.SwiftBlock4;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.UnparsedTextList;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.field.Field20C;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt0xx.MTUnknownMessage;
import com.prowidesoftware.swift.model.mt.mt2xx.MT292;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;


/**
 * {@inheritDoc}
 */
@Component("swiftUtilHandler")
public class ProwideSwiftUtilHandlerImpl implements SwiftUtilHandler<AbstractMessage> {

	private SwiftMessageConverterHandler<AbstractMessage> swiftMessageConverterHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AbstractMessage parseSingleMessage(String messageString, SwiftMessageFormats messageFormat) {
		return CollectionUtils.getFirstElement(parseMessage(messageString, messageFormat));
	}


	@Override
	public List<AbstractMessage> parseMessage(String messageString, SwiftMessageFormats messageFormat) {
		AssertUtils.assertNotEmpty(messageString, "Swift Message Text cannot be empty.");
		AssertUtils.assertNotNull(messageFormat, "Swift Message Format cannot be null.");
		return new ArrayList<>(getSwiftMessageConverterHandler().parse(messageString, messageFormat).values());
	}


	@Override
	public String getMessageText(AbstractMessage message) {
		if (message.isMT()) {
			AbstractMT abstractMt = (AbstractMT) message;
			return abstractMt.message();
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	@Override
	public String getFormattedMessageText(String messageText, SwiftMessageFormats messageFormat) {
		if (messageFormat == SwiftMessageFormats.MT) {
			PrintoutWriter printoutWriter = new PrintoutWriter();
			AbstractMT abstractMt = (AbstractMT) parseSingleMessage(messageText, messageFormat);
			return printoutWriter.print(abstractMt);
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	@Override
	public void replaceSwiftMessageTransactionReferenceNumber(AbstractMessage message, SwiftIdentifier swiftIdentifier) {
		ValidationUtils.assertNotNull(message, "Message cannot be null");
		ValidationUtils.assertNotNull(swiftIdentifier, "Swift Identifier cannot be null");

		if (message.isMT()) {
			SwiftMessage swiftMessage = ((AbstractMT) message).getSwiftMessage();
			if (swiftMessage != null && swiftMessage.getBlock4() != null) {
				SwiftBlock4 swiftBlock4 = swiftMessage.getBlock4();
				if (swiftBlock4.containsField(Field20.NAME)) {
					Tag tag20 = swiftBlock4.getTagByName(Field20.NAME);
					tag20.setValue(Long.toString(swiftIdentifier.getId()));
					AssertUtils.assertTrue(StringUtils.isEqual(Long.toString(swiftIdentifier.getId())
							, swiftBlock4.getFieldByName(Field20.NAME).getComponent(1))
							, () -> String.format("The Swift Identifier fields should have been changed to %s but is %s"
									, swiftIdentifier.getId()
									, swiftBlock4.getFieldByName(Field20.NAME).getComponent(1)));
				}
				else if (swiftBlock4.containsField(Field20C.NAME)) {
					Tag tag20C = swiftBlock4.getTagByName(Field20C.NAME);
					String replacementTag20C = "";
					if (StringUtils.isEqual(tag20C.asField().getComponent(1), "SEME")) {
						replacementTag20C += ":SEME//";
					}
					replacementTag20C += Long.toString(swiftIdentifier.getId());
					tag20C.setValue(replacementTag20C);
					AssertUtils.assertTrue(StringUtils.isEqual(Long.toString(swiftIdentifier.getId())
							, swiftBlock4.getFieldByName(Field20C.NAME).getComponent(2))
							, () -> String.format("The Swift Identifier fields should have been changed to %s but is %s"
									, swiftIdentifier.getId()
									, swiftBlock4.getFieldByName(Field20C.NAME).getComponent(2)));
				}
				else {
					throw new RuntimeException("Could not replace the Swift Message Transaction Reference Number, the message does not have a block 20 or 20C.");
				}
			}
			else {
				throw new RuntimeException("Could not replace the Swift Message Transaction Reference Number, the message does not have a block 4.");
			}
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	@Override
	public String getMessageTypeCode(AbstractMessage message) {
		if (message.isMT()) {
			AbstractMT abstractMt = (AbstractMT) message;
			String messageType = abstractMt.getMessageType();
			if (StringUtils.isEmpty(messageType) || abstractMt.getSwiftMessage().isServiceMessage21()) {
				return "";
			}
			return "MT" + messageType;
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	@Override
	public String getTransactionReferenceNumber(AbstractMessage message) {
		ValidationUtils.assertNotNull(message, "Message cannot be null");
		if (!message.isMT()) {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
		// fields must be fetched in order.
		final Map<String, List<Field>> fieldMap = Stream.of(message)
				.filter(AbstractMessage::isMT)
				.filter(AbstractMT.class::isInstance)
				.map(AbstractMT.class::cast)
				.map(AbstractMT::getSwiftMessage)
				.filter(Objects::nonNull)
				.map(SwiftMessage::getBlock4)
				.filter(Objects::nonNull)
				.flatMap(b -> Stream.concat(Arrays.stream(b.getFieldsByName(Field20.NAME)), Arrays.stream(b.getFieldsByName(Field20C.NAME))))
				.filter(f -> !(f instanceof Field20C) || Objects.equals(((Field20C) f).getQualifier(), "SEME"))
				.collect(groupingBy(Field::getName));

		if (fieldMap.containsKey(Field20.NAME)) {
			// MT-292 has Field 20 twice, only get the first one.
			String messageNumber = ((AbstractMT) message).getSwiftMessage().getType();
			AssertUtils.assertTrue(fieldMap.get(Field20.NAME).size() <= 1 || StringUtils.isEqual(MT292.NAME, messageNumber), () -> "The message must contain only one Field20 " + fieldMap.get(Field20.NAME).size());
			return fieldMap.get(Field20.NAME).stream().map(Field20.class::cast).map(Field20::getReference).findFirst().orElse(null);
		}
		else if (fieldMap.containsKey(Field20C.NAME)) {
			AssertUtils.assertTrue(fieldMap.get(Field20C.NAME).size() <= 1, () -> "The message must contain only one Field20C with Qualifier 'SEME' "
					+ fieldMap.get(Field20C.NAME).size());
			return fieldMap.get(Field20C.NAME).stream().map(Field20C.class::cast).map(Field20C::getReference).findFirst().orElse(null);
		}
		return null;
	}


	@Override
	public Long getMessageReferenceNumber(AbstractMessage message) {
		ValidationUtils.assertNotNull(message, "Message cannot be null");
		if (!message.isMT()) {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
		List<String> references = Stream.of(message)
				.filter(AbstractMessage::isMT)
				.filter(AbstractMT.class::isInstance)
				.map(AbstractMT.class::cast)
				.map(AbstractMT::getSwiftMessage)
				.filter(Objects::nonNull)
				.map(SwiftMessage::getBlock4)
				.filter(Objects::nonNull)
				.flatMap(b -> Arrays.stream(b.getFieldsByName(Field20C.NAME)))
				.map(Field20C.class::cast)
				.filter(f -> Objects.equals(f.getQualifier(), "PREV"))
				.map(Field20C::getReference)
				.filter(s -> !StringUtils.isEmpty(s))
				.collect(Collectors.toList());
		AssertUtils.assertTrue(references.size() <= 1, "There should be only one Field20C with qualifier 'PREV' " + references);
		return references.stream().map(Long::parseLong).findFirst().orElse(null);
	}


	@Override
	public String getReceiverBusinessIdentifierCode(AbstractMessage message) {
		ValidationUtils.assertNotNull(message, "Message cannot be null");
		if (message.isMT()) {
			AbstractMT abstractMT = (AbstractMT) message;
			return abstractMT.getReceiver();
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	@Override
	public String getSenderBusinessIdentifierCode(AbstractMessage message) {
		ValidationUtils.assertNotNull(message, "Message cannot be null");
		if (message.isMT()) {
			AbstractMT abstractMT = (AbstractMT) message;
			String result = abstractMT.getSender();
			return !StringUtils.isEmpty(result) ? result.substring(0, 8) : null;
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	/**
	 * Returns true if message application id is: 21 = GPA/FIN Message (ACK/NAK/UAK/UNK)
	 */
	@Override
	public boolean isSystemAcknowledgementMessage(AbstractMessage message) {
		ValidationUtils.assertNotNull(message, "Message cannot be null");
		if (message.isMT()) {
			AbstractMT abstractMT = (AbstractMT) message;
			return abstractMT.getSwiftMessage().isServiceMessage21();
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	/**
	 * Determine if the provided message is a system message (ACK/NAK) and return true for ack and false for NAK.
	 */
	@Override
	public boolean isAcknowledged(AbstractMessage message) {
		ValidationUtils.assertNotNull(message, "Message cannot be null");
		if (message.isMT()) {
			AbstractMT abstractMT = (AbstractMT) message;
			com.prowidesoftware.swift.model.SwiftMessage swiftMessage = abstractMT.getSwiftMessage();
			if (swiftMessage != null && swiftMessage.isServiceMessage21()) {
				return swiftMessage.isAck();
			}
			return false;
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	/**
	 * For the provided acknowledgement message, get its corresponding service message.
	 * For FIN messages with a Service Identifier of 21 or 25 (system message), the sequence number is that of the acknowledged service message.
	 */
	@Override
	public AbstractMessage getAcknowledgedMessage(AbstractMessage message, List<AbstractMessage> messages) {
		AssertUtils.assertNotNull(message, "Message cannot be null.");
		AssertUtils.assertNotEmpty(messages, "Message list cannot be null.");
		AssertUtils.assertTrue(isSystemAcknowledgementMessage(message), "Message must be a system message.");

		if (message.isMT()) {
			List<AbstractMT> matches = messages
					.stream()
					.map(AbstractMT.class::cast)
					.filter(a -> !a.getSwiftMessage().isServiceMessage21())
					.collect(Collectors.toList());
			return matches.isEmpty() ? null : matches.get(0);
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	@Override
	public List<AbstractMessage> getAllMessagesList(AbstractMessage abstractMessage) {
		if (abstractMessage.isMT()) {
			List<com.prowidesoftware.swift.model.SwiftMessage> swiftMessages = new ArrayList<>();
			doGatherRecursive(((AbstractMT) abstractMessage).getSwiftMessage(), swiftMessages);
			List<AbstractMessage> results = new ArrayList<>();
			for (com.prowidesoftware.swift.model.SwiftMessage swiftMessage : swiftMessages) {
				AbstractMT abstractMT = swiftMessage.toMT();
				if (abstractMT == null) {
					abstractMT = new MTUnknownMessage(swiftMessage);
				}
				results.add(abstractMT);
			}
			return results;
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	/**
	 * Iterate through the provided message, parse un-parsed messages and return the cumulative map of messages
	 * to their corresponding acknowledgements.  Messages with no acknowledgements are returned with a 'true' value.
	 */
	@Override
	public Map<AbstractMessage, Boolean> getAllMessagesMap(AbstractMessage abstractMessage) {
		if (abstractMessage.isMT()) {
			List<AbstractMessage> messages = getAllMessagesList(abstractMessage);
			List<AbstractMessage> systemMessages = messages
					.stream()
					.map(AbstractMT.class::cast)
					.filter(a -> a.getSwiftMessage().isServiceMessage21())
					.collect(Collectors.toList());

			final Map<AbstractMessage, Boolean> results = new LinkedHashMap<>();
			for (AbstractMessage msg : systemMessages) {
				AbstractMessage serviceMsg = getAcknowledgedMessage(msg, messages);
				ValidationUtils.assertNotNull(serviceMsg, "A system message should have an associated service message.");
				messages.remove(serviceMsg);
				results.put(serviceMsg, isAcknowledged(msg));
			}
			messages
					.stream()
					.map(AbstractMT.class::cast)
					.filter(a -> !a.getSwiftMessage().isServiceMessage21())
					.filter(a -> !results.containsKey(a))
					.forEach(m -> results.put(m, Boolean.TRUE));
			return results;
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void doGatherRecursive(com.prowidesoftware.swift.model.SwiftMessage swiftMessage, List<com.prowidesoftware.swift.model.SwiftMessage> messageList) {
		if (swiftMessage == null) {
			return;
		}
		messageList.add(swiftMessage);
		UnparsedTextList unparsedTextList = swiftMessage.getUnparsedTexts();
		for (int i = 0; i < unparsedTextList.size(); i++) {
			if (unparsedTextList.isMessage(i)) {
				doGatherRecursive(unparsedTextList.getTextAsMessage(i), messageList);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageConverterHandler<AbstractMessage> getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler<AbstractMessage> swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}
}
