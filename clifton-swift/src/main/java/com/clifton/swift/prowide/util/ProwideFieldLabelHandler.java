package com.clifton.swift.prowide.util;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;


@Component("prowideFieldLabelHandler")
public class ProwideFieldLabelHandler implements InitializingBean {

	public static final ClassPathResource propertyResource = new ClassPathResource("pw_swift_labels_en.properties");

	private Properties properties;


	@Override
	public void afterPropertiesSet() throws Exception {
		this.properties = PropertiesLoaderUtils.loadProperties(propertyResource);
	}


	public String getMessageFieldLabel(String messageType, String fieldName) {
		if (!StringUtils.isEmpty(fieldName)) {
			String fieldSegment = "field" + fieldName;
			String messageSegment = StringUtils.isEmpty(messageType) ? "" : "[" + messageType + "]";
			String nameSegment = ".name";
			Object value;
			String key = fieldSegment + messageSegment + nameSegment;
			if (this.properties.containsKey(key)) {
				value = this.properties.get(key);
			}
			else {
				value = this.properties.getProperty(fieldSegment + nameSegment);
			}
			return (value == null) ? "" : value.toString();
		}
		return null;
	}


	public List<String> getMessageComponentLabels(String fieldName) {
		if (!StringUtils.isEmpty(fieldName)) {
			if (Character.isDigit(fieldName.charAt(0))) {
				Object value = this.properties.get("field" + fieldName + ".components");
				if (value != null) {
					String[] splitLabels = value.toString().split("\\\\-");
					return CollectionUtils.createList(splitLabels).stream().map(String::trim).collect(Collectors.toList());
				}
			}
		}
		return Collections.emptyList();
	}
}
