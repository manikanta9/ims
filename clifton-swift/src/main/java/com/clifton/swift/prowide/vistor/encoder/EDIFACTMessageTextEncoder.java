package com.clifton.swift.prowide.vistor.encoder;

import com.clifton.core.util.StringUtils;
import com.prowidesoftware.swift.model.field.Field77F;
import com.prowidesoftware.swift.model.mt.mt1xx.MT105;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EDIFACTMessageTextEncoder implements MessageTextEncoder {

	// regular expression for the accepted EBCDIC characters.
	public static final Pattern acceptedEDIFACTCharacterPattern = Pattern.compile("^[A-Z0-9/\\-\\?:\\(\\)\\.\\+,' \\=\\!%&\\*\\<\\>\";]+$");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean accept(String messageType, String field) {
		return MT105.NAME.equals(messageType) && Field77F.NAME.equals(field);
	}


	@Override
	public boolean hasReservedCharacters(String value) {
		Matcher acceptedMatcher = acceptedEDIFACTCharacterPattern.matcher(value);
		return !acceptedMatcher.matches();
	}


	@Override
	public String encode(String value) {
		if (!StringUtils.isEmpty(value)) {
			return value.replaceAll("[^A-Z0-9/\\-\\?:\\(\\)\\.\\+,' \\=\\!%&\\*\\<\\>\";]", StringUtils.EMPTY_STRING);
		}
		return value;
	}
}
