package com.clifton.swift.prowide.vistor.encoder;

import com.clifton.core.logging.LogUtils;
import com.prowidesoftware.swift.model.field.Field70F;
import com.prowidesoftware.swift.model.field.Field70G;
import com.prowidesoftware.swift.model.field.Field77F;
import com.prowidesoftware.swift.model.field.Field77T;
import com.prowidesoftware.swift.model.mt.mt1xx.MT103_REMIT;
import com.prowidesoftware.swift.model.mt.mt1xx.MT105;
import com.prowidesoftware.swift.model.mt.mt5xx.MT564;
import com.prowidesoftware.swift.model.mt.mt5xx.MT568;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EBCDICMessageTextEncoder implements MessageTextEncoder {

	// the encoding name for EBCDIC
	public static final String EBCDIC_CHARSET = "Cp1047";

	// the reserved escape prefix, ??
	private static final byte[] ESCAPE_SEQUENCE_PREFIX = {111, 111};

	// regular expression for the accepted EBCDIC characters.
	public static final Pattern acceptedEBCDICCharacterPattern = Pattern.compile("^[\\w/\\-\\?:\\(\\)\\.\\+,' \\r\\n]+$");

	// EBCDIC code ranges for accepted characters.
	private static final int[][] acceptedEBCDICByteRangeList = {
			new int[]{129, 137}, // a-i
			new int[]{145, 153}, // j-r
			new int[]{162, 169}, // s-z
			new int[]{193, 201}, // A-I
			new int[]{209, 217}, // J-R
			new int[]{226, 233}, // S-Z
			new int[]{240, 249}, // 0-9
			new int[]{96, 97},   // - /
			new int[]{111, 111}, // ?
			new int[]{122, 122}, // :
			new int[]{77, 78},   // ( +
			new int[]{93, 93},   // )
			new int[]{75, 75},   // .
			new int[]{107, 107}, // ,
			new int[]{125, 125}, // '
			new int[]{21, 21},   // new line \n
			new int[]{13, 13},   // carriage return \r
			new int[]{64, 64}   // space
	};

	private Map<String, List<String>> exceptionMessageTypes = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public EBCDICMessageTextEncoder() {
		this.exceptionMessageTypes.put(MT105.NAME, Collections.singletonList(Field77F.NAME));
		this.exceptionMessageTypes.put(MT568.NAME, Collections.singletonList(Field70F.NAME));
		this.exceptionMessageTypes.put(MT564.NAME, Collections.singletonList(Field70G.NAME));
		this.exceptionMessageTypes.put(MT103_REMIT.NAME, Collections.singletonList(Field77T.NAME));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean accept(String messageType, String field) {
		return !(this.exceptionMessageTypes.containsKey(messageType) && this.exceptionMessageTypes.get(messageType).contains(field));
	}


	@Override
	public boolean hasReservedCharacters(String value) {
		Matcher acceptedMatcher = acceptedEBCDICCharacterPattern.matcher(value);
		return !acceptedMatcher.matches();
	}


	@Override
	public String encode(String value) {
		// convert to EBCDIC bytes, only convert the bytes to a string when done doing replacements.
		try (ByteArrayOutputStream after = new ByteArrayOutputStream()) {
			byte[] before = value.getBytes(EBCDIC_CHARSET);
			for (byte c : before) {
				// must be unsigned
				final int code = c & 0xff;
				// check the accepted byte range and do replacement when necessary.
				if (Arrays.stream(acceptedEBCDICByteRangeList).noneMatch(r -> code >= r[0] && code <= r[1])) {
					// character's hexadecimal EBCDIC code preceded by two question marks (??) as an escape sequence
					after.write(ESCAPE_SEQUENCE_PREFIX);
					after.write(Integer.toHexString(code).toUpperCase().getBytes(EBCDIC_CHARSET));
				}
				else {
					after.write(code);
				}
			}
			return new String(after.toByteArray(), EBCDIC_CHARSET);
		}
		catch (Exception e) {
			LogUtils.error(EBCDICMessageTextEncoder.class, "Could not replace reserved SWIFT characters for value " + value, e);
			// if something goes wrong log it and return original value unchanged.
		}

		return value;
	}
}
