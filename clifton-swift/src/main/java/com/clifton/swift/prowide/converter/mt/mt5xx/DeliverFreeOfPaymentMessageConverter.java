package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.clifton.instruction.messages.transfers.DeliverFreeAgainstPayment;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.prowide.converter.mt.AbstractSwiftMtMessageConverter;
import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.mt.mt5xx.MT540;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Stream;


@Component
public class DeliverFreeOfPaymentMessageConverter extends AbstractSwiftMtMessageConverter<DeliverFreeAgainstPayment, MT540> {

	@Override
	public Class<?> getInternalMessageClass() {
		return DeliverFreeAgainstPayment.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MT540.class;
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	@Override
	public MT540 toSwiftEntity(DeliverFreeAgainstPayment entity) {
		MT540 message = new MT540();
		AgainstPaymentMessageBuilder.buildSwiftMessage(message, entity, getAgainstPaymentBlockProvider());
		return message;
	}


	@Override
	public DeliverFreeAgainstPayment fromSwiftEntity(MT540 message) {
		DeliverFreeAgainstPayment deliverFreeAgainstPayment = new DeliverFreeAgainstPayment();
		deliverFreeAgainstPayment.setSenderBIC(message.getSender());
		deliverFreeAgainstPayment.setReceiverBIC(message.getReceiver());
		deliverFreeAgainstPayment.setBuy(true);
		Stream.of(
				getSequenceBlockFields(message.getSequenceA(), MT540.SequenceA.START_END_16RS),
				getSequenceBlockFields(message.getSequenceB(), MT540.SequenceB.START_END_16RS),
				getSequenceBlockFields(message.getSequenceC(), MT540.SequenceC.START_END_16RS),
				getSequenceBlockFields(message.getSequenceD(), MT540.SequenceD.START_END_16RS),
				getSequenceBlockFields(message.getSequenceE(), MT540.SequenceE.START_END_16RS)
		)
				.flatMap(Function.identity())
				.forEach(b -> AgainstPaymentMessageBuilder.Setters.forEach(s -> s.accept(b, deliverFreeAgainstPayment)));
		return deliverFreeAgainstPayment;
	}


	public AgainstPaymentBlockProvider getAgainstPaymentBlockProvider() {
		return new AgainstPaymentBlockProvider() {
			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA() {
				return MT540.SequenceA::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceA1() {
				return MT540.SequenceA1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB() {
				return MT540.SequenceB::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceB1() {
				return MT540.SequenceB1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceC() {
				return MT540.SequenceC::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceD() {
				return MT540.SequenceD::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE() {
				return MT540.SequenceE::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE1() {
				return MT540.SequenceE1::newInstance;
			}


			@Override
			public Function<SwiftTagListBlock[], SwiftTagListBlock> getSequenceE3() {
				return MT540.SequenceE3::newInstance;
			}
		};
	}
}
