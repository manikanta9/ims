package com.clifton.swift.prowide.vistor;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.prowide.vistor.encoder.EBCDICMessageTextEncoder;
import com.clifton.swift.prowide.vistor.encoder.EDIFACTMessageTextEncoder;
import com.clifton.swift.prowide.vistor.encoder.MessageTextEncoder;
import com.clifton.swift.prowide.vistor.encoder.ZCharacterSetMessageTextEncoder;
import com.prowidesoftware.swift.model.SwiftBlock2;
import com.prowidesoftware.swift.model.SwiftBlock4;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.utils.BaseMessageVisitor;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <code>SwiftMessageTextEncoder</code>
 * Computer-based terminals communicating with SWIFT use EBCDIC code except:
 * <ul>
 * <li>In field 77F of MT 105, the EDIFACT level A character set, as defined in ISO 9735, is used.</li>
 * <li>In fields 70F of MT 568, field 70G of the MT 564, and 77T of MT 103 REMIT, Information Service Character Set (Z Character Set) is used.</li>
 * </ul>
 * <p>
 * EBCDIC:
 * The characters Cr and Lf must never be used as single characters and must only be used together in the sequence CrLf, that is, LfCr is not allowed.
 * When the character sequence CrLf is used in a field format with several lines, it is used to indicate the end of one line of text and the start of the next line of text.
 * SWIFT recommends using the character's hexadecimal EBCDIC code, preceded by two question marks (??) as escape sequence.
 */
public class SwiftMessageTextEncoder extends BaseMessageVisitor {

	// regular expression for a singular control characters or control characters in the wrong order; must be (\r\n)
	public static final String SINGLE_CONTROL_CHARACTER_STRING = "(?<!\\r)\\n|\\r(?!\\n)";
	public static final Pattern singleControlCharacterPattern = Pattern.compile(SINGLE_CONTROL_CHARACTER_STRING);

	private static final List<MessageTextEncoder> encoders = Arrays.asList(
			new EBCDICMessageTextEncoder(),
			new EDIFACTMessageTextEncoder(),
			new ZCharacterSetMessageTextEncoder()
	);

	private String messageType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void startBlock2(SwiftBlock2 b) {
		this.messageType = b.getMessageType();
	}


	@Override
	public void tag(SwiftBlock4 swiftBlock4, Tag tag) {
		String value = tag.getValue();
		if (!StringUtils.isEmpty(value)) {
			MessageTextEncoder encoder = encoders.stream().filter(e -> e.accept(this.messageType, tag.getName())).findFirst().orElse(null);
			// reserved characters
			if (encoder != null && encoder.hasReservedCharacters(value)) {
				value = encoder.encode(value);
			}
			// singular control characters
			Matcher controlMatcher = singleControlCharacterPattern.matcher(value);
			if (controlMatcher.find()) {
				value = value.replaceAll(SINGLE_CONTROL_CHARACTER_STRING, "\r\n");
			}
			tag.setValue(value);
		}
	}
}
