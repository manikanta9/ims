package com.clifton.swift.prowide.converter.builder;

import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.field.Field;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * <code>SwiftImmutableFieldBuilder</code> delay construction of an immutable block until
 * all its fields are added, only fields can be added to this immutable block.
 */
public class SwiftImmutableFieldBuilder implements SwiftBlockBuilder {

	private Function<Field[], SwiftTagListBlock> blockSupplier;
	private List<Field> fields = new ArrayList<>();

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private SwiftImmutableFieldBuilder(Function<Field[], SwiftTagListBlock> blockSupplier) {
		this.blockSupplier = blockSupplier;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public static SwiftImmutableFieldBuilder create(Function<Field[], SwiftTagListBlock> blockSupplier) {
		return new SwiftImmutableFieldBuilder(blockSupplier);
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public SwiftImmutableFieldBuilder withField(BooleanSupplier predicate, Supplier<Field> componentSupplier) {
		if (predicate.getAsBoolean()) {
			this.fields.add(componentSupplier.get());
		}
		return this;
	}


	@Override
	public SwiftTagListBlock toBlock() {
		return this.blockSupplier.apply(this.fields.toArray(new Field[this.fields.size()]));
	}
}
