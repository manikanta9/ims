package com.clifton.swift.prowide.converter;

import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverter;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.converter.SwiftMessageConverterLocator;
import com.clifton.swift.prowide.util.ProwideSwiftUtils;
import com.prowidesoftware.swift.io.parser.SwiftParser;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt0xx.MTUnknownMessage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author mwacker
 */
@Component("swiftMessageConverterHandler")
public class ProwideSwiftMessageConverterHandlerImpl implements SwiftMessageConverterHandler<AbstractMessage> {

	private SwiftMessageConverterLocator swiftMessageConverterLocator;


	@Override
	public Map<String, AbstractMessage> parse(String text, SwiftMessageFormats messageFormat) {
		if (messageFormat == SwiftMessageFormats.MT) {
			try {
				Map<String, AbstractMessage> result = new LinkedHashMap<>();
				String[] messageStrings = text.split(SwiftMessageConverterHandler.SWIFT_MESSAGE_DELIMITER);
				for (String messageString : messageStrings) {
					AbstractMessage message = AbstractMT.parse(messageString);
					if (message == null) {
						// the specific message type could not be created, return an Unknown message.
						SwiftMessage swiftMessage = (new SwiftParser(messageString)).message();
						message = new MTUnknownMessage(swiftMessage);
					}
					result.put(messageString, message);
				}
				return result;
			}
			catch (IOException e) {
				throw new RuntimeException("Could not parse swift message [" + text + "].", e);
			}
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public <F extends InstructionMessage, Q extends AbstractMessage> F toInstructionEntity(Q message) {
		SwiftMessageFormats messageFormat = ProwideSwiftUtils.getMessageFormat(message);
		SwiftMessageConverter<F, Q> converter = (SwiftMessageConverter<F, Q>) getSwiftMessageConverter(message, messageFormat);
		return converter.fromSwiftEntity(message);
	}


	@SuppressWarnings("unchecked")
	@Override
	public <Q extends AbstractMessage, F extends InstructionMessage> Q fromInstructionEntity(F entity, SwiftMessageFormats messageFormat) {
		SwiftMessageConverter<F, Q> converter = (SwiftMessageConverter<F, Q>) getSwiftMessageConverter(entity, messageFormat);
		String message = converter.toSwiftMessage(entity);
		return (Q) CollectionUtils.getFirstElement(parse(message, messageFormat).values());
	}


	@Override
	public <F extends InstructionMessage> String toSwiftMessage(F entity, SwiftMessageFormats messageFormat) {
		if (messageFormat == SwiftMessageFormats.MT) {
			return ((AbstractMT) fromInstructionEntity(entity, SwiftMessageFormats.MT)).message();
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("rawtypes")
	protected <T> SwiftMessageConverter getSwiftMessageConverter(T messageObject, SwiftMessageFormats messageFormat) {
		return getSwiftMessageConverterLocator().locate(messageObject, messageFormat);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageConverterLocator getSwiftMessageConverterLocator() {
		return this.swiftMessageConverterLocator;
	}


	public void setSwiftMessageConverterLocator(SwiftMessageConverterLocator swiftMessageConverterLocator) {
		this.swiftMessageConverterLocator = swiftMessageConverterLocator;
	}
}
