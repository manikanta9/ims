package com.clifton.swift.prowide.converter.builder;

import com.prowidesoftware.swift.model.SwiftTagListBlock;


/**
 * <code>SwiftBlockBuilder</code> common to all the builder, the only method common to all builders is the
 * getting the end result.
 */
public interface SwiftBlockBuilder {

	public SwiftTagListBlock toBlock();
}
