package com.clifton.swift.prowide.converter.mt;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.swift.message.converter.SwiftMessageConverter;
import com.clifton.swift.prowide.vistor.SwiftMessageTextEncoder;
import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.field.Field16R;
import com.prowidesoftware.swift.model.field.Field16S;
import com.prowidesoftware.swift.model.mt.AbstractMT;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Stream;


/**
 * <code>AbstractSwiftMtMessageConverter</code> Escapes any illegal characters from the Abstract MT message's text fields.
 */
public abstract class AbstractSwiftMtMessageConverter<E extends InstructionMessage, S extends AbstractMT> implements SwiftMessageConverter<E, S> {

	@Override
	public String toSwiftMessage(E entity) {
		LogUtils.info(getClass(), "Converting Swift Message from Instruction Entity using converter implementation:  " + this.getClass().getName());
		AbstractMT abstractMT = toSwiftEntity(entity);
		// make sure all reserved characters are escaped.
		SwiftMessageTextEncoder encoder = new SwiftMessageTextEncoder();
		abstractMT.getSwiftMessage().visit(encoder);
		return abstractMT.message();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected Stream<BlockField> getSequenceBlockFields(SwiftTagListBlock sequence, String block) {
		return Stream.of(sequence)
				.flatMap(s -> Arrays.stream(s.asTagArray()))
				.map(Tag::asField)
				.filter(f -> !StringUtils.equalsAny(f.getName(), Field16R.NAME, Field16S.NAME))
				.map(f -> new BlockField(block, f));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static class BlockField {

		private final String block;
		private final Field field;


		public BlockField(String block, Field field) {
			this.block = block;
			this.field = field;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			BlockField that = (BlockField) o;
			return Objects.equals(this.block, that.block) &&
					Objects.equals(this.field, that.field);
		}


		@Override
		public int hashCode() {

			return Objects.hash(this.block, this.field);
		}


		public String getBlock() {
			return this.block;
		}


		public Field getField() {
			return this.field;
		}
	}
}
