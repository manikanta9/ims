package com.clifton.swift.prowide.converter.builder;

import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;


/**
 * <code>SwiftMutableBlockBuilder</code> build a mutable block and iteratively allow appending of field, tag and block components.
 */
public class SwiftMutableBlockBuilder implements SwiftBlockBuilder {

	private SwiftTagListBlock mutableBlock;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private SwiftMutableBlockBuilder() {
		this.mutableBlock = new SwiftTagListBlock();
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public static SwiftMutableBlockBuilder createBlock() {
		return new SwiftMutableBlockBuilder();
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public SwiftMutableBlockBuilder withField(Supplier<Field> fieldSupplier) {
		this.mutableBlock.append(fieldSupplier.get());
		return this;
	}


	public SwiftMutableBlockBuilder withField(BooleanSupplier predicate, Supplier<Field> fieldSupplier) {
		if (predicate.getAsBoolean()) {
			this.mutableBlock.append(fieldSupplier.get());
		}
		return this;
	}


	public SwiftMutableBlockBuilder withTag(BooleanSupplier predicate, Supplier<Tag> tagSupplier) {
		if (predicate.getAsBoolean()) {
			this.mutableBlock.append(tagSupplier.get());
		}
		return this;
	}


	public SwiftMutableBlockBuilder withBlock(Supplier<SwiftTagListBlock> blockSupplier) {
		this.mutableBlock.append(blockSupplier.get());
		return this;
	}


	public SwiftMutableBlockBuilder withBlock(BooleanSupplier predicate, Supplier<SwiftTagListBlock> blockSupplier) {
		if (predicate.getAsBoolean()) {
			this.mutableBlock.append(blockSupplier.get());
		}
		return this;
	}


	@Override
	public SwiftTagListBlock toBlock() {
		return this.mutableBlock;
	}
}
