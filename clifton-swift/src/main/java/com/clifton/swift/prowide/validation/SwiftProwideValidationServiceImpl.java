package com.clifton.swift.prowide.validation;

import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.validation.SwiftValidationService;
import com.prowidesoftware.swift.validator.ValidationEngine;
import com.prowidesoftware.swift.validator.ValidationProblem;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
@Service("swiftValidationService")
public class SwiftProwideValidationServiceImpl implements SwiftValidationService {

	@Override
	public String validateSwiftMessage(String message, SwiftMessageFormats messageType) {
		ValidationEngine engine = new ValidationEngine();
		try {
			engine.initialize();

			List<ValidationProblem> r = new ArrayList<>();

			switch (messageType) {
				case MX:
					r = engine.validateMxMessage(message);
					break;
				case MT:
					r = engine.validateMtMessage(message);
					break;
			}
			return SwiftProwideValidationUtils.getValidationProblemsAsString(r);
		}
		finally {
			engine.dispose();
		}
	}
}
