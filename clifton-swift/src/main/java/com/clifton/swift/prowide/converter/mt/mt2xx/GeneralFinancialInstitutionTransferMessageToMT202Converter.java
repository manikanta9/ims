package com.clifton.swift.prowide.converter.mt.mt2xx;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.prowide.converter.mt.AbstractSwiftMtMessageConverter;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.field.Field16R;
import com.prowidesoftware.swift.model.field.Field16S;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.field.Field21;
import com.prowidesoftware.swift.model.field.Field32A;
import com.prowidesoftware.swift.model.field.Field53B;
import com.prowidesoftware.swift.model.field.Field57A;
import com.prowidesoftware.swift.model.field.Field58A;
import com.prowidesoftware.swift.model.field.Field58D;
import com.prowidesoftware.swift.model.field.Field72;
import com.prowidesoftware.swift.model.mt.mt2xx.MT202;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Stream;


/**
 * @author mwacker
 */
@Component
public class GeneralFinancialInstitutionTransferMessageToMT202Converter extends AbstractSwiftMtMessageConverter<GeneralFinancialInstitutionTransferMessage, MT202> {

	public static final String TAG_SHOULD_ONLY_APPEAR_ONCE = "Tag should only appear once ";
	// see: java.text.SimpleDateFormat
	public static final String DATE_FORMAT = "yyMMdd";

	private static final List<BiConsumer<Field, GeneralFinancialInstitutionTransferMessage>> Setters = Arrays.asList(
			(field, generalFinancialInstitutionTransferMessage) -> {
				if (field instanceof Field20) {
					Field20 field20 = (Field20) field;
					ValidationUtils.assertNull(generalFinancialInstitutionTransferMessage.getTransactionReferenceNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					generalFinancialInstitutionTransferMessage.setTransactionReferenceNumber(field20.getReference());
				}
			},
			(field, generalFinancialInstitutionTransferMessage) -> {
				if (field instanceof Field21) {
					Field21 field21 = (Field21) field;
					ValidationUtils.assertNull(generalFinancialInstitutionTransferMessage.getMessagePurpose(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					generalFinancialInstitutionTransferMessage.setMessagePurpose(TransferMessagePurposes.findByCode(field21.getValue()));
				}
			},
			(field, generalFinancialInstitutionTransferMessage) -> {
				if (field instanceof Field32A) {
					Field32A field32A = (Field32A) field;
					ValidationUtils.assertNull(generalFinancialInstitutionTransferMessage.getAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					Date valueDate = DateUtils.toDate(field32A.getDate(), DATE_FORMAT);
					generalFinancialInstitutionTransferMessage.setValueDate(valueDate);
					generalFinancialInstitutionTransferMessage.setCurrency(field32A.getCurrency());
					generalFinancialInstitutionTransferMessage.setAmount(new BigDecimal(field32A.getAmountAsNumber().toString()));
				}
			},
			(field, generalFinancialInstitutionTransferMessage) -> {
				if (field instanceof Field53B) {
					Field53B field53B = (Field53B) field;
					ValidationUtils.assertNull(generalFinancialInstitutionTransferMessage.getCustodyAccountNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					generalFinancialInstitutionTransferMessage.setCustodyAccountNumber(field53B.getAccount());
				}
			},
			(field, generalFinancialInstitutionTransferMessage) -> {
				if (field instanceof Field57A) {
					Field57A field57A = (Field57A) field;
					ValidationUtils.assertNull(generalFinancialInstitutionTransferMessage.getIntermediaryBIC(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					generalFinancialInstitutionTransferMessage.setIntermediaryBIC(field57A.getBIC());
				}
			},
			(field, generalFinancialInstitutionTransferMessage) -> {
				if (field instanceof Field58A) {
					Field58A field58A = (Field58A) field;
					ValidationUtils.assertNull(generalFinancialInstitutionTransferMessage.getIntermediaryAccountNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					generalFinancialInstitutionTransferMessage.setIntermediaryAccountNumber(field58A.getAccount());
					generalFinancialInstitutionTransferMessage.setBeneficiaryBIC(field58A.getBIC());
				}
			},
			(field, generalFinancialInstitutionTransferMessage) -> {
				if (field instanceof Field58D) {
					Field58D field58D = (Field58D) field;
					ValidationUtils.assertNull(generalFinancialInstitutionTransferMessage.getIntermediaryAccountNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					generalFinancialInstitutionTransferMessage.setIntermediaryAccountNumber(field58D.getAccount());
					generalFinancialInstitutionTransferMessage.setBeneficiaryAccountName(field58D.getNameAndAddress());
				}
			}
			// Field72 cannot be reversed, it has a destructive transformation made to the original account #, '-' were removed.
	);


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	@Override
	public Class<?> getInternalMessageClass() {
		return GeneralFinancialInstitutionTransferMessage.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MT202.class;
	}


	@Override
	public GeneralFinancialInstitutionTransferMessage fromSwiftEntity(MT202 message) {
		GeneralFinancialInstitutionTransferMessage generalFinancialInstitutionTransferMessage = new GeneralFinancialInstitutionTransferMessage();
		generalFinancialInstitutionTransferMessage.setSenderBIC(message.getSender());
		generalFinancialInstitutionTransferMessage.setReceiverBIC(message.getReceiver());
		Stream.of(message.getSwiftMessage())
				.map(SwiftMessage::getBlock4)
				.flatMap(s -> Arrays.stream(s.asTagArray()))
				.map(Tag::asField)
				.filter(f -> !StringUtils.equalsAny(f.getName(), Field16R.NAME, Field16S.NAME))
				.distinct()
				.forEach(f -> GeneralFinancialInstitutionTransferMessageToMT202Converter.Setters.forEach(s -> s.accept(f, generalFinancialInstitutionTransferMessage)));
		return generalFinancialInstitutionTransferMessage;
	}


	@Override
	public MT202 toSwiftEntity(GeneralFinancialInstitutionTransferMessage entity) {

		//// ********** FIELD ORDER MATTERS, FIELDS MUST BE ADDED IN THE CORRECT ORDER ********** ////

		MT202 message = new MT202();

		// Set sender and receiver BIC codes
		message.setSender(entity.getSenderBIC());
		message.setReceiver(entity.getReceiverBIC());

		/*
		 * Start adding the message's fields in correct order
		 */
		message.addField(new Field20(entity.getTransactionReferenceNumber()));
		message.addField(new Field21(entity.getMessagePurpose().getCode()));

		/*
		 * Add a field using comprehensive setters API
		 */
		Field32A f32A = new Field32A()
				.setDate(DateUtils.fromDate(entity.getValueDate(), DATE_FORMAT))
				.setCurrency(entity.getCurrency())
				.setAmount(entity.getAmount());
		message.addField(f32A);

		/*
		 * Add the orderer field
		 */
		Field53B f53b = new Field53B()
				.setAccount(entity.getCustodyAccountNumber());
		message.addField(f53b);

		Field57A f57a = new Field57A()
				.setBIC(entity.getIntermediaryBIC());
		message.addField(f57a);


		/*
		 * Add the beneficiary field
		 */
		if (!StringUtils.isEmpty(entity.getBeneficiaryBIC())) {
			Field58A f58a = new Field58A()
					.setAccount(entity.getIntermediaryAccountNumber())
					.setBIC(entity.getBeneficiaryBIC());
			message.addField(f58a);
		}
		else {
			Field58D f58d = new Field58D()
					.setAccount(entity.getIntermediaryAccountNumber())
					.setNameAndAddress(entity.getBeneficiaryAccountName());
			message.addField(f58d);
		}


		if (!StringUtils.isEmpty(entity.getBeneficiaryAccountNumber())) {
			Field72 f72 = new Field72().setNarrative("/BNF/" + entity.getBeneficiaryAccountNumber().replace("-", "") + "/");
			message.addField(f72);
		}

		// remove block 3
		// message.getSwiftMessage().setBlock3(null);

		return message;
	}
}
