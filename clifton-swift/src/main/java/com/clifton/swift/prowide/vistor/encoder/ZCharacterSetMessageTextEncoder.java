package com.clifton.swift.prowide.vistor.encoder;

import com.clifton.core.util.StringUtils;
import com.prowidesoftware.swift.model.field.Field70F;
import com.prowidesoftware.swift.model.field.Field70G;
import com.prowidesoftware.swift.model.field.Field77T;
import com.prowidesoftware.swift.model.mt.mt1xx.MT103_REMIT;
import com.prowidesoftware.swift.model.mt.mt5xx.MT564;
import com.prowidesoftware.swift.model.mt.mt5xx.MT568;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ZCharacterSetMessageTextEncoder implements MessageTextEncoder {

	// regular expression for the accepted EBCDIC characters.
	public static final Pattern acceptedZCharacterSetPattern = Pattern.compile("^[A-z0-9/\\-\\?:\\(\\)\\.\\+,' \\=\\!%&\\*\\<\\>\";\\{@#_\\r\\n]+$");

	private Map<String, List<String>> acceptedMessageFields = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ZCharacterSetMessageTextEncoder() {
		this.acceptedMessageFields.put(MT568.NAME, Collections.singletonList(Field70F.NAME));
		this.acceptedMessageFields.put(MT564.NAME, Collections.singletonList(Field70G.NAME));
		this.acceptedMessageFields.put(MT103_REMIT.NAME, Collections.singletonList(Field77T.NAME));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean accept(String messageType, String field) {
		return this.acceptedMessageFields.containsKey(messageType) && this.acceptedMessageFields.get(messageType).contains(field);
	}


	@Override
	public boolean hasReservedCharacters(String value) {
		Matcher acceptedMatcher = acceptedZCharacterSetPattern.matcher(value);
		return !acceptedMatcher.matches();
	}


	@Override
	public String encode(String value) {
		if (!StringUtils.isEmpty(value)) {
			return value.replaceAll("[^A-z0-9/\\-\\?:\\(\\)\\.\\+,' \\=\\!%&\\*\\<\\>\";\\{@#_\\r\\n]", StringUtils.EMPTY_STRING);
		}
		return value;
	}
}
