package com.clifton.swift.prowide.converter.builder;

import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;

import java.util.function.Function;


/**
 * <code>SwiftMessageBlockBuilder</code> Builder for constructing a SWIFT message.
 * SWIFT messages blocks and fields must be appended in pre-determined order.
 * SWIFT will drop empty blocks.
 * Mutable blocks allow iterative appending of a mixture of field, block and tag components.
 * Immutable blocks must be build from a single type only, its construction is delayed
 * until toBlock() is called.
 * Predicates will determine when a component is added to the block, only when the component
 * is included in a block will it be created via the provided supplier.  The supplier,
 * predicate are forcing the creation of individual immutable builders in order to prevent
 * runtime casting exceptions when using generics.
 */
public class SwiftMessageBlockBuilder {

	private SwiftBlockBuilder swiftBlockBuilder;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	/**
	 * Used for plain SwiftTagListBlock, can be appended with Tags, Fields and Blocks.
	 */
	public static SwiftMutableBlockBuilder createMutableBlock() {
		return SwiftMutableBlockBuilder.createBlock();
	}


	/**
	 * Used for immutable block with tags only.
	 */
	public static SwiftImmutableTagBuilder createImmutableBlockFromTags(Function<Tag[], SwiftTagListBlock> blockSupplier) {
		return SwiftImmutableTagBuilder.create(blockSupplier);
	}


	/**
	 * Used for immutable block with fields only.
	 */
	public static SwiftImmutableFieldBuilder createImmutableBlockFromFields(Function<Field[], SwiftTagListBlock> blockSupplier) {
		return SwiftImmutableFieldBuilder.create(blockSupplier);
	}


	/**
	 * Used for immutable block with blocks only.
	 */
	public static SwiftImmutableBlockBuilder createImmutableBlockFromBlocks(Function<SwiftTagListBlock[], SwiftTagListBlock> blockSupplier) {
		return SwiftImmutableBlockBuilder.create(blockSupplier);
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public SwiftTagListBlock toBlock() {
		return this.swiftBlockBuilder.toBlock();
	}
}
