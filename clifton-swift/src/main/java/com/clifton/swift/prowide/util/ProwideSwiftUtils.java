package com.clifton.swift.prowide.util;

import com.clifton.swift.message.SwiftMessageFormats;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.MessageStandardType;


public class ProwideSwiftUtils {

	public static SwiftMessageFormats findMessageFormat(MessageStandardType messageStandardType) {
		for (SwiftMessageFormats swiftMessageFormats : SwiftMessageFormats.values()) {
			if (messageStandardType.toString().equals(swiftMessageFormats.toString())) {
				return swiftMessageFormats;
			}
		}
		throw new RuntimeException("Swift Message Format not found for message standard type " + messageStandardType.toString());
	}


	public static SwiftMessageFormats getMessageFormat(AbstractMessage message) {
		return message.isMT() ? SwiftMessageFormats.MT : SwiftMessageFormats.MX;
	}
}
