package com.clifton.swift.prowide.vistor.encoder;

/**
 * <code>MessageTextEncoder</code> implementations will handle specific message text encoding rules.
 */
public interface MessageTextEncoder {

	public boolean accept(String messageType, String field);


	public String encode(String value);


	public boolean hasReservedCharacters(String value);
}
