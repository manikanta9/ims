package com.clifton.swift.prowide.converter.mt.mt2xx;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.prowide.converter.mt.AbstractSwiftMtMessageConverter;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.field.Field16R;
import com.prowidesoftware.swift.model.field.Field16S;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.field.Field21;
import com.prowidesoftware.swift.model.field.Field25;
import com.prowidesoftware.swift.model.field.Field30;
import com.prowidesoftware.swift.model.field.Field32B;
import com.prowidesoftware.swift.model.field.Field52A;
import com.prowidesoftware.swift.model.field.Field56A;
import com.prowidesoftware.swift.model.mt.mt2xx.MT210;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.stream.Stream;


/**
 * @author mwacker
 */
@Component
public class NoticeToReceiveTransferMessageToMT210Converter extends AbstractSwiftMtMessageConverter<NoticeToReceiveTransferMessage, MT210> {

	public static final String TAG_SHOULD_ONLY_APPEAR_ONCE = "Tag should only appear once ";
	// see: java.text.SimpleDateFormat
	public static final String DATE_FORMAT = "yyMMdd";

	private static final List<BiConsumer<Field, NoticeToReceiveTransferMessage>> Setters = Arrays.asList(
			(field, noticeToReceiveTransferMessage) -> {
				if (field instanceof Field20) {
					Field20 field20 = (Field20) field;
					ValidationUtils.assertNull(noticeToReceiveTransferMessage.getTransactionReferenceNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					noticeToReceiveTransferMessage.setTransactionReferenceNumber(field20.getReference());
				}
			},
			(field, noticeToReceiveTransferMessage) -> {
				if (field instanceof Field25) {
					Field25 field25 = (Field25) field;
					ValidationUtils.assertNull(noticeToReceiveTransferMessage.getReceiverAccountNumber(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					noticeToReceiveTransferMessage.setReceiverAccountNumber(field25.getAccount());
				}
			},
			(field, noticeToReceiveTransferMessage) -> {
				if (field instanceof Field30) {
					Field30 field30 = (Field30) field;
					ValidationUtils.assertNull(noticeToReceiveTransferMessage.getValueDate(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					Date valueDate = DateUtils.toDate(field30.getDate(), DATE_FORMAT);
					noticeToReceiveTransferMessage.setValueDate(valueDate);
				}
			},
			(field, noticeToReceiveTransferMessage) -> {
				if (field instanceof Field21) {
					Field21 field21 = (Field21) field;
					ValidationUtils.assertNull(noticeToReceiveTransferMessage.getMessagePurpose(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					noticeToReceiveTransferMessage.setMessagePurpose(TransferMessagePurposes.findByCode(field21.getValue()));
				}
			},
			(field, noticeToReceiveTransferMessage) -> {
				if (field instanceof Field32B) {
					Field32B field32B = (Field32B) field;
					ValidationUtils.assertNull(noticeToReceiveTransferMessage.getAmount(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					noticeToReceiveTransferMessage.setCurrency(field32B.getCurrency());
					noticeToReceiveTransferMessage.setAmount(new BigDecimal(field32B.getAmountAsNumber().toString()));
				}
			},
			(field, noticeToReceiveTransferMessage) -> {
				if (field instanceof Field52A) {
					Field52A field52A = (Field52A) field;
					ValidationUtils.assertNull(noticeToReceiveTransferMessage.getOrderingBIC(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					noticeToReceiveTransferMessage.setOrderingBIC(field52A.getBIC());
				}
			},
			(field, noticeToReceiveTransferMessage) -> {
				if (field instanceof Field56A) {
					Field56A field56A = (Field56A) field;
					ValidationUtils.assertNull(noticeToReceiveTransferMessage.getIntermediaryBIC(), TAG_SHOULD_ONLY_APPEAR_ONCE + field.getName());
					noticeToReceiveTransferMessage.setIntermediaryBIC(field56A.getBIC());
				}
			}
	);


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	@Override
	public Class<?> getInternalMessageClass() {
		return NoticeToReceiveTransferMessage.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MT210.class;
	}


	@Override
	public NoticeToReceiveTransferMessage fromSwiftEntity(MT210 message) {
		NoticeToReceiveTransferMessage noticeToReceiveTransferMessage = new NoticeToReceiveTransferMessage();
		noticeToReceiveTransferMessage.setSenderBIC(message.getSender());
		noticeToReceiveTransferMessage.setReceiverBIC(message.getReceiver());
		Stream.of(message.getSwiftMessage())
				.map(SwiftMessage::getBlock4)
				.flatMap(s -> Arrays.stream(s.asTagArray()))
				.map(Tag::asField)
				.filter(f -> !StringUtils.equalsAny(f.getName(), Field16R.NAME, Field16S.NAME))
				.distinct()
				.forEach(f -> NoticeToReceiveTransferMessageToMT210Converter.Setters.forEach(s -> s.accept(f, noticeToReceiveTransferMessage)));
		return noticeToReceiveTransferMessage;
	}


	@Override
	public MT210 toSwiftEntity(NoticeToReceiveTransferMessage entity) {

		//// ********** FIELD ORDER MATTERS, FIELDS MUST BE ADDED IN THE CORRECT ORDER ********** ////

		MT210 message = new MT210();

		// Set sender and receiver BIC codes
		message.setSender(entity.getSenderBIC());
		message.setReceiver(entity.getReceiverBIC());

		// add the transaction reference
		message.addField(new Field20(entity.getTransactionReferenceNumber()));

		// add the receiving account number: note the field order matters this needs to be after 20 and before 30
		message.addField(new Field25(entity.getReceiverAccountNumber()));

		// add the message date: note the field order matters this needs to be after 25 and before 21
		message.addField(new Field30(DateUtils.fromDate(entity.getValueDate(), DATE_FORMAT)));
		message.addField(new Field21(entity.getMessagePurpose().getCode()));

		Field32B f32B = new Field32B()
				.setCurrency(entity.getCurrency())
				.setAmount(entity.getAmount());
		message.addField(f32B);

		Field52A f52a = new Field52A()
				.setBIC(entity.getOrderingBIC());
		message.addField(f52a);

		Field56A f56a = new Field56A()
				.setBIC(entity.getIntermediaryBIC());
		message.addField(f56a);

		return message;
	}
}
