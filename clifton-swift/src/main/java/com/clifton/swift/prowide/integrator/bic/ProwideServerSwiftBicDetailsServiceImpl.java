package com.clifton.swift.prowide.integrator.bic;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.message.bic.SwiftBicDetail;
import com.clifton.swift.message.bic.SwiftBicDetailService;
import com.prowidesoftware.swift.BICDirectory;
import com.prowidesoftware.swift.model.BICRecord;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


/**
 * <code>ProwideServerSwiftBicDetailsServiceImpl</code> included as part of the Prowide
 * Integrator services package, this will fetch BIC details from a Prowide provided
 * embedded Derby database.
 * <p>
 * WARNING:  The BIC database is not up to date.
 * <p>
 * BIC:
 * AAAA - bank (Alpha)
 * BB - country (ISO 3166-1 Alpha)
 * CC - location (Alpha-Numeric)
 * DDD - branch (optional, Alpha-Numeric, use XXX to refer to the primary office.)
 */
@Service("swiftBicDetailService")
public class ProwideServerSwiftBicDetailsServiceImpl implements SwiftBicDetailService, InitializingBean {

	// Queries the embedded Derby database using JDBC.
	private BICDirectory bicRepository;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Will only return the {@link SwiftBicDetail} record if the provided code
	 * is 8 or 11 characters long and only one match was found.
	 *
	 * @param code - an 8 or 11 alpha numeric BIC.
	 * @return - the detailed BIC information
	 */
	@Override
	public SwiftBicDetail getSwiftBicDetails(String code) {
		if (!StringUtils.isEmpty(code) && (code.length() == 8 || code.length() == 11)) {
			// assume primary branch
			if (code.length() == 8) {
				code += "XXX";
			}
			List<BICRecord> found = getBicRepository().query(code, true);
			if (found != null && found.size() == 1) {
				BICRecord bicRecord = found.get(0);
				return populateSwiftBicDetails(bicRecord);
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SwiftBicDetail populateSwiftBicDetails(BICRecord bicRecord) {
		SwiftBicDetail swiftBicDetail = new SwiftBicDetail();
		swiftBicDetail.setBranch(bicRecord.getBranch());
		swiftBicDetail.setBranchInformation(bicRecord.getBranchInformation());
		swiftBicDetail.setCityHeading(bicRecord.getCityHeading());
		swiftBicDetail.setCode(bicRecord.getCode());
		swiftBicDetail.setCountryName(bicRecord.getCountryName());
		swiftBicDetail.setExtraInformation(bicRecord.getExtraInformation());
		swiftBicDetail.setInstitutionName(bicRecord.getInstitutionName());
		swiftBicDetail.setLocation(bicRecord.getLocation());
		swiftBicDetail.setPhysicalAddressPart1(bicRecord.getPhysicalAddressPart1());
		swiftBicDetail.setPhysicalAddressPart2(bicRecord.getPhysicalAddressPart2());
		swiftBicDetail.setPhysicalAddressPart3(bicRecord.getPhysicalAddressPart3());
		swiftBicDetail.setSubtypeIndication(bicRecord.getSubtypeIndication());
		swiftBicDetail.setValueAddedServices(bicRecord.getValueAddedServices());
		swiftBicDetail.setPOBLocation(bicRecord.getPOBLocation());
		swiftBicDetail.setPOBCountryName(bicRecord.getPOBCountryName());
		swiftBicDetail.setPOBNumber(bicRecord.getPOBNumber());
		return swiftBicDetail;
	}


	private BICDirectory getBicRepository() {
		return this.bicRepository;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() throws Exception {
		// change the location and file name for derby.log created by Prowide when looking up BIC information.
		Path derbyLogFile = Files.createTempFile("prowide_bic_derby", ".log");
		System.setProperty("derby.stream.error.field", derbyLogFile.toAbsolutePath().toString());
		this.bicRepository = new BICDirectory();
	}
}
