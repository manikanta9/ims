package com.clifton.swift.prowide.converter.mx;

import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverter;
import com.prowidesoftware.swift.model.mx.MxPacs00900102;
import com.prowidesoftware.swift.model.mx.dic.AccountIdentification4Choice;
import com.prowidesoftware.swift.model.mx.dic.ActiveCurrencyAndAmount;
import com.prowidesoftware.swift.model.mx.dic.BranchAndFinancialInstitutionIdentification4;
import com.prowidesoftware.swift.model.mx.dic.CashAccount16;
import com.prowidesoftware.swift.model.mx.dic.CreditTransferTransactionInformation13;
import com.prowidesoftware.swift.model.mx.dic.FinancialInstitutionCreditTransferV02;
import com.prowidesoftware.swift.model.mx.dic.FinancialInstitutionIdentification7;
import com.prowidesoftware.swift.model.mx.dic.GenericAccountIdentification1;
import com.prowidesoftware.swift.model.mx.dic.GroupHeader35;
import com.prowidesoftware.swift.model.mx.dic.PaymentIdentification3;
import com.prowidesoftware.swift.model.mx.dic.SettlementInformation13;
import com.prowidesoftware.swift.model.mx.dic.SettlementMethod1Code;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * @author mwacker
 */
public class InstructionEntityCashMxPacs00900102Converter implements SwiftMessageConverter<GeneralFinancialInstitutionTransferMessage, MxPacs00900102> {


	public static final String TBEXO_12345 = "TBEXO12345";


	@Override
	public Class<?> getInternalMessageClass() {
		return GeneralFinancialInstitutionTransferMessage.class;
	}


	@Override
	public Class<?> getNativeSwiftClass() {
		return MxPacs00900102.class;
	}


	@Override
	public String toSwiftMessage(GeneralFinancialInstitutionTransferMessage entity) {
		return toSwiftEntity(entity).message();
	}


	@Override
	public MxPacs00900102 toSwiftEntity(GeneralFinancialInstitutionTransferMessage entity) {
		MxPacs00900102 mx = new MxPacs00900102();

		/*
		 * Initialize main message content main objects
		 */
		mx.setFinInstnCdtTrf(new FinancialInstitutionCreditTransferV02().setGrpHdr(new GroupHeader35()));

		/*
		 * General Information
		 */
		mx.getFinInstnCdtTrf().getGrpHdr().setMsgId(entity.getTransactionReferenceNumber());
		mx.getFinInstnCdtTrf().getGrpHdr().setNbOfTxs("1");

		/*
		 * Settlement Information
		 */
		mx.getFinInstnCdtTrf().getGrpHdr().setSttlmInf(new SettlementInformation13());
		mx.getFinInstnCdtTrf().getGrpHdr().getSttlmInf().setSttlmMtd(SettlementMethod1Code.INDA);
		mx.getFinInstnCdtTrf().getGrpHdr().getSttlmInf().setSttlmAcct(
				(new CashAccount16()).setId(
						(new AccountIdentification4Choice()).setOthr(
								(new GenericAccountIdentification1()).setId(entity.getCustodyAccountNumber()))));

		/*
		 * Instructing Agent
		 */
		mx.getFinInstnCdtTrf().getGrpHdr().setInstgAgt(
				(new BranchAndFinancialInstitutionIdentification4()).setFinInstnId(
						(new FinancialInstitutionIdentification7()).setBIC(entity.getReceiverBIC())));


		/*
		 * Payment Transaction Information
		 */
		CreditTransferTransactionInformation13 cti = new CreditTransferTransactionInformation13();

		/*
		 * Transaction Identification
		 */
		cti.setPmtId(new PaymentIdentification3());
		cti.getPmtId().setInstrId(TBEXO_12345);
		cti.getPmtId().setEndToEndId(TBEXO_12345);
		cti.getPmtId().setTxId(TBEXO_12345);

		/*
		 * Transaction Amount
		 */
		ActiveCurrencyAndAmount amount = new ActiveCurrencyAndAmount();
		amount.setCcy("USD");
		amount.setValue(new BigDecimal("23453"));
		cti.setIntrBkSttlmAmt(amount);


		mx.getFinInstnCdtTrf().addCdtTrfTxInf(cti);

		return mx;
	}


	@Override
	public GeneralFinancialInstitutionTransferMessage fromSwiftEntity(MxPacs00900102 swiftEntity) {
		return null;
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MX;
	}


	public static XMLGregorianCalendar getXMLGregorianCalendar(Date date) {
		try {
			Calendar calendar = GregorianCalendar.getInstance();
			calendar.setTime(date);
			DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
			return datatypeFactory.newXMLGregorianCalendar((GregorianCalendar) calendar);
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to create XMLGregorianCalendar from [" + DateUtils.fromDate(date) + "].", e);
		}
	}
}
