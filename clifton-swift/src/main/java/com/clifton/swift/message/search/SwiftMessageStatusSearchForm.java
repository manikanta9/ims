package com.clifton.swift.message.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * <code>SwiftMessageStatusSearchForm</code>
 */
public class SwiftMessageStatusSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String description;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
