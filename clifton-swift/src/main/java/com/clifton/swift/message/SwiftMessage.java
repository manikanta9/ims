package com.clifton.swift.message;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.swift.identifier.SwiftIdentifier;

import java.util.Date;


/**
 * The <code>SwiftMessage</code>Stores stores messages that are sent to and from the system.
 *
 * @author mwacker
 */
public class SwiftMessage extends BaseSimpleEntity<Long> {

	/**
	 * The SWIFT message type.
	 */
	private SwiftMessageType type;

	/**
	 * The message identifier for the SWIFT message.
	 */
	private SwiftIdentifier identifier;

	/**
	 * The message processing status.
	 */
	private SwiftMessageStatus messageStatus;

	/**
	 * The date and time that message was sent.
	 */
	private Date messageDateAndTime;

	/**
	 * The MX or MT SWIFT message text.
	 */
	private String text;

	/**
	 * Indicates the format of the text payload (MX or MT).
	 */
	private SwiftMessageFormats messageFormat;

	/**
	 * The sender BIC parsed from the message.
	 */
	private String senderBusinessIdentifierCode;

	/**
	 * The receiver BIC parsed from the message.
	 */
	private String receiverBusinessIdentifierCode;

	/**
	 * Indicates if the message was sent or received
	 */
	private boolean incoming;

	/**
	 * Indicates if the message was acknowledged.
	 */
	private Boolean acknowledged;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageType getType() {
		return this.type;
	}


	public void setType(SwiftMessageType type) {
		this.type = type;
	}


	public SwiftIdentifier getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(SwiftIdentifier identifier) {
		this.identifier = identifier;
	}


	public SwiftMessageStatus getMessageStatus() {
		return this.messageStatus;
	}


	public void setMessageStatus(SwiftMessageStatus messageStatus) {
		this.messageStatus = messageStatus;
	}


	public Date getMessageDateAndTime() {
		return this.messageDateAndTime;
	}


	public void setMessageDateAndTime(Date messageDateAndTime) {
		this.messageDateAndTime = messageDateAndTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public SwiftMessageFormats getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(SwiftMessageFormats messageFormat) {
		this.messageFormat = messageFormat;
	}


	public boolean isIncoming() {
		return this.incoming;
	}


	public void setIncoming(boolean incoming) {
		this.incoming = incoming;
	}


	public String getSenderBusinessIdentifierCode() {
		return this.senderBusinessIdentifierCode;
	}


	public void setSenderBusinessIdentifierCode(String senderBusinessIdentifierCode) {
		this.senderBusinessIdentifierCode = senderBusinessIdentifierCode;
	}


	public String getReceiverBusinessIdentifierCode() {
		return this.receiverBusinessIdentifierCode;
	}


	public void setReceiverBusinessIdentifierCode(String receiverBusinessIdentifierCode) {
		this.receiverBusinessIdentifierCode = receiverBusinessIdentifierCode;
	}


	public Boolean getAcknowledged() {
		return this.acknowledged;
	}


	public void setAcknowledged(Boolean acknowledged) {
		this.acknowledged = acknowledged;
	}
}
