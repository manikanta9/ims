package com.clifton.swift.message;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>SwiftMessageField</code> defines a SwiftMessageField field used to display a Swift message is a readable fashion on the UI.
 */
public class SwiftMessageField implements Serializable {

	private String label;
	private String tag;
	private String value;
	private String formatted;

	private List<SwiftMessageField> children;


	public SwiftMessageField(String label) {
		this(label, null);
	}


	public SwiftMessageField(String label, String tag) {
		this(label, tag, null);
	}


	public SwiftMessageField(String label, String tag, String value) {
		this(label, tag, value, null);
	}


	public SwiftMessageField(String label, String tag, String value, String formatted) {
		this.label = label;
		this.tag = tag;
		this.value = value;
		this.formatted = formatted;
	}


	public boolean isLeaf() {
		return getChildren() == null || getChildren().isEmpty();
	}


	@Override
	public String toString() {
		return this.label + "\t" + this.value + "\t" + this.formatted;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getTag() {
		return this.tag;
	}


	public void setTag(String tag) {
		this.tag = tag;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public String getFormatted() {
		return this.formatted;
	}


	public void setFormatted(String fieldEnumValue) {
		this.formatted = fieldEnumValue;
	}


	public List<SwiftMessageField> getChildren() {
		return this.children;
	}


	public void setChildren(List<SwiftMessageField> childFieldList) {
		this.children = childFieldList;
	}
}
