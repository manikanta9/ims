package com.clifton.swift.message.converter;

import com.clifton.swift.message.SwiftMessageFormats;


/**
 * @author mwacker
 */
public interface SwiftMessageConverterLocator {

	@SuppressWarnings("rawtypes")
	public <T> SwiftMessageConverter locate(T messageObject, SwiftMessageFormats messageFormat);
}
