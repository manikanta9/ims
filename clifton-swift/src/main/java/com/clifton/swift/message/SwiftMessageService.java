package com.clifton.swift.message;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.message.search.SwiftMessageSourceEntitySearchForm;
import com.clifton.swift.message.search.SwiftMessageStatusSearchForm;
import com.clifton.swift.message.search.SwiftMessageTypeSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * <code>SwiftMessageService</code> business service methods dealing Swift message saving and finding.
 */
public interface SwiftMessageService {

	public static final String MT_ACK_NAK_MESSAGE_TYPE_NAME = "MT ACK/NAK";
	public static final String MT_UNKNOWN_MESSAGE_TYPE_NAME = "Unknown System Messages";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessage getSwiftMessage(long id);


	@SecureMethod(dtoClass = SwiftMessage.class)
	public List<SwiftMessageField> getSwiftMessageFieldList(long id);


	public List<SwiftMessage> getSwiftMessageList(SwiftMessageSearchForm searchForm);


	public List<SwiftMessage> getSwiftMessageListBySourceEntity(SwiftMessageSourceEntitySearchForm searchForm);


	@DoNotAddRequestMapping
	public SwiftMessage saveSwiftMessage(SwiftMessage swiftMessage);


	/**
	 * Reprocess does NOT mean resubmit to the Swift network; attempt to parse the messages,
	 * determine the message status and put the message status response back on the synchronous
	 * message queue for processing by IMS.
	 */
	@RequestMapping("swiftReprocessMessageList")
	public void reprocessSwiftMessageList(long[] messageIdList);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageType getSwiftMessageType(short id);


	public SwiftMessageType getSwiftMessageTypeByName(String name);


	public SwiftMessageType getSwiftMessageTypeVersionCode(String versionName);


	public List<SwiftMessageType> getSwiftMessageTypeList(SwiftMessageTypeSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageStatus getSwiftMessageStatus(short id);


	public SwiftMessageStatus getSwiftMessageStatusByName(String name);


	public List<SwiftMessageStatus> getSwiftMessageStatusList(SwiftMessageStatusSearchForm searchForm);
}
