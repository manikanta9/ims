package com.clifton.swift.message.bic;

import java.io.Serializable;
import java.util.Objects;


public class SwiftBicDetail implements Serializable {

	public String code;
	public String branch;
	public String institutionName;
	public String branchInformation;
	public String cityHeading;
	public String subtypeIndication;
	public String valueAddedServices;
	public String extraInformation;
	public String physicalAddressPart1;
	public String physicalAddressPart2;
	public String physicalAddressPart3;
	public String physicalAddressPart4;
	public String location;
	public String countryName;
	public String POBNumber;
	public String POBLocation;
	public String POBCountryName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SwiftBicDetail that = (SwiftBicDetail) o;
		return Objects.equals(this.code, that.code);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.code);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCode() {
		return this.code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getBranch() {
		return this.branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getInstitutionName() {
		return this.institutionName;
	}


	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}


	public String getBranchInformation() {
		return this.branchInformation;
	}


	public void setBranchInformation(String branchInformation) {
		this.branchInformation = branchInformation;
	}


	public String getCityHeading() {
		return this.cityHeading;
	}


	public void setCityHeading(String cityHeading) {
		this.cityHeading = cityHeading;
	}


	public String getSubtypeIndication() {
		return this.subtypeIndication;
	}


	public void setSubtypeIndication(String subtypeIndication) {
		this.subtypeIndication = subtypeIndication;
	}


	public String getValueAddedServices() {
		return this.valueAddedServices;
	}


	public void setValueAddedServices(String valueAddedServices) {
		this.valueAddedServices = valueAddedServices;
	}


	public String getExtraInformation() {
		return this.extraInformation;
	}


	public void setExtraInformation(String extraInformation) {
		this.extraInformation = extraInformation;
	}


	public String getPhysicalAddressPart1() {
		return this.physicalAddressPart1;
	}


	public void setPhysicalAddressPart1(String physicalAddressPart1) {
		this.physicalAddressPart1 = physicalAddressPart1;
	}


	public String getPhysicalAddressPart2() {
		return this.physicalAddressPart2;
	}


	public void setPhysicalAddressPart2(String physicalAddressPart2) {
		this.physicalAddressPart2 = physicalAddressPart2;
	}


	public String getPhysicalAddressPart3() {
		return this.physicalAddressPart3;
	}


	public void setPhysicalAddressPart3(String physicalAddressPart3) {
		this.physicalAddressPart3 = physicalAddressPart3;
	}


	public String getPhysicalAddressPart4() {
		return this.physicalAddressPart4;
	}


	public void setPhysicalAddressPart4(String physicalAddressPart4) {
		this.physicalAddressPart4 = physicalAddressPart4;
	}


	public String getLocation() {
		return this.location;
	}


	public void setLocation(String location) {
		this.location = location;
	}


	public String getCountryName() {
		return this.countryName;
	}


	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}


	public String getPOBNumber() {
		return this.POBNumber;
	}


	public void setPOBNumber(String POBNumber) {
		this.POBNumber = POBNumber;
	}


	public String getPOBLocation() {
		return this.POBLocation;
	}


	public void setPOBLocation(String POBLocation) {
		this.POBLocation = POBLocation;
	}


	public String getPOBCountryName() {
		return this.POBCountryName;
	}


	public void setPOBCountryName(String POBCountryName) {
		this.POBCountryName = POBCountryName;
	}
}
