package com.clifton.swift.message.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * @author mwacker
 */
public class SwiftMessageTypeSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String description;

	@SearchField
	private String mtVersionName;

	@SearchField
	private String mxVersionName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getMtVersionName() {
		return this.mtVersionName;
	}


	public void setMtVersionName(String mtVersionName) {
		this.mtVersionName = mtVersionName;
	}


	public String getMxVersionName() {
		return this.mxVersionName;
	}


	public void setMxVersionName(String mxVersionName) {
		this.mxVersionName = mxVersionName;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}
}
