package com.clifton.swift.message;

import com.clifton.core.beans.NamedSimpleEntity;


/**
 * The SWIFT message type.  For example, there will be an entry for "General Financial Institution Transfer" with MT version of MT202 and MX version of MxPacs00900102.
 *
 * @author mwacker
 */
public class SwiftMessageType extends NamedSimpleEntity<Short> {

	public static final String UNKNOWN_MESSAGE_TYPE_NAME = "Unknown System Messages";

	/**
	 * The name of the MT SWIFT message
	 */
	private String mtVersionName;

	/**
	 * The name of the MX SWIFT message
	 */
	private String mxVersionName;

	/**
	 * Indicates that the messages of this type are system messages.  For example, SWIFT ACK/NAK.
	 */
	private boolean systemMessage;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getMtVersionName() {
		return this.mtVersionName;
	}


	public void setMtVersionName(String mtVersionName) {
		this.mtVersionName = mtVersionName;
	}


	public String getMxVersionName() {
		return this.mxVersionName;
	}


	public void setMxVersionName(String mxVersionName) {
		this.mxVersionName = mxVersionName;
	}


	public boolean isSystemMessage() {
		return this.systemMessage;
	}


	public void setSystemMessage(boolean systemMessage) {
		this.systemMessage = systemMessage;
	}
}
