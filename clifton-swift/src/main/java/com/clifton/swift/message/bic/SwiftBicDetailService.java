package com.clifton.swift.message.bic;

import com.clifton.core.security.authorization.SecureMethod;


public interface SwiftBicDetailService {

	@SecureMethod(securityResource = "SWIFT")
	public SwiftBicDetail getSwiftBicDetails(String code);
}
