package com.clifton.swift.message.converter;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.message.SwiftMessageFormats;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <code>SwiftMessageConverterLocatorImpl</code> cache the {@link SwiftMessageConverter} instances and provide accessors for fetching
 * the correct converter for the intended message type.
 */
@SuppressWarnings("rawtypes")
@Component
public class SwiftMessageConverterLocatorImpl implements SwiftMessageConverterLocator, InitializingBean, ApplicationContextAware {

	private final Map<SwiftMessageFormats, Map<Class<?>, SwiftMessageConverter>> converterMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings({"rawtypes"})
	public void afterPropertiesSet() {
		Map<String, SwiftMessageConverter> beanMap = getApplicationContext().getBeansOfType(SwiftMessageConverter.class);

		// need a map with journal type names as keys instead of bean names
		for (Map.Entry<String, SwiftMessageConverter> stringSwiftMessageConverterEntry : beanMap.entrySet()) {
			SwiftMessageConverter converter = stringSwiftMessageConverterEntry.getValue();

			Map<Class<?>, SwiftMessageConverter> typeConverterMap = getConverterMap().computeIfAbsent(converter.getSupportedMessageFormat(), format -> new ConcurrentHashMap<>());

			ValidationUtils.assertNotNull(converter.getNativeSwiftClass(), "The converter [" + converter.getClass().getName() + "] must provide a native message class.");
			ValidationUtils.assertFalse(typeConverterMap.containsKey(converter.getNativeSwiftClass()), "A convert for [" + converter.getNativeSwiftClass() + "] is already registered.  Cannot register [" + converter.getClass().getName() + "].");
			typeConverterMap.putIfAbsent(converter.getNativeSwiftClass(), converter);

			ValidationUtils.assertNotNull(converter.getInternalMessageClass(), "The converter [" + converter.getClass().getName() + "] must provide an internal message class.");
			ValidationUtils.assertFalse(typeConverterMap.containsKey(converter.getInternalMessageClass()), "A convert for [" + converter.getInternalMessageClass() + "] is already registered.  Cannot register [" + converter.getClass().getName() + "].");
			typeConverterMap.putIfAbsent(converter.getInternalMessageClass(), converter);
		}
	}


	@Override
	public <T> SwiftMessageConverter locate(T messageObject, SwiftMessageFormats messageFormat) {
		AssertUtils.assertNotNull(messageObject, "Required message object cannot be null.");

		SwiftMessageConverter result = getConverterMap().computeIfAbsent(messageFormat, format -> new ConcurrentHashMap<>()).get(messageObject.getClass());
		AssertUtils.assertNotNull(result, "Cannot locate SwiftMessageConverter for message object of type '%1s'.", messageObject.getClass().getName());

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<SwiftMessageFormats, Map<Class<?>, SwiftMessageConverter>> getConverterMap() {
		return this.converterMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
