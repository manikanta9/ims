package com.clifton.swift.message.converter;


import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.swift.message.SwiftMessageFormats;

import java.util.Map;


/**
 * The <code>SwiftMessageConverterHandler</code> defines a component that will be used to
 * parse incoming messages to the defined format and convert them to and from InstructionEntities.
 * This handler should have a registry of SwiftMessageConverters which will be used to get the
 * correct converter based of the instruction or message class.
 *
 * @author mwacker
 */
public interface SwiftMessageConverterHandler<M> {

	public static final String SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED = "Swift message format type is not supported.";
	public static final String SWIFT_MESSAGE_DELIMITER = "\\$";


	/**
	 * Parse a message to the implemented format.
	 */
	public Map<String, M> parse(String text, SwiftMessageFormats messageFormat);


	/**
	 * Convert a native message to an internal Swift entity.
	 */
	public <F extends InstructionMessage, Q extends M> F toInstructionEntity(Q message);


	/**
	 * Convert an internal Swift entity to the native format.
	 */
	public <Q extends M, F extends InstructionMessage> Q fromInstructionEntity(F entity, SwiftMessageFormats messageFormat);


	/**
	 * Convert to a SWIFT message string.
	 */
	public <F extends InstructionMessage> String toSwiftMessage(F entity, SwiftMessageFormats messageFormat);
}
