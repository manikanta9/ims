package com.clifton.swift.message;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * <code>SwiftMessageStatus</code> tracks the status of Swift Messages as they go through stages of being sent and received
 * from the SWIFT network.
 * <ul>
 * <li>LOGGED - The message has been logged</li>
 * <li>QUEUED - The message has been sent to Spring Integration to be queued up before outputting to the file system.</li>
 * <li>SENT - The message(s) file has been uploaded to the output folder.</li>
 * <li>ERROR - An error prevented a messages from being processed successfully.</li>
 * <li>COMPLETE - The message has been successfully processed.</li>
 * </ul>
 */
@CacheByName
public class SwiftMessageStatus extends NamedEntity<Short> {

	public enum SwiftMessageStatusNames {
		LOGGED,
		QUEUED,
		SENT,
		ERROR,
		COMPLETE
	}


	public SwiftMessageStatusNames getSwiftMessageStatusName() {
		return SwiftMessageStatusNames.valueOf(getName());
	}
}
