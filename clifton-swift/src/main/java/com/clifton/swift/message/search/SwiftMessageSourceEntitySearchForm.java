package com.clifton.swift.message.search;

import com.clifton.core.dataaccess.search.form.SearchForm;


@SearchForm(hasOrmDtoClass = false)
public class SwiftMessageSourceEntitySearchForm extends SwiftMessageSearchForm {

	private Integer sourceEntityId;
	private String systemTableName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSourceEntityId() {
		return this.sourceEntityId;
	}


	public void setSourceEntityId(Integer sourceEntityId) {
		this.sourceEntityId = sourceEntityId;
	}


	public String getSystemTableName() {
		return this.systemTableName;
	}


	public void setSystemTableName(String systemTableName) {
		this.systemTableName = systemTableName;
	}
}
