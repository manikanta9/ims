package com.clifton.swift.message;

/**
 * <code>SwiftMessageProcessor</code> defines methods used to add message items to a queue
 * which will process the messages into the SwiftMessage table.
 */
public interface SwiftMessageProcessor<T> {

	public void processMessage(T message);
}
