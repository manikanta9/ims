package com.clifton.swift.message.converter;

import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.swift.message.SwiftMessageFormats;


/**
 * Defines a convert that can convert and object from a InstructionEntity to a SWIFT message entity and back.
 *
 * @author mwacker
 */
public interface SwiftMessageConverter<E extends InstructionMessage, S> {

	/**
	 * The internal message class the implements InstructionMessage.
	 * <p>
	 * NOTE: This is used to register the converter for a specific message type.
	 */
	public Class<?> getInternalMessageClass();


	/**
	 * The external message class.  For example, a prowide MT202 object which is of
	 * type "com.prowidesoftware.swift.model.mt.mt2xx.MT202".
	 * <p>
	 * NOTE: This is used to register the converter for a specific message type.
	 */
	public Class<?> getNativeSwiftClass();


	/**
	 * Return the supported SwiftMessageFormats, either MX or MT
	 */
	public SwiftMessageFormats getSupportedMessageFormat();


	/**
	 * Convert a internal instruction message to a SWIFT message string.
	 */
	public String toSwiftMessage(E entity);


	/**
	 * Convert a internal instruction message to the native SWIFT object.
	 */
	public S toSwiftEntity(E entity);


	/**
	 * Convert native SWIFT object to an internal instruction entity.
	 * <p><p>
	 * <b>NOTE: When creating these methods field ORDER matters!</b>
	 */
	public E fromSwiftEntity(S swiftEntity);
}
