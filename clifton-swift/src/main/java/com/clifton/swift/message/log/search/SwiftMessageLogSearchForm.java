package com.clifton.swift.message.log.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.swift.message.SwiftMessageFormats;

import java.util.Date;


/**
 * @author mwacker
 */
public class SwiftMessageLogSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "id")
	private Long id;

	@SearchField
	public String text;

	@SearchField
	public String errorMessage;

	@SearchField
	public Date messageDateAndTime;

	@SearchField
	private Boolean incoming;

	@SearchField
	private SwiftMessageFormats messageFormat;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public String getErrorMessage() {
		return this.errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public Date getMessageDateAndTime() {
		return this.messageDateAndTime;
	}


	public void setMessageDateAndTime(Date messageDateAndTime) {
		this.messageDateAndTime = messageDateAndTime;
	}


	public Boolean getIncoming() {
		return this.incoming;
	}


	public void setIncoming(Boolean incoming) {
		this.incoming = incoming;
	}


	public SwiftMessageFormats getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(SwiftMessageFormats messageFormat) {
		this.messageFormat = messageFormat;
	}
}
