package com.clifton.swift.message;

/**
 * SWIFT Message Type Formats
 *
 * @author mwacker
 */
public enum SwiftMessageFormats {
	/**
	 * XML Style Swift Message Type
	 */
	MX,

	/**
	 * Text base SWIFT message.
	 */
	MT;


	public static SwiftMessageFormats fromIsMt(boolean isMT) {
		return isMT ? SwiftMessageFormats.MT : SwiftMessageFormats.MX;
	}
}
