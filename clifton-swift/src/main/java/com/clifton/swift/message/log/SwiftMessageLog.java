package com.clifton.swift.message.log;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.swift.message.SwiftMessageFormats;

import java.util.Date;


/**
 * A temporary log table for incoming and out going messages.
 *
 * @author mwacker
 */
public class SwiftMessageLog extends BaseSimpleEntity<Long> {

	/**
	 * The date and time that message was sent.
	 */
	private Date messageDateAndTime;

	/**
	 * The MX or MT SWIFT message text.
	 */
	private String text;

	/**
	 * Indicated if the message was sent or received by the sent.
	 */
	private boolean incoming;

	/**
	 * Any error that occured while processing the message from the log to the message table.
	 */
	private String errorMessage;


	/**
	 * The message format of the log entry.
	 */
	private SwiftMessageFormats messageFormat;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getMessageDateAndTime() {
		return this.messageDateAndTime;
	}


	public void setMessageDateAndTime(Date messageDateAndTime) {
		this.messageDateAndTime = messageDateAndTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public boolean isIncoming() {
		return this.incoming;
	}


	public void setIncoming(boolean incoming) {
		this.incoming = incoming;
	}


	public String getErrorMessage() {
		return this.errorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public SwiftMessageFormats getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(SwiftMessageFormats messageFormat) {
		this.messageFormat = messageFormat;
	}
}
