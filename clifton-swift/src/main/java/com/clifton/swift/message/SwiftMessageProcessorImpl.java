package com.clifton.swift.message;

import com.clifton.core.util.AssertUtils;


/**
 * <code>SwiftMessageProcessorImpl</code>
 */
public class SwiftMessageProcessorImpl<T> implements SwiftMessageProcessor<T> {

	@Override
	public void processMessage(T message) {
		AssertUtils.fail("Method is not implemented");
	}
}
