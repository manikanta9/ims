package com.clifton.swift.message.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.swift.message.SwiftMessageFormats;

import java.util.Date;


/**
 * @author mwacker
 */
public class SwiftMessageSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "id")
	private Long id;

	@SearchField(searchField = "type.id")
	public Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "type")
	public String typeName;

	@SearchField(searchField = "mtVersionName", searchFieldPath = "type", comparisonConditions = ComparisonConditions.LIKE)
	public String mtVersionName;

	@SearchField(searchField = "mxVersionName", searchFieldPath = "type", comparisonConditions = ComparisonConditions.LIKE)
	public String mxVersionName;

	@SearchField(searchField = "uniqueStringIdentifier", searchFieldPath = "identifier")
	public String uniqueStringIdentifier;

	@SearchField(searchField = "identifier.id")
	public Long identifierId;

	@SearchField(searchField = "identifier.id")
	private Long[] identifierIds;

	@SearchField(searchField = "sourceSystemFkFieldId", searchFieldPath = "identifier")
	public Long sourceSystemFkFieldId;

	@SearchField(searchField = "sourceSystemFkFieldId", searchFieldPath = "identifier", comparisonConditions = ComparisonConditions.IN)
	public Long[] sourceSystemFkFieldIdList;

	@SearchField(searchField = "name", searchFieldPath = "identifier.sourceSystemModule", comparisonConditions = ComparisonConditions.EQUALS)
	public String swiftSourceSystemModuleNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "identifier.sourceSystemModule.sourceSystem", comparisonConditions = ComparisonConditions.EQUALS)
	public String swiftSourceSystemNameEquals;

	@SearchField
	public String text;

	@SearchField
	public Date messageDateAndTime;

	@SearchField
	public SwiftMessageFormats messageFormat;

	@SearchField
	private String senderBusinessIdentifierCode;

	@SearchField
	private String receiverBusinessIdentifierCode;

	@SearchField
	private Boolean incoming;

	@SearchField
	private Boolean acknowledged;

	@SearchField(searchField = "messageStatus.id")
	public Short messageStatusId;

	@SearchField(searchField = "messageStatus.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	public Short excludeMessageStatusId;

	@SearchField(searchField = "messageStatus.id", comparisonConditions = ComparisonConditions.IN)
	public Short[] messageStatusIdList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.uniqueStringIdentifier = uniqueStringIdentifier;
	}


	public Long getIdentifierId() {
		return this.identifierId;
	}


	public void setIdentifierId(Long identifierId) {
		this.identifierId = identifierId;
	}


	public Long[] getIdentifierIds() {
		return this.identifierIds;
	}


	public void setIdentifierIds(Long[] identifierIds) {
		this.identifierIds = identifierIds;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}


	public Long[] getSourceSystemFkFieldIdList() {
		return this.sourceSystemFkFieldIdList;
	}


	public void setSourceSystemFkFieldIdList(Long[] sourceSystemFkFieldIdList) {
		this.sourceSystemFkFieldIdList = sourceSystemFkFieldIdList;
	}


	public String getSwiftSourceSystemModuleNameEquals() {
		return this.swiftSourceSystemModuleNameEquals;
	}


	public void setSwiftSourceSystemModuleNameEquals(String swiftSourceSystemModuleNameEquals) {
		this.swiftSourceSystemModuleNameEquals = swiftSourceSystemModuleNameEquals;
	}


	public String getSwiftSourceSystemNameEquals() {
		return this.swiftSourceSystemNameEquals;
	}


	public void setSwiftSourceSystemNameEquals(String swiftSourceSystemNameEquals) {
		this.swiftSourceSystemNameEquals = swiftSourceSystemNameEquals;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getMessageDateAndTime() {
		return this.messageDateAndTime;
	}


	public void setMessageDateAndTime(Date messageDateAndTime) {
		this.messageDateAndTime = messageDateAndTime;
	}


	public SwiftMessageFormats getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(SwiftMessageFormats messageFormat) {
		this.messageFormat = messageFormat;
	}


	public String getSenderBusinessIdentifierCode() {
		return this.senderBusinessIdentifierCode;
	}


	public void setSenderBusinessIdentifierCode(String senderBusinessIdentifierCode) {
		this.senderBusinessIdentifierCode = senderBusinessIdentifierCode;
	}


	public String getReceiverBusinessIdentifierCode() {
		return this.receiverBusinessIdentifierCode;
	}


	public void setReceiverBusinessIdentifierCode(String receiverBusinessIdentifierCode) {
		this.receiverBusinessIdentifierCode = receiverBusinessIdentifierCode;
	}


	public Boolean getIncoming() {
		return this.incoming;
	}


	public void setIncoming(Boolean incoming) {
		this.incoming = incoming;
	}


	public String getMtVersionName() {
		return this.mtVersionName;
	}


	public void setMtVersionName(String mtVersionName) {
		this.mtVersionName = mtVersionName;
	}


	public String getMxVersionName() {
		return this.mxVersionName;
	}


	public void setMxVersionName(String mxVersionName) {
		this.mxVersionName = mxVersionName;
	}


	public Boolean getAcknowledged() {
		return this.acknowledged;
	}


	public void setAcknowledged(Boolean acknowledged) {
		this.acknowledged = acknowledged;
	}


	public Short getMessageStatusId() {
		return this.messageStatusId;
	}


	public void setMessageStatusId(Short messageStatusId) {
		this.messageStatusId = messageStatusId;
	}


	public Short getExcludeMessageStatusId() {
		return this.excludeMessageStatusId;
	}


	public void setExcludeMessageStatusId(Short excludeMessageStatusId) {
		this.excludeMessageStatusId = excludeMessageStatusId;
	}


	public Short[] getMessageStatusIdList() {
		return this.messageStatusIdList;
	}


	public void setMessageStatusIdList(Short[] messageStatusIdList) {
		this.messageStatusIdList = messageStatusIdList;
	}
}
