package com.clifton.swift.message.log;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.message.log.search.SwiftMessageLogSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * @author mwacker
 */
public interface SwiftMessageLogService {

	public SwiftMessageLog getSwiftMessageLog(long id);


	public List<SwiftMessageLog> getSwiftMessageLogList(SwiftMessageLogSearchForm searchForm);


	public SwiftMessageLog saveSwiftMessageLog(SwiftMessageLog bean);


	public void deleteSwiftMessageLog(long id);


	@RequestMapping("swiftReprocessMessageLogList")
	@SecureMethod(dtoClass = SwiftMessageLog.class)
	public void reprocessSwiftMessageList(long[] messageLogIdList);


	@SecureMethod(securityResource = "SWIFT")
	public List<SwiftMessageField> getSwiftMessageLogFieldList(long id);
}
