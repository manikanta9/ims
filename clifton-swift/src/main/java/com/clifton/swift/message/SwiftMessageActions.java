package com.clifton.swift.message;

/**
 * @author tstebner
 */
public enum SwiftMessageActions {

	/**
	 * Reprocess the SWIFT message from the message table.
	 */
	REPROCESS_SWIFT_MESSAGE,

	/**
	 * Reprocess the SWIFT message from the message log table to message table.
	 */
	REPROCESS_SWIFT_LOG_MESSAGE
}
