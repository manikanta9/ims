package com.clifton.swift.system;

import com.clifton.core.beans.NamedSimpleEntity;
import com.clifton.swift.message.SwiftMessageFormats;


/**
 * Represents a module in source system.  For example, source system could be IMS and
 * the module could be M2M.  The source system should be parsable from a format list IMS:M2M:123456.
 *
 * @author mwacker
 */
public class SwiftSourceSystemModule extends NamedSimpleEntity<Short> {

	private SwiftSourceSystem sourceSystem;
	/**
	 * The target SWIFT message format.  If a message is received is a different format, it will be converted.
	 */
	private SwiftMessageFormats targetMessageFormat;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftSourceSystem getSourceSystem() {
		return this.sourceSystem;
	}


	public void setSourceSystem(SwiftSourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}


	public SwiftMessageFormats getTargetMessageFormat() {
		return this.targetMessageFormat;
	}


	public void setTargetMessageFormat(SwiftMessageFormats targetMessageFormat) {
		this.targetMessageFormat = targetMessageFormat;
	}
}
