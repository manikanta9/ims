package com.clifton.swift.system;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.system.search.SwiftSourceSystemModuleSearchForm;
import com.clifton.swift.system.search.SwiftSourceSystemSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * {@inheritDoc}
 */
@Service
public class SwiftSourceSystemServiceImpl implements SwiftSourceSystemService {

	private AdvancedUpdatableDAO<SwiftSourceSystem, Criteria> swiftSourceSystemDAO;
	private AdvancedUpdatableDAO<SwiftSourceSystemModule, Criteria> swiftSourceSystemModuleDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftSourceSystem getSwiftSourceSystem(Short id) {
		return getSwiftSourceSystemDAO().findByPrimaryKey(id);
	}


	@Override
	public SwiftSourceSystem saveSwiftSourceSystem(SwiftSourceSystem swiftSourceSystem) {
		return getSwiftSourceSystemDAO().save(swiftSourceSystem);
	}


	@Override
	public SwiftSourceSystem getSwiftSourceSystemByName(String sourceSystemName) {
		return getSwiftSourceSystemDAO().findOneByField("name", sourceSystemName);
	}


	@Override
	public SwiftSourceSystem getSwiftSourceSystemByBIC(String sourceSystemBIC) {
		SwiftSourceSystemSearchForm searchForm = new SwiftSourceSystemSearchForm();
		searchForm.setBusinessIdentifierCode(sourceSystemBIC);
		return CollectionUtils.getFirstElement(getSwiftSourceSystemList(searchForm));
	}


	@Override
	public List<SwiftSourceSystem> getSwiftSourceSystemList(SwiftSourceSystemSearchForm searchForm) {
		return getSwiftSourceSystemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftSourceSystemModule getSwiftSourceSystemModule(Short id) {
		return getSwiftSourceSystemModuleDAO().findByPrimaryKey(id);
	}


	@Override
	public SwiftSourceSystemModule saveSwiftSourceSystemModule(SwiftSourceSystemModule swiftSourceSystemModule) {
		return getSwiftSourceSystemModuleDAO().save(swiftSourceSystemModule);
	}


	@Override
	public List<SwiftSourceSystemModule> getSwiftSourceSystemModuleList(SwiftSourceSystemModuleSearchForm searchForm) {
		return getSwiftSourceSystemModuleDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SwiftSourceSystemModule getSwiftSourceSystemModuleByName(String sourceSystemModuleName, short sourceSystemId) {
		SwiftSourceSystemModuleSearchForm searchForm = new SwiftSourceSystemModuleSearchForm();
		searchForm.setName(sourceSystemModuleName);
		searchForm.setSourceSystemId(sourceSystemId);
		return CollectionUtils.getFirstElementStrict(getSwiftSourceSystemModuleList(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SwiftSourceSystem, Criteria> getSwiftSourceSystemDAO() {
		return this.swiftSourceSystemDAO;
	}


	public void setSwiftSourceSystemDAO(AdvancedUpdatableDAO<SwiftSourceSystem, Criteria> swiftSourceSystemDAO) {
		this.swiftSourceSystemDAO = swiftSourceSystemDAO;
	}


	public AdvancedUpdatableDAO<SwiftSourceSystemModule, Criteria> getSwiftSourceSystemModuleDAO() {
		return this.swiftSourceSystemModuleDAO;
	}


	public void setSwiftSourceSystemModuleDAO(AdvancedUpdatableDAO<SwiftSourceSystemModule, Criteria> swiftSourceSystemModuleDAO) {
		this.swiftSourceSystemModuleDAO = swiftSourceSystemModuleDAO;
	}
}
