package com.clifton.swift.system;

import com.clifton.core.beans.NamedSimpleEntity;


/**
 * Defines a source system that can send SWIFT messages.  For example, IMS.
 *
 * @author mwacker
 */
public class SwiftSourceSystem extends NamedSimpleEntity<Short> {

	/**
	 * The BIC code for the source system.  Used to identify the source system for incoming messages.
	 */
	private String businessIdentifierCode;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBusinessIdentifierCode() {
		return this.businessIdentifierCode;
	}


	public void setBusinessIdentifierCode(String businessIdentifierCode) {
		this.businessIdentifierCode = businessIdentifierCode;
	}
}
