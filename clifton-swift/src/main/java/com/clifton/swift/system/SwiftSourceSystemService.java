package com.clifton.swift.system;

import com.clifton.swift.system.search.SwiftSourceSystemModuleSearchForm;
import com.clifton.swift.system.search.SwiftSourceSystemSearchForm;

import java.util.List;


/**
 * <code>SwiftSourceSystemService</code> methods for fetching and saving both {@link SwiftSourceSystem} and {@link SwiftSourceSystemModule} entities.
 */
public interface SwiftSourceSystemService {

	public SwiftSourceSystemModule getSwiftSourceSystemModule(Short id);


	public SwiftSourceSystemModule saveSwiftSourceSystemModule(SwiftSourceSystemModule swiftSourceSystemModule);


	public List<SwiftSourceSystemModule> getSwiftSourceSystemModuleList(SwiftSourceSystemModuleSearchForm searchForm);


	public SwiftSourceSystemModule getSwiftSourceSystemModuleByName(String sourceSystemModuleName, short sourceSystemId);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftSourceSystem getSwiftSourceSystem(Short id);


	public SwiftSourceSystem saveSwiftSourceSystem(SwiftSourceSystem swiftSourceSystem);


	public SwiftSourceSystem getSwiftSourceSystemByName(String sourceSystemName);


	/**
	 * Get the source system using the BIC code.
	 */
	public SwiftSourceSystem getSwiftSourceSystemByBIC(String sourceSystemBIC);


	public List<SwiftSourceSystem> getSwiftSourceSystemList(SwiftSourceSystemSearchForm searchForm);
}
