package com.clifton.swift.converter.beans;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;


/**
 * <code>MessageReportErrorValues</code> 2.8 Report Errors, The following codes are returned in field 461 of Delivery Subset Status Reports, Undelivered Message Reports,
 * and Undelivered SSI Update Notification Reports.
 */
public enum MessageReportErrorValues {

	EMPTY_REPORT("001", "Empty report"),
	END_UNDELIVERED_REPORT("002", "End of undelivered report"),
	SYSTEM_RECOVERY("003", "System undergoing major recovery or system not completely synchronised yet"),
	TOO_MANY_UNDELIVERED_MSGS("004", "Too many undelivered messages"),
	FALL_BACK_REGIONAL_PROCESSOR("005", "User on fall back Regional Processor, cannot generate report"),
	REFERENCED_MESSAGE_MISSING("006", "The message referenced in the request could not be found."),
	INVALID_RPT_DESTINATION("007", "Invalid destination for report. The sender of the request must be the same as the sender of the message referenced in the request."),
	NO_MT671_FOUND("008", "No MTs 671 were found for the referenced MT 670."),
	TERMINAL_INVALID_STATE("009", "Requesting logical terminal in invalid state"),
	BRANCH_CODE_INVALID("016", "Branch code is not 'XXX'"),
	SYSTEM_INTERNAL_PROBLEMS("099", "System internal problems, contact your Customer Support Center");

	private static final Pattern pattern = Pattern.compile("^[0-9]{3}+$");

	private final String code;
	private final String label;


	MessageReportErrorValues(String code, String label) {
		this.code = code;
		this.label = label;
	}


	public static MessageReportErrorValues fromCode(String code) {
		Optional<MessageReportErrorValues> value = Arrays
				.stream(MessageReportErrorValues.values())
				.filter(v -> Objects.equals(v.getCode(), code))
				.findFirst();
		return value.isPresent() ? value.get() : null;
	}


	public static boolean validateCode(String code) {
		return pattern.matcher(code).matches();
	}


	public static MessageReportErrorValues getDefault() {
		return SYSTEM_INTERNAL_PROBLEMS;
	}


	public String getCode() {
		return this.code;
	}


	public String getLabel() {
		return this.label;
	}
}
