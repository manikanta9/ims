package com.clifton.swift.converter.beans;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;


/**
 * <code>MessageAbortReasonValues</code> 2.6 Abort Reasons, The following codes are returned in field 432 of abort notifications and, for the FINCopy service, Message Refusals.
 */
public enum MessageAbortReasonValues {

	MESSAGE_TOO_OLD("01", "Message too old (remained undelivered for n days)"),
	TOO_MANY_UNSUCCESSFUL_ATTEMPTS("02", "Too many unsuccessful delivery attempts"),
	DESTINATION_DISABLED("03", "Destination disabled"),
	OPERATION_ABORTED("04", "Operator aborted"),
	MESSAGE_RECOVERY_DECRYPTION_FAILURE("05", "Message could not be recovered after a major system failure because it was user encrypted"),
	FIN_INTERFACE_MESSAGE_INCOMPATIBILITY("06", "Message type incompatible with the FIN interface mode"),
	MESSAGE_TOO_OLD_AUTHORIZED("11", "Message is too old, but was authorised"),
	TOO_MANY_DELIVERY_ATTEMPTS("12", "Too many delivery attempts, but message was authorised"),
	DESTINATION_DISABLED_AUTHORIZED("13", "Destination is disabled, but message was authorised"),
	MESSAGE_TOO_LONG_AUTHORIZED("14", "Message is too long, but was authorised"),
	MESSAGE_TOO_OLD_BYPASSED("21", "Message is too old and was bypassed"),
	TOO_MANY_DELIVERY_ATTEMPTS_BYPASSED("22", "Too many delivery attempts and the message was bypassed"),
	DESTINATION_DISABLED_BYPASSED("23", "Destination is disabled and the message was bypassed"),
	MESSAGE_TOO_LONG_BYPASSED("24", "Message is too long and was bypassed"),
	MESSAGE_HELD_FOR_APPROVAL("29", "Message held for approval prior to Bypass mode and aborted"),
	MESSAGE_TOO_OLD_NOT_AUTHORIZED("32", "Message is too old and was not authorised"),
	COPY_SERVICE_ABORTED("33", "Copy message to the copy service server was aborted"),
	FIN_COPY_PARAMS_INCORRECT("35", "FINCopy service parameter(s) incorrectly defined in FIN"),
	SYSTEM_ERROR("99", "System Error"),
	SANCTIONS_SCREENING_ABORTED("S1", "Sanctions screening service aborted on request of the subscribing user.");

	private static final Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{2}+$");

	private final String code;
	private final String label;


	MessageAbortReasonValues(String code, String label) {
		this.code = code;
		this.label = label;
	}


	public static MessageAbortReasonValues fromCode(String code) {
		Optional<MessageAbortReasonValues> value = Arrays
				.stream(MessageAbortReasonValues.values())
				.filter(v -> Objects.equals(v.getCode(), code))
				.findFirst();
		return value.isPresent() ? value.get() : null;
	}


	public static boolean validateCode(String code) {
		return pattern.matcher(code).matches();
	}


	public static MessageAbortReasonValues getDefault() {
		return SYSTEM_ERROR;
	}


	public String getCode() {
		return this.code;
	}


	public String getLabel() {
		return this.label;
	}
}
