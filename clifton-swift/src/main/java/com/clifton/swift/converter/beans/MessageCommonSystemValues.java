package com.clifton.swift.converter.beans;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;


/**
 * <code>MessageCommonSystemValues</code> code to label mappings for common message field values.
 */
public enum MessageCommonSystemValues {

	ACCEPTED("0", "Accepted"),
	REJECTED("1", "Rejected"),
	ALL_UNDELIVERED("RT", "All undelivered at report time"),
	UNDELIVERED_HOURS("nn", "Undelivered for more than nn hours (range: 00≤nn≤24)"),
	DATE_SENSITIVE_UNDELIVERED("VD", "Value-date-sensitive message undelivered after the Receiver's cut-off time on the value date."),
	REPORT_SENT_AFTER_RESTART("CS", "this report has been sent following a FIN cold start.");

	private final String code;
	private final String label;


	MessageCommonSystemValues(String code, String label) {
		this.code = code;
		this.label = label;
	}


	public static MessageCommonSystemValues fromCode(String code) {
		Optional<MessageCommonSystemValues> value = Arrays
				.stream(MessageCommonSystemValues.values())
				.filter(v -> Objects.equals(v.getCode(), code))
				.findFirst();
		return value.isPresent() ? value.get() : null;
	}


	public String getCode() {
		return this.code;
	}


	public String getLabel() {
		return this.label;
	}
}
