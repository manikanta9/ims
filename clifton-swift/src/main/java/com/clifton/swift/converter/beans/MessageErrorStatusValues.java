package com.clifton.swift.converter.beans;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;


/**
 * <code>MessageErrorStatusValues</code> 2.5 Message Status, The message status is returned in field 431 of non-delivery warnings,
 * undelivered message reports, and retrieved messages.
 */
public enum MessageErrorStatusValues {

	DELIVERED("01", "Delivered"),
	REJECTED("02", "Rejected by destinee"),
	ABORTED("04", "Aborted"),
	NO_DELIVERY("07", "No delivery attempt"),
	UNSUCCESSFUL_ATTEMPTS("08", "One or more unsuccessful attempts"),
	NOT_DELIVERED("10", "Message not delivered (that is no delivery history) for one of the following reasons: \nMessage negatively acknowledged on input \nMessage is Login, Select, Quit or Logout Request \nMessage was sent in local test mode (not deliverable) \n Message is  097 (processed directly by Slice Processor and never delivered to any logical terminal, and so does not have a delivery history)"),
	INTERRUPTED("12", "Intercepted"),
	HELD_FOR_APPROVAL("29", "Message held for approval prior to Bypass mode and aborted"),
	AUTHORIZED_BY_COPY_SERVICE("31", "Authorised by the copy service or Sanctions Screening over SWIFT service subscriber and delivered"),
	NOT_AUTHORIZED_BY_COPY_SERVICE("32", "Not authorised by the copy service or Sanctions Screening over SWIFT service subscriber and aborted by the system"),
	COPY_MESSAGE_ABORTED("33", "Copy message is aborted and not delivered to the copy service"),
	COPY_SERVICE_ABORTED("34", "Authorised by the copy service or Sanctions Screening over SWIFT service subscriber but aborted by the system"),
	REFUSED_BY_COPY_SERVICE("35", "Not yet authorised/refused by the copy service or Sanctions Screening over SWIFT service subscriber"),
	NO_DELIVERY_ATTEMPTED("37", "Authorised by the copy service or Sanctions Screening over SWIFT service subscriber but no delivery attempted"),
	UNSUCCESSFUL_DELIVERY_ATTEMPTS("38", "Authorised by the copy service or Sanctions Screening over SWIFT service subscriber but one or more unsuccessful delivery attempts"),
	BYPASSED_COPY_SERVICE_DELIVERY("41", "Copy service bypassed and message delivered"),
	BYPASSED_COPY_SERVICE_ABORTED("44", "Copy service bypassed but message aborted by the system"),
	BYPASSED_COPY_SERVICE_NO_DELIVERY("47", "Copy service bypassed but no delivery attempted"),
	BYPASSED_COPY_SERVICE_UNSUCCESSFUL("48", "Copy service bypassed but one or more unsuccessful delivery attempts"),
	CENTRAL_INSTITUTION_REFUSAL("49", "Refused by central institution. Abort notification  019 contains an alphanumeric abort value"),
	CLEARING_COMPUTER_REFUSAL("70", "Refusal from the Clearing Computer, and delivery aborted; the Sender of the payment message should also receive an  998 / S n75 Error Message from the Clearing Computer giving further reasons for the refusal."),
	MESSAGE_FORMAT_ERROR("71", "Refusal from the Clearing Computer because of a message format error that prevented normal processing, and delivery aborted."),
	SYSTEM_ERROR("99", "System error");

	private static final Pattern pattern = Pattern.compile("^[0-9]{2}+$");

	private final String code;
	private final String label;


	MessageErrorStatusValues(String code, String label) {
		this.code = code;
		this.label = label;
	}


	public static MessageErrorStatusValues fromCode(String code) {
		Optional<MessageErrorStatusValues> value = Arrays
				.stream(MessageErrorStatusValues.values())
				.filter(v -> Objects.equals(v.getCode(), code))
				.findFirst();
		return value.isPresent() ? value.get() : null;
	}


	public static boolean validateCode(String code) {
		return pattern.matcher(code).matches();
	}


	public static MessageErrorStatusValues getDefault() {
		return SYSTEM_ERROR;
	}


	public String getCode() {
		return this.code;
	}


	public String getLabel() {
		return this.label;
	}
}
