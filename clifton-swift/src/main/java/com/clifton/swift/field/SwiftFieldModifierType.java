package com.clifton.swift.field;

import com.clifton.core.beans.NamedSimpleEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;

import java.util.List;


/**
 * <code>SwiftFieldModifierType</code> an action to be performed by a field modifier defined by the indicated className.
 */
public class SwiftFieldModifierType extends NamedSimpleEntity<Integer> {

	private String className;

	/**
	 * The list of field property types that are applicable for this modifier type.
	 */
	@OneToManyEntity(serviceBeanName = "swiftFieldModifierService", serviceMethodName = "getSwiftFieldModifierPropertyTypeListByType")
	private List<SwiftFieldModifierPropertyType> propertyTypeList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getClassName() {
		return this.className;
	}


	public void setClassName(String className) {
		// should I expect an implementation of com.clifton.swift.field.SwiftFieldModifierBean ?
		this.className = className;
	}


	public List<SwiftFieldModifierPropertyType> getPropertyTypeList() {
		return this.propertyTypeList;
	}


	public void setPropertyTypeList(List<SwiftFieldModifierPropertyType> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}
}
