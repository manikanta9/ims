package com.clifton.swift.field;

import com.clifton.core.beans.NamedEntity;


/**
 * <code>SwiftFieldModifierDataType</code>
 */
public class SwiftFieldModifierDataType extends NamedEntity<Short> {

	public static final String INTEGER = "INTEGER";
	public static final String DECIMAL = "DECIMAL";
	public static final String STRING = "STRING";
	public static final String DATE = "DATE";
	public static final String BOOLEAN = "BOOLEAN";
	public static final String TEXT = "TEXT";
	public static final String SECRET = "SECRET";
	public static final String BINARY = "BINARY";

	private boolean numeric;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isNumeric() {
		return this.numeric;
	}


	public void setNumeric(boolean numeric) {
		this.numeric = numeric;
	}
}
