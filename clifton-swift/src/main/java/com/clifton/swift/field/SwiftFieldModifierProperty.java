package com.clifton.swift.field;

import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareEntity;


/**
 * <code>SwiftFieldModifierProperty</code> defines a property used by a field modifier.
 */
public class SwiftFieldModifierProperty extends BaseNamedSystemUserInterfaceAwareEntity<Integer> {

	private SwiftFieldModifier fieldModifier;
	private SwiftFieldModifierPropertyType fieldModifierPropertyType;
	private String propertyValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifier getFieldModifier() {
		return this.fieldModifier;
	}


	public void setFieldModifier(SwiftFieldModifier fieldModifier) {
		this.fieldModifier = fieldModifier;
	}


	public SwiftFieldModifierPropertyType getFieldModifierPropertyType() {
		return this.fieldModifierPropertyType;
	}


	public void setFieldModifierPropertyType(SwiftFieldModifierPropertyType fieldModifierPropertyType) {
		this.fieldModifierPropertyType = fieldModifierPropertyType;
	}


	public String getPropertyValue() {
		return this.propertyValue;
	}


	public void setPropertyValue(String value) {
		this.propertyValue = value;
	}
}
