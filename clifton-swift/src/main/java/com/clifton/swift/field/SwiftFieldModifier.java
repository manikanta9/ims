package com.clifton.swift.field;

import com.clifton.core.beans.BaseSimpleEntity;

import java.util.List;


/**
 * <code>SwiftFieldModifier</code> database entity used by specific {@link SwiftFieldModifierBean} implementations
 */
public class SwiftFieldModifier extends BaseSimpleEntity<Integer> {

	/**
	 * The link to field modifier implementation.
	 */
	private SwiftFieldModifierType fieldModifierType;

	/**
	 * List of field filters used to determine if this field modifier applies to a specific message.
	 */
	private List<SwiftFieldModifierFilter> filterList;

	/**
	 * List of field modifier properties used by this field modifier.
	 */
	private List<SwiftFieldModifierProperty> propertyList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierType getFieldModifierType() {
		return this.fieldModifierType;
	}


	public void setFieldModifierType(SwiftFieldModifierType fieldModifierType) {
		this.fieldModifierType = fieldModifierType;
	}


	public List<SwiftFieldModifierFilter> getFilterList() {
		return this.filterList;
	}


	public void setFilterList(List<SwiftFieldModifierFilter> filterList) {
		this.filterList = filterList;
	}


	public List<SwiftFieldModifierProperty> getPropertyList() {
		return this.propertyList;
	}


	public void setPropertyList(List<SwiftFieldModifierProperty> propertyList) {
		this.propertyList = propertyList;
	}
}
