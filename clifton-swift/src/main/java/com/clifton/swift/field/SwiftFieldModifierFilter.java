package com.clifton.swift.field;

import com.clifton.core.beans.BaseSimpleEntity;


/**
 * <code>SwiftFieldModifierFilter</code> provides a mechanism to restrict the messages that a specific field modifier applies.
 * These are only used by the associated field modifiers in order to build a regular expression based on filter values.
 */
public class SwiftFieldModifierFilter extends BaseSimpleEntity<Integer> {

	private SwiftFieldModifier fieldModifier;
	private String fieldTag;
	private String fieldValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifier getFieldModifier() {
		return this.fieldModifier;
	}


	public void setFieldModifier(SwiftFieldModifier fieldModifier) {
		this.fieldModifier = fieldModifier;
	}


	public String getFieldTag() {
		return this.fieldTag;
	}


	public void setFieldTag(String fieldTag) {
		this.fieldTag = fieldTag;
	}


	public String getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
}
