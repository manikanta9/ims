package com.clifton.swift.field;

import com.clifton.system.userinterface.BaseNamedSystemUserInterfaceAwareEntity;


/**
 * <code>SwiftFieldModifierPropertyType</code> property type used by field modifiers, includes label, required, order by information and
 * other information used in part by the user interface.
 */
public class SwiftFieldModifierPropertyType extends BaseNamedSystemUserInterfaceAwareEntity<Integer> {

	private SwiftFieldModifierType fieldModifierType;
	private SwiftFieldModifierDataType modifierDataType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierType getFieldModifierType() {
		return this.fieldModifierType;
	}


	public void setFieldModifierType(SwiftFieldModifierType fieldModifierType) {
		this.fieldModifierType = fieldModifierType;
	}


	public SwiftFieldModifierDataType getModifierDataType() {
		return this.modifierDataType;
	}


	public void setModifierDataType(SwiftFieldModifierDataType modifierDataType) {
		this.modifierDataType = modifierDataType;
	}
}
