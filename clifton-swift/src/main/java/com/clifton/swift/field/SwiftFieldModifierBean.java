package com.clifton.swift.field;

/**
 * <code>SwiftFieldModifierService</code> modifies targeted Swift message fields.
 * Implementations reside in the specific Swift provider packages, for instance,
 * prowide.
 */
public interface SwiftFieldModifierBean<T> {

	public void execute(T message, SwiftFieldModifier modifier);
}
