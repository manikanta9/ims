package com.clifton.swift.identifier;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;


/**
 * The <code>SwiftIdentifier</code> defines a SWIFT message identifier used to link messages for a single process together.
 *
 * @author mwacker
 */
public class SwiftIdentifier extends BaseSimpleEntity<Long> {

	/**
	 * The identifier the this instance replaced.
	 */
	private SwiftIdentifier referencedIdentifier;
	/**
	 * The unique id which was either generated or received for the message.  This string represents
	 * the exact value that will be in the id fields.
	 */
	private String uniqueStringIdentifier;
	/**
	 * The source system module that the message was received from. (M2M)
	 */
	private SwiftSourceSystemModule sourceSystemModule;
	/**
	 * The source system that the message was received from. Has to match the system on
	 * the sourceSystemModule and it's here because we need to index the uniqueStringIdentifier
	 * as unique per sourceSystem.
	 */
	private SwiftSourceSystem sourceSystem;
	/**
	 * The identity of the object on the source system. (Entity ID in IMS)
	 */
	private Long sourceSystemFkFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftIdentifier getReferencedIdentifier() {
		return this.referencedIdentifier;
	}


	public void setReferencedIdentifier(SwiftIdentifier referencedIdentifier) {
		this.referencedIdentifier = referencedIdentifier;
	}


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.uniqueStringIdentifier = uniqueStringIdentifier;
	}


	public SwiftSourceSystemModule getSourceSystemModule() {
		return this.sourceSystemModule;
	}


	public void setSourceSystemModule(SwiftSourceSystemModule sourceSystemModule) {
		this.sourceSystemModule = sourceSystemModule;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}


	public SwiftSourceSystem getSourceSystem() {
		return this.sourceSystem;
	}


	public void setSourceSystem(SwiftSourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
}
