package com.clifton.swift.identifier;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.identifier.search.SwiftIdentifierSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * {@inheritDoc}
 */
@Component
public class SwiftIdentifierServiceImpl implements SwiftIdentifierService {

	private AdvancedUpdatableDAO<SwiftIdentifier, Criteria> swiftIdentifierDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftIdentifier getSwiftIdentifier(long id) {
		return getSwiftIdentifierDAO().findByPrimaryKey(id);
	}


	@Override
	public SwiftIdentifier getSwiftIdentifierByUniqueId(String uniqueId, short sourceSystemId, Long messageReferenceNumber) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(uniqueId), "Cannot look up [SwiftIdentifier] for unique id [" + uniqueId + "].");
		SwiftIdentifierSearchForm searchForm = new SwiftIdentifierSearchForm();
		searchForm.setUniqueStringIdentifier(uniqueId);
		searchForm.setSourceSystemId(sourceSystemId);
		searchForm.setReferencedIdentifierId(messageReferenceNumber);
		return CollectionUtils.getOnlyElement(getSwiftIdentifierList(searchForm));
	}


	@Override
	public List<SwiftIdentifier> getSwiftIdentifierList(SwiftIdentifierSearchForm searchForm) {
		return getSwiftIdentifierDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SwiftIdentifier saveSwiftIdentifier(SwiftIdentifier identifier) {
		ValidationUtils.assertNotNull(identifier.getSourceSystem(), "Source system is required to save identifier.", "sourceSystem");
		ValidationUtils.assertNotNull(identifier.getSourceSystemModule(), "Source system module is required to save identifier.", "sourceSystemModule");
		ValidationUtils.assertEquals(identifier.getSourceSystem(), identifier.getSourceSystemModule().getSourceSystem(), "Cannot create an identifier with source system [" + identifier.getSourceSystem()
				+ "] and a module from source system [" + identifier.getSourceSystemModule().getSourceSystem() + "].");

		return getSwiftIdentifierDAO().save(identifier);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SwiftIdentifier, Criteria> getSwiftIdentifierDAO() {
		return this.swiftIdentifierDAO;
	}


	public void setSwiftIdentifierDAO(AdvancedUpdatableDAO<SwiftIdentifier, Criteria> swiftIdentifierDAO) {
		this.swiftIdentifierDAO = swiftIdentifierDAO;
	}
}
