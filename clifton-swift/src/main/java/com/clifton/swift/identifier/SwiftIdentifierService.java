package com.clifton.swift.identifier;

import com.clifton.swift.identifier.search.SwiftIdentifierSearchForm;

import java.util.List;


/**
 * <code>SwiftIdentifierService </code> provides functionality to fetch and save
 * {@link SwiftIdentifier} entities.  This is exposed to both the client and server,
 * it will change in the future when we build out an API for this instead.
 * <p>
 * Included client-side for expediency.
 */
public interface SwiftIdentifierService {

	public SwiftIdentifier getSwiftIdentifier(long id);


	public SwiftIdentifier getSwiftIdentifierByUniqueId(String uniqueId, short sourceSystemId, Long messageReferenceNumber);


	public List<SwiftIdentifier> getSwiftIdentifierList(SwiftIdentifierSearchForm searchForm);


	public SwiftIdentifier saveSwiftIdentifier(SwiftIdentifier identifier);
}
