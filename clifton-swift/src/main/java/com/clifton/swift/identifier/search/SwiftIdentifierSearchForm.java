package com.clifton.swift.identifier.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class SwiftIdentifierSearchForm extends BaseEntitySearchForm {

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String uniqueStringIdentifier;

	@SearchField(searchField = "sourceSystem.id")
	private Short sourceSystemId;

	@SearchField(searchField = "sourceSystemModule.id")
	private Short sourceSystemModuleId;

	@SearchField
	private Long sourceSystemFkFieldId;

	@SearchField(searchField = "referencedIdentifier.id")
	private Long referencedIdentifierId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.uniqueStringIdentifier = uniqueStringIdentifier;
	}


	public Short getSourceSystemId() {
		return this.sourceSystemId;
	}


	public void setSourceSystemId(Short sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}


	public Short getSourceSystemModuleId() {
		return this.sourceSystemModuleId;
	}


	public void setSourceSystemModuleId(Short sourceSystemModuleId) {
		this.sourceSystemModuleId = sourceSystemModuleId;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}


	public Long getReferencedIdentifierId() {
		return this.referencedIdentifierId;
	}


	public void setReferencedIdentifierId(Long referencedIdentifierId) {
		this.referencedIdentifierId = referencedIdentifierId;
	}
}
