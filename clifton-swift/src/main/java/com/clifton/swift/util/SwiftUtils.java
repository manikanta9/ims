package com.clifton.swift.util;

/**
 * @author mwacker
 */
public class SwiftUtils {

	public static String removeInvalidMessageCharacters(String fieldValue) {
		return fieldValue.replaceAll("&", "");
	}
}
