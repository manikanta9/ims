package com.clifton.swift.util;

import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.message.SwiftMessageFormats;

import java.util.List;
import java.util.Map;


/**
 * <code>SwiftUtilService</code> methods dealing with low level swift messages.
 */
public interface SwiftUtilHandler<M> {

	public M parseSingleMessage(String messageString, SwiftMessageFormats messageFormat);


	public List<M> parseMessage(String messageString, SwiftMessageFormats messageFormat);


	public String getMessageText(M message);


	public String getMessageTypeCode(M message);


	public String getTransactionReferenceNumber(M message);


	public Long getMessageReferenceNumber(M message);


	/**
	 * Generate a text representation in a human readable form, this format cannot be consumed by the SWIFT network.
	 */
	public String getFormattedMessageText(String messageText, SwiftMessageFormats messageFormat);


	/**
	 * Find the SWIFT message Transaction Reference Number (Field 20) and replace its value with the primary key of the provided Swift Identifier.
	 */
	public void replaceSwiftMessageTransactionReferenceNumber(M message, SwiftIdentifier swiftIdentifier);


	/**
	 * Gets the BIC from the Receiver field on an MT message.  MX messages are not supported.
	 */
	public String getReceiverBusinessIdentifierCode(M message);


	/**
	 * Gets the BIC from the Sender field on an MT message.  MX messages are not supported.
	 */
	public String getSenderBusinessIdentifierCode(M message);


	/**
	 * Is the message an ACK (Acknowledgement) or NACK (Negative Acknowledgement), MT021 system message.
	 */
	public boolean isSystemAcknowledgementMessage(M message);


	public boolean isAcknowledged(M message);


	public M getAcknowledgedMessage(M message, List<M> messages);


	public List<M> getAllMessagesList(M abstractMessage);


	public Map<M, Boolean> getAllMessagesMap(M abstractMessage);
}

