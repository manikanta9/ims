package com.clifton.swift.messaging.xml;

import com.clifton.core.messaging.converter.xml.MessagingXStreamConfigurer;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.thoughtworks.xstream.XStream;
import org.springframework.stereotype.Component;


/**
 * The {@link SwiftMessagingXStreamConfigurer} provides {@link XStream} customizations to support serialization and deserialization for Swift messaging DTOs. Customizations may
 * include functionality to reduce the length of serialized content or support serialization and deserialization of otherwise-problematic types.
 *
 * @author MikeH
 */
@Component
public class SwiftMessagingXStreamConfigurer implements MessagingXStreamConfigurer {

	@Override
	public void configure(XStream xStream) {
		// SwiftIdentifier customization
		xStream.useAttributeFor(SwiftIdentifier.class, "uniqueStringIdentifier");
		xStream.omitField(SwiftIdentifier.class, "newlyCreated");

		// SwiftMessagingMessage customization
		xStream.useAttributeFor(SwiftMessagingMessage.class, "uniqueStringIdentifier");

		// Other customization
		xStream.registerConverter(new SwiftFieldModifierXStreamConverter());
	}
}
