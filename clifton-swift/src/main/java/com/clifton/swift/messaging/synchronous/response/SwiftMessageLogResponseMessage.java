package com.clifton.swift.messaging.synchronous.response;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;
import com.clifton.swift.message.log.SwiftMessageLog;

import java.util.List;


/**
 * <code>SwiftMessageLogResponseMessage</code>
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageLogResponseMessage extends AbstractSynchronousResponseMessage {

	private List<SwiftMessageLog> messageLogList;


	public List<SwiftMessageLog> getMessageLogList() {
		return this.messageLogList;
	}


	public void setMessageLogList(List<SwiftMessageLog> messageLogList) {
		this.messageLogList = messageLogList;
	}
}
