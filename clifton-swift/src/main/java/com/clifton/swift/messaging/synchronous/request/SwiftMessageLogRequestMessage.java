package com.clifton.swift.messaging.synchronous.request;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.swift.message.log.search.SwiftMessageLogSearchForm;

import java.util.HashMap;
import java.util.Map;


/**
 * <code>SwiftMessageLogRequestMessage</code>
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageLogRequestMessage extends SwiftMessageLogSearchForm implements SynchronousRequestMessage {

	private String machineName;
	private final Map<String, String> properties = new HashMap<>();
	private Integer requestTimeout;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getResponseClass() {
		return null;
	}


	@Override
	public String getMachineName() {
		return this.machineName;
	}


	@Override
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}


	@Override
	public Map<String, String> getProperties() {
		return this.properties;
	}


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
}
