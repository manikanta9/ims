package com.clifton.swift.messaging.synchronous.response;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;
import com.clifton.swift.message.SwiftMessageType;

import java.util.List;


/**
 * <code>SwiftMessageTypeResponseMessage</code>
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageTypeResponseMessage extends AbstractSynchronousResponseMessage {

	private List<SwiftMessageType> messageTypeList;


	public List<SwiftMessageType> getSwiftMessageTypeList() {
		return this.messageTypeList;
	}


	public void setSwiftMessageTypeList(List<SwiftMessageType> messageTypeList) {
		this.messageTypeList = messageTypeList;
	}
}
