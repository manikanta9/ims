package com.clifton.swift.messaging.asynchronous;


import com.clifton.core.messaging.AbstractMessagingMessage;
import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.swift.message.SwiftMessageFormats;


/**
 * The <code>SwiftMessagingMessage</code> is an asynchronous Swift message used to
 * send Swift message strings to and from a Swift server.
 */
@MessageType(MessageTypes.XML)
public class SwiftMessagingMessage extends AbstractMessagingMessage implements AsynchronousMessage {

	public Integer messageDelaySeconds;
	private String messageText;
	private String uniqueStringIdentifier;
	/**
	 * Unique name for the source system
	 */
	private String sourceSystemName;

	/**
	 * Unique name for the source system sub module
	 */
	private String sourceSystemModuleName;

	/**
	 * The identity of the object on the source system. (Entity ID in IMS)
	 */
	private Long sourceSystemFkFieldId;

	/**
	 * Identifies the message text format, either MT or MX.
	 */
	private SwiftMessageFormats messageFormat;

	/**
	 * For SWIFT cancel messages, this would reference the Swift Identifier table's primary key for the message being cancelled.
	 * The Swift Identifier table's primary key is used by SWIFT as the message's unique identifier.
	 */
	private Long messageReferenceNumber;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getMessageDelaySeconds() {
		return this.messageDelaySeconds;
	}


	public void setMessageDelaySeconds(Integer messageDelaySeconds) {
		this.messageDelaySeconds = messageDelaySeconds;
	}


	public String getMessageText() {
		return this.messageText;
	}


	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.uniqueStringIdentifier = uniqueStringIdentifier;
	}


	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public String getSourceSystemModuleName() {
		return this.sourceSystemModuleName;
	}


	public void setSourceSystemModuleName(String sourceSystemModuleName) {
		this.sourceSystemModuleName = sourceSystemModuleName;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}


	public SwiftMessageFormats getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(SwiftMessageFormats messageFormat) {
		this.messageFormat = messageFormat;
	}


	public Long getMessageReferenceNumber() {
		return this.messageReferenceNumber;
	}


	public void setMessageReferenceNumber(Long messageReferenceNumber) {
		this.messageReferenceNumber = messageReferenceNumber;
	}
}
