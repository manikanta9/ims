package com.clifton.swift.messaging.synchronous.response;

import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;


/**
 * Response message for SwiftMessageActionRequestMessage.  If the error property is set, then there
 * was an error, otherwise the action processed successfully.
 *
 * @author tstebner
 */
public class SwiftMessageActionResponseMessage extends AbstractSynchronousResponseMessage {
	// nothing for now
}
