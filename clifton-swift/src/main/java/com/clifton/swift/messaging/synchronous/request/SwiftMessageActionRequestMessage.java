package com.clifton.swift.messaging.synchronous.request;

import com.clifton.core.messaging.synchronous.AbstractSynchronousRequestMessage;
import com.clifton.swift.message.SwiftMessageActions;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageActionResponseMessage;


/**
 * @author tstebner
 */
public class SwiftMessageActionRequestMessage extends AbstractSynchronousRequestMessage {

	/**
	 * The action to take on the swift entries.
	 */
	private SwiftMessageActions swiftMessageAction;

	/**
	 * The list of entries to be acted upon.
	 */
	private long[] swiftMessageIdList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getResponseClass() {
		return SwiftMessageActionResponseMessage.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageActions getSwiftMessageAction() {
		return this.swiftMessageAction;
	}


	public void setSwiftMessageAction(SwiftMessageActions swiftMessageAction) {
		this.swiftMessageAction = swiftMessageAction;
	}


	public long[] getSwiftMessageIdList() {
		return this.swiftMessageIdList;
	}


	public void setSwiftMessageIdList(long[] swiftMessageIdList) {
		this.swiftMessageIdList = swiftMessageIdList;
	}
}
