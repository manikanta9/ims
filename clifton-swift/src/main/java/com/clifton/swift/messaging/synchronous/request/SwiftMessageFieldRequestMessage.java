package com.clifton.swift.messaging.synchronous.request;

import com.clifton.core.messaging.AbstractMessagingMessage;
import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;


/**
 * <code>SwiftMessageFieldRequestMessage</code>
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageFieldRequestMessage extends AbstractMessagingMessage implements SynchronousRequestMessage {

	private Long messageId;

	private Integer requestTimeout;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getResponseClass() {
		return null;
	}


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}


	public Long getMessageId() {
		return this.messageId;
	}


	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}
}
