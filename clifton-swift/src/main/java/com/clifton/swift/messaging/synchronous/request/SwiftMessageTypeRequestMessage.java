package com.clifton.swift.messaging.synchronous.request;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.swift.message.search.SwiftMessageTypeSearchForm;

import java.util.HashMap;
import java.util.Map;


/**
 * <code>SwiftMessageTypeRequestMessage</code> enable synchronous queue request for Swift Message Type Records.
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageTypeRequestMessage extends SwiftMessageTypeSearchForm implements SynchronousRequestMessage {

	@SearchField(searchField = "id")
	private Short entryId;

	private String machineName;
	private final Map<String, String> properties = new HashMap<>();
	private Integer requestTimeout;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getEntryId() {
		return this.entryId;
	}


	public void setEntryId(Short entryId) {
		this.entryId = entryId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getResponseClass() {
		return null;
	}


	@Override
	public String getMachineName() {
		return this.machineName;
	}


	@Override
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}


	@Override
	public Map<String, String> getProperties() {
		return this.properties;
	}


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
}
