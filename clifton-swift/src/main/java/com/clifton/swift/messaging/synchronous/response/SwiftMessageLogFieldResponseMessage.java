package com.clifton.swift.messaging.synchronous.response;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;
import com.clifton.swift.message.SwiftMessageField;

import java.util.List;


/**
 * <code>SwiftMessageLogFieldResponseMessage</code>
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageLogFieldResponseMessage extends AbstractSynchronousResponseMessage {

	private List<SwiftMessageField> messageFieldList;


	public List<SwiftMessageField> getMessageFieldList() {
		return this.messageFieldList;
	}


	public void setMessageFieldList(List<SwiftMessageField> messageFieldList) {
		this.messageFieldList = messageFieldList;
	}
}
