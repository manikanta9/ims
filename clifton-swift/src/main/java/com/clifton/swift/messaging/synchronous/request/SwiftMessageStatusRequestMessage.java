package com.clifton.swift.messaging.synchronous.request;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.swift.message.search.SwiftMessageStatusSearchForm;

import java.util.HashMap;
import java.util.Map;


/**
 * <code>SwiftMessageStatusRequestMessage</code> enable synchronous queue request for Swift Message Status Records.
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageStatusRequestMessage extends SwiftMessageStatusSearchForm implements SynchronousRequestMessage {

	@SearchField(searchField = "id")
	private Short id;

	private String machineName;
	private Map<String, String> properties = new HashMap<>();
	private Integer requestTimeout;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getResponseClass() {
		return null;
	}


	@Override
	public String getMachineName() {
		return this.machineName;
	}


	@Override
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}


	@Override
	public Map<String, String> getProperties() {
		return this.properties;
	}


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
}
