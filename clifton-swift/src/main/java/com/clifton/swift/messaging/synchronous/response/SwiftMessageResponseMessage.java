package com.clifton.swift.messaging.synchronous.response;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;
import com.clifton.swift.message.SwiftMessage;

import java.util.List;


/**
 * <code>SwiftMessageResponseMessage</code>
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageResponseMessage extends AbstractSynchronousResponseMessage {

	private List<SwiftMessage> messageList;


	public List<SwiftMessage> getMessageList() {
		return this.messageList;
	}


	public void setMessageList(List<SwiftMessage> messageList) {
		this.messageList = messageList;
	}
}
