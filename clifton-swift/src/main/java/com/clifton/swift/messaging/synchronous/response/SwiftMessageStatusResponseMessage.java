package com.clifton.swift.messaging.synchronous.response;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;
import com.clifton.swift.message.SwiftMessageStatus;

import java.util.List;


/**
 * <code>SwiftMessageStatusResponseMessage</code>
 */
@MessageType(MessageTypes.XML)
public class SwiftMessageStatusResponseMessage extends AbstractSynchronousResponseMessage {

	private List<SwiftMessageStatus> messageStatusList;


	public List<SwiftMessageStatus> getSwiftMessageStatusList() {
		return this.messageStatusList;
	}


	public void setSwiftMessageStatusList(List<SwiftMessageStatus> messageTypeList) {
		this.messageStatusList = messageTypeList;
	}
}
