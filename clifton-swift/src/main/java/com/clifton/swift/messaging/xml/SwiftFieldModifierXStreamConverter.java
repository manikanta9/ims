package com.clifton.swift.messaging.xml;

import com.clifton.core.beans.BeanUtils;
import com.clifton.swift.field.SwiftFieldModifier;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


/**
 * The {@link SwiftFieldModifierXStreamConverter} simplifies XML conversion for {@link SwiftFieldModifier} entities when registered on an {@link XStream} instance.
 *
 * @author MikeH
 */
public class SwiftFieldModifierXStreamConverter implements Converter {

	@Override
	public boolean canConvert(Class type) {
		return SwiftFieldModifier.class == type;
	}


	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, @SuppressWarnings("unused") MarshallingContext context) {
		// SwiftIdentifier identifier = (SwiftIdentifier) source;
		//writer.endNode();
	}


	@Override
	public Object unmarshal(HierarchicalStreamReader reader, @SuppressWarnings("unused") UnmarshallingContext context) {
		SwiftIdentifier identifier = new SwiftIdentifier();
		SwiftIdentifier ref = new SwiftIdentifier();
		identifier.setReferencedIdentifier(ref);

		while (reader.hasMoreChildren()) {
			reader.moveDown();
			BeanUtils.setPropertyValue(ref, reader.getNodeName(), reader.getValue(), true);
			reader.moveUp();
		}

		return identifier;
	}
}
