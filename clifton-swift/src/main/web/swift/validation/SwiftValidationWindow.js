Clifton.swift.validation.SwiftValidationWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'SWIFT Message Validation',
	width: 800,
	height: 675,
	iconCls: 'swift',

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		instructions: 'This form will validate a SWIFT message.  Select the message type MT/MX, paste the message text in the Message field and click validate.  A list of any errors on the message will appear in the validation response field.',
		listeners: {
			afterload: function(fp, isClosing) {
				if (fp.getWindow().doValidationAfterLoad) {
					fp.validateMessage();
				}
			}
		},
		items: [
			{
				fieldLabel: 'Message Type', name: 'messageType', displayField: 'name', valueField: 'value', xtype: 'combo', mode: 'local', emptyText: 'None',
				store: {
					xtype: 'arraystore',
					fields: ['value', 'name'],
					data: [
						['MX', 'MX'],
						['MT', 'MT']
					]
				}
			},
			{fieldLabel: 'Message', name: 'message', xtype: 'textarea', qtip: 'Optional comma-delimited list of column names.', height: 350},
			{xtype: 'label', html: '<hr/>'},
			{fieldLabel: 'Validation Response', name: 'response', xtype: 'textarea', submitValue: false, readOnly: true, anchor: '-35 -450'}
		],

		validateMessage: function() {
			const form = this.getForm();
			form.submit(Ext.applyIf({
				url: 'swiftMessageValidate.json',
				waitMsg: 'Rebuilding...',
				success: function(form, action) {
					form.findField('response').setValue(action.result.data);
				}
			}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
		},

		buttons: [{
			text: 'Validate',
			iconCls: 'run',
			width: 150,
			handler: function() {
				const owner = this.findParentByType('formpanel');
				owner.validateMessage();
			}
		}]
	}]
});
