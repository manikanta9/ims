Clifton.swift.message.SwiftMessageAdministrationWindow = Ext.extend(TCG.app.Window, {
	id: 'swiftMessageSetupWindow',
	title: 'SWIFT Administration',
	iconCls: 'swift',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'SWIFT Messages',
				items: [{
					name: 'swiftMessageListFind',
					xtype: 'swift-message-grid-panel',
					rowSelectionModel: 'checkbox',
					instructions: 'A list of all SWIFT message.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Message Type', xtype: 'toolbar-combo', name: 'messageTypeId', width: 200, url: 'swiftMessageTypeListFind.json'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('messageDateAndTime', {'after': new Date().add(Date.DAY, -2)});
						}
						const params = {
							readUncommittedRequested: true
						};
						const v = TCG.getChildByName(this.getTopToolbar(), 'messageTypeId').getValue();
						if (v) {
							params.typeId = v;
						}
						return params;
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.swift.message.SwiftMessageWindow',
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Reprocess Selected',
								tooltip: 'Reprocess selected messages from the Swift message table.',
								iconCls: 'run',
								handler: function() {
									const sm = gridPanel.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one message to be processed.', 'No message(s) Selected');
									}
									else {
										const messages = sm.getSelections();
										const messageIdList = [];
										for (let i = 0; i < messages.length; i++) {
											messageIdList[i] = messages[i].id;
										}
										const loader = new TCG.data.JsonLoader({
											waitMsg: 'Processing Messages',
											waitTarget: gridPanel,
											params: {
												messageIdList: messageIdList
											},
											onLoad: function(record, conf) {
												gridPanel.reload();
											}
										});
										loader.load('swiftReprocessMessageList.json');
									}
								}
							}, '-');
						}
					}
				}]
			},


			{
				title: 'SWIFT Log Entries',
				items: [{
					name: 'swiftMessageLogListFind',
					xtype: 'gridpanel',
					rowSelectionModel: 'checkbox',
					appendStandardColumns: false,
					instructions: 'A list of SWIFT messages that have been received but not processed.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: {searchField: 'id'}},
						{header: 'Message Date/Time', width: 30, dataIndex: 'messageDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Error Text', width: 80, dataIndex: 'errorMessage'},
						{header: 'Message Text', width: 80, dataIndex: 'text'},
						{
							header: 'Message Format',
							width: 15,
							dataIndex: 'messageFormat',
							hidden: true,
							filter: {
								type: 'list',
								options: [
									['MT', 'SWIFT FIN/MT message'],
									['MX', 'SWIFT XML/MX message']
								]
							}
						},
						{header: 'Incoming', width: 15, dataIndex: 'incoming', type: 'boolean'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('messageDateAndTime', {'after': new Date().add(Date.DAY, -2)});
						}
						return {
							readUncommittedRequested: true
						};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.swift.message.SwiftMessageLogWindow',
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Reprocess Selected',
								tooltip: 'Reprocess selected messages from the Swift message log table to the message table.',
								iconCls: 'run',
								handler: function() {
									const sm = gridPanel.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one message to be processed.', 'No message(s) Selected');
									}
									else {
										const messages = sm.getSelections();
										const messageIdList = [];
										for (let i = 0; i < messages.length; i++) {
											messageIdList[i] = messages[i].id;
										}
										const loader = new TCG.data.JsonLoader({
											waitMsg: 'Processing Messages',
											waitTarget: gridPanel,
											params: {
												messageLogIdList: messageIdList
											},
											onLoad: function(record, conf) {
												gridPanel.reload();
											}
										});
										loader.load('swiftReprocessMessageLogList.json');
									}
								}
							}, '-');
						}
					}
				}]
			},


			{
				title: 'Source System Modules',
				items: [{
					name: 'swiftSourceSystemModuleListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'A list of SWIFT Source System Module records.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: {searchField: 'id'}},
						{header: 'Source System Name', width: 50, dataIndex: 'sourceSystem.name'},
						{header: 'Source System Module', width: 50, dataIndex: 'name'},
						{header: 'Description', width: 80, dataIndex: 'description'},
						{header: 'Message Format', width: 50, dataIndex: 'targetMessageFormat'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.swift.message.SwiftSourceSystemModuleWindow'
					}
				}]
			},


			{
				title: 'SWIFT Message Types',
				items: [{
					name: 'swiftMessageTypeListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'A list of SWIFT Message Type records.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: {searchField: 'id'}},
						{header: 'Message Type Name', width: 50, dataIndex: 'name'},
						{header: 'Description', width: 80, dataIndex: 'description', renderer: 'htmlEncode'},
						{header: 'MT Version Name', width: 50, dataIndex: 'mtVersionName'},
						{header: 'MX Version Name', width: 50, dataIndex: 'mxVersionName'},
						{header: 'System Message', width: 25, dataIndex: 'systemMessage', type: 'boolean', readOnly: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.swift.message.SwiftMessageTypeWindow'
					}
				}]
			}
		]
	}]
});
