Clifton.swift.message.SwiftMessageWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'SWIFT Message',
	width: 950,
	height: 600,
	iconCls: 'swift',
	enableShowInfo: false,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Message',
				tbar: [{
					text: 'Validate',
					iconCls: 'run',
					handler: function() {
						const win = TCG.getParentByClass(this, Ext.Window);
						const formPanel = win.getMainFormPanel();
						const form = formPanel.getForm();
						const cm = {
							messageType: form.findField('messageFormat').getValue(),
							message: form.findField('text').getValue()
						};

						const clz = 'Clifton.swift.validation.SwiftValidationWindow';
						const cmpId = TCG.getComponentId(clz, win.tableName + '.' + win.fkFieldId);
						TCG.createComponent(clz, {
							id: cmpId,
							defaultDataIsReal: true,
							defaultData: cm,
							doValidationAfterLoad: true
						});
					}
				}, '-'],
				items: [{
					xtype: 'formpanel',
					labelWidth: 120,
					labelFieldName: 'identifier.uniqueStringIdentifier',
					url: 'swiftMessage.json',
					items: [
						{fieldLabel: 'ID', name: 'id', xtype: 'displayfield', hidden: true},
						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{fieldLabel: 'Message Type', name: 'type.name', xtype: 'linkfield', detailIdField: 'type.id', detailPageClass: 'Clifton.swift.message.SwiftMessageTypeWindow'},
									{fieldLabel: 'MT Code', name: 'type.mtVersionName', xtype: 'displayfield'},
									{fieldLabel: 'Message Format', name: 'messageFormat', xtype: 'displayfield'},
									{fieldLabel: 'Sender BIC', name: 'senderBusinessIdentifierCode', xtype: 'displayfield'},
									{fieldLabel: 'Source System', name: 'identifier.sourceSystemModule.sourceSystem.name', xtype: 'linkfield', detailIdField: 'identifier.sourceSystemModule.sourceSystem.id', detailPageClass: 'Clifton.swift.message.SwiftSourceSystemWindow'}
								]
							}, {
								rows: [
									{fieldLabel: 'Unique Identifier', name: 'identifier.uniqueStringIdentifier', xtype: 'displayfield'},
									{fieldLabel: 'MX Code', name: 'type.mxVersionName', xtype: 'displayfield'},
									{fieldLabel: 'Message Date/Time', name: 'messageDateAndTime', xtype: 'displayfield', readOnly: true},
									{fieldLabel: 'Receiver BIC', name: 'receiverBusinessIdentifierCode', xtype: 'displayfield'},
									{fieldLabel: 'Source Module', name: 'identifier.sourceSystemModule.name', xtype: 'linkfield', detailIdField: 'identifier.sourceSystemModule.id', detailPageClass: 'Clifton.swift.message.SwiftSourceSystemModuleWindow'}
								]
							}]
						},
						{fieldLabel: 'Message Text', name: 'text', xtype: 'textarea', readOnly: true, qtip: 'Raw Message Text', anchor: '-35 -170'},
						{fieldLabel: 'Message Status', name: 'messageStatus.name', xtype: 'displayfield', readOnly: true, qtip: 'Indicates the status of the SWIFT message within the submission workflow: LOGGED, QUEUED, SENT, ERROR, CCOMPLETE.'},
						{boxLabel: 'Incoming message', name: 'incoming', xtype: 'checkbox', labelSeparator: '', disabled: true},
						{boxLabel: 'Acknowledged', name: 'acknowledged', xtype: 'checkbox', labelSeparator: '', disabled: true}
					]
				}]
			},


			{
				title: 'Formatted Message',
				appendStandardColumns: false,
				xtype: 'treegrid',
				defaults: {
					anchor: '-35 -35'
				},
				viewConfig: {
					forceFit: true
				},
				afterRender: function() {
					TCG.tree.TreeGrid.superclass.afterRender.apply(this, arguments);
					const tree = this;
					tree.animate = true;
					tree.expandAll();
					tree.animate = false;
				},
				name: 'swiftMessageFieldList',
				getLoadParams: function() {
					const win = this.getWindow();
					return {id: win.params.id};
				},
				getLoadURL: function() {
					return this.name + '.json';
				},
				readOnly: true,
				columns: [
					{header: 'Label', width: 350, dataIndex: 'label'},
					{header: 'Tag', width: 80, dataIndex: 'tag'},
					{header: 'Value', width: 170, dataIndex: 'value'},
					{header: 'Formatted Value', width: 300, dataIndex: 'formatted'}
				]
			}
		]
	}]
});

