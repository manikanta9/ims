Clifton.swift.message.SwiftMessageLogWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'SWIFT Logged Message',
	width: 800,
	height: 650,
	iconCls: 'swift',
	enableShowInfo: false,

	tbar: [
		{
			text: 'Validate',
			iconCls: 'run',
			handler: function() {
				const win = TCG.getParentByClass(this, Ext.Window);
				const formPanel = win.getMainFormPanel();
				const form = formPanel.getForm();
				const cm = {
					messageType: 'MT',
					message: form.findField('text').getValue()
				};

				const clz = 'Clifton.swift.validation.SwiftValidationWindow';
				const cmpId = TCG.getComponentId(clz, win.tableName + '.' + win.fkFieldId);
				TCG.createComponent(clz, {
					id: cmpId,
					defaultDataIsReal: true,
					defaultData: cm,
					doValidationAfterLoad: true
				});
			}
		}
	],

	items: [{
		xtype: 'tabpanel',
		items: [{
			title: 'Message',
			items: [{
				xtype: 'formpanel',
				labelWidth: 120,
				url: 'swiftMessageLog.json',
				labelFieldName: 'id',
				items: [
					{
						xtype: 'columnpanel',
						columns: [{
							rows: [
								{fieldLabel: 'Message Date/Time', name: 'messageDateAndTime', xtype: 'displayfield', readOnly: true}
							]
						}, {
							rows: [
								{fieldLabel: 'Message Format', name: 'messageFormat', xtype: 'displayfield', readOnly: true}
							]
						}]
					},
					{fieldLabel: 'Error Text', name: 'errorMessage', xtype: 'textarea', readOnly: true, height: 300},
					{fieldLabel: 'Message Text', name: 'text', xtype: 'textarea', readOnly: true, anchor: '-35 -400'},
					{boxLabel: 'Incoming message', name: 'incoming', xtype: 'checkbox', labelSeparator: '', disabled: true}
				]
			}]
		},
			{
				title: 'Formatted',
				appendStandardColumns: false,
				xtype: 'treegrid',
				defaults: {
					anchor: '-35 -35'
				},
				viewConfig: {
					forceFit: true
				},
				afterRender: function() {
					TCG.tree.TreeGrid.superclass.afterRender.apply(this, arguments);
					const tree = this;
					tree.animate = true;
					tree.expandAll();
					tree.animate = false;
				},
				name: 'swiftMessageLogFieldList',
				getLoadParams: function() {
					const win = this.getWindow();
					return {id: win.params.id};
				},
				getLoadURL: function() {
					return this.name + '.json';
				},
				readOnly: true,
				columns: [
					{header: 'Label', width: 350, dataIndex: 'label'},
					{header: 'Tag', width: 80, dataIndex: 'tag'},
					{header: 'Value', width: 170, dataIndex: 'value'},
					{header: 'Formatted Value', width: 350, dataIndex: 'formatted'}
				]
			}
		]
	}]
});
