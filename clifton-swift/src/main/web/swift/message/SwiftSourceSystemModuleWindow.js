Clifton.swift.message.SwiftSourceSystemModuleWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'SWIFT Source System Module',
	width: 500,
	height: 270,
	iconCls: 'swift',
	enableShowInfo: false,

	items: [{
		xtype: 'formpanel',
		readOnly: true,
		labelWidth: 140,
		url: 'swiftSourceSystemModule.json',
		items: [
			{fieldLabel: 'Source System Name', name: 'sourceSystem.name', xtype: 'linkfield', detailIdField: 'sourceSystem.id', detailPageClass: 'Clifton.swift.message.SwiftSourceSystemWindow'},
			{fieldLabel: 'Source System Module', name: 'name', xtype: 'displayfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Message Format', name: 'targetMessageFormat', xtype: 'displayfield'}
		]
	}]
});
