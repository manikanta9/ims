Clifton.swift.message.SwiftSourceSystemWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'SWIFT Source System',
	width: 500,
	height: 270,
	iconCls: 'swift',

	items: [{
		xtype: 'formpanel',
		readOnly: true,
		labelWidth: 140,
		url: 'swiftSourceSystem.json',
		items: [
			{fieldLabel: 'Source System Name', name: 'name', xtype: 'displayfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Business Identifier Code', name: 'businessIdentifierCode', xtype: 'displayfield'}
		]
	}]
});
