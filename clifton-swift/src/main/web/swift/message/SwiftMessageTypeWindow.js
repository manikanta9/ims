Clifton.swift.message.SwiftMessageTypeWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'SWIFT Message Type',
	width: 500,
	height: 350,
	iconCls: 'swift',
	enableShowInfo: false,

	items: [{
		xtype: 'formpanel',
		readOnly: true,
		labelWidth: 120,
		url: 'swiftMessageType.json',
		items: [
			{fieldLabel: 'Message Type Name', name: 'name', xtype: 'displayfield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'MT Version Name', name: 'mtVersionName', xtype: 'displayfield'},
			{fieldLabel: 'MX Version Name', name: 'mxVersionName', xtype: 'displayfield'},
			{fieldLabel: 'System Message', name: 'systemMessage', xtype: 'checkbox', disabled: true}
		]
	}]
});
