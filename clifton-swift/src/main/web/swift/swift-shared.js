Ext.ns('Clifton.swift', 'Clifton.swift.message', 'Clifton.swift.validation');

Clifton.swift.message.SwiftMessageGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'swiftMessageListBySourceEntityFind',
	xtype: 'gridpanel',
	appendStandardColumns: false,
	tableName: 'OVERRIDE',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: {searchField: 'id'}},
		{header: 'Message Type', width: 70, dataIndex: 'type.name', filter: {type: 'combo', searchFieldName: 'typeId', url: 'swiftMessageTypeListFind.json?orderBy=name'}},
		{header: 'MT Code', width: 15, dataIndex: 'type.mtVersionName', filter: {searchField: 'mtVersionName'}},
		{header: 'MX Code', width: 15, dataIndex: 'type.mxVersionName', hidden: true, filter: {searchField: 'mxVersionName'}},
		{header: 'Source System', width: 15, dataIndex: 'identifier.sourceSystemModule.sourceSystem.name', hidden: true, filter: false, sortable: false},
		{header: 'Source Module', width: 15, dataIndex: 'identifier.sourceSystemModule.name', hidden: true, filter: false, sortable: false},
		{header: 'Unique Identifier', width: 25, dataIndex: 'identifier.uniqueStringIdentifier', filter: {searchField: 'uniqueStringIdentifier'}},
		{header: 'Sender BIC', width: 25, dataIndex: 'senderBusinessIdentifierCode'},
		{header: 'Receiver BIC', width: 25, dataIndex: 'receiverBusinessIdentifierCode'},
		{header: 'Message Format', width: 15, dataIndex: 'messageFormat', filter: false, hidden: true, sortable: false},
		{header: 'Message Date/Time', width: 35, dataIndex: 'messageDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Message Text', width: 70, dataIndex: 'text', filter: false, sortable: false},
		{header: 'Message Status', width: 20, dataIndex: 'messageStatus.name', filter: {type: 'combo', searchFieldName: 'messageStatusId', url: 'swiftMessageStatusListFind.json?orderBy=name'}},
		{header: 'Incoming', width: 20, dataIndex: 'incoming', type: 'boolean', readOnly: true},
		{
			header: 'Acknowledged',
			hidden: true,
			width: 20,
			dataIndex: 'acknowledged',
			type: 'boolean',
			readOnly: true,
			renderer: function(v, metaData, r) {
				if (r.data['incoming']) {
					metaData.css = v ? 'buy-light' : 'sell-light';
					return v ? 'ACK' : 'NAK';
				}
			}
		}
	],
	getEntityId: function() {
		const win = TCG.getParentByClass(this, Ext.Window);
		return win.getMainFormId();
	},
	getLoadParams: function(firstLoad) {
		const gridPanel = this;
		return {
			sourceEntityId: gridPanel.getEntityId(),
			systemTableName: gridPanel.tableName
		};
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.swift.message.SwiftMessageWindow'
	}
});
Ext.reg('swift-message-grid-panel', Clifton.swift.message.SwiftMessageGridPanel);


Clifton.trade.DefaultTradeWindowAdditionalTabs[Clifton.trade.DefaultTradeWindowAdditionalTabs.length] = {
	title: 'SWIFT Messages',
	items: [{
		xtype: 'swift-message-grid-panel',
		instructions: 'SWIFT messages created for Trade Instructions.',
		tableName: 'Trade'
	}]
};
