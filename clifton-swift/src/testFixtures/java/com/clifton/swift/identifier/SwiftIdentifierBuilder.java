package com.clifton.swift.identifier;

import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemBuilder;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.system.SwiftSourceSystemModuleBuilder;


public class SwiftIdentifierBuilder {

	private SwiftIdentifier swiftIdentifier;


	private SwiftIdentifierBuilder(SwiftIdentifier swiftIdentifier) {
		this.swiftIdentifier = swiftIdentifier;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftIdentifierBuilder createSwiftIdentifier() {
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();

		swiftIdentifier.setId((long) 1);
		swiftIdentifier.setUniqueStringIdentifier("IMS:M2M:56872");
		swiftIdentifier.setSourceSystem(SwiftSourceSystemBuilder.createSwiftSourceSystemIMS().getSwiftSourceSystem());
		swiftIdentifier.setSourceSystemModule(SwiftSourceSystemModuleBuilder.createSwiftSourceSystemModuleIMS_MTM().getSwiftSourceSystemModule());
		swiftIdentifier.setSourceSystemFkFieldId(Long.valueOf(SwiftSourceSystemBuilder.createSwiftSourceSystemIMS().getSwiftSourceSystem().getId()));

		return new SwiftIdentifierBuilder(swiftIdentifier);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftIdentifierBuilder withId(long id) {
		this.swiftIdentifier.setId(id);
		return this;
	}


	public SwiftIdentifierBuilder withUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.swiftIdentifier.setUniqueStringIdentifier(uniqueStringIdentifier);
		return this;
	}


	public SwiftIdentifierBuilder withSourceSystem(SwiftSourceSystem swiftSourceSystem) {
		this.swiftIdentifier.setSourceSystem(swiftSourceSystem);
		return this;
	}


	public SwiftIdentifierBuilder withSourceSystemModule(SwiftSourceSystemModule swiftSourceSystemModule) {
		this.swiftIdentifier.setSourceSystemModule(swiftSourceSystemModule);
		return this;
	}


	public SwiftIdentifierBuilder withSystemFkFieldId(long systemFkFieldId) {
		this.swiftIdentifier.setSourceSystemFkFieldId(systemFkFieldId);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftIdentifier getSwiftIdentifier() {
		return this.swiftIdentifier;
	}
}
