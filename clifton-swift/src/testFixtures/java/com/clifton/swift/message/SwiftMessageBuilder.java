package com.clifton.swift.message;

import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierBuilder;

import java.util.Date;


public class SwiftMessageBuilder {

	public static final String TEST_INSTRUCTION_MESSAGE = "{1:F01FOOSEDR0AXXX0000000000}{2:I202FOORECV0XXXXN}{4:\n" +
			":20:IMS:M2M:56872\n" +
			":21:MARG\n" +
			":32A:161026EUR10120,\n" +
			":53B:/12345678901234567890\n" +
			":57A:BANKANC0XXX\n" +
			":58A:/12345678901234567XXX\n" +
			"BANKANC0XXX\n}";

	private SwiftMessage swiftMessage;


	private SwiftMessageBuilder(SwiftMessage swiftMessage) {
		this.swiftMessage = swiftMessage;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftMessageBuilder createSwiftMessageMt202() {
		SwiftMessage swiftMessage = new SwiftMessage();

		swiftMessage.setId((long) 1);
		swiftMessage.setText(TEST_INSTRUCTION_MESSAGE);
		swiftMessage.setSenderBusinessIdentifierCode("SENDER_BIC");
		swiftMessage.setReceiverBusinessIdentifierCode("RECEIVER_BIC");
		swiftMessage.setMessageDateAndTime(new Date());
		swiftMessage.setIdentifier(SwiftIdentifierBuilder.createSwiftIdentifier().getSwiftIdentifier());
		swiftMessage.setIncoming(false);
		swiftMessage.setType(SwiftMessageTypeBuilder.createSwiftMessageTypeMt202().getSwiftMessageType());

		return new SwiftMessageBuilder(swiftMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageBuilder withId(long id) {
		this.swiftMessage.setId(id);
		return this;
	}


	public SwiftMessageBuilder withText(String text) {
		this.swiftMessage.setText(text);
		return this;
	}


	public SwiftMessageBuilder withSenderBusinessIdentifierCode(String senderBusinessIdentifierCode) {
		this.swiftMessage.setSenderBusinessIdentifierCode(senderBusinessIdentifierCode);
		return this;
	}


	public SwiftMessageBuilder withReceiverBusinessIdentifierCode(String receiverBusinessIdentifierCode) {
		this.swiftMessage.setReceiverBusinessIdentifierCode(receiverBusinessIdentifierCode);
		return this;
	}


	public SwiftMessageBuilder withMessageDateAndTime(Date messageDateAndTime) {
		this.swiftMessage.setMessageDateAndTime(messageDateAndTime);
		return this;
	}


	public SwiftMessageBuilder withIdentifier(SwiftIdentifier swiftIdentifier) {
		this.swiftMessage.setIdentifier(swiftIdentifier);
		return this;
	}


	public SwiftMessageBuilder withIncoming(boolean incoming) {
		this.swiftMessage.setIncoming(incoming);
		return this;
	}


	public SwiftMessageBuilder withType(SwiftMessageType swiftMessageType) {
		this.swiftMessage.setType(swiftMessageType);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessage getSwiftMessage() {
		return this.swiftMessage;
	}
}
