package com.clifton.swift.message;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class SwiftMessageTypeBuilder {

	private SwiftMessageType swiftMessageType;


	private SwiftMessageTypeBuilder(SwiftMessageType swiftMessageType) {
		this.swiftMessageType = swiftMessageType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftMessageTypeBuilder createSwiftMessageTypeMt202() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 1);
		swiftMessageType.setName("General Financial Institution Transfer Message");
		swiftMessageType.setDescription("General Financial Institution Transfer Message");
		swiftMessageType.setSystemMessage(false);
		swiftMessageType.setMtVersionName("MT202");
		swiftMessageType.setMxVersionName("MxPacs00900102");

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}


	public static SwiftMessageTypeBuilder createSwiftMessageTypeMt210() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 2);
		swiftMessageType.setName("Notice To Receive Transfer Message");
		swiftMessageType.setDescription("Notice To Receive Transfer Message");
		swiftMessageType.setSystemMessage(false);
		swiftMessageType.setMtVersionName("MT210");

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}


	public static SwiftMessageTypeBuilder createSwiftMessageTypeAck() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 3);
		swiftMessageType.setName("MT ACK/NAK");
		swiftMessageType.setDescription("An MT ACK/NAK message");
		swiftMessageType.setSystemMessage(true);

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}


	public static SwiftMessageTypeBuilder createSwiftMessageTypeUnknown() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 4);
		swiftMessageType.setName("Unknown System Messages");
		swiftMessageType.setDescription("Any unknown system message");
		swiftMessageType.setSystemMessage(true);

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}


	public static SwiftMessageTypeBuilder createSwiftMessageTypeMt019() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 5);
		swiftMessageType.setName("Abort Notification");
		swiftMessageType.setDescription("This message notifies the sender that the system has been unable to deliver the message specified in the <text-block>, and has been forced to abort it instead.<br/><br/>If the aborted message contains a field tag 103 (in the user header) or was copied through FINInform in Y-copy mode, the field tag 619 containing a copy of the field tag 103 is added to the MT 019 format. This applies to all codes recorded in a field tag 103.");
		swiftMessageType.setSystemMessage(true);
		swiftMessageType.setMtVersionName("MT019");

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}


	public static SwiftMessageTypeBuilder createSwiftMessageTypeMt081() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 6);
		swiftMessageType.setName("Daily Check Report");
		swiftMessageType.setDescription("This message lists the number of messages sent and received for all FIN or General Purpose Application sessions closed since the previous Daily Check Report.");
		swiftMessageType.setSystemMessage(true);
		swiftMessageType.setMtVersionName("MT081");

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}


	public static SwiftMessageTypeBuilder createSwiftMessageTypeMt082() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 7);
		swiftMessageType.setName("Undelivered Message Report at a Fixed Hour");
		swiftMessageType.setDescription("This message is generated at a time, local to the user, specified in an MT 044 Undelivered Report Rules Redefinition, and lists all undelivered messages at generation time.<br/><br/>SWIFT has developed a process that is designed to generate UNDELV reports that reflect the situation no more than 15 minutes before the event that led to the cold start. The report is in the form of the MT 082 Undelivered Message Report at a Fixed Hour. Delivery of this special UNDELV report occurs through the normal channels, once the user's FIN logical terminal has successfully logged in and selected the FIN service.");
		swiftMessageType.setSystemMessage(true);
		swiftMessageType.setMtVersionName("MT082");

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}


	public static SwiftMessageTypeBuilder createSwiftMessageTypeMt094() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();

		swiftMessageType.setId((short) 54);
		swiftMessageType.setName("Broadcast");
		swiftMessageType.setDescription("This message is the result of a system or user request to broadcast information. It is the response to an MT 074 Broadcast Request.");
		swiftMessageType.setSystemMessage(true);
		swiftMessageType.setMtVersionName("MT094");

		return new SwiftMessageTypeBuilder(swiftMessageType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static List<SwiftMessageType> getSwiftMessageTypeList() {
		return Arrays.stream(SwiftMessageTypeBuilder.class.getMethods())
				.filter(m -> m.getName().startsWith("create"))
				.filter(m -> m.getReturnType().equals(SwiftMessageTypeBuilder.class))
				.filter(m -> Modifier.isStatic(m.getModifiers()))
				.filter(m -> Modifier.isPublic(m.getModifiers()))
				.filter(m -> m.getParameterCount() == 0)
				.map(m -> {
					try {
						return SwiftMessageTypeBuilder.class.cast(m.invoke(null)).getSwiftMessageType();
					}
					catch (Exception e) {
						throw new RuntimeException(e);
					}
				}).collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageType getSwiftMessageType() {
		return this.swiftMessageType;
	}
}
