package com.clifton.swift.message.log;

import java.util.Date;


public class SwiftMessageLogBuilder {

	public static final String TEST_INSTRUCTION_MESSAGE = "{1:F01PPSCUS60AXXX0013000161}{2:I210CNORUS40XETDN}{4:" +
			":20:IMS471747MTM" +
			":25:RYC02" +
			":30:161222" +
			":21:MARG" +
			":32B:USD34391" +
			":52A:MSNYUS30" +
			":56A:IRVTUS30XXX" +
			"-}{5:{MAC:00000000}{CHK:7D6B3A9E818F}{TNG:}}";

	public static final String SWIFT_ERROR_MESSAGE = "Failed to processes incoming SWIFT message [AbstractMT [m=com.prowidesoftware.swift.model.SwiftMessage@28a4d194[b=com.prowidesoftware.swift.model.SwiftBlock1@437b7cf2[a=F,b=21,c=PPSCUS60AXXX,d=0013,e=000161,id=<null>,unparsedTexts=<null>,input=<null>,output=<null>,blockType=<null>],c=<null>,d=<null>,e=com.prowidesoftware.swift.model.SwiftBlock4[[Tag[177:1612221244], Tag[451:0]]],f=<null>,g=<null>,userBlocks=<null>,unparsedTexts=com.prowidesoftware.swift.model.UnparsedTextList@36454219[a=<null>,b=[{1:F01PPSCUS60AXXX0013000161}{2:I210CNORUS40XETDN}{4:" +
			":20:IMS471747MTM" +
			":25:RYC02" +
			":30:161222" +
			":21:MARG" +
			":32B:USD34391," +
			":52A:MSNYUS30" +
			":56A:IRVTUS30XXX" +
			"-}{5:{MAC:00000000}{CHK:7D6B3A9E818F}{TNG:}}]],id=<null>,h=false]]]." +
			"CAUSED BY: java.lang.RuntimeException: Failed to processes incoming SWIFT message [AbstractMT [m=com.prowidesoftware.swift.model.SwiftMessage@28a4d194[b=com.prowidesoftware.swift.model.SwiftBlock1@437b7cf2[a=F,b=21,c=PPSCUS60AXXX,d=0013,e=000161,id=<null>,unparsedTexts=<null>,input=<null>,output=<null>,blockType=<null>],c=<null>,d=<null>,e=com.prowidesoftware.swift.model.SwiftBlock4[[Tag[177:1612221244], Tag[451:0]]],f=<null>,g=<null>,userBlocks=<null>,unparsedTexts=com.prowidesoftware.swift.model.UnparsedTextList@36454219[a=<null>,b=[{1:F01PPSCUS60AXXX0013000161}{2:I210CNORUS40XETDN}{4:" +
			":20:IMS471747MTM" +
			":25:RYC02" +
			":30:161222" +
			":21:MARG" +
			":32B:USD34391," +
			":52A:MSNYUS30" +
			":56A:IRVTUS30XXX" +
			"-}{5:{MAC:00000000}{CHK:7D6B3A9E818F}{TNG:}}]],id=<null>,h=false]]]." +
			"at com.clifton.swift.prowide.server.router.ProwideSwiftServerMessageRouterImpl.receive_aroundBody0(ProwideSwiftServerMessageRouterImpl.java:61)" +
			"at com.clifton.swift.prowide.server.router.ProwideSwiftServerMessageRouterImpl$AjcClosure1.run(ProwideSwiftServerMessageRouterImpl.java:1)" +
			"at org.aspectj.runtime.reflect.JoinPointImpl.proceed(JoinPointImpl.java:149)" +
			"at com.clifton.core.dataaccess.transactions.HibernateAspectJAnnotationTransactionAspect$1.proceedWithInvocation(HibernateAspectJAnnotationTransactionAspect.java:129)" +
			"at org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:281)" +
			"at com.clifton.core.dataaccess.transactions.HibernateAspectJAnnotationTransactionAspect.setTransactionManager(HibernateAspectJAnnotationTransactionAspect.java:125)" +
			"at com.clifton.swift.prowide.server.router.ProwideSwiftServerMessageRouterImpl.receive(ProwideSwiftServerMessageRouterImpl.java:54)" +
			"at com.clifton.swift.incoming.file.handler.SwiftProcessedReceptionFileHandler.lambda$handleFile$0(SwiftProcessedReceptionFileHandler.java:49)" +
			"at com.clifton.swift.message.log.SwiftMessageLogHandlerImpl.callWithTransaction_aroundBody0(SwiftMessageLogHandlerImpl.java:59)" +
			"at com.clifton.swift.message.log.SwiftMessageLogHandlerImpl$AjcClosure1.run(SwiftMessageLogHandlerImpl.java:1)" +
			"at org.aspectj.runtime.reflect.JoinPointImpl.proceed(JoinPointImpl.java:149)" +
			"at com.clifton.core.dataaccess.transactions.HibernateAspectJAnnotationTransactionAspect$1.proceedWithInvocation(HibernateAspectJAnnotationTransactionAspect.java:129)" +
			"at org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:281)" +
			"at com.clifton.core.dataaccess.transactions.HibernateAspectJAnnotationTransactionAspect.setTransactionManager(HibernateAspectJAnnotationTransactionAspect.java:125)" +
			"at com.clifton.swift.message.log.SwiftMessageLogHandlerImpl.callWithTransaction(SwiftMessageLogHandlerImpl.java:59)" +
			"at com.clifton.swift.message.log.SwiftMessageLogHandlerImpl.processSwiftMessage(SwiftMessageLogHandlerImpl.java:38)" +
			"at com.clifton.swift.incoming.file.handler.SwiftProcessedReceptionFileHandler.handleFile(SwiftProcessedReceptionFileHandler.java:46)" +
			"at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)" +
			"at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)" +
			"at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.j...";

	private SwiftMessageLog swiftMessageLog;


	private SwiftMessageLogBuilder(SwiftMessageLog swiftMessageLog) {
		this.swiftMessageLog = swiftMessageLog;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftMessageLogBuilder createSwiftMessageLogMt210() {
		SwiftMessageLog swiftMessageLog = new SwiftMessageLog();

		swiftMessageLog.setId((long) 1);
		swiftMessageLog.setText(TEST_INSTRUCTION_MESSAGE);
		swiftMessageLog.setMessageDateAndTime(new Date());
		swiftMessageLog.setIncoming(false);
		swiftMessageLog.setErrorMessage(SWIFT_ERROR_MESSAGE);

		return new SwiftMessageLogBuilder(swiftMessageLog);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageLogBuilder withId(long id) {
		this.swiftMessageLog.setId(id);
		return this;
	}


	public SwiftMessageLogBuilder withText(String text) {
		this.swiftMessageLog.setText(text);
		return this;
	}


	public SwiftMessageLogBuilder withMessageDateAndTime(Date messageDateAndTime) {
		this.swiftMessageLog.setMessageDateAndTime(messageDateAndTime);
		return this;
	}


	public SwiftMessageLogBuilder withIncoming(boolean incoming) {
		this.swiftMessageLog.setIncoming(incoming);
		return this;
	}


	public SwiftMessageLogBuilder withErrorMessage(String errorMessage) {
		this.swiftMessageLog.setErrorMessage(errorMessage);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageLog getSwiftMessageLog() {
		return this.swiftMessageLog;
	}
}
