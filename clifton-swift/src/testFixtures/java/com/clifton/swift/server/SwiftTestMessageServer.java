package com.clifton.swift.server;

import com.clifton.swift.server.message.SwiftTestCommands;
import com.clifton.swift.server.message.SwiftTestMessageConduit;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatusMessage;
import com.clifton.swift.server.runner.SwiftTestServerRunner;
import com.clifton.swift.server.runner.autoclient.SwiftTestAutoClient;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;
import org.springframework.context.ApplicationContext;

import java.time.Duration;
import java.time.temporal.ChronoUnit;


/**
 * <code>SwiftTestMessageServer</code> boot the Fake Auto Client in its own thread, Spring context and embedded database.  Additionally, it provides a bidirectional messaging
 * conduit between a JUnit Test and the Fake Auto Client.
 */
public class SwiftTestMessageServer {

	private SwiftTestMessageConduit swiftTestStatusMessageConduit;
	private SwiftTestMessageConduit swiftTestCommandMessageConduit;
	private SwiftTestAutoClient<?> swiftTestAutoClient;

	private final SwiftTestServerRunner runner;
	private final String testDataPath;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestMessageServer(String propertiesPath, String testDataPath) {
		this.runner = new SwiftTestServerRunner(propertiesPath);
		this.testDataPath = testDataPath;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public void start() throws Exception {
		ApplicationContext applicationContext = this.runner.startup();
		if (applicationContext == null) {
			throw new RuntimeException("Could not start Swift server.");
		}
		initializeFromContext(applicationContext);
		this.swiftTestAutoClient.startup();
	}


	public void shutdown() throws Exception {
		this.runner.shutdown();
		if (this.swiftTestAutoClient != null) {
			this.swiftTestAutoClient.shutdown();
		}
	}


	public ApplicationContext getApplicationContext() {
		return this.runner.getApplicationContext();
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestStatusMessage awaitStatusMessage(SwiftTestMessageStatuses status, Duration duration) throws InterruptedException {
		SwiftTestStatusMessage statusMessage = this.swiftTestStatusMessageConduit.awaitForStatus(status, duration);
		if (statusMessage == null) {
			throw new RuntimeException("Timed out waiting on response");
		}
		return statusMessage;
	}


	public String sendStartTestSession() {
		this.swiftTestCommandMessageConduit.sendCommandMessage(SwiftTestCommands.START_TEST_SESSION);
		try {
			SwiftTestStatusMessage swiftTestMessageStatus = this.swiftTestStatusMessageConduit.awaitForStatus(SwiftTestMessageStatuses.STARTED_TEST_SESSION, Duration.of(5, ChronoUnit.SECONDS));
			if (swiftTestMessageStatus == null) {
				throw new RuntimeException("Timed out waiting for the response to the command to start the session.");
			}
			return swiftTestMessageStatus.getPayload();
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Test thread was interrupted unexpectedly.", e);
		}
	}


	public void sendStopTestSession() {
		this.swiftTestCommandMessageConduit.sendCommandMessage(SwiftTestCommands.END_TEST_SESSION);
		try {
			SwiftTestStatusMessage swiftTestMessageStatus = this.swiftTestStatusMessageConduit.awaitForStatus(SwiftTestMessageStatuses.ENDED_TEST_SESSION, Duration.of(5, ChronoUnit.SECONDS));
			if (swiftTestMessageStatus == null) {
				throw new RuntimeException("Timed out waiting for the response to the command to end the session.");
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Test thread was interrupted unexpectedly.", e);
		}
	}


	public void sendSwiftTestCase(SwiftTestCase swiftTestCase) {
		this.swiftTestCommandMessageConduit.sendTestCase(swiftTestCase);
		try {
			SwiftTestStatusMessage swiftTestMessageStatus = this.swiftTestStatusMessageConduit.awaitForStatus(SwiftTestMessageStatuses.RECEIVED_TEST_CASE, Duration.of(5, ChronoUnit.SECONDS));
			if (swiftTestMessageStatus == null) {
				throw new RuntimeException("Timed out waiting for the response to the command to receive test session.");
			}
			if (swiftTestMessageStatus.getStatus() != SwiftTestMessageStatuses.RECEIVED_TEST_CASE) {
				throw new RuntimeException("Did not receive Receive test case confirmation " + swiftTestMessageStatus.getStatus());
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Test thread was interrupted unexpectedly.", e);
		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private void initializeFromContext(ApplicationContext applicationContext) {
		SwiftTestMessageMigration swiftTestMessageMigration = applicationContext.getBean(SwiftTestMessageMigration.class);
		swiftTestMessageMigration.migrate(this.testDataPath);
		this.swiftTestStatusMessageConduit = (SwiftTestMessageConduit) applicationContext.getBean("swiftTestStatusMessageConduit");
		this.swiftTestCommandMessageConduit = (SwiftTestMessageConduit) applicationContext.getBean("swiftTestCommandMessageConduit");
		this.swiftTestAutoClient = applicationContext.getBean(SwiftTestAutoClient.class);
	}
}
