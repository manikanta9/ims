package com.clifton.swift.server.runner.autoclient;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.mt.AbstractMT;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * <code>SwiftEquivalence</code> does the symmetric comparison between two messages and stores the differences.
 */
public class SwiftEquivalence {

	private static final BiFunction<Set<Field>, Set<Field>, List<Field>> symmetricDifference = (tags1, tags2) -> {
		Set<Field> elements1 = new HashSet<>(tags1);
		Set<Field> elements2 = new HashSet<>(tags2);

		Set<Field> result = new HashSet<>(elements1);
		result.addAll(elements2);
		elements1.retainAll(elements2);
		result.removeAll(elements1);

		return new ArrayList<>(result);
	};

	private static final BiFunction<List<Field>, List<Field>, List<Pair<Field, Field>>> cartesianProduct = (incoming, template) ->
			incoming.stream()
					.flatMap(i -> template.stream().map(t -> Pair.of(i, t)))
					.collect(Collectors.toList());

	private static final BiFunction<Set<Field>, Set<Field>, List<SwiftTagDifference>> buildDifferences = (incoming, template) -> {
		// for each difference, move the field to it's originating list.
		final List<Pair<List<Field>, List<Field>>> differencePairs = symmetricDifference.apply(incoming, template).stream()
				.collect(Collectors.groupingBy(Field::getName))
				.entrySet()
				.stream()
				.map(e -> {
					List<Field> in = new ArrayList<>();
					List<Field> tpt = new ArrayList<>();
					e.getValue().forEach(f -> {
						if (incoming.contains(f)) {
							in.add(f);
						}
						if (template.contains(f)) {
							tpt.add(f);
						}
					});
					if (in.isEmpty()) {
						in.add(new MissingField(e.getValue().iterator().next()));
					}
					if (tpt.isEmpty()) {
						tpt.add(new MissingField(e.getValue().iterator().next()));
					}
					return Pair.of(in, tpt);
				})
				.collect(Collectors.toList());

		// do cartesian product for fields difference list
		final List<Pair<Field, Field>> cartesionProduct = differencePairs.stream()
				.flatMap(p -> cartesianProduct.apply(p.getFirst(), p.getSecond()).stream())
				.collect(Collectors.toList());

		// get degree of difference and map by field (with value).
		final Map<Field, List<Triple<Field, Field, Integer>>> differenceByDegree = cartesionProduct.stream()
				.map(p -> {
					List<String> first = p.first.getComponents();
					List<String> second = p.second.getComponents();
					int size = first.size() < second.size() ? second.size() : first.size();
					int degree = 0;
					for (int i = 0; i < size; i++) {
						String a = i < first.size() ? first.get(i) : "";
						String b = i < second.size() ? second.get(i) : "";
						if (!StringUtils.isEqual(a, b)) {
							degree++;
						}
					}
					return Triple.of(p.first, p.second, degree);
				})
				.collect(Collectors.groupingBy(Triple::getFirst));

		// create differences from the two fields using matching field with the lowest degree of difference.
		return differenceByDegree.entrySet().stream()
				.map(e ->
						e.getValue().stream()
								// lowest degree of difference should match up fields with the same qualifiers.
								.min(Comparator.comparing(Triple::getThird))
								.flatMap(minimum -> Optional.of(SwiftTagDifference.of(minimum.getFirst().getName(), () -> minimum.getSecond().getValue(), () -> minimum.getFirst().getValue())))
								.orElse(null)
				)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	};

	private final List<SwiftTagDifference> differences;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private SwiftEquivalence(List<SwiftTagDifference> differences) {
		this.differences = differences;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Perform the symmetric comparison between the provided template and request messages and returns the differences.
	 */
	public static SwiftEquivalence areEquivalent(String template, String request) {
		List<SwiftTagDifference> differences = doEquivalence(template, request);
		return new SwiftEquivalence(differences);
	}


	private static List<SwiftTagDifference> doEquivalence(String template, String request) {
		try {
			AbstractMT templateAbstractMT = AbstractMT.parse(template);
			final Set<Field> templateList = new HashSet<>(SwiftMessageConverter.getFieldList(templateAbstractMT.getSwiftMessage()));

			AbstractMT requestAbstractMT = AbstractMT.parse(request);
			final Set<Field> requestList = new HashSet<>(SwiftMessageConverter.getFieldList(requestAbstractMT.getSwiftMessage()));

			if (!Objects.equals(templateList, requestList)) {
				return buildDifferences.apply(requestList, templateList);
			}
		}
		catch (IOException e) {
			throw new RuntimeException("Could not parse messages", e);
		}

		return Collections.emptyList();
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "SwiftEquivalence{" +
				"different=" + !this.differences.isEmpty() +
				", differences=" + this.differences +
				'}';
	}


	public boolean isDifferent() {
		return !this.differences.isEmpty();
	}


	public List<SwiftTagDifference> getDifferences() {

		return this.differences;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	private static class MissingField extends Field {

		private Field field;


		MissingField(Field field) {
			super(0);
			this.field = field;
			this.components = new ArrayList<>();
		}


		@Override
		public void parse(String s) {
			AssertUtils.fail("Parse not supported.");
		}


		@Override
		public String getValueDisplay(int i, Locale locale) {
			AssertUtils.fail("getValueDisplay not supported.");
			return null;
		}


		@Override
		public String getValue() {
			return this.joinComponents();
		}


		@Override
		public String getName() {
			return this.field.getName();
		}


		@Override
		public String componentsPattern() {
			return this.field.componentsPattern();
		}


		@Override
		public String parserPattern() {
			return this.field.parserPattern();
		}


		@Override
		public String validatorPattern() {
			return this.field.validatorPattern();
		}


		@Override
		public boolean isOptional(int i) {
			return this.field.isOptional(i);
		}


		@Override
		public boolean isGeneric() {
			return this.field.isGeneric();
		}


		@Override
		public int componentsSize() {
			return 0;
		}


		@Override
		protected List<String> getComponentLabels() {
			return Collections.emptyList();
		}


		@Override
		protected Map<Integer, String> getComponentMap() {
			return Collections.emptyMap();
		}
	}

	private static class Pair<A, B> {

		private final A first;
		private final B second;


		private Pair(A first, B second) {
			this.first = first;
			this.second = second;
		}


		public static <A, B> Pair<A, B> of(A first, B second) {
			return new Pair<>(first, second);
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			Pair<?, ?> tuple = (Pair<?, ?>) o;
			return Objects.equals(this.first, tuple.first) &&
					Objects.equals(this.second, tuple.second);
		}


		@Override
		public int hashCode() {

			return Objects.hash(this.first, this.second);
		}


		public A getFirst() {
			return this.first;
		}


		public B getSecond() {
			return this.second;
		}
	}

	private static class Triple<A, B, C> {

		private final A first;
		private final B second;
		private final C third;


		private Triple(A first, B second, C third) {
			this.first = first;
			this.second = second;
			this.third = third;
		}


		public static <A, B, C> Triple<A, B, C> of(A first, B second, C third) {
			return new Triple<>(first, second, third);
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			Triple<?, ?, ?> triple = (Triple<?, ?, ?>) o;
			return Objects.equals(this.first, triple.first) &&
					Objects.equals(this.second, triple.second) &&
					Objects.equals(this.third, triple.third);
		}


		@Override
		public int hashCode() {

			return Objects.hash(this.first, this.second, this.third);
		}


		@Override
		public String toString() {
			return "Triple{" +
					"first=" + this.first +
					", second=" + this.second +
					", third=" + this.third +
					'}';
		}


		public A getFirst() {
			return this.first;
		}


		public B getSecond() {
			return this.second;
		}


		public C getThird() {
			return this.third;
		}
	}

	public static class SwiftTagDifference {

		private final String tagNumber;
		private final String templateValue;
		private final String requestValue;


		private SwiftTagDifference(String tagNumber, String templateValue, String requestValue) {
			this.tagNumber = tagNumber;
			this.templateValue = templateValue;
			this.requestValue = requestValue;
		}


		public static SwiftTagDifference of(String tagNumber, Supplier<String> templateValue, Supplier<String> requestValue) {
			return new SwiftTagDifference(tagNumber, templateValue.get(), requestValue.get());
		}


		@Override
		public String toString() {
			return "SwiftTagDifference{" +
					"tagNumber='" + this.tagNumber + '\'' +
					", templateValue='" + this.templateValue + '\'' +
					", requestValue='" + this.requestValue + '\'' +
					'}';
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			SwiftTagDifference that = (SwiftTagDifference) o;
			return Objects.equals(this.tagNumber, that.tagNumber) &&
					Objects.equals(this.templateValue, that.templateValue) &&
					Objects.equals(this.requestValue, that.requestValue);
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.tagNumber, this.templateValue, this.requestValue);
		}


		public String getTagNumber() {
			return this.tagNumber;
		}


		public String getTemplateValue() {
			return this.templateValue;
		}


		public String getRequestValue() {
			return this.requestValue;
		}
	}
}
