package com.clifton.swift.server.runner.autoclient;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.server.message.SwiftTestCommandMessage;
import com.clifton.swift.server.message.SwiftTestMessageConduit;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatus;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.util.SwiftUtilHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;


/**
 * <code>SwiftTestAutoClient</code> A 'fake' Auto Client, this will intercept IMS sourced JMS Swift messages and return an appropriate response.
 */
@Component("swiftServerOutgoingFileMessageHandler")
public class SwiftTestAutoClient<M> implements SwiftServerOutgoingFileMessageHandler {

	private SwiftServerMessageRouter<M> swiftServerMessageRouter;
	private SwiftUtilHandler<M> swiftUtilHandler;

	private SwiftTestMessageConduit swiftTestStatusMessageConduit;
	private SwiftTestMessageConduit swiftTestCommandMessageConduit;

	// separate thread to monitor the command queue
	private final ExecutorService executorService = Executors.newSingleThreadExecutor();
	private Future<?> conduitHandlerFuture;

	// the test session
	private AtomicReference<SwiftTestSession> currentSession = new AtomicReference<>(null);

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void send(SwiftMessage swiftMessage) {
		AssertUtils.assertNotNull(swiftMessage, "Swift Message cannot be null.");
		String request = swiftMessage.getText();
		SwiftTestResponse swiftTestResponse = getResponse(request);
		if (swiftTestResponse == null) {
			getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.FAILED, "The response could not be found for the request " + request);
			this.currentSession.set(null);
		}
		else if (swiftTestResponse.isError()) {
			getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.FAILED, swiftTestResponse.toString());
		}
		else {
			M message = getSwiftUtilHandler().parseSingleMessage(swiftTestResponse.getResponse(), SwiftMessageFormats.MT);
			SwiftServerMessageRouterCommand<M> command = SwiftServerMessageRouterCommand.ofMessageAndString(message, swiftTestResponse.getResponse());
			getSwiftServerMessageRouter().receive(command);
			if (swiftTestResponse.getStatus() == SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE) {
				getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, "Last Template Response");
				setSessionFinished();
			}
			else {
				getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.INTERMEDIATE_RESULT, "Intermediate Test Response");
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public void startup() {
		this.conduitHandlerFuture = this.executorService.submit(() -> {
			// the command queue handler.
			while (!Thread.interrupted()) {
				try {
					SwiftTestCommandMessage<?> swiftTestCommandMessage = this.getSwiftTestCommandMessageConduit().awaitForCommandMessage();
					switch (swiftTestCommandMessage.getSwiftCommand()) {
						case SEND_TEST_CASE: {
							SwiftTestSession swiftTestSession = SwiftTestAutoClient.this.currentSession.get();
							if (swiftTestSession == null) {
								getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.FAILED, "The session has not be started yet.");
							}
							else {
								swiftTestSession.setSwiftTestCase((SwiftTestCase) swiftTestCommandMessage.getPayload());
								getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.RECEIVED_TEST_CASE, "received test case.");
							}
							break;
						}
						case END_TEST_SESSION: {
							SwiftTestSession swiftTestSession = SwiftTestAutoClient.this.currentSession.get();
							if (swiftTestSession == null) {
								getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.FAILED, "The session has not be started yet.");
							}
							else {
								swiftTestSession.setStatus(SwiftTestStatus.READY);
								getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.ENDED_TEST_SESSION, "ended session.");
							}
							break;
						}
						case START_TEST_SESSION: {
							SwiftTestSession swiftTestSession = new SwiftTestSession();
							SwiftTestAutoClient.this.currentSession.set(swiftTestSession);
							getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.STARTED_TEST_SESSION, swiftTestSession.getTestIdentifier());
							break;
						}
						default:
							LogUtils.warn(getClass(), "Unhandled Command " + swiftTestCommandMessage.getSwiftCommand());
					}
				}
				catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					// shutdown
				}
			}
		});
	}


	@PreDestroy
	public void shutdown() {
		if (this.conduitHandlerFuture != null && (!this.conduitHandlerFuture.isCancelled() || !this.conduitHandlerFuture.isDone())) {
			this.conduitHandlerFuture.cancel(false);
		}
		if (!this.executorService.isShutdown()) {
			this.executorService.shutdownNow();
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private SwiftTestResponse getResponse(String request) {
		SwiftTestSession swiftTestSession = SwiftTestAutoClient.this.currentSession.get();
		if (swiftTestSession == null) {
			getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.FAILED, "No session exist before receipt of request.");
			return null;
		}
		if (swiftTestSession.getStatus() != SwiftTestStatus.READY) {
			String message = "Session is not in ready state (" + swiftTestSession.getStatus() + ").  ";
			if (swiftTestSession.getSwiftTestCase() == null) {
				message += "The test case has not been set.";
			}
			getSwiftTestStatusMessageConduit().sendStatusMessage(SwiftTestMessageStatuses.FAILED, message);
			return null;
		}

		return swiftTestSession.getSwiftTestCase().getResponse(request);
	}


	private void setSessionFinished() {
		SwiftTestSession swiftTestSession = SwiftTestAutoClient.this.currentSession.get();
		if (swiftTestSession != null) {
			swiftTestSession.setStatus(SwiftTestStatus.FINISED);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestMessageConduit getSwiftTestStatusMessageConduit() {
		return this.swiftTestStatusMessageConduit;
	}


	public void setSwiftTestStatusMessageConduit(SwiftTestMessageConduit swiftTestStatusMessageConduit) {
		this.swiftTestStatusMessageConduit = swiftTestStatusMessageConduit;
	}


	public SwiftTestMessageConduit getSwiftTestCommandMessageConduit() {
		return this.swiftTestCommandMessageConduit;
	}


	public void setSwiftTestCommandMessageConduit(SwiftTestMessageConduit swiftTestCommandMessageConduit) {
		this.swiftTestCommandMessageConduit = swiftTestCommandMessageConduit;
	}


	public SwiftServerMessageRouter<M> getSwiftServerMessageRouter() {
		return this.swiftServerMessageRouter;
	}


	public void setSwiftServerMessageRouter(SwiftServerMessageRouter<M> swiftServerMessageRouter) {
		this.swiftServerMessageRouter = swiftServerMessageRouter;
	}


	public SwiftUtilHandler<M> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<M> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}
}
