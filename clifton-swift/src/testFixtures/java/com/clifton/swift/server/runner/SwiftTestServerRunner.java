package com.clifton.swift.server.runner;

import com.clifton.core.context.CurrentContextApplicationListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


public class SwiftTestServerRunner {

	public static final String SWIFT_CONTEXT_PROPERTIES = "swift.context.properties";
	public static final String SPRING_PROFILE_ACTIVE = "spring_profiles_active";
	public static final String CLASSPATH_TEST_SERVER_CONTEXT_XML = "classpath:com/clifton/swift/server/runner/swift-test-server-configuration-context.xml";

	private ClassPathXmlApplicationContext applicationContext;

	private final SpringRefreshedListener springRefreshedListener = new SpringRefreshedListener();
	private final SpringClosedListener springClosedListener = new SpringClosedListener();

	private String swiftContextProperties;


	public SwiftTestServerRunner(String swiftContextProperties) {
		this.swiftContextProperties = swiftContextProperties;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext startup() throws Exception {
		System.setProperty(SWIFT_CONTEXT_PROPERTIES, this.swiftContextProperties);
		System.setProperty(SPRING_PROFILE_ACTIVE, "swift");

		this.applicationContext = new ClassPathXmlApplicationContext(new String[]{CLASSPATH_TEST_SERVER_CONTEXT_XML}, false);
		this.applicationContext.getEnvironment().setActiveProfiles("swift");
		this.applicationContext.addApplicationListener(this.springRefreshedListener);
		this.applicationContext.addApplicationListener(this.springClosedListener);
		this.applicationContext.registerShutdownHook();
		this.applicationContext.refresh();
		this.springRefreshedListener.awaitRefresh();
		return this.applicationContext;
	}


	public void shutdown() throws Exception {
		try {
			if (this.applicationContext != null) {
				DataSource dataSource = (DataSource) this.applicationContext.getBean("dataSource");
				JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
				jdbcTemplate.execute("SHUTDOWN");
				this.applicationContext.close();
				this.springClosedListener.awaitClose();
				this.applicationContext = null;
			}
		}
		finally {
			System.clearProperty(SWIFT_CONTEXT_PROPERTIES);
			System.clearProperty(SPRING_PROFILE_ACTIVE);
		}
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	class SpringRefreshedListener implements CurrentContextApplicationListener<ContextRefreshedEvent> {

		private CountDownLatch contextInitializationLatch = new CountDownLatch(1);


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Override
		public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
			this.contextInitializationLatch.countDown();
		}


		public void awaitRefresh() throws InterruptedException {
			this.contextInitializationLatch.await(1, TimeUnit.MINUTES);
		}


		@Override
		public void setApplicationContext(ApplicationContext context) {
			//need to override for ApplicationContextListener interface
		}


		@Override
		public ApplicationContext getApplicationContext() {
			return SwiftTestServerRunner.this.applicationContext;
		}
	}


	class SpringClosedListener implements CurrentContextApplicationListener<ContextClosedEvent> {

		private CountDownLatch contextClosedLatch = new CountDownLatch(1);


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Override
		public void onCurrentContextApplicationEvent(ContextClosedEvent event) {
			this.contextClosedLatch.countDown();
		}


		public void awaitClose() throws InterruptedException {
			this.contextClosedLatch.await(3, TimeUnit.MINUTES);
		}


		@Override
		public void setApplicationContext(ApplicationContext context) {
			//need to override for ApplicationContextListener interface
		}


		@Override
		public ApplicationContext getApplicationContext() {
			return SwiftTestServerRunner.this.applicationContext;
		}
	}
}
