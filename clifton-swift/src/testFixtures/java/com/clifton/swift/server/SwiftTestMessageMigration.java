package com.clifton.swift.server;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonMigrationStrategy;
import com.clifton.core.dataaccess.dao.event.User;
import com.clifton.core.dataaccess.migrate.MigrationJdbcTemplate;
import com.clifton.core.dataaccess.migrate.MigrationUtils;
import com.clifton.core.dataaccess.migrate.execution.action.MigrationExecutionActionHandlerImpl;
import com.clifton.core.dataaccess.migrate.execution.database.MigrationExecutionMSSQLDatabaseHandlerImpl;
import com.clifton.core.dataaccess.migrate.reader.MigrationDefinitionReader;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.dataaccess.migrate.schema.sql.Sql;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


/**
 * <code>SwiftTestMessageMigration</code> uses the provided data path to the XML based migration file containing the data necessary to populate the SWIFT server with
 * enough information for the server to boot and process the test messages.
 */
public class SwiftTestMessageMigration implements ApplicationContextAware {

	private static final String DATA_MIGRATION_FILE = "Data.xml";
	private static final String EXPORT_SQL_FILE_SUFFIX = "DataExport.sql";

	private final MigrationDefinitionReader migrationDefinitionReader;
	private final MigrationJdbcTemplate migrationJdbcTemplate;
	private final JsonHandler<JacksonMigrationStrategy> jsonHandler;

	private ApplicationContext applicationContext;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestMessageMigration(MigrationDefinitionReader migrationDefinitionReader, MigrationJdbcTemplate migrationJdbcTemplate, JsonHandler<JacksonMigrationStrategy> jsonHandler) {
		this.migrationDefinitionReader = migrationDefinitionReader;
		this.migrationJdbcTemplate = migrationJdbcTemplate;
		this.jsonHandler = jsonHandler;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public void migrate(String migrationDataPath) {
		Map<String, Action> actionMap = MigrationUtils.populateActionMap(this.applicationContext);
		List<Sql> sqlList = new ArrayList<>();
		List<Action> actionList = new ArrayList<>();

		Consumer<Resource> resourceConsumer = resource -> {
			if (resource.exists()) {
				Schema schema = this.migrationDefinitionReader.loadMigration(null, resource);
				sqlList.addAll(schema.getSqlList());
				actionList.addAll(schema.getActionList());
			}
		};

		resourceConsumer.accept(new ClassPathResource(migrationDataPath + "/" + DATA_MIGRATION_FILE));

		executeSql(sqlList);
		executeActions(actionList, actionMap);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private void executeSql(List<Sql> sqlList) {
		MigrationExecutionMSSQLDatabaseHandlerImpl executionDatabaseHandler = new MigrationExecutionMSSQLDatabaseHandlerImpl();
		executionDatabaseHandler.setMigrationJdbcTemplate(this.migrationJdbcTemplate);
		for (Sql sql : CollectionUtils.getIterable(sqlList)) {
			for (String sqlString : CollectionUtils.getIterable(sql.getStatementList())) {
				executionDatabaseHandler.execute(sqlString);
			}
		}
	}


	private void executeActions(List<Action> actionList, Map<String, Action> actionMap) {
		MigrationExecutionActionHandlerImpl executionActionHandler = new MigrationExecutionActionHandlerImpl();
		executionActionHandler.setJsonHandler(this.jsonHandler);
		executionActionHandler.setMigrationUser(getUser());
		executionActionHandler.executeActions(actionList, actionMap, this.applicationContext);
	}


	private IdentityObject getUser() {
		User user = new User();
		user.setId((short) 0);
		return user;
	}
}
