package com.clifton.swift.server.message;

/**
 * <code>SwiftTestCommandMessage</code> a {@link SwiftTestMessageConduit} directed message for communicating the various commands {@link SwiftTestCommands} from a Junit test
 * to the running Fake Auto Client.
 */
public class SwiftTestCommandMessage<P> implements SwiftTestMessage<P> {

	private final SwiftTestCommands command;


	public SwiftTestCommandMessage(SwiftTestCommands command) {
		this.command = command;
	}


	public SwiftTestCommands getSwiftCommand() {
		return this.command;
	}


	@Override
	public P getPayload() {
		return null;
	}
}
