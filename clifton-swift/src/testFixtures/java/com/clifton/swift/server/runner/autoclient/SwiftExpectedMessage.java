package com.clifton.swift.server.runner.autoclient;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.UnparsedTextList;
import com.prowidesoftware.swift.model.mt.AbstractMT;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <code>SwiftExpectedMessage</code> has a template message and the replacements values for tags that are expected to change.
 */
public class SwiftExpectedMessage {

	private String template;
	// [match value, replacement value]
	private Map<SwiftMessageComponentValue, SwiftMessageComponentValue> replacements = new HashMap<>();
	private List<SwiftCopyFromRequestTag> copyFromRequestTags = new ArrayList<>();

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private SwiftExpectedMessage(String template) {
		this.template = template;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Apply replacement values and and copy request values to the template message.
	 */
	public String getComparisonTemplate(String request) throws IOException {
		AbstractMT requestMessage = AbstractMT.parse(request);
		AbstractMT templateMessage = AbstractMT.parse(this.template);
		List<Map<AbstractMT, Map<String, List<Tag>>>> swiftTagMap = getAllSwiftMessageTagMaps(templateMessage);
		for (Map<AbstractMT, Map<String, List<Tag>>> messageToTagMap : swiftTagMap) {
			for (Map<String, List<Tag>> tagMap : messageToTagMap.values()) {
				performReplacements(tagMap);
				performCopies(tagMap, requestMessage);
			}
		}
		return swiftTagMap.stream()
				.flatMap(e -> e.keySet().stream())
				.peek(mt -> mt.getSwiftMessage().setUnparsedTexts(new UnparsedTextList()))
				.map(AbstractMT::message)
				.collect(Collectors.joining());
	}


	/**
	 * Only apply replacement values to the provided request.
	 */
	public String performComparisonReplacements(String request) throws IOException {
		AbstractMT requestMessage = AbstractMT.parse(request);
		List<Map<AbstractMT, Map<String, List<Tag>>>> messageTagMaps = getAllSwiftMessageTagMaps(requestMessage);
		for (Map<AbstractMT, Map<String, List<Tag>>> messageToTagMap : messageTagMaps) {
			for (Map<String, List<Tag>> tagMap : messageToTagMap.values()) {
				performReplacements(tagMap);
				performCopies(tagMap, requestMessage);
			}
		}
		return messageTagMaps.stream()
				.flatMap(e -> e.keySet().stream())
				.peek(mt -> mt.getSwiftMessage().setUnparsedTexts(new UnparsedTextList()))
				.map(AbstractMT::message)
				.collect(Collectors.joining());
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * do the replacements on the provided modifiedTagMap
	 */
	private void performReplacements(final Map<String, List<Tag>> modifiedTagMap) {
		for (Map.Entry<SwiftMessageComponentValue, SwiftMessageComponentValue> entry : this.replacements.entrySet()) {
			String tagName = entry.getKey().getTag();
			String oldTagValue = entry.getKey().getValue();
			String newTagValue = entry.getValue().getValue();
			List<Tag> tags = CollectionUtils.asNonNullList(modifiedTagMap.get(tagName));
			for (Tag tag : tags) {
				if (StringUtils.isEqual(tag.getValue(), oldTagValue)) {
					tag.setValue(newTagValue);
				}
			}
		}
	}


	/**
	 * Copy the tag values from the provided replacementValuesMessage to the modifiedTagMap.
	 */
	private void performCopies(final Map<String, List<Tag>> modifiedTagMap, final AbstractMT replacementValuesMessage) {
		final Map<String, List<Tag>> requestTagMap = SwiftMessageConverter.getTagMap(replacementValuesMessage.getSwiftMessage());
		for (SwiftCopyFromRequestTag copyFromRequestTag : this.copyFromRequestTags) {
			List<Tag> templateTags = CollectionUtils.asNonNullList(modifiedTagMap.get(copyFromRequestTag.getTag())).stream()
					.filter(t -> {
						if (StringUtils.isEmpty(copyFromRequestTag.getQualifier())) {
							return true;
						}
						return t.asField().getComponents().contains(copyFromRequestTag.getQualifier());
					}).collect(Collectors.toList());
			AssertUtils.assertTrue(templateTags.size() <= 1 || copyFromRequestTag.isMatchOrder(),
					"Can only copy [%s] from request to response for a single template tag match [%s].", copyFromRequestTag, templateTags.size());

			List<Tag> requestTags = CollectionUtils.asNonNullList(requestTagMap.get(copyFromRequestTag.getTag())).stream()
					.filter(t -> {
						if (StringUtils.isEmpty(copyFromRequestTag.getQualifier())) {
							return true;
						}
						return t.asField().getComponents().contains(copyFromRequestTag.getQualifier());
					}).collect(Collectors.toList());
			AssertUtils.assertTrue(requestTags.size() <= 1 || copyFromRequestTag.isMatchOrder(),
					"Can only copy [%s] from request to response for a single request tag match [%s].", copyFromRequestTag, requestTags.size());

			if (!templateTags.isEmpty() && !requestTags.isEmpty()) {
				if (copyFromRequestTag.isMatchOrder()) {
					AssertUtils.assertEquals(requestTags.size(), templateTags.size(),
							() -> String.format("Could not copy tag [%s] from server request to template cause the tag counts are not equal [%s] versus [%s]",
									copyFromRequestTag.getTag(), requestTags.size(), templateTags.size()));
					for (int i = 0; i < templateTags.size(); i++) {
						String requestValue = requestTags.get(i).getValue();
						templateTags.get(i).setValue(requestValue);
					}
				}
				else {
					String requestValue = requestTags.iterator().next().getValue();
					templateTags.iterator().next().setValue(requestValue);
				}
			}
		}
	}


	private List<Map<AbstractMT, Map<String, List<Tag>>>> getAllSwiftMessageTagMaps(AbstractMT abstractMT) throws IOException {
		List<Map<AbstractMT, Map<String, List<Tag>>>> results = new ArrayList<>();
		doGatherRecursive(abstractMT, results);
		return results;
	}


	private void doGatherRecursive(AbstractMT abstractMT, List<Map<AbstractMT, Map<String, List<Tag>>>> results) throws IOException {
		if (abstractMT == null || abstractMT.getSwiftMessage() == null) {
			return;
		}
		SwiftMessage swiftMessage = abstractMT.getSwiftMessage();
		Map<String, List<Tag>> templateMap = SwiftMessageConverter.getTagMap(swiftMessage);
		Map<AbstractMT, Map<String, List<Tag>>> messageMap = new LinkedHashMap<>();
		messageMap.put(abstractMT, templateMap);
		results.add(messageMap);
		UnparsedTextList unparsedTextList = swiftMessage.getUnparsedTexts();
		for (int i = 0; i < unparsedTextList.size(); i++) {
			if (unparsedTextList.isMessage(i)) {
				String messageText = unparsedTextList.getText(i);
				AbstractMT childMessage = AbstractMT.parse(messageText);
				doGatherRecursive(childMessage, results);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	public static class SwiftExpectedMessageBuilder {

		private SwiftExpectedMessage swiftExpectedMessage;


		private SwiftExpectedMessageBuilder(SwiftExpectedMessage swiftExpectedMessage) {
			this.swiftExpectedMessage = swiftExpectedMessage;
		}


		public static SwiftExpectedMessage.SwiftExpectedMessageBuilder create(String template) {
			return new SwiftExpectedMessage.SwiftExpectedMessageBuilder(new SwiftExpectedMessage(template));
		}


		public SwiftExpectedMessage.SwiftExpectedMessageBuilder withReplacement(String tag, String oldTagValue, String newTagValue) {
			this.swiftExpectedMessage.replacements.put(SwiftMessageComponentValue.of(tag, oldTagValue), SwiftMessageComponentValue.of(tag, newTagValue));
			return this;
		}


		public SwiftExpectedMessage.SwiftExpectedMessageBuilder withCopyRequestValue(String tag) {
			return withCopyRequestValue(tag, null);
		}


		public SwiftExpectedMessage.SwiftExpectedMessageBuilder withCopyRequestValue(String tag, boolean matchOrder) {
			this.swiftExpectedMessage.copyFromRequestTags.add(new SwiftCopyFromRequestTag(tag, null, true));
			return this;
		}


		public SwiftExpectedMessage.SwiftExpectedMessageBuilder withCopyRequestValue(String tag, String qualifier) {
			this.swiftExpectedMessage.copyFromRequestTags.add(new SwiftCopyFromRequestTag(tag, qualifier, false));
			return this;
		}


		public SwiftExpectedMessage build() {
			return this.swiftExpectedMessage;
		}
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private static class SwiftCopyFromRequestTag {

		private final String tag;
		private final String qualifier;
		private final boolean matchOrder;


		private SwiftCopyFromRequestTag(String tag, String qualifier, boolean matchOrder) {
			this.tag = tag;
			this.qualifier = qualifier;
			this.matchOrder = matchOrder;
		}


		public String getTag() {
			return this.tag;
		}


		public String getQualifier() {
			return this.qualifier;
		}


		public boolean isMatchOrder() {
			return this.matchOrder;
		}


		@Override
		public String toString() {
			return "SwiftCopyFromRequestTag{" +
					"tag='" + this.tag + '\'' +
					", qualifier='" + this.qualifier + '\'' +
					", matchOrder=" + this.matchOrder +
					'}';
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			SwiftCopyFromRequestTag that = (SwiftCopyFromRequestTag) o;
			return this.matchOrder == that.matchOrder &&
					Objects.equals(this.tag, that.tag) &&
					Objects.equals(this.qualifier, that.qualifier);
		}


		@Override
		public int hashCode() {

			return Objects.hash(this.tag, this.qualifier, this.matchOrder);
		}
	}

	private static class SwiftMessageComponentValue {

		private final String tag;
		private final String value;


		private SwiftMessageComponentValue(String tag, String value) {
			this.tag = tag;
			this.value = value;
		}


		public static SwiftMessageComponentValue of(String tag, String value) {
			return new SwiftMessageComponentValue(tag, value);
		}


		public String getTag() {
			return this.tag;
		}


		public String getValue() {
			return this.value;
		}
	}
}
