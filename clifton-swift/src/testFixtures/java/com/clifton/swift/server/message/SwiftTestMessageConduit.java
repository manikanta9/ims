package com.clifton.swift.server.message;

import com.clifton.core.logging.LogUtils;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;

import java.time.Duration;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;


/**
 * <code>SwiftTestMessageConduit</code> a communication conduit between a JUnit Test and the Fake Auto Client.
 */
public class SwiftTestMessageConduit {

	private final BlockingQueue<SwiftTestMessage<?>> messageQueue;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestMessageConduit(BlockingQueue<SwiftTestMessage<?>> messageQueue) {
		this.messageQueue = messageQueue;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public void sendStatusMessage(SwiftTestMessageStatuses status, String message) {
		send(new SwiftTestStatusMessage(status, message));
	}


	public void sendTestCase(SwiftTestCase swiftTestCase) {
		send(new SwiftTestCaseMessage(swiftTestCase));
	}


	public void sendCommandMessage(SwiftTestCommands command) {
		send(new SwiftTestCommandMessage<>(command));
	}


	public SwiftTestStatusMessage awaitForStatus(SwiftTestMessageStatuses status, Duration duration) throws InterruptedException {
		SwiftTestStatusMessage swiftTestMessage;
		do {
			swiftTestMessage = (SwiftTestStatusMessage) this.messageQueue.poll(duration.getSeconds(), TimeUnit.SECONDS);
			if (swiftTestMessage != null && swiftTestMessage.getStatus() == SwiftTestMessageStatuses.INTERMEDIATE_RESULT) {
				LogUtils.info(getClass(), "INTERMEDIATE TEST RESULT:  " + swiftTestMessage);
			}
		}
		while (swiftTestMessage != null && swiftTestMessage.getStatus() != status && swiftTestMessage.getStatus() != SwiftTestMessageStatuses.FAILED);

		return swiftTestMessage;
	}


	public SwiftTestCommandMessage<?> awaitForCommandMessage() throws InterruptedException {
		return (SwiftTestCommandMessage<?>) this.messageQueue.take();
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private void send(SwiftTestMessage<?> message) {
		try {
			this.messageQueue.put(message);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Error putting message to the queue.", e);
		}
	}
}
