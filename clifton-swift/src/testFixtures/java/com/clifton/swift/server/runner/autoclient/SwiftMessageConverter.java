package com.clifton.swift.server.runner.autoclient;

import com.prowidesoftware.swift.model.SwiftBlock3;
import com.prowidesoftware.swift.model.SwiftBlock4;
import com.prowidesoftware.swift.model.SwiftBlock5;
import com.prowidesoftware.swift.model.SwiftBlockUser;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.utils.BaseMessageVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>SwiftMessageConverter</code> extract a message's fields or tags from all blocks.
 */
public class SwiftMessageConverter extends BaseMessageVisitor {

	private final SwiftMessage swiftMessage;
	private final List<Tag> tagList = new ArrayList<>();


	private SwiftMessageConverter(SwiftMessage swiftMessage) {
		this.swiftMessage = swiftMessage;
	}


	/**
	 * Get all fields from the provided Swift message, this will include fields from all blocks.
	 * Fields provide component level information.
	 * Note:  fields are immutable.
	 */
	public static List<Field> getFieldList(SwiftMessage swiftMessage) {
		SwiftMessageConverter swiftMessageConverter = new SwiftMessageConverter(swiftMessage);
		return swiftMessageConverter.getFieldList();
	}


	/**
	 * Get all tags from the provided Swift message, this will include tags from all blocks.
	 * A tag's value is the concatenation of its component values.
	 * Note:  tags are mutable.
	 */
	public static List<Tag> getTagList(SwiftMessage swiftMessage) {
		SwiftMessageConverter swiftMessageConverter = new SwiftMessageConverter(swiftMessage);
		return swiftMessageConverter.getTagList();
	}


	/**
	 * Provides a map of the provided swift messages tags grouped by the tag name.
	 * The tag name is the numeric value concatenated with the letter option (19A).
	 */
	public static Map<String, List<Tag>> getTagMap(SwiftMessage swiftMessage) {
		return getTagList(swiftMessage).stream().collect(Collectors.groupingBy(Tag::getName));
	}


	private List<Field> getFieldList() {
		this.swiftMessage.visit(this);
		return this.tagList.stream().map(Tag::asField).collect(Collectors.toList());
	}


	private List<Tag> getTagList() {
		this.swiftMessage.visit(this);
		return this.tagList;
	}


	@Override
	public void tag(SwiftBlock3 swiftBlock3, Tag tag) {
		this.tagList.add(tag);
	}


	@Override
	public void tag(SwiftBlock4 swiftBlock4, Tag tag) {
		this.tagList.add(tag);
	}


	@Override
	public void tag(SwiftBlock5 swiftBlock5, Tag tag) {
		this.tagList.add(tag);
	}


	@Override
	public void tag(SwiftBlockUser swiftBlockUser, Tag tag) {
		this.tagList.add(tag);
	}
}
