package com.clifton.swift.server.runner.autoclient;

import com.clifton.swift.server.message.SwiftTestMessageStatuses;


/**
 * <code>SwiftTestResponse</code> the fake test server's response to a unit test initiated IMS Swift request message.
 * When the request matches the template test case request, the response field contains the Swift message to return
 * via JMS to the IMS server.  If the request did not match the expected template test case request, the response
 * message is return along with the swift equivalence object which provides textual information on how the messages
 * differed on a per tag basis.
 */
public class SwiftTestResponse {

	private final SwiftEquivalence swiftEquivalence;
	private final String response;
	private final SwiftTestMessageStatuses status;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private SwiftTestResponse(SwiftEquivalence swiftEquivalence, String response, SwiftTestMessageStatuses status) {
		this.swiftEquivalence = swiftEquivalence;
		this.response = response;
		this.status = status;
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "SwiftTestResponse{" +
				"swiftEquivalence=" + this.swiftEquivalence +
				", response='" + this.response + '\'' +
				", status=" + this.status +
				'}';
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public static SwiftTestResponse build(SwiftEquivalence swiftEquivalence, String message, SwiftTestMessageStatuses status) {
		return new SwiftTestResponse(swiftEquivalence, message, status);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftEquivalence getSwiftEquivalence() {
		return this.swiftEquivalence;
	}


	public String getResponse() {
		return this.response;
	}


	public boolean isError() {
		return this.status == SwiftTestMessageStatuses.FAILED;
	}


	public SwiftTestMessageStatuses getStatus() {
		return this.status;
	}
}
