package com.clifton.swift.server.runner.autoclient;

import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * <code>SwiftTestCase</code> stores the list of expected requests to responses for the current Swift Test Session.
 */
public class SwiftTestCase {

	private static final String TEMPLATE_ERROR_RESPONSE = "{1:F21PPSCUS66AXXX0072074855}{4:{177:1710180637}{451:1}}";

	private List<Map.Entry<SwiftExpectedMessage, SwiftExpectedMessage>> responseMap = new ArrayList<>();
	private int currentIndex = 0;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private SwiftTestCase() {
		// builder only
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestResponse getResponse(String request) {
		if (StringUtils.isEmpty(request)) {
			SwiftEquivalence swiftEquivalence = SwiftEquivalence.areEquivalent(null, request);
			return SwiftTestResponse.build(swiftEquivalence, "The request cannot be empty.", SwiftTestMessageStatuses.FAILED);
		}
		if (this.currentIndex < this.responseMap.size()) {
			try {
				Map.Entry<SwiftExpectedMessage, SwiftExpectedMessage> currentRequest = this.responseMap.get(this.currentIndex);
				String incomingRequest = currentRequest.getKey().performComparisonReplacements(request);
				String templateRequest = currentRequest.getKey().getComparisonTemplate(request);
				SwiftEquivalence requestEquivalence = SwiftEquivalence.areEquivalent(templateRequest, incomingRequest);
				String updatedResponse = currentRequest.getValue().getComparisonTemplate(request);
				if (requestEquivalence.isDifferent()) {
					return SwiftTestResponse.build(requestEquivalence, updatedResponse, SwiftTestMessageStatuses.FAILED);
				}
				this.currentIndex++;
				SwiftTestMessageStatuses statuses = this.currentIndex == this.responseMap.size() ? SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE : SwiftTestMessageStatuses.INTERMEDIATE_RESULT;
				return SwiftTestResponse.build(requestEquivalence, updatedResponse, statuses);
			}
			catch (Exception ex) {
				SwiftEquivalence swiftEquivalence = SwiftEquivalence.areEquivalent(TEMPLATE_ERROR_RESPONSE, request);
				return SwiftTestResponse.build(swiftEquivalence, "Error performing value replacement on the template messages " + ExceptionUtils.getDetailedMessage(ex), SwiftTestMessageStatuses.FAILED);
			}
		}
		else {
			SwiftEquivalence swiftEquivalence = SwiftEquivalence.areEquivalent(TEMPLATE_ERROR_RESPONSE, request);
			return SwiftTestResponse.build(swiftEquivalence, "The more responses were expected than were represented in the test case.", SwiftTestMessageStatuses.FAILED);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	public static class SwiftTestCaseBuilder {

		private SwiftTestCase swiftTestCase;


		private SwiftTestCaseBuilder(SwiftTestCase swiftTestCase) {
			this.swiftTestCase = swiftTestCase;
		}


		public static SwiftTestCaseBuilder create() {
			return new SwiftTestCaseBuilder(new SwiftTestCase());
		}


		public SwiftTestCaseBuilder with(SwiftExpectedMessage request, SwiftExpectedMessage response) {
			this.swiftTestCase.responseMap.add(new AbstractMap.SimpleEntry<>(request, response));
			return this;
		}


		public SwiftTestCase build() {
			return this.swiftTestCase;
		}
	}
}
