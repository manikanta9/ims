package com.clifton.swift.server.message;

/**
 * <code>SwiftTestMessageStatuses</code> the set of statuses that can be communicated between a Junit Test and the Fake Auto Client.
 */
public enum SwiftTestMessageStatuses {
	WAITING_FOR_REQUEST,
	INTERMEDIATE_RESULT,
	RECEIVED_TEST_CASE,
	STARTED_TEST_SESSION,
	ENDED_TEST_SESSION,
	FINISHED_TEST_SESSION,
	LAST_TEMPLATE_RESPONSE,
	FAILED
}
