package com.clifton.swift.server.message;

/**
 * <code>SwiftTestStatus</code> the set of allowable states for the Fake Auto Client during Junit execution.
 */
public enum SwiftTestStatus {
	NO_TEST_CASE,
	READY,
	FAILED,
	SUCCESS,
	FINISED
}
