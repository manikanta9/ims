package com.clifton.swift.server.runner.autoclient;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.server.message.SwiftTestStatus;

import java.util.UUID;


/**
 * <code>SwiftTestSession</code> tracks the progress of a single swift test instance.
 */
public class SwiftTestSession {

	private final String testIdentifier;
	private SwiftTestCase swiftTestCase;
	private SwiftTestStatus status;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestSession() {
		this.testIdentifier = UUID.randomUUID().toString();
		this.status = SwiftTestStatus.NO_TEST_CASE;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public String getTestIdentifier() {
		return this.testIdentifier;
	}


	public SwiftTestCase getSwiftTestCase() {
		return this.swiftTestCase;
	}


	public void setSwiftTestCase(SwiftTestCase swiftTestCase) {
		AssertUtils.assertNotNull(swiftTestCase, "Test case cannot be null.");
		this.swiftTestCase = swiftTestCase;
		this.status = SwiftTestStatus.READY;
	}


	public SwiftTestStatus getStatus() {
		return this.status;
	}


	public void setStatus(SwiftTestStatus status) {
		this.status = status;
	}
}
