package com.clifton.swift.server.message;

/**
 * <code>SwiftTestCommands</code> the set of commands the Fake Auto Client accepts
 */
public enum SwiftTestCommands {
	START_TEST_SESSION,
	END_TEST_SESSION,
	SEND_TEST_CASE
}
