package com.clifton.swift.server.message;

import com.clifton.swift.server.runner.autoclient.SwiftTestCase;


/**
 * <code>SwiftTestCaseMessage</code> a {@link SwiftTestMessageConduit} directed message for communicating the test request/response information from a Junit test to the
 * running Fake Auto Client.
 */
public class SwiftTestCaseMessage extends SwiftTestCommandMessage<SwiftTestCase> {

	private final SwiftTestCase swiftTestCase;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestCaseMessage(SwiftTestCase swiftTestCase) {
		super(SwiftTestCommands.SEND_TEST_CASE);
		this.swiftTestCase = swiftTestCase;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftTestCase getPayload() {
		return this.swiftTestCase;
	}
}
