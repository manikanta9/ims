package com.clifton.swift.server;

import com.clifton.core.logging.message.LogMessage;
import com.clifton.core.logging.message.LogMessageService;
import com.clifton.core.logging.message.search.LogMessageSearchForm;
import com.clifton.core.util.dataaccess.PagingArrayList;


/**
 * <code>SwiftTestServerLoggingService</code> a do nothing logging service.
 */
public class SwiftTestServerLoggingService implements LogMessageService {

	@Override
	public LogMessage getLogMessage(String identifier) {
		return new LogMessage();
	}


	@Override
	public PagingArrayList<LogMessage> getLogMessageList(LogMessageSearchForm searchForm) {
		return new PagingArrayList<>();
	}
}
