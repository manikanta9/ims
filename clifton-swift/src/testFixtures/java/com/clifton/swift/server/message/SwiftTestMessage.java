package com.clifton.swift.server.message;

/**
 * <code>SwiftTestMessage</code> a {@link SwiftTestMessageConduit} directed message for communicating between a Junit test and the Fake Auto Client.
 */
public interface SwiftTestMessage<P> {

	public P getPayload();
}
