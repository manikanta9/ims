package com.clifton.swift.server.message;

/**
 * <code>SwiftTestStatusMessage</code> Messages communicated via a shared queue between a Junit test and
 * the Fake Auto Client.
 */
public class SwiftTestStatusMessage implements SwiftTestMessage<String> {

	private SwiftTestMessageStatuses status;
	private String payload;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public SwiftTestStatusMessage(SwiftTestMessageStatuses status, String message) {
		this.status = status;
		this.payload = message;
	}


	@Override
	public String toString() {
		return "SwiftTestMessage{" +
				"status=" + this.status +
				", message='" + this.payload + '\'' +
				'}';
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPayload() {
		return this.payload;
	}


	public SwiftTestMessageStatuses getStatus() {
		return this.status;
	}
}
