package com.clifton.swift.field;

public class SwiftFieldModifierFilterBuilder {

	private SwiftFieldModifierFilter swiftFieldModifierFilter;


	private SwiftFieldModifierFilterBuilder(SwiftFieldModifierFilter swiftFieldModifierFilter) {
		this.swiftFieldModifierFilter = swiftFieldModifierFilter;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftFieldModifierFilterBuilder createModifierFilter() {
		SwiftFieldModifierFilter swiftFieldModifierFilter = new SwiftFieldModifierFilter();

		swiftFieldModifierFilter.setFieldModifier(SwiftFieldModifierBuilder.createModifier().getSwiftFieldModifier());
		swiftFieldModifierFilter.setFieldValue("Field Value");
		swiftFieldModifierFilter.setFieldTag("Field Tag");

		return new SwiftFieldModifierFilterBuilder(swiftFieldModifierFilter);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierFilterBuilder withId(int id) {
		getSwiftFieldModifierFilter().setId(id);
		return this;
	}


	public SwiftFieldModifierFilterBuilder withFieldTag(String fieldTag) {
		getSwiftFieldModifierFilter().setFieldTag(fieldTag);
		return this;
	}


	public SwiftFieldModifierFilterBuilder withFieldValue(String fieldValue) {
		getSwiftFieldModifierFilter().setFieldValue(fieldValue);
		return this;
	}


	public SwiftFieldModifierFilterBuilder withSwiftFieldModifier(SwiftFieldModifier swiftFieldModifier) {
		getSwiftFieldModifierFilter().setFieldModifier(swiftFieldModifier);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierFilter getSwiftFieldModifierFilter() {
		return this.swiftFieldModifierFilter;
	}
}
