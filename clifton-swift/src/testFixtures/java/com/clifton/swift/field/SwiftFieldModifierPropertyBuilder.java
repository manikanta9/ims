package com.clifton.swift.field;

public class SwiftFieldModifierPropertyBuilder {

	private SwiftFieldModifierProperty swiftFieldModifierProperty;


	private SwiftFieldModifierPropertyBuilder(SwiftFieldModifierProperty swiftFieldModifierProperty) {
		this.swiftFieldModifierProperty = swiftFieldModifierProperty;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftFieldModifierPropertyBuilder createModifierProperty() {

		SwiftFieldModifierProperty swiftFieldModifierProperty = new SwiftFieldModifierProperty();
		swiftFieldModifierProperty.setName("JUnit Property");
		swiftFieldModifierProperty.setLabel("Junit Property Label");
		swiftFieldModifierProperty.setDescription("Junit Property Description");
		swiftFieldModifierProperty.setOrder(1);
		swiftFieldModifierProperty.setRequired(true);
		swiftFieldModifierProperty.setPropertyValue("JUnit Property Value");
		swiftFieldModifierProperty.setFieldModifier(SwiftFieldModifierBuilder.createModifier().getSwiftFieldModifier());
		swiftFieldModifierProperty.setFieldModifierPropertyType(SwiftFieldModifierPropertyTypeBuilder.createModifierPropertyType().getSwiftFieldModifierPropertyType());

		return new SwiftFieldModifierPropertyBuilder(swiftFieldModifierProperty);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierPropertyBuilder withId(int id) {
		getSwiftFieldModifierProperty().setId(id);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withName(String name) {
		getSwiftFieldModifierProperty().setName(name);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withDescription(String description) {
		getSwiftFieldModifierProperty().setDescription(description);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withLabel(String label) {
		getSwiftFieldModifierProperty().setLabel(label);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withOrder(int order) {
		getSwiftFieldModifierProperty().setOrder(order);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withRequired(boolean required) {
		getSwiftFieldModifierProperty().setRequired(required);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withValue(String value) {
		getSwiftFieldModifierProperty().setPropertyValue(value);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withSwiftFieldModifier(SwiftFieldModifier swiftFieldModifier) {
		getSwiftFieldModifierProperty().setFieldModifier(swiftFieldModifier);
		return this;
	}


	public SwiftFieldModifierPropertyBuilder withSwiftFieldModifierPropertyType(SwiftFieldModifierPropertyType swiftFieldModifierPropertyType) {
		getSwiftFieldModifierProperty().setFieldModifierPropertyType(swiftFieldModifierPropertyType);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierProperty getSwiftFieldModifierProperty() {
		return this.swiftFieldModifierProperty;
	}
}
