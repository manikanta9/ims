package com.clifton.swift.field;

public class SwiftFieldModifierTypeBuilder {

	private SwiftFieldModifierType swiftFieldModifierType;


	private SwiftFieldModifierTypeBuilder(SwiftFieldModifierType swiftFieldModifierType) {
		this.swiftFieldModifierType = swiftFieldModifierType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftFieldModifierTypeBuilder createModifierType() {

		SwiftFieldModifierType swiftFieldModifierType = new SwiftFieldModifierType();

		swiftFieldModifierType.setName("JUnit Modifier Type");
		swiftFieldModifierType.setClassName("com.clifton.swift.field.SwiftFieldModifierBean");
		swiftFieldModifierType.setDescription("JUnit Modifier Type Description");
		swiftFieldModifierType.setLabel("JUnit Modifier");

		return new SwiftFieldModifierTypeBuilder(swiftFieldModifierType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierTypeBuilder withId(int id) {
		getSwiftFieldModifierType().setId(id);
		return this;
	}


	public SwiftFieldModifierTypeBuilder withName(String name) {
		getSwiftFieldModifierType().setName(name);
		return this;
	}


	public SwiftFieldModifierTypeBuilder withDescription(String description) {
		getSwiftFieldModifierType().setDescription(description);
		return this;
	}


	public SwiftFieldModifierTypeBuilder withLabel(String label) {
		getSwiftFieldModifierType().setLabel(label);
		return this;
	}


	public SwiftFieldModifierTypeBuilder withClassName(String className) {
		getSwiftFieldModifierType().setClassName(className);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierType getSwiftFieldModifierType() {
		return this.swiftFieldModifierType;
	}
}
