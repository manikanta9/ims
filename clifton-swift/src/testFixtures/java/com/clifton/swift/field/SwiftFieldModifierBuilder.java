package com.clifton.swift.field;

import java.util.List;


public class SwiftFieldModifierBuilder {

	private SwiftFieldModifier swiftFieldModifier;


	private SwiftFieldModifierBuilder(SwiftFieldModifier swiftFieldModifier) {
		this.swiftFieldModifier = swiftFieldModifier;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftFieldModifierBuilder createModifier() {

		SwiftFieldModifier swiftFieldModifier = new SwiftFieldModifier();
		swiftFieldModifier.setFieldModifierType(SwiftFieldModifierTypeBuilder.createModifierType().getSwiftFieldModifierType());
		return new SwiftFieldModifierBuilder(swiftFieldModifier);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierBuilder withId(int id) {
		getSwiftFieldModifier().setId(id);
		return this;
	}


	public SwiftFieldModifierBuilder withSwiftFieldModifierType(SwiftFieldModifierType swiftFieldModifierType) {
		getSwiftFieldModifier().setFieldModifierType(swiftFieldModifierType);
		return this;
	}


	public SwiftFieldModifierBuilder withSwiftFieldModifierFilterList(List<SwiftFieldModifierFilter> swiftFieldModifierFilterList) {
		getSwiftFieldModifier().setFilterList(swiftFieldModifierFilterList);
		return this;
	}


	public SwiftFieldModifierBuilder withSwiftFieldModifierProperty(List<SwiftFieldModifierProperty> swiftFieldModifierPropertyList) {
		getSwiftFieldModifier().setPropertyList(swiftFieldModifierPropertyList);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifier getSwiftFieldModifier() {
		return this.swiftFieldModifier;
	}
}
