package com.clifton.swift.field;

public class SwiftFieldModifierPropertyTypeBuilder {

	private SwiftFieldModifierPropertyType swiftFieldModifierPropertyType;


	private SwiftFieldModifierPropertyTypeBuilder(SwiftFieldModifierPropertyType swiftFieldModifierPropertyType) {
		this.swiftFieldModifierPropertyType = swiftFieldModifierPropertyType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftFieldModifierPropertyTypeBuilder createModifierPropertyType() {

		SwiftFieldModifierPropertyType swiftFieldModifierPropertyType = new SwiftFieldModifierPropertyType();
		swiftFieldModifierPropertyType.setName("JUnit Property Type");
		swiftFieldModifierPropertyType.setLabel("Junit Property Type Label");
		swiftFieldModifierPropertyType.setDescription("Junit Property Type Description");
		SwiftFieldModifierDataType swiftFieldModifierDataType = new SwiftFieldModifierDataType();
		swiftFieldModifierDataType.setDescription("A string or a set of characters.");
		swiftFieldModifierDataType.setName("STRING");
		swiftFieldModifierDataType.setLabel("String");
		swiftFieldModifierDataType.setNumeric(false);
		swiftFieldModifierPropertyType.setModifierDataType(swiftFieldModifierDataType);
		swiftFieldModifierPropertyType.setOrder(1);
		swiftFieldModifierPropertyType.setFieldModifierType(
				SwiftFieldModifierTypeBuilder.createModifierType().getSwiftFieldModifierType());
		swiftFieldModifierPropertyType.setRequired(true);

		return new SwiftFieldModifierPropertyTypeBuilder(swiftFieldModifierPropertyType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierPropertyTypeBuilder withId(int id) {
		getSwiftFieldModifierPropertyType().setId(id);
		return this;
	}


	public SwiftFieldModifierPropertyTypeBuilder withName(String name) {
		getSwiftFieldModifierPropertyType().setName(name);
		return this;
	}


	public SwiftFieldModifierPropertyTypeBuilder withDescription(String description) {
		getSwiftFieldModifierPropertyType().setDescription(description);
		return this;
	}


	public SwiftFieldModifierPropertyTypeBuilder withLabel(String label) {
		getSwiftFieldModifierPropertyType().setLabel(label);
		return this;
	}


	public SwiftFieldModifierPropertyTypeBuilder withOrder(int order) {
		getSwiftFieldModifierPropertyType().setOrder(order);
		return this;
	}


	public SwiftFieldModifierPropertyTypeBuilder withSystemDataType(SwiftFieldModifierDataType systemDataType) {
		getSwiftFieldModifierPropertyType().setModifierDataType(systemDataType);
		return this;
	}


	public SwiftFieldModifierPropertyTypeBuilder withSwiftFieldModifierType(SwiftFieldModifierType swiftFieldModifierType) {
		getSwiftFieldModifierPropertyType().setFieldModifierType(swiftFieldModifierType);
		return this;
	}


	public SwiftFieldModifierPropertyTypeBuilder withRequired(boolean required) {
		getSwiftFieldModifierPropertyType().setRequired(required);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierPropertyType getSwiftFieldModifierPropertyType() {
		return this.swiftFieldModifierPropertyType;
	}
}
