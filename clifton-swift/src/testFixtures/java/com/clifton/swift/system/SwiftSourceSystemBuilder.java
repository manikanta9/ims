package com.clifton.swift.system;

public class SwiftSourceSystemBuilder {

	private SwiftSourceSystem swiftSourceSystem;


	private SwiftSourceSystemBuilder(SwiftSourceSystem swiftSourceSystem) {
		this.swiftSourceSystem = swiftSourceSystem;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftSourceSystemBuilder createSwiftSourceSystemIMS() {
		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();

		swiftSourceSystem.setId((short) 1);
		swiftSourceSystem.setName("IMS");
		swiftSourceSystem.setDescription("The main IMS system");
		swiftSourceSystem.setBusinessIdentifierCode("PPTEST66");

		return new SwiftSourceSystemBuilder(swiftSourceSystem);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftSourceSystemBuilder withId(short id) {
		this.swiftSourceSystem.setId(id);
		return this;
	}


	public SwiftSourceSystemBuilder withName(String name) {
		this.swiftSourceSystem.setName(name);
		return this;
	}


	public SwiftSourceSystemBuilder withDescription(String description) {
		this.swiftSourceSystem.setDescription(description);
		return this;
	}


	public SwiftSourceSystemBuilder withBusinessIdentifierCode(String businessIdentifierCode) {
		this.swiftSourceSystem.setBusinessIdentifierCode(businessIdentifierCode);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftSourceSystem getSwiftSourceSystem() {
		return this.swiftSourceSystem;
	}
}
