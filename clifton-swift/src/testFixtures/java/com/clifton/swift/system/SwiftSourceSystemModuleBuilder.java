package com.clifton.swift.system;

import com.clifton.swift.message.SwiftMessageFormats;


public class SwiftSourceSystemModuleBuilder {

	private SwiftSourceSystemModule swiftSourceSystemModule;


	private SwiftSourceSystemModuleBuilder(SwiftSourceSystemModule swiftSourceSystemModule) {
		this.swiftSourceSystemModule = swiftSourceSystemModule;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftSourceSystemModuleBuilder createSwiftSourceSystemModuleIMS_MTM() {
		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();

		swiftSourceSystemModule.setId((short) 1);
		swiftSourceSystemModule.setName("MTM");
		swiftSourceSystemModule.setDescription("Mark to Market");
		swiftSourceSystemModule.setTargetMessageFormat(SwiftMessageFormats.MT);

		return new SwiftSourceSystemModuleBuilder(swiftSourceSystemModule);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftSourceSystemModuleBuilder withId(short id) {
		this.swiftSourceSystemModule.setId(id);
		return this;
	}


	public SwiftSourceSystemModuleBuilder withName(String name) {
		this.swiftSourceSystemModule.setName(name);
		return this;
	}


	public SwiftSourceSystemModuleBuilder withDescription(String description) {
		this.swiftSourceSystemModule.setDescription(description);
		return this;
	}


	public SwiftSourceSystemModuleBuilder withTargetMessageFormat(SwiftMessageFormats swiftMessageFormats) {
		this.swiftSourceSystemModule.setTargetMessageFormat(swiftMessageFormats);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftSourceSystemModule getSwiftSourceSystemModule() {
		return this.swiftSourceSystemModule;
	}
}
