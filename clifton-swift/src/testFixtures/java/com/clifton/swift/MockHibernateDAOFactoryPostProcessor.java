package com.clifton.swift;

import com.clifton.core.dataaccess.dao.hibernate.HibernateUpdatableDAO;
import com.clifton.core.dataaccess.migrate.reader.XmlMigrationHelper;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.util.CollectionUtils;
import org.mockito.Mockito;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.util.regex.Pattern;


public class MockHibernateDAOFactoryPostProcessor implements BeanFactoryPostProcessor {

	private static final String daoNamePattern = ".*";
	private static final String schemaFilePrefix = "META-INF/schema/schema-clifton-";
	private static final String schemaFileSuffix = ".xml";
	private static final String daoClassName = HibernateUpdatableDAO.class.getName();

	private String[] projectNameList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		XmlMigrationHelper xmlHelper = new XmlMigrationHelper();
		int projectCount = (getProjectNameList() == null) ? 0 : getProjectNameList().length;
		Pattern daoNames = Pattern.compile(daoNamePattern);
		ResourceLoader loader = new DefaultResourceLoader();
		for (int i = 0; i < projectCount; i++) {
			String fileName = schemaFilePrefix + getProjectNameList()[i] + schemaFileSuffix;
			Resource resource = loader.getResource(fileName);
			if (resource.exists()) {
				Schema schema = xmlHelper.loadFromXml(fileName);
				for (Table table : CollectionUtils.getIterable(schema.getTableList())) {
					if (daoNames.matcher(table.getDaoName()).matches()) {
						addHibernateDAO((DefaultListableBeanFactory) beanFactory, table);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void addHibernateDAO(DefaultListableBeanFactory beanFactory, Table table) {
		String beanName = table.getDaoName();
		if (!beanFactory.containsBean(beanName)) {
			beanFactory.registerBeanDefinition(beanName, BeanDefinitionBuilder
					.rootBeanDefinition(Mockito.class)
					.setFactoryMethod("mock")
					.addConstructorArgValue(daoClassName)
					.getBeanDefinition());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String[] getProjectNameList() {
		return this.projectNameList;
	}


	public void setProjectNameList(String[] projectNameList) {
		this.projectNameList = projectNameList;
	}
}
