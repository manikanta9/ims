package com.clifton.swift.client.instruction;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * <code>InstructionCancellationHandlerImpl</code> Swift-specific implementation generates cancellation Instruction messages for previously sent messages.
 * The cancellation messages are clones of the originals except for additional information necessary for the generator to recognize it as a cancellation and
 * format the outgoing Swift message accordingly.  In the case of Receive or Deliver against SWIFT messages, the Instruction only requires a change to the
 * function value and a message reference back to the original.
 */
@Component
public class InstructionCancellationHandlerImpl<M> implements InstructionCancellationHandler {

	private SwiftMessageService swiftClientMessageService;
	private SwiftMessageConverterHandler<M> swiftMessageConverterHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<InstructionMessage> generateCancellationMessageList(Instruction instruction) {
		List<InstructionMessage> result = new ArrayList<>();

		// get the original Swift messages that are being cancelled.
		List<SwiftMessage> swiftMessageList = getSwiftMessageList(instruction);

		// assume a zero amount cash instruction
		if (CollectionUtils.isEmpty(swiftMessageList)) {
			LogUtils.warn(getClass(), "Expected existing Swift Messages for Instruction Item " + instruction.getIdentity());
			return Collections.emptyList();
		}

		for (SwiftMessage targetSwiftMessage : swiftMessageList) {
			// parse the original Swift message into an AbstractMt message.
			List<M> parsedMessages = new ArrayList<>(getSwiftMessageConverterHandler().parse(targetSwiftMessage.getText(), SwiftMessageFormats.MT).values());
			M parsedMessage = CollectionUtils.getOnlyElement(parsedMessages);
			AssertUtils.assertNotNull(parsedMessage, "The SWIFT message could not be parsed " + targetSwiftMessage.getText());

			// reverse the original message back into an instruction message
			InstructionMessage clonedInstructionMessage = getSwiftMessageConverterHandler().toInstructionEntity(CollectionUtils.getFirstElementStrict(parsedMessages));

			// set the reference values necessary to cancel it.
			clonedInstructionMessage.setMessageReferenceNumber(targetSwiftMessage.getIdentifier().getId());
			clonedInstructionMessage.setTransactionReferenceNumber(targetSwiftMessage.getIdentifier().getUniqueStringIdentifier().concat("_CANCEL"));

			// set cancellation values specific to the message type before adding to the results
			result.add(clonedInstructionMessage.generateCancellationMessage());
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<SwiftMessage> getSwiftMessageList(Instruction instruction) {
		AssertUtils.assertNotNull(instruction, "Instruction is required.");

		SwiftMessageSearchForm swiftMessageSearchForm = new SwiftMessageSearchForm();
		swiftMessageSearchForm.setSwiftSourceSystemNameEquals(instruction.getSystemTable().getDataSource().getAlias());
		swiftMessageSearchForm.setSourceSystemFkFieldId(Long.parseLong(instruction.getIdentity().toString()));
		swiftMessageSearchForm.setSwiftSourceSystemModuleNameEquals("II");
		swiftMessageSearchForm.setIncoming(false);

		return getSwiftClientMessageService().getSwiftMessageList(swiftMessageSearchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageService getSwiftClientMessageService() {
		return this.swiftClientMessageService;
	}


	public void setSwiftClientMessageService(SwiftMessageService swiftClientMessageService) {
		this.swiftClientMessageService = swiftClientMessageService;
	}


	public SwiftMessageConverterHandler<M> getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler<M> swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}
}
