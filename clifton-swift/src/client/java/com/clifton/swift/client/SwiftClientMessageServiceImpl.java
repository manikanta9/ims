package com.clifton.swift.client;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.messaging.jms.synchronous.SynchronousMessageSender;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageActions;
import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.message.search.SwiftMessageSourceEntitySearchForm;
import com.clifton.swift.message.search.SwiftMessageStatusSearchForm;
import com.clifton.swift.message.search.SwiftMessageTypeSearchForm;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageActionRequestMessage;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageFieldRequestMessage;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageRequestMessage;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageStatusRequestMessage;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageTypeRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageActionResponseMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageFieldResponseMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageResponseMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageStatusResponseMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageTypeResponseMessage;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>SwiftClientMessageServiceImpl</code> client implementation or the SwiftMessageService.  This
 * implementation uses synchronous JMS messages to retrieve lists of message from the SWIFT server.
 * <p>
 * This is used on the IMS-side for web exposed service endpoints to the SWIFT server over JMS
 */
@Service
public class SwiftClientMessageServiceImpl<P> implements SwiftMessageService {

	private SynchronousMessageSender<SynchronousRequestMessage> swiftClientSynchronousMessageSender;

	private SystemSchemaService systemSchemaService;
	private InstructionUtilHandler<Instruction, P> instructionUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessage getSwiftMessage(long id) {
		SwiftMessageRequestMessage message = new SwiftMessageRequestMessage();
		message.setId(id);
		SwiftMessageResponseMessage response = (SwiftMessageResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return CollectionUtils.getOnlyElement(response.getMessageList());
		}
		return null;
	}


	@Override
	public List<SwiftMessageField> getSwiftMessageFieldList(long id) {
		SwiftMessageFieldRequestMessage message = new SwiftMessageFieldRequestMessage();
		message.setMessageId(id);
		SwiftMessageFieldResponseMessage response = (SwiftMessageFieldResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return response.getMessageFieldList();
		}
		return null;
	}


	@Override
	public List<SwiftMessage> getSwiftMessageList(SwiftMessageSearchForm searchForm) {
		SwiftMessageRequestMessage message = new SwiftMessageRequestMessage();
		BeanUtils.copyProperties(searchForm, message);
		SwiftMessageResponseMessage response = (SwiftMessageResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return response.getMessageList();
		}
		return null;
	}


	@Override
	public List<SwiftMessage> getSwiftMessageListBySourceEntity(SwiftMessageSourceEntitySearchForm searchForm) {
		AssertUtils.assertNotEmpty(searchForm.getSystemTableName(), "System Table cannot be empty.");
		AssertUtils.assertNotNull(searchForm.getSourceEntityId(), "Source Entity Identifier cannot be empty.");

		List<Instruction> instructionList = getInstructionUtilHandler().getInstructionList(searchForm.getSystemTableName(), searchForm.getSourceEntityId());

		if (!CollectionUtils.isEmpty(instructionList)) {
			Long[] instructionIdList = instructionList.stream().map(Instruction::getIdentity).map(Integer.class::cast).map(Long::valueOf).toArray(Long[]::new);

			SystemTable instructionTable = getSystemSchemaService().getSystemTableByName("InvestmentInstructionItem");
			AssertUtils.assertNotNull(instructionTable, "Instruction System Table could not be found.");
			AssertUtils.assertNotEmpty(instructionTable.getAlias(), "Instruction System Table Alias cannot be empty.");

			searchForm.setSwiftSourceSystemModuleNameEquals(instructionTable.getAlias());
			searchForm.setSourceSystemFkFieldIdList(instructionIdList);

			List<SwiftMessage> swiftMessages = getSwiftMessageList(searchForm);

			// try the old way.
			if (CollectionUtils.isEmpty(swiftMessages)) {
				SystemTable sourceEntityTable = getSystemSchemaService().getSystemTableByName(searchForm.getSystemTableName());
				AssertUtils.assertNotNull(instructionTable, "System Table could not be found " + searchForm.getSystemTableName());
				AssertUtils.assertNotEmpty(instructionTable.getAlias(), "System Table Alias cannot be empty " + searchForm.getSystemTableName());

				searchForm.setSourceSystemFkFieldIdList(new Long[]{Long.valueOf(searchForm.getSourceEntityId())});
				searchForm.setSwiftSourceSystemModuleNameEquals(sourceEntityTable.getAlias());
				swiftMessages = getSwiftMessageList(searchForm);
			}

			return swiftMessages;
		}
		return null;
	}


	@Override
	public SwiftMessage saveSwiftMessage(SwiftMessage swiftMessage) {
		throw new RuntimeException("[" + this.getClass().getName() + "] does not support saving [SwiftMessage] objects.");
	}


	@Override
	public void reprocessSwiftMessageList(long[] messageIdList) {
		SwiftMessageActionRequestMessage requestMessage = new SwiftMessageActionRequestMessage();
		requestMessage.setSwiftMessageIdList(messageIdList);
		requestMessage.setSwiftMessageAction(SwiftMessageActions.REPROCESS_SWIFT_MESSAGE);
		SwiftMessageActionResponseMessage response = (SwiftMessageActionResponseMessage) getSwiftClientSynchronousMessageSender().call(requestMessage);
		if (response.getError() != null) {
			throw new RuntimeException("Failed to reprocess SWIFT message entries.", response.getError());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageType getSwiftMessageType(short id) {
		SwiftMessageTypeRequestMessage message = new SwiftMessageTypeRequestMessage();
		message.setEntryId(id);
		SwiftMessageTypeResponseMessage response = (SwiftMessageTypeResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return CollectionUtils.getOnlyElement(response.getSwiftMessageTypeList());
		}
		return null;
	}


	@Override
	public SwiftMessageType getSwiftMessageTypeByName(String name) {
		SwiftMessageTypeRequestMessage message = new SwiftMessageTypeRequestMessage();
		message.setName(name);
		SwiftMessageTypeResponseMessage response = (SwiftMessageTypeResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return CollectionUtils.getOnlyElement(response.getSwiftMessageTypeList());
		}
		return null;
	}


	@Override
	public SwiftMessageType getSwiftMessageTypeVersionCode(String versionName) {
		SwiftMessageTypeRequestMessage message = new SwiftMessageTypeRequestMessage();
		if (versionName != null && versionName.startsWith(SwiftMessageFormats.MT.toString())) {
			message.setMtVersionName(versionName);
		}
		else if (versionName != null && versionName.toLowerCase().startsWith(SwiftMessageFormats.MX.toString().toLowerCase())) {
			message.setMxVersionName(versionName);
		}
		SwiftMessageTypeResponseMessage response = (SwiftMessageTypeResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return CollectionUtils.getOnlyElement(response.getSwiftMessageTypeList());
		}
		return null;
	}


	@Override
	public List<SwiftMessageType> getSwiftMessageTypeList(SwiftMessageTypeSearchForm searchForm) {
		SwiftMessageTypeRequestMessage message = new SwiftMessageTypeRequestMessage();
		BeanUtils.copyProperties(searchForm, message);
		SwiftMessageTypeResponseMessage response = (SwiftMessageTypeResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return response.getSwiftMessageTypeList();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageStatus getSwiftMessageStatus(short id) {
		SwiftMessageStatusRequestMessage message = new SwiftMessageStatusRequestMessage();
		message.setId(id);
		SwiftMessageStatusResponseMessage response = (SwiftMessageStatusResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return CollectionUtils.getOnlyElement(response.getSwiftMessageStatusList());
		}
		return null;
	}


	@Override
	public SwiftMessageStatus getSwiftMessageStatusByName(String name) {
		SwiftMessageStatusRequestMessage message = new SwiftMessageStatusRequestMessage();
		message.setName(name);
		SwiftMessageStatusResponseMessage response = (SwiftMessageStatusResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return CollectionUtils.getOnlyElement(response.getSwiftMessageStatusList());
		}
		return null;
	}


	@Override
	public List<SwiftMessageStatus> getSwiftMessageStatusList(SwiftMessageStatusSearchForm searchForm) {
		SwiftMessageStatusRequestMessage message = new SwiftMessageStatusRequestMessage();
		BeanUtils.copyProperties(searchForm, message);
		SwiftMessageStatusResponseMessage response = (SwiftMessageStatusResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return response.getSwiftMessageStatusList();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SynchronousMessageSender<SynchronousRequestMessage> getSwiftClientSynchronousMessageSender() {
		return this.swiftClientSynchronousMessageSender;
	}


	public void setSwiftClientSynchronousMessageSender(SynchronousMessageSender<SynchronousRequestMessage> swiftClientSynchronousMessageSender) {
		this.swiftClientSynchronousMessageSender = swiftClientSynchronousMessageSender;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public InstructionUtilHandler<Instruction, P> getInstructionUtilHandler() {
		return this.instructionUtilHandler;
	}


	public void setInstructionUtilHandler(InstructionUtilHandler<Instruction, P> instructionUtilHandler) {
		this.instructionUtilHandler = instructionUtilHandler;
	}
}
