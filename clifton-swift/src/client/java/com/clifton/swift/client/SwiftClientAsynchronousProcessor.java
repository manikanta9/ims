package com.clifton.swift.client;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.instruction.messaging.InstructionMessagingResponseHandler;
import com.clifton.instruction.messaging.message.InstructionMessagingStatusMessage;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;


/**
 * The <code>SwiftClientAsynchronousProcessor</code> processes incoming asynchronous JMS messages from the the Swift server.
 */
public class SwiftClientAsynchronousProcessor implements AsynchronousProcessor<AsynchronousMessage> {

	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;
	private ContextHandler contextHandler;
	private SecurityUserService securityUserService;
	private InstructionMessagingResponseHandler instructionMessagingResponseHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(AsynchronousMessage asynchronousMessage, AsynchronousMessageHandler handler) {
		if (asynchronousMessage != null) {
			// set the user for database
			setRunAsUser();

			if (asynchronousMessage instanceof InstructionMessagingStatusMessage) {
				getInstructionMessagingResponseHandler().process((InstructionMessagingStatusMessage) asynchronousMessage);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void setRunAsUser() {
		if ((getContextHandler() != null) && getContextHandler().getBean(Context.USER_BEAN_NAME) == null) {
			SecurityUser result = getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
			getContextHandler().setBean(Context.USER_BEAN_NAME, result);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public InstructionMessagingResponseHandler getInstructionMessagingResponseHandler() {
		return this.instructionMessagingResponseHandler;
	}


	public void setInstructionMessagingResponseHandler(InstructionMessagingResponseHandler instructionMessagingResponseHandler) {
		this.instructionMessagingResponseHandler = instructionMessagingResponseHandler;
	}
}
