package com.clifton.swift.client;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.messaging.jms.synchronous.SynchronousMessageSender;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.message.SwiftMessageActions;
import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.message.log.SwiftMessageLog;
import com.clifton.swift.message.log.SwiftMessageLogService;
import com.clifton.swift.message.log.search.SwiftMessageLogSearchForm;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageActionRequestMessage;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageLogFieldRequestMessage;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageLogRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageActionResponseMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageLogFieldResponseMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageLogResponseMessage;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>SwiftClientMessageLogServiceImpl</code> client implementation of the SwiftMessageLogService.  This
 * implementation uses synchronous JMS messages to retrieve lists of log messages from the SWIFT server.
 * <p>
 * This is used on the IMS-side for web exposed service endpoints to the SWIFT server over JMS
 */
@Service
public class SwiftClientMessageLogServiceImpl implements SwiftMessageLogService {

	private SynchronousMessageSender<SynchronousRequestMessage> swiftClientSynchronousMessageSender;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageLog getSwiftMessageLog(long id) {
		SwiftMessageLogRequestMessage message = new SwiftMessageLogRequestMessage();
		message.setId(id);
		SwiftMessageLogResponseMessage response = (SwiftMessageLogResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return CollectionUtils.getOnlyElement(response.getMessageLogList());
		}
		return null;
	}


	@Override
	public List<SwiftMessageLog> getSwiftMessageLogList(SwiftMessageLogSearchForm searchForm) {
		SwiftMessageLogRequestMessage message = new SwiftMessageLogRequestMessage();
		BeanUtils.copyProperties(searchForm, message);
		SwiftMessageLogResponseMessage response = (SwiftMessageLogResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return response.getMessageLogList();
		}
		return null;
	}


	@Override
	public List<SwiftMessageField> getSwiftMessageLogFieldList(long id) {
		SwiftMessageLogFieldRequestMessage message = new SwiftMessageLogFieldRequestMessage();
		message.setId(id);
		SwiftMessageLogFieldResponseMessage response = (SwiftMessageLogFieldResponseMessage) getSwiftClientSynchronousMessageSender().call(message);
		if (response != null) {
			return response.getMessageFieldList();
		}
		return null;
	}


	@Override
	public SwiftMessageLog saveSwiftMessageLog(SwiftMessageLog bean) {
		throw new RuntimeException("[" + this.getClass().getName() + "] does not support saving [SwiftMessage] objects.");
	}


	@Override
	public void deleteSwiftMessageLog(long id) {
		throw new RuntimeException("[" + this.getClass().getName() + "] does not support deleting [SwiftMessage] objects.");
	}


	@Override
	public void reprocessSwiftMessageList(long[] messageLogIdList) {
		SwiftMessageActionRequestMessage requestMessage = new SwiftMessageActionRequestMessage();
		requestMessage.setSwiftMessageIdList(messageLogIdList);
		requestMessage.setSwiftMessageAction(SwiftMessageActions.REPROCESS_SWIFT_LOG_MESSAGE);
		SwiftMessageActionResponseMessage response = (SwiftMessageActionResponseMessage) getSwiftClientSynchronousMessageSender().call(requestMessage);
		if (response.getError() != null) {
			throw new RuntimeException("Failed to reprocess SWIFT message log entries.", response.getError());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SynchronousMessageSender<SynchronousRequestMessage> getSwiftClientSynchronousMessageSender() {
		return this.swiftClientSynchronousMessageSender;
	}


	public void setSwiftClientSynchronousMessageSender(SynchronousMessageSender<SynchronousRequestMessage> swiftClientSynchronousMessageSender) {
		this.swiftClientSynchronousMessageSender = swiftClientSynchronousMessageSender;
	}
}
