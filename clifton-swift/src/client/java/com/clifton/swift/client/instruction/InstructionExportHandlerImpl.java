package com.clifton.swift.client.instruction;

import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.instruction.export.InstructionExportHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.util.SwiftUtilHandler;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * Implement the export handler for the instruction project to send the instruction as
 * a SWIFT message.
 *
 * @author mwacker
 */
@Component
@SuppressWarnings({"rawtypes"})
public class InstructionExportHandlerImpl implements InstructionExportHandler {

	private AsynchronousMessageHandler swiftClientAsynchronousMessageHandler;
	private SwiftMessageConverterHandler swiftMessageConverterHandler;
	private SystemSchemaUtilHandler systemSchemaUtilHandler;
	private SwiftUtilHandler swiftUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends InstructionMessage> void send(List<T> entityList) {
		List<SwiftMessagingMessage> messageList = new ArrayList<>();

		for (T entity : CollectionUtils.getIterable(entityList)) {
			String messageText = previewExportMessage(entity);

			SwiftMessagingMessage message = new SwiftMessagingMessage();
			message.setMessageText(messageText);
			message.setUniqueStringIdentifier(entity.getTransactionReferenceNumber());
			message.setMessageFormat(SwiftMessageFormats.MT);

			String transactionReferenceNumber = getItemEntityUniqueStringIdentifier(entity.getTransactionReferenceNumber());
			String primaryIdentifier = getItemEntityUniqueStringIdentifier(transactionReferenceNumber);
			String[] idParts = getSystemSchemaUtilHandler().getEntityUniqueIdComponents(primaryIdentifier);
			message.setSourceSystemName(idParts[0]);
			message.setSourceSystemModuleName(idParts[1]);
			message.setSourceSystemFkFieldId(Long.valueOf(idParts[2]));
			message.setMessageReferenceNumber(entity.getMessageReferenceNumber());
			messageList.add(message);
		}

		for (SwiftMessagingMessage message : CollectionUtils.getIterable(messageList)) {
			getSwiftClientAsynchronousMessageHandler().send(message);
		}
	}


	@Override
	public <T extends InstructionMessage> String previewExportMessage(List<T> entityList) {
		List<String> messageStringList = new ArrayList<>();
		for (T entity : CollectionUtils.getIterable(entityList)) {
			messageStringList.add(previewExportMessage(entity));
		}
		return StringUtils.join(messageStringList, "$");
	}


	@Override
	public <T extends InstructionMessage> String previewFormattedExportMessage(List<T> entityList) {
		List<String> messageStringList = new ArrayList<>();
		for (T entity : CollectionUtils.getIterable(entityList)) {
			messageStringList.add(previewFormattedExportMessage(entity));
		}
		return StringUtils.join(messageStringList, "\n");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Return the table identifier string with the format: data source alias (IMS) + entity ID + table alias (MTM).
	 * The uniqueStringIdentifier parameter can be a either a packed string in the format (IMS101T;IMS202TF;IMS303AT)
	 * or a single table reference (IMS231MTM).  Returns the first component of the packed string (Trade), otherwise,
	 * returns the string as is.
	 */
	private String getItemEntityUniqueStringIdentifier(String uniqueStringIdentifier) {
		AssertUtils.assertNotEmpty(uniqueStringIdentifier, "Expected a SWIFT message unique string identifier.");
		List<String> componentParts = CollectionUtils.createList(uniqueStringIdentifier.split(InstructionGenerator.REFERENCE_COMPONENT_DELIMITER));
		return CollectionUtils.getFirstElement(componentParts);
	}


	private <T extends InstructionMessage> String previewExportMessage(T entity) {
		return getSwiftMessageConverterHandler().toSwiftMessage(entity, SwiftMessageFormats.MT);
	}


	private <T extends InstructionMessage> String previewFormattedExportMessage(T entity) {
		String messageText = getSwiftMessageConverterHandler().toSwiftMessage(entity, SwiftMessageFormats.MT);
		return getSwiftUtilHandler().getFormattedMessageText(messageText, SwiftMessageFormats.MT);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AsynchronousMessageHandler getSwiftClientAsynchronousMessageHandler() {
		return this.swiftClientAsynchronousMessageHandler;
	}


	public void setSwiftClientAsynchronousMessageHandler(AsynchronousMessageHandler swiftClientAsynchronousMessageHandler) {
		this.swiftClientAsynchronousMessageHandler = swiftClientAsynchronousMessageHandler;
	}


	public SystemSchemaUtilHandler getSystemSchemaUtilHandler() {
		return this.systemSchemaUtilHandler;
	}


	public void setSystemSchemaUtilHandler(SystemSchemaUtilHandler systemSchemaUtilHandler) {
		this.systemSchemaUtilHandler = systemSchemaUtilHandler;
	}


	public SwiftMessageConverterHandler getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}


	public SwiftUtilHandler getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}
}
