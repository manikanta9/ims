package com.prowidesoftware.swift.model.mt.mt0xx;

import com.prowidesoftware.swift.model.field.Field102;
import com.prowidesoftware.swift.model.field.Field103;
import com.prowidesoftware.swift.model.field.Field106;
import com.prowidesoftware.swift.model.field.Field107;
import com.prowidesoftware.swift.model.field.Field108;
import com.prowidesoftware.swift.model.field.Field171;
import com.prowidesoftware.swift.model.field.Field175;
import com.prowidesoftware.swift.model.field.Field202;
import com.prowidesoftware.swift.model.field.Field203;
import com.prowidesoftware.swift.model.field.Field206;
import com.prowidesoftware.swift.model.field.Field301;
import com.prowidesoftware.swift.model.field.Field302;
import com.prowidesoftware.swift.model.field.Field303;
import com.prowidesoftware.swift.model.field.Field305;
import com.prowidesoftware.swift.model.field.Field331;
import com.prowidesoftware.swift.model.field.Field332;
import com.prowidesoftware.swift.model.field.Field335;
import com.prowidesoftware.swift.model.field.Field339;
import com.prowidesoftware.swift.model.field.Field341;
import com.prowidesoftware.swift.model.field.Field344;
import com.prowidesoftware.swift.model.field.Field345;
import com.prowidesoftware.swift.model.field.Field346;
import com.prowidesoftware.swift.model.field.Field347;
import com.prowidesoftware.swift.model.field.Field348;
import com.prowidesoftware.swift.model.field.Field349;
import com.prowidesoftware.swift.model.field.Field431;
import com.prowidesoftware.swift.model.field.Field432;
import com.prowidesoftware.swift.model.field.Field461;
import com.prowidesoftware.swift.model.field.Field619;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class MT0XXTest {

	private static final String MSG_46 = "{1:F01VNDZBET2AXXX0015000905}{2:I046SWFTXXXXXXXXS}{4:{303:A}{301:RT}}{5:{CHK:B2190DEBB2D5}}";
	private static final String MSG_47 = "{1:A01VNDZBET2XXX0841000001}{2:I047SWFTXXXXXXXX}{4:{206:Y}{348:L}{339:FLD13C}{344:N}{347:13C}{344:U}{347:13C}{339:BBB9XX}{349:1}{344:N9}{346:BBB}{344:U9}{346:BBB}{339:BBBALL}{344:N}{346:BBB}{344:U}{346:BBB}{339:FINCPY}{344:N}{345:ABCXYZ}{344:U}{345:ABCXYZ}{339:INVFND}{339:INVFND}{345:502509515}{344:U}{345:502509515}{339:ANSWER}{344:N}{345:196296396496596696796896996}{344:U}{345:196296396496596696796896996}{339:PAYXFR}{344:N12}{344:U12}{339:SYSTEM}{344:S}{339:OTHERS}{344:U}{344:N}}{5:{CHK:5DE1754F7DF0}}";
	private static final String MSG_48 = "{1:F01VNDZBET2AXXX0015000906}{2:I048SWFTXXXXXXXXS}{5:{CHK:4454D4405050}}";
	private static final String MSG_82 = "{1:F01PPSCUS60AXXX0010000074}{2:O0821602161122EANEXXXXAXXX00022708931611220802S}{4:{202:0001}{203:0001}{171:161122}{175:1600}{301:RT}{335:1522010605VNDZBET2AXXX0018000377999BANKBEBBXXXX}{108:TEST5}{431:007}{103:COP}{461:001}}{5:{CHK:3902211721A5}{SYS:}{TNG:}}{S:{COP:P}}";
	private static final String MSG_66 = "{1:F01VNDZBET2AXXX0015001488}{2:O0661343010605DYLRXXXXAXXX00000124790106051443S}{4:{202:0001}{203:0001}{171:010605}{175:1342}{301:RT}{335:1522010605VNDZBET2AXXX0018000377999BANKBEBBXXXX}{108:TEST5}{431:007}{103:COP}{461:001}}{5:{CHK:5FA7E517C515}{SYS:1442010605VNDZBET2AXXX0015000905}}";
	private static final String MSG_68 = "{1:F01VNDZBET2AXXX0015001489}{2:O0681343010605DYDYXXXXGXXX00000003400106051443S}{4:{302:N}{341:08}{301:RT}}{5:{CHK:B363C3B73833}{SYS:1443010605VNDZBET2AXXX0015000906}}";
	private static final String MSG_81 = "{1:F01VNDZBET2BXXX0002000009}{2:O0812308010605DYDYXXXXGXXX00000130090106061606S}{4:{202:0001}{203:0001}{305:A}{331:001601060513470106051437A61000000000002000374000374000242000243}{331:001701060514540106051509000000002000002000375000376000244000245}{331:001801060515110106051539001000002000001000377000378000246000246}{332:000004000005}{305:B}{331:000101060515040106051539001000003000007000001000003000001000007}{332:000003000007}{305:C}{332:000000000000}}{5:{CHK:8D0719A6F103}{SYS:}{DLM:}}";
	private static final String MSG_44 = "{1:F01VNDZBET2AXXX0015000903}{2:I044SWFTXXXXXXXXS}{4:{302:N}{341:08}{301:RT}}{5:{CHK:2FA2C6A5A931}}";
	private static final String MSG_64 = "{1:F01VNDZBET2AXXX0015001491}{2:O0641347010605ABLRXXXXGXXX00000003420106051447S}{4:{302:N}{341:08}{301:RT}}{5:{CHK:B363C3B73833}{SYS:1441010605VNDZBET2AXXX0015000903}}";
	private static final String MSG_19 = "{1:F01VNDZBET2AXXX0117002343}{2:O0191409010605DYLRXXXXCXXX00000030020106051509S}{4:{175:0604}{106:140901VNDZBET2AXXX0021000443}{108:TESTINGID}{102:BBBNBEBBAXXX}{107:THISIS107}{432:12}{619:CPY}}{5:{CHK:08215D74A5E8}{SYS:4344360605VNDZBET2AXXX0015000879}}";


	@Test
	public void message46() throws IOException {
		// MESSAGE 46
		AbstractMT abstractMT = AbstractMT.parse(MSG_46);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT046);
		MT046 mt046 = (MT046) abstractMT;
		String parsedText = mt046.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_46));
		Field301 field301 = mt046.getField301();
		Assertions.assertNotNull(field301);
		MatcherAssert.assertThat("RT", IsEqual.equalTo(field301.getValue()));
		MatcherAssert.assertThat("301", IsEqual.equalTo(field301.getName()));
		Field303 field303 = mt046.getField303();
		Assertions.assertNotNull(field303);
		MatcherAssert.assertThat("A", IsEqual.equalTo(field303.getValue()));
		MatcherAssert.assertThat("303", IsEqual.equalTo(field303.getName()));
		// should illicit an MT066 Solicited Undelivered Message Report
	}


	@Test
	public void message47() throws IOException {
		// MESSAGE 47
		AbstractMT abstractMT = AbstractMT.parse(MSG_47);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT047);
		MT047 mt047 = (MT047) abstractMT;
		String parsedText = mt047.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_47));
		// 206
		Field206 field206 = mt047.getField206();
		Assertions.assertNotNull(field206);
		MatcherAssert.assertThat("Y", IsEqual.equalTo(field206.getValue()));
		MatcherAssert.assertThat("206", IsEqual.equalTo(field206.getName()));
		// 348
		Field348 field348 = mt047.getField348();
		Assertions.assertNotNull(field348);
		MatcherAssert.assertThat("L", IsEqual.equalTo(field348.getValue()));
		MatcherAssert.assertThat("348", IsEqual.equalTo(field348.getName()));
		// REPEATS
		// 339
		List<String> field339Expected = Arrays.asList("FLD13C", "BBB9XX", "BBBALL", "FINCPY", "INVFND", "INVFND", "ANSWER", "PAYXFR", "SYSTEM", "OTHERS");
		List<Field339> field339List = mt047.getField339();
		Assertions.assertNotNull(field339List);
		Assertions.assertEquals(10, field339List.size());
		MatcherAssert.assertThat("339", IsEqual.equalTo(field339List.get(0).getName()));
		List<String> field339Received = field339List.stream().map(Field339::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field339Expected.size(), field339Received.size());
		Assertions.assertArrayEquals(field339Expected.toArray(), field339Received.toArray(), "Field339 values should equal");
		// 349
		List<Field349> field349List = mt047.getField349();
		Assertions.assertNotNull(field349List);
		Assertions.assertEquals(1, field349List.size());
		MatcherAssert.assertThat("1", IsEqual.equalTo(field349List.get(0).getValue()));
		MatcherAssert.assertThat("349", IsEqual.equalTo(field349List.get(0).getName()));
		// 344
		List<String> field344Expected = Arrays.asList("N", "U", "N9", "U9", "N", "U", "N", "U", "U", "N", "U", "N12", "U12", "S", "U", "N");
		List<Field344> field344List = mt047.getField344();
		Assertions.assertNotNull(field344List);
		Assertions.assertEquals(16, field344List.size());
		MatcherAssert.assertThat("344", IsEqual.equalTo(field344List.get(0).getName()));
		List<String> field344Received = field344List.stream().map(Field344::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field344Expected.size(), field344Received.size());
		Assertions.assertArrayEquals(field344Expected.toArray(), field344Received.toArray(), "Field344 values should equal");
		// 345
		List<String> field345Expected = Arrays.asList("ABCXYZ", "ABCXYZ", "502509515", "502509515", "196296396496596696796896996", "196296396496596696796896996");
		List<Field345> field345List = mt047.getField345();
		Assertions.assertNotNull(field345List);
		Assertions.assertEquals(6, field345List.size());
		MatcherAssert.assertThat("345", IsEqual.equalTo(field345List.get(0).getName()));
		List<String> field345Received = field345List.stream().map(Field345::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field345Expected.size(), field345Received.size());
		Assertions.assertArrayEquals(field345Expected.toArray(), field345Received.toArray(), "Field345 values should equal");
		// 346
		List<String> field346Expected = Arrays.asList("BBB", "BBB", "BBB", "BBB");
		List<Field346> field346List = mt047.getField346();
		Assertions.assertNotNull(field346List);
		Assertions.assertEquals(4, field346List.size());
		MatcherAssert.assertThat("346", IsEqual.equalTo(field346List.get(0).getName()));
		List<String> field346Received = field346List.stream().map(Field346::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field346Expected.size(), field346Received.size());
		Assertions.assertArrayEquals(field346Expected.toArray(), field346Received.toArray(), "Field346 values should equal");
		// 347
		List<String> field347Expected = Arrays.asList("13C", "13C");
		List<Field347> field347List = mt047.getField347();
		Assertions.assertNotNull(field347List);
		Assertions.assertEquals(2, field347List.size());
		MatcherAssert.assertThat("347", IsEqual.equalTo(field347List.get(0).getName()));
		List<String> field347Received = field347List.stream().map(Field347::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field347Expected.size(), field347Received.size());
		Assertions.assertArrayEquals(field347Expected.toArray(), field347Received.toArray(), "Field347 values should equal");
	}


	@Test
	public void message48() throws IOException {
		// MESSAGE 48
		AbstractMT abstractMT = AbstractMT.parse(MSG_48);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT048);
		MT048 mt048 = (MT048) abstractMT;
		String parsedText = mt048.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_48));
	}


	@Test
	public void message82() throws IOException {
		// MESSAGE 82
		AbstractMT abstractMT = AbstractMT.parse(MSG_82);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT082);
		MT082 mt082 = (MT082) abstractMT;
		String parsedText = mt082.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_82));
		// 202
		Field202 field202 = mt082.getField202();
		Assertions.assertNotNull(field202);
		MatcherAssert.assertThat("0001", IsEqual.equalTo(field202.getValue()));
		MatcherAssert.assertThat("202", IsEqual.equalTo(field202.getName()));
		// 203
		Field203 field203 = mt082.getField203();
		Assertions.assertNotNull(field203);
		MatcherAssert.assertThat("0001", IsEqual.equalTo(field203.getValue()));
		MatcherAssert.assertThat("203", IsEqual.equalTo(field203.getName()));
		// 171
		Field171 field171 = mt082.getField171();
		Assertions.assertNotNull(field171);
		MatcherAssert.assertThat("161122", IsEqual.equalTo(field171.getValue()));
		MatcherAssert.assertThat("171", IsEqual.equalTo(field171.getName()));
		// 175
		Field175 field175 = mt082.getField175();
		Assertions.assertNotNull(field175);
		MatcherAssert.assertThat("1600", IsEqual.equalTo(field175.getValue()));
		MatcherAssert.assertThat("175", IsEqual.equalTo(field175.getName()));
		// 301
		Field301 field301 = mt082.getField301();
		Assertions.assertNotNull(field301);
		MatcherAssert.assertThat("RT", IsEqual.equalTo(field301.getValue()));
		MatcherAssert.assertThat("301", IsEqual.equalTo(field301.getName()));
		// Group 1.1 - This group may be repeated up to 95 times.
		Field335 field335 = mt082.getField335();
		Assertions.assertNotNull(field335);
		MatcherAssert.assertThat("1522010605VNDZBET2AXXX0018000377999BANKBEBBXXXX", IsEqual.equalTo(field335.getValue()));
		Field108 field108 = mt082.getField108();
		Assertions.assertNotNull(field108);
		MatcherAssert.assertThat("TEST5", IsEqual.equalTo(field108.getValue()));
		// Group 1.2
		Field431 field431 = mt082.getField431();
		Assertions.assertNotNull(field431);
		MatcherAssert.assertThat("007", IsEqual.equalTo(field431.getValue()));
		Field103 field103 = mt082.getField103();
		Assertions.assertNotNull(field103);
		MatcherAssert.assertThat("COP", IsEqual.equalTo(field103.getValue()));
		// Group 2
		// 461
		Field461 field461 = mt082.getField461();
		Assertions.assertNotNull(field461);
		MatcherAssert.assertThat("001", IsEqual.equalTo(field461.getValue()));
		MatcherAssert.assertThat("461", IsEqual.equalTo(field461.getName()));
	}


	@Test
	public void message66() throws IOException {
		// MESSAGE 66
		AbstractMT abstractMT = AbstractMT.parse(MSG_66);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT066);
		MT066 mt066 = (MT066) abstractMT;
		String parsedText = mt066.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_66));
		// 202
		Field202 field202 = mt066.getField202();
		Assertions.assertNotNull(field202);
		MatcherAssert.assertThat("0001", IsEqual.equalTo(field202.getValue()));
		MatcherAssert.assertThat("202", IsEqual.equalTo(field202.getName()));
		// 203
		Field203 field203 = mt066.getField203();
		Assertions.assertNotNull(field203);
		MatcherAssert.assertThat("0001", IsEqual.equalTo(field203.getValue()));
		MatcherAssert.assertThat("203", IsEqual.equalTo(field203.getName()));
		// 171
		Field171 field171 = mt066.getField171();
		Assertions.assertNotNull(field171);
		MatcherAssert.assertThat("010605", IsEqual.equalTo(field171.getValue()));
		MatcherAssert.assertThat("171", IsEqual.equalTo(field171.getName()));
		// 175
		Field175 field175 = mt066.getField175();
		Assertions.assertNotNull(field175);
		MatcherAssert.assertThat("1342", IsEqual.equalTo(field175.getValue()));
		MatcherAssert.assertThat("175", IsEqual.equalTo(field175.getName()));
		// 301
		Field301 field301 = mt066.getField301();
		Assertions.assertNotNull(field301);
		MatcherAssert.assertThat("RT", IsEqual.equalTo(field301.getValue()));
		MatcherAssert.assertThat("301", IsEqual.equalTo(field301.getName()));
		// Group 1.1 - This group may be repeated up to 95 times.
		Field335 field335 = mt066.getField335();
		Assertions.assertNotNull(field335);
		MatcherAssert.assertThat("1522010605VNDZBET2AXXX0018000377999BANKBEBBXXXX", IsEqual.equalTo(field335.getValue()));
		Field108 field108 = mt066.getField108();
		Assertions.assertNotNull(field108);
		MatcherAssert.assertThat("TEST5", IsEqual.equalTo(field108.getValue()));
		// Group 1.2
		Field431 field431 = mt066.getField431();
		Assertions.assertNotNull(field431);
		MatcherAssert.assertThat("007", IsEqual.equalTo(field431.getValue()));
		Field103 field103 = mt066.getField103();
		Assertions.assertNotNull(field103);
		MatcherAssert.assertThat("COP", IsEqual.equalTo(field103.getValue()));
		// Group 2
		// 461
		Field461 field461 = mt066.getField461();
		Assertions.assertNotNull(field461);
		MatcherAssert.assertThat("001", IsEqual.equalTo(field461.getValue()));
		MatcherAssert.assertThat("461", IsEqual.equalTo(field461.getName()));
	}


	@Test
	public void message68() throws IOException {
		// MESSAGE 68
		AbstractMT abstractMT = AbstractMT.parse(MSG_68);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT068);
		MT068 mt068 = (MT068) abstractMT;
		String parsedText = mt068.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_68));
		// 302
		Field302 field302 = mt068.getField302();
		Assertions.assertNotNull(field302);
		MatcherAssert.assertThat("N", IsEqual.equalTo(field302.getValue()));
		MatcherAssert.assertThat("302", IsEqual.equalTo(field302.getName()));
		// 341
		Field341 field341 = mt068.getField341();
		Assertions.assertNotNull(field341);
		MatcherAssert.assertThat("08", IsEqual.equalTo(field341.getValue()));
		MatcherAssert.assertThat("341", IsEqual.equalTo(field341.getName()));
		// 301
		Field301 field301 = mt068.getField301();
		Assertions.assertNotNull(field301);
		MatcherAssert.assertThat("RT", IsEqual.equalTo(field301.getValue()));
		MatcherAssert.assertThat("301", IsEqual.equalTo(field301.getName()));
	}


	@Test
	public void message81() throws IOException {
		// MESSAGE 81
		AbstractMT abstractMT = AbstractMT.parse(MSG_81);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT081);
		MT081 mt081 = (MT081) abstractMT;
		String parsedText = mt081.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_81));
		// 202
		Field202 field202 = mt081.getField202();
		Assertions.assertNotNull(field202);
		MatcherAssert.assertThat("0001", IsEqual.equalTo(field202.getValue()));
		MatcherAssert.assertThat("202", IsEqual.equalTo(field202.getName()));
		// 203
		Field203 field203 = mt081.getField203();
		Assertions.assertNotNull(field203);
		MatcherAssert.assertThat("0001", IsEqual.equalTo(field203.getValue()));
		MatcherAssert.assertThat("203", IsEqual.equalTo(field203.getName()));
		// 305
		List<String> field305Expected = Arrays.asList("A", "B", "C");
		List<Field305> field305List = mt081.getField305();
		Assertions.assertNotNull(field305List);
		Assertions.assertEquals(3, field305List.size());
		MatcherAssert.assertThat("305", IsEqual.equalTo(field305List.get(0).getName()));
		List<String> field305Received = field305List.stream().map(Field305::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field305Expected.size(), field305Received.size());
		Assertions.assertArrayEquals(field305Expected.toArray(), field305Received.toArray(), "Field305 values should equal");
		// 331
		List<String> field331Expected = Arrays.asList(
				"001601060513470106051437A61000000000002000374000374000242000243",
				"001701060514540106051509000000002000002000375000376000244000245",
				"001801060515110106051539001000002000001000377000378000246000246",
				"000101060515040106051539001000003000007000001000003000001000007");
		List<Field331> field331List = mt081.getField331();
		Assertions.assertNotNull(field331List);
		Assertions.assertEquals(4, field331List.size());
		MatcherAssert.assertThat("331", IsEqual.equalTo(field331List.get(0).getName()));
		List<String> field331Received = field331List.stream().map(Field331::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field331Expected.size(), field331Received.size());
		Assertions.assertArrayEquals(field331Expected.toArray(), field331Received.toArray(), "Field331 values should equal");
		// 332
		List<String> field332Expected = Arrays.asList(
				"000004000005",
				"000003000007",
				"000000000000");
		List<Field332> field332List = mt081.getField332();
		Assertions.assertNotNull(field332List);
		Assertions.assertEquals(3, field332List.size());
		MatcherAssert.assertThat("332", IsEqual.equalTo(field332List.get(0).getName()));
		List<String> field332Received = field332List.stream().map(Field332::getValue).collect(Collectors.toList());
		Assertions.assertEquals(field332Expected.size(), field332Received.size());
		Assertions.assertArrayEquals(field332Expected.toArray(), field332Received.toArray(), "Field332 values should equal");
	}


	@Test
	public void message44() throws IOException {
		// MESSAGE 44
		AbstractMT abstractMT = AbstractMT.parse(MSG_44);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT044);
		MT044 mt044 = (MT044) abstractMT;
		String parsedText = mt044.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_44));
		// 302
		Field302 field302 = mt044.getField302();
		Assertions.assertNotNull(field302);
		MatcherAssert.assertThat("N", IsEqual.equalTo(field302.getValue()));
		MatcherAssert.assertThat("302", IsEqual.equalTo(field302.getName()));
		// 341
		Field341 field341 = mt044.getField341();
		Assertions.assertNotNull(field341);
		MatcherAssert.assertThat("08", IsEqual.equalTo(field341.getValue()));
		MatcherAssert.assertThat("341", IsEqual.equalTo(field341.getName()));
		// 301
		Field301 field301 = mt044.getField301();
		Assertions.assertNotNull(field301);
		MatcherAssert.assertThat("RT", IsEqual.equalTo(field301.getValue()));
		MatcherAssert.assertThat("301", IsEqual.equalTo(field301.getName()));
	}


	@Test
	public void message64() throws IOException {
		// MESSAGE 64
		AbstractMT abstractMT = AbstractMT.parse(MSG_64);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT064);
		MT064 mt064 = (MT064) abstractMT;
		String parsedText = mt064.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_64));
		// 302
		Field302 field302 = mt064.getField302();
		Assertions.assertNotNull(field302);
		MatcherAssert.assertThat("N", IsEqual.equalTo(field302.getValue()));
		MatcherAssert.assertThat("302", IsEqual.equalTo(field302.getName()));
		// 341
		Field341 field341 = mt064.getField341();
		Assertions.assertNotNull(field341);
		MatcherAssert.assertThat("08", IsEqual.equalTo(field341.getValue()));
		MatcherAssert.assertThat("341", IsEqual.equalTo(field341.getName()));
		// 301
		Field301 field301 = mt064.getField301();
		Assertions.assertNotNull(field301);
		MatcherAssert.assertThat("RT", IsEqual.equalTo(field301.getValue()));
		MatcherAssert.assertThat("301", IsEqual.equalTo(field301.getName()));
	}


	@Test
	public void message19() throws IOException {
		// MESSAGE 19
		AbstractMT abstractMT = AbstractMT.parse(MSG_19);
		Assertions.assertNotNull(abstractMT);
		Assertions.assertTrue(abstractMT instanceof MT019);
		MT019 mt019 = (MT019) abstractMT;
		String parsedText = mt019.message();
		MatcherAssert.assertThat(parsedText, IsEqual.equalTo(MSG_19));
		Field175 field175 = mt019.getField175();
		Assertions.assertNotNull(field175);
		MatcherAssert.assertThat("0604", IsEqual.equalTo(field175.getValue()));
		MatcherAssert.assertThat("175", IsEqual.equalTo(field175.getName()));
		Field106 field106 = mt019.getField106();
		Assertions.assertNotNull(field106);
		MatcherAssert.assertThat("140901VNDZBET2AXXX0021000443", IsEqual.equalTo(field106.getValue()));
		MatcherAssert.assertThat("106", IsEqual.equalTo(field106.getName()));
		Field108 field108 = mt019.getField108();
		Assertions.assertNotNull(field108);
		MatcherAssert.assertThat("TESTINGID", IsEqual.equalTo(field108.getValue()));
		MatcherAssert.assertThat("108", IsEqual.equalTo(field108.getName()));
		Field102 field102 = mt019.getField102();
		Assertions.assertNotNull(field102);
		MatcherAssert.assertThat("BBBNBEBBAXXX", IsEqual.equalTo(field102.getValue()));
		MatcherAssert.assertThat("102", IsEqual.equalTo(field102.getName()));
		Field107 field107 = mt019.getField107();
		Assertions.assertNotNull(field107);
		MatcherAssert.assertThat("THISIS107", IsEqual.equalTo(field107.getValue()));
		MatcherAssert.assertThat("107", IsEqual.equalTo(field107.getName()));
		Field432 field432 = mt019.getField432();
		Assertions.assertNotNull(field432);
		MatcherAssert.assertThat("12", IsEqual.equalTo(field432.getValue()));
		MatcherAssert.assertThat("432", IsEqual.equalTo(field432.getName()));
		Field619 field619 = mt019.getField619();
		Assertions.assertNotNull(field619);
		MatcherAssert.assertThat("CPY", IsEqual.equalTo(field619.getValue()));
		MatcherAssert.assertThat("619", IsEqual.equalTo(field619.getName()));
	}
}
