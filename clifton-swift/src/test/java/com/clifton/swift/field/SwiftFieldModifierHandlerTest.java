package com.clifton.swift.field;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.util.SwiftUtilHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.field.Field21;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftFieldModifierHandlerTest {

	private static final String TEST_INSTRUCTION_MESSAGE = "{1:F01FOOSEDR0AXXX0000000000}{2:I202FOORECV0XXXXN}{4:\n" +
			":20:IMS:M2M:56872\n" +
			":21:MARG\n" +
			":32A:161026EUR10120,\n" +
			":53B:/12345678901234567890\n" +
			":57A:BANKANC0XXX\n" +
			":58A:/12345678901234567XXX\n" +
			"BANKANC0XXX\n}";

	@Resource
	public SwiftFieldModifierHandler<AbstractMessage> swiftFieldModifierHandler;

	@Resource
	private SwiftUtilHandler<AbstractMessage> swiftUtilHandler;

	@Resource
	private AdvancedUpdatableDAO<SwiftFieldModifier, Criteria> swiftFieldModifierDAO;

	@Resource
	private ReadOnlyDAO<SwiftFieldModifierFilter> swiftFieldModifierFilterDAO;


	@SuppressWarnings("unchecked")
	@Test
	public void testApplyFieldModifiers() {
		Assertions.assertNotNull(this.swiftFieldModifierHandler);
		Assertions.assertNotNull(this.swiftUtilHandler);

		SwiftFieldModifierType swiftFieldModifierType = SwiftFieldModifierTypeBuilder
				.createModifierType()
				.withClassName(MockSwiftFieldModifierProcessor.class.getName())
				.getSwiftFieldModifierType();
		SwiftFieldModifier swiftFieldModifier = SwiftFieldModifierBuilder
				.createModifier()
				.withId(1)
				.withSwiftFieldModifierType(swiftFieldModifierType)
				.withSwiftFieldModifierProperty(Collections.singletonList(SwiftFieldModifierPropertyBuilder
						.createModifierProperty()
						.withName("21")
						.withValue("CHANGE")
						.getSwiftFieldModifierProperty()))
				.getSwiftFieldModifier();
		Mockito.when(this.swiftFieldModifierDAO.findBySearchCriteria(Mockito.any(SearchConfigurer.class))).thenReturn(Collections.singletonList(swiftFieldModifier));
		List<SwiftFieldModifierFilter> filters = Collections.singletonList(
				SwiftFieldModifierFilterBuilder
						.createModifierFilter()
						.withFieldTag("21")
						.withFieldValue("MARG")
						.getSwiftFieldModifierFilter()
		);
		Mockito.when(this.swiftFieldModifierFilterDAO.findByField("fieldModifier.id", 1)).thenReturn(filters);
		AbstractMessage swiftMessage = this.swiftUtilHandler.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);
		this.swiftFieldModifierHandler.applyFieldModifiers(swiftMessage);
		AbstractMT abstractMT = (AbstractMT) swiftMessage;
		Field field = abstractMT.getSwiftMessage().field("21");
		Assertions.assertNotNull(field);
		MatcherAssert.assertThat(field.getValue(), IsEqual.equalTo("CHANGE"));
		Assertions.assertTrue(field instanceof Field21);
	}
}
