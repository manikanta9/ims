package com.clifton.swift.field;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.SystemDataType;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@Transactional
@Rollback
public class SwiftFieldModifierServiceTest {

	@Resource
	public ContextHandler contextHandler;

	@Resource
	public SwiftFieldModifierService swiftFieldModifierService;


	@BeforeEach
	public void before() {
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(999).shortValue());
		user.setUserName(SecurityUser.SYSTEM_USER);
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@AfterEach
	public void after() {
		this.contextHandler.removeBean(Context.USER_BEAN_NAME);
	}


	@Test
	public void testBasicEntityPersistence() {
		// Data Type
		SwiftFieldModifierDataType swiftFieldModifierDataType = new SwiftFieldModifierDataType();
		swiftFieldModifierDataType.setDescription("A string or a set of characters.");
		swiftFieldModifierDataType.setName("STRING");
		swiftFieldModifierDataType.setLabel("String");
		swiftFieldModifierDataType.setNumeric(false);
		this.swiftFieldModifierService.saveSwiftFieldModifierDataType(swiftFieldModifierDataType);
		swiftFieldModifierDataType = this.swiftFieldModifierService.getSwiftFieldModifierDataType(SystemDataType.STRING);
		Assertions.assertNotNull(swiftFieldModifierDataType);
		Assertions.assertTrue(swiftFieldModifierDataType.getId() > 0);
		Assertions.assertFalse(swiftFieldModifierDataType.isNewBean());

		MatcherAssert.assertThat(SystemDataType.STRING, IsEqual.equalTo(swiftFieldModifierDataType.getName()));
		MatcherAssert.assertThat("A string or a set of characters.", IsEqual.equalTo(swiftFieldModifierDataType.getDescription()));
		MatcherAssert.assertThat("String", IsEqual.equalTo(swiftFieldModifierDataType.getLabel()));
		MatcherAssert.assertThat(false, IsEqual.equalTo(swiftFieldModifierDataType.isNumeric()));

		// Modifier Type.
		SwiftFieldModifierType swiftFieldModifierType = SwiftFieldModifierTypeBuilder.createModifierType().getSwiftFieldModifierType();
		this.swiftFieldModifierService.saveSwiftFieldModifierType(swiftFieldModifierType);
		swiftFieldModifierType = this.swiftFieldModifierService.getSwiftFieldModifierType(swiftFieldModifierType.getName());
		Assertions.assertNotNull(swiftFieldModifierType);
		Assertions.assertTrue(swiftFieldModifierType.getId() > 0);
		Assertions.assertFalse(swiftFieldModifierType.isNewBean());

		MatcherAssert.assertThat("JUnit Modifier Type", IsEqual.equalTo(swiftFieldModifierType.getName()));
		MatcherAssert.assertThat("com.clifton.swift.field.SwiftFieldModifierBean", IsEqual.equalTo(swiftFieldModifierType.getClassName()));
		MatcherAssert.assertThat("JUnit Modifier Type Description", IsEqual.equalTo(swiftFieldModifierType.getDescription()));
		MatcherAssert.assertThat("JUnit Modifier", IsEqual.equalTo(swiftFieldModifierType.getLabel()));

		// property type.
		SwiftFieldModifierPropertyType swiftFieldModifierPropertyType = SwiftFieldModifierPropertyTypeBuilder.
				createModifierPropertyType()
				.withSystemDataType(swiftFieldModifierDataType)
				.withSwiftFieldModifierType(swiftFieldModifierType)
				.getSwiftFieldModifierPropertyType();
		this.swiftFieldModifierService.saveSwiftFieldModifierPropertyType(swiftFieldModifierPropertyType);
		swiftFieldModifierPropertyType = this.swiftFieldModifierService.getSwiftFieldModifierPropertyType(swiftFieldModifierPropertyType.getName());
		Assertions.assertNotNull(swiftFieldModifierPropertyType);
		Assertions.assertTrue(swiftFieldModifierPropertyType.getId() > 0);
		Assertions.assertFalse(swiftFieldModifierPropertyType.isNewBean());

		MatcherAssert.assertThat("JUnit Property Type", IsEqual.equalTo(swiftFieldModifierPropertyType.getName()));
		MatcherAssert.assertThat("Junit Property Type Label", IsEqual.equalTo(swiftFieldModifierPropertyType.getLabel()));
		Assertions.assertNotNull(swiftFieldModifierPropertyType.getModifierDataType());
		MatcherAssert.assertThat(1, IsEqual.equalTo(swiftFieldModifierPropertyType.getOrder()));
		Assertions.assertNotNull(swiftFieldModifierPropertyType.getModifierDataType());
		Assertions.assertNotNull(swiftFieldModifierPropertyType.getFieldModifierType());
		MatcherAssert.assertThat(true, IsEqual.equalTo(swiftFieldModifierPropertyType.isRequired()));

		// modifier
		SwiftFieldModifier swiftFieldModifier = SwiftFieldModifierBuilder
				.createModifier()
				.withSwiftFieldModifierType(swiftFieldModifierType)
				.getSwiftFieldModifier();
		this.swiftFieldModifierService.saveSwiftFieldModifier(swiftFieldModifier);
		swiftFieldModifier = this.swiftFieldModifierService.getSwiftFieldModifier(swiftFieldModifier.getId());
		Assertions.assertNotNull(swiftFieldModifier);
		Assertions.assertTrue(swiftFieldModifier.getId() > 0);
		Assertions.assertFalse(swiftFieldModifier.isNewBean());

		Assertions.assertNotNull(swiftFieldModifier.getFieldModifierType());
		MatcherAssert.assertThat("JUnit Modifier Type", IsEqual.equalTo(swiftFieldModifier.getFieldModifierType().getName()));

		// filter
		SwiftFieldModifierFilter swiftFieldModifierFilter = SwiftFieldModifierFilterBuilder
				.createModifierFilter()
				.withSwiftFieldModifier(swiftFieldModifier)
				.getSwiftFieldModifierFilter();
		this.swiftFieldModifierService.saveSwiftFieldModifierFilter(swiftFieldModifierFilter);
		swiftFieldModifierFilter = this.swiftFieldModifierService.getSwiftFieldModifierFilter(swiftFieldModifierFilter.getId());
		Assertions.assertNotNull(swiftFieldModifierFilter);
		Assertions.assertTrue(swiftFieldModifierFilter.getId() > 0);
		Assertions.assertFalse(swiftFieldModifierFilter.isNewBean());

		MatcherAssert.assertThat("Field Tag", IsEqual.equalTo(swiftFieldModifierFilter.getFieldTag()));
		MatcherAssert.assertThat("Field Value", IsEqual.equalTo(swiftFieldModifierFilter.getFieldValue()));
		Assertions.assertNotNull(swiftFieldModifierFilter.getFieldModifier());

		// filter list
		List<SwiftFieldModifierFilter> filters = this.swiftFieldModifierService.getSwiftFieldModifierFilterListByModifier(swiftFieldModifier.getId());
		Assertions.assertNotNull(filters);
		Assertions.assertFalse(filters.isEmpty());
		swiftFieldModifierFilter = filters.get(0);
		MatcherAssert.assertThat("Field Tag", IsEqual.equalTo(swiftFieldModifierFilter.getFieldTag()));
		MatcherAssert.assertThat("Field Value", IsEqual.equalTo(swiftFieldModifierFilter.getFieldValue()));
		Assertions.assertNotNull(swiftFieldModifierFilter.getFieldModifier());

		// property
		SwiftFieldModifierProperty swiftFieldModifierProperty = SwiftFieldModifierPropertyBuilder
				.createModifierProperty()
				.withSwiftFieldModifier(swiftFieldModifier)
				.withSwiftFieldModifierPropertyType(swiftFieldModifierPropertyType)
				.getSwiftFieldModifierProperty();
		this.swiftFieldModifierService.saveSwiftFieldModifierProperty(swiftFieldModifierProperty);
		swiftFieldModifierProperty = this.swiftFieldModifierService.getSwiftFieldModifierProperty(swiftFieldModifierProperty.getId());
		Assertions.assertNotNull(swiftFieldModifierProperty);
		Assertions.assertTrue(swiftFieldModifierProperty.getId() > 0);
		Assertions.assertFalse(swiftFieldModifierProperty.isNewBean());

		// property list
		List<SwiftFieldModifierProperty> properties = this.swiftFieldModifierService.getSwiftFieldModifierPropertyListByModifier(swiftFieldModifier.getId());
		Assertions.assertNotNull(properties);
		Assertions.assertFalse(properties.isEmpty());
		swiftFieldModifierProperty = properties.get(0);
		MatcherAssert.assertThat("JUnit Property", IsEqual.equalTo(swiftFieldModifierProperty.getName()));
		MatcherAssert.assertThat("JUnit Property Value", IsEqual.equalTo(swiftFieldModifierProperty.getPropertyValue()));
		Assertions.assertNotNull(swiftFieldModifierProperty.getFieldModifier());
	}
}
