package com.clifton.swift.field;

import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.junit.jupiter.api.Assertions;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class MockSwiftFieldModifierProcessor implements SwiftFieldModifierBean<AbstractMessage> {

	@Override
	public void execute(AbstractMessage message, SwiftFieldModifier modifier) {
		List<SwiftFieldModifierProperty> properties = modifier.getPropertyList();
		for (SwiftFieldModifierProperty property : properties) {
			String propertyValue = property.getPropertyValue();
			String name = property.getName();
			Assertions.assertTrue(message.isMT());
			AbstractMT abstractMT = (AbstractMT) message;
			List<String> tagNameList = abstractMT.getSwiftMessage().getTagNames();
			Assertions.assertTrue(tagNameList.contains(name));
			Field field = abstractMT.getSwiftMessage().field(name);
			Assertions.assertNotNull(field);
			Tag tag = field.asTag();
			tag.setValue(propertyValue);
			// not sure we can do this without knowing which block the field resides.
			abstractMT.getSwiftMessage().getBlock4().removeTag(name);
			abstractMT.append(tag);
		}
	}
}
