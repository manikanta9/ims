package com.clifton.swift.prowide.converter.mt.mt2xx;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.transfers.GeneralFinancialInstitutionTransferMessage;
import com.clifton.instruction.messages.transfers.TransferCancellationMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterLocator;
import com.clifton.swift.prowide.converter.mt.ConversionMapping;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt2xx.MT202;
import com.prowidesoftware.swift.model.mt.mt2xx.MT292;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class GeneralFinancialInstitutionTransferMessageToMT202ConverterTest {

	private static final String EXPECTED_GENERAL_FINANCIAL_TRANSFER = "{1:F01PNBPGB2DAXXX0000000000}{2:I202NWNBSGS1XXXXN}{3:{121:fbc03f20-d6f9-4826-92aa-b9c8a8534795}}{4:\r\n:20:transactionReferenceNumber\r\n:21:MARG\r\n:32A:170803USD123,456\r\n:53B:/custodyAccountNumber\r\n:57A:UNVKCY2N\r\n:58A:/intermediaryAccountNumber\r\nBSYSIE21\r\n:72:/BNF/beneficiaryAccountNumber/\r\n-}";
	private static final String EXPECTED_GENERAL_FINANCIAL_TRANSFER_CANCELLATION = "{1:F01PNBPGB2DAXXX0000000000}{2:I292NWNBSGS1XXXXN}{4:\r\n" +
			":20:transactionReferenceNumber\r\n" +
			":21:123\r\n" +
			":11S:202\r\n" +
			"170803\r\n" +
			":20:123\r\n" +
			":21:MARG\r\n" +
			":32A:170803USD123,456\r\n" +
			":58A:/intermediaryAccountNumber\r\n" +
			"BSYSIE21\r\n" +
			"-}";

	private List<MT202ConversionMapping> expectedValueList = Arrays.asList(
			new MT202ConversionMapping("transactionReferenceNumber", "transactionReferenceNumber", (m) -> m.getField20().getValue()),
			new MT202ConversionMapping("senderBIC", "PNBPGB2D", (m) -> m.getSender().substring(0, 8)),
			new MT202ConversionMapping("receiverBIC", "NWNBSGS1", (m) -> m.getReceiver().substring(0, 8)),
			new MT202ConversionMapping("messagePurpose", TransferMessagePurposes.DAILY_MARGIN, (m) ->
					Stream.of(m.getField21().getReference()).map(type ->
							Arrays.stream(TransferMessagePurposes.values()).filter(e ->
									StringUtils.isEqual(type, e.getCode())).findFirst().orElse(null)).findFirst().orElse(null)
			),
			new MT202ConversionMapping("currency", "USD", (m) -> m.getField32A().getCurrency()),
			new MT202ConversionMapping("valueDate", DateUtils.asUtilDate(LocalDate.of(2017, 8, 3)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getField32A().getDate(), DateTimeFormatter.ofPattern("yyMMdd")))),
			new MT202ConversionMapping("amount", new BigDecimal("123.456"), (m) -> m.getField32A().getAmountAsNumber()),
			new MT202ConversionMapping("custodyAccountNumber", "custodyAccountNumber", (m) -> m.getField53B().getAccount()),
			new MT202ConversionMapping("custodyBIC", "MEORUS61", (m) -> null),
			new MT202ConversionMapping("intermediaryBIC", "UNVKCY2N", (m) -> m.getField57A().getBIC()),
			new MT202ConversionMapping("intermediaryAccountNumber", "intermediaryAccountNumber", (m) -> m.getField58A().getAccount()),
			new MT202ConversionMapping("beneficiaryBIC", "BSYSIE21", (m) -> m.getField58A().getBIC()),
			new MT202ConversionMapping("beneficiaryAccountNumber", "beneficiaryAccountNumber", (m) -> m.getField72().getNarrative().substring(5, "beneficiaryAccountNumber".length() + 5)),
			new MT202ConversionMapping("beneficiaryAccountName", "beneficiaryAccountName", (m) -> null),
			new MT202ConversionMapping("messageReferenceNumber", null, (m) -> null)
	);

	private List<MT292ConversionMapping> expectedCancelledValueList = Arrays.asList(
			new MT292ConversionMapping("transactionReferenceNumber", "transactionReferenceNumber", (m) -> m.getField20().getValue()),
			new MT292ConversionMapping("messageReferenceNumber", null, (m) -> Long.parseLong(m.getField21().getValue())),
			new MT292ConversionMapping("senderBIC", "PNBPGB2D", (m) -> m.getSender().substring(0, 8)),
			new MT292ConversionMapping("receiverBIC", "NWNBSGS1", (m) -> m.getReceiver().substring(0, 8)),
			new MT292ConversionMapping("valueDate", DateUtils.asUtilDate(LocalDate.of(2017, 8, 3)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getField11S().getDate(), DateTimeFormatter.ofPattern("yyMMdd"))))
	);


	private static String replaceUniqueTransferMessageReference(String message) {
		StringBuilder builder = new StringBuilder(message);
		int index = message.indexOf("3:{121:") + "3:{121:".length();
		builder.delete(index, index + "fbc03f20-d6f9-4826-92aa-b9c8a8534795".length());
		return builder.insert(index, "fbc03f20-d6f9-4826-92aa-b9c8a8534795").toString();
	}


	@Test
	public void testToSwiftEntity() {
		GeneralFinancialInstitutionTransferMessage generalFinancialInstitutionTransferMessage = new GeneralFinancialInstitutionTransferMessage();
		Map<String, Object> expectedValueMap = new HashMap<>();
		this.expectedValueList.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		expectedValueMap.forEach((key, value) -> BeanUtils.setPropertyValue(generalFinancialInstitutionTransferMessage, key, value));

		Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(generalFinancialInstitutionTransferMessage);
		MapDifference<String, Object> differences = Maps.difference(expectedValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "All properties should have been set by reflection " + differences.toString());

		List<String> allPropertyNames = BeanUtils.getPropertyNames(GeneralFinancialInstitutionTransferMessage.class, true, true);
		allPropertyNames.removeAll(valueMap.keySet());
		Assertions.assertTrue(allPropertyNames.isEmpty(), "Should have all available properties set" + allPropertyNames);

		GeneralFinancialInstitutionTransferMessageToMT202Converter converter = new GeneralFinancialInstitutionTransferMessageToMT202Converter();
		String messageString = converter.toSwiftMessage(generalFinancialInstitutionTransferMessage);
		MatcherAssert.assertThat(replaceUniqueTransferMessageReference(messageString), IsEqual.equalTo(EXPECTED_GENERAL_FINANCIAL_TRANSFER));

		final MT202 mt202 = converter.toSwiftEntity(generalFinancialInstitutionTransferMessage);
		Map<String, Object> mtValueMap = this.expectedValueList.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt202)), CollectionUtils.throwingCollectCombiner());
		List<String> expectedDifferences = Arrays.asList("custodyBIC", "beneficiaryAccountName");
		final MapDifference<String, Object> allDifferences = Maps.difference(mtValueMap, valueMap);
		expectedDifferences.forEach(k -> Assertions.assertTrue(allDifferences.entriesDiffering().containsKey(k), "Expected difference for property " + k));
		expectedDifferences.forEach(k -> {
			valueMap.remove(k);
			mtValueMap.remove(k);
		});
		differences = Maps.difference(mtValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "MT message values are not the same as GeneralFinancialInstitutionTransfer values " + differences.toString());
	}


	@Test
	public <M extends AbstractMT> void testToSwiftEntityCancellation() {
		List<ConversionMapping<MT202>> cancellationOverrides = this.expectedValueList.stream()
				.map(mapping -> StringUtils.isEqual(mapping.getProperty(), "messageReferenceNumber") ? new ConversionMapping<MT202>("messageReferenceNumber", 123L, (m) -> Long.parseLong(m.getField20().getValue())
				) : mapping)
				.collect(Collectors.toList());
		GeneralFinancialInstitutionTransferMessage generalFinancialInstitutionTransferMessage = new GeneralFinancialInstitutionTransferMessage();
		Map<String, Object> expectedValueMap = new HashMap<>();
		this.expectedValueList.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		cancellationOverrides.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		expectedValueMap.forEach((key, value) -> BeanUtils.setPropertyValue(generalFinancialInstitutionTransferMessage, key, value));

		Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(generalFinancialInstitutionTransferMessage);
		MapDifference<String, Object> differences = Maps.difference(expectedValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "All properties should have been set by reflection " + differences.toString());

		List<String> allPropertyNames = BeanUtils.getPropertyNames(GeneralFinancialInstitutionTransferMessage.class, true, true);
		allPropertyNames.removeAll(valueMap.keySet());
		Assertions.assertTrue(allPropertyNames.isEmpty(), "Should have all available properties set" + allPropertyNames);

		GeneralFinancialInstitutionTransferMessageToMT202Converter transferMessageToMT202Converter = new GeneralFinancialInstitutionTransferMessageToMT202Converter();
		SwiftMessageConverterLocator mockLocator = Mockito.mock(SwiftMessageConverterLocator.class);
		Mockito.when(mockLocator.locate(ArgumentMatchers.any(GeneralFinancialInstitutionTransferMessage.class), ArgumentMatchers.same(SwiftMessageFormats.MT)))
				.thenReturn(transferMessageToMT202Converter);
		TransferCancellationMessage transferCancellationMessage = new TransferCancellationMessage(generalFinancialInstitutionTransferMessage);
		FinancialInstitutionTransferMessageToMT292Converter<M> converter = new FinancialInstitutionTransferMessageToMT292Converter<>();
		converter.setSwiftMessageConverterLocator(mockLocator);
		String messageString = converter.toSwiftMessage(transferCancellationMessage);
		MatcherAssert.assertThat(messageString, IsEqual.equalTo(EXPECTED_GENERAL_FINANCIAL_TRANSFER_CANCELLATION));

		final MT292 mt292 = converter.toSwiftEntity(transferCancellationMessage);
		Map<String, Object> mt292ValueMap = this.expectedCancelledValueList.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt292)), CollectionUtils.throwingCollectCombiner());
		final MT202 mt202 = transferMessageToMT202Converter.toSwiftEntity((GeneralFinancialInstitutionTransferMessage) transferCancellationMessage.getOriginalMessage());
		Map<String, Object> mt202ValueMap = this.expectedValueList.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt202)), CollectionUtils.throwingCollectCombiner());
		Map<String, Object> mtValueMap = new HashMap<>();
		mtValueMap.putAll(mt202ValueMap);
		mtValueMap.putAll(mt292ValueMap);
		List<String> expectedDifferences = Arrays.asList("custodyBIC", "beneficiaryAccountName");
		MapDifference<String, Object> allDifferences = Maps.difference(mtValueMap, valueMap);
		expectedDifferences.forEach(k -> Assertions.assertTrue(allDifferences.entriesDiffering().containsKey(k), "Expected difference for property " + k));
		expectedDifferences.forEach(k -> {
			mtValueMap.remove(k);
			valueMap.remove(k);
		});
		differences = Maps.difference(mtValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "MT message values are not the same as GeneralFinancialInstitutionTransfer values " + differences.toString());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFromSwiftEntity() throws IOException {
		MT202 abstractMt = (MT202) AbstractMT.parse(EXPECTED_GENERAL_FINANCIAL_TRANSFER);
		abstractMt.getSwiftMessage().setUETR("fbc03f20-d6f9-4826-92aa-b9c8a8534795");
		GeneralFinancialInstitutionTransferMessageToMT202Converter transferMessageToMT202Converter = new GeneralFinancialInstitutionTransferMessageToMT202Converter();
		GeneralFinancialInstitutionTransferMessage transferMessage = transferMessageToMT202Converter.fromSwiftEntity(abstractMt);
		// this is a lossy field value, it cannot be reversed, however, it is not required in the cancellation message.
		transferMessage.setBeneficiaryAccountNumber("beneficiaryAccountNumber");
		String roundTripMessage = transferMessageToMT202Converter.toSwiftMessage(transferMessage);
		MatcherAssert.assertThat(replaceUniqueTransferMessageReference(roundTripMessage), IsEqual.equalTo(EXPECTED_GENERAL_FINANCIAL_TRANSFER));
	}


	private static class MT202ConversionMapping extends ConversionMapping<MT202> {

		public MT202ConversionMapping(String property, Object value, Function<MT202, Object> mapper) {
			super(property, value, mapper);
		}
	}

	private static class MT292ConversionMapping extends ConversionMapping<MT292> {

		public MT292ConversionMapping(String property, Object value, Function<MT292, Object> mapper) {
			super(property, value, mapper);
		}
	}
}
