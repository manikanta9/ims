package com.clifton.swift.prowide.server.router.handler;

import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.messaging.error.SwiftMessagingErrorService;
import com.clifton.swift.server.handler.SwiftIncomingMessageHandler;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.util.SwiftUtilHandler;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration("classpath:com/clifton/swift/prowide/server/router/handler/prowide-swift-incoming-message-handler.xml")
@ExtendWith(SpringExtension.class)
public class SwiftIncomingOperationalMessageHandlerTest<M> {

	private static final String MSG_202 = "{1:F01PPSCUS60AXXX0011000005}{2:I202CNORUS40XCOLN}{4:\n" +
			":20:IMSCOL45983010\n" +
			":21:MARG\n" +
			":32A:161110USD900222,64\n" +
			":53B:/SUR55\n" +
			":57A:CNORSGSG\n" +
			":58A:/15700043\n" +
			"GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:AA844061483C}{TNG:}}";

	@Resource
	private SwiftIncomingMessageHandler<M> swiftIncomingOperationalMessageHandler;

	@Resource
	private SwiftMessagingErrorService swiftMessagingErrorService;

	@Resource
	private AsynchronousMessageHandler swiftServerAsynchronousMessageHandler;

	@Resource
	private SwiftUtilHandler<M> swiftUtilHandler;


	@Test
	public void handleTest() {
		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 1);
		swiftSourceSystem.setName("swift source system");

		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
		swiftSourceSystemModule.setId((short) 1);
		swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);

		SwiftMessage swiftMessage01 = new SwiftMessage();
		swiftMessage01.setId((long) 1);
		swiftMessage01.setText(MSG_202);
		swiftMessage01.setMessageFormat(SwiftMessageFormats.MT);

		SwiftIdentifier swiftIdentifier01 = new SwiftIdentifier();
		swiftIdentifier01.setId((long) 1);
		swiftIdentifier01.setUniqueStringIdentifier("IMSCOL4598301");
		swiftIdentifier01.setSourceSystemModule(swiftSourceSystemModule);

		swiftMessage01.setIdentifier(swiftIdentifier01);
		SwiftMessageType swiftMessageType202 = new SwiftMessageType();
		swiftMessageType202.setId((short) 1);
		swiftMessageType202.setMtVersionName("MT202");
		swiftMessage01.setType(swiftMessageType202);

		M message = this.swiftUtilHandler.parseSingleMessage(MSG_202, SwiftMessageFormats.MT);

		this.swiftIncomingOperationalMessageHandler.handle(swiftMessage01, SwiftServerMessageRouterCommand.ofMessageAndIdentifier(message, swiftIdentifier01));
		Mockito.verify(this.swiftMessagingErrorService, Mockito.times(0)).postErrorMessage(Mockito.any(), Mockito.any());
		Mockito.verify(this.swiftServerAsynchronousMessageHandler, Mockito.times(1)).send(Mockito.any());
	}
}
