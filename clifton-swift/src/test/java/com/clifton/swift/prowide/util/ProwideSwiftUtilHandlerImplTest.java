package com.clifton.swift.prowide.util;

import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.prowide.converter.ProwideSwiftMessageConverterHandlerImpl;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt0xx.MT094;
import com.prowidesoftware.swift.model.mt.mt2xx.MT202;
import com.prowidesoftware.swift.model.mt.mt2xx.MT210;
import com.prowidesoftware.swift.model.mt.mt5xx.MT541;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;


public class ProwideSwiftUtilHandlerImplTest {

	public static final String MESSAGES_INCLUDING_094 = "{1:F21PPSCUS66AXXX0018000638}{4:{177:1701191526}{451:0}}{1:F01PPSCUS66AXXX0018000638}{2:O0941526170119NDNDXXXX9XXX00016791891701190726S}{4:{135:N}{136:B74142}{129:01/01}{130:/05/SWIFT BIC\n" +
			"/02/ADDITION}{134:MIDLGB22\n" +
			"HSBC BANK PLC\n" +
			"LONDON}{312:PLEASE BE ADVISED THAT MIDLGB2195W WILL BE PUBLISHED IN:\n" +
			"THE BIC ONLINE FREE SEARCH AS OF 21 JANUARY 2017\n" +
			"THE MONTHLY FILE OF 04 FEBRUARY 2017\n" +
			"THE NEXT POSSIBLE BIC DIRECTORY PAPER VERSION}}{5:{CHK:A30D181482B9}{SYS:1525170119MIDLGB22BXXX9596378211}}{S:{COP:P}}${1:F21PPSCUS66AXXX0018002622}{4:{177:1701190726}{451:0}}{1:F01PPSCUS66AXXX0018002622}{2:I210CNORUS44XETDN}{4:\n" +
			":20:IMS481172MTM\n" +
			":25:26-65136\n" +
			":30:170119\n" +
			":21:MARG\n" +
			":32B:USD2145,\n" +
			":52A:GOLDUS33\n" +
			":56A:MRMDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:96581340FDAC}}";

	public static final String MT541 = "{1:F01PPSCUS60AXXX0000000000}{2:I541CNORUS40XETDN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//IMS708032T;IMS890258TF;IMS7092185AT\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			":16R:TRADDET\n" +
			":94B::TRAD//EXCH/IFUS\n" +
			":98A::SETT//20170619\n" +
			":98A::TRAD//20170619\n" +
			":90B::DEAL//ACTU/USD996,63\n" +
			":35B:/TS/MESU7\n" +
			"mini MSCI Emg Mkt Sep17\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/FUT\n" +
			":11A::DENO//USD\n" +
			":98A::EXPI//20170915\n" +
			":36B::SIZE//UNIT/50,\n" +
			":16S:FIA\n" +
			":22F::PROC//OPEP\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/10,\n" +
			":97A::SAFE//SJO10\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95Q::PSET//XX\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::DEAG//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::SELL//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD22,1\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD0,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::EXEC//USD22,1\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	public static final String MT202 = "{1:F01PPSCUS66AXXX0000000000}{2:I202CNORUS44XETDN}{4:\n" +
			":20:IMS472677MTM\n" +
			":21:MARG\n" +
			":32A:161222USD300380,\n" +
			":53B:/RYC03\n" +
			":57A:IRVTUS3NXXX\n" +
			":58A:/89-0000-6978\n" +
			"MSNYUS33\n" +
			":72:/BNF/052BAG2S/\n" +
			"-}";

	private static final String EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES_CANCEL = "{1:F01PNBPGB2DAXXX0000000000}{2:I543NWNBSGS1XXXXN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//transactionReferenceNumber\r\n" +
			":23G:CANC\r\n" +
			":16R:LINK\r\n" +
			":20C::PREV//123\r\n" +
			":16S:LINK\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":94B::TRAD//EXCH/USD\r\n" +
			":98A::SETT//20170803\r\n" +
			":98A::TRAD//20170804\r\n" +
			":90B::DEAL//ACTU/USD150,31\r\n" +
			":35B:ISIN 122333444\r\n" +
			":35B:/TS/STATE_STREET_FUND_NUMBER\r\n" +
			"Security Description\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/CS\r\n" +
			":11A::DENO//USD\r\n" +
			":36B::SIZE//UNIT/112,19\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//UNIT/999,111\r\n" +
			":97A::SAFE//custodyAccountNumber\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//DTCYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95Q::REAG//123\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95Q::BUYR//456\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD258,369\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD129,352\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::EXEC//USD5,77\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::REGF//USD3,444\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";


	@Test
	public void testParse094Message() {
		ProwideSwiftMessageConverterHandlerImpl prowideSwiftMessageConverterHandler = new ProwideSwiftMessageConverterHandlerImpl();
		Map<String, AbstractMessage> map = prowideSwiftMessageConverterHandler.parse(MESSAGES_INCLUDING_094, SwiftMessageFormats.MT);
		Assertions.assertNotNull(map);
		Assertions.assertEquals(2, map.values().size());
		String[] splits = MESSAGES_INCLUDING_094.split("\\$");
		Assertions.assertEquals(2, splits.length);
		Assertions.assertTrue(map.containsKey(splits[0]));
		Assertions.assertTrue(map.containsKey(splits[1]));
		ProwideSwiftUtilHandlerImpl prowideSwiftUtilHandler = new ProwideSwiftUtilHandlerImpl();
		Map<AbstractMessage, Boolean> ackedMessages = prowideSwiftUtilHandler.getAllMessagesMap(map.get(splits[0]));
		Assertions.assertEquals(1, ackedMessages.values().size());
		Assertions.assertTrue(MT094.class.isAssignableFrom(ackedMessages.keySet().iterator().next().getClass()));
		ackedMessages = prowideSwiftUtilHandler.getAllMessagesMap(map.get(splits[1]));
		Assertions.assertEquals(1, ackedMessages.values().size());
		Assertions.assertTrue(MT210.class.isAssignableFrom(ackedMessages.keySet().iterator().next().getClass()));
	}


	@Test
	public void testReplaceSwiftMessageTransactionReferenceNumber() throws Exception {
		ProwideSwiftUtilHandlerImpl prowideSwiftUtilHandler = new ProwideSwiftUtilHandlerImpl();
		MT541 mt541 = (MT541) AbstractMT.parse(MT541);
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftIdentifier.setId(123456L);
		prowideSwiftUtilHandler.replaceSwiftMessageTransactionReferenceNumber(mt541, swiftIdentifier);
		MatcherAssert.assertThat(mt541.message().replaceAll("\\s", ""), IsEqual.equalTo(MT541.replace(":20C::SEME//IMS708032T;IMS890258TF;IMS7092185AT"
				, ":20C::SEME//123456").replaceAll("\\s", "")));

		MT202 mt202 = (MT202) AbstractMT.parse(MT202);
		swiftIdentifier.setId(789L);
		prowideSwiftUtilHandler.replaceSwiftMessageTransactionReferenceNumber(mt202, swiftIdentifier);
		MatcherAssert.assertThat(mt202.message().replaceAll("\\s", ""), IsEqual.equalTo(MT202.replace(":20:IMS472677MTM"
				, ":20:789").replaceAll("\\s", "")));
	}


	@Test
	public void testGetTransactionReferenceNumber() throws Exception {
		ProwideSwiftUtilHandlerImpl prowideSwiftUtilHandler = new ProwideSwiftUtilHandlerImpl();
		MT541 mt541 = (MT541) AbstractMT.parse(MT541);
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftIdentifier.setId(123456L);
		prowideSwiftUtilHandler.replaceSwiftMessageTransactionReferenceNumber(mt541, swiftIdentifier);
		String referenceNumber = prowideSwiftUtilHandler.getTransactionReferenceNumber(mt541);
		MatcherAssert.assertThat(referenceNumber, IsEqual.equalTo("123456"));

		MT202 mt202 = (MT202) AbstractMT.parse(MT202);
		swiftIdentifier.setId(789L);
		prowideSwiftUtilHandler.replaceSwiftMessageTransactionReferenceNumber(mt202, swiftIdentifier);
		referenceNumber = prowideSwiftUtilHandler.getTransactionReferenceNumber(mt202);
		MatcherAssert.assertThat(referenceNumber, IsEqual.equalTo("789"));
	}


	@Test
	public void testGetMessageReferenceNumber() throws Exception {
		ProwideSwiftUtilHandlerImpl prowideSwiftUtilHandler = new ProwideSwiftUtilHandlerImpl();
		MT202 mt202 = (MT202) AbstractMT.parse(MT202);
		Long messageReferenceNumber = prowideSwiftUtilHandler.getMessageReferenceNumber(mt202);
		Assertions.assertNull(messageReferenceNumber);
		AbstractMT message = AbstractMT.parse(EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES_CANCEL);
		messageReferenceNumber = prowideSwiftUtilHandler.getMessageReferenceNumber(message);
		MatcherAssert.assertThat(messageReferenceNumber, IsEqual.equalTo(123L));
		String transactionReferenceNumber = prowideSwiftUtilHandler.getTransactionReferenceNumber(message);
		MatcherAssert.assertThat(transactionReferenceNumber, IsEqual.equalTo("transactionReferenceNumber"));
	}
}
