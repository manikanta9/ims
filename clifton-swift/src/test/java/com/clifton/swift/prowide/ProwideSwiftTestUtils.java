package com.clifton.swift.prowide;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.mt.AbstractMT;


/**
 * Created by mwacker on 12/9/2016.
 */
public class ProwideSwiftTestUtils {


	public static void setUniqueId(AbstractMessage message, String uniqueStringIdentifier) {
		validate(message);
		if (message.isMT()) {
			AbstractMT abstractMt = (AbstractMT) message;
			Field20 field20 = Field20.get(abstractMt.getSwiftMessage());
			Field20 changed = field20.setComponent1(uniqueStringIdentifier);
			abstractMt.getSwiftMessage().getBlock4().removeTag(Field20.NAME);
			abstractMt.getSwiftMessage().getBlock4().append(changed);
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}


	private static void validate(AbstractMessage abstractMessage) {
		ValidationUtils.assertNotNull(abstractMessage, "Message cannot be null");
		if (abstractMessage.isMT()) {
			AbstractMT abstractMT = (AbstractMT) abstractMessage;
			if (abstractMT.getSwiftMessage() == null || abstractMT.getSwiftMessage().getBlock4() == null
					|| !abstractMT.getSwiftMessage().getBlock4().containsField(Field20.NAME)) {
				throw new RuntimeException("Could not find the identifier field in the message, probably due to a parsing error.");
			}
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}
}
