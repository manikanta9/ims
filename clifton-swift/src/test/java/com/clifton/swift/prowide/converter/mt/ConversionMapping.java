package com.clifton.swift.prowide.converter.mt;

import com.prowidesoftware.swift.model.mt.AbstractMT;

import java.util.function.Function;


public class ConversionMapping<M extends AbstractMT> {

	private String property;
	private Object value;
	private Function<M, Object> mapper;


	public ConversionMapping(String property, Object value, Function<M, Object> mapper) {
		this.property = property;
		this.value = value;
		this.mapper = mapper;
	}


	public String getProperty() {
		return this.property;
	}


	public Object getValue() {
		return this.value;
	}


	public Function<M, Object> getMapper() {
		return this.mapper;
	}
}
