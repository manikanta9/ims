package com.clifton.swift.prowide.vistor;

import com.prowidesoftware.swift.model.SwiftTagListBlock;
import com.prowidesoftware.swift.model.field.Field35B;
import com.prowidesoftware.swift.model.field.Field70F;
import com.prowidesoftware.swift.model.field.Field77F;
import com.prowidesoftware.swift.model.field.Field97A;
import com.prowidesoftware.swift.model.mt.mt1xx.MT105;
import com.prowidesoftware.swift.model.mt.mt5xx.MT541;
import com.prowidesoftware.swift.model.mt.mt5xx.MT568;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Test;


public class SwiftMessageTextEncoderTest {

	@Test
	public void testEBCDICIllegalCharacter() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		// illegal $
		MT541 message = new MT541();
		message.append(MT541.SequenceB.newInstance(
				new SwiftTagListBlock()
						.append(new Field35B().setComponent1("/TS/IBM").setDescription("This has $ which is illegal"))
				)
		);
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nThis has $ which is illegal"));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getSequenceB().getFieldsByName(Field35B.NAME)[0].getComponent(4), IsEqual.equalTo("This has ??5B which is illegal"));
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nThis has ??5B which is illegal"));
	}


	@Test
	public void testEBCDICNoIllegalCharacter() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		// no illegal characters
		MT541 message = new MT541();
		message.append(MT541.SequenceB.newInstance(
				new SwiftTagListBlock()
						.append(new Field35B().setComponent1("/TS/IBM").setDescription("Must not change"))
				)
		);
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nMust not change"));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getSequenceB().getFieldsByName(Field35B.NAME)[0].getComponent(4), IsEqual.equalTo("Must not change"));
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nMust not change"));
	}


	@Test
	public void testEBCDICAllLegalCharacter() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		// no illegal characters
		MT541 message = new MT541();
		message.append(MT541.SequenceB.newInstance(
				new SwiftTagListBlock()
						// 4*35x
						.append(new Field35B().setComponent1("/TS/IBM").setDescription("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-?:().,'+ "))
				)
		);
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-?:().,'+ "));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getSequenceB().getFieldsByName(Field35B.NAME)[0].getComponent(4), IsEqual.equalTo("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-?:().,'+ "));
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-?:().,'+ "));
	}


	@Test
	public void testSingleControlCharacterSetDescription() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		// SWIFT will fix singular control characters when using setDescription
		MT541 message = new MT541();
		message.append(MT541.SequenceB.newInstance(
				new SwiftTagListBlock()
						.append(new Field35B().setComponent1("/TS/IBM").setDescription("Lines cannot have embedded singular \r carriage returns."))
				)
		);
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nLines cannot have embedded singular \r\n carriage returns."));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getSequenceB().getFieldsByName(Field35B.NAME)[0].getComponent(4), IsEqual.equalTo("Lines cannot have embedded singular "));
		MatcherAssert.assertThat(message.getSequenceB().getFieldsByName(Field35B.NAME)[0].getComponent(5), IsEqual.equalTo(" carriage returns."));
		MatcherAssert.assertThat(message.getField35B().get(0).getValue(), IsEqual.equalTo("/TS/IBM\r\nLines cannot have embedded singular \r\n carriage returns."));
	}


	@Test
	public void testSingleControlCharacterSetComponent() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		// SWIFT when not using setDescription to handle control characters.
		MT541 message = new MT541();
		message.append(MT541.SequenceC.newInstance(
				new SwiftTagListBlock()
						// (:4c//35x)
						.append(new Field97A().setQualifier("SAFE").setAccount("embedded singular \r carriage return"))
				)
		);
		MatcherAssert.assertThat(message.getField97A().get(0).getValue(), IsEqual.equalTo(":SAFE//embedded singular \r carriage return"));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getSequenceC().getFieldsByName(Field97A.NAME)[0].getComponent(2), IsEqual.equalTo("embedded singular \r\n carriage return"));
		MatcherAssert.assertThat(message.getField97A().get(0).getValue(), IsEqual.equalTo(":SAFE//embedded singular \r\n carriage return"));
	}


	@Test
	public void testEDIFACTLegal() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		MT105 message = new MT105();
		message.addField(new Field77F("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,-()/='+:?!\"%&*<>;"));
		MatcherAssert.assertThat(message.getField77F().getValue(), IsEqual.equalTo("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,-()/='+:?!\"%&*<>;"));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getField77F().getValue(), IsEqual.equalTo("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 .,-()/='+:?!\"%&*<>;"));
	}


	@Test
	public void testEDIFACTIllegal() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		MT105 message = new MT105();
		message.addField(new Field77F("A\r\n$Z"));
		MatcherAssert.assertThat(message.getField77F().getValue(), IsEqual.equalTo("A\r\n$Z"));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getField77F().getValue(), IsEqual.equalTo("AZ"));
	}


	@Test
	public void testZCharacterSetLegal() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		MT568 message = new MT568();
		message.addField(new Field70F("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 \r\n.,-()/='+:?!\"%&*<>;{@#_"));
		MatcherAssert.assertThat(message.getField70F().get(0).getValue(), IsEqual.equalTo(":ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 \r\n.,-()/='+:?!\"%&*<>;{@#_//"));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getField70F().get(0).getValue(), IsEqual.equalTo(":ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 \r\n.,-()/='+:?!\"%&*<>;{@#_//"));
	}


	@Test
	public void testZCharacterSetIllegal() {
		SwiftMessageTextEncoder swiftMessageTextEncoder = new SwiftMessageTextEncoder();
		MT568 message = new MT568();
		message.addField(new Field70F("A\r\n$Z"));
		MatcherAssert.assertThat(message.getField70F().get(0).getValue(), IsEqual.equalTo(":A\r\n$Z//"));

		message.getSwiftMessage().visit(swiftMessageTextEncoder);
		MatcherAssert.assertThat(message.getField70F().get(0).getValue(), IsEqual.equalTo(":A\r\nZ//"));
	}
}
