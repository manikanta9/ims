package com.clifton.swift.prowide.server.router;

import com.clifton.core.util.StringUtils;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.instruction.messaging.message.InstructionMessagingStatusMessage;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.identifier.SwiftIdentifierHandlerImpl;
import com.clifton.swift.identifier.SwiftIdentifierService;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.message.SwiftMessageTypeBuilder;
import com.clifton.swift.message.search.SwiftMessageTypeSearchForm;
import com.clifton.swift.prowide.server.router.handler.SwiftIncomingOperationalMessageHandler;
import com.clifton.swift.prowide.server.router.handler.SwiftIncomingSystemMessageHandler;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.server.status.SwiftServerStatusHandlerImpl;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.system.SwiftSourceSystemService;
import com.clifton.swift.util.SwiftUtilHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockingDetails;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ProwideSwiftServerMessageRouterImplTest {

	public static final String SWIFT_UNKNOWN_298 = "{1:F21PPSCUS60AXXX0009000001}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000001}{2:I298CNORUS40XCOLN}{4:\n" +
			":20:IMSCOL4598301\n" +
			":21:MARG\n" +
			":32A:161110USD900222,64\n" +
			":53B:/SUR55\n" +
			":57A:CNORSGSG\n" +
			":58A:/15700043\n" +
			"GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:BDCB376335F5}{TNG:}}";

	public static final String SWIFT_ACK_202 = "{1:F21PPSCUS60AXXX0009000001}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000001}{2:I202CNORUS40XCOLN}{4:\n" +
			":20:IMSCOL4598301\n" +
			":21:MARG\n" +
			":32A:161110USD900222,64\n" +
			":53B:/SUR55\n" +
			":57A:CNORSGSG\n" +
			":58A:/15700043\n" +
			"GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:BDCB376335F5}{TNG:}}";

	public static final String SWIFT_ACK_094 = "{1:F21PPSCUS66AXXX0256006354}{4:{177:1911131416}{451:0}}{1:F01PPSCUS66AXXX0256006354}{2:O0941416191113MTMTXXXX9XXX00515521031911130616S}{4:{135:N}{136:B75022}{129:02/02}{130:/01/BANK\n" +
			"/01/OPERATIONAL}{134:BKTRUS33\n" +
			"DEUTSCHE BANK TRUST COMPANY AMERICAS\n" +
			"NEW YORK,NY}{312:IF WE BECOME AWARE THAT CORRESPONDENT ACCOUNT YOU HOLD\n" +
			"AT OUR FINANCIAL INSTITUTION HAS PROCESSED ANY PROHIBITED\n" +
			"TRANSACTIONS INVOLVING A FINANCIAL INSTITUTION IN DESIGNATED\n" +
			"JURISDICTION(31 CFR 1010.661), INCLUDING ANY AGENCIES, BRANCHES,\n" +
			"OFFICES, OR SUBSIDIARIES THEREOF, WE WILL BE REQUIRED TO TAKE\n" +
			"APPROPRIATE STEPS TO PREVENT SUCH ACCESS, INCLUDING TERMINATING\n" +
			"YOUR ACCOUNT. AS A REMINDER, PER US REGULATIONS ISSUED OR\n" +
			"PROPOSED UNDER SECTION 311 OF USA PATRIOT ACT, DBTCA WILL NOT\n" +
			"ESTABLISH, MAINTAIN, ADMINISTER, OR MANAGE ACCOUNT FOR OR ON\n" +
			"BEHALF OF BELOW, AND IF IDENTIFIED THAT THE CORRESPONDENT \n" +
			"ACCOUNT YOU HOLD AT DBTCA HAS PROCESSED ANY TRANSACTIONS\n" +
			"INVOLVING BELOW, DBTCA WILL BE REQUIRED TO TAKE APPROPRIATE \n" +
			"STEPS TO PREVENT SUCH ACCESS, INCLUDING TERMINATING YOUR ACCOUNT.\n" +
			"PLEASE SEE SPECIAL MEASURES FOR JURISDICTIONS, FINANCIAL\n" +
			"INSTITUTIONS, OR INTERNATIONAL TRANSACTIONS OF PRIMARY\n" +
			"MONEY LAUNDERING CONCERN: \n" +
			"HTTPS://WWW.FINCEN.GOV\n" +
			"/RESOURCES\n" +
			"/STATUTES-AND-REGULATIONS\n" +
			"/311-SPECIAL-MEASURES}}{5:{CHK:3A5688C2B977}{SYS:1415191113BKTRUS33AXXX0350000352}}{S:{COP:P}}";

	@Resource
	private SwiftServerMessageRouter<AbstractMessage> swiftServerMessageRouter;

	@Resource
	private SwiftUtilHandler<AbstractMessage> utilService;

	@Resource
	private SwiftIdentifierHandler<AbstractMessage> swiftIdentifierHandler;

	@Mock
	private SwiftMessageService swiftMessageService;

	@Mock
	private SwiftSourceSystemService swiftSourceSystemService;

	@Mock
	private SwiftIdentifierService swiftIdentifierService;


	private List<SwiftMessageType> swiftMessageTypeList = SwiftMessageTypeBuilder.getSwiftMessageTypeList();


	@BeforeEach
	public <M> void before() {
		MockitoAnnotations.initMocks(this);

		Assertions.assertTrue(this.swiftServerMessageRouter instanceof ProwideSwiftServerMessageRouterImpl);
		Assertions.assertTrue(this.swiftIdentifierHandler instanceof SwiftIdentifierHandlerImpl);

		// manually inject mocks into services.
		((ProwideSwiftServerMessageRouterImpl) this.swiftServerMessageRouter).setSwiftServerMessageService(this.swiftMessageService);

		((SwiftIdentifierHandlerImpl<AbstractMessage>) this.swiftIdentifierHandler).setSwiftServerMessageService(this.swiftMessageService);
		((SwiftIdentifierHandlerImpl<AbstractMessage>) this.swiftIdentifierHandler).setSwiftSourceSystemService(this.swiftSourceSystemService);
		((SwiftIdentifierHandlerImpl<AbstractMessage>) this.swiftIdentifierHandler).setSwiftIdentifierService(this.swiftIdentifierService);

		Mockito.when(this.swiftMessageService.saveSwiftMessage(Mockito.any(SwiftMessage.class))).then(invocation -> invocation.getArguments()[0]);

		Mockito.when(this.swiftMessageService.getSwiftMessageTypeByName(Mockito.anyString())).then(invocation -> {
			String name = (String) invocation.getArguments()[0];
			return this.swiftMessageTypeList
					.stream()
					.filter(n -> Objects.equals(n.getName(), name))
					.findFirst()
					.orElseThrow(RuntimeException::new);
		});

		Mockito.when(this.swiftMessageService.getSwiftMessageTypeList(Mockito.any(SwiftMessageTypeSearchForm.class))).then(invocation -> {
			SwiftMessageTypeSearchForm swiftMessageTypeSearchForm = (SwiftMessageTypeSearchForm) invocation.getArguments()[0];
			if (StringUtils.isEmpty(swiftMessageTypeSearchForm.getName())) {
				return this.swiftMessageTypeList
						.stream()
						.filter(n -> Objects.equals(n.getName(), swiftMessageTypeSearchForm.getName()))
						.findFirst()
						.orElseThrow(RuntimeException::new);
			}
			if (StringUtils.isEmpty(swiftMessageTypeSearchForm.getMtVersionName())) {
				return this.swiftMessageTypeList
						.stream()
						.filter(n -> Objects.equals(n.getMtVersionName(), swiftMessageTypeSearchForm.getMtVersionName()))
						.findFirst()
						.orElseThrow(RuntimeException::new);
			}
			throw new RuntimeException("Type not found" + swiftMessageTypeSearchForm);
		});

		Mockito.when(this.swiftMessageService.getSwiftMessageTypeVersionCode(Mockito.anyString())).then(invocation -> {
			String versionName = (String) invocation.getArguments()[0];
			return this.swiftMessageTypeList
					.stream()
					.filter(n -> Objects.equals(n.getMtVersionName(), versionName))
					.findFirst()
					.orElse(null);
		});

		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 1);
		swiftSourceSystem.setDescription("The main IMS system");
		swiftSourceSystem.setName("IMS");
		swiftSourceSystem.setBusinessIdentifierCode("PPSCUS60");
		Mockito.when(this.swiftSourceSystemService.getSwiftSourceSystemByBIC("PPSCUS60")).thenReturn(swiftSourceSystem);

		ProwideSwiftServerMessageRouterImpl prowideSwiftServerMessageRouter = (ProwideSwiftServerMessageRouterImpl) this.swiftServerMessageRouter;
		SwiftIncomingOperationalMessageHandler swiftIncomingOperationalMessageHandler = prowideSwiftServerMessageRouter.getSwiftIncomingOperationalMessageHandler();
		Assertions.assertNotNull(swiftIncomingOperationalMessageHandler);
		MockingDetails ackHandlerMockingDetails = Mockito.mockingDetails(swiftIncomingOperationalMessageHandler);
		SwiftIncomingSystemMessageHandler swiftIncomingSystemMessageHandler = prowideSwiftServerMessageRouter.getSwiftIncomingSystemMessageHandler();
		Assertions.assertNotNull(swiftIncomingSystemMessageHandler);
		if (!ackHandlerMockingDetails.isSpy()) {
			swiftIncomingOperationalMessageHandler = Mockito.spy(swiftIncomingOperationalMessageHandler);
			prowideSwiftServerMessageRouter.setSwiftIncomingOperationalMessageHandler(swiftIncomingOperationalMessageHandler);
			swiftIncomingOperationalMessageHandler.setSwiftServerStatusHandler(Mockito.spy(swiftIncomingOperationalMessageHandler.getSwiftServerStatusHandler()));

			swiftIncomingSystemMessageHandler = Mockito.spy(swiftIncomingSystemMessageHandler);
			prowideSwiftServerMessageRouter.setSwiftIncomingSystemMessageHandler(swiftIncomingSystemMessageHandler);
		}
		else {
			Mockito.reset(swiftIncomingOperationalMessageHandler.getSwiftServerStatusHandler(), swiftIncomingOperationalMessageHandler, swiftIncomingSystemMessageHandler);
		}
	}


	@Test
	public void receiveAckUnknownHandler() {
		AbstractMessage message = this.utilService.parseSingleMessage(SWIFT_UNKNOWN_298, SwiftMessageFormats.MT);
		Assertions.assertNotNull(message);
		SwiftServerMessageRouterCommand<AbstractMessage> command = SwiftServerMessageRouterCommand.ofMessageAndString(message, SWIFT_UNKNOWN_298);
		Assertions.assertThrows(RuntimeException.class, () -> this.swiftServerMessageRouter.receive(command));
	}


	@SuppressWarnings("unchecked")
	@Test
	public void receiveAck202Handler() {
		SwiftIdentifier swiftIdentifier01 = new SwiftIdentifier();
		swiftIdentifier01.setId((long) 1);
		swiftIdentifier01.setUniqueStringIdentifier("IMSCOL4598301");
		swiftIdentifier01.setSourceSystemFkFieldId((long) 1);
		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 1);
		swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);
		swiftIdentifier01.setSourceSystemModule(swiftSourceSystemModule);

		AbstractMessage message = this.utilService.parseSingleMessage(SWIFT_ACK_202, SwiftMessageFormats.MT);
		Assertions.assertNotNull(message);
		SwiftServerMessageRouterCommand<AbstractMessage> command = SwiftServerMessageRouterCommand.ofMessageAndString(message, SWIFT_ACK_202);

		// return the identifier
		Mockito.when(this.swiftIdentifierService.getSwiftIdentifierByUniqueId("IMSCOL4598301", swiftSourceSystem.getId(), null)).thenReturn(swiftIdentifier01);

		// test method.
		this.swiftServerMessageRouter.receive(command);

		// the swift message must be saved.
		ArgumentCaptor<SwiftMessage> swiftMessageArgumentCaptor = ArgumentCaptor.forClass(SwiftMessage.class);
		Mockito.verify(this.swiftMessageService, Mockito.times(1)).saveSwiftMessage(swiftMessageArgumentCaptor.capture());
		MatcherAssert.assertThat(swiftMessageArgumentCaptor.getAllValues().get(0).getType().getName(), IsEqual.equalTo("General Financial Institution Transfer Message"));

		ProwideSwiftServerMessageRouterImpl prowideSwiftServerMessageRouter = (ProwideSwiftServerMessageRouterImpl) this.swiftServerMessageRouter;
		// the operational message handler must be called once.
		@SuppressWarnings({"rawtypes"})
		ArgumentCaptor<SwiftServerMessageRouterCommand> swiftServerMessageRouterCommandArgumentCaptor = ArgumentCaptor.forClass(SwiftServerMessageRouterCommand.class);
		Mockito.verify(prowideSwiftServerMessageRouter.getSwiftIncomingOperationalMessageHandler(), Mockito.times(1))
				.handle(Mockito.any(SwiftMessage.class), swiftServerMessageRouterCommandArgumentCaptor.capture());
		Assertions.assertFalse(swiftServerMessageRouterCommandArgumentCaptor.getAllValues().get(0).getSwiftIdentifierList().isEmpty());
		MatcherAssert.assertThat(swiftServerMessageRouterCommandArgumentCaptor.getAllValues().get(0).getMessageString(), IsEqual.equalTo(SWIFT_ACK_202));

		// must put to asynchronous queue
		Mockito.verify(prowideSwiftServerMessageRouter.getSwiftIncomingOperationalMessageHandler().getSwiftServerStatusHandler(), Mockito.times(1))
				.sendSwiftExportStatus(Mockito.anyString(), Mockito.isNull(), Mockito.isNull(), Mockito.eq(InstructionStatuses.PROCESSED), Mockito.isNull());
		SwiftServerStatusHandlerImpl ssshi = (SwiftServerStatusHandlerImpl) prowideSwiftServerMessageRouter.getSwiftIncomingOperationalMessageHandler().getSwiftServerStatusHandler();
		Mockito.verify(ssshi.getSwiftServerAsynchronousMessageHandler(), Mockito.times(1)).send(Mockito.any(InstructionMessagingStatusMessage.class));
	}


	@SuppressWarnings("unchecked")
	@Test
	public void receiveAck094Handler() {
		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 1);
		swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);

		AbstractMessage message = this.utilService.parseSingleMessage(SWIFT_ACK_094, SwiftMessageFormats.MT);
		Assertions.assertNotNull(message);
		SwiftServerMessageRouterCommand<AbstractMessage> command = SwiftServerMessageRouterCommand.ofMessageAndString(message, SWIFT_ACK_094);

		// test method.
		this.swiftServerMessageRouter.receive(command);

		// the swift message must be saved.
		ArgumentCaptor<SwiftMessage> swiftMessageArgumentCaptor = ArgumentCaptor.forClass(SwiftMessage.class);
		Mockito.verify(this.swiftMessageService, Mockito.times(1)).saveSwiftMessage(swiftMessageArgumentCaptor.capture());
		MatcherAssert.assertThat(swiftMessageArgumentCaptor.getAllValues().get(0).getType().getName(), IsEqual.equalTo("Broadcast"));

		ProwideSwiftServerMessageRouterImpl prowideSwiftServerMessageRouter = (ProwideSwiftServerMessageRouterImpl) this.swiftServerMessageRouter;
		// the system message handler must be called once.
		@SuppressWarnings({"rawtypes"})
		ArgumentCaptor<SwiftServerMessageRouterCommand> swiftServerMessageRouterCommandArgumentCaptor = ArgumentCaptor.forClass(SwiftServerMessageRouterCommand.class);
		Mockito.verify(prowideSwiftServerMessageRouter.getSwiftIncomingSystemMessageHandler(), Mockito.times(1))
				.handle(Mockito.any(SwiftMessage.class), swiftServerMessageRouterCommandArgumentCaptor.capture());
		Assertions.assertTrue(swiftServerMessageRouterCommandArgumentCaptor.getAllValues().get(0).getSwiftIdentifierList().isEmpty());
		MatcherAssert.assertThat(swiftServerMessageRouterCommandArgumentCaptor.getAllValues().get(0).getMessageString(), IsEqual.equalTo(SWIFT_ACK_094));
	}
}
