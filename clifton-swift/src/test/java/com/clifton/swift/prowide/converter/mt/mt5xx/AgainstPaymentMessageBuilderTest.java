package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt5xx.MT541;
import com.prowidesoftware.swift.model.mt.mt5xx.MT543;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Test;

import java.io.IOException;


public class AgainstPaymentMessageBuilderTest {

	private static final String EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES = "{1:F01PNBPGB2DAXXX0000000000}{2:I543NWNBSGS1XXXXN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//transactionReferenceNumber\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":94B::TRAD//EXCH/USD\r\n" +
			":98A::SETT//20170803\r\n" +
			":98A::TRAD//20170804\r\n" +
			":90B::DEAL//ACTU/USD150,31\r\n" +
			":35B:ISIN 122333444\r\n" +
			":35B:/TS/STATE_STREET_FUND_NUMBER\r\n" +
			"Security Description\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/CS\r\n" +
			":11A::DENO//USD\r\n" +
			":36B::SIZE//UNIT/112,19\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//UNIT/999,111\r\n" +
			":97A::SAFE//custodyAccountNumber\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//DTCYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::REAG/DTCYID/123\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::BUYR/DTCYID/456\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD258,369\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD129,352\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::EXEC//USD5,77\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::REGF//USD3,444\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES_CANCEL = "{1:F01PNBPGB2DAXXX0000000000}{2:I543NWNBSGS1XXXXN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//transactionReferenceNumber\r\n" +
			":23G:CANC\r\n" +
			":16R:LINK\r\n" +
			":20C::PREV//123\r\n" +
			":16S:LINK\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":94B::TRAD//EXCH/USD\r\n" +
			":98A::SETT//20170803\r\n" +
			":98A::TRAD//20170804\r\n" +
			":90B::DEAL//ACTU/USD150,31\r\n" +
			":35B:ISIN 122333444\r\n" +
			":35B:/TS/STATE_STREET_FUND_NUMBER\r\n" +
			"Security Description\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/CS\r\n" +
			":11A::DENO//USD\r\n" +
			":36B::SIZE//UNIT/112,19\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//UNIT/999,111\r\n" +
			":97A::SAFE//custodyAccountNumber\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//DTCYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::REAG/DTCYID/123\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::BUYR/DTCYID/456\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD258,369\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD129,352\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::EXEC//USD5,77\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::REGF//USD3,444\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_RECEIEVE_AGAINST_PAYMENT_FUTURE = "{1:F01PNBPGB2DAXXX0000000000}{2:I543NWNBSGS1XXXXN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//transactionReferenceNumber\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":94B::TRAD//EXCH/USD\r\n" +
			":98A::SETT//20170803\r\n" +
			":98A::TRAD//20170804\r\n" +
			":90B::DEAL//DISC/USD555,666\r\n" +
			":35B:/TS/securitySymbol\r\n" +
			"securityDescription\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/FUT\r\n" +
			":11A::DENO//USD\r\n" +
			":98A::EXPI//20170802\r\n" +
			":36B::SIZE//UNIT/777,888\r\n" +
			":16S:FIA\r\n" +
			":22F::PROC//OPEP\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//UNIT/999,111\r\n" +
			":97A::SAFE//custodyAccountNumber\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95Q::PSET//XX\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::REAG//PNBPCNSH\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::BUYR//PNBPDEFF\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD333,444\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD0,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::EXEC//USD333,444\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_DELIVER_AGAINST_PAYMENT_EQUITIES = "{1:F01PPSCUS66AXXX0000000000}{2:I541IRVTUS3NXIBKN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//IMS762578T??5EIMS951169TF\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":98A::SETT//20171109\r\n" +
			":98A::TRAD//20171107\r\n" +
			":90B::DEAL//ACTU/USD59,89336735\r\n" +
			":35B:ISIN US78464A6727\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/ETF\r\n" +
			":11A::DENO//USD\r\n" +
			":36B::SIZE//UNIT/1,\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//UNIT/1960,\r\n" +
			":97A::SAFE//943729\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//DTCYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::SELL/DTCYID/0501\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD117410,6\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD117391,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::EXEC//USD19,6\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::REGF//USD0,\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_DELIVER_AGAINST_PAYMENT_FUTURES = "{1:F01PPSCUS66AXXX0000000000}{2:I541SBOSUS3UXIMSN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//75000\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":94B::TRAD//EXCH/XCME\r\n" +
			":98A::SETT//20171018\r\n" +
			":98A::TRAD//20171018\r\n" +
			":90B::DEAL//ACTU/USD2559,5\r\n" +
			":35B:/TS/ESZ7\r\n" +
			"SP500 EMINI FUT  Dec17\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/FUT\r\n" +
			":11A::DENO//USD\r\n" +
			":98A::EXPI//20171215\r\n" +
			":36B::SIZE//UNIT/50,\r\n" +
			":16S:FIA\r\n" +
			":22F::PROC//OPEP\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//UNIT/34,\r\n" +
			":97A::SAFE//16VX\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95Q::PSET//XX\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::DEAG//MSNYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::SELL//MSNYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD74,46\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD0,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::EXEC//USD74,46\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_DELIVER_AGAINST_PAYMENT_TIPS = "{1:F01PPSCUS66AXXX0000000000}{2:I541MELNUS3PXGLBN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//456\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":98A::SETT//20171201\r\n" +
			":98A::TRAD//20171130\r\n" +
			":90A::DEAL//PRCT/99,8125\r\n" +
			":35B:ISIN US912810RW09\r\n" +
			":35B:/TS/912810RW0\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/TIPS\r\n" +
			":11A::DENO//USD\r\n" +
			":98A::MATU//20470215\r\n" +
			":98A::ISSU//20170215\r\n" +
			":92A::INTR//0,875\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//AMOR/9345467,2\r\n" +
			":36B::SETT//FAMT/9140000,\r\n" +
			":97A::SAFE//SWBF0909002\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//FRNYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::SELL/DTCYID/7256\r\n" +
			":97A::SAFE//safekeeping\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD9351943,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD9327944,45\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::ACRU//USD23998,55\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_DELIVER_AGAINST_OPTIONS = "{1:F01PPSCUS66AXXX0000000000}{2:I541IRVTUS3NXIBKN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//678\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":94B::TRAD//EXCH/XCBO\r\n" +
			":98A::SETT//20180117\r\n" +
			":98A::TRAD//20180116\r\n" +
			":90B::DEAL//ACTU/USD10,7\r\n" +
			":35B:/TS/SPXW US 02/16/18 P2650\r\n" +
			"SPXW US 02/16/18 P2650\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/OPT\r\n" +
			":12B::OPST//EURO\r\n" +
			":12B::OPTI//PUTO\r\n" +
			":11A::DENO//USD\r\n" +
			":98A::EXPI//20180216\r\n" +
			":90B::EXER//ACTU/USD2650,\r\n" +
			":36B::SIZE//UNIT/100,\r\n" +
			":16S:FIA\r\n" +
			":22F::PROC//OPEP\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//UNIT/6,\r\n" +
			":97A::SAFE//927801\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95Q::PSET//XX\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::DEAG/DTCYID/0418\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::SELL/DTCYID/0418\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD0,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD6420,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::EXEC//USD0,\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_DELIVER_AGAINST_PAYMENT_REPO = "{1:F01PNBPGB2DAXXX0000000000}{2:I541NWNBSGS1XXXXN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//transactionReferenceNumber\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":98A::SETT//20160929\r\n" +
			":98A::TRAD//20160929\r\n" +
			":90A::DEAL//PRCT/100,5\r\n" +
			":35B:ISIN ISIN Number\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/RVRP\r\n" +
			":11A::DENO//USD\r\n" +
			":98A::MATU//20360625\r\n" +
			":98A::ISSU//20060525\r\n" +
			":92A::INTR//2,35969\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//FAMT/9400000,\r\n" +
			":97A::SAFE//179896\r\n" +
			":16S:FIAC\r\n" +
			":16R:REPO\r\n" +
			":98A::TERM//20150929\r\n" +
			":22F::RERT//FIXE\r\n" +
			":92A::REPO//3333,33\r\n" +
			":92A::SHAI//0,587\r\n" +
			":99B::TOCO//001\r\n" +
			":19A::TRTE//USD13779827,16\r\n" +
			":19A::ACRU//USD13779999,16\r\n" +
			":16S:REPO\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//RVPO\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//IRVTUS3NIBK\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::DEAG/USFW/021000018\r\n" +
			":97A::SAFE//123456\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::SELL/DTCYID/901\r\n" +
			":97A::SAFE//1\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD14056032,23\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD1135449,51\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";


	@Test
	public void testReceiveAgainstEquities() throws IOException {
		MT543 abstractMt = (MT543) AbstractMT.parse(EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES);
		ReceiveAgainstPaymentMessageConverter receiveAgainstPaymentMessageConverter = new ReceiveAgainstPaymentMessageConverter();
		ReceiveAgainstPayment receiveAgainstPayment = receiveAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = receiveAgainstPaymentMessageConverter.toSwiftMessage(receiveAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES));
	}


	@Test
	public void testReceiveAgainstEquitiesCancel() throws IOException {
		MT543 abstractMt = (MT543) AbstractMT.parse(EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES_CANCEL);
		ReceiveAgainstPaymentMessageConverter receiveAgainstPaymentMessageConverter = new ReceiveAgainstPaymentMessageConverter();
		ReceiveAgainstPayment receiveAgainstPayment = receiveAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = receiveAgainstPaymentMessageConverter.toSwiftMessage(receiveAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_RECEIVE_AGAINST_PAYMENT_EQUITIES_CANCEL));
	}


	@Test
	public void testReceiveAgainstFutures() throws IOException {
		MT543 abstractMt = (MT543) AbstractMT.parse(EXPECTED_RECEIEVE_AGAINST_PAYMENT_FUTURE);
		ReceiveAgainstPaymentMessageConverter receiveAgainstPaymentMessageConverter = new ReceiveAgainstPaymentMessageConverter();
		ReceiveAgainstPayment receiveAgainstPayment = receiveAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = receiveAgainstPaymentMessageConverter.toSwiftMessage(receiveAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_RECEIEVE_AGAINST_PAYMENT_FUTURE));
	}


	@Test
	public void testReceiveAgainstRepo() throws IOException {
		MT541 abstractMt = (MT541) AbstractMT.parse(EXPECTED_DELIVER_AGAINST_PAYMENT_REPO);
		DeliverAgainstPaymentMessageConverter deliverAgainstPaymentMessageConverter = new DeliverAgainstPaymentMessageConverter();
		DeliverAgainstPayment deliverAgainstPayment = deliverAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = deliverAgainstPaymentMessageConverter.toSwiftMessage(deliverAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_DELIVER_AGAINST_PAYMENT_REPO));
	}


	@Test
	public void testDeliverAgainstEquities() throws IOException {
		MT541 abstractMt = (MT541) AbstractMT.parse(EXPECTED_DELIVER_AGAINST_PAYMENT_EQUITIES);
		DeliverAgainstPaymentMessageConverter deliverAgainstPaymentMessageConverter = new DeliverAgainstPaymentMessageConverter();
		DeliverAgainstPayment deliverAgainstPayment = deliverAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = deliverAgainstPaymentMessageConverter.toSwiftMessage(deliverAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_DELIVER_AGAINST_PAYMENT_EQUITIES));
	}


	@Test
	public void testDeliverAgainstFutures() throws IOException {
		MT541 abstractMt = (MT541) AbstractMT.parse(EXPECTED_DELIVER_AGAINST_PAYMENT_FUTURES);
		DeliverAgainstPaymentMessageConverter deliverAgainstPaymentMessageConverter = new DeliverAgainstPaymentMessageConverter();
		DeliverAgainstPayment deliverAgainstPayment = deliverAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = deliverAgainstPaymentMessageConverter.toSwiftMessage(deliverAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_DELIVER_AGAINST_PAYMENT_FUTURES));
	}


	@Test
	public void testDeliverAgainstTips() throws IOException {
		MT541 abstractMt = (MT541) AbstractMT.parse(EXPECTED_DELIVER_AGAINST_PAYMENT_TIPS);
		DeliverAgainstPaymentMessageConverter deliverAgainstPaymentMessageConverter = new DeliverAgainstPaymentMessageConverter();
		DeliverAgainstPayment deliverAgainstPayment = deliverAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = deliverAgainstPaymentMessageConverter.toSwiftMessage(deliverAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_DELIVER_AGAINST_PAYMENT_TIPS));
	}


	@Test
	public void testDeliverAgainstOptions() throws IOException {
		MT541 abstractMt = (MT541) AbstractMT.parse(EXPECTED_DELIVER_AGAINST_OPTIONS);
		DeliverAgainstPaymentMessageConverter deliverAgainstPaymentMessageConverter = new DeliverAgainstPaymentMessageConverter();
		DeliverAgainstPayment deliverAgainstPayment = deliverAgainstPaymentMessageConverter.fromSwiftEntity(abstractMt);
		String roundTripMessage = deliverAgainstPaymentMessageConverter.toSwiftMessage(deliverAgainstPayment);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_DELIVER_AGAINST_OPTIONS));
	}
}
