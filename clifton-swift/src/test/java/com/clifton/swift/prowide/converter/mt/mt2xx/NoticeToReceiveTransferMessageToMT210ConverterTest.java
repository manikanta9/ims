package com.clifton.swift.prowide.converter.mt.mt2xx;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.transfers.NoticeToReceiveTransferMessage;
import com.clifton.instruction.messages.transfers.TransferCancellationMessage;
import com.clifton.instruction.messages.transfers.TransferMessagePurposes;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterLocator;
import com.clifton.swift.prowide.converter.mt.ConversionMapping;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mt.mt2xx.MT210;
import com.prowidesoftware.swift.model.mt.mt2xx.MT292;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


public class NoticeToReceiveTransferMessageToMT210ConverterTest {

	private static final String EXPECTED_NOTICE_TO_RECEIVE = "{1:F01PNBPGB2DAXXX0000000000}{2:I210NWNBSGS1XXXXN}{4:\r\n:20:transactionReferenceNumber\r\n:25:receiverAccountNumber\r\n:30:170803\r\n:21:MARG\r\n:32B:USD123,456\r\n:52A:MEORUS61\r\n:56A:UNVKCY2N\r\n-}";

	private static final String EXPECTED_NOTICE_TO_RECEIVE_CANCELLATION = "{1:F01PNBPGB2DAXXX0000000000}{2:I292NWNBSGS1XXXXN}{4:\r\n" +
			":20:transactionReferenceNumber\r\n" +
			":21:123\r\n" +
			":11S:210\r\n" +
			"170803\r\n" +
			":20:123\r\n" +
			":30:170803\r\n" +
			":21:MARG\r\n" +
			":32B:USD123,456\r\n" +
			"-}";


	private List<MT210ConversionMapping> expectedValueList = Arrays.asList(
			new MT210ConversionMapping("transactionReferenceNumber", "transactionReferenceNumber", (m) -> m.getField20().getValue()),
			new MT210ConversionMapping("senderBIC", "PNBPGB2D", (m) -> m.getSender().substring(0, 8)),
			new MT210ConversionMapping("receiverBIC", "NWNBSGS1", (m) -> m.getReceiver().substring(0, 8)),
			new MT210ConversionMapping("messagePurpose", TransferMessagePurposes.DAILY_MARGIN, (m) -> null),
			new MT210ConversionMapping("currency", "USD", (m) -> m.getField32B().get(0).getComponent1()),
			new MT210ConversionMapping("valueDate", DateUtils.asUtilDate(LocalDate.of(2017, 8, 3)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getField30().getValue(), DateTimeFormatter.ofPattern("yyMMdd")))),
			new MT210ConversionMapping("amount", new BigDecimal("123.456"), (m) -> m.getField32B().get(0).getComponent2AsNumber()),
			new MT210ConversionMapping("receiverAccountNumber", "receiverAccountNumber", (m) -> m.getField25().getAccount()),
			new MT210ConversionMapping("orderingBIC", "MEORUS61", (m) -> m.getField52A().get(0).getBIC()),
			new MT210ConversionMapping("intermediaryBIC", "UNVKCY2N", (m) -> m.getField56A().get(0).getBIC()),
			new MT210ConversionMapping("messageReferenceNumber", null, (m) -> null)
	);

	private List<MT292ConversionMapping> expectedCancelledValueList = Arrays.asList(
			new MT292ConversionMapping("transactionReferenceNumber", "transactionReferenceNumber", (m) -> m.getField20().getValue()),
			new MT292ConversionMapping("messageReferenceNumber", null, (m) -> Long.parseLong(m.getField21().getValue())),
			new MT292ConversionMapping("senderBIC", "PNBPGB2D", (m) -> m.getSender().substring(0, 8)),
			new MT292ConversionMapping("receiverBIC", "NWNBSGS1", (m) -> m.getReceiver().substring(0, 8)),
			new MT292ConversionMapping("valueDate", DateUtils.asUtilDate(LocalDate.of(2017, 8, 3)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getField11S().getDate(), DateTimeFormatter.ofPattern("yyMMdd"))))
	);


	@Test
	public void testToSwiftEntity() {
		NoticeToReceiveTransferMessage noticeToReceiveTransferMessage = new NoticeToReceiveTransferMessage();
		Map<String, Object> expectedValueMap = new HashMap<>();
		this.expectedValueList.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		expectedValueMap.forEach((key, value) -> BeanUtils.setPropertyValue(noticeToReceiveTransferMessage, key, value));

		Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(noticeToReceiveTransferMessage);
		MapDifference<String, Object> differences = Maps.difference(expectedValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "All properties should have been set by reflection " + differences.toString());

		List<String> allPropertyNames = BeanUtils.getPropertyNames(NoticeToReceiveTransferMessage.class, true, true);
		allPropertyNames.removeAll(valueMap.keySet());
		Assertions.assertTrue(allPropertyNames.isEmpty(), "Should have all available properties set" + allPropertyNames);

		NoticeToReceiveTransferMessageToMT210Converter converter = new NoticeToReceiveTransferMessageToMT210Converter();
		String messageString = converter.toSwiftMessage(noticeToReceiveTransferMessage);
		MatcherAssert.assertThat(messageString, IsEqual.equalTo(EXPECTED_NOTICE_TO_RECEIVE));

		final MT210 mt210 = converter.toSwiftEntity(noticeToReceiveTransferMessage);
		Map<String, Object> mtValueMap = this.expectedValueList.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt210)), CollectionUtils.throwingCollectCombiner());
		List<String> expectedDifferences = Collections.singletonList("messagePurpose");
		final MapDifference<String, Object> allDifferences = Maps.difference(mtValueMap, valueMap);
		expectedDifferences.forEach(k -> Assertions.assertTrue(allDifferences.entriesDiffering().containsKey(k), "Expected difference for property " + k));
		expectedDifferences.forEach(k -> {
			valueMap.remove(k);
			mtValueMap.remove(k);
		});
		differences = Maps.difference(mtValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "MT message values are not the same as NoticeToReceive values " + differences.toString());
	}


	@Test
	public <M extends AbstractMT> void testToSwiftEntityCancellation() {
		List<ConversionMapping<MT210>> cancellationOverrides = this.expectedValueList.stream()
				.map(mapping -> StringUtils.isEqual(mapping.getProperty(), "messageReferenceNumber") ? new ConversionMapping<MT210>("messageReferenceNumber", 123L, (m) -> Long.parseLong(m.getField20().getValue())
				) : mapping)
				.collect(Collectors.toList());
		NoticeToReceiveTransferMessage noticeToReceiveTransferMessage = new NoticeToReceiveTransferMessage();
		Map<String, Object> expectedValueMap = new HashMap<>();
		this.expectedValueList.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		cancellationOverrides.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		expectedValueMap.forEach((key, value) -> BeanUtils.setPropertyValue(noticeToReceiveTransferMessage, key, value));

		Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(noticeToReceiveTransferMessage);
		MapDifference<String, Object> differences = Maps.difference(expectedValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "All properties should have been set by reflection " + differences.toString());

		List<String> allPropertyNames = BeanUtils.getPropertyNames(NoticeToReceiveTransferMessage.class, true, true);
		allPropertyNames.removeAll(valueMap.keySet());
		Assertions.assertTrue(allPropertyNames.isEmpty(), "Should have all available properties set" + allPropertyNames);

		NoticeToReceiveTransferMessageToMT210Converter transferMessageToMT210Converter = new NoticeToReceiveTransferMessageToMT210Converter();
		SwiftMessageConverterLocator mockLocator = Mockito.mock(SwiftMessageConverterLocator.class);
		Mockito.when(mockLocator.locate(ArgumentMatchers.any(NoticeToReceiveTransferMessage.class), ArgumentMatchers.same(SwiftMessageFormats.MT)))
				.thenReturn(transferMessageToMT210Converter);
		TransferCancellationMessage transferCancellationMessage = new TransferCancellationMessage(noticeToReceiveTransferMessage);
		FinancialInstitutionTransferMessageToMT292Converter<M> converter = new FinancialInstitutionTransferMessageToMT292Converter<>();
		converter.setSwiftMessageConverterLocator(mockLocator);
		String messageString = converter.toSwiftMessage(transferCancellationMessage);
		MatcherAssert.assertThat(messageString, IsEqual.equalTo(EXPECTED_NOTICE_TO_RECEIVE_CANCELLATION));

		final MT292 mt292 = converter.toSwiftEntity(transferCancellationMessage);
		Map<String, Object> mt292ValueMap = this.expectedCancelledValueList.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt292)), CollectionUtils.throwingCollectCombiner());
		final MT210 mt210 = transferMessageToMT210Converter.toSwiftEntity((NoticeToReceiveTransferMessage) transferCancellationMessage.getOriginalMessage());
		Map<String, Object> mt202ValueMap = this.expectedValueList.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt210)), CollectionUtils.throwingCollectCombiner());
		Map<String, Object> mtValueMap = new HashMap<>();
		mtValueMap.putAll(mt202ValueMap);
		mtValueMap.putAll(mt292ValueMap);
		List<String> expectedDifferences = Collections.singletonList("messagePurpose");
		MapDifference<String, Object> allDifferences = Maps.difference(mtValueMap, valueMap);
		expectedDifferences.forEach(k -> Assertions.assertTrue(allDifferences.entriesDiffering().containsKey(k), "Expected difference for property " + k));
		expectedDifferences.forEach(k -> {
			mtValueMap.remove(k);
			valueMap.remove(k);
		});
		differences = Maps.difference(mtValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "MT message values are not the same as NoticeToReceive values " + differences.toString());
	}


	@Test
	public void testFromSwiftEntity() throws IOException {
		MT210 abstractMt = (MT210) AbstractMT.parse(EXPECTED_NOTICE_TO_RECEIVE);
		NoticeToReceiveTransferMessageToMT210Converter noticeToReceiveTransferMessageToMT210Converter = new NoticeToReceiveTransferMessageToMT210Converter();
		NoticeToReceiveTransferMessage noticeToReceiveTransferMessage = noticeToReceiveTransferMessageToMT210Converter.fromSwiftEntity(abstractMt);
		String roundTripMessage = noticeToReceiveTransferMessageToMT210Converter.toSwiftMessage(noticeToReceiveTransferMessage);
		MatcherAssert.assertThat(roundTripMessage, IsEqual.equalTo(EXPECTED_NOTICE_TO_RECEIVE));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class MT210ConversionMapping extends ConversionMapping<MT210> {

		public MT210ConversionMapping(String property, Object value, Function<MT210, Object> mapper) {
			super(property, value, mapper);
		}
	}

	private static class MT292ConversionMapping extends ConversionMapping<MT292> {

		public MT292ConversionMapping(String property, Object value, Function<MT292, Object> mapper) {
			super(property, value, mapper);
		}
	}
}
