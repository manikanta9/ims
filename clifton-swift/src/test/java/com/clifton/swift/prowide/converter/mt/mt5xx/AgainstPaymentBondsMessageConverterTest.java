package com.clifton.swift.prowide.converter.mt.mt5xx;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;
import com.clifton.swift.prowide.converter.mt.ConversionMapping;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.prowidesoftware.swift.model.field.Field11A;
import com.prowidesoftware.swift.model.field.Field12A;
import com.prowidesoftware.swift.model.field.Field19A;
import com.prowidesoftware.swift.model.field.Field20C;
import com.prowidesoftware.swift.model.field.Field22F;
import com.prowidesoftware.swift.model.field.Field23G;
import com.prowidesoftware.swift.model.field.Field35B;
import com.prowidesoftware.swift.model.field.Field36B;
import com.prowidesoftware.swift.model.field.Field90A;
import com.prowidesoftware.swift.model.field.Field92A;
import com.prowidesoftware.swift.model.field.Field95P;
import com.prowidesoftware.swift.model.field.Field95R;
import com.prowidesoftware.swift.model.field.Field97A;
import com.prowidesoftware.swift.model.field.Field98A;
import com.prowidesoftware.swift.model.mt.mt5xx.MT541;
import com.prowidesoftware.swift.model.mt.mt5xx.MT543;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class AgainstPaymentBondsMessageConverterTest {

	// :20C::SEME//IMS1II??5EIMS852433T??5EIMS1053022TF
	private static final String EXPECTED_RECEIVE_AGAINST = "{1:F01PNBPGB2DAXXX0000000000}{2:I543NWNBSGS1XXXXN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//transactionReferenceNumber\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":98A::SETT//20160929\r\n" +
			":98A::TRAD//20160929\r\n" +
			":90A::DEAL//PRCT/100,5\r\n" +
			":35B:ISIN US31395DUC19\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/TIPS\r\n" +
			":11A::DENO//USD\r\n" +
			":98A::MATU//20360625\r\n" +
			":98A::ISSU//20060525\r\n" +
			":92A::INTR//2,35969\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//AMOR/1129800,51\r\n" +
			":36B::SETT//FAMT/20500000,\r\n" +
			":97A::SAFE//NDLG07\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//FRNYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::REAG/USFW/021000018\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::BUYR/DTCYID/2095\r\n" +
			":97A::SAFE//1\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD1136116,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD1135449,51\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::ACRU//USD666,49\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";

	private static final String EXPECTED_DELIVER_AGAINST_OPTIONS = "{1:F01PNBPGB2DAXXX0000000000}{2:I541NWNBSGS1XXXXN}{4:\r\n" +
			":16R:GENL\r\n" +
			":20C::SEME//transactionReferenceNumber\r\n" +
			":23G:NEWM\r\n" +
			":16S:GENL\r\n" +
			":16R:TRADDET\r\n" +
			":98A::SETT//20160929\r\n" +
			":98A::TRAD//20160929\r\n" +
			":90A::DEAL//PRCT/100,5\r\n" +
			":35B:ISIN US31395DUC19\r\n" +
			":16R:FIA\r\n" +
			":12A::CLAS/ISIT/TIPS\r\n" +
			":11A::DENO//USD\r\n" +
			":98A::MATU//20360625\r\n" +
			":98A::ISSU//20060525\r\n" +
			":92A::INTR//2,35969\r\n" +
			":16S:FIA\r\n" +
			":16S:TRADDET\r\n" +
			":16R:FIAC\r\n" +
			":36B::SETT//AMOR/1129800,51\r\n" +
			":36B::SETT//FAMT/20500000,\r\n" +
			":97A::SAFE//NDLG07\r\n" +
			":16S:FIAC\r\n" +
			":16R:SETDET\r\n" +
			":22F::SETR//TRAD\r\n" +
			":16R:SETPRTY\r\n" +
			":95P::PSET//FRNYUS33\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::DEAG/USFW/021000018\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:SETPRTY\r\n" +
			":95R::SELL/DTCYID/2095\r\n" +
			":97A::SAFE//1\r\n" +
			":16S:SETPRTY\r\n" +
			":16R:AMT\r\n" +
			":19A::SETT//USD1136116,\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::DEAL//USD1135449,51\r\n" +
			":16S:AMT\r\n" +
			":16R:AMT\r\n" +
			":19A::ACRU//USD666,49\r\n" +
			":16S:AMT\r\n" +
			":16S:SETDET\r\n" +
			"-}";


	private List<ConversionMapping<MT543>> expectedValueList = Arrays.asList(
			new ConversionMapping<MT543>("buy", false, (m) -> null),
			new ConversionMapping<MT543>("accountingNotional", null, (m) -> null),
			new ConversionMapping<MT543>("commissionAmount", null, (m) -> null),
			new ConversionMapping<MT543>("custodyAccountNumber", "NDLG07", (m) -> m.getSequenceC().getFieldByName(Field97A.NAME).getComponent(2)),
			new ConversionMapping<MT543>("custodyBIC", "PNBPCATT", (m) -> null),
			new ConversionMapping<MT543>("isitcCode", ISITCCodes.TIPS, (m) -> ISITCCodes.valueOf(m.getSequenceB1().getFieldByName(Field12A.NAME).getComponent(3))),
			new ConversionMapping<MT543>("open", null, (m) -> null),
			new ConversionMapping<MT543>("price", new BigDecimal("100.5"), (m) -> m.getSequenceB().getFieldByName(Field90A.NAME).getComponentAsNumber(4)),
			new ConversionMapping<MT543>("priceMultiplier", null, (m) -> null),
			new ConversionMapping<MT543>("priceType", TradeMessagePriceTypes.PERCENTAGE, (m) ->
					Stream.of(m.getSequenceB().getFieldByName(Field90A.NAME).getComponent(2)).map(type ->
							Arrays.stream(TradeMessagePriceTypes.values()).filter(e ->
									StringUtils.isEqual(type, e.getCode())).findFirst().orElse(null)).findFirst().orElse(null)
			),
			new ConversionMapping<MT543>("quantity", null, (m) -> null),
			new ConversionMapping<MT543>("receiverBIC", "NWNBSGS1", (m) -> m.getReceiver().substring(0, 8)),
			new ConversionMapping<MT543>("securityCurrency", "USD", (m) -> m.getSequenceB1().getFieldByName(Field11A.NAME).getComponent(2)),
			new ConversionMapping<MT543>("securityDescription", null, (m) -> null),
			new ConversionMapping<MT543>("securityExchangeCode", null, (m) -> null),
			new ConversionMapping<MT543>("securityExpirationDate", null, (m) -> null),
			new ConversionMapping<MT543>("securitySymbol", null, (m) -> null),
			new ConversionMapping<MT543>("senderBIC", "PNBPGB2D", (m) -> m.getSender().substring(0, 8)),
			new ConversionMapping<MT543>("settlementDate", DateUtils.asUtilDate(LocalDate.of(2016, 9, 29)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getSequenceB().getFieldsByName(Field98A.NAME)[0].getComponent(2), DateTimeFormatter.BASIC_ISO_DATE))),
			new ConversionMapping<MT543>("tradeDate", DateUtils.asUtilDate(LocalDate.of(2016, 9, 29)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getSequenceB().getFieldsByName(Field98A.NAME)[1].getComponent(2), DateTimeFormatter.BASIC_ISO_DATE))),
			new ConversionMapping<MT543>("messageFunctions", MessageFunctions.NEW_MESSAGE, m ->
					Stream.of(m.getSequenceA().getFieldByName(Field23G.NAME).getComponent(1)).map(function ->
							Arrays.stream(MessageFunctions.values()).filter(e ->
									StringUtils.isEqual(function, e.getCode())).findFirst().orElse(null)).findFirst().orElse(null)
			),
			new ConversionMapping<MT543>("transactionReferenceNumber", "transactionReferenceNumber", (m) -> m.getSequenceA().getFieldByName(Field20C.NAME).getComponent(2)),
			new ConversionMapping<MT543>("maturityDate", DateUtils.asUtilDate(LocalDate.of(2036, 6, 25)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getSequenceB1().getFieldsByName(Field98A.NAME)[0].getComponent(2), DateTimeFormatter.BASIC_ISO_DATE))),
			new ConversionMapping<MT543>("issueDate", DateUtils.asUtilDate(LocalDate.of(2006, 5, 25)), (m) -> DateUtils.asUtilDate(LocalDate.parse(m.getSequenceB1().getFieldsByName(Field98A.NAME)[1].getComponent(2), DateTimeFormatter.BASIC_ISO_DATE))),
			new ConversionMapping<MT543>("currentFactor", null, m -> null),
			new ConversionMapping<MT543>("interestRate", new BigDecimal("2.35969"), m -> m.getSequenceB1().getFieldByName(Field92A.NAME).getComponentAsNumber(3)),
			new ConversionMapping<MT543>("isin", "US31395DUC19", (m) -> m.getSequenceB().getFieldsByName(Field35B.NAME)[0].getComponent(2)),
			new ConversionMapping<MT543>("accruedInterest", new BigDecimal("666.49"), (m) -> m.getSequenceE3List().get(2).getFieldByName(Field19A.NAME).getComponentAsNumber(4)),
			new ConversionMapping<MT543>("placeOfSettlement", "FRNYUS33", m -> m.getSequenceE1List().get(0).getFieldByName(Field95P.NAME).getComponent(2)),
			new ConversionMapping<MT543>("regulatoryAmount", null, m -> null),
			new ConversionMapping<MT543>("brokerCommissionAmount", null, m -> null),
			new ConversionMapping<MT543>("amortizedFaceAmount", new BigDecimal("1129800.51"), (m) -> m.getSequenceC().getFieldsByName(Field36B.NAME)[0].getComponentAsNumber(3)),
			new ConversionMapping<MT543>("dealAmount", new BigDecimal("1135449.51"), (m) -> m.getSequenceE3List().get(1).getFieldByName(Field19A.NAME).getComponentAsNumber(4)),
			new ConversionMapping<MT543>("netSettlementAmount", new BigDecimal("1136116"), (m) -> m.getSequenceE3List().get(0).getFieldByName(Field19A.NAME).getComponentAsNumber(4)),
			new ConversionMapping<MT543>("originalFaceAmount", new BigDecimal("20500000"), (m) -> m.getSequenceC().getFieldsByName(Field36B.NAME)[1].getComponentAsNumber(3)),
			new ConversionMapping<MT543>("clearingBrokerIdentifier", new PartyIdentifier("021000018", MessagePartyIdentifierTypes.USFW), (m) -> new PartyIdentifier(m.getSequenceE1List().get(1).getFieldByName(Field95R.NAME).getComponent(3), MessagePartyIdentifierTypes.USFW)),
			new ConversionMapping<MT543>("executingBrokerIdentifier", new PartyIdentifier("2095", MessagePartyIdentifierTypes.DTC), (m) -> new PartyIdentifier(m.getSequenceE1List().get(2).getFieldByName(Field95R.NAME).getComponent(3), MessagePartyIdentifierTypes.DTC)),
			new ConversionMapping<MT543>("messageReferenceNumber", null, (m) -> null),
			new ConversionMapping<MT543>("optionStyle", null, (m) -> null),
			new ConversionMapping<MT543>("optionType", null, (m) -> null),
			new ConversionMapping<MT543>("strikePrice", null, (m) -> null),
			new ConversionMapping<MT543>("safekeepingAccount", "1", (m) -> m.getSequenceE1List().get(2).getFieldByName(Field97A.NAME).getComponent(2)),
			new ConversionMapping<MT543>("repurchaseAgreement", false, (m) -> null),
			new ConversionMapping<MT543>("repoClosingDate", null, (m) -> null),
			new ConversionMapping<MT543>("repoNetCashAmount", null, (m) -> null),
			new ConversionMapping<MT543>("repoAccruedInterestAmount", null, (m) -> null),
			new ConversionMapping<MT543>("repoInterestRate", null, (m) -> null),
			new ConversionMapping<MT543>("repoRateType", null, (m) -> null),
			new ConversionMapping<MT543>("repoHaircutPercentage", null, (m) -> null),
			new ConversionMapping<MT543>("clearingSafekeepingAccount", null, (m) -> null),
			new ConversionMapping<MT543>("settlementTransactionType", SettlementTransactionTypes.TRADE, (m) -> SettlementTransactionTypes.findByCode(m.getSequenceE().getFieldByName(Field22F.NAME).getComponent(3))),
			new ConversionMapping<MT543>("exposureType", null, (m) -> null)
	);


	@Test
	public void testReceiveAgainst() {
		ReceiveAgainstPayment receiveAgainstPayment = new ReceiveAgainstPayment();
		Map<String, Object> expectedValueMap = new HashMap<>();
		this.expectedValueList.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		expectedValueMap.forEach((key, value) -> BeanUtils.setPropertyValue(receiveAgainstPayment, key, value));

		Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(receiveAgainstPayment);
		MapDifference<String, Object> differences = Maps.difference(expectedValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "All properties should have been set by reflection " + differences.toString());

		List<String> allPropertyNames = BeanUtils.getPropertyNames(ReceiveAgainstPayment.class, true, true);
		allPropertyNames.removeAll(valueMap.keySet());
		Assertions.assertTrue(allPropertyNames.isEmpty(), "Should have all available properties set" + allPropertyNames);

		ReceiveAgainstPaymentMessageConverter converter = new ReceiveAgainstPaymentMessageConverter();
		String messageString = converter.toSwiftMessage(receiveAgainstPayment);
		MatcherAssert.assertThat(messageString, IsEqual.equalTo(EXPECTED_RECEIVE_AGAINST));

		final MT543 mt543 = converter.toSwiftEntity(receiveAgainstPayment);
		Map<String, Object> mtValueMap = this.expectedValueList.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt543)), CollectionUtils.throwingCollectCombiner());
		List<String> expectedDifferences = Arrays.asList("custodyBIC", "buy", "repurchaseAgreement");
		final MapDifference<String, Object> allDifferences = Maps.difference(mtValueMap, valueMap);
		expectedDifferences.forEach(k -> Assertions.assertTrue(allDifferences.entriesDiffering().containsKey(k), "Expected difference for property " + k));
		expectedDifferences.forEach(k -> {
			valueMap.remove(k);
			mtValueMap.remove(k);
		});
		differences = Maps.difference(mtValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "MT message values are not the same as ReceiveAgainstPayment values " + differences.toString());
	}


	@Test
	public void testDeliverAgainst() {
		List<ConversionMapping<MT543>> expectedDeliverAgainstValues = this.expectedValueList.stream()
				.map(mapping -> StringUtils.isEqual(mapping.getProperty(), "buy") ? new ConversionMapping<MT543>("buy", true, (m) -> null) : mapping)
				.collect(Collectors.toList());
		DeliverAgainstPayment deliverAgainstPayment = new DeliverAgainstPayment();
		Map<String, Object> expectedValueMap = new HashMap<>();
		expectedDeliverAgainstValues.forEach(m -> expectedValueMap.put(m.getProperty(), m.getValue()));
		expectedValueMap.forEach((key, value) -> BeanUtils.setPropertyValue(deliverAgainstPayment, key, value));

		Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(deliverAgainstPayment);
		MapDifference<String, Object> differences = Maps.difference(expectedValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "All properties should have been set by reflection " + differences.toString());

		List<String> allPropertyNames = BeanUtils.getPropertyNames(DeliverAgainstPayment.class, true, true);
		allPropertyNames.removeAll(valueMap.keySet());
		Assertions.assertTrue(allPropertyNames.isEmpty(), "Should have all available properties set" + allPropertyNames);

		DeliverAgainstPaymentMessageConverter converter = new DeliverAgainstPaymentMessageConverter();
		String messageString = converter.toSwiftMessage(deliverAgainstPayment);
		MatcherAssert.assertThat(messageString, IsEqual.equalTo(EXPECTED_DELIVER_AGAINST_OPTIONS));

		final MT541 mt541 = converter.toSwiftEntity(deliverAgainstPayment);
		final MT543 mt543 = new MT543();
		BeanUtils.copyProperties(mt541, mt543);
		Map<String, Object> mtValueMap = expectedDeliverAgainstValues.stream()
				.collect(HashMap::new, (r, f) -> r.put(f.getProperty(), f.getMapper().apply(mt543)), CollectionUtils.throwingCollectCombiner());
		List<String> expectedDifferences = Arrays.asList("custodyBIC", "buy", "repurchaseAgreement");
		final MapDifference<String, Object> allDifferences = Maps.difference(mtValueMap, valueMap);
		expectedDifferences.forEach(k -> Assertions.assertTrue(allDifferences.entriesDiffering().containsKey(k), "Expected difference for property " + k));
		expectedDifferences.forEach(k -> {
			valueMap.remove(k);
			mtValueMap.remove(k);
		});
		differences = Maps.difference(mtValueMap, valueMap);
		Assertions.assertTrue(differences.areEqual(), "MT message values are not the same as DeliverAgainstPayment values " + differences.toString());
	}
}
