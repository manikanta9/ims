package com.clifton.swift.prowide.integrator;

import com.clifton.swift.message.bic.SwiftBicDetail;
import com.clifton.swift.prowide.integrator.bic.ProwideServerSwiftBicDetailsServiceImpl;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class SwiftBicDetailServiceTest {

	@Test
	public void testBicRecordRetrieval() throws Exception {
		ProwideServerSwiftBicDetailsServiceImpl detailService = new ProwideServerSwiftBicDetailsServiceImpl();
		detailService.afterPropertiesSet();
		SwiftBicDetail swiftBicDetail = detailService.getSwiftBicDetails("GOLDUS33");
		Assertions.assertNotNull(swiftBicDetail);
		MatcherAssert.assertThat(swiftBicDetail.getBranch(), IsEqual.equalTo("XXX"));
		MatcherAssert.assertThat(swiftBicDetail.getCountryName(), IsEqual.equalTo("UNITED STATES OF AMERICA"));
		MatcherAssert.assertThat(swiftBicDetail.getInstitutionName(), IsEqual.equalTo("GOLDMAN SACHS AND CO. LLC"));
		MatcherAssert.assertThat(swiftBicDetail.getSubtypeIndication(), IsEqual.equalTo("SUPE"));
		MatcherAssert.assertThat(swiftBicDetail.getCode(), IsEqual.equalTo("GOLDUS33"));
	}
}
