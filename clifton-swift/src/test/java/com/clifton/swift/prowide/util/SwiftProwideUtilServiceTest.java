package com.clifton.swift.prowide.util;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SwiftProwideUtilServiceTest<M> {

	private static final String TEST_INSTRUCTION_MESSAGE = "{1:F01FOOSEDR0AXXX0000000000}{2:I202FOORECV0XXXXN}{4:\n" +
			":20:IMS:M2M:56872\n" +
			":21:MARG\n" +
			":32A:161026EUR10120,\n" +
			":53B:/12345678901234567890\n" +
			":57A:BANKANC0XXX\n" +
			":58A:/12345678901234567XXX\n" +
			"BANKANC0XXX\n}";

	private static final String TEST_TWO_MESSAGES_IN_ONE = "{1:F21PPSCUS60AXXX0009000001}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000001}{2:I202CNORUS40XCOLN}{4:\n" +
			":20:IMSCOL4598301\n" +
			":21:MARG\n" +
			":32A:161110USD900222,64\n" +
			":53B:/SUR55\n" +
			":57A:CNORSGSG\n" +
			":58A:/15700043\n" +
			"GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:BDCB376335F5}{TNG:}}${1:F21PPSCUS60AXXX0009000002}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000002}{2:I210CNORUS40XCOLN}{4:\n" +
			":20:IMS4594491MTM\n" +
			":25:ATV02\n" +
			":30:161109\n" +
			":21:MARG\n" +
			":32B:USD166737,5\n" +
			":52A:PPSCUS60\n" +
			":56A:GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:73B9CDA2F754}{TNG:}}";

	public static final String SWIFT_MULTIPLE_202 = "{1:F21PPSCUS60AXXX0009000001}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000001}{2:I202CNORUS40XCOLN}{4:\n" +
			":20:IMSCOL4598301\n" +
			":21:MARG\n" +
			":32A:161110USD900222,64\n" +
			":53B:/SUR55\n" +
			":57A:CNORSGSG\n" +
			":58A:/15700043\n" +
			"GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:BDCB376335F5}{TNG:}}${1:F21PPSCUS60AXXX0009000002}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000002}{2:I210CNORUS40XCOLN}{4:\n" +
			":20:IMS4594491MTM\n" +
			":25:ATV02\n" +
			":30:161109\n" +
			":21:MARG\n" +
			":32B:USD166737,5\n" +
			":52A:PPSCUS60\n" +
			":56A:GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:73B9CDA2F754}{TNG:}}";

	@Resource
	private ProwideSwiftUtilHandlerImpl utilService;
	@Resource
	private ProwideServerSwiftUtilHandlerImpl serverUtilService;


	@Test
	public void testParse() {
		Assertions.assertNotNull(this.utilService);
		AbstractMessage message = this.utilService.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);
		Assertions.assertNotNull(message);
	}


	@Test
	public void testParseTwoMessagesInOne() {
		Assertions.assertNotNull(this.utilService);
		AbstractMessage message = this.utilService.parseSingleMessage(TEST_TWO_MESSAGES_IN_ONE, SwiftMessageFormats.MT);
		Assertions.assertNotNull(message);
	}


	@Test
	public void testMessageType() {
		AbstractMessage message = this.utilService.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);
		String messageType = this.utilService.getMessageTypeCode(message);
		Assertions.assertNotNull(messageType);
		Assertions.assertFalse(StringUtils.isEmpty(messageType));
		MatcherAssert.assertThat(messageType, IsEqual.equalTo("MT202"));
	}


	@Test
	public void getUniqueId() {
		AbstractMessage message = this.utilService.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);
		String uniqueId = this.utilService.getTransactionReferenceNumber(message);
		Assertions.assertNotNull(uniqueId);
		MatcherAssert.assertThat(uniqueId, IsEqual.equalTo("IMS:M2M:56872"));
	}


	@Test
	public void isSystemMessage() {
		AbstractMessage message = this.utilService.parseSingleMessage(SWIFT_MULTIPLE_202, SwiftMessageFormats.MT);
		Assertions.assertTrue(this.utilService.isSystemAcknowledgementMessage(message));
	}


	@Test
	public void isAcknowledged() {
		AbstractMessage message = this.utilService.parseSingleMessage(SWIFT_MULTIPLE_202, SwiftMessageFormats.MT);
		Assertions.assertTrue(this.utilService.isAcknowledged(message));
	}


	@Test
	public void getAllMessagesList() {
		List<AbstractMessage> messageList = this.utilService.parseMessage(SWIFT_MULTIPLE_202, SwiftMessageFormats.MT);
		List<AbstractMessage> messages = new ArrayList<>();
		messageList.forEach(message -> messages.addAll(this.utilService.getAllMessagesList(message)));

		Assertions.assertEquals(4, messages.size());
		List<AbstractMT> abstractMTList = messages.stream().map(AbstractMT.class::cast).collect(Collectors.toList());

		MatcherAssert.assertThat(abstractMTList.get(0).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F21PPSCUS60AXXX0009000001"));
		MatcherAssert.assertThat(abstractMTList.get(1).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F01PPSCUS60AXXX0009000001"));
		MatcherAssert.assertThat(abstractMTList.get(2).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F21PPSCUS60AXXX0009000002"));
		MatcherAssert.assertThat(abstractMTList.get(3).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F01PPSCUS60AXXX0009000002"));
	}


	@Test
	@Disabled
	public void getAcknowledgedMessage() {
		AbstractMessage message = this.utilService.parseSingleMessage(SWIFT_MULTIPLE_202, SwiftMessageFormats.MT);
		List<AbstractMessage> messageList = this.utilService.parseMessage(SWIFT_MULTIPLE_202, SwiftMessageFormats.MT);
		List<AbstractMessage> messages = new ArrayList<>();
		messageList.forEach(msg -> messages.addAll(this.utilService.getAllMessagesList(msg)));

		AbstractMessage match = this.utilService.getAcknowledgedMessage(message, messages);
		Assertions.assertNotNull(match);
		MatcherAssert.assertThat(((AbstractMT) match).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F01PPSCUS60AXXX0009000001"));
		Assertions.assertTrue(((AbstractMT) messages.get(1)).getSwiftMessage().isServiceMessage21());
		match = this.utilService.getAcknowledgedMessage(messages.get(1), messages);
		MatcherAssert.assertThat(((AbstractMT) match).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F01PPSCUS60AXXX0009000002"));
	}


	@Test
	@Disabled  // message order is unpredictable
	public void getAllMessagesMap() {
		List<AbstractMessage> messageList = this.utilService.parseMessage(SWIFT_MULTIPLE_202, SwiftMessageFormats.MT);
		Map<AbstractMessage, Boolean> messageMap = new LinkedHashMap<>();
		messageList.forEach(msg -> messageMap.putAll(this.utilService.getAllMessagesMap(msg)));

		Assertions.assertEquals(2, messageMap.size());
		List<AbstractMT> abstractMTList = messageMap.keySet().stream().map(AbstractMT.class::cast).collect(Collectors.toList());
		MatcherAssert.assertThat(abstractMTList.get(0).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F01PPSCUS60AXXX0009000001"));
		MatcherAssert.assertThat(abstractMTList.get(1).getSwiftMessage().getBlock1().getValue(), IsEqual.equalTo("F01PPSCUS60AXXX0009000002"));
	}


	@Test
	public void buildSwiftMessagingMessageSuppressedNull() {
		AbstractMessage message = this.utilService.parseSingleMessage("{", SwiftMessageFormats.MT);
		SwiftMessagingMessage swiftMessagingMessage = this.serverUtilService.buildSwiftMessagingMessage(message, true);
		Assertions.assertNotNull(swiftMessagingMessage);
	}


	@Test
	public void buildSwiftMessagingMessageSuppressed() {
		AbstractMessage message = this.utilService.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);
		SwiftMessagingMessage swiftMessagingMessage = this.serverUtilService.buildSwiftMessagingMessage(message, true);
		Assertions.assertNotNull(swiftMessagingMessage);
		MatcherAssert.assertThat(swiftMessagingMessage.getUniqueStringIdentifier(), IsEqual.equalTo("IMS:M2M:56872"));
	}
}
