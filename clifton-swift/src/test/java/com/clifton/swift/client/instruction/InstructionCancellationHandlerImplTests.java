package com.clifton.swift.client.instruction;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierService;
import com.clifton.swift.identifier.search.SwiftIdentifierSearchForm;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.prowide.vistor.encoder.EBCDICMessageTextEncoder;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.SystemTableBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@ContextConfiguration
@Transactional
public class InstructionCancellationHandlerImplTests<M> extends BaseInMemoryDatabaseTests {

	@Resource
	private InstructionCancellationHandler instructionCancellationHandler;

	@Resource
	private SwiftIdentifierService swiftIdentifierService;

	@Resource
	private SwiftSourceSystemService swiftSourceSystemService;

	@Resource
	private SwiftMessageConverterHandler<M> swiftMessageConverterHandler;

	@Resource
	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void generateTradeCancellationMessageListTest() {
		SwiftSourceSystem swiftSourceSystem = this.swiftSourceSystemService.getSwiftSourceSystemByName("IMS");
		Assertions.assertNotNull(swiftSourceSystem);

		SwiftIdentifierSearchForm swiftIdentifierSearchForm = new SwiftIdentifierSearchForm();
		swiftIdentifierSearchForm.setSourceSystemId(swiftSourceSystem.getId());
		swiftIdentifierSearchForm.setOrderBy("id");
		List<SwiftIdentifier> identifierList = this.swiftIdentifierService.getSwiftIdentifierList(swiftIdentifierSearchForm)
				.stream()
				.filter(i -> i.getUniqueStringIdentifier().endsWith("AT"))
				.collect(Collectors.toList());
		Assertions.assertFalse(identifierList.isEmpty());

		AtomicInteger primaryKey = new AtomicInteger(123456);
		final List<InstructionMessage> cancellations = identifierList.stream()
				.map(i -> (Instruction) new Instruction() {

					private int identity = primaryKey.getAndIncrement();
					private int fkFieldId = i.getSourceSystemFkFieldId().intValue();
					private SystemTable systemTable = SystemTableBuilder.createTrade().toSystemTable();


					@Override
					public Integer getFkFieldId() {
						return this.fkFieldId;
					}


					@Override
					public SystemTable getSystemTable() {
						return this.systemTable;
					}


					@Override
					public Serializable getIdentity() {
						return this.identity;
					}


					@Override
					public boolean isNewBean() {
						return false;
					}
				})
				.flatMap(i -> this.instructionCancellationHandler.generateCancellationMessageList(i).stream())
				.collect(Collectors.toList());

		Set<MessageFunctions> functionsSet = cancellations.stream()
				.map(AbstractTradeMessage.class::cast)
				.map(AbstractTradeMessage::getMessageFunctions)
				.collect(Collectors.toSet());
		MatcherAssert.assertThat(functionsSet.size(), IsEqual.equalTo(1));
		Assertions.assertTrue(functionsSet.contains(MessageFunctions.CANCEL));

		List<String> reversedMessages = cancellations.stream()
				.map(AbstractTradeMessage.class::cast)
				.peek(c -> {
					c.setMessageReferenceNumber(null);
					c.setMessageFunctions(MessageFunctions.NEW_MESSAGE);
				})
				.map(i -> this.swiftMessageConverterHandler.toSwiftMessage(i, SwiftMessageFormats.MT))
				.map(t -> t.replaceAll("[\\n\\r]", ""))
				.collect(Collectors.toList());
		List<String> originalMessages = identifierList.stream()
				.flatMap(i -> {
					SwiftMessageSearchForm searchForm = new SwiftMessageSearchForm();
					searchForm.setIdentifierId(i.getId());
					searchForm.setIncoming(false);
					return this.swiftServerMessageService.getSwiftMessageList(searchForm).stream();
				})
				.map(SwiftMessage::getText)
				.map(t -> t.replaceAll("[\\n\\r]", ""))
				.collect(Collectors.toList());
		Assertions.assertEquals(reversedMessages.size(), originalMessages.size());
		Assertions.assertEquals(identifierList.size(), reversedMessages.size());
		EBCDICMessageTextEncoder encoder = new EBCDICMessageTextEncoder();
		for (int i = 0; i < reversedMessages.size(); i++) {
			SwiftIdentifier identifier = identifierList.get(i);
			String uniqueStringIdentifier = encoder.encode(identifier.getUniqueStringIdentifier().concat("_CANCEL"));
			String reversed = reversedMessages.get(i).replace(uniqueStringIdentifier, identifier.getId().toString());
			MatcherAssert.assertThat(originalMessages.get(i), IsEqual.equalTo(reversed));
		}
	}
}
