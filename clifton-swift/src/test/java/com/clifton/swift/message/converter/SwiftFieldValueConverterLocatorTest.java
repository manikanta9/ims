package com.clifton.swift.message.converter;

import com.clifton.swift.prowide.message.converter.SwiftBicConverter;
import com.clifton.swift.prowide.message.converter.SwiftFieldValueConverter;
import com.clifton.swift.prowide.message.converter.SwiftFieldValueConverterLocator;
import com.prowidesoftware.swift.model.field.Field;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftFieldValueConverterLocatorTest {

	@Resource
	private SwiftFieldValueConverterLocator<?> swiftFieldValueFormatterLocator;


	@Test
	@SuppressWarnings("unchecked")
	public void testFormatterLoading() throws IOException {
		Assertions.assertNotNull(this.swiftFieldValueFormatterLocator);
		List<SwiftFieldValueConverter<Field>> converterList = (List<SwiftFieldValueConverter<Field>>) ReflectionTestUtils.getField(this.swiftFieldValueFormatterLocator, "converterList");
		Assertions.assertNotNull(converterList);
		Assertions.assertFalse(converterList.isEmpty());
		for (SwiftFieldValueConverter<Field> converter : converterList) {
			if (converter instanceof SwiftBicConverter) {
				SwiftBicConverter swiftFieldValueBicFormatter = SwiftBicConverter.class.cast(converter);
				Assertions.assertNotNull(swiftFieldValueBicFormatter.getSwiftBicDetailService());
			}
		}
	}
}
