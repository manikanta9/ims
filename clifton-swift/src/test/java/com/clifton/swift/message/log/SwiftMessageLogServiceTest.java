package com.clifton.swift.message.log;

import com.clifton.swift.client.SwiftClientMessageLogServiceImpl;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandlerLocator;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageLogRequestMessage;
import com.clifton.swift.server.synchronous.handler.SwiftMessageLogRequestHandler;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Collections;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@DirtiesContext
public class SwiftMessageLogServiceTest {

	@Resource
	private SwiftMessageLogService swiftClientMessageLogService;

	@Resource
	private SwiftSynchronousMessageHandlerLocator swiftSynchronousMessageHandlerLocator;

	@Mock
	private SwiftMessageLogService swiftServerMessageLogService;

	private SwiftMessageLog targetSwiftMessageLog = SwiftMessageLogBuilder.createSwiftMessageLogMt210().getSwiftMessageLog();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void before() {
		MockitoAnnotations.initMocks(this);
		Mockito.reset(this.swiftServerMessageLogService);

		// context is setup for client-side, change the server-side message service implementation.
		SwiftMessageLogRequestHandler logHandler = (SwiftMessageLogRequestHandler) this.swiftSynchronousMessageHandlerLocator.locate(SwiftMessageLogRequestMessage.class);
		logHandler.setSwiftServerMessageLogService(this.swiftServerMessageLogService);
		Mockito.when(this.swiftServerMessageLogService.getSwiftMessageLogList(Mockito.any())).thenReturn(Collections.singletonList(this.targetSwiftMessageLog));
	}


	@Test
	public void getSwiftMessageLog() {
		Assertions.assertTrue(this.swiftClientMessageLogService instanceof SwiftClientMessageLogServiceImpl);

		// synchronous, no need to worry about threads or waiting.
		SwiftMessageLog returnedSwiftMessageLog = this.swiftClientMessageLogService.getSwiftMessageLog(1);
		Assertions.assertNotNull(returnedSwiftMessageLog);

		Mockito.verify(this.swiftServerMessageLogService, Mockito.times(1)).getSwiftMessageLogList(Mockito.any());

		// "As much as is reasonably practical, the hashCode method defined by class Object does return distinct integers for distinct objects"
		Assertions.assertNotEquals(System.identityHashCode(returnedSwiftMessageLog.hashCode()), System.identityHashCode(this.targetSwiftMessageLog));

		MatcherAssert.assertThat(returnedSwiftMessageLog.getText(), IsEqual.equalTo(this.targetSwiftMessageLog.getText()));
	}
}
