package com.clifton.swift.message;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.prowide.validation.SwiftProwideValidationUtils;
import com.prowidesoftware.swift.model.field.Field20;
import com.prowidesoftware.swift.model.field.Field21;
import com.prowidesoftware.swift.model.field.Field32A;
import com.prowidesoftware.swift.model.field.Field50A;
import com.prowidesoftware.swift.model.field.Field58D;
import com.prowidesoftware.swift.model.field.Field59;
import com.prowidesoftware.swift.model.field.Field71A;
import com.prowidesoftware.swift.model.mt.mt2xx.MT202;
import com.prowidesoftware.swift.model.mx.MxPacs00900102;
import com.prowidesoftware.swift.model.mx.dic.AccountIdentification4Choice;
import com.prowidesoftware.swift.model.mx.dic.ActiveCurrencyAndAmount;
import com.prowidesoftware.swift.model.mx.dic.BranchAndFinancialInstitutionIdentification4;
import com.prowidesoftware.swift.model.mx.dic.CashAccount16;
import com.prowidesoftware.swift.model.mx.dic.CreditTransferTransactionInformation13;
import com.prowidesoftware.swift.model.mx.dic.FinancialInstitutionCreditTransferV02;
import com.prowidesoftware.swift.model.mx.dic.FinancialInstitutionIdentification7;
import com.prowidesoftware.swift.model.mx.dic.GenericAccountIdentification1;
import com.prowidesoftware.swift.model.mx.dic.GroupHeader35;
import com.prowidesoftware.swift.model.mx.dic.PaymentIdentification3;
import com.prowidesoftware.swift.model.mx.dic.SettlementInformation13;
import com.prowidesoftware.swift.model.mx.dic.SettlementMethod1Code;
import com.prowidesoftware.swift.translations.MxPacs00900102_MT202_Translation;
import com.prowidesoftware.swift.validator.ValidationEngine;
import com.prowidesoftware.swift.validator.ValidationProblem;
import org.junit.jupiter.api.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


/**
 * @author mwacker
 */
public class SwiftMessageTest {

	@Test
	public void testSwiftMessageGenerationAndConversion() {
		MxPacs00900102 message = testSwiftMessageGeneration();
		MxPacs00900102_MT202_Translation translator = new MxPacs00900102_MT202_Translation();
		MT202 m202 = translator.translate(message);

		//System.out.println(message.message());

		System.out.println(m202.message());
		System.out.println(m202.xml());

		ValidationEngine engine = new ValidationEngine();
		try {
			engine.initialize();

			List<ValidationProblem> r = engine.validateMxMessage(message.message());
			SwiftProwideValidationUtils.getValidationProblemsAsString(r);

			r = engine.validateMtMessage(m202.message());
			SwiftProwideValidationUtils.getValidationProblemsAsString(r);
		}
		finally {
			engine.dispose();
		}
	}


	@Test
	public void testSwiftMessageGeneration202() {
		MT202 message = new MT202();
//		message.setReceiver();

		Field58D f58D = new Field58D();
//		f58D.getName()
		message.addField(new Field58D());


		/*
		 * Set sender and receiver BIC codes
		 */
		message.setSender("FOOSEDR0AXXX");
		message.setReceiver("FOORECV0XXXX");

		/*
		 * Start adding the message's fields in correct order
		 */
		message.addField(new Field20("REFERENCE"));
		message.addField(new Field21("CRED"));

		/*
		 * Add a field using comprehensive setters API
		 */
		Field32A f32A = new Field32A()
				.setDate(Calendar.getInstance())
				.setCurrency("EUR")
				.setAmount("1234567,89");
		message.addField(f32A);

		/*
		 * Add the orderer field
		 */
		Field50A f50A = new Field50A()
				.setAccount("12345678901234567890")
				.setBIC("FOOBANKXXXXX");
		message.addField(f50A);

		/*
		 * Add the beneficiary field
		 */
		Field59 f59 = new Field59()
				.setAccount("12345678901234567890")
				.setNameAndAddress("JOE DOE");
		message.addField(f59);

		/*
		 * Add the commission indication
		 */
		message.addField(new Field71A("OUR"));

		System.out.println(message.message());
	}


	private MxPacs00900102 testSwiftMessageGeneration() {
		/*
		 * Initialize the MX object
		 */
		MxPacs00900102 mx = new MxPacs00900102();

		/*
		 * Initialize main message content main objects
		 */
		mx.setFinInstnCdtTrf(new FinancialInstitutionCreditTransferV02().setGrpHdr(new GroupHeader35()));

		/*
		 * General Information
		 */
		mx.getFinInstnCdtTrf().getGrpHdr().setMsgId("TBEXO12345");
		mx.getFinInstnCdtTrf().getGrpHdr().setCreDtTm(getXMLGregorianCalendarNow());
		mx.getFinInstnCdtTrf().getGrpHdr().setNbOfTxs("1");

		/*
		 * Settlement Information
		 */
		mx.getFinInstnCdtTrf().getGrpHdr().setSttlmInf(new SettlementInformation13());
		mx.getFinInstnCdtTrf().getGrpHdr().getSttlmInf().setSttlmMtd(SettlementMethod1Code.INDA);
		mx.getFinInstnCdtTrf().getGrpHdr().getSttlmInf().setSttlmAcct(
				(new CashAccount16()).setId(
						(new AccountIdentification4Choice()).setOthr(
								(new GenericAccountIdentification1()).setId("00010013800002001234"))));

		/*
		 * Instructing Agent
		 */
		mx.getFinInstnCdtTrf().getGrpHdr().setInstgAgt(
				(new BranchAndFinancialInstitutionIdentification4()).setFinInstnId(
						(new FinancialInstitutionIdentification7()).setBIC("FOOBARC0XXX")));

		/*
		 * Instructed Agent
		 */
		mx.getFinInstnCdtTrf().getGrpHdr().setInstdAgt(
				(new BranchAndFinancialInstitutionIdentification4()).setFinInstnId(
						(new FinancialInstitutionIdentification7()).setBIC("BANKANC0XXX")));

		/*
		 * Payment Transaction Information
		 */
		CreditTransferTransactionInformation13 cti = new CreditTransferTransactionInformation13();

		/*
		 * Transaction Identification
		 */
		cti.setPmtId(new PaymentIdentification3());
		cti.getPmtId().setInstrId("TBEXO12345");
		cti.getPmtId().setEndToEndId("TBEXO12345");
		cti.getPmtId().setTxId("TBEXO12345");

		/*
		 * Transaction Amount
		 */
		ActiveCurrencyAndAmount amount = new ActiveCurrencyAndAmount();
		amount.setCcy("USD");
		amount.setValue(new BigDecimal("23453"));
		cti.setIntrBkSttlmAmt(amount);

		/*
		 * Transaction Value Date
		 */
		cti.setIntrBkSttlmDt(getXMLGregorianCalendarNow());

		/*
		 * Transaction Charges
		 */
		//cti.setChrgBr(ChargeBearerType1Code.DEBT);

		/*
		 * Orderer Name & Address
		 */
		cti.setDbtr(new BranchAndFinancialInstitutionIdentification4());
//		cti.getDbtr().setNm("JOE DOE");
//		cti.getDbtr().setPstlAdr((new PostalAddress6()).addAdrLine("310 Field Road, NY"));

		/*
		 * Orderer Account
		 */
		cti.setDbtrAcct(
				(new CashAccount16()).setId(
						(new AccountIdentification4Choice()).setOthr(
								(new GenericAccountIdentification1()).setId("01111001759234567890"))));
		/*
		 * Order Financial Institution
		 */
		cti.setDbtrAgt(
				(new BranchAndFinancialInstitutionIdentification4()).setFinInstnId(
						(new FinancialInstitutionIdentification7()).setBIC("FOOBARC0XXX")));

		/*
		 * Beneficiary Institution
		 */
		cti.setCdtrAgt((new BranchAndFinancialInstitutionIdentification4()).setFinInstnId((new FinancialInstitutionIdentification7()).setBIC("BANKANC0XXX")));

		/*
		 * Beneficiary Name & Address
		 */
//		cti.setCdtr(new PartyIdentification32());
//		cti.getCdtr().setNm("TEST CORP");
//		cti.getCdtr().setPstlAdr((new PostalAddress6().addAdrLine("Nellis ABC, NV")));

		/*
		 * Beneficiary Account
		 */
//		cti.setCdtrAcct(
//				(new CashAccount16()).setId(
//						(new AccountIdentification4Choice()).setOthr(
//								(new GenericAccountIdentification1()).setId("00013500510020179998"))));


		cti.setCdtrAcct(
				(new CashAccount16()).setId(
						(new AccountIdentification4Choice()).setOthr(new GenericAccountIdentification1()).setIBAN("00013500510020179998")));

		//cti.setCdtrAcct((new CashAccount16()).setNm("00013500510020179998"));

		mx.getFinInstnCdtTrf().addCdtTrfTxInf(cti);

		return mx;
	}


	public static XMLGregorianCalendar getXMLGregorianCalendarNow() {
		GregorianCalendar gregorianCalendar = new GregorianCalendar();
		DatatypeFactory datatypeFactory = null;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		}
		catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		}
		AssertUtils.assertNotNull(datatypeFactory, "Cannot create new XMLGregorianCalendar on null DatatypeFactory");
		XMLGregorianCalendar now = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);
		return now;
	}
}
