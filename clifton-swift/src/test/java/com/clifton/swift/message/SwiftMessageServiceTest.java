package com.clifton.swift.message;

import com.clifton.swift.client.SwiftClientMessageServiceImpl;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandlerLocator;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageRequestMessage;
import com.clifton.swift.server.synchronous.handler.SwiftMessageRequestHandler;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Collections;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@DirtiesContext
public class SwiftMessageServiceTest {

	@Resource
	private SwiftMessageService swiftClientMessageService;

	@Resource
	private SwiftSynchronousMessageHandlerLocator swiftSynchronousMessageHandlerLocator;

	@Mock
	private SwiftMessageService swiftServerMessageService;

	private SwiftMessage targetSwiftMessage = SwiftMessageBuilder.createSwiftMessageMt202().getSwiftMessage();


	@BeforeEach
	public void before() {
		MockitoAnnotations.initMocks(this);
		Mockito.reset(this.swiftServerMessageService);

		// context is setup for client-side, change the server-side message service implementation.
		SwiftMessageRequestHandler logHandler = (SwiftMessageRequestHandler) this.swiftSynchronousMessageHandlerLocator.locate(SwiftMessageRequestMessage.class);
		logHandler.setSwiftServerMessageService(this.swiftServerMessageService);
		Mockito.when(this.swiftServerMessageService.getSwiftMessageList(Mockito.any())).thenReturn(Collections.singletonList(this.targetSwiftMessage));
	}


	@Test
	public void getSwiftMessage() {
		Assertions.assertTrue(this.swiftClientMessageService instanceof SwiftClientMessageServiceImpl);

		// synchronous, no need to worry about threads or waiting.
		SwiftMessage returnedSwiftMessage = this.swiftClientMessageService.getSwiftMessage(1);
		Assertions.assertNotNull(returnedSwiftMessage);

		Mockito.verify(this.swiftServerMessageService, Mockito.times(1)).getSwiftMessageList(Mockito.any());

		// "As much as is reasonably practical, the hashCode method defined by class Object does return distinct integers for distinct objects"
		Assertions.assertNotEquals(System.identityHashCode(returnedSwiftMessage.hashCode()), System.identityHashCode(this.targetSwiftMessage));

		MatcherAssert.assertThat(returnedSwiftMessage.getText(), IsEqual.equalTo(this.targetSwiftMessage.getText()));
		MatcherAssert.assertThat(returnedSwiftMessage.getType(), IsEqual.equalTo(this.targetSwiftMessage.getType()));
		MatcherAssert.assertThat(returnedSwiftMessage.getIdentifier(), IsEqual.equalTo(this.targetSwiftMessage.getIdentifier()));
	}
}
