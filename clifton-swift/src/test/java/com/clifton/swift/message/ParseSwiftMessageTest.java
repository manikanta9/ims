package com.clifton.swift.message;

import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;


public class ParseSwiftMessageTest {

	public static final String SWIFT_MULTIPLE_081 = "{1:F21PPSCUS60AXXX0009000061}{4:{177:1611170802}{451:0}}{1:F01PPSCUS60AXXX0009000061}{2:O0810802161117EAEAXXXXGXXX00003333011611170002S}" +
			"{4:{202:0001}{203:0001}{305:A}{332:000000000000}}{5:{CHK:D8539E643A6A}{SYS:}{TNG:}}{S:{COP:P}}${1:A21PPSCUS60AXXX0009000005}{4:{177:1611170802}{451:0}}" +
			"{1:A01PPSCUS60AXXX0009000005}{2:O0810802161117EAEAXXXXGXXX00004849531611170002}{4:{202:0001}{203:0001}{305:A}{332:000000000000}}{5:{CHK:D8539E643A6A}{SYS:}{TNG:}}{S:{COP:P}}";
	public static final String SWIFT_MULTIPLE_202 = "{1:F21PPSCUS60AXXX0009000001}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000001}{2:I202CNORUS40XCOLN}{4:\n" +
			":20:IMSCOL4598301\n" +
			":21:MARG\n" +
			":32A:161110USD900222,64\n" +
			":53B:/SUR55\n" +
			":57A:CNORSGSG\n" +
			":58A:/15700043\n" +
			"GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:BDCB376335F5}{TNG:}}${1:F21PPSCUS60AXXX0009000002}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000002}{2:I210CNORUS40XCOLN}{4:\n" +
			":20:IMS4594491MTM\n" +
			":25:ATV02\n" +
			":30:161109\n" +
			":21:MARG\n" +
			":32B:USD166737,5\n" +
			":52A:PPSCUS60\n" +
			":56A:GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:73B9CDA2F754}{TNG:}}";
	public static final String SWIFT_MULTIPLE_082 = "{1:F21PPSCUS60AXXX0009000062}{4:{177:1611171601}{451:0}}{1:F01PPSCUS60AXXX0009000062}{2:O0821601161117EANGXXXXAXXX00003332241611170801S}" +
			"{4:{202:0001}{203:0001}{171:161117}{175:1600}{301:RT}{335:0742161117PPSCUS60AXXX0009000001202CNORUS40XCOL}{108:IMSCOL4598301}{335:0742161117PPSCUS60AXXX0009000002210CNORUS40XCOL}" +
			"{108:IMS4594491MTM}{461:002}}{5:{CHK:34946E064D9F}{SYS:}{TNG:}}{S:{COP:P}}";


	@Test
	public void testMultiple081Messages() throws IOException {
		AbstractMT abstractMT = AbstractMT.parse(SWIFT_MULTIPLE_081);
		// message 1 is an ACK
		Assertions.assertNotNull(abstractMT);
		com.prowidesoftware.swift.model.SwiftMessage msg1 = abstractMT.getSwiftMessage();
		Assertions.assertNotNull(msg1);
		Assertions.assertTrue(abstractMT.getSwiftMessage().isServiceMessage21());
		Assertions.assertTrue(abstractMT.getSwiftMessage().isAck());
		MatcherAssert.assertThat("F21PPSCUS60AXXX0009000061", IsEqual.equalTo(msg1.getBlock1().getValue()));
		// message 2 goes along with the ACK and is 081
		MatcherAssert.assertThat(1, IsEqual.equalTo(msg1.getUnparsedTextsSize()));
		com.prowidesoftware.swift.model.SwiftMessage mt081 = abstractMT.getSwiftMessage().getUnparsedTexts().getTextAsMessage(0);
		Assertions.assertNotNull(mt081);
		MatcherAssert.assertThat("081", IsEqual.equalTo(mt081.getType()));
		MatcherAssert.assertThat("F01PPSCUS60AXXX0009000061", IsEqual.equalTo(mt081.getBlock1().getValue()));
		// message 3 is another ACK
		MatcherAssert.assertThat(2, IsEqual.equalTo(mt081.getUnparsedTextsSize()));
		com.prowidesoftware.swift.model.SwiftMessage ack2 = mt081.getUnparsedTexts().getTextAsMessage(0);
		Assertions.assertNotNull(ack2);
		Assertions.assertTrue(abstractMT.getSwiftMessage().isServiceMessage21());
		Assertions.assertTrue(abstractMT.getSwiftMessage().isAck());
		MatcherAssert.assertThat("A21PPSCUS60AXXX0009000005", IsEqual.equalTo(ack2.getBlock1().getValue()));
		// message 4 is the second 081
		com.prowidesoftware.swift.model.SwiftMessage mt0812 = mt081.getUnparsedTexts().getTextAsMessage(1);
		Assertions.assertNotNull(mt0812);
		MatcherAssert.assertThat("081", IsEqual.equalTo(mt081.getType()));
		MatcherAssert.assertThat("A01PPSCUS60AXXX0009000005", IsEqual.equalTo(mt0812.getBlock1().getValue()));
	}


	@Test
	public void testMultiple202Messages() throws IOException {
		AbstractMT abstractMT = AbstractMT.parse(SWIFT_MULTIPLE_202);
		// message 1 is an ACK
		Assertions.assertNotNull(abstractMT);
		com.prowidesoftware.swift.model.SwiftMessage msg1 = abstractMT.getSwiftMessage();
		Assertions.assertNotNull(msg1);
		Assertions.assertTrue(abstractMT.getSwiftMessage().isServiceMessage21());
		Assertions.assertTrue(abstractMT.getSwiftMessage().isAck());
		MatcherAssert.assertThat("F21PPSCUS60AXXX0009000001", IsEqual.equalTo(msg1.getBlock1().getValue()));
		// message 2 goes along with the ACK and is 202
		MatcherAssert.assertThat(1, IsEqual.equalTo(msg1.getUnparsedTextsSize()));
		com.prowidesoftware.swift.model.SwiftMessage mt202 = abstractMT.getSwiftMessage().getUnparsedTexts().getTextAsMessage(0);
		Assertions.assertNotNull(mt202);
		MatcherAssert.assertThat("202", IsEqual.equalTo(mt202.getType()));
		MatcherAssert.assertThat("F01PPSCUS60AXXX0009000001", IsEqual.equalTo(mt202.getBlock1().getValue()));
		// message 3 is another ACK
		MatcherAssert.assertThat(2, IsEqual.equalTo(mt202.getUnparsedTextsSize()));
		com.prowidesoftware.swift.model.SwiftMessage ack2 = mt202.getUnparsedTexts().getTextAsMessage(0);
		Assertions.assertNotNull(ack2);
		Assertions.assertTrue(abstractMT.getSwiftMessage().isServiceMessage21());
		Assertions.assertTrue(abstractMT.getSwiftMessage().isAck());
		MatcherAssert.assertThat("F21PPSCUS60AXXX0009000002", IsEqual.equalTo(ack2.getBlock1().getValue()));
		// message 4 is the second 202
		com.prowidesoftware.swift.model.SwiftMessage mt20202 = mt202.getUnparsedTexts().getTextAsMessage(1);
		Assertions.assertNotNull(mt20202);
		MatcherAssert.assertThat("202", IsEqual.equalTo(mt202.getType()));
		MatcherAssert.assertThat("F01PPSCUS60AXXX0009000002", IsEqual.equalTo(mt20202.getBlock1().getValue()));
	}


	@Test
	public void testMultiple082Messages() throws IOException {
		AbstractMT abstractMT = AbstractMT.parse(SWIFT_MULTIPLE_082);
		// message 1 is an ACK
		Assertions.assertNotNull(abstractMT);
		com.prowidesoftware.swift.model.SwiftMessage msg1 = abstractMT.getSwiftMessage();
		Assertions.assertNotNull(msg1);
		Assertions.assertTrue(abstractMT.getSwiftMessage().isServiceMessage21());
		Assertions.assertTrue(abstractMT.getSwiftMessage().isAck());
		MatcherAssert.assertThat("F21PPSCUS60AXXX0009000062", IsEqual.equalTo(msg1.getBlock1().getValue()));
		// message 2 goes along with the ACK and is 082
		MatcherAssert.assertThat(1, IsEqual.equalTo(msg1.getUnparsedTextsSize()));
		com.prowidesoftware.swift.model.SwiftMessage mt082 = abstractMT.getSwiftMessage().getUnparsedTexts().getTextAsMessage(0);
		Assertions.assertNotNull(mt082);
		MatcherAssert.assertThat("082", IsEqual.equalTo(mt082.getType()));
		MatcherAssert.assertThat("F01PPSCUS60AXXX0009000062", IsEqual.equalTo(mt082.getBlock1().getValue()));
		// message 3 is another ACK
		MatcherAssert.assertThat(0, IsEqual.equalTo(mt082.getUnparsedTextsSize()));
	}
}
