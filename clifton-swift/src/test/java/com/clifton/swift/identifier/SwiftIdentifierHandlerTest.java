package com.clifton.swift.identifier;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.identifier.search.SwiftIdentifierSearchForm;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.system.search.SwiftSourceSystemModuleSearchForm;
import com.clifton.swift.util.SwiftUtilHandler;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftIdentifierHandlerTest<M> {

	private static final String TEST_INSTRUCTION_MESSAGE = "{1:F01FOOSEDR0AXXX0000000000}{2:I202FOORECV0XXXXN}{4:\n" +
			":20:IMS:M2M:56872\n" +
			":21:MARG\n" +
			":32A:161026EUR10120,\n" +
			":53B:/12345678901234567890\n" +
			":57A:BANKANC0XXX\n" +
			":58A:/12345678901234567XXX\n" +
			"BANKANC0XXX\n}";

	private static final String TEST_671_MESSAGE = "{1:F01PPSCUS66AXXX0065001784}{2:O6711338170925EBEBXXXXDXXX00076682931709250638N}{3:{108:E670170925741817}}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//MEDSCHGG/SSI-5\n" +
			":23G:NEWM\n" +
			":95P::SUBM//MEDSCHGG\n" +
			":22F::UDTP//CASH\n" +
			":16S:GENL\n" +
			":16R:SSIDET\n" +
			":22H::SSIP//NEWS\n" +
			":11A::SETT//XAG\n" +
			":98A::EFFD//20170929\n" +
			":22F::MARK//COMM\n" +
			":16R:CSHPRTY\n" +
			":95P::BENM//MEDSCHGG\n" +
			":16S:CSHPRTY\n" +
			":16R:CSHPRTY\n" +
			":95P::ACCW//AGRICHGG\n" +
			":97A::CASH//2000994L\n" +
			":16S:CSHPRTY\n" +
			":16R:CSHPRTY\n" +
			":95P::INT1//CHASGB2LBUL\n" +
			":97A::CASH//6061\n" +
			":16S:CSHPRTY\n" +
			":16R:OTHRDET\n" +
			":70E::ADTX//LOCO LONDON\n" +
			":16S:OTHRDET\n" +
			":16S:SSIDET\n" +
			":16R:SSIDET\n" +
			":22H::SSIP//NEWS\n" +
			":11A::SETT//XAU\n" +
			":98A::EFFD//20170929\n" +
			":22F::MARK//COMM\n" +
			":16R:CSHPRTY\n" +
			":95P::BENM//MEDSCHGG\n" +
			":16S:CSHPRTY\n" +
			":16R:CSHPRTY\n" +
			":95P::ACCW//AGRICHGG\n" +
			":97A::CASH//2000994L\n" +
			":16S:CSHPRTY\n" +
			":16R:CSHPRTY\n" +
			":95P::INT1//CHASGB2LBUL\n" +
			":97A::CASH//0248\n" +
			":16S:CSHPRTY\n" +
			":16R:OTHRDET\n" +
			":70E::ADTX//LOCO LONDON\n" +
			":16S:OTHRDET\n" +
			":16S:SSIDET\n" +
			":16R:SSIDET\n" +
			":22H::SSIP//NEWS\n" +
			":11A::SETT//XAG\n" +
			":98A::EFFD//20170929\n" +
			":22F::MARK//COMM\n" +
			":16R:CSHPRTY\n" +
			":95P::BENM//MEDSCHGG\n" +
			":16S:CSHPRTY\n" +
			":16R:CSHPRTY\n" +
			":95P::ACCW//CRESCHZZ80A\n" +
			":97A::CASH//CH04 0483 5090 3445 4401 2\n" +
			":16S:CSHPRTY\n" +
			":16R:OTHRDET\n" +
			":70E::ADTX//LOCO ZURICH\n" +
			":16S:OTHRDET\n" +
			":16S:SSIDET\n" +
			":16R:SSIDET\n" +
			":22H::SSIP//NEWS\n" +
			":11A::SETT//XAU\n" +
			":98A::EFFD//20170929\n" +
			":22F::MARK//COMM\n" +
			":16R:CSHPRTY\n" +
			":95P::BENM//MEDSCHGG\n" +
			":16S:CSHPRTY\n" +
			":16R:CSHPRTY\n" +
			":95P::ACCW//CRESCHZZ80A\n" +
			":97A::CASH//CH31 0483 5090 3445 4401 1\n" +
			":16S:CSHPRTY\n" +
			":16R:OTHRDET\n" +
			":70E::ADTX//LOCO ZURICH\n" +
			":16S:OTHRDET\n" +
			":16S:SSIDET\n" +
			":16R:OTHRDET\n" +
			":70E::ADTX//CONTACT DETAILS FOR FOREX, MONEY \n" +
			"MARKET, DERIVATIVE DEALS, PAYMENT \n" +
			"AND PRECIOUS METAL :\n" +
			"TEL: +41 22 906 06 44\n" +
			"TREASURY(AT)BANKMED.CH\n" +
			":70E::ADTX//CONTACT DETAILS FOR FINANCIAL \n" +
			"INSTITUTIONS RELATIONS :\n" +
			"TEL: +41 22 906 06 06\n" +
			"OPERATIONS(AT)BANKMED.CH\n" +
			":70E::ADTX//EFFECTIVE CHANGES ON GBP, TRY, XAG \n" +
			"(LOCO ZRH) AND XAU (LOCO ZRH).\n" +
			"OTHER UPDATES ARE RELATED TO \n" +
			"FORMAT AMENDMENT OF PREVIOUS \n" +
			"MESSAGE.\n" +
			":16S:OTHRDET\n" +
			"-}{5:{MAC:00000000}{CHK:67A96C0D35C3}}{S:{SAC:}{COP:P}}";

	@Resource
	SwiftIdentifierHandler<M> swiftIdentifierHandler;

	@Resource
	private SwiftUtilHandler<M> utilService;

	@Resource
	private AdvancedUpdatableDAO<SwiftIdentifier, Criteria> swiftIdentifierDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftSourceSystem, Criteria> swiftSourceSystemDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftSourceSystemModule, Criteria> swiftSourceSystemModuleDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftMessageStatus, Criteria> swiftMessageStatusDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftMessage, Criteria> swiftMessageDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftMessageType, Criteria> swiftMessageTypeDAO;


	@SuppressWarnings("unchecked")
	@BeforeEach
	public void before() {
		Mockito.reset(this.swiftIdentifierDAO, this.swiftSourceSystemDAO, this.swiftSourceSystemModuleDAO, this.swiftMessageStatusDAO, this.swiftMessageDAO, this.swiftMessageTypeDAO);
	}


	@SuppressWarnings("unchecked")
	@Test
	public void getSwiftIdentifierDuplicate() {
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftIdentifier.setId(56872L);
		swiftIdentifier.setUniqueStringIdentifier("IMS:M2M:56872");

		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 11);
		swiftSourceSystem.setName("IMS");

		Mockito.when(this.swiftSourceSystemDAO.findOneByField("name", "IMS")).thenReturn(swiftSourceSystem);

		Mockito.when(this.swiftMessageStatusDAO.findBySearchCriteria(Mockito.any())).thenReturn(Collections.singletonList(new SwiftMessageStatus()));

		Mockito.when(this.swiftMessageTypeDAO.findBySearchCriteria(Mockito.any())).thenReturn(Collections.singletonList(new SwiftMessageType()));

		Mockito.when(this.swiftIdentifierDAO.findBySearchCriteria(Mockito.any(SearchConfigurer.class))).then(invocation -> {
			List<Object> args = CollectionUtils.createList(invocation.getArguments());
			if (!args.isEmpty()) {
				HibernateSearchFormConfigurer searchFormConfigurer = (HibernateSearchFormConfigurer) args.get(0);
				SwiftIdentifierSearchForm searchForm = (SwiftIdentifierSearchForm) ReflectionTestUtils.getField(searchFormConfigurer, "sortableSearchForm");
				Assertions.assertNotNull(searchForm);
				String uniqueIdentifier = searchForm.getUniqueStringIdentifier();
				MatcherAssert.assertThat(uniqueIdentifier, IsEqual.equalTo(swiftIdentifier.getUniqueStringIdentifier()));
				return Collections.singletonList(swiftIdentifier);
			}
			return null;
		});

		Mockito.when(this.swiftMessageDAO.findBySearchCriteria(Mockito.any())).thenReturn(Collections.singletonList(new SwiftMessage()));

		SwiftMessagingMessage swiftMessagingMessage = new SwiftMessagingMessage();
		swiftMessagingMessage.setSourceSystemName("IMS");
		swiftMessagingMessage.setUniqueStringIdentifier(swiftIdentifier.getUniqueStringIdentifier());
		swiftMessagingMessage.setSourceSystemModuleName("M2M");
		swiftMessagingMessage.setMessageFormat(SwiftMessageFormats.MT);
		swiftMessagingMessage.setMessageText(TEST_INSTRUCTION_MESSAGE);

		M message = this.utilService.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);

		Assertions.assertThrows(RuntimeException.class, () -> {
			SwiftIdentifier returned = this.swiftIdentifierHandler.getOrCreateSwiftIdentifier(swiftMessagingMessage, message);
			MatcherAssert.assertThat(returned.getUniqueStringIdentifier(), IsEqual.equalTo("IMS:M2M:56872"));
		});
	}


	@Test
	public void testSwift671Message() {
		SwiftMessageType swiftMessageType = new SwiftMessageType();
		swiftMessageType.setId((short) 57);
		swiftMessageType.setName("SSI Update Notification");
		swiftMessageType.setMtVersionName("MT671");
		swiftMessageType.setSystemMessage(true);
		Mockito.when(this.swiftMessageTypeDAO.findBySearchCriteria(Mockito.any())).thenReturn(Collections.singletonList(swiftMessageType));
		M message = this.utilService.parseSingleMessage(TEST_671_MESSAGE, SwiftMessageFormats.MT);
		List<SwiftIdentifier> list = this.swiftIdentifierHandler.getSwiftIdentifierList(message);
		Assertions.assertTrue(list.isEmpty());
	}


	@SuppressWarnings("unchecked")
	@Test
	public void getSwiftIdentifierErrorStatus() {
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftIdentifier.setId(56872L);
		swiftIdentifier.setUniqueStringIdentifier("IMS:M2M:56872");

		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 11);
		swiftSourceSystem.setName("IMS");

		Mockito.when(this.swiftSourceSystemDAO.findOneByField("name", "IMS")).thenReturn(swiftSourceSystem);

		Mockito.when(this.swiftMessageStatusDAO.findBySearchCriteria(Mockito.any())).thenReturn(Collections.singletonList(new SwiftMessageStatus()));

		Mockito.when(this.swiftMessageTypeDAO.findBySearchCriteria(Mockito.any())).thenReturn(Collections.singletonList(new SwiftMessageType()));

		Mockito.when(this.swiftIdentifierDAO.findBySearchCriteria(Mockito.any(SearchConfigurer.class))).then(invocation -> {
			List<Object> args = CollectionUtils.createList(invocation.getArguments());
			if (!args.isEmpty()) {
				HibernateSearchFormConfigurer searchFormConfigurer = (HibernateSearchFormConfigurer) args.get(0);
				SwiftIdentifierSearchForm searchForm = (SwiftIdentifierSearchForm) ReflectionTestUtils.getField(searchFormConfigurer, "sortableSearchForm");
				Assertions.assertNotNull(searchForm);
				String uniqueIdentifier = searchForm.getUniqueStringIdentifier();
				MatcherAssert.assertThat(uniqueIdentifier, IsEqual.equalTo(swiftIdentifier.getUniqueStringIdentifier()));
				return Collections.singletonList(swiftIdentifier);
			}
			return null;
		});

		SwiftMessagingMessage swiftMessagingMessage = new SwiftMessagingMessage();
		swiftMessagingMessage.setSourceSystemName("IMS");
		swiftMessagingMessage.setUniqueStringIdentifier(swiftIdentifier.getUniqueStringIdentifier());
		swiftMessagingMessage.setSourceSystemModuleName("M2M");
		swiftMessagingMessage.setMessageFormat(SwiftMessageFormats.MT);
		swiftMessagingMessage.setMessageText(TEST_INSTRUCTION_MESSAGE);

		M message = this.utilService.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);

		SwiftIdentifier returned = this.swiftIdentifierHandler.getOrCreateSwiftIdentifier(swiftMessagingMessage, message);
		MatcherAssert.assertThat(returned.getUniqueStringIdentifier(), IsEqual.equalTo("IMS:M2M:56872"));
	}


	@SuppressWarnings("unchecked")
	@Test
	public void createSwiftIdentifier() {
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftIdentifier.setId(56872L);
		swiftIdentifier.setUniqueStringIdentifier("IMS:M2M:56872");

		Mockito.when(this.swiftIdentifierDAO.save(Mockito.any(SwiftIdentifier.class))).then(invocation -> invocation.getArguments()[0]);

		Mockito.when(this.swiftIdentifierDAO.findBySearchCriteria(Mockito.any(SearchConfigurer.class))).then(invocation -> {
			// this means it doesn't already exist, force it to create one.
			return null;
		});

		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 11);
		swiftSourceSystem.setName("IMS");

		Mockito.when(this.swiftSourceSystemDAO.findOneByField("name", "IMS")).thenReturn(swiftSourceSystem);

		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
		swiftSourceSystemModule.setId((short) 1);
		swiftSourceSystemModule.setName("M2M");
		swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);
		swiftSourceSystemModule.setTargetMessageFormat(SwiftMessageFormats.MT);

		Mockito.when(this.swiftSourceSystemModuleDAO.findBySearchCriteria(Mockito.any(SearchConfigurer.class))).thenAnswer(invocation -> {
			HibernateSearchFormConfigurer searchFormConfigurer = (HibernateSearchFormConfigurer) invocation.getArguments()[0];
			SwiftSourceSystemModuleSearchForm searchForm = (SwiftSourceSystemModuleSearchForm) ReflectionTestUtils.getField(searchFormConfigurer, "sortableSearchForm");
			Assertions.assertNotNull(searchForm);
			if (searchForm.getName().equals(swiftSourceSystemModule.getName()) && searchForm.getSourceSystemId().equals(swiftSourceSystem.getId())) {
				return Collections.singletonList(swiftSourceSystemModule);
			}
			return null;
		});

		SwiftMessagingMessage swiftMessagingMessage = new SwiftMessagingMessage();
		swiftMessagingMessage.setSourceSystemName(swiftSourceSystem.getName());
		swiftMessagingMessage.setUniqueStringIdentifier(swiftIdentifier.getUniqueStringIdentifier());
		swiftMessagingMessage.setSourceSystemModuleName(swiftSourceSystemModule.getName());
		swiftMessagingMessage.setMessageFormat(SwiftMessageFormats.MT);
		swiftMessagingMessage.setMessageText(TEST_INSTRUCTION_MESSAGE);
		swiftMessagingMessage.setSourceSystemFkFieldId(12345L);

		M message = this.utilService.parseSingleMessage(TEST_INSTRUCTION_MESSAGE, SwiftMessageFormats.MT);

		SwiftIdentifier returned = this.swiftIdentifierHandler.getOrCreateSwiftIdentifier(swiftMessagingMessage, message);
		Assertions.assertNotNull(returned);
		MatcherAssert.assertThat(returned.getUniqueStringIdentifier(), IsEqual.equalTo(swiftIdentifier.getUniqueStringIdentifier()));
		MatcherAssert.assertThat(returned.getSourceSystemFkFieldId(), IsEqual.equalTo(12345L));
		MatcherAssert.assertThat(returned.getSourceSystemModule().getId(), IsEqual.equalTo((short) 1));

		Mockito.verify(this.swiftSourceSystemDAO, Mockito.times(1)).findOneByField("name", "IMS");
		Mockito.verify(this.swiftSourceSystemModuleDAO, Mockito.times(1)).findBySearchCriteria(Mockito.any(SearchConfigurer.class));
		Mockito.verify(this.swiftIdentifierDAO, Mockito.times(1)).save(Mockito.any(SwiftIdentifier.class));
	}
}
