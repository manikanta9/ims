package com.clifton.swift;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Stream;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SwiftProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "swift";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("com.prowidesoftware.");
		imports.add("javax.xml.datatype.");
		imports.add("com.thoughtworks.xstream");
		imports.add("org.springframework.beans.BeansException");
		imports.add("org.springframework.beans.factory.config.BeanFactoryPostProcessor");
		imports.add("org.springframework.beans.factory.config.ConfigurableListableBeanFactory");
		imports.add("org.springframework.beans.factory.BeanFactory");
		imports.add("org.springframework.beans.factory.config.ConfigurableBeanFactory");
		imports.add("org.springframework.context.annotation.Scope");

		// this allows posting to the JSM error queue
		imports.add("org.springframework.jms.core.JmsTemplate");
		imports.add("javax.jms.Message");

		// field format locator and format classes.
		imports.add("org.springframework.beans.factory.config.AutowireCapableBeanFactory");
		imports.add("javax.annotation.PostConstruct");
		imports.add("javax.annotation.Resource");
	}


	@Override
	protected void configureApprovedTestClassImports(@SuppressWarnings("unused") List<String> imports) {
		imports.add("org.springframework.test.util.ReflectionTestUtils");
		imports.add("com.prowidesoftware.swift");
		imports.add("javax.sql.DataSource");
		imports.add("javax.xml.datatype");
		imports.add("com.thoughtworks.xstream");
		imports.add("org.springframework.beans.factory");
		imports.add("com.google.common.collect.MapDifference");
		imports.add("com.google.common.collect.Maps");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> result = super.getAllowedContextManagedBeanSuffixNames();
		result.add("Router");
		result.add("Visitor");
		return result;
	}


	@Override
	public boolean isMethodSkipped(Method method) {
		return Stream
				.of(
						"saveSwiftFieldModifier",
						"saveSwiftIdentifier"
				)
				.anyMatch(name -> name.equals(method.getName()));
	}
}
