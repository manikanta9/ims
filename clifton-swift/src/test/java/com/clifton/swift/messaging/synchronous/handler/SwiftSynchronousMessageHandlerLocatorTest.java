package com.clifton.swift.messaging.synchronous.handler;

import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageResponseMessage;
import com.clifton.swift.server.synchronous.handler.SwiftMessageRequestHandler;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;
import java.util.Map;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftSynchronousMessageHandlerLocatorTest {

	@Resource
	private SwiftSynchronousMessageHandlerLocator swiftSynchronousMessageHandlerLocator;


	@Test
	public void testMessageRequestHandler() {
		Assertions.assertNotNull(this.swiftSynchronousMessageHandlerLocator);
		@SuppressWarnings({"rawtypes", "unchecked"})
		Map<Class<? extends SynchronousRequestMessage>, SwiftSynchronousMessageHandler> handlerMap = (Map<Class<? extends SynchronousRequestMessage>, SwiftSynchronousMessageHandler>)
				ReflectionTestUtils.getField(this.swiftSynchronousMessageHandlerLocator, "handlerMap");
		Assertions.assertNotNull(handlerMap);
		Assertions.assertFalse(handlerMap.isEmpty());
		Assertions.assertTrue(handlerMap.keySet().size() >= 5);
		SwiftMessageRequestHandler swiftMessageRequestHandler = (SwiftMessageRequestHandler) this.swiftSynchronousMessageHandlerLocator.locate(SwiftMessageRequestMessage.class);
		Assertions.assertNotNull(swiftMessageRequestHandler);
		swiftMessageRequestHandler.setSwiftServerMessageService(Mockito.mock(SwiftMessageService.class));
		MatcherAssert.assertThat(SwiftMessageRequestMessage.class, IsEqual.equalTo(swiftMessageRequestHandler.getSupportedRequestType()));
		SwiftMessageResponseMessage swiftMessageResponseMessage = swiftMessageRequestHandler.handle(new SwiftMessageRequestMessage());
		Assertions.assertNotNull(swiftMessageResponseMessage);
	}
}
