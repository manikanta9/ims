package com.clifton.swift.messaging;


import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class XmlMessagingTest {

	@Test
	public void testXmlLoadListResponse() {
		SwiftMessagingMessage message = XmlMessagingTestHelper.loadFromFile("com/clifton/swift/messaging/SwiftMessagingMessage-message.xml");
		Assertions.assertNotNull(message);

		Assertions.assertEquals(message.getProperties().get("x"), "y");
		Assertions.assertEquals(message.getProperties().get("y"), "x");

		Assertions.assertEquals("DUDE", message.getMessageText());
		Assertions.assertEquals("MW", message.getMachineName());
	}


	@Test
	public void testToXml() {
		SwiftMessagingMessage msg = new SwiftMessagingMessage();
		msg.setMachineName("MW");
		msg.setMessageText("ABC");
		msg.getProperties().put("x", "x");
		String xml = XmlMessagingTestHelper.toXml(msg);
		Assertions.assertNotNull(xml);
	}
}
