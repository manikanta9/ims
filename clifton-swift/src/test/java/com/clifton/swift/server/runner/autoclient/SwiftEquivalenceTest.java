package com.clifton.swift.server.runner.autoclient;

import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;


public class SwiftEquivalenceTest {

	public static final String TEMPLATE = "{1:F01PPSCUS66AXXX0000000000}{2:I541SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//111111\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//333333\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			":16R:TRADDET\n" +
			":94B::TRAD//EXCH/XCME\n" +
			":98A::SETT//20170823\n" +
			":98A::TRAD//20170823\n" +
			":90B::DEAL//ACTU/USD1,\n" +
			":35B:/TS/FAU7\n" +
			"SP MID 400 EMINI Sep17\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/FUT\n" +
			":11A::DENO//USD\n" +
			":98A::EXPI//20170915\n" +
			":36B::SIZE//UNIT/100,\n" +
			":16S:FIA\n" +
			":22F::PROC//OPEP\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/7,\n" +
			":97A::SAFE//251757972\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95Q::PSET//XX\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::DEAG//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::SELL//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD15,33\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD0,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::EXEC//USD15,33\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	public static final String REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I541SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//22222\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//444444\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			":16R:TRADDET\n" +
			":94B::TRAD//EXCH/XCME\n" +
			":98A::SETT//20170823\n" +
			":98A::TRAD//20170823\n" +
			":90B::DEAL//ACTU/USD1,\n" +
			":35B:/TS/FAU7\n" +
			"SP MID 400 EMINI Sep17\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/FUT\n" +
			":11A::DENO//USD\n" +
			":98A::EXPI//20170915\n" +
			":36B::SIZE//UNIT/100,\n" +
			":16S:FIA\n" +
			":22F::PROC//OPEP\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/7,\n" +
			":97A::SAFE//1495990614\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95Q::PSET//XX\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::DEAG//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::SELL//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD2,19\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD0,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::EXEC//USD15,33\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";


	@Test
	public void equivalencyTest() {
		List<SwiftEquivalence.SwiftTagDifference> differenceList = SwiftEquivalence.areEquivalent(TEMPLATE, REQUEST).getDifferences();
		Assertions.assertFalse(differenceList.isEmpty());
		differenceList.sort(Comparator.comparing(SwiftEquivalence.SwiftTagDifference::hashCode));
		Assertions.assertEquals(4, differenceList.size());
		MatcherAssert.assertThat(differenceList.get(0), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("19A", () -> ":SETT//USD15,33", () -> ":SETT//USD2,19")));
		MatcherAssert.assertThat(differenceList.get(1), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("97A", () -> ":SAFE//251757972", () -> ":SAFE//1495990614")));
		MatcherAssert.assertThat(differenceList.get(2), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("20C", () -> ":SEME//111111", () -> ":SEME//22222")));
		MatcherAssert.assertThat(differenceList.get(3), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("20C", () -> ":PREV//333333", () -> ":PREV//444444")));
	}


	@Test
	public void equivalencyExtraTemplateFieldTest() {
		String modifiedRequest = REQUEST.replaceFirst(":22F::SETR//TRAD\n", "");
		MatcherAssert.assertThat(modifiedRequest, IsNot.not(IsEqual.equalTo(REQUEST)));
		List<SwiftEquivalence.SwiftTagDifference> differenceList = SwiftEquivalence.areEquivalent(TEMPLATE, modifiedRequest).getDifferences();
		Assertions.assertFalse(differenceList.isEmpty());
		differenceList.sort(Comparator.comparing(SwiftEquivalence.SwiftTagDifference::hashCode));
		Assertions.assertEquals(5, differenceList.size());
		MatcherAssert.assertThat(differenceList.get(0), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("22F", () -> ":SETR//TRAD", () -> "")));
		MatcherAssert.assertThat(differenceList.get(1), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("19A", () -> ":SETT//USD15,33", () -> ":SETT//USD2,19")));
		MatcherAssert.assertThat(differenceList.get(2), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("97A", () -> ":SAFE//251757972", () -> ":SAFE//1495990614")));
		MatcherAssert.assertThat(differenceList.get(3), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("20C", () -> ":SEME//111111", () -> ":SEME//22222")));
		MatcherAssert.assertThat(differenceList.get(4), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("20C", () -> ":PREV//333333", () -> ":PREV//444444")));
	}


	@Test
	public void equivalencyExtraRequestFieldTest() {
		String modifiedTemplate = TEMPLATE.replaceFirst(":22F::SETR//TRAD\n", "");
		MatcherAssert.assertThat(modifiedTemplate, IsNot.not(IsEqual.equalTo(TEMPLATE)));
		List<SwiftEquivalence.SwiftTagDifference> differenceList = SwiftEquivalence.areEquivalent(modifiedTemplate, REQUEST).getDifferences();
		Assertions.assertFalse(differenceList.isEmpty());
		differenceList.sort(Comparator.comparing(SwiftEquivalence.SwiftTagDifference::hashCode));
		Assertions.assertEquals(5, differenceList.size());
		MatcherAssert.assertThat(differenceList.get(0), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("22F", () -> "", () -> ":SETR//TRAD")));
		MatcherAssert.assertThat(differenceList.get(1), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("19A", () -> ":SETT//USD15,33", () -> ":SETT//USD2,19")));
		MatcherAssert.assertThat(differenceList.get(2), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("97A", () -> ":SAFE//251757972", () -> ":SAFE//1495990614")));
		MatcherAssert.assertThat(differenceList.get(3), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("20C", () -> ":SEME//111111", () -> ":SEME//22222")));
		MatcherAssert.assertThat(differenceList.get(4), IsEqual.equalTo(SwiftEquivalence.SwiftTagDifference.of("20C", () -> ":PREV//333333", () -> ":PREV//444444")));
	}


	@Test
	public void replacementTest() throws IOException {
		SwiftExpectedMessage swiftExpectedMessage = SwiftExpectedMessage.SwiftExpectedMessageBuilder
				.create(TEMPLATE)
				.withReplacement("97A", ":SAFE//251757972", ":SAFE//1495990614")
				.withReplacement("19A", ":SETT//USD15,33", ":SETT//USD2,19")
				.withCopyRequestValue("20C", "SEME")
				.withCopyRequestValue("20C", "PREV")
				.build();
		String replaced = swiftExpectedMessage.getComparisonTemplate(REQUEST);
		List<SwiftEquivalence.SwiftTagDifference> differenceList = SwiftEquivalence.areEquivalent(replaced, REQUEST).getDifferences();
		Assertions.assertTrue(differenceList.isEmpty(), differenceList.toString());
	}
}
