package com.clifton.swift.server.runner.autoclient;

import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Test;

import java.io.IOException;


public class SwiftExpectedMessageTest {

	private static final String TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE = "{1:F01PPSCUS66AXXX0000000000}{2:I202CNORUS44XETDN}{4:\r\n" +
			":20:templateRequest\r\n" +
			":21:MARG\r\n" +
			":32A:171018USD2220,00\r\n" +
			":53B:/RSHP12\r\n" +
			":57A:IRVTUS3NXXX\r\n" +
			":58A:/89-0000-6978\r\n" +
			"MSNYUS33\r\n" +
			":72:/BNF/052BAINO/\r\n" +
			"-}";

	private static final String TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE = "{1:F01PPSCUS66AXXX0000000000}{2:I202CNORUS44XETDN}{4:\r\n" +
			":20:actualRequest\r\n" +
			":21:MARG\r\n" +
			":32A:171018USD2220,00\r\n" +
			":53B:/RSHP12\r\n" +
			":57A:IRVTUS3NXXX\r\n" +
			":58A:/89-0000-6978\r\n" +
			"MSNYUS33\r\n" +
			":72:/BNF/052BAINO/\r\n" +
			"-}";


	@Test
	public void baselineTest() throws IOException {
		SwiftExpectedMessage swiftExpectedRequestMessage = SwiftExpectedMessage.SwiftExpectedMessageBuilder.create(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE).build();
		String unmodifiedTemplate = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(unmodifiedTemplate, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE));
		String stillUnmodifiedTemplate = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(stillUnmodifiedTemplate, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE));
		String templateOnly = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(templateOnly, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE));
	}


	@Test
	public void comparisonTemplateTest() throws IOException {
		SwiftExpectedMessage swiftExpectedRequestMessage = SwiftExpectedMessage.SwiftExpectedMessageBuilder.create(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE)
				.withCopyRequestValue("20")
				.build();
		String unmodifiedTemplate = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(unmodifiedTemplate, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE));
		String modifiedTemplate = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(modifiedTemplate, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE.replace("templateRequest", "actualRequest")));
	}


	@Test
	public void performComparisonReplacementsTest() throws IOException {
		SwiftExpectedMessage swiftExpectedRequestMessage = SwiftExpectedMessage.SwiftExpectedMessageBuilder.create(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE)
				.withReplacement("20", "doesNotMatch", "replacementValue")
				.build();
		String unmodifiedTemplate = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(unmodifiedTemplate, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE));
		String modifiedTemplate = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(modifiedTemplate, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE));
		swiftExpectedRequestMessage = SwiftExpectedMessage.SwiftExpectedMessageBuilder.create(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE)
				.withReplacement("20", "templateRequest", "replacementValue")
				.build();
		modifiedTemplate = swiftExpectedRequestMessage.getComparisonTemplate(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(modifiedTemplate, IsEqual.equalTo(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE.replace("templateRequest", "replacementValue")));
		modifiedTemplate = swiftExpectedRequestMessage.performComparisonReplacements(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(modifiedTemplate, IsEqual.equalTo(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE));
		swiftExpectedRequestMessage = SwiftExpectedMessage.SwiftExpectedMessageBuilder.create(TEST_REQUEST_TEMPLATE_INSTRUCTION_MESSAGE)
				.withReplacement("20", "actualRequest", "replacementValue")
				.build();
		modifiedTemplate = swiftExpectedRequestMessage.performComparisonReplacements(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE);
		MatcherAssert.assertThat(modifiedTemplate, IsEqual.equalTo(TEST_REQUEST_ACTUAL_INSTRUCTION_MESSAGE.replace("actualRequest", "replacementValue")));
	}
}
