package com.clifton.swift.system;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.swift.message.SwiftMessageFormats;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@Transactional
@Rollback
public class SwiftSourceSystemServiceTest {

	@Resource
	public ContextHandler contextHandler;

	@Resource
	public SwiftSourceSystemService swiftSourceSystemService;


	@BeforeEach
	public void before() {
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(999).shortValue());
		user.setUserName(SecurityUser.SYSTEM_USER);
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@AfterEach
	public void after() {
		this.contextHandler.removeBean(Context.USER_BEAN_NAME);
	}


	@Test
	public void testBasicEntityPersistence() {
		Assertions.assertNotNull(this.swiftSourceSystemService);

		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setDescription("This is the source system description.");
		swiftSourceSystem.setLabel("Source System Label");
		swiftSourceSystem.setName("Source System Name");

		this.swiftSourceSystemService.saveSwiftSourceSystem(swiftSourceSystem);
		swiftSourceSystem = this.swiftSourceSystemService.getSwiftSourceSystemByName("Source System Name");
		Assertions.assertNotNull(swiftSourceSystem);
		swiftSourceSystem = this.swiftSourceSystemService.getSwiftSourceSystem(swiftSourceSystem.getId());
		Assertions.assertNotNull(swiftSourceSystem);

		MatcherAssert.assertThat(swiftSourceSystem.getName(), IsEqual.equalTo("Source System Name"));
		MatcherAssert.assertThat(swiftSourceSystem.getLabel(), IsEqual.equalTo("Source System Label"));
		MatcherAssert.assertThat(swiftSourceSystem.getDescription(), IsEqual.equalTo("This is the source system description."));

		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
		swiftSourceSystemModule.setTargetMessageFormat(SwiftMessageFormats.MT);
		swiftSourceSystemModule.setName("Source System Module Name");
		swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);
		swiftSourceSystemModule.setLabel("Source System Module Label");
		swiftSourceSystemModule.setDescription("Source System Module Description");

		this.swiftSourceSystemService.saveSwiftSourceSystemModule(swiftSourceSystemModule);
		swiftSourceSystemModule = this.swiftSourceSystemService.getSwiftSourceSystemModuleByName("Source System Module Name", swiftSourceSystem.getId());
		Assertions.assertNotNull(swiftSourceSystemModule);
		swiftSourceSystemModule = this.swiftSourceSystemService.getSwiftSourceSystemModule(swiftSourceSystemModule.getId());
		Assertions.assertNotNull(swiftSourceSystemModule);

		MatcherAssert.assertThat(swiftSourceSystemModule.getName(), IsEqual.equalTo("Source System Module Name"));
		MatcherAssert.assertThat(swiftSourceSystemModule.getLabel(), IsEqual.equalTo("Source System Module Label"));
		MatcherAssert.assertThat(swiftSourceSystemModule.getDescription(), IsEqual.equalTo("Source System Module Description"));
		Assertions.assertNotNull(swiftSourceSystemModule.getTargetMessageFormat());
		Assertions.assertEquals(SwiftMessageFormats.MT, swiftSourceSystemModule.getTargetMessageFormat());
		Assertions.assertNotNull(swiftSourceSystemModule.getSourceSystem());
	}
}
