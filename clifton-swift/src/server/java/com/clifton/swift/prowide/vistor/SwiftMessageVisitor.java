package com.clifton.swift.prowide.vistor;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.prowide.message.converter.SwiftFieldValueConverter;
import com.clifton.swift.prowide.message.converter.SwiftFieldValueConverterLocator;
import com.clifton.swift.prowide.util.ProwideFieldLabelHandler;
import com.prowidesoftware.swift.model.SwiftBlock;
import com.prowidesoftware.swift.model.SwiftBlock1;
import com.prowidesoftware.swift.model.SwiftBlock2;
import com.prowidesoftware.swift.model.SwiftBlock2Input;
import com.prowidesoftware.swift.model.SwiftBlock2Output;
import com.prowidesoftware.swift.model.SwiftBlock3;
import com.prowidesoftware.swift.model.SwiftBlock4;
import com.prowidesoftware.swift.model.SwiftBlock5;
import com.prowidesoftware.swift.model.SwiftBlockUser;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.Tag;
import com.prowidesoftware.swift.model.field.Field;
import com.prowidesoftware.swift.utils.IMessageVisitor;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * <code>SwiftMessageVisitor</code> this must be a prototype bean.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SwiftMessageVisitor implements IMessageVisitor {

	@Resource
	private ProwideFieldLabelHandler prowideFieldLabelHandler;

	@Resource
	private SwiftFieldValueConverterLocator<Field> swiftFieldValueFormatterLocator;

	private SwiftMessage currentSwiftMessage;
	private Deque<SwiftMessageField> messages = new ArrayDeque<>();
	private Map<SwiftBlock, SwiftMessageField> blocks = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void reset() {
		this.currentSwiftMessage = null;
		this.messages.clear();
		this.blocks.clear();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void startMessage(SwiftMessage swiftMessage) {
		this.currentSwiftMessage = swiftMessage;
		String label = (swiftMessage.isServiceMessage21()) ? (swiftMessage.isAck()) ? "ACK" : "NACK" : swiftMessage.getType();
		SwiftMessageField swiftMessageField = new SwiftMessageField(label, null, "");
		swiftMessageField.setChildren(new ArrayList<>());
		this.messages.add(swiftMessageField);
	}


	@Override
	public void endMessage(SwiftMessage swiftMessage) {
		this.currentSwiftMessage = null;
	}


	@Override
	public void startBlock1(SwiftBlock1 swiftBlock) {
		SwiftMessageField swiftMessageBlockField = new SwiftMessageField("Block" + swiftBlock.getName());
		this.blocks.put(swiftBlock, swiftMessageBlockField);

		List<SwiftMessageField> children = new ArrayList<>();
		SwiftMessageField swiftMessageField = new SwiftMessageField("Application", null, swiftBlock.getApplicationId());
		children.add(swiftMessageField);
		swiftMessageField = new SwiftMessageField("Service", null, swiftBlock.getServiceId());
		children.add(swiftMessageField);
		swiftMessageField = new SwiftMessageField("Logical Terminal", null, swiftBlock.getLogicalTerminal());
		children.add(swiftMessageField);
		swiftMessageField = new SwiftMessageField("Session Number", null, swiftBlock.getSessionNumber());
		children.add(swiftMessageField);
		swiftMessageField = new SwiftMessageField("Sequence Number", null, swiftBlock.getSequenceNumber());
		children.add(swiftMessageField);

		swiftMessageBlockField.setChildren(children);
		this.messages.getLast().getChildren().add(swiftMessageBlockField);
	}


	@Override
	public void endBlock1(SwiftBlock1 swiftBlock1) {
		// nothing.
	}


	@Override
	public void startBlock2(SwiftBlock2 swiftBlock) {
		SwiftMessageField swiftMessageBlockField = new SwiftMessageField("Block" + swiftBlock.getName());
		this.blocks.put(swiftBlock, swiftMessageBlockField);

		List<SwiftMessageField> children = new ArrayList<>();
		SwiftBlock2.MessagePriority messagePriority = swiftBlock.getMessagePriorityType();
		if (messagePriority != null) {
			SwiftMessageField swiftMessageField = new SwiftMessageField("Message Priority", null, swiftBlock.getMessagePriority(), messagePriority.getLabel());
			children.add(swiftMessageField);
		}
		SwiftMessageField swiftMessageField = new SwiftMessageField("Message Type", null, swiftBlock.getMessageType());
		children.add(swiftMessageField);
		swiftMessageField = new SwiftMessageField("Direction", null, swiftBlock.isInput() ? "Incoming" : "Outgoing");
		children.add(swiftMessageField);
		if (SwiftBlock2Input.class.isAssignableFrom(swiftBlock.getClass())) {
			SwiftBlock2Input swiftBlock2Input = SwiftBlock2Input.class.cast(swiftBlock);
			swiftMessageField = new SwiftMessageField("Receiver Address", null, swiftBlock2Input.getReceiverAddress());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("Delivery Monitoring", null, swiftBlock2Input.getDeliveryMonitoring());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("Obsolescence Period", null, swiftBlock2Input.getObsolescencePeriod());
			children.add(swiftMessageField);
		}
		else if (SwiftBlock2Output.class.isAssignableFrom(swiftBlock.getClass())) {
			SwiftBlock2Output swiftBlock2Output = SwiftBlock2Output.class.cast(swiftBlock);
			swiftMessageField = new SwiftMessageField("Sender Input Time", null, swiftBlock2Output.getSenderInputTime());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("MIR Date", null, swiftBlock2Output.getMIRDate());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("MIR Logical Terminal", null, swiftBlock2Output.getMIRLogicalTerminal());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("MIR Session Number", null, swiftBlock2Output.getMIRSessionNumber());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("MIR Sequence Number", null, swiftBlock2Output.getMIRSequenceNumber());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("Receiver Output Date", null, swiftBlock2Output.getReceiverOutputDate());
			children.add(swiftMessageField);
			swiftMessageField = new SwiftMessageField("Receiver Output Time", null, swiftBlock2Output.getReceiverOutputTime());
			children.add(swiftMessageField);
		}

		swiftMessageBlockField.setChildren(children);
		this.messages.getLast().getChildren().add(swiftMessageBlockField);
	}


	@Override
	public void endBlock2(SwiftBlock2 swiftBlock2) {
		// nothing
	}


	@Override
	public void startBlock3(SwiftBlock3 swiftBlock3) {
		SwiftMessageField swiftMessageBlockField = new SwiftMessageField("Block" + swiftBlock3.getName());
		this.blocks.put(swiftBlock3, swiftMessageBlockField);
		swiftMessageBlockField.setChildren(new ArrayList<>());
		this.messages.getLast().getChildren().add(swiftMessageBlockField);
	}


	@Override
	public void endBlock3(SwiftBlock3 swiftBlock3) {
		// nothing
	}


	@Override
	public void startBlock4(SwiftBlock4 swiftBlock4) {
		SwiftMessageField swiftMessageBlockField = new SwiftMessageField("Block" + swiftBlock4.getName());
		this.blocks.put(swiftBlock4, swiftMessageBlockField);
		swiftMessageBlockField.setChildren(new ArrayList<>());
		this.messages.getLast().getChildren().add(swiftMessageBlockField);
	}


	@Override
	public void endBlock4(SwiftBlock4 swiftBlock4) {
		// nothing
	}


	@Override
	public void startBlock5(SwiftBlock5 swiftBlock5) {
		SwiftMessageField swiftMessageBlockField = new SwiftMessageField("Block" + swiftBlock5.getName());
		this.blocks.put(swiftBlock5, swiftMessageBlockField);
		swiftMessageBlockField.setChildren(new ArrayList<>());
		this.messages.getLast().getChildren().add(swiftMessageBlockField);
	}


	@Override
	public void endBlock5(SwiftBlock5 swiftBlock5) {
		// nothing
	}


	@Override
	public void startBlockUser(SwiftBlockUser swiftBlockUser) {
		SwiftMessageField swiftMessageBlockField = new SwiftMessageField("Block" + swiftBlockUser.getName());
		this.blocks.put(swiftBlockUser, swiftMessageBlockField);
		List<SwiftMessageField> children = new ArrayList<>();
		SwiftMessageField swiftMessageField = new SwiftMessageField("Block Name", null, swiftBlockUser.getBlockName());
		children.add(swiftMessageField);
		Integer sortKey = swiftBlockUser.getSortKey();
		if (sortKey != null) {
			swiftMessageField = new SwiftMessageField("Sort Key", null, swiftBlockUser.getSortKey().toString());
		}
		children.add(swiftMessageField);

		swiftMessageBlockField.setChildren(children);
		this.messages.getLast().getChildren().add(swiftMessageBlockField);
	}


	@Override
	public void endBlockUser(SwiftBlockUser swiftBlockUser) {
		// nothing
	}


	@Override
	public void tag(SwiftBlock3 swiftBlock3, Tag tag) {
		SwiftMessageField swiftMessageField = getSwiftMessageField(tag);
		SwiftMessageField swiftMessageBlockField = this.blocks.get(swiftBlock3);
		swiftMessageBlockField.getChildren().add(swiftMessageField);
	}


	@Override
	public void tag(SwiftBlock4 swiftBlock4, Tag tag) {
		SwiftMessageField swiftMessageField = getSwiftMessageField(tag);
		SwiftMessageField swiftMessageBlockField = this.blocks.get(swiftBlock4);
		swiftMessageBlockField.getChildren().add(swiftMessageField);
	}


	@Override
	public void tag(SwiftBlock5 swiftBlock5, Tag tag) {
		SwiftMessageField swiftMessageField = getSwiftMessageField(tag);
		SwiftMessageField swiftMessageBlockField = this.blocks.get(swiftBlock5);
		swiftMessageBlockField.getChildren().add(swiftMessageField);
	}


	@Override
	public void tag(SwiftBlockUser swiftBlockUser, Tag tag) {
		SwiftMessageField swiftMessageField = getSwiftMessageField(tag);
		SwiftMessageField swiftMessageBlockField = this.blocks.get(swiftBlockUser);
		swiftMessageBlockField.getChildren().add(swiftMessageField);
	}


	@Override
	public void value(SwiftBlock1 swiftBlock1, String value) {
		SwiftMessageField swiftMessageField = this.blocks.get(swiftBlock1);
		swiftMessageField.setValue(value);
	}


	@Override
	public void value(SwiftBlock2 swiftBlock2, String value) {
		SwiftMessageField swiftMessageField = this.blocks.get(swiftBlock2);
		swiftMessageField.setValue(value);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SwiftMessageField getSwiftMessageField(Tag tag) {
		String label = getFieldLabel(tag);
		String value = tag.getValue();
		String number = tag.getName();
		SwiftMessageField swiftMessageField = new SwiftMessageField(label, number, value);
		Field field = tag.asField();
		if (field != null) {
			String formatted = field.getValueDisplay(Locale.ENGLISH);
			if (!StringUtils.isEqual(formatted, value)) {
				swiftMessageField.setFormatted(formatted);
			}
			List<String> components = field.getComponents();
			if (!components.isEmpty()) {
				List<SwiftMessageField> children = getSwiftMessageFieldList(field);
				if (children.size() == 1) {
					SwiftMessageField componentField = children.get(0);
					swiftMessageField.setValue(componentField.getValue());
					if (!StringUtils.isEmpty(componentField.getFormatted())) {
						swiftMessageField.setFormatted(componentField.getFormatted());
					}
				}
				else {
					swiftMessageField.setChildren(children);
				}
			}
		}

		return swiftMessageField;
	}


	private List<SwiftMessageField> getSwiftMessageFieldList(Field field) {
		List<SwiftMessageField> children = new ArrayList<>();
		List<String> components = field.getComponents();
		String[] formatted = getFormattedValues(field);
		for (int i = 0; i < components.size(); i++) {
			String fieldLabel = field.getComponentLabel(i + 1);
			String value = StringUtils.isEmpty(components.get(i)) ? "" : components.get(i);
			SwiftMessageField swiftMessageField = new SwiftMessageField(fieldLabel, null, value);
			String formattedValue = field.getValueDisplay(i + 1, Locale.ENGLISH);
			if (!StringUtils.isEmpty(formatted[i])) {
				swiftMessageField.setFormatted(formatted[i]);
			}
			else if (!StringUtils.isEqual(formattedValue, value)) {
				swiftMessageField.setFormatted(formattedValue);
			}
			children.add(swiftMessageField);
		}
		return children;
	}


	private String getFieldLabel(Tag tag) {
		String label = null;
		SwiftMessage swiftMessage = this.currentSwiftMessage;
		if (swiftMessage != null) {
			String type = swiftMessage.getType();
			if (!StringUtils.isEmpty(tag.getName()) && Character.isDigit(tag.getName().charAt(0))) {
				label = getProwideFieldLabelHandler().getMessageFieldLabel(type, tag.getName());
				List<String> componentLabels = getProwideFieldLabelHandler().getMessageComponentLabels(tag.getName());
				if (!StringUtils.isEmpty(label) && !componentLabels.isEmpty()) {
					if (label.contains(", ")) {
						componentLabels.clear();
						componentLabels.add(label);
					}
					else {
						componentLabels.add(0, label);
					}
				}
				label = (componentLabels.isEmpty()) ? label : String.join(" - ", componentLabels);
			}
		}

		return label == null ? tag.getName() : label;
	}


	private String[] getFormattedValues(Field field) {
		String[] formatted = new String[field.componentsSize()];
		String messageType = this.currentSwiftMessage.isServiceMessage21() ? "021" : this.currentSwiftMessage.getType();
		List<SwiftFieldValueConverter<Field>> formatterList = getSwiftFieldValueFormatterLocator().locate(messageType, field);
		for (SwiftFieldValueConverter<Field> formatter : formatterList) {
			String[] values = formatter.formatFieldComponents(field);
			for (int i = 0; i < values.length; i++) {
				if (!StringUtils.isEmpty(values[i])) {
					formatted[i] = values[i];
				}
			}
		}
		return formatted;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Collection<SwiftMessageField> getSwiftMessageFieldList() {
		return this.messages;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ProwideFieldLabelHandler getProwideFieldLabelHandler() {
		return this.prowideFieldLabelHandler;
	}


	public void setProwideFieldLabelHandler(ProwideFieldLabelHandler prowideFieldLabelHandler) {
		this.prowideFieldLabelHandler = prowideFieldLabelHandler;
	}


	public SwiftFieldValueConverterLocator<Field> getSwiftFieldValueFormatterLocator() {
		return this.swiftFieldValueFormatterLocator;
	}


	public void setSwiftFieldValueFormatterLocator(SwiftFieldValueConverterLocator<Field> swiftFieldValueFormatterLocator) {
		this.swiftFieldValueFormatterLocator = swiftFieldValueFormatterLocator;
	}
}
