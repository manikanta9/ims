package com.clifton.swift.prowide.message.converter;

/**
 * <code>SwiftFieldValueConverter</code> Format message-specific field values to human readable form.
 */
public interface SwiftFieldValueConverter<F> {

	public boolean accepts(String messageNumber, F field);


	public String[] formatFieldComponents(F field);
}
