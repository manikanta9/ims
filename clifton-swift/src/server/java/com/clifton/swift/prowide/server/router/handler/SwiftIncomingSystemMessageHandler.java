package com.clifton.swift.prowide.server.router.handler;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.server.handler.SwiftIncomingMessageHandler;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.prowidesoftware.swift.model.AbstractMessage;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


/**
 * <code>SwiftIncomingSystemMessageHandler</code> Handles messages marked as {@link SwiftMessageType#isSystemMessage()}.
 * <p>
 * The messages are saved only; they are not sent to IMS.
 */
@Component
public class SwiftIncomingSystemMessageHandler implements SwiftIncomingMessageHandler<AbstractMessage> {

	private static final List<String> rejectedTypeNames = Arrays.asList(
			"MT202",
			"MT210",
			"MT541",
			"MT543",
			"MT292"
	);


	@Override
	public void handle(SwiftMessage swiftMessage, SwiftServerMessageRouterCommand<AbstractMessage> command) {
		String typeName = command.getMessage().isMT() ? swiftMessage.getType().getMtVersionName() : swiftMessage.getType().getMxVersionName();
		AssertUtils.assertFalse(rejectedTypeNames.contains(typeName), "The system message handler should not be used for message type " + typeName);

		// no further action is necessary for system messages, just save them to the database.
	}


	@Override
	public String[] getSupportedMessageType() {
		return new String[0];
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}
}
