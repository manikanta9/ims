package com.clifton.swift.prowide.message.converter;

import java.util.List;


/**
 * <code>SwiftFieldValueConverterLocator</code> locate the formatter for a message and field.
 */
public interface SwiftFieldValueConverterLocator<F> {

	public List<SwiftFieldValueConverter<F>> locate(String messageNumber, F field);
}
