package com.clifton.swift.prowide.util;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.prowide.vistor.SwiftMessageVisitor;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.util.SwiftServerUtilHandler;
import com.clifton.swift.util.SwiftUtilHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.SwiftMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;


/**
 * {@inheritDoc}
 */
@Component("swiftServerUtilHandler")
public class ProwideServerSwiftUtilHandlerImpl implements SwiftServerUtilHandler<AbstractMessage>, BeanFactoryAware {

	private BeanFactory beanFactory;

	private SwiftUtilHandler<AbstractMessage> swiftUtilHandler;
	private SwiftIdentifierHandler<AbstractMessage> swiftIdentifierHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SwiftMessageField> getSwiftMessageFieldList(String messageString) {
		List<SwiftMessageField> results = new ArrayList<>();

		SwiftMessageVisitor swiftMessageVisitor = getBeanFactory().getBean(SwiftMessageVisitor.class);
		for (AbstractMessage msg : getSwiftUtilHandler().parseMessage(messageString, SwiftMessageFormats.MT)) {
			List<AbstractMessage> msgList = getSwiftUtilHandler().getAllMessagesList(msg);
			for (AbstractMessage abstractMessage : msgList) {
				swiftMessageVisitor.reset();
				AbstractMT abstractMT = (AbstractMT) abstractMessage;
				SwiftMessage swiftMessage = abstractMT.getSwiftMessage();
				swiftMessage.visit(swiftMessageVisitor);
				Collection<SwiftMessageField> messageFieldList = swiftMessageVisitor.getSwiftMessageFieldList();
				results.addAll(messageFieldList);
			}
		}
		return results;
	}


	/**
	 * Convert the provided message into a SwiftMessagingMessage suitable for sending via JMS, the suppressErrors
	 * parameter will ignore errors and attempt to populate the SwiftMessagingMessage with as much information as
	 * possible without throwing any exceptions.
	 */
	@Override
	public SwiftMessagingMessage buildSwiftMessagingMessage(AbstractMessage abstractMessage, boolean suppressErrors) {
		SwiftMessagingMessage swiftMessagingMessage = new SwiftMessagingMessage();

		swiftMessagingMessage.setMessageFormat(getValue(() -> ProwideSwiftUtils.findMessageFormat(abstractMessage.getMessageStandardType()), suppressErrors));
		swiftMessagingMessage.setMessageText(getValue(() -> getSwiftUtilHandler().getMessageText(abstractMessage), suppressErrors));
		SwiftIdentifier swiftIdentifier = getValue(() -> getSwiftIdentifierHandler().getSwiftIdentifierForMessage(abstractMessage), suppressErrors);
		swiftIdentifier = swiftIdentifier == null ? new SwiftIdentifier() : swiftIdentifier;
		String transactionReferenceNumber = getValue(() -> getSwiftUtilHandler().getTransactionReferenceNumber(abstractMessage), suppressErrors);
		Long messageReferenceNumber = getValue(() -> getSwiftUtilHandler().getMessageReferenceNumber(abstractMessage), suppressErrors);
		swiftMessagingMessage.setMessageReferenceNumber(messageReferenceNumber);
		String uniqueStringIdentifier = StringUtils.isEmpty(swiftIdentifier.getUniqueStringIdentifier()) ? transactionReferenceNumber : swiftIdentifier.getUniqueStringIdentifier();
		swiftMessagingMessage.setUniqueStringIdentifier(uniqueStringIdentifier);
		SwiftSourceSystemModule swiftSourceSystemModule = swiftIdentifier.getSourceSystemModule() == null ? new SwiftSourceSystemModule() : swiftIdentifier.getSourceSystemModule();
		swiftMessagingMessage.setSourceSystemModuleName(swiftSourceSystemModule.getName());
		SwiftSourceSystem swiftSourceSystem = swiftSourceSystemModule.getSourceSystem() == null ? new SwiftSourceSystem() : swiftSourceSystemModule.getSourceSystem();
		swiftMessagingMessage.setSourceSystemName(swiftSourceSystem.getName());
		swiftMessagingMessage.setSourceSystemFkFieldId(swiftIdentifier.getSourceSystemFkFieldId());

		return swiftMessagingMessage;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <V> V getValue(Supplier<V> value, boolean suppress) {
		try {
			return value.get();
		}
		catch (Exception e) {
			if (suppress) {
				return null;
			}
			else {
				throw e;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BeanFactory getBeanFactory() {
		return this.beanFactory;
	}


	public SwiftUtilHandler<AbstractMessage> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<AbstractMessage> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}


	public SwiftIdentifierHandler<AbstractMessage> getSwiftIdentifierHandler() {
		return this.swiftIdentifierHandler;
	}


	public void setSwiftIdentifierHandler(SwiftIdentifierHandler<AbstractMessage> swiftIdentifierHandler) {
		this.swiftIdentifierHandler = swiftIdentifierHandler;
	}
}
