package com.clifton.swift.prowide.message.converter;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.converter.beans.MessageReportErrorValues;
import com.prowidesoftware.swift.model.field.Field;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>SwiftErrorStatusConverter</code> handle fetching the error text for
 * specific field value report error codes.
 */
@Component
public class SwiftErrorReportConverter implements SwiftFieldValueConverter<Field> {

	private static final Map<String, List<String>> messageFields = new HashMap<>();


	static {
		List<String> fields = Collections.singletonList("461");
		messageFields.put("052", fields);
		messageFields.put("072", fields);
		messageFields.put("071", fields);
		messageFields.put("082", fields);
		messageFields.put("083", fields);
	}


	@Override
	public boolean accepts(String messageNumber, Field field) {
		return messageFields.containsKey(messageNumber) && messageFields.get(messageNumber).contains(field.getName());
	}


	@Override
	public String[] formatFieldComponents(Field field) {
		String[] formatted = new String[field.componentsSize()];
		for (int i = 0; i < field.componentsSize(); i++) {
			String code = field.getComponent(i + 1);
			if (!StringUtils.isEmpty(code)) {
				MessageReportErrorValues value = MessageReportErrorValues.fromCode(code);
				if (value != null) {
					formatted[i] = value.getLabel();
				}
				else if (MessageReportErrorValues.validateCode(code)) {
					formatted[i] = MessageReportErrorValues.getDefault().getLabel();
				}
			}
		}
		return formatted;
	}
}
