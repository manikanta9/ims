package com.clifton.swift.prowide.message.converter;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.message.bic.SwiftBicDetail;
import com.clifton.swift.message.bic.SwiftBicDetailService;
import com.prowidesoftware.swift.model.field.BICContainer;
import com.prowidesoftware.swift.model.field.Field;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


/**
 * <code>SwiftBicConverter</code> convert SWIFT field BIC values to the institution name.
 */
@Component
public class SwiftBicConverter implements SwiftFieldValueConverter<Field> {

	@Resource
	private SwiftBicDetailService swiftBicDetailService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean accepts(String messageNumber, Field field) {
		return BICContainer.class.isAssignableFrom(field.getClass());
	}


	@Override
	public String[] formatFieldComponents(final Field field) {
		String[] formatted = new String[field.componentsSize()];
		if (BICContainer.class.isAssignableFrom(field.getClass())) {
			BICContainer bicContainer = BICContainer.class.cast(field);
			List<String> bics = bicContainer.bicStrings();
			for (int i = 0; i < field.componentsSize(); i++) {
				String value = field.getComponent(i + 1);
				if (!StringUtils.isEmpty(value) && bics.contains(value)) {
					SwiftBicDetail detail = getSwiftBicDetailService().getSwiftBicDetails(value);
					if (detail != null) {
						formatted[i] = detail.getInstitutionName();
					}
				}
			}
		}
		return formatted;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftBicDetailService getSwiftBicDetailService() {
		return this.swiftBicDetailService;
	}


	public void setSwiftBicDetailService(SwiftBicDetailService swiftBicDetailService) {
		this.swiftBicDetailService = swiftBicDetailService;
	}
}
