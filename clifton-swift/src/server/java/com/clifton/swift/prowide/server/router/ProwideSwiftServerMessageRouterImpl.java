package com.clifton.swift.prowide.server.router;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.prowide.server.router.handler.SwiftIncomingOperationalMessageHandler;
import com.clifton.swift.prowide.server.router.handler.SwiftIncomingSystemMessageHandler;
import com.clifton.swift.server.handler.SwiftIncomingMessageHandler;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.util.SwiftUtilHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;
import java.util.Optional;


/**
 * {@inheritDoc}
 */
@Component("swiftServerMessageRouter")
public class ProwideSwiftServerMessageRouterImpl implements SwiftServerMessageRouter<AbstractMessage> {

	private SwiftIdentifierHandler<AbstractMessage> swiftIdentifierHandler;
	private SwiftMessageService swiftServerMessageService;
	private SwiftUtilHandler<AbstractMessage> swiftUtilHandler;
	private SwiftServerOutgoingFileMessageHandler swiftServerOutgoingFileMessageHandler;
	private SwiftIncomingOperationalMessageHandler swiftIncomingOperationalMessageHandler;
	private SwiftIncomingSystemMessageHandler swiftIncomingSystemMessageHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Send the Swift Message to Spring Integration for outputting to the emission folder.
	 */
	@Override
	public void send(SwiftMessage message) {
		AssertUtils.assertNotNull(message, "Swift message parameter is required.");
		AssertUtils.assertFalse(message.isNewBean(), "The Swift message parameter must have been persisted.");

		getSwiftServerOutgoingFileMessageHandler().send(message);
	}


	/**
	 * Received the message from the Spring Integration, send to the acknowledgement handler to send the status message to IMS.
	 */
	@Override
	@Transactional
	public void receive(SwiftServerMessageRouterCommand<AbstractMessage> command) {
		AssertUtils.assertNotNull(command.getMessage(), "Incoming Swift message cannot be null.");
		try {
			SwiftMessage swiftMessage = saveMessage(command, true);
			getSwiftIncomingMessageHandler(command.getMessage(), swiftMessage.getType()).handle(swiftMessage, command);
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to processes incoming SWIFT message [" + command.getMessage() + "].", e);
			throw new RuntimeException("Failed to processes incoming SWIFT message. Cause:  '" + e.getMessage() + "'", e);
		}
	}


	/**
	 * Save SwiftMessage to the database, command may be updated with the identifier list, return the saved swift message entity.
	 */
	@Override
	public SwiftMessage saveMessage(SwiftServerMessageRouterCommand<AbstractMessage> command, boolean incoming) {
		if (incoming) {
			ValidationUtils.assertNotNull(command.getMessage(), "Parsed message cannot be null.");
			command.setSwiftIdentifierList(getSwiftIdentifierHandler().getSwiftIdentifierList(command.getMessage()));
		}
		SwiftMessageType swiftMessageType = getMessageType(command);
		AssertUtils.assertNotNull(swiftMessageType, "Could not fetch Message type record");

		SwiftMessage swiftMessage = new SwiftMessage();
		swiftMessage.setText(!StringUtils.isEmpty(command.getMessageString()) ? command.getMessageString() : getSwiftUtilHandler().getMessageText(command.getMessage()));
		swiftMessage.setMessageDateAndTime(new Date());
		if (command.getSwiftIdentifierList().size() == 1) {
			swiftMessage.setIdentifier(command.getSwiftIdentifierList().get(0));
		}
		swiftMessage.setIncoming(incoming);
		swiftMessage.setType(swiftMessageType);
		if (incoming) {
			setAcknowledgedMessageType(command, swiftMessage);
		}
		swiftMessage.setMessageStatus(getSwiftServerMessageService().getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.LOGGED.name()));
		swiftMessage.setReceiverBusinessIdentifierCode(getSwiftUtilHandler().getReceiverBusinessIdentifierCode(command.getMessage()));
		swiftMessage.setSenderBusinessIdentifierCode(getSwiftUtilHandler().getSenderBusinessIdentifierCode(command.getMessage()));
		swiftMessage.setMessageFormat(SwiftMessageFormats.fromIsMt(command.getMessage().isMT()));

		return getSwiftServerMessageService().saveSwiftMessage(swiftMessage);
	}


	@Override
	public SwiftIncomingMessageHandler<AbstractMessage> getSwiftIncomingMessageHandler(AbstractMessage message, SwiftMessageType swiftMessageType) {
		if (message.isMT()) {
			SwiftIncomingMessageHandler<AbstractMessage> handler = null;
			if (((AbstractMT) message).getSwiftMessage().isServiceMessage21()) {
				// acknowledged message (should be all of them)
				if (SwiftMessageType.UNKNOWN_MESSAGE_TYPE_NAME.equals(swiftMessageType.getName())) {
					// add to the swift message type table?
					throw new RuntimeException("Unhandled message type " + ((AbstractMT) message).getMessageType());
				}
				if (swiftMessageType.isSystemMessage()) {
					// system message are save but not sent to IMS
					handler = getSwiftIncomingSystemMessageHandler();
				}
				else {
					// 202, 210, 541, 543; save and send to IMS
					handler = getSwiftIncomingOperationalMessageHandler();
				}
				ValidationUtils.assertTrue(SwiftMessageFormats.MT == handler.getSupportedMessageFormat(), "The handler does not support " + SwiftMessageFormats.MT);
			}
			ValidationUtils.assertNotNull(handler, "Expected a Swift message handler for the message type " + swiftMessageType.getMtVersionName());
			return handler;
		}
		throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SwiftMessageType getMessageType(SwiftServerMessageRouterCommand<AbstractMessage> command) {
		SwiftMessageType result;
		if (getSwiftUtilHandler().isSystemAcknowledgementMessage(command.getMessage())) {
			result = getSwiftServerMessageService().getSwiftMessageTypeByName(SwiftMessageService.MT_ACK_NAK_MESSAGE_TYPE_NAME);
		}
		else {
			String typeCode = getSwiftUtilHandler().getMessageTypeCode(command.getMessage());
			result = getSwiftServerMessageService().getSwiftMessageTypeVersionCode(typeCode);
			if (result == null) {
				result = getSwiftServerMessageService().getSwiftMessageTypeByName(SwiftMessageService.MT_UNKNOWN_MESSAGE_TYPE_NAME);
			}
		}
		return result;
	}


	private void setAcknowledgedMessageType(SwiftServerMessageRouterCommand<AbstractMessage> command, SwiftMessage swiftMessage) {
		if (getSwiftUtilHandler().isSystemAcknowledgementMessage(command.getMessage())) {
			Map<AbstractMessage, Boolean> messages = getSwiftUtilHandler().getAllMessagesMap(command.getMessage());
			Optional<Map.Entry<AbstractMessage, Boolean>> message = messages.entrySet().stream().findFirst();
			if (message.isPresent()) {
				Map.Entry<AbstractMessage, Boolean> entry = message.get();
				String typeCode = getSwiftUtilHandler().getMessageTypeCode(entry.getKey());
				SwiftMessageType type = getSwiftServerMessageService().getSwiftMessageTypeVersionCode(typeCode);
				if (type == null) {
					type = getSwiftServerMessageService().getSwiftMessageTypeByName(SwiftMessageService.MT_UNKNOWN_MESSAGE_TYPE_NAME);
				}
				swiftMessage.setType(type);
				swiftMessage.setAcknowledged(entry.getValue());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftIdentifierHandler<AbstractMessage> getSwiftIdentifierHandler() {
		return this.swiftIdentifierHandler;
	}


	public void setSwiftIdentifierHandler(SwiftIdentifierHandler<AbstractMessage> swiftIdentifierHandler) {
		this.swiftIdentifierHandler = swiftIdentifierHandler;
	}


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}


	public SwiftUtilHandler<AbstractMessage> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<AbstractMessage> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}


	public SwiftServerOutgoingFileMessageHandler getSwiftServerOutgoingFileMessageHandler() {
		return this.swiftServerOutgoingFileMessageHandler;
	}


	public void setSwiftServerOutgoingFileMessageHandler(SwiftServerOutgoingFileMessageHandler swiftServerOutgoingFileMessageHandler) {
		this.swiftServerOutgoingFileMessageHandler = swiftServerOutgoingFileMessageHandler;
	}


	public SwiftIncomingOperationalMessageHandler getSwiftIncomingOperationalMessageHandler() {
		return this.swiftIncomingOperationalMessageHandler;
	}


	public void setSwiftIncomingOperationalMessageHandler(SwiftIncomingOperationalMessageHandler swiftIncomingOperationalMessageHandler) {
		this.swiftIncomingOperationalMessageHandler = swiftIncomingOperationalMessageHandler;
	}


	public SwiftIncomingSystemMessageHandler getSwiftIncomingSystemMessageHandler() {
		return this.swiftIncomingSystemMessageHandler;
	}


	public void setSwiftIncomingSystemMessageHandler(SwiftIncomingSystemMessageHandler swiftIncomingSystemMessageHandler) {
		this.swiftIncomingSystemMessageHandler = swiftIncomingSystemMessageHandler;
	}
}
