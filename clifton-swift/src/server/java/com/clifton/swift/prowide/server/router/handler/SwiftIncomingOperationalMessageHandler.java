package com.clifton.swift.prowide.server.router.handler;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.server.handler.SwiftIncomingMessageHandler;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import com.clifton.swift.util.SwiftUtilHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftIncomingOperationalMessageHandler</code> return a status message based on an acknowledgement value.
 * <p>
 * A pair of messages are received for each message sent.  The first message of the pair is an acknowledgement (ACK OR NACK)
 * and the second is the actual operational message.
 * A success or failure response is constructed and sent back to IMS over the message queue.
 */
@Component
public class SwiftIncomingOperationalMessageHandler implements SwiftIncomingMessageHandler<AbstractMessage> {

	private SwiftServerStatusHandler swiftServerStatusHandler;
	private SwiftUtilHandler<AbstractMessage> swiftUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void handle(SwiftMessage swiftMessage, SwiftServerMessageRouterCommand<AbstractMessage> command) {
		AssertUtils.assertNotNull(swiftMessage, "Swift Message cannot be null.");

		InstructionStatuses status = getSwiftUtilHandler().isAcknowledged(command.getMessage()) ? InstructionStatuses.PROCESSED : InstructionStatuses.FAILED;

		for (SwiftIdentifier identifier : CollectionUtils.getIterable(command.getSwiftIdentifierList())) {
			// do not send status updates for duplicates
			if (!isTransactionReferenceNumberDuplicate(identifier.getUniqueStringIdentifier())) {
				getSwiftServerStatusHandler().sendSwiftExportStatus(
						identifier.getUniqueStringIdentifier(),
						identifier.getSourceSystemModule().getSourceSystem().getName(),
						identifier.getSourceSystemModule().getName(),
						status,
						null
				);
			}
		}
	}


	@Override
	public String[] getSupportedMessageType() {
		return null;
	}


	@Override
	public SwiftMessageFormats getSupportedMessageFormat() {
		return SwiftMessageFormats.MT;
	}


	public boolean isTransactionReferenceNumberDuplicate(String transactionNumber) {
		AssertUtils.assertNotNull(transactionNumber, "Transaction Number is required.");
		return transactionNumber.contains("_DUPLICATE_");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftServerStatusHandler getSwiftServerStatusHandler() {
		return this.swiftServerStatusHandler;
	}


	public void setSwiftServerStatusHandler(SwiftServerStatusHandler swiftServerStatusHandler) {
		this.swiftServerStatusHandler = swiftServerStatusHandler;
	}


	public SwiftUtilHandler<AbstractMessage> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<AbstractMessage> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}
}
