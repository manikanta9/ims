package com.clifton.swift.prowide.message.converter;

import com.prowidesoftware.swift.model.field.Field;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>SwiftFieldValueConverterLocatorImpl</code> loads Formatter implementations.
 */
@Component
public class SwiftFieldValueConverterLocatorImpl implements SwiftFieldValueConverterLocator<Field>, InitializingBean, ApplicationContextAware {

	private List<SwiftFieldValueConverter<Field>> converterList = new ArrayList<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SwiftFieldValueConverter<Field>> locate(String messageNumber, Field field) {
		return getConverterList()
				.stream()
				.filter(f -> f.accepts(messageNumber, field))
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public void afterPropertiesSet() {
		Map<String, SwiftFieldValueConverter> beanMap = getApplicationContext().getBeansOfType(SwiftFieldValueConverter.class);
		Collection<SwiftFieldValueConverter> formatterBeans = beanMap.values();
		for (SwiftFieldValueConverter formatter : formatterBeans) {
			getConverterList().add(formatter);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public List<SwiftFieldValueConverter<Field>> getConverterList() {
		return this.converterList;
	}


	public void setConverterList(List<SwiftFieldValueConverter<Field>> converterList) {
		this.converterList = converterList;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
