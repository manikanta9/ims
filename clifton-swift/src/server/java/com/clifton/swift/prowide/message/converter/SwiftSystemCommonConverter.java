package com.clifton.swift.prowide.message.converter;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.converter.beans.MessageCommonSystemValues;
import com.prowidesoftware.swift.model.field.Field;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>SwiftSystemCommonConverter</code> convert SWIFT system messages (MT0##) common field values to the label value.
 */
@Component
public class SwiftSystemCommonConverter implements SwiftFieldValueConverter<Field> {

	private static final Map<String, List<String>> messageFields = new HashMap<>();


	static {
		List<String> fields = Arrays.asList("301", "451");
		messageFields.put("021", fields);
		messageFields.put("044", fields);
		messageFields.put("046", fields);
		messageFields.put("064", fields);
		messageFields.put("066", fields);
		messageFields.put("068", fields);
		messageFields.put("082", fields);
		messageFields.put("083", fields);
		messageFields.put("097", fields);
	}


	@Override
	public boolean accepts(String messageNumber, Field field) {
		return messageFields.containsKey(messageNumber) && messageFields.get(messageNumber).contains(field.getName());
	}


	@Override
	public String[] formatFieldComponents(Field field) {
		String[] formatted = new String[field.componentsSize()];
		for (int i = 0; i < field.componentsSize(); i++) {
			String code = field.getComponent(i + 1);
			if (!StringUtils.isEmpty(code)) {
				MessageCommonSystemValues value = MessageCommonSystemValues.fromCode(code);
				if (value != null) {
					formatted[i] = value.getLabel();
				}
			}
		}
		return formatted;
	}
}
