package com.clifton.swift.prowide.message.converter.instruction;

import com.clifton.core.util.StringUtils;
import com.clifton.instruction.messaging.SSIIndicatorValues;
import com.clifton.swift.prowide.message.converter.SwiftFieldValueConverter;
import com.prowidesoftware.swift.model.field.Field;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code></code> convert SWIFT message MT461 common field values to the label value.
 */
@Component
public class SwiftSSIIndicatorConverter implements SwiftFieldValueConverter<Field> {

	private static final Map<String, List<String>> messageFields = new HashMap<>();


	static {
		List<String> fields = Collections.singletonList("22F");
		messageFields.put("671", fields);
	}


	@Override
	public boolean accepts(String messageNumber, Field field) {
		return messageFields.containsKey(messageNumber) && messageFields.get(messageNumber).contains(field.getName());
	}


	@Override
	public String[] formatFieldComponents(Field field) {
		String[] formatted = new String[field.componentsSize()];
		for (int i = 0; i < field.componentsSize(); i++) {
			String code = field.getComponent(i + 1);
			if (!StringUtils.isEmpty(code)) {
				SSIIndicatorValues value = SSIIndicatorValues.fromCode(code);
				if (value != null) {
					formatted[i] = value.getLabel();
				}
			}
		}
		return formatted;
	}
}
