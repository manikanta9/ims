package com.clifton.swift.prowide.message.converter;

import com.clifton.core.util.StringUtils;
import com.prowidesoftware.swift.model.field.CurrencyContainer;
import com.prowidesoftware.swift.model.field.Field;
import org.springframework.stereotype.Component;

import java.util.Currency;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>SwiftCurrencyConverter</code> convert SWIFT currency field values to the label value.
 */
@Component
public class SwiftCurrencyConverter implements SwiftFieldValueConverter<Field> {

	private static final Map<String, String> currencyCodeMap = Currency.getAvailableCurrencies()
			.stream()
			.collect(Collectors.toMap(Currency::getCurrencyCode, Currency::getDisplayName));


	@Override
	public boolean accepts(String messageNumber, Field field) {
		return CurrencyContainer.class.isAssignableFrom(field.getClass());
	}


	@Override
	public String[] formatFieldComponents(Field field) {
		String[] formatted = new String[field.componentsSize()];
		for (int i = 0; i < field.componentsSize(); i++) {
			String value = field.getComponent(i + 1);
			if (!StringUtils.isEmpty(value) && currencyCodeMap.containsKey(value)) {
				formatted[i] = currencyCodeMap.get(value);
			}
		}
		return formatted;
	}
}
