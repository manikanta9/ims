package com.clifton.swift.prowide.message.converter;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.converter.beans.MessageAbortReasonValues;
import com.prowidesoftware.swift.model.field.Field;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>SwiftErrorStatusConverter</code> handle fetching the error text for
 * specific field value error codes.
 */
@Component
public class SwiftErrorAbortConverter implements SwiftFieldValueConverter<Field> {

	private static final Map<String, List<String>> messageFields = new HashMap<>();


	static {
		List<String> fields = Collections.singletonList("432");
		messageFields.put("019", fields);
		messageFields.put("097", fields);
	}


	@Override
	public boolean accepts(String messageNumber, Field field) {
		return messageFields.containsKey(messageNumber) && messageFields.get(messageNumber).contains(field.getName());
	}


	@Override
	public String[] formatFieldComponents(Field field) {
		String[] formatted = new String[field.componentsSize()];
		for (int i = 0; i < field.componentsSize(); i++) {
			String code = field.getComponent(i + 1);
			if (!StringUtils.isEmpty(code)) {
				MessageAbortReasonValues value = MessageAbortReasonValues.fromCode(code);
				if (value != null) {
					formatted[i] = value.getLabel();
				}
				else if (MessageAbortReasonValues.validateCode(code)) {
					formatted[i] = MessageAbortReasonValues.getDefault().getLabel();
				}
			}
		}
		return formatted;
	}
}
