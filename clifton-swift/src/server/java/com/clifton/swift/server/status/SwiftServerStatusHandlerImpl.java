package com.clifton.swift.server.status;

import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.instruction.messaging.message.InstructionMessagingStatusMessage;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class SwiftServerStatusHandlerImpl implements SwiftServerStatusHandler {

	private AsynchronousMessageHandler swiftServerAsynchronousMessageHandler;


	@Override
	public void sendSwiftExportStatus(String uniqueStringIdentifier, String sourceName, String sourceModuleName, InstructionStatuses status, String message) {
		InstructionMessagingStatusMessage statusMessage = new InstructionMessagingStatusMessage();
		statusMessage.setUniqueStringIdentifier(uniqueStringIdentifier);
		statusMessage.setStatus(status);
		statusMessage.setMessage(message);
		statusMessage.setSourceSystemName(sourceName);
		statusMessage.setSourceSystemModuleName(sourceModuleName);
		getSwiftServerAsynchronousMessageHandler().send(statusMessage);
	}


	public AsynchronousMessageHandler getSwiftServerAsynchronousMessageHandler() {
		return this.swiftServerAsynchronousMessageHandler;
	}


	public void setSwiftServerAsynchronousMessageHandler(AsynchronousMessageHandler swiftServerAsynchronousMessageHandler) {
		this.swiftServerAsynchronousMessageHandler = swiftServerAsynchronousMessageHandler;
	}
}
