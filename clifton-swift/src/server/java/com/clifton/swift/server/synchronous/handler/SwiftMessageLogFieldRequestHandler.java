package com.clifton.swift.server.synchronous.handler;

import com.clifton.swift.message.log.SwiftMessageLogService;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageLogFieldRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageLogFieldResponseMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftMessageLogFieldRequestHandler</code> handle requests for SwiftMessageField records originating from log messages.
 */
@Component
public class SwiftMessageLogFieldRequestHandler implements SwiftSynchronousMessageHandler<SwiftMessageLogFieldResponseMessage, SwiftMessageLogFieldRequestMessage> {

	private SwiftMessageLogService swiftServerMessageLogService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageLogFieldResponseMessage handle(SwiftMessageLogFieldRequestMessage requestMessage) {
		SwiftMessageLogFieldResponseMessage result = new SwiftMessageLogFieldResponseMessage();
		result.setMessageFieldList(getSwiftServerMessageLogService().getSwiftMessageLogFieldList(requestMessage.getId()));
		return result;
	}


	@Override
	public Class<SwiftMessageLogFieldRequestMessage> getSupportedRequestType() {
		return SwiftMessageLogFieldRequestMessage.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageLogService getSwiftServerMessageLogService() {
		return this.swiftServerMessageLogService;
	}


	public void setSwiftServerMessageLogService(SwiftMessageLogService swiftServerMessageLogService) {
		this.swiftServerMessageLogService = swiftServerMessageLogService;
	}
}
