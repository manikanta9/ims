package com.clifton.swift.server;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.message.search.SwiftMessageSourceEntitySearchForm;
import com.clifton.swift.message.search.SwiftMessageStatusSearchForm;
import com.clifton.swift.message.search.SwiftMessageTypeSearchForm;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.util.SwiftServerUtilHandler;
import com.clifton.swift.util.SwiftUtilHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * <code>SwiftServerMessageServiceImpl</code> Swift server-side implementation of the SwiftMessageService, clients will never
 * connect to these service endpoints, they are all marked accordingly.
 */
@Component
public class SwiftServerMessageServiceImpl<M> implements SwiftMessageService {

	private AdvancedUpdatableDAO<SwiftMessage, Criteria> swiftMessageDAO;
	private AdvancedUpdatableDAO<SwiftMessageType, Criteria> swiftMessageTypeDAO;
	private AdvancedUpdatableDAO<SwiftMessageStatus, Criteria> swiftMessageStatusDAO;

	private SwiftIdentifierHandler<M> swiftIdentifierHandler;
	private SwiftMessageConverterHandler<M> swiftMessageConverterHandler;
	private SwiftServerMessageRouter<M> swiftServerMessageRouter;
	private SwiftServerUtilHandler<AbstractMessage> swiftServerUtilHandler;
	private SwiftUtilHandler<M> swiftUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessage getSwiftMessage(long id) {
		return getSwiftMessageDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SwiftMessageField> getSwiftMessageFieldList(long id) {
		SwiftMessage swiftMessage = getSwiftMessageDAO().findByPrimaryKey(id);
		return getSwiftServerUtilHandler().getSwiftMessageFieldList(swiftMessage.getText());
	}


	@Override
	public List<SwiftMessage> getSwiftMessageList(SwiftMessageSearchForm searchForm) {
		return getSwiftMessageDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SwiftMessage> getSwiftMessageListBySourceEntity(SwiftMessageSourceEntitySearchForm searchForm) {
		throw new RuntimeException("[" + this.getClass().getName() + "] does not support fetching Swift Message List By Source Entity.");
	}


	@Override
	public SwiftMessage saveSwiftMessage(SwiftMessage swiftMessage) {
		return getSwiftMessageDAO().save(swiftMessage);
	}


	@Override
	public void reprocessSwiftMessageList(long[] messageIdList) {
		// used for exception handling in the for-loop below
		StringBuilder errors = null;
		Throwable firstCause = null;

		for (Long messageId : messageIdList) {
			SwiftMessage swiftMessage = getSwiftMessage(messageId);
			try {
				ValidationUtils.assertTrue(swiftMessage.isIncoming(), "Only incoming messages can be reprocessed.");
				Map<String, M> messageList = getSwiftMessageConverterHandler().parse(swiftMessage.getText(), swiftMessage.getMessageFormat());
				for (Map.Entry<String, M> messageEntry : CollectionUtils.getIterable(messageList.entrySet())) {
					ValidationUtils.assertTrue(getSwiftUtilHandler().isSystemAcknowledgementMessage(messageEntry.getValue()), "Can only reprocess messages with acknowledgements.");
					// bypass log and swift message persistence.
					SwiftServerMessageRouterCommand<M> command = SwiftServerMessageRouterCommand.ofMessageAndString(messageEntry.getValue(), messageEntry.getKey());
					command.setSwiftIdentifierList(getSwiftIdentifierHandler().getSwiftIdentifierList(messageEntry.getValue()));
					ValidationUtils.assertNotEmpty(command.getSwiftIdentifierList(), "Could not find the Identifier for the message.");
					getSwiftServerMessageRouter().getSwiftIncomingMessageHandler(command.getMessage(), swiftMessage.getType()).handle(swiftMessage, command);
				}
			}
			catch (Throwable e) {
				boolean firstError = false;
				if (errors == null) {
					firstCause = e;
					firstError = true;
					errors = new StringBuilder(1024);
				}
				else {
					errors.append("\n\n");
				}
				errors.append("Could not process SWIFT message [").append(swiftMessage.getText()).append("]");
				if (!firstError) {
					errors.append("\nCAUSED BY ");
					errors.append(ExceptionUtils.getDetailedMessage(e));
				}
			}
		}
		if (errors != null) {
			throw new RuntimeException(errors.toString(), firstCause);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageType getSwiftMessageType(short id) {
		return getSwiftMessageTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public SwiftMessageType getSwiftMessageTypeByName(String name) {
		SwiftMessageTypeSearchForm searchForm = new SwiftMessageTypeSearchForm();
		searchForm.setNameEquals(name);
		return CollectionUtils.getFirstElement(getSwiftMessageTypeList(searchForm));
	}


	@Override
	public SwiftMessageType getSwiftMessageTypeVersionCode(String versionName) {
		SwiftMessageTypeSearchForm searchForm = new SwiftMessageTypeSearchForm();
		if (versionName != null && versionName.startsWith(SwiftMessageFormats.MT.toString())) {
			searchForm.setMtVersionName(versionName);
		}
		else if (versionName != null && versionName.toLowerCase().startsWith(SwiftMessageFormats.MX.toString().toLowerCase())) {
			searchForm.setMxVersionName(versionName);
		}
		searchForm.setLimit(1);
		return CollectionUtils.getFirstElement(getSwiftMessageTypeList(searchForm));
	}


	@Override
	public List<SwiftMessageType> getSwiftMessageTypeList(SwiftMessageTypeSearchForm searchForm) {
		return getSwiftMessageTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageStatus getSwiftMessageStatus(short id) {
		return getSwiftMessageStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public SwiftMessageStatus getSwiftMessageStatusByName(String name) {
		SwiftMessageStatusSearchForm searchForm = new SwiftMessageStatusSearchForm();
		searchForm.setNameEquals(name);
		return CollectionUtils.getFirstElement(getSwiftMessageStatusList(searchForm));
	}


	@Override
	public List<SwiftMessageStatus> getSwiftMessageStatusList(SwiftMessageStatusSearchForm searchForm) {
		return getSwiftMessageStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SwiftMessage, Criteria> getSwiftMessageDAO() {
		return this.swiftMessageDAO;
	}


	public void setSwiftMessageDAO(AdvancedUpdatableDAO<SwiftMessage, Criteria> swiftMessageDAO) {
		this.swiftMessageDAO = swiftMessageDAO;
	}


	public AdvancedUpdatableDAO<SwiftMessageType, Criteria> getSwiftMessageTypeDAO() {
		return this.swiftMessageTypeDAO;
	}


	public void setSwiftMessageTypeDAO(AdvancedUpdatableDAO<SwiftMessageType, Criteria> swiftMessageTypeDAO) {
		this.swiftMessageTypeDAO = swiftMessageTypeDAO;
	}


	public AdvancedUpdatableDAO<SwiftMessageStatus, Criteria> getSwiftMessageStatusDAO() {
		return this.swiftMessageStatusDAO;
	}


	public void setSwiftMessageStatusDAO(AdvancedUpdatableDAO<SwiftMessageStatus, Criteria> swiftMessageStatusDAO) {
		this.swiftMessageStatusDAO = swiftMessageStatusDAO;
	}


	public SwiftIdentifierHandler<M> getSwiftIdentifierHandler() {
		return this.swiftIdentifierHandler;
	}


	public void setSwiftIdentifierHandler(SwiftIdentifierHandler<M> swiftIdentifierHandler) {
		this.swiftIdentifierHandler = swiftIdentifierHandler;
	}


	public SwiftMessageConverterHandler<M> getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler<M> swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}


	public SwiftServerMessageRouter<M> getSwiftServerMessageRouter() {
		return this.swiftServerMessageRouter;
	}


	public void setSwiftServerMessageRouter(SwiftServerMessageRouter<M> swiftServerMessageRouter) {
		this.swiftServerMessageRouter = swiftServerMessageRouter;
	}


	public SwiftServerUtilHandler<AbstractMessage> getSwiftServerUtilHandler() {
		return this.swiftServerUtilHandler;
	}


	public void setSwiftServerUtilHandler(SwiftServerUtilHandler<AbstractMessage> swiftServerUtilHandler) {
		this.swiftServerUtilHandler = swiftServerUtilHandler;
	}


	public SwiftUtilHandler<M> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<M> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}
}
