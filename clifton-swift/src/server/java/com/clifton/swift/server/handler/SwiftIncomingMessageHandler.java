package com.clifton.swift.server.handler;

import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;


/**
 * <code>SwiftIncomingMessageHandler</code> implementations will take a Swift message of a specific type and
 * handle further parsing of embedded messages and routing.
 */
public interface SwiftIncomingMessageHandler<M> {

	public void handle(SwiftMessage swiftMessage, SwiftServerMessageRouterCommand<M> command);


	public String[] getSupportedMessageType();


	public SwiftMessageFormats getSupportedMessageFormat();
}
