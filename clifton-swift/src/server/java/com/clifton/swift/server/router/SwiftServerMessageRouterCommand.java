package com.clifton.swift.server.router;

import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.identifier.SwiftIdentifier;

import java.util.List;


/**
 * Holder for SWIFT message data the will be processed.
 *
 * @author mwacker
 */
public class SwiftServerMessageRouterCommand<M> {

	private M message;

	private String messageString;

	private List<SwiftIdentifier> swiftIdentifierList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static <M> SwiftServerMessageRouterCommand<M> ofMessage(M message) {
		return ofMessageIdentifierAndString(message, null, null);
	}


	public static <M> SwiftServerMessageRouterCommand<M> ofMessageAndIdentifier(M message, SwiftIdentifier identifier) {
		return ofMessageIdentifierAndString(message, null, identifier);
	}


	public static <M> SwiftServerMessageRouterCommand<M> ofMessageAndString(M message, String messageString) {
		return ofMessageIdentifierAndString(message, messageString, null);
	}


	public static <M> SwiftServerMessageRouterCommand<M> ofMessageIdentifierAndString(M message, String messageString, SwiftIdentifier identifier) {
		SwiftServerMessageRouterCommand<M> command = new SwiftServerMessageRouterCommand<>();
		command.message = message;
		command.messageString = messageString;
		if (identifier != null) {
			command.swiftIdentifierList = CollectionUtils.createList(identifier);
		}
		return command;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public M getMessage() {
		return this.message;
	}


	public void setMessage(M message) {
		this.message = message;
	}


	public String getMessageString() {
		return this.messageString;
	}


	public void setMessageString(String messageString) {
		this.messageString = messageString;
	}


	public List<SwiftIdentifier> getSwiftIdentifierList() {
		return this.swiftIdentifierList;
	}


	public void setSwiftIdentifierList(List<SwiftIdentifier> swiftIdentifierList) {
		this.swiftIdentifierList = swiftIdentifierList;
	}
}
