package com.clifton.swift.server.synchronous.handler;

import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageResponseMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftMessageRequestHandler</code> handles request for SwiftMessages.
 */
@Component
public class SwiftMessageRequestHandler implements SwiftSynchronousMessageHandler<SwiftMessageResponseMessage, SwiftMessageRequestMessage> {

	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageResponseMessage handle(SwiftMessageRequestMessage requestMessage) {
		SwiftMessageResponseMessage result = new SwiftMessageResponseMessage();
		result.setMessageList(getSwiftServerMessageService().getSwiftMessageList(requestMessage));
		return result;
	}


	@Override
	public Class<SwiftMessageRequestMessage> getSupportedRequestType() {
		return SwiftMessageRequestMessage.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
