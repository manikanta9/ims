package com.clifton.swift.server.synchronous.handler;

import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageStatusRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageStatusResponseMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftMessageStatusRequestHandler</code> handle requests for SwiftMessage status records.
 */
@Component
public class SwiftMessageStatusRequestHandler implements SwiftSynchronousMessageHandler<SwiftMessageStatusResponseMessage, SwiftMessageStatusRequestMessage> {

	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageStatusResponseMessage handle(SwiftMessageStatusRequestMessage requestMessage) {
		SwiftMessageStatusResponseMessage result = new SwiftMessageStatusResponseMessage();
		result.setSwiftMessageStatusList(getSwiftServerMessageService().getSwiftMessageStatusList(requestMessage));
		return result;
	}


	@Override
	public Class<SwiftMessageStatusRequestMessage> getSupportedRequestType() {
		return SwiftMessageStatusRequestMessage.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
