package com.clifton.swift.server.synchronous.handler;

import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageFieldRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageFieldResponseMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftMessageFieldRequestHandler</code> handle requests for SwiftMessageField records.
 */
@Component
public class SwiftMessageFieldRequestHandler implements SwiftSynchronousMessageHandler<SwiftMessageFieldResponseMessage, SwiftMessageFieldRequestMessage> {

	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageFieldResponseMessage handle(SwiftMessageFieldRequestMessage requestMessage) {
		SwiftMessageFieldResponseMessage result = new SwiftMessageFieldResponseMessage();
		result.setMessageFieldList(getSwiftServerMessageService().getSwiftMessageFieldList(requestMessage.getMessageId()));
		return result;
	}


	@Override
	public Class<SwiftMessageFieldRequestMessage> getSupportedRequestType() {
		return SwiftMessageFieldRequestMessage.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
