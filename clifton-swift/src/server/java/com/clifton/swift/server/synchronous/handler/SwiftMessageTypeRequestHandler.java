package com.clifton.swift.server.synchronous.handler;

import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageTypeRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageTypeResponseMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftMessageTypeRequestHandler</code> handle requests for SwiftMessageType records.
 */
@Component
public class SwiftMessageTypeRequestHandler implements SwiftSynchronousMessageHandler<SwiftMessageTypeResponseMessage, SwiftMessageTypeRequestMessage> {

	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageTypeResponseMessage handle(SwiftMessageTypeRequestMessage requestMessage) {
		SwiftMessageTypeResponseMessage result = new SwiftMessageTypeResponseMessage();
		result.setSwiftMessageTypeList(getSwiftServerMessageService().getSwiftMessageTypeList(requestMessage));
		return result;
	}


	@Override
	public Class<SwiftMessageTypeRequestMessage> getSupportedRequestType() {
		return SwiftMessageTypeRequestMessage.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
