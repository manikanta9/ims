package com.clifton.swift.server.synchronous;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessageStatus;
import com.clifton.core.messaging.jms.synchronous.AbstractSynchronousStatusProcessor;
import com.clifton.core.messaging.synchronous.SynchronousStatusRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousStatusResponseMessage;
import com.clifton.core.util.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;


/**
 * <code>SwiftServerMessagingSynchronousStatusProcessor</code> handles JMS message request to check the health status of the SWIFT server.
 */
public class SwiftServerMessagingSynchronousStatusProcessor extends AbstractSynchronousStatusProcessor {


	private String swiftReceptionFolderPath;
	private String swiftEmissionFolderPath;
	private long maxEmissionFileAgeMinutes;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SynchronousStatusResponseMessage process(SynchronousStatusRequestMessage message) {
		SynchronousStatusResponseMessage synchronousStatusResponseMessage = super.process(message);
		List<MessageStatus> messageStatuses = new ArrayList<>();
		try {
			checkAutoClientFolderAccessible(messageStatuses);
			// if the reception folder is down, don't attempt emission folder check.
			doProcessEmissionFolder(messageStatuses);
		}
		catch (Exception e) {
			MessageStatus messageStatus = createErrorMessageStatus(String.format("Unexpected exception [%s] checking SWIFT server reception folder.", e.getMessage()), MessageStatus.SWIFT_FOLDER_STATUS);
			messageStatuses.add(messageStatus);
		}
		synchronousStatusResponseMessage.getMessageStatusList().addAll(messageStatuses);
		return synchronousStatusResponseMessage;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks the existence and accessibility of the SMB mapped SWIFT Reception folder.
	 * <p>
	 * Makes sure the 'Reception' folder exists, create the 'heath-check.tmp' file and delete the previously created 'health-check.tmp'.
	 */
	private void checkAutoClientFolderAccessible(List<MessageStatus> statusList) throws IOException {
		Path newTempFilePath = null;
		try {
			Path receptionFolderPath = Paths.get(getSwiftReceptionFolderPath()).toAbsolutePath();
			if (!receptionFolderPath.toFile().exists()) {
				statusList.add(createErrorMessageStatus(String.format("Could not access SWIFT Reception Integration [%s] folder.", receptionFolderPath.toAbsolutePath().toString()), MessageStatus.SWIFT_FOLDER_STATUS));
			}
			else {
				Path tempFilePath = receptionFolderPath.resolveSibling(Paths.get("health-check.tmp"));
				newTempFilePath = Files.createFile(tempFilePath);
				if (!newTempFilePath.toFile().exists()) {
					statusList.add(createErrorMessageStatus(String.format("Could not create temporary file [%s] in SWIFT Reception Integration [%s] folder."
							, tempFilePath.getFileName().toString(), receptionFolderPath.toAbsolutePath().toString()), MessageStatus.SWIFT_FOLDER_STATUS));
				}
			}
		}
		finally {
			if (newTempFilePath != null) {
				deleteTemporaryFile(statusList, newTempFilePath);
			}
		}
	}


	private void deleteTemporaryFile(List<MessageStatus> statusList, Path tempFilePath) {
		try {
			Files.delete(tempFilePath);
		}
		catch (Exception e) {
			statusList.add(createErrorMessageStatus(String.format("Could not delete temporary file [%s] from Swift Reception Integration folder, exception [%s]"
					, tempFilePath.getFileName().toString(), getSwiftReceptionFolderPath()), MessageStatus.SWIFT_FOLDER_STATUS));
		}
	}

	////////////////////////////////////////////////////////////////////////////


	private void doProcessEmissionFolder(List<MessageStatus> messageStatuses) {
		try {
			checkAutoClientFolderContentsAge(messageStatuses);
		}
		catch (Exception e) {
			MessageStatus messageStatus = createErrorMessageStatus(String.format("Unexpected exception [%s] checking SWIFT server emission folder.", e.getMessage()), MessageStatus.SWIFT_EMISSION_FOLDER_STATUS);
			messageStatuses.add(messageStatus);
		}
	}


	/**
	 * Checks the contents of the 'Emission' folder and reports any files older than 5 minutes.
	 */
	private void checkAutoClientFolderContentsAge(List<MessageStatus> statusList) {
		Path emissionFolderPath = Paths.get(getSwiftEmissionFolderPath()).toAbsolutePath();
		if (!emissionFolderPath.toFile().exists()) {
			statusList.add(createErrorMessageStatus(String.format("Could not access SWIFT Emission Integration [%s] folder.", emissionFolderPath.toAbsolutePath().toString()), MessageStatus.SWIFT_EMISSION_FOLDER_STATUS));
		}
		else {
			final long currentTimeMillis = System.currentTimeMillis();
			final long maximumDuration = Duration.ofMinutes(getMaxEmissionFileAgeMinutes()).toMillis();
			long oldFileCount = CollectionUtils.createList(emissionFolderPath.toFile().listFiles()).stream()
					.filter(f -> (currentTimeMillis - getLastModifiedTime(f).toMillis()) > maximumDuration)
					.count();
			if (oldFileCount > 0) {
				statusList.add(createErrorMessageStatus(String.format("[%s] files in the SWIFT Emission Integration folder have not been processed for at least five minutes."
						, oldFileCount), MessageStatus.SWIFT_EMISSION_FOLDER_STATUS));
			}
		}
	}


	private FileTime getLastModifiedTime(File file) {
		try {
			return Files.getLastModifiedTime(file.toPath(), LinkOption.NOFOLLOW_LINKS);
		}
		catch (IOException e) {
			LogUtils.error(this.getClass(), "Could not read file " + file.getName());
		}
		return FileTime.fromMillis(System.currentTimeMillis());
	}


	////////////////////////////////////////////////////////////////////////////


	private MessageStatus createErrorMessageStatus(String message, String responseName) {
		MessageStatus messageStatus = new MessageStatus();
		messageStatus.setError(true);
		messageStatus.setResponseName(responseName);
		messageStatus.setResponseStatus(message);
		return messageStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSwiftReceptionFolderPath() {
		return this.swiftReceptionFolderPath;
	}


	public void setSwiftReceptionFolderPath(String swiftReceptionFolderPath) {
		this.swiftReceptionFolderPath = swiftReceptionFolderPath;
	}


	public String getSwiftEmissionFolderPath() {
		return this.swiftEmissionFolderPath;
	}


	public void setSwiftEmissionFolderPath(String swiftEmissionFolderPath) {
		this.swiftEmissionFolderPath = swiftEmissionFolderPath;
	}


	public long getMaxEmissionFileAgeMinutes() {
		return this.maxEmissionFileAgeMinutes;
	}


	public void setMaxEmissionFileAgeMinutes(long maxEmissionFileAgeMinutes) {
		this.maxEmissionFileAgeMinutes = maxEmissionFileAgeMinutes;
	}
}
