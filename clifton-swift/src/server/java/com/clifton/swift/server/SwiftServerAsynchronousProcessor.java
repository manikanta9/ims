package com.clifton.swift.server;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.swift.field.SwiftFieldModifierHandler;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import com.clifton.swift.system.SwiftSourceSystemService;
import com.clifton.swift.util.SwiftUtilHandler;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * <code>SwiftServerAsynchronousProcessor</code> server-side processor for incoming asynchronous swift
 * messages, usually these are {@link SwiftMessagingMessage} objects.
 */
public class SwiftServerAsynchronousProcessor<M> implements AsynchronousProcessor<AsynchronousMessage> {

	private SwiftServerMessageRouter<M> swiftServerMessageRouter;
	private SwiftFieldModifierHandler<M> swiftFieldModifierHandler;
	private SwiftUtilHandler<M> swiftUtilHandler;
	private SwiftSourceSystemService swiftSourceSystemService;
	private SwiftServerStatusHandler swiftServerStatusHandler;
	private SwiftIdentifierHandler<M> swiftIdentifierHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Upon failure, send a failure status message back to IMS.
	 */
	@Override
	public void process(AsynchronousMessage msg, AsynchronousMessageHandler handler) {
		if (msg instanceof SwiftMessagingMessage) {
			SwiftMessagingMessage swiftMessagingMessage = (SwiftMessagingMessage) msg;
			try {
				// saves in its own transaction
				SwiftMessage swiftMessage = saveSwiftMessage(swiftMessagingMessage);

				ValidationUtils.assertNotNull(getSwiftServerMessageRouter(), "No message router is defined.");
				getSwiftServerMessageRouter().send(swiftMessage);
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Failed to process SWIFT message [" + ((SwiftMessagingMessage) msg).getMessageText() + "].", e);
				handleBasicError(e, swiftMessagingMessage);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Save the Swift Message and Identifier in its own transaction.  After removal from JMS, message should be immediately persisted (LOGGED).
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected SwiftMessage saveSwiftMessage(SwiftMessagingMessage messagingMessage) {
		M swiftMessage = getSwiftUtilHandler().parseSingleMessage(messagingMessage.getMessageText(), messagingMessage.getMessageFormat());
		SwiftIdentifier identifier = getSwiftIdentifierHandler().getOrCreateSwiftIdentifier(messagingMessage, swiftMessage);
		getSwiftUtilHandler().replaceSwiftMessageTransactionReferenceNumber(swiftMessage, identifier);
		SwiftServerMessageRouterCommand<M> swiftMessageCommand = SwiftServerMessageRouterCommand.ofMessageAndIdentifier(swiftMessage, identifier);
		return getSwiftServerMessageRouter().saveMessage(swiftMessageCommand, false);
	}


	/**
	 * Handles the export status and sends export status messages, creates a failed instruction status message and places the message on the asynchronous queue.
	 *
	 * @Return true if the error was handled and false if not.
	 */
	private void handleBasicError(Exception e, SwiftMessagingMessage message) {
		String errorMessage = e.getMessage() + "\nCAUSED BY: " + ExceptionUtils.getFullStackTrace(e);
		getSwiftServerStatusHandler().sendSwiftExportStatus(message.getUniqueStringIdentifier(), message.getSourceSystemName(), message.getSourceSystemModuleName(), InstructionStatuses.FAILED, errorMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftServerMessageRouter<M> getSwiftServerMessageRouter() {
		return this.swiftServerMessageRouter;
	}


	public void setSwiftServerMessageRouter(SwiftServerMessageRouter<M> swiftServerMessageRouter) {
		this.swiftServerMessageRouter = swiftServerMessageRouter;
	}


	public SwiftFieldModifierHandler<M> getSwiftFieldModifierHandler() {
		return this.swiftFieldModifierHandler;
	}


	public void setSwiftFieldModifierHandler(SwiftFieldModifierHandler<M> swiftFieldModifierHandler) {
		this.swiftFieldModifierHandler = swiftFieldModifierHandler;
	}


	public SwiftUtilHandler<M> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<M> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}


	public SwiftServerStatusHandler getSwiftServerStatusHandler() {
		return this.swiftServerStatusHandler;
	}


	public void setSwiftServerStatusHandler(SwiftServerStatusHandler swiftServerStatusHandler) {
		this.swiftServerStatusHandler = swiftServerStatusHandler;
	}


	public SwiftSourceSystemService getSwiftSourceSystemService() {
		return this.swiftSourceSystemService;
	}


	public void setSwiftSourceSystemService(SwiftSourceSystemService swiftSourceSystemService) {
		this.swiftSourceSystemService = swiftSourceSystemService;
	}


	public SwiftIdentifierHandler<M> getSwiftIdentifierHandler() {
		return this.swiftIdentifierHandler;
	}


	public void setSwiftIdentifierHandler(SwiftIdentifierHandler<M> swiftIdentifierHandler) {
		this.swiftIdentifierHandler = swiftIdentifierHandler;
	}
}
