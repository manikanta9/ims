package com.clifton.swift.server.synchronous.handler;

import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.log.SwiftMessageLogService;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageActionRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageActionResponseMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftMessageActionRequestHandler</code> handle requests for Swift Message Actions.
 */
@Component
public class SwiftMessageActionRequestHandler implements SwiftSynchronousMessageHandler<SwiftMessageActionResponseMessage, SwiftMessageActionRequestMessage> {

	private SwiftMessageLogService swiftServerMessageLogService;
	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageActionResponseMessage handle(SwiftMessageActionRequestMessage requestMessage) {
		SwiftMessageActionResponseMessage response = new SwiftMessageActionResponseMessage();
		try {
			switch (requestMessage.getSwiftMessageAction()) {
				case REPROCESS_SWIFT_LOG_MESSAGE:
					getSwiftServerMessageLogService().reprocessSwiftMessageList(requestMessage.getSwiftMessageIdList());
					break;
				case REPROCESS_SWIFT_MESSAGE:
					getSwiftServerMessageService().reprocessSwiftMessageList(requestMessage.getSwiftMessageIdList());
					break;
				default:
					throw new RuntimeException("No handler found for action " + requestMessage.getSwiftMessageAction());
			}
		}
		catch (Exception e) {
			response.setError(e);
		}
		return response;
	}


	@Override
	public Class<SwiftMessageActionRequestMessage> getSupportedRequestType() {
		return SwiftMessageActionRequestMessage.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageLogService getSwiftServerMessageLogService() {
		return this.swiftServerMessageLogService;
	}


	public void setSwiftServerMessageLogService(SwiftMessageLogService swiftServerMessageLogService) {
		this.swiftServerMessageLogService = swiftServerMessageLogService;
	}


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
