package com.clifton.swift.server.field;

import com.clifton.swift.field.SwiftFieldModifierBean;
import com.clifton.swift.field.SwiftFieldModifierHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;


/**
 * The <code>SwiftFieldModifierPostProcessor</code> finds all SwiftFieldModifierService implementations
 * and adds them to the field modifier map in the 'SwiftFieldModifierBean' object.
 */
public class SwiftFieldModifierPostProcessor<T> implements BeanFactoryPostProcessor {

	@SuppressWarnings("unchecked")
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		SwiftFieldModifierHandler<T> swiftFieldModifierHandler = (SwiftFieldModifierHandler<T>) beanFactory.getBean("swiftFieldModifierHandler");
		String[] matchingBeans = beanFactory.getBeanNamesForType(SwiftFieldModifierBean.class);
		for (String name : matchingBeans) {
			SwiftFieldModifierBean<T> fieldModifierHandler = (SwiftFieldModifierBean<T>) beanFactory.getBean(name);
			swiftFieldModifierHandler.register(fieldModifierHandler);
		}
	}
}
