package com.clifton.swift.server;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.log.SwiftMessageLog;
import com.clifton.swift.message.log.SwiftMessageLogHandler;
import com.clifton.swift.message.log.SwiftMessageLogService;
import com.clifton.swift.message.log.search.SwiftMessageLogSearchForm;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.util.SwiftServerUtilHandler;
import com.prowidesoftware.swift.model.AbstractMessage;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * <code>SwiftServerMessageLogServiceImpl</code> Swift server-side implementation of the SwiftMessageLogService, clients will never
 * connect to these service endpoints, they are all marked accordingly.
 */
@SuppressWarnings("rawtypes")
@Component
public class SwiftServerMessageLogServiceImpl implements SwiftMessageLogService {

	private AdvancedUpdatableDAO<SwiftMessageLog, Criteria> swiftMessageLogDAO;

	private SwiftMessageConverterHandler swiftMessageConverterHandler;
	private SwiftMessageLogHandler swiftMessageLogHandler;
	private SwiftServerMessageRouter swiftServerMessageRouter;
	private SwiftServerUtilHandler<AbstractMessage> swiftServerUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageLog getSwiftMessageLog(long id) {
		return getSwiftMessageLogDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SwiftMessageLog> getSwiftMessageLogList(SwiftMessageLogSearchForm searchForm) {
		return getSwiftMessageLogDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SwiftMessageField> getSwiftMessageLogFieldList(long id) {
		SwiftMessageLog swiftMessageLog = getSwiftMessageLogDAO().findByPrimaryKey(id);
		return getSwiftServerUtilHandler().getSwiftMessageFieldList(swiftMessageLog.getText());
	}


	@Override
	public SwiftMessageLog saveSwiftMessageLog(SwiftMessageLog bean) {
		return getSwiftMessageLogDAO().save(bean);
	}


	@Override
	public void deleteSwiftMessageLog(long id) {
		getSwiftMessageLogDAO().delete(id);
	}


	@Override
	@SuppressWarnings("unchecked")
	public void reprocessSwiftMessageList(long[] messageLogIdList) {
		// used for exception handling in the for-loop below
		StringBuilder errors = null;
		Throwable firstCause = null;


		for (Long messageId : messageLogIdList) {
			SwiftMessageLog logMessage = getSwiftMessageLog(messageId);
			try {

				getSwiftMessageLogHandler().processSwiftMessage(logMessage, log -> {
					Map<String, ?> messageList = getSwiftMessageConverterHandler().parse(log.getText(), log.getMessageFormat());
					for (Map.Entry<String, ?> messageEntry : CollectionUtils.getIterable(messageList.entrySet())) {
						getSwiftServerMessageRouter().receive(SwiftServerMessageRouterCommand.ofMessageAndString(messageEntry.getValue(), messageEntry.getKey()));
					}
					return log;
				});
			}
			catch (Throwable e) {
				boolean firstError = false;
				if (errors == null) {
					firstCause = e;
					firstError = true;
					errors = new StringBuilder(1024);
				}
				else {
					errors.append("\n\n");
				}
				errors.append("Could not process SWIFT message [").append(logMessage.getText()).append("]");
				if (!firstError) {
					errors.append("\nCAUSED BY ");
					errors.append(ExceptionUtils.getDetailedMessage(e));
				}
			}
		}

		if (errors != null) {
			throw new RuntimeException(errors.toString(), firstCause);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SwiftMessageLog, Criteria> getSwiftMessageLogDAO() {
		return this.swiftMessageLogDAO;
	}


	public void setSwiftMessageLogDAO(AdvancedUpdatableDAO<SwiftMessageLog, Criteria> swiftMessageLogDAO) {
		this.swiftMessageLogDAO = swiftMessageLogDAO;
	}


	public SwiftMessageConverterHandler getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}


	public SwiftMessageLogHandler getSwiftMessageLogHandler() {
		return this.swiftMessageLogHandler;
	}


	public void setSwiftMessageLogHandler(SwiftMessageLogHandler swiftMessageLogHandler) {
		this.swiftMessageLogHandler = swiftMessageLogHandler;
	}


	public SwiftServerMessageRouter getSwiftServerMessageRouter() {
		return this.swiftServerMessageRouter;
	}


	public void setSwiftServerMessageRouter(SwiftServerMessageRouter swiftServerMessageRouter) {
		this.swiftServerMessageRouter = swiftServerMessageRouter;
	}


	public SwiftServerUtilHandler<AbstractMessage> getSwiftServerUtilHandler() {
		return this.swiftServerUtilHandler;
	}


	public void setSwiftServerUtilHandler(SwiftServerUtilHandler<AbstractMessage> swiftServerUtilHandler) {
		this.swiftServerUtilHandler = swiftServerUtilHandler;
	}
}
