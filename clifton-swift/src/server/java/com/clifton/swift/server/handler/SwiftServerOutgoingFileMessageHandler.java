package com.clifton.swift.server.handler;

import com.clifton.swift.message.SwiftMessage;


/**
 * <code>SwiftOutgoingMessageHandler</code> implementations will take a Swift message, convert the message to its
 * transfer format, and send to the swift network.
 */
public interface SwiftServerOutgoingFileMessageHandler {

	public static final String PAYLOAD_GROUP_HEADER_KEY = "payload_group";
	public static final String PAYLOAD_BYTES_HEADER_KEY = "payload_bytes";
	public static final String PAYLOAD_MESSAGE_COUNT_KEY = "payload_message_count";


	public void send(SwiftMessage swiftMessage);
}
