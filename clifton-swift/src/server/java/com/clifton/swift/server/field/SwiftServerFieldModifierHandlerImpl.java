package com.clifton.swift.server.field;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.swift.field.SwiftFieldModifier;
import com.clifton.swift.field.SwiftFieldModifierBean;
import com.clifton.swift.field.SwiftFieldModifierFilter;
import com.clifton.swift.field.SwiftFieldModifierHandler;
import com.clifton.swift.field.SwiftFieldModifierService;
import com.clifton.swift.field.search.SwiftFieldModifierSearchForm;
import com.clifton.swift.util.SwiftUtilHandler;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * {@inheritDoc}
 */
@Component("swiftFieldModifierHandler")
public class SwiftServerFieldModifierHandlerImpl<T> implements SwiftFieldModifierHandler<T> {

	public static final String SWIFT_FIELD_SEPARATOR = ":";
	public static final String SWIFT_VALUE_SEPARATOR = ":";

	private SwiftFieldModifierService swiftFieldModifierService;
	private SwiftUtilHandler<T> swiftUtilHandler;

	private Map<String, SwiftFieldModifierBean<T>> handlerMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyFieldModifiers(T message) {
		SwiftFieldModifierSearchForm searchForm = new SwiftFieldModifierSearchForm();
		List<SwiftFieldModifier> fieldModifierList = getSwiftFieldModifierService().getSwiftFieldModifierList(searchForm);

		String messageText = getSwiftUtilHandler().getMessageText(message);
		for (SwiftFieldModifier modifier : CollectionUtils.getIterable(fieldModifierList)) {
			Pattern pattern = getPatternForModifier(modifier.getId());
			if (pattern != null) {
				Matcher matcher = pattern.matcher(messageText);
				if (matcher.find()) {
					String key = modifier.getFieldModifierType().getClassName();
					SwiftFieldModifierBean<T> bean = this.handlerMap.get(key);
					bean.execute(message, modifier);
				}
			}
		}
	}


	@Override
	public void register(SwiftFieldModifierBean<T> swiftFieldModifierBean) {
		AssertUtils.assertNotNull(swiftFieldModifierBean, "Field Modifier Handler cannot be null.");

		this.handlerMap.put(swiftFieldModifierBean.getClass().getName(), swiftFieldModifierBean);
	}


	@Override
	public Pattern getPatternForModifier(int modifierId) {
		List<SwiftFieldModifierFilter> filters = getSwiftFieldModifierService().getSwiftFieldModifierFilterListByModifier(modifierId);

		Pattern pattern = null;
		if (!CollectionUtils.isEmpty(filters)) {
			String[] values = new String[filters.size()];
			int i = 0;
			for (SwiftFieldModifierFilter filter : filters) {
				values[i] = "(?:" + SWIFT_FIELD_SEPARATOR + filter.getFieldTag() + SWIFT_VALUE_SEPARATOR + filter.getFieldValue() + ")";
				i++;
			}
			String regex = StringUtils.join(values, ".*");
			pattern = Pattern.compile(regex);
		}
		return pattern;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFieldModifierService getSwiftFieldModifierService() {
		return this.swiftFieldModifierService;
	}


	public void setSwiftFieldModifierService(SwiftFieldModifierService swiftFieldModifierService) {
		this.swiftFieldModifierService = swiftFieldModifierService;
	}


	public SwiftUtilHandler<T> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<T> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}
}
