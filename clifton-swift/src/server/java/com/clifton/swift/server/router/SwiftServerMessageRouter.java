package com.clifton.swift.server.router;

import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.server.handler.SwiftIncomingMessageHandler;


/**
 * The <code>SwiftServerMessageRouter</code> defines an object the will route messages to and from Swift.
 *
 * @author TerryS
 */
public interface SwiftServerMessageRouter<M> {

	/**
	 * Receive a message from Swift and forward via the asynchronous JMS queue to IMS.
	 */
	public void receive(SwiftServerMessageRouterCommand<M> command);


	/**
	 * Send a message to Swift network.
	 */
	public void send(SwiftMessage message);


	/**
	 * Save SwiftMessage to the database
	 */
	public SwiftMessage saveMessage(SwiftServerMessageRouterCommand<M> command, boolean incoming);


	/**
	 * Find the message specific handler
	 */
	public SwiftIncomingMessageHandler<M> getSwiftIncomingMessageHandler(M message, SwiftMessageType swiftMessageType);
}
