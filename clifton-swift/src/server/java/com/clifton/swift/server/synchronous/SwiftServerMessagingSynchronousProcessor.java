package com.clifton.swift.server.synchronous;

import com.clifton.core.messaging.jms.synchronous.SynchronousProcessor;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousResponseMessage;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandlerLocator;


/**
 * <code>SwiftServerMessagingSynchronousProcessor</code>
 */
public class SwiftServerMessagingSynchronousProcessor implements SynchronousProcessor<SynchronousRequestMessage, SynchronousResponseMessage> {

	private SwiftSynchronousMessageHandlerLocator swiftSynchronousMessageHandlerLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SynchronousResponseMessage process(SynchronousRequestMessage message) {
		@SuppressWarnings("unchecked")
		SwiftSynchronousMessageHandler<SynchronousResponseMessage, SynchronousRequestMessage> handler = getSwiftSynchronousMessageHandlerLocator().locate(message.getClass());
		if (handler == null) {
			throw new RuntimeException("[" + message.getClass().getSimpleName() + "] is not supported by [SwiftServerMessagingSynchronousProcessor]");
		}
		return handler.handle(message);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftSynchronousMessageHandlerLocator getSwiftSynchronousMessageHandlerLocator() {
		return this.swiftSynchronousMessageHandlerLocator;
	}


	public void setSwiftSynchronousMessageHandlerLocator(SwiftSynchronousMessageHandlerLocator swiftSynchronousMessageHandlerLocator) {
		this.swiftSynchronousMessageHandlerLocator = swiftSynchronousMessageHandlerLocator;
	}
}
