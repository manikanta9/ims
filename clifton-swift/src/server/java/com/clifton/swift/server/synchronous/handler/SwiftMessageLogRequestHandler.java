package com.clifton.swift.server.synchronous.handler;

import com.clifton.swift.message.log.SwiftMessageLogService;
import com.clifton.swift.messaging.synchronous.handler.SwiftSynchronousMessageHandler;
import com.clifton.swift.messaging.synchronous.request.SwiftMessageLogRequestMessage;
import com.clifton.swift.messaging.synchronous.response.SwiftMessageLogResponseMessage;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftMessageLogRequestHandler</code> handles request for SwiftLogMessages.
 */
@Component
public class SwiftMessageLogRequestHandler implements SwiftSynchronousMessageHandler<SwiftMessageLogResponseMessage, SwiftMessageLogRequestMessage> {

	private SwiftMessageLogService swiftServerMessageLogService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageLogResponseMessage handle(SwiftMessageLogRequestMessage requestMessage) {
		SwiftMessageLogResponseMessage result = new SwiftMessageLogResponseMessage();
		result.setMessageLogList(getSwiftServerMessageLogService().getSwiftMessageLogList(requestMessage));
		return result;
	}


	@Override
	public Class<SwiftMessageLogRequestMessage> getSupportedRequestType() {
		return SwiftMessageLogRequestMessage.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageLogService getSwiftServerMessageLogService() {
		return this.swiftServerMessageLogService;
	}


	public void setSwiftServerMessageLogService(SwiftMessageLogService swiftServerMessageLogService) {
		this.swiftServerMessageLogService = swiftServerMessageLogService;
	}
}
