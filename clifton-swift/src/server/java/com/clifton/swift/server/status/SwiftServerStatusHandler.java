package com.clifton.swift.server.status;

import com.clifton.instruction.messaging.InstructionStatuses;


/**
 * @author mwacker
 */
public interface SwiftServerStatusHandler {

	public void sendSwiftExportStatus(String uniqueStringIdentifier, String sourceName, String sourceModuleName, InstructionStatuses status, String message);
}
