package com.clifton.swift.identifier;

import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;

import java.util.List;


public interface SwiftIdentifierHandler<M> {

	public SwiftIdentifier getSwiftIdentifierForMessage(M message);


	public SwiftIdentifier getOrCreateSwiftIdentifier(SwiftMessagingMessage messagingMessage, M message);


	public List<SwiftIdentifier> getSwiftIdentifierList(M message);
}
