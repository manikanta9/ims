package com.clifton.swift.identifier;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.system.SwiftSourceSystemService;
import com.clifton.swift.util.SwiftUtilHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The <code>SwiftIdentifierHandlerImpl</code> creates the Swift identifier for the message and applies the new id to the message.
 */
@Component
public class SwiftIdentifierHandlerImpl<M> implements SwiftIdentifierHandler<M> {

	private SwiftUtilHandler<M> swiftUtilHandler;
	private SwiftMessageService swiftServerMessageService;
	private SwiftIdentifierService swiftIdentifierService;
	private SwiftSourceSystemService swiftSourceSystemService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a {@link SwiftIdentifier} for the provided Message if it doesn't already exist, otherwise, return the existing.
	 *
	 * @param message retrieved from the JMS queue.
	 * @return a newly created {@link SwiftIdentifier} or null if the unique identifier string is blank or the message type is not known.
	 * @throws RuntimeException if the identifier already exists.
	 */
	@Override
	public SwiftIdentifier getOrCreateSwiftIdentifier(SwiftMessagingMessage messagingMessage, M message) {
		AssertUtils.assertNotNull(messagingMessage, "Swift Message cannot be null");

		SwiftSourceSystem swiftSourceSystem = getSwiftSourceSystemFromMessage(messagingMessage);
		if (swiftSourceSystem != null) {
			String uniqueStringIdentifier = messagingMessage.getUniqueStringIdentifier();
			ValidationUtils.assertNotEmpty(uniqueStringIdentifier, "Swift unique identifier string cannot be empty.");

			// this message is a replacement or cancellation of the referenced message.
			Long messageReferenceNumber = messagingMessage.getMessageReferenceNumber();

			String messageTypeCode = getSwiftUtilHandler().getMessageTypeCode(message);
			SwiftMessageType swiftMessageType = getSwiftServerMessageService().getSwiftMessageTypeVersionCode(messageTypeCode);

			SwiftIdentifier swiftIdentifier = getSwiftIdentifierService().getSwiftIdentifierByUniqueId(uniqueStringIdentifier, swiftSourceSystem.getId(), messageReferenceNumber);
			if (swiftIdentifier == null) {
				swiftIdentifier = createSwiftIdentifierFromMessage(messagingMessage, swiftSourceSystem, messageReferenceNumber);
			}
			else if (!hasOnlySwiftMessageStatus(messagingMessage, swiftMessageType, SwiftMessageStatus.SwiftMessageStatusNames.ERROR)) {
				// only allow those Swift messages with identifiers to be sent if they've previously failed.
				throw new RuntimeException("Swift Message was already processed " + messagingMessage.getUniqueStringIdentifier());
			}
			return swiftIdentifier;
		}
		else {
			throw new RuntimeException("Cannot find SwiftSourceSystem for [" + messagingMessage.getSourceSystemName() + "].");
		}
	}


	@Override
	public SwiftIdentifier getSwiftIdentifierForMessage(M message) {
		AssertUtils.assertNotNull(message, "Swift Message cannot be null");

		SwiftMessageType type = getSwiftMessageType(message);
		if (type != null) {
			String transactionReferenceNumber = getSwiftUtilHandler().getTransactionReferenceNumber(message);
			Long messageReferenceNumber = getSwiftUtilHandler().getMessageReferenceNumber(message);
			if (!StringUtils.isEmpty(transactionReferenceNumber)) {
				SwiftSourceSystem swiftSourceSystem = getSwiftSourceSystemFromMessage(message);
				if (swiftSourceSystem != null) {
					try {
						// transaction reference number is the Swift Identifier's primary key.
						long primaryKey = Long.parseLong(transactionReferenceNumber);
						return getSwiftIdentifierService().getSwiftIdentifier(primaryKey);
					}
					catch (NumberFormatException e) {
						// old messages may still use SWIFT Identifier's unique string, eventually this can be removed
						return getSwiftIdentifierService().getSwiftIdentifierByUniqueId(transactionReferenceNumber, swiftSourceSystem.getId(), messageReferenceNumber);
					}
				}
			}
		}
		return null;
	}


	@Override
	public List<SwiftIdentifier> getSwiftIdentifierList(M message) {
		ValidationUtils.assertNotNull(message, "Message cannot be null.");

		Map<M, Boolean> messages = getSwiftUtilHandler().getAllMessagesMap(message);
		return messages.keySet()
				.stream()
				.map(this::getSwiftIdentifierForMessage)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean hasOnlySwiftMessageStatus(SwiftMessagingMessage message, SwiftMessageType swiftMessageType, SwiftMessageStatus.SwiftMessageStatusNames swiftMessageStatus) {
		AssertUtils.assertNotNull(message, "Swift messaging message is required");
		AssertUtils.assertNotNull(swiftMessageType, "Swift Message Type is required.");
		AssertUtils.assertNotEmpty(message.getUniqueStringIdentifier(), "Unique string identifier for a message cannot be empty.");
		AssertUtils.assertNotNull(swiftMessageStatus, "Swift Message Status is required.");

		SwiftMessageStatus status = getSwiftServerMessageService().getSwiftMessageStatusByName(swiftMessageStatus.name());

		SwiftMessageSearchForm swiftMessageSearchForm = new SwiftMessageSearchForm();
		swiftMessageSearchForm.setUniqueStringIdentifier(message.getUniqueStringIdentifier());
		swiftMessageSearchForm.setExcludeMessageStatusId(status.getId());
		swiftMessageSearchForm.setTypeId(swiftMessageType.getId());
		swiftMessageSearchForm.setLimit(1);

		return CollectionUtils.isEmpty(getSwiftServerMessageService().getSwiftMessageList(swiftMessageSearchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SwiftSourceSystem getSwiftSourceSystemFromMessage(SwiftMessagingMessage message) {
		String sourceSystemName = message.getSourceSystemName();
		if (!StringUtils.isEmpty(sourceSystemName)) {
			return getSwiftSourceSystemService().getSwiftSourceSystemByName(sourceSystemName);
		}
		return null;
	}


	private SwiftSourceSystem getSwiftSourceSystemFromMessage(M message) {
		String bic = getSwiftUtilHandler().getSenderBusinessIdentifierCode(message);
		return getSwiftSourceSystemService().getSwiftSourceSystemByBIC(bic);
	}


	private SwiftIdentifier createSwiftIdentifierFromMessage(SwiftMessagingMessage message, SwiftSourceSystem swiftSourceSystem, Long messageReferenceNumber) {
		AssertUtils.assertNotNull(message, "Swift Transfer Message cannot be null");
		String uniqueStringIdentifier = message.getUniqueStringIdentifier();
		ValidationUtils.assertNotEmpty(uniqueStringIdentifier, "Swift unique identifier string cannot be empty.");

		try {
			String sourceSystemModuleName = message.getSourceSystemModuleName();
			ValidationUtils.assertNotEmpty(sourceSystemModuleName, "Source System module name cannot be empty.");
			SwiftSourceSystemModule module = getSwiftSourceSystemService().getSwiftSourceSystemModuleByName(sourceSystemModuleName, swiftSourceSystem.getId());
			ValidationUtils.assertNotNull(module, "Cannot find SwiftSourceSystemModule for [" + sourceSystemModuleName + "].");

			SwiftIdentifier identifier = new SwiftIdentifier();
			identifier.setUniqueStringIdentifier(uniqueStringIdentifier);
			if (Objects.nonNull(messageReferenceNumber)) {
				SwiftIdentifier referencedIdentifier = getSwiftIdentifierService().getSwiftIdentifier(messageReferenceNumber);
				identifier.setReferencedIdentifier(referencedIdentifier);
			}
			identifier.setSourceSystemFkFieldId(message.getSourceSystemFkFieldId());
			identifier.setSourceSystem(swiftSourceSystem);
			identifier.setSourceSystemModule(module);

			return getSwiftIdentifierService().saveSwiftIdentifier(identifier);
		}
		catch (ValidationException e) {
			throw e;
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to create swift identifier for uniqueId [" + uniqueStringIdentifier + "].", e);
		}
	}


	private SwiftMessageType getSwiftMessageType(M message) {
		if (getSwiftUtilHandler().isSystemAcknowledgementMessage(message)) {
			return getSwiftServerMessageService().getSwiftMessageTypeByName(SwiftMessageService.MT_ACK_NAK_MESSAGE_TYPE_NAME);
		}
		SwiftMessageType result = null;
		String typeCode = getSwiftUtilHandler().getMessageTypeCode(message);
		if (!StringUtils.isEmpty(typeCode)) {
			result = getSwiftServerMessageService().getSwiftMessageTypeVersionCode(typeCode);
		}
		if (result == null) {
			result = getSwiftServerMessageService().getSwiftMessageTypeByName(SwiftMessageService.MT_UNKNOWN_MESSAGE_TYPE_NAME);
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftUtilHandler<M> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<M> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}


	public SwiftIdentifierService getSwiftIdentifierService() {
		return this.swiftIdentifierService;
	}


	public void setSwiftIdentifierService(SwiftIdentifierService swiftIdentifierService) {
		this.swiftIdentifierService = swiftIdentifierService;
	}


	public SwiftSourceSystemService getSwiftSourceSystemService() {
		return this.swiftSourceSystemService;
	}


	public void setSwiftSourceSystemService(SwiftSourceSystemService swiftSourceSystemService) {
		this.swiftSourceSystemService = swiftSourceSystemService;
	}
}

