package com.clifton.swift.message.log;


import com.clifton.swift.message.SwiftMessageFormats;

import java.util.function.Function;


/**
 * @author mwacker
 */
public interface SwiftMessageLogHandler {

	/**
	 * Logs a SWIFT message to the message log.
	 */
	public SwiftMessageLog logSwiftMessage(String text, boolean incoming, SwiftMessageFormats messageFormat);


	/**
	 * Calls the message log processing function and will update the message log with any processing error or delete
	 * it if the are no errors.
	 */
	public <V> V processSwiftMessage(SwiftMessageLog messageLog, Function<SwiftMessageLog, V> functionReturningPropertyValue);
}
