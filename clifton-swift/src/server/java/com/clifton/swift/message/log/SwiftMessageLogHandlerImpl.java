package com.clifton.swift.message.log;

import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.swift.message.SwiftMessageFormats;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.function.Function;


/**
 * @author mwacker
 */
@Component
public class SwiftMessageLogHandlerImpl implements SwiftMessageLogHandler {

	private SwiftMessageLogService swiftServerMessageLogService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftMessageLog logSwiftMessage(String text, boolean incoming, SwiftMessageFormats messageFormat) {
		SwiftMessageLog messageLog = new SwiftMessageLog();
		messageLog.setText(text);
		messageLog.setMessageDateAndTime(new Date());
		messageLog.setIncoming(incoming);
		messageLog.setMessageFormat(messageFormat);
		return getSwiftServerMessageLogService().saveSwiftMessageLog(messageLog);
	}


	@Override
	public <R> R processSwiftMessage(SwiftMessageLog messageLog, Function<SwiftMessageLog, R> logProcessingFunction) {
		boolean hasError = false;
		try {
			return callWithTransaction(messageLog, logProcessingFunction);
		}
		catch (Exception e) {
			hasError = true;
			messageLog.setErrorMessage(StringUtils.formatStringUpToNCharsWithDots(e.getMessage() + "\nCAUSED BY: " + ExceptionUtils.getFullStackTrace(e), 3996));
			getSwiftServerMessageLogService().saveSwiftMessageLog(messageLog);
		}
		finally {
			if (!hasError) {
				getSwiftServerMessageLogService().deleteSwiftMessageLog(messageLog.getId());
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected <R> R callWithTransaction(SwiftMessageLog messageLog, Function<SwiftMessageLog, R> logProcessingFunction) {
		return logProcessingFunction.apply(messageLog);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageLogService getSwiftServerMessageLogService() {
		return this.swiftServerMessageLogService;
	}


	public void setSwiftServerMessageLogService(SwiftMessageLogService swiftServerMessageLogService) {
		this.swiftServerMessageLogService = swiftServerMessageLogService;
	}
}
