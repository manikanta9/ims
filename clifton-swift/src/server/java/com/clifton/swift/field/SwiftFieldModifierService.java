package com.clifton.swift.field;

import com.clifton.swift.field.search.SwiftFieldModifierSearchForm;

import java.util.List;


/**
 * <code>SwiftFieldModifierTypeHandler</code> fetch and save field modifiers.
 */
public interface SwiftFieldModifierService {

	public SwiftFieldModifier getSwiftFieldModifier(int id);


	public SwiftFieldModifier saveSwiftFieldModifier(SwiftFieldModifier fieldModifier);


	public List<SwiftFieldModifier> getSwiftFieldModifierList(final SwiftFieldModifierSearchForm searchForm);


	public void deleteSwiftFieldModifier(int id);


	public SwiftFieldModifierType saveSwiftFieldModifierType(SwiftFieldModifierType swiftFieldModifierType);


	public SwiftFieldModifierType getSwiftFieldModifierType(String typeName);


	public SwiftFieldModifierDataType saveSwiftFieldModifierDataType(SwiftFieldModifierDataType systemDataType);


	public SwiftFieldModifierDataType getSwiftFieldModifierDataType(String dataTypeName);


	public SwiftFieldModifierPropertyType saveSwiftFieldModifierPropertyType(SwiftFieldModifierPropertyType swiftFieldModifierPropertyType);


	public SwiftFieldModifierPropertyType getSwiftFieldModifierPropertyType(String propertyTypeName);


	public SwiftFieldModifierFilter saveSwiftFieldModifierFilter(SwiftFieldModifierFilter swiftFieldModifierFilter);


	public SwiftFieldModifierFilter getSwiftFieldModifierFilter(int id);


	public List<SwiftFieldModifierFilter> getSwiftFieldModifierFilterListByModifier(int modifierId);


	public SwiftFieldModifierProperty saveSwiftFieldModifierProperty(SwiftFieldModifierProperty swiftFieldModifierProperty);


	public SwiftFieldModifierProperty getSwiftFieldModifierProperty(Integer id);


	public List<SwiftFieldModifierProperty> getSwiftFieldModifierPropertyListByModifier(int modifierId);
}
