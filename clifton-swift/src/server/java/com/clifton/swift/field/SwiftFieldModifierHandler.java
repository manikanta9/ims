package com.clifton.swift.field;

import java.util.regex.Pattern;


/**
 * The <code>SwiftFieldModifierBean</code> contains a list of all registered
 * {@link SwiftFieldModifierBean} implementations.  It will get all field modifiers for a specific
 * message type and apply them to the provided message, no clone is created.
 */
public interface SwiftFieldModifierHandler<T> {

	public void applyFieldModifiers(T message);


	public void register(SwiftFieldModifierBean<T> swiftFieldModifierBean);


	public Pattern getPatternForModifier(int fieldModifierId);
}
