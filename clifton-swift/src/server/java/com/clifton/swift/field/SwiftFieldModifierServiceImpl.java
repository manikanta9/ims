package com.clifton.swift.field;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.field.search.SwiftFieldModifierSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * {@inheritDoc}
 */
@Component
public class SwiftFieldModifierServiceImpl implements SwiftFieldModifierService {

	public static final String FIELD_MODIFIER_CACHE = "com.clifton.swift.server.field.modifier.FIELD_MODIFIER_CACHE";

	private AdvancedUpdatableDAO<SwiftFieldModifier, Criteria> swiftFieldModifierDAO;
	private AdvancedUpdatableDAO<SwiftFieldModifierFilter, Criteria> swiftFieldModifierFilterDAO;
	private AdvancedUpdatableDAO<SwiftFieldModifierProperty, Criteria> swiftFieldModifierPropertyDAO;
	private AdvancedUpdatableDAO<SwiftFieldModifierType, Criteria> swiftFieldModifierTypeDAO;
	private AdvancedUpdatableDAO<SwiftFieldModifierDataType, Criteria> swiftFieldModifierDataTypeDAO;
	private AdvancedUpdatableDAO<SwiftFieldModifierPropertyType, Criteria> swiftFieldModifierPropertyTypeDAO;

	private CacheHandler<Integer, SwiftFieldModifier> cacheHandler;

	////////////////////////////////////////////////////////////////////////////
	////////              SwiftFieldModifier                         ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftFieldModifier getSwiftFieldModifier(int id) {
		SwiftFieldModifier result = getCacheHandler().get(FIELD_MODIFIER_CACHE, id);
		if (result == null) {
			result = getSwiftFieldModifierDAO().findByPrimaryKey(id);
			if (result != null) {
				List<SwiftFieldModifierProperty> fieldModifierProperties = getSwiftFieldModifierPropertyListByModifier(id);
				result.setPropertyList(fieldModifierProperties);
				List<SwiftFieldModifierFilter> swiftFieldModifierFilters = getSwiftFieldModifierFilterListByModifier(id);
				result.setFilterList(swiftFieldModifierFilters);
				getCacheHandler().put(FIELD_MODIFIER_CACHE, id, result);
			}
		}

		return result;
	}


	@Override
	public SwiftFieldModifier saveSwiftFieldModifier(SwiftFieldModifier fieldModifier) {
		ValidationUtils.assertNotNull(fieldModifier.getFieldModifierType(), "Swift Field Modifier Type is required.");
		return getSwiftFieldModifierDAO().save(fieldModifier);
	}


	@Override
	public void deleteSwiftFieldModifier(int id) {
		// cascade delete
		List<SwiftFieldModifierFilter> fixFieldModifierFilterList = getSwiftFieldModifierFilterListByModifier(id);
		List<SwiftFieldModifierProperty> fixFieldModifierPropertyList = getSwiftFieldModifierPropertyListByModifier(id);

		if (fixFieldModifierFilterList != null) {
			getSwiftFieldModifierFilterDAO().deleteList(fixFieldModifierFilterList);
		}

		if (fixFieldModifierPropertyList != null) {
			getSwiftFieldModifierPropertyDAO().deleteList(fixFieldModifierPropertyList);
		}

		getSwiftFieldModifierDAO().delete(id);
	}


	@Override
	public List<SwiftFieldModifier> getSwiftFieldModifierList(SwiftFieldModifierSearchForm searchForm) {
		return getSwiftFieldModifierDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              SwiftFieldModifierType                     ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftFieldModifierType saveSwiftFieldModifierType(SwiftFieldModifierType swiftFieldModifierType) {
		ValidationUtils.assertNotEmpty(swiftFieldModifierType.getClassName(), "Swift Field Modifier Class Name is required.");
		return getSwiftFieldModifierTypeDAO().save(swiftFieldModifierType);
	}


	@Override
	public SwiftFieldModifierType getSwiftFieldModifierType(String typeName) {
		return CollectionUtils.getOnlyElement(getSwiftFieldModifierTypeDAO().findByField("name", typeName));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              SystemDataType                             ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftFieldModifierDataType saveSwiftFieldModifierDataType(SwiftFieldModifierDataType systemDataType) {
		return getSwiftFieldModifierDataTypeDAO().save(systemDataType);
	}


	@Override
	public SwiftFieldModifierDataType getSwiftFieldModifierDataType(String dataTypeName) {
		return CollectionUtils.getOnlyElement(getSwiftFieldModifierDataTypeDAO().findByField("name", dataTypeName));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              SwiftFieldModifierPropertyType             ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftFieldModifierPropertyType saveSwiftFieldModifierPropertyType(SwiftFieldModifierPropertyType swiftFieldModifierPropertyType) {
		ValidationUtils.assertNotNull(swiftFieldModifierPropertyType.getFieldModifierType(), "Swift Field Modifier Data Type is required.");
		ValidationUtils.assertNotNull(swiftFieldModifierPropertyType.getFieldModifierType(), "Swift Field Modifier Type is required.");

		return getSwiftFieldModifierPropertyTypeDAO().save(swiftFieldModifierPropertyType);
	}


	@Override
	public SwiftFieldModifierPropertyType getSwiftFieldModifierPropertyType(String propertyTypeName) {
		return CollectionUtils.getOnlyElement(getSwiftFieldModifierPropertyTypeDAO().findByField("name", propertyTypeName));
	}

	////////////////////////////////////////////////////////////////////////////
	////////              SwiftFieldModifierFilter                   ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftFieldModifierFilter saveSwiftFieldModifierFilter(SwiftFieldModifierFilter swiftFieldModifierFilter) {
		return getSwiftFieldModifierFilterDAO().save(swiftFieldModifierFilter);
	}


	@Override
	public SwiftFieldModifierFilter getSwiftFieldModifierFilter(int id) {
		return getSwiftFieldModifierFilterDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SwiftFieldModifierFilter> getSwiftFieldModifierFilterListByModifier(int modifierId) {
		return getSwiftFieldModifierFilterDAO().findByField("fieldModifier.id", modifierId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              SwiftFieldModifierProperty                 ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftFieldModifierProperty saveSwiftFieldModifierProperty(SwiftFieldModifierProperty swiftFieldModifierProperty) {
		return getSwiftFieldModifierPropertyDAO().save(swiftFieldModifierProperty);
	}


	@Override
	public SwiftFieldModifierProperty getSwiftFieldModifierProperty(Integer id) {
		return getSwiftFieldModifierPropertyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SwiftFieldModifierProperty> getSwiftFieldModifierPropertyListByModifier(int modifierId) {
		return getSwiftFieldModifierPropertyDAO().findByField("fieldModifier.id", modifierId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SwiftFieldModifier, Criteria> getSwiftFieldModifierDAO() {
		return this.swiftFieldModifierDAO;
	}


	public void setSwiftFieldModifierDAO(AdvancedUpdatableDAO<SwiftFieldModifier, Criteria> swiftFieldModifierDAO) {
		this.swiftFieldModifierDAO = swiftFieldModifierDAO;
	}


	public AdvancedUpdatableDAO<SwiftFieldModifierFilter, Criteria> getSwiftFieldModifierFilterDAO() {
		return this.swiftFieldModifierFilterDAO;
	}


	public void setSwiftFieldModifierFilterDAO(AdvancedUpdatableDAO<SwiftFieldModifierFilter, Criteria> swiftFieldModifierFilterDAO) {
		this.swiftFieldModifierFilterDAO = swiftFieldModifierFilterDAO;
	}


	public AdvancedUpdatableDAO<SwiftFieldModifierProperty, Criteria> getSwiftFieldModifierPropertyDAO() {
		return this.swiftFieldModifierPropertyDAO;
	}


	public void setSwiftFieldModifierPropertyDAO(AdvancedUpdatableDAO<SwiftFieldModifierProperty, Criteria> swiftFieldModifierPropertyDAO) {
		this.swiftFieldModifierPropertyDAO = swiftFieldModifierPropertyDAO;
	}


	public AdvancedUpdatableDAO<SwiftFieldModifierType, Criteria> getSwiftFieldModifierTypeDAO() {
		return this.swiftFieldModifierTypeDAO;
	}


	public void setSwiftFieldModifierTypeDAO(AdvancedUpdatableDAO<SwiftFieldModifierType, Criteria> swiftFieldModifierTypeDAO) {
		this.swiftFieldModifierTypeDAO = swiftFieldModifierTypeDAO;
	}


	public AdvancedUpdatableDAO<SwiftFieldModifierDataType, Criteria> getSwiftFieldModifierDataTypeDAO() {
		return this.swiftFieldModifierDataTypeDAO;
	}


	public void setSwiftFieldModifierDataTypeDAO(AdvancedUpdatableDAO<SwiftFieldModifierDataType, Criteria> swiftFieldModifierDataTypeDAO) {
		this.swiftFieldModifierDataTypeDAO = swiftFieldModifierDataTypeDAO;
	}


	public AdvancedUpdatableDAO<SwiftFieldModifierPropertyType, Criteria> getSwiftFieldModifierPropertyTypeDAO() {
		return this.swiftFieldModifierPropertyTypeDAO;
	}


	public void setSwiftFieldModifierPropertyTypeDAO(AdvancedUpdatableDAO<SwiftFieldModifierPropertyType, Criteria> swiftFieldModifierPropertyTypeDAO) {
		this.swiftFieldModifierPropertyTypeDAO = swiftFieldModifierPropertyTypeDAO;
	}


	public CacheHandler<Integer, SwiftFieldModifier> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Integer, SwiftFieldModifier> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
