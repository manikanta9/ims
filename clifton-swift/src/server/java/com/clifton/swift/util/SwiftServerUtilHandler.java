package com.clifton.swift.util;

import com.clifton.swift.message.SwiftMessageField;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;

import java.util.List;


/**
 * The {@link SwiftServerUtilHandler} provides the subset of Swift utility handler methods which are intended for server-side-only usage.
 *
 * @author MikeH
 * @see SwiftUtilHandler
 */
public interface SwiftServerUtilHandler<M> {

	public List<SwiftMessageField> getSwiftMessageFieldList(String messageString);


	public SwiftMessagingMessage buildSwiftMessagingMessage(M abstractMessage, boolean suppressErrors);
}
