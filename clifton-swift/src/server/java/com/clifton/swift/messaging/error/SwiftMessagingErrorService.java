package com.clifton.swift.messaging.error;

import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;


/**
 * <code>SwiftMessagingErrorServiceImpl</code> allow sending messages to the JSM error queue.
 * This should be used by JMS automatically, however, this allows very low level Spring Integration
 * exceptions to be placed into the same queue.
 */
public interface SwiftMessagingErrorService {

	public void postErrorMessage(SwiftMessagingMessage swiftMessagingMessage, Throwable throwable);
}
