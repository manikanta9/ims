package com.clifton.swift.messaging.synchronous.handler;

import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @see SwiftSynchronousMessageHandlerLocator
 */
@Component
@SuppressWarnings("rawtypes")
public class SwiftSynchronousMessageHandlerLocatorImpl implements SwiftSynchronousMessageHandlerLocator, InitializingBean, ApplicationContextAware {

	private Map<Class<? extends SynchronousRequestMessage>, SwiftSynchronousMessageHandler> handlerMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SwiftSynchronousMessageHandler locate(Class<? extends SynchronousRequestMessage> request) {
		return getHandlerMap().get(request);
	}


	@Override
	public void afterPropertiesSet() {
		Map<String, SwiftSynchronousMessageHandler> beanMap = getApplicationContext().getBeansOfType(SwiftSynchronousMessageHandler.class);

		for (Map.Entry<String, SwiftSynchronousMessageHandler> stringSwiftSynchronousMessageHandlerEntry : beanMap.entrySet()) {
			SwiftSynchronousMessageHandler handler = stringSwiftSynchronousMessageHandlerEntry.getValue();
			@SuppressWarnings("unchecked")
			Class<SynchronousRequestMessage> request = handler.getSupportedRequestType();
			if (getHandlerMap().containsKey(request)) {
				throw new RuntimeException("Cannot register '" + stringSwiftSynchronousMessageHandlerEntry.getKey() + "' as a generator for request '" + request
						+ "' because this request already has a registered handler.");
			}
			getHandlerMap().put(request, handler);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<Class<? extends SynchronousRequestMessage>, SwiftSynchronousMessageHandler> getHandlerMap() {
		return this.handlerMap;
	}


	public void setHandlerMap(Map<Class<? extends SynchronousRequestMessage>, SwiftSynchronousMessageHandler> handlerMap) {
		this.handlerMap = handlerMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
