package com.clifton.swift.messaging.synchronous.handler;

import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousResponseMessage;


/**
 * <code>SwiftSynchronousMessageHandler</code> accepts a message from the JMS synchronous queue and creates the appropriate response
 * message for putting back on the JSM queue.
 */
public interface SwiftSynchronousMessageHandler<R extends SynchronousResponseMessage, T extends SynchronousRequestMessage> {

	public R handle(T requestMessage);


	public Class<T> getSupportedRequestType();
}
