package com.clifton.swift.messaging.error;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.AbstractXStreamMessageHandler;
import com.clifton.core.messaging.jms.JmsMessageCreatorUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProducerCallback;
import com.clifton.core.messaging.jms.asynchronous.JmsAsynchronousErrorMessage;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.SystemUtils;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Message;
import java.util.Map;
import java.util.UUID;


/**
 * {@inheritDoc}
 */
public class SwiftMessagingErrorServiceImpl extends AbstractXStreamMessageHandler implements SwiftMessagingErrorService {

	private JmsTemplate jmsTemplate;
	private String errorQueue;
	private Map<String, String> defaultMessageProperties;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void postErrorMessage(SwiftMessagingMessage swiftMessagingMessage, Throwable throwable) {
		JmsAsynchronousErrorMessage errorMessage = new JmsAsynchronousErrorMessage();
		errorMessage.setError(throwable);
		MessageTypes swiftMessageType = ObjectUtils.coalesce(JmsMessageCreatorUtils.getMessageType(swiftMessagingMessage), getDefaultMessageType());
		MessageTypes errorMessageType = ObjectUtils.coalesce(JmsMessageCreatorUtils.getMessageType(errorMessage), getDefaultMessageType());
		getJmsTemplate().execute(new AsynchronousProducerCallback(session -> {
			// set the machine name.
			String machineName = SystemUtils.getMachineName();
			errorMessage.setMachineName(machineName);

			if ((getDefaultMessageProperties() != null) && (!getDefaultMessageProperties().isEmpty())) {
				for (Map.Entry<String, String> entry : getDefaultMessageProperties().entrySet()) {
					BeanUtils.setPropertyValue(errorMessage, entry.getKey(), entry.getValue(), true);
				}
			}

			// embed the swift messaging message into the error message.
			Message swiftMessage = JmsMessageCreatorUtils.createMessage(swiftMessagingMessage, session, swiftMessageType, getJmsTemplate().getMessageConverter());
			errorMessage.setMessage(swiftMessage);

			// wrap the serialized error message in a JMS message.
			Message returnMessage = JmsMessageCreatorUtils.createMessage(errorMessage, session, errorMessageType, getJmsTemplate().getMessageConverter());

			// add the machine name to the actual JMS message
			returnMessage.setStringProperty(MessagingMessage.MESSAGE_MACHINE_NAME_PROPERTY_NAME, machineName);

			String corId = UUID.randomUUID().toString();
			returnMessage.setJMSCorrelationID(corId);

			if ((getDefaultMessageProperties() != null) && (!getDefaultMessageProperties().isEmpty())) {
				for (Map.Entry<String, String> entry : getDefaultMessageProperties().entrySet()) {
					returnMessage.setStringProperty(entry.getKey(), entry.getValue());
				}
			}

			if (!errorMessage.getProperties().isEmpty()) {
				for (Map.Entry<String, String> entry : errorMessage.getProperties().entrySet()) {
					returnMessage.setStringProperty(entry.getKey(), entry.getValue());
				}
			}

			return returnMessage;
		}, getErrorQueue(), getJmsTemplate()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public String getErrorQueue() {
		return this.errorQueue;
	}


	public void setErrorQueue(String errorQueue) {
		this.errorQueue = errorQueue;
	}


	public Map<String, String> getDefaultMessageProperties() {
		return this.defaultMessageProperties;
	}


	public void setDefaultMessageProperties(Map<String, String> defaultMessageProperties) {
		this.defaultMessageProperties = defaultMessageProperties;
	}
}
