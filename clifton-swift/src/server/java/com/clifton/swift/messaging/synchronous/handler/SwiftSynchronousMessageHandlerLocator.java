package com.clifton.swift.messaging.synchronous.handler;

import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;


/**
 * <code>SwiftSynchronousMessageHandlerLocator</code> Gather all the {@link SwiftSynchronousMessageHandler} implementations and return the correct handler
 * corresponding the the request message.
 */
@SuppressWarnings("rawtypes")
public interface SwiftSynchronousMessageHandlerLocator {

	public SwiftSynchronousMessageHandler locate(Class<? extends SynchronousRequestMessage> request);
}
