import com.clifton.gradle.plugin.build.registerVariant

val clientVariant = registerVariant("client", dependsOnMain = true)
val serverVariant = registerVariant("server", dependsOnMain = true)

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// Used by prowide
	runtimeOnly("org.apache.derby:derby")
	runtimeOnly("javax.xml.bind:jaxb-api")
	runtimeOnly("com.sun.xml.bind:jaxb-impl:2.3.1")
	runtimeOnly("com.google.code.gson:gson")
	runtimeOnly("org.mozilla:rhino:1.7.7.2")
	runtimeOnly("org.apache.commons:commons-lang3")
	runtimeOnly("org.apache.commons:commons-text:1.6")

	api("com.prowidesoftware:pw-iso20022:SRU2021-9.2.0")
	api("com.prowidesoftware:pw-swift-core:SRU2021-9.2.0")
	api("com.prowidesoftware:pw-swift-integrator-data:SRU2021-9.2.0")
	api("com.prowidesoftware:pw-swift-integrator-sdk:SRU2021-9.2.0")
	api("com.prowidesoftware:pw-swift-integrator-translations:SRU2021-9.2.0")
	api("com.prowidesoftware:pw-swift-integrator-validation:SRU2021-9.2.0")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-instruction"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-instruction")))
	testFixturesApi(testFixtures(project(":clifton-system")))

	testImplementation("org.springframework.integration:spring-integration-core")
	testImplementation("org.springframework.integration:spring-integration-file")
}
