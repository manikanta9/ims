dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-core"))
	api(project(":clifton-core:clifton-core-messaging"))
	api(project(":clifton-system"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
}
