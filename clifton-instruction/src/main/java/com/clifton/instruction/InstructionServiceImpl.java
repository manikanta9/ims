package com.clifton.instruction;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.export.InstructionExportHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.generator.InstructionGeneratorLocator;
import com.clifton.instruction.messages.InstructionMessage;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


/**
 * @author mwacker
 */
@Service
public class InstructionServiceImpl<M extends InstructionMessage, P, I extends Instruction> implements InstructionService<M> {

	private InstructionExportHandler instructionExportHandler;
	private DaoLocator daoLocator;
	private InstructionGeneratorLocator<M, IdentityObject, P> instructionGeneratorLocator;
	private InstructionUtilHandler<I, P> instructionUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean sendInstruction(Instruction instruction) {
		return sendInstruction(instruction, false);
	}


	@Override
	public boolean cancelInstruction(Instruction instruction) {
		return sendInstruction(instruction, true);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<M> getInstructionMessage(Instruction instruction, boolean cancel) {
		IdentityObject dto = getIdentityObject(instruction);
		InstructionGenerator<M, IdentityObject, P> generator = getInstructionGeneratorLocator().locate((Class<IdentityObject>) dto.getClass());
		List<M> messages = (cancel ? generator.generateInstructionCancelMessageList(instruction) : generator.generateInstructionMessageList(instruction));
		if (!messages.isEmpty()) {
			List<String> additionalBics = getInstructionUtilHandler().getAdditionalRecipientBicList(generator.getDeliveryInstructionProvider(instruction, dto));
			List<M> additional = (List<M>) cloneToAdditionalRecipients(messages.get(0), additionalBics);
			messages.addAll(additional);
		}
		return messages;
	}


	@Override
	public List<InstructionMessage> cloneToAdditionalRecipients(InstructionMessage instructionMessage, List<String> receiverBics) {
		AtomicInteger sequence = new AtomicInteger(1);
		return receiverBics.stream()
				.filter(b -> !Objects.equals(b, instructionMessage.getReceiverBIC()))
				.map(b -> {
					InstructionMessage duplicate = BeanUtils.newInstance(instructionMessage.getClass());
					BeanUtils.copyProperties(instructionMessage, duplicate);
					duplicate.setReceiverBIC(b);
					duplicate.setTransactionReferenceNumber(getTransactionReferenceDuplicate(instructionMessage.getTransactionReferenceNumber(), sequence.getAndIncrement()));
					return duplicate;
				}).collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean sendInstruction(Instruction instruction, boolean cancel) {
		AssertUtils.assertNotNull(getDaoLocator(), "daoLocator is missing: cannot locate dao for table " + instruction.getSystemTable().getName());
		List<M> messageList = getInstructionMessage(instruction, cancel);
		if (!CollectionUtils.isEmpty(messageList)) {
			getInstructionExportHandler().send(messageList);
			return true;
		}
		return false;
	}


	private IdentityObject getIdentityObject(Instruction instruction) {
		ReadOnlyDAO<IdentityObject> dao = this.daoLocator.locate(instruction.getSystemTable().getName());
		return dao.findByPrimaryKey(instruction.getFkFieldId());
	}


	private String getTransactionReferenceDuplicate(String transactionNumber, int sequence) {
		AssertUtils.assertNotNull(transactionNumber, "Transaction Number is required.");
		return transactionNumber.concat("_DUPLICATE_").concat(Integer.toString(sequence));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InstructionGeneratorLocator<M, IdentityObject, P> getInstructionGeneratorLocator() {
		return this.instructionGeneratorLocator;
	}


	public void setInstructionGeneratorLocator(InstructionGeneratorLocator<M, IdentityObject, P> instructionGeneratorLocator) {
		this.instructionGeneratorLocator = instructionGeneratorLocator;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public InstructionExportHandler getInstructionExportHandler() {
		return this.instructionExportHandler;
	}


	public void setInstructionExportHandler(InstructionExportHandler instructionExportHandler) {
		this.instructionExportHandler = instructionExportHandler;
	}


	public InstructionUtilHandler<I, P> getInstructionUtilHandler() {
		return this.instructionUtilHandler;
	}


	public void setInstructionUtilHandler(InstructionUtilHandler<I, P> instructionUtilHandler) {
		this.instructionUtilHandler = instructionUtilHandler;
	}
}
