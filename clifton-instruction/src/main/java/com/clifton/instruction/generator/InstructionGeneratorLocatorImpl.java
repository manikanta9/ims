package com.clifton.instruction.generator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.AssertUtils;
import com.clifton.instruction.messages.InstructionMessage;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>InstructionGeneratorLocatorImpl</code> class locates InstructionGenerator implementations for corresponding classes.
 * It auto-discovers all application context beans that implement InstructionGenerator interface and registers them so that they can be located.
 *
 * @author mwacker
 */
@Component
public class InstructionGeneratorLocatorImpl<M extends InstructionMessage, E extends IdentityObject, P> implements InstructionGeneratorLocator<M, E, P>, InitializingBean, ApplicationContextAware {

	private Map<Class<E>, InstructionGenerator<M, E, P>> instructionGeneratorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InstructionGenerator<M, E, P> locate(Class<E> entityClass) {
		AssertUtils.assertNotNull(entityClass, "Required entity class cannot be null.");
		InstructionGenerator<M, E, P> result = getInstructionGeneratorMap().get(entityClass);
		if (result == null) {
			// CollateralBalance -> OTCCollateralBalance, OptionsEscrowCollateralBalance, OptionsMarginCollateralBalance
			result = getInstructionGeneratorMap().get(entityClass.getSuperclass());
		}
		AssertUtils.assertNotNull(result, "Cannot locate InstructionGenerator for  the [" + entityClass + "] class.");
		return result;
	}


	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void afterPropertiesSet() {
		Map<String, InstructionGenerator> beanMap = getApplicationContext().getBeansOfType(InstructionGenerator.class);

		// need a map with journal type names as keys instead of bean names
		for (Map.Entry<String, InstructionGenerator> stringInstructionGeneratorEntry : beanMap.entrySet()) {
			InstructionGenerator processor = stringInstructionGeneratorEntry.getValue();
			if (getInstructionGeneratorMap().containsKey(processor.getSupportedMessageClass())) {
				throw new RuntimeException("Cannot register '" + stringInstructionGeneratorEntry.getKey() + "' as a generator for messages of type '" + processor.getSupportedMessageClass()
						+ "' because this type already has a registered generator.");
			}
			getInstructionGeneratorMap().put(processor.getSupportedMessageClass(), processor);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<Class<E>, InstructionGenerator<M, E, P>> getInstructionGeneratorMap() {
		return this.instructionGeneratorMap;
	}


	public void setInstructionGeneratorMap(Map<Class<E>, InstructionGenerator<M, E, P>> instructionGeneratorMap) {
		this.instructionGeneratorMap = instructionGeneratorMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
