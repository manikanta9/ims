package com.clifton.instruction.generator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.instruction.messages.InstructionMessage;


/**
 * The <code>InstructionGeneratorLocator</code> interface defines a method for locating InstructionGenerator objects
 * that are registered for each instruction class.
 *
 * @author mwacker
 */
public interface InstructionGeneratorLocator<M extends InstructionMessage, E extends IdentityObject, P> {

	/**
	 * Returns InstructionGenerator registered for the specified class.
	 */
	public InstructionGenerator<M, E, P> locate(Class<E> entityClass);
}
