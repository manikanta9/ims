package com.clifton.instruction.generator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.InstructionMessage;

import java.util.List;


/**
 * Should be implemented by objects that will provide conversions of DTO objects to instruction entities.
 * For example, this will be implemented in the accounting project to generate NoticeToReceiveTransferMessage
 * and GeneralFinancialInstitutionTransferMessage items for M2M entries.
 *
 * @author mwacker
 */
public interface InstructionGenerator<M extends InstructionMessage, E extends IdentityObject, P> {

	public static final String REFERENCE_COMPONENT_DELIMITER = ";";


	/**
	 * Return the table identifier string with the format: data source alias (IMS) + entity ID + table alias (MTM).
	 * The uniqueStringIdentifier parameter can be a either a packed string in the format (IMS101T;IMS202TF;IMS303AT)
	 * or a single table reference (IMS231MTM).  Returns the first component of the packed string (Trade), otherwise,
	 * returns the string as is.
	 */
	public static String getItemEntityUniqueStringIdentifier(String uniqueStringIdentifier) {
		AssertUtils.assertNotEmpty(uniqueStringIdentifier, "Expected a SWIFT message unique string identifier.");
		List<String> componentParts = CollectionUtils.createList(uniqueStringIdentifier.split(InstructionGenerator.REFERENCE_COMPONENT_DELIMITER));
		return CollectionUtils.getFirstElement(componentParts);
	}


	public Class<E> getSupportedMessageClass();


	public boolean isCancellationSupported();


	public List<M> generateInstructionMessageList(Instruction instruction);


	/**
	 * Generate a cancel instruction messages for the for the provided entity.
	 * A Trade dtoObject would return a list of AbstractTradeMessage implementations, for example, Receive or Deliver Against Instruction objects.
	 */
	public List<M> generateInstructionCancelMessageList(Instruction instruction);


	/**
	 * Get the list of additional recipients for the provided instruction and source
	 */
	public P getDeliveryInstructionProvider(Instruction instruction, E source);
}
