package com.clifton.instruction;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.instruction.messages.InstructionMessage;

import java.util.List;


/**
 * Defines methods used to generate and send instructions to an external party.
 *
 * @author mwacker
 */
public interface InstructionService<M extends InstructionMessage> {

	/**
	 * Create a list of duplicated Instruction Messages with the same type as the provided Instruction Message and using the receiver BIC from the provided BICs.
	 */
	@DoNotAddRequestMapping
	public List<InstructionMessage> cloneToAdditionalRecipients(InstructionMessage instructionMessage, List<String> receiverBics);


	/**
	 * Generate an InstructionEntity from and Instruction and send it via the InstructionExportHandler.
	 *
	 * @return true if an instruction was send, false if it was skipped
	 */
	@DoNotAddRequestMapping
	public boolean sendInstruction(Instruction instruction);


	/**
	 * Generate a cancellation Instruction Entity from an Instruction and send it via the InstructionExportHandler.
	 *
	 * @return true if an instruction was send, false if it was skipped
	 */
	@DoNotAddRequestMapping
	public boolean cancelInstruction(Instruction instruction);


	/**
	 * Get the instruction message for the provided instruction.
	 */
	@DoNotAddRequestMapping
	public List<M> getInstructionMessage(Instruction instruction, boolean cancel);
}
