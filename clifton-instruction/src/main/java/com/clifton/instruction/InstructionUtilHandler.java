package com.clifton.instruction;

import com.clifton.core.beans.IdentityObject;

import java.util.List;


public interface InstructionUtilHandler<I extends Instruction, P> {

	public List<I> getInstructionList(String systemTableName, Integer fkFieldId);


	/**
	 * Build the Transaction Reference Number from entities used to build the instruction message.
	 * Unique Identifier String, example:  IMS1234II;IMS101P;IMS202PD
	 */
	public String getTransactionReferenceNumber(List<IdentityObject> components);


	/**
	 * Get the list of additional party BICs using the provider.
	 */
	List<String> getAdditionalRecipientBicList(P selectionProvider);
}
