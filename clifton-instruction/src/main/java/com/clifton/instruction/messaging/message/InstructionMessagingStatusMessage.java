package com.clifton.instruction.messaging.message;

import com.clifton.core.messaging.asynchronous.AbstractAsynchronousMessage;
import com.clifton.instruction.messaging.InstructionStatuses;


/**
 * @author mwacker
 */
public class InstructionMessagingStatusMessage extends AbstractAsynchronousMessage {

	private InstructionStatuses status;
	private String message;

	/**
	 * Supplied by the client. Can be used to associate to a source table and id.  For example, IMS123456MTM could represent
	 * FkFieldId 123456 in the IMS database on the AccountingM2MDaily table.
	 */
	private String uniqueStringIdentifier;

	/**
	 * Unique name for the source system
	 */
	private String sourceSystemName;

	/**
	 * Unique name for the source system sub module
	 */
	private String sourceSystemModuleName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InstructionStatuses getStatus() {
		return this.status;
	}


	public void setStatus(InstructionStatuses status) {
		this.status = status;
	}


	public String getMessage() {
		return this.message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.uniqueStringIdentifier = uniqueStringIdentifier;
	}


	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public String getSourceSystemModuleName() {
		return this.sourceSystemModuleName;
	}


	public void setSourceSystemModuleName(String sourceSystemModuleName) {
		this.sourceSystemModuleName = sourceSystemModuleName;
	}
}
