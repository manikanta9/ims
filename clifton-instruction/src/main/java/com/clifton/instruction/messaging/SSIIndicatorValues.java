package com.clifton.instruction.messaging;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;


/**
 * <code>SSIIndicatorValues</code> field values for message MT 671 Standing Settlement Instruction Update Notification.
 */
public enum SSIIndicatorValues {

	SENDER_MSG_REFERENCE("EFFD", "Sender's Message Reference"),
	SSI_UPDATE_NOTIFICATION("MARK", "New SSI update notification message."),
	FUTURE("FUTU", "Future"),
	OUTSTANDING_TRADE("OUTS", "Outstanding Trade"),
	RE_CONFIRMATION_TYPE("RCON", "Reconfirmation"),
	SETTLEMENT_DATE("SETT", "Settlement Date"),
	TRADE_DATE("TRAD", "Trade Date"),
	ANY("ANYY", "Any"),
	CASH("CASH", "Cash"),
	COLLECTIONS("COLL", "Collections"),
	COMMODITIES("COMM", "Commodities"),
	COMMERCIAL_PAYMENTS("COPA", "Commercial Payments"),
	DERIVATIVES("DERI", "Derivatives"),
	DOCUMENTARY_CREDITS("DOCC", "Documentary Credits"),
	FOREIGN_EXCHANGE("FOEX", "Foreign Exchange"),
	GUARANTEES("GUAR", "Guarantees"),
	LETTERS_OF_CREDIT("LETT", "Letters of Credit"),
	LOANS("LOAN", "Loans"),
	MONEY_MARKETS("MMKT", "Money Markets"),
	NON_DELIVERABLE_FORWARD("NDLF", "Non Deliverable Forward"),
	OPTIONS("OPTI", "Options"),
	SECURITIES("SECU", "Securities"),
	TRADE_FINANCE("TFIN", "Trade Finance"),
	TREASURY("TREA", "Treasury"),
	PAYMENT_METHOD_INDICATOR("PH", "Payment Method Indicator"),
	COVER("COVE", "Cover");

	private final String code;
	private final String label;


	SSIIndicatorValues(String code, String label) {
		this.code = code;
		this.label = label;
	}


	public static SSIIndicatorValues fromCode(String code) {
		Optional<SSIIndicatorValues> value = Arrays
				.stream(SSIIndicatorValues.values())
				.filter(v -> Objects.equals(v.getCode(), code))
				.findFirst();
		return value.isPresent() ? value.get() : null;
	}


	public String getCode() {
		return this.code;
	}


	public String getLabel() {
		return this.label;
	}
}
