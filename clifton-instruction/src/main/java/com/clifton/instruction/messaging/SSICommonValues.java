package com.clifton.instruction.messaging;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;


/**
 * <code>SSICommonValues</code> field values for message MT 671 Standing Settlement Instruction Update Notification.
 */
public enum SSICommonValues {

	SENDERS_MSG_REFERENCE("SEME", "Sender's Message Reference"),
	NEW_SSI_UPDATE_NOTIFICATION("NEWM", "New SSI update notification message."),
	COPY_DUPLICATE("CODU", "Copy Duplicate"),
	COPY("COPY", "Copy"),
	DUPLICATE("DUPL", "Duplicate"),
	RELATED_MSG_REFERENCE("RELA", "Related Message Reference"),
	PREVIOUS_MSG_REFERENCE("PREV", "Previous Message Reference"),
	SSI_UPDATE_TYPE("UDTP", "SSI Update Type"),
	CASH("CASH", "Cash"),
	SAFEKEEPING_ACCOUNT("SAGE", "Safekeeping Account"),
	SSI_PURPOSE("SSIP", "SSI Purpose"),
	NEW("NEWS", "New"),
	RE_CONFIRMATION("RECO", "Re-confirmation"),
	SSI_REFERENCE("SSIR", "SSI Reference"),
	SSI_CURRENCY("SETT", "SSI Currency"),
	EFFECTIVE_DATE("EFFD", "Effective Date"),
	MARKET_AREA_INDICATOR("MARK", "Market Area Indicator"),
	CASH_PARTIES("CSHPRTY", "Cash Parties"),
	OTHER_DETAILS("OTHRDET", "Other Details"),
	ADDITIONAL_TEXT_NARRATIVE("ADTX", "Additional Text Narrative"),
	SSI_DETAILS("SSIDET", "Standing Settlement Instruction Details"),
	GENERAL_INFO("GENL", "General Information");

	private final String code;
	private final String label;


	SSICommonValues(String code, String label) {
		this.code = code;
		this.label = label;
	}


	public static SSICommonValues fromCode(String code) {
		Optional<SSICommonValues> value = Arrays
				.stream(SSICommonValues.values())
				.filter(v -> Objects.equals(v.getCode(), code))
				.findFirst();
		return value.isPresent() ? value.get() : null;
	}


	public String getCode() {
		return this.code;
	}


	public String getLabel() {
		return this.label;
	}
}
