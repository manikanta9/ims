package com.clifton.instruction.messaging;

import com.clifton.instruction.messaging.message.InstructionMessagingStatusMessage;


/**
 * Defines methods used to process incoming export status messages.
 *
 * @author mwacker
 */
public interface InstructionMessagingResponseHandler {

	public void process(InstructionMessagingStatusMessage message);
}
