package com.clifton.instruction.messaging;

/**
 * @author mwacker
 */
public enum InstructionStatuses {
	RECEIVED, PROCESSED, REPROCESSED, FAILED
}
