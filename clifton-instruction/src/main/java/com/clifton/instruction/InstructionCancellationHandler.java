package com.clifton.instruction;

import com.clifton.instruction.messages.InstructionMessage;

import java.util.List;


/**
 * <code>InstructionCancellationHandler</code> provides methods used to gather implementation-specific message information necessary for
 * generating cancellation messages.
 */
public interface InstructionCancellationHandler {

	/**
	 * Provided the list of Instruction trade messages, convert them to cancellation messages.
	 */
	public List<InstructionMessage> generateCancellationMessageList(Instruction instruction);
}
