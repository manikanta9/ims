package com.clifton.instruction.export;

import com.clifton.instruction.messages.InstructionMessage;

import java.util.List;


/**
 * Defines an object that will be used to handle send instruction messages.  There should only be
 * one implementation of this class in the context.  Currently, the instruction export handler is
 * implemented by the Swift project and used to send SWIFT messages.
 *
 * @author mwacker
 */
public interface InstructionExportHandler {

	public <T extends InstructionMessage> void send(List<T> entityList);


	/**
	 * Generates a preview of the final message that will be exported.
	 */
	public <T extends InstructionMessage> String previewExportMessage(List<T> entityList);


	/**
	 * Generate a formatted preview of the exported message
	 */
	public <T extends InstructionMessage> String previewFormattedExportMessage(List<T> entityList);
}
