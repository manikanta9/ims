package com.clifton.instruction;

import com.clifton.core.beans.IdentityObject;
import com.clifton.system.schema.SystemTable;


/**
 * Should be implemented by any object that can be used to return a table and
 * id of an object for which an InstructionEntity can be generated.
 *
 * @author mwacker
 */
public interface Instruction extends IdentityObject {

	public Integer getFkFieldId();


	public SystemTable getSystemTable();
}
