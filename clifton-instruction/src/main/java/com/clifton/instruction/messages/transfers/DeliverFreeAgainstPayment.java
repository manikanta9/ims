package com.clifton.instruction.messages.transfers;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;


/**
 * <code>DeliverFreeAgainstPayment</code> same as DeliverAgainstPayment except this will trigger MT540 rather than MT541.
 * The messages are the same except the settlement amount is omitted on MT540.
 */
public class DeliverFreeAgainstPayment extends AbstractFreeAgainstPayment {
	// marker for triggering the MT540 converter


	@Override
	@ValueIgnoringGetter
	public boolean isBuy() {
		return true;
	}
}
