package com.clifton.instruction.messages.beans;

/**
 * ISITC Codes - investment security identifier codes established by the International Securities Association for Institutional Trade Communication (ISITC) which is a standards
 * organization.
 */
public enum ISITCCodes {
	CS("Common Stock", ISITCClassifications.EQUITIES),
	ETF("Exchange Traded Fund", ISITCClassifications.EQUITIES),
	MF("Mutual Fund", ISITCClassifications.EQUITIES),
	PS("Preferred Stock", ISITCClassifications.EQUITIES),

	RTS("Rights", ISITCClassifications.ENTITLEMENTS),
	WAR("Warrant", ISITCClassifications.ENTITLEMENTS),

	FUT("Futures", ISITCClassifications.LISTED_DERIVATIVES),
	OPT("Option", ISITCClassifications.LISTED_DERIVATIVES),

	BA("Bankers Acceptance", ISITCClassifications.FIXED_INCOME),
	BKL("Bank Loans (syndicated)", ISITCClassifications.FIXED_INCOME),
	CBO("Collateralized Bond Obligation", ISITCClassifications.FIXED_INCOME),
	CD("Certificate of Deposit", ISITCClassifications.FIXED_INCOME),
	CDO("Collateralized Debt Obligation", ISITCClassifications.FIXED_INCOME),
	CLO("Collateralized Loan Obligation", ISITCClassifications.FIXED_INCOME),
	CMO("Collateralized Mort. Obl. (incl. sinking funds)", ISITCClassifications.FIXED_INCOME),
	CORP("Corporate Bond", ISITCClassifications.FIXED_INCOME),
	CP("Commercial Paper", ISITCClassifications.FIXED_INCOME),
	CPP("Commercial Private Placement", ISITCClassifications.FIXED_INCOME),
	DISC("Discount Note", ISITCClassifications.FIXED_INCOME),
	FAD("FRNYUS33eral Agency Discount ", ISITCClassifications.FIXED_INCOME),
	FHA("FRNYUS33eral Housing Authority", ISITCClassifications.FIXED_INCOME),
	FHL("FRNYUS33eral Home Loan", ISITCClassifications.FIXED_INCOME),
	FN("FRNYUS33eral National Mortgage Association", ISITCClassifications.FIXED_INCOME),
	FRN("Floating Rate Note", ISITCClassifications.FIXED_INCOME),
	GN("Government National Mortgage Association", ISITCClassifications.FIXED_INCOME),
	GOVT("Treasuries and Agency Debentures", ISITCClassifications.FIXED_INCOME),
	IET("Mortgage IO-ette", ISITCClassifications.FIXED_INCOME),
	MPP("Mortgage - Private Placement", ISITCClassifications.FIXED_INCOME),
	MUNI("Municipal Bond", ISITCClassifications.FIXED_INCOME),
	SL("Student Loan Marketing Association", ISITCClassifications.FIXED_INCOME),
	STF("Short Term Investment Fund", ISITCClassifications.FIXED_INCOME),
	STRP("Treasury Strips", ISITCClassifications.FIXED_INCOME),
	TIPS("Treasury inflation Protected Security", ISITCClassifications.FIXED_INCOME),
	TN("Treasury Note", ISITCClassifications.FIXED_INCOME),
	TRPS("Trust Preferred Stock", ISITCClassifications.FIXED_INCOME),
	UBWW("Unitized Bonds with Warrants", ISITCClassifications.FIXED_INCOME),
	UNBD("Unitized Bonds", ISITCClassifications.FIXED_INCOME),
	USTB("US Treasury Bill", ISITCClassifications.FIXED_INCOME),
	VRDN("Variable Rate Discount Note", ISITCClassifications.FIXED_INCOME),
	ZOO("Cats, Tigers & Lions", ISITCClassifications.FIXED_INCOME),

	BFW("Forwards - Bonds", ISITCClassifications.STRUCTURED_PRODUCTS),
	FRA("Forward Rate Agreement", ISITCClassifications.STRUCTURED_PRODUCTS),
	RP("Repurchase Agreement", ISITCClassifications.STRUCTURED_PRODUCTS),
	RVRP("Reverse Repurchase Agreement", ISITCClassifications.STRUCTURED_PRODUCTS),
	TRP("Triparty Repurchase Agreement", ISITCClassifications.STRUCTURED_PRODUCTS),
	RTRP("Triparty Reverse Repurchase Agreement", ISITCClassifications.STRUCTURED_PRODUCTS),

	FXF("Forwards - FX Forward", ISITCClassifications.REFERENTIAL_INSTRUMENTS),
	FXS("FX Spot", ISITCClassifications.REFERENTIAL_INSTRUMENTS),

	NONE("NONE", null);

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private String description;
	private ISITCClassifications classification;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	ISITCCodes(String description, ISITCClassifications classification) {
		this.description = description;
		this.classification = classification;
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public String getDescription() {
		return this.description;
	}


	public ISITCClassifications getClassification() {
		return this.classification;
	}
}
