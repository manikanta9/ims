package com.clifton.instruction.messages;

/**
 * <code>InstructionMessage</code> base interface for all instruction messages.
 */
public interface InstructionMessage {

	public String getTransactionReferenceNumber();


	public void setTransactionReferenceNumber(String transactionReferenceNumber);


	public String getSenderBIC();


	public void setSenderBIC(String senderBIC);


	public String getReceiverBIC();


	public void setReceiverBIC(String receiverBIC);


	public Long getMessageReferenceNumber();


	public void setMessageReferenceNumber(Long messageReferenceNumber);


	public InstructionMessage generateCancellationMessage();
}
