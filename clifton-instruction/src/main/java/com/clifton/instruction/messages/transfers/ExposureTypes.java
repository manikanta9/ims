package com.clifton.instruction.messages.transfers;

import java.util.Arrays;
import java.util.Objects;


/**
 * Exposure Type Indicator
 * Field22F in SETDET with qualifier COLA.
 *
 * @author terrys
 */
public enum ExposureTypes {

	FUTURES("FUTR", "Futures", "Related to futures trading activity."),
	OTC_DERIVATIVES("OTCD", "OTC Derivatives", "Over-the-counter (OTC) Derivatives in general for example contracts which are traded and privately negotiated"),
	OTC_INITIAL_MARGIN("CCPC", "CCP Collateral", "Collateral covering the initial margin requirements for OTC trades cleared through a CCP."),
	EQUITY_OPTION("EQPT", "Equity Option", "Trading of equity option (Also known as stock options)."),
	REPURCHASE_AGREEMENT("REPO", "Repurchase Agreement", "In support of a repurchase agreement transaction."),
	REVERSE_REPURCHASE_AGREEMENT("RVPO", "Reverse Repurchase Agreement", "In support of a reverse repurchase agreement transaction.");

	final private String code;
	final private String name;
	final private String description;


	ExposureTypes(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}


	public static ExposureTypes findByCode(String code) {
		return Arrays.stream(ExposureTypes.values())
				.filter(e -> Objects.equals(code, e.getCode()))
				.findFirst()
				.orElse(null);
	}


	public String getCode() {
		return this.code;
	}


	public String getName() {
		return this.name;
	}


	public String getDescription() {
		return this.description;
	}
}
