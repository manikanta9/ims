package com.clifton.instruction.messages.transfers;

/**
 * Defines a transfer used to order the movement of funds to the beneficiary institution.
 * <p>
 * Corresponds to a SWIFT MT202 message "General Financial Institution Transfer".
 *
 * @author mwacker
 */
public class GeneralFinancialInstitutionTransferMessage extends AbstractTransferMessage {

	private String custodyAccountNumber;
	private String custodyBIC;

	private String intermediaryBIC;
	private String intermediaryAccountNumber;

	private String beneficiaryBIC;
	private String beneficiaryAccountNumber;
	private String beneficiaryAccountName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCustodyAccountNumber() {
		return this.custodyAccountNumber;
	}


	public void setCustodyAccountNumber(String custodyAccountNumber) {
		this.custodyAccountNumber = custodyAccountNumber;
	}


	public String getCustodyBIC() {
		return this.custodyBIC;
	}


	public void setCustodyBIC(String custodyBIC) {
		this.custodyBIC = custodyBIC;
	}


	public String getBeneficiaryBIC() {
		return this.beneficiaryBIC;
	}


	public void setBeneficiaryBIC(String beneficiaryBIC) {
		this.beneficiaryBIC = beneficiaryBIC;
	}


	public String getBeneficiaryAccountNumber() {
		return this.beneficiaryAccountNumber;
	}


	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}


	public String getBeneficiaryAccountName() {
		return this.beneficiaryAccountName;
	}


	public void setBeneficiaryAccountName(String beneficiaryAccountName) {
		this.beneficiaryAccountName = beneficiaryAccountName;
	}


	public String getIntermediaryBIC() {
		return this.intermediaryBIC;
	}


	public void setIntermediaryBIC(String intermediaryBIC) {
		this.intermediaryBIC = intermediaryBIC;
	}


	public String getIntermediaryAccountNumber() {
		return this.intermediaryAccountNumber;
	}


	public void setIntermediaryAccountNumber(String intermediaryAccountNumber) {
		this.intermediaryAccountNumber = intermediaryAccountNumber;
	}
}
