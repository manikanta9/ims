package com.clifton.instruction.messages.transfers;

import java.util.Arrays;
import java.util.Objects;


/**
 * Type of Settlement Transaction Indicator.
 * Field22F in SETDET with qualifier SETR.
 *
 * @author terrys
 */
public enum SettlementTransactionTypes {

	COLLATERAL_IN("COLI", "Collateral In", "Relates to a collateral transaction, from the point of view of the collateral taker or its agent (broker)."),
	COLLATERAL_OUT("COLO", "Collateral Out", "Relates to a collateral transaction, from the point of view of the collateral giver or its agent (client)."),
	REPURCHASE_AGREEMENT("REPU", "Repo", "Relates to a repurchase agreement transaction."),
	REVERSE_REPURCHASE_AGREEMENT("RVPO", "Reverse Repo", "Relates to a reverse repurchase agreement transaction."),
	TRADE("TRAD", "Trade", "Relates to the settlement of a trade.");

	final private String code;
	final private String name;
	final private String description;


	SettlementTransactionTypes(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}


	public static SettlementTransactionTypes findByCode(String code) {
		return Arrays.stream(SettlementTransactionTypes.values())
				.filter(e -> Objects.equals(code, e.getCode()))
				.findFirst()
				.orElse(null);
	}


	public String getCode() {
		return this.code;
	}


	public String getName() {
		return this.name;
	}


	public String getDescription() {
		return this.description;
	}
}
