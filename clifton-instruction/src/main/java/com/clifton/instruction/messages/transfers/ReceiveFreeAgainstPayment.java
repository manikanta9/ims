package com.clifton.instruction.messages.transfers;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;


/**
 * <code>ReceiveFreeAgainstPayment</code> same as ReceiveAgainstPayment except this will trigger MT542 rather than MT543.
 * The messages are the same except the settlement amount is omitted on MT542.
 */
public class ReceiveFreeAgainstPayment extends AbstractFreeAgainstPayment {
	// marker for triggering the MT542 converter


	@Override
	@ValueIgnoringGetter
	public boolean isBuy() {
		return false;
	}
}
