package com.clifton.instruction.messages.trade;

import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.InstructionSecurityMessage;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.beans.RepoRateTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.instruction.messages.transfers.ExposureTypes;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Holds the common message fields for DeliverAgainstPayment and ReceiveAgainstPayment.
 *
 * @author mwacker
 */
public abstract class AbstractTradeMessage extends InstructionSecurityMessage {

	private MessageFunctions messageFunctions;

	/**
	 * Primarily applies to Position GL Accounts and indicates whether this transaction opens a new position or closes an existing position.
	 * <p>
	 * Cannot be a primitive, the presence of the open Boolean value triggers the appearance of a field in the resulting message.
	 */
	private Boolean open;

	/**
	 * Trade is buy or sell.
	 */
	private boolean buy;

	private String custodyBIC;
	private String custodyAccountNumber;

	/**
	 * Clearing agent BIC or DTC Number.
	 */
	private PartyIdentifier clearingBrokerIdentifier;

	/**
	 * Executing Agent BIC or DTC Number.
	 */
	private PartyIdentifier executingBrokerIdentifier;

	private Date tradeDate;
	private Date settlementDate;

	private TradeMessagePriceTypes priceType;
	private BigDecimal price;
	private BigDecimal quantity;
	private BigDecimal accountingNotional;
	private BigDecimal commissionAmount;

	/**
	 * Some securities may adjust accounting notional by this multiplier that is tied to something.
	 * TIPS use it to adjust for changes in inflation: Index Ratio.
	 */
	private BigDecimal currentFactor;

	/**
	 * Accrued interest addresses the problem regarding the ownership of the next coupon if the bond is sold in the period between coupons: Only the current owner can receive
	 * the coupon payment, but the investor who sold the bond must be compensated for the period of time for which he or she owned the bond. In other words, the previous
	 * owner must be paid the interest that accrued before the sale.
	 * Accrued Interest = Current Face * Coupon Rate * Accrual Days / Days in Year
	 */
	private BigDecimal accruedInterest;

	/**
	 * The institution in whose books the transaction will occur (for example, Clearstream, CBF, Euroclear). The Place of Settlement determines the Market Rules for the transaction.
	 */
	private String placeOfSettlement;

	/**
	 * The SEC does not impose or set any of the fees that investors must pay to their brokers. Instead, under Section 31 of the Securities Exchange Act of 1934,
	 * self-regulatory organizations (SROs) -- such as the Financial Industry Regulatory Authority (FINRA) and all of the national securities exchanges -- must pay transaction
	 * fees to the SEC based on the volume of securities that are sold on their markets. These fees are designed to recover the costs incurred by the government, including the SEC,
	 * for supervising and regulating the securities markets and securities professionals.
	 */
	private BigDecimal regulatoryAmount;

	/**
	 * A brokerage fee is a fee charged by an agent or agent’s company to conduct transactions between buyers and sellers. The broker charges the brokerage fee for services
	 * such as purchases, sales, and advice on the transaction, negotiations or delivery.
	 */
	private BigDecimal brokerCommissionAmount;

	/**
	 * If you purchase the bond for $1,150, the extra $150 is called the bond premium, Generally accepted accounting practices requires that you amortize that premium over the
	 * life of the bond to maturity to normalize the interest.  At the maturity date, all of the premium will be amortized and the recorded bond value will be $1,000, which is
	 * what you will receive when you cash the bond.  The easiest method of amortizing bonds is straight-line, meaning dividing the discount or premium by the number of years
	 * left to maturity and taking that amount onto the income statement every year. For example, if our bond premium is $150 and we have 11 years left to maturity, the
	 * straight-line method will result in an interest expense on the income statement of $13.64 per year.
	 */
	private BigDecimal amortizedFaceAmount;

	/**
	 * Face value is the nominal value or dollar value of a security stated by the issuer. For stocks, it is the original cost of the stock shown on the certificate. For bonds,
	 * it is the amount paid to the holder at maturity, generally $1,000. It is also known as "par value" or simply "par."
	 */
	private BigDecimal originalFaceAmount;

	/**
	 * Trade amount, total amount paid for shares.
	 * Notional = ROUND(Price * Quantity * Price Multiplier, 2)
	 */
	private BigDecimal dealAmount;

	/**
	 * Notional Amount + Accrued Interest.
	 * ROUND(Price * Quantity * Price Multiplier, 2) + Accrued Interest
	 */
	private BigDecimal netSettlementAmount;

	/**
	 * The account for which securities are to be withdrawn or to which securities are to be deposited
	 */
	private String safekeepingAccount;


	/**
	 * The clearing account for which securities are to be withdrawn or to which securities are to be deposited
	 */
	private String clearingSafekeepingAccount;

	/**
	 * Is this a REPO
	 */
	private boolean repurchaseAgreement = false;


	/**
	 * The net cash amount for a REPO
	 */
	private BigDecimal repoNetCashAmount;


	/**
	 * The REPO closing date.
	 */
	private Date repoClosingDate;


	/**
	 * The REPO accrued interest amount
	 */
	private BigDecimal repoAccruedInterestAmount;


	/**
	 * The REPO interest rate
	 */
	private BigDecimal repoInterestRate;

	/**
	 * The REPO rate type.
	 */
	private RepoRateTypes repoRateType;


	/**
	 * The REPO Securities Haircut percent
	 */
	private BigDecimal repoHaircutPercentage;


	/**
	 * Type of Settlement Transaction Indicator.
	 * 22F qualifier SETR.
	 */
	private SettlementTransactionTypes settlementTransactionType;


	/**
	 * Exposure Type Indicator
	 * 22F qualifier COLA.
	 */
	private ExposureTypes exposureType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InstructionMessage generateCancellationMessage() {
		this.setMessageFunctions(MessageFunctions.CANCEL);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MessageFunctions getMessageFunctions() {
		return this.messageFunctions;
	}


	public void setMessageFunctions(MessageFunctions messageFunctions) {
		this.messageFunctions = messageFunctions;
	}


	public Boolean getOpen() {
		return this.open;
	}


	public void setOpen(Boolean open) {
		this.open = open;
	}


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public TradeMessagePriceTypes getPriceType() {
		return this.priceType;
	}


	public void setPriceType(TradeMessagePriceTypes priceType) {
		this.priceType = priceType;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public String getCustodyBIC() {
		return this.custodyBIC;
	}


	public void setCustodyBIC(String custodyBIC) {
		this.custodyBIC = custodyBIC;
	}


	public String getCustodyAccountNumber() {
		return this.custodyAccountNumber;
	}


	public void setCustodyAccountNumber(String custodyAccountNumber) {
		this.custodyAccountNumber = custodyAccountNumber;
	}


	public PartyIdentifier getClearingBrokerIdentifier() {
		return this.clearingBrokerIdentifier;
	}


	public void setClearingBrokerIdentifier(PartyIdentifier clearingBrokerIdentifier) {
		this.clearingBrokerIdentifier = clearingBrokerIdentifier;
	}


	public PartyIdentifier getExecutingBrokerIdentifier() {
		return this.executingBrokerIdentifier;
	}


	public void setExecutingBrokerIdentifier(PartyIdentifier executingBrokerIdentifier) {
		this.executingBrokerIdentifier = executingBrokerIdentifier;
	}


	public BigDecimal getAccountingNotional() {
		return this.accountingNotional;
	}


	public void setAccountingNotional(BigDecimal accountingNotional) {
		this.accountingNotional = accountingNotional;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public BigDecimal getCurrentFactor() {
		return this.currentFactor;
	}


	public void setCurrentFactor(BigDecimal currentFactor) {
		this.currentFactor = currentFactor;
	}


	public BigDecimal getAccruedInterest() {
		return this.accruedInterest;
	}


	public void setAccruedInterest(BigDecimal accruedInterest) {
		this.accruedInterest = accruedInterest;
	}


	public String getPlaceOfSettlement() {
		return this.placeOfSettlement;
	}


	public void setPlaceOfSettlement(String placeOfSettlement) {
		this.placeOfSettlement = placeOfSettlement;
	}


	public BigDecimal getRegulatoryAmount() {
		return this.regulatoryAmount;
	}


	public void setRegulatoryAmount(BigDecimal regulatoryAmount) {
		this.regulatoryAmount = regulatoryAmount;
	}


	public BigDecimal getBrokerCommissionAmount() {
		return this.brokerCommissionAmount;
	}


	public void setBrokerCommissionAmount(BigDecimal brokerCommissionAmount) {
		this.brokerCommissionAmount = brokerCommissionAmount;
	}


	public BigDecimal getAmortizedFaceAmount() {
		return this.amortizedFaceAmount;
	}


	public void setAmortizedFaceAmount(BigDecimal amortizedFaceAmount) {
		this.amortizedFaceAmount = amortizedFaceAmount;
	}


	public BigDecimal getOriginalFaceAmount() {
		return this.originalFaceAmount;
	}


	public void setOriginalFaceAmount(BigDecimal originalFaceAmount) {
		this.originalFaceAmount = originalFaceAmount;
	}


	public BigDecimal getDealAmount() {
		return this.dealAmount;
	}


	public void setDealAmount(BigDecimal dealAmount) {
		this.dealAmount = dealAmount;
	}


	public BigDecimal getNetSettlementAmount() {
		return this.netSettlementAmount;
	}


	public void setNetSettlementAmount(BigDecimal netSettlementAmount) {
		this.netSettlementAmount = netSettlementAmount;
	}


	public String getSafekeepingAccount() {
		return this.safekeepingAccount;
	}


	public void setSafekeepingAccount(String safekeepingAccount) {
		this.safekeepingAccount = safekeepingAccount;
	}


	public String getClearingSafekeepingAccount() {
		return this.clearingSafekeepingAccount;
	}


	public void setClearingSafekeepingAccount(String clearingSafekeepingAccount) {
		this.clearingSafekeepingAccount = clearingSafekeepingAccount;
	}


	public boolean isRepurchaseAgreement() {
		return this.repurchaseAgreement;
	}


	public void setRepurchaseAgreement(boolean repurchaseAgreement) {
		this.repurchaseAgreement = repurchaseAgreement;
	}


	public BigDecimal getRepoNetCashAmount() {
		return this.repoNetCashAmount;
	}


	public void setRepoNetCashAmount(BigDecimal repoNetCashAmount) {
		this.repoNetCashAmount = repoNetCashAmount;
	}


	public Date getRepoClosingDate() {
		return this.repoClosingDate;
	}


	public void setRepoClosingDate(Date repoClosingDate) {
		this.repoClosingDate = repoClosingDate;
	}


	public BigDecimal getRepoAccruedInterestAmount() {
		return this.repoAccruedInterestAmount;
	}


	public void setRepoAccruedInterestAmount(BigDecimal repoAccruedInterestAmount) {
		this.repoAccruedInterestAmount = repoAccruedInterestAmount;
	}


	public BigDecimal getRepoInterestRate() {
		return this.repoInterestRate;
	}


	public void setRepoInterestRate(BigDecimal repoInterestRate) {
		this.repoInterestRate = repoInterestRate;
	}


	public RepoRateTypes getRepoRateType() {
		return this.repoRateType;
	}


	public void setRepoRateType(RepoRateTypes repoRateType) {
		this.repoRateType = repoRateType;
	}


	public BigDecimal getRepoHaircutPercentage() {
		return this.repoHaircutPercentage;
	}


	public void setRepoHaircutPercentage(BigDecimal repoHaircutPercentage) {
		this.repoHaircutPercentage = repoHaircutPercentage;
	}


	public SettlementTransactionTypes getSettlementTransactionType() {
		return this.settlementTransactionType;
	}


	public void setSettlementTransactionType(SettlementTransactionTypes settlementTransactionType) {
		this.settlementTransactionType = settlementTransactionType;
	}


	public ExposureTypes getExposureType() {
		return this.exposureType;
	}


	public void setExposureType(ExposureTypes exposureType) {
		this.exposureType = exposureType;
	}
}
