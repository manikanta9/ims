package com.clifton.instruction.messages;

import java.io.Serializable;


/**
 * <code>InstructionPreviewMessage</code> data transfer object only, used by the instruction message preview window.
 * Has fields for raw message text and human readable formatted message text.
 */
public class InstructionPreviewMessage implements Serializable {

	private String message;
	private String formattedMessage;


	public InstructionPreviewMessage(String message, String formattedMessage) {
		this.message = message;
		this.formattedMessage = formattedMessage;
	}


	public String getMessage() {
		return this.message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getFormattedMessage() {
		return this.formattedMessage;
	}


	public void setFormattedMessage(String formattedMessage) {
		this.formattedMessage = formattedMessage;
	}
}
