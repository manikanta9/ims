package com.clifton.instruction.messages;

/**
 * Defines a base instruction entity the will be implemented by different instruction types
 * which are generated in different place in the system.  For example, InstructionEntityCash
 * implements this class and will be generate from the collateral and account M2M projects.
 *
 * @author mwacker
 */
public class InstructionMessageBase implements InstructionMessage {

	private String transactionReferenceNumber;

	private String senderBIC;
	private String receiverBIC;

	/**
	 * For SWIFT cancel messages, this would reference the Swift Identifier table's primary key for the message being cancelled.
	 * The Swift Identifier table's primary key is used by SWIFT as the message's unique identifier.
	 */
	private Long messageReferenceNumber;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InstructionMessage generateCancellationMessage() {
		throw new UnsupportedOperationException("Message class does not support cancellations." + this.getClass().getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTransactionReferenceNumber() {
		return this.transactionReferenceNumber;
	}


	@Override
	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		this.transactionReferenceNumber = transactionReferenceNumber;
	}


	@Override
	public String getSenderBIC() {
		return this.senderBIC;
	}


	@Override
	public void setSenderBIC(String senderBIC) {
		this.senderBIC = senderBIC;
	}


	@Override
	public String getReceiverBIC() {
		return this.receiverBIC;
	}


	@Override
	public void setReceiverBIC(String receiverBIC) {
		this.receiverBIC = receiverBIC;
	}


	@Override
	public Long getMessageReferenceNumber() {
		return this.messageReferenceNumber;
	}


	@Override
	public void setMessageReferenceNumber(Long messageReferenceNumber) {
		this.messageReferenceNumber = messageReferenceNumber;
	}
}
