package com.clifton.instruction.messages.beans;

import java.util.Arrays;
import java.util.Objects;


public enum RepoRateTypes {

	/**
	 * Rate is fixed
	 */
	FIXED("FIXE"),

	/**
	 * Rate is variable.
	 */
	VARIABLE("VARI");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String code;


	RepoRateTypes(String code) {
		this.code = code;
	}


	/**
	 * Get the enum value (or null) corresponding to the provided String code.
	 */
	public static RepoRateTypes findByCode(String code) {
		return Arrays.stream(RepoRateTypes.values())
				.filter(e -> Objects.equals(code, e.getCode()))
				.findFirst()
				.orElse(null);
	}


	public String getCode() {
		return this.code;
	}
}
