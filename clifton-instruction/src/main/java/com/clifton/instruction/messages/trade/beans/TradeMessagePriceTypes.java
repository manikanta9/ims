package com.clifton.instruction.messages.trade.beans;

import java.util.Arrays;
import java.util.Objects;


/**
 * @author mwacker
 */
public enum TradeMessagePriceTypes {
	/**
	 * Discount	Price expressed as the number of percentage points below par, for example, a discount price of 2.0% equals a price of 98 when par is 100.
	 */
	DISCOUNT("DISC"),
	/**
	 * Percentage Price expressed as a percentage of par.
	 */
	PERCENTAGE("PRCT"),
	/**
	 * Premium	Price expressed as the number of percentage points above par, for example, a premium price of 2.0% equals a price of 102 when par is 100.
	 */
	PREMIUM("PREM"),
	/**
	 * Yield	Price expressed as a yield.
	 */
	YIELD("YIEL"),
	/**
	 * Actual Amount	Price expressed as an amount of currency per unit or per share.
	 */
	ACTUAL("ACTU");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String code;


	TradeMessagePriceTypes(String code) {
		this.code = code;
	}


	/**
	 * Get the enum value (or null) corresponding to the provided String code.
	 */
	public static TradeMessagePriceTypes findByCode(String code) {
		return Arrays.stream(TradeMessagePriceTypes.values())
				.filter(e -> Objects.equals(code, e.getCode()))
				.findFirst()
				.orElse(null);
	}


	public String getCode() {
		return this.code;
	}
}
