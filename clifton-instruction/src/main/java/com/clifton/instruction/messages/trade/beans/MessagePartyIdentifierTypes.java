package com.clifton.instruction.messages.trade.beans;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;


/**
 * <code>MessagePartyIdentifierTypes</code> the format for the party identifiers.
 * The clearing and executing brokers can be specified in either BIC or DTC format depending on the security.
 * Fixed Income Securities require the clearing Broker to be specified using the USFW; 9 digit participant ID (ABA)
 */
public enum MessagePartyIdentifierTypes {
	DTC("DTCYID"),
	BIC(null),
	USFW("USFW");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String code;


	MessagePartyIdentifierTypes(String code) {
		this.code = code;
	}


	/**
	 * Get the enum value (or null) corresponding to the provided String code.
	 */
	public static MessagePartyIdentifierTypes findByCode(String code) {
		String matchValue = Optional.ofNullable(code)
				.map(String::trim)
				.filter(s -> !s.isEmpty())
				.orElse(null);
		return Arrays.stream(MessagePartyIdentifierTypes.values())
				.filter(e -> Objects.equals(matchValue, e.getCode()))
				.findFirst()
				.orElse(null);
	}


	public String getCode() {
		return this.code;
	}
}
