package com.clifton.instruction.messages.trade;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;


/**
 * This message is sent by an account owner to an account servicer (account servicing institution). The account owner may be a global custodian which has an account with its local agent (sub custodian) or an investment management institution or a broker/dealer which has an account with their custodian.
 * <p>
 * This message is used to:
 * <ul>
 * <li>instruct the delivery of financial instruments against payment, physically or by book-entry, to a specified party (the function of the message is NEWM)</li>
 * <li>request the cancellation of a deliver against payment instruction previously sent by the account owner (the function of the message is CANC)</li>
 * <li>pre-advise the account servicer of a forthcoming deliver against payment instruction (the function of the message is PREA).</li>
 * </ul>
 * <p>
 * The instruction may be linked to other settlement instructions, for example, for a turnaround or back-to-back, or other transactions, for example, foreign exchange deal, using the linkages sequence.
 *
 * @author mwacker
 */
public class ReceiveAgainstPayment extends AbstractTradeMessage {

	@Override
	@ValueIgnoringGetter
	public boolean isBuy() {
		return false;
	}
}
