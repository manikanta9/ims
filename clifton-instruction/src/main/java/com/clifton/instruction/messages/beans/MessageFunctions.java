package com.clifton.instruction.messages.beans;

import java.util.Arrays;
import java.util.Objects;


/**
 * @author mwacker
 */
public enum MessageFunctions {
	/**
	 * Message requesting the cancellation of a previously sent message.
	 */
	CANCEL("CANC"),

	/**
	 * New message.
	 */
	NEW_MESSAGE("NEWM"),

	/**
	 * Message preadvising a settlement instruction. It can be used for matching purposes, but is, without further notice from the account owner, not binding for execution.
	 */
	PRE_ADVICE("PREA");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String code;


	MessageFunctions(String code) {
		this.code = code;
	}


	/**
	 * Get the enum value (or null) corresponding to the provided String code.
	 */
	public static MessageFunctions findByCode(String code) {
		return Arrays.stream(MessageFunctions.values())
				.filter(e -> Objects.equals(code, e.getCode()))
				.findFirst()
				.orElse(null);
	}


	public String getCode() {
		return this.code;
	}
}
