package com.clifton.instruction.messages.transfers;

/**
 * An advance notice to the account servicing institution that it will receive funds to be credited to the Sender's account.
 * <p>
 * Corresponds to a SWIFT MT210 message "Notice to Receive".
 *
 * @author mwacker
 */
public class NoticeToReceiveTransferMessage extends AbstractTransferMessage {

	/**
	 * This field identifies the account at the receiver to be credited with the incoming funds.
	 */
	private String receiverAccountNumber;

	/**
	 * The BIC of the ordering institution, for Futures this would be the FCM.
	 */
	private String orderingBIC;

	/**
	 * This field specifies the financial institution from which the Receiver is to receive the funds.
	 */
	private String intermediaryBIC;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getReceiverAccountNumber() {
		return this.receiverAccountNumber;
	}


	public void setReceiverAccountNumber(String receiverAccountNumber) {
		this.receiverAccountNumber = receiverAccountNumber;
	}


	public String getIntermediaryBIC() {
		return this.intermediaryBIC;
	}


	public void setIntermediaryBIC(String intermediaryBIC) {
		this.intermediaryBIC = intermediaryBIC;
	}


	public String getOrderingBIC() {
		return this.orderingBIC;
	}


	public void setOrderingBIC(String orderingBIC) {
		this.orderingBIC = orderingBIC;
	}
}
