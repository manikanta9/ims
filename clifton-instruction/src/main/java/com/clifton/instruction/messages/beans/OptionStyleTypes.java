package com.clifton.instruction.messages.beans;

import java.util.Arrays;
import java.util.Objects;


/**
 * <code>OptionStyleTypes</code>
 */
public enum OptionStyleTypes {
	AMERICAN("AMER", "American"),
	EUROPEAN("EURO", "European"),
	ASIAN("ASIA", "Asian"),
	BERMUDAN("BERM", "Bermudan"),
	CANADA("CANA", "Canadian"),
	OTHER("OTHR", "Other");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String code;
	private String name;


	OptionStyleTypes(String code, String name) {
		this.code = code;
		this.name = name;
	}


	/**
	 * Get the enum value (or null) corresponding to the provided String code.
	 */
	public static OptionStyleTypes findByCode(String code) {
		return Arrays.stream(OptionStyleTypes.values())
				.filter(e -> Objects.equals(code, e.getCode()))
				.findFirst()
				.orElse(null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCode() {
		return this.code;
	}


	public String getName() {
		return this.name;
	}
}
