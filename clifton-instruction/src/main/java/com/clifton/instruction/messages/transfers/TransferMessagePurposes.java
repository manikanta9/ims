package com.clifton.instruction.messages.transfers;

import java.util.Arrays;
import java.util.Objects;


/**
 * @author mwacker
 */
public enum TransferMessagePurposes {

	/**
	 * Initial futures margin. Where such payment is owned by the client and is available for use by them on return.
	 */
	INITIAL_MARGIN("MGCC"),

	/**
	 * Collateral associated with an ISDA or Central Clearing Agreement that is covering the initial margin requirements for OTC trades clearing through a CCP.
	 */
	INITIAL_MARGIN_OTC("CCPC"),

	/**
	 * Daily margin on listed derivatives – not segregated as collateral associated with an FCM agreement. Examples
	 * include listed futures and options margin payments; premiums for listed options not covered in the MT54X message.
	 */
	DAILY_MARGIN("MARG"),

	/**
	 * Margin variation on your OTC derivatives clearing through a CCP.
	 * <p>
	 * If the Initial Margin and Variation Margins are netted then the CCPM code word shall still be utilized. The Variation
	 * Margin amounts shall be detailed on the Variation Margin Flow report. Therefore the custodians will know the amount
	 * of variation margin that was part of the netted variation margin amount associated with an ISDA or Central Clearing
	 * Agreement.
	 */
	DAILY_MARGIN_OTC("CCPM"),

	/**
	 * Cash collateral payment for swaps associated with an ISDA agreement where such payment is not segregated and
	 * is available for use by the client upon return.  Includes any cash collateral payments made under the terms of a CSA
	 * agreement for instruments such as swaps and FX forwards.
	 */
	COLLATERAL_SWAP_PAYMENT("SWCC"),

	/**
	 * Cash collateral payment for swaps associated with an ISDA agreement where such payment is segregated and
	 * not available for use by the client.  Includes any cash collateral payments made under the terms of a CSA agreement
	 * for instruments such as swaps and FX forwards.
	 */
	COLLATERAL_SWAP_PAYMENT_ISDA("SWBC"),

	/**
	 * Bi-lateral repo broker owned collateral associated with a repo master agreement – GMRA or MRA Master Repo Agreements
	 */
	COLLATERAL_REPO_BROKER("RPBC"),

	/**
	 * Repo client owned collateral associated with a repo master agreement – GMRA or MRA Master Repo Agreements
	 */
	COLLATERAL_REPO_CLIENT("RPCC"),

	/**
	 * Cash collateral payment for OTC options associated with an FCM agreement. Where such payment is not segregated and is available for use by the client upon return.
	 */
	COLLATERAL_LISTED_OPTIONS("OPCC");

	private final String code;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	TransferMessagePurposes(String code) {
		this.code = code;
	}


	/**
	 * Get the enum value (or null) corresponding to the provided String code.
	 */
	public static TransferMessagePurposes findByCode(String code) {
		return Arrays.stream(TransferMessagePurposes.values())
				.filter(e -> Objects.equals(code, e.getCode()))
				.findFirst()
				.orElse(null);
	}


	public String getCode() {
		return this.code;
	}
}
