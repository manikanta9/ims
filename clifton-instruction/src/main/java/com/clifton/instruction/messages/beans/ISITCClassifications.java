package com.clifton.instruction.messages.beans;

/**
 * <code>ISITCClassifications</code> enumeration for the investment instrument top-level categorization.
 */
public enum ISITCClassifications {

	EQUITIES("Equities"),
	ENTITLEMENTS("Entitlements"),
	LISTED_DERIVATIVES("Listed Derivatives"),
	FIXED_INCOME("Fixed Income and Debt"),
	STRUCTURED_PRODUCTS("Structured Products"),
	REFERENTIAL_INSTRUMENTS("Referential Instruments");

	private String description;


	ISITCClassifications(String description) {
		this.description = description;
	}


	public String getDescription() {
		return this.description;
	}
}
