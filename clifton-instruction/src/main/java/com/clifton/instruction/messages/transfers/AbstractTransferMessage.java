package com.clifton.instruction.messages.transfers;

import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.InstructionMessageBase;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Represents any transfer instruction message.
 * <p>
 * Corresponds to a SWIFT MT2XX message.
 *
 * @author mwacker
 */
public class AbstractTransferMessage extends InstructionMessageBase {

	private TransferMessagePurposes messagePurpose;


	private String currency;
	private Date valueDate;
	private BigDecimal amount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InstructionMessage generateCancellationMessage() {
		return new TransferCancellationMessage(this);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public Date getValueDate() {
		return this.valueDate;
	}


	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public TransferMessagePurposes getMessagePurpose() {
		return this.messagePurpose;
	}


	public void setMessagePurpose(TransferMessagePurposes messagePurpose) {
		this.messagePurpose = messagePurpose;
	}
}
