package com.clifton.instruction.messages.transfers;

import com.clifton.instruction.messages.trade.AbstractTradeMessage;

import java.math.BigDecimal;


public class AbstractFreeAgainstPayment extends AbstractTradeMessage {

	/**
	 * Market Value: Market Value of a position is the amount of money that one would pay to buy that position or get to sell it.
	 * We use the following formula to calculate market value which works in all cases (futures, stocks, bonds, etc.).
	 * Market Value = Notional - Cost Basis + Cost + Accrual.
	 */
	private BigDecimal marketValue;


	public BigDecimal getMarketValue() {
		return this.marketValue;
	}


	public void setMarketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
	}
}
