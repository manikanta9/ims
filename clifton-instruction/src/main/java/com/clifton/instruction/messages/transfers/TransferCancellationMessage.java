package com.clifton.instruction.messages.transfers;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.instruction.messages.InstructionMessage;


/**
 * <code>TransferCancellationMessage</code> exists as a selector for the correct SwiftMessageConverter implementation to cancel financial transfer messages.
 * <p>
 * Fields needed for cancellation purposes only can be placed here.
 */
public class TransferCancellationMessage implements InstructionMessage {

	private AbstractTransferMessage originalMessage;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TransferCancellationMessage(AbstractTransferMessage originalMessage) {
		this.originalMessage = originalMessage;
	}


	@Override
	public InstructionMessage generateCancellationMessage() {
		throw new UnsupportedOperationException("Message class does not support cancellations." + this.getClass().getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractTransferMessage getOriginalMessage() {
		return this.originalMessage;
	}


	public void setOriginalMessage(AbstractTransferMessage originalMessage) {
		this.originalMessage = originalMessage;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTransactionReferenceNumber() {
		return getOriginalMessage().getTransactionReferenceNumber();
	}


	@Override
	public void setTransactionReferenceNumber(String transactionReferenceNumber) {
		getOriginalMessage().setTransactionReferenceNumber(transactionReferenceNumber);
	}


	@Override
	public String getSenderBIC() {
		return getOriginalMessage().getSenderBIC();
	}


	@Override
	public void setSenderBIC(String senderBIC) {
		getOriginalMessage().setSenderBIC(senderBIC);
	}


	@Override
	public String getReceiverBIC() {
		return getOriginalMessage().getReceiverBIC();
	}


	@Override
	public void setReceiverBIC(String receiverBIC) {
		getOriginalMessage().setReceiverBIC(receiverBIC);
	}


	@Override
	public Long getMessageReferenceNumber() {
		return getOriginalMessage().getMessageReferenceNumber();
	}


	@Override
	@ValueChangingSetter
	public void setMessageReferenceNumber(Long messageReferenceNumber) {
		getOriginalMessage().setMessageReferenceNumber(messageReferenceNumber);
	}
}
