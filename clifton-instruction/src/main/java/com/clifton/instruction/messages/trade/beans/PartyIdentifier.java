package com.clifton.instruction.messages.trade.beans;

import java.util.Objects;


/**
 * <code>PartyIdentifier</code> associates a string identifier with its format type.
 * For instance, CNORUS44 is the BIC formatted identifier for Northern Trust.
 * <p>
 * The type is used by the SWIFT message converter to determine which field type to output.
 * A BIC formatted identifier is outputted using Field 95P and a DTC formatted identifier
 * is outputted using Field 95Q on MT-541 and MT-543 messages.
 */
public class PartyIdentifier {

	private final String identifier;
	private final MessagePartyIdentifierTypes type;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PartyIdentifier(String identifier, MessagePartyIdentifierTypes type) {
		this.identifier = identifier;
		this.type = type;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PartyIdentifier that = (PartyIdentifier) o;
		return Objects.equals(this.identifier, that.identifier) &&
				this.type == that.type;
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.identifier, this.type);
	}


	@Override
	public String toString() {
		return "PartyIdentifier{" +
				"identifier='" + this.identifier + '\'' +
				", type=" + this.type +
				'}';
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getIdentifier() {
		return this.identifier;
	}


	public MessagePartyIdentifierTypes getType() {
		return this.type;
	}
}
