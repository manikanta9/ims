package com.clifton.instruction.messages;

import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.OptionStyleTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * All security fields for an InstructionMessageBase.
 *
 * @author mwacker
 */
public class InstructionSecurityMessage extends InstructionMessageBase {

	private String securitySymbol;
	private String securityDescription;
	private String securityCurrency;
	private String securityExchangeCode;
	/**
	 * International Securities Association for Institutional Trade Communication (ISITC) defined codes for financial instruments.
	 */
	private ISITCCodes isitcCode;
	/**
	 * The International Securities Identification Number (ISIN) is a code that uniquely identifies a specific securities issue. The organization that allocates ISINs in any
	 * particular country is the country's respective National Numbering Agency (NNA).
	 */
	private String isin;
	/**
	 * Option Style - European, American, Asian
	 */
	private OptionStyleTypes optionStyle;
	/**
	 * Option Type (Put or Call)
	 */
	private String optionType;
	/**
	 * Exercise or Strike Price -  how much the underlying will be purchased or sold for under the terms of the contract
	 */
	private BigDecimal strikePrice;
	private Date securityExpirationDate;
	private BigDecimal priceMultiplier;
	/**
	 * Date on which a security is issued. In case of a bond, the date from which a bondholder is entitled to receive interest irrespective of the date the bond was purchased
	 * or delivered.
	 */
	private Date issueDate;
	/**
	 * Maturity date is the date on which the principal amount of a note, draft, acceptance bond or another debt instrument becomes due and is repaid to the investor
	 * and interest payments stop. (End Date)
	 */
	private Date maturityDate;
	/**
	 * The annual interest rate paid on a bond, expressed as a percentage of the face value.
	 * It is also referred to as the "coupon rate," "coupon percent rate" and "nominal yield."
	 */
	private BigDecimal interestRate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public String getSecurityDescription() {
		return this.securityDescription;
	}


	public void setSecurityDescription(String securityDescription) {
		this.securityDescription = securityDescription;
	}


	public String getSecurityExchangeCode() {
		return this.securityExchangeCode;
	}


	public void setSecurityExchangeCode(String securityExchangeCode) {
		this.securityExchangeCode = securityExchangeCode;
	}


	public String getSecurityCurrency() {
		return this.securityCurrency;
	}


	public void setSecurityCurrency(String securityCurrency) {
		this.securityCurrency = securityCurrency;
	}


	public ISITCCodes getIsitcCode() {
		return this.isitcCode;
	}


	public void setIsitcCode(ISITCCodes isitcCode) {
		this.isitcCode = isitcCode;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public Date getSecurityExpirationDate() {
		return this.securityExpirationDate;
	}


	public void setSecurityExpirationDate(Date securityExpirationDate) {
		this.securityExpirationDate = securityExpirationDate;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public OptionStyleTypes getOptionStyle() {
		return this.optionStyle;
	}


	public void setOptionStyle(OptionStyleTypes optionStyle) {
		this.optionStyle = optionStyle;
	}


	public String getOptionType() {
		return this.optionType;
	}


	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}


	public Date getIssueDate() {
		return this.issueDate;
	}


	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	public BigDecimal getStrikePrice() {
		return this.strikePrice;
	}


	public void setStrikePrice(BigDecimal strikePrice) {
		this.strikePrice = strikePrice;
	}
}
