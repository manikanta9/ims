Clifton.instruction.message.InstructionMessagePreviewWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Instruction Message Preview',
	width: 800,
	height: 600,
	iconCls: 'shopping-cart',
	enableShowInfo: false,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Message',
				tbar: [{
					text: 'Validate',
					iconCls: 'run',
					width: 150,
					handler: function() {
						const win = TCG.getParentByClass(this, Ext.Window);
						const formPanel = win.getMainFormPanel();
						const form = formPanel.getForm();
						const cm = {
							messageType: 'MT',
							message: form.findField('message').getValue()
						};

						const clz = 'Clifton.swift.validation.SwiftValidationWindow';
						const cmpId = TCG.getComponentId(clz, win.tableName + '.' + win.fkFieldId);
						TCG.createComponent(clz, {
							id: cmpId,
							defaultDataIsReal: true,
							defaultData: cm,
							doValidationAfterLoad: false
						});
					}
				}, '-'],
				items: [{
					xtype: 'formpanel',
					labelWidth: 120,
					instructions: 'This form will validate a SWIFT message.  Select the message type MT/MX, paste the message text in the Message field and click validate.  A list of any errors on the message will appear in the validation response field.',
					items: [
						{fieldLabel: 'Message', name: 'message', xtype: 'textarea', height: 350, anchor: '-35 -50', readOnly: true}
					]
				}]
			},
			{
				title: 'Formatted Message',
				items: [{
					xtype: 'formpanel',
					labelWidth: 120,
					items: [
						{fieldLabel: 'Formatted Message', name: 'formattedMessage', xtype: 'textarea', height: 350, anchor: '-35 -50', readOnly: true}
					]
				}]
			}
		]
	}]
});
