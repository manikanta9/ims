Ext.ns('Clifton.instruction', 'Clifton.instruction.message');

Clifton.instruction.openInstructionPreview = function(fkFieldId, tableName, opener) {
	if (Clifton.instruction.openInstructionPreview.supportedTables.indexOf(tableName, 0) >= 0) {
		let tableParams = 'tableName=' + tableName;
		if (TCG.isEquals(tableName, 'AccountingPositionTransfer')) {
			tableParams = 'tableNames=' + tableName + '&tableNames=CollateralBalance';
		}
		TCG.createComponent('TCG.app.OKCancelWindow', {
			title: 'Select Investment Instruction Definition.',
			iconCls: 'fax',
			height: 150,
			width: 600,
			modal: true,
			openerCt: opener,
			items: [{
				xtype: 'formpanel',
				instructions: 'Select an Investment Instruction Definition for generating the Instruction Message.',
				labelWidth: 140,
				items: [
					{
						fieldLabel: 'Instruction Definition',
						name: 'definition.name',
						hiddenName: 'definition.id',
						xtype: 'combo',
						url: 'investmentInstructionDefinitionListFind.json?' + tableParams + '&excludeWorkflowStatusName:Inactive',
						allowBlank: false,
						forceSelection: true,
						doNotAddContextMenu: true
					}
				]
			}],
			saveWindow: function(closeOnSuccess, forceSubmit) {
				const panel = this.getMainFormPanel();
				const definitionId = panel.getFormValue('definition.id');
				Clifton.instruction.openInstructionPreview.preview(fkFieldId, tableName, definitionId);
				this.closeWindow();
			}
		});
	}
	else {
		Clifton.instruction.openInstructionPreview.preview(fkFieldId, tableName);
	}
};

Clifton.instruction.openInstructionPreview.supportedTables = [
	'CollateralBalance',
	'AccountingM2MDaily',
	'Trade',
	'AccountingPositionTransfer',
	'LendingRepo'
];

Clifton.instruction.openInstructionPreview.preview = function(fkFieldId, tableName, definitionId) {
	let url = 'investmentInstructionPreview.json?fkFieldId=' + fkFieldId + '&tableName=' + tableName;
	if (definitionId) {
		url = url.concat('&definitionId=', definitionId);
	}
	TCG.data.getDataValuePromise(url)
			.then(function(messageText) {
				const cm = JSON.parse(messageText);
				const clz = 'Clifton.instruction.message.InstructionMessagePreviewWindow';
				const cmpId = TCG.getComponentId(clz, tableName + '.' + fkFieldId);
				TCG.createComponent(clz, {
					id: cmpId,
					tableName: tableName,
					fkFieldId: fkFieldId,
					defaultDataIsReal: true,
					defaultData: cm.data
				});
			});
};

Clifton.investment.instruction.run.runGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'investmentInstructionRunListFind',
	instructions: 'An Instruction Run is a collection of items to be sent to designated Instruction Contacts. They are created by selecting items in the Instruction Items tab and pressing the Send button. A run is created for each contact that matches contact group and recipient company of that instruction.',
	wikiPage: 'IT/Instruction+Runs',
	xtype: 'gridpanel',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Instruction', width: 100, dataIndex: 'investmentInstruction.label', filter: {searchFieldName: 'searchPattern'}},
		{header: 'Run Type', width: 10, dataIndex: 'runType', filter: {searchFieldName: 'runTypeList', type: 'list', options: Clifton.investment.instruction.RunTypes}},
		{
			header: 'Status', width: 15, dataIndex: 'runStatus.name',
			filter: {searchFieldName: 'instructionRunStatusList', type: 'list', options: Clifton.investment.instruction.StatusList},
			renderer: function(v, m, r) {
				return (Clifton.investment.instruction.renderStatus(v, m, r));
			}
		},
		{header: 'Detached', width: 10, dataIndex: 'detached', type: 'boolean', tooltip: 'The run will not update the status of the Instruction Item or Instruction that it was created for'},
		{header: 'Scheduled Date', width: 20, dataIndex: 'scheduledDate', hidden: true},
		{header: 'Start Date', width: 20, dataIndex: 'startDate'},
		{header: 'End Date', width: 20, dataIndex: 'endDate'}
	],
	editor: {
		addEnabled: false,
		deleteEnabled: false,
		detailPageClass: 'Clifton.investment.instruction.run.RunWindow'
	},
	getLoadParams: function() {
		const investmentInstructionId = this.getWindow().params.id;
		return {instructionId: investmentInstructionId};
	}
});
Ext.reg('investment-instruction-run-grid', Clifton.investment.instruction.run.runGrid);

Clifton.investment.instruction.run.openInstructionRunErrorWindow = function(itemIds, opener) {
	if (itemIds && itemIds.length > 0) {
		TCG.createComponent('TCG.app.OKCancelWindow', {
			title: 'Select Investment Instruction Runs',
			iconCls: 'fax',
			height: 300,
			width: 700,
			modal: true,
			openerCt: opener,
			okButtonText: 'Mark Error',
			items: [
				{
					name: 'investmentInstructionRunForItemListLatest',
					xtype: 'investment-instruction-run-grid',
					instructions: 'Select latest Investment Instruction Runs to mark as \'ERROR\'.',
					rowSelectionModel: 'checkbox',
					columnOverrides: [
						{width: 80, dataIndex: 'investmentInstruction.label', hidden: true},
						{width: 30, dataIndex: 'runType'}
					],
					editor: {
						addEnabled: false,
						deleteEnabled: false
					},
					getLoadParams: function() {
						return {
							itemIds: itemIds
						};
					}
				}
			],
			saveWindow: function(closeOnSuccess, forceSubmit) {
				const window = this;
				const gridPanel = window.items.items[0];
				const grid = gridPanel.grid;
				const rows = grid.getSelectionModel().getSelections();
				if (rows.length > 0) {
					const ids = [];
					for (let i = 0; i < rows.length; i++) {
						ids.push(rows[i].data.id);
					}
					const loader = new TCG.data.JsonLoader({
						waitTarget: window,
						waitMsg: 'Marking Runs...',
						params: {runIds: ids}
					});
					loader.load('investmentInstructionRunListErrorSave.json');
				}
				this.closeWindow();
			}
		});
	}
};
