package com.clifton.instruction;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class InstructionProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "instruction";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.beans.");
	}
}
