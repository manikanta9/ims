package com.clifton.instruction;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.instruction.messages.trade.DeliverAgainstPaymentBuilder;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class InstructionServiceImplTest<M extends InstructionMessage, P, I extends Instruction> {

	private InstructionServiceImpl<M, P, I> instructionService = new InstructionServiceImpl<>();


	@Test
	public void cloneToAdditionalRecipientsTest() {
		InstructionMessage deliverAgainstPayment = DeliverAgainstPaymentBuilder.createEmptyDeliverAgainstPayment()
				.withMessageFunctions(MessageFunctions.NEW_MESSAGE)
				.withOpen(true)
				.withCustodyBIC("Custody")
				.withCustodyAccountNumber("Custody")
				.withClearingBrokerIdentifier("Clearing", MessagePartyIdentifierTypes.BIC)
				.withExecutingBrokerIdentifier("Execute", MessagePartyIdentifierTypes.DTC)
				.withTradeDate(DateUtils.toDate("17/18/19"))
				.withSettlementDate(DateUtils.toDate("14/15/16"))
				.withPriceType(TradeMessagePriceTypes.PERCENTAGE)
				.withPrice(new BigDecimal("4.55"))
				.withQuantity(new BigDecimal("8.99"))
				.withAccountingNotional(new BigDecimal("1.22"))
				.withCommissionAmount(new BigDecimal("2.33"))
				.withCurrentFactor(new BigDecimal("4.56"))
				.withAccruedInterest(new BigDecimal("8.09"))
				.withPlaceOfSettlement("SETTLE")
				.withRegulatoryAmount(new BigDecimal("4.21"))
				.withBrokerCommissionAmount(new BigDecimal("3.66"))
				.withAmortizedFaceAmount(new BigDecimal("21.11"))
				.withOriginalFaceAmount(new BigDecimal("89.00"))
				.withDealAmount(new BigDecimal("56.6"))
				.withNetSettlementAmount(new BigDecimal("98.2"))
				.withSafekeepingAccount("Safe")
				.withClearingSafekeepingAccount("Clear Safe")
				.withRepurchaseAgreement(true)
				.withRepoNetCashAmount(new BigDecimal("753.33"))
				.withRepoClosingDate(DateUtils.toDate("1/2/18"))
				.withRepoAccruedInterestAmount(new BigDecimal("987.32"))
				.withRepoInterestRate(new BigDecimal("321.1"))
				.withRepoHaircutPercentage(new BigDecimal("245.4"))

				.withSecuritySymbol("Symbol")
				.withSecurityDescription("Description")
				.withSecurityCurrency("USD")
				.withSecurityExchangeCode("Exchange")
				.withIsitcCode(ISITCCodes.TIPS)
				.withIsin("ISIN")
				.withOptionStyleType(OptionStyleTypes.AMERICAN)
				.withOptionType("Option Type")
				.withStrikePrice(new BigDecimal("42.2"))
				.withSecurityExpirationDate(DateUtils.toDate("2/3/19"))
				.withPriceMultiplier(new BigDecimal("6.77"))
				.withIssueDate(DateUtils.toDate("9/7/18"))
				.withMaturityDate(DateUtils.toDate("5/6/19"))
				.withInterestRate(new BigDecimal("5.98"))

				.withTransactionReferenceNumber("Transaction")
				.withSenderBIC("Sender")
				.withReceiverBIC("Receiver")
				.withMessageReferenceNumber(123456L)
				.toInstructionMessage();
		List<InstructionMessage> clones = this.instructionService.cloneToAdditionalRecipients(deliverAgainstPayment, Arrays.asList("BIC_ONE", "BIC_TWO"));
		Assertions.assertEquals(2, clones.size());

		Map<String, Object> original = BeanUtils.describe(deliverAgainstPayment);
		original.put("receiverBIC", "BIC_ONE");
		original.put("transactionReferenceNumber", "Transaction_DUPLICATE_1");
		Assertions.assertTrue(clones.get(0) instanceof DeliverAgainstPayment);
		Assertions.assertEquals(original, BeanUtils.describe(clones.get(0)));

		original.put("receiverBIC", "BIC_TWO");
		original.put("transactionReferenceNumber", "Transaction_DUPLICATE_2");
		Assertions.assertTrue(clones.get(1) instanceof DeliverAgainstPayment);
		Assertions.assertEquals(original, BeanUtils.describe(clones.get(1)));
	}
}
