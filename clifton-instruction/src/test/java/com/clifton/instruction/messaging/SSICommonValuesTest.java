package com.clifton.instruction.messaging;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;


public class SSICommonValuesTest {

	@Test
	public void preventDuplicateCodes() {
		Map<String, Long> duplicates = Arrays.stream(SSICommonValues.values()).collect(groupingBy(SSICommonValues::getCode, counting()));
		duplicates = duplicates.entrySet().stream().filter(entry -> entry.getValue() > 1).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		Assertions.assertEquals(0, duplicates.size(), "Duplicate Codes Are Not Allowed:  " + duplicates.toString());
	}
}
