package com.clifton.instruction.messages;

import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.OptionStyleTypes;

import java.math.BigDecimal;
import java.util.Date;


public class InstructionSecurityMessageBuilder extends InstructionMessageBuilder {

	protected InstructionSecurityMessageBuilder(InstructionSecurityMessage instructionSecurityMessage) {
		super(instructionSecurityMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InstructionSecurityMessageBuilder createInstructionSecurityMessageBuilder() {
		return new InstructionSecurityMessageBuilder(new InstructionSecurityMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InstructionSecurityMessageBuilder withTransactionReferenceNumber(String transactionReferenceNumber) {
		return (InstructionSecurityMessageBuilder) super.withTransactionReferenceNumber(transactionReferenceNumber);
	}


	@Override
	public InstructionSecurityMessageBuilder withSenderBIC(String senderBIC) {
		return (InstructionSecurityMessageBuilder) super.withSenderBIC(senderBIC);
	}


	@Override
	public InstructionSecurityMessageBuilder withReceiverBIC(String receiverBIC) {
		return (InstructionSecurityMessageBuilder) super.withReceiverBIC(receiverBIC);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InstructionSecurityMessageBuilder withSecuritySymbol(String securitySymbol) {
		getInstructionSecurityMessage().setSecuritySymbol(securitySymbol);
		return this;
	}


	public InstructionSecurityMessageBuilder withSecurityDescription(String securityDescription) {
		getInstructionSecurityMessage().setSecurityDescription(securityDescription);
		return this;
	}


	public InstructionSecurityMessageBuilder withSecurityCurrency(String securityCurrency) {
		getInstructionSecurityMessage().setSecurityCurrency(securityCurrency);
		return this;
	}


	public InstructionSecurityMessageBuilder withSecurityExchangeCode(String securityExchangeCode) {
		getInstructionSecurityMessage().setSecurityExchangeCode(securityExchangeCode);
		return this;
	}


	public InstructionSecurityMessageBuilder withIsitcCode(ISITCCodes isitcCode) {
		getInstructionSecurityMessage().setIsitcCode(isitcCode);
		return this;
	}


	public InstructionSecurityMessageBuilder withIsin(String isin) {
		getInstructionSecurityMessage().setIsin(isin);
		return this;
	}


	public InstructionSecurityMessageBuilder withOptionStyleType(OptionStyleTypes optionStyle) {
		getInstructionSecurityMessage().setOptionStyle(optionStyle);
		return this;
	}


	public InstructionSecurityMessageBuilder withOptionType(String optionType) {
		getInstructionSecurityMessage().setOptionType(optionType);
		return this;
	}


	public InstructionSecurityMessageBuilder withStrikePrice(BigDecimal strikePrice) {
		getInstructionSecurityMessage().setStrikePrice(strikePrice);
		return this;
	}


	public InstructionSecurityMessageBuilder withSecurityExpirationDate(Date securityExpirationDate) {
		getInstructionSecurityMessage().setSecurityExpirationDate(securityExpirationDate);
		return this;
	}


	public InstructionSecurityMessageBuilder withPriceMultiplier(BigDecimal priceMultiplier) {
		getInstructionSecurityMessage().setPriceMultiplier(priceMultiplier);
		return this;
	}


	public InstructionSecurityMessageBuilder withIssueDate(Date issueDate) {
		getInstructionSecurityMessage().setIssueDate(issueDate);
		return this;
	}


	public InstructionSecurityMessageBuilder withMaturityDate(Date maturityDate) {
		getInstructionSecurityMessage().setMaturityDate(maturityDate);
		return this;
	}


	public InstructionSecurityMessageBuilder withInterestRate(BigDecimal interestRate) {
		getInstructionSecurityMessage().setInterestRate(interestRate);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected InstructionSecurityMessage getInstructionSecurityMessage() {
		return (InstructionSecurityMessage) super.getInstructionMessage();
	}


	public InstructionSecurityMessage toInstructionSecurityMessage() {
		return getInstructionSecurityMessage();
	}
}
