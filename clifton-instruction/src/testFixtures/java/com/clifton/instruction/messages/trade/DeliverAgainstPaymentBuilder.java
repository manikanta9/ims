package com.clifton.instruction.messages.trade;

import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;

import java.math.BigDecimal;
import java.util.Date;


public class DeliverAgainstPaymentBuilder extends AbstractTradeMessageBuilder {

	private DeliverAgainstPaymentBuilder(DeliverAgainstPayment deliverAgainstPayment) {
		super(deliverAgainstPayment);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static DeliverAgainstPaymentBuilder createEmptyDeliverAgainstPayment() {
		return new DeliverAgainstPaymentBuilder(new DeliverAgainstPayment());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DeliverAgainstPaymentBuilder withMessageFunctions(MessageFunctions messageFunctions) {
		return (DeliverAgainstPaymentBuilder) super.withMessageFunctions(messageFunctions);
	}


	@Override
	public DeliverAgainstPaymentBuilder withOpen(boolean open) {
		return (DeliverAgainstPaymentBuilder) super.withOpen(open);
	}


	@Override
	public DeliverAgainstPaymentBuilder withCustodyBIC(String custodyBIC) {
		return (DeliverAgainstPaymentBuilder) super.withCustodyBIC(custodyBIC);
	}


	@Override
	public DeliverAgainstPaymentBuilder withCustodyAccountNumber(String custodyAccountNumber) {
		return (DeliverAgainstPaymentBuilder) super.withCustodyAccountNumber(custodyAccountNumber);
	}


	@Override
	public DeliverAgainstPaymentBuilder withClearingBrokerIdentifier(String clearingBrokerIdentifier, MessagePartyIdentifierTypes type) {
		return (DeliverAgainstPaymentBuilder) super.withClearingBrokerIdentifier(clearingBrokerIdentifier, type);
	}


	@Override
	public DeliverAgainstPaymentBuilder withExecutingBrokerIdentifier(String executingBrokerIdentifier, MessagePartyIdentifierTypes type) {
		return (DeliverAgainstPaymentBuilder) super.withExecutingBrokerIdentifier(executingBrokerIdentifier, type);
	}


	@Override
	public DeliverAgainstPaymentBuilder withTradeDate(Date tradeDate) {
		return (DeliverAgainstPaymentBuilder) super.withTradeDate(tradeDate);
	}


	@Override
	public DeliverAgainstPaymentBuilder withSettlementDate(Date settlementDate) {
		return (DeliverAgainstPaymentBuilder) super.withSettlementDate(settlementDate);
	}


	@Override
	public DeliverAgainstPaymentBuilder withPriceType(TradeMessagePriceTypes priceType) {
		return (DeliverAgainstPaymentBuilder) super.withPriceType(priceType);
	}


	@Override
	public DeliverAgainstPaymentBuilder withPrice(BigDecimal price) {
		return (DeliverAgainstPaymentBuilder) super.withPrice(price);
	}


	@Override
	public DeliverAgainstPaymentBuilder withQuantity(BigDecimal quantity) {
		return (DeliverAgainstPaymentBuilder) super.withQuantity(quantity);
	}


	@Override
	public DeliverAgainstPaymentBuilder withAccountingNotional(BigDecimal accountingNotional) {
		return (DeliverAgainstPaymentBuilder) super.withAccountingNotional(accountingNotional);
	}


	@Override
	public DeliverAgainstPaymentBuilder withCommissionAmount(BigDecimal commissionAmount) {
		return (DeliverAgainstPaymentBuilder) super.withCommissionAmount(commissionAmount);
	}


	@Override
	public DeliverAgainstPaymentBuilder withSecuritySymbol(String securitySymbol) {
		return (DeliverAgainstPaymentBuilder) super.withSecuritySymbol(securitySymbol);
	}


	@Override
	public DeliverAgainstPaymentBuilder withSecurityDescription(String securityDescription) {
		return (DeliverAgainstPaymentBuilder) super.withSecurityDescription(securityDescription);
	}


	@Override
	public DeliverAgainstPaymentBuilder withSecurityCurrency(String securityCurrency) {
		return (DeliverAgainstPaymentBuilder) super.withSecurityCurrency(securityCurrency);
	}


	@Override
	public DeliverAgainstPaymentBuilder withSecurityExchangeCode(String securityExchangeCode) {
		return (DeliverAgainstPaymentBuilder) super.withSecurityExchangeCode(securityExchangeCode);
	}


	@Override
	public DeliverAgainstPaymentBuilder withIsitcCode(ISITCCodes isitcCode) {
		return (DeliverAgainstPaymentBuilder) super.withIsitcCode(isitcCode);
	}


	@Override
	public DeliverAgainstPaymentBuilder withSecurityExpirationDate(Date securityExpirationDate) {
		return (DeliverAgainstPaymentBuilder) super.withSecurityExpirationDate(securityExpirationDate);
	}


	@Override
	public DeliverAgainstPaymentBuilder withPriceMultiplier(BigDecimal priceMultiplier) {
		return (DeliverAgainstPaymentBuilder) super.withPriceMultiplier(priceMultiplier);
	}


	@Override
	public DeliverAgainstPaymentBuilder withTransactionReferenceNumber(String transactionReferenceNumber) {
		return (DeliverAgainstPaymentBuilder) super.withTransactionReferenceNumber(transactionReferenceNumber);
	}


	@Override
	public DeliverAgainstPaymentBuilder withSenderBIC(String senderBIC) {
		return (DeliverAgainstPaymentBuilder) super.withSenderBIC(senderBIC);
	}


	@Override
	public DeliverAgainstPaymentBuilder withReceiverBIC(String receiverBIC) {
		return (DeliverAgainstPaymentBuilder) super.withReceiverBIC(receiverBIC);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DeliverAgainstPayment getDeliverAgainstPayment() {
		return (DeliverAgainstPayment) super.getAbstractTradeMessage();
	}


	public DeliverAgainstPayment toDeliverAgainstPayment() {
		return getDeliverAgainstPayment();
	}
}
