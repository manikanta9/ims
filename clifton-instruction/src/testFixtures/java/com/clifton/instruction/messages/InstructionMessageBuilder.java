package com.clifton.instruction.messages;

public class InstructionMessageBuilder {

	private InstructionMessage instructionMessage;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected InstructionMessageBuilder(InstructionMessage instructionMessage) {
		this.instructionMessage = instructionMessage;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InstructionMessageBuilder createEmptyInstructionMessageBuilder() {
		return new InstructionMessageBuilder(new InstructionMessageBase());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InstructionMessageBuilder withTransactionReferenceNumber(String transactionReferenceNumber) {
		getInstructionMessage().setTransactionReferenceNumber(transactionReferenceNumber);
		return this;
	}


	public InstructionMessageBuilder withSenderBIC(String senderBIC) {
		getInstructionMessage().setSenderBIC(senderBIC);
		return this;
	}


	public InstructionMessageBuilder withReceiverBIC(String receiverBIC) {
		getInstructionMessage().setReceiverBIC(receiverBIC);
		return this;
	}


	public InstructionMessageBuilder withMessageReferenceNumber(Long messageReferenceNumber) {
		getInstructionMessage().setMessageReferenceNumber(messageReferenceNumber);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected InstructionMessage getInstructionMessage() {
		return this.instructionMessage;
	}


	public InstructionMessage toInstructionMessage() {
		return getInstructionMessage();
	}
}
