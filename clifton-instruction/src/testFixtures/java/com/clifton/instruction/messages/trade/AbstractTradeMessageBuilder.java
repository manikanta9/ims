package com.clifton.instruction.messages.trade;

import com.clifton.instruction.messages.InstructionSecurityMessageBuilder;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.instruction.messages.transfers.ExposureTypes;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;

import java.math.BigDecimal;
import java.util.Date;


public class AbstractTradeMessageBuilder extends InstructionSecurityMessageBuilder {

	protected AbstractTradeMessageBuilder(AbstractTradeMessage tradeMessage) {
		super(tradeMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractTradeMessageBuilder withMessageFunctions(MessageFunctions messageFunctions) {
		getAbstractTradeMessage().setMessageFunctions(messageFunctions);
		return this;
	}


	public AbstractTradeMessageBuilder withOpen(boolean open) {
		getAbstractTradeMessage().setOpen(open);
		return this;
	}


	public AbstractTradeMessageBuilder withCustodyBIC(String custodyBIC) {
		getAbstractTradeMessage().setCustodyBIC(custodyBIC);
		return this;
	}


	public AbstractTradeMessageBuilder withCustodyAccountNumber(String custodyAccountNumber) {
		getAbstractTradeMessage().setCustodyAccountNumber(custodyAccountNumber);
		return this;
	}


	public AbstractTradeMessageBuilder withClearingBrokerIdentifier(String clearingBrokerIdentifier, MessagePartyIdentifierTypes type) {
		getAbstractTradeMessage().setClearingBrokerIdentifier(new PartyIdentifier(clearingBrokerIdentifier, type));
		return this;
	}


	public AbstractTradeMessageBuilder withExecutingBrokerIdentifier(String executingBrokerIdentifier, MessagePartyIdentifierTypes type) {
		getAbstractTradeMessage().setExecutingBrokerIdentifier(new PartyIdentifier(executingBrokerIdentifier, type));
		return this;
	}


	public AbstractTradeMessageBuilder withTradeDate(Date tradeDate) {
		getAbstractTradeMessage().setTradeDate(tradeDate);
		return this;
	}


	public AbstractTradeMessageBuilder withSettlementDate(Date settlementDate) {
		getAbstractTradeMessage().setSettlementDate(settlementDate);
		return this;
	}


	public AbstractTradeMessageBuilder withPriceType(TradeMessagePriceTypes priceType) {
		getAbstractTradeMessage().setPriceType(priceType);
		return this;
	}


	public AbstractTradeMessageBuilder withPrice(BigDecimal price) {
		getAbstractTradeMessage().setPrice(price);
		return this;
	}


	public AbstractTradeMessageBuilder withQuantity(BigDecimal quantity) {
		getAbstractTradeMessage().setQuantity(quantity);
		return this;
	}


	public AbstractTradeMessageBuilder withAccountingNotional(BigDecimal accountingNotional) {
		getAbstractTradeMessage().setAccountingNotional(accountingNotional);
		return this;
	}


	public AbstractTradeMessageBuilder withCommissionAmount(BigDecimal commissionAmount) {
		getAbstractTradeMessage().setCommissionAmount(commissionAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withCurrentFactor(BigDecimal currentFactor) {
		getAbstractTradeMessage().setCurrentFactor(currentFactor);
		return this;
	}


	public AbstractTradeMessageBuilder withAccruedInterest(BigDecimal accruedInterest) {
		getAbstractTradeMessage().setAccruedInterest(accruedInterest);
		return this;
	}


	public AbstractTradeMessageBuilder withPlaceOfSettlement(String placeOfSettlement) {
		getAbstractTradeMessage().setPlaceOfSettlement(placeOfSettlement);
		return this;
	}


	public AbstractTradeMessageBuilder withRegulatoryAmount(BigDecimal regulatoryAmount) {
		getAbstractTradeMessage().setRegulatoryAmount(regulatoryAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withBrokerCommissionAmount(BigDecimal brokerCommissionAmount) {
		getAbstractTradeMessage().setBrokerCommissionAmount(brokerCommissionAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withAmortizedFaceAmount(BigDecimal amortizedFaceAmount) {
		getAbstractTradeMessage().setAmortizedFaceAmount(amortizedFaceAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withOriginalFaceAmount(BigDecimal originalFaceAmount) {
		getAbstractTradeMessage().setOriginalFaceAmount(originalFaceAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withDealAmount(BigDecimal dealAmount) {
		getAbstractTradeMessage().setDealAmount(dealAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withNetSettlementAmount(BigDecimal netSettlementAmount) {
		getAbstractTradeMessage().setNetSettlementAmount(netSettlementAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withSafekeepingAccount(String safekeepingAccount) {
		getAbstractTradeMessage().setSafekeepingAccount(safekeepingAccount);
		return this;
	}


	public AbstractTradeMessageBuilder withClearingSafekeepingAccount(String clearingSafekeepingAccount) {
		getAbstractTradeMessage().setClearingSafekeepingAccount(clearingSafekeepingAccount);
		return this;
	}


	public AbstractTradeMessageBuilder withRepurchaseAgreement(boolean repurchaseAgreement) {
		getAbstractTradeMessage().setRepurchaseAgreement(repurchaseAgreement);
		return this;
	}


	public AbstractTradeMessageBuilder withRepoNetCashAmount(BigDecimal repoNetCashAmount) {
		getAbstractTradeMessage().setRepoNetCashAmount(repoNetCashAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withRepoClosingDate(Date repoClosingDate) {
		getAbstractTradeMessage().setRepoClosingDate(repoClosingDate);
		return this;
	}


	public AbstractTradeMessageBuilder withRepoAccruedInterestAmount(BigDecimal repoAccruedInterestAmount) {
		getAbstractTradeMessage().setRepoAccruedInterestAmount(repoAccruedInterestAmount);
		return this;
	}


	public AbstractTradeMessageBuilder withRepoInterestRate(BigDecimal repoInterestRate) {
		getAbstractTradeMessage().setRepoInterestRate(repoInterestRate);
		return this;
	}


	public AbstractTradeMessageBuilder withRepoHaircutPercentage(BigDecimal repoHaircutPercentage) {
		getAbstractTradeMessage().setRepoHaircutPercentage(repoHaircutPercentage);
		return this;
	}


	public AbstractTradeMessageBuilder withSettlementTransactionType(SettlementTransactionTypes settlementTransactionType) {
		getAbstractTradeMessage().setSettlementTransactionType(settlementTransactionType);
		return this;
	}


	public AbstractTradeMessageBuilder withExposureType(ExposureTypes exposureType) {
		getAbstractTradeMessage().setExposureType(exposureType);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected AbstractTradeMessage getAbstractTradeMessage() {
		return (AbstractTradeMessage) super.getInstructionMessage();
	}
}
