package com.clifton.instruction.messages.trade;

import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;

import java.math.BigDecimal;
import java.util.Date;


public class ReceiveAgainstPaymentBuilder extends AbstractTradeMessageBuilder {

	private ReceiveAgainstPaymentBuilder(ReceiveAgainstPayment receiveAgainstPayment) {
		super(receiveAgainstPayment);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ReceiveAgainstPaymentBuilder createEmptyReceiveAgainstPayment() {
		return new ReceiveAgainstPaymentBuilder(new ReceiveAgainstPayment());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReceiveAgainstPaymentBuilder withMessageFunctions(MessageFunctions messageFunctions) {
		return (ReceiveAgainstPaymentBuilder) super.withMessageFunctions(messageFunctions);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withOpen(boolean open) {
		return (ReceiveAgainstPaymentBuilder) super.withOpen(open);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withCustodyBIC(String custodyBIC) {
		return (ReceiveAgainstPaymentBuilder) super.withCustodyBIC(custodyBIC);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withCustodyAccountNumber(String custodyAccountNumber) {
		return (ReceiveAgainstPaymentBuilder) super.withCustodyAccountNumber(custodyAccountNumber);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withClearingBrokerIdentifier(String clearingBrokerIdentifier, MessagePartyIdentifierTypes type) {
		return (ReceiveAgainstPaymentBuilder) super.withClearingBrokerIdentifier(clearingBrokerIdentifier, type);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withExecutingBrokerIdentifier(String executingBrokerIdentifier, MessagePartyIdentifierTypes type) {
		return (ReceiveAgainstPaymentBuilder) super.withExecutingBrokerIdentifier(executingBrokerIdentifier, type);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withTradeDate(Date tradeDate) {
		return (ReceiveAgainstPaymentBuilder) super.withTradeDate(tradeDate);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withSettlementDate(Date settlementDate) {
		return (ReceiveAgainstPaymentBuilder) super.withSettlementDate(settlementDate);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withPriceType(TradeMessagePriceTypes priceType) {
		return (ReceiveAgainstPaymentBuilder) super.withPriceType(priceType);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withPrice(BigDecimal price) {
		return (ReceiveAgainstPaymentBuilder) super.withPrice(price);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withQuantity(BigDecimal quantity) {
		return (ReceiveAgainstPaymentBuilder) super.withQuantity(quantity);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withAccountingNotional(BigDecimal accountingNotional) {
		return (ReceiveAgainstPaymentBuilder) super.withAccountingNotional(accountingNotional);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withCommissionAmount(BigDecimal commissionAmount) {
		return (ReceiveAgainstPaymentBuilder) super.withCommissionAmount(commissionAmount);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withSecuritySymbol(String securitySymbol) {
		return (ReceiveAgainstPaymentBuilder) super.withSecuritySymbol(securitySymbol);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withSecurityDescription(String securityDescription) {
		return (ReceiveAgainstPaymentBuilder) super.withSecurityDescription(securityDescription);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withSecurityCurrency(String securityCurrency) {
		return (ReceiveAgainstPaymentBuilder) super.withSecurityCurrency(securityCurrency);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withSecurityExchangeCode(String securityExchangeCode) {
		return (ReceiveAgainstPaymentBuilder) super.withSecurityExchangeCode(securityExchangeCode);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withIsitcCode(ISITCCodes isitcCode) {
		return (ReceiveAgainstPaymentBuilder) super.withIsitcCode(isitcCode);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withSecurityExpirationDate(Date securityExpirationDate) {
		return (ReceiveAgainstPaymentBuilder) super.withSecurityExpirationDate(securityExpirationDate);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withPriceMultiplier(BigDecimal priceMultiplier) {
		return (ReceiveAgainstPaymentBuilder) super.withPriceMultiplier(priceMultiplier);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withTransactionReferenceNumber(String transactionReferenceNumber) {
		return (ReceiveAgainstPaymentBuilder) super.withTransactionReferenceNumber(transactionReferenceNumber);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withSenderBIC(String senderBIC) {
		return (ReceiveAgainstPaymentBuilder) super.withSenderBIC(senderBIC);
	}


	@Override
	public ReceiveAgainstPaymentBuilder withReceiverBIC(String receiverBIC) {
		return (ReceiveAgainstPaymentBuilder) super.withReceiverBIC(receiverBIC);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ReceiveAgainstPayment getReceiveAgainstPayment() {
		return (ReceiveAgainstPayment) super.getAbstractTradeMessage();
	}


	public ReceiveAgainstPayment toReceiveAgainstPayment() {
		return getReceiveAgainstPayment();
	}
}
