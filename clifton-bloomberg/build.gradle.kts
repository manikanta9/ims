import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant

val clientVariant = registerVariant("client", dependsOnMain = true)
val serverVariant = registerVariant("server", dependsOnMain = true)

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	serverVariant.api("com.bloomberglp.blpapi:blpapi:3.17.1-1")
	serverVariant.api(project(":clifton-bloomberg:clifton-mds-api")) { usingVariant("java-client") }


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	api(project(":clifton-core"))
	api(project(":clifton-security"))
	api(project(":clifton-marketdata"))

	clientVariant.api(project(":clifton-accounting"))


	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-accounting")))
}
