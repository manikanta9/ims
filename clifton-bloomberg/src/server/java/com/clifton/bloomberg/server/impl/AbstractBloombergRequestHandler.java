package com.clifton.bloomberg.server.impl;


import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.Request;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.server.BloombergRequestHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;

import java.util.Map;


public abstract class AbstractBloombergRequestHandler<T extends BloombergResponseMessage> implements BloombergRequestHandler<T> {

	@Override
	public void populateRequest(Request request, BloombergRequestMessage requestMessage) {
		requestMessage.getBloombergRequestData().getElementList().forEach((name, element) -> {
			if ((element.getRequestValue() != null) && !element.getRequestValue().isEmpty()) {
				// single value request element
				LogUtils.info(getClass(), "Adding element " + element.getName() + " = " + element.getRequestValue());
				request.set(element.getName(), element.getRequestValue());
			}
			else {
				// multi-value request element
				Element requestElement = request.getElement(element.getName());
				for (String obj : element.getRequestData()) {
					LogUtils.info(getClass(), "Appending '" + obj + "' to element '" + element.getName() + "'");
					requestElement.appendValue(obj);
				}
			}

			if (!requestMessage.getOverrides().isEmpty()) {
				Element overrides = request.getElement("overrides");
				for (Map.Entry<String, String> entry : requestMessage.getOverrides().entrySet()) {
					Element override = overrides.appendElement();
					override.setElement("fieldId", entry.getKey());
					override.setElement("value", entry.getValue());
				}
			}
		});
	}


	@Override
	public T processPartialResponse(Message msg, @SuppressWarnings("unused") BloombergRequestMessage originalRequestMessage, T responseMessage) {
		T response = responseMessage == null ? BeanUtils.newInstance(getResponseClass()) : responseMessage;
		readResponse(msg, response);
		return response;
	}


	/**
	 * NOTE: the following impl will likely work and then we can delete the overrides; need to test it when things are working
	 * return (Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	 */
	protected abstract Class<T> getResponseClass();


	protected abstract void readResponse(Message msg, T responseMessage);
}
