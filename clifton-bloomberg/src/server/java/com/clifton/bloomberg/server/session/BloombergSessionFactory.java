package com.clifton.bloomberg.server.session;

import com.clifton.bloomberg.server.session.impl.BloombergBackOfficeSession;
import com.clifton.bloomberg.server.session.impl.BloombergBpipeSession;
import com.clifton.bloomberg.server.session.impl.BloombergDefaultSession;
import com.clifton.bloomberg.server.session.impl.BloombergMdsSession;
import com.clifton.bloomberg.server.session.impl.BloombergSubscriptionSession;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.mds.api.client.BloombergMarketDataApi;


/**
 * <code>BloombergSessionFactory</code> is a factory for creating {@link BloombergSession}s based on provided {@link BloombergSessionOptions}.
 *
 * @author NickK
 */
public class BloombergSessionFactory {

	private AsynchronousMessageHandler subscriptionMessageHandler;


	private BloombergMarketDataApi bloombergMarketDataApi;


	/**
	 * Returns a new {@link BloombergSession} for the provided {@link BloombergSessionOptions};
	 */
	public BloombergSession createSession(BloombergSessionOptions bloombergSessionOptions) {
		if (bloombergSessionOptions.isBPipe()) {
			if (bloombergSessionOptions.isSubscription()) {
				return new BloombergSubscriptionSession(bloombergSessionOptions, getSubscriptionMessageHandler());
			}
			return new BloombergBpipeSession(bloombergSessionOptions);
		}
		else if (bloombergSessionOptions.isBackOffice()) {
			return new BloombergBackOfficeSession(bloombergSessionOptions);
		}
		else if (bloombergSessionOptions.isMds()) {
			return new BloombergMdsSession(bloombergSessionOptions, getBloombergMarketDataApi());
		}
		return new BloombergDefaultSession(bloombergSessionOptions);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AsynchronousMessageHandler getSubscriptionMessageHandler() {
		return this.subscriptionMessageHandler;
	}


	public void setSubscriptionMessageHandler(AsynchronousMessageHandler subscriptionMessageHandler) {
		this.subscriptionMessageHandler = subscriptionMessageHandler;
	}


	public BloombergMarketDataApi getBloombergMarketDataApi() {
		return this.bloombergMarketDataApi;
	}


	public void setBloombergMarketDataApi(BloombergMarketDataApi bloombergMarketDataApi) {
		this.bloombergMarketDataApi = bloombergMarketDataApi;
	}
}
