package com.clifton.bloomberg.server.session;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Identity;
import com.bloomberglp.blpapi.Service;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.server.session.identity.BloombergIdentity;

import java.util.List;


/**
 * <code>BloombergSession</code> is an interface defining methods for interacting with a Bloomberg session.
 *
 * @author NickK
 */
public interface BloombergSession {

	/**
	 * Returns the options this session was created with.
	 */
	public BloombergSessionOptions getOptions();


	/**
	 * Signal start for the session.
	 */
	public void start();


	/**
	 * Signal stop for the session.
	 */
	public void stop();


	/**
	 * Processes the provided {@link BloombergRequestMessage} synchronously by waiting for and
	 * returning the received {@link BloombergResponseMessage}. If the session supports authentication,
	 * the user making the request is authenticated and authorized while making the request.
	 * <p>
	 * Authenticated users remain active for the life of the session.
	 */
	public BloombergResponseMessage submitSynchronousRequest(BloombergRequestMessage bloombergRequest) throws Exception;


	/**
	 * Attempts to authenticate the provided {@link BloombergIdentity} against this session,
	 * returning the created {@link Identity}.
	 */
	public Identity authenticate(BloombergIdentity bloombergIdentity);


	/**
	 * Returns a list of {@link BloombergIdentity}s that this session is aware of that are entitled to
	 * see the provided "eidData" {@link Element} and {@link Service}.
	 */
	public List<BloombergIdentity> getEntitledUserList(Element entitlements, Service service);


	/**
	 * Each request or authenticated user has a unique {@link CorrelationID}. Provided the
	 * correlation ID to this method to cancel a request or close a user session.
	 */
	public void cancel(CorrelationID correlationID);
}
