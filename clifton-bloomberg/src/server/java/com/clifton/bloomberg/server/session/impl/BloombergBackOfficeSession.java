package com.clifton.bloomberg.server.session.impl;

import com.clifton.bloomberg.messages.request.BloombergRequestElement;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.request.HistoricalDataRequest;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.bloomberg.server.session.BloombergSessionOptions;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Represents an interface to the Backoffice data that is stored in Hadoop via Hive
 *
 * @author theodorez
 */
public class BloombergBackOfficeSession extends BloombergDefaultSession {

	public static final String SESSION_NAME = "BackOffice";

	private static final String INVESTMENT_TYPE_TABLE_NAME_CACHE_NAME = "investmentTypeTableNameCache";
	private static final String INVESTMENT_TYPE_TABLE_NAME_CACHE_KEY = "investmentTypeTableNameMap";

	private CacheHandler<String, Map<String, String>> cacheHandler;

	private final SqlHandler sqlHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergBackOfficeSession(BloombergSessionOptions bloombergSessionOptions) {
		super(bloombergSessionOptions, null);
		this.sqlHandler = bloombergSessionOptions.getSqlHandlerLocator().locate("backOfficeDataSource");
		this.cacheHandler = bloombergSessionOptions.getCacheHandler();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public BloombergResponseMessage submitSynchronousRequest(BloombergRequestMessage bloombergRequest) {
		start();
		LogUtils.info(this.getClass(), "Bloomberg BackOffice [Hive] Session is handling request.");
		HistoricalDataRequest historicalDataRequest;
		if (bloombergRequest instanceof HistoricalDataRequest) {
			historicalDataRequest = (HistoricalDataRequest) bloombergRequest;
		}
		else {
			throw new ValidationException("Cannot process message. Request not an instance of HistoricalDataRequest.");
		}

		List<String> fields = getFieldList(historicalDataRequest);

		List<String> securities = historicalDataRequest.getBloombergRequestData().getElement(BloombergRequestMessage.SECURITIES_KEY).getRequestData();
		LogUtils.debug(LogCommand.ofMessageSupplier(getClass(), () -> "Securities being requested: " + StringUtils.collectionToCommaDelimitedString(securities)));

		String investmentType = historicalDataRequest.getInvestmentType();
		LogUtils.debug(getClass(), "Requested Investment Type: " + investmentType);

		BloombergRequestElement requestStartDate = historicalDataRequest.getBloombergRequestData().getElement("startDate");
		BloombergRequestElement requestEndDate = historicalDataRequest.getBloombergRequestData().getElement("endDate");

		Date startDate = requestStartDate != null ? DateUtils.toDate(requestStartDate.getRequestValue(), "yyyyMMdd") : null;
		LogUtils.debug(getClass(), "Requested Start Date: " + startDate);

		Date endDate = requestEndDate != null ? DateUtils.toDate(requestEndDate.getRequestValue(), "yyyyMMdd") : null;
		LogUtils.debug(getClass(), "Requested End Date: " + endDate);

		Map<Date, ReferenceDataResponse> historicalDataResponseMap = null;
		try {
			historicalDataResponseMap = getSqlHandler().executeSelect(getSelectCommand(fields, securities, investmentType, startDate, endDate), getReferenceDataMapResultSetExtractor(fields));
		}
		catch (BadSqlGrammarException bsge) {
			handleBadGrammarException(bsge);
		}
		HistoricalDataResponse responseMessage = new HistoricalDataResponse();
		responseMessage.setValues(historicalDataResponseMap);
		LogUtils.debug(getClass(), "Returning Historical Data Response");

		return responseMessage;
	}


	@Override
	public void start() {
		if (!isSessionOpen()) {
			LogUtils.info(getClass(), "Starting Hive Session");
		}
	}


	@Override
	public void stop() {
		LogUtils.info(getClass(), "Stopping Hive connection.");
	}


	///////////////////////////////////////////////////////////////////////////
	//////////////              Helper methods                  ///////////////
	///////////////////////////////////////////////////////////////////////////
	private ResultSetExtractor<Map<Date, ReferenceDataResponse>> getReferenceDataMapResultSetExtractor(List<String> fields) {
		return resultSet -> {
			Map<Integer, DataTypeNames> columnTypeMap = getColumnDataTypeMap(resultSet, fields);

			Map<Date, ReferenceDataResponse> responseMap = new HashMap<>();
			while (resultSet.next()) {
				Date date = resultSet.getDate("FILE_DATE");
				String security = resultSet.getString("INTERNAL_IDENTIFIER");

				ReferenceDataResponse referenceDataResponse = responseMap.computeIfAbsent(date, dateKey -> new ReferenceDataResponse());
				if (referenceDataResponse == null) {
					referenceDataResponse = responseMap.computeIfAbsent(date, k -> new ReferenceDataResponse());
				}

				Map<String, Object> securityFieldValueMap = referenceDataResponse.addSecurity(security);

				for (int i = 1; i < fields.size() + 1; i++) {
					securityFieldValueMap.put(fields.get(i - 1), getFieldValue(resultSet, i, columnTypeMap));
				}
				LogUtils.debug(LogCommand.ofMessageSupplier(this.getClass(), () -> "Processed result for security: " + security + " for date: " + date));
				LogUtils.trace(LogCommand.ofMessageSupplier(getClass(), () -> "Field Values: " + securityFieldValueMap.toString()));
			}
			return responseMap;
		};
	}


	private List<String> getFieldList(BloombergRequestMessage bloombergRequest) {
		List<String> fields = new ArrayList<>();
		fields.add("FILE_DATE");
		fields.add("INTERNAL_IDENTIFIER");
		fields.addAll(bloombergRequest.getBloombergRequestData().getElement("fields").getRequestData().stream().filter(field -> !StringUtils.isEmpty(field)).collect(Collectors.toList()));
		LogUtils.debug(LogCommand.ofMessageSupplier(this.getClass(), () -> "Fields requested: " + StringUtils.collectionToCommaDelimitedString(fields)));
		return fields;
	}


	private SqlSelectCommand getSelectCommand(List<String> fields, List<String> securities, String investmentType, Date startDate, Date endDate) {
		final String validFieldCharacterRegex = "^[a-zA-Z0-9_]*$";

		//Parameterizes the list of securities for the IN clause
		StringBuilder securityParameterizationBuilder = new StringBuilder();
		for (String security : securities) {
			if (!StringUtils.isEmpty(security)) {
				securityParameterizationBuilder.append("?,");
			}
		}
		String inClauseQuestionMarks = StringUtils.substringBeforeLast(securityParameterizationBuilder.toString(), ",");

		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(StringUtils.collectionToCommaDelimitedString(fields, from -> {
			if (from.matches(validFieldCharacterRegex)) {
				return from;
			}
			else {
				throw new ValidationException("Invalid field requested from Hive: " + from);
			}
		}));
		command.setFromClause(getTableToQueryUsingInvestmentType(investmentType));
		command.setWhereClause("FILE_DATE BETWEEN ? AND ? AND INTERNAL_IDENTIFIER IN (" + inClauseQuestionMarks + ")");

		command.addDateParameterValue(startDate);
		command.addDateParameterValue(endDate);
		for (String security : securities) {
			if (!StringUtils.isEmpty(security)) {
				command.addStringParameterValue(security);
			}
		}

		return command;
	}


	private String getTableToQueryUsingInvestmentType(String investmentType) {
		ValidationUtils.assertNotEmpty(investmentType, "Hive table could not be located because no InvestmentType was passed");
		String fromClause = getInvestmentTypeTableNameCache().get(investmentType);
		ValidationUtils.assertNotEmpty(fromClause, "Corresponding Hive table not found for requested InvestmentType: " + investmentType);
		return "bloomberg." + fromClause;
	}


	private Object getFieldValue(ResultSet hiveResults, Integer columnNumber, Map<Integer, DataTypeNames> columnTypeMap) throws SQLException {
		switch (columnTypeMap.get(columnNumber)) {
			case DATE:
				return hiveResults.getDate(columnNumber);
			case DECIMAL:
				return hiveResults.getBigDecimal(columnNumber);
			case INTEGER:
				return hiveResults.getInt(columnNumber);
			case STRING:
			case TEXT:
				return hiveResults.getString(columnNumber);
			case TIME:
				return hiveResults.getTime(columnNumber);
			case BOOLEAN:
				return hiveResults.getBoolean(columnNumber);
			case BINARY:
				return hiveResults.getBinaryStream(columnNumber);
			default:
				throw new ValidationException("Data type not found");
		}
	}


	private Map<Integer, DataTypeNames> getColumnDataTypeMap(ResultSet hiveResults, List<String> fields) throws SQLException {
		Map<Integer, DataTypeNames> columnTypeMap = new HashMap<>();

		for (int i = 1; i < fields.size() + 1; i++) {
			columnTypeMap.put(i, DataTypeNames.valueOf(hiveResults.getMetaData().getColumnTypeName(i).toUpperCase()));
		}

		return columnTypeMap;
	}


	private Map<String, String> getInvestmentTypeTableNameCache() {
		Map<String, String> cache = getCacheHandler().get(INVESTMENT_TYPE_TABLE_NAME_CACHE_NAME, INVESTMENT_TYPE_TABLE_NAME_CACHE_KEY);
		if (cache == null) {
			LogUtils.debug(getClass(), "Cache is empty. Populating Cache from Hive");
			cache = getInvestmentTypeTableNameMappings();
			getCacheHandler().put(INVESTMENT_TYPE_TABLE_NAME_CACHE_NAME, INVESTMENT_TYPE_TABLE_NAME_CACHE_KEY, cache);
		}
		return cache;
	}


	/**
	 * Pulls the latest mappings for Investment Type to Hive Table
	 */
	private Map<String, String> getInvestmentTypeTableNameMappings() {
		LogUtils.debug(getClass(), "Getting Investment Type Table Name Map from Hive");
		Map<String, String> investmentTypeTableNameMap = null;

		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause("investment_type, table_name");
		command.setFromClause("CLIFTON.InvestmentTypeTableName");
		try {
			ResultSetExtractor<Map<String, String>> extractor = resultSet -> {
				Map<String, String> typeTableMap = new HashMap<>();
				while (resultSet.next()) {
					typeTableMap.put(resultSet.getString("investment_type"), resultSet.getString("table_name"));
				}
				return typeTableMap;
			};
			investmentTypeTableNameMap = getSqlHandler().executeSelect(command, extractor);
			LogUtils.info(getClass(), "Investment Type Table Name Map retrieved from Hive");
			LogUtils.debug(LogCommand.ofMessageSupplier(getClass(), investmentTypeTableNameMap::toString));
		}
		catch (BadSqlGrammarException bsge) {
			handleBadGrammarException(bsge);
		}

		if (investmentTypeTableNameMap == null) {
			throw new RuntimeException("Investment Type Table Name Cache could not be populated");
		}

		return investmentTypeTableNameMap;
	}


	private void handleBadGrammarException(BadSqlGrammarException bsge) {
		SQLException sqlException = bsge.getSQLException();
		if (sqlException.getErrorCode() == 10004 && "42000".equals(sqlException.getSQLState())) {
			//Pattern to pull out the invalid column or table
			Pattern p = Pattern.compile("(?<=Invalid table alias or column reference ')(.*)(?=':)");
			Matcher m = p.matcher(sqlException.getMessage());
			if (m.find()) {
				throw new ValidationException("Unsupported field(s) requested: " + m.group(1));
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public CacheHandler<String, Map<String, String>> getCacheHandler() {
		return this.cacheHandler;
	}
}
