package com.clifton.bloomberg.server;

import com.clifton.bloomberg.messages.request.SubscriptionRequest;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.bloomberg.server.session.BloombergSessionManager;
import com.clifton.bloomberg.server.util.BloombergUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;


/**
 * <code>BloombergServerSubscriptionAsynchronousProcessor</code> is a server side {@link AsynchronousProcessor} for
 * consuming {@link SubscriptionRequest} messages.
 *
 * @author NickK
 */
public class BloombergServerSubscriptionAsynchronousProcessor implements AsynchronousProcessor<SubscriptionRequest> {

	private BloombergSessionManager bloombergSessionManager;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void process(SubscriptionRequest message, AsynchronousMessageHandler handler) {
		try {
			BloombergResponseMessage response = getBloombergSessionManager().getSession(message).submitSynchronousRequest(message);
			if (response instanceof SubscriptionDataResponse) {
				handler.send((SubscriptionDataResponse) response);
			}
			else {
				LogUtils.warn(getClass(), "Received unexpected response type for Subscription Request {" + response.getClass().getName() + "}");
			}
		}
		catch (Exception e) {
			String errorMessage = "Failed to process Bloomberg Subscription Request [" + message + "]";
			LogUtils.warn(getClass(), errorMessage, e);
			SubscriptionDataResponse response = new SubscriptionDataResponse();
			response.getProperties().putAll(message.getProperties());
			response.setError(BloombergUtils.convertBloombergExceptionForMessaging(errorMessage, e));
			handler.send(response);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergSessionManager getBloombergSessionManager() {
		return this.bloombergSessionManager;
	}


	public void setBloombergSessionManager(BloombergSessionManager bloombergSessionManager) {
		this.bloombergSessionManager = bloombergSessionManager;
	}
}
