package com.clifton.bloomberg.server.util;

import com.bloomberglp.blpapi.Datetime;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Event;
import com.bloomberglp.blpapi.EventQueue;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.Name;
import com.bloomberglp.blpapi.Schema;
import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.BloombergSequenceElement;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.server.BloombergProcessorImpl;
import com.clifton.bloomberg.server.BloombergRequestHandler;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.converter.string.ObjectToStringConverter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <code>BloombergUtils</code> contains constants and utility methods for interacting with
 * requesting and retrieving Bloomberg data.
 *
 * @author NickK
 */
public class BloombergUtils {

	private static final Converter<Object, String> OBJECT_STRING_CONVERTER = new ObjectToStringConverter();

	///////////////////////////////////////////////////////////////////////////
	/////////////////              Constants               ////////////////////
	///////////////////////////////////////////////////////////////////////////

	//Services
	public static final String API_AUTH_SVC_NAME = "//blp/apiauth";

	// Security and Field
	public static final Name SECURITY_DATA = Name.getName("securityData");
	public static final Name SECURITY = Name.getName("security");
	public static final Name SECURITY_ERROR = new Name("securityError");
	public static final Name FIELD_DATA = new Name("fieldData");
	public static final Name FIELD_EXCEPTIONS = new Name("fieldExceptions");
	public static final Name FIELD_ID = new Name("fieldId");
	public static final Name DATE = new Name("date");

	// Subscription
	public static final Name SUBSCRIPTION_STARTED = Name.getName("SubscriptionStarted");
	public static final Name SUBSCRIPTION_FAILURE = Name.getName("SubscriptionFailure");
	public static final Name SUBSCRIPTION_TERMINATED = Name.getName("SubscriptionTerminated");
	public static final Name SUBSCRIPTION_STREAMS_ACTIVATED = Name.getName("SubscriptionStreamsActivated");
	public static final Name SUBSCRIPTION_STREAMS_DEACTIVATED = Name.getName("SubscriptionStreamsDeactivated");
	public static final Name MARKET_DATA_EVENTS = Name.getName("MarketDataEvents");
	public static final Name MARKET_DATA_EVENT_TYPE = Name.getName("MKTDATA_EVENT_TYPE");
	public static final Name MARKET_DATA_EVENT_SUBTYPE = Name.getName("MKTDATA_EVENT_SUBTYPE");

	// Authentication and Authorization
	public static final Name TOKEN_SUCCESS = Name.getName("TokenGenerationSuccess");
	public static final Name TOKEN = Name.getName("token");
	public static final Name AUTHORIZATION_SUCCESS = Name.getName("AuthorizationSuccess");
	public static final Name AUTHORIZATION_FAILURE = Name.getName("AuthorizationFailure");
	public static final Name AUTHORIZATION_REVOKED = Name.getName("AuthorizationRevoked");
	public static final Name ENTITLEMENT_CHANGED = Name.getName("EntitlementChanged");
	public static final Name ENTITLEMENT_ID_DATA = Name.getName("eidData");
	public static final Name REQUEST_FAILURE = Name.getName("RequestFailure");

	// Session, Service, Admin and Request
	public static final Name SESSION_TERMINATED = Name.getName("SessionTerminated");
	public static final Name SLOW_CONSUMER_WARNING = Name.getName("SlowConsumerWarning");
	public static final Name SLOW_CONSUMER_WARNING_CLEARED = Name.getName("SlowConsumerWarningCleared");
	public static final Name DATA_LOSS = Name.getName("DataLoss");
	public static final Name REQUEST_RESPONSE_ERROR = Name.getName("ResponseError");

	// Error or Details
	public static final Name RESPONSE_ERROR = Name.getName("responseError");
	public static final Name ERROR_INFO = new Name("errorInfo");
	public static final Name REASON = Name.getName("reason");
	public static final Name SOURCE = Name.getName("source");
	public static final Name CATEGORY = Name.getName("category");
	public static final Name SUBCATEGORY = Name.getName("subcategory");
	public static final Name CODE = Name.getName("code");
	public static final Name ERROR_CODE = Name.getName("errorCode");
	public static final Name MESSAGE = Name.getName("message");
	public static final Name DESCRIPTION = Name.getName("description");


	private static final String BBCOMM_PROCESS = "bbcomm.exe";
	private static final String BBCOMM_FOLDER = "C:/blp/DAPI";

	///////////////////////////////////////////////////////////////////////////
	/////////////////              Methods                 ////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Start the bbcomm application needed to use the API.  If an instance is already running, bbcomm will detect it and not start a second instance.
	 * See the bbcomm log file typical in c:\WINDOWS\Temp\Bloomberg\Log\bbcomm.log or c:\blp\api\bbcomm.log.
	 */
	public static void startBBComm() {
		Pattern readyPattern = Pattern.compile("ready", Pattern.CASE_INSENSITIVE);
		BufferedReader bufferReader = null;
		try {
			Process bbcomm = new ProcessBuilder(FileUtils.combinePath(BBCOMM_FOLDER, BBCOMM_PROCESS)).start();
			bufferReader = new BufferedReader(new InputStreamReader(bbcomm.getInputStream()));
			String line;
			// if bbcomm was already running, this process will close, and the input stream will close
			// otherwise, it will eventually output a line:
			// BLOOMBERG COMMUNICATION SERVER READY on Port: whatever
			int safetyCounter = 0;
			while ((line = bufferReader.readLine()) != null) {
				LogUtils.info(BloombergProcessorImpl.class, "BBCOMM START: " + line);

				Matcher readyMatcher = readyPattern.matcher(line);
				if (readyMatcher.find()) {
					break;
				}

				safetyCounter++;
				if (safetyCounter > 1000) {
					throw new RuntimeException("Failed to verify that the bbcomm process failed after reading [" + safetyCounter + "] lines from the process output.");
				}
			}
		}
		catch (Exception e) {
			LogUtils.error(BloombergUtils.class, "bbcomm init failed: %s", e);
		}
		finally {
			if (bufferReader != null) {
				try {
					bufferReader.close();
				}
				catch (IOException e) {
					LogUtils.error(BloombergProcessorImpl.class, "Failed to close bbcomm BufferedReader.");
				}
			}
		}
	}


	/**
	 * Listens on the provided {@link EventQueue} to process the result(s) of the provided {@link BloombergRequestMessage}.
	 * The {@link BloombergRequestHandler} will be used to process the response(s).
	 *
	 * @throws BloombergRequestException if the request fails
	 */
	public static BloombergResponseMessage handleSynchronousSingleRequest(EventQueue eventQueue, BloombergRequestHandler<BloombergResponseMessage> bloombergRequestHandler, BloombergRequestMessage requestMessage) throws Exception {
		BloombergResponseMessage responseMessage = null;
		while (true) {
			Event event = eventQueue.nextEvent(requestMessage.getRequestTimeout() != null ? requestMessage.getRequestTimeout() - 200 : 2800);
			if (event.eventType() == Event.EventType.PARTIAL_RESPONSE) {
				LogUtils.debug(BloombergUtils.class, "Processing Partial Response: " + event);
				responseMessage = processResponseEvent(event, bloombergRequestHandler, requestMessage, responseMessage);
			}
			else if (event.eventType() == Event.EventType.RESPONSE) {
				LogUtils.debug(BloombergUtils.class, "Processing Response: " + event);
				return processResponseEvent(event, bloombergRequestHandler, requestMessage, responseMessage);
			}
			else if (event.eventType() == Event.EventType.TIMEOUT) {
				throw new RuntimeException("Failed to complete request in [" + requestMessage.getRequestTimeout() + "].");
			}
			else {
				for (Message message : event) {
					LogUtils.debug(BloombergUtils.class, message.toString());
					if (event.eventType() == Event.EventType.SESSION_STATUS) {
						if ("SessionTerminated".equals(message.messageType().toString())) {
							break;
						}
					}
				}
			}
		}
	}


	/**
	 * Processes the provided response event using the {@link BloombergRequestHandler}. If the {@link BloombergResponseMessage} is null, a new one will be created.
	 * To aggregate partial response events, use the previously returned response message should be provided.
	 *
	 * @throws BloombergRequestException if the request fails
	 */
	private static BloombergResponseMessage processResponseEvent(Event event, BloombergRequestHandler<BloombergResponseMessage> bloombergRequestHandler, BloombergRequestMessage requestMessage, BloombergResponseMessage responseMessage) {
		for (Message message : event) {
			if (message.hasElement(RESPONSE_ERROR)) {
				BloombergError bError = BloombergUtils.populateErrorIfExists(message.getElement(RESPONSE_ERROR));
				throw new BloombergRequestException(bError);
			}
			responseMessage = bloombergRequestHandler.processPartialResponse(message, requestMessage, responseMessage);
		}
		return responseMessage;
	}


	/**
	 * Returns a {@link BloombergError} with populated details from the provided {@link Element}.
	 * Returns null if the element is not recognized as {@link #SECURITY_ERROR}, {@link #FIELD_EXCEPTIONS},
	 * or {@link #RESPONSE_ERROR}.
	 */
	public static BloombergError populateErrorIfExists(Element rootElement) {
		BloombergError error = null;
		if (rootElement.hasElement(SECURITY_ERROR)) {
			Element securityError = rootElement.getElement(SECURITY_ERROR);
			error = new BloombergError();
			error.setErrorType(BloombergError.BloombergErrorTypes.SECURITY);
			error.setErrorField(rootElement.getElementAsString(SECURITY));
			setErrorInfoFields(securityError, error);
		}
		else if (rootElement.hasElement(FIELD_EXCEPTIONS)) {
			Element fieldExceptionsArray = rootElement.getElement(FIELD_EXCEPTIONS);
			if (fieldExceptionsArray.numValues() > 0) {
				Element fieldException = fieldExceptionsArray.getValueAsElement(0);//TODO: for now only setting first field error, have to think about how to do multiple
				error = new BloombergError();
				error.setErrorType(BloombergError.BloombergErrorTypes.FIELD);
				error.setErrorField(fieldException.getElementAsString(FIELD_ID));
				Element errorInfo = fieldException.getElement(ERROR_INFO);
				setErrorInfoFields(errorInfo, error);
			}
		}
		else if (RESPONSE_ERROR.equals(rootElement.name())) {
			error = new BloombergError();
			error.setErrorType(BloombergError.BloombergErrorTypes.RESPONSE);
			setErrorInfoFields(rootElement, error);
		}

		if (error != null) {
			if (rootElement.hasElement(SECURITY)) {
				error.setSecurity(rootElement.getElement(SECURITY).getValueAsString());
			}
		}
		return error;
	}


	public static void setErrorInfoFields(Element errorInfo, BloombergError error) {
		error.setSource(OBJECT_STRING_CONVERTER.convert(readValue(errorInfo.getElement(SOURCE))));
		error.setCategory(OBJECT_STRING_CONVERTER.convert(readValue(errorInfo.getElement(CATEGORY))));
		if (errorInfo.hasElement(CODE)) {
			error.setCode(OBJECT_STRING_CONVERTER.convert(readValue(errorInfo.getElement(CODE))));
		}
		else if (errorInfo.hasElement(ERROR_CODE)) {
			error.setCode(OBJECT_STRING_CONVERTER.convert(readValue(errorInfo.getElement(ERROR_CODE))));
		}
		if (errorInfo.hasElement(MESSAGE)) {
			error.setMessage(OBJECT_STRING_CONVERTER.convert(readValue(errorInfo.getElement(MESSAGE))));
		}
		else if (errorInfo.hasElement(DESCRIPTION)) {
			error.setMessage(OBJECT_STRING_CONVERTER.convert(readValue(errorInfo.getElement(DESCRIPTION))));
		}
		if (errorInfo.hasElement(SUBCATEGORY)) {
			error.setSubcategory(OBJECT_STRING_CONVERTER.convert(readValue(errorInfo.getElement(SUBCATEGORY))));
		}
	}


	/**
	 * Returns the value for the provided {@link Element} based on the elements datatype.
	 *
	 * @see Element#datatype()
	 */
	public static Object readValue(Element element) {
		if (element.isNull()) {
			return null;
		}
		Schema.Datatype type = element.datatype();
		switch (type.intValue()) {
			case Schema.Datatype.Constants.SEQUENCE:
				List<BloombergSequenceElement> returnList = new ArrayList<>();
				for (int i = 0; i < element.numValues(); ++i) {
					Element valueData = element.getValueAsElement(i);
					BloombergSequenceElement seqElement = new BloombergSequenceElement();
					for (int j = 0; j < valueData.numElements(); ++j) {
						Element valueProperty = valueData.getElement(j);
						seqElement.add(valueProperty.name().toString(), readValue(valueProperty));
					}
					returnList.add(seqElement);
				}
				return returnList;
			case Schema.Datatype.Constants.STRING:
				return element.getValueAsString();
			case Schema.Datatype.Constants.FLOAT32:
				return element.getValueAsFloat32();
			case Schema.Datatype.Constants.FLOAT64:
				return element.getValueAsFloat64();
			case Schema.Datatype.Constants.INT32:
				return element.getValueAsInt32();
			case Schema.Datatype.Constants.INT64:
				return element.getValueAsInt64();
			case Schema.Datatype.Constants.BOOL:
				return element.getValueAsBool();
			case Schema.Datatype.Constants.BYTEARRAY:
				return element.getValueAsBytes();
			case Schema.Datatype.Constants.CHAR:
				return Character.toString(element.getValueAsChar()); //note that since we are placing this in a map, that it needs to be an object, not a char
			case Schema.Datatype.Constants.DATE:
			case Schema.Datatype.Constants.TIME:
			case Schema.Datatype.Constants.DATETIME: {
				Datetime date = element.getValueAsDatetime();
				return date.calendar().getTime();
			}
			case Schema.Datatype.Constants.ENUMERATION:
				return element.getValueAsEnumeration();
			default:
				return null;
		}
	}


	/**
	 * Replaces Bloomberg exceptions with generic ones that can be included in messages without
	 * causing class not found issues on the client side.
	 *
	 * @param message   the message to prefix a newly created exception with if the provided exception is from Bloomberg
	 * @param exception the Exception to convert
	 */
	public static Exception convertBloombergExceptionForMessaging(String message, Exception exception) {
		if ("com.bloomberglp.blpapi".equals(exception.getClass().getPackage().getName())) {
			String causeMessage = StringUtils.isEmpty(exception.getMessage()) ? exception.getClass().getSimpleName() : exception.getMessage();
			return new RuntimeException(message + ": " + causeMessage);
		}
		return exception;
	}
}
