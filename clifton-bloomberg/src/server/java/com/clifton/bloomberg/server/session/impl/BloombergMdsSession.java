package com.clifton.bloomberg.server.session.impl;

import com.clifton.bloomberg.messages.request.BloombergRequestElement;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.request.HistoricalDataRequest;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.bloomberg.server.session.BloombergSessionOptions;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.mds.api.client.BloombergMarketDataApi;
import com.clifton.mds.api.client.model.Frequency;
import com.clifton.mds.api.client.model.HistoricalSecurityScheduler;
import com.clifton.mds.api.client.model.Period;
import com.clifton.mds.api.client.model.QueryResponseSecurityDto;
import com.clifton.mds.api.client.model.SecurityDto;
import com.clifton.mds.api.client.model.SecurityFieldDto;
import com.clifton.mds.api.client.model.SecurityScheduler;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Represents an interface to the Market Data Service
 *
 * @author paulj
 */
public class BloombergMdsSession extends BloombergDefaultSession {

	public static final String SESSION_NAME = "MDS";
	public static final String TICKER = "TICKER";

	private BloombergMarketDataApi bloombergMarketDataApi;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergMdsSession(BloombergSessionOptions bloombergSessionOptions, BloombergMarketDataApi bloombergMarketDataApi) {
		super(bloombergSessionOptions);
		setBloombergMarketDataApi(bloombergMarketDataApi);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void preStart() {
		// Overridden to prevent BBCOMM start in parent
	}


	@Override
	public void start() {
		LogUtils.info(getClass(), "Starting MDS Session");
	}


	@Override
	public void stop() {
		LogUtils.info(getClass(), "Stopping MDS session.");
	}


	@Override
	public BloombergResponseMessage submitSynchronousRequest(BloombergRequestMessage bloombergRequest) {
		start();

		if (!(bloombergRequest instanceof HistoricalDataRequest) && !(bloombergRequest instanceof ReferenceDataRequest)) {
			throw new ValidationException("Cannot process message. Request not an instance of HistoricalDataRequest or ReferenceDataRequest.");
		}

		List<String> fields = getFieldList(bloombergRequest);

		List<String> securities = bloombergRequest.getBloombergRequestData().getElement(BloombergRequestMessage.SECURITIES_KEY).getRequestData();
		LogUtils.debug(LogCommand.ofMessageSupplier(getClass(), () -> "Securities being requested: " + StringUtils.collectionToCommaDelimitedString(securities)));

		BloombergRequestElement requestStartDate = bloombergRequest.getBloombergRequestData().getElement("startDate");
		BloombergRequestElement requestEndDate = bloombergRequest.getBloombergRequestData().getElement("endDate");
		Date scheduleDate = bloombergRequest.getScheduleDate();
		String scheduleTime = bloombergRequest.getScheduleTime();

		Date startDate = requestStartDate != null ? DateUtils.toDate(requestStartDate.getRequestValue(), "yyyyMMdd") : null;
		LogUtils.debug(getClass(), "Requested Start Date: " + startDate);

		Date endDate = requestEndDate != null ? DateUtils.toDate(requestEndDate.getRequestValue(), "yyyyMMdd") : null;
		LogUtils.debug(getClass(), "Requested End Date: " + endDate);

		LogUtils.debug(getClass(), "Requested Schedule Date: " + scheduleDate);
		LogUtils.debug(getClass(), "Requested Schedule Date: " + scheduleTime);

		if (bloombergRequest instanceof HistoricalDataRequest) {
			return (scheduleDate == null && scheduleTime == null) ? getHistoricalMarketDataServiceData(fields, securities, startDate, endDate) :
					scheduleHistoricalMarketDataService(fields, securities, scheduleDate, scheduleTime, startDate, endDate);
		}
		else {
			return (scheduleDate == null && scheduleTime == null) ? getMarketDataServiceData(fields, securities) :
					scheduleMarketDataService(fields, securities, scheduleDate, scheduleTime);
		}
	}


	private ReferenceDataResponse getMarketDataServiceData(List<String> fields, List<String> securities) {
		QueryResponseSecurityDto dtoResponse = getBloombergMarketDataApi().getLatestSecurityMarketDataUsingGET(fields, TICKER, securities, false);
		return processDtoResponse(dtoResponse, new Date());
	}


	private HistoricalDataResponse getHistoricalMarketDataServiceData(List<String> fields, List<String> securities, Date beginDate, Date endDate) {
		HistoricalDataResponse historicalResponse = new HistoricalDataResponse();
		Map<Date, ReferenceDataResponse> historicalValues = new HashMap<>();

		for (LocalDate currentDate = DateUtils.asLocalDate(beginDate); currentDate.isBefore(DateUtils.asLocalDate(endDate)); currentDate = currentDate.plusDays(1)) {
			QueryResponseSecurityDto dtoResponse = getBloombergMarketDataApi().getHistoricalSecurityMarketDataUsingGET(currentDate, fields, TICKER, securities);
			historicalValues.put(DateUtils.asUtilDate(currentDate), processDtoResponse(dtoResponse, DateUtils.asUtilDate(currentDate)));
		}
		historicalResponse.setValues(historicalValues);
		return historicalResponse;
	}


	private ReferenceDataResponse scheduleMarketDataService(List<String> fields, List<String> securities, Date startDate, String startTime) {
		SecurityScheduler securityScheduler = new SecurityScheduler();
		securityScheduler.setFields(fields);
		securityScheduler.setSecurities(securities);
		securityScheduler.setIdentifierType(TICKER);
		securityScheduler.setStartDate(DateUtils.asLocalDate(startDate != null ? startDate : new Date()));
		securityScheduler.setStartTime(startTime != null ? startTime : "00:00:00");
		securityScheduler.setFrequency(Frequency.ONCE);

		ResponseEntity<Void> responseEntity = getBloombergMarketDataApi().scheduleMarketDataRequestUsingPOSTWithHttpInfo(securityScheduler);
		if (responseEntity.getStatusCodeValue() != 200) {
			throw new RuntimeException("Invalid response when scheduling reference MDS retrieval. Status Code = " + responseEntity.getStatusCodeValue());
		}

		return processSchedulingResponse(fields, securities, startDate, startTime);
	}


	private HistoricalDataResponse scheduleHistoricalMarketDataService(List<String> fields, List<String> securities, Date startDate, String startTime, Date beginDate, Date endDate) {
		HistoricalSecurityScheduler historicalSecurityScheduler = new HistoricalSecurityScheduler();
		historicalSecurityScheduler.setFields(fields);
		historicalSecurityScheduler.setPeriod(Period.DAILY);
		historicalSecurityScheduler.setSecurities(securities);
		historicalSecurityScheduler.setStartDate(DateUtils.asLocalDate(startDate != null ? startDate : new Date()));
		historicalSecurityScheduler.setIdentifierType(TICKER);
		historicalSecurityScheduler.setStartTime(startTime != null ? startTime : "00:00:00");
		historicalSecurityScheduler.setFrequency(Frequency.ONCE);
		historicalSecurityScheduler.setPeriodStartDate(DateUtils.asLocalDate(beginDate));
		historicalSecurityScheduler.setPeriodEndDate(DateUtils.asLocalDate(endDate));

		ResponseEntity<Void> responseEntity = getBloombergMarketDataApi().scheduleMarketDataHistoricalRequestUsingPOSTWithHttpInfo(historicalSecurityScheduler);
		if (responseEntity.getStatusCodeValue() != 200) {
			throw new RuntimeException("Invalid response when scheduling historical MDS retrieval. Status Code = " + responseEntity.getStatusCodeValue());
		}

		HistoricalDataResponse historicalResponse = new HistoricalDataResponse();
		Map<Date, ReferenceDataResponse> historicalValues = new HashMap<>();

		for (LocalDate currentDate = DateUtils.asLocalDate(beginDate); currentDate.isBefore(DateUtils.asLocalDate(endDate)); currentDate = currentDate.plusDays(1)) {
			historicalValues.put(DateUtils.asUtilDate(currentDate), processSchedulingResponse(fields, securities, startDate, startTime));
		}
		historicalResponse.setValues(historicalValues);

		return historicalResponse;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BloombergMarketDataApi getBloombergMarketDataApi() {
		return this.bloombergMarketDataApi;
	}


	public void setBloombergMarketDataApi(BloombergMarketDataApi bloombergMarketDataApi) {
		this.bloombergMarketDataApi = bloombergMarketDataApi;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	private List<String> getFieldList(BloombergRequestMessage bloombergRequest) {
		List<String> fields = new ArrayList<>();
		fields.addAll(bloombergRequest.getBloombergRequestData().getElement("fields").getRequestData().stream().filter(field -> !StringUtils.isEmpty(field)).collect(Collectors.toList()));
		LogUtils.debug(LogCommand.ofMessageSupplier(this.getClass(), () -> "Fields requested: " + StringUtils.collectionToCommaDelimitedString(fields)));
		return fields;
	}


	private ReferenceDataResponse processDtoResponse(QueryResponseSecurityDto dtoResponse, Date currentDate) {
		if (dtoResponse == null) {
			throw new RuntimeException("Unable to retrieve from MDS.");
		}
		if (dtoResponse.getResponseCode() != 0) {
			throw new RuntimeException("Unable to retrieve from MDS. Response code = " + dtoResponse.getResponseCode());
		}
		if (dtoResponse.getResultSet().isEmpty()) {
			throw new RuntimeException("No results returned from MDS for date = " + currentDate);
		}
		ReferenceDataResponse response = new ReferenceDataResponse();
		Map<String, Map<String, Object>> values = new HashMap<>();
		for (SecurityDto securityDto : dtoResponse.getResultSet()) {
			Map<String, Object> securityFields = new HashMap<>();
			for (SecurityFieldDto securityFieldDto : securityDto.getFields()) {
				securityFields.put(securityFieldDto.getName(), securityFieldDto.getValue());
			}
			values.put(securityDto.getKey(), securityFields);
		}
		response.setValues(values);

		return response;
	}


	private ReferenceDataResponse processSchedulingResponse(List<String> fields, List<String> securities, Date startDate, String startTime) {
		ReferenceDataResponse response = new ReferenceDataResponse();
		Map<String, Map<String, Object>> values = new HashMap<>();
		for (String security : securities) {
			Map<String, Object> securityFields = new HashMap<>();
			for (String securityField : fields) {
				securityFields.put(securityField, "Scheduled for " + startDate + " at " + startTime + " PST.");
			}
			values.put(security, securityFields);
		}
		response.setValues(values);

		return response;
	}
}
