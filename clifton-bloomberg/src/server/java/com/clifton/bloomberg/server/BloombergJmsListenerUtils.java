package com.clifton.bloomberg.server;


import com.clifton.core.logging.LogUtils;
import org.springframework.jms.listener.DefaultMessageListenerContainer;


/**
 * The <code>BloombergJmsListenerUtils</code> utility class used to stop the JMS listener
 * and restart it after a specific amount of time.
 *
 * @author mwacker
 */
public class BloombergJmsListenerUtils {

	/**
	 * Creates a new thread that will stop the JMS listener and schedule the restart.
	 * <p>
	 * NOTE: A new thread is need or the jms listener shutdown will not complete correctly because the current
	 * thread needs to complete it's processing for the message listener to close.
	 *
	 * @param waitTime - number of seconds to wait before restarting the jms listener
	 */
	public static void pauseListener(final DefaultMessageListenerContainer jmsListener, final int waitTime) {
		Thread thread = new Thread(() -> {
			// shutdown the jms listener
			jmsListener.shutdown();

			int count = 0;
			while (count < waitTime) {
				try {
					Thread.sleep(1000);
					count++;
				}
				catch (InterruptedException e) {
					LogUtils.error(BloombergJmsListenerUtils.class, "JMS listener pause thread interrupted before wait time expired.", e);
					Thread.currentThread().interrupt();
					break;
				}
			}

			LogUtils.error(BloombergJmsListenerUtils.class, "Starting JMS listening after waiting [" + waitTime + "] seconds.");
			jmsListener.initialize();
			jmsListener.start();
		}


		);

		thread.start();
	}
}
