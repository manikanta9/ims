package com.clifton.bloomberg.server.session;

import com.bloomberglp.blpapi.CorrelationID;


/**
 * <code>BloombergCorrelationAware</code> is an interface for objects that have {@link CorrelationID}s that must be
 * managed by the application to properly create and invalidate Bloomberg authentication or authorization items.
 * <p>
 * Any user or application request directed at Bloomberg B-Pipe must be authenticated and have a valid {@link com.bloomberglp.blpapi.Identity}
 * and {@link CorrelationID}. The identity and correlation ID can be reused for the life of a session and should be properly cleared when no longer
 * used. Authenticating too frequently for a user, can produce issues with authentication session limits.
 *
 * @author NickK
 */
public interface BloombergCorrelationAware {

	/**
	 * Get the active {@link CorrelationID} for the object. The result should be null if there is no valid correlation with Bloomberg.
	 */
	CorrelationID getCorrelationId();


	/**
	 * Set the {@link CorrelationID} for the object. Pass null to symbolize the object has no valid correlation with Bloomberg.
	 */
	void setCorrelationId(CorrelationID correlationId);
}
