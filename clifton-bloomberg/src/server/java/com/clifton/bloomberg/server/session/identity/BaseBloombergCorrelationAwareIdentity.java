package com.clifton.bloomberg.server.session.identity;

import com.bloomberglp.blpapi.CorrelationID;
import com.clifton.bloomberg.server.session.BloombergCorrelationAware;


/**
 * <code>BaseBloombergCorrelationAwareIdentity</code> is a base class for any {@link BloombergCorrelationAware}.
 *
 * @author NickK
 */
public abstract class BaseBloombergCorrelationAwareIdentity implements BloombergCorrelationAware {

	private CorrelationID correlationId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public CorrelationID getCorrelationId() {
		return this.correlationId;
	}


	@Override
	public void setCorrelationId(CorrelationID correlationId) {
		this.correlationId = correlationId;
	}
}
