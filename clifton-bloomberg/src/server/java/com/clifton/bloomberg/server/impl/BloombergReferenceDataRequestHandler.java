package com.clifton.bloomberg.server.impl;


import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Message;
import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.bloomberg.server.util.BloombergUtils;
import com.clifton.core.logging.LogUtils;

import java.util.Map;


/**
 * The <code>BloombergReferenceDataRequestHandler</code> handles the following ReferenceDataResponse model.
 * <ReferenceDataResponse>
 * <responseError attributes (source,code, category, message,subcategory)> occurs 0-1 times
 * <securityData attributes (security,sequence number)> occurs * times (once for each requested security)
 * <fieldData attributes (value)> occurs * times (once for each requested field)
 * <fieldExceptions attributes (fieldId, message)> occurs 0-1 times
 * <errorInfo attributes (source,code,category,message,subcategory)>
 * </fieldExceptions>
 * <securityError attributes (source,code,category,message,subcategory)> occurs 0-* times
 * </securityData>
 * </ReferenceDataResponse>
 * <p/>
 * Given a request for "EURUSD Curncy" for fields "BID, ASK, ID_CUSIP, PX_LAST, TICKER" this is the response.
 * securityData[] = {
 * securityData = {
 * security = EURUSD Curncy
 * fieldExceptions[] = {
 * fieldExceptions = {
 * fieldId = ID_CUSIP
 * errorInfo = {
 * source = 164::bbdbd1
 * code = 9
 * category = BAD_FLD
 * message = Field not applicable to security
 * subcategory = NOT_APPLICABLE_TO_REF_DATA
 * }
 * }
 * }
 * sequenceNumber = 0
 * fieldData = {
 * BID = 1.274
 * PX_LAST = 1.2741
 * ASK = 1.2742
 * TICKER = EURUSD
 * }
 * }
 * }
 */
public class BloombergReferenceDataRequestHandler extends AbstractBloombergRequestHandler<ReferenceDataResponse> {

	@Override
	protected Class<ReferenceDataResponse> getResponseClass() {
		return ReferenceDataResponse.class;
	}


	@Override
	protected void readResponse(Message msg, ReferenceDataResponse responseMessage) {
		Element securityDataArray = msg.getElement(BloombergUtils.SECURITY_DATA);
		for (int i = 0; i < securityDataArray.numValues(); ++i) {
			Element securityData = securityDataArray.getValueAsElement(i);
			BloombergError potentialError = BloombergUtils.populateErrorIfExists(securityData);
			if (potentialError != null) {
				responseMessage.setBloomError(potentialError);
			}
			LogUtils.debug(this.getResponseClass(), securityData.getElementAsString(BloombergUtils.SECURITY));
			Element fieldDataArray = securityData.getElement(BloombergUtils.FIELD_DATA);
			Map<String, Object> values = responseMessage.addSecurity(securityData.getElementAsString(BloombergUtils.SECURITY));
			for (int j = 0; j < fieldDataArray.numElements(); ++j) {
				Element fieldData = fieldDataArray.getElement(j);
				if (fieldData.isNull()) {
					continue;
				}
				values.put(fieldData.name().toString(), BloombergUtils.readValue(fieldData));
			}
		}
	}
}
