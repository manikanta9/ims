package com.clifton.bloomberg.server.session.impl;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Event;
import com.bloomberglp.blpapi.EventHandler;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.Service;
import com.bloomberglp.blpapi.Session;
import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.response.BloombergSubscriptionSecurityData;
import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.bloomberg.server.session.BloombergSession;
import com.clifton.bloomberg.server.session.BloombergSessionOptions;
import com.clifton.bloomberg.server.session.identity.BloombergIdentity;
import com.clifton.bloomberg.server.session.identity.BloombergSubscriptionIdentity;
import com.clifton.bloomberg.server.util.BloombergUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * <code>BloombergDefaultEventHandler</code> represents a default {@link EventHandler}
 * for handling asynchronous {@link Session} events.
 *
 * @author NickK
 */
public class BloombergDefaultEventHandler implements EventHandler {

	private final BloombergSession bloombergSession;
	private final AsynchronousMessageHandler subscriptionMessageHandler;


	public BloombergDefaultEventHandler(BloombergSession bloombergSession) {
		this(bloombergSession, null);
	}


	public BloombergDefaultEventHandler(BloombergSession bloombergSession, AsynchronousMessageHandler subscriptionMessageHandler) {
		this.bloombergSession = bloombergSession;
		this.subscriptionMessageHandler = subscriptionMessageHandler;
	}


	@Override
	public void processEvent(Event event, Session session) {
		try {
			switch (event.eventType().intValue()) {
				case Event.EventType.Constants.SESSION_STATUS: {
					handleSessionStatusEvent(event);
					break;
				}
				case Event.EventType.Constants.AUTHORIZATION_STATUS: {
					processAuthorizationStatusEvent(event);
					break;
				}
				case Event.EventType.Constants.SUBSCRIPTION_DATA:
					processSubscriptionDataEvent(event);
					break;
				case Event.EventType.Constants.SUBSCRIPTION_STATUS:
					processSubscriptionStatus(event);
					break;
				case Event.EventType.Constants.ADMIN:
					processAdminEvent(event);
					break;
				case Event.EventType.Constants.RESPONSE:
				case Event.EventType.Constants.PARTIAL_RESPONSE: {
					processResponseEvent(event);
					break;
				}
				case Event.EventType.Constants.SERVICE_STATUS:
				case Event.EventType.Constants.REQUEST_STATUS:
				default: {
					handleGenericStatusEvent(event);
				}
			}
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to process event.", e);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void handleSessionStatusEvent(Event event) {
		for (Message message : event) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Session status event received: ", message));

			if (message.messageType() == BloombergUtils.SESSION_TERMINATED) {
				// close the session to clear resources a session will be recreated on the next request
				this.bloombergSession.stop();
			}
		}
	}


	private void handleGenericStatusEvent(Event event) {
		for (Message message : event) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Generic status event received: ", message));
		}
	}


	private void processAuthorizationStatusEvent(Event event) {
		for (Message message : event) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Authorization status event received: ", message));

			if (message.messageType() == BloombergUtils.AUTHORIZATION_REVOKED) {
				// Existing identity is invalid. Reauthorize user and obtain a new identity.
				CorrelationID correlationId = message.correlationID();
				if (correlationId.isObject()) {
					BloombergIdentity bloombergIdentity = (BloombergIdentity) correlationId.object();
					if (bloombergIdentity.getCorrelationId() != null) {
						this.bloombergSession.cancel(bloombergIdentity.getCorrelationId());
						bloombergIdentity.setCorrelationId(null);
					}
					this.bloombergSession.authenticate(bloombergIdentity);
				}
			}
			else if (message.messageType() == BloombergUtils.ENTITLEMENT_CHANGED) {
				// This is just informational. Continue to use existing identity.
			}
		}
	}


	private void processSubscriptionStatus(Event event) {
		Map<String, SubscriptionDataResponse> responseMap = new HashMap<>();
		for (Message message : event) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Subscription status event received: ", message));

			String security = message.topicName();
			if (!StringUtils.isEmpty(security)) {
				MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses status = null;
				Throwable error = null;
				if (message.messageType() == BloombergUtils.SUBSCRIPTION_TERMINATED) {
					status = MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.TERMINATED;
				}
				else if (message.messageType() == BloombergUtils.SUBSCRIPTION_STARTED) {
					status = MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.ACTIVE;
				}
				else if (message.messageType() == BloombergUtils.SUBSCRIPTION_FAILURE) {
					status = MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.FAILED;
					BloombergError bloombergError = new BloombergError();
					bloombergError.setErrorType(BloombergError.BloombergErrorTypes.SUBSCRIPTION);
					BloombergUtils.setErrorInfoFields(message.getElement(BloombergUtils.REASON), bloombergError);
					error = new BloombergRequestException(bloombergError);
				}
				else {
					//message.messageType() == BloombergUtils.SUBSCRIPTION_STREAMS_DEACTIVATED
					//message.messageType() == BloombergUtils.SUBSCRIPTION_STREAMS_ACTIVATED) {
					// TODO research what do we should do here; not sure what events these should be processed as
					continue;
				}

				// Create a response for each user identity
				for (int i = 0; i < message.numCorrelationIds(); i++) {
					CorrelationID correlationID = message.correlationID(i);
					BloombergSubscriptionIdentity subscriptionIdentity = (BloombergSubscriptionIdentity) correlationID.object();
					if (status == MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.FAILED) {
						// clear the correlation ID so subsequent subscription requests can be made
						subscriptionIdentity.setCorrelationId(null);
					}
					BloombergSubscriptionSecurityData subscriptionSecurityData = getSubscriptionDataForSubscriptionIdentityAndSecurity(responseMap, status, subscriptionIdentity, security);
					if (subscriptionSecurityData.getDataTypeQualifier() == null) {
						subscriptionSecurityData.setDataTypeQualifier(message.messageType().toString());
					}
					if (error != null && subscriptionSecurityData.getError() == null) {
						subscriptionSecurityData.setError(error);
					}
				}
			}
		}
		sendSubscriptionDataResponses(responseMap.values());
	}


	private void processSubscriptionDataEvent(Event event) {
		Map<String, SubscriptionDataResponse> responseMap = new HashMap<>();
		for (Message message : event) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Subscription data event received: ", message));

			if (message.messageType() == BloombergUtils.MARKET_DATA_EVENTS) {
				Element messageElement = message.asElement();
				Element entitlements = getEntitlementsFromMessageElement(messageElement);
				List<Integer> failedEntitlementList = new ArrayList<>();
				for (int i = 0; i < message.numCorrelationIds(); i++) {
					BloombergSubscriptionIdentity subscriptionIdentity = (BloombergSubscriptionIdentity) message.correlationID(i).object();
					String security = subscriptionIdentity.getSecurity();
					failedEntitlementList.clear();
					if (entitlements == null || subscriptionIdentity.getBloombergIdentity().getIdentity().hasEntitlements(entitlements, message.service(), failedEntitlementList)) {
						BloombergSubscriptionSecurityData subscriptionSecurityData = getSubscriptionDataForSubscriptionIdentityAndSecurity(responseMap, MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.ACTIVE, subscriptionIdentity, security);
						if (subscriptionSecurityData.getDataTypeQualifier() == null) {
							subscriptionSecurityData.setDataTypeQualifier(message.messageType().toString());
						}
						subscriptionIdentity.getFieldList().forEach(fieldName -> subscriptionSecurityData.getSecurityDataValueMap()
								.computeIfAbsent(fieldName, f -> messageElement.hasElement(fieldName) ? BloombergUtils.readValue(messageElement.getElement(fieldName)) : null));
					}
				}
			}
		}
		sendSubscriptionDataResponses(responseMap.values());
	}


	/**
	 * Returns a new or existing {@link BloombergSubscriptionSecurityData} object for the provided {@link BloombergSubscriptionIdentity} and security.
	 * The map will be updated to contain any new {@link SubscriptionDataResponse}s created. New responses will only be created for
	 * newly encountered {@link BloombergSubscriptionIdentity#getBloombergIdentity()}s.
	 */
	private BloombergSubscriptionSecurityData getSubscriptionDataForSubscriptionIdentityAndSecurity(Map<String, SubscriptionDataResponse> responseMap, MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses status, BloombergSubscriptionIdentity subscriptionIdentity, String security) {
		// map responses by user - each response can have multiple security responses
		String destinationSource = subscriptionIdentity.getMessageSource();
		String destinationMachine = subscriptionIdentity.getMessageMachine();
		String sourceKey = StringUtils.generateKey(status, destinationSource, destinationMachine);
		SubscriptionDataResponse response = responseMap.computeIfAbsent(sourceKey, key -> {
			SubscriptionDataResponse newResponse = new SubscriptionDataResponse();
			Optional.ofNullable(destinationSource).ifPresent(source -> newResponse.getProperties().put(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME, source));
			Optional.ofNullable(destinationMachine).ifPresent(machine -> newResponse.getProperties().put(MessagingMessage.MESSAGE_MACHINE_NAME_PROPERTY_NAME, machine));
			return newResponse;
		});
		BloombergSubscriptionSecurityData securityDataResponse = response.addSecurity(security);
		if (securityDataResponse.getStatus() == null) {
			securityDataResponse.setStatus(status);
		}
		securityDataResponse.addIdentity(subscriptionIdentity.getBloombergIdentity().getBloombergRequestIdentity());
		return securityDataResponse;
	}


	/**
	 * Sends each {@link SubscriptionDataResponse} in the proved collection that is non null and non empty.
	 */
	private void sendSubscriptionDataResponses(Collection<SubscriptionDataResponse> subscriptionDataResponseCollection) {
		AsynchronousMessageHandler messageHandler = getSubscriptionMessageHandler();
		if (messageHandler == null) {
			LogUtils.error(getClass(), "Attempted to send subscription data response, but no handler is available.");
		}
		else {
			CollectionUtils.getStream(subscriptionDataResponseCollection)
					.filter(response -> response != null && !response.getSubscriptionData().isEmpty())
					.forEach(messageHandler::send);
		}
	}


	private void processAdminEvent(Event event) {
		for (Message message : event) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Admin status event received: ", message));

			// An admin event can have more than one messages.
			if (message.messageType() == BloombergUtils.SLOW_CONSUMER_WARNING) {
				// TODO take action to speed up consuming events
				LogUtils.warn(LogCommand.ofMessage(getClass(), "Slow consumer warning: ", message));
			}
			else if (message.messageType() == BloombergUtils.SLOW_CONSUMER_WARNING_CLEARED) {
				// If action was taken upon slow warning, can revert them
				LogUtils.warn(LogCommand.ofMessage(getClass(), "Slow consumer warning cleared: ", message));
			}
			else if (message.messageType() == BloombergUtils.DATA_LOSS) {
				if (message.hasElement(BloombergUtils.SOURCE)) {
					String sourceStr = message.getElementAsString(BloombergUtils.SOURCE);
					if ("InProc".equals(sourceStr)) {
						// DataLoss was generated "InProc". This can only happen if applications are processing events slowly
						// and are not able to keep-up with the incoming events. May want to throttle subscriptions
						LogUtils.warn(LogCommand.ofMessage(getClass(), "Data Loss In Proc: ", message));
					}
				}
			}
		}
	}


	private void processResponseEvent(Event event) {
		for (Message message : event) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Response event received: ", message));

			if (message.hasElement(BloombergUtils.RESPONSE_ERROR)) {
				BloombergError bError = BloombergUtils.populateErrorIfExists(message.getElement(BloombergUtils.RESPONSE_ERROR));
				throw new BloombergRequestException(bError);
			}
			// We have a valid response. Distribute it to the one or more users entitled
			Service service = message.service();
			Element securities = message.getElement(BloombergUtils.SECURITY_DATA);
			int numSecurities = securities.numValues();
			for (int i = 0; i < numSecurities; ++i) {
				Element security = securities.getValueAsElement(i);
				Element entitlements = getEntitlementsFromMessageElement(security);
				this.bloombergSession.getEntitledUserList(entitlements, service);
				// TODO Distribute asynchronous message response to the user for each Identity.
			}
		}
	}


	/**
	 * Returns the entitlement element from the provided element. If there is no entitlements or the session is not in application mode, null is returned.
	 * Null indicates no entitlements are needed to see the data of the message element.
	 */
	private Element getEntitlementsFromMessageElement(Element messageElement) {
		return this.bloombergSession.getOptions().getAuthenticationMode() == BloombergSessionOptions.BloombergBPipeAuthenticationMode.APPLICATION_ONLY
				? messageElement.hasElement(BloombergUtils.ENTITLEMENT_ID_DATA) ? messageElement.getElement(BloombergUtils.ENTITLEMENT_ID_DATA) : null
				: null;
	}


	private AsynchronousMessageHandler getSubscriptionMessageHandler() {
		return this.subscriptionMessageHandler;
	}
}
