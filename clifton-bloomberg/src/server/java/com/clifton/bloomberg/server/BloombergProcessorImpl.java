package com.clifton.bloomberg.server;


import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.request.ServiceManagementRequest;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.messages.response.ServiceManagementResponse;
import com.clifton.bloomberg.server.session.BloombergSessionManager;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.synchronous.SynchronousProcessor;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import java.util.concurrent.atomic.AtomicInteger;

import static com.clifton.bloomberg.messages.BloombergOperations.ServiceManagement;


/**
 * The <code>BloombergProcessorImpl</code> processes request by calling the Bloomberg API directly.
 * Needs to be run locally on a Bloomberg terminal as the API does not allow remote access.
 */
public class BloombergProcessorImpl implements SynchronousProcessor<BloombergRequestMessage, BloombergResponseMessage> {

	private BloombergSessionManager bloombergSessionManager;

	/**
	 * Thread safe modifiable current count of consecutive errors.
	 */
	private AtomicInteger currentConsecutiveErrorCount = new AtomicInteger(0);
	/**
	 * The max number of consecutive errors.
	 * <p>
	 * NOTE: Default is 5 errors.
	 */
	private int maxConsecutiveErrorCount = 5;
	/**
	 * The number of seconds to stop receiving messages after max consecutive error count reached.
	 * <p>
	 * NOTE: Default is 15 minutes.
	 */
	private int errorWaitTime = 15 * 60;

	private DefaultMessageListenerContainer jmsListener;

	private CacheHandler<String, String> cacheHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BloombergResponseMessage process(BloombergRequestMessage bloombergRequest) {
		try {
			if (bloombergRequest.getOperation() != ServiceManagement) {
				LogUtils.info(LogCommand.ofMessage(getClass(), "Non service management message received. Processing will be handled by the appropriate Bloomberg Session"));
				BloombergResponseMessage result = getBloombergSessionManager().getSession(bloombergRequest).submitSynchronousRequest(bloombergRequest);
				resetCurrentConsecutiveErrorCount();
				return result;
			}
			else {
				LogUtils.info(LogCommand.ofMessage(getClass(), "Service management message received."));
				return processServiceManagementRequest(bloombergRequest);
			}
		}
		catch (BloombergRequestException be) {
			return handleProcessingError(bloombergRequest, be);
		}
		catch (Exception e) {
			// stop the session on errors caused be something other than a bloomberg response
			getBloombergSessionManager().stopSession(bloombergRequest);
			return handleProcessingError(bloombergRequest, e);
		}
	}


	private BloombergResponseMessage processServiceManagementRequest(BloombergRequestMessage bloombergRequest) {
		ServiceManagementRequest managementRequest = (ServiceManagementRequest) bloombergRequest;
		ServiceManagementResponse response = new ServiceManagementResponse();
		switch (managementRequest.getManagementAction()) {
			case CLEAR_CACHE_ALL:
				LogUtils.info(LogCommand.ofMessage(getClass(), "Clearing all caches"));
				getCacheHandler().clearAll();
				break;
			default:
				response.setError(new ValidationException("Management Action not supported: " + managementRequest.getManagementAction()));
		}
		return response;
	}


	///////////////////////////////////////////////////////////////////////////
	//////////////              Helper Methods                  ///////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Handles an error while processing a request. The base implementation logs the processing error,
	 * conditionally pauses listening on the event queue if the concurrent error count threshold has been reached,
	 * and throws a {@link RuntimeException} with the cause.
	 */
	private BloombergResponseMessage handleProcessingError(BloombergRequestMessage bloombergRequest, Throwable throwable) {
		LogUtils.warn(getClass(), "Failed to process bloomberg request [" + bloombergRequest + "].", throwable);

		//If the consecutiveErrorCountDisabledFlag is null treat as false and proceed to checking threshold
		// otherwise if it is not null then check to make sure it is not disabled
		if ((bloombergRequest.getConsecutiveErrorCountDisabled() == null || (bloombergRequest.getConsecutiveErrorCountDisabled() != null && !bloombergRequest.getConsecutiveErrorCountDisabled()))
				&& isConsecutiveErrorCountThresholdReached()) {
			pauseListening();
		}

		throw new RuntimeException("Failed to query bloomberg.", throwable);
	}


	private boolean isConsecutiveErrorCountThresholdReached() {
		return getMaxConsecutiveErrorCount() > 0 && incrementCurrentConsecutiveErrorCount() >= getMaxConsecutiveErrorCount();
	}


	private synchronized void pauseListening() {
		LogUtils.error(getClass(), "Stopping listening because [" + getMaxConsecutiveErrorCount() + "] consecutive errors have occurred.");
		resetCurrentConsecutiveErrorCount();
		BloombergJmsListenerUtils.pauseListener(getJmsListener(), getErrorWaitTime());
	}


	private int incrementCurrentConsecutiveErrorCount() {
		return this.currentConsecutiveErrorCount.incrementAndGet();
	}


	private void resetCurrentConsecutiveErrorCount() {
		this.currentConsecutiveErrorCount.set(0);
	}


	///////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setters              ///////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergSessionManager getBloombergSessionManager() {
		return this.bloombergSessionManager;
	}


	public void setBloombergSessionManager(BloombergSessionManager bloombergSessionManager) {
		this.bloombergSessionManager = bloombergSessionManager;
	}


	public int getMaxConsecutiveErrorCount() {
		return this.maxConsecutiveErrorCount;
	}


	public void setMaxConsecutiveErrorCount(int maxConsecutiveErrorCount) {
		this.maxConsecutiveErrorCount = maxConsecutiveErrorCount;
	}


	public int getErrorWaitTime() {
		return this.errorWaitTime;
	}


	public void setErrorWaitTime(int errorWaitTime) {
		this.errorWaitTime = errorWaitTime;
	}


	public DefaultMessageListenerContainer getJmsListener() {
		return this.jmsListener;
	}


	public void setJmsListener(DefaultMessageListenerContainer jmsListener) {
		this.jmsListener = jmsListener;
	}


	public CacheHandler<String, String> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, String> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
