package com.clifton.bloomberg.server;


import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.Request;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;


/**
 * The <code>BloombergRequestHandler</code> interface defines methods for processing Bloomberg requests.
 * Different implementations may exists for historic vs reference data requests.
 */
public interface BloombergRequestHandler<T extends BloombergResponseMessage> {

	/**
	 * Populate the specified Bloomberg Request object from our message.
	 */
	public void populateRequest(Request request, BloombergRequestMessage requestMessage);


	/**
	 * A response from Bloomberg may contain multiple messages.  Populates our response from Bloomberg's Message. Keeps accumulating
	 * response if multiple messages were returned.
	 */
	public T processPartialResponse(Message msg, BloombergRequestMessage originalRequestMessage, T responseMessage);
}
