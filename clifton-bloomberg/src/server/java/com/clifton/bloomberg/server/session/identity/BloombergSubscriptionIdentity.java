package com.clifton.bloomberg.server.session.identity;

import com.clifton.bloomberg.server.session.BloombergCorrelationAware;
import com.clifton.core.util.compare.CompareUtils;

import java.util.List;
import java.util.Objects;


/**
 * <code>BloombergSubscriptionIdentity</code> represents a subscription for a security feed.
 * A subscription stays active until terminated by the user or the session.
 *
 * @author NickK
 */
public class BloombergSubscriptionIdentity extends BaseBloombergCorrelationAwareIdentity implements BloombergCorrelationAware {

	private final BloombergIdentity bloombergIdentity;
	private final String security;
	private List<String> fieldList;
	/**
	 * Properties used for routing to the originating server.
	 */
	private String messageSource;
	private String messageMachine;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergSubscriptionIdentity(BloombergIdentity bloombergIdentity, String security) {
		this.bloombergIdentity = bloombergIdentity;
		this.security = security;
	}


	@Override
	public String toString() {
		return "{bloombergIdentity=" + getBloombergIdentity() + ", security=" + getSecurity() + "}";
	}


	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		else if (!(object instanceof BloombergSubscriptionIdentity)) {
			return false;
		}
		BloombergSubscriptionIdentity toCompare = (BloombergSubscriptionIdentity) object;
		return CompareUtils.isEqual(getBloombergIdentity(), toCompare.getBloombergIdentity())
				&& CompareUtils.isEqual(getSecurity(), toCompare.getSecurity());
	}


	@Override
	public int hashCode() {
		return Objects.hash(
				getBloombergIdentity(),
				getSecurity()
		);
	}


	public BloombergIdentity getBloombergIdentity() {
		return this.bloombergIdentity;
	}


	public String getSecurity() {
		return this.security;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setters              ///////////////
	///////////////////////////////////////////////////////////////////////////


	public List<String> getFieldList() {
		return this.fieldList;
	}


	public void setFieldList(List<String> fieldList) {
		this.fieldList = fieldList;
	}


	public String getMessageSource() {
		return this.messageSource;
	}


	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}


	public String getMessageMachine() {
		return this.messageMachine;
	}


	public void setMessageMachine(String messageMachine) {
		this.messageMachine = messageMachine;
	}
}
