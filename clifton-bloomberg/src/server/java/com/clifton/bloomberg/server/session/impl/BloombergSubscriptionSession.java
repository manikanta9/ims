package com.clifton.bloomberg.server.session.impl;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.Subscription;
import com.bloomberglp.blpapi.SubscriptionIterator;
import com.bloomberglp.blpapi.SubscriptionList;
import com.clifton.bloomberg.messages.request.BloombergRequestData;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.request.SubscriptionRequest;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.messages.response.BloombergSubscriptionSecurityData;
import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.bloomberg.server.session.BloombergSessionOptions;
import com.clifton.bloomberg.server.session.identity.BloombergIdentity;
import com.clifton.bloomberg.server.session.identity.BloombergSubscriptionIdentity;
import com.clifton.bloomberg.server.util.BloombergUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * <code>BloombergSubscriptionSession</code> is an enhanced {@link BloombergBpipeSession} capable
 * of managing user defined security subscription feeds.
 *
 * @author NickK
 */
public class BloombergSubscriptionSession extends BloombergBpipeSession {

	/*
	 * Map of subscription identities for each user identity. This structure is used to track subscriptions
	 * for each user to resubscribe and unsubscribe as needed based user requests and session events.
	 * Thus, a map is used with the object used as key and value, so we do not have to iterate a set.
	 * The application should listen for SUBSCRIPTION_STATUS events.
	 */
	private final Map<BloombergIdentity, Map<BloombergSubscriptionIdentity, Subscription>> subscriptionMap = new ConcurrentHashMap<>();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergSubscriptionSession(BloombergSessionOptions bloombergSessionOptions, AsynchronousMessageHandler subscriptionMessageHandler) {
		super(bloombergSessionOptions, bloombergSession -> new BloombergDefaultEventHandler(bloombergSession, subscriptionMessageHandler));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void preStop() {
		cancelCachedSubscriptions();
		super.preStop();
	}


	@Override
	public BloombergResponseMessage submitSynchronousRequest(BloombergRequestMessage bloombergRequest) throws Exception {
		// Verify started
		start();

		if (bloombergRequest instanceof SubscriptionRequest) {
			return processSubscriptionRequest((SubscriptionRequest) bloombergRequest);
		}

		return super.submitSynchronousRequest(bloombergRequest);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Processes the provided subscription request by adding new subscriptions, updating existing subscriptions, or removing subscriptions
	 * based on the request's {@link MarketDataSubscriptionOperations}.
	 * <p>
	 * <br/>- If the request is {@link MarketDataSubscriptionOperations#SUBSCRIBE}, new subscriptions are subscribed to, and existing subscription matches are resubscribed to.
	 * <br/>- If the request is {@link MarketDataSubscriptionOperations#RESUBSCRIBE}, existing subscription matches are resubscribed to.
	 * <br/>- If the request is {@link MarketDataSubscriptionOperations#UNSUBSCRIBE}, existing subscription matches are unsubscribed to.
	 * <br/>- If the request is {@link MarketDataSubscriptionOperations#LIST}, existing active subscriptions will be returned.
	 * <br/>- If the request is {@link MarketDataSubscriptionOperations#CLEAR_SUBSCRIPTIONS}, existing active subscriptions and authenticated sessions will be cleared.
	 *
	 * @return a {@link BloombergResponseMessage} with the current status of the processed subscriptions
	 * @throws IOException if a session request fails
	 */
	private BloombergResponseMessage processSubscriptionRequest(SubscriptionRequest subscriptionRequest) throws IOException {
		MarketDataSubscriptionOperations subscriptionOperation = subscriptionRequest.getSubscriptionOperation();
		final SubscriptionList subscriptionList = new SubscriptionList();
		Throwable error = null;
		switch (subscriptionOperation) {
			case UNSUBSCRIBE: {
				processUnsubscribeRequest(subscriptionRequest, subscriptionList);
				break;
			}
			case LIST: {
				populateActiveSubscriptionList(subscriptionList);
				break;
			}
			case CLEAR_SUBSCRIPTIONS: {
				clearActiveSubscriptions();
				break;
			}
			default: { // subscribe and resubscribe
				// subscribes to new and resubscribes to updated subscriptions; each subscription is added to the subscription list response
				BloombergIdentity bloombergIdentity = authenticateUserForRequest(subscriptionRequest);
				SubscriptionHolder subscriptionHolder = getSubscriptionHolder(bloombergIdentity, subscriptionRequest, subscriptionOperation);
				try {
					if (subscriptionHolder.hasNewSubscriptionList()) {
						subscriptionHolder.getNewSubscriptionList().forEach(subscriptionList::add);
						getSession().subscribe(subscriptionHolder.getNewSubscriptionList(), bloombergIdentity.getIdentity());
					}
					if (subscriptionHolder.hasUpdateSubscriptionList()) {
						subscriptionHolder.getUpdateSubscriptionList().forEach(subscriptionList::add);
						getSession().resubscribe(subscriptionHolder.getUpdateSubscriptionList());
					}
				}
				catch (Exception e) {
					error = BloombergUtils.convertBloombergExceptionForMessaging("Subscribe request failed", e);
				}
			}
		}

		SubscriptionDataResponse response = getSubscriptionStatusResponse(subscriptionList);
		if (error != null) {
			response.setError(error);
		}
		response.getProperties().putAll(subscriptionRequest.getProperties());
		return response;
	}


	private void processUnsubscribeRequest(SubscriptionRequest subscriptionRequest, SubscriptionList subscriptionList) {
		// unsubscribes active subscriptions and adds each to the subscription list response
		BloombergIdentity bloombergIdentity;
		// If the user logs into another machine and has subscriptions active with a different IP address,
		// the cached identity must be removed and all cached subscriptions canceled.
		boolean authenticationFailed = false;
		try {
			bloombergIdentity = authenticateUserForRequest(subscriptionRequest);
		}
		catch (Exception e) {
			authenticationFailed = true;
			bloombergIdentity = BloombergIdentity.forUser(subscriptionRequest.getIdentity());
		}

		Map<BloombergSubscriptionIdentity, Subscription> userSubscriptionMap = CollectionUtils.getValue(this.subscriptionMap, bloombergIdentity, ConcurrentHashMap::new);
		SubscriptionHolder subscriptionHolder;
		if (authenticationFailed) {
			// Add all cached subscriptions to the holder to be canceled
			subscriptionHolder = new SubscriptionHolder();
			userSubscriptionMap.values().forEach(subscriptionHolder::addInvalidSubscription);
		}
		else {
			subscriptionHolder = getSubscriptionHolder(bloombergIdentity, subscriptionRequest, subscriptionRequest.getSubscriptionOperation());
		}

		if (subscriptionHolder.hasUpdateSubscriptionList()) {
			subscriptionHolder.getUpdateSubscriptionList().forEach(subscriptionList::add);
			try {
				// only process (un)subscription request if authenticated
				getSession().unsubscribe(subscriptionList);
			}
			catch (Exception e) {
				// log the failed unsubscribe, but there is no reason to return it in the response. The subscription will be cancelled and cleared from the cache.
				LogUtils.warn(getClass(), "Unsubscribe request failed", BloombergUtils.convertBloombergExceptionForMessaging("Unsubscribe failed", e));
			}
		}
		if (subscriptionHolder.hasInvalidSubscriptionList()) {
			subscriptionHolder.getInvalidSubscriptionList().forEach(subscriptionList::add);
		}

		// clear subscriptions for user from cache
		subscriptionList.forEach(subscription -> {
			BloombergSubscriptionIdentity subscriptionIdentity = (BloombergSubscriptionIdentity) subscription.correlationID().object();
			this.cancelBloombergCorrelation(subscriptionIdentity);
			userSubscriptionMap.remove(subscriptionIdentity);
		});
		if (userSubscriptionMap.isEmpty()) {
			// remove subscription entry for this identity if there are no more subscriptions active
			this.cancelBloombergCorrelation(bloombergIdentity);
			this.subscriptionMap.remove(bloombergIdentity);
		}
		if (this.subscriptionMap.isEmpty()) {
			// attempt to cancel any active cached identities
			this.cancelCachedIdentities();
		}
	}


	private BloombergIdentity authenticateUserForRequest(BloombergRequestMessage bloombergRequest) {
		ValidationUtils.assertNotNull(bloombergRequest.getIdentity(), "Unable to create a Subscription for a Bloomberg request with no user Identity.");
		BloombergIdentity bloombergIdentity = BloombergIdentity.forUser(bloombergRequest.getIdentity());
		authenticate(bloombergIdentity);
		return bloombergIdentity;
	}


	/**
	 * Processes the provided subscription request details and returns a {@link SubscriptionHolder} containing {@link SubscriptionList}s of
	 * {@link Subscription}s to subscribe to (new) and those applicable to being resubscribed or unsubscribed to (existing).
	 * <p>
	 * While processing the request, the local subscription cache is updated to reflect the subscription changes.
	 * <p>
	 * If the operation is {@link MarketDataSubscriptionOperations#UNSUBSCRIBE}, the new subscriptions are not populated and instead
	 * removed from the subscription cache.
	 */
	private SubscriptionHolder getSubscriptionHolder(BloombergIdentity bloombergIdentity, SubscriptionRequest subscriptionRequest, MarketDataSubscriptionOperations operation) {
		BloombergRequestData subscriptionRequestData = subscriptionRequest.getBloombergRequestData();
		Map<BloombergSubscriptionIdentity, Subscription> userSubscriptionMap = CollectionUtils.getValue(this.subscriptionMap, bloombergIdentity, ConcurrentHashMap::new);
		SubscriptionHolder subscriptionHolder = new SubscriptionHolder();
		List<String> optionsList = subscriptionRequestData.getElement(SubscriptionRequest.SUBSCRIPTION_OPTIONS_KEY).getRequestData();
		subscriptionRequestData.getElementList().forEach((name, element) -> {
			if (!SubscriptionRequest.SUBSCRIPTION_OPTIONS_KEY.equals(name) && !SubscriptionRequest.SUBSCRIPTION_OPERATION_KEY.equals(name)) {
				/*
				 * Generate a new map key that will match an existing entry based on hash key.
				 * If there is no matching entry, create a new subscription entry in cache.
				 * If it matches an existing entry, update the entry subscription for resubscribing.
				 */
				BloombergSubscriptionIdentity newSubscriptionIdentity = new BloombergSubscriptionIdentity(bloombergIdentity, name);
				newSubscriptionIdentity.setMessageSource(subscriptionRequest.getProperties().get(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME));
				newSubscriptionIdentity.setMessageMachine(subscriptionRequest.getMachineName());

				// compute the new entry based on subscription status
				userSubscriptionMap.compute(newSubscriptionIdentity, (subscriptionIdentity, currentSubscription) -> {
					//remove duplicate fields
					List<String> fieldList = CollectionUtils.getStream(element.getRequestData())
							.filter(Objects::nonNull)
							.map(field -> field.split(",")) // fields may be entered as "BID,ASK,LAST_PRICE"
							.flatMap(Arrays::stream)
							.filter(str -> !StringUtils.isEmpty(str))
							.distinct()
							.collect(Collectors.toList());

					// Each subscription with Bloomberg has a unique CorrelationID representing it. Set the BloombergSubscriptionIdentity
					// as the object on the CorrelationID so the system can determine the user and security for each CorrelationID.
					CorrelationID correlationId;
					Consumer<Subscription> subscriptionListConsumer;
					boolean removeMapping = false;
					if (currentSubscription == null) {
						// CorrelationId is always necessary for status check for returning response to client.
						correlationId = new CorrelationID(subscriptionIdentity);
						if (!operation.isSubscribe()) {
							// remove to avoid unnecessary mapping entry; must populate the holder with the subscription so it can return a response to the client
							removeMapping = true;
							subscriptionListConsumer = subscriptionHolder::addInvalidSubscription;
						}
						else {
							// create new
							subscriptionIdentity.setCorrelationId(correlationId);
							subscriptionIdentity.setFieldList(fieldList);
							subscriptionListConsumer = subscriptionHolder::addNewSubscription;
						}
					}
					else {
						// subscription exists in cache
						BloombergSubscriptionIdentity currentSubscriptionIdentity = (BloombergSubscriptionIdentity) currentSubscription.correlationID().object();
						currentSubscriptionIdentity.setFieldList(fieldList);
						if (currentSubscriptionIdentity.getCorrelationId() == null) {
							if (!operation.isSubscribe()) {
								// remove the invalid mapping do avoid updating an unnecessary mapping entry
								// must populate the holder with the subscription so it can return a response to the client
								removeMapping = true;
								subscriptionListConsumer = subscriptionHolder::addInvalidSubscription;
							}
							else {
								subscriptionListConsumer = subscriptionHolder::addNewSubscription;
							}
							// need to create a new CorrelationID because the previous subscription failed or became invalid
							currentSubscriptionIdentity.setCorrelationId(new CorrelationID(currentSubscriptionIdentity));
						}
						else {
							subscriptionListConsumer = subscriptionHolder::addUpdateSubscription;
						}
						correlationId = currentSubscriptionIdentity.getCorrelationId();
					}

					Subscription subscription = new Subscription(name, fieldList, optionsList, correlationId);
					subscriptionListConsumer.accept(subscription);
					return (removeMapping) ? null : subscription;
				});
			}
		});
		return subscriptionHolder;
	}


	/**
	 * Returns a {@link SubscriptionDataResponse} containing the current subscription status for each
	 * subscription in the provided {@link SubscriptionList};
	 */
	private SubscriptionDataResponse getSubscriptionStatusResponse(SubscriptionList subscriptionList) {
		SubscriptionDataResponse response = new SubscriptionDataResponse();
		if (subscriptionList != null) {
			List<BloombergSubscriptionIdentity> subscriptionIdentityList = new ArrayList<>();
			subscriptionList.forEach(subscription -> subscriptionIdentityList.add((BloombergSubscriptionIdentity) subscription.correlationID().object()));
			long startMillis = System.currentTimeMillis();
			long millisTimeout = 3000L;
			boolean stop = false;
			// iterate over the subscriptions until we get a non-pending status for each, until we meet a millis timeout
			while (!stop && !subscriptionIdentityList.isEmpty()) {
				Iterator<BloombergSubscriptionIdentity> subscriptionIdentityIterator = subscriptionIdentityList.iterator();
				while (subscriptionIdentityIterator.hasNext()) {
					BloombergSubscriptionIdentity subscriptionIdentity = subscriptionIdentityIterator.next();
					BloombergSubscriptionSecurityData subscriptionData = response.addSecurity(subscriptionIdentity.getSecurity());
					subscriptionData.getUserIdentitySet().add(subscriptionIdentity.getBloombergIdentity().getBloombergRequestIdentity());
					subscriptionData.setDataTypeQualifier("SubscriptionStatus");
					try {
						int status = getSession().subscriptionStatus(subscriptionIdentity.getCorrelationId());
						if (status == Session.SubscriptionStatus.RECEIVING_MESSAGES.intValue()
								|| status == Session.SubscriptionStatus.WAITING_FOR_MESSAGES.intValue()) {
							subscriptionData.setStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.ACTIVE);
						}
						else if (status == Session.SubscriptionStatus.SUBSCRIBING.intValue()) {
							subscriptionData.setStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.PENDING);
							// TODO remove this for now and return after first iteration; client will udpate status
							// continue; // keep trying to get an updated status
						}
						else {
							subscriptionData.setStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.INACTIVE);
						}
					}
					catch (Exception e) {
						subscriptionData.setStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.FAILED);
						subscriptionData.setError(BloombergUtils.convertBloombergExceptionForMessaging("Subscription status check failed", e));
					}
					subscriptionIdentityIterator.remove();
				}
				stop = (System.currentTimeMillis() - startMillis) > millisTimeout;
			}
		}
		return response;
	}


	/**
	 * Cancels cached application and user identities to invalidate authorized users.
	 */
	private void cancelCachedSubscriptions() {
		LogUtils.info(getClass(), "Canceling active subscriptions.");
		// Cancel and remove applicable user identity entries after subscriptions are canceled and removed
		this.subscriptionMap.entrySet().removeIf(entry -> {
			cancelUserSubscriptionMapEntries(entry.getValue());
			return this.cancelBloombergCorrelation(entry.getKey());
		});
	}


	private void cancelUserSubscriptionMapEntries(Map<BloombergSubscriptionIdentity, Subscription> userSubscriptionMap) {
		if (!CollectionUtils.isEmpty(userSubscriptionMap)) {
			// Cancel and remove applicable subscription entries
			userSubscriptionMap.entrySet().removeIf(subscriptionEntry -> cancelBloombergCorrelation(subscriptionEntry.getKey()));
		}
	}


	private void populateActiveSubscriptionList(SubscriptionList subscriptionList) throws IOException {
		// sync session subscriptions with cached subscriptions and return the active session subscriptions
		Set<BloombergSubscriptionIdentity> activeSubscriptionIdentitySet = new HashSet<>();
		syncSubscriptionCache(
				// adds the session subscription to cache if it does not exist
				(bloombergSubscriptionIdentity, subscription) -> {
					activeSubscriptionIdentitySet.add(bloombergSubscriptionIdentity);
					Map<BloombergSubscriptionIdentity, Subscription> userSubscriptionMap = this.subscriptionMap.computeIfAbsent(bloombergSubscriptionIdentity.getBloombergIdentity(), key -> new ConcurrentHashMap<>());
					if (!userSubscriptionMap.containsKey(bloombergSubscriptionIdentity)) {
						userSubscriptionMap.put(bloombergSubscriptionIdentity, subscription);
					}
				},
				// removes those entries not in the session subscriptions and adds active subscriptions to the returned list
				identitySubscriptionMapEntry -> {
					Map<BloombergSubscriptionIdentity, Subscription> userSubscriptionMap = identitySubscriptionMapEntry.getValue();
					userSubscriptionMap.forEach((bloombergSubscriptionIdentity, subscription) -> {
						if (activeSubscriptionIdentitySet.contains(bloombergSubscriptionIdentity)) {
							subscriptionList.add(subscription);
						}
						else {
							cancelBloombergCorrelation(bloombergSubscriptionIdentity);
						}
					});
					if (CollectionUtils.isEmpty(userSubscriptionMap)) {
						// no more valid subscriptions for identity, so remove the entry
						return cancelBloombergCorrelation(identitySubscriptionMapEntry.getKey());
					}
					return false;
				});
	}


	private void clearActiveSubscriptions() throws IOException {
		// unsubscribe all subscriptions in session and cache
		syncSubscriptionCache(
				// removes all session subscriptions from cache and cancels the subscription
				(bloombergSubscriptionIdentity, subscription) -> {
					Map<BloombergSubscriptionIdentity, Subscription> userSubscriptionMap = this.subscriptionMap.get(bloombergSubscriptionIdentity.getBloombergIdentity());
					if (!CollectionUtils.isEmpty(userSubscriptionMap)) {
						Optional.ofNullable(userSubscriptionMap.remove(bloombergSubscriptionIdentity))
								.ifPresent(s -> cancelBloombergCorrelation(bloombergSubscriptionIdentity));
					}
				},
				// removes and cancels all remaining subscriptions in cache that were not present in the session
				identitySubscriptionMapEntry -> {
					cancelUserSubscriptionMapEntries(identitySubscriptionMapEntry.getValue());
					// no more valid subscriptions for identity, so cancel and remove the entry
					return cancelBloombergCorrelation(identitySubscriptionMapEntry.getKey());
				});
	}


	/**
	 * Method allows for syncing the local subscription cache with active subscriptions in the session in two steps.
	 * First, it iterates all session subscriptions allowing each subscription to be processed in the provided sessionSubscriptionConsumer {@link BiConsumer}.
	 * Second, the local subscription cache entries are iterated for removal based on the provided identitySubscriptionMapEntryRemovalPredicate {@link Predicate},
	 */
	private void syncSubscriptionCache(BiConsumer<BloombergSubscriptionIdentity, Subscription> sessionSubscriptionConsumer, Predicate<Map.Entry<BloombergIdentity, Map<BloombergSubscriptionIdentity, Subscription>>> identitySubscriptionMapEntryRemovalPredicate) throws IOException {
		// sync cached subscriptions with those on the session.
		SubscriptionIterator iterator = getSession().subscriptionIterator();
		// subscription list for those subscriptions with no correlationID or BloombergSubscriptionIdentity (should never occur because they should be removed from BPipe)
		SubscriptionList subscriptionListToUnsubscribe = new SubscriptionList();
		while (iterator.hasNext()) {
			Subscription subscription = iterator.next();
			BloombergSubscriptionIdentity bloombergSubscriptionIdentity = Optional.ofNullable(subscription.correlationID())
					.map(correlationId -> (BloombergSubscriptionIdentity) correlationId.object())
					.orElse(null);
			if (bloombergSubscriptionIdentity == null) {
				subscriptionListToUnsubscribe.add(subscription);
			}
			else {
				sessionSubscriptionConsumer.accept(bloombergSubscriptionIdentity, subscription);
			}
		}
		if (!subscriptionListToUnsubscribe.isEmpty()) {
			getSession().unsubscribe(subscriptionListToUnsubscribe);
		}

		this.subscriptionMap.entrySet().removeIf(identitySubscriptionMapEntryRemovalPredicate);
		if (this.subscriptionMap.isEmpty()) {
			// attempt to cancel any active cached identities
			this.cancelCachedIdentities();
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Private helper class used to track subscriptions while processing a request.
	 * Non-cached subscriptions are new and can be created. Cached subscriptions can be updated, viewed, and/or removed.
	 */
	private static final class SubscriptionHolder {

		/**
		 * Subscription list of new subscriptions
		 */
		private final SubscriptionList newSubscriptionList = new SubscriptionList();
		/**
		 * Subscription list of existing subscriptions to update
		 */
		private final SubscriptionList updateSubscriptionList = new SubscriptionList();
		/**
		 * Subscription list of invalid subscriptions; those being unsubscribed which to not actually exist
		 */
		private final SubscriptionList invalidSubscriptionList = new SubscriptionList();


		public void addNewSubscription(Subscription subscription) {
			this.newSubscriptionList.add(subscription);
		}


		public SubscriptionList getNewSubscriptionList() {
			return this.newSubscriptionList;
		}


		public boolean hasNewSubscriptionList() {
			return !getNewSubscriptionList().isEmpty();
		}


		public void addUpdateSubscription(Subscription subscription) {
			this.updateSubscriptionList.add(subscription);
		}


		public boolean hasUpdateSubscriptionList() {
			return !getUpdateSubscriptionList().isEmpty();
		}


		public SubscriptionList getUpdateSubscriptionList() {
			return this.updateSubscriptionList;
		}


		public void addInvalidSubscription(Subscription subscription) {
			this.invalidSubscriptionList.add(subscription);
		}


		public SubscriptionList getInvalidSubscriptionList() {
			return this.invalidSubscriptionList;
		}


		public boolean hasInvalidSubscriptionList() {
			return !getInvalidSubscriptionList().isEmpty();
		}
	}
}
