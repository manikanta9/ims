package com.clifton.bloomberg.server.session.impl;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.EventHandler;
import com.bloomberglp.blpapi.EventQueue;
import com.bloomberglp.blpapi.Identity;
import com.bloomberglp.blpapi.Request;
import com.bloomberglp.blpapi.Service;
import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.server.BloombergRequestHandler;
import com.clifton.bloomberg.server.session.BloombergSession;
import com.clifton.bloomberg.server.session.BloombergSessionOptions;
import com.clifton.bloomberg.server.session.identity.BloombergIdentity;
import com.clifton.bloomberg.server.util.BloombergUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;


/**
 * <code>BloombergDefaultSession</code> represents a Terminal Bloomberg {@link Session} wrapper that does not
 * support authentication. To support authentication and authorization see {@link BloombergBpipeSession}.
 *
 * @author NickK
 */
public class BloombergDefaultSession implements BloombergSession {

	private final Map<String, Service> serviceMap = new ConcurrentHashMap<>();

	private final BloombergSessionOptions bloombergSessionOptions;
	private final EventHandler eventHandler;
	/**
	 * A session can only be started once, so upon every close, the session reference should be cleared.
	 * Each start should create a new session.
	 */
	private final AtomicReference<Session> session = new AtomicReference<>();
	private final AtomicBoolean open = new AtomicBoolean();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergDefaultSession(BloombergSessionOptions bloombergSessionOptions) {
		this(bloombergSessionOptions, null);
	}


	public BloombergDefaultSession(BloombergSessionOptions bloombergSessionOptions, Function<BloombergSession, EventHandler> eventHandlerFunction) {
		this.bloombergSessionOptions = bloombergSessionOptions;
		this.eventHandler = (eventHandlerFunction == null ? getEventHandlerFunction() : eventHandlerFunction).apply(this);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns an EventHandler resulting from {@link BloombergDefaultEventHandler#BloombergDefaultEventHandler(BloombergSession)}
	 * using this session reference as the argument.
	 */
	protected Function<BloombergSession, EventHandler> getEventHandlerFunction() {
		return BloombergDefaultEventHandler::new;
	}


	/**
	 * Starts the session if not currently started.
	 */
	@Override
	public void start() {
		if (!isSessionOpen()) {
			try {
				if (this.session.compareAndSet(null, new Session(this.bloombergSessionOptions.getSessionOptions(), this.eventHandler))) {
					preStart();
					// Set open to true first thing. The close in the catch will reset it.
					this.open.set(true);
					if (!getSession().start()) {
						throw new RuntimeException("Call to bloomberg session start method return false.");
					}
					postStart();
				}
			}
			catch (Throwable e) {
				if (e instanceof InterruptedException) {
					Thread.currentThread().interrupt();
				}
				// Attempt to stop the Session if an error occurs trying to start it to clean up any cached references.
				stop();
				StringBuilder message = new StringBuilder("Failed to start session ");
				if (getOptions().getSessionOptions().getServerAddresses() == null) {
					message.append("on host: ").append(getOptions().getSessionOptions().getServerHost()).append(":").append(getOptions().getSessionOptions().getServerPort()).append('.');
				}
				else {
					message.append("using server addresses: {").append(StringUtils.join(getOptions().getSessionOptions().getServerAddresses(), SessionOptions.ServerAddress::toString, ",")).append("}.");
				}
				throw new RuntimeException(message.toString(), e);
			}
		}
	}


	/**
	 * Hook for sessions to process initialization prior to starting.
	 */
	protected void preStart() {
		BloombergUtils.startBBComm();
	}


	/**
	 * Hook for sessions to process initialization after starting.
	 */
	protected void postStart() {
		// nothing by default
	}


	/**
	 * Stops the session if started.
	 */
	@Override
	public void stop() {
		if (isSessionOpen()) {
			try {
				Session sessionToClose = getSession();
				if (this.session.compareAndSet(sessionToClose, null)) {
					preStop();
					try {
						this.open.set(false);
						this.serviceMap.clear();
					}
					finally {
						sessionToClose.stop();
					}
					postStop();
				}
			}
			catch (Throwable e) {
				if (e instanceof InterruptedException) {
					Thread.currentThread().interrupt();
				}
				LogUtils.warn(getClass(), "Failed to stop Bloomberg session.", e);
			}
			finally {
				this.session.set(null);
			}
		}
	}


	/**
	 * Hook for sessions to process cleanup prior to stopping.
	 */
	protected void preStop() {
		// nothing by default
	}


	/**
	 * Hook for sessions to process cleanup after stopping.
	 */
	protected void postStop() {
		// nothing by default
	}


	@Override
	public BloombergResponseMessage submitSynchronousRequest(BloombergRequestMessage bloombergRequest) throws Exception {
		// Verify started
		start();

		Service service = getService(bloombergRequest.getServiceUrl());
		Request request = service.createRequest(bloombergRequest.getOperation().toString());

		// build Bloomberg request message
		BloombergRequestHandler<BloombergResponseMessage> requestHandler = getOptions().getRequestHandler();
		requestHandler.populateRequest(request, bloombergRequest);

		LogUtils.debug(getClass(), request.toString());

		EventQueue eventQueue = new EventQueue();
		getSession().sendRequest(request, eventQueue, null);
		return BloombergUtils.handleSynchronousSingleRequest(eventQueue, requestHandler, bloombergRequest);
	}


	/**
	 * This implementation does not support authentication and returns null.
	 */
	@Override
	public Identity authenticate(@SuppressWarnings("unused") BloombergIdentity bloombergIdentity) {
		return null;
	}


	/**
	 * This implementation does not support authentication and returns an empty list.
	 */
	@Override
	public List<BloombergIdentity> getEntitledUserList(@SuppressWarnings("unused") Element entitlements, @SuppressWarnings("unused") Service service) {
		return Collections.emptyList();
	}


	@Override
	public void cancel(CorrelationID correlationID) {
		if (correlationID != null && isSessionOpen()) {
			getSession().cancel(correlationID);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////              Helper methods                  ///////////////
	///////////////////////////////////////////////////////////////////////////


	protected boolean isSessionOpen() {
		return this.open.get();
	}


	/**
	 * Returns the {@link Service} for the provided service name. The services are created once and cached
	 * for reuse. If it is not in cache, it will be created using {@link Session#openService(String)} and
	 * {@link Session#getService(String)}.
	 */
	protected Service getService(String service) {
		if (StringUtils.isEmpty(service)) {
			throw new RuntimeException("Failed to open [] service url.");
		}
		return CollectionUtils.getValue(this.serviceMap, service, () -> {
			if (isSessionOpen()) {
				try {
					if (getSession().openService(service)) {
						return getSession().getService(service);
					}
				}
				catch (Exception e) {
					if (e instanceof InterruptedException) {
						Thread.currentThread().interrupt();
					}
					throw new RuntimeException("Failed to open [" + service + "] service url.", e);
				}
			}
			throw new RuntimeException("Unable to open [" + service + "] service url because the session is closed.");
		});
	}


	/**
	 * Returns the {@link BloombergSessionOptions} used to create this session.
	 */
	@Override
	public BloombergSessionOptions getOptions() {
		return this.bloombergSessionOptions;
	}


	/**
	 * Returns the underlying {@link Session} managed by this session wrapper
	 */
	protected Session getSession() {
		return this.session.get();
	}
}
