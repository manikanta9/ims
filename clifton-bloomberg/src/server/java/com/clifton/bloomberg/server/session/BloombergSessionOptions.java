package com.clifton.bloomberg.server.session;

import com.bloomberglp.blpapi.SessionOptions;
import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.server.BloombergRequestHandler;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.sql.SqlHandlerLocator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Map;


/**
 * <code>BloombergSessionOptions</code> contains information for establishing sessions to Bloomberg
 * and information to assist in processing requests.
 *
 * @author NickK
 */
public class BloombergSessionOptions {

	private final SessionOptions sessionOptions;
	private final BloombergOperations bloombergOperation;
	private final BloombergRequestHandler<BloombergResponseMessage> requestHandler;
	private final boolean bPipe;
	private final boolean backOffice;
	private final boolean mds;
	private final String applicationName;
	private final BloombergBPipeAuthenticationMode authenticationMode;
	private final SqlHandlerLocator sqlHandlerLocator;
	private final CacheHandler<String, Map<String, String>> cacheHandler;


	private BloombergSessionOptions(BloombergSessionOptionsBuilder builder) {
		this.sessionOptions = builder.sessionOptions;
		this.bloombergOperation = builder.bloombergOperation;
		this.requestHandler = builder.requestHandler;
		this.bPipe = builder.bPipe;
		this.backOffice = builder.backOffice;
		this.mds = builder.mds;
		this.applicationName = builder.applicationName;
		this.authenticationMode = builder.authenticationMode;
		this.sqlHandlerLocator = builder.sqlHandlerLocator;
		this.cacheHandler = builder.cacheHandler;
		if (!isSubscription()) {
			ValidationUtils.assertNotNull(getRequestHandler(), "Unable to create a Bloomberg Session for [" + getBloombergOperation().name() + "] operations without a Request Handler.");
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public SessionOptions getSessionOptions() {
		return this.sessionOptions;
	}


	public BloombergOperations getBloombergOperation() {
		return this.bloombergOperation;
	}


	public BloombergRequestHandler<BloombergResponseMessage> getRequestHandler() {
		return this.requestHandler;
	}


	public boolean isSubscription() {
		return getBloombergOperation() == BloombergOperations.Subscription;
	}


	public boolean isBPipe() {
		return this.bPipe;
	}


	public boolean isBackOffice() {
		return this.backOffice;
	}


	public boolean isMds() {
		return this.mds;
	}


	public String getApplicationName() {
		return this.applicationName;
	}


	public BloombergBPipeAuthenticationMode getAuthenticationMode() {
		return this.authenticationMode;
	}


	public SqlHandlerLocator getSqlHandlerLocator() {
		return this.sqlHandlerLocator;
	}


	public CacheHandler<String, Map<String, String>> getCacheHandler() {
		return this.cacheHandler;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	public enum BloombergBPipeAuthenticationMode {
		/**
		 * This mode defines the authorization options where each user is authenticated and authorized individually with no application.
		 * The entitlements are applied based on the user only
		 * The {@link BloombergSession} will handle the identities for authorization.
		 */
		USER_ONLY("AuthenticationMode=USER_ONLY"),
		/**
		 * This mode defines the authorization options where the application and each user must authenticate and authorize individually.
		 * The entitlements are applied based on both the application and user; if the user can see data and the application cannot, the data will not be shown
		 * The {@link BloombergSession} will handle the identities for authorization.
		 */
		APPLICATION_ONLY("AuthenticationMode=APPLICATION_ONLY;ApplicationAuthenticationType=APPNAME_AND_KEY;ApplicationName=", true),
		/**
		 * This mode defines to authenticate with intersection of application and user entitlements.
		 * Supports additional parameters for authenticating users, which may function differently based on where the service is running.
		 * <br/>-AuthenticationType=OS_LOGON
		 * <br/>-AuthenticationType=DIRECTORY_SERVICE;DirSvcProperty=mail (where mail is the AD parameter to match the user)
		 */
		USER_AND_APPLICATION("AuthenticationMode=USER_AND_APPLICATION;ApplicationAuthenticationType=APPNAME_AND_KEY;ApplicationName=", true);


		private final String authenticationOptionsPrefix;
		private final boolean applicationNameRequired;


		BloombergBPipeAuthenticationMode(String authenticationOptionsPrefix) {
			this(authenticationOptionsPrefix, false);
		}


		BloombergBPipeAuthenticationMode(String authenticationOptionsPrefix, boolean applicationNameRequired) {
			this.authenticationOptionsPrefix = authenticationOptionsPrefix;
			this.applicationNameRequired = applicationNameRequired;
		}


		public boolean isApplicationNameRequired() {
			return this.applicationNameRequired;
		}


		public String getAuthenticationOptionsPrefix() {
			return this.authenticationOptionsPrefix;
		}


		public String getAuthenticationOptions() {
			return getAuthenticationOptions(null);
		}


		public String getAuthenticationOptions(String applicationName) {
			if (isApplicationNameRequired()) {
				ValidationUtils.assertFalse(StringUtils.isEmpty(applicationName), "Application Name is required for authentication mode: " + this.name());
				return getAuthenticationOptionsPrefix() + applicationName;
			}
			return getAuthenticationOptionsPrefix();
		}
	}

	public static final class BloombergSessionOptionsBuilder {

		private final BloombergOperations bloombergOperation;
		private final BloombergRequestHandler<BloombergResponseMessage> requestHandler;
		private final SessionOptions sessionOptions = new SessionOptions();
		private BloombergBPipeAuthenticationMode authenticationMode;
		private String applicationName;
		private boolean bPipe;
		private boolean backOffice;
		private boolean mds;
		private SqlHandlerLocator sqlHandlerLocator;
		private CacheHandler<String, Map<String, String>> cacheHandler;


		private BloombergSessionOptionsBuilder(SessionOptions.ServerAddress[] serverAddresses, BloombergOperations bloombergOperation, BloombergRequestHandler<BloombergResponseMessage> requestHandler) {
			this.sessionOptions.setServerAddresses(serverAddresses);
			this.bloombergOperation = bloombergOperation;
			this.requestHandler = requestHandler;
		}


		public static BloombergSessionOptionsBuilder forBackOffice(SessionOptions.ServerAddress[] serverAddresses, BloombergOperations bloombergOperation, BloombergRequestHandler<BloombergResponseMessage> requestHandler, SqlHandlerLocator sqlHandlerLocator, CacheHandler<String, Map<String, String>> cacheHandler) {
			BloombergSessionOptionsBuilder builder = new BloombergSessionOptionsBuilder(serverAddresses, bloombergOperation, requestHandler);
			builder.withSqlHandlerLocator(sqlHandlerLocator);
			builder.withCacheHandler(cacheHandler);
			builder.backOffice = true;
			return builder;
		}


		public static BloombergSessionOptionsBuilder forBPipe(BloombergBPipeAuthenticationMode authenticationMode, SessionOptions.ServerAddress[] serverAddresses, BloombergOperations bloombergOperation, BloombergRequestHandler<BloombergResponseMessage> requestHandler) {
			BloombergSessionOptionsBuilder builder = new BloombergSessionOptionsBuilder(serverAddresses, bloombergOperation, requestHandler);
			builder.withAuthenticationMode(authenticationMode);
			builder.bPipe = true;
			return builder;
		}


		public static BloombergSessionOptionsBuilder forMds(SessionOptions.ServerAddress[] serverAddresses, BloombergOperations bloombergOperation, BloombergRequestHandler<BloombergResponseMessage> requestHandler) {
			BloombergSessionOptionsBuilder builder = new BloombergSessionOptionsBuilder(serverAddresses, bloombergOperation, requestHandler);
			builder.mds = true;
			return builder;
		}


		public static BloombergSessionOptionsBuilder forSubscription(BloombergBPipeAuthenticationMode authenticationMode, SessionOptions.ServerAddress[] serverAddresses) {
			return forBPipe(authenticationMode, serverAddresses, BloombergOperations.Subscription, null);
		}


		public static BloombergSessionOptionsBuilder forTerminal(SessionOptions.ServerAddress[] serverAddresses, BloombergOperations bloombergOperation, BloombergRequestHandler<BloombergResponseMessage> requestHandler) {
			return new BloombergSessionOptionsBuilder(serverAddresses, bloombergOperation, requestHandler);
		}


		public BloombergSessionOptionsBuilder withAuthenticationMode(BloombergBPipeAuthenticationMode authenticationMode) {
			this.authenticationMode = authenticationMode;
			return this;
		}


		public BloombergSessionOptionsBuilder withApplicationName(String applicationName) {
			this.applicationName = applicationName;
			return this;
		}


		public BloombergSessionOptionsBuilder withSqlHandlerLocator(SqlHandlerLocator sqlHandlerLocator) {
			this.sqlHandlerLocator = sqlHandlerLocator;
			return this;
		}


		public BloombergSessionOptionsBuilder withCacheHandler(CacheHandler<String, Map<String, String>> cacheHandler) {
			this.cacheHandler = cacheHandler;
			return this;
		}


		/**
		 * Set to true to allow overlapping subscriptions to be received with one message containing all users with access to it instead of a unique message per user.
		 */
		public BloombergSessionOptionsBuilder withMultipleCorrelatorsPerMessage(boolean allowMultipleCorrelatorsPerMessage) {
			this.sessionOptions.setAllowMultipleCorrelatorsPerMsg(allowMultipleCorrelatorsPerMessage);
			return this;
		}


		public BloombergSessionOptions build() {
			if (this.authenticationMode != null) {
				this.sessionOptions.setAuthenticationOptions(this.authenticationMode.getAuthenticationOptions(this.applicationName));
			}

			return new BloombergSessionOptions(this);
		}
	}
}
