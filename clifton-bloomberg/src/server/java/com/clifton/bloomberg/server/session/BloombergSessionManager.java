package com.clifton.bloomberg.server.session;

import com.bloomberglp.blpapi.Session;
import com.bloomberglp.blpapi.SessionOptions;
import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.server.BloombergRequestHandler;
import com.clifton.bloomberg.server.session.impl.BloombergBackOfficeSession;
import com.clifton.bloomberg.server.session.impl.BloombergMdsSession;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.sql.SqlHandlerLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;


/**
 * <code>BloombergSessionManager</code> assists in managing {@link BloombergSession}s. This class
 * is responsible for initializing components necessary for establishing sessions, supplying
 * connection options to session instances, and providing sessions based on use.
 *
 * @author NickK
 */
public class BloombergSessionManager {

	/**
	 * Thread safe reference to the Bloomberg {@link Session}s.
	 * Is a map of sessions by operation to avoid long latency requests slowing down short latency requests.
	 */
	private static final Map<BloombergOperations, BloombergSession> bloombergSessionMap = new ConcurrentHashMap<>();

	/**
	 * Thread safe reference to request handlers by operation. This map is used by both BPipe and BBComm sessions.
	 */
	private final Map<BloombergOperations, BloombergRequestHandler<BloombergResponseMessage>> handlerMap = new ConcurrentHashMap<>();

	private String serverAddressListString = "localhost:8194";
	/**
	 * If using BPipe sessions, there are multiple authentication options: USER_ONLY and APPLICATION_ONLY.
	 */
	private String authenticationMode;
	/**
	 * If using a BPipe session with APPLICATION_ONLY authentication mode, an application name is required to define the identity of the session on the BPipe.
	 */
	private String applicationName;
	/**
	 * If using a BPipe session, a session can be created to handle subscriptions - real-time data feeds.
	 */
	private boolean enableSubscriptions;
	/**
	 * Is using BPipe session and subscriptions are enabled, this flag will indicate overlapping security subscriptions can be grouped so security
	 * data can come in one message including the users subscribed instead of a message of security data for each user.
	 */
	private boolean allowMultipleCorrelatorsPerMessage;
	/**
	 * Factory for creating Bloomberg sessions.
	 */
	private BloombergSessionFactory bloombergSessionFactory;

	private SqlHandlerLocator sqlHandlerLocator;

	private CacheHandler<String, Map<String, String>> cacheHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void start() {
		SessionOptions.ServerAddress[] serverAddresses = getServerAddresses();
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(serverAddresses), "Unable to initialize sessions because there are no Server Addresses defined.");

		BiFunction<BloombergOperations, BloombergRequestHandler<BloombergResponseMessage>, BloombergSessionOptions> sessionOptionsFunction;
		BloombergSessionFactory sessionFactory = getBloombergSessionFactory();
		if (StringUtils.isEqual(getApplicationName(), BloombergBackOfficeSession.SESSION_NAME)) {
			LogUtils.info(getClass(), "Initializing BackOffice Sessions.");
			sessionOptionsFunction = (bloombergOperation, bloombergRequestHandler) -> BloombergSessionOptions.BloombergSessionOptionsBuilder.forBackOffice(serverAddresses, bloombergOperation, bloombergRequestHandler, getSqlHandlerLocator(), getCacheHandler()).build();
		}
		else if (StringUtils.isEqual(getApplicationName(), BloombergMdsSession.SESSION_NAME)) {
			LogUtils.info(getClass(), "Initializing MDS Sessions.");
			sessionOptionsFunction = (bloombergOperation, bloombergRequestHandler) -> BloombergSessionOptions.BloombergSessionOptionsBuilder.forMds(serverAddresses, bloombergOperation, bloombergRequestHandler).build();
		}
		else if (StringUtils.isEmpty(getAuthenticationMode())) {
			LogUtils.info(getClass(), "Initialization Terminal Sessions.");
			sessionOptionsFunction = (bloombergOperation, bloombergRequestHandler) -> BloombergSessionOptions.BloombergSessionOptionsBuilder.forTerminal(serverAddresses, bloombergOperation, bloombergRequestHandler).build();
		}
		else {
			BloombergSessionOptions.BloombergBPipeAuthenticationMode authMode = BloombergSessionOptions.BloombergBPipeAuthenticationMode.valueOf(getAuthenticationMode());
			LogUtils.info(getClass(), "Initialization BPipe Sessions using authentication mode " + getAuthenticationMode());
			sessionOptionsFunction = (bloombergOperation, bloombergRequestHandler) -> BloombergSessionOptions.BloombergSessionOptionsBuilder.forBPipe(authMode, serverAddresses, bloombergOperation, bloombergRequestHandler)
					.withApplicationName(getApplicationName())
					.build();
			if (isEnableSubscriptions()) {
				LogUtils.info(getClass(), "Registering session for " + BloombergOperations.Subscription);
				bloombergSessionMap.put(BloombergOperations.Subscription, sessionFactory.createSession(
						BloombergSessionOptions.BloombergSessionOptionsBuilder.forSubscription(authMode, serverAddresses)
								.withApplicationName(getApplicationName())
								.withMultipleCorrelatorsPerMessage(isAllowMultipleCorrelatorsPerMessage())
								.build()));
			}
		}
		this.handlerMap.forEach((bloombergOperation, bloombergRequestHandler) -> {
			LogUtils.info(getClass(), "Registering session for " + bloombergOperation);
			bloombergSessionMap.put(bloombergOperation, sessionFactory.createSession(sessionOptionsFunction.apply(bloombergOperation, bloombergRequestHandler)));
		});
	}


	public void stop() {
		LogUtils.info(getClass(), "Stopping Bloomberg Sessions.");
		bloombergSessionMap.values().forEach(BloombergSession::stop);
	}


	public BloombergSession getSession(BloombergRequestMessage bloombergRequestMessage) {
		BloombergSession bloombergSession = bloombergSessionMap.get(bloombergRequestMessage.getOperation());
		ValidationUtils.assertNotNull(bloombergSession, "This Bloomberg service is unable to process requests for operation [" + bloombergRequestMessage.getOperation() + "].");
		return bloombergSession;
	}


	public void stopSession(BloombergRequestMessage bloombergRequestMessage) {
		BloombergSession bloombergSession = getSession(bloombergRequestMessage);
		bloombergSession.stop();
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////               Helper Methods                 ///////////////
	///////////////////////////////////////////////////////////////////////////


	private SessionOptions.ServerAddress[] getServerAddresses() {
		List<SessionOptions.ServerAddress> serverAddressList = new ArrayList<>();
		if (!StringUtils.isEmpty(getServerAddressListString())) {
			String[] addresses = getServerAddressListString().split(";");
			ArrayUtils.getStream(addresses)
					.filter(a -> !StringUtils.isEmpty(a))
					.map(address -> {
						String[] hostPort = address.split(":");
						if (!StringUtils.isEmpty(hostPort[0]) && !StringUtils.isEmpty(hostPort[1])) {
							return new SessionOptions.ServerAddress(hostPort[0], Integer.valueOf(hostPort[1]));
						}
						return null;
					})
					.filter(Objects::nonNull)
					.forEach(serverAddressList::add);
		}
		return serverAddressList.toArray(new SessionOptions.ServerAddress[0]);
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setters              ///////////////
	///////////////////////////////////////////////////////////////////////////


	public Map<BloombergOperations, BloombergRequestHandler<BloombergResponseMessage>> getHandlerMap() {
		return this.handlerMap;
	}


	@ValueChangingSetter
	public void setHandlerMap(Map<BloombergOperations, BloombergRequestHandler<BloombergResponseMessage>> handlerMap) {
		if (handlerMap != null) {
			this.handlerMap.putAll(handlerMap);
		}
	}


	public String getServerAddressListString() {
		return this.serverAddressListString;
	}


	public void setServerAddressListString(String serverAddressListString) {
		this.serverAddressListString = serverAddressListString;
	}


	public String getAuthenticationMode() {
		return this.authenticationMode;
	}


	public void setAuthenticationMode(String authenticationMode) {
		this.authenticationMode = authenticationMode;
	}


	public String getApplicationName() {
		return this.applicationName;
	}


	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}


	public boolean isEnableSubscriptions() {
		return this.enableSubscriptions;
	}


	public void setEnableSubscriptions(boolean enableSubscriptions) {
		this.enableSubscriptions = enableSubscriptions;
	}


	public boolean isAllowMultipleCorrelatorsPerMessage() {
		return this.allowMultipleCorrelatorsPerMessage;
	}


	public void setAllowMultipleCorrelatorsPerMessage(boolean allowMultipleCorrelatorsPerMessage) {
		this.allowMultipleCorrelatorsPerMessage = allowMultipleCorrelatorsPerMessage;
	}


	public BloombergSessionFactory getBloombergSessionFactory() {
		return this.bloombergSessionFactory;
	}


	public void setBloombergSessionFactory(BloombergSessionFactory bloombergSessionFactory) {
		this.bloombergSessionFactory = bloombergSessionFactory;
	}


	public SqlHandlerLocator getSqlHandlerLocator() {
		return this.sqlHandlerLocator;
	}


	public void setSqlHandlerLocator(SqlHandlerLocator sqlHandlerLocator) {
		this.sqlHandlerLocator = sqlHandlerLocator;
	}


	public CacheHandler<String, Map<String, String>> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, Map<String, String>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
