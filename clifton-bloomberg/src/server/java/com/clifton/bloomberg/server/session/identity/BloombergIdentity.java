package com.clifton.bloomberg.server.session.identity;

import com.bloomberglp.blpapi.Identity;
import com.clifton.bloomberg.messages.request.BloombergRequestIdentity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Objects;


/**
 * <code>BloombergIdentity</code> represents a user or application that can be authenticated and authorized
 * with Bloomberg  for requesting and receiving data. An identity stays active for the life of the session.
 *
 * @author NickK
 */
public class BloombergIdentity extends BaseBloombergCorrelationAwareIdentity implements LabeledObject {

	public enum Type {
		APPLICATION,
		USER
	}

	/**
	 * Properties set after a successful authorization request - stay valid for the life of a Session
	 */
	private Identity identity;

	/**
	 * Properties set to authorize an application or user.
	 * APPLICATION requires name
	 * USER requires authId and IP
	 */
	private final Type type;

	private String name;
	private String ipAddress;
	private BloombergRequestIdentity bloombergRequestIdentity;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private BloombergIdentity(Type type) {
		this.type = type;
	}


	public static BloombergIdentity forApplication(String name) {
		BloombergIdentity bloombergIdentity = new BloombergIdentity(Type.APPLICATION);
		bloombergIdentity.setName(name);
		return bloombergIdentity;
	}


	public static BloombergIdentity forUser(BloombergRequestIdentity requestIdentity) {
		ValidationUtils.assertNotNull(requestIdentity, "Unable to process request because the Bloomberg Request Identity is missing.");
		BloombergIdentity bloombergIdentity = new BloombergIdentity(Type.USER);
		bloombergIdentity.setBloombergRequestIdentity(requestIdentity);
		return bloombergIdentity;
	}


	public Type getType() {
		return this.type;
	}


	/**
	 * A short label to use for uniqueness. An identity with an application name or user ID can only be used once within Bloomberg, thus, it makes it unique.
	 * This label is used for {@link #equals(Object)} and {@link #hashCode()}.
	 */
	@Override
	public String getLabel() {
		return String.join(":", getType().name(), (getType() == Type.APPLICATION ? getName() : getAuthenticationId()));
	}


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("{type=").append(getType());
		if (getType() == Type.APPLICATION) {
			builder.append(", name=").append(getName());
		}
		else {
			builder.append(", authId=").append(getAuthenticationId())
					.append(", ipAddress=").append(getIpAddress());
		}
		return builder.append("}").toString();
	}


	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		else if (!(object instanceof BloombergIdentity)) {
			return false;
		}
		BloombergIdentity toCompare = (BloombergIdentity) object;
		Object authId;
		Object toCompareAuthId;
		if (getType() == Type.APPLICATION) {
			authId = getName();
			toCompareAuthId = toCompare.getName();
		}
		else {
			authId = getAuthenticationId();
			toCompareAuthId = toCompare.getAuthenticationId();
		}
		return CompareUtils.isEqual(getType(), toCompare.getType())
				&& CompareUtils.isEqual(authId, toCompareAuthId);
	}


	@Override
	public int hashCode() {
		return Objects.hash(
				getType(),
				getType() == Type.APPLICATION ? getName() : getAuthenticationId()
		);
	}


	public boolean isAuthorized() {
		return getIdentity() != null && getCorrelationId() != null;
	}


	public boolean isTerminalIdentity() {
		return getUuid() != null && getBloombergRequestIdentity().getTerminalIpAddress() != null;
	}

	///////////////////////////////////////////////////////////////////////////
	////////////////          Convenience Methods            //////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getUuid() {
		return getBloombergRequestIdentity().getUuid();
	}


	public String getAuthenticationId() {
		return getBloombergRequestIdentity().getAuthenticationId();
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setters              ///////////////
	///////////////////////////////////////////////////////////////////////////


	public Identity getIdentity() {
		return this.identity;
	}


	public void setIdentity(Identity identity) {
		this.identity = identity;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getIpAddress() {
		return this.ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public BloombergRequestIdentity getBloombergRequestIdentity() {
		return this.bloombergRequestIdentity;
	}


	@ValueChangingSetter
	public void setBloombergRequestIdentity(BloombergRequestIdentity bloombergRequestIdentity) {
		this.bloombergRequestIdentity = bloombergRequestIdentity;
		setIpAddress(isTerminalIdentity() ? bloombergRequestIdentity.getTerminalIpAddress() : bloombergRequestIdentity.getClientIpAddress());
	}
}
