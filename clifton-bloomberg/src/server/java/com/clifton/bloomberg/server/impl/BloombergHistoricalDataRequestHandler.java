package com.clifton.bloomberg.server.impl;


import com.bloomberglp.blpapi.Datetime;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Message;
import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.bloomberg.server.util.BloombergUtils;
import com.clifton.core.logging.LogUtils;

import java.util.Date;
import java.util.Map;


/**
 * The <code>BloombergHistoricalDataRequestHandler</code> handles the following HistoricalDataResponse model.
 * <HistoricalDataResponse>
 * <responseError attributes (source,code, category, message,subcategory)> occurs 0-1 times
 * <securityData attributes (security,sequence number)> occurs * times (once for each requested security)
 * <fieldData attributes (value,date,relativeDate)> occurs * times (once for each requested field)
 * <fieldExceptions attributes (fieldId, message)> occurs 0-1 times
 * <errorInfo attributes (source,code,category,message,subcategory)>
 * </fieldExceptions>
 * <securityError attributes (source,code,category,message,subcategory)> occurs 0-* times
 * </securityData>
 * </HistoricalDataResponse>
 * <p/>
 * Example: request of "BSX Equity" with Fields "OPEN" and "PX_LAST" on dates "4/29/2010 and 4/30/2010" give this response:
 * <p/>
 * securityData = {
 * security = BSX Equity
 * sequenceNumber = 0
 * fieldData[] = {
 * fieldData = {
 * date = 2010-04-29
 * OPEN = 6.99
 * PX_LAST = 6.96
 * }
 * fieldData = {
 * date = 2010-04-30
 * OPEN = 6.98
 * PX_LAST = 6.88
 * }
 * }
 * }
 */
public class BloombergHistoricalDataRequestHandler extends AbstractBloombergRequestHandler<HistoricalDataResponse> {

	@Override
	protected Class<HistoricalDataResponse> getResponseClass() {
		return HistoricalDataResponse.class;
	}


	@Override
	protected void readResponse(Message msg, HistoricalDataResponse responseMessage) {
		Element securityData = msg.getElement(BloombergUtils.SECURITY_DATA);
		String securityName = securityData.getElementAsString(BloombergUtils.SECURITY);

		BloombergError potentialError = BloombergUtils.populateErrorIfExists(securityData);
		if (potentialError != null) {
			responseMessage.setBloomError(potentialError);
		}

		Element fieldDataArray = securityData.getElement(BloombergUtils.FIELD_DATA);

		for (int i = 0; i < fieldDataArray.numValues(); ++i) {
			Element fieldData = fieldDataArray.getValueAsElement(i);
			Datetime securityDate = fieldData.getElementAsDatetime(BloombergUtils.DATE);
			Date date = securityDate.calendar().getTime();

			ReferenceDataResponse securityResponse = responseMessage.addDate(date);
			Map<String, Object> values = securityResponse.addSecurity(securityName);
			for (int j = 0; j < fieldData.numElements(); ++j) {
				Element field = fieldData.getElement(j);

				if ("date".equals(field.name().toString())) {
					continue;
				}
				if (field.isNull()) {
					continue;
				}

				LogUtils.info(getClass(), field.name() + " = " + field.getValueAsString());
				values.put(field.name().toString(), BloombergUtils.readValue(field));
			}
		}
	}
}
