package com.clifton.bloomberg.server.session.impl;

import com.bloomberglp.blpapi.CorrelationID;
import com.bloomberglp.blpapi.DuplicateCorrelationIDException;
import com.bloomberglp.blpapi.Element;
import com.bloomberglp.blpapi.Event;
import com.bloomberglp.blpapi.EventHandler;
import com.bloomberglp.blpapi.EventQueue;
import com.bloomberglp.blpapi.Identity;
import com.bloomberglp.blpapi.Message;
import com.bloomberglp.blpapi.Request;
import com.bloomberglp.blpapi.Service;
import com.bloomberglp.blpapi.Session;
import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;
import com.clifton.bloomberg.server.BloombergRequestHandler;
import com.clifton.bloomberg.server.session.BloombergCorrelationAware;
import com.clifton.bloomberg.server.session.BloombergSession;
import com.clifton.bloomberg.server.session.BloombergSessionOptions;
import com.clifton.bloomberg.server.session.identity.BloombergIdentity;
import com.clifton.bloomberg.server.util.BloombergUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * <code>BloombergBpipeSession</code> represents a Bloomberg BPipe {@link Session} wrapper
 * that supports authentication and authorization of users requests.
 *
 * @author NickK
 */
public class BloombergBpipeSession extends BloombergDefaultSession {

	/*
	 * Identity map could be a set, but is used for lookup purposes based on the identity's unique user properties.
	 * Thus, a map is used with the object used as key and value, so we do not have to iterate a set.
	 * Identities are valid for the lifetime of a session as long as the user is logged in to Bloomberg.
	 * The application should listen for AUTHORIZATION_STATUS events for AuthorizationRevoked messages.
	 */
	protected final Map<BloombergIdentity, BloombergIdentity> identityMap = new ConcurrentHashMap<>();

	/**
	 * If this is a BPipe session, this holds the application's authentication/authorization details.
	 */
	protected final AtomicReference<BloombergIdentity> applicationIdentity = new AtomicReference<>();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BloombergBpipeSession(BloombergSessionOptions bloombergSessionOptions) {
		super(bloombergSessionOptions);
	}


	public BloombergBpipeSession(BloombergSessionOptions bloombergSessionOptions, Function<BloombergSession, EventHandler> eventHandlerFunction) {
		super(bloombergSessionOptions, eventHandlerFunction);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void preStart() {
		// Overridden to prevent BBCOMM start in parent
	}


	/**
	 * Authenticates the BPipe application upon starting.
	 */
	@Override
	protected void postStart() {
		// If there is an application name, authenticate the application
		Optional.ofNullable(this.applicationIdentity.updateAndGet(currentIdentity -> {
			if (currentIdentity == null) {
				if (getOptions().getAuthenticationMode().isApplicationNameRequired() && !StringUtils.isEmpty(getOptions().getApplicationName())) {
					// initial start, create the identity
					return BloombergIdentity.forApplication(getOptions().getApplicationName());
				}
				return null;
			}
			else {
				// subsequent start attempt, clear the existing identity details
				cancel(currentIdentity.getCorrelationId());
				currentIdentity.setCorrelationId(null);
				return currentIdentity;
			}
		})).ifPresent(this::authenticate);
		authenticateCachedIdentities();
	}


	/**
	 * Cancels all cached {@link Identity}s before stopping the session.
	 */
	@Override
	protected void preStop() {
		// cancel cached identities before stopping session
		cancelCachedIdentities();
	}


	@Override
	public BloombergResponseMessage submitSynchronousRequest(BloombergRequestMessage bloombergRequest) throws Exception {
		// Verify started
		start();

		Service service = getService(bloombergRequest.getServiceUrl());
		Request request = service.createRequest(bloombergRequest.getOperation().toString());

		// build Bloomberg request message
		BloombergRequestHandler<BloombergResponseMessage> requestHandler = getOptions().getRequestHandler();
		requestHandler.populateRequest(request, bloombergRequest);

		LogUtils.debug(getClass(), request.toString());

		EventQueue eventQueue = new EventQueue();
		// This is a BPipe service and an Identity is required
		Identity identity = (bloombergRequest.getIdentity() == null)
				? (getApplicationIdentity() != null ? getApplicationIdentity().getIdentity() : null)
				: authenticate(BloombergIdentity.forUser(bloombergRequest.getIdentity()));
		AssertUtils.assertNotNull(identity, "The request did not include a BPipe identity and the Bloomberg BPipe Service is not configured to use an application identity.");
		getSession().sendRequest(request, identity, eventQueue, null);
		return BloombergUtils.handleSynchronousSingleRequest(eventQueue, requestHandler, bloombergRequest);
	}


	/**
	 * Authenticates the user based on credentials defined on the provided {@link BloombergIdentity}.
	 *
	 * @throws RuntimeException if authorization fails.
	 */
	@Override
	public Identity authenticate(BloombergIdentity bloombergIdentity) {
		// Verify started
		start();

		BloombergIdentity cachedIdentity = this.identityMap.get(bloombergIdentity);
		if (cachedIdentity != null && cachedIdentity.isAuthorized()) {
			return cachedIdentity.getIdentity();
		}

		// We only want one authentication attempt per user. Atomically authenticate and add the identity to the cache.
		// Locking should not block all users at the same time, but this is a potential area of contention.
		cachedIdentity = this.identityMap.compute(bloombergIdentity, (blmIdentity, currentValue) -> {
			if (currentValue != null && currentValue.isAuthorized()) {
				return currentValue;
			}
			// authenticate the user
			Identity identity = getSession().createIdentity();
			Request authorizationRequest = createAuthorizationRequest(bloombergIdentity);
			CorrelationID correlationId = new CorrelationID(bloombergIdentity);
			Exception error = null;
			try {
				EventQueue eventQueue = new EventQueue();
				getSession().sendAuthorizationRequest(authorizationRequest, identity, eventQueue, correlationId);
				Event event = eventQueue.nextEvent();
				if (event.eventType() == Event.EventType.RESPONSE ||
						event.eventType() == Event.EventType.PARTIAL_RESPONSE ||
						event.eventType() == Event.EventType.REQUEST_STATUS) {
					for (Message message : event) {
						LogUtils.debug(getClass(), "Authorization message: " + message);

						if (message.messageType().equals(BloombergUtils.AUTHORIZATION_SUCCESS)) {
							bloombergIdentity.setCorrelationId(correlationId);
							bloombergIdentity.setIdentity(identity);
							return bloombergIdentity;
						}
						else if (message.messageType() == BloombergUtils.AUTHORIZATION_FAILURE
								|| message.messageType() == BloombergUtils.REQUEST_FAILURE) {
							BloombergError authorizationFailure = new BloombergError();
							authorizationFailure.setErrorType(BloombergError.BloombergErrorTypes.AUTHORIZATION);
							BloombergUtils.setErrorInfoFields(message.getElement(BloombergUtils.REASON), authorizationFailure);
							throw new BloombergRequestException(authorizationFailure);
						}
					}
					LogUtils.warn(LogCommand.ofMessage(getClass(), "Authorization event received: ", event));
				}
				else {
					LogUtils.warn(LogCommand.ofMessage(getClass(), "Received unknown authorization event: ", event));
				}
			}
			catch (DuplicateCorrelationIDException d) {
				// For some reason we get a duplicate correlation from Bloomberg. Either BPipe does not honor our correlation cancellation or it is a timing issue.
				if (bloombergIdentity.getCorrelationId() == null) {
					bloombergIdentity.setCorrelationId(correlationId);
				}
				LogUtils.warn(getClass(), "Encountered " + (bloombergIdentity.getIdentity() == null ? "non" : "") + "reusable duplicate Bloomberg Correlation for " + bloombergIdentity + ": " + d.description());
				if (bloombergIdentity.getIdentity() != null) {
					// identity should be valid and can use it
					return bloombergIdentity;
				}
				// attempt to cancel the bloomberg identity to clean up and throw the exception to be handled
				cancelBloombergCorrelation(bloombergIdentity);
				error = d;
			}
			catch (Exception e) {
				if (e instanceof InterruptedException) {
					Thread.currentThread().interrupt();
				}
				error = e;
			}
			String errorMessage = "Authorization failed for [" + bloombergIdentity + "]";
			if (error == null) {
				throw new RuntimeException(errorMessage);
			}
			else if (error instanceof BloombergRequestException) {
				throw new RuntimeException(errorMessage + ": " + error.getMessage());
			}
			LogUtils.warn(getClass(), errorMessage, error);
			throw new RuntimeException(errorMessage, BloombergUtils.convertBloombergExceptionForMessaging(errorMessage, error));
		});

		return cachedIdentity.getIdentity();
	}


	/**
	 * Returns a list of {@link BloombergIdentity}s that this session is aware of that are entitled to
	 * see the provided "eidData" {@link Element} and {@link Service}.
	 */
	@Override
	public List<BloombergIdentity> getEntitledUserList(Element entitlements, Service service) {
		if (entitlements == null) {
			return Collections.emptyList();
		}
		return this.identityMap.keySet().stream()
				// remove this application and those users that are not entitled
				.filter(bloombergIdentity -> !bloombergIdentity.equals(getApplicationIdentity())
						&& bloombergIdentity.getIdentity().hasEntitlements(entitlements, service))
				.collect(Collectors.toList());
	}


	/**
	 * Returns the {@link BloombergIdentity} for this BPipe application session.
	 */
	protected BloombergIdentity getApplicationIdentity() {
		return this.applicationIdentity.get();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private Request createAuthorizationRequest(BloombergIdentity bloombergIdentity) {
		Service authService = getService(BloombergUtils.API_AUTH_SVC_NAME);
		Request authRequest = authService.createAuthorizationRequest();
		switch (bloombergIdentity.getType()) {
			case APPLICATION: {
				String token = generateApplicationToken();
				authRequest.set("token", token);
				break;
			}
			case USER: {
				authRequest.set("authId", bloombergIdentity.getAuthenticationId());
				authRequest.set("ipAddress", bloombergIdentity.getIpAddress());
				break;
			}
		}
		return authRequest;
	}


	private String generateApplicationToken() {
		Exception error = null;
		try {
			EventQueue tokenEventQueue = new EventQueue();
			getSession().generateToken(new CorrelationID(), tokenEventQueue);
			Event event = tokenEventQueue.nextEvent();
			if (event.eventType() == Event.EventType.TOKEN_STATUS ||
					event.eventType() == Event.EventType.REQUEST_STATUS) {
				for (Message message : event) {
					LogUtils.debug(getClass(), "Generate token message: " + message);

					if (message.messageType() == BloombergUtils.TOKEN_SUCCESS) {
						return message.getElementAsString(BloombergUtils.TOKEN);
					}
				}
			}
		}
		catch (Exception e) {
			if (e instanceof InterruptedException) {
				Thread.currentThread().interrupt();
			}
			error = e;
		}
		String errorMessage = "Failed to receive an application token for application authorization request.";
		if (error == null) {
			throw new RuntimeException(errorMessage);
		}
		throw new RuntimeException(errorMessage, BloombergUtils.convertBloombergExceptionForMessaging(errorMessage, error));
	}


	/**
	 * Cancels cached application and user identities to invalidate authorized users.
	 */
	protected void cancelCachedIdentities() {
		LogUtils.info(getClass(), "Canceling cached identities.");
		// Cancel and remove applicable entries
		this.identityMap.entrySet().removeIf(entry -> cancelBloombergCorrelation(entry.getKey()));
	}


	/**
	 * Attempts to authenticate cached application and user identities against an opened session.
	 * Identities that fail to authenticate are removed from cache.
	 */
	private void authenticateCachedIdentities() {
		if (isSessionOpen()) {
			this.identityMap.keySet().stream()
					.filter(bloombergIdentity -> bloombergIdentity != null && !bloombergIdentity.equals(getApplicationIdentity()))
					.forEach(this::authenticate);
		}
	}


	/**
	 * Cancels an active {@link CorrelationID} for the provided {@link BloombergCorrelationAware} object.
	 * After the correlation is canceled, the correlation will be set to null on the provided object.
	 * <p>
	 * Returns true if the current correlation aware object is canceled or no longer active.
	 */
	protected boolean cancelBloombergCorrelation(BloombergCorrelationAware bloombergCorrelationAware) {
		if (bloombergCorrelationAware != null && bloombergCorrelationAware.getCorrelationId() != null) {
			// cancel the authorized identity and clear the value to be reset
			try {
				getSession().cancel(bloombergCorrelationAware.getCorrelationId());
			}
			catch (Exception e) {
				// the session may be down
				LogUtils.warn(getClass(), "Failed to cancel correlation: " + bloombergCorrelationAware, e);
			}
			finally {
				bloombergCorrelationAware.setCorrelationId(null);
			}
		}
		return true;
	}
}
