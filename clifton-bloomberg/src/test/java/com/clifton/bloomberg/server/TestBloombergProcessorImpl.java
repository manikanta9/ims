package com.clifton.bloomberg.server;


import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.core.messaging.jms.synchronous.SynchronousProcessor;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousResponseMessage;

import java.util.Map;


public class TestBloombergProcessorImpl implements SynchronousProcessor<SynchronousRequestMessage, SynchronousResponseMessage> {

	@Override
	public SynchronousResponseMessage process(SynchronousRequestMessage synchronousRequestMessage) {

		ReferenceDataRequest request = (ReferenceDataRequest) synchronousRequestMessage;
		ReferenceDataResponse response = new ReferenceDataResponse();

		for (String security : request.getBloombergRequestData().getElement(BloombergRequestMessage.SECURITIES_KEY).getRequestData()) {
			Map<String, Object> securityData = response.addSecurity(security);

			securityData.put("TICKER", security.substring(0, security.length() / 2));
			securityData.put("BID", 100.0D);
			securityData.put("PX_LAST", 100.0D);
			securityData.put("ASK", 100.0D);
			securityData.put("ID_CUSIP", security);
		}

		return response;
	}
}
