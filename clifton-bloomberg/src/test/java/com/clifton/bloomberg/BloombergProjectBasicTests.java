package com.clifton.bloomberg;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author NickK
 */

@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BloombergProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "bloomberg";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("javax.annotation.Resource");
		imports.add("java.sql.Types");
		imports.add("java.sql.ResultSet");
		imports.add("java.sql.SQLException");
		imports.add("com.bloomberglp.");
		imports.add("org.springframework.jms.listener.");
		imports.add("org.springframework.jdbc.");
	}


	@Override
	public Set<String> getEnumAllowedNamesOverride() {
		Set<String> enumOverrides = new HashSet<>();
		enumOverrides.add("com.clifton.bloomberg.messages.BloombergPeriodicity");
		return enumOverrides;
	}
}
