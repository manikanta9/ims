package com.clifton.bloomberg.messages.request;

import com.clifton.bloomberg.client.BloombergMarketDataProvider;
import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.converter.template.TemplateConverter.TemplateValueRetriever;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;


public class BloombergRequestMessageTests {

	private enum ExpressionVariables {
		DATE, SETTLEMENT_DATE
	}


	@SuppressWarnings("rawtypes")
	@Test
	public void testOverridesDateFormatting() {
		BloombergMarketDataProvider b = new BloombergMarketDataProvider();
		TemplateConverter tc = new FreemarkerTemplateConverter();

		Map<Enum<?>, TemplateValueRetriever> expressionParameters = new HashMap<>();
		expressionParameters.put(ExpressionVariables.DATE, b.new BasicDateRetriever(DateUtils.toDate("11/05/2012")));

		ReferenceDataRequest request = new ReferenceDataRequest();
		Assertions.assertEquals(0, request.getOverrides().size());
		request.addOverrides(tc.convertOverrides("SETTLE_DT=${DATE}", expressionParameters));
		Assertions.assertEquals("{SETTLE_DT=20121105}", request.getOverrides().toString());

		expressionParameters.put(ExpressionVariables.DATE, b.new BasicDateRetriever(DateUtils.toDate("11/05/2012")));
		request = new ReferenceDataRequest();
		Assertions.assertEquals(0, request.getOverrides().size());
		request.addOverrides(tc.convertOverrides("SETTLE_DT=${DATE}", expressionParameters));
		Assertions.assertEquals("{SETTLE_DT=20121105}", request.getOverrides().toString());

		expressionParameters.put(ExpressionVariables.DATE, b.new BasicDateRetriever(DateUtils.toDate("11/05/2012")));
		request = new ReferenceDataRequest();
		Assertions.assertEquals(0, request.getOverrides().size());
		request.addOverrides(tc.convertOverrides("SETTLE_DT=${DATE},SW_CURVE_DT=${DATE}", expressionParameters));
		Assertions.assertEquals("{SW_CURVE_DT=20121105, SETTLE_DT=20121105}", request.getOverrides().toString());
	}


	@SuppressWarnings("rawtypes")
	@Test
	public void testOverridesDateFormatting_ERROR() {
		TestUtils.expectException(ValidationException.class, () -> {
			BloombergMarketDataProvider b = new BloombergMarketDataProvider();
			TemplateConverter tc = new FreemarkerTemplateConverter();

			Map<Enum<?>, TemplateValueRetriever> expressionParameters = new HashMap<>();
			expressionParameters.put(ExpressionVariables.DATE, b.new BasicDateRetriever(DateUtils.toDate("11/05/2012")));

			ReferenceDataRequest request = new ReferenceDataRequest();
			Assertions.assertEquals(0, request.getOverrides().size());

			request.addOverrides(tc.convertOverrides("SETTLE_DT${DATE}", expressionParameters));
		});
	}
}
