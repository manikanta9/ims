package com.clifton.bloomberg.client;

import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.system.SystemTestObjectFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
@Disabled
public class BloombergTests {

	@Resource
	private MarketDataProviderLocator marketDataProviderLocator;

	@Mock
	private MarketDataSourceService marketDataSourceService;


	@SuppressWarnings("rawtypes")
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		MarketDataSource dataSource = new MarketDataSource();
		dataSource.setName("Bloomberg");

		Mockito.when(this.marketDataSourceService.getMarketDataSourceByName(ArgumentMatchers.anyString())).thenReturn(new MarketDataSource());

		((BloombergMarketDataProvider) this.marketDataProviderLocator.locate(dataSource)).setMarketDataSourceService(this.marketDataSourceService);
		((BloombergMarketDataProvider) this.marketDataProviderLocator.locate(dataSource)).setTemplateConverter(new FreemarkerTemplateConverter());
	}


	@Test
	public void testMarketDataProviderLocator_for_Bloomberg() {
		// make sure bloomberg market data provider is properly registered
		MarketDataSource dataSource = new MarketDataSource();
		dataSource.setName("Bloomberg");

		MarketDataProvider<MarketDataValue> marketDataProvider = this.marketDataProviderLocator.locate(dataSource);
		Assertions.assertNotNull(marketDataProvider);
	}


	@Test
	public void testPopulateMarketDataValueFromBloombergResponseValueExpression() {
		try {
			Map<String, Map<String, Object>> values = new HashMap<>();
			Map<String, Object> variables = new HashMap<>();
			variables.put("SW_VAL_PREMIUM", 100.0);
			variables.put("SW_PAY_NOTL_AMT", 100.0);

			values.put("ESU3", variables);

			ReferenceDataResponse referenceDataResponse = new ReferenceDataResponse();
			referenceDataResponse.setValues(values);

			InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "ESU3", InvestmentType.FUTURES, new BigDecimal("2"), "USD");

			MarketDataSource dataSource = new MarketDataSource();
			dataSource.setName("Bloomberg");
			MarketDataProvider<MarketDataValue> marketDataProvider = this.marketDataProviderLocator.locate(dataSource);

			MarketDataField dataField = new MarketDataField();
			dataField.setName("Open Price");
			dataField.setDecimalPrecision(1);
			dataField.setValueExpression("(${SW_VAL_PREMIUM} + ${SW_PAY_NOTL_AMT}) / ${SW_PAY_NOTL_AMT} / ${investmentSecurity.instrument.priceMultiplier}");
			dataField.setExternalFieldName("SW_VAL_PREMIUM");
			dataField.setDataType(SystemTestObjectFactory.DECIMAL);

			Method method = marketDataProvider.getClass().getDeclaredMethod("populateMarketDataValueFromBloombergResponse", ReferenceDataResponse.class, MarketDataField.class, Date.class,
					InvestmentSecurity.class);
			method.setAccessible(true);
			MarketDataValue value = (MarketDataValue) method.invoke(marketDataProvider, referenceDataResponse, dataField, new Date(), security);

			Assertions.assertTrue(value.getMeasureValue().compareTo(new BigDecimal(1)) == 0);
		}
		catch (Exception e) {
			Assertions.fail(e.getLocalizedMessage());
		}
	}


	@Test
	public void testOverrideExpression() {
		MarketDataSource dataSource = new MarketDataSource();
		dataSource.setName("Bloomberg");
		Date date = DateUtils.toDate("3/12/2012");

		String expression = "SETTLE_DT=${SETTLEMENT_DATE},SW_CURVE_DT=${DATE},CDS_MKT_TYPE=M";

		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "ESU3", InvestmentType.FUTURES, new BigDecimal("2"), "USD");

		Method method;
		try {
			method = (this.marketDataProviderLocator.locate(dataSource)).getClass().getDeclaredMethod("convertOverrides", String.class, InvestmentSecurity.class, Date.class);

			method.setAccessible(true);
			String result = (String) method.invoke(this.marketDataProviderLocator.locate(dataSource), expression, security, date);

			Assertions.assertEquals("SETTLE_DT=20120313,SW_CURVE_DT=20120312,CDS_MKT_TYPE=M", result);
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail(e.getLocalizedMessage());
		}
	}


	@Test
	public void testMidCalculated() {
		String valueExp = "<#if PX_BID??>(${PX_ASK}+${PX_BID})/2<#else>${PX_ASK}</#if>";
		try {
			Map<String, Map<String, Object>> values = new HashMap<>();
			Map<String, Object> variables = new HashMap<>();
			variables.put("PX_ASK", 5.0);
			variables.put("PX_BID", 15.0);

			values.put("ESU3", variables);

			ReferenceDataResponse referenceDataResponse = new ReferenceDataResponse();
			referenceDataResponse.setValues(values);

			InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(18356, "ESU3", InvestmentType.FUTURES, new BigDecimal("2"), "USD");

			MarketDataSource dataSource = new MarketDataSource();
			dataSource.setName("Bloomberg");
			MarketDataProvider<MarketDataValue> marketDataProvider = this.marketDataProviderLocator.locate(dataSource);

			MarketDataField dataField = new MarketDataField();
			dataField.setName("Mid - Calculated");
			dataField.setDecimalPrecision(10);
			dataField.setValueExpression(valueExp);
			dataField.setExternalFieldName("PX_ASK");
			dataField.setDataType(SystemTestObjectFactory.DECIMAL);

			Method method = marketDataProvider.getClass().getDeclaredMethod("populateMarketDataValueFromBloombergResponse", ReferenceDataResponse.class, MarketDataField.class, Date.class,
					InvestmentSecurity.class);
			method.setAccessible(true);
			MarketDataValue value = (MarketDataValue) method.invoke(marketDataProvider, referenceDataResponse, dataField, new Date(), security);

			Assertions.assertTrue(value.getMeasureValue().compareTo(new BigDecimal(10)) == 0);
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail(e.getLocalizedMessage());
		}
	}
}
