package com.clifton.bloomberg.client;

import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Map;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ClientTests {

	@Resource
	private BloombergDataService bloombergDataService;


	@Test
	public void testClient() throws BloombergRequestException {

		ReferenceDataRequest referenceDataRequest = new ReferenceDataRequest("BID", "PX_LAST", "ASK", "ID_CUSIP", "TICKER");
		referenceDataRequest.addSecurity("TEST45");

		ReferenceDataResponse response = this.bloombergDataService.getReferenceData(referenceDataRequest);
		Assertions.assertEquals(response.getValues().size(), 1);

		for (Map.Entry<String, Map<String, Object>> securitySymbolEntry : response.getValues().entrySet()) {
			Map<String, Object> fields = securitySymbolEntry.getValue();
			Assertions.assertEquals("TEST45", securitySymbolEntry.getKey());
			Assertions.assertTrue(!fields.keySet().isEmpty());
			for (Map.Entry<String, Object> stringObjectEntry : fields.entrySet()) {
				if ("BID".equals(stringObjectEntry.getKey())) {
					Assertions.assertEquals(100.0, stringObjectEntry.getValue());
				}
				else if ("ASK".equals(stringObjectEntry.getKey())) {
					Assertions.assertEquals(100.0, stringObjectEntry.getValue());
				}
				else if ("PX_LAST".equals(stringObjectEntry.getKey())) {
					Assertions.assertEquals(100.0, stringObjectEntry.getValue());
				}
				else if ("ID_CUSIP".equals(stringObjectEntry.getKey())) {
					Assertions.assertEquals("TEST45", stringObjectEntry.getValue().toString());
				}
				else if ("TICKER".equals(stringObjectEntry.getKey())) {
					Assertions.assertEquals("TES", stringObjectEntry.getValue().toString());
				}
			}
		}
	}
}
