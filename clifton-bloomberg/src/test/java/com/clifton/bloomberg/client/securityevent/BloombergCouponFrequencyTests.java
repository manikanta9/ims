package com.clifton.bloomberg.client.securityevent;


import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


public class BloombergCouponFrequencyTests {

	////////////////////////////////////////////////////////////////////////////
	//////////////////           ANNUAL             ////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAnnualNextPeriodEndDate() {
		Date nextDate = BloombergCouponFrequencies.ANNUAL.getNextPeriodEndDate(DateUtils.toDate("2/28/2011"));
		Assertions.assertEquals(DateUtils.toDate("2/28/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.ANNUAL.getNextPeriodEndDate(DateUtils.toDate("2/28/2012"));
		Assertions.assertEquals(DateUtils.toDate("2/27/2013"), nextDate);

		nextDate = BloombergCouponFrequencies.ANNUAL.getNextPeriodEndDate(DateUtils.toDate("03/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("03/14/2011"), nextDate);
	}


	@Test
	public void testAnnualPeriodStartEndDate() {
		Date nextDate = BloombergCouponFrequencies.ANNUAL.getPeriodStartDate(DateUtils.toDate("2/28/2011"));
		Assertions.assertEquals(DateUtils.toDate("3/1/2010"), nextDate);

		nextDate = BloombergCouponFrequencies.ANNUAL.getPeriodStartDate(DateUtils.toDate("2/28/2012"));
		Assertions.assertEquals(DateUtils.toDate("3/1/2011"), nextDate);

		nextDate = BloombergCouponFrequencies.ANNUAL.getPeriodStartDate(DateUtils.toDate("03/14/2010"));
		Assertions.assertEquals(DateUtils.toDate("03/15/2009"), nextDate);
	}


	@Test
	public void testAnnualNextPayDate() {
		Date nextDate = BloombergCouponFrequencies.ANNUAL.getNextPayDate(DateUtils.toDate("2/28/2011"));
		Assertions.assertEquals(DateUtils.toDate("2/29/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.ANNUAL.getNextPayDate(nextDate);
		Assertions.assertEquals(DateUtils.toDate("2/28/2013"), nextDate);

		nextDate = BloombergCouponFrequencies.ANNUAL.getNextPayDate(DateUtils.toDate("03/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("03/15/2011"), nextDate);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////////////           SEMIANNUAL         ////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSemiAnnualNextPeriodEndDate() {
		// 912828QZ6
		Date nextDate = BloombergCouponFrequencies.SEMIANNUAL.getNextPeriodEndDate(DateUtils.toDate("11/30/2011"));
		Assertions.assertEquals(DateUtils.toDate("5/30/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getNextPeriodEndDate(DateUtils.toDate("5/31/2012"));
		Assertions.assertEquals(DateUtils.toDate("11/29/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getNextPeriodEndDate(DateUtils.toDate("2/1/2006"));
		Assertions.assertEquals(DateUtils.toDate("7/31/2006"), nextDate);
	}


	@Test
	public void testSemiAnnualPeriodStartDate() {
		Date nextDate = BloombergCouponFrequencies.SEMIANNUAL.getPeriodStartDate(DateUtils.toDate("11/29/2011"));
		Assertions.assertEquals(DateUtils.toDate("5/31/2011"), nextDate);

		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getPeriodStartDate(DateUtils.toDate("05/30/2012"));
		Assertions.assertEquals(DateUtils.toDate("11/30/2011"), nextDate);

		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getPeriodStartDate(DateUtils.toDate("05/31/2010"));
		Assertions.assertEquals(DateUtils.toDate("12/1/2009"), nextDate);

		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getPeriodStartDate(DateUtils.toDate("05/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("11/16/2009"), nextDate);
	}


	@Test
	public void testSemiAnnualNextPayDate() {
		// 912828QZ6
		Date nextDate = BloombergCouponFrequencies.SEMIANNUAL.getNextPayDate(DateUtils.toDate("11/30/2011"));
		Assertions.assertEquals(DateUtils.toDate("05/31/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getNextPayDate(nextDate);
		Assertions.assertEquals(DateUtils.toDate("11/30/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getNextPayDate(nextDate);
		Assertions.assertEquals(DateUtils.toDate("05/31/2013"), nextDate);

		// 912828LM0
		nextDate = BloombergCouponFrequencies.SEMIANNUAL.getNextPayDate(DateUtils.toDate("03/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("09/15/2010"), nextDate);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////////////           QUARTERLY          ////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testQuarterlyNextPeriodEndDate() {
		BloombergCouponFrequencies freq = BloombergCouponFrequencies.QUARTERLY;
		Date nextDate = freq.getNextPeriodEndDate(DateUtils.toDate("1/1/2006"));
		Assertions.assertEquals(31, DateUtils.getDayOfMonth(nextDate));
		Assertions.assertEquals(3, DateUtils.getMonthOfYear(nextDate)); //month off one
		Assertions.assertEquals(2006, DateUtils.getYear(nextDate));
	}


	@Test
	public void testQuarterlyPeriodStartDate() {
		Date nextDate = BloombergCouponFrequencies.QUARTERLY.getPeriodStartDate(DateUtils.toDate("11/29/2011"));
		Assertions.assertEquals(DateUtils.toDate("8/31/2011"), nextDate);

		nextDate = BloombergCouponFrequencies.QUARTERLY.getPeriodStartDate(DateUtils.toDate("05/30/2012"));
		Assertions.assertEquals(DateUtils.toDate("2/29/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.QUARTERLY.getPeriodStartDate(DateUtils.toDate("05/31/2010"));
		Assertions.assertEquals(DateUtils.toDate("03/1/2010"), nextDate);

		nextDate = BloombergCouponFrequencies.QUARTERLY.getPeriodStartDate(DateUtils.toDate("05/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("2/16/2010"), nextDate);
	}


	@Test
	public void testQuarterlyNextPayDate() {
		Date nextDate = BloombergCouponFrequencies.QUARTERLY.getNextPayDate(DateUtils.toDate("2/29/2012"));
		Assertions.assertEquals(DateUtils.toDate("5/31/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.QUARTERLY.getNextPayDate(nextDate);
		Assertions.assertEquals(DateUtils.toDate("8/31/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.QUARTERLY.getNextPayDate(nextDate);
		Assertions.assertEquals(DateUtils.toDate("11/30/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.QUARTERLY.getNextPayDate(nextDate);
		Assertions.assertEquals(DateUtils.toDate("2/28/2013"), nextDate);

		nextDate = BloombergCouponFrequencies.QUARTERLY.getNextPayDate(DateUtils.toDate("03/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("06/15/2010"), nextDate);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////////////           MONTHLY            ////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMonthlyNextPeriodEndDate() {
		BloombergCouponFrequencies freq = BloombergCouponFrequencies.MONTHLY;
		Date nextDate = freq.getNextPeriodEndDate(DateUtils.toDate("1/1/2006"));
		Assertions.assertEquals(31, DateUtils.getDayOfMonth(nextDate));
		Assertions.assertEquals(1, DateUtils.getMonthOfYear(nextDate)); //month off one
		Assertions.assertEquals(2006, DateUtils.getYear(nextDate));
	}


	@Test
	public void testMonthlyNextPeriodEndDate_Feb() {
		BloombergCouponFrequencies freq = BloombergCouponFrequencies.MONTHLY;
		Date nextDate = freq.getNextPeriodEndDate(DateUtils.toDate("2/1/2006"));
		Assertions.assertEquals(28, DateUtils.getDayOfMonth(nextDate));
		Assertions.assertEquals(2, DateUtils.getMonthOfYear(nextDate)); //month off one
		Assertions.assertEquals(2006, DateUtils.getYear(nextDate));
	}


	@Test
	public void testMonthlyPeriodStartDate() {
		Date nextDate = BloombergCouponFrequencies.MONTHLY.getPeriodStartDate(DateUtils.toDate("11/29/2011"));
		Assertions.assertEquals(DateUtils.toDate("10/31/2011"), nextDate);

		nextDate = BloombergCouponFrequencies.MONTHLY.getPeriodStartDate(DateUtils.toDate("05/30/2012"));
		Assertions.assertEquals(DateUtils.toDate("4/30/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.MONTHLY.getPeriodStartDate(DateUtils.toDate("05/31/2010"));
		Assertions.assertEquals(DateUtils.toDate("05/1/2010"), nextDate);

		nextDate = BloombergCouponFrequencies.MONTHLY.getPeriodStartDate(DateUtils.toDate("05/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("4/16/2010"), nextDate);
	}


	@Test
	public void testMonthlyNextPayDate() {
		Date nextDate = BloombergCouponFrequencies.MONTHLY.getNextPayDate(DateUtils.toDate("2/29/2012"));
		Assertions.assertEquals(DateUtils.toDate("3/31/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.MONTHLY.getNextPayDate(nextDate);
		Assertions.assertEquals(DateUtils.toDate("4/30/2012"), nextDate);

		nextDate = BloombergCouponFrequencies.MONTHLY.getNextPayDate(DateUtils.toDate("1/31/2013"));
		Assertions.assertEquals(DateUtils.toDate("2/28/2013"), nextDate);

		nextDate = BloombergCouponFrequencies.MONTHLY.getNextPayDate(DateUtils.toDate("03/15/2010"));
		Assertions.assertEquals(DateUtils.toDate("04/15/2010"), nextDate);
	}
}
