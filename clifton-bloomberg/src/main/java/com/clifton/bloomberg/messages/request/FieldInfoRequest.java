package com.clifton.bloomberg.messages.request;


import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.bloomberg.messages.response.BloombergResponseMessage;


public class FieldInfoRequest extends BloombergRequestMessage {

	private final BloombergRequestData bloombergRequestData;
	private String serviceUrl = "//blp/apiflds";
	private final BloombergOperations operation = BloombergOperations.FieldInfoRequest;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FieldInfoRequest() {
		this.bloombergRequestData = new BloombergRequestData();
		this.bloombergRequestData.addElement(new BloombergRequestElement("id"));
		this.bloombergRequestData.addElement(new BloombergRequestElement("returnFieldDocumentation"));
	}


	@Override
	public Class<?> getResponseClass() {
		return BloombergResponseMessage.class;
	}


	@Override
	public String getServiceUrl() {
		return this.serviceUrl;
	}


	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}


	@Override
	public BloombergOperations getOperation() {
		return this.operation;
	}


	@Override
	public BloombergRequestData getBloombergRequestData() {
		return this.bloombergRequestData;
	}
}
