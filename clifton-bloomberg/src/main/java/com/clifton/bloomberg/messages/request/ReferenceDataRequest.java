package com.clifton.bloomberg.messages.request;


import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;

import java.util.List;


/**
 * The <code>ReferenceDataRequest</code> is a model of the structure for a ReferenceDataRequest in Bloomberg.
 * It represents latest as opposed to historic data.
 */
public class ReferenceDataRequest extends BloombergRequestMessage {

	private final BloombergRequestData bloombergRequestData;
	private String serviceUrl = "//blp/refdata";
	private final BloombergOperations operation = BloombergOperations.ReferenceDataRequest;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReferenceDataRequest() {
		this.bloombergRequestData = new BloombergRequestData();
		this.bloombergRequestData.addElement(new BloombergRequestElement(BloombergRequestMessage.SECURITIES_KEY));
		this.bloombergRequestData.addElement(new BloombergRequestElement(BloombergRequestMessage.FIELDS_KEY));
	}


	public ReferenceDataRequest(String... fields) {
		this();
		BloombergRequestElement element = getBloombergRequestData().getElement(BloombergRequestMessage.FIELDS_KEY);
		for (String field : fields) {
			element.appendRequestValue(field);
		}
	}


	@Override
	public Class<?> getResponseClass() {
		return HistoricalDataResponse.class;
	}


	@Override
	public String getServiceUrl() {
		return this.serviceUrl;
	}


	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}


	@Override
	public BloombergOperations getOperation() {
		return this.operation;
	}


	@Override
	public BloombergRequestData getBloombergRequestData() {
		return this.bloombergRequestData;
	}


	public void addSecurity(String security) {
		BloombergRequestElement element = getBloombergRequestData().getElement(BloombergRequestMessage.SECURITIES_KEY);
		if (element.getRequestData().size() >= 10) {
			throw new RuntimeException("Cannot add security '" + security + "' to request because it will exceed the maximum of 10 securities per request.");
		}
		if (!element.containsRequestValue(security)) {
			element.appendRequestValue(security);
		}
	}


	public void addSecurityList(List<String> securityList) {
		for (String security : securityList) {
			addSecurity(security);
		}
	}
}
