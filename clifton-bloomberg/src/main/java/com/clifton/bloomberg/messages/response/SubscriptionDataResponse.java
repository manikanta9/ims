package com.clifton.bloomberg.messages.response;

import com.clifton.core.messaging.asynchronous.AbstractAsynchronousMessage;
import com.clifton.core.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * <code>SubscriptionDataResponse</code> contains data for one or more security that was
 * received by a Bloomberg subscription data event.
 *
 * @author NickK
 */
public class SubscriptionDataResponse extends AbstractAsynchronousMessage implements BloombergResponseMessage {

	private final Map<String, BloombergSubscriptionSecurityData> subscriptionData = new HashMap<>();
	private Throwable error;
	private Integer requestTimeout;

	///////////////////////////////////////////////////////////////////////////


	@Override
	public Throwable getError() {
		return this.error;
	}


	@Override
	public void setError(Throwable exception) {
		this.error = exception;
	}


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * If the response does not have a mapping for the provided security, a new {@link BloombergSubscriptionSecurityData} is created,
	 * registered, and returned. If the response already has a mapping for the security, the mapped object is returned.
	 */
	public BloombergSubscriptionSecurityData addSecurity(String security) {
		return CollectionUtils.getValue(this.subscriptionData, security, () -> new BloombergSubscriptionSecurityData(security));
	}


	public Map<String, BloombergSubscriptionSecurityData> getSubscriptionData() {
		return this.subscriptionData;
	}


	/**
	 * Returns true if this response contains {@link BloombergSubscriptionSecurityData} for the provided security.
	 */
	public boolean containsSubscriptionDataForSecurity(String security) {
		return this.subscriptionData.containsKey(security);
	}


	/**
	 * Returns the {@link BloombergSubscriptionSecurityData} for the provided security if {@link #containsSubscriptionDataForSecurity(String)}, otherwise returns null.
	 */
	public BloombergSubscriptionSecurityData getSubscriptionDataForSecurity(String security) {
		return this.subscriptionData.get(security);
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
}
