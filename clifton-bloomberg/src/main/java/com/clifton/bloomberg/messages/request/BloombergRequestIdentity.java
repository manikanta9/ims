package com.clifton.bloomberg.messages.request;

import com.clifton.core.util.StringUtils;
import com.clifton.marketdata.provider.MarketDataUserIdentity;
import com.clifton.security.user.SecurityUser;


/**
 * <code>BloombergRequestIdentity</code> class holds Bloomberg user properties that
 * should be included in a {@link BloombergRequestMessage} for validating entitlements.
 *
 * @author NickK
 */
public class BloombergRequestIdentity implements MarketDataUserIdentity {

	/**
	 * BPipe users are authorized with the Parametric domain user name. This is hte domain prefix to use.
	 */
	public static final String BPIPE_DOMAIN_PREFIX = "Paraport\\";

	/**
	 * ID of the SecurityUser this request identity is for. The ID can be used for reverse lookup.
	 */
	private Short securityUserId;

	/**
	 * Terminal UUID
	 */
	private Integer uuid;
	/**
	 * ID for non-terminal user
	 */
	private String authenticationId;
	/**
	 * IP address for the client machine making the IMS request. Bloomberg requires each user to be
	 * identified to a machine. This IP may be useful to route messages back to the client as well.
	 */
	private String clientIpAddress;
	/**
	 * Terminal user's terminal IP address.
	 */
	private String terminalIpAddress;

	///////////////////////////////////////////////////////////////////////////


	public static BloombergRequestIdentity of(SecurityUser securityUser, String clientIpAddress) {
		BloombergRequestIdentity identity = new BloombergRequestIdentity();
		identity.setSecurityUserId(securityUser.getId());
		identity.setAuthenticationId(BPIPE_DOMAIN_PREFIX + (StringUtils.isEmpty(securityUser.getIntegrationPseudonym()) ? securityUser.getUserName() : securityUser.getIntegrationPseudonym()));
		identity.setClientIpAddress(clientIpAddress);
		return identity;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		String userId;
		String ipAddress;
		if (getUuid() == null) {
			userId = getAuthenticationId();
			ipAddress = getClientIpAddress();
		}
		else {
			userId = getUuid().toString();
			ipAddress = getTerminalIpAddress() != null ? getTerminalIpAddress() : getClientIpAddress();
		}
		return "{userId=" + userId + ", ipAddress=" + ipAddress + "}";
	}


	@Override
	public Short getSecurityUserId() {
		return this.securityUserId;
	}


	@Override
	public String getClientIpAddress() {
		return this.clientIpAddress;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void setSecurityUserId(Short securityUserId) {
		this.securityUserId = securityUserId;
	}


	public void setClientIpAddress(String clientIpAddress) {
		this.clientIpAddress = clientIpAddress;
	}


	public Integer getUuid() {
		return this.uuid;
	}


	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}


	public String getAuthenticationId() {
		return this.authenticationId;
	}


	public void setAuthenticationId(String authenticationId) {
		this.authenticationId = authenticationId;
	}


	public String getTerminalIpAddress() {
		return this.terminalIpAddress;
	}


	public void setTerminalIpAddress(String terminalIpAddress) {
		this.terminalIpAddress = terminalIpAddress;
	}
}
