package com.clifton.bloomberg.messages;


import java.util.HashMap;
import java.util.Map;


/**
 * The <code>BloombergSequenceElement</code> class represents a Map of name to value pairs.
 * For example, for "MULTI_CPN_SCHEDULE" field it can contain date to corresponding coupon.
 */
public class BloombergSequenceElement {

	private Map<String, Object> values = new HashMap<>();


	public Map<String, Object> getValues() {
		return this.values;
	}


	public void setValues(Map<String, Object> values) {
		this.values = values;
	}


	public void add(String name, Object readValue) {
		getValues().put(name, readValue);
	}
}
