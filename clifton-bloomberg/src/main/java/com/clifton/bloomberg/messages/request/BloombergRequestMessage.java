package com.clifton.bloomberg.messages.request;


import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.AbstractSynchronousRequestMessage;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BloombergRequestMessage</code> class is a base class that should be extended by all Bloomberg request implementations.
 */
@MessageType(MessageTypes.XML)
public abstract class BloombergRequestMessage extends AbstractSynchronousRequestMessage {

	/**
	 * Key name used to store investment securities inside of BloombergRequestData.
	 */
	public static final String SECURITIES_KEY = "securities";

	/**
	 * Key name used to store market data fields inside of BloombergRequestData.
	 */
	public static final String FIELDS_KEY = "fields";

	/**
	 * Bloomberg specific date format.
	 */
	public static final String DATE_FORMAT = "yyyyMMdd";

	/**
	 * Enumerator of possible internally defined system variables
	 * used in a template for Bloomberg
	 */
	public enum ExpressionVariables {
		DATE, SETTLEMENT_DATE
	}

	/**
	 * When using BPipe, a user identity is required to validate entitlements. The identity
	 * is defined by the UUID and IP Address for the Bloomberg user.
	 */
	private BloombergRequestIdentity identity;

	/**
	 * Turns off shutting down the terminal after number of errors is received
	 */

	private Boolean consecutiveErrorCountDisabled;

	/**
	 * Overrides the number of times a message will be retried
	 */
	private Integer retryCountOverride;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * Optional request overrides. For example, for Interest Rate Swap valuation call one
	 * can use overrides to specify settlement and curve dates:
	 * pricingDataRequest.addOverride("SETTLE_DT", "20111012");
	 * pricingDataRequest.addOverride("SW_CURVE_DT", "20111012");
	 */
	private final Map<String, String> overrides = new HashMap<>();

	/**
	 * Optional request schedule date and time. When using MDS, the request can be scheduled
	 * with Bloomberg
	 */
	private Date scheduleDate;
	private String scheduleTime;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract String getServiceUrl();


	public abstract BloombergOperations getOperation();


	public abstract BloombergRequestData getBloombergRequestData();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		BloombergRequestData data = getBloombergRequestData();
		if (data != null) {
			StringBuilder result = new StringBuilder(64);
			result.append("Requesting ");
			BloombergRequestElement element = data.getElement(FIELDS_KEY);
			result.append((element != null) ? element.getRequestData() : "NO FIELDS");
			element = data.getElement(SECURITIES_KEY);
			result.append(" for ");
			result.append((element != null) ? element.getRequestData() : "NO SECURITIES");
			return result.toString();
		}
		return "NO DATA";
	}


	public void setRequestTimeoutOverrideIfLarger(Integer newTimeout) {
		if (newTimeout != null) {
			if ((getRequestTimeout() == null) || (getRequestTimeout() < newTimeout)) {
				setRequestTimeout(newTimeout);
			}
		}
	}


	public void addField(String field) {
		BloombergRequestElement element = getBloombergRequestData().getElement(FIELDS_KEY);
		if (!element.containsRequestValue(field)) {
			element.appendRequestValue(field);
		}
	}


	public void addFields(List<String> fields) {
		for (String field : fields) {
			addField(field);
		}
	}


	public List<String> getFieldList() {
		BloombergRequestElement element = getBloombergRequestData().getElement(FIELDS_KEY);
		return element.getRequestData();
	}


	public String getSecurity() {
		BloombergRequestElement element = getBloombergRequestData().getElement(SECURITIES_KEY);
		if (element.getRequestData().size() == 1) {
			return element.getRequestData().get(0);
		}
		return null;
	}


	public List<String> getSecurityList() {
		BloombergRequestElement element = getBloombergRequestData().getElement(SECURITIES_KEY);
		return element.getRequestData();
	}


	public Map<String, String> getOverrides() {
		return this.overrides;
	}


	/**
	 * Adds the specified override field to this request.  The value must follow Bloomberg format (dates, etc.)
	 * If the field was already set, then will update it to the new value.
	 */
	public void addOverride(String overrideField, String overrideValue) {
		this.overrides.put(overrideField, overrideValue);
	}


	/**
	 * Adds the specified override date field to this request.
	 * If the field was already set, then will update it to the new value.
	 */
	public void addOverride(String overrideField, Date overrideDateValue) {
		addOverride(overrideField, DateUtils.fromDate(overrideDateValue, DATE_FORMAT));
	}


	/**
	 * Add the specified override parameters to this request.
	 * Format example: "SETTLE_DT=20121128,SW_CURVE_DT=20121128"
	 */
	public void addOverrides(String overrideParams) {
		if (!StringUtils.isEmpty(overrideParams)) {
			String[] params = overrideParams.split(",");
			for (String param : params) {
				String[] nameValue = param.split("=");
				if (nameValue.length != 2) {
					throw new ValidationException("Illegal format for overrideParams: " + overrideParams);
				}
				addOverride(nameValue[0], nameValue[1]);
			}
		}
	}


	/**
	 * Returns true if date marker is present in the specified overrideParams.
	 * This means that replacement needs to run and that reference vs historic request should be made.
	 */
	public boolean isDateUsedInOverrides(String overrideParams) {
		if (overrideParams != null) {
			for (ExpressionVariables variable : ExpressionVariables.values()) {
				if (overrideParams.contains(variable.name())) {
					return true;
				}
			}
		}
		return false;
	}


	public BloombergRequestIdentity getIdentity() {
		return this.identity;
	}


	public void setIdentity(BloombergRequestIdentity identity) {
		this.identity = identity;
	}


	@Override
	public Boolean getConsecutiveErrorCountDisabled() {
		return this.consecutiveErrorCountDisabled;
	}


	public void setConsecutiveErrorCountDisabled(Boolean consecutiveErrorCountDisabled) {
		this.consecutiveErrorCountDisabled = consecutiveErrorCountDisabled;
	}


	@Override
	public Integer getRetryCountOverride() {
		return this.retryCountOverride;
	}


	public void setRetryCountOverride(Integer retryCountOverride) {
		this.retryCountOverride = retryCountOverride;
	}


	public Date getScheduleDate() {
		return this.scheduleDate;
	}


	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}


	public String getScheduleTime() {
		return this.scheduleTime;
	}


	public void setScheduleTime(String scheduleTime) {
		this.scheduleTime = scheduleTime;
	}
}
