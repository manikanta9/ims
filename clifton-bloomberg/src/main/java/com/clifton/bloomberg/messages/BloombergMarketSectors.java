package com.clifton.bloomberg.messages;


public enum BloombergMarketSectors {

	GOVERNMENT("Govt"), CORP_BONDS("Corp"), MUNICIPAL("Muni"), INDEX("Index"), EQUITY("Equity"), MONEY_MARKET("M-Mkt"),
	CURRENCY("Curncy"), CLIENT("Client"), PREFERRED_STOCK("Pfd"), COMMODITY("Comdty"), MORTGAGE("Mtge");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String keyName;


	BloombergMarketSectors(String keyName) {
		this.keyName = keyName;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getKeyName() {
		return this.keyName;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns BloombergMarketSector for the corresponding key.
	 */
	public static BloombergMarketSectors getBloombergMarketSectorByKey(String key) {
		for (BloombergMarketSectors sector : BloombergMarketSectors.values()) {
			if (sector.getKeyName().equalsIgnoreCase(key)) {
				return sector;
			}
		}
		throw new IllegalArgumentException("Cannot find Bloomberg market sector for key: " + key);
	}
}
