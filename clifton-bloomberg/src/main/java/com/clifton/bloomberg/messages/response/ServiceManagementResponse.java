package com.clifton.bloomberg.messages.response;

import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;


/**
 * @author theodorez
 */
public class ServiceManagementResponse extends AbstractSynchronousResponseMessage implements BloombergResponseMessage {
	//nothing for now
}
