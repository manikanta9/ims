package com.clifton.bloomberg.messages.request;


import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.bloomberg.messages.BloombergPeriodicity;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>HistoricalDataRequest</code> is a model of the structure for a HistoricalDataRequest in Bloomberg:
 * get values for a set of fields for a security for the specified date range.
 */
public class HistoricalDataRequest extends BloombergRequestMessage {

	private final String serviceUrl;
	private final BloombergRequestData bloombergRequestData;
	private final BloombergOperations operation = BloombergOperations.HistoricalDataRequest;

	/**
	 * When using Backoffice, an investmentType Name can be passed to identify the table that should be queried
	 */
	private String investmentType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public HistoricalDataRequest() {
		this.serviceUrl = "//blp/refdata";
		this.bloombergRequestData = new BloombergRequestData();
		this.bloombergRequestData.addElement(new BloombergRequestElement(BloombergRequestMessage.SECURITIES_KEY));
		this.bloombergRequestData.addElement(new BloombergRequestElement(BloombergRequestMessage.FIELDS_KEY));
		this.bloombergRequestData.addElement(new BloombergRequestElement("startDate"));
		this.bloombergRequestData.addElement(new BloombergRequestElement("endDate"));
		this.bloombergRequestData.addElement(new BloombergRequestElement("periodicitySelection"));
	}


	public HistoricalDataRequest(String... fields) {
		this();
		BloombergRequestElement element = getBloombergRequestData().getElement(BloombergRequestMessage.FIELDS_KEY);
		for (String field : fields) {
			element.appendRequestValue(field);
		}
	}


	@Override
	public Class<?> getResponseClass() {
		return HistoricalDataResponse.class;
	}


	@Override
	public String getServiceUrl() {
		return this.serviceUrl;
	}


	@Override
	public BloombergOperations getOperation() {
		return this.operation;
	}


	@Override
	public BloombergRequestData getBloombergRequestData() {
		return this.bloombergRequestData;
	}


	/**
	 * Sets the only security on the request to the one provided
	 */
	public void setSecurity(String security) {
		BloombergRequestElement element = getBloombergRequestData().getElement(BloombergRequestMessage.SECURITIES_KEY);
		if (element.getRequestData().size() == 1) {
			element.getRequestData().set(0, security);
		}
		else {
			element.getRequestData().add(security);
		}
	}


	/**
	 * Adds the security to the list of securities to be requested
	 */
	public void addSecurity(String security) {
		BloombergRequestElement element = getBloombergRequestData().getElement(BloombergRequestMessage.SECURITIES_KEY);
		element.getRequestData().add(security);
	}


	public Date getStartDate() {
		return DateUtils.toDate(getBloombergRequestData().getElement("startDate").getRequestValue(), DATE_FORMAT);
	}


	@ValueChangingSetter
	public void setStartDate(Date startDate) {
		getBloombergRequestData().getElement("startDate").setRequestValue(DateUtils.fromDate(startDate, DATE_FORMAT));
	}


	public Date getEndDate() {
		return DateUtils.toDate(getBloombergRequestData().getElement("endDate").getRequestValue(), DATE_FORMAT);
	}


	@ValueChangingSetter
	public void setEndDate(Date endDate) {
		getBloombergRequestData().getElement("endDate").setRequestValue(DateUtils.fromDate(endDate, DATE_FORMAT));
	}


	public BloombergPeriodicity getPeriodicitySelection() {
		String periodicity = getBloombergRequestData().getElement("periodicitySelection").getRequestValue();
		if (periodicity != null) {
			return BloombergPeriodicity.valueOf(periodicity);
		}
		else {
			return BloombergPeriodicity.DAILY;
		}
	}


	public void setPeriodicitySelection(BloombergPeriodicity periodicitySelection) {
		getBloombergRequestData().getElement("periodicitySelection").setRequestValue(periodicitySelection.toString());
	}


	/**
	 * Sets the investment type
	 */
	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}
}
