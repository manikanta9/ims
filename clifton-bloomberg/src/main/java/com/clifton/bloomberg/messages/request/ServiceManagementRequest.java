package com.clifton.bloomberg.messages.request;

import com.clifton.bloomberg.messages.BloombergOperations;


/**
 * Holder for administrative actions that need to be performed at runtime.
 *
 * @author theodorez
 */
public class ServiceManagementRequest extends BloombergRequestMessage {

	private final BloombergOperations operation = BloombergOperations.ServiceManagement;

	private final ServiceManagementAction action;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ServiceManagementRequest(ServiceManagementAction action) {
		this.action = action;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ServiceManagementAction getManagementAction() {
		return this.action;
	}


	public ServiceManagementAction getAction() {
		return this.action;
	}


	@Override
	public String getServiceUrl() {
		return null;
	}


	@Override
	public BloombergOperations getOperation() {
		return this.operation;
	}


	@Override
	public BloombergRequestData getBloombergRequestData() {
		return null;
	}


	@Override
	public Class<?> getResponseClass() {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public enum ServiceManagementAction {
		CLEAR_CACHE_ALL
	}
}
