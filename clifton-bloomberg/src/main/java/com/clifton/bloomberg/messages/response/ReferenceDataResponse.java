package com.clifton.bloomberg.messages.response;


import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.request.BloombergRequestIdentity;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;
import com.clifton.core.util.CollectionUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class ReferenceDataResponse extends AbstractSynchronousResponseMessage implements BloombergResponseMessage {

	/**
	 * Maps security symbols to corresponding field/value maps.
	 */
	private Map<String, Map<String, Object>> values = new HashMap<>();
	/**
	 * Identities that should receive the data of this response.
	 */
	private Set<BloombergRequestIdentity> identitySet = new HashSet<>();
	private BloombergError bloomError;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns user friendly error message if an error occurred or null if there was no error.
	 */
	public String getErrorMessage() {
		if (getError() != null) {
			return getError().getMessage();
		}
		if (this.bloomError != null) {
			// try to make error messages as friendly as we can
			if ("NOT_APPLICABLE_TO_REF_DATA".equals(this.bloomError.getSubcategory())) {
				return this.bloomError.getErrorField() + " " + this.bloomError.getMessage() + " " + this.bloomError.getSecurity();
			}
			return this.bloomError.getSecurity() + ": " + this.bloomError.getMessage();
		}
		return null;
	}


	/**
	 * Creates a new Map of property name/value pairs for the specified security (if one doesn't already exist)
	 * and returns it.  If the Map already exists for the specified security, returns existing Map.
	 */
	public Map<String, Object> addSecurity(String security) {
		return CollectionUtils.getValue(this.values, security, HashMap::new);
	}


	/**
	 * If properties are set for only one security, then returns the value of the specified property.
	 * If multiple securities are included in the response, returns null.
	 */
	public Object getSingleSecurityPropertyValue(String propertyName) {
		Set<String> securityKeys = this.values.keySet();
		if (CollectionUtils.getSize(securityKeys) != 1) {
			return null;
		}
		return this.values.get(securityKeys.iterator().next()).get(propertyName);
	}


	public Map<String, Map<String, Object>> getValues() {
		return this.values;
	}


	public void setValues(Map<String, Map<String, Object>> values) {
		this.values = values;
	}


	public void addIdentity(BloombergRequestIdentity identity) {
		this.identitySet.add(identity);
	}


	public Set<BloombergRequestIdentity> getIdentitySet() {
		return this.identitySet;
	}


	public BloombergError getBloomError() {
		return this.bloomError;
	}


	public void setBloomError(BloombergError bloomError) {
		this.bloomError = bloomError;
	}
}
