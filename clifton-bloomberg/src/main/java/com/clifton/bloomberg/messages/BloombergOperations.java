package com.clifton.bloomberg.messages;


/**
 * The <code>BloombergOperations</code> represents the different operations available via the Bloomberg API.
 */
public enum BloombergOperations {

	ReferenceDataRequest, HistoricalDataRequest, FieldInfoRequest, Subscription, ServiceManagement
}
