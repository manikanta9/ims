package com.clifton.bloomberg.messages.response;


import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.synchronous.SynchronousResponseMessage;


/**
 * The <code>BloombergResponseMessage</code> interface defines a response message received from Bloomberg service.
 * Multiple implementations may exist: reference data response, historic data response, etc.
 */
@MessageType(MessageTypes.XML)
public interface BloombergResponseMessage extends SynchronousResponseMessage {

	// empty
}
