package com.clifton.bloomberg.messages.request;


import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>BloombergRequestException</code> is thrown when invalid data is sent to the bloomberg service.
 * For example, if you sent an invalid security name, or if a field is not valid for a certain security.
 * The offending field, and all the data associated to the error will be in the wrapped BloombergError class.
 * See the errorType to determine if it is a Security or a Field level error.
 * Request errors are reported as straight runtime errors, and not this error.
 */
public class BloombergRequestException extends ValidationException {

	private final BloombergError bloombergError;
	private ReferenceDataResponse response;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BloombergRequestException(BloombergError error) {
		super(getErrorMessage(error));
		this.bloombergError = error;
	}


	public BloombergRequestException(BloombergError error, ReferenceDataResponse response) {
		super(getErrorMessage(error));
		this.bloombergError = error;
		this.response = response;
	}


	private static String getErrorMessage(BloombergError error) {
		StringBuilder builder = new StringBuilder();
		BloombergError.BloombergErrorTypes errorType = error.getErrorType();
		if (errorType == BloombergError.BloombergErrorTypes.SUBSCRIPTION || errorType == BloombergError.BloombergErrorTypes.AUTHORIZATION) {
			builder.append(errorType).append(": ");
		}
		else if ((errorType == BloombergError.BloombergErrorTypes.SECURITY) && error.getSecurity() != null && error.getSecurity().equals(error.getErrorField())) {
			// avoid duplicate information
			builder.append(error.getSecurity()).append(": ");
		}
		else {
			builder.append(error.getSecurity()).append(": ").append(errorType).append(" ").append(error.getErrorField()).append("\n");
		}
		builder.append(error.getMessage()).append(", Category ").append(error.getCategory()).append("/").append(error.getSubcategory());
		return builder.toString();
	}


	public BloombergError getBloombergError() {
		return this.bloombergError;
	}


	public ReferenceDataResponse getResponse() {
		return this.response;
	}


	public void setResponse(ReferenceDataResponse response) {
		this.response = response;
	}
}
