package com.clifton.bloomberg.messages.request;

import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <code>SubscriptionRequest</code> contains one or more securities with corresponding
 * field to subscribe to on Bloomberg. Each request can specify a {@link MarketDataSubscriptionOperations}
 * and options, such as interval, that apply to all security subscriptions included in the request.
 * <p>
 * Subscription requests are made with the following syntax.
 * <pre>
 * "//blp/mktdata/ticker/IBM US Equity?fields=BID,ASK&interval=2"
 *  \-----------/\------/\-----------/\------------------------/
 *      |          |         |                  |
 *   Service    Prefix   Instrument           Suffix
 * </pre>
 *
 * @author NickK
 */
public class SubscriptionRequest extends BloombergRequestMessage implements AsynchronousMessage {

	private static final String SUBSCRIPTION_SERVICE_URL = "//blp/mktdata";

	public static final String SUBSCRIPTION_OPERATION_KEY = "operation";
	public static final String SUBSCRIPTION_OPTIONS_KEY = "options";
	private static final String OPTIONS_INTERVAL_PREFIX = "interval=";
	private static final String OPTIONS_DELAYED = "delayed";
	public static final int DEFAULT_SUBSCRIPTION_INTERVAL_SECONDS = 10;

	private final BloombergRequestData bloombergRequestData;
	private Integer messageDelaySeconds;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SubscriptionRequest(MarketDataSubscriptionOperations operation) {
		this.bloombergRequestData = new BloombergRequestData();
		BloombergRequestElement optionsElement = new BloombergRequestElement(SUBSCRIPTION_OPTIONS_KEY);
		optionsElement.appendRequestValue(OPTIONS_INTERVAL_PREFIX + DEFAULT_SUBSCRIPTION_INTERVAL_SECONDS);
		this.bloombergRequestData.addElement(optionsElement);
		setSubscriptionOperation(operation);
		setRequestTimeoutOverrideIfLarger(5000); // TODO make this configurable
	}


	@Override
	public String toString() {
		BloombergRequestData data = getBloombergRequestData();
		if (data != null) {
			return "Requesting " + getOperation() + " for " + data.getElementList();
		}
		return "NO DATA";
	}


	@Override
	public String getServiceUrl() {
		return SUBSCRIPTION_SERVICE_URL;
	}


	@Override
	public BloombergOperations getOperation() {
		return BloombergOperations.Subscription;
	}


	@Override
	public BloombergRequestData getBloombergRequestData() {
		return this.bloombergRequestData;
	}


	@Override
	public Class<?> getResponseClass() {
		return SubscriptionDataResponse.class;
	}


	@Override
	public Integer getMessageDelaySeconds() {
		return this.messageDelaySeconds;
	}


	public void setMessageDelaySeconds(Integer messageDelaySeconds) {
		this.messageDelaySeconds = messageDelaySeconds;
	}


	@ValueChangingSetter
	public void setSubscriptionOperation(MarketDataSubscriptionOperations operation) {
		BloombergRequestElement operationElement = new BloombergRequestElement(SUBSCRIPTION_OPERATION_KEY);
		operationElement.setRequestValue(operation.name());
		this.bloombergRequestData.addElement(operationElement);
	}


	public MarketDataSubscriptionOperations getSubscriptionOperation() {
		return MarketDataSubscriptionOperations.valueOf(this.bloombergRequestData.getElement(SubscriptionRequest.SUBSCRIPTION_OPERATION_KEY).getRequestValue());
	}


	public void setIntervalSeconds(int interval) {
		BloombergRequestElement optionsElement = this.bloombergRequestData.getElement(SUBSCRIPTION_OPTIONS_KEY);
		for (int i = 0; i < CollectionUtils.getSize(optionsElement.getRequestData()); i++) {
			if (optionsElement.getRequestData().get(i).startsWith(OPTIONS_INTERVAL_PREFIX)) {
				optionsElement.getRequestData().set(i, OPTIONS_INTERVAL_PREFIX + interval);
				break;
			}
		}
	}


	public void setDelayed(boolean delayed) {
		BloombergRequestElement optionsElement = this.bloombergRequestData.getElement(SUBSCRIPTION_OPTIONS_KEY);
		boolean delayedSet = false;
		for (int i = 0; i < CollectionUtils.getSize(optionsElement.getRequestData()); i++) {
			if (OPTIONS_DELAYED.equals(optionsElement.getRequestData().get(i))) {
				if (!delayed) {
					optionsElement.getRequestData().remove(i);
				}
				else {
					delayedSet = true;
				}
				break;
			}
		}
		if (!delayedSet && delayed) {
			optionsElement.appendRequestValue(OPTIONS_DELAYED);
		}
	}


	public void addSubscription(String security, String... fields) {
		if (!StringUtils.isEmpty(security) && !ArrayUtils.isEmpty(fields)) {
			BloombergRequestElement securityFieldsElement = new BloombergRequestElement(BloombergRequestMessage.FIELDS_KEY);
			ArrayUtils.getStream(fields).forEach(securityFieldsElement::appendRequestValue);
			this.bloombergRequestData.addElement(security, securityFieldsElement);
		}
	}


	public void addSubscription(String security, List<String> fieldList) {
		if (!StringUtils.isEmpty(security) && !CollectionUtils.isEmpty(fieldList)) {
			BloombergRequestElement securityFieldsElement = new BloombergRequestElement(BloombergRequestMessage.FIELDS_KEY);
			CollectionUtils.getStream(fieldList).forEach(securityFieldsElement::appendRequestValue);
			this.bloombergRequestData.addElement(security, securityFieldsElement);
		}
	}


	public boolean hasSubscription() {
		return !getSubscriptionSecuritySet().isEmpty();
	}


	public Set<String> getSubscriptionSecuritySet() {
		return this.bloombergRequestData.getElementList().keySet()
				.stream()
				.filter(key -> !(SUBSCRIPTION_OPTIONS_KEY.equals(key) || SUBSCRIPTION_OPERATION_KEY.equals(key)))
				.collect(Collectors.toSet());
	}
}
