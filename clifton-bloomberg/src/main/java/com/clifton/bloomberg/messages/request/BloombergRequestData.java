package com.clifton.bloomberg.messages.request;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>BloombergRequestData</code> defines the data used to populate the Bloomberg terminal request.
 *
 * @author mwacker
 */
public class BloombergRequestData implements Serializable {

	//This is named elementList instead of elementMap because the target schema is looking for elementList.
	private final Map<String, BloombergRequestElement> elementList = new HashMap<>();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void addElement(String elementName, BloombergRequestElement element) {
		this.elementList.put(elementName, element);
	}


	public void addElement(BloombergRequestElement element) {
		if ((element.getName() == null) || element.getName().isEmpty()) {
			throw new RuntimeException("Cannot add an element to '" + BloombergRequestData.class + "' without a name.");
		}
		this.elementList.put(element.getName(), element);
	}


	public Map<String, BloombergRequestElement> getElementList() {
		return new HashMap<>(this.elementList);
	}


	public BloombergRequestElement getElement(String elementName) {
		return this.elementList.get(elementName);
	}


	public int numElements() {
		return this.elementList.size();
	}


	public boolean hasElements() {
		return numElements() > 0;
	}
}
