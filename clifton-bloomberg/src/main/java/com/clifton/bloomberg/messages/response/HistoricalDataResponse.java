package com.clifton.bloomberg.messages.response;


import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.core.messaging.synchronous.AbstractSynchronousResponseMessage;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class HistoricalDataResponse extends AbstractSynchronousResponseMessage implements BloombergResponseMessage {

	private Map<Date, ReferenceDataResponse> values = new HashMap<>();
	private BloombergError bloomError;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public HistoricalDataResponse() {
		//
	}


	public ReferenceDataResponse addDate(Date date) {
		if (this.values.containsKey(date)) {
			return this.values.get(date);
		}
		ReferenceDataResponse newValues = new ReferenceDataResponse();
		this.values.put(date, newValues);
		return newValues;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<Date, ReferenceDataResponse> getValues() {
		return this.values;
	}


	public void setValues(Map<Date, ReferenceDataResponse> values) {
		this.values = values;
	}


	public BloombergError getBloomError() {
		return this.bloomError;
	}


	public void setBloomError(BloombergError bloomError) {
		this.bloomError = bloomError;
	}
}
