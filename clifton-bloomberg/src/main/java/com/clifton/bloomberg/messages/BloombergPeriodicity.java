package com.clifton.bloomberg.messages;


/**
 * The <code>BloombergPeriodicity</code> represents the available periods for a HistoricalDataRequest
 */
public enum BloombergPeriodicity {
	DAILY, WEEKLY, MONTHLY, QUARTERLY, SEMI_ANNUALLY, YEARLY
}
