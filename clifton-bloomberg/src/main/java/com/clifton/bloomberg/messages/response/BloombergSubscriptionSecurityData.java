package com.clifton.bloomberg.messages.response;

import com.clifton.marketdata.provider.subscription.AbstractMarketDataSubscriptionSecurityData;


/**
 * <code>BloombergSubscriptionSecurityData</code> holds data for a Bloomberg subscription event including:
 * the status, event type, field data, and user identities with access to the data.
 *
 * @author NickK
 */
public class BloombergSubscriptionSecurityData extends AbstractMarketDataSubscriptionSecurityData {


	public BloombergSubscriptionSecurityData(String securitySymbol) {
		super(securitySymbol);
	}
}
