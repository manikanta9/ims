package com.clifton.bloomberg.messages;


/**
 * The <code>BloombergError</code> class contains details about Bloomberg errors.
 */
public class BloombergError {

	private BloombergErrorTypes errorType;

	private String source;
	private String category;
	private String message;
	private String subcategory;
	private String code;
	private String errorField; //either the security or the field
	private String security;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public enum BloombergErrorTypes {
		SECURITY, FIELD, RESPONSE, AUTHORIZATION, SUBSCRIPTION
	}


	public String getSource() {
		return this.source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getCategory() {
		return this.category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getMessage() {
		return this.message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getSubcategory() {
		return this.subcategory;
	}


	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}


	public String getCode() {
		return this.code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public BloombergErrorTypes getErrorType() {
		return this.errorType;
	}


	public void setErrorType(BloombergErrorTypes errorType) {
		this.errorType = errorType;
	}


	public String getErrorField() {
		return this.errorField;
	}


	public void setErrorField(String errorField) {
		this.errorField = errorField;
	}


	public String getSecurity() {
		return this.security;
	}


	public void setSecurity(String security) {
		this.security = security;
	}
}
