package com.clifton.bloomberg.messages.request;


import com.clifton.core.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BloombergRequestElement</code> defines an element that will be requested from the Bloomberg terminal.
 * <p>
 * For example, reference data requests will contain 2 elements: "fields" and "securities".
 * Corresponding values will be fields requested: "PX_LAST", "PX_SETTLE" and securities that the fields are requested for: "ESM1"
 *
 * @author mwacker
 */
public class BloombergRequestElement implements Serializable {

	private String name;

	// some request elements have single value "startDate", "endDate" while others can have multiple values: "fields"
	private String requestValue;
	private final List<String> requestData = new ArrayList<>();


	public BloombergRequestElement(String name) {
		this.name = name;
	}


	public BloombergRequestElement() {
		//
	}


	@Override
	public String toString() {
		return "[" + (StringUtils.isEmpty(getRequestValue()) ? StringUtils.collectionToCommaDelimitedString(getRequestData()) : getRequestValue()) + "]";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void appendRequestValue(String valueName) {
		this.requestData.add(valueName);
	}


	public boolean containsRequestValue(String valueName) {
		return this.requestData.contains(valueName);
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<String> getRequestData() {
		return this.requestData;
	}


	public String getRequestValue() {
		return this.requestValue;
	}


	public void setRequestValue(String requestValue) {
		this.requestValue = requestValue;
	}
}
