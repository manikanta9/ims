Clifton.bloomberg.BloombergStatisticDetailWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Bloomberg Service Statistics',
	width: 1000,
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'bloombergServiceAnalytics.json',
		readOnly: true,
		items: [
			{
				xtype: 'panel',
				layout: 'column',
				defaults: {
					layout: 'form',
					defaults: {readOnly: true, anchor: '0'}
				},

				items: [
					{
						columnWidth: .3,
						labelWidth: 130,
						items: [
							{fieldLabel: 'Total Requests', name: 'requestCount', xtype: 'integerfield'},
							{fieldLabel: 'Total Successes', name: 'successfulRequests', xtype: 'integerfield'},
							{fieldLabel: 'Total Failures', name: 'failedRequests', xtype: 'integerfield'},
							{fieldLabel: 'Last Success', name: 'lastSuccessfulResponse', xtype: 'textfield'},
							{fieldLabel: 'Last Failure', name: 'lastFailedResponse', xtype: 'textfield'}
						]
					},
					{columnWidth: .05, items: [{html: '&nbsp;'}]},
					{
						columnWidth: .28,
						labelWidth: 180,
						items: [
							{fieldLabel: 'Average Request Duration (ms)', name: 'averageRequestDuration', xtype: 'integerfield'},
							{fieldLabel: 'Minimum Request Duration (ms)', name: 'minimumRequestDuration', xtype: 'integerfield'},
							{fieldLabel: 'Maximum Request Duration (ms)', name: 'maximumRequestDuration', xtype: 'integerfield'},
							{fieldLabel: 'Last Request Duration (ms)', name: 'lastRequestDuration', xtype: 'integerfield'},
							{fieldLabel: 'Last Request Successful', name: 'lastRequestSuccessful', xtype: 'textfield'}
						]
					},
					{columnWidth: .05, items: [{html: '&nbsp;'}]},
					{
						columnWidth: .32,
						labelWidth: 200,
						items: [
							{fieldLabel: 'Field Info Request Count', name: 'operationsCount.FieldInfoRequest', xtype: 'integerfield'},
							{fieldLabel: 'Historical Data Request Count', name: 'operationsCount.HistoricalDataRequest', xtype: 'integerfield'},
							{fieldLabel: 'Reference Data Request Count', name: 'operationsCount.ReferenceDataRequest', xtype: 'integerfield'},
							{fieldLabel: 'Service Management Request Count', name: 'operationsCount.ServiceManagement', xtype: 'integerfield'},
							{fieldLabel: 'Subscription Request Count', name: 'operationsCount.Subscription', xtype: 'integerfield'}
						]
					}
				]
			},
			{fieldLabel: 'Last Error Message', name: 'lastErrorMessage', xtype: 'textarea', height: 250}
		],
		getLoadParams: function(a) {
			return {
				serviceName: a.getMainForm().idFieldValue
			};
		}
	}]
});
