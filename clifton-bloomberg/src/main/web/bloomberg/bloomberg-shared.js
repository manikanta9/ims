Ext.ns('Clifton.bloomberg');

Clifton.bloomberg.BloombergStatisticsFormPanel = Ext.extend(TCG.form.FormPanel, {
	xtype: 'formpanel',
	instructions: 'Displays statistics for each bloomberg endpoint.  Requests that timeout before receiving a response from a bloomberg instance are displayed under the service \'localhost\'.',
	loadValidation: false,
	labelWidth: 80,
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	defaults: {flex: 0},
	wikiPage: 'IT/Bloomberg+Integration',
	initComponent: function() {
		this.items = this.formPanelItems;
		TCG.callSuper(this, 'initComponent', arguments);
	},
	getLoadURL: () => 'bloombergServiceAnalyticsAggregate.json',
	tbar: [
		{
			text: 'Reload',
			xtype: 'button',
			iconCls: 'table-refresh',
			handler: button => TCG.getParentFormPanel(button).reload()
		}, '-', {
			text: 'Clear stats',
			xtype: 'button',
			iconCls: 'table-refresh',
			handler: button => {
				const fp = TCG.getParentFormPanel(button);
				fp.clearFields(fp.items);
				TCG.data.getDataPromise('bloombergServiceAnalyticsClear.json', this).then(fp.reload());
			}
		}, '-', {
			text: 'Help',
			iconCls: 'help',
			tooltip: 'Open WIKI Help Window',
			handler: function(button) {
				const formPanel = TCG.getParentFormPanel(button);
				const url = formPanel.wikiPage;
				if (url === false) {
					TCG.showError('Help Page was not defined.', 'No Help Page');
				}
				else {
					TCG.openWIKI(url, 'Open WIKI Help Window', 1400, 700, false, 'help');
				}
			}
		}
	],
	reload: function(params) {
		this.load({
			url: encodeURI(this.getLoadURL()),
			params: params,
			waitMsg: 'Loading...',
			success: (form, action) => {
				this.loadJsonResult(action.result, true);
				this.fireEvent('afterload', this);
			},
			...TCG.form.submitDefaults
		});
	},
	formPanelItems: [
		{
			xtype: 'panel',
			layout: 'column',
			defaults: {
				layout: 'form',
				defaults: {readOnly: true, anchor: '0', value: ''}
			},

			items: [
				{
					columnWidth: .25,
					labelWidth: 130,
					items: [
						{fieldLabel: 'Total Requests', name: 'requestCount', xtype: 'integerfield'},
						{fieldLabel: 'Total Successes', name: 'successfulRequests', xtype: 'integerfield'},
						{fieldLabel: 'Total Failures', name: 'failedRequests', xtype: 'integerfield'},
						{fieldLabel: 'Last Success', name: 'lastSuccessfulResponse', xtype: 'textfield'},
						{fieldLabel: 'Last Failure', name: 'lastFailedResponse', xtype: 'textfield'}
					]
				},
				{columnWidth: .1, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .25,
					labelWidth: 180,
					items: [

						{fieldLabel: 'Average Request Duration (ms)', name: 'averageRequestDuration', xtype: 'integerfield'},
						{fieldLabel: 'Minimum Request Duration (ms)', name: 'minimumRequestDuration', xtype: 'integerfield'},
						{fieldLabel: 'Maximum Request Duration (ms)', name: 'maximumRequestDuration', xtype: 'integerfield'},
						{fieldLabel: 'Last Request Duration (ms)', name: 'lastRequestDuration', xtype: 'integerfield'}

					]
				},
				{columnWidth: .1, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .3,
					labelWidth: 200,
					items: [
						{
							fieldLabel: 'Field Info Request Count', name: 'operationsCount.FieldInfoRequest', xtype: 'integerfield',
							tooltip: 'Returns information about one or more fields. Also, consult FLDS&lt;GO&gt; on Bloom Terminal'
						},
						{
							fieldLabel: 'Historical Data Request Count', name: 'operationsCount.HistoricalDataRequest', xtype: 'integerfield',
							tooltip: 'Enables you to retrieve end-of-day data for a set of securities for a specified period of time in intervals of days, weeks, months, quarters, half-years, or years.'
						},
						{
							fieldLabel: 'Reference Data Request Count', name: 'operationsCount.ReferenceDataRequest', xtype: 'integerfield',
							tooltip: 'Enables you to retrieve current snapshot data, including pricing, descriptive data, and fundamental data.'
						},
						{fieldLabel: 'Service Management Request Count', name: 'operationsCount.ServiceManagement', xtype: 'integerfield'},
						{fieldLabel: 'Subscription Request Count', name: 'operationsCount.Subscription', xtype: 'integerfield'}
					]
				}
			]
		},
		{
			xtype: 'bloomberg-statistics-grid',
			height: 450
		}
	]
});
Ext.reg('bloomberg-statistics-formpanel', Clifton.bloomberg.BloombergStatisticsFormPanel);

// Register the Bloomberg statistics view
Clifton.core.stats.StatsListTabs.push({
	title: 'Bloomberg Statistics',
	items: [{
		xtype: 'bloomberg-statistics-formpanel'
	}]
});

Clifton.bloomberg.BloombergStatisticsGrid = Ext.extend(TCG.grid.FormGridPanel, {
	xtype: 'formgrid',
	title: 'Per-Endpoint Statistics',
	storeRoot: 'analyticsPerServiceSet',
	viewConfig: {markDirty: false, forceFit: true},
	collapsible: false,
	autoHeight: false,
	readOnly: true,
	flex: 1,
	detailPageClass: 'Clifton.bloomberg.BloombergStatisticDetailWindow',
	getDetailPageId: function(grid, row) {
		return row.json.name;
	},
	decorateRow: function(v, metaData, r) {
		if (!r.json.lastRequestSuccessful) {
			metaData.css = 'x-grid3-row-highlight-light-red';
		}
		else if (v > 2000) {
			metaData.css = 'x-grid3-row-highlight-light-yellow';
		}
		return v;
	},
	columnsConfig: [
		{header: 'Bloomberg Service', dataIndex: 'name', width: 75},
		{header: 'Service Type', dataIndex: 'serviceType', width: 55},
		{header: 'Total Requests', dataIndex: 'requestCount', type: 'int', width: 55},
		{header: 'Successes', dataIndex: 'successfulRequests', type: 'int', width: 50},
		{header: 'Failures', dataIndex: 'failedRequests', type: 'int', width: 45},
		{
			header: 'Last Request Successful', dataIndex: 'lastRequestSuccessful', width: 85,
			renderer: function(v, metaData, r) {
				metaData.css = v ? 'x-grid3-row-highlight-light-green' : 'x-grid3-row-highlight-light-red';
				return v;
			}
		},
		{
			header: 'Avg Request', dataIndex: 'averageRequestDuration', type: 'int', width: 50,
			renderer: function(v, metaData, r) {
				return this.gridPanel.decorateRow(v, metaData, r);
			}
		},
		{
			header: 'Min Request', dataIndex: 'minimumRequestDuration', type: 'int', width: 50,
			renderer: function(v, metaData, r) {
				return this.gridPanel.decorateRow(v, metaData, r);
			}
		},
		{
			header: 'Max Request', dataIndex: 'maximumRequestDuration', type: 'int', width: 50,
			renderer: function(v, metaData, r) {
				return this.gridPanel.decorateRow(v, metaData, r);
			}
		},
		{
			header: 'Last Request', dataIndex: 'lastRequestDuration', type: 'int', width: 50,
			renderer: function(v, metaData, r) {
				return this.gridPanel.decorateRow(v, metaData, r);
			}
		},
		{
			header: 'Last Success', dataIndex: 'lastSuccessfulResponse', width: 75,
			renderer: function(v, metaData, r) {
				const diff = (new Date()) - (new Date(v));
				// greater than 1 day in milliseconds
				if (diff > 85400000) {
					metaData.css = 'requestFailed';
				}
				return v;
			}
		},
		{header: 'Field Info Requests', dataIndex: 'operationsCount.FieldInfoRequest', type: 'int', hidden: true},
		{header: 'Historical Data Requests', dataIndex: 'operationsCount.HistoricalDataRequest', type: 'int', hidden: true},
		{header: 'Reference Data Requests', dataIndex: 'operationsCount.ReferenceDataRequest', type: 'int', hidden: true},
		{header: 'Service Management Requests', dataIndex: 'operationsCount.ServiceManagement', type: 'int', hidden: true},
		{header: 'Subscription Request', dataIndex: 'operationsCount.Subscription', type: 'int', hidden: true}
	]
});
Ext.reg('bloomberg-statistics-grid', Clifton.bloomberg.BloombergStatisticsGrid);

