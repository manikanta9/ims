Clifton.bloomberg.QueryBloombergWindow = Ext.extend(TCG.app.Window, {
	id: 'bloombergQueryBloombergWindow',
	title: 'Bloomberg Lookup',
	iconCls: 'bloomberg',
	height: 500,
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},

	items: [
		{
			xtype: 'formpanel',
			instructions: 'Use this screen to lookup market data directly from Bloomberg.',
			labelWidth: 160,
			loadValidation: false, // using the form only to get background color/padding
			tbar: [{
				text: 'View Bloomberg Statistics',
				iconCls: 'doctor',
				xtype: 'button',
				handler: function(fp) {
					TCG.createComponent('Clifton.bloomberg.BloombergStatisticsWindow', {
						openerCt: fp
					});
				}
			}, '-'],
			items: [
				{fieldLabel: 'Symbol', name: 'symbol', allowBlank: false},
				{
					fieldLabel: 'Yellow Key (Not Required)', name: 'yellowKey', displayField: 'name', valueField: 'valueField', xtype: 'combo', mode: 'local', hiddenName: 'yellowKey',
					store: new Ext.data.ArrayStore({
						fields: ['name', 'valueField'],
						data: [['Client', 'Client'], ['Commodity', 'comdty'], ['Corporate', 'Corp'], ['Currency', 'Curncy'], ['Equity', 'Equity'], ['Government', 'Govt'], ['Index', 'Index'], ['Money Market', 'M-Mkt'], ['Mortgage', 'Mtge'], ['Municipal', 'Muni'], ['Preferred Stock', 'Pfd']]
					})
				},
				{fieldLabel: 'Investment Type', name: 'investmentTypeName', hiddenName: 'investmentTypeName', valueField: 'name', xtype: 'combo', loadAll: true, url: 'investmentTypeList.json', detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow', qtip: 'OPTIONAL - Used to manually specify an Investment Type for the requested security. Useful when we may not have the security in the system.'},
				{fieldLabel: 'Pricing Source', name: 'pricingSource', hiddenName: 'pricingSource', displayField: 'label', valueField: 'symbol', xtype: 'combo', url: 'marketDataSourcePricingSourceListFind.json?marketDataSourceName=Bloomberg', detailPageClass: 'Clifton.marketdata.datasource.DataSourcePricingSourceWindow'},
				{fieldLabel: 'Exchange Code', name: 'exchangeCodeId', hiddenName: 'exchangeCodeId', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json', detailPageClass: 'Clifton.investment.setup.ExchangeWindow', qtip: 'The main stock exchange where the security is bought and sold.'},
				{fieldLabel: 'Field', name: 'field', allowBlank: false, xtype: 'combo', forceSelection: false, displayField: 'externalFieldName', url: 'marketDataFieldListFind.json'},
				{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
				{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
				{fieldLabel: 'Overrides', name: 'overrides', qtip: 'Example: SETTLE_DT=20121128,SW_CURVE_DT=20121128'},
				{
					fieldLabel: 'Provider Override', name: 'providerOverride', displayField: 'name', valueField: 'valueField', xtype: 'combo', mode: 'local', hiddenName: 'providerOverride',
					store: new Ext.data.ArrayStore({
						fields: ['name', 'valueField', 'description'],
						data: Clifton.marketdata.provider.OVERRIDE_OPTIONS
					})
				},
				{fieldLabel: 'Provider Override Timeout', name: 'providerOverrideTimeout', xtype: 'integerfield', minValue: 0, qtip: 'The amount of time (in milliseconds) that should elapse before the market data request times out'},
				{fieldLabel: 'Scheduled Run Date', name: 'scheduleDate', xtype: 'datefield'},
				{fieldLabel: 'Scheduled Run Time', name: 'scheduleTime', xtype: 'timefield', format: 'H:i:s', qtip: 'Based on PST'}
			],
			buttonAlign: 'right',
			buttons: [
				{
					text: 'Load Data',
					handler: function() {
						const form = this.findParentByType('formpanel').getForm();
						const grid = Ext.getCmp('bloombergQueryBloombergGrid');
						const store = grid.grid.store;
						form.submit(Ext.applyIf({
							url: 'bloombergShortResponseList.json',
							success: function(form, action) {
								store.loadData(action.result, true);
							}
						}, Ext.applyIf({timeout: 200}, TCG.form.submitDefaults)));
					}
				}
			]
		},


		{
			flex: 1,
			xtype: 'panel',
			layout: 'fit',
			layoutConfig: {align: 'stretch'},
			items: [{
				layout: 'vbox',
				xtype: 'gridpanel',
				id: 'bloombergQueryBloombergGrid',
				columns: [
					{header: 'Source Name', dataIndex: 'machineName'},
					{header: 'Security', dataIndex: 'securityName'},
					{header: 'Field Name', dataIndex: 'fieldName'},
					{
						header: 'Field Value', dataIndex: 'fieldValue',
						renderer: function(v) {
							if (Ext.type(v) === 'array') {
								let result = '';
								for (let i = 0; i < v.length; i++) {
									const o = v[i];
									result += (i === 0) ? '{' : ',{';
									for (const p of Object.keys(o)) {
										result += p + ' = ' + o[p];
									}
									result += '}';
								}
								return result;
							}
							return v;
						}
					},
					{header: 'Field Date', dataIndex: 'fieldDate', type: 'date'}
				],
				createStore: function(fields) {
					const gridPanel = this;
					const storeConf = {
						fields: fields,
						listeners: {
							datachanged: function(store, records, opts) {
								gridPanel.updateCount();
							},
							load: function(store, records, opts) {
								const a = function() {
									return this.grid.getView().focusRow(store.getCount() - 1);
								};
								a.defer(500, this);
							}, scope: this
						}
					};
					const storeClass = TCG.data.JsonStore;

					return new storeClass(storeConf);
				}
			}]
		}
	]
});
