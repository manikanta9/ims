Clifton.bloomberg.BloombergStatisticsWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Bloomberg Service Statistics',
	width: 1200,
	height: 700,

	items: [{
		xtype: 'bloomberg-statistics-formpanel'
	}]
});
