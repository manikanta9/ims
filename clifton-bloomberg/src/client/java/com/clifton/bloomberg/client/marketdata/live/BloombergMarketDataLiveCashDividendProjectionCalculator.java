package com.clifton.bloomberg.client.marketdata.live;

import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveFieldValueCalculator;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * <code>BloombergMarketDataLiveCashDividendProjectionCalculator</code> is a {@link com.clifton.marketdata.live.MarketDataLiveFieldValueCalculator}
 * for looking up historic and projected cash dividends for stock securities. The options with a stock underlying can also use this calculator.
 * <p>
 * Refer to {@link DividendFunctions} for values that this calculator is capable of calculating from a dividend schedule calculated from a
 * {@link BloombergMarketDataLiveTableCalculator} producing a dividend schedule.
 *
 * @author NickK
 */
public class BloombergMarketDataLiveCashDividendProjectionCalculator extends BaseBloombergMarketDataLiveCalculator implements ValidationAware {

	private Integer dividendScheduleCalculatorBeanId;
	private DividendFunctions dividendFunction;

	private SystemBeanService systemBeanService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getDividendScheduleCalculatorBeanId(), "A Dividend Schedule Calculator Bean is required", "dividendScheduleCalculatorBeanId");
		ValidationUtils.assertNotNull(getDividendFunction(), "A dividend project function is required to calculate the live value from historic and projected dividends", "dividendFunction");

		Object calculator = getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(getDividendScheduleCalculatorBeanId()));
		ValidationUtils.assertTrue(calculator instanceof BloombergMarketDataLiveTableCalculator, "The defined calculator must be of a Market Data Live Cash Dividend Schedule Calculator");
	}


	@Override
	public MarketDataLive calculate(InvestmentSecurity security, MarketDataField field, MarketDataLiveCommand command) {
		MarketDataLiveFieldValueCalculator calculator = (MarketDataLiveFieldValueCalculator) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(getDividendScheduleCalculatorBeanId()));
		MarketDataLive liveScheduleValue = calculator.calculate(security, field, command);
		if (liveScheduleValue == null || liveScheduleValue.getValue() == null) {
			return null;
		}
		AssertUtils.assertTrue(liveScheduleValue.getDataTypeName() == DataTypeNames.TABLE, "The dividend schedule is not the expected data type of " + DataTypeNames.TABLE);
		Object schedule = liveScheduleValue.getValue();
		if (schedule instanceof DataTable) {
			DataTable dividendSchedule = (DataTable) schedule;
			Object value = getDividendFunction().calculate(dividendSchedule, security, DateUtils.clearTime(new Date()));
			return value == null ? null : new MarketDataLive(security.getId(), field.getName(), field.getDataType().asDataTypeNames(), value, new Date());
		}
		else {
			throw new RuntimeException("The dividend schedule retrieved is not a Dividend Projection Schedule");
		}
	}


	private static Date getExDateColumnValueForRow(DataRow row) {
		return (row == null) ? null : (Date) row.getValue("Ex Date");
	}


	private static BigDecimal getDividendAmountForRow(DataRow row) {
		return (row == null) ? null : (BigDecimal) row.getValue("Amount");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	private enum DividendFunctions {
		/**
		 * For a stock security is used in the request, returns the next ex date on or after the current day.
		 * If an option security is used in the request, returns the next ex date for the underlying stock or fund of the option.
		 */
		NEXT_EX_DATE {
			@Override
			Object calculate(DataTable dataTable, InvestmentSecurity security, Date startDate) {
				return dataTable.getRowList().stream()
						.map(BloombergMarketDataLiveCashDividendProjectionCalculator::getExDateColumnValueForRow)
						.filter(exDate -> DateUtils.isDateAfterOrEqual(exDate, startDate))
						.findFirst()
						.orElse(null);
			}
		},
		/**
		 * For a stock security is used in the request, returns the previous ex date before the current day.
		 * If an option security is used in the request, returns the previous ex date for the underlying stock or fund of the option.
		 */
		PREVIOUS_EX_DATE {
			@Override
			Object calculate(DataTable dataTable, InvestmentSecurity security, Date startDate) {
				List<DataRow> rowList = dataTable.getRowList();
				Date response = null;
				for (DataRow row : rowList) {
					Date exDate = getExDateColumnValueForRow(row);
					if (DateUtils.isDateAfterOrEqual(exDate, startDate)) {
						break;
					}
					else {
						response = exDate;
					}
				}
				return response;
			}
		},
		/**
		 * For a stock security is used in the request, returns the next dividend amount on or after the current day.
		 * If an option security is used in the request, returns the next dividend amount for the underlying stock or fund of the option.
		 */
		NEXT_DIVIDEND {
			@Override
			Object calculate(DataTable dataTable, InvestmentSecurity security, Date startDate) {
				// get first dividend
				return dataTable.getRowList().stream()
						.filter(row -> DateUtils.isDateAfterOrEqual(getExDateColumnValueForRow(row), startDate))
						.map(BloombergMarketDataLiveCashDividendProjectionCalculator::getDividendAmountForRow)
						.findFirst()
						.orElse(null);
			}
		},
		/**
		 * For a stock security is used in the request, returns the previous dividend amount before the current day.
		 * If an option security is used in the request, returns the previous dividend amount for the underlying stock or fund of the option.
		 */
		PREVIOUS_DIVIDEND {
			@Override
			Object calculate(DataTable dataTable, InvestmentSecurity security, Date startDate) {
				List<DataRow> rowList = dataTable.getRowList();
				BigDecimal response = null;
				for (DataRow row : rowList) {
					Date exDate = getExDateColumnValueForRow(row);
					if (DateUtils.isDateAfterOrEqual(exDate, startDate)) {
						break;
					}
					else {
						response = getDividendAmountForRow(row);
					}
				}
				return response;
			}
		},
		/**
		 * If a stock security is used in the request, behaves the same as {@link #NEXT_DIVIDEND}.
		 * If an option security is used in the request, returns the aggregated dividend amounts of all dividends from the current day until and including the option's expiration date.
		 */
		PROJECTED_DIVIDEND {
			@Override
			Object calculate(DataTable dataTable, InvestmentSecurity security, Date startDate) {
				if (InvestmentUtils.isOption(security)) {
					// aggregate dividends now through option's expiration
					return dataTable.getRowList().stream()
							.filter(row -> {
								Date exDate = getExDateColumnValueForRow(row);
								if (DateUtils.isDateAfterOrEqual(exDate, startDate)) {
									return DateUtils.isDateBeforeOrEqual(exDate, security.getLastDeliveryDate(), false);
								}
								return false;
							})
							.map(BloombergMarketDataLiveCashDividendProjectionCalculator::getDividendAmountForRow)
							.reduce(BigDecimal.ZERO, MathUtils::add);
				}
				else {
					return NEXT_DIVIDEND.calculate(dataTable, security, startDate);
				}
			}
		};


		abstract Object calculate(DataTable dataTable, InvestmentSecurity security, Date startDate);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getDividendScheduleCalculatorBeanId() {
		return this.dividendScheduleCalculatorBeanId;
	}


	public void setDividendScheduleCalculatorBeanId(Integer dividendScheduleCalculatorBeanId) {
		this.dividendScheduleCalculatorBeanId = dividendScheduleCalculatorBeanId;
	}


	public DividendFunctions getDividendFunction() {
		return this.dividendFunction;
	}


	public void setDividendFunction(DividendFunctions dividendFunction) {
		this.dividendFunction = dividendFunction;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
