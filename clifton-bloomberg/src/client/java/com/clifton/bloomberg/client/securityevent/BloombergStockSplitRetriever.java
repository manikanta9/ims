package com.clifton.bloomberg.client.securityevent;


import com.clifton.bloomberg.client.BloombergRequestCommand;
import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventStatus;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BloombergStockSplitRetriever</code> performs stock split event updates for securities.
 * <p>
 * A stock split or stock divide increases the number of shares in a public company.
 * The price is adjusted such that the before and after market capitalization of the company
 * remains the same and dilution does not occur.
 * <p>
 * This retriever pulls in the latest stock split events that will occur in the future.
 *
 * @author apopp
 */
public class BloombergStockSplitRetriever extends BaseBloombergEventRetriever {

	/**
	 * The date in which a company makes public that they have decided to perform a stock split in the near future.
	 * This is to give notice that it will occur prior to it actually occurring.
	 */
	private static final String DECLARE_DATE_FIELD = "EQY_DVD_SPL_ANN_DT_NEXT";

	/**
	 * The date the stock split will actually occur and trading begins on the split adjusted basis.
	 */
	private static final String EX_DATE_FIELD = "EQY_DVD_SPL_EX_DT_NEXT";

	/**
	 * Traditionally, the record date was used to determine who should receive shares from the stock split.
	 * It is the date the issuer looks at its records to determine who holds positions.  In today's world
	 * this is no longer used and as long as you hold shares by the actual split date (EX_DATE_FIELD), your
	 * shares will be affected.  Record date was meaningful and used in the days of paper certificates and is
	 * now handled by brokers and issuers directly.
	 */
	private static final String RECORD_DATE_FIELD = "EQY_DVD_SPL_RECORD_DT_NEXT";

	/**
	 * Date which the issuer will make payment of the most recent stock split to holders.
	 */
	private static final String EVENT_DATE_FIELD = "EQY_DVD_SPL_PAY_DT_NEXT";

	/**
	 * Factor that the price, earnings per share, and share data will be adjusted by as a result of the most recent stock split.
	 * Factor adjustments can occur in many ways, usually as "1 for 2" or "2 for 1" stock splits.
	 */
	private static final String ADJUSTMENT_RATIO_FIELD = "EQY_DVD_SPL_SPLIT_RATIO_NEXT";

	/**
	 * The type of IMS security event this retriever represents
	 */
	private static final String EVENT_TYPE = "Stock Split";

	/**
	 * Bloomberg error code indicating the security did not support the requested field
	 */
	private static final String BAD_FIELD = "BAD_FLD";

	////////////////////////////////////////////////////////////////////////////

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentInstrumentService investmentInstrumentService;


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEvent(InvestmentSecurity security) {
		ReferenceDataResponse response = performStockSplitRequest(security);

		if (response == null) {
			return null;
		}

		String ratio = (String) response.getSingleSecurityPropertyValue(ADJUSTMENT_RATIO_FIELD);
		Date declare = (Date) response.getSingleSecurityPropertyValue(DECLARE_DATE_FIELD);
		Date ex = (Date) response.getSingleSecurityPropertyValue(EX_DATE_FIELD);
		Date record = (Date) response.getSingleSecurityPropertyValue(RECORD_DATE_FIELD);
		Date event = (Date) response.getSingleSecurityPropertyValue(EVENT_DATE_FIELD);

		return buildStockSplitEvent(security, ratio, declare, ex, record, event);
	}


	/**
	 * Given an investment security query Bloomberg for any current stock split events.
	 */
	private ReferenceDataResponse performStockSplitRequest(InvestmentSecurity security) {
		ReferenceDataRequest dataRequest = new ReferenceDataRequest(ADJUSTMENT_RATIO_FIELD, DECLARE_DATE_FIELD, EX_DATE_FIELD, RECORD_DATE_FIELD, EVENT_DATE_FIELD);
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		dataRequest.addSecurity(command.getBloombergIdentifier());

		ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);

		if (response.getBloomError() != null) {
			BloombergError error = response.getBloomError();

			if ((response.getSingleSecurityPropertyValue(ADJUSTMENT_RATIO_FIELD) != null)
					&& (response.getSingleSecurityPropertyValue(EX_DATE_FIELD) != null || response.getSingleSecurityPropertyValue(RECORD_DATE_FIELD) != null)) {
				//some fields may not exist but we only need the adjustment ratio and ex or record date to create the event
				return response;
			}
			else if (BAD_FIELD.equals(error.getCategory())) {
				//adjustment ratio or ex and record date doesn't exist therefore stock splits most likely do not apply to this security
				return null;
			}
			throw new BloombergRequestException(error); //received an error we cannot handle
		}

		//At this point we either have a good response or one
		//  with sufficient fields to build the event
		return response;
	}


	/**
	 * Build a stock split event from supplied parameters.
	 * In order to build the event we need the stock split ratio and either the ex or record date.
	 * <p>
	 * If ex date is null then set ex date to two business days prior to record date.
	 * This is defined by Regulation 703.02 (part 2) Stock Split/Stock Rights/Stock Dividend Listing Process which states that
	 * "Normally, a distribution of less than 25 % is traded "ex" (without the distribution) on and after the second business day prior to the record date. "
	 */
	private InvestmentSecurityEvent buildStockSplitEvent(InvestmentSecurity security, String ratio, Date declare, Date ex, Date record, Date event) {
		ValidationUtils.assertTrue(ex != null || record != null, "Could not find stock split ex or record date missing for security [" + security.getSymbol() + "]");
		ValidationUtils.assertNotNull(ratio, "Could not find stock split ratio for security [" + security.getSymbol() + "]");

		//sanity clean up
		ratio = ratio.toLowerCase();
		ratio = ratio.trim();

		ValidationUtils.assertTrue(ratio.contains("for"), "Stock split ratio has incorrect format for security [" + security.getSymbol() + "], expected ratio containing 'for' but received [" + ratio
				+ "]");

		InvestmentSecurityEvent splitEvent = new InvestmentSecurityEvent();
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(EVENT_TYPE);
		splitEvent.setType(eventType);
		splitEvent.setStatus(getInvestmentSecurityEventService().getInvestmentSecurityEventStatusByName(InvestmentSecurityEventStatus.STATUS_APPROVED));
		splitEvent.setSecurity(security);

		//Bloomberg sends this field as "number for number", ex "1 for 2"
		splitEvent.setBeforeEventValue(new BigDecimal(ratio.split(" ")[2]));
		splitEvent.setAfterEventValue(new BigDecimal(ratio.split(" ")[0]));

		if (ex == null) {
			// there are instances when Bloomberg doesn't return EX date which is required: assume 2 business days before Record Date
			ex = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(record, getInvestmentCalculator().getInvestmentSecurityCalendar(splitEvent.getSecurity()).getId()), -2);
		}

		splitEvent.setExDate(ex);
		splitEvent.setDeclareDate((declare != null) ? declare : ex);
		splitEvent.setEventDate((event != null) ? event : ex);
		splitEvent.setRecordDate((record != null) ? record : ex);

		return splitEvent;
	}


	@Override
	@SuppressWarnings("unused")
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventHistory(InvestmentSecurity security, boolean ignoreSeedEvents) {
		throw new RuntimeException("Historical stock split retrieval is not supported");
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
