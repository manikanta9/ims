package com.clifton.bloomberg.client;

import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.marketdata.provider.subscription.event.MarketDataSubscriptionDataEvent;


/**
 * <code>BloombergClientSubscriptionAsynchronousProcessor</code> is a client side {@link AsynchronousProcessor} for
 * consuming {@link SubscriptionDataResponse} messages.
 *
 * @author NickK
 */
public class BloombergClientSubscriptionAsynchronousProcessor implements AsynchronousProcessor<SubscriptionDataResponse> {


	private EventHandler eventHandler;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void process(SubscriptionDataResponse message, @SuppressWarnings("unused") AsynchronousMessageHandler handler) {
		if (message != null) {
			message.getSubscriptionData().forEach((security, securityData) -> {
				if (!CollectionUtils.isEmpty(securityData.getUserIdentitySet())) {
					LogUtils.debug(LogCommand.ofMessageSupplier(getClass(), () -> "Received Bloomberg subscription data for " + securityData));

					getEventHandler().raiseEvent(MarketDataSubscriptionDataEvent.ofEventTarget(securityData));
				}
			});
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
}
