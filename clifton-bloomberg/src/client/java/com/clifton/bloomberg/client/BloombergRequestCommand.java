package com.clifton.bloomberg.client;


import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.datasource.MarketDataSourceSecurityHolder;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;


/**
 * The <code>BloombergRequestCommand</code> is a helper class that holds Bloomberg request parameters: symbol, yellow key, overrides, etc.
 *
 * @author vgomelsky
 */
public class BloombergRequestCommand extends MarketDataSourceSecurityHolder {

	private final String pricingSource;
	private final String marketSector;
	private final String exchangeCode;
	private final String investmentType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BloombergRequestCommand(MarketDataSourceSecurityHolder holder, MarketDataFieldMapping mapping, InvestmentSecurity security) {
		super(holder.getOriginalSymbol(), holder.getDataSourceSymbol(), holder.getExternalFieldOverridesForLatest(), holder.getExternalFieldOverridesForHistoric());
		this.pricingSource = mapping.getPricingSource() != null ? mapping.getPricingSource().getSymbol() : null;
		this.exchangeCode = mapping.getExchangeCodeType() != null ? mapping.getExchangeCodeType().getSecurityExchangeCode(security) : null;
		this.marketSector = mapping.getMarketSector().getName(); // yellow key
		this.investmentType = InvestmentUtils.getInvestmentTypeNameForSecurity(security);
	}


	public BloombergRequestCommand(MarketDataSourceSecurityHolder holder, String pricingSource, String marketSector, String exchangeCode, String investmentType) {
		super(holder.getOriginalSymbol(), holder.getDataSourceSymbol(), holder.getExternalFieldOverridesForLatest(), holder.getExternalFieldOverridesForHistoric());
		this.pricingSource = pricingSource;
		this.marketSector = marketSector; // yellow key
		this.exchangeCode = exchangeCode;
		this.investmentType = investmentType;
	}


	public BloombergRequestCommand(MarketDataSourceSecurityHolder holder, String pricingSource, String marketSector, String exchangeCode, InvestmentSecurity security) {
		this(holder, pricingSource, marketSector, exchangeCode, InvestmentUtils.getInvestmentTypeNameForSecurity(security));
	}


	public BloombergRequestCommand(String symbol, String pricingSource, String marketSector, String exchangeCode, String investmentType) {
		this(new MarketDataSourceSecurityHolder(symbol), pricingSource, marketSector, exchangeCode, investmentType);
	}


	public BloombergRequestCommand(String symbol, String marketSector) {
		this(symbol, null, marketSector, null, null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBloombergIdentifier() {
		String symbol = getSymbol();
		return getBloombergIdentifierBySymbolAndSector(symbol, this.marketSector);
	}


	public String getBloombergIdentifierBySymbolAndSector(String symbol, String marketSector) {
		if (!StringUtils.isEmpty(this.exchangeCode)) {
			String exchCodeSuffix = " " + this.exchangeCode;
			// make sure primary exchange is not already part of the symbol
			if (!symbol.endsWith(exchCodeSuffix)) {
				symbol = symbol + exchCodeSuffix;
			}
		}
		if (StringUtils.isEmpty(this.pricingSource)) {
			return symbol.toUpperCase() + " " + marketSector;
		}
		return symbol.toUpperCase() + "@" + this.pricingSource + " " + marketSector;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getPricingSource() {
		return this.pricingSource;
	}


	public String getMarketSector() {
		return this.marketSector;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}
}
