package com.clifton.bloomberg.client.analytics;

import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.BloombergOperations;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.analytics.BaseMessageServiceAnalytics;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;


/**
 * This class is a container to hold analytic data for a Bloomberg service
 *
 * @author MitchellF
 */
public class BloombergServiceAnalytics extends BaseMessageServiceAnalytics {


	private final Map<BloombergOperations, LongAdder> operationsCount = new ConcurrentHashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BloombergServiceAnalytics(String name, String serviceType) {
		super(name, serviceType);
	}


	@Override
	public void updateAnalytics(Long requestTime, MessagingMessage request, MessagingMessage response, String messageSource) {

		super.updateAnalytics(requestTime, request, response, messageSource);

		BloombergOperations operation = (BloombergOperations) BeanUtils.getPropertyValue(request, "operation");
		if (operation != null) {
			updateOperationsCount(operation);
		}
	}


	@Override
	public void clear() {
		super.clear();
		getOperationsCount().values().forEach(LongAdder::reset);
	}


	@Override
	protected String getErrorMessageForResponse(MessagingMessage request, MessagingMessage response) {

		StringBuilder messageToReturn = new StringBuilder();

		BloombergError bloombergError = (BloombergError) BeanUtils.getPropertyValue(response, "bloomError");
		if (bloombergError == null) {
			Throwable err = (Throwable) BeanUtils.getPropertyValue(response, "error");
			if (err != null) {
				messageToReturn.append(err.getMessage());
			}
		}
		else {
			messageToReturn.append((new BloombergRequestException(bloombergError)).getMessage());
		}
		if (messageToReturn.length() > 0) {
			return messageToReturn
					.append("\nRequest Details: ").append(request)
					.append("\nService URL: ").append(((BloombergRequestMessage) request).getServiceUrl())
					.append("\nOperation: ").append(((BloombergRequestMessage) request).getOperation())
					.append("\nRequest Identity: ").append(((BloombergRequestMessage) request).getIdentity())
					.toString();
		}
		return null;
	}


	public void updateOperationsCount(BloombergOperations operation) {
		this.operationsCount.computeIfAbsent(operation, o -> new LongAdder()).increment();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and setters                       //////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<BloombergOperations, LongAdder> getOperationsCount() {
		return this.operationsCount;
	}
}
