package com.clifton.bloomberg.client;


import com.clifton.bloomberg.messages.BloombergMarketSectors;
import com.clifton.bloomberg.messages.BloombergPeriodicity;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.BloombergRequestIdentity;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage.ExpressionVariables;
import com.clifton.bloomberg.messages.request.HistoricalDataRequest;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.request.SubscriptionRequest;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.converter.template.TemplateConverter.TemplateValueRetriever;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceSecurityHolder;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.DataFieldValueResponse;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import com.clifton.marketdata.provider.MarketDataUserIdentity;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import com.clifton.marketdata.rates.MarketDataInterestRateIndexMapping;
import com.clifton.marketdata.rates.transformation.MarketDataValueTransformer;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.SystemDataType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>BloombergMarketDataProvider</code> class implements retrieval of market data information
 * from the Bloomberg data source.
 */
public class BloombergMarketDataProvider<T extends MarketDataValueGeneric<?>> implements MarketDataProvider<T> {

	public static final String DATASOURCE_NAME = "Bloomberg";

	private BloombergDataService bloombergDataService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private SecurityUserService securityUserService;
	private SystemBeanService systemBeanService;

	private InvestmentCalculator investmentCalculator;
	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getMarketDataSourceName() {
		return DATASOURCE_NAME;
	}

	////////////////////////////////////////////////////////////////////////////
	//////               Interest Rates Retrieval Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataInterestRate getMarketDataInterestRateLatest(MarketDataInterestRateIndexMapping mapping) {
		if (mapping.getReferenceTwo() != null) {
			if (mapping.getReferenceOne().isActive()) {
				BloombergRequestCommand command = new BloombergRequestCommand(mapping.getSymbol(), mapping.getReferenceTwo().getName());
				ReferenceDataResponse response = getReferenceDataResponseForSingleSecurity(command, null, new MarketDataField(BloombergFields.ASK));
				MarketDataInterestRate rate = populateMarketDataInterestRateFromBloombergResponse(response, BloombergFields.ASK, new Date());
				rate.setInterestRateIndex(mapping.getReferenceOne());
				return rate;
			}
		}
		return null;
	}


	@Override
	public MarketDataInterestRate getMarketDataInterestRateForDate(MarketDataInterestRateIndexMapping mapping, Date date) {
		if (mapping.getReferenceOne().isActiveOnDate(date)) {
			return CollectionUtils.getFirstElement(getMarketDataInterestRateListForPeriod(mapping, date, date));
		}
		return null;
	}


	@Override
	public List<MarketDataInterestRate> getMarketDataInterestRateListForPeriod(MarketDataInterestRateIndexMapping mapping, Date fromDate, Date toDate) {
		if (mapping.getReferenceTwo() != null) {
			BloombergRequestCommand command = new BloombergRequestCommand(mapping.getSymbol(), mapping.getReferenceTwo().getName());
			HistoricalDataResponse response = getHistoricalDataResponse(command, fromDate, toDate, new MarketDataField(BloombergFields.PX_LAST));
			List<MarketDataInterestRate> returnRates = new ArrayList<>();
			for (Map.Entry<Date, ReferenceDataResponse> dateReferenceDataResponseEntry : response.getValues().entrySet()) {
				if (mapping.getReferenceOne().isActiveOnDate(dateReferenceDataResponseEntry.getKey())) {
					ReferenceDataResponse referenceDataResponse = dateReferenceDataResponseEntry.getValue();
					MarketDataInterestRate rate = populateMarketDataInterestRateFromBloombergResponse(referenceDataResponse, BloombergFields.PX_LAST, dateReferenceDataResponseEntry.getKey());
					rate.setInterestRateIndex(mapping.getReferenceOne());
					if (mapping.getValueTransformationBean() != null) {
						SystemBean transformationBean = mapping.getValueTransformationBean();
						MarketDataValueTransformer<BigDecimal> transformer = (MarketDataValueTransformer<BigDecimal>) getSystemBeanService().getBeanInstance(transformationBean);
						rate.setInterestRate(transformer.transform(rate.getInterestRate()));
					}
					returnRates.add(rate);
				}
			}
			return returnRates;
		}
		return null;
	}


	private MarketDataInterestRate populateMarketDataInterestRateFromBloombergResponse(ReferenceDataResponse response, String fieldName, Date date) {
		MarketDataInterestRate rate = new MarketDataInterestRate();
		rate.setInterestRateDate(DateUtils.clearTime(date));
		rate.setDataSource(getBloombergDataSource());
		for (Map<String, Object> fields : response.getValues().values()) {
			BigDecimal value = BigDecimal.valueOf((Double) fields.get(fieldName)); // ASK and PX_LAST are both double
			rate.setInterestRate(value.setScale(9, RoundingMode.HALF_UP)); //Have to round out result to 9 digits for Hibernate to store as decimal(19,10)
		}
		return rate;
	}

	////////////////////////////////////////////////////////////////////////////
	//////               Exchange Rates Retrieval Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataExchangeRate getMarketDataExchangeRateLatest(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency) {
		BloombergRequestCommand command = new BloombergRequestCommand(fromCurrency.getSymbol() + toCurrency.getSymbol(), BloombergMarketSectors.CURRENCY.getKeyName());
		ReferenceDataResponse response = getReferenceDataResponseForSingleSecurity(command, null, new MarketDataField(BloombergFields.PX_LAST));
		MarketDataExchangeRate rate = populateMarketDataExchangeRateFromBloombergResponse(response, BloombergFields.PX_LAST, new Date());
		rate.setFromCurrency(fromCurrency);
		rate.setToCurrency(toCurrency);
		return rate;
	}


	@Override
	public MarketDataExchangeRate getMarketDataExchangeRateForDate(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date date) {
		return getMarketDataExchangeRateListForPeriod(fromCurrency, toCurrency, date, date).get(0);
	}


	@Override
	public List<MarketDataExchangeRate> getMarketDataExchangeRateListForPeriod(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date fromDate, Date toDate) {
		if (fromDate == null) {
			throw new IllegalArgumentException("The start/from date cannot be null");
		}
		if (toDate == null) {
			LogUtils.info(getClass(), "End/to date is null, Bloomberg defaults null to current date.");
		}
		BloombergRequestCommand command = new BloombergRequestCommand(fromCurrency.getSymbol() + toCurrency.getSymbol(), BloombergMarketSectors.CURRENCY.getKeyName());
		HistoricalDataResponse response = getHistoricalDataResponse(command, fromDate, toDate, new MarketDataField(BloombergFields.PX_LAST));
		List<MarketDataExchangeRate> returnRates = new ArrayList<>();
		for (Map.Entry<Date, ReferenceDataResponse> dateReferenceDataResponseEntry : response.getValues().entrySet()) {
			ReferenceDataResponse referenceDataResponse = dateReferenceDataResponseEntry.getValue();
			MarketDataExchangeRate rate = populateMarketDataExchangeRateFromBloombergResponse(referenceDataResponse, BloombergFields.PX_LAST, dateReferenceDataResponseEntry.getKey());
			rate.setFromCurrency(fromCurrency);
			rate.setToCurrency(toCurrency);
			returnRates.add(rate);
		}
		return returnRates;
	}


	private MarketDataExchangeRate populateMarketDataExchangeRateFromBloombergResponse(ReferenceDataResponse response, String fieldName, Date date) {
		MarketDataExchangeRate rate = new MarketDataExchangeRate();
		rate.setRateDate(DateUtils.clearTime(date));
		rate.setDataSource(getBloombergDataSource());
		for (Map<String, Object> fields : response.getValues().values()) {
			BigDecimal value = BigDecimal.valueOf((Double) fields.get(fieldName)); // ASK and PX_LAST are both double
			rate.setExchangeRate(value.setScale(9, RoundingMode.HALF_UP)); //Have to round out result to 9 digits for Hibernate to store as decimal(19,10)
		}
		return rate;
	}

	////////////////////////////////////////////////////////////////////////////
	//////              Data Field Value Retrieval Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataFieldValueResponse<T> getMarketDataValueLatest(DataFieldValueCommand command) {
		Date today = new Date();
		ReferenceDataResponse response = getReferenceDataResponse(command, today);
		return DataFieldValueResponse.createResponse(getSecurityMarketDataValueMapFromBloombergResponse(response, today, command));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private HistoricalDataRequest createHistoricalDataRequest(DataFieldValueCommand command) {
		HistoricalDataRequest request = new HistoricalDataRequest();
		request.setStartDate(command.getStartDate());
		request.setEndDate(command.getEndDate());
		request.setPeriodicitySelection(BloombergPeriodicity.DAILY);
		request.setRequestTimeoutOverrideIfLarger(command.getMarketDataProviderTimeoutOverride());
		request.setConsecutiveErrorCountDisabled(command.getConsecutiveErrorCountDisabled());
		request.setRetryCountOverride(command.getRetryCountOverride());

		if (command.hasProviderOverride()) {
			MarketDataProviderOverrides providerOverride = command.getProviderOverride();
			request.getProperties().put(providerOverride.getOverrideKey(), providerOverride.getOverrideValue());
		}
		return request;
	}


	@Override
	public List<T> getMarketDataValueListHistorical(DataFieldValueCommand command, List<String> errors) {
		if (command.getStartDate() == null) {
			throw new IllegalArgumentException("The start/from date cannot be null");
		}
		if (command.getEndDate() == null) {
			LogUtils.info(getClass(), "End/to date is null, Bloomberg defaults null to current date.");
		}
		List<T> result = new ArrayList<>();
		boolean suppressException = true;

		Map<InvestmentSecurity, BloombergRequestCommand> commandMap = new HashMap<>();
		Set<MarketDataField> fieldSet = new HashSet<>();

		boolean batchSizeOverrideEnabled = command.isBatchSizeEnabled();

		HistoricalDataRequest request = createHistoricalDataRequest(command);

		HistoricalDataResponse response = new HistoricalDataResponse();
		for (InvestmentSecurity security : command.getInvestmentSecurityList()) {
			// skip securities that are no longer active during the specified date range
			if (security.getEndDate() == null || DateUtils.isDateBeforeOrEqual(command.getEndDate(), security.getEndDate(), false)) {
				//do not request data for inactive instruments
				if (!security.getInstrument().isInactive()) {
					//By default the batch size override is 1; so a single request will be made per security
					//In the event 0 is specified; all securities will be sent in one request
					if (batchSizeOverrideEnabled && MathUtils.isEqual(CollectionUtils.getSize(request.getSecurityList()), command.getMarketDataProviderBatchSizeOverride())) {
						HistoricalDataResponse batchResponse = getBloombergDataService().getHistoricalData(request, suppressException);
						mergeHistoricDataResponse(response, batchResponse);
						request = createHistoricalDataRequest(command);
					}
					BloombergRequestCommand bloombergRequestCommand = new BloombergRequestCommand(getBloombergSymbol(security.getSymbol(), command.getMarketSector()), command.getPricingSource(), command.getMarketSector(), command.getExchangeCodeFromType(security), security);
					commandMap.put(security, bloombergRequestCommand);

					boolean includeInRequest = true;
					for (MarketDataField marketDataField : command.getDataFieldList(security)) {
						fieldSet.add(marketDataField);
						String overrides = StringUtils.coalesce(false, marketDataField.getExternalFieldOverridesForHistoric(), bloombergRequestCommand.getExternalFieldOverridesForHistoric());
						if (request.isDateUsedInOverrides(overrides)) {
							includeInRequest = false;
							//If overriding date, then perform ReferenceDataRequest to populate the value
							HistoricalDataResponse overridesResponse = performExternalFieldOverrideHistoricalDataRequest(bloombergRequestCommand, command.getStartDate(), command.getEndDate(), command.getDataFieldList(security).toArray(new MarketDataField[0]));
							mergeHistoricDataResponse(response, overridesResponse);
							break;
						}
						suppressException = populateRequestForMarketDataField(request, marketDataField, overrides, suppressException);
					}

					if (includeInRequest) {
						request.addSecurity(bloombergRequestCommand.getBloombergIdentifier());
						request.setInvestmentType(bloombergRequestCommand.getInvestmentType());
					}
				}
			}
		}

		try {
			if (!CollectionUtils.isEmpty(request.getSecurityList())) {
				HistoricalDataResponse batchResponse = getBloombergDataService().getHistoricalData(request, suppressException);
				mergeHistoricDataResponse(response, batchResponse);
			}

			List<MarketDataField> fieldList = new ArrayList<>(fieldSet);
			for (InvestmentSecurity security : command.getInvestmentSecurityList()) {
				for (Map.Entry<Date, ReferenceDataResponse> dateReferenceDataResponseEntry : response.getValues().entrySet()) {
					extractValuesFromResponse(result, dateReferenceDataResponseEntry.getValue(), commandMap.get(security), security, fieldList, dateReferenceDataResponseEntry.getKey(), errors);
				}
			}
		}
		catch (BloombergRequestException e) {
			throw new ValidationException("Error getting historic data: " + e.getMessage(), e);
		}

		return result;
	}
	////////////////////////////////////////////////////////////////////////////
	//////                 Data Field Subscription Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<MarketDataSubscriptionSecurityData> subscribeToMarketData(DataFieldValueCommand command) {
		return processMarketDataSubscription(command, MarketDataSubscriptionOperations.SUBSCRIBE);
	}


	@Override
	public List<MarketDataSubscriptionSecurityData> unsubscribeToMarketData(DataFieldValueCommand command) {
		return processMarketDataSubscription(command, MarketDataSubscriptionOperations.UNSUBSCRIBE);
	}


	@Override
	public List<MarketDataSubscriptionSecurityData> processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations subscriptionOperation) {
		SubscriptionRequest request = new SubscriptionRequest(subscriptionOperation);
		return processSubscriptionRequest(request);
	}
	////////////////////////////////////////////////////////////////////////////
	//////                     Misc Helpers                            /////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataSourceSecurityHolder getBloombergSymbol(String securitySymbol, String marketSector) {
		return getMarketDataSourceService().getMarketDataSourceSecuritySymbol(securitySymbol, getMarketDataSourceName(), marketSector);
	}


	private List<String> getRequestFields(String fieldName) {
		List<String> result = new ArrayList<>();
		if (!StringUtils.isEmpty(fieldName)) {
			if (fieldName.contains(",")) {
				String[] fieldList = fieldName.split(",");
				for (String field : fieldList) {
					if (!StringUtils.isEmpty(field)) {
						result.add(field);
					}
				}
			}
			else {
				result.add(fieldName);
			}
		}
		return result;
	}

	///////////////////////////////////////////////////////////////////////////
	////////////            Response Data Extraction              /////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Extracts the specified fields for the specified security from the specified response.
	 * If errors are present, they're added to the specified errors List.  If the List is null, corresponding exception is thrown.
	 */
	private void extractValuesFromResponse(List<T> returnValues, ReferenceDataResponse response, BloombergRequestCommand bloombergRequestCommand, InvestmentSecurity security, List<MarketDataField> fieldList, Date date, List<String> errors) {
		for (MarketDataField field : fieldList) {
			if (field.isLatestValueOnly() && (DateUtils.compare(new Date(), date, false) != 0)) {
				continue;
			}
			try {
				T value = getMarketDataValueFromBloombergResponse(response, field, date, security, bloombergRequestCommand);
				LogUtils.info(getClass(), field + " on " + date + " = " + value);
				if (value != null) {
					returnValues.add(value);
				}
			}
			catch (Throwable e) {
				if (errors == null) {
					throw e;
				}
				errors.add("'" + field.getName() + "' for '" + security.getSymbol() + "': " + e.getMessage());
			}
		}
	}


	@SuppressWarnings("unchecked")
	private T getMarketDataValueFromBloombergResponse(ReferenceDataResponse referenceDataResponse, MarketDataField dataField, Date date, InvestmentSecurity security, BloombergRequestCommand bloombergRequestCommand) {
		T value = null;
		// field value map will be null if the security was not included in the request (e.g. security is no longer active)
		Map<String, Object> fieldValueMap = referenceDataResponse.getValues().get(bloombergRequestCommand.getBloombergIdentifier());
		if (fieldValueMap != null) {
			if (SystemDataType.INTEGER.equals(dataField.getDataType().getName())) {
				value = (T) new MarketDataValueGeneric<Integer>();
			}
			else if (dataField.getDataType().isNumeric()) {
				value = (T) new MarketDataValue();
			}
			else {
				value = (T) new MarketDataValueGeneric<>();
			}

			value.setDataField(dataField);
			value.setDataSource(getBloombergDataSource());
			value.setInvestmentSecurity(security);
			if (dataField.isTimeSensitive()) {
				value.setMeasureTime(new Time(date));
			}
			value.setMeasureDate(DateUtils.clearTime(date));

			Object bloomValue = fieldValueMap.get(dataField.getExternalFieldName());
			// has a value expression, get expression value as string then convert to its specific data type.
			if (!StringUtils.isEmpty(dataField.getValueExpression())) {
				if (security != null) {
					addSystemFields(fieldValueMap, security);
				}
				String expressionValue = getTemplateConverter().convertExpression(dataField.getValueExpression(), fieldValueMap);
				if (StringUtils.isEmpty(expressionValue)) {
					bloomValue = null; // empty string means no value: null
				}
				else if (dataField.getDataType().isNumeric()) {
					// evaluate the expression and return BigDecimal
					bloomValue = CoreMathUtils.evaluateExpression(expressionValue);
				}
				else {
					bloomValue = expressionValue;
				}
				// bloomValue is null, BigDecimal or String; convert to integer based on field type.
				if (bloomValue != null && SystemDataType.INTEGER.equals(dataField.getDataType().getName())) {
					if (bloomValue instanceof String) {
						bloomValue = Integer.parseInt((String) bloomValue);
					}
					else {
						// must be a BigDecimal
						bloomValue = ((BigDecimal) bloomValue).intValue();
					}
				}
			}

			if (bloomValue == null) {
				// not an error, just no data available for the field on the specified date (no settlement price until the market is closed, etc.)
				LogUtils.info(getClass(), "Bloomberg did not send back a value for field " + dataField.getExternalFieldName() + " for " + (security != null ? security : bloombergRequestCommand.getBloombergIdentifier()) + " on " + date);
				return null;
			}
			else if (dataField.getDataType().isNumeric()) {
				// if this market data field has an expression specified, then calculate its field value from it
				if (bloomValue instanceof BigDecimal) {
					BigDecimal fieldValue = (BigDecimal) bloomValue;
					((MarketDataValue) value).setMeasureValue(fieldValue.setScale(dataField.getDecimalPrecision(), RoundingMode.HALF_UP));
				}
				else if (bloomValue instanceof Double) {
					// Only support numeric values at the moment
					BigDecimal fieldValue = BigDecimal.valueOf((Double) bloomValue);
					((MarketDataValue) value).setMeasureValue(fieldValue.setScale(dataField.getDecimalPrecision(), RoundingMode.HALF_UP));
				}
				else if (bloomValue instanceof Integer) {
					// Only support numeric values at the moment
					((MarketDataValueGeneric<Integer>) value).setMeasureValue((Integer) bloomValue);
				}
				else {
					throw new IllegalStateException("When no Value Expression is defined for field '" + dataField.getName() + "', the value for '" + dataField.getExternalFieldName()
							+ "' must be numeric: " + bloomValue);
				}
			}
			else {
				((MarketDataValueGeneric<Object>) value).setMeasureValue(bloomValue);
			}
		}
		return value;
	}


	private Map<InvestmentSecurity, Map<MarketDataField, T>> getSecurityMarketDataValueMapFromBloombergResponse(ReferenceDataResponse referenceDataResponse, Date date, DataFieldValueCommand command) {
		Map<InvestmentSecurity, Map<MarketDataField, T>> results = new HashMap<>();
		command.getInvestmentSecurityList().forEach(security -> {
			Map<MarketDataField, T> dataFieldMap = new HashMap<>();
			try {
				BloombergRequestCommand bloombergRequestCommand = createBloombergRequestCommand(security, command);
				for (MarketDataField dataField : command.getDataFieldList(security)) {
					dataFieldMap.put(dataField, getMarketDataValueFromBloombergResponse(referenceDataResponse, dataField, date, security, bloombergRequestCommand));
				}
			}
			catch (ValidationException e) {
				if (!command.isSuppressErrors()) {
					throw e;
				}
				command.getErrors().add(e.getMessage());
			}
			results.put(security, dataFieldMap);
		});
		return results;
	}


	/**
	 * Add internally defined entities to our field mapping for use with our market data field value expression.
	 * Ex. priceMultiplier from investmentSecurity
	 */
	private void addSystemFields(Map<String, Object> fields, InvestmentSecurity security) {
		fields.put("investmentSecurity", security);
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////              Reference Data Request             ////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieve "refdata" from Bloomberg for the specified {@link DataFieldValueCommand} and date.
	 * If multiple {@link InvestmentSecurity}s are included in the command, a single request will be attempted.
	 * If multiple requests are made, the responses will be merged so only one {@link ReferenceDataResponse} is returned.
	 */
	private ReferenceDataResponse getReferenceDataResponse(DataFieldValueCommand command, Date date) {
		ReferenceDataRequest request = new ReferenceDataRequest();
		ReferenceDataResponse response = new ReferenceDataResponse();
		boolean suppressException = command.isSuppressErrors();
		if (command.hasProviderOverride()) {
			MarketDataProviderOverrides providerOverride = command.getProviderOverride();
			request.getProperties().put(providerOverride.getOverrideKey(), providerOverride.getOverrideValue());
		}
		request.setRequestTimeoutOverrideIfLarger(command.getMarketDataProviderTimeoutOverride());

		request.setConsecutiveErrorCountDisabled(command.getConsecutiveErrorCountDisabled());
		request.setRetryCountOverride(command.getRetryCountOverride());

		for (InvestmentSecurity security : command.getInvestmentSecurityList()) {
			if (security.getEndDate() == null || security.getEndDate().after(date)) {
				try {
					if (CollectionUtils.getSize(request.getSecurityList()) == 10) {
						// Have more than 10 securities, we need to batch the requests and merge the results. Create a new request after each batch
						performReferenceDataRequestMergingResults(response, request, suppressException, command.getErrors());
						request = new ReferenceDataRequest();
					}
					BloombergRequestCommand requestCommand = createBloombergRequestCommand(security, command);
					populateReferenceDataRequestResponseForBloombergRequestCommand(response, request, command.getDataFieldList(security), requestCommand, suppressException, command.getErrors());
				}
				catch (ValidationException e) {
					if (!command.isSuppressErrors()) {
						throw e;
					}
					command.getErrors().add(e.getMessage());
				}
			}
		}
		performReferenceDataRequestMergingResults(response, request, suppressException, command.getErrors());
		if (Objects.nonNull(response.getBloomError()) && Objects.isNull(command.getErrors())) {
			// If exceptions were suppressed and there was not a provided list of strings to append, to throw an exception saying so.
			// This is here to avoid eating errors without having them returned to the caller in some way.
			throw new IllegalArgumentException("Cannot append data provider error to an unspecified error list: " + response.getBloomError().getMessage());
		}
		return response;
	}


	/**
	 * Retrieve "refdata" from Bloomberg for the specified {@link BloombergRequestCommand} and {@link MarketDataField}s.
	 * If errorList is not null, errors encountered will be added to it rather than throwing an exception.
	 */
	private ReferenceDataResponse getReferenceDataResponseForSingleSecurity(BloombergRequestCommand command, List<String> errorList, MarketDataField... marketDataFieldList) {
		ReferenceDataResponse response = new ReferenceDataResponse();
		ReferenceDataRequest request = new ReferenceDataRequest();
		boolean suppressException = false;

		populateReferenceDataRequestResponseForBloombergRequestCommand(response, request, CollectionUtils.createList(marketDataFieldList), command, suppressException, errorList);
		performReferenceDataRequestMergingResults(response, request, suppressException, errorList);
		if (Objects.nonNull(response.getBloomError()) && Objects.isNull(errorList)) {
			// If exceptions were suppressed and there was not a provided list of strings to append, to throw an exception saying so.
			// This is here to avoid eating errors without having them returned to the caller in some way.
			throw new IllegalArgumentException("Cannot append data provider error to an unspecified error list: " + response.getBloomError().getMessage());
		}
		return response;
	}


	private BloombergRequestCommand createBloombergRequestCommand(InvestmentSecurity security, DataFieldValueCommand command) {
		if (command.getMarketSector() == null) {
			MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), getBloombergDataSource());
			if (mapping == null) {
				throw new ValidationException("Bloomberg Market Data Field Mapping is not defined for security [" + security.getSymbol() + "].  Unable to determine which market sector to lookup values from.");
			}
			String marketSector = mapping.getMarketSector().getName();
			return new BloombergRequestCommand(getBloombergSymbol(security.getSymbol(), marketSector), mapping, security);
		}
		else {
			String exchangeCode = command.getExchangeCode() == null ? command.getExchangeCodeFromType(security) : command.getExchangeCode();
			return new BloombergRequestCommand(getBloombergSymbol(security.getSymbol(),
					command.getMarketSector()), command.getPricingSource(), command.getMarketSector(), exchangeCode, security);
		}
	}


	/**
	 * Populates the provided request with the security and fields provided. If the {@link MarketDataField} is overridden,
	 * an override request is submitted and the response is merged into the provided response for the security instead of
	 * adding the field to the request for the security.
	 */
	private void populateReferenceDataRequestResponseForBloombergRequestCommand(ReferenceDataResponse responseToPopulate, ReferenceDataRequest requestToPopulate, List<MarketDataField> dataFields, BloombergRequestCommand bloombergRequestCommand, boolean suppressException, List<String> errorList) {
		requestToPopulate.addSecurity(bloombergRequestCommand.getBloombergIdentifier());
		for (MarketDataField marketDataField : dataFields) {
			if (marketDataField.getSymbolOverrideLookupField() != null) {
				performSymbolOverrideLookupFieldRequestMergingResults(responseToPopulate, marketDataField, bloombergRequestCommand, suppressException, errorList);
			}
			else {
				suppressException = populateRequestForMarketDataFieldWithLatestOverrides(requestToPopulate, marketDataField, bloombergRequestCommand, suppressException);
			}
		}
	}


	/**
	 * Submits the {@link ReferenceDataRequest} and merges all of the {@link ReferenceDataResponse} values with the provided response destination.
	 *
	 * @param responseDestination The response that data values retrieved from the provided request should be merged into
	 * @param request             The request to submit to Bloomberg
	 * @param suppressException   If true, the errors from Bloomberg will be suppressed
	 * @param errorList           If non-null, any errors coming from Bloomberg will be appended to it
	 */
	private void performReferenceDataRequestMergingResults(ReferenceDataResponse responseDestination, ReferenceDataRequest request, boolean suppressException, List<String> errorList) {
		performReferenceDataRequestMergingResults(responseDestination, request, suppressException, errorList, null);
	}


	/**
	 * Submits the {@link ReferenceDataRequest} and merges all of the {@link ReferenceDataResponse} values into the provided response destination under the
	 * provided Bloomberg identifier key.
	 *
	 * @param responseDestination        the response that data values retrieved from the provided request should be merged into
	 * @param request                    the request to submit to Bloomberg
	 * @param suppressException          if true, the errors from Bloomberg will be suppressed
	 * @param errorList                  if non-null, any errors coming from Bloomberg will be appended to it
	 * @param bloombergIdentifierToMerge if non-null, the response data values for the request will be merged into the destination response under this key
	 */
	private void performReferenceDataRequestMergingResults(ReferenceDataResponse responseDestination, ReferenceDataRequest request, boolean suppressException, List<String> errorList, String bloombergIdentifierToMerge) {
		// Do not make a request if there are no securities or fields in the request. This could happen if the security being requested is no longer active.
		if (!CollectionUtils.isEmpty(request.getSecurityList()) && !CollectionUtils.isEmpty(request.getBloombergRequestData().getElement(ReferenceDataRequest.FIELDS_KEY).getRequestData())) {
			try {
				ReferenceDataResponse fullResponse = getBloombergDataService().getReferenceData(request, suppressException);
				mergeReferenceDataResponses(responseDestination, fullResponse, bloombergIdentifierToMerge);
				if (fullResponse.getBloomError() != null) {
					if (errorList != null) {
						errorList.add(fullResponse.getErrorMessage());
					}
					responseDestination.setBloomError(fullResponse.getBloomError());
				}
			}
			catch (BloombergRequestException e) {
				if (errorList == null) {
					throw new IllegalArgumentException("Cannot append data provider error to an unspecified error list", e);
				}
				errorList.add("Securities: " + StringUtils.join(request.getSecurityList(), ",") + " Message: " + e.getMessage());
				responseDestination.setBloomError(e.getResponse().getBloomError());
			}
		}
	}


	/**
	 * Merges the response data values from the source response into the destination response.
	 *
	 * @param destinationResponse        destination response
	 * @param sourceResponse             response data values to put into the destination response
	 * @param bloombergIdentifierToMerge if non-null, the source data will be merged into the destination for this key
	 */
	private void mergeReferenceDataResponses(ReferenceDataResponse destinationResponse, ReferenceDataResponse sourceResponse, String bloombergIdentifierToMerge) {
		Map<String, Map<String, Object>> destination = destinationResponse.getValues();
		Map<String, Map<String, Object>> source = sourceResponse.getValues();
		source.forEach((key, valueMap) -> {
			String mergeKey = bloombergIdentifierToMerge == null ? key : bloombergIdentifierToMerge;
			if (destination.containsKey(mergeKey)) {
				destination.get(mergeKey).putAll(valueMap);
			}
			else {
				destination.put(mergeKey, valueMap);
			}
		});
	}


	private void mergeHistoricDataResponse(HistoricalDataResponse destinationResponse, HistoricalDataResponse sourceResponse) {
		Map<Date, ReferenceDataResponse> destinationDateToReferenceValueMap = destinationResponse.getValues();
		Map<Date, ReferenceDataResponse> sourceDateToReferenceValueMap = sourceResponse.getValues();
		sourceDateToReferenceValueMap.forEach((date, referenceData) -> {
			ReferenceDataResponse destinationReferenceData = destinationDateToReferenceValueMap.computeIfAbsent(date, k -> new ReferenceDataResponse());
			mergeReferenceDataResponses(destinationReferenceData, referenceData, null);
		});
	}


	/**
	 * Performs a request to Bloomberg for the symbol override lookup field and merges the response into the provided {@link ReferenceDataResponse}.
	 */
	private void performSymbolOverrideLookupFieldRequestMergingResults(ReferenceDataResponse responseDestination, MarketDataField marketDataField, BloombergRequestCommand bloombergRequestCommand, boolean suppressException, List<String> errorList) {
		String symbolOverrideValue = getSymbolOverrideLookupFieldValue(marketDataField, bloombergRequestCommand);
		if (symbolOverrideValue != null) {
			ReferenceDataRequest symbolOverrideRequest = new ReferenceDataRequest();
			symbolOverrideRequest.addSecurity(bloombergRequestCommand.getBloombergIdentifierBySymbolAndSector(symbolOverrideValue, (marketDataField.getSymbolOverrideMarketSector() != null) ? marketDataField.getSymbolOverrideMarketSector().getName() : null));
			suppressException = populateRequestForMarketDataFieldWithLatestOverrides(symbolOverrideRequest, marketDataField, bloombergRequestCommand, suppressException);
			performReferenceDataRequestMergingResults(responseDestination, symbolOverrideRequest, suppressException, errorList, bloombergRequestCommand.getBloombergIdentifier());
		}
	}


	private String getSymbolOverrideLookupFieldValue(MarketDataField marketDataField, BloombergRequestCommand command) {
		MarketDataField symbolLookupField = BeanUtils.cloneBean(marketDataField, true, false);
		symbolLookupField.setExternalFieldName(marketDataField.getSymbolOverrideLookupField());
		symbolLookupField.setSymbolOverrideLookupField(null);
		symbolLookupField.setSymbolOverrideMarketSector(null);
		symbolLookupField.setValueExpression(null);

		String symbolOverrideMarketSectorName = (marketDataField.getSymbolOverrideMarketSector() != null) ? marketDataField.getSymbolOverrideMarketSector().getName() : null;
		BloombergRequestCommand bloombergRequestCommand = new BloombergRequestCommand(getBloombergSymbol(command.getOriginalSymbol(), symbolOverrideMarketSectorName), null, symbolOverrideMarketSectorName, null, command.getInvestmentType());
		ReferenceDataResponse response = getReferenceDataResponseForSingleSecurity(bloombergRequestCommand, null, symbolLookupField);
		MarketDataValueGeneric<?> value = getMarketDataValueFromBloombergResponse(response, symbolLookupField, new Date(), null, bloombergRequestCommand);
		return (value != null) ? (String) value.getMeasureValue() : null;
	}


	private boolean populateRequestForMarketDataFieldWithLatestOverrides(BloombergRequestMessage request, MarketDataField marketDataField, BloombergRequestCommand command, boolean suppressException) {
		String overrides = StringUtils.coalesce(false, marketDataField.getExternalFieldOverridesForLatest(), command.getExternalFieldOverridesForLatest());
		return populateRequestForMarketDataField(request, marketDataField, overrides, suppressException);
	}


	private boolean populateRequestForMarketDataField(BloombergRequestMessage request, MarketDataField marketDataField, String overrides, boolean suppressException) {
		request.addFields(getRequestFields(marketDataField.getExternalFieldName()));
		request.setRequestTimeoutOverrideIfLarger(marketDataField.getRequestTimeoutMillis());

		//Add all fields we need from our value expression
		if (!StringUtils.isEmpty(marketDataField.getValueExpression()) && !MathUtils.isNumber(marketDataField.getValueExpression())) {
			request.addFields(parseFieldsFromExpression(marketDataField.getValueExpression()));
			suppressException = true;
		}

		if (!StringUtils.isEmpty(overrides)) {
			request.addOverrides(overrides);
		}

		return suppressException;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////              Historic Data Request              ////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieve historical data from Bloomberg for the specified {@link BloombergRequestCommand}, {@link MarketDataField}s, and date range.
	 */
	private HistoricalDataResponse getHistoricalDataResponse(BloombergRequestCommand command, Date fromDate, Date toDate, MarketDataField... marketDataFields) {
		String bloombergIdentifier = command.getBloombergIdentifier();
		HistoricalDataRequest request = new HistoricalDataRequest();
		request.setSecurity(bloombergIdentifier);
		Date startDate = DateUtils.clearTime(fromDate);
		Date endDate = DateUtils.clearTime(toDate);
		request.setStartDate(startDate);
		request.setEndDate(endDate);
		request.setPeriodicitySelection(BloombergPeriodicity.DAILY);
		boolean suppressException = marketDataFields.length > 1;

		for (MarketDataField marketDataField : marketDataFields) {
			String overrides = StringUtils.coalesce(false, marketDataField.getExternalFieldOverridesForHistoric(), command.getExternalFieldOverridesForHistoric());
			if (request.isDateUsedInOverrides(overrides)) {
				return performExternalFieldOverrideHistoricalDataRequest(command, startDate, endDate, marketDataFields);
			}
			suppressException = populateRequestForMarketDataField(request, marketDataField, overrides, suppressException);
		}

		// Perform a request for all securities that did not have historic data overridden
		try {
			return getBloombergDataService().getHistoricalData(request, suppressException);
		}
		catch (BloombergRequestException e) {
			throw new ValidationException("Error getting historic data for " + bloombergIdentifier + ": " + e.getMessage(), e);
		}
	}


	private HistoricalDataResponse performExternalFieldOverrideHistoricalDataRequest(BloombergRequestCommand command, Date startDate, Date endDate, MarketDataField... marketDataFields) {
		// need to convert historic into a reference request: dates are being overridden
		HistoricalDataResponse response = new HistoricalDataResponse();
		boolean suppressException = false;
		if (endDate == null) {
			endDate = DateUtils.clearTime(new Date());
		}
		if (startDate.compareTo(endDate) > 0) {
			throw new ValidationException("Start date " + startDate + " cannot be after end date " + endDate);
		}
		while (startDate.compareTo(endDate) <= 0) {
			// do a separate call for each date and then aggregate results
			ReferenceDataRequest referenceRequest = new ReferenceDataRequest();
			referenceRequest.addSecurity(command.getBloombergIdentifier());
			for (MarketDataField field : marketDataFields) {
				String fieldOverrides = StringUtils.coalesce(false, field.getExternalFieldOverridesForHistoric(), command.getExternalFieldOverridesForHistoric());
				suppressException = populateRequestForMarketDataField(referenceRequest, field, convertOverrides(fieldOverrides, command.getOriginalSymbol(), startDate), suppressException);
			}

			ReferenceDataResponse referenceResponse = getBloombergDataService().getReferenceData(referenceRequest, suppressException);
			response.getValues().put(startDate, referenceResponse);
			startDate = DateUtils.addDays(startDate, 1);
		}
		return response;
	}


	/**
	 * Returns overrides parameters string with actual parameter values using Bloomberg syntax.
	 * For example, "SETTLE_DT=20121128,SW_CURVE_DT=20121128" from "SETTLE_DT=${SETTLEMENT_DATE},SW_CURVE_DT=${DATE}"
	 */
	private String convertOverrides(String overrides, String securitySymbol, Date startDate) {
		return getTemplateConverter().convertOverrides(overrides, createOverrideMap(securitySymbol, startDate));
	}


	private Map<Enum<?>, TemplateValueRetriever> createOverrideMap(String symbol, Date date) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(symbol, null);

		if (security == null) {
			throw new ValidationException("Issue with handling market data field overrides, security [" + symbol + "] does not exist");
		}

		return createOverrideMap(security, date);
	}


	private Map<Enum<?>, TemplateValueRetriever> createOverrideMap(InvestmentSecurity security, Date date) {
		Map<Enum<?>, TemplateValueRetriever> expressionParameters = new HashMap<>();

		expressionParameters.put(ExpressionVariables.DATE, new BasicDateRetriever(date));
		expressionParameters.put(ExpressionVariables.SETTLEMENT_DATE, new SettlementDateRetriever(getInvestmentCalculator(), security, date));

		return expressionParameters;
	}


	private List<String> parseFieldsFromExpression(String expression) {
		return new ArrayList<>(getTemplateConverter().extractDataFields(expression, new String[]{"investmentSecurity"}));
	}


	private MarketDataSource getBloombergDataSource() {
		return getMarketDataSourceService().getMarketDataSourceByName(BloombergMarketDataProvider.DATASOURCE_NAME);
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////             Subscription Data Request              //////////
	///////////////////////////////////////////////////////////////////////////


	private List<MarketDataSubscriptionSecurityData> processMarketDataSubscription(DataFieldValueCommand command, MarketDataSubscriptionOperations operation) {
		if (CollectionUtils.isEmpty(command.getInvestmentSecurityList())) {
			return Collections.emptyList();
		}

		SubscriptionRequest subscriptionRequest = new SubscriptionRequest(operation);
		command.forEach((security, marketDataFieldList) -> {
			BloombergRequestCommand requestCommand = createBloombergRequestCommand(security, command);
			String[] fields = CollectionUtils.getStream(marketDataFieldList)
					.map(MarketDataField::getExternalFieldName)
					.toArray(String[]::new);
			subscriptionRequest.addSubscription(requestCommand.getBloombergIdentifier(), fields);
		});
		if (command.hasMarketDataUserIdentity()) {
			MarketDataUserIdentity identity = command.getMarketDataUserIdentity();
			if (identity instanceof BloombergRequestIdentity) {
				subscriptionRequest.setIdentity((BloombergRequestIdentity) identity);
			}
			else {
				SecurityUser user = getSecurityUserService().getSecurityUser(identity.getSecurityUserId());
				BloombergRequestIdentity bloombergRequestIdentity = BloombergRequestIdentity.of(user, identity.getClientIpAddress());
				subscriptionRequest.setIdentity(bloombergRequestIdentity);
			}
		}
		return processSubscriptionRequest(subscriptionRequest);
	}


	private List<MarketDataSubscriptionSecurityData> processSubscriptionRequest(SubscriptionRequest request) {
		SubscriptionDataResponse bloombergResponse = getBloombergDataService().processSubscriptionRequest(request);
		return CollectionUtils.getStream(bloombergResponse.getSubscriptionData().values())
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BloombergDataService getBloombergDataService() {
		return this.bloombergDataService;
	}


	public void setBloombergDataService(BloombergDataService bloombergDataService) {
		this.bloombergDataService = bloombergDataService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public class SettlementDateRetriever implements TemplateConverter.TemplateValueRetriever {

		private final InvestmentCalculator calculator;
		private final InvestmentSecurity security;
		private final Date startDate;


		public SettlementDateRetriever(InvestmentCalculator calculator, InvestmentSecurity security, Date startDate) {
			this.calculator = calculator;
			this.security = security;
			this.startDate = startDate;
		}


		@Override
		public String getValue() {
			return DateUtils.fromDate(this.calculator.calculateSettlementDate(this.security, null, this.startDate), BloombergRequestMessage.DATE_FORMAT);
		}
	}

	public class BasicDateRetriever implements TemplateConverter.TemplateValueRetriever {

		private final Date date;


		public BasicDateRetriever(Date date) {
			this.date = date;
		}


		@Override
		public String getValue() {
			return DateUtils.fromDate(this.date, BloombergRequestMessage.DATE_FORMAT);
		}
	}
}
