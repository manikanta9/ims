package com.clifton.bloomberg.client.securityevent;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommandInternal;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.bloomberg.client.BloombergFields;
import com.clifton.bloomberg.client.BloombergRequestCommand;
import com.clifton.bloomberg.messages.BloombergMarketSectors;
import com.clifton.bloomberg.messages.BloombergSequenceElement;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;


/**
 * The <code>BloombergCashCouponPaymentRetriever</code> class retrieves latest and historic Cash Coupon Payment
 * events from Bloomberg.
 * <p>
 * NOTE: initial refactoring kept most of Aaron's logic but placed it into a better design.
 *
 * @author vgomelsky
 */
public class BloombergCashCouponPaymentRetriever extends BaseBondBloombergEventRetriever {

	/**
	 * Bloomberg may change precision of coupon values from time to time.  Ignore within the following tolerance.
	 * For example, 2.7641 vs 2.764099 for 41161pfr9
	 */
	private BigDecimal roundingTolerance = new BigDecimal("0.000001");

	/**
	 * If existing event description equals ignoring case to this value, then it was entered manually and is likely inaccurate (best estimate).
	 * For example, at month end we might now know actual coupon rate for a floating coupon bond.  A temporary rate (previous coupon rate) is
	 * entered into the system with "manual entry" description which is used as best approximation for accrual calculation.
	 * <p>
	 * Once the real rate is known, we will update coupon value and description accordingly.
	 */
	private String preliminaryEventDescription = "manual entry";
	private String updatedEventDescription = "updated";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingJournalService accountingJournalService;
	private AccountingEventJournalService accountingEventJournalService;
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;
	private AccountingPostingService accountingPostingService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean prepareInvestmentSecurityEvent(InvestmentSecurityEvent event) {
		if (event == null) {
			return false; // skip
		}

		// adjust for End Of Day convention
		event.setAccrualEndDate(DateUtils.addDays(event.getAccrualEndDate(), 1));
		if (DateUtils.compare(event.getAccrualEndDate(), event.getExDate(), false) == 0) {
			event.setExDate(DateUtils.addDays(event.getExDate(), 1));
		}

		// get event for same date if exists or previous event
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(event.getSecurity().getId());
		searchForm.setTypeId(event.getType().getId());
		searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN_OR_EQUALS, event.getEventDate()));
		searchForm.setOrderBy("eventDate:desc#id:desc");
		searchForm.setLimit(1);
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		InvestmentSecurityEvent existingEvent = CollectionUtils.getOnlyElement(eventList);
		if (existingEvent != null) {
			// check if this is a duplicate event
			if (DateUtils.compare(existingEvent.getEventDate(), event.getEventDate(), false) == 0) {
				boolean skip = false;
				if (MathUtils.isLessThanOrEqual(MathUtils.subtract(existingEvent.getAfterEventValue(), event.getAfterEventValue()).abs(), getRoundingTolerance())) {
					skip = true;
				}

				if (getPreliminaryEventDescription().equalsIgnoreCase(existingEvent.getEventDescription())) {
					// may need to unbook/delete/re-generate/re-book corresponding event  journal if it exists
					AccountingEventJournalSearchForm journalSearchForm = new AccountingEventJournalSearchForm();
					journalSearchForm.setSecurityEventId(existingEvent.getId());
					List<AccountingEventJournal> journalList = getAccountingEventJournalService().getAccountingEventJournalList(journalSearchForm);
					if (!CollectionUtils.isEmpty(journalList)) {
						if (skip) {
							// same value: just update description and don't unbook rebook
							existingEvent.setEventDescription(getUpdatedEventDescription());
							// update precision to avoid validation errors
							existingEvent.setBeforeAndAfterEventValue(MathUtils.round(existingEvent.getBeforeEventValue(), existingEvent.getType().getDecimalPrecision()));
							getInvestmentSecurityEventService().saveInvestmentSecurityEvent(existingEvent);
							return false;
						}
						for (AccountingEventJournal eventJournal : journalList) {
							AccountingJournal journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(AccountingEventJournal.class.getSimpleName(), eventJournal.getId(), 2);
							if (journal != null) {
								getAccountingPostingService().unpostAccountingJournal(journal.getId());
							}
							journal = getAccountingJournalService().getAccountingJournalBySourceAndSequence(AccountingEventJournal.class.getSimpleName(), eventJournal.getId(), 1);
							if (journal != null) {
								getAccountingPostingService().unpostAccountingJournal(journal.getId());
							}
							getAccountingEventJournalService().deleteAccountingEventJournal(eventJournal.getId());
						}
					}

					// found manual entry that needs to be replaced with the actual from Bloomberg
					existingEvent.setEventDescription(getUpdatedEventDescription());
					existingEvent.setBeforeEventValue(MathUtils.round(event.getBeforeEventValue(), event.getType().getDecimalPrecision()));
					existingEvent.setAfterEventValue(MathUtils.round(event.getAfterEventValue(), event.getType().getDecimalPrecision()));
					existingEvent.setExDate(event.getExDate());
					existingEvent.setAccrualStartDate(event.getAccrualStartDate());
					existingEvent.setAccrualEndDate(event.getAccrualEndDate());
					getInvestmentSecurityEventService().saveInvestmentSecurityEvent(existingEvent);

					if (!CollectionUtils.isEmpty(journalList)) {
						getAccountingEventJournalGeneratorService().generateAccountingEventJournal(EventJournalGeneratorCommandInternal.forEvent(existingEvent, AccountingEventJournalGeneratorTypes.POST));
					}
					skip = true;
				}

				if (skip) {
					return false; // skip a duplicate
				}
				// updated coupon amount: insert duplicate and resolve notification (will need to research, decide which one is right, regenerate journals, etc.)
			}
			else {
				// previous event: update accrual start date if necessary
				int diff = DateUtils.getDaysDifference(event.getAccrualStartDate(), existingEvent.getAccrualEndDate());
				if (diff >= -10 && diff <= 10) { // 10 day tolerance: some bonds have non standard accrual periods: 709163ee5
					event.setAccrualStartDate(existingEvent.getAccrualEndDate());
				}
			}
		}

		return true; // good
	}


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEvent(InvestmentSecurity security) {
		String couponType = (String) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, "Coupon Type", false);
		if ("None".equalsIgnoreCase(couponType) || "Zero".equalsIgnoreCase(couponType)) {
			return null;
		}

		// request coupon specific fields for the specified security
		ReferenceDataRequest dataRequest = new ReferenceDataRequest("COMPLETELY_CALLED", "CALLED", "MATURITY", "CPN_TYP");
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		dataRequest.addSecurity(command.getBloombergIdentifier());
		if (command.getMarketSector().equals(BloombergMarketSectors.MORTGAGE.getKeyName())) {
			dataRequest.addField(BloombergFields.MTG_PAY_DELAY);
			dataRequest.addField("MTG_ACC_RT");
			dataRequest.addField("MTG_ACC_RT_START_DT");
			dataRequest.addField("MTG_ACC_RT_END_DT");
			dataRequest.addField(BloombergFields.NOMINAL_PAYMENT_DAY);
		}
		// always need this fields because mortgage specific fields are not always set: 280907BC9
		dataRequest.addField("CPN");
		dataRequest.addField("NXT_CPN_DT"); // The date on which the next coupon will be paid.
		// Date on which the previous coupon was paid based on the settlement date of the security if the date is later than the first coupon date.
		// For bonds that haven't reached the first coupon date, this field will equal the interest accrual date.
		dataRequest.addField("PREV_CPN_DT");
		dataRequest.addField(BloombergFields.LAST_REFIX_DT); // auction bonds may not have next date but last date instead: 709163EF2
		dataRequest.addField("CPN_FREQ");

		if (command.getMarketSector().equals(BloombergMarketSectors.GOVERNMENT.getKeyName())) {
			// British Linkers (Gilts) have Ex Date which is 8 days before period end date and use negative accrual from Ex to End of Period date
			// NOTE: there maybe other securities that use a different field for this
			dataRequest.addField("GILTS_EX_DVD_DT");
		}

		ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);
		if (response.getBloomError() != null && "BAD_SEC".equals(response.getBloomError().getCategory())) {
			throw new BloombergRequestException(response.getBloomError());
		}

		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT);
		event.setType(eventType);
		event.setStatus(event.getType().getForceEventStatus());
		event.setSecurity(security);

		for (Map<String, Object> fields : response.getValues().values()) {
			String cpnType = (String) fields.get("CPN_TYP");
			if ("FIXED".equals(cpnType) || "FIXED, OID".equals(cpnType) || "STEP CPN".equals(cpnType)) {
				// NOTE: these are usually auto-generated based on fixed schedule
				// do we want to skip them?  what if not in the system or different??? Ex Date for Gilts?
				// return null; // no need to update fixed CPNs, should already be generated
			}
			cpnType = couponType;
			if ("Auction".equals(cpnType)) {
				return null; // TODO: Steve may help find a way to get this information from Bloomberg
			}
			String called = (String) fields.get("CALLED");
			String completelyCalled = (String) fields.get("COMPLETELY_CALLED");
			if ("Y".equals(called) || "Y".equals(completelyCalled)) //the security has no pending coupon payment
			{
				return null;
			}
			Date nextPayDate;
			Double cpnAmount;
			Date startAccDate;
			Date endAccDate;
			Date exDate = null;
			if (command.getMarketSector().equals(BloombergMarketSectors.MORTGAGE.getKeyName()) && fields.get("MTG_ACC_RT") != null) { // 280907BC9 isn't really mortgage
				cpnAmount = (Double) fields.get("MTG_ACC_RT");
				startAccDate = (Date) fields.get("MTG_ACC_RT_START_DT");
				endAccDate = (Date) fields.get("MTG_ACC_RT_END_DT");
				// periods may shift because of weekends and holidays so nominal payment date doesn't stay mid month
				int paymentDelay = parsePaymentDelay((String) fields.get(BloombergFields.MTG_PAY_DELAY));
				nextPayDate = DateUtils.addDays(endAccDate, (paymentDelay + 1));
				int paymentDay = (Integer) fields.get(BloombergFields.NOMINAL_PAYMENT_DAY);
				if (DateUtils.getDayOfMonth(nextPayDate) != paymentDay && paymentDelay > 30) { //assumes month is correctly calculated; long delay bonds need this adjustment
					Calendar cal = new GregorianCalendar();
					cal.setTime(nextPayDate);
					cal.set(Calendar.DAY_OF_MONTH, paymentDay);
					nextPayDate = cal.getTime();
				}
			}
			else {
				nextPayDate = (Date) fields.get("NXT_CPN_DT");
				if (nextPayDate == null) {
					nextPayDate = (Date) fields.get(BloombergFields.LAST_REFIX_DT);
				}
				cpnAmount = (Double) fields.get("CPN");
				endAccDate = DateUtils.addDays(nextPayDate, -1);
				startAccDate = (Date) fields.get("PREV_CPN_DT");// frequency.getPeriodStartDate(endAccDate);
				exDate = (Date) fields.get("GILTS_EX_DVD_DT"); // British bonds: Gilts
			}

			if (startAccDate == null || cpnAmount == null) {
				return null; // the security has no pending coupon payment
			}

			BigDecimal coupon = BigDecimal.valueOf(cpnAmount).setScale(eventType.getDecimalPrecision(), BigDecimal.ROUND_HALF_UP);
			event.setBeforeAndAfterEventValue(coupon);
			event.setExDate(exDate); // British bonds: Gilts
			event.setAccrualStartDate(startAccDate);
			event.setAccrualEndDate(endAccDate);
			event.setPaymentDate(nextPayDate);
		}

		return event;
	}


	@Override
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventHistory(InvestmentSecurity security, @SuppressWarnings("unused") boolean ignoreSeedEvents) {
		// request historic coupon specific fields for the specified security
		ReferenceDataRequest dataRequest = new ReferenceDataRequest("CPN_FREQ", "CPN_TYP", "CPN", "FIRST_CPN_DT", "INT_ACC_DT", "EX_DIV_DAYS");
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		dataRequest.addSecurity(command.getBloombergIdentifier());
		dataRequest.addField(BloombergFields.MTG_PAY_DELAY);
		dataRequest.addField("MULTI_CPN_SCHEDULE");

		ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);
		if (response.getBloomError() != null && "BAD_SEC".equals(response.getBloomError().getCategory())) {
			throw new BloombergRequestException(response.getBloomError());
		}

		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT);
		for (Map<String, Object> fields : response.getValues().values()) {
			String cpnType = (String) fields.get("CPN_TYP");
			if ("ZERO COUPON".equals(cpnType)) {
				continue;
			}

			// Gilts (British Bonds) usually have Ex Date 8 days before payment (other securities have this field as null: End or Period + 1)
			Integer exDays = (Integer) fields.get("EX_DIV_DAYS");

			if ("TIPS".equals(security.getInstrument().getHierarchy().getName()) || "T-Notes".equals(security.getInstrument().getHierarchy().getName()) || "FIXED".equals(cpnType)
					|| "FIXED, OID".equals(cpnType)) {
				return createFixedCouponEventHistory((Integer) fields.get("CPN_FREQ"), (Double) fields.get("CPN"), (Date) fields.get("FIRST_CPN_DT"), eventType, security,
						parsePaymentDelay((String) fields.get(BloombergFields.MTG_PAY_DELAY)), exDays);
			}
			else if ("STEP CPN".equals(cpnType)) {
				List<InvestmentSecurityEvent> fixedCouponEvents = createFixedCouponEventHistory((Integer) fields.get("CPN_FREQ"), (Double) fields.get("CPN"), (Date) fields.get("FIRST_CPN_DT"),
						eventType, security, parsePaymentDelay((String) fields.get(BloombergFields.MTG_PAY_DELAY)), exDays);
				@SuppressWarnings("unchecked")
				List<StepSchedule> schedule = generateStepUpSchedule((List<BloombergSequenceElement>) fields.get("MULTI_CPN_SCHEDULE"), (Date) fields.get("INT_ACC_DT"));
				adjustCouponsWithSchedule(fixedCouponEvents, schedule);
				return fixedCouponEvents;
			}
			throw new ValidationException("Only fixed or step-up coupon historic generation is allowed. Bloomberg Coupon Type = " + cpnType);
		}

		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentSecurityEvent> createFixedCouponEventHistory(Integer freq, Double rate, Date firstCouponDate, InvestmentSecurityEventType couponEventType, InvestmentSecurity security, Integer paymentDelay, Integer exDays) {

		BloombergCouponFrequencies frequency = BloombergCouponFrequencies.getFreqFromValue(freq);
		Date periodPaymentDate = firstCouponDate;
		Date periodStartDate = frequency.getPeriodStartDate(DateUtils.addDays(periodPaymentDate, (paymentDelay + 1) * -1));
		int couponCount = 0;
		List<InvestmentSecurityEvent> events = new ArrayList<>();
		while (DateUtils.compare(periodPaymentDate, security.getEndDate(), false) <= 0) {
			InvestmentSecurityEvent couponEvent = createCouponEvent(rate, couponEventType, security, periodPaymentDate, periodStartDate, paymentDelay, exDays);
			events.add(couponEvent);
			periodPaymentDate = frequency.getNextPayDate(periodPaymentDate);
			periodStartDate = DateUtils.addDays(couponEvent.getAccrualEndDate(), 1);

			// safety check against infinite loops
			couponCount++;
			if (couponCount > 5000) {
				throw new RuntimeException("Infinite loop in createFixedCouponEventHistory(" + freq + ", " + rate + ", " + firstCouponDate + ", " + couponEventType + ", " + security + ", " + paymentDelay + ")");
			}
		}

		return events;
	}


	private InvestmentSecurityEvent createCouponEvent(Double rate, InvestmentSecurityEventType couponEventType, InvestmentSecurity security, Date periodPaymentDate, Date periodStartDate, int paymentDelay, Integer exDays) {
		InvestmentSecurityEvent couponEvent = new InvestmentSecurityEvent();
		couponEvent.setType(couponEventType);
		couponEvent.setStatus(couponEvent.getType().getForceEventStatus());
		couponEvent.setBeforeAndAfterEventValue(BigDecimal.valueOf(rate).setScale(couponEventType.getDecimalPrecision(), BigDecimal.ROUND_HALF_UP));
		couponEvent.setAccrualStartDate(periodStartDate);
		couponEvent.setAccrualEndDate(DateUtils.addDays(periodPaymentDate, (paymentDelay + 1) * -1));
		couponEvent.setPaymentDate(periodPaymentDate);
		if (exDays != null) {
			couponEvent.setExDate(DateUtils.addDays(periodPaymentDate, -exDays)); // Gilts (British bonds)
		}
		couponEvent.setSecurity(security);
		return couponEvent;
	}


	private List<StepSchedule> generateStepUpSchedule(List<BloombergSequenceElement> list, Date startDate) {
		Date scheduleStartDate = startDate;
		List<StepSchedule> schedule = new ArrayList<>(list.size());
		for (BloombergSequenceElement element : list) {
			Date periodEndDate = (Date) element.getValues().get("Period End Date");
			Double coupon = (Double) element.getValues().get("Coupon");
			schedule.add(new StepSchedule(BigDecimal.valueOf(coupon), scheduleStartDate, periodEndDate));
			scheduleStartDate = periodEndDate;
		}
		return schedule;
	}


	private void adjustCouponsWithSchedule(List<InvestmentSecurityEvent> fixedCouponEvents, List<StepSchedule> schedule) {
		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(fixedCouponEvents)) {
			event.setBeforeAndAfterEventValue(getCouponFromSchedule(schedule, event.getPaymentDate()).setScale(event.getType().getDecimalPrecision(), BigDecimal.ROUND_HALF_UP));
		}
	}


	private BigDecimal getCouponFromSchedule(List<StepSchedule> schedule, Date paymentDate) {
		for (StepSchedule item : schedule) {
			if (paymentDate.compareTo(item.getStartDate()) > 0 && paymentDate.compareTo(item.getEndDate()) <= 0) {
				return item.getCoupon();
			}
		}
		return BigDecimal.ONE;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getRoundingTolerance() {
		return this.roundingTolerance;
	}


	public void setRoundingTolerance(BigDecimal roundingTolerance) {
		this.roundingTolerance = roundingTolerance;
	}


	public String getPreliminaryEventDescription() {
		return this.preliminaryEventDescription;
	}


	public void setPreliminaryEventDescription(String preliminaryEventDescription) {
		this.preliminaryEventDescription = preliminaryEventDescription;
	}


	public String getUpdatedEventDescription() {
		return this.updatedEventDescription;
	}


	public void setUpdatedEventDescription(String updatedEventDescription) {
		this.updatedEventDescription = updatedEventDescription;
	}


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}


	public AccountingEventJournalGeneratorService getAccountingEventJournalGeneratorService() {
		return this.accountingEventJournalGeneratorService;
	}


	public void setAccountingEventJournalGeneratorService(AccountingEventJournalGeneratorService accountingEventJournalGeneratorService) {
		this.accountingEventJournalGeneratorService = accountingEventJournalGeneratorService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	private static class StepSchedule {

		BigDecimal coupon;
		Date startDate;
		Date endDate;


		public StepSchedule(BigDecimal coupon, Date startDate, Date endDate) {
			super();
			this.coupon = coupon;
			this.startDate = startDate;
			this.endDate = endDate;
		}


		public BigDecimal getCoupon() {
			return this.coupon;
		}


		public void setCoupon(BigDecimal coupon) {
			this.coupon = coupon;
		}


		public Date getStartDate() {
			return this.startDate;
		}


		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}


		public Date getEndDate() {
			return this.endDate;
		}


		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
	}
}
