package com.clifton.bloomberg.client;


import com.clifton.bloomberg.client.analytics.AggregateBloombergServiceAnalytics;
import com.clifton.bloomberg.client.analytics.BloombergServiceAnalytics;
import com.clifton.bloomberg.messages.BloombergPeriodicity;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.BloombergRequestIdentity;
import com.clifton.bloomberg.messages.request.BloombergRequestMessage;
import com.clifton.bloomberg.messages.request.HistoricalDataRequest;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.request.ServiceManagementRequest;
import com.clifton.bloomberg.messages.request.SubscriptionRequest;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.bloomberg.messages.response.ServiceManagementResponse;
import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.synchronous.SynchronousMessageSender;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.exchange.InvestmentExchangeService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import com.clifton.security.system.SecuritySystemService;
import com.clifton.security.system.SecuritySystemUser;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
@ExcludeFromComponentScan
public class BloombergDataServiceImpl implements BloombergDataService {

	@Resource
	private SynchronousMessageSender<BloombergRequestMessage> bloombergClientMessageSender;

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;
	private SecuritySystemService securitySystemService;
	private InvestmentExchangeService investmentExchangeService;
	private ContextHandler contextHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BloombergServiceAnalytics getBloombergServiceAnalyticsAggregate() {
		return (BloombergServiceAnalytics) getBloombergClientMessageSender().getAnalyticsContainer();
	}


	@Override
	public BloombergServiceAnalytics getBloombergServiceAnalytics(String serviceName) {
		BloombergServiceAnalytics bloombergServiceAnalytics = (BloombergServiceAnalytics) getBloombergClientMessageSender().getAnalyticsContainer();
		if (bloombergServiceAnalytics != null) {
			if (bloombergServiceAnalytics instanceof AggregateBloombergServiceAnalytics) {
				return ((AggregateBloombergServiceAnalytics) bloombergServiceAnalytics).getAnalyticsForService(serviceName);
			}
		}
		return bloombergServiceAnalytics;
	}


	@Override
	public void clearBloombergServiceAnalytics() {
		if (getBloombergClientMessageSender() != null && getBloombergClientMessageSender().getAnalyticsContainer() != null) {
			getBloombergClientMessageSender().getAnalyticsContainer().clear();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReferenceDataResponse getReferenceData(ReferenceDataRequest requestMessage) {
		return getReferenceData(requestMessage, false);
	}


	@Override
	public ReferenceDataResponse getReferenceData(ReferenceDataRequest requestMessage, boolean suppressException) {
		populateBloombergRequestWithIdentity(requestMessage);
		ReferenceDataResponse response = (ReferenceDataResponse) getBloombergClientMessageSender().call(requestMessage);
		if (response.getBloomError() != null) {
			if (!suppressException) {
				throw new BloombergRequestException(response.getBloomError(), response);
			}
			else {
				Exception e = new BloombergRequestException(response.getBloomError(), response);
				LogUtils.warn(getClass(), e.getMessage(), e);
			}
		}
		return response;
	}


	@Override
	public ReferenceDataResponse getReferenceData(List<String> securities, String... fields) {
		ReferenceDataRequest requestMessage = new ReferenceDataRequest(fields);
		requestMessage.addSecurityList(securities);
		return getReferenceData(requestMessage);
	}


	// This returns a Map<String, Object> because the Map<Date, Object> private member of the HistoricalDataResponse
	// class does not deserialize correctly (date conversion to a string in the JSON causes the deserialization to fail
	// when attempting to bind back to a date - custom deserializer could be used as an option as well, or change the
	// Map representation in the HistoricalDataResponse object to Map<String, ReferenceDataResponse>
	@Override
	public Map<String, ReferenceDataResponse> getHistoricalData(HistoricalDataRequest requestMessage) {
		HistoricalDataResponse historicalDataResponse = getHistoricalData(requestMessage, false);
		Map<String, ReferenceDataResponse> historicalDataMap = new HashMap<>();
		for (Map.Entry<Date, ReferenceDataResponse> dateReferenceDataResponseEntry : historicalDataResponse.getValues().entrySet()) {
			historicalDataMap.put(DateUtils.fromDate(dateReferenceDataResponseEntry.getKey(), DateUtils.FIX_DATE_FORMAT_INPUT), dateReferenceDataResponseEntry.getValue());
		}
		return historicalDataMap;
	}


	@Override
	public HistoricalDataResponse getHistoricalData(HistoricalDataRequest requestMessage, boolean suppressException) {
		populateBloombergRequestWithIdentity(requestMessage);
		HistoricalDataResponse response = (HistoricalDataResponse) getBloombergClientMessageSender().call(requestMessage);
		if (response.getBloomError() != null) {

			if (!suppressException) {
				throw new BloombergRequestException(response.getBloomError());
			}
			else {
				Exception e = new BloombergRequestException(response.getBloomError());
				LogUtils.warn(getClass(), "Bloomberg request returned a error that was suppressed: " + e.getMessage(), e);
			}
		}
		return response;
	}


	@Override
	public List<BloombergShortResponse> getBloombergShortResponseList(BloombergShortResponseCommand command) {
		ValidationUtils.assertNotEmpty(command.getField(), "Field(s) must be specified to make a Bloomberg request.");
		String[] requestFields = ArrayUtils.getStream(command.getField().toUpperCase().split(","))
				.distinct()
				.toArray(String[]::new);

		String securitySymbol = getBloombergSymbol(command);

		List<BloombergShortResponse> returnList = new ArrayList<>();
		if (command.getStartDate() == null) {
			// Reference Data request
			ReferenceDataRequest referenceRequest = new ReferenceDataRequest(requestFields);
			referenceRequest.addSecurityList(CollectionUtils.createList(securitySymbol));
			referenceRequest.addOverrides(command.getOverrides());
			if (!StringUtils.isEmpty(command.getProviderOverride())) {
				MarketDataProviderOverrides provider = MarketDataProviderOverrides.valueOf(command.getProviderOverride());
				referenceRequest.getProperties().put(provider.getOverrideKey(), provider.getOverrideValue());
			}
			referenceRequest.setScheduleDate(command.getScheduleDate());
			referenceRequest.setScheduleTime(command.getScheduleTime());
			ReferenceDataResponse response = getReferenceData(referenceRequest);

			Date now = new Date();
			for (String securityValue : response.getValues().keySet()) {
				Map<String, Object> fields = response.getValues().get(securityValue);
				for (String requestField : requestFields) {
					returnList.add(new BloombergShortResponse(now, requestField, fields.get(requestField), command.getSymbol(), response.getMachineName()));
				}
			}
			return returnList;
		}

		InvestmentSecurity securityToLookup = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(securitySymbol, null);
		String investmentTypeName = command.getInvestmentTypeName();
		if (StringUtils.isEmpty(investmentTypeName)) {
			investmentTypeName = InvestmentUtils.getInvestmentTypeNameForSecurity(securityToLookup);
		}

		// historical data request
		Date endDate = command.getEndDate();
		if (endDate == null) {
			endDate = new Date();
		}
		ValidationUtils.assertBefore(command.getStartDate(), endDate, "endDate");
		HistoricalDataRequest historicRequest = new HistoricalDataRequest(requestFields);
		historicRequest.setStartDate(command.getStartDate());
		historicRequest.setEndDate(endDate);
		historicRequest.setSecurity(securitySymbol);
		if (!StringUtils.isEmpty(investmentTypeName)) {
			historicRequest.setInvestmentType(investmentTypeName);
		}
		historicRequest.setPeriodicitySelection(BloombergPeriodicity.DAILY);
		historicRequest.addOverrides(command.getOverrides());
		if (!StringUtils.isEmpty(command.getProviderOverride())) {
			MarketDataProviderOverrides provider = MarketDataProviderOverrides.valueOf(command.getProviderOverride());
			historicRequest.getProperties().put(provider.getOverrideKey(), provider.getOverrideValue());
		}
		if (MathUtils.isGreaterThan(command.getProviderOverrideTimeout(), 0)) {
			historicRequest.setRequestTimeoutOverrideIfLarger(command.getProviderOverrideTimeout());
		}
		historicRequest.setScheduleDate(command.getScheduleDate());
		historicRequest.setScheduleTime(command.getScheduleTime());
		HistoricalDataResponse response = getHistoricalData(historicRequest, false);

		for (Date date : response.getValues().keySet()) {
			ReferenceDataResponse referenceDataResponse = response.getValues().get(date);
			for (String securityValue : referenceDataResponse.getValues().keySet()) {
				Map<String, Object> fields = referenceDataResponse.getValues().get(securityValue);
				for (String requestField : requestFields) {
					returnList.add(new BloombergShortResponse(date, requestField, fields.get(requestField), command.getSymbol(), response.getMachineName()));
				}
			}
		}

		returnList.sort(Comparator.comparing(BloombergShortResponse::getFieldDate));
		return returnList;
	}


	@Override
	public SubscriptionDataResponse processSubscriptionRequest(SubscriptionRequest subscriptionRequest) {
		ValidationUtils.assertNotNull(subscriptionRequest.getOperation(), "A Subscription Operation is required in order to process a subscription request.");
		ValidationUtils.assertTrue(subscriptionRequest.getBloombergRequestData().hasElements(), "The Subscription request contains no security subscriptions.");

		if (MarketDataProviderOverrides.TERMINAL.getOverrideValue().equals(getBloombergClientMessageSender().getMessageSource())) {
			// override the MessageSource to BPIPE to subscription requests
			MarketDataProviderOverrides override = MarketDataProviderOverrides.BPIPE;
			subscriptionRequest.getProperties().put(override.getOverrideKey(), override.getOverrideValue());
		}

		/*
		 * Subscription message routing should be directed to the same service for all requests to make sure the subscribe,
		 * unsubscribe, and list is accurate. We will assume that there will only be one BPIPE Bloomberg service. If multiple
		 * Bloomberg BPIPE services are available, a broadcast may be necessary (topic).
		 */
		populateBloombergRequestWithIdentity(subscriptionRequest);
		return (SubscriptionDataResponse) getBloombergClientMessageSender().call(subscriptionRequest);
	}


	@Override
	public ServiceManagementResponse clearBloombergServiceCachesForProvider(String providerName) {
		ServiceManagementRequest request = new ServiceManagementRequest(ServiceManagementRequest.ServiceManagementAction.CLEAR_CACHE_ALL);
		if (!StringUtils.isEmpty(providerName)) {
			MarketDataProviderOverrides provider = MarketDataProviderOverrides.valueOf(providerName);
			request.getProperties().put(provider.getOverrideKey(), provider.getOverrideValue());
		}
		return processServiceManagementAction(request);
	}


	@Override
	public ServiceManagementResponse processServiceManagementAction(ServiceManagementRequest managementRequest) {
		return (ServiceManagementResponse) getBloombergClientMessageSender().call(managementRequest);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataFieldMapping getMarketDataFieldMapping(InvestmentSecurity security) {
		MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSourceByName(BloombergMarketDataProvider.DATASOURCE_NAME);
		MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), dataSource);
		ValidationUtils.assertNotNull(mapping, "There is no Instrument Mapping setup for " + security.getLabel() + " to get the market sector from.");
		return mapping;
	}


	private MarketDataSource getBloombergDataSource() {
		return getMarketDataSourceService().getMarketDataSourceByName(BloombergMarketDataProvider.DATASOURCE_NAME);
	}


	private void populateBloombergRequestWithIdentity(BloombergRequestMessage requestMessage) {
		if (requestMessage.getIdentity() == null) {
			SecurityUser securityUser = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
			if (securityUser != null) {
				// Obtain client IP from the web request in the context. This assumes the client's IP is the same as the Bloomberg terminal/device IP.
				String clientIpAddress = (String) getContextHandler().getBean(Context.REQUEST_CLIENT_ADDRESS);
				BloombergRequestIdentity requestIdentity = BloombergRequestIdentity.of(securityUser, clientIpAddress);

				SecuritySystemUser securitySystemUser = getSecuritySystemService().getSecuritySystemUserBySystemIdAndUserId(getBloombergDataSource().getSecuritySystem().getId(), securityUser.getId());
				if (securitySystemUser != null && MathUtils.isNumber(securitySystemUser.getUserName())) {
					requestIdentity.setUuid(Integer.valueOf(securitySystemUser.getUserName()));
					// At some point it would be nice to obtain terminal IP so the client and terminal IPs do not have to match. Until then, the client IP will be used.
				}

				requestMessage.setIdentity(requestIdentity);
			}
		}
	}


	private String getBloombergSymbol(BloombergShortResponseCommand command) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(command.getSymbol(), null);
		ValidationUtils.assertNotNull(security, "No Security found for symbol " + command.getSymbol());

		String marketSector = command.getYellowKey();

		Lazy<MarketDataFieldMapping> fieldMappingLazy = new Lazy<>(() -> getMarketDataFieldMapping(security));

		if (StringUtils.isEmpty(command.getYellowKey())) {
			marketSector = fieldMappingLazy.get().getMarketSector().getName();
		}

		StringBuilder stringToReturn = new StringBuilder(command.getSymbol().toUpperCase());

		if (command.getExchangeCodeId() != null) {
			InvestmentExchange exchange = getInvestmentExchangeService().getInvestmentExchange(command.getExchangeCodeId());
			stringToReturn.append(" ")
					.append(exchange.getExchangeCode());
		}
		else if (fieldMappingLazy.get().getExchangeCodeType() != null) {
			stringToReturn.append(" ")
					.append(fieldMappingLazy.get().getExchangeCodeType().getSecurityExchangeCode(security));
		}

		if (command.getPricingSource() != null) {
			stringToReturn.append("@")
					.append(command.getPricingSource());
		}

		return stringToReturn.append(" ")
				.append(marketSector)
				.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SynchronousMessageSender<BloombergRequestMessage> getBloombergClientMessageSender() {
		return this.bloombergClientMessageSender;
	}


	public void setBloombergClientMessageSender(SynchronousMessageSender<BloombergRequestMessage> bloombergClientMessageSender) {
		this.bloombergClientMessageSender = bloombergClientMessageSender;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public SecuritySystemService getSecuritySystemService() {
		return this.securitySystemService;
	}


	public void setSecuritySystemService(SecuritySystemService securitySystemService) {
		this.securitySystemService = securitySystemService;
	}


	public InvestmentExchangeService getInvestmentExchangeService() {
		return this.investmentExchangeService;
	}


	public void setInvestmentExchangeService(InvestmentExchangeService investmentExchangeService) {
		this.investmentExchangeService = investmentExchangeService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
