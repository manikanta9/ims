package com.clifton.bloomberg.client.marketdata.live;

import com.clifton.bloomberg.client.BloombergRequestCommand;
import com.clifton.bloomberg.messages.BloombergSequenceElement;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.db.ClassToSqlTypeConverter;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;


/**
 * <code>BloombergMarketDataLiveTableCalculator</code> is a {@link com.clifton.marketdata.live.MarketDataLiveFieldValueCalculator}
 * for generating a table of data from a Bloomberg sequence or grid of data.
 * <p>
 * One use for this {@link com.clifton.system.bean.SystemBeanType} is in getting a dividend schedule for stocks and funds.
 *
 * @author NickK
 */
public class BloombergMarketDataLiveTableCalculator extends BaseBloombergMarketDataLiveCalculator implements ValidationAware {

	private static final String CACHE_NAME = "BloombergReferenceTables";

	private CacheHandler<Integer, MarketDataLiveTable> cacheHandler;

	private String[] tableColumnNames;
	private String[] bloombergPropertyNames;

	private Integer defaultAgeInSeconds = 60 * 60 * 12; // 12 hours
	private Boolean useTerminal;

	private String bloombergFieldOverride;
	private String sortColumn;

	private final AtomicReference<Map<String, String[]>> columnNameToExtractionFieldNamesMap = new AtomicReference<>();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getTableColumnNames(), "Table Column Names is required to define the table", "tableColumnNames");
		ValidationUtils.assertNotNull(getBloombergPropertyNames(), "Bloomberg Property Names is required for extracting sequence data values", "bloombergPropertyNames");
		ValidationUtils.assertTrue(getTableColumnNames().length == getBloombergPropertyNames().length, "Table Column Names and Bloomberg Property Names must contain the same number of elements");
	}


	@Override
	public MarketDataLive calculate(InvestmentSecurity security, MarketDataField field, MarketDataLiveCommand command) {
		MarketDataLiveTable dividendProjection = getMarketDataCashDividendProjection(security, field, command);
		if (dividendProjection == null) {
			return null;
		}
		return new MarketDataLive(security.getId(), field.getName(), DataTypeNames.TABLE, dividendProjection.getDataTable(), new Date());
	}


	private MarketDataLiveTable getMarketDataCashDividendProjection(InvestmentSecurity security, MarketDataField field, MarketDataLiveCommand command) {
		InvestmentSecurity requestSecurity = security;
		if (InvestmentUtils.isOption(security)) {
			requestSecurity = security.getUnderlyingSecurity();
		}
		if (!InvestmentUtils.isSecurityOfType(requestSecurity, InvestmentType.STOCKS) && !InvestmentUtils.isSecurityOfType(requestSecurity, InvestmentType.FUNDS)) {
			throw new ValidationException("Only able to retrieve cash dividend projections for Stocks and Options");
		}

		MarketDataLiveTable marketDataLiveTable = getMarketDataLiveTableFromCache(requestSecurity, command.getMaxAgeInSeconds());
		if (marketDataLiveTable == null) {
			marketDataLiveTable = getMarketDataLiveTableFromBloomberg(requestSecurity, field, command);
			cacheMarketDataLiveTable(requestSecurity, marketDataLiveTable);
		}
		return marketDataLiveTable;
	}


	private MarketDataLiveTable getMarketDataLiveTableFromCache(InvestmentSecurity security, Integer maxAgeInSeconds) {
		MarketDataLiveTable result = getCacheHandler().get(CACHE_NAME, security.getId());
		if (result != null) {
			// check if cache value is stale
			int maxAge = (maxAgeInSeconds == null) ? getDefaultAgeInSeconds() : maxAgeInSeconds;
			if (DateUtils.getSecondsDifference(new Date(), result.getMeasureDate()) > maxAge) {
				result = null;
			}
		}
		return result;
	}


	private void cacheMarketDataLiveTable(InvestmentSecurity security, MarketDataLiveTable projection) {
		getCacheHandler().put(CACHE_NAME, security.getId(), projection);
	}


	private MarketDataLiveTable getMarketDataLiveTableFromBloomberg(InvestmentSecurity security, MarketDataField field, MarketDataLiveCommand command) {
		ReferenceDataRequest request = createBloombergDataRequest(security, field, command);
		Date now = new Date();
		Map<String, Integer> columnToTypeMap = new HashMap<>();
		List<Object[]> dataRowList = new ArrayList<>();
		try {
			ReferenceDataResponse response = getBloombergDataService().getReferenceData(request);
			Map<String, Object> securityFieldValueMap = response.getValues().get(request.getSecurity());
			for (Map.Entry<String, Object> securityFieldValueEntry : securityFieldValueMap.entrySet()) {
				Object bloomValue = securityFieldValueEntry.getValue();
				if (bloomValue instanceof List) {
					// field for dividend sequence requested
					List<?> listValue = (List<?>) bloomValue;
					Object firstListValue = CollectionUtils.getFirstElement(listValue);
					if (firstListValue instanceof BloombergSequenceElement) {
						@SuppressWarnings("unchecked")
						List<BloombergSequenceElement> sequenceList = (List<BloombergSequenceElement>) listValue;

						ClassToSqlTypeConverter sqlTypeConverter = new ClassToSqlTypeConverter();
						sequenceList.forEach(element -> {
							// each element should have at a minimum the ex date and dividend per share
							Map<String, Object> elementValues = element.getValues();
							List<Object> valueList = new ArrayList<>();
							getDataTableColumnToExtractionFieldNamesMap().forEach((column, extractionPropertyNames) -> {
								Object columnValue = getResponseValueForKeys(elementValues, extractionPropertyNames);
								if (columnValue != null) {
									int columnType = sqlTypeConverter.convert(columnValue.getClass());
									switch (columnType) {
										case Types.TIMESTAMP: {
											columnValue = (columnValue instanceof Date) ? (Date) columnValue : DateUtils.toDate(columnValue.toString());
											break;
										}
										case Types.INTEGER:
										case Types.SMALLINT:
										case Types.BIGINT:
										case Types.DECIMAL: {
											columnType = Types.DECIMAL;
											columnValue = ((columnValue instanceof Number) ? BigDecimal.valueOf(((Number) columnValue).doubleValue()) : new BigDecimal(columnValue.toString()))
													.setScale(field.getDecimalPrecision(), RoundingMode.HALF_UP);
											break;
										}
										default: {
											// leave object value as is
										}
									}
									columnToTypeMap.put(column, columnType);
								}
								valueList.add(columnValue);
							});
							dataRowList.add(valueList.toArray());
						});
					}
				}
				else {
					throw new RuntimeException("Expected to receive a sequence of values from Bloomberg. Received: " + ((bloomValue == null) ? null : bloomValue.getClass()));
				}
			}
			DataColumn[] dataColumns = getDataTableColumnToExtractionFieldNamesMap().keySet().stream()
					.map(column -> new DataColumnImpl(column, columnToTypeMap.get(column)))
					.toArray(DataColumn[]::new);
			DataTable dataTable = new PagingDataTableImpl(dataColumns);
			dataRowList.forEach(row -> dataTable.addRow(new DataRowImpl(dataTable, row)));

			if (getSortColumn() != null) {
				dataTable.getRowList().sort((row1, row2) -> CompareUtils.compare(row1.getValue(getSortColumn()), row2.getValue(getSortColumn())));
			}
			return new MarketDataLiveTable(dataTable, now);
		}
		catch (BloombergRequestException e) {
			String errorCategory = e.getBloombergError().getCategory();
			if (errorCategory != null && errorCategory.contains("BAD_FLD")) {
				// if the field is not applicable avoid looking it up again until the age expires for a lookup
				DataColumn[] dataColumns = getDataTableColumnToExtractionFieldNamesMap().keySet().stream()
						.map(columnName -> new DataColumnImpl(columnName, Types.VARCHAR))
						.toArray(DataColumn[]::new);
				return new MarketDataLiveTable(new PagingDataTableImpl(dataColumns), now);
			}
			throw e;
		}
	}


	private ReferenceDataRequest createBloombergDataRequest(InvestmentSecurity security, MarketDataField field, MarketDataLiveCommand marketDataLiveCommand) {
		String externalFieldName = getBloombergFieldOverride() != null ? getBloombergFieldOverride() : field.getExternalFieldName();
		AssertUtils.assertFalse(StringUtils.isEmpty(externalFieldName), "Could not find the Bloomberg field to retrieve from the schedule calculator bean or the Market Data Field. Both objects return no value.");
		String[] requestFieldNames = externalFieldName.split(",");
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(requestFieldNames), "Market Data Field {" + field.getName() + "} is missing an External Field Name");

		ReferenceDataRequest request = new ReferenceDataRequest(requestFieldNames);
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		request.addSecurity(command.getBloombergIdentifier());
		request.setRequestTimeoutOverrideIfLarger(ObjectUtils.coalesce(marketDataLiveCommand.getProviderTimeoutOverride(), field.getRequestTimeoutMillis()));
		request.addOverrides(field.getExternalFieldOverridesForLatest());
		final MarketDataProviderOverrides providerOverride;
		if (BooleanUtils.isTrue(getUseTerminal())) {
			providerOverride = MarketDataProviderOverrides.TERMINAL;
		}
		else if (marketDataLiveCommand.getProviderOverride() != null) {
			providerOverride = marketDataLiveCommand.getProviderOverride();
		}
		else {
			providerOverride = MarketDataProviderOverrides.BPIPE;
		}
		request.getProperties().put(providerOverride.getOverrideKey(), providerOverride.getOverrideValue());
		return request;
	}


	/**
	 * Returns the first value with one of the provided keys. If no value can be found, the exception supplier is used to throw an exception.
	 */
	private Object getResponseValueForKeys(Map<String, Object> responseData, String[] keys) {
		return ArrayUtils.getStream(keys)
				.filter(name -> !StringUtils.isEmpty(name))
				.map(responseData::get)
				.filter(Objects::nonNull)
				.findAny()
				.orElse(null);
	}


	private Map<String, String[]> getDataTableColumnToExtractionFieldNamesMap() {
		return this.columnNameToExtractionFieldNamesMap.updateAndGet(currentValue -> {
			if (currentValue != null) {
				return currentValue;
			}

			String[] columnNames = getTableColumnNames();
			String[] bloombergProperties = getBloombergPropertyNames();
			Map<String, String[]> columnToExtractionFieldNamesMap = new LinkedHashMap<>();
			for (int i = 0; i < columnNames.length; i++) {
				columnToExtractionFieldNamesMap.put(columnNames[i], getNullSafePropertyNamesForName(bloombergProperties[i]));
			}
			return columnToExtractionFieldNamesMap;
		});
	}


	private String[] getNullSafePropertyNamesForName(String propertyName) {
		return StringUtils.isEmpty(propertyName) ? new String[0] : propertyName.split(",");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	private static final class MarketDataLiveTable {

		private final DataTable dataTable;
		private final Date measureDate;


		private MarketDataLiveTable(DataTable dataTable, Date measureDate) {
			this.dataTable = dataTable;
			this.measureDate = measureDate;
		}


		public Date getMeasureDate() {
			return this.measureDate;
		}


		public DataTable getDataTable() {
			return this.dataTable;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public CacheHandler<Integer, MarketDataLiveTable> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Integer, MarketDataLiveTable> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public String[] getTableColumnNames() {
		return this.tableColumnNames;
	}


	public void setTableColumnNames(String[] tableColumnNames) {
		this.tableColumnNames = tableColumnNames;
	}


	public String[] getBloombergPropertyNames() {
		return this.bloombergPropertyNames;
	}


	public void setBloombergPropertyNames(String[] bloombergPropertyNames) {
		this.bloombergPropertyNames = bloombergPropertyNames;
	}


	public Integer getDefaultAgeInSeconds() {
		return this.defaultAgeInSeconds;
	}


	public void setDefaultAgeInSeconds(Integer defaultAgeInSeconds) {
		this.defaultAgeInSeconds = defaultAgeInSeconds;
	}


	public Boolean getUseTerminal() {
		return this.useTerminal;
	}


	public void setUseTerminal(Boolean useTerminal) {
		this.useTerminal = useTerminal;
	}


	public String getBloombergFieldOverride() {
		return this.bloombergFieldOverride;
	}


	public void setBloombergFieldOverride(String bloombergFieldOverride) {
		this.bloombergFieldOverride = bloombergFieldOverride;
	}


	public String getSortColumn() {
		return this.sortColumn;
	}


	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}


	public AtomicReference<Map<String, String[]>> getColumnNameToExtractionFieldNamesMap() {
		return this.columnNameToExtractionFieldNamesMap;
	}
}
