package com.clifton.bloomberg.client;

import java.util.Date;


/**
 * A command for housing the parameters available to make simple requests to a given Bloomberg provider
 *
 * @author theodorez
 */
public class BloombergShortResponseCommand {

	private String symbol;
	private String field;
	private String yellowKey;
	private String investmentTypeName;
	private String pricingSource;
	private Short exchangeCodeId;
	private Date startDate;
	private Date endDate;
	private String overrides;
	private String providerOverride;
	private Integer providerOverrideTimeout;
	private Date scheduleDate;
	private String scheduleTime;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getField() {
		return this.field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public String getYellowKey() {
		return this.yellowKey;
	}


	public void setYellowKey(String yellowKey) {
		this.yellowKey = yellowKey;
	}


	public String getInvestmentTypeName() {
		return this.investmentTypeName;
	}


	public void setInvestmentTypeName(String investmentTypeName) {
		this.investmentTypeName = investmentTypeName;
	}


	public String getPricingSource() {
		return this.pricingSource;
	}


	public void setPricingSource(String pricingSource) {
		this.pricingSource = pricingSource;
	}


	public Short getExchangeCodeId() {
		return this.exchangeCodeId;
	}


	public void setExchangeCodeId(Short exchangeCodeId) {
		this.exchangeCodeId = exchangeCodeId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getOverrides() {
		return this.overrides;
	}


	public void setOverrides(String overrides) {
		this.overrides = overrides;
	}


	public String getProviderOverride() {
		return this.providerOverride;
	}


	public void setProviderOverride(String providerOverride) {
		this.providerOverride = providerOverride;
	}


	public Integer getProviderOverrideTimeout() {
		return this.providerOverrideTimeout;
	}


	public void setProviderOverrideTimeout(Integer providerOverrideTimeout) {
		this.providerOverrideTimeout = providerOverrideTimeout;
	}


	public Date getScheduleDate() {
		return this.scheduleDate;
	}


	public void setScheduleDate(Date scheduleDate) {
		this.scheduleDate = scheduleDate;
	}


	public String getScheduleTime() {
		return this.scheduleTime;
	}


	public void setScheduleTime(String scheduleTime) {
		this.scheduleTime = scheduleTime;
	}
}
