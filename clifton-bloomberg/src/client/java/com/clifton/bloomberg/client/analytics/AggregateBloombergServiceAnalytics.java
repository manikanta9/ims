package com.clifton.bloomberg.client.analytics;

import com.clifton.core.messaging.MessagingMessage;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author MitchellF
 */
public class AggregateBloombergServiceAnalytics extends BloombergServiceAnalytics {

	/**
	 * This is a map containing statistics for each individual service endpoint, identified by machine names
	 */
	private final Map<String, BloombergServiceAnalytics> analyticsPerService = new ConcurrentHashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AggregateBloombergServiceAnalytics(String name, String serviceType) {
		super(name, serviceType);
	}


	// calculated getter
	public Collection<BloombergServiceAnalytics> getAnalyticsPerServiceSet() {
		return new HashSet<>(getAnalyticsPerService().values());
	}


	@Override
	public void updateAnalytics(Long requestTime, MessagingMessage request, MessagingMessage response, String messageSource) {

		String serviceName = response.getMachineName();

		this.analyticsPerService.computeIfAbsent(serviceName, key -> new BloombergServiceAnalytics(response.getMachineName(), messageSource))
				.updateAnalytics(requestTime, request, response, messageSource);

		super.updateAnalytics(requestTime, request, response, messageSource);
	}


	@Override
	public void clear() {
		super.clear();
		getAnalyticsPerService().clear();
	}


	public BloombergServiceAnalytics getAnalyticsForService(String serviceName) {
		return getAnalyticsPerService().get(serviceName);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Map<String, BloombergServiceAnalytics> getAnalyticsPerService() {
		return this.analyticsPerService;
	}
}
