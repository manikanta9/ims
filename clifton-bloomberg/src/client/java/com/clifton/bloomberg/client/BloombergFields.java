package com.clifton.bloomberg.client;


public interface BloombergFields {

	/**
	 * Asking pricing for a security.
	 */
	public static final String ASK = "ASK";

	/**
	 * Usually latest price for a security. For index ratios, interest rates, etc. it's the latest value.
	 */
	public static final String PX_LAST = "PX_LAST";

	////////////////////////////////////////////////////////////////////////////

	/**
	 * The day of month when the payment is made (assuming it's not a weekend or holiday) which could push the payment further: modified following.
	 * Use this day to calculate accrual period end date.  If 25 and payment date is on 27th with 0 day payment delay then end of period is on 24.
	 */
	public static final String NOMINAL_PAYMENT_DAY = "NOMINAL_PAYMENT_DAY";

	/**
	 * The number of days that nominal payment date is delayed after accrual end. If the value is 0, no delay, then payment is made the very next
	 * day after accrual period end date.
	 */
	public static final String MTG_PAY_DELAY = "MTG_PAY_DELAY";

	/**
	 * For auction bonds next coupon payment date may not be known so this field will indicate payment date for current coupon.
	 * Example: 709163EF2
	 */
	public static final String LAST_REFIX_DT = "LAST_REFIX_DT";
}
