package com.clifton.bloomberg.client.securityevent;


import com.clifton.bloomberg.client.BloombergFields;
import com.clifton.bloomberg.client.BloombergRequestCommand;
import com.clifton.bloomberg.messages.BloombergMarketSectors;
import com.clifton.bloomberg.messages.BloombergSequenceElement;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BloombergFactorChangeRetriever</code> class retrieves latest and historic Factor Change
 * events from Bloomberg.
 * <p>
 * NOTE: initial refactoring kept most of Aaron's logic but placed it into a better design.
 *
 * @author vgomelsky
 */
public class BloombergFactorChangeRetriever extends BaseBondBloombergEventRetriever {

	/**
	 * Bloomberg may change precision of current/previous factor values from time to time.  Ignore within the following tolerance.
	 */
	private BigDecimal roundingTolerance = new BigDecimal("0.00000001");


	@Override
	public boolean prepareInvestmentSecurityEvent(InvestmentSecurityEvent event) {
		if (event == null) {
			return false; // skip
		}

		// skip factor changes form 1 to 1 or from 0 to 0 (no change now and no need to store)
		if (MathUtils.compare(event.getBeforeEventValue(), event.getAfterEventValue()) == 0) {
			if (MathUtils.compare(event.getBeforeEventValue(), BigDecimal.ONE) == 0 || MathUtils.compare(event.getBeforeEventValue(), BigDecimal.ZERO) == 0) {
				return false; // skip
			}
		}

		// adjust for End Of Day convention
		event.setAccrualEndDate(DateUtils.addDays(event.getAccrualEndDate(), 1));
		if (DateUtils.compare(event.getAccrualEndDate(), event.getExDate(), false) == 0) {
			event.setExDate(DateUtils.addDays(event.getExDate(), 1));
		}

		// check if this is a duplicate event
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(event.getSecurity().getId());
		searchForm.setTypeId(event.getType().getId());
		searchForm.setEventDate(event.getEventDate());
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		if (!CollectionUtils.isEmpty(eventList)) {
			// Bloomberg may change precision between invocations: still a duplicate
			InvestmentSecurityEvent existingEvent = CollectionUtils.getOnlyElement(eventList);
			if (MathUtils.isLessThan(MathUtils.subtract(existingEvent.getAfterEventValue(), event.getAfterEventValue()).abs(), getRoundingTolerance())) {
				return false; // skip
			}
			// if the difference is big enough: then need to research this manually: was previous factor incorrect? delete it and keep this one?
		}

		// lookup previous factor in the system and update previous value if necessary to adjust for Bloomberg precision inconsistencies
		searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(event.getSecurity().getId());
		searchForm.setTypeId(event.getType().getId());
		searchForm.addSearchRestriction(new SearchRestriction("eventDate", ComparisonConditions.LESS_THAN, event.getEventDate()));
		searchForm.setOrderBy("eventDate:desc#id:desc");
		searchForm.setLimit(1);
		eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		InvestmentSecurityEvent previousEvent = CollectionUtils.getFirstElement(eventList);
		if (previousEvent != null) {
			if (MathUtils.isLessThanOrEqual(MathUtils.subtract(previousEvent.getAfterEventValue(), event.getBeforeEventValue()).abs(), getRoundingTolerance())) {
				event.setBeforeEventValue(previousEvent.getAfterEventValue());
			}
			//  update accrual start date if necessary to match previous event end
			int diff = DateUtils.getDaysDifference(event.getAccrualStartDate(), previousEvent.getAccrualEndDate());
			if (diff >= -10 && diff <= 10) { // 10 day tolerance: some bonds have non standard accrual periods or holidays
				event.setAccrualStartDate(previousEvent.getAccrualEndDate());
				// adjust the end date if periods are monthly from first to last day of month
				if (DateUtils.isLastDayOfMonth(previousEvent.getAccrualEndDate())) {
					if (DateUtils.getMonthOfYear(previousEvent.getAccrualStartDate()) == DateUtils.getMonthOfYear(previousEvent.getAccrualEndDate())) {
						event.setAccrualEndDate(DateUtils.getLastDayOfMonth(event.getAccrualStartDate()));
						event.setExDate(DateUtils.addDays(event.getAccrualEndDate(), 1));
					}
				}
			}
		}

		// if before and after within the tolerance: no factor change (make them the same)
		if (MathUtils.isLessThan(MathUtils.subtract(event.getAfterEventValue(), event.getBeforeEventValue()).abs(), getRoundingTolerance())) {
			if (previousEvent != null) {
				event.setAfterEventValue(event.getBeforeEventValue()); // keep previous and set after to it as well
			}
			else {
				event.setBeforeEventValue(event.getAfterEventValue()); // first time: assume after is the more accurate one
			}
		}

		return true; // ready to be saved
	}


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEvent(InvestmentSecurity security) {
		// request factor change specific fields for the specified security
		ReferenceDataRequest dataRequest = new ReferenceDataRequest("MTG_FACTOR", "MTG_PREV_FACTOR", "MTG_FACTOR_PAY_DT", BloombergFields.MTG_PAY_DELAY, "CPN_FREQ", "MTG_FACTOR_DT",
				"MTG_PRINC_LOSSES", "MTG_ORIG_AMT"); // BloombergFields.NOMINAL_PAYMENT_DAY
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		dataRequest.addSecurity(command.getBloombergIdentifier());

		ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);
		if (response.getBloomError() != null && "BAD_SEC".equals(response.getBloomError().getCategory())) {
			throw new BloombergRequestException(response.getBloomError());
		}

		// populate event from the fields returned
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.FACTOR_CHANGE);
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		event.setType(eventType);
		event.setStatus(event.getType().getForceEventStatus());
		event.setSecurity(security);

		DayCountConventions dayCountConvention = DayCountConventions.getDayCountConventionByKey((String) getSystemColumnValueHandler().getSystemColumnValueForEntity(security,
				InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION, false));

		for (Map<String, Object> fields : response.getValues().values()) {
			Double factor = (Double) fields.get("MTG_FACTOR");
			Date nextPayDate = (Date) fields.get("MTG_FACTOR_PAY_DT");

			if (nextPayDate == null) {
				//the security has no pending factor
				return null;
			}

			int paymentDelay = parsePaymentDelay((String) fields.get(BloombergFields.MTG_PAY_DELAY));
			event.setBeforeEventValue(BigDecimal.valueOf((Double) fields.get("MTG_PREV_FACTOR")).setScale(eventType.getDecimalPrecision(), BigDecimal.ROUND_HALF_UP));
			event.setAfterEventValue(BigDecimal.valueOf(factor).setScale(eventType.getDecimalPrecision(), BigDecimal.ROUND_HALF_UP));
			// sometimes Bloomberg gives us previous factor that doesn't have the same decimal precision (no factor change)
			if (MathUtils.isLessThan(event.getBeforeEventValue().subtract(event.getAfterEventValue()).abs(), new BigDecimal("0.000000001"))) {
				// assume that factor change can never be this small: we get 10 decimals but for before sometimes receive 9
				event.setBeforeEventValue(event.getAfterEventValue());
			}

			// calculate loss percentage if any: Loss Amount = Original Face * MTG_PRINC_LOSSES / MTG_ORIG_AMT
			Double loss = (Double) fields.get("MTG_PRINC_LOSSES");
			Double originalAmount = (Double) fields.get("MTG_ORIG_AMT");
			if (!MathUtils.isNullOrZero(loss) && !MathUtils.isNullOrZero(originalAmount)) {
				// Example: 126685CZ7 in January of 2012 (virtually every month)
				event.setAdditionalEventValue(MathUtils.divide(BigDecimal.valueOf(loss), BigDecimal.valueOf(originalAmount)));
				event.setAdditionalEventValue(event.getAdditionalEventValue().multiply(new BigDecimal("100"))); // convert to percent
			}

			BloombergCouponFrequencies frequency = BloombergCouponFrequencies.getFreqFromValue((Integer) fields.get("CPN_FREQ"));
			// 54 day delay: 83164J3N4
			// 24 day delay: 76110WNH5
			//  0 day delay: 04541GCG5
			event.setAccrualStartDate(frequency.getPeriodStartDate(DateUtils.addDays(nextPayDate, -(paymentDelay + 1))));
			event.setAccrualEndDate(DateUtils.addDays(nextPayDate, -(paymentDelay + 1)));
			if (dayCountConvention == DayCountConventions.THRITY_THREESIXTY && paymentDelay > 30) {
				event.setAccrualEndDate(frequency.getNextPeriodEndDate(event.getAccrualStartDate()));
			}
			event.setPaymentDate(nextPayDate);

			event.setEventDescription((String) fields.get("MTG_FACTOR_DT"));
		}

		return event;
	}


	@Override
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventHistory(InvestmentSecurity security, @SuppressWarnings("unused") boolean ignoreSeedEvents) {
		ReferenceDataRequest dataRequest = new ReferenceDataRequest("CPN_FREQ");
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		dataRequest.addSecurity(command.getBloombergIdentifier());

		if (command.getMarketSector().equals(BloombergMarketSectors.MORTGAGE.getKeyName())) {
			dataRequest.addField("MTG_HIST_FACT");
			dataRequest.addField(BloombergFields.MTG_PAY_DELAY);
			ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);
			if (response.getBloomError() != null && "BAD_SEC".equals(response.getBloomError().getCategory())) {
				throw new BloombergRequestException(response.getBloomError());
			}

			InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.FACTOR_CHANGE);
			for (Map<String, Object> fields : response.getValues().values()) {
				@SuppressWarnings("unchecked")
				List<InvestmentSecurityEvent> factorHistory = pivotBloomHistoryList((List<BloombergSequenceElement>) fields.get("MTG_HIST_FACT"), "Payment Date", "Factor", eventType, security, true);

				Integer freq = (Integer) fields.get("CPN_FREQ");
				if (freq != null) {
					BloombergCouponFrequencies frequency = BloombergCouponFrequencies.getFreqFromValue(freq);

					BigDecimal previousFactor = BigDecimal.ONE;
					int paymentDelay = parsePaymentDelay((String) fields.get(BloombergFields.MTG_PAY_DELAY));
					List<InvestmentSecurityEvent> events = new ArrayList<>();
					for (InvestmentSecurityEvent factorEvent : CollectionUtils.getIterable(factorHistory)) {
						factorEvent.setBeforeEventValue(previousFactor.setScale(factorEvent.getType().getDecimalPrecision(), BigDecimal.ROUND_HALF_UP));
						previousFactor = factorEvent.getAfterEventValue();
						factorEvent.setAccrualEndDate(DateUtils.addDays(factorEvent.getPaymentDate(), (paymentDelay + 1) * -1));
						factorEvent.setAccrualStartDate(frequency.getPeriodStartDate(paymentDelay == 0 ? factorEvent.getPaymentDate() : factorEvent.getAccrualEndDate()));
					}

					if (factorHistory != null) {
						events.addAll(factorHistory);
					}
					return events;
				}
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentSecurityEvent> pivotBloomHistoryList(List<BloombergSequenceElement> elements, String keyName, String valueName, InvestmentSecurityEventType eventType,
	                                                            InvestmentSecurity security, boolean usePaymentDate) {
		if (elements == null) {
			return null;
		}
		List<InvestmentSecurityEvent> events = new ArrayList<>(elements.size());
		for (BloombergSequenceElement element : elements) {
			Date bloomDate = (Date) element.getValues().get(keyName);
			Date paymentDate = usePaymentDate ? bloomDate : null;
			Date startAccrualDate = usePaymentDate ? null : bloomDate;
			InvestmentSecurityEvent couponEvent = new InvestmentSecurityEvent();
			couponEvent.setType(eventType);
			couponEvent.setStatus(couponEvent.getType().getForceEventStatus());
			couponEvent.setBeforeAndAfterEventValue(BigDecimal.valueOf((Double) element.getValues().get(valueName)).setScale(eventType.getDecimalPrecision(), BigDecimal.ROUND_HALF_UP));
			couponEvent.setAccrualStartDate(startAccrualDate);
			couponEvent.setPaymentDate(paymentDate);
			couponEvent.setSecurity(security);
			events.add(couponEvent);
		}
		Collections.reverse(events); //have oldest event first
		return events;
	}


	public BigDecimal getRoundingTolerance() {
		return this.roundingTolerance;
	}


	public void setRoundingTolerance(BigDecimal roundingTolerance) {
		this.roundingTolerance = roundingTolerance;
	}
}
