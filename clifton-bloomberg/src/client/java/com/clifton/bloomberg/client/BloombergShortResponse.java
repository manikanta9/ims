package com.clifton.bloomberg.client;


import com.clifton.bloomberg.messages.BloombergSequenceElement;
import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BloombergShortResponse</code> class is a value objects that contains most important fields of a response from Bloomberg.
 */
public class BloombergShortResponse {

	private Date fieldDate;
	private String fieldName;
	private String securityName;
	private Object fieldValue;
	private String machineName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BloombergShortResponse(Date fieldDate, String fieldName, Object fieldValue, String securityName) {
		this(fieldDate, fieldName, fieldValue, securityName, null);
	}


	public BloombergShortResponse(Date fieldDate, String fieldName, Object fieldValue, String securityName, String machineName) {
		this.fieldDate = fieldDate;
		this.fieldName = fieldName;
		this.fieldValue = fieldValue;
		// convert List of BloombergSequenceElement into readable form
		if (fieldValue instanceof List) {
			List<?> listValue = (List<?>) fieldValue;
			Object element = CollectionUtils.getFirstElement(listValue);
			if (element instanceof BloombergSequenceElement) {
				List<Map<String, Object>> result = new ArrayList<>(listValue.size());
				for (Object listElement : listValue) {
					BloombergSequenceElement sequenceElement = (BloombergSequenceElement) listElement;
					result.add(sequenceElement.getValues());
				}
				this.fieldValue = result;
			}
		}
		this.securityName = securityName;
		this.machineName = machineName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getFieldDate() {
		return this.fieldDate;
	}


	public void setFieldDate(Date fieldDate) {
		this.fieldDate = fieldDate;
	}


	public String getFieldName() {
		return this.fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public Object getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}


	public String getSecurityName() {
		return this.securityName;
	}


	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}


	public String getMachineName() {
		return this.machineName;
	}


	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
}
