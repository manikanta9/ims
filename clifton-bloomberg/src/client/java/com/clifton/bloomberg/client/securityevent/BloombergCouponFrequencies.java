package com.clifton.bloomberg.client.securityevent;


import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>BloombergCouponFrequency</code> enum defines bond coupon payment frequencies.
 *
 * @author akorver
 */
public enum BloombergCouponFrequencies {

	ANNUAL(1) {
		@Override
		public Date getNextPeriodEndDate(Date periodStartDate) {
			if (DateUtils.isLastDayOfMonth(periodStartDate)) {
				// always keep last day of year
				return DateUtils.addDays(DateUtils.addYears(DateUtils.addDays(periodStartDate, 1), 1), -2);
			}
			return DateUtils.addDays(DateUtils.addYears(periodStartDate, 1), -1);
		}


		@Override
		public Date getPeriodStartDate(Date periodEndDate) {
			if (DateUtils.isLastDayOfMonth(periodEndDate)) {
				// always keep last day of year
				return DateUtils.addYears(DateUtils.addDays(periodEndDate, 1), -1);
			}
			return DateUtils.addDays(DateUtils.addYears(periodEndDate, -1), 1);
		}


		@Override
		public Date getNextPayDate(Date payDate) {
			if (DateUtils.isLastDayOfMonth(payDate)) {
				// always keep last day of year
				return DateUtils.addDays(DateUtils.addYears(DateUtils.addDays(payDate, 1), 1), -1);
			}
			return DateUtils.addYears(payDate, 1);
		}
	},

	SEMIANNUAL(2) {
		@Override
		public Date getNextPeriodEndDate(Date periodStartDate) {
			if (DateUtils.isLastDayOfMonth(periodStartDate)) {
				// always keep last day of month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(periodStartDate, 1), 6), -2);
			}
			return DateUtils.addDays(DateUtils.addMonths(periodStartDate, 6), -1);
		}


		@Override
		public Date getPeriodStartDate(Date periodEndDate) {
			if (DateUtils.isLastDayOfMonth(periodEndDate)) {
				// always use first day of month
				return DateUtils.addMonths(DateUtils.addDays(periodEndDate, 1), -6);
			}
			if (DateUtils.isLastDayOfMonth(DateUtils.addDays(periodEndDate, 1))) {
				// payment is on last day of month and period end is on second to last: start is on last day of the month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(periodEndDate, 2), -6), -1);
			}
			return DateUtils.addDays(DateUtils.addMonths(periodEndDate, -6), 1);
		}


		@Override
		public Date getNextPayDate(Date payDate) {
			if (DateUtils.isLastDayOfMonth(payDate)) {
				// always keep last day of month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(payDate, 1), 6), -1);
			}
			return DateUtils.addMonths(payDate, 6);
		}
	},

	QUARTERLY(4) {
		@Override
		public Date getNextPeriodEndDate(Date periodStartDate) {
			if (DateUtils.isLastDayOfMonth(periodStartDate)) {
				// always keep last day of month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(periodStartDate, 1), 3), -2);
			}
			return DateUtils.addDays(DateUtils.addMonths(periodStartDate, 3), -1);
		}


		@Override
		public Date getPeriodStartDate(Date periodEndDate) {
			if (DateUtils.isLastDayOfMonth(periodEndDate)) {
				// always use first day of month
				return DateUtils.addMonths(DateUtils.addDays(periodEndDate, 1), -3);
			}
			if (DateUtils.isLastDayOfMonth(DateUtils.addDays(periodEndDate, 1))) {
				// payment is on last day of month and period end is on second to last: start is on last day of the month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(periodEndDate, 2), -3), -1);
			}
			return DateUtils.addDays(DateUtils.addMonths(periodEndDate, -3), 1);
		}


		@Override
		public Date getNextPayDate(Date payDate) {
			if (DateUtils.isLastDayOfMonth(payDate)) {
				// always keep last day of month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(payDate, 1), 3), -1);
			}
			return DateUtils.addMonths(payDate, 3);
		}
	},

	MONTHLY(12) {
		@Override
		public Date getNextPeriodEndDate(Date periodStartDate) {
			if (DateUtils.isLastDayOfMonth(periodStartDate)) {
				// always keep last day of month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(periodStartDate, 1), 1), -2);
			}
			return DateUtils.addDays(DateUtils.addMonths(periodStartDate, 1), -1);
		}


		@Override
		public Date getPeriodStartDate(Date periodEndDate) {
			if (DateUtils.isLastDayOfMonth(periodEndDate)) {
				// always use first day of month
				return DateUtils.addMonths(DateUtils.addDays(periodEndDate, 1), -1);
			}
			if (DateUtils.isLastDayOfMonth(DateUtils.addDays(periodEndDate, 1))) {
				// payment is on last day of month and period end is on second to last: start is on last day of the month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(periodEndDate, 2), -1), -1);
			}
			if (DateUtils.getDayOfMonth(periodEndDate) == 1) {
				// use first day of month
				return DateUtils.addMonths(periodEndDate, -1);
			}
			return DateUtils.addDays(DateUtils.addMonths(periodEndDate, -1), 1);
		}


		@Override
		public Date getNextPayDate(Date payDate) {
			if (DateUtils.isLastDayOfMonth(payDate)) {
				// always keep last day of month
				return DateUtils.addDays(DateUtils.addMonths(DateUtils.addDays(payDate, 1), 1), -1);
			}
			return DateUtils.addMonths(payDate, 1);
		}
	},

	WEEKLY(52) {
		@Override
		public Date getNextPeriodEndDate(Date periodStartDate) {
			return DateUtils.addDays(periodStartDate, 7);
		}


		@Override
		public Date getPeriodStartDate(Date periodEndDate) {
			return DateUtils.addDays(periodEndDate, -7);
		}


		@Override
		public Date getNextPayDate(Date payDate) {
			return DateUtils.addDays(payDate, 7);
		}
	};

	private final int frequency;


	BloombergCouponFrequencies(int freq) {
		this.frequency = freq;
	}


	public int getFrequency() {
		return this.frequency;
	}


	public static BloombergCouponFrequencies getFreqFromValue(int freq) {
		for (BloombergCouponFrequencies freqEnum : BloombergCouponFrequencies.values()) {
			if (freqEnum.getFrequency() == freq) {
				return freqEnum;
			}
		}
		throw new IllegalArgumentException("Cannot find BloombergCouponFrequencies enum that corresponds to value = " + freq);
	}


	/**
	 * Given a periodStartDate, return the next end date
	 */
	public abstract Date getNextPeriodEndDate(Date periodStartDate);


	/**
	 * Given an end date of a period, return the start date
	 */
	public abstract Date getPeriodStartDate(Date periodEndDate);


	/**
	 * Given a period pay date, calc the next pay date
	 */
	public abstract Date getNextPayDate(Date payDate);
}
