package com.clifton.bloomberg.client.securitylocator;


import com.clifton.bloomberg.client.BloombergMarketDataProvider;
import com.clifton.bloomberg.messages.BloombergMarketSectors;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.securitylocator.InvestmentSecurityLocator;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;

import java.util.List;


/**
 * The <code>BloombergInvestmentSecurityLocator</code> class implements Bloomberg specific logic that can
 * lookup a security based on symbol, primary exchange and yellow key combinations.
 * <p>
 * For example, Bloomberg locator can map the following 2 symbols to AAPL: "AAPL UW Equity", "AAPL Equity"
 *
 * @author vgomelsky
 */
public class BloombergInvestmentSecurityLocator implements InvestmentSecurityLocator {

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getPriorityOrder() {
		return 0;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity(String symbol) {
		int index = symbol.lastIndexOf(' ');
		if (index != -1) {
			String cleanSymbol = symbol.substring(0, index);
			String symbolSuffix = symbol.substring(index + 1);
			// check if the suffix is Yellow Key
			try {
				String marketSector = BloombergMarketSectors.getBloombergMarketSectorByKey(symbolSuffix).getKeyName();
				MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSourceByName(BloombergMarketDataProvider.DATASOURCE_NAME);

				// look for security by symbol without yellow key
				List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(cleanSymbol);
				if (CollectionUtils.isEmpty(securityList)) {
					// check if primary exchange is specified
					index = cleanSymbol.lastIndexOf(' ');
					if (index != -1) {
						// look for security by clean symbol and exchange code
						String exchangeCode = cleanSymbol.substring(index + 1);
						cleanSymbol = cleanSymbol.substring(0, index);
						securityList = getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(cleanSymbol);
						securityList = BeanUtils.filter(securityList, security -> security.belongsToPrimaryOrCompositeExchange(exchangeCode));
					}
				}

				for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
					MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), dataSource);
					if (mapping != null && mapping.getMarketSector() != null && mapping.getMarketSector().getName().equalsIgnoreCase(marketSector)) {
						return security;
					}
				}
			}
			catch (IllegalArgumentException e) {
				// not a yellow key: keep null
			}
		}

		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
