package com.clifton.bloomberg.client.securityevent;


import com.clifton.bloomberg.client.BloombergRequestCommand;
import com.clifton.bloomberg.messages.BloombergSequenceElement;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.event.sequence.InvestmentEventSequenceRetrieverUtilHandler;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class BloombergInterestRateSwapRetriever extends BaseBloombergEventRetriever {

	public final static String RECEIVE_ACCRUAL_SCHEDULE = "RECEIVE_ACCRUAL_SCHEDULE";

	public final static String FIXED_LEG_RESET_RATE = "sw_cpn_spread";

	public final static String PAY_ACCRUAL_SCHEDULE = "PAY_ACCRUAL_SCHEDULE";

	public final static String PAY_REC_FIXED = "SW_PAY_REC_FIXED";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;
	private CalendarScheduleService calendarScheduleService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private InvestmentEventSequenceRetrieverUtilHandler investmentEventSequenceRetrieverUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String eventFixingScheduleBusinessDayConvention = "Business Day Convention";
	private String eventFixingCalendarCustomFieldName;
	private String eventFixingCycleCustomFieldName;

	private String securityEventTypeName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean prepareInvestmentSecurityEvent(InvestmentSecurityEvent event) {
		if (event == null) {
			return false; // skip
		}

		// check if this is a duplicate event
		InvestmentSecurityEventSearchForm searchForm = getInvestmentEventSequenceRetrieverUtilHandler().getInvestmentSecurityEventDuplicateSearchForm(event);
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		return CollectionUtils.isEmpty(eventList);
	}


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEvent(@SuppressWarnings("unused") InvestmentSecurity security) {
		throw new RuntimeException("Historical dividend retrieval is not supported");
	}


	@Override
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventHistory(InvestmentSecurity security, @SuppressWarnings("unused") boolean ignoreSeedEvents) {
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(getSecurityEventTypeName());
		ValidationUtils.assertNotNull(eventType, "Cannot find security event type for [" + getSecurityEventTypeName() + "].");
		ReferenceDataRequest dataRequest;

		String payRec = getPayRecFixed(security);


		String fixedLegAccrualScheduleField = "RECEIVE".equals(payRec) ? RECEIVE_ACCRUAL_SCHEDULE : PAY_ACCRUAL_SCHEDULE;
		String floatingLegAccrualScheduleField = "RECEIVE".equals(payRec) ? PAY_ACCRUAL_SCHEDULE : RECEIVE_ACCRUAL_SCHEDULE;
		switch (eventType.getName()) {
			case "Fixed Leg Payment":
				dataRequest = new ReferenceDataRequest(fixedLegAccrualScheduleField, FIXED_LEG_RESET_RATE);
				break;
			case "Floating Leg Payment":
				dataRequest = new ReferenceDataRequest(floatingLegAccrualScheduleField);
				break;
			default:
				throw new ValidationException("BloombergInterestRateSwapRetriever does not support event type [" + eventType.getName() + "].");
		}

		BloombergRequestCommand command = getBloombergRequestCommand(security);
		dataRequest.addSecurity(command.getBloombergIdentifier());

		ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);
		if (response.getBloomError() != null && "BAD_SEC".equals(response.getBloomError().getCategory())) {
			throw new BloombergRequestException(response.getBloomError());
		}

		for (Map<String, Object> fields : response.getValues().values()) {
			switch (eventType.getName()) {
				case "Fixed Leg Payment":
					return loadFixedLeg(security, eventType, fields, fixedLegAccrualScheduleField);
				case "Floating Leg Payment":
					return loadFloatingLeg(security, eventType, fields, floatingLegAccrualScheduleField);
			}
		}
		return null;
	}


	@SuppressWarnings({"unchecked"})
	private List<InvestmentSecurityEvent> loadFloatingLeg(InvestmentSecurity security, InvestmentSecurityEventType eventType, Map<String, Object> fields, String accrualScheduleField) {
		List<InvestmentSecurityEvent> result = new ArrayList<>();
		List<BloombergSequenceElement> sequenceList = (List<BloombergSequenceElement>) fields.get(accrualScheduleField);

		for (BloombergSequenceElement sequence : CollectionUtils.getIterable(sequenceList)) {
			InvestmentSecurityEvent event = createEvent(security, eventType, sequence, BigDecimal.ZERO);
			event.setAdditionalDate(getAdditionalDate(event));
			result.add(event);
		}
		return result;
	}


	@SuppressWarnings("unchecked")
	private List<InvestmentSecurityEvent> loadFixedLeg(InvestmentSecurity security, InvestmentSecurityEventType eventType, Map<String, Object> fields, String accrualScheduleField) {
		List<InvestmentSecurityEvent> result = new ArrayList<>();
		BigDecimal resetRate = BigDecimal.valueOf((double) fields.get(FIXED_LEG_RESET_RATE)).setScale(8, RoundingMode.HALF_UP);
		List<BloombergSequenceElement> sequenceList = (List<BloombergSequenceElement>) fields.get(accrualScheduleField);

		for (BloombergSequenceElement sequence : CollectionUtils.getIterable(sequenceList)) {
			result.add(createEvent(security, eventType, sequence, resetRate));
		}
		return result;
	}


	public InvestmentSecurityEvent createEvent(InvestmentSecurity security, InvestmentSecurityEventType eventType, BloombergSequenceElement sequence, BigDecimal rate) {
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		event.setSecurity(security);
		event.setType(eventType);
		event.setStatus(event.getType().getForceEventStatus());
		// Bloomberg uses EOD (End Of Day) accrual convention: start/end on EOD (start date == end date of previous period)
		// To convert to our convention: add 1 day to start to skip accrual on start date (our dates never overlap)
		event.setEventDate((Date) sequence.getValues().get("Accrual End"));
		event.setExDate(event.getEventDate());
		event.setAccrualEndDate(event.getEventDate());
		event.setAccrualStartDate((Date) sequence.getValues().get("Accrual Start"));
		event.setBeforeAndAfterEventValue(rate);
		return event;
	}


	private String getPayRecFixed(InvestmentSecurity security) {
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		ReferenceDataRequest dataRequest = new ReferenceDataRequest(PAY_REC_FIXED);
		dataRequest.addSecurity(command.getBloombergIdentifier());

		ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);
		if (response.getBloomError() != null && "BAD_SEC".equals(response.getBloomError().getCategory())) {
			throw new BloombergRequestException(response.getBloomError());
		}

		if (!CollectionUtils.isEmpty(response.getValues())) {
			Map<String, Object> fields = response.getValues().get(command.getBloombergIdentifier());
			return fields.get(PAY_REC_FIXED).toString();
		}

		return null;
	}


	/**
	 * Calculate the additional date for the Floating leg event.
	 */
	private Date getAdditionalDate(InvestmentSecurityEvent event) {
		if (!StringUtils.isEmpty(getEventFixingCycleCustomFieldName()) && !StringUtils.isEmpty(getEventFixingCalendarCustomFieldName())) {
			Calendar fixingCalendar = getFixingCalendar(event.getSecurity());
			Integer fixingCycle = getFixingCycle(event.getSecurity());
			BusinessDayConventions businessDayConventions = getFixingBusinessDayConvention(event.getSecurity());

			if (MathUtils.isNullOrZero(fixingCycle)) {
				return event.getAccrualStartDate();
			}


			return getInvestmentEventSequenceRetrieverUtilHandler().getFixingDate(fixingCalendar, businessDayConventions, fixingCycle, event.getAccrualStartDate());
		}
		return null;
	}


	private Integer getFixingCycle(InvestmentSecurity security) {
		return getField(security, getEventFixingCycleCustomFieldName());
	}


	private Calendar getFixingCalendar(InvestmentSecurity security) {
		return getCalendarByName(security, getEventFixingCalendarCustomFieldName());
	}


	private Calendar getCalendarByName(InvestmentSecurity security, String name) {
		if (!StringUtils.isEmpty(name)) {
			Integer calendarId = getField(security, name);
			ValidationUtils.assertNotNull(calendarId, name + " is required to build an event sequence.");
			return getCalendarSetupService().getCalendar(calendarId.shortValue());
		}
		return null;
	}


	private BusinessDayConventions getFixingBusinessDayConvention(InvestmentSecurity security) {
		return BusinessDayConventions.valueOf(getField(security, getEventFixingScheduleBusinessDayConvention()));
	}


	@SuppressWarnings("unchecked")
	private <T> T getField(InvestmentSecurity security, String name) {
		if (!StringUtils.isEmpty(name)) {
			Object value = getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, name, false);
			if (value != null) {
				return (T) value;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSecurityEventTypeName() {
		return this.securityEventTypeName;
	}


	public void setSecurityEventTypeName(String securityEventTypeName) {
		this.securityEventTypeName = securityEventTypeName;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public String getEventFixingCalendarCustomFieldName() {
		return this.eventFixingCalendarCustomFieldName;
	}


	public void setEventFixingCalendarCustomFieldName(String eventFixingCalendarCustomFieldName) {
		this.eventFixingCalendarCustomFieldName = eventFixingCalendarCustomFieldName;
	}


	public String getEventFixingCycleCustomFieldName() {
		return this.eventFixingCycleCustomFieldName;
	}


	public void setEventFixingCycleCustomFieldName(String eventFixingCycleCustomFieldName) {
		this.eventFixingCycleCustomFieldName = eventFixingCycleCustomFieldName;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public String getEventFixingScheduleBusinessDayConvention() {
		return this.eventFixingScheduleBusinessDayConvention;
	}


	public void setEventFixingScheduleBusinessDayConvention(String eventFixingScheduleBusinessDayConvention) {
		this.eventFixingScheduleBusinessDayConvention = eventFixingScheduleBusinessDayConvention;
	}


	public CalendarScheduleService getCalendarScheduleService() {
		return this.calendarScheduleService;
	}


	public void setCalendarScheduleService(CalendarScheduleService calendarScheduleService) {
		this.calendarScheduleService = calendarScheduleService;
	}


	public InvestmentEventSequenceRetrieverUtilHandler getInvestmentEventSequenceRetrieverUtilHandler() {
		return this.investmentEventSequenceRetrieverUtilHandler;
	}


	public void setInvestmentEventSequenceRetrieverUtilHandler(InvestmentEventSequenceRetrieverUtilHandler investmentEventSequenceRetrieverUtilHandler) {
		this.investmentEventSequenceRetrieverUtilHandler = investmentEventSequenceRetrieverUtilHandler;
	}
}
