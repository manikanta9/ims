package com.clifton.bloomberg.client.securityevent;


import com.clifton.system.schema.column.value.SystemColumnValueHandler;


/**
 * The <code>BaseBondBloombergEventRetriever</code> class has a few helper methods specific to bond
 * events: coupon payments and factor changes.  It should be extended by corresponding retriever implementations.
 *
 * @author vgomelsky
 */
public abstract class BaseBondBloombergEventRetriever extends BaseBloombergEventRetriever {

	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the number of days from accrual period end day to payment day: payment delay.
	 * Parses it from Bloomberg specific format.  Example, "14 DAYS" => "14".
	 */
	protected int parsePaymentDelay(String payDelay) {
		if (payDelay == null) {
			return 0;
		}
		return Integer.parseInt(payDelay.split(" ")[0]);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
