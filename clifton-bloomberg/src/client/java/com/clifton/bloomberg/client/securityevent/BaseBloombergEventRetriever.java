package com.clifton.bloomberg.client.securityevent;


import com.clifton.bloomberg.client.BloombergDataService;
import com.clifton.bloomberg.client.BloombergMarketDataProvider;
import com.clifton.bloomberg.client.BloombergRequestCommand;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.retriever.BaseInvestmentSecurityEventRetriever;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceSecurityHolder;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;


/**
 * The <code>BaseBloombergEventRetriever</code> class contains common methods/services for all Bloomberg
 * event retrievers and should be extended by all implementations.
 *
 * @author vgomelsky
 */
public abstract class BaseBloombergEventRetriever extends BaseInvestmentSecurityEventRetriever {

	private BloombergDataService bloombergDataService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected BloombergRequestCommand getBloombergRequestCommand(InvestmentSecurity security) {
		MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSourceByName(BloombergMarketDataProvider.DATASOURCE_NAME);
		MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), dataSource);
		ValidationUtils.assertNotNull(mapping, "There is no Instrument Mapping setup for " + security.getLabel() + " to get the market sector from.");

		String marketSector = mapping.getMarketSector().getName();
		String exchangeCode = mapping.getExchangeCodeType() != null ? mapping.getExchangeCodeType().getSecurityExchangeCode(security) : null;
		String pricingSource = mapping.getPricingSource() != null ? mapping.getPricingSource().getSymbol() : null;

		MarketDataSourceSecurityHolder securityHolder = getMarketDataSourceService().getMarketDataSourceSecuritySymbol(security.getSymbol(), BloombergMarketDataProvider.DATASOURCE_NAME, marketSector);

		return new BloombergRequestCommand(securityHolder, pricingSource, marketSector, exchangeCode, security);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public BloombergDataService getBloombergDataService() {
		return this.bloombergDataService;
	}


	public void setBloombergDataService(BloombergDataService bloombergDataService) {
		this.bloombergDataService = bloombergDataService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
