package com.clifton.bloomberg.client;


import com.clifton.bloomberg.client.analytics.BloombergServiceAnalytics;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.HistoricalDataRequest;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.request.ServiceManagementRequest;
import com.clifton.bloomberg.messages.request.SubscriptionRequest;
import com.clifton.bloomberg.messages.response.HistoricalDataResponse;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.bloomberg.messages.response.ServiceManagementResponse;
import com.clifton.bloomberg.messages.response.SubscriptionDataResponse;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;


public interface BloombergDataService {

	/**
	 * Calls the ReferenceDataRequest operation on the //blp/refdata service on Bloomberg
	 *
	 * @throws BloombergRequestException if an error is returned by Bloomberg
	 */
	@DoNotAddRequestMapping
	public ReferenceDataResponse getReferenceData(ReferenceDataRequest requestMessage);


	/**
	 * Calls the ReferenceDataRequest operation on the //blp/refdata service on Bloomberg
	 * Will not throw an exception if suppressException is true.  This is useful if you retrieve many fields at once, and don't want to error on just one field
	 */
	@ModelAttribute("data")
	@RequestMapping("bloombergReferenceData")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public ReferenceDataResponse getReferenceData(ReferenceDataRequest requestMessage, boolean suppressException);


	/**
	 * Creates a request based on the security list calls the Reference Data Service on Bloomberg
	 *
	 * @param securities <code>List</code> of security identifiers for Bloomberg
	 * @param fields     Each field will be retrieved for each security in the list.
	 * @throws BloombergRequestException if an error is returned by Bloomberg
	 */
	@DoNotAddRequestMapping
	public ReferenceDataResponse getReferenceData(List<String> securities, String... fields);


	/**
	 * Calls the HistoricalDataRequest operation on the //blp/refdata service on Bloomberg.
	 *
	 * @throws BloombergRequestException if an error is returned by Bloomberg
	 */
	@ModelAttribute("data")
	@RequestMapping("bloombergHistoricalData")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public Map<String, ReferenceDataResponse> getHistoricalData(HistoricalDataRequest requestMessage);


	/**
	 * Calls the HistoricalDataRequest operation on the //blp/refdata service on Bloomberg.
	 * <p>
	 *
	 * @param suppressException set to true to suppress exception and return it as part of response object instead
	 */
	@DoNotAddRequestMapping
	public HistoricalDataResponse getHistoricalData(HistoricalDataRequest requestMessage, boolean suppressException);


	/**
	 * If startDate is null, then makes ReferenceDataRequest, otherwise makes HistoricalDataRequest for the specified arguments.
	 *
	 * @param command has optional 'overrides' property that can request override parameters in the following format: "SETTLE_DT=20121128,SW_CURVE_DT=20121128"
	 */
	@ModelAttribute("data")
	@RequestMapping("bloombergShortResponseList")
	@SecureMethod(disableSecurity = true)
	public List<BloombergShortResponse> getBloombergShortResponseList(BloombergShortResponseCommand command);


	/**
	 * Calls the Bloomberg API with the provided {@link SubscriptionRequest} to {@link MarketDataSubscriptionOperations#SUBSCRIBE},
	 * {@link MarketDataSubscriptionOperations#RESUBSCRIBE}, or {@link MarketDataSubscriptionOperations#UNSUBSCRIBE} to
	 * one or more security data feeds.
	 */
	@DoNotAddRequestMapping
	public SubscriptionDataResponse processSubscriptionRequest(SubscriptionRequest subscriptionRequest);


	/**
	 * Accessible via the UI for clearing the service caches. Clears the caches on the service specified.
	 */
	@RequestMapping("bloombergClearCachesForProvider")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public ServiceManagementResponse clearBloombergServiceCachesForProvider(String providerName);


	/**
	 * Not accessible via the UI -
	 * Provides a method of sending management requests to the bloomberg service
	 */
	@DoNotAddRequestMapping
	public ServiceManagementResponse processServiceManagementAction(ServiceManagementRequest managementRequest);


	@SecureMethod(disableSecurity = true)
	public BloombergServiceAnalytics getBloombergServiceAnalyticsAggregate();


	/**
	 * Here the id parameter represents the service name
	 */
	@SecureMethod(disableSecurity = true)
	public BloombergServiceAnalytics getBloombergServiceAnalytics(String id);


	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void clearBloombergServiceAnalytics();
}
