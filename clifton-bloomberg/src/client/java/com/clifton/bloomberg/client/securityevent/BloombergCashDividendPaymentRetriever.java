package com.clifton.bloomberg.client.securityevent;


import com.clifton.bloomberg.client.BloombergRequestCommand;
import com.clifton.bloomberg.messages.BloombergError;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventStatus;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BloombergCashDividendPaymentRetriever</code> class retrieves latest and historic Cash Dividend Payment
 * events from Bloomberg.
 * <p>
 * NOTE: initial refactoring kept most of Aaron's logic but placed it into a better design.
 *
 * @author vgomelsky
 */
public class BloombergCashDividendPaymentRetriever extends BaseBloombergEventRetriever {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentInstrumentService investmentInstrumentService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityEvent getInvestmentSecurityEvent(InvestmentSecurity security) {
		// request dividend specific fields for the specified security
		ReferenceDataRequest dataRequest = new ReferenceDataRequest("DVD_EX_DT", "DVD_PAY_DT", "DVD_RECORD_DT", "DVD_DECLARED_DT", "DVD_SH_LAST", "DVD_TYP_LAST", "DVD_CRNCY");
		BloombergRequestCommand command = getBloombergRequestCommand(security);
		dataRequest.addSecurity(command.getBloombergIdentifier());

		ReferenceDataResponse response = getBloombergDataService().getReferenceData(dataRequest, true);
		if (response.getBloomError() != null) {
			BloombergError error = response.getBloomError();
			Object lastType = response.getSingleSecurityPropertyValue("DVD_TYP_LAST");
			if ("BAD_FLD".equals(error.getCategory())) {
				if ("DVD_EX_DT".equals(error.getErrorField())) {
					// ignore this error which many securities that could pay dividends but don't return (GLD, etc.)
					return null;
				}
				else if ("DVD_PAY_DT".equals(error.getErrorField())) {
					if ("Discontinued".equals(lastType) || "Omitted".equals(lastType) || "Liquidation".equals(lastType)) {
						// ignore this error: securities that used to pay dividends but no longer do (AAPL, Y, etc.)
						return null;
					}
					if ("Regular Cash".equals(lastType) && response.getSingleSecurityPropertyValue("DVD_EX_DT") == null && response.getSingleSecurityPropertyValue("DVD_PAY_DT") == null) {
						// No Ex and Payment date usually means that the company is skipping a dividend (WM)
						return null;
					}
				}
				else if ("DVD_SH_LAST".equals(error.getErrorField())) {
					if ("Daily Accrual".equals(lastType)) {
						// doesn't have a payment at this time (BPLBX on February 1)
						return null;
					}
					// sometimes dates are known but payment amount isn't (QQQ in January of 2012)
					Date exDate = (Date) response.getSingleSecurityPropertyValue("DVD_EX_DT");
					if (exDate != null && DateUtils.addDays(exDate, -15).compareTo(new Date()) > 0) {
						// ignore if Ex Date is more than 15 days from today
						return null;
					}
				}
			}
			throw new BloombergRequestException(error);
		}

		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		InvestmentSecurityEventType eventType = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT);
		event.setType(eventType);
		event.setStatus(getInvestmentSecurityEventService().getInvestmentSecurityEventStatusByName(InvestmentSecurityEventStatus.STATUS_APPROVED));
		event.setSecurity(security);
		for (Map<String, Object> fields : response.getValues().values()) {
			if (fields.get("DVD_SH_LAST") == null) {
				return null;
			}

			// only update dividends that are Regular cash
			if ("Cancelled".equals(fields.get("DVD_TYP_LAST")) || "Daily Accrual".equals(fields.get("DVD_TYP_LAST"))) {
				return null;
			}

			BigDecimal dividend = BigDecimal.valueOf((Double) fields.get("DVD_SH_LAST")).setScale(eventType.getDecimalPrecision(), BigDecimal.ROUND_HALF_UP);
			event.setBeforeAndAfterEventValue(dividend);
			event.setDeclareDate((Date) fields.get("DVD_DECLARED_DT"));
			event.setExDate((Date) fields.get("DVD_EX_DT"));
			event.setEventDate((Date) fields.get("DVD_PAY_DT"));
			event.setRecordDate((Date) fields.get("DVD_RECORD_DT"));
			event.setEventDescription((String) fields.get("DVD_TYP_LAST"));

			String dividendCurrency = (String) fields.get("DVD_CRNCY");
			if (!StringUtils.isEmpty(dividendCurrency)) {
				InvestmentSecurity ccy = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(dividendCurrency, true);
				ValidationUtils.assertNotNull(ccy, "Cannot find dividend currency with symbol '" + dividendCurrency + "' for " + security.getLabel());
				event.setAdditionalSecurity(ccy);
			}

			if (event.getExDate() == null && event.getRecordDate() != null) {
				// there are instances when Bloomberg doesn't return EX date which is required: assume 2 business days before Record Date
				event.setExDate(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(event.getRecordDate(), getInvestmentCalculator().getInvestmentSecurityCalendar(event.getSecurity()).getId()), -2));
			}
		}
		return event;
	}


	@Override
	@SuppressWarnings("unused")
	public List<InvestmentSecurityEvent> getInvestmentSecurityEventHistory(InvestmentSecurity security, boolean ignoreSeedEvents) {
		throw new RuntimeException("Historical dividend retrieval is not supported");
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
