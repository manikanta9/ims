import com.clifton.gradle.plugin.build.usingVariant

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// Used by Crowd
	runtimeOnly("org.codehaus.xfire:xfire-aegis:1.2.6") {
		exclude(module = "XmlSchema")
		exclude(module = "jaxen")
		exclude(module = "junit")
	}

	// WebJars
	// implementation("org.webjars:extjs:3.3.0") // TODO: Switch to official webjars distribution
	implementation("org.webjars:webjars-locator:0.30")

	implementation("sencha:extjs:3.3.0:clifton-webjar")
	implementation("sencha:extjs-printer:1.0.0:webjar")
	implementation("org.webjars.npm:jsoneditor:5.5.7")

	implementation("org.springframework.boot:spring-boot-starter-actuator") {
		exclude(module = "spring-boot-starter-logging")
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	javascript(project(":clifton-core"))
	javascript(project(":clifton-portal"))

	jsNode(project(":clifton-portal-ui", configuration = "jsNode-prod-admin"))

	implementation(project(":clifton-core"))

	implementation(project(":clifton-portal")) { usingVariant("api") }
	implementation(project(":clifton-portal-ui"))
	implementation(project(":clifton-application"))


	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-application")))
	testFixturesApi(testFixtures(project(":clifton-core")))

}
