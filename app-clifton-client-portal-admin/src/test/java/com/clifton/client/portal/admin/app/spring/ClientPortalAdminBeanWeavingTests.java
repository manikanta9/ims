package com.clifton.client.portal.admin.app.spring;

import com.clifton.client.portal.admin.app.ClientPortalAdminApplication;
import com.clifton.core.context.spring.AbstractBeanWeavingTests;
import com.clifton.core.context.spring.SpringLoadTimeWeavingConfiguration;
import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestPropertySource;


/**
 * The {@link ClientPortalAdminBeanWeavingTests} attempts to validate that all types are properly woven during context initialization.
 *
 * @author lnaylor
 */
@SpringBootTest
//Used to fix issues that arise from the missing compile-time property replacements during CI builds
@TestPropertySource(locations = {"/app-clifton-client-portal-admin-test.properties", "/crowd-test.properties", "/spring-boot-test.properties"})
@ContextHierarchy({
		@ContextConfiguration(name = "root", classes = SpringLoadTimeWeavingConfiguration.class),
		@ContextConfiguration(name = "child", locations = "ClientPortalAdminBeanWeavingTests-context.xml"),
		@ContextConfiguration(name = "child", classes = ClientPortalAdminApplication.class)
})
@Tag("memory-db") // Run with in-memory database tests so that load-time weaving for transactions is enabled; weaving is disabled during standard unit tests
public class ClientPortalAdminBeanWeavingTests extends AbstractBeanWeavingTests {

	public ClientPortalAdminBeanWeavingTests(ApplicationContext applicationContext) {
		super(applicationContext);
	}
}
