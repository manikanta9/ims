package com.clifton.client.portal.admin.app;

import com.clifton.core.test.BasicProjectTests;
import com.clifton.portal.email.PortalEmail;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ClientPortalAdminAppBasicProjectTests extends BasicProjectTests {

	@Resource
	private PortalEmail portalEmail;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getProjectPrefix() {
		return "portal-admin-app";
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Controller");
		basicProjectAllowedSuffixNamesList.add("portalEmail");
		return basicProjectAllowedSuffixNamesList;
	}


	@Override
	public boolean isServiceSkipped(String beanName) {
		return "portalAdminSecurityController".equals(beanName);
	}


	@Override
	protected void configureRequiredApplicationContextBeans(Map<String, Object> beanMap) {
		beanMap.put("portalEmail", this.portalEmail);
	}
}
