package com.clifton.client.portal.admin.app.security.oauth;

import com.clifton.client.portal.admin.app.security.PortalAdminSecurityController;
import com.clifton.core.security.oauth.BaseSecurityOAuthUserTokenServiceImpl;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;


public class PortalAdminSecurityOAuthUserTokenServiceImpl extends BaseSecurityOAuthUserTokenServiceImpl<PortalSecurityUser> {


	private PortalSecurityUserService portalSecurityUserService;

	private PortalAdminSecurityController portalAdminSecurityController;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected PortalSecurityUser getSecurityUserCurrent() {
		return getPortalAdminSecurityController().getPortalSecurityUserCurrent();
	}


	@Override
	protected PortalSecurityUser getSecurityUserByName(String userName) {
		return getPortalSecurityUserService().getPortalSecurityUserByUserName(userName, false);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalAdminSecurityController getPortalAdminSecurityController() {
		return this.portalAdminSecurityController;
	}


	public void setPortalAdminSecurityController(PortalAdminSecurityController portalAdminSecurityController) {
		this.portalAdminSecurityController = portalAdminSecurityController;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
