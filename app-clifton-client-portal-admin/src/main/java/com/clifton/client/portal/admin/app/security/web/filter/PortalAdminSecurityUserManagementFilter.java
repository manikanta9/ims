package com.clifton.client.portal.admin.app.security.web.filter;


import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.timer.TimerHandler;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.core.web.view.JsonView;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The <code>PortalAdminSecurityUserManagementFilter</code> class is a servlet filter that puts current {@link
 * com.clifton.portal.security.user.PortalSecurityUser} into request {@link Context} before its execution, then chains remaining filter and finally clears the
 * user in the end.
 * <p>
 * Also does timing of execution.
 *
 * @author manderson
 */
@Component
public class PortalAdminSecurityUserManagementFilter implements Filter {

	private ContextHandler contextHandler;
	private HandlerExceptionResolver webExceptionResolver;
	private PortalSecurityUserService portalSecurityUserService;
	private SystemRequestStatsService systemRequestStatsService;
	private TimerHandler timerHandler;

	private JsonView webJsonView;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void init(FilterConfig config) throws ServletException {
		// do nothing
	}


	@Override
	public void destroy() {
		// do nothing
	}


	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		getTimerHandler().startTimer();
		HttpServletRequest request = (HttpServletRequest) req;

		try {
			PortalSecurityUser user = getPrincipalUser();
			if (user != null) {
				// 2. store current user in context
				getContextHandler().setBean(Context.USER_BEAN_NAME, user);
				getContextHandler().setBean(Context.REQUEST_PARAMETER_MAP, req.getParameterMap());
			}
			//Execute everything else
			chain.doFilter(req, res);
		}
		catch (Exception e) {
			try {
				// in case there's an exception, resolve it and render results
				HttpServletResponse response = (HttpServletResponse) res;
				ModelAndView modelAndView = getWebExceptionResolver().resolveException(request, response, null, e);
				AssertUtils.assertNotNull(modelAndView, "Exception resolver returned null ModelAndView");
				View view = modelAndView.getView();
				if (view == null) {
					view = getWebJsonView();
				}
				view.render(modelAndView.getModel(), request, response);
			}
			catch (Throwable innerException) {
				// do not throw exception if connection to the client was closed: there's nowhere to write response
				if (!ExceptionUtils.isConnectionBrokenException(innerException)) {
					// let's hope that this never happens
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, "ORIGINAL: ", ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(e))).withRequest(request));

					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), innerException, "INNER: ", ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(innerException))).withRequest(request));
					throw new ServletException("Error in generation of output for another error.", e);
				}
			}
		}
		finally {
			// 3. clear the user in the end
			Object user = getContextHandler().removeBean(Context.USER_BEAN_NAME);

			// clear full context so that the next request can have a clean start
			// NOTE: assumes this is the last part of request to be executed that may need the context
			getContextHandler().clear();

			// stop the timer and record stats
			getTimerHandler().stopTimer();
			getSystemRequestStatsService().saveSystemRequestStats(request, (HttpServletResponse) res, user, getTimerHandler().getDurationNano(), 0, 0);
			getTimerHandler().reset();
		}
	}


	protected PortalSecurityUser getPrincipalUser() {
		if (SecurityContextHolder.getContext().getAuthentication() == null) {
			throw new RuntimeException("Cannot find authentication in security context - Authentication is null.");
		}

		// 1. retrieve authenticated principal from security context
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal == null) {
			throw new RuntimeException("Cannot find authentication principal in security context.");
		}

		if (principal instanceof CrowdUserDetails) {
			PortalSecurityUser portalSecurityUser = new PortalSecurityUser();
			portalSecurityUser.setUsername(((CrowdUserDetails) principal).getUsername());
			portalSecurityUser.setFirstName(((CrowdUserDetails) principal).getFirstName());
			portalSecurityUser.setLastName(((CrowdUserDetails) principal).getLastName());
			return portalSecurityUser;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public HandlerExceptionResolver getWebExceptionResolver() {
		return this.webExceptionResolver;
	}


	public void setWebExceptionResolver(HandlerExceptionResolver webExceptionResolver) {
		this.webExceptionResolver = webExceptionResolver;
	}


	public TimerHandler getTimerHandler() {
		return this.timerHandler;
	}


	public void setTimerHandler(TimerHandler timerHandler) {
		this.timerHandler = timerHandler;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public JsonView getWebJsonView() {
		return this.webJsonView;
	}


	public void setWebJsonView(JsonView webJsonView) {
		this.webJsonView = webJsonView;
	}
}
