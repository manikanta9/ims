package com.clifton.client.portal.admin.app.config;

import com.clifton.application.context.config.web.ApplicationWebServerConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.DelegatingFilterProxy;


/**
 * The {@link PortalAdminWebServerConfiguration} configures the portalAdminSecurityUserManagementFilterRegistration bean, which
 * is used in place of the securityUserManagementFilterRegistration bean defined in {@link ApplicationWebServerConfiguration}.
 *
 * @author lnaylor
 */
@Configuration
public class PortalAdminWebServerConfiguration {

	@Configuration
	public static class PortalAdminFilterConfiguration {

		@Bean
		public FilterRegistrationBean<DelegatingFilterProxy> portalAdminSecurityUserManagementFilterRegistration() {
			DelegatingFilterProxy filter = new DelegatingFilterProxy();
			FilterRegistrationBean<DelegatingFilterProxy> registration = new FilterRegistrationBean<>(filter);
			registration.setName("portalAdminSecurityUserManagementFilter");
			registration.setOrder(200);
			registration.addInitParameter("targetFilterLifecycle", "true");
			registration.addUrlPatterns("/oauth/authorize", "/oauth/token", "*.json", "/ws/*");
			registration.addServletNames(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);
			return registration;
		}
	}
}
