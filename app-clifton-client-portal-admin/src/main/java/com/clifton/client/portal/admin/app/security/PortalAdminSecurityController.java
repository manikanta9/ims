package com.clifton.client.portal.admin.app.security;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.portal.security.user.PortalSecurityUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author manderson
 */
@Controller
public class PortalAdminSecurityController {

	private ContextHandler contextHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@RequestMapping("portalSecurityUserCurrent")
	public PortalSecurityUser getPortalSecurityUserCurrent() {
		// the user is put in the Context by PortalAdminSecurityUserManagementFilter
		return (PortalSecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
