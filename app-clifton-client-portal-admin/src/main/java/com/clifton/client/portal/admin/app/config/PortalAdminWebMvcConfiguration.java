package com.clifton.client.portal.admin.app.config;

import com.clifton.core.context.AutowireByNameBean;
import com.clifton.core.context.BeanModifier;
import com.clifton.core.context.BeanModifierTarget;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.web.bind.WebBindingHandler;
import com.clifton.core.web.bind.WebBindingHandlerImpl;
import com.clifton.core.web.bind.WebBindingInitializerWithValidation;
import com.clifton.core.web.converter.BigDecimalHttpMessageConverter;
import com.clifton.core.web.converter.BooleanHttpMessageConverter;
import com.clifton.core.web.converter.DateHttpMessageConverter;
import com.clifton.core.web.converter.IntegerHttpMessageConverter;
import com.clifton.core.web.handler.LoggingExceptionResolver;
import com.clifton.core.web.mvc.ConventionBasedRequestMappingHandlerMapping;
import com.clifton.core.web.mvc.ValidatingRequestMappingHandlerAdapter;
import com.clifton.core.web.mvc.ValidatingServletModelAttributeMethodProcessor;
import com.clifton.core.web.stats.RequestProcessorManagementServiceImpl;
import com.clifton.core.web.view.DynamicRequestToViewNameTranslator;
import com.clifton.core.web.view.DynamicViewResolver;
import com.clifton.core.web.view.FileDownloadView;
import com.clifton.core.web.view.FileUploadView;
import com.clifton.core.web.view.JsonView;
import com.clifton.core.web.view.MigrationActionDownloadView;
import com.clifton.portal.web.bind.PortalWebBindingDataRetriever;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.RequestToViewNameTranslator;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.support.WebContentGenerator;
import org.springframework.web.servlet.view.BeanNameViewResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerView;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;


/**
 * The {@link PortalAdminWebMvcConfiguration} provides web-MVC-related context configurations.
 *
 * @author lnaylor
 */
@Configuration
@EnableWebMvc
public class PortalAdminWebMvcConfiguration {

	@Configuration
	public static class HandlerMappingConfiguration {

		@AutowireByNameBean
		public HandlerMapping handlerMapping() {
			ConventionBasedRequestMappingHandlerMapping conventionBasedRequestMappingHandlerMapping = new ConventionBasedRequestMappingHandlerMapping();
			// Execute early for performance, as most requests will be resolved by this mapping
			conventionBasedRequestMappingHandlerMapping.setOrder(0);
			conventionBasedRequestMappingHandlerMapping.setExposeAPI(true);
			return conventionBasedRequestMappingHandlerMapping;
		}


		/**
		 * Enables <code>POST</code> request handling for all registered mappings.
		 */
		@BeanModifierTarget("resourceHandlerMapping")
		public BeanModifier<SimpleUrlHandlerMapping> resourceHandlerMappingModifier() {
			return (bean, beanName) -> {
				WebContentGenerator currentValue = (WebContentGenerator) bean.getHandlerMap().get("/**");
				if (currentValue != null) {
					currentValue.setSupportedMethods(ArrayUtils.add(currentValue.getSupportedMethods(), "POST", 0));
				}
			};
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class HandlerAdapterConfiguration {


		@AutowireByNameBean
		public ValidatingRequestMappingHandlerAdapter requestMappingHandlerAdapter() {
			ValidatingRequestMappingHandlerAdapter handlerAdapter = new ValidatingRequestMappingHandlerAdapter();
			handlerAdapter.setTransactionalBeanPrepare(false);
			handlerAdapter.setCustomArgumentResolvers(CollectionUtils.createList(validatingServletModelAttributeMethodProcessor()));
			handlerAdapter.setWebBindingInitializer(webBindingInitializer());
			handlerAdapter.setMessageConverters(CollectionUtils.createList(
					byteArrayHttpMessageConverter(),
					stringHttpMessageConverter(),
					sourceHttpMessageConverter(),
					allEncompassingFormHttpMessageConverter(),
					dateHttpMessageConverter(),
					bigDecimalHttpMessageConverter(),
					booleanHttpMessageConverter(),
					integerHttpMessageConverter()
			));
			return handlerAdapter;
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@AutowireByNameBean
		public WebBindingInitializerWithValidation webBindingInitializer() {
			return new WebBindingInitializerWithValidation();
		}


		@AutowireByNameBean
		public ValidatingServletModelAttributeMethodProcessor validatingServletModelAttributeMethodProcessor() {
			return new ValidatingServletModelAttributeMethodProcessor(true);
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Bean
		public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
			return new ByteArrayHttpMessageConverter();
		}


		@Bean
		public StringHttpMessageConverter stringHttpMessageConverter() {
			return new StringHttpMessageConverter();
		}


		@Bean
		public SourceHttpMessageConverter<?> sourceHttpMessageConverter() {
			return new SourceHttpMessageConverter<>();
		}


		@Bean
		public AllEncompassingFormHttpMessageConverter allEncompassingFormHttpMessageConverter() {
			return new AllEncompassingFormHttpMessageConverter();
		}


		@Bean
		public DateHttpMessageConverter dateHttpMessageConverter() {
			return new DateHttpMessageConverter();
		}


		@Bean
		public BigDecimalHttpMessageConverter bigDecimalHttpMessageConverter() {
			return new BigDecimalHttpMessageConverter();
		}


		@Bean
		public BooleanHttpMessageConverter booleanHttpMessageConverter() {
			return new BooleanHttpMessageConverter();
		}


		@Bean
		public IntegerHttpMessageConverter integerHttpMessageConverter() {
			return new IntegerHttpMessageConverter();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class WebBindingConfiguration {

		@AutowireByNameBean
		public WebBindingHandler webBindingHandler() {
			return new WebBindingHandlerImpl();
		}


		@AutowireByNameBean
		public PortalWebBindingDataRetriever portalWebBindingDataRetriever() {
			PortalWebBindingDataRetriever portalWebBindingDataRetriever = new PortalWebBindingDataRetriever();
			portalWebBindingDataRetriever.setOrder(100);
			return portalWebBindingDataRetriever;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class ExceptionHandlerConfiguration {

		@AutowireByNameBean
		public LoggingExceptionResolver webExceptionResolver(JacksonHandlerImpl externalServiceJacksonHandler) {
			LoggingExceptionResolver loggingExceptionResolver = new LoggingExceptionResolver();
			loggingExceptionResolver.setDefaultErrorView(DynamicViewResolver.DYNAMIC_VIEW_NAME);
			Map<Pattern, String> urlPatternToErrorViewNameMap = new HashMap<>();
			urlPatternToErrorViewNameMap.put(Pattern.compile(".*" + ContextConventionUtils.API_ROOT + "/.*"), "externalServiceJacksonErrorView");
			loggingExceptionResolver.setUrlPatternToErrorViewNameMap(urlPatternToErrorViewNameMap);
			Properties statusCodes = new Properties();
			statusCodes.put("externalServiceJacksonErrorView", "418");
			loggingExceptionResolver.setStatusCodes(statusCodes);
			loggingExceptionResolver.setJacksonHandler(externalServiceJacksonHandler);
			return loggingExceptionResolver;
		}


		@Bean
		public WebMvcConfigurer webExceptionResolverConfigurer(HandlerExceptionResolver webExceptionResolver) {
			// Select our custom exception resolver as the singular exception resolver to include during processing
			return new WebMvcConfigurer() {
				@Override
				public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> resolvers) {
					resolvers.add(webExceptionResolver);
				}
			};
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class ViewConfiguration {

		@Bean
		public WebMvcConfigurer viewConfigurationWebMvcConfigurer() {
			return new WebMvcConfigurer() {
				@Override
				public void addViewControllers(ViewControllerRegistry registry) {
					registry.addViewController("/").setViewName("index");
				}
			};
		}


		/**
		 * The standard view resolver, overriding the default view resolver provided by {WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter#viewResolver(BeanFactory)}.
		 */
		@AutowireByNameBean(name = DispatcherServlet.VIEW_RESOLVER_BEAN_NAME)
		public ViewResolver viewResolver() {
			Map<Pattern, View> urlPatternMap = new HashMap<>();
			urlPatternMap.put(Pattern.compile(".*" + ContextConventionUtils.API_ROOT + "/.*"), externalServiceJacksonView());
			urlPatternMap.put(Pattern.compile(".*Upload.json"), fileUploadView());
			urlPatternMap.put(Pattern.compile(".*Download.json"), fileDownloadView());

			Map<String, View> outputFormatMap = new HashMap<>();
			outputFormatMap.put("migrationActions", webMigrationActionDownloadView());

			DynamicViewResolver resolver = new DynamicViewResolver();
			resolver.setDefaultDownloadView(fileDownloadView());
			resolver.setUrlPatternToViewMap(urlPatternMap);
			resolver.setOutputFormatToDownloadViewMap(outputFormatMap);
			resolver.setStaticView(webJsonView());
			resolver.setOrder(Ordered.HIGHEST_PRECEDENCE);
			return resolver;
		}


		// TODO: Should be replaced with org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration.WebMvcAutoConfigurationAdapter.beanNameViewResolver; auto-configuration ordering issue seems to prevent this
		@Bean
		public ViewResolver beanNameViewResolver() {
			BeanNameViewResolver beanNameViewResolver = new BeanNameViewResolver();
			beanNameViewResolver.setOrder(100); // Execute this before the InternalResourceViewResolver
			return beanNameViewResolver;
		}


		@BeanModifierTarget("freeMarkerViewResolver")
		public BeanModifier<FreeMarkerViewResolver> freeMarkerViewResolverViewClassModifier() {
			return (bean, beanName) ->
					bean.setViewClass(RequestExposingFreeMarkerView.class);
		}


		public static class RequestExposingFreeMarkerView extends FreeMarkerView {

			@Override
			protected void exposeHelpers(Map<String, Object> model, HttpServletRequest request) throws Exception {
				super.exposeHelpers(model, request);
				model.put("request", request);
			}
		}


		/**
		 * This modifier needs to regenerate the configuration (bean.setConfiguration) since a stale one will already have been generated via afterPropertiesSet
		 */
		@BeanModifierTarget("freeMarkerConfigurer")
		public BeanModifier<FreeMarkerConfigurer> freeMarkerConfigurerVariablesModifier(WebApplicationContext applicationContext) {
			return (bean, beanName) -> {
				bean.setFreemarkerVariables(MapUtils.ofEntries(
						MapUtils.entry("applicationContext", applicationContext),
						MapUtils.entry("environment", applicationContext.getEnvironment())
				));
				bean.setConfiguration(bean.createConfiguration());
			};
		}


		/**
		 * The view name provider used by Spring when no explicit view name is provided for the request.
		 */
		@AutowireByNameBean(name = DispatcherServlet.REQUEST_TO_VIEW_NAME_TRANSLATOR_BEAN_NAME)
		public RequestToViewNameTranslator requestToViewNameTranslator() {
			return new DynamicRequestToViewNameTranslator();
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@AutowireByNameBean
		public JsonView webJsonView() {
			JsonView view = new JsonView();
			view.setRootNodeName("data");
			view.setSuccessPropertyName("success");
			view.setResponseBufferSize((int) Math.pow(2, 16)); // 64 KB
			view.setResponseLimitSize((long) Math.pow(2, 30)); // 1 GB
			return view;
		}


		@AutowireByNameBean
		public FileDownloadView<?> fileDownloadView() {
			FileDownloadView<?> view = new FileDownloadView<>();
			view.setRootNodeName("data");
			view.setSuccessPropertyName("success");
			view.setResponseBufferSize((int) Math.pow(2, 16)); // 64 KB
			view.setResponseLimitSize((long) Math.pow(2, 30)); // 1 GB
			return view;
		}


		@AutowireByNameBean
		public FileUploadView fileUploadView() {
			FileUploadView view = new FileUploadView();
			view.setRootNodeName("data");
			view.setSuccessPropertyName("success");
			view.setResponseBufferSize((int) Math.pow(2, 16)); // 64 KB
			view.setResponseLimitSize((long) Math.pow(2, 30)); // 1 GB
			return view;
		}


		@AutowireByNameBean
		public MigrationActionDownloadView webMigrationActionDownloadView() {
			MigrationActionDownloadView view = new MigrationActionDownloadView();
			view.setRootNodeName("data");
			view.setSuccessPropertyName("success");
			view.setResponseBufferSize((int) Math.pow(2, 16)); // 64 KB
			view.setResponseLimitSize((long) Math.pow(2, 30)); // 1 GB
			return view;
		}


		/**
		 * Before converting to Spring Boot, both <code>externalServiceJacksonView</code> and <code>externalServiceJacksonErrorView</code>
		 * had their object mappers set with the object mapper used by <code>externalServiceJacksonHandler</code>. The below code will
		 * initialize them to use normal <code>ObjectMapper</code> class. So far, no issues have come from this, but if any bugs start
		 * appearing this may need to be updated.
		 */
		@Bean
		public MappingJackson2JsonView externalServiceJacksonView() {
			MappingJackson2JsonView view = new MappingJackson2JsonView();
			view.setExtractValueFromSingleKeyModel(true);
			return view;
		}


		@Bean
		public MappingJackson2JsonView externalServiceJacksonErrorView() {
			MappingJackson2JsonView mappingJackson2JsonView = new MappingJackson2JsonView();
			mappingJackson2JsonView.setExtractValueFromSingleKeyModel(true);
			return mappingJackson2JsonView;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@AutowireByNameBean
	public RequestProcessorManagementServiceImpl requestProcessorManagementService(JsonView webJsonView) {
		RequestProcessorManagementServiceImpl requestProcessorManagementService = new RequestProcessorManagementServiceImpl();
		requestProcessorManagementService.setJsonView(webJsonView);
		return requestProcessorManagementService;
	}


	@AutowireByNameBean
	public JacksonObjectMapper objectMapper() {
		return new JacksonObjectMapper();
	}
}

