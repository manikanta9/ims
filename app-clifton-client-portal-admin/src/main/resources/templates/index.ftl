<#-- @ftlvariable name="environment" type="org.springframework.core.env.Environment" -->
<?xml version="1.0" encoding="UTF-8" ?>
<#assign extJsRootUrl = environment.getRequiredProperty("application.extjs.rootUrl")>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Parametric Portal Admin</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<link rel="SHORTCUT ICON" href="core/images/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/ux/gridfilters/css/GridFilters.css"/>
	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/ux/gridfilters/css/RangeMenu.css"/>
	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/resources/css/ext-all.css"/>
	<link rel="stylesheet" type="text/css" href="${extJsRootUrl}/ux/css/ux-all.css"/>
	<link rel="stylesheet" type="text/css" href="core/css/application.css" media="screen"/>

	<!-- Used for JSON migration functionality -->
	<link rel="stylesheet" type="text/css" href="webjars/jsoneditor/5.5.7/dist/jsoneditor.css"/>
	<link rel="stylesheet" type="text/css" href="core/css/jsoneditor-ims-theme.css"/>

	<script type="text/javascript" src="${extJsRootUrl}/adapter/ext/ext-base-debug.js"></script>
	<script type="text/javascript" src="${extJsRootUrl}/ext-all-debug.js"></script>
	<script type="text/javascript" src="${extJsRootUrl}/ux/ux-all-debug.js"></script>
	<script type="text/javascript" src="${extJsRootUrl}/ux/FieldReplicator.js"></script>
	<script type="text/javascript" src="webjars/extjs-printer/1.0.0/Printer-all.js"></script>

	<!-- Used for JSON migration functionality -->
	<script type="text/javascript" src="webjars/jsoneditor/5.5.7/dist/jsoneditor.min.js"></script>

	<script type="text/javascript">
		Ext.ns('TCG');
		TCG.ExtRootUrl = '${extJsRootUrl}';
		TCG.environmentLevel = '${environment.getRequiredProperty("application.environment.level")}';
		TCG.PageConfiguration = {
			applicationName: '${environment.getRequiredProperty("application.name")}'.toLowerCase(),
			instance: {
				text: '${environment.getRequiredProperty("application.instance.text")}',
				color: '${environment.getRequiredProperty("application.instance.text.color")}',
				size: '${environment.getRequiredProperty("application.instance.text.size")}',
				version: '${environment.getRequiredProperty("application.instance.version")}',
				theme: '${environment.getRequiredProperty("application.instance.theme")}'
			},
			graylog: {
				apiScheme: '${environment.getRequiredProperty("graylog.api.scheme")}',
				apiHost: '${environment.getRequiredProperty("graylog.api.host")}'
			},
			clientPortal: {
				adminUrl: '${environment.getRequiredProperty("portal.admin.client.ui.url")}'
			}
		};
	</script>

	<script type="text/javascript" src="core/js/tcg.js"></script>
	<script type="text/javascript" src="core/js/tcg-app.js"></script>
	<script type="text/javascript" src="core/js/tcg-cache.js"></script>
	<script type="text/javascript" src="core/js/tcg-data.js"></script>
	<script type="text/javascript" src="core/js/tcg-file.js"></script>
	<script type="text/javascript" src="core/js/tcg-form.js"></script>
	<script type="text/javascript" src="core/js/tcg-grid.js"></script>
	<script type="text/javascript" src="core/js/tcg-gridfilters.js"></script>
	<script type="text/javascript" src="core/js/tcg-gridsummary.js"></script>
	<script type="text/javascript" src="core/js/tcg-toolbar.js"></script>
	<script type="text/javascript" src="core/js/tcg-tree.js"></script>

	<script type="text/javascript" src="core/core-shared.js"></script>
	<script type="text/javascript" src="portal/portal-api-shared.js"></script>
	<script type="text/javascript" src="js/portal/portal-shared.js"></script>


	<!-- Application Bootstrap -->
	<script type="text/javascript" src="js/app-client-portal-admin-bootstrap.js"></script>
</head>


</html>

