Clifton.portal.PATH = window.location.pathname;
Clifton.portal.PATH_LENGTH = Clifton.portal.PATH ? Clifton.portal.PATH.length : 0;

Clifton.portal.PortalStatsListWindow = Ext.extend(TCG.app.Window, {
	id: 'coreStatsListWindow',
	title: 'System Access Stats',
	iconCls: 'timer',
	width: 1200,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Request Stats',
				items: [{
					xtype: 'gridpanel',
					name: 'systemRequestStatsListFind',
					instructions: 'The following statistics was collected for system requests. Time is shown in seconds.',
					limitRequestedProperties: false,
					forceLocalFiltering: true,
					isPagingEnabled: function() {
						return false;
					},
					columns: [
						{header: 'Source', dataIndex: 'requestSource', width: 50},
						{
							header: 'URI', dataIndex: 'requestURI', width: 160,
							renderer: function(v, metaData, r) {
								if (r.data.requestSource === 'WEB') {
									// remove app name
									return Clifton.portal.PATH_LENGTH === 0 ? v : v.substring(Clifton.portal.PATH_LENGTH);
								}
								return v;
							}
						},
						{header: 'Hits', dataIndex: 'executeCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Total Number of URI Hits'},
						{header: 'Slow Hits', dataIndex: 'slowExecuteCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Number of URI Hits Slower than 1 Second'},
						{
							header: '% Slow', dataIndex: 'slowExecutePercent', width: 40, type: 'float', sortable: false, tooltip: 'Percent of URI Hits Slower than 1 Second',
							renderer: function(value, metaData, r) {
								const data = r.data;
								return TCG.numberFormat(100 * data['slowExecuteCount'] / data['executeCount'], '0,000.00 %');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return 100 * data['slowExecuteCount'] / data['executeCount'];
							}
						},
						{header: 'Total Time', dataIndex: 'totalTimeSeconds', width: 40, type: 'currency', summaryType: 'sum', defaultSortColumn: true, defaultSortDirection: 'DESC', tooltip: 'Total Execution Time in Seconds'},
						{header: 'Total Time Formatted', dataIndex: 'totalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Min Time', dataIndex: 'minTotalTimeSeconds', width: 40, type: 'float', summaryType: 'min', tooltip: 'Minimum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							}
						},
						{header: 'Min Time Formatted', dataIndex: 'minTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Max Time', dataIndex: 'maxTotalTimeSeconds', width: 40, type: 'float', summaryType: 'max', tooltip: 'Maximum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							}
						},
						{header: 'Max Time Formatted', dataIndex: 'maxTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Avg Time', dataIndex: 'avgTimeSeconds', width: 40, type: 'float', summaryType: 'average', tooltip: 'Average Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return data['totalTimeSeconds'] / data['executeCount'];
							}
						},
						{header: 'Avg Time Formatted', dataIndex: 'avgTimeFormatted', width: 60, hidden: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('requestSource', 'WEB', true, true);
						}
					},
					editor: {
						detailPageClass: 'Clifton.core.stats.StatsWindow',
						getDefaultDataForExisting: function(gridPanel, row) {
							return row.json;
						},
						addEnabled: false,
						addToolbarDeleteButton: function(toolBar) {
							toolBar.add({
								text: 'Clear',
								tooltip: 'Remove stats for selected row',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select URI to be cleared.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection clearing is not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										Ext.Msg.confirm('Clear Selected Stats', 'Would you like to clear stats for selected URI?', function(a) {
											if (a === 'yes') {
												const data = sm.getSelected().data;
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Clearing...',
													params: {
														requestSource: data.requestSource,
														requestURI: data.requestURI
													}
												});
												loader.load('systemRequestStatsDelete.json');
											}
										});
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},

			{
				title: 'Portal Request Stats',
				items: [{
					xtype: 'gridpanel',
					name: 'portalSystemRequestStatsListFind',
					instructions: 'The following statistics was collected for portal system requests. Time is shown in seconds.',
					limitRequestedProperties: false,
					forceLocalFiltering: true,
					isPagingEnabled: function() {
						return false;
					},
					columns: [
						{header: 'Source', dataIndex: 'requestSource', width: 50},
						{
							header: 'URI', dataIndex: 'requestURI', width: 160,
							renderer: function(v, metaData, r) {
								if (r.data.requestSource === 'WEB') {
									// remove app name
									return Clifton.portal.PATH_LENGTH === 0 ? v : v.substring(Clifton.portal.PATH_LENGTH);
								}
								return v;
							}
						},
						{header: 'Hits', dataIndex: 'executeCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Total Number of URI Hits'},
						{header: 'Slow Hits', dataIndex: 'slowExecuteCount', width: 40, type: 'int', summaryType: 'sum', tooltip: 'Number of URI Hits Slower than 1 Second'},
						{
							header: '% Slow', dataIndex: 'slowExecutePercent', width: 40, type: 'float', sortable: false, tooltip: 'Percent of URI Hits Slower than 1 Second',
							renderer: function(value, metaData, r) {
								const data = r.data;
								return TCG.numberFormat(100 * data['slowExecuteCount'] / data['executeCount'], '0,000.00 %');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return 100 * data['slowExecuteCount'] / data['executeCount'];
							}
						},
						{header: 'Total Time', dataIndex: 'totalTimeSeconds', width: 40, type: 'currency', summaryType: 'sum', defaultSortColumn: true, defaultSortDirection: 'DESC', tooltip: 'Total Execution Time in Seconds'},
						{header: 'Total Time Formatted', dataIndex: 'totalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Min Time', dataIndex: 'minTotalTimeSeconds', width: 40, type: 'float', summaryType: 'min', tooltip: 'Minimum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							}
						},
						{header: 'Min Time Formatted', dataIndex: 'minTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Max Time', dataIndex: 'maxTotalTimeSeconds', width: 40, type: 'float', summaryType: 'max', tooltip: 'Maximum Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							}
						},
						{header: 'Max Time Formatted', dataIndex: 'maxTotalTimeFormatted', width: 60, hidden: true},
						{
							header: 'Avg Time', dataIndex: 'avgTimeSeconds', width: 40, type: 'float', summaryType: 'average', tooltip: 'Average Execution Time in Seconds',
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(value, '0,000.000');
							},
							summaryCalculation: function(v, r, field, data, col) {
								return data['totalTimeSeconds'] / data['executeCount'];
							}
						},
						{header: 'Avg Time Formatted', dataIndex: 'avgTimeFormatted', width: 60, hidden: true}
					],
					plugins: {ptype: 'gridsummary'},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('requestSource', 'WEB', true, true);
						}
					},
					editor: {
						detailPageClass: 'Clifton.core.stats.StatsWindow',
						getDefaultDataForExisting: function(gridPanel, row) {
							return row.json;
						},
						addEnabled: false,
						addToolbarDeleteButton: function(toolBar) {
							toolBar.add({
								text: 'Clear',
								tooltip: 'Remove stats for selected row',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select URI to be cleared.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection clearing is not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										Ext.Msg.confirm('Clear Selected Stats', 'Would you like to clear stats for selected URI?', function(a) {
											if (a === 'yes') {
												const data = sm.getSelected().data;
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Clearing...',
													params: {
														requestSource: data.requestSource,
														requestURI: data.requestURI
													}
												});
												loader.load('portalSystemRequestStatsDelete.json');
											}
										});
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},

			{
				title: 'Portal Cache Stats',
				items: [{
					xtype: 'gridpanel',
					name: 'portalSystemCacheStats',
					instructions: 'The following cache statistics was collected. "statistics" property must be enabled in config file in order to see stats.',
					columns: [
						{header: 'Cache Name', dataIndex: 'cacheName', width: 150},
						{header: 'Count', dataIndex: 'size', width: 30, type: 'int', summaryType: 'sum', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Memory Size', dataIndex: 'inMemorySize', width: 40, type: 'int', summaryType: 'sum'},
						{header: 'Time to Live', dataIndex: 'timeToLive', width: 35, type: 'int', title: 'Time before cache expires in seconds'},
						{header: 'Stats Enabled', dataIndex: 'statisticsEnabled', width: 40, type: 'boolean'},
						{header: 'Hit Count', dataIndex: 'cacheHitCount', width: 30, type: 'int', summaryType: 'sum'},
						{header: 'Miss Count', dataIndex: 'cacheMissCount', width: 30, type: 'int', summaryType: 'sum'},
						{
							header: 'Hit %', dataIndex: 'cacheHitCount', width: 25, type: 'float', sortable: false,
							renderer: function(value, metaData, r) {
								return TCG.numberFormat(100 * value / (value + r.data.cacheMissCount), '0,000.00 %');
							}
						}
					],
					getTopToolbarFilters: function(toolbar) {
						return ['-', {
							fieldLabel: '', boxLabel: 'Calculate Size', xtype: 'checkbox', name: 'calculateSize',
							listeners: {
								check: function(field) {
									TCG.getParentByClass(field, Ext.Panel).reload();
								}
							}
						}];
					},
					getLoadParams: function(firstLoad) {
						const calcSize = TCG.getChildByName(this.getTopToolbar(), 'calculateSize');
						return {calculateSize: calcSize.checked};
					},
					plugins: {ptype: 'gridsummary'},
					// show memory stats in the top toolbar
					updateCount: function() {
						const t = this.getTopToolbar();
						if (t) {
							const d = TCG.data.getData('portalSystemMemoryState.json', this);
							const mb = 1024 * 1024;
							const label = d ? 'Free: ' + Ext.util.Format.number(d.freeMemory / mb, '0') + 'MB | Total: ' + Ext.util.Format.number(d.totalMemory / mb, '0') + 'MB | Max: ' + Ext.util.Format.number(d.maxMemory / mb, '0') + 'MB' : 'N/A';
							const last = t.items.get(t.items.length - 1);
							if (last.text && last.text.indexOf('Free:') === 0) {
								last.setText(label);
							}
							else {
								t.add('->', {xtype: 'tbtext', text: label});
							}
							t.syncSize();
						}
					},
					editor: {
						addEnabled: false,
						addToolbarDeleteButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Clear',
								tooltip: 'Remove all items from selected cache',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a cache to be cleared.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection clearing is not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										Ext.Msg.confirm('Clear Selected Cache', 'Would you like to clear all elements from selected cache?', function(a) {
											if (a === 'yes') {
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Clearing...',
													params: {cacheName: sm.getSelected().data.cacheName}
												});
												loader.load('portalSystemCacheDelete.json');
											}
										});
									}
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Load Cache',
								tooltip: 'Ability to pre-load caches for the portal.  Can optionally clear all existing caches.',
								iconCls: 'run',
								scope: this,
								menu: new Ext.menu.Menu({
									items: [
										{
											text: 'Load Cache (With Clear)',
											tooltip: 'Clear all existing caches and pre-load cache data.',
											iconCls: 'run',
											handler: function(b) {
												const loader = new TCG.data.JsonLoader({
													waitTarget: gridPanel,
													waitMsg: 'Clearing and Loading...',
													timeout: 120000,
													params: {doNotClearCache: false},
													onLoad: function(record, conf) {
														gridPanel.reload();
													}
												});
												loader.load('portalCacheReload.json');
											}
										},
										{
											text: 'Load Cache (Do Not Clear)',
											tooltip: 'Pre-load cache data, do not clear any existing cache data',
											iconCls: 'run',
											handler: function(b) {
												const loader = new TCG.data.JsonLoader({
													waitTarget: gridPanel,
													timeout: 120000,
													params: {doNotClearCache: true},
													onLoad: function(record, conf) {
														gridPanel.reload();
													}
												});
												loader.load('portalCacheReload.json');
											}
										}
									]
								})
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Enable Stats',
								tooltip: 'Enabled collection of Cache statistics for selected cache (all caches if none is selected): slight performance overhead',
								iconCls: 'checked',
								scope: this,
								handler: function() {
									const sm = this.grid.getSelectionModel();
									const cacheName = (sm.getCount() === 1) ? sm.getSelected().data.cacheName : '';
									TCG.getResponseText('portalSystemCacheStatsEnabled.json?enableStatistics=true&cacheName=' + cacheName, this);
									this.grid.ownerCt.reload();
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Disable Stats',
								tooltip: 'Disable collection of Cache statistics for selected cache (all caches if none is selected): slight performance improvement',
								iconCls: 'unchecked',
								scope: this,
								handler: function() {
									const sm = this.grid.getSelectionModel();
									const cacheName = (sm.getCount() === 1) ? sm.getSelected().data.cacheName : '';
									TCG.getResponseText('portalSystemCacheStatsEnabled.json?enableStatistics=false&cacheName=' + cacheName, this);
									this.grid.ownerCt.reload();
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'URL Mappings',
				items: [
					{
						name: 'contextUrlMappingDefinitionList',
						xtype: 'gridpanel',
						columns: [
							{header: 'URL Pattern', width: 60, dataIndex: 'url'},
							{header: 'Bean Name', width: 40, dataIndex: 'beanName'},
							{header: 'Bean Class', width: 100, dataIndex: 'beanClassName'},
							{header: 'Method', width: 200, dataIndex: 'methodSignature'}
						],
						editor: {
							drillDownOnly: true
						}
					}
				]
			},


			{
				title: 'Portal URL Mappings',
				items: [
					{
						name: 'portalContextUrlMappingDefinitionList',
						xtype: 'gridpanel',
						columns: [
							{header: 'URL Pattern', width: 60, dataIndex: 'url'},
							{header: 'Bean Name', width: 40, dataIndex: 'beanName'},
							{header: 'Bean Class', width: 100, dataIndex: 'beanClassName'},
							{header: 'Method', width: 200, dataIndex: 'methodSignature'}
						],
						editor: {
							drillDownOnly: true
						}
					}
				]
			},

			{
				title: 'Portal Scheduled Runners',
				items: [{
					xtype: 'core-scheduled-runner-grid',
					name: 'portalRunnerConfigListFind',
					restartUrl: 'portalRunnerControllerReschedule.json',
					clearCompletedUrl: 'portalRunnerCompletedClear.json',
					getLoadParams: function() {
						return undefined; // include all
					}
				}]
			}
		]
	}]
});
