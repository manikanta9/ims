Clifton.portal.feedback.QuestionWindow = Ext.extend(Clifton.portal.DetailWindow, {
	titlePrefix: 'Feedback Question',
	iconCls: 'question',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'portal-formpanel',
					url: 'portalFeedbackQuestion.json',

					getWarningMessage: function(form) {
						let msg = undefined;
						if (TCG.isTrue(form.formValues.disabled)) {
							msg = 'This question has been disabled and is not available to the client for entry.';
						}
						return msg;
					},

					items: [
						{fieldLabel: 'Parent Question', name: 'parent.name', hiddenName: 'parent.id', xtype: 'combo', url: 'portalFeedbackQuestionListFind.json?rootOnly=true', detailPageClass: 'Clifton.portal.feedback.QuestionWindow'},
						{fieldLabel: 'Question Name', name: 'name'},
						{
							fieldLabel: 'Display Order', name: 'displayOrder', xtype: 'spinnerfield', allowNegative: false,
							qtip: 'Display order is by parent order, then child order'
						},
						{fieldLabel: '', boxLabel: 'Disabled', name: 'disabled', xtype: 'checkbox', qtip: 'Disabled questions are saved historically if there are results associated with them, however are not available for entry now.'},
						{fieldLabel: 'Question Text', name: 'questionText', xtype: 'textarea', height: 35, grow: true},

						{
							xtype: 'fieldset-checkbox',
							checkboxName: 'selectableOption',
							title: 'Options',

							instructions: 'If the question involves a multiple choice answer, then the following options will be displayed for choices.  If not, then a text area will be available for the user to enter free text.',
							defaults: {
								anchor: '0'
							},
							items: [
								{
									xtype: 'formgrid',
									storeRoot: 'optionList',
									dtoClass: 'com.clifton.portal.feedback.PortalFeedbackQuestionOption',
									columnsConfig: [
										{header: 'Option Value', width: 300, dataIndex: 'name', editor: {xtype: 'textfield'}},
										{
											header: 'Description', width: 350, dataIndex: 'description', editor: {xtype: 'textfield'},
											tooltip: 'Will be displayed as optional tooltip for the option'
										},
										{
											header: 'Order', width: 75, dataIndex: 'displayOrder', editor: {xtype: 'spinnerfield'}, type: 'int',
											tooltip: 'Option display order'
										}
									]
								}
							]
						}

					]
				}]
			},
			{
				title: 'Results',
				items: [{
					name: 'portalFeedbackResultListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'These are the results from users who entered feedback on this question.',

					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// Default to those that are Entered within the last 30 days
							this.setFilterValue('resultDate', {'after': new Date().add(Date.DAY, -30)});
						}
						const w = this.getWindow();

						return {
							feedbackQuestionId: w.getMainFormId()
						};
					},

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Result', width: 300, dataIndex: 'resultText'},
						{header: 'Entered By', width: 75, dataIndex: 'securityUser.label', filter: {searchFieldName: 'userSearchPattern'}},
						{header: 'E-mail', width: 75, hidden: true, dataIndex: 'securityUser.username', filter: {searchFieldName: 'username'}},
						{header: 'Company Name', width: 75, dataIndex: 'securityUser.companyName', filter: {searchFieldName: 'userCompanyName'}},
						{header: 'Title', width: 75, hidden: true, dataIndex: 'securityUser.title', filter: {searchFieldName: 'userTitle'}},
						{header: 'Phone Number', width: 75, hidden: true, dataIndex: 'securityUser.phoneNumber', filter: {searchFieldName: 'userPhoneNumber'}},
						{header: 'Entered On', width: 75, dataIndex: 'resultDate'}
					],

					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.feedback.ResultWindow'
					}
				}]
			}
		]
	}]
});
