Clifton.portal.feedback.FeedbackEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Feedback Entry',
	iconCls: 'question',
	width: 1000,
	height: 650,

	items: [{
		xtype: 'formwithdynamicfields',
		defaults: {anchor: '-5'},
		labelWidth: 25,

		getSaveURL: function() {
			return 'portalFeedbackResultEntrySave.json';
		},

		instructions: 'Thank you for visiting the client portal.  In the months to come, we will continue to evolve this experience to provide additional Resources, Tools, and client account management capabilities.  Feedback from our clients will help us continue to enhance your experience.',

		dynamicTriggerValueFieldName: 'id',
		dynamicFieldTypePropertyName: 'feedbackQuestion',
		dynamicFieldListPropertyName: 'resultList',
		dynamicFieldValuePropertyName: 'resultText',
		dynamicFieldsUrl: 'portalFeedbackQuestionListFind.json?disabled=false&orderBy=orderExpanded&populateOptions=true',
		dynamicFieldClass: 'com.clifton.portal.feedback.PortalFeedbackResult',

		items: [],


		onRender: function() {
			TCG.form.FormPanelWithDynamicFields.superclass.onRender.apply(this, arguments);
			const triggerField = this.getForm().findField(this.dynamicTriggerFieldName);
			this.resetDynamicFields(triggerField, true);
		},

		// returns form field for the specified field type
		getDynamicFieldFromRecord: function(fp, fieldType, index) {
			const fieldLabel = {
				xtype: 'sectionheaderfield',
				name: fieldType.name,
				submitValue: false,
				header: fieldType.questionText
			};

			if (fieldType.parent) {
				fieldLabel.header = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + fieldLabel.header;
			}
			const field = {
				xtype: 'textarea',
				name: this.getFieldNameFromType(fieldType),
				fieldLabel: '',
				submitValue: false,
				allowBlank: true,
				fieldTypeId: fieldType.id // used during bean property creation to identify field type
			};
			if (fieldType.description) {
				field.qtip = fieldType.description;
			}

			if (fieldType.selectableOption) {
				field.xtype = 'radiogroup';
				field.items = [];
				for (let i = 0; i < fieldType.optionList.length; i++) {
					const option = fieldType.optionList[i];
					field.items.push({fieldLabel: '', xtype: 'radio', boxLabel: option.name, qtip: option.description, name: field.name, inputValue: option.name});
				}
			}
			else {
				field.height = 35;
				field.grow = true;
			}
			//return field;
			return [fieldLabel, field];
		},

		// add dynamic field values separately
		getSubmitParams: function() {
			let result = {};
			if (this.dynamicFields) {
				let submitCount = 0;
				for (let i = 0; i < this.dynamicFields.length; i++) {
					const field = this.dynamicFields[i];
					// do not submit fields with no values
					if (TCG.isNotNull(field) && field.getValue() !== '' && field.submitDetailField !== false && field.fieldTypeId) {
						result[this.dynamicFieldListPropertyName + '[' + submitCount + '].' + this.dynamicFieldTypePropertyName + '.id'] = field.fieldTypeId;
						const value = this.getValueFromField(field);
						if (this.dynamicFieldClass) {
							result[this.dynamicFieldListPropertyName + '[' + submitCount + '].class'] = this.dynamicFieldClass;
						}
						result[this.dynamicFieldListPropertyName + '[' + submitCount + '].' + this.dynamicFieldValuePropertyName] = value;
						submitCount++;
					}
				}
			}
			if (this.appendSubmitParams) {
				result = this.appendSubmitParams(result);
			}
			return result;
		},
		getValueFromField: function(field) {
			let value = field.getValue();
			if (field.xtype === 'radiogroup') {
				if (field.getValue()) {
					value = field.getValue().getGroupValue();
				}
			}
			return value;
		}
	}]
});
