Clifton.portal.feedback.FeedbackSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'feedbackSetupWindow',
	title: 'Feedback Setup',
	iconCls: 'question',
	width: 1000,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Results',
				items: [{
					name: 'portalFeedbackResultListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'These are the results from users who entered feedback on the Portal.',
					groupField: 'feedbackQuestion.questionText',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Responses" : "Response"]}',
					remoteSort: true,

					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Question Name', width: 100, dataIndex: 'feedbackQuestion.name', hidden: true, filter: {searchFieldName: 'feedbackQuestionId', type: 'combo', url: 'portalFeedbackQuestionListFind.json'}},
						{header: 'Question Text', width: 100, dataIndex: 'feedbackQuestion.questionText', hidden: true, filter: {searchFieldName: 'feedbackQuestionText'}},
						{header: 'Result', width: 300, dataIndex: 'resultText'},
						{header: 'Entered By', width: 75, dataIndex: 'securityUser.label', filter: {searchFieldName: 'userSearchPattern'}},
						{header: 'E-mail', width: 75, hidden: true, dataIndex: 'securityUser.username', filter: {searchFieldName: 'username'}},
						{header: 'Company Name', width: 75, dataIndex: 'securityUser.companyName', filter: {searchFieldName: 'userCompanyName'}},
						{header: 'Title', width: 75, hidden: true, dataIndex: 'securityUser.title', filter: {searchFieldName: 'userTitle'}},
						{header: 'Phone Number', width: 75, hidden: true, dataIndex: 'securityUser.phoneNumber', filter: {searchFieldName: 'userPhoneNumber'}},
						{header: 'Entered On', width: 75, dataIndex: 'resultDate'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Question', xtype: 'combo', name: 'feedbackQuestionId', width: 220, url: 'portalFeedbackQuestionListFind.json', linkedFilter: 'feedbackQuestion.name'}
						];
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.portal.feedback.ResultWindow'
					},
					getExportFileName: function() {
						return 'Feedback Results';
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// Default to those that are Entered within the last 30 days
							this.setFilterValue('resultDate', {'after': new Date().add(Date.DAY, -30)});
						}
					}
				}]
			},
			{
				title: 'Questions',
				items: [{
					name: 'portalFeedbackQuestionListFind',
					xtype: 'gridpanel',
					isPagingEnabled: function() {
						return true;
					},
					instructions: 'Portal Feedback Questions can be answered by users at any time to provide feedback on their experience using the Client Portal.',
					additionalPropertiesToRequest: 'parent.id',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{
							header: 'Question Name', width: 100, dataIndex: 'name',
							renderer: function(v, metaData, r) {
								if (!r.json.parent) {
									return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
								}
								return '<div style="FONT-WEIGHT: bold; COLOR: #777777; PADDING-LEFT: 15px">' + v + '</div>';
							}
						},
						{header: 'Question Text', width: 150, dataIndex: 'questionText'},
						{
							header: 'Selectable Options', width: 40, dataIndex: 'selectableOption', type: 'boolean',
							tooltip: 'If checked, then question has available options for user to select as their answer.  Otherwise, the user is allowed to enter free text response.'
						},
						{header: 'Order', width: 20, dataIndex: 'displayOrder', type: 'int'},
						{header: 'Disabled', width: 40, dataIndex: 'disabled', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.portal.feedback.QuestionWindow'
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('disabled', false);
						}
					}
				}]
			}

		]
	}]
});


