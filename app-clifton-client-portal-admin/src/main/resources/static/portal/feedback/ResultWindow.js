Clifton.portal.feedback.ResultWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'Portal Feedback Result',
	iconCls: 'question',

	items: [{
		xtype: 'portal-formpanel',
		url: 'portalFeedbackResult.json',
		readOnly: true,
		labelFieldName: 'feedbackQuestion.name',
		items: [
			{fieldLabel: 'Question', name: 'feedbackQuestion.questionText', xtype: 'linkfield', detailIdField: 'feedbackQuestion.id', detailPageClass: 'Clifton.portal.feedback.QuestionWindow'},
			{fieldLabel: 'Result', name: 'resultText', xtype: 'textarea', height: 50, grow: true},
			{
				xtype: 'formtable',
				columns: 4,
				defaults: {
					width: '95%'
				},
				items: [
					{html: 'Entered By:', cellWidth: '110px'},
					{name: 'securityUser.label', detailIdField: 'securityUser.id', xtype: 'linkfield', detailPageClass: 'Clifton.portal.security.user.UserWindow'},
					{html: 'Entered On:'},
					{name: 'resultDate', xtype: 'displayfield'}
				]
			}
		]
	}]
});
