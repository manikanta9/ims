Clifton.portal.GetPortalFileCurrentViewEntity = function() {
	let portalEntityId = (Ext.getCmp('viewEntity4').getValue());
	if (TCG.isBlank(portalEntityId)) {
		portalEntityId = (Ext.getCmp('viewEntity3').getValue());
	}
	if (TCG.isBlank(portalEntityId)) {
		portalEntityId = (Ext.getCmp('viewEntity2').getValue());
	}
	if (TCG.isBlank(portalEntityId)) {
		portalEntityId = (Ext.getCmp('viewEntity').getValue());
	}
	return portalEntityId;
};

Clifton.portal.GetPortalFileCurrentViewUser = function() {
	return Ext.getCmp('viewAsUser').getValue();
};

Clifton.portal.GetPortalEntityCurrentViewActiveOnly = function() {
	return !TCG.isTrue(Ext.getCmp('includeInactiveViewEntities').getValue());
};


// Overrides Portal API Shared So that view as user/entity parameters are applied
Clifton.portal.file.downloadFiles = function(portalFileIds, zipFiles, componentScope) {
	let url = 'portalFileDownload.json?portalFileId=';
	if (Ext.isArray(portalFileIds)) {
		if (portalFileIds.length > Clifton.portal.file.MaxDownloadCount) {
			TCG.showError('You have selected [' + portalFileIds.length + '] files.  Please limit your selection to ' + Clifton.portal.file.MaxDownloadCount + ' files.', 'Too many files selected');
			return;
		}
		url = 'portalFileListDownload.json?portalFileIds=';
	}
	url += portalFileIds;
	const viewAsUserId = Clifton.portal.GetPortalFileCurrentViewUser();
	if (TCG.isNotBlank(viewAsUserId)) {
		url += '&viewAsUserId=' + viewAsUserId;
	}
	// Note: This is used for Tracking, so we know which entity the user was looking at when they downloaded the file
	const assignedPortalEntityId = Clifton.portal.GetPortalFileCurrentViewEntity();
	if (!TCG.isBlank(assignedPortalEntityId)) {
		url += '&assignedPortalEntityId=' + assignedPortalEntityId;
	}
	if (TCG.isTrue(zipFiles)) {
		url += '&zipFiles=true';
	}
	TCG.downloadFile(url, null, componentScope);
};


Clifton.portal.file.FileCategoryTreePanel = Ext.extend(TCG.tree.AdvancedTreePanel, {
	title: '',
	rootNodeText: 'Folders',
	rootVisible: true,
	width: 275,
	detailPageClass: 'Clifton.portal.file.setup.FileCategoryWindow',
	approvedOnly: true,
	name: 'portalFileCategoryTree',

	getCategoryId: function() {
		const categoryName = this.getWindow().categoryName;
		if (TCG.isNotNull(categoryName)) {
			// FIX TO USE DATA PROMISE
			const result = TCG.data.getData('portalFileCategoryListFind.json?rootOnly=true&categoryNameEquals=' + categoryName, this, 'portal.file.category.' + categoryName);
			if (result && result[0]) {
				return result[0].id;
			}
			return null;
		}
	},

	initComponent: function() {
		let url = 'portalFileCategoryListFind.json?orderBy=orderExpanded';
		if (TCG.isTrue(this.approvedOnly)) {
			url += '&limitToCategoriesWithAvailableApprovedFiles=true';
		}
		else {
			url += '&limitToCategoriesWithAvailableFiles=true';
		}

		const parentId = this.getCategoryId();
		if (TCG.isNotNull(this.getWindow().categoryName)) {
			this.rootNodeText = this.getWindow().categoryName;
		}

		this.loader = new TCG.tree.JsonTreeLoader({
			dataUrl: url,
			qtipNode: 'description',
			getRootParams: function() {
				if (TCG.isNotNull(parentId)) {
					return {parentId: parentId};
				}
				else {
					return {rootOnly: true};
				}
			}
		});
		this.loader.on('beforeload', function(treeLoader, node) {
			treeLoader.baseParams.limitToCategoriesForAssignedPortalEntityId = Clifton.portal.GetPortalFileCurrentViewEntity();
			treeLoader.baseParams.viewAsUserId = Clifton.portal.GetPortalFileCurrentViewUser();
		}, this);
		this.root = {
			nodeType: 'async',
			text: this.rootNodeText
		};

		Clifton.portal.file.FileCategoryTreePanel.superclass.initComponent.call(this);
	},

	afterRender: function() {
		Clifton.portal.file.FileCategoryTreePanel.superclass.afterRender.apply(this, arguments);
		this.expandAll();
	},

	onContextMenu: function(node, e) {
		node.select();
		const tree = node.getOwnerTree();
		if (!(this.contextMenu instanceof Ext.Component)) { // lazy initialization
			this.contextMenu.items = [
				{value: 'rf-reload', text: 'Reload Sub Folders', iconCls: 'table-refresh'},
				{value: 'rf-open', text: 'View Selected Folder', iconCls: 'info'}
			];
			this.contextMenu = tree.createComponent(this.contextMenu, 'menu');
		}
		const c = this.contextMenu;
		if (c) {
			c.contextNode = node;
			// disable info for the root node
			const root = !Ext.isNumber(node.id);
			c.items.get(1).setDisabled(root);
			c.showAt(e.getXY());
		}
	},

	// component that has the items managed by these folders: gridpanel, etc.
	getItemComponent: function() {
		return TCG.getChildByName(this.getWindow(), 'portalFileExtendedListFind');
	},

	onNodeSelected: function(id) {
		const tree = this;
		const grid = this.getItemComponent();
		grid.getCategoryParams = function() {
			if (TCG.isNull(id)) {
				const parentId = tree.getCategoryId();
				if (TCG.isNotNull(parentId)) {
					return {fileCategoryIdExpanded: parentId};
				}
				return {};
			}
			return {fileCategoryIdExpanded: id};
		};
		grid.reload();
	}
});
Ext.reg('portal-file-folders', Clifton.portal.file.FileCategoryTreePanel);


// TODO CHANGE ALL GRIDS TO portal-gridpanel xtype and remove (see portal-api-shared for extended class)
// Changes Standard Columns to include Create User/Update User
Clifton.portal.security.GridPanel = Ext.override(TCG.grid.GridPanel, {
	standardColumns: [ // these columns will be added to the end of the grid if corresponding dataIndex fields are defined
		{
			header: 'Created By', width: 30, dataIndex: 'createUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		},
		{header: 'Created On', width: 40, dataIndex: 'createDate', hidden: true},
		{
			header: 'Updated By', width: 30, dataIndex: 'updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		},
		{header: 'Updated On', width: 40, dataIndex: 'updateDate', hidden: true}
	]
});

