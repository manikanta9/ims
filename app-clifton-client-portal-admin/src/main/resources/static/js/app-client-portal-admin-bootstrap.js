// disable accidental Browser back button or refresh for non-local instances
TCG.enableConfirmBeforeLeavingWindow();

TCG.ApplicationName = TCG.PageConfiguration.applicationName;

// NOTE: '*-shared.js' files register all name spaces for corresponding projects
Ext.ns('Clifton.core.errors', 'Clifton.core.stats');

TCG.app.app = new TCG.app.Application({
	northAutoHeight: true,
	getTopBar: function() {
		return {
			html: '<div class="x-panel-header">' +
				'<table cellspacing="0" cellpadding="0" width="100%" class="app-header">' +
				'<tr>' +
				'<td width="23" qtip="Parametric Portal Admin"><img src="core/images/icons/parametric.png" height="16" border="0" /></td>' +
				'<td style="FONT-WEIGHT: bold; COLOR: #646464; FONT-FAMILY: Arial">Parametric Portal Admin</td>' +
				'<td align="right" id="welcomeMessage">Welcome!</td>' +
				'</tr>' +
				'</table>' +
				'</div>'
		};
	},
	getBody: function() {
		let suffix = '';
		const instanceText = TCG.PageConfiguration.instance.text;
		if (TCG.isNotBlank(instanceText)) {
			suffix = `<br /><br /><br /><div style="WIDTH: 100%; TEXT-ALIGN: center; COLOR: ${TCG.PageConfiguration.instance.color}; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: ${TCG.PageConfiguration.instance.size};">${instanceText}</div>`;
		}
		return {
			html: TCG.trimWhitespace(`
				<div style="WIDTH: 100%; TEXT-ALIGN: center; PADDING-TOP: 50px">
					<div><img src="core/images/logo/Parametric-120px-height.png" border="0" /></div>
					<div style="PADDING-TOP: 20px; COLOR: #646464; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 20pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">Parametric Portal Admin</div>
					</div>
					<div style="COLOR: #34abdb; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 16pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">${TCG.PageConfiguration.instance.version}</div>
					</div>
					${suffix}
				</div>
			`)
		};
	},

	reloadPortalFiles: function() {
		this.windows.each(function(w) {
			const grid = TCG.getChildByName(w, 'portalFileExtendedListFind');
			if (grid && grid.reload) {
				grid.reload();
			}
			const treeGrid = TCG.getChildByName(w, 'portalFileCategoryTree');
			if (treeGrid && treeGrid.root && treeGrid.root.reload) {
				treeGrid.root.reload();
				treeGrid.expandAll();
			}
			// User Management
			const userManagementGrid = TCG.getChildByName(w, 'portalSecurityUserAssignmentListFind');
			if (userManagementGrid && userManagementGrid.reload) {
				userManagementGrid.reload();
			}
			// Entity View Types
			const entityViewTypesGrid = TCG.getChildByName(w, 'portalEntityViewTypeListFind');
			if (entityViewTypesGrid && entityViewTypesGrid.reload) {
				entityViewTypesGrid.reload();
			}
			// Page Elements
			const pageElementGrid = TCG.getChildByName(w, 'portalPageElementListFind');
			if (pageElementGrid && pageElementGrid.reload) {
				pageElementGrid.reload();
			}
		});
	},

	getMenuBar: function() {
		const mb = new Ext.Toolbar({height: 120});
		mb.render(Ext.getBody());
		mb.add(
			{
				text: 'Documentation',
				iconCls: 'contract',
				handler: function() {
					const clz = 'Clifton.portal.file.CategoryFileListWindow';
					const cmpId = TCG.getComponentId(clz + ':Documentation', id);
					TCG.createComponent(clz, {
						id: cmpId,
						title: 'Documentation',
						iconCls: 'contract',
						categoryName: 'Documentation',
						openerCt: this
					});
				}
			},

			'-',

			{
				text: 'Reporting',
				iconCls: 'report',
				handler: function() {
					const clz = 'Clifton.portal.file.CategoryFileListWindow';
					const cmpId = TCG.getComponentId(clz + ':Reporting', id);
					TCG.createComponent(clz, {
						id: cmpId,
						title: 'Reporting',
						iconCls: 'report',
						categoryName: 'Reporting',
						openerCt: this
					});
				}
			},

			'-',

			{
				text: 'Invoices',
				iconCls: 'billing',
				handler: function() {
					const clz = 'Clifton.portal.file.CategoryFileListWindow';
					const cmpId = TCG.getComponentId(clz + ':Invoices', id);
					TCG.createComponent(clz, {
						id: cmpId,
						title: 'Invoices',
						categoryName: 'Invoices',
						columnOverrides: [{dataIndex: 'displayText', hidden: false, header: 'Status'}],
						iconCls: 'billing',
						openerCt: this
					});
				}
			},

			'-',

			{
				text: 'Strategy Overview',
				iconCls: 'services',
				handler: function() {
					const clz = 'Clifton.portal.file.CategoryFileListWindow';
					const cmpId = TCG.getComponentId(clz + ':StrategyOverview', id);
					TCG.createComponent(clz, {
						id: cmpId,
						title: 'Strategy Overview',
						categoryName: 'Strategy Overview',
						iconCls: 'services',
						openerCt: this
					});
				}
			},

			'-',

			{
				text: 'Contact Us',
				iconCls: 'people',
				handler: function() {
					const clz = 'Clifton.portal.file.CategoryFileListWindow';
					const cmpId = TCG.getComponentId(clz + ':ContactUs', id);
					TCG.createComponent(clz, {
						id: cmpId,
						title: 'Contact Us',
						categoryName: 'Contact Us',
						hideViewFileButton: true,
						iconCls: 'people',
						openerCt: this
					});
				}
			},

			'-',

			{
				text: 'Help',
				iconCls: 'help',
				handler: function() {
					const clz = 'Clifton.portal.file.CategoryFileListWindow';
					const cmpId = TCG.getComponentId(clz + ':Help', id);
					TCG.createComponent(clz, {
						id: cmpId,
						title: 'Help',
						categoryName: 'Help',
						iconCls: 'help',
						openerCt: this
					});
				}
			},

			'-',

			{
				text: 'Settings',
				iconCls: 'tools',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'My Notifications',
							iconCls: 'email',
							tooltip: 'Use with View As User to see/edit notifications for that user.',
							handler: function() {
								const userId = Clifton.portal.GetPortalFileCurrentViewUser();
								if (TCG.isBlank(userId)) {
									TCG.showError('Please select a View As User from the selection.');
								}
								else {
									const className = 'Clifton.portal.notification.subscription.NotificationSubscriptionEntryWindow';
									const cmpId = TCG.getComponentId(className, userId);
									TCG.createComponent(className, {
										id: cmpId,
										defaultData: {securityUser: {id: userId}}
									});
								}
							}
						},
						{
							text: 'Password Reset',
							iconCls: 'key',
							tooltip: 'Reset your password',
							handler: function() {
								TCG.createComponent('Clifton.portal.security.user.UserPasswordResetWindow');
							}

						},
						'-',
						{
							text: 'User Management',
							iconCls: 'user',
							handler: function() {
								TCG.createComponent('Clifton.portal.security.user.UserAssignmentListWindow');
							}
						},
						'-',
						{
							text: 'View Types',
							iconCls: 'view',
							handler: function() {
								TCG.createComponent('Clifton.portal.entity.setup.EntityViewTypeListWindow');
							}
						},
						{
							text: 'Page Elements',
							iconCls: 'views',
							handler: () => TCG.createComponent('Clifton.portal.page.PageElementListWindow')
						}
					]
				})
			},

			'-',

			{
				text: 'Feedback',
				iconCls: 'question',
				handler: function() {
					TCG.createComponent('Clifton.portal.feedback.FeedbackEntryWindow');
				}
			},

			'-',

			{
				text: 'File Management',
				iconCls: 'disk',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Upload One File',
							iconCls: 'upload',
							handler: function() {
								TCG.createComponent('Clifton.portal.file.FileUploadWindow');
							}
						},
						{
							text: 'Upload Multiple Files',
							iconCls: 'upload',
							handler: function() {
								TCG.createComponent('Clifton.portal.file.FileUploadPendingListWindow');
							}
						},

						'-',
						{
							text: 'Client Portal Management',
							iconCls: 'www',
							handler: function() {
								TCG.createComponent('Clifton.portal.PortalManagementWindow');
							}
						}
					]
				})
			},

			'-',

			{
				text: 'Admin',
				iconCls: 'lock',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Portal Files',
							iconCls: 'disk',
							handler: function() {
								TCG.createComponent('Clifton.portal.file.FileListWindow');
							}
						},

						'-',

						{
							text: 'Portal Entities',
							iconCls: 'list',
							handler: function() {
								TCG.createComponent('Clifton.portal.entity.EntityListWindow');
							}
						},

						{
							text: 'Portal Entity Uploads',
							iconCls: 'import',
							menu: new Ext.menu.Menu({
								items: [
									{
										text: 'Portal Entity Uploads',
										tooltip: 'Upload portal entities (Client Relationships, Clients, etc.) from a source system.',

										handler: function() {
											TCG.createComponent('Clifton.portal.entity.upload.EntityUploadWindow');
										}
									},
									{
										text: 'Portal Entity Rollup Uploads',
										tooltip: 'Upload portal entity rollups, which is the associated between existing entities, like Commingled Vehicles and their investing client accounts.',
										handler: function() {
											TCG.createComponent('Clifton.portal.entity.upload.EntityRollupUploadWindow');
										}
									}
								]
							})
						},

						'-',

						{
							text: 'Feedback Setup',
							iconCls: 'question',
							handler: function() {
								TCG.createComponent('Clifton.portal.feedback.FeedbackSetupWindow');
							}
						},

						'-',

						{
							text: 'Notification Setup',
							iconCls: 'email',
							handler: function() {
								TCG.createComponent('Clifton.portal.notification.setup.NotificationSetupWindow');
							}
						},

						'-',

						{
							text: 'Security',
							iconCls: 'lock',
							handler: function() {
								TCG.createComponent('Clifton.portal.security.SecuritySetupWindow');
							}
						},
						'-',

						{
							text: 'Event Tracking',
							iconCls: 'event',
							handler: function() {
								TCG.createComponent('Clifton.portal.tracking.TrackingSetupWindow');
							}
						},
						'-',
						{
							text: 'Centralized Logging',
							iconCls: 'explorer',
							handler: function() {
								const config = {
									additionalTabs: Clifton.portal.logging.LoggingAdditionalTabs,
									application: 'Portal',
									graylogUrl: `${TCG.PageConfiguration.graylog.apiScheme}://${TCG.PageConfiguration.graylog.apiHost}`
								};
								TCG.createComponent('Clifton.core.logging.LogMessageListWindow', config);
							}
						},
						'-',
						{
							text: 'System Access Stats',
							iconCls: 'timer',
							handler: function() {
								TCG.createComponent('Clifton.portal.PortalStatsListWindow');
							}
						}]
				})
			},

			'-',
			new TCG.app.WindowMenu(this),
			'-',
			{
				text: 'Help',
				menu: {
					items: [
						TCG.app.ContactSupportMenu
					]
				}
			},
			'-',
			{
				text: 'Logout',
				id: 'logoutMenu',
				handler: () => TCG.data.AuthHandler.logout()
			},

			'->'
		);
		mb.add(this.getViewMenuBar(this));
		return mb;

	},
	getViewMenuBar: function(app) {
		const mb = new Ext.Toolbar();
		mb.render(Ext.getBody());

		mb.add(
			{
				xtype: 'buttongroup',
				columns: 1,
				width: 300,
				title: 'View As User',
				id: 'viewAsUserGroup',
				items: [
					{
						xtype: 'combo',
						detailPageClass: 'Clifton.portal.security.user.UserWindow',
						disableAddNewItem: true,
						url: 'portalSecurityUserListFind.json?userRolePortalEntitySpecific=true&disabled=false',
						displayField: 'label',
						name: 'viewAsUser',
						id: 'viewAsUser',
						width: 340,
						emptyText: '< Select User >',
						qtip: 'Select a user to view files and settings as that user.',
						changeValue: function() {
							Ext.getCmp('viewEntity').clearAndReset();
							app.reloadPortalFiles();
						},
						changeValue_set: function() {
							this.enableCheckbox(false);
							this.changeValue();
						},
						changeValue_clear: function() {
							this.enableCheckbox(true);
							this.changeValue();
						},
						enableCheckbox: function(enable) {
							const viewInactive = TCG.getChildByName(this.ownerCt, 'includeInactiveViewEntities');
							if (TCG.isTrue(enable)) {
								viewInactive.setDisabled(false);
							}
							else {
								viewInactive.setValue(false);
								viewInactive.setDisabled(true);
							}
						},
						listeners: {
							afterRender: function() {
								this.setValue = this.setValue.createSequence(this.changeValue_set, this);
								this.clearValue = this.setValue.createSequence(this.changeValue_clear, this);
							}
						}
					},
					{xtype: 'label', ctCls: 'x-btn-group-header', cls: 'x-btn-group-header-text', html: '<br>Additional View Options', qtip: 'Cannot be used when a user is selected.  By Default all searches are limited to active portal entities, which is what the client sees.  In rare cases can look at inactive entities which could be helpful in finding some historical docs needed by request.'},
					{
						boxLabel: 'Include Inactive View Entities',
						xtype: 'checkbox',
						id: 'includeInactiveViewEntities',
						name: 'includeInactiveViewEntities',
						listeners: {
							'check': function(checkbox, checked) {
								// Don't clear selected values, but reset the store for all
								Ext.getCmp('viewEntity').resetStore();
								Ext.getCmp('viewEntity2').resetStore();
								Ext.getCmp('viewEntity3').resetStore();
								Ext.getCmp('viewEntity4').resetStore();
							}
						}
					}
				]
			}
		);

		mb.add(' ');

		mb.add(
			{
				xtype: 'buttongroup',
				columns: 1,
				title: 'View Entity',
				items: [
					{
						xtype: 'combo',
						detailPageClass: 'Clifton.portal.entity.EntityWindow',
						disableAddNewItem: true,
						url: 'portalEntityListFind.json?securableEntityHasApprovedFilesPosted=true&securableEntityLevel=1',
						displayField: 'label',
						name: 'viewEntity',
						id: 'viewEntity',
						width: 350,
						emptyText: '< Select Organization Group >',
						requestedProps: 'entitySourceSection.name|entitySourceSection.description',
						beforequery: function(queryEvent) {
							queryEvent.combo.store.setBaseParam('viewAsUserId', Clifton.portal.GetPortalFileCurrentViewUser());
							const activeOnly = TCG.isTrue(Clifton.portal.GetPortalEntityCurrentViewActiveOnly());
							queryEvent.combo.store.setBaseParam('active', activeOnly ? activeOnly : '');
						},
						changeValue_clear: function() {
							this.changeValue();
						},
						changeValue: function() {
							TCG.getChildByName(this.ownerCt, 'viewEntity2').clearAndReset();
							app.reloadPortalFiles();
						},
						listeners: {
							select: function(combo, record, index) {
								combo.getTipTarget().dom.qtip = TCG.getValue('entitySourceSection.description', record.json);
							},
							afterRender: function() {
								this.setValue = this.setValue.createSequence(this.changeValue, this);
								this.clearValue = this.setValue.createSequence(this.changeValue_clear, this);
							}
						}
					},
					{
						xtype: 'combo',
						detailPageClass: 'Clifton.portal.entity.EntityWindow',
						disableAddNewItem: true,
						url: 'portalEntityListFind.json?securableEntityHasApprovedFilesPosted=true&securableEntityLevel=2',
						displayField: 'label',
						name: 'viewEntity2',
						id: 'viewEntity2',
						width: 350,
						emptyText: '< Select Organization >',
						requestedProps: 'entitySourceSection.name|entitySourceSection.description',
						beforequery: function(queryEvent) {
							const cmb = queryEvent.combo;
							const parentCombo = TCG.getChildByName(cmb.ownerCt, 'viewEntity');
							queryEvent.combo.store.setBaseParam('parentId', parentCombo.getValue());
							queryEvent.combo.store.setBaseParam('viewAsUserId', Clifton.portal.GetPortalFileCurrentViewUser());
							const activeOnly = TCG.isTrue(Clifton.portal.GetPortalEntityCurrentViewActiveOnly());
							queryEvent.combo.store.setBaseParam('active', activeOnly ? activeOnly : '');
						},
						changeValue_clear: function() {
							this.changeValue();
							const viewEntity3Field = TCG.getChildByName(this.ownerCt, 'viewEntity3');
							viewEntity3Field.setDisabled(true);
							viewEntity3Field.setEmptyText('< Select >');
						},
						changeValue: function() {
							TCG.getChildByName(this.ownerCt, 'viewEntity3').clearAndReset();
							app.reloadPortalFiles();
						},
						listeners: {
							select: function(combo, record, index) {
								combo.getTipTarget().dom.qtip = TCG.getValue('entitySourceSection.description', record.json);
								const viewEntity3Field = TCG.getChildByName(this.ownerCt, 'viewEntity3');
								viewEntity3Field.setDisabled(false);
								viewEntity3Field.viewLevel = 3;
								if (TCG.isEquals('Sister Client', TCG.getValue('entitySourceSection.name', record.json))) {
									viewEntity3Field.setEmptyText('< Select Client Account >');
									viewEntity3Field.viewLevel = 4;
								}
								else if (TCG.isEquals('Sister Client Group', TCG.getValue('entitySourceSection.name', record.json))) {
									viewEntity3Field.setEmptyText('< Select Sister Client >');
								}
								else {
									viewEntity3Field.setEmptyText('< Select Client >');
								}
							},
							afterRender: function() {
								this.setValue = this.setValue.createSequence(this.changeValue, this);
								this.clearValue = this.setValue.createSequence(this.changeValue_clear, this);
							}
						}
					},
					{
						xtype: 'combo',
						detailPageClass: 'Clifton.portal.entity.EntityWindow',
						disableAddNewItem: true,
						url: 'portalEntityListFind.json?securableEntityHasApprovedFilesPosted=true&securable=true',
						name: 'viewEntity3',
						id: 'viewEntity3',
						viewLevel: 3,
						disabled: true,
						displayField: 'label',
						requestedProps: 'entitySourceSection.name|entitySourceSection.description',
						width: 350,
						changeValue_clear: function() {
							this.changeValue();
							const viewEntity4Field = TCG.getChildByName(this.ownerCt, 'viewEntity4');
							viewEntity4Field.setDisabled(true);
							viewEntity4Field.setEmptyText('< Select >');
						},
						changeValue: function() {
							TCG.getChildByName(this.ownerCt, 'viewEntity4').clearAndReset();
							app.reloadPortalFiles();
						},
						listeners: {
							select: function(combo, record, index) {
								combo.getTipTarget().dom.qtip = TCG.getValue('entitySourceSection.description', record.json);
								const viewEntity4Field = TCG.getChildByName(this.ownerCt, 'viewEntity4');
								if (TCG.isNotEquals('Client Account', TCG.getValue('entitySourceSection.name', record.json))) {
									viewEntity4Field.setDisabled(false);
									viewEntity4Field.setEmptyText('< Select Client Account >');
								}
								else {
									viewEntity4Field.setEmptyText('< N/A >');
								}
							},
							afterRender: function() {
								this.setValue = this.setValue.createSequence(this.changeValue, this);
								this.clearValue = this.setValue.createSequence(this.changeValue_clear, this);
							}
						},
						beforequery: function(queryEvent) {
							const cmb = queryEvent.combo;
							const parentCombo = TCG.getChildByName(cmb.ownerCt, 'viewEntity2');
							queryEvent.combo.store.setBaseParam('securableEntityLevel', cmb.viewLevel);
							queryEvent.combo.store.setBaseParam('parentId', parentCombo.getValue());
							queryEvent.combo.store.setBaseParam('viewAsUserId', Clifton.portal.GetPortalFileCurrentViewUser());
							const activeOnly = TCG.isTrue(Clifton.portal.GetPortalEntityCurrentViewActiveOnly());
							queryEvent.combo.store.setBaseParam('active', activeOnly ? activeOnly : '');
						}
					},
					{
						xtype: 'combo',
						detailPageClass: 'Clifton.portal.entity.EntityWindow',
						disableAddNewItem: true,
						url: 'portalEntityListFind.json?securableEntityHasApprovedFilesPosted=true&securableEntityLevel=4',
						name: 'viewEntity4',
						id: 'viewEntity4',
						disabled: true,
						displayField: 'label',
						requestedProps: 'entitySourceSection.name|entitySourceSection.description',
						width: 350,
						qtip: 'Client Account is selected parent is a client. Does not apply for Sister Clients',
						changeValue: function() {
							app.reloadPortalFiles();
						},
						listeners: {
							select: function(combo, record, index) {
								combo.getTipTarget().dom.qtip = TCG.getValue('entitySourceSection.description', record.json);
							},
							afterRender: function() {
								this.setValue = this.setValue.createSequence(this.changeValue, this);
								this.clearValue = this.setValue.createSequence(this.changeValue, this);
							}
						},
						beforequery: function(queryEvent) {
							const cmb = queryEvent.combo;
							const parentCombo = TCG.getChildByName(cmb.ownerCt, 'viewEntity3');
							queryEvent.combo.store.setBaseParam('parentId', parentCombo.getValue());

							queryEvent.combo.store.setBaseParam('viewAsUserId', Clifton.portal.GetPortalFileCurrentViewUser());
							const activeOnly = TCG.isTrue(Clifton.portal.GetPortalEntityCurrentViewActiveOnly());
							queryEvent.combo.store.setBaseParam('active', activeOnly ? activeOnly : '');
						}
					}
				]
			}
		);
		return mb;

	}
});

TCG.app.app.addListener('afterload', function() {
	// easy way to distinguish environments
	const theme = TCG.PageConfiguration.instance.theme;
	if (TCG.isNotBlank(theme)) {
		Ext.util.CSS.swapStyleSheet('theme', theme);
	}

	TCG.data.AuthHandler.initLogin('portalSecurityUserCurrent.json', user => {
		Ext.fly('welcomeMessage').update('Welcome ' + user.label);

		// Pre-Load/Cache all users (used for Created By/Updated By columns) - only gets id and label
		const loader = new TCG.data.JsonLoader(Ext.apply({
			onLoad: (records, conf) => {
				if (records && records.length > 0) {
					for (const record of records) {
						TCG.CacheUtils.put('DATA_CACHE', record, `security.user.${record.id}_Application=Internal`);
					}
				}
			},
			// Hide Error Message
			onFailure: () => true // return true to cancel standard processing/error message
		}));
		loader.load('portalSecurityUserListFind.json?requestedPropertiesRoot=data&requestedProperties=id|label');
	});


	// Set the Portal Admin Client UI URL
	Clifton.portal.AdminClientUIUrl = TCG.PageConfiguration.clientPortal.adminUrl;

	const loadWindowClass = TCG.getQueryParams()['loadWindowClass'];
	if (loadWindowClass) {
		const loadWindowParams = TCG.getQueryParams()['loadWindowParams'];
		TCG.createComponent(loadWindowClass, {params: Ext.decode(loadWindowParams)});
	}

	const map = new Ext.KeyMap(document, [{ // disable confirm on F5 to allow explicit browser reload
		key: Ext.EventObject.F5,
		fn: function() {
			TCG.enableConfirmBeforeLeavingWindow(true);
			window.setTimeout(TCG.enableConfirmBeforeLeavingWindow, 1000);
		}
	}]);
	map.stopEvent = false;
});

function menuSelect(a) {
	TCG.showError('Menu item "' + a.text + '" is currently under construction', 'Under Construction');
}
