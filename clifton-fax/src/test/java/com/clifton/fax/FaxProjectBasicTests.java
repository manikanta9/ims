package com.clifton.fax;


import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fax.xmedius.api.Fax;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class FaxProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "fax";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("javax.xml.bind.");
		imports.add("javax.xml.ws.");
		imports.add("javax.xml.datatype.");
		imports.add("javax.xml.namespace.");
		imports.add("javax.xml.JAXBElement");
		imports.add("javax.jws.");
		imports.add("java.net.");
		imports.add("java.math.");
	}


	@Override
	public Set<String> getEnumAllowedNamesOverride() {
		Set<String> names = new HashSet<>();
		names.add("com.clifton.fax.xmedius.api.BannerLocation");
		names.add("com.clifton.fax.xmedius.api.FaxFormat");
		names.add("com.clifton.fax.xmedius.api.FaxResolution");
		names.add("com.clifton.fax.xmedius.api.InboundKeywordProperty");
		names.add("com.clifton.fax.xmedius.api.InboundSortProperty");
		names.add("com.clifton.fax.xmedius.api.InboundStatus");
		names.add("com.clifton.fax.xmedius.api.InboundUserKeywordProperty");
		names.add("com.clifton.fax.xmedius.api.OutboundSortProperty");
		names.add("com.clifton.fax.xmedius.api.OutboundStatus");
		names.add("com.clifton.fax.xmedius.api.OutboundUserKeywordProperty");
		names.add("com.clifton.fax.xmedius.api.OutgoingStatus");
		names.add("com.clifton.fax.xmedius.api.SendingPriority");

		return names;
	}

	@Override
	protected List<Class<?>> getSkipGetSetTestClasses() {
		return CollectionUtils.createList(
				Fax.class // constructor may fail because it connects to: https://mnfax.paraport.com:8443/faxservice/Fax?wsdl
		);
	}

}
