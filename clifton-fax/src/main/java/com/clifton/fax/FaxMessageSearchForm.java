package com.clifton.fax;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;


@NonPersistentObject
@SearchForm(hasOrmDtoClass = false)
public class FaxMessageSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm implements ValidationAware {

	private FaxMessageTypes faxMessageType;

	private Integer errorCode;

	private String faxId;

	private Integer numberOfPages;

	private String recipientName;

	private String recipientNumber;

	private String senderName;

	private String senderNumber;

	private String subject;

	private String status;

	/**
	 * The ID of the sender
	 */
	private String userId;


	////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getFaxMessageType(), "A message type must be specified in order to search for faxes.");
	}

	////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////


	public FaxMessageTypes getFaxMessageType() {
		return this.faxMessageType;
	}


	public void setFaxMessageType(FaxMessageTypes faxMessageType) {
		this.faxMessageType = faxMessageType;
	}


	public Integer getErrorCode() {
		return this.errorCode;
	}


	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}


	public String getFaxId() {
		return this.faxId;
	}


	public void setFaxId(String faxId) {
		this.faxId = faxId;
	}


	public Integer getNumberOfPages() {
		return this.numberOfPages;
	}


	public void setNumberOfPages(Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}


	public String getRecipientName() {
		return this.recipientName;
	}


	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}


	public String getRecipientNumber() {
		return this.recipientNumber;
	}


	public void setRecipientNumber(String recipientNumber) {
		this.recipientNumber = recipientNumber;
	}


	public String getSenderName() {
		return this.senderName;
	}


	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}


	public String getSenderNumber() {
		return this.senderNumber;
	}


	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getStatus() {
		return this.status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getUserId() {
		return this.userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}
}
