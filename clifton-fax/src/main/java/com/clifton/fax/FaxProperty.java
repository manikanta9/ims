package com.clifton.fax;


public class FaxProperty {

	private String faxPropertyName;
	private String faxPropertyValue;


	public FaxProperty() {
		//empty constructor
	}


	public FaxProperty(String propertyName, String propertyValue) {
		this.faxPropertyName = propertyName;
		this.faxPropertyValue = propertyValue;
	}


	public String getFaxPropertyName() {
		return this.faxPropertyName;
	}


	public void setFaxPropertyName(String faxPropertyName) {
		this.faxPropertyName = faxPropertyName;
	}


	public String getFaxPropertyValue() {
		return this.faxPropertyValue;
	}


	public void setFaxPropertyValue(String faxPropertyValue) {
		this.faxPropertyValue = faxPropertyValue;
	}
}
