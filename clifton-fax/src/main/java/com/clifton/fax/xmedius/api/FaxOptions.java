package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaxOptions complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaxOptions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="priority" type="{http://ws.xm.faxserver.com/}SendingPriority" minOccurs="0"/>
 *         &lt;element name="noOfRetries" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="resolution" type="{http://ws.xm.faxserver.com/}FaxResolution" minOccurs="0"/>
 *         &lt;element name="notifyOnSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="notifyOnFailure" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="csid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notifyOnBroadcast" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxOptions", propOrder = {
		"priority",
		"noOfRetries",
		"resolution",
		"notifyOnSuccess",
		"notifyOnFailure",
		"csid",
		"notifyOnBroadcast"
})
@XmlSeeAlso({
		ProfileFaxOptions.class
})
public class FaxOptions {

	@XmlSchemaType(name = "string")
	private SendingPriority priority;
	@XmlSchemaType(name = "unsignedInt")
	private Long noOfRetries;
	@XmlSchemaType(name = "string")
	private FaxResolution resolution;
	private Boolean notifyOnSuccess;
	private Boolean notifyOnFailure;
	private String csid;
	private Boolean notifyOnBroadcast;


	/**
	 * Gets the value of the priority property.
	 *
	 * @return possible object is
	 * {@link SendingPriority }
	 */
	public SendingPriority getPriority() {
		return this.priority;
	}


	/**
	 * Sets the value of the priority property.
	 *
	 * @param value allowed object is
	 *              {@link SendingPriority }
	 */
	public void setPriority(SendingPriority value) {
		this.priority = value;
	}


	/**
	 * Gets the value of the noOfRetries property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getNoOfRetries() {
		return this.noOfRetries;
	}


	/**
	 * Sets the value of the noOfRetries property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setNoOfRetries(Long value) {
		this.noOfRetries = value;
	}


	/**
	 * Gets the value of the resolution property.
	 *
	 * @return possible object is
	 * {@link FaxResolution }
	 */
	public FaxResolution getResolution() {
		return this.resolution;
	}


	/**
	 * Sets the value of the resolution property.
	 *
	 * @param value allowed object is
	 *              {@link FaxResolution }
	 */
	public void setResolution(FaxResolution value) {
		this.resolution = value;
	}


	/**
	 * Gets the value of the notifyOnSuccess property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getNotifyOnSuccess() {
		return this.notifyOnSuccess;
	}


	/**
	 * Sets the value of the notifyOnSuccess property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setNotifyOnSuccess(Boolean value) {
		this.notifyOnSuccess = value;
	}


	/**
	 * Gets the value of the notifyOnFailure property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getNotifyOnFailure() {
		return this.notifyOnFailure;
	}


	/**
	 * Sets the value of the notifyOnFailure property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setNotifyOnFailure(Boolean value) {
		this.notifyOnFailure = value;
	}


	/**
	 * Gets the value of the csid property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCsid() {
		return this.csid;
	}


	/**
	 * Sets the value of the csid property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCsid(String value) {
		this.csid = value;
	}


	/**
	 * Gets the value of the notifyOnBroadcast property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getNotifyOnBroadcast() {
		return this.notifyOnBroadcast;
	}


	/**
	 * Sets the value of the notifyOnBroadcast property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setNotifyOnBroadcast(Boolean value) {
		this.notifyOnBroadcast = value;
	}
}
