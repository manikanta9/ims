package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for UserProfile complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="UserProfile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="senderInfo" type="{http://ws.xm.faxserver.com/}SenderInfo"/>
 *         &lt;element name="defaultCoverSheet" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="coverSheets" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="senderBillingCodes" type="{http://ws.xm.faxserver.com/}BillingGroup"/>
 *         &lt;element name="recipientBillingCodes" type="{http://ws.xm.faxserver.com/}BillingGroup"/>
 *         &lt;element name="faxOptions" type="{http://ws.xm.faxserver.com/}ProfileFaxOptions"/>
 *         &lt;element name="faxSecurityOptions" type="{http://ws.xm.faxserver.com/}FaxSecurityOptions"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserProfile", propOrder = {
		"senderInfo",
		"defaultCoverSheet",
		"coverSheets",
		"senderBillingCodes",
		"recipientBillingCodes",
		"faxOptions",
		"faxSecurityOptions"
})
public class UserProfile {

	@XmlElement(required = true)
	private SenderInfo senderInfo;
	@XmlElement(required = true)
	private String defaultCoverSheet;
	private List<String> coverSheets;
	@XmlElement(required = true)
	private BillingGroup senderBillingCodes;
	@XmlElement(required = true)
	private BillingGroup recipientBillingCodes;
	@XmlElement(required = true)
	private ProfileFaxOptions faxOptions;
	@XmlElement(required = true)
	private FaxSecurityOptions faxSecurityOptions;


	/**
	 * Gets the value of the senderInfo property.
	 *
	 * @return possible object is
	 * {@link SenderInfo }
	 */
	public SenderInfo getSenderInfo() {
		return this.senderInfo;
	}


	/**
	 * Sets the value of the senderInfo property.
	 *
	 * @param value allowed object is
	 *              {@link SenderInfo }
	 */
	public void setSenderInfo(SenderInfo value) {
		this.senderInfo = value;
	}


	/**
	 * Gets the value of the defaultCoverSheet property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getDefaultCoverSheet() {
		return this.defaultCoverSheet;
	}


	/**
	 * Sets the value of the defaultCoverSheet property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setDefaultCoverSheet(String value) {
		this.defaultCoverSheet = value;
	}


	/**
	 * Gets the value of the coverSheets property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the coverSheets property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getCoverSheets().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link String }
	 */
	public List<String> getCoverSheets() {
		if (this.coverSheets == null) {
			this.coverSheets = new ArrayList<>();
		}
		return this.coverSheets;
	}


	/**
	 * Gets the value of the senderBillingCodes property.
	 *
	 * @return possible object is
	 * {@link BillingGroup }
	 */
	public BillingGroup getSenderBillingCodes() {
		return this.senderBillingCodes;
	}


	/**
	 * Sets the value of the senderBillingCodes property.
	 *
	 * @param value allowed object is
	 *              {@link BillingGroup }
	 */
	public void setSenderBillingCodes(BillingGroup value) {
		this.senderBillingCodes = value;
	}


	/**
	 * Gets the value of the recipientBillingCodes property.
	 *
	 * @return possible object is
	 * {@link BillingGroup }
	 */
	public BillingGroup getRecipientBillingCodes() {
		return this.recipientBillingCodes;
	}


	/**
	 * Sets the value of the recipientBillingCodes property.
	 *
	 * @param value allowed object is
	 *              {@link BillingGroup }
	 */
	public void setRecipientBillingCodes(BillingGroup value) {
		this.recipientBillingCodes = value;
	}


	/**
	 * Gets the value of the faxOptions property.
	 *
	 * @return possible object is
	 * {@link ProfileFaxOptions }
	 */
	public ProfileFaxOptions getFaxOptions() {
		return this.faxOptions;
	}


	/**
	 * Sets the value of the faxOptions property.
	 *
	 * @param value allowed object is
	 *              {@link ProfileFaxOptions }
	 */
	public void setFaxOptions(ProfileFaxOptions value) {
		this.faxOptions = value;
	}


	/**
	 * Gets the value of the faxSecurityOptions property.
	 *
	 * @return possible object is
	 * {@link FaxSecurityOptions }
	 */
	public FaxSecurityOptions getFaxSecurityOptions() {
		return this.faxSecurityOptions;
	}


	/**
	 * Sets the value of the faxSecurityOptions property.
	 *
	 * @param value allowed object is
	 *              {@link FaxSecurityOptions }
	 */
	public void setFaxSecurityOptions(FaxSecurityOptions value) {
		this.faxSecurityOptions = value;
	}
}
