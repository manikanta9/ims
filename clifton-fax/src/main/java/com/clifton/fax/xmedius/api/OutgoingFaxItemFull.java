package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutgoingFaxItemFull complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OutgoingFaxItemFull">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}OutgoingFaxItem">
 *       &lt;sequence>
 *         &lt;element name="image" type="{http://ws.xm.faxserver.com/}FaxImage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingFaxItemFull", propOrder = {
		"image"
})
public class OutgoingFaxItemFull
		extends OutgoingFaxItem {

	private FaxImage image;


	/**
	 * Gets the value of the image property.
	 *
	 * @return possible object is
	 * {@link FaxImage }
	 */
	public FaxImage getImage() {
		return this.image;
	}


	/**
	 * Sets the value of the image property.
	 *
	 * @param value allowed object is
	 *              {@link FaxImage }
	 */
	public void setImage(FaxImage value) {
		this.image = value;
	}
}
