package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOutgoingFax complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getOutgoingFax">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="faxId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOutgoingFax", propOrder = {
		"faxId"
})
public class GetOutgoingFax {

	private String faxId;


	/**
	 * Gets the value of the faxId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFaxId() {
		return this.faxId;
	}


	/**
	 * Sets the value of the faxId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setFaxId(String value) {
		this.faxId = value;
	}
}
