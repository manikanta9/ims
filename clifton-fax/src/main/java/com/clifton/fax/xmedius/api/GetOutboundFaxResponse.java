package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOutboundFaxResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getOutboundFaxResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fax" type="{http://ws.xm.faxserver.com/}OutboundUserFaxItemFull" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOutboundFaxResponse", propOrder = {
		"fax"
})
public class GetOutboundFaxResponse {

	private OutboundUserFaxItemFull fax;


	/**
	 * Gets the value of the fax property.
	 *
	 * @return possible object is
	 * {@link OutboundUserFaxItemFull }
	 */
	public OutboundUserFaxItemFull getFax() {
		return this.fax;
	}


	/**
	 * Sets the value of the fax property.
	 *
	 * @param value allowed object is
	 *              {@link OutboundUserFaxItemFull }
	 */
	public void setFax(OutboundUserFaxItemFull value) {
		this.fax = value;
	}
}
