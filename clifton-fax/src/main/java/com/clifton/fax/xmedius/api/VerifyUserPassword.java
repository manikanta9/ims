package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for verifyUserPassword complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="verifyUserPassword">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "verifyUserPassword", propOrder = {
		"userId",
		"password",
		"otp"
})
public class VerifyUserPassword {

	private String userId;
	private String password;
	private String otp;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the password property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getPassword() {
		return this.password;
	}


	/**
	 * Sets the value of the password property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setPassword(String value) {
		this.password = value;
	}


	/**
	 * Gets the value of the otp property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getOtp() {
		return this.otp;
	}


	/**
	 * Sets the value of the otp property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setOtp(String value) {
		this.otp = value;
	}
}
