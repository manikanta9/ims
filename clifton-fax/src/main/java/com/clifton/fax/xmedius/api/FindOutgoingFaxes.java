package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for findOutgoingFaxes complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="findOutgoingFaxes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startIndex" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findOutgoingFaxes", propOrder = {
		"userId",
		"startIndex",
		"maxCount"
})
public class FindOutgoingFaxes {

	private String userId;
	private int startIndex;
	private int maxCount;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the startIndex property.
	 */
	public int getStartIndex() {
		return this.startIndex;
	}


	/**
	 * Sets the value of the startIndex property.
	 */
	public void setStartIndex(int value) {
		this.startIndex = value;
	}


	/**
	 * Gets the value of the maxCount property.
	 */
	public int getMaxCount() {
		return this.maxCount;
	}


	/**
	 * Sets the value of the maxCount property.
	 */
	public void setMaxCount(int value) {
		this.maxCount = value;
	}
}
