package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutboundUserSearchInfo complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OutboundUserSearchInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}OutboundSearchInfo">
 *       &lt;sequence>
 *         &lt;element name="isDeleted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutboundUserSearchInfo", propOrder = {
		"isDeleted",
		"note"
})
public class OutboundUserSearchInfo
		extends OutboundSearchInfo {

	private Boolean isDeleted;
	private String note;


	/**
	 * Gets the value of the isDeleted property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}


	/**
	 * Sets the value of the isDeleted property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setIsDeleted(Boolean value) {
		this.isDeleted = value;
	}


	/**
	 * Gets the value of the note property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getNote() {
		return this.note;
	}


	/**
	 * Sets the value of the note property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setNote(String value) {
		this.note = value;
	}
}
