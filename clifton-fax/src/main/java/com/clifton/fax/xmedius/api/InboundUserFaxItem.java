package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InboundUserFaxItem complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InboundUserFaxItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}InboundFaxItem">
 *       &lt;sequence>
 *         &lt;element name="senderName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="submittedTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="isForwarded" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isViewed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InboundUserFaxItem", propOrder = {
		"senderName",
		"submittedTime",
		"isForwarded",
		"isViewed",
		"note"
})
@XmlSeeAlso({
		InboundUserFaxItemFull.class
})
public class InboundUserFaxItem
		extends InboundFaxItem {

	private String senderName;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar submittedTime;
	private Boolean isForwarded;
	private Boolean isViewed;
	private String note;


	/**
	 * Gets the value of the senderName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSenderName() {
		return this.senderName;
	}


	/**
	 * Sets the value of the senderName property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSenderName(String value) {
		this.senderName = value;
	}


	/**
	 * Gets the value of the submittedTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getSubmittedTime() {
		return this.submittedTime;
	}


	/**
	 * Sets the value of the submittedTime property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setSubmittedTime(XMLGregorianCalendar value) {
		this.submittedTime = value;
	}


	/**
	 * Gets the value of the isForwarded property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getIsForwarded() {
		return this.isForwarded;
	}


	/**
	 * Sets the value of the isForwarded property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setIsForwarded(Boolean value) {
		this.isForwarded = value;
	}


	/**
	 * Gets the value of the isViewed property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getIsViewed() {
		return this.isViewed;
	}


	/**
	 * Sets the value of the isViewed property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setIsViewed(Boolean value) {
		this.isViewed = value;
	}


	/**
	 * Gets the value of the note property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getNote() {
		return this.note;
	}


	/**
	 * Sets the value of the note property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setNote(String value) {
		this.note = value;
	}
}
