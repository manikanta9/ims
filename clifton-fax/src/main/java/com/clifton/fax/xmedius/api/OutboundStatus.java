package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutboundStatus.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OutboundStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSpecified"/>
 *     &lt;enumeration value="Sent"/>
 *     &lt;enumeration value="Failed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "OutboundStatus")
@XmlEnum
public enum OutboundStatus {

	@XmlEnumValue("NotSpecified")
	NOT_SPECIFIED("NotSpecified"),
	@XmlEnumValue("Sent")
	SENT("Sent"),
	@XmlEnumValue("Failed")
	FAILED("Failed");
	private final String value;


	OutboundStatus(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static OutboundStatus fromValue(String v) {
		for (OutboundStatus c : OutboundStatus.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
