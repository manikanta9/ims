package com.clifton.fax.xmedius.search;

/**
 * theodorez
 */
public abstract class XmediusSearch {

	private static final String WILDCARD = "%";

	private String userSearchId = WILDCARD;
	private Integer startIndex;
	private Integer maxCount;


	public String getUserSearchId() {
		return this.userSearchId;
	}


	public void setUserSearchId(String userSearchId) {
		this.userSearchId = userSearchId;
	}


	public Integer getStartIndex() {
		return this.startIndex;
	}


	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}


	public Integer getMaxCount() {
		return this.maxCount;
	}


	public void setMaxCount(Integer maxCount) {
		this.maxCount = maxCount;
	}
}
