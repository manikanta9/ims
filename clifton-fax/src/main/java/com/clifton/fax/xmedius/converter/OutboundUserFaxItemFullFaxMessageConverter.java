package com.clifton.fax.xmedius.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.fax.FaxMessage;
import com.clifton.fax.xmedius.api.FaxImage;
import com.clifton.fax.xmedius.api.OutboundStatus;
import com.clifton.fax.xmedius.api.OutboundUserFaxItemFull;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;

import java.util.HashSet;
import java.util.Set;


public class OutboundUserFaxItemFullFaxMessageConverter implements Converter<OutboundUserFaxItemFull, FaxMessage> {

	@Override
	public FaxMessage convert(OutboundUserFaxItemFull from) {
		Set<String> propertiesToExclude = new HashSet<>();

		FaxMessage faxMessage = new FaxMessage();
		faxMessage.setFaxServerIdentifier((String) FaxMessageXmediusUtils.getPropertyValue(from, "transactionId", propertiesToExclude));
		faxMessage.setSubject((String) FaxMessageXmediusUtils.getPropertyValue(from, "subject", propertiesToExclude));
		faxMessage.setSenderEmail((String) FaxMessageXmediusUtils.getPropertyValue(from, "userId", propertiesToExclude));
		faxMessage.setRecipientName((String) FaxMessageXmediusUtils.getPropertyValue(from, "recipientName", propertiesToExclude));
		faxMessage.setRecipientNumber((String) FaxMessageXmediusUtils.getPropertyValue(from, "originalDestination", propertiesToExclude));
		faxMessage.setComments((String) FaxMessageXmediusUtils.getPropertyValue(from, "note", propertiesToExclude));
		if (from.getErrorCode() != null && !StringUtils.isEmpty(from.getErrorDescription())) {
			faxMessage.setException(new Exception((String) FaxMessageXmediusUtils.getPropertyValue(from, "errorDescription", propertiesToExclude)));
		}
		faxMessage.setAttachments(FaxMessageXmediusUtils.getAttachments((FaxImage) FaxMessageXmediusUtils.getPropertyValue(from, "image", propertiesToExclude)));
		faxMessage.setStatus(((OutboundStatus) FaxMessageXmediusUtils.getPropertyValue(from, "status", propertiesToExclude)).value());
		faxMessage.setAdditionalProperties(FaxMessageXmediusUtils.getAdditionalPropertiesList(BeanUtils.describe(from), propertiesToExclude));
		return faxMessage;
	}
}
