package com.clifton.fax.xmedius.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.fax.FaxMessage;
import com.clifton.fax.xmedius.api.OutboundStatus;
import com.clifton.fax.xmedius.api.OutboundUserFaxItem;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.HashSet;
import java.util.Set;


/**
 * @author theodorez
 */
public class OutboundUserFaxItemFaxMessageConverter implements Converter<OutboundUserFaxItem, FaxMessage> {


	@Override
	public FaxMessage convert(OutboundUserFaxItem from) {
		Set<String> propertiesToExclude = new HashSet<>();

		FaxMessage faxMessage = new FaxMessage();
		faxMessage.setFaxServerIdentifier((String) FaxMessageXmediusUtils.getPropertyValue(from, "transactionId", propertiesToExclude));
		faxMessage.setSubject((String) FaxMessageXmediusUtils.getPropertyValue(from, "subject", propertiesToExclude));
		faxMessage.setSenderEmail((String) FaxMessageXmediusUtils.getPropertyValue(from, "userId", propertiesToExclude));
		faxMessage.setRecipientName((String) FaxMessageXmediusUtils.getPropertyValue(from, "recipientName", propertiesToExclude));
		faxMessage.setRecipientNumber((String) FaxMessageXmediusUtils.getPropertyValue(from, "originalDestination", propertiesToExclude));
		faxMessage.setComments((String) FaxMessageXmediusUtils.getPropertyValue(from, "note", propertiesToExclude));
		faxMessage.setEndDate(FaxMessageXmediusUtils.getDateFromXmlGregorianCalendar((XMLGregorianCalendar) FaxMessageXmediusUtils.getPropertyValue(from, "completedTime", propertiesToExclude)));
		faxMessage.setNumberOfPages(((Long) FaxMessageXmediusUtils.getPropertyValue(from, "pagesSubmitted", propertiesToExclude)).intValue());
		faxMessage.setStartDate(FaxMessageXmediusUtils.getDateFromXmlGregorianCalendar((XMLGregorianCalendar) FaxMessageXmediusUtils.getPropertyValue(from, "submittedTime", propertiesToExclude)));
		if (from.getErrorCode() != null && !StringUtils.isEmpty(from.getErrorDescription())) {
			faxMessage.setException(new Exception((String) FaxMessageXmediusUtils.getPropertyValue(from, "errorDescription", propertiesToExclude)));
		}
		faxMessage.setStatus(((OutboundStatus) FaxMessageXmediusUtils.getPropertyValue(from, "status", propertiesToExclude)).value());
		faxMessage.setAdditionalProperties(FaxMessageXmediusUtils.getAdditionalPropertiesList(BeanUtils.describe(from), propertiesToExclude));
		return faxMessage;
	}
}
