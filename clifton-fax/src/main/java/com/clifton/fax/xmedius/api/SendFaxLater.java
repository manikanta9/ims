package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for sendFaxLater complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="sendFaxLater">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipient" type="{http://ws.xm.faxserver.com/}FaxRecipient" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="attachment" type="{http://ws.xm.faxserver.com/}AttachedFile" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="coverPage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderInfo" type="{http://ws.xm.faxserver.com/}SenderInfo" minOccurs="0"/>
 *         &lt;element name="faxOptions" type="{http://ws.xm.faxserver.com/}FaxOptions" minOccurs="0"/>
 *         &lt;element name="delayUntil" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendFaxLater", propOrder = {
		"userId",
		"subject",
		"comment",
		"recipient",
		"attachment",
		"coverPage",
		"senderInfo",
		"faxOptions",
		"delayUntil"
})
public class SendFaxLater {

	private String userId;
	private String subject;
	private String comment;
	private List<FaxRecipient> recipient;
	private List<AttachedFile> attachment;
	private String coverPage;
	private SenderInfo senderInfo;
	private FaxOptions faxOptions;
	@XmlSchemaType(name = "anySimpleType")
	private Object delayUntil;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the subject property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSubject() {
		return this.subject;
	}


	/**
	 * Sets the value of the subject property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSubject(String value) {
		this.subject = value;
	}


	/**
	 * Gets the value of the comment property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getComment() {
		return this.comment;
	}


	/**
	 * Sets the value of the comment property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setComment(String value) {
		this.comment = value;
	}


	/**
	 * Gets the value of the recipient property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the recipient property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getRecipient().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link FaxRecipient }
	 */
	public List<FaxRecipient> getRecipient() {
		if (this.recipient == null) {
			this.recipient = new ArrayList<>();
		}
		return this.recipient;
	}


	/**
	 * Gets the value of the attachment property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the attachment property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getAttachment().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link AttachedFile }
	 */
	public List<AttachedFile> getAttachment() {
		if (this.attachment == null) {
			this.attachment = new ArrayList<>();
		}
		return this.attachment;
	}


	/**
	 * Gets the value of the coverPage property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCoverPage() {
		return this.coverPage;
	}


	/**
	 * Sets the value of the coverPage property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCoverPage(String value) {
		this.coverPage = value;
	}


	/**
	 * Gets the value of the senderInfo property.
	 *
	 * @return possible object is
	 * {@link SenderInfo }
	 */
	public SenderInfo getSenderInfo() {
		return this.senderInfo;
	}


	/**
	 * Sets the value of the senderInfo property.
	 *
	 * @param value allowed object is
	 *              {@link SenderInfo }
	 */
	public void setSenderInfo(SenderInfo value) {
		this.senderInfo = value;
	}


	/**
	 * Gets the value of the faxOptions property.
	 *
	 * @return possible object is
	 * {@link FaxOptions }
	 */
	public FaxOptions getFaxOptions() {
		return this.faxOptions;
	}


	/**
	 * Sets the value of the faxOptions property.
	 *
	 * @param value allowed object is
	 *              {@link FaxOptions }
	 */
	public void setFaxOptions(FaxOptions value) {
		this.faxOptions = value;
	}


	/**
	 * Gets the value of the delayUntil property.
	 *
	 * @return possible object is
	 * {@link Object }
	 */
	public Object getDelayUntil() {
		return this.delayUntil;
	}


	/**
	 * Sets the value of the delayUntil property.
	 *
	 * @param value allowed object is
	 *              {@link Object }
	 */
	public void setDelayUntil(Object value) {
		this.delayUntil = value;
	}
}
