package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutboundUserKeywordProperty.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OutboundUserKeywordProperty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="SenderBillingCode"/>
 *     &lt;enumeration value="ModifiedDestination"/>
 *     &lt;enumeration value="ErrorCode"/>
 *     &lt;enumeration value="RecipientName"/>
 *     &lt;enumeration value="RemoteCsid"/>
 *     &lt;enumeration value="Subject"/>
 *     &lt;enumeration value="TransactionId"/>
 *     &lt;enumeration value="OriginalTransactionId"/>
 *     &lt;enumeration value="BroadcastId"/>
 *     &lt;enumeration value="UserId"/>
 *     &lt;enumeration value="Note"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "OutboundUserKeywordProperty")
@XmlEnum
public enum OutboundUserKeywordProperty {

	@XmlEnumValue("All")
	ALL("All"),
	@XmlEnumValue("SenderBillingCode")
	SENDER_BILLING_CODE("SenderBillingCode"),
	@XmlEnumValue("ModifiedDestination")
	MODIFIED_DESTINATION("ModifiedDestination"),
	@XmlEnumValue("ErrorCode")
	ERROR_CODE("ErrorCode"),
	@XmlEnumValue("RecipientName")
	RECIPIENT_NAME("RecipientName"),
	@XmlEnumValue("RemoteCsid")
	REMOTE_CSID("RemoteCsid"),
	@XmlEnumValue("Subject")
	SUBJECT("Subject"),
	@XmlEnumValue("TransactionId")
	TRANSACTION_ID("TransactionId"),
	@XmlEnumValue("OriginalTransactionId")
	ORIGINAL_TRANSACTION_ID("OriginalTransactionId"),
	@XmlEnumValue("BroadcastId")
	BROADCAST_ID("BroadcastId"),
	@XmlEnumValue("UserId")
	USER_ID("UserId"),
	@XmlEnumValue("Note")
	NOTE("Note");
	private final String value;


	OutboundUserKeywordProperty(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static OutboundUserKeywordProperty fromValue(String v) {
		for (OutboundUserKeywordProperty c : OutboundUserKeywordProperty.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
