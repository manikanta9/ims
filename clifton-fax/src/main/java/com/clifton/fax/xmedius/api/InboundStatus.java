package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InboundStatus.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InboundStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSpecified"/>
 *     &lt;enumeration value="Received"/>
 *     &lt;enumeration value="Failed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "InboundStatus")
@XmlEnum
public enum InboundStatus {

	@XmlEnumValue("NotSpecified")
	NOT_SPECIFIED("NotSpecified"),
	@XmlEnumValue("Received")
	RECEIVED("Received"),
	@XmlEnumValue("Failed")
	FAILED("Failed");
	private final String value;


	InboundStatus(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static InboundStatus fromValue(String v) {
		for (InboundStatus c : InboundStatus.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
