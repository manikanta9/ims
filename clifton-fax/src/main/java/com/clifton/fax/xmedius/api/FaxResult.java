package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for FaxResult complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaxResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="broadcastId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxResult", propOrder = {
		"broadcastId",
		"transactionId"
})
public class FaxResult {

	@XmlElement(required = true)
	private String broadcastId;
	@XmlElement(required = true)
	private List<String> transactionId;


	/**
	 * Gets the value of the broadcastId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBroadcastId() {
		return this.broadcastId;
	}


	/**
	 * Sets the value of the broadcastId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setBroadcastId(String value) {
		this.broadcastId = value;
	}


	/**
	 * Gets the value of the transactionId property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the transactionId property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getTransactionId().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link String }
	 */
	public List<String> getTransactionId() {
		if (this.transactionId == null) {
			this.transactionId = new ArrayList<>();
		}
		return this.transactionId;
	}
}
