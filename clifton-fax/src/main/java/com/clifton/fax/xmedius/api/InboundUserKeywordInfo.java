package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for InboundUserKeywordInfo complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InboundUserKeywordInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}KeywordInfo">
 *       &lt;sequence>
 *         &lt;element name="property" type="{http://ws.xm.faxserver.com/}InboundUserKeywordProperty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InboundUserKeywordInfo", propOrder = {
		"property"
})
public class InboundUserKeywordInfo
		extends KeywordInfo {

	@XmlElement(nillable = true)
	@XmlSchemaType(name = "string")
	private List<InboundUserKeywordProperty> property;


	/**
	 * Gets the value of the property property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the property property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getProperty().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link InboundUserKeywordProperty }
	 */
	public List<InboundUserKeywordProperty> getProperty() {
		if (this.property == null) {
			this.property = new ArrayList<>();
		}
		return this.property;
	}
}
