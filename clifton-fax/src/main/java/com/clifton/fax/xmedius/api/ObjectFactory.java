package com.clifton.fax.xmedius.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.clifton.fax.xmedius.api2 package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

	private final static QName _FindOutboundFaxesResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutboundFaxesResponse");
	private final static QName _GetOutboundFaxAdvanced_QNAME = new QName("http://ws.xm.faxserver.com/", "getOutboundFaxAdvanced");
	private final static QName _MarkAsViewed_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsViewed");
	private final static QName _GetOutboundFax_QNAME = new QName("http://ws.xm.faxserver.com/", "getOutboundFax");
	private final static QName _DeletePermanentlyResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "deletePermanentlyResponse");
	private final static QName _GetOutgoingFax_QNAME = new QName("http://ws.xm.faxserver.com/", "getOutgoingFax");
	private final static QName _GetPersonsInGroupRecursiveResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getPersonsInGroupRecursiveResponse");
	private final static QName _SendFaxLaterResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "sendFaxLaterResponse");
	private final static QName _FindContactsResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "findContactsResponse");
	private final static QName _GetInboundFaxAdvanced_QNAME = new QName("http://ws.xm.faxserver.com/", "getInboundFaxAdvanced");
	private final static QName _FindInboundFaxes_QNAME = new QName("http://ws.xm.faxserver.com/", "findInboundFaxes");
	private final static QName _FindOutboundFaxes_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutboundFaxes");
	private final static QName _FindOutgoingFaxesResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutgoingFaxesResponse");
	private final static QName _GetOutboundFaxAdvancedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getOutboundFaxAdvancedResponse");
	private final static QName _FaxException_QNAME = new QName("http://ws.xm.faxserver.com/", "FaxException");
	private final static QName _FindOutgoingFaxesAdvancedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutgoingFaxesAdvancedResponse");
	private final static QName _CancelBroadcastResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "cancelBroadcastResponse");
	private final static QName _FindOutgoingFaxes_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutgoingFaxes");
	private final static QName _SetUserData_QNAME = new QName("http://ws.xm.faxserver.com/", "setUserData");
	private final static QName _AddContactResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "addContactResponse");
	private final static QName _GetInboundFaxResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getInboundFaxResponse");
	private final static QName _Cancel_QNAME = new QName("http://ws.xm.faxserver.com/", "cancel");
	private final static QName _FindInboundFaxesAdvanced_QNAME = new QName("http://ws.xm.faxserver.com/", "findInboundFaxesAdvanced");
	private final static QName _FindOutgoingFaxesAdvanced_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutgoingFaxesAdvanced");
	private final static QName _GetOutboundFaxResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getOutboundFaxResponse");
	private final static QName _ResubmitToResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "resubmitToResponse");
	private final static QName _ResubmitLaterResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "resubmitLaterResponse");
	private final static QName _VerifyUserPassword_QNAME = new QName("http://ws.xm.faxserver.com/", "verifyUserPassword");
	private final static QName _GetAuditLogs_QNAME = new QName("http://ws.xm.faxserver.com/", "getAuditLogs");
	private final static QName _SendFaxResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "sendFaxResponse");
	private final static QName _GetInboundFaxAdvancedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getInboundFaxAdvancedResponse");
	private final static QName _MarkAsDeletedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsDeletedResponse");
	private final static QName _GetVersion_QNAME = new QName("http://ws.xm.faxserver.com/", "getVersion");
	private final static QName _GetUserDataResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getUserDataResponse");
	private final static QName _ResubmitTo_QNAME = new QName("http://ws.xm.faxserver.com/", "resubmitTo");
	private final static QName _GetInboundFax_QNAME = new QName("http://ws.xm.faxserver.com/", "getInboundFax");
	private final static QName _CancelBroadcast_QNAME = new QName("http://ws.xm.faxserver.com/", "cancelBroadcast");
	private final static QName _FindInboundFaxesResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "findInboundFaxesResponse");
	private final static QName _ResubmitResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "resubmitResponse");
	private final static QName _FindOutboundFaxesAdvancedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutboundFaxesAdvancedResponse");
	private final static QName _SetUserDataResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "setUserDataResponse");
	private final static QName _FindInboundFaxesAdvancedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "findInboundFaxesAdvancedResponse");
	private final static QName _MarkAsUnviewed_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsUnviewed");
	private final static QName _GetOutgoingFaxResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getOutgoingFaxResponse");
	private final static QName _FindOutboundFaxesAdvanced_QNAME = new QName("http://ws.xm.faxserver.com/", "findOutboundFaxesAdvanced");
	private final static QName _MarkAsDeleted_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsDeleted");
	private final static QName _SendFax_QNAME = new QName("http://ws.xm.faxserver.com/", "sendFax");
	private final static QName _GetUserData_QNAME = new QName("http://ws.xm.faxserver.com/", "getUserData");
	private final static QName _DeletePermanently_QNAME = new QName("http://ws.xm.faxserver.com/", "deletePermanently");
	private final static QName _Resubmit_QNAME = new QName("http://ws.xm.faxserver.com/", "resubmit");
	private final static QName _SaveNote_QNAME = new QName("http://ws.xm.faxserver.com/", "saveNote");
	private final static QName _CancelResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "cancelResponse");
	private final static QName _MarkAsUndeleted_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsUndeleted");
	private final static QName _MarkAsUndeletedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsUndeletedResponse");
	private final static QName _VerifyUserPasswordResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "verifyUserPasswordResponse");
	private final static QName _GetVersionResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getVersionResponse");
	private final static QName _SaveNoteResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "saveNoteResponse");
	private final static QName _AddContact_QNAME = new QName("http://ws.xm.faxserver.com/", "addContact");
	private final static QName _FindContacts_QNAME = new QName("http://ws.xm.faxserver.com/", "findContacts");
	private final static QName _ResubmitLater_QNAME = new QName("http://ws.xm.faxserver.com/", "resubmitLater");
	private final static QName _GetUserProfile_QNAME = new QName("http://ws.xm.faxserver.com/", "getUserProfile");
	private final static QName _SendFaxLater_QNAME = new QName("http://ws.xm.faxserver.com/", "sendFaxLater");
	private final static QName _MarkAsViewedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsViewedResponse");
	private final static QName _RetrySendingNow_QNAME = new QName("http://ws.xm.faxserver.com/", "retrySendingNow");
	private final static QName _GetPersonsInGroupRecursive_QNAME = new QName("http://ws.xm.faxserver.com/", "getPersonsInGroupRecursive");
	private final static QName _MarkAsUnviewedResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "markAsUnviewedResponse");
	private final static QName _GetAuditLogsResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getAuditLogsResponse");
	private final static QName _GetUserProfileResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "getUserProfileResponse");
	private final static QName _RetrySendingNowResponse_QNAME = new QName("http://ws.xm.faxserver.com/", "retrySendingNowResponse");
	private final static QName _ProfileFaxOptionsMaxResolution_QNAME = new QName("", "maxResolution");
	private final static QName _ProfileFaxOptionsMaxPriority_QNAME = new QName("", "maxPriority");
	private final static QName _ProfileFaxOptionsNotifyOnFailure_QNAME = new QName("", "notifyOnFailure");
	private final static QName _ProfileFaxOptionsNotifyOnSuccess_QNAME = new QName("", "notifyOnSuccess");
	private final static QName _ProfileFaxOptionsMaxNoOfRetries_QNAME = new QName("", "maxNoOfRetries");


	/**
	 * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.clifton.fax.xmedius.api
	 */
	public ObjectFactory() {
	}


	/**
	 * Create an instance of {@link Cancel }
	 */
	public Cancel createCancel() {
		return new Cancel();
	}


	/**
	 * Create an instance of {@link AddContactResponse }
	 */
	public AddContactResponse createAddContactResponse() {
		return new AddContactResponse();
	}


	/**
	 * Create an instance of {@link GetInboundFaxResponse }
	 */
	public GetInboundFaxResponse createGetInboundFaxResponse() {
		return new GetInboundFaxResponse();
	}


	/**
	 * Create an instance of {@link FindInboundFaxesAdvanced }
	 */
	public FindInboundFaxesAdvanced createFindInboundFaxesAdvanced() {
		return new FindInboundFaxesAdvanced();
	}


	/**
	 * Create an instance of {@link FindOutgoingFaxesAdvanced }
	 */
	public FindOutgoingFaxesAdvanced createFindOutgoingFaxesAdvanced() {
		return new FindOutgoingFaxesAdvanced();
	}


	/**
	 * Create an instance of {@link GetOutboundFaxResponse }
	 */
	public GetOutboundFaxResponse createGetOutboundFaxResponse() {
		return new GetOutboundFaxResponse();
	}


	/**
	 * Create an instance of {@link ResubmitToResponse }
	 */
	public ResubmitToResponse createResubmitToResponse() {
		return new ResubmitToResponse();
	}


	/**
	 * Create an instance of {@link ResubmitLaterResponse }
	 */
	public ResubmitLaterResponse createResubmitLaterResponse() {
		return new ResubmitLaterResponse();
	}


	/**
	 * Create an instance of {@link VerifyUserPassword }
	 */
	public VerifyUserPassword createVerifyUserPassword() {
		return new VerifyUserPassword();
	}


	/**
	 * Create an instance of {@link SendFaxResponse }
	 */
	public SendFaxResponse createSendFaxResponse() {
		return new SendFaxResponse();
	}


	/**
	 * Create an instance of {@link GetAuditLogs }
	 */
	public GetAuditLogs createGetAuditLogs() {
		return new GetAuditLogs();
	}


	/**
	 * Create an instance of {@link GetInboundFaxAdvancedResponse }
	 */
	public GetInboundFaxAdvancedResponse createGetInboundFaxAdvancedResponse() {
		return new GetInboundFaxAdvancedResponse();
	}


	/**
	 * Create an instance of {@link MarkAsDeletedResponse }
	 */
	public MarkAsDeletedResponse createMarkAsDeletedResponse() {
		return new MarkAsDeletedResponse();
	}


	/**
	 * Create an instance of {@link FindOutboundFaxesResponse }
	 */
	public FindOutboundFaxesResponse createFindOutboundFaxesResponse() {
		return new FindOutboundFaxesResponse();
	}


	/**
	 * Create an instance of {@link MarkAsViewed }
	 */
	public MarkAsViewed createMarkAsViewed() {
		return new MarkAsViewed();
	}


	/**
	 * Create an instance of {@link GetOutboundFaxAdvanced }
	 */
	public GetOutboundFaxAdvanced createGetOutboundFaxAdvanced() {
		return new GetOutboundFaxAdvanced();
	}


	/**
	 * Create an instance of {@link GetOutboundFax }
	 */
	public GetOutboundFax createGetOutboundFax() {
		return new GetOutboundFax();
	}


	/**
	 * Create an instance of {@link DeletePermanentlyResponse }
	 */
	public DeletePermanentlyResponse createDeletePermanentlyResponse() {
		return new DeletePermanentlyResponse();
	}


	/**
	 * Create an instance of {@link GetOutgoingFax }
	 */
	public GetOutgoingFax createGetOutgoingFax() {
		return new GetOutgoingFax();
	}


	/**
	 * Create an instance of {@link GetPersonsInGroupRecursiveResponse }
	 */
	public GetPersonsInGroupRecursiveResponse createGetPersonsInGroupRecursiveResponse() {
		return new GetPersonsInGroupRecursiveResponse();
	}


	/**
	 * Create an instance of {@link SendFaxLaterResponse }
	 */
	public SendFaxLaterResponse createSendFaxLaterResponse() {
		return new SendFaxLaterResponse();
	}


	/**
	 * Create an instance of {@link GetInboundFaxAdvanced }
	 */
	public GetInboundFaxAdvanced createGetInboundFaxAdvanced() {
		return new GetInboundFaxAdvanced();
	}


	/**
	 * Create an instance of {@link FindContactsResponse }
	 */
	public FindContactsResponse createFindContactsResponse() {
		return new FindContactsResponse();
	}


	/**
	 * Create an instance of {@link FindOutboundFaxes }
	 */
	public FindOutboundFaxes createFindOutboundFaxes() {
		return new FindOutboundFaxes();
	}


	/**
	 * Create an instance of {@link FindOutgoingFaxesResponse }
	 */
	public FindOutgoingFaxesResponse createFindOutgoingFaxesResponse() {
		return new FindOutgoingFaxesResponse();
	}


	/**
	 * Create an instance of {@link GetOutboundFaxAdvancedResponse }
	 */
	public GetOutboundFaxAdvancedResponse createGetOutboundFaxAdvancedResponse() {
		return new GetOutboundFaxAdvancedResponse();
	}


	/**
	 * Create an instance of {@link FindInboundFaxes }
	 */
	public FindInboundFaxes createFindInboundFaxes() {
		return new FindInboundFaxes();
	}


	/**
	 * Create an instance of {@link FindOutgoingFaxesAdvancedResponse }
	 */
	public FindOutgoingFaxesAdvancedResponse createFindOutgoingFaxesAdvancedResponse() {
		return new FindOutgoingFaxesAdvancedResponse();
	}


	/**
	 * Create an instance of {@link FaxException }
	 */
	public FaxException createFaxException() {
		return new FaxException();
	}


	/**
	 * Create an instance of {@link CancelBroadcastResponse }
	 */
	public CancelBroadcastResponse createCancelBroadcastResponse() {
		return new CancelBroadcastResponse();
	}


	/**
	 * Create an instance of {@link FindOutgoingFaxes }
	 */
	public FindOutgoingFaxes createFindOutgoingFaxes() {
		return new FindOutgoingFaxes();
	}


	/**
	 * Create an instance of {@link SetUserData }
	 */
	public SetUserData createSetUserData() {
		return new SetUserData();
	}


	/**
	 * Create an instance of {@link CancelResponse }
	 */
	public CancelResponse createCancelResponse() {
		return new CancelResponse();
	}


	/**
	 * Create an instance of {@link MarkAsUndeleted }
	 */
	public MarkAsUndeleted createMarkAsUndeleted() {
		return new MarkAsUndeleted();
	}


	/**
	 * Create an instance of {@link VerifyUserPasswordResponse }
	 */
	public VerifyUserPasswordResponse createVerifyUserPasswordResponse() {
		return new VerifyUserPasswordResponse();
	}


	/**
	 * Create an instance of {@link MarkAsUndeletedResponse }
	 */
	public MarkAsUndeletedResponse createMarkAsUndeletedResponse() {
		return new MarkAsUndeletedResponse();
	}


	/**
	 * Create an instance of {@link SaveNoteResponse }
	 */
	public SaveNoteResponse createSaveNoteResponse() {
		return new SaveNoteResponse();
	}


	/**
	 * Create an instance of {@link GetVersionResponse }
	 */
	public GetVersionResponse createGetVersionResponse() {
		return new GetVersionResponse();
	}


	/**
	 * Create an instance of {@link FindContacts }
	 */
	public FindContacts createFindContacts() {
		return new FindContacts();
	}


	/**
	 * Create an instance of {@link AddContact }
	 */
	public AddContact createAddContact() {
		return new AddContact();
	}


	/**
	 * Create an instance of {@link ResubmitLater }
	 */
	public ResubmitLater createResubmitLater() {
		return new ResubmitLater();
	}


	/**
	 * Create an instance of {@link GetUserProfile }
	 */
	public GetUserProfile createGetUserProfile() {
		return new GetUserProfile();
	}


	/**
	 * Create an instance of {@link SendFaxLater }
	 */
	public SendFaxLater createSendFaxLater() {
		return new SendFaxLater();
	}


	/**
	 * Create an instance of {@link MarkAsViewedResponse }
	 */
	public MarkAsViewedResponse createMarkAsViewedResponse() {
		return new MarkAsViewedResponse();
	}


	/**
	 * Create an instance of {@link RetrySendingNow }
	 */
	public RetrySendingNow createRetrySendingNow() {
		return new RetrySendingNow();
	}


	/**
	 * Create an instance of {@link MarkAsUnviewedResponse }
	 */
	public MarkAsUnviewedResponse createMarkAsUnviewedResponse() {
		return new MarkAsUnviewedResponse();
	}


	/**
	 * Create an instance of {@link GetPersonsInGroupRecursive }
	 */
	public GetPersonsInGroupRecursive createGetPersonsInGroupRecursive() {
		return new GetPersonsInGroupRecursive();
	}


	/**
	 * Create an instance of {@link GetUserProfileResponse }
	 */
	public GetUserProfileResponse createGetUserProfileResponse() {
		return new GetUserProfileResponse();
	}


	/**
	 * Create an instance of {@link RetrySendingNowResponse }
	 */
	public RetrySendingNowResponse createRetrySendingNowResponse() {
		return new RetrySendingNowResponse();
	}


	/**
	 * Create an instance of {@link GetAuditLogsResponse }
	 */
	public GetAuditLogsResponse createGetAuditLogsResponse() {
		return new GetAuditLogsResponse();
	}


	/**
	 * Create an instance of {@link GetVersion }
	 */
	public GetVersion createGetVersion() {
		return new GetVersion();
	}


	/**
	 * Create an instance of {@link ResubmitTo }
	 */
	public ResubmitTo createResubmitTo() {
		return new ResubmitTo();
	}


	/**
	 * Create an instance of {@link GetUserDataResponse }
	 */
	public GetUserDataResponse createGetUserDataResponse() {
		return new GetUserDataResponse();
	}


	/**
	 * Create an instance of {@link GetInboundFax }
	 */
	public GetInboundFax createGetInboundFax() {
		return new GetInboundFax();
	}


	/**
	 * Create an instance of {@link CancelBroadcast }
	 */
	public CancelBroadcast createCancelBroadcast() {
		return new CancelBroadcast();
	}


	/**
	 * Create an instance of {@link FindOutboundFaxesAdvancedResponse }
	 */
	public FindOutboundFaxesAdvancedResponse createFindOutboundFaxesAdvancedResponse() {
		return new FindOutboundFaxesAdvancedResponse();
	}


	/**
	 * Create an instance of {@link SetUserDataResponse }
	 */
	public SetUserDataResponse createSetUserDataResponse() {
		return new SetUserDataResponse();
	}


	/**
	 * Create an instance of {@link FindInboundFaxesResponse }
	 */
	public FindInboundFaxesResponse createFindInboundFaxesResponse() {
		return new FindInboundFaxesResponse();
	}


	/**
	 * Create an instance of {@link ResubmitResponse }
	 */
	public ResubmitResponse createResubmitResponse() {
		return new ResubmitResponse();
	}


	/**
	 * Create an instance of {@link MarkAsUnviewed }
	 */
	public MarkAsUnviewed createMarkAsUnviewed() {
		return new MarkAsUnviewed();
	}


	/**
	 * Create an instance of {@link FindInboundFaxesAdvancedResponse }
	 */
	public FindInboundFaxesAdvancedResponse createFindInboundFaxesAdvancedResponse() {
		return new FindInboundFaxesAdvancedResponse();
	}


	/**
	 * Create an instance of {@link GetOutgoingFaxResponse }
	 */
	public GetOutgoingFaxResponse createGetOutgoingFaxResponse() {
		return new GetOutgoingFaxResponse();
	}


	/**
	 * Create an instance of {@link FindOutboundFaxesAdvanced }
	 */
	public FindOutboundFaxesAdvanced createFindOutboundFaxesAdvanced() {
		return new FindOutboundFaxesAdvanced();
	}


	/**
	 * Create an instance of {@link MarkAsDeleted }
	 */
	public MarkAsDeleted createMarkAsDeleted() {
		return new MarkAsDeleted();
	}


	/**
	 * Create an instance of {@link SendFax }
	 */
	public SendFax createSendFax() {
		return new SendFax();
	}


	/**
	 * Create an instance of {@link GetUserData }
	 */
	public GetUserData createGetUserData() {
		return new GetUserData();
	}


	/**
	 * Create an instance of {@link Resubmit }
	 */
	public Resubmit createResubmit() {
		return new Resubmit();
	}


	/**
	 * Create an instance of {@link SaveNote }
	 */
	public SaveNote createSaveNote() {
		return new SaveNote();
	}


	/**
	 * Create an instance of {@link DeletePermanently }
	 */
	public DeletePermanently createDeletePermanently() {
		return new DeletePermanently();
	}


	/**
	 * Create an instance of {@link InboundUserFaxItemFull }
	 */
	public InboundUserFaxItemFull createInboundUserFaxItemFull() {
		return new InboundUserFaxItemFull();
	}


	/**
	 * Create an instance of {@link BillingCode }
	 */
	public BillingCode createBillingCode() {
		return new BillingCode();
	}


	/**
	 * Create an instance of {@link OutboundSortInfo }
	 */
	public OutboundSortInfo createOutboundSortInfo() {
		return new OutboundSortInfo();
	}


	/**
	 * Create an instance of {@link OutboundUserSearchInfo }
	 */
	public OutboundUserSearchInfo createOutboundUserSearchInfo() {
		return new OutboundUserSearchInfo();
	}


	/**
	 * Create an instance of {@link OutboundUserKeywordInfo }
	 */
	public OutboundUserKeywordInfo createOutboundUserKeywordInfo() {
		return new OutboundUserKeywordInfo();
	}


	/**
	 * Create an instance of {@link OutboundSearchInfo }
	 */
	public OutboundSearchInfo createOutboundSearchInfo() {
		return new OutboundSearchInfo();
	}


	/**
	 * Create an instance of {@link InboundUserKeywordInfo }
	 */
	public InboundUserKeywordInfo createInboundUserKeywordInfo() {
		return new InboundUserKeywordInfo();
	}


	/**
	 * Create an instance of {@link InboundSortInfo }
	 */
	public InboundSortInfo createInboundSortInfo() {
		return new InboundSortInfo();
	}


	/**
	 * Create an instance of {@link FaxResponse }
	 */
	public FaxResponse createFaxResponse() {
		return new FaxResponse();
	}


	/**
	 * Create an instance of {@link OutboundFaxItem }
	 */
	public OutboundFaxItem createOutboundFaxItem() {
		return new OutboundFaxItem();
	}


	/**
	 * Create an instance of {@link InboundUserFaxItem }
	 */
	public InboundUserFaxItem createInboundUserFaxItem() {
		return new InboundUserFaxItem();
	}


	/**
	 * Create an instance of {@link FaxRecipient }
	 */
	public FaxRecipient createFaxRecipient() {
		return new FaxRecipient();
	}


	/**
	 * Create an instance of {@link OutgoingFaxItemFull }
	 */
	public OutgoingFaxItemFull createOutgoingFaxItemFull() {
		return new OutgoingFaxItemFull();
	}


	/**
	 * Create an instance of {@link OutboundUserFaxItemFull }
	 */
	public OutboundUserFaxItemFull createOutboundUserFaxItemFull() {
		return new OutboundUserFaxItemFull();
	}


	/**
	 * Create an instance of {@link BillingGroup }
	 */
	public BillingGroup createBillingGroup() {
		return new BillingGroup();
	}


	/**
	 * Create an instance of {@link InboundKeywordInfo }
	 */
	public InboundKeywordInfo createInboundKeywordInfo() {
		return new InboundKeywordInfo();
	}


	/**
	 * Create an instance of {@link InboundUserFaxResponse }
	 */
	public InboundUserFaxResponse createInboundUserFaxResponse() {
		return new InboundUserFaxResponse();
	}


	/**
	 * Create an instance of {@link AuditLog }
	 */
	public AuditLog createAuditLog() {
		return new AuditLog();
	}


	/**
	 * Create an instance of {@link FaxImage }
	 */
	public FaxImage createFaxImage() {
		return new FaxImage();
	}


	/**
	 * Create an instance of {@link Version }
	 */
	public Version createVersion() {
		return new Version();
	}


	/**
	 * Create an instance of {@link PhonebookPerson }
	 */
	public PhonebookPerson createPhonebookPerson() {
		return new PhonebookPerson();
	}


	/**
	 * Create an instance of {@link OutgoingFaxResponse }
	 */
	public OutgoingFaxResponse createOutgoingFaxResponse() {
		return new OutgoingFaxResponse();
	}


	/**
	 * Create an instance of {@link OutgoingSearchInfo }
	 */
	public OutgoingSearchInfo createOutgoingSearchInfo() {
		return new OutgoingSearchInfo();
	}


	/**
	 * Create an instance of {@link PhonebookGroup }
	 */
	public PhonebookGroup createPhonebookGroup() {
		return new PhonebookGroup();
	}


	/**
	 * Create an instance of {@link FaxOptions }
	 */
	public FaxOptions createFaxOptions() {
		return new FaxOptions();
	}


	/**
	 * Create an instance of {@link PhonebookSearchResponse }
	 */
	public PhonebookSearchResponse createPhonebookSearchResponse() {
		return new PhonebookSearchResponse();
	}


	/**
	 * Create an instance of {@link UserProfile }
	 */
	public UserProfile createUserProfile() {
		return new UserProfile();
	}


	/**
	 * Create an instance of {@link FaxSecurityOptions }
	 */
	public FaxSecurityOptions createFaxSecurityOptions() {
		return new FaxSecurityOptions();
	}


	/**
	 * Create an instance of {@link OutgoingFaxItem }
	 */
	public OutgoingFaxItem createOutgoingFaxItem() {
		return new OutgoingFaxItem();
	}


	/**
	 * Create an instance of {@link InboundUserSearchInfo }
	 */
	public InboundUserSearchInfo createInboundUserSearchInfo() {
		return new InboundUserSearchInfo();
	}


	/**
	 * Create an instance of {@link DateTimeRange }
	 */
	public DateTimeRange createDateTimeRange() {
		return new DateTimeRange();
	}


	/**
	 * Create an instance of {@link FaxResult }
	 */
	public FaxResult createFaxResult() {
		return new FaxResult();
	}


	/**
	 * Create an instance of {@link SenderInfo }
	 */
	public SenderInfo createSenderInfo() {
		return new SenderInfo();
	}


	/**
	 * Create an instance of {@link InboundSearchInfo }
	 */
	public InboundSearchInfo createInboundSearchInfo() {
		return new InboundSearchInfo();
	}


	/**
	 * Create an instance of {@link AttachedFile }
	 */
	public AttachedFile createAttachedFile() {
		return new AttachedFile();
	}


	/**
	 * Create an instance of {@link KeywordInfo }
	 */
	public KeywordInfo createKeywordInfo() {
		return new KeywordInfo();
	}


	/**
	 * Create an instance of {@link InboundFaxItem }
	 */
	public InboundFaxItem createInboundFaxItem() {
		return new InboundFaxItem();
	}


	/**
	 * Create an instance of {@link AuditLogResponse }
	 */
	public AuditLogResponse createAuditLogResponse() {
		return new AuditLogResponse();
	}


	/**
	 * Create an instance of {@link ProfileFaxOptions }
	 */
	public ProfileFaxOptions createProfileFaxOptions() {
		return new ProfileFaxOptions();
	}


	/**
	 * Create an instance of {@link OutboundUserFaxItem }
	 */
	public OutboundUserFaxItem createOutboundUserFaxItem() {
		return new OutboundUserFaxItem();
	}


	/**
	 * Create an instance of {@link OutboundUserFaxResponse }
	 */
	public OutboundUserFaxResponse createOutboundUserFaxResponse() {
		return new OutboundUserFaxResponse();
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutboundFaxesResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutboundFaxesResponse")
	public JAXBElement<FindOutboundFaxesResponse> createFindOutboundFaxesResponse(FindOutboundFaxesResponse value) {
		return new JAXBElement<>(_FindOutboundFaxesResponse_QNAME, FindOutboundFaxesResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetOutboundFaxAdvanced }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getOutboundFaxAdvanced")
	public JAXBElement<GetOutboundFaxAdvanced> createGetOutboundFaxAdvanced(GetOutboundFaxAdvanced value) {
		return new JAXBElement<>(_GetOutboundFaxAdvanced_QNAME, GetOutboundFaxAdvanced.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsViewed }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsViewed")
	public JAXBElement<MarkAsViewed> createMarkAsViewed(MarkAsViewed value) {
		return new JAXBElement<>(_MarkAsViewed_QNAME, MarkAsViewed.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetOutboundFax }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getOutboundFax")
	public JAXBElement<GetOutboundFax> createGetOutboundFax(GetOutboundFax value) {
		return new JAXBElement<>(_GetOutboundFax_QNAME, GetOutboundFax.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DeletePermanentlyResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "deletePermanentlyResponse")
	public JAXBElement<DeletePermanentlyResponse> createDeletePermanentlyResponse(DeletePermanentlyResponse value) {
		return new JAXBElement<>(_DeletePermanentlyResponse_QNAME, DeletePermanentlyResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetOutgoingFax }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getOutgoingFax")
	public JAXBElement<GetOutgoingFax> createGetOutgoingFax(GetOutgoingFax value) {
		return new JAXBElement<>(_GetOutgoingFax_QNAME, GetOutgoingFax.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetPersonsInGroupRecursiveResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getPersonsInGroupRecursiveResponse")
	public JAXBElement<GetPersonsInGroupRecursiveResponse> createGetPersonsInGroupRecursiveResponse(GetPersonsInGroupRecursiveResponse value) {
		return new JAXBElement<>(_GetPersonsInGroupRecursiveResponse_QNAME, GetPersonsInGroupRecursiveResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SendFaxLaterResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "sendFaxLaterResponse")
	public JAXBElement<SendFaxLaterResponse> createSendFaxLaterResponse(SendFaxLaterResponse value) {
		return new JAXBElement<>(_SendFaxLaterResponse_QNAME, SendFaxLaterResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindContactsResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findContactsResponse")
	public JAXBElement<FindContactsResponse> createFindContactsResponse(FindContactsResponse value) {
		return new JAXBElement<>(_FindContactsResponse_QNAME, FindContactsResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetInboundFaxAdvanced }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getInboundFaxAdvanced")
	public JAXBElement<GetInboundFaxAdvanced> createGetInboundFaxAdvanced(GetInboundFaxAdvanced value) {
		return new JAXBElement<>(_GetInboundFaxAdvanced_QNAME, GetInboundFaxAdvanced.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindInboundFaxes }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findInboundFaxes")
	public JAXBElement<FindInboundFaxes> createFindInboundFaxes(FindInboundFaxes value) {
		return new JAXBElement<>(_FindInboundFaxes_QNAME, FindInboundFaxes.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutboundFaxes }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutboundFaxes")
	public JAXBElement<FindOutboundFaxes> createFindOutboundFaxes(FindOutboundFaxes value) {
		return new JAXBElement<>(_FindOutboundFaxes_QNAME, FindOutboundFaxes.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutgoingFaxesResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutgoingFaxesResponse")
	public JAXBElement<FindOutgoingFaxesResponse> createFindOutgoingFaxesResponse(FindOutgoingFaxesResponse value) {
		return new JAXBElement<>(_FindOutgoingFaxesResponse_QNAME, FindOutgoingFaxesResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetOutboundFaxAdvancedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getOutboundFaxAdvancedResponse")
	public JAXBElement<GetOutboundFaxAdvancedResponse> createGetOutboundFaxAdvancedResponse(GetOutboundFaxAdvancedResponse value) {
		return new JAXBElement<>(_GetOutboundFaxAdvancedResponse_QNAME, GetOutboundFaxAdvancedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FaxException }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "FaxException")
	public JAXBElement<FaxException> createFaxException(FaxException value) {
		return new JAXBElement<>(_FaxException_QNAME, FaxException.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutgoingFaxesAdvancedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutgoingFaxesAdvancedResponse")
	public JAXBElement<FindOutgoingFaxesAdvancedResponse> createFindOutgoingFaxesAdvancedResponse(FindOutgoingFaxesAdvancedResponse value) {
		return new JAXBElement<>(_FindOutgoingFaxesAdvancedResponse_QNAME, FindOutgoingFaxesAdvancedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link CancelBroadcastResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "cancelBroadcastResponse")
	public JAXBElement<CancelBroadcastResponse> createCancelBroadcastResponse(CancelBroadcastResponse value) {
		return new JAXBElement<>(_CancelBroadcastResponse_QNAME, CancelBroadcastResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutgoingFaxes }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutgoingFaxes")
	public JAXBElement<FindOutgoingFaxes> createFindOutgoingFaxes(FindOutgoingFaxes value) {
		return new JAXBElement<>(_FindOutgoingFaxes_QNAME, FindOutgoingFaxes.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SetUserData }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "setUserData")
	public JAXBElement<SetUserData> createSetUserData(SetUserData value) {
		return new JAXBElement<>(_SetUserData_QNAME, SetUserData.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddContactResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "addContactResponse")
	public JAXBElement<AddContactResponse> createAddContactResponse(AddContactResponse value) {
		return new JAXBElement<>(_AddContactResponse_QNAME, AddContactResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetInboundFaxResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getInboundFaxResponse")
	public JAXBElement<GetInboundFaxResponse> createGetInboundFaxResponse(GetInboundFaxResponse value) {
		return new JAXBElement<>(_GetInboundFaxResponse_QNAME, GetInboundFaxResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Cancel }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "cancel")
	public JAXBElement<Cancel> createCancel(Cancel value) {
		return new JAXBElement<>(_Cancel_QNAME, Cancel.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindInboundFaxesAdvanced }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findInboundFaxesAdvanced")
	public JAXBElement<FindInboundFaxesAdvanced> createFindInboundFaxesAdvanced(FindInboundFaxesAdvanced value) {
		return new JAXBElement<>(_FindInboundFaxesAdvanced_QNAME, FindInboundFaxesAdvanced.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutgoingFaxesAdvanced }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutgoingFaxesAdvanced")
	public JAXBElement<FindOutgoingFaxesAdvanced> createFindOutgoingFaxesAdvanced(FindOutgoingFaxesAdvanced value) {
		return new JAXBElement<>(_FindOutgoingFaxesAdvanced_QNAME, FindOutgoingFaxesAdvanced.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetOutboundFaxResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getOutboundFaxResponse")
	public JAXBElement<GetOutboundFaxResponse> createGetOutboundFaxResponse(GetOutboundFaxResponse value) {
		return new JAXBElement<>(_GetOutboundFaxResponse_QNAME, GetOutboundFaxResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ResubmitToResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "resubmitToResponse")
	public JAXBElement<ResubmitToResponse> createResubmitToResponse(ResubmitToResponse value) {
		return new JAXBElement<>(_ResubmitToResponse_QNAME, ResubmitToResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ResubmitLaterResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "resubmitLaterResponse")
	public JAXBElement<ResubmitLaterResponse> createResubmitLaterResponse(ResubmitLaterResponse value) {
		return new JAXBElement<>(_ResubmitLaterResponse_QNAME, ResubmitLaterResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link VerifyUserPassword }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "verifyUserPassword")
	public JAXBElement<VerifyUserPassword> createVerifyUserPassword(VerifyUserPassword value) {
		return new JAXBElement<>(_VerifyUserPassword_QNAME, VerifyUserPassword.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetAuditLogs }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getAuditLogs")
	public JAXBElement<GetAuditLogs> createGetAuditLogs(GetAuditLogs value) {
		return new JAXBElement<>(_GetAuditLogs_QNAME, GetAuditLogs.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SendFaxResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "sendFaxResponse")
	public JAXBElement<SendFaxResponse> createSendFaxResponse(SendFaxResponse value) {
		return new JAXBElement<>(_SendFaxResponse_QNAME, SendFaxResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetInboundFaxAdvancedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getInboundFaxAdvancedResponse")
	public JAXBElement<GetInboundFaxAdvancedResponse> createGetInboundFaxAdvancedResponse(GetInboundFaxAdvancedResponse value) {
		return new JAXBElement<>(_GetInboundFaxAdvancedResponse_QNAME, GetInboundFaxAdvancedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsDeletedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsDeletedResponse")
	public JAXBElement<MarkAsDeletedResponse> createMarkAsDeletedResponse(MarkAsDeletedResponse value) {
		return new JAXBElement<>(_MarkAsDeletedResponse_QNAME, MarkAsDeletedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetVersion }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getVersion")
	public JAXBElement<GetVersion> createGetVersion(GetVersion value) {
		return new JAXBElement<>(_GetVersion_QNAME, GetVersion.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetUserDataResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getUserDataResponse")
	public JAXBElement<GetUserDataResponse> createGetUserDataResponse(GetUserDataResponse value) {
		return new JAXBElement<>(_GetUserDataResponse_QNAME, GetUserDataResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ResubmitTo }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "resubmitTo")
	public JAXBElement<ResubmitTo> createResubmitTo(ResubmitTo value) {
		return new JAXBElement<>(_ResubmitTo_QNAME, ResubmitTo.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetInboundFax }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getInboundFax")
	public JAXBElement<GetInboundFax> createGetInboundFax(GetInboundFax value) {
		return new JAXBElement<>(_GetInboundFax_QNAME, GetInboundFax.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link CancelBroadcast }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "cancelBroadcast")
	public JAXBElement<CancelBroadcast> createCancelBroadcast(CancelBroadcast value) {
		return new JAXBElement<>(_CancelBroadcast_QNAME, CancelBroadcast.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindInboundFaxesResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findInboundFaxesResponse")
	public JAXBElement<FindInboundFaxesResponse> createFindInboundFaxesResponse(FindInboundFaxesResponse value) {
		return new JAXBElement<>(_FindInboundFaxesResponse_QNAME, FindInboundFaxesResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ResubmitResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "resubmitResponse")
	public JAXBElement<ResubmitResponse> createResubmitResponse(ResubmitResponse value) {
		return new JAXBElement<>(_ResubmitResponse_QNAME, ResubmitResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutboundFaxesAdvancedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutboundFaxesAdvancedResponse")
	public JAXBElement<FindOutboundFaxesAdvancedResponse> createFindOutboundFaxesAdvancedResponse(FindOutboundFaxesAdvancedResponse value) {
		return new JAXBElement<>(_FindOutboundFaxesAdvancedResponse_QNAME, FindOutboundFaxesAdvancedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SetUserDataResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "setUserDataResponse")
	public JAXBElement<SetUserDataResponse> createSetUserDataResponse(SetUserDataResponse value) {
		return new JAXBElement<>(_SetUserDataResponse_QNAME, SetUserDataResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindInboundFaxesAdvancedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findInboundFaxesAdvancedResponse")
	public JAXBElement<FindInboundFaxesAdvancedResponse> createFindInboundFaxesAdvancedResponse(FindInboundFaxesAdvancedResponse value) {
		return new JAXBElement<>(_FindInboundFaxesAdvancedResponse_QNAME, FindInboundFaxesAdvancedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsUnviewed }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsUnviewed")
	public JAXBElement<MarkAsUnviewed> createMarkAsUnviewed(MarkAsUnviewed value) {
		return new JAXBElement<>(_MarkAsUnviewed_QNAME, MarkAsUnviewed.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetOutgoingFaxResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getOutgoingFaxResponse")
	public JAXBElement<GetOutgoingFaxResponse> createGetOutgoingFaxResponse(GetOutgoingFaxResponse value) {
		return new JAXBElement<>(_GetOutgoingFaxResponse_QNAME, GetOutgoingFaxResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindOutboundFaxesAdvanced }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findOutboundFaxesAdvanced")
	public JAXBElement<FindOutboundFaxesAdvanced> createFindOutboundFaxesAdvanced(FindOutboundFaxesAdvanced value) {
		return new JAXBElement<>(_FindOutboundFaxesAdvanced_QNAME, FindOutboundFaxesAdvanced.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsDeleted }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsDeleted")
	public JAXBElement<MarkAsDeleted> createMarkAsDeleted(MarkAsDeleted value) {
		return new JAXBElement<>(_MarkAsDeleted_QNAME, MarkAsDeleted.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SendFax }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "sendFax")
	public JAXBElement<SendFax> createSendFax(SendFax value) {
		return new JAXBElement<>(_SendFax_QNAME, SendFax.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetUserData }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getUserData")
	public JAXBElement<GetUserData> createGetUserData(GetUserData value) {
		return new JAXBElement<>(_GetUserData_QNAME, GetUserData.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link DeletePermanently }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "deletePermanently")
	public JAXBElement<DeletePermanently> createDeletePermanently(DeletePermanently value) {
		return new JAXBElement<>(_DeletePermanently_QNAME, DeletePermanently.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Resubmit }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "resubmit")
	public JAXBElement<Resubmit> createResubmit(Resubmit value) {
		return new JAXBElement<>(_Resubmit_QNAME, Resubmit.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SaveNote }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "saveNote")
	public JAXBElement<SaveNote> createSaveNote(SaveNote value) {
		return new JAXBElement<>(_SaveNote_QNAME, SaveNote.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link CancelResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "cancelResponse")
	public JAXBElement<CancelResponse> createCancelResponse(CancelResponse value) {
		return new JAXBElement<>(_CancelResponse_QNAME, CancelResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsUndeleted }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsUndeleted")
	public JAXBElement<MarkAsUndeleted> createMarkAsUndeleted(MarkAsUndeleted value) {
		return new JAXBElement<>(_MarkAsUndeleted_QNAME, MarkAsUndeleted.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsUndeletedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsUndeletedResponse")
	public JAXBElement<MarkAsUndeletedResponse> createMarkAsUndeletedResponse(MarkAsUndeletedResponse value) {
		return new JAXBElement<>(_MarkAsUndeletedResponse_QNAME, MarkAsUndeletedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link VerifyUserPasswordResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "verifyUserPasswordResponse")
	public JAXBElement<VerifyUserPasswordResponse> createVerifyUserPasswordResponse(VerifyUserPasswordResponse value) {
		return new JAXBElement<>(_VerifyUserPasswordResponse_QNAME, VerifyUserPasswordResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetVersionResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getVersionResponse")
	public JAXBElement<GetVersionResponse> createGetVersionResponse(GetVersionResponse value) {
		return new JAXBElement<>(_GetVersionResponse_QNAME, GetVersionResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SaveNoteResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "saveNoteResponse")
	public JAXBElement<SaveNoteResponse> createSaveNoteResponse(SaveNoteResponse value) {
		return new JAXBElement<>(_SaveNoteResponse_QNAME, SaveNoteResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link AddContact }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "addContact")
	public JAXBElement<AddContact> createAddContact(AddContact value) {
		return new JAXBElement<>(_AddContact_QNAME, AddContact.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FindContacts }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "findContacts")
	public JAXBElement<FindContacts> createFindContacts(FindContacts value) {
		return new JAXBElement<>(_FindContacts_QNAME, FindContacts.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link ResubmitLater }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "resubmitLater")
	public JAXBElement<ResubmitLater> createResubmitLater(ResubmitLater value) {
		return new JAXBElement<>(_ResubmitLater_QNAME, ResubmitLater.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetUserProfile }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getUserProfile")
	public JAXBElement<GetUserProfile> createGetUserProfile(GetUserProfile value) {
		return new JAXBElement<>(_GetUserProfile_QNAME, GetUserProfile.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SendFaxLater }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "sendFaxLater")
	public JAXBElement<SendFaxLater> createSendFaxLater(SendFaxLater value) {
		return new JAXBElement<>(_SendFaxLater_QNAME, SendFaxLater.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsViewedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsViewedResponse")
	public JAXBElement<MarkAsViewedResponse> createMarkAsViewedResponse(MarkAsViewedResponse value) {
		return new JAXBElement<>(_MarkAsViewedResponse_QNAME, MarkAsViewedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link RetrySendingNow }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "retrySendingNow")
	public JAXBElement<RetrySendingNow> createRetrySendingNow(RetrySendingNow value) {
		return new JAXBElement<>(_RetrySendingNow_QNAME, RetrySendingNow.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetPersonsInGroupRecursive }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getPersonsInGroupRecursive")
	public JAXBElement<GetPersonsInGroupRecursive> createGetPersonsInGroupRecursive(GetPersonsInGroupRecursive value) {
		return new JAXBElement<>(_GetPersonsInGroupRecursive_QNAME, GetPersonsInGroupRecursive.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link MarkAsUnviewedResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "markAsUnviewedResponse")
	public JAXBElement<MarkAsUnviewedResponse> createMarkAsUnviewedResponse(MarkAsUnviewedResponse value) {
		return new JAXBElement<>(_MarkAsUnviewedResponse_QNAME, MarkAsUnviewedResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetAuditLogsResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getAuditLogsResponse")
	public JAXBElement<GetAuditLogsResponse> createGetAuditLogsResponse(GetAuditLogsResponse value) {
		return new JAXBElement<>(_GetAuditLogsResponse_QNAME, GetAuditLogsResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link GetUserProfileResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "getUserProfileResponse")
	public JAXBElement<GetUserProfileResponse> createGetUserProfileResponse(GetUserProfileResponse value) {
		return new JAXBElement<>(_GetUserProfileResponse_QNAME, GetUserProfileResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link RetrySendingNowResponse }{@code >}}
	 */
	@XmlElementDecl(namespace = "http://ws.xm.faxserver.com/", name = "retrySendingNowResponse")
	public JAXBElement<RetrySendingNowResponse> createRetrySendingNowResponse(RetrySendingNowResponse value) {
		return new JAXBElement<>(_RetrySendingNowResponse_QNAME, RetrySendingNowResponse.class, null, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link FaxResolution }{@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "maxResolution", scope = ProfileFaxOptions.class)
	public JAXBElement<FaxResolution> createProfileFaxOptionsMaxResolution(FaxResolution value) {
		return new JAXBElement<>(_ProfileFaxOptionsMaxResolution_QNAME, FaxResolution.class, ProfileFaxOptions.class, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link SendingPriority }{@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "maxPriority", scope = ProfileFaxOptions.class)
	public JAXBElement<SendingPriority> createProfileFaxOptionsMaxPriority(SendingPriority value) {
		return new JAXBElement<>(_ProfileFaxOptionsMaxPriority_QNAME, SendingPriority.class, ProfileFaxOptions.class, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "notifyOnFailure", scope = ProfileFaxOptions.class)
	public JAXBElement<Boolean> createProfileFaxOptionsNotifyOnFailure(Boolean value) {
		return new JAXBElement<>(_ProfileFaxOptionsNotifyOnFailure_QNAME, Boolean.class, ProfileFaxOptions.class, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "notifyOnSuccess", scope = ProfileFaxOptions.class)
	public JAXBElement<Boolean> createProfileFaxOptionsNotifyOnSuccess(Boolean value) {
		return new JAXBElement<>(_ProfileFaxOptionsNotifyOnSuccess_QNAME, Boolean.class, ProfileFaxOptions.class, value);
	}


	/**
	 * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
	 */
	@XmlElementDecl(namespace = "", name = "maxNoOfRetries", scope = ProfileFaxOptions.class)
	public JAXBElement<Long> createProfileFaxOptionsMaxNoOfRetries(Long value) {
		return new JAXBElement<>(_ProfileFaxOptionsMaxNoOfRetries_QNAME, Long.class, ProfileFaxOptions.class, value);
	}
}
