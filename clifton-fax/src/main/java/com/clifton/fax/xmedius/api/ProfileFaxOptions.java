package com.clifton.fax.xmedius.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for ProfileFaxOptions complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ProfileFaxOptions">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}FaxOptions">
 *       &lt;sequence>
 *         &lt;element name="maxPriority" type="{http://ws.xm.faxserver.com/}SendingPriority"/>
 *         &lt;element name="maxNoOfRetries" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *         &lt;element name="maxResolution" type="{http://ws.xm.faxserver.com/}FaxResolution"/>
 *         &lt;element name="notifyOnSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="notifyOnFailure" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProfileFaxOptions", propOrder = {
		"rest"
})
public class ProfileFaxOptions
		extends FaxOptions {

	@XmlElementRefs({
			@XmlElementRef(name = "maxPriority", type = JAXBElement.class, required = false),
			@XmlElementRef(name = "maxNoOfRetries", type = JAXBElement.class, required = false),
			@XmlElementRef(name = "maxResolution", type = JAXBElement.class, required = false),
			@XmlElementRef(name = "notifyOnSuccess", type = JAXBElement.class, required = false),
			@XmlElementRef(name = "notifyOnFailure", type = JAXBElement.class, required = false)
	})
	private List<JAXBElement<?>> rest;


	/**
	 * Gets the rest of the content model.
	 *
	 * <p>
	 * You are getting this "catch-all" property because of the following reason:
	 * The field name "NotifyOnSuccess" is used by two different parts of a schema. See:
	 * line 920 of https://mnfax.paraport.com:443/faxservice/Fax?xsd=1
	 * line 537 of https://mnfax.paraport.com:443/faxservice/Fax?xsd=1
	 * <p>
	 * To get rid of this property, apply a property customization to one
	 * of both of the following declarations to change their names:
	 * Gets the value of the rest property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the rest property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getRest().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link JAXBElement }{@code <}{@link SendingPriority }{@code >}
	 * {@link JAXBElement }{@code <}{@link Long }{@code >}
	 * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
	 * {@link JAXBElement }{@code <}{@link FaxResolution }{@code >}
	 * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
	 */
	public List<JAXBElement<?>> getRest() {
		if (this.rest == null) {
			this.rest = new ArrayList<>();
		}
		return this.rest;
	}
}
