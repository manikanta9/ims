package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InboundSortProperty.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InboundSortProperty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ReceivedTime"/>
 *     &lt;enumeration value="Duration"/>
 *     &lt;enumeration value="DnisOrDid"/>
 *     &lt;enumeration value="RemoteCsid"/>
 *     &lt;enumeration value="Ani"/>
 *     &lt;enumeration value="Dtmf"/>
 *     &lt;enumeration value="LocalCsid"/>
 *     &lt;enumeration value="LowLevelErrorMessage"/>
 *     &lt;enumeration value="Status"/>
 *     &lt;enumeration value="ErrorCode"/>
 *     &lt;enumeration value="PagesReceived"/>
 *     &lt;enumeration value="SiteName"/>
 *     &lt;enumeration value="ArchivedTime"/>
 *     &lt;enumeration value="ChannelNumber"/>
 *     &lt;enumeration value="Speed"/>
 *     &lt;enumeration value="TransactionId"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "InboundSortProperty")
@XmlEnum
public enum InboundSortProperty {

	@XmlEnumValue("ReceivedTime")
	RECEIVED_TIME("ReceivedTime"),
	@XmlEnumValue("Duration")
	DURATION("Duration"),
	@XmlEnumValue("DnisOrDid")
	DNIS_OR_DID("DnisOrDid"),
	@XmlEnumValue("RemoteCsid")
	REMOTE_CSID("RemoteCsid"),
	@XmlEnumValue("Ani")
	ANI("Ani"),
	@XmlEnumValue("Dtmf")
	DTMF("Dtmf"),
	@XmlEnumValue("LocalCsid")
	LOCAL_CSID("LocalCsid"),
	@XmlEnumValue("LowLevelErrorMessage")
	LOW_LEVEL_ERROR_MESSAGE("LowLevelErrorMessage"),
	@XmlEnumValue("Status")
	STATUS("Status"),
	@XmlEnumValue("ErrorCode")
	ERROR_CODE("ErrorCode"),
	@XmlEnumValue("PagesReceived")
	PAGES_RECEIVED("PagesReceived"),
	@XmlEnumValue("SiteName")
	SITE_NAME("SiteName"),
	@XmlEnumValue("ArchivedTime")
	ARCHIVED_TIME("ArchivedTime"),
	@XmlEnumValue("ChannelNumber")
	CHANNEL_NUMBER("ChannelNumber"),
	@XmlEnumValue("Speed")
	SPEED("Speed"),
	@XmlEnumValue("TransactionId")
	TRANSACTION_ID("TransactionId");
	private final String value;


	InboundSortProperty(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static InboundSortProperty fromValue(String v) {
		for (InboundSortProperty c : InboundSortProperty.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
