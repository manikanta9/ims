package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for verifyUserPasswordResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="verifyUserPasswordResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="verification" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "verifyUserPasswordResponse", propOrder = {
		"verification"
})
public class VerifyUserPasswordResponse {

	private boolean verification;


	/**
	 * Gets the value of the verification property.
	 */
	public boolean isVerification() {
		return this.verification;
	}


	/**
	 * Sets the value of the verification property.
	 */
	public void setVerification(boolean value) {
		this.verification = value;
	}
}
