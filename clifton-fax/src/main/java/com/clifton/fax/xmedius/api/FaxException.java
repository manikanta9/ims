package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FaxException complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaxException">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="detail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="vendorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxException", propOrder = {
		"code",
		"detail",
		"message",
		"time",
		"vendorCode",
		"version"
})
public class FaxException {

	private int code;
	private String detail;
	private String message;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar time;
	private String vendorCode;
	private String version;


	/**
	 * Gets the value of the code property.
	 */
	public int getCode() {
		return this.code;
	}


	/**
	 * Sets the value of the code property.
	 */
	public void setCode(int value) {
		this.code = value;
	}


	/**
	 * Gets the value of the detail property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getDetail() {
		return this.detail;
	}


	/**
	 * Sets the value of the detail property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setDetail(String value) {
		this.detail = value;
	}


	/**
	 * Gets the value of the message property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getMessage() {
		return this.message;
	}


	/**
	 * Sets the value of the message property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setMessage(String value) {
		this.message = value;
	}


	/**
	 * Gets the value of the time property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getTime() {
		return this.time;
	}


	/**
	 * Sets the value of the time property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setTime(XMLGregorianCalendar value) {
		this.time = value;
	}


	/**
	 * Gets the value of the vendorCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getVendorCode() {
		return this.vendorCode;
	}


	/**
	 * Sets the value of the vendorCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setVendorCode(String value) {
		this.vendorCode = value;
	}


	/**
	 * Gets the value of the version property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getVersion() {
		return this.version;
	}


	/**
	 * Sets the value of the version property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setVersion(String value) {
		this.version = value;
	}
}
