package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getInboundFaxAdvanced complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getInboundFaxAdvanced">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="faxId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="faxFormat" type="{http://ws.xm.faxserver.com/}FaxFormat" minOccurs="0"/>
 *         &lt;element name="maxPages" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="bannerLocation" type="{http://ws.xm.faxserver.com/}BannerLocation" minOccurs="0"/>
 *         &lt;element name="locale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="timezone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInboundFaxAdvanced", propOrder = {
		"faxId",
		"faxFormat",
		"maxPages",
		"bannerLocation",
		"locale",
		"timezone"
})
public class GetInboundFaxAdvanced {

	private String faxId;
	@XmlSchemaType(name = "string")
	private FaxFormat faxFormat;
	private Integer maxPages;
	@XmlSchemaType(name = "string")
	private BannerLocation bannerLocation;
	private String locale;
	private String timezone;


	/**
	 * Gets the value of the faxId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFaxId() {
		return this.faxId;
	}


	/**
	 * Sets the value of the faxId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setFaxId(String value) {
		this.faxId = value;
	}


	/**
	 * Gets the value of the faxFormat property.
	 *
	 * @return possible object is
	 * {@link FaxFormat }
	 */
	public FaxFormat getFaxFormat() {
		return this.faxFormat;
	}


	/**
	 * Sets the value of the faxFormat property.
	 *
	 * @param value allowed object is
	 *              {@link FaxFormat }
	 */
	public void setFaxFormat(FaxFormat value) {
		this.faxFormat = value;
	}


	/**
	 * Gets the value of the maxPages property.
	 *
	 * @return possible object is
	 * {@link Integer }
	 */
	public Integer getMaxPages() {
		return this.maxPages;
	}


	/**
	 * Sets the value of the maxPages property.
	 *
	 * @param value allowed object is
	 *              {@link Integer }
	 */
	public void setMaxPages(Integer value) {
		this.maxPages = value;
	}


	/**
	 * Gets the value of the bannerLocation property.
	 *
	 * @return possible object is
	 * {@link BannerLocation }
	 */
	public BannerLocation getBannerLocation() {
		return this.bannerLocation;
	}


	/**
	 * Sets the value of the bannerLocation property.
	 *
	 * @param value allowed object is
	 *              {@link BannerLocation }
	 */
	public void setBannerLocation(BannerLocation value) {
		this.bannerLocation = value;
	}


	/**
	 * Gets the value of the locale property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getLocale() {
		return this.locale;
	}


	/**
	 * Sets the value of the locale property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setLocale(String value) {
		this.locale = value;
	}


	/**
	 * Gets the value of the timezone property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getTimezone() {
		return this.timezone;
	}


	/**
	 * Sets the value of the timezone property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setTimezone(String value) {
		this.timezone = value;
	}
}
