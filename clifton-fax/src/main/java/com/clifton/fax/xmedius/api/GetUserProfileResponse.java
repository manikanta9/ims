package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUserProfileResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getUserProfileResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userProfile" type="{http://ws.xm.faxserver.com/}UserProfile" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserProfileResponse", propOrder = {
		"userProfile"
})
public class GetUserProfileResponse {

	private UserProfile userProfile;


	/**
	 * Gets the value of the userProfile property.
	 *
	 * @return possible object is
	 * {@link UserProfile }
	 */
	public UserProfile getUserProfile() {
		return this.userProfile;
	}


	/**
	 * Sets the value of the userProfile property.
	 *
	 * @param value allowed object is
	 *              {@link UserProfile }
	 */
	public void setUserProfile(UserProfile value) {
		this.userProfile = value;
	}
}
