package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaxImage complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaxImage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="mimeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxImage", propOrder = {
		"content",
		"mimeType"
})
public class FaxImage {

	@XmlElement(required = true)
	private byte[] content;
	@XmlElement(required = true)
	private String mimeType;


	/**
	 * Gets the value of the content property.
	 *
	 * @return possible object is
	 * byte[]
	 */
	public byte[] getContent() {
		return this.content;
	}


	/**
	 * Sets the value of the content property.
	 *
	 * @param value allowed object is
	 *              byte[]
	 */
	public void setContent(byte[] value) {
		this.content = value;
	}


	/**
	 * Gets the value of the mimeType property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getMimeType() {
		return this.mimeType;
	}


	/**
	 * Sets the value of the mimeType property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setMimeType(String value) {
		this.mimeType = value;
	}
}
