package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for PhonebookPerson complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="PhonebookPerson">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="organisation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subBillingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="street" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PhonebookPerson", propOrder = {
		"id",
		"name",
		"fax",
		"organisation",
		"billingCode",
		"subBillingCode",
		"city",
		"country",
		"email",
		"title",
		"mobile",
		"pager",
		"phone",
		"zipCode",
		"state",
		"street"
})
public class PhonebookPerson {

	private List<String> id;
	@XmlElement(required = true)
	private String name;
	@XmlElement(required = true)
	private String fax;
	private String organisation;
	private String billingCode;
	private String subBillingCode;
	private String city;
	private String country;
	private String email;
	private String title;
	private String mobile;
	private String pager;
	private String phone;
	private String zipCode;
	private String state;
	private String street;


	/**
	 * Gets the value of the id property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the id property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getId().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link String }
	 */
	public List<String> getId() {
		if (this.id == null) {
			this.id = new ArrayList<>();
		}
		return this.id;
	}


	/**
	 * Gets the value of the name property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * Sets the value of the name property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setName(String value) {
		this.name = value;
	}


	/**
	 * Gets the value of the fax property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFax() {
		return this.fax;
	}


	/**
	 * Sets the value of the fax property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setFax(String value) {
		this.fax = value;
	}


	/**
	 * Gets the value of the organisation property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getOrganisation() {
		return this.organisation;
	}


	/**
	 * Sets the value of the organisation property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setOrganisation(String value) {
		this.organisation = value;
	}


	/**
	 * Gets the value of the billingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBillingCode() {
		return this.billingCode;
	}


	/**
	 * Sets the value of the billingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setBillingCode(String value) {
		this.billingCode = value;
	}


	/**
	 * Gets the value of the subBillingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSubBillingCode() {
		return this.subBillingCode;
	}


	/**
	 * Sets the value of the subBillingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSubBillingCode(String value) {
		this.subBillingCode = value;
	}


	/**
	 * Gets the value of the city property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCity() {
		return this.city;
	}


	/**
	 * Sets the value of the city property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCity(String value) {
		this.city = value;
	}


	/**
	 * Gets the value of the country property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCountry() {
		return this.country;
	}


	/**
	 * Sets the value of the country property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCountry(String value) {
		this.country = value;
	}


	/**
	 * Gets the value of the email property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getEmail() {
		return this.email;
	}


	/**
	 * Sets the value of the email property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setEmail(String value) {
		this.email = value;
	}


	/**
	 * Gets the value of the title property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getTitle() {
		return this.title;
	}


	/**
	 * Sets the value of the title property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setTitle(String value) {
		this.title = value;
	}


	/**
	 * Gets the value of the mobile property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getMobile() {
		return this.mobile;
	}


	/**
	 * Sets the value of the mobile property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setMobile(String value) {
		this.mobile = value;
	}


	/**
	 * Gets the value of the pager property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getPager() {
		return this.pager;
	}


	/**
	 * Sets the value of the pager property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setPager(String value) {
		this.pager = value;
	}


	/**
	 * Gets the value of the phone property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getPhone() {
		return this.phone;
	}


	/**
	 * Sets the value of the phone property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setPhone(String value) {
		this.phone = value;
	}


	/**
	 * Gets the value of the zipCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getZipCode() {
		return this.zipCode;
	}


	/**
	 * Sets the value of the zipCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setZipCode(String value) {
		this.zipCode = value;
	}


	/**
	 * Gets the value of the state property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getState() {
		return this.state;
	}


	/**
	 * Sets the value of the state property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setState(String value) {
		this.state = value;
	}


	/**
	 * Gets the value of the street property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getStreet() {
		return this.street;
	}


	/**
	 * Sets the value of the street property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setStreet(String value) {
		this.street = value;
	}
}
