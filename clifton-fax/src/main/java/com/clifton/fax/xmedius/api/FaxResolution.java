package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaxResolution.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FaxResolution">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Low"/>
 *     &lt;enumeration value="High"/>
 *     &lt;enumeration value="Fine"/>
 *     &lt;enumeration value="UltraFine"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "FaxResolution")
@XmlEnum
public enum FaxResolution {

	@XmlEnumValue("Low")
	LOW("Low"),
	@XmlEnumValue("High")
	HIGH("High"),
	@XmlEnumValue("Fine")
	FINE("Fine"),
	@XmlEnumValue("UltraFine")
	ULTRA_FINE("UltraFine");
	private final String value;


	FaxResolution(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static FaxResolution fromValue(String v) {
		for (FaxResolution c : FaxResolution.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
