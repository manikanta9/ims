package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaxSecurityOptions complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaxSecurityOptions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="overrideFaxOptions" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="overrideSenderInfo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="attachCoverSheet" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="rightToFax" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxSecurityOptions", propOrder = {
		"overrideFaxOptions",
		"overrideSenderInfo",
		"attachCoverSheet",
		"rightToFax"
})
public class FaxSecurityOptions {

	private boolean overrideFaxOptions;
	private boolean overrideSenderInfo;
	private boolean attachCoverSheet;
	private boolean rightToFax;


	/**
	 * Gets the value of the overrideFaxOptions property.
	 */
	public boolean isOverrideFaxOptions() {
		return this.overrideFaxOptions;
	}


	/**
	 * Sets the value of the overrideFaxOptions property.
	 */
	public void setOverrideFaxOptions(boolean value) {
		this.overrideFaxOptions = value;
	}


	/**
	 * Gets the value of the overrideSenderInfo property.
	 */
	public boolean isOverrideSenderInfo() {
		return this.overrideSenderInfo;
	}


	/**
	 * Sets the value of the overrideSenderInfo property.
	 */
	public void setOverrideSenderInfo(boolean value) {
		this.overrideSenderInfo = value;
	}


	/**
	 * Gets the value of the attachCoverSheet property.
	 */
	public boolean isAttachCoverSheet() {
		return this.attachCoverSheet;
	}


	/**
	 * Sets the value of the attachCoverSheet property.
	 */
	public void setAttachCoverSheet(boolean value) {
		this.attachCoverSheet = value;
	}


	/**
	 * Gets the value of the rightToFax property.
	 */
	public boolean isRightToFax() {
		return this.rightToFax;
	}


	/**
	 * Sets the value of the rightToFax property.
	 */
	public void setRightToFax(boolean value) {
		this.rightToFax = value;
	}
}
