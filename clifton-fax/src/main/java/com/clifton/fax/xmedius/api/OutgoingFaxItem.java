package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OutgoingFaxItem complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OutgoingFaxItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://ws.xm.faxserver.com/}OutgoingStatus" minOccurs="0"/>
 *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modifiedDestination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="broadcastId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originalTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderBillingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="senderSubBillingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientBillingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recipientSubBillingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originalDestination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="priority" type="{http://ws.xm.faxserver.com/}SendingPriority" minOccurs="0"/>
 *         &lt;element name="remoteCsid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="channelNumber" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="attemptCount" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="noOfRetries" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="retryDelay" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="speed" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="submittedTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="completedTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="delayUntil" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="pagesSubmitted" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="pagesSent" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="errorDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingFaxItem", propOrder = {
		"id",
		"status",
		"subject",
		"modifiedDestination",
		"broadcastId",
		"transactionId",
		"originalTransactionId",
		"userId",
		"senderBillingCode",
		"senderSubBillingCode",
		"recipientName",
		"recipientBillingCode",
		"recipientSubBillingCode",
		"originalDestination",
		"priority",
		"remoteCsid",
		"channelNumber",
		"attemptCount",
		"noOfRetries",
		"retryDelay",
		"speed",
		"submittedTime",
		"completedTime",
		"delayUntil",
		"duration",
		"pagesSubmitted",
		"pagesSent",
		"errorCode",
		"errorDescription"
})
@XmlSeeAlso({
		OutgoingFaxItemFull.class
})
public class OutgoingFaxItem {

	@XmlElement(required = true)
	private String id;
	@XmlSchemaType(name = "string")
	private OutgoingStatus status;
	private String subject;
	private String modifiedDestination;
	private String broadcastId;
	private String transactionId;
	private String originalTransactionId;
	private String userId;
	private String senderBillingCode;
	private String senderSubBillingCode;
	private String recipientName;
	private String recipientBillingCode;
	private String recipientSubBillingCode;
	private String originalDestination;
	@XmlSchemaType(name = "string")
	private SendingPriority priority;
	private String remoteCsid;
	@XmlSchemaType(name = "unsignedInt")
	private Long channelNumber;
	@XmlSchemaType(name = "unsignedInt")
	private Long attemptCount;
	@XmlSchemaType(name = "unsignedInt")
	private Long noOfRetries;
	@XmlSchemaType(name = "unsignedInt")
	private Long retryDelay;
	private Long speed;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar submittedTime;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar completedTime;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar delayUntil;
	@XmlSchemaType(name = "unsignedInt")
	private Long duration;
	@XmlSchemaType(name = "unsignedInt")
	private Long pagesSubmitted;
	@XmlSchemaType(name = "unsignedInt")
	private Long pagesSent;
	@XmlSchemaType(name = "unsignedInt")
	private Long errorCode;
	private String errorDescription;


	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getId() {
		return this.id;
	}


	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setId(String value) {
		this.id = value;
	}


	/**
	 * Gets the value of the status property.
	 *
	 * @return possible object is
	 * {@link OutgoingStatus }
	 */
	public OutgoingStatus getStatus() {
		return this.status;
	}


	/**
	 * Sets the value of the status property.
	 *
	 * @param value allowed object is
	 *              {@link OutgoingStatus }
	 */
	public void setStatus(OutgoingStatus value) {
		this.status = value;
	}


	/**
	 * Gets the value of the subject property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSubject() {
		return this.subject;
	}


	/**
	 * Sets the value of the subject property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSubject(String value) {
		this.subject = value;
	}


	/**
	 * Gets the value of the modifiedDestination property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getModifiedDestination() {
		return this.modifiedDestination;
	}


	/**
	 * Sets the value of the modifiedDestination property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setModifiedDestination(String value) {
		this.modifiedDestination = value;
	}


	/**
	 * Gets the value of the broadcastId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBroadcastId() {
		return this.broadcastId;
	}


	/**
	 * Sets the value of the broadcastId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setBroadcastId(String value) {
		this.broadcastId = value;
	}


	/**
	 * Gets the value of the transactionId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getTransactionId() {
		return this.transactionId;
	}


	/**
	 * Sets the value of the transactionId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setTransactionId(String value) {
		this.transactionId = value;
	}


	/**
	 * Gets the value of the originalTransactionId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getOriginalTransactionId() {
		return this.originalTransactionId;
	}


	/**
	 * Sets the value of the originalTransactionId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setOriginalTransactionId(String value) {
		this.originalTransactionId = value;
	}


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the senderBillingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSenderBillingCode() {
		return this.senderBillingCode;
	}


	/**
	 * Sets the value of the senderBillingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSenderBillingCode(String value) {
		this.senderBillingCode = value;
	}


	/**
	 * Gets the value of the senderSubBillingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSenderSubBillingCode() {
		return this.senderSubBillingCode;
	}


	/**
	 * Sets the value of the senderSubBillingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSenderSubBillingCode(String value) {
		this.senderSubBillingCode = value;
	}


	/**
	 * Gets the value of the recipientName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getRecipientName() {
		return this.recipientName;
	}


	/**
	 * Sets the value of the recipientName property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setRecipientName(String value) {
		this.recipientName = value;
	}


	/**
	 * Gets the value of the recipientBillingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getRecipientBillingCode() {
		return this.recipientBillingCode;
	}


	/**
	 * Sets the value of the recipientBillingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setRecipientBillingCode(String value) {
		this.recipientBillingCode = value;
	}


	/**
	 * Gets the value of the recipientSubBillingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getRecipientSubBillingCode() {
		return this.recipientSubBillingCode;
	}


	/**
	 * Sets the value of the recipientSubBillingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setRecipientSubBillingCode(String value) {
		this.recipientSubBillingCode = value;
	}


	/**
	 * Gets the value of the originalDestination property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getOriginalDestination() {
		return this.originalDestination;
	}


	/**
	 * Sets the value of the originalDestination property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setOriginalDestination(String value) {
		this.originalDestination = value;
	}


	/**
	 * Gets the value of the priority property.
	 *
	 * @return possible object is
	 * {@link SendingPriority }
	 */
	public SendingPriority getPriority() {
		return this.priority;
	}


	/**
	 * Sets the value of the priority property.
	 *
	 * @param value allowed object is
	 *              {@link SendingPriority }
	 */
	public void setPriority(SendingPriority value) {
		this.priority = value;
	}


	/**
	 * Gets the value of the remoteCsid property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getRemoteCsid() {
		return this.remoteCsid;
	}


	/**
	 * Sets the value of the remoteCsid property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setRemoteCsid(String value) {
		this.remoteCsid = value;
	}


	/**
	 * Gets the value of the channelNumber property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getChannelNumber() {
		return this.channelNumber;
	}


	/**
	 * Sets the value of the channelNumber property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setChannelNumber(Long value) {
		this.channelNumber = value;
	}


	/**
	 * Gets the value of the attemptCount property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getAttemptCount() {
		return this.attemptCount;
	}


	/**
	 * Sets the value of the attemptCount property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setAttemptCount(Long value) {
		this.attemptCount = value;
	}


	/**
	 * Gets the value of the noOfRetries property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getNoOfRetries() {
		return this.noOfRetries;
	}


	/**
	 * Sets the value of the noOfRetries property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setNoOfRetries(Long value) {
		this.noOfRetries = value;
	}


	/**
	 * Gets the value of the retryDelay property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getRetryDelay() {
		return this.retryDelay;
	}


	/**
	 * Sets the value of the retryDelay property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setRetryDelay(Long value) {
		this.retryDelay = value;
	}


	/**
	 * Gets the value of the speed property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getSpeed() {
		return this.speed;
	}


	/**
	 * Sets the value of the speed property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setSpeed(Long value) {
		this.speed = value;
	}


	/**
	 * Gets the value of the submittedTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getSubmittedTime() {
		return this.submittedTime;
	}


	/**
	 * Sets the value of the submittedTime property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setSubmittedTime(XMLGregorianCalendar value) {
		this.submittedTime = value;
	}


	/**
	 * Gets the value of the completedTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getCompletedTime() {
		return this.completedTime;
	}


	/**
	 * Sets the value of the completedTime property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setCompletedTime(XMLGregorianCalendar value) {
		this.completedTime = value;
	}


	/**
	 * Gets the value of the delayUntil property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getDelayUntil() {
		return this.delayUntil;
	}


	/**
	 * Sets the value of the delayUntil property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setDelayUntil(XMLGregorianCalendar value) {
		this.delayUntil = value;
	}


	/**
	 * Gets the value of the duration property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getDuration() {
		return this.duration;
	}


	/**
	 * Sets the value of the duration property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setDuration(Long value) {
		this.duration = value;
	}


	/**
	 * Gets the value of the pagesSubmitted property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getPagesSubmitted() {
		return this.pagesSubmitted;
	}


	/**
	 * Sets the value of the pagesSubmitted property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setPagesSubmitted(Long value) {
		this.pagesSubmitted = value;
	}


	/**
	 * Gets the value of the pagesSent property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getPagesSent() {
		return this.pagesSent;
	}


	/**
	 * Sets the value of the pagesSent property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setPagesSent(Long value) {
		this.pagesSent = value;
	}


	/**
	 * Gets the value of the errorCode property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getErrorCode() {
		return this.errorCode;
	}


	/**
	 * Sets the value of the errorCode property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setErrorCode(Long value) {
		this.errorCode = value;
	}


	/**
	 * Gets the value of the errorDescription property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getErrorDescription() {
		return this.errorDescription;
	}


	/**
	 * Sets the value of the errorDescription property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setErrorDescription(String value) {
		this.errorDescription = value;
	}
}
