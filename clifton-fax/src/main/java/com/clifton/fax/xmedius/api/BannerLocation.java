package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BannerLocation.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BannerLocation">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NO_BANNER"/>
 *     &lt;enumeration value="BANNER_ON_ALL_PAGES"/>
 *     &lt;enumeration value="BANNER_ON_FIRST_PAGE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "BannerLocation")
@XmlEnum
public enum BannerLocation {

	NO_BANNER,
	BANNER_ON_ALL_PAGES,
	BANNER_ON_FIRST_PAGE;


	public String value() {
		return name();
	}


	public static BannerLocation fromValue(String v) {
		return valueOf(v);
	}
}
