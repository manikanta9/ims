package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SenderInfo complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SenderInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="salutation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pager" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subBillingCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SenderInfo", propOrder = {
		"salutation",
		"firstName",
		"lastName",
		"title",
		"email",
		"mobile",
		"pager",
		"billingCode",
		"subBillingCode",
		"company",
		"address",
		"city",
		"state",
		"country",
		"zipCode",
		"phone",
		"fax"
})
public class SenderInfo {

	private String salutation;
	private String firstName;
	private String lastName;
	private String title;
	private String email;
	private String mobile;
	private String pager;
	private String billingCode;
	private String subBillingCode;
	private String company;
	private String address;
	private String city;
	private String state;
	private String country;
	private String zipCode;
	private String phone;
	private String fax;


	/**
	 * Gets the value of the salutation property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSalutation() {
		return this.salutation;
	}


	/**
	 * Sets the value of the salutation property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSalutation(String value) {
		this.salutation = value;
	}


	/**
	 * Gets the value of the firstName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFirstName() {
		return this.firstName;
	}


	/**
	 * Sets the value of the firstName property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setFirstName(String value) {
		this.firstName = value;
	}


	/**
	 * Gets the value of the lastName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getLastName() {
		return this.lastName;
	}


	/**
	 * Sets the value of the lastName property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setLastName(String value) {
		this.lastName = value;
	}


	/**
	 * Gets the value of the title property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getTitle() {
		return this.title;
	}


	/**
	 * Sets the value of the title property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setTitle(String value) {
		this.title = value;
	}


	/**
	 * Gets the value of the email property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getEmail() {
		return this.email;
	}


	/**
	 * Sets the value of the email property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setEmail(String value) {
		this.email = value;
	}


	/**
	 * Gets the value of the mobile property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getMobile() {
		return this.mobile;
	}


	/**
	 * Sets the value of the mobile property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setMobile(String value) {
		this.mobile = value;
	}


	/**
	 * Gets the value of the pager property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getPager() {
		return this.pager;
	}


	/**
	 * Sets the value of the pager property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setPager(String value) {
		this.pager = value;
	}


	/**
	 * Gets the value of the billingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBillingCode() {
		return this.billingCode;
	}


	/**
	 * Sets the value of the billingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setBillingCode(String value) {
		this.billingCode = value;
	}


	/**
	 * Gets the value of the subBillingCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSubBillingCode() {
		return this.subBillingCode;
	}


	/**
	 * Sets the value of the subBillingCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSubBillingCode(String value) {
		this.subBillingCode = value;
	}


	/**
	 * Gets the value of the company property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCompany() {
		return this.company;
	}


	/**
	 * Sets the value of the company property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCompany(String value) {
		this.company = value;
	}


	/**
	 * Gets the value of the address property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getAddress() {
		return this.address;
	}


	/**
	 * Sets the value of the address property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setAddress(String value) {
		this.address = value;
	}


	/**
	 * Gets the value of the city property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCity() {
		return this.city;
	}


	/**
	 * Sets the value of the city property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCity(String value) {
		this.city = value;
	}


	/**
	 * Gets the value of the state property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getState() {
		return this.state;
	}


	/**
	 * Sets the value of the state property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setState(String value) {
		this.state = value;
	}


	/**
	 * Gets the value of the country property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCountry() {
		return this.country;
	}


	/**
	 * Sets the value of the country property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCountry(String value) {
		this.country = value;
	}


	/**
	 * Gets the value of the zipCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getZipCode() {
		return this.zipCode;
	}


	/**
	 * Sets the value of the zipCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setZipCode(String value) {
		this.zipCode = value;
	}


	/**
	 * Gets the value of the phone property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getPhone() {
		return this.phone;
	}


	/**
	 * Sets the value of the phone property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setPhone(String value) {
		this.phone = value;
	}


	/**
	 * Gets the value of the fax property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFax() {
		return this.fax;
	}


	/**
	 * Sets the value of the fax property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setFax(String value) {
		this.fax = value;
	}
}
