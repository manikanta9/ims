package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InboundFaxItem complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InboundFaxItem">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="receivedTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="dnisOrDid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="remoteCsid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ani" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="archivedTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="channelNumber" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="dtmf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="localCsid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pagesReceived" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/>
 *         &lt;element name="siteName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="speed" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="status" type="{http://ws.xm.faxserver.com/}InboundStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InboundFaxItem", propOrder = {
		"id",
		"receivedTime",
		"duration",
		"dnisOrDid",
		"remoteCsid",
		"ani",
		"archivedTime",
		"channelNumber",
		"dtmf",
		"errorCode",
		"localCsid",
		"errorDescription",
		"pagesReceived",
		"siteName",
		"speed",
		"transactionId",
		"status"
})
@XmlSeeAlso({
		InboundUserFaxItem.class
})
public class InboundFaxItem {

	@XmlElement(required = true)
	private String id;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar receivedTime;
	@XmlSchemaType(name = "unsignedInt")
	private Long duration;
	private String dnisOrDid;
	private String remoteCsid;
	private String ani;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar archivedTime;
	@XmlSchemaType(name = "unsignedInt")
	private Long channelNumber;
	private String dtmf;
	@XmlSchemaType(name = "unsignedInt")
	private Long errorCode;
	private String localCsid;
	private String errorDescription;
	@XmlSchemaType(name = "unsignedInt")
	private Long pagesReceived;
	private String siteName;
	private Long speed;
	private String transactionId;
	@XmlSchemaType(name = "string")
	private InboundStatus status;


	/**
	 * Gets the value of the id property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getId() {
		return this.id;
	}


	/**
	 * Sets the value of the id property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setId(String value) {
		this.id = value;
	}


	/**
	 * Gets the value of the receivedTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getReceivedTime() {
		return this.receivedTime;
	}


	/**
	 * Sets the value of the receivedTime property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setReceivedTime(XMLGregorianCalendar value) {
		this.receivedTime = value;
	}


	/**
	 * Gets the value of the duration property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getDuration() {
		return this.duration;
	}


	/**
	 * Sets the value of the duration property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setDuration(Long value) {
		this.duration = value;
	}


	/**
	 * Gets the value of the dnisOrDid property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getDnisOrDid() {
		return this.dnisOrDid;
	}


	/**
	 * Sets the value of the dnisOrDid property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setDnisOrDid(String value) {
		this.dnisOrDid = value;
	}


	/**
	 * Gets the value of the remoteCsid property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getRemoteCsid() {
		return this.remoteCsid;
	}


	/**
	 * Sets the value of the remoteCsid property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setRemoteCsid(String value) {
		this.remoteCsid = value;
	}


	/**
	 * Gets the value of the ani property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getAni() {
		return this.ani;
	}


	/**
	 * Sets the value of the ani property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setAni(String value) {
		this.ani = value;
	}


	/**
	 * Gets the value of the archivedTime property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getArchivedTime() {
		return this.archivedTime;
	}


	/**
	 * Sets the value of the archivedTime property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setArchivedTime(XMLGregorianCalendar value) {
		this.archivedTime = value;
	}


	/**
	 * Gets the value of the channelNumber property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getChannelNumber() {
		return this.channelNumber;
	}


	/**
	 * Sets the value of the channelNumber property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setChannelNumber(Long value) {
		this.channelNumber = value;
	}


	/**
	 * Gets the value of the dtmf property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getDtmf() {
		return this.dtmf;
	}


	/**
	 * Sets the value of the dtmf property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setDtmf(String value) {
		this.dtmf = value;
	}


	/**
	 * Gets the value of the errorCode property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getErrorCode() {
		return this.errorCode;
	}


	/**
	 * Sets the value of the errorCode property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setErrorCode(Long value) {
		this.errorCode = value;
	}


	/**
	 * Gets the value of the localCsid property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getLocalCsid() {
		return this.localCsid;
	}


	/**
	 * Sets the value of the localCsid property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setLocalCsid(String value) {
		this.localCsid = value;
	}


	/**
	 * Gets the value of the errorDescription property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getErrorDescription() {
		return this.errorDescription;
	}


	/**
	 * Sets the value of the errorDescription property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setErrorDescription(String value) {
		this.errorDescription = value;
	}


	/**
	 * Gets the value of the pagesReceived property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getPagesReceived() {
		return this.pagesReceived;
	}


	/**
	 * Sets the value of the pagesReceived property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setPagesReceived(Long value) {
		this.pagesReceived = value;
	}


	/**
	 * Gets the value of the siteName property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSiteName() {
		return this.siteName;
	}


	/**
	 * Sets the value of the siteName property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSiteName(String value) {
		this.siteName = value;
	}


	/**
	 * Gets the value of the speed property.
	 *
	 * @return possible object is
	 * {@link Long }
	 */
	public Long getSpeed() {
		return this.speed;
	}


	/**
	 * Sets the value of the speed property.
	 *
	 * @param value allowed object is
	 *              {@link Long }
	 */
	public void setSpeed(Long value) {
		this.speed = value;
	}


	/**
	 * Gets the value of the transactionId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getTransactionId() {
		return this.transactionId;
	}


	/**
	 * Sets the value of the transactionId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setTransactionId(String value) {
		this.transactionId = value;
	}


	/**
	 * Gets the value of the status property.
	 *
	 * @return possible object is
	 * {@link InboundStatus }
	 */
	public InboundStatus getStatus() {
		return this.status;
	}


	/**
	 * Sets the value of the status property.
	 *
	 * @param value allowed object is
	 *              {@link InboundStatus }
	 */
	public void setStatus(InboundStatus value) {
		this.status = value;
	}
}
