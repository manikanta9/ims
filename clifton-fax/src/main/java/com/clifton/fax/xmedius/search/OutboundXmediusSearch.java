package com.clifton.fax.xmedius.search;

import com.clifton.fax.xmedius.api.OutboundSortInfo;
import com.clifton.fax.xmedius.api.OutboundUserKeywordInfo;
import com.clifton.fax.xmedius.api.OutboundUserSearchInfo;


public class OutboundXmediusSearch extends XmediusSearch {

	private OutboundUserSearchInfo outboundUserSearchInfo;
	private OutboundSortInfo outboundSortInfo;
	private OutboundUserKeywordInfo outboundUserKeywordInfo;


	public OutboundUserSearchInfo getOutboundUserSearchInfo() {
		return this.outboundUserSearchInfo;
	}


	public void setOutboundUserSearchInfo(OutboundUserSearchInfo outboundUserSearchInfo) {
		this.outboundUserSearchInfo = outboundUserSearchInfo;
	}


	public OutboundSortInfo getOutboundSortInfo() {
		return this.outboundSortInfo;
	}


	public void setOutboundSortInfo(OutboundSortInfo outboundSortInfo) {
		this.outboundSortInfo = outboundSortInfo;
	}


	public OutboundUserKeywordInfo getOutboundUserKeywordInfo() {
		return this.outboundUserKeywordInfo;
	}


	public void setOutboundUserKeywordInfo(OutboundUserKeywordInfo outboundUserKeywordInfo) {
		this.outboundUserKeywordInfo = outboundUserKeywordInfo;
	}
}
