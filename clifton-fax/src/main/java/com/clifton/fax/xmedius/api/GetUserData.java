package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUserData complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getUserData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUserData", propOrder = {
		"userId",
		"dataId"
})
public class GetUserData {

	private String userId;
	private String dataId;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the dataId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getDataId() {
		return this.dataId;
	}


	/**
	 * Sets the value of the dataId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setDataId(String value) {
		this.dataId = value;
	}
}
