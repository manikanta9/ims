package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Version complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Version">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="product" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="webservices" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Version", propOrder = {
		"product",
		"webservices"
})
public class Version {

	@XmlElement(required = true)
	private String product;
	@XmlElement(required = true)
	private String webservices;


	/**
	 * Gets the value of the product property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getProduct() {
		return this.product;
	}


	/**
	 * Sets the value of the product property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setProduct(String value) {
		this.product = value;
	}


	/**
	 * Gets the value of the webservices property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getWebservices() {
		return this.webservices;
	}


	/**
	 * Sets the value of the webservices property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setWebservices(String value) {
		this.webservices = value;
	}
}
