package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for findContacts complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="findContacts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="searchQuery" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="maxCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findContacts", propOrder = {
		"userId",
		"searchQuery",
		"maxCount"
})
public class FindContacts {

	private String userId;
	private String searchQuery;
	private int maxCount;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the searchQuery property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSearchQuery() {
		return this.searchQuery;
	}


	/**
	 * Sets the value of the searchQuery property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSearchQuery(String value) {
		this.searchQuery = value;
	}


	/**
	 * Gets the value of the maxCount property.
	 */
	public int getMaxCount() {
		return this.maxCount;
	}


	/**
	 * Sets the value of the maxCount property.
	 */
	public void setMaxCount(int value) {
		this.maxCount = value;
	}
}
