package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttachedFile complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AttachedFile">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice>
 *           &lt;element name="contents" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *           &lt;element name="contentsUri" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachedFile", propOrder = {
		"name",
		"contents",
		"contentsUri"
})
public class AttachedFile {

	@XmlElement(required = true)
	private String name;
	private byte[] contents;
	private String contentsUri;


	/**
	 * Gets the value of the name property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * Sets the value of the name property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setName(String value) {
		this.name = value;
	}


	/**
	 * Gets the value of the contents property.
	 *
	 * @return possible object is
	 * byte[]
	 */
	public byte[] getContents() {
		return this.contents;
	}


	/**
	 * Sets the value of the contents property.
	 *
	 * @param value allowed object is
	 *              byte[]
	 */
	public void setContents(byte[] value) {
		this.contents = value;
	}


	/**
	 * Gets the value of the contentsUri property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getContentsUri() {
		return this.contentsUri;
	}


	/**
	 * Sets the value of the contentsUri property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setContentsUri(String value) {
		this.contentsUri = value;
	}
}
