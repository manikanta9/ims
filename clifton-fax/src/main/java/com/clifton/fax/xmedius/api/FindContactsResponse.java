package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for findContactsResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="findContactsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.xm.faxserver.com/}PhonebookSearchResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findContactsResponse", propOrder = {
		"_return"
})
public class FindContactsResponse {

	@XmlElement(name = "return")
	private PhonebookSearchResponse _return;


	/**
	 * Gets the value of the return property.
	 *
	 * @return possible object is
	 * {@link PhonebookSearchResponse }
	 */
	public PhonebookSearchResponse getReturn() {
		return this._return;
	}


	/**
	 * Sets the value of the return property.
	 *
	 * @param value allowed object is
	 *              {@link PhonebookSearchResponse }
	 */
	public void setReturn(PhonebookSearchResponse value) {
		this._return = value;
	}
}
