package com.clifton.fax.xmedius.search;

import com.clifton.fax.xmedius.api.InboundSortInfo;
import com.clifton.fax.xmedius.api.InboundUserKeywordInfo;
import com.clifton.fax.xmedius.api.InboundUserSearchInfo;


public class InboundXmediusSearch extends XmediusSearch {

	private InboundSortInfo inboundSortInfo;
	private InboundUserSearchInfo inboundUserSearchInfo;
	private InboundUserKeywordInfo inboundUserKeywordInfo;


	public InboundSortInfo getInboundSortInfo() {
		return this.inboundSortInfo;
	}


	public void setInboundSortInfo(InboundSortInfo inboundSortInfo) {
		this.inboundSortInfo = inboundSortInfo;
	}


	public InboundUserSearchInfo getInboundUserSearchInfo() {
		return this.inboundUserSearchInfo;
	}


	public void setInboundUserSearchInfo(InboundUserSearchInfo inboundUserSearchInfo) {
		this.inboundUserSearchInfo = inboundUserSearchInfo;
	}


	public InboundUserKeywordInfo getInboundUserKeywordInfo() {
		return this.inboundUserKeywordInfo;
	}


	public void setInboundUserKeywordInfo(InboundUserKeywordInfo inboundUserKeywordInfo) {
		this.inboundUserKeywordInfo = inboundUserKeywordInfo;
	}
}
