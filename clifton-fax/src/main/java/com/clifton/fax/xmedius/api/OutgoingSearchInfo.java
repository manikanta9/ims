package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutgoingSearchInfo complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OutgoingSearchInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="priority" type="{http://ws.xm.faxserver.com/}SendingPriority" minOccurs="0"/>
 *         &lt;element name="originalDestination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modifiedDestination" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="broadcastId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originalTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="submittedTime" type="{http://ws.xm.faxserver.com/}DateTimeRange" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutgoingSearchInfo", propOrder = {
		"priority",
		"originalDestination",
		"modifiedDestination",
		"broadcastId",
		"originalTransactionId",
		"transactionId",
		"userId",
		"submittedTime"
})
public class OutgoingSearchInfo {

	@XmlSchemaType(name = "string")
	private SendingPriority priority;
	private String originalDestination;
	private String modifiedDestination;
	private String broadcastId;
	private String originalTransactionId;
	private String transactionId;
	private String userId;
	private DateTimeRange submittedTime;


	/**
	 * Gets the value of the priority property.
	 *
	 * @return possible object is
	 * {@link SendingPriority }
	 */
	public SendingPriority getPriority() {
		return this.priority;
	}


	/**
	 * Sets the value of the priority property.
	 *
	 * @param value allowed object is
	 *              {@link SendingPriority }
	 */
	public void setPriority(SendingPriority value) {
		this.priority = value;
	}


	/**
	 * Gets the value of the originalDestination property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getOriginalDestination() {
		return this.originalDestination;
	}


	/**
	 * Sets the value of the originalDestination property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setOriginalDestination(String value) {
		this.originalDestination = value;
	}


	/**
	 * Gets the value of the modifiedDestination property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getModifiedDestination() {
		return this.modifiedDestination;
	}


	/**
	 * Sets the value of the modifiedDestination property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setModifiedDestination(String value) {
		this.modifiedDestination = value;
	}


	/**
	 * Gets the value of the broadcastId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getBroadcastId() {
		return this.broadcastId;
	}


	/**
	 * Sets the value of the broadcastId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setBroadcastId(String value) {
		this.broadcastId = value;
	}


	/**
	 * Gets the value of the originalTransactionId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getOriginalTransactionId() {
		return this.originalTransactionId;
	}


	/**
	 * Sets the value of the originalTransactionId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setOriginalTransactionId(String value) {
		this.originalTransactionId = value;
	}


	/**
	 * Gets the value of the transactionId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getTransactionId() {
		return this.transactionId;
	}


	/**
	 * Sets the value of the transactionId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setTransactionId(String value) {
		this.transactionId = value;
	}


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the submittedTime property.
	 *
	 * @return possible object is
	 * {@link DateTimeRange }
	 */
	public DateTimeRange getSubmittedTime() {
		return this.submittedTime;
	}


	/**
	 * Sets the value of the submittedTime property.
	 *
	 * @param value allowed object is
	 *              {@link DateTimeRange }
	 */
	public void setSubmittedTime(DateTimeRange value) {
		this.submittedTime = value;
	}
}
