package com.clifton.fax.xmedius.api;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import java.util.List;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 */
@WebService(name = "FaxService", targetNamespace = "http://ws.xm.faxserver.com/")
@XmlSeeAlso({
		ObjectFactory.class
})
public interface FaxService {


	/**
	 * @return returns com.clifton.fax.xmedius.api.Version
	 */
	@WebMethod
	@WebResult(name = "version", targetNamespace = "")
	@RequestWrapper(localName = "getVersion", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetVersion")
	@ResponseWrapper(localName = "getVersionResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetVersionResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getVersionRequest", output = "http://ws.xm.faxserver.com/FaxService/getVersionResponse")
	public Version getVersion();


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "cancel", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.Cancel")
	@ResponseWrapper(localName = "cancelResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.CancelResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/cancelRequest", output = "http://ws.xm.faxserver.com/FaxService/cancelResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/cancel/Fault/FaxException")
	})
	public void cancel(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param dataId
	 * @param userId
	 * @return returns java.lang.String
	 */
	@WebMethod
	@WebResult(name = "data", targetNamespace = "")
	@RequestWrapper(localName = "getUserData", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetUserData")
	@ResponseWrapper(localName = "getUserDataResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetUserDataResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getUserDataRequest", output = "http://ws.xm.faxserver.com/FaxService/getUserDataResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getUserData/Fault/FaxException")
	})
	public String getUserData(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "dataId", targetNamespace = "")
					String dataId)
			throws FaxException_Exception
	;


	/**
	 * @param dataId
	 * @param data
	 * @param userId
	 */
	@WebMethod
	@RequestWrapper(localName = "setUserData", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SetUserData")
	@ResponseWrapper(localName = "setUserDataResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SetUserDataResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/setUserDataRequest", output = "http://ws.xm.faxserver.com/FaxService/setUserDataResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/setUserData/Fault/FaxException")
	})
	public void setUserData(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "dataId", targetNamespace = "")
					String dataId,
			@WebParam(name = "data", targetNamespace = "")
					String data)
			throws FaxException_Exception
	;


	/**
	 * @param userId
	 * @return returns com.clifton.fax.xmedius.api.UserProfile
	 */
	@WebMethod
	@WebResult(name = "userProfile", targetNamespace = "")
	@RequestWrapper(localName = "getUserProfile", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetUserProfile")
	@ResponseWrapper(localName = "getUserProfileResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetUserProfileResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getUserProfileRequest", output = "http://ws.xm.faxserver.com/FaxService/getUserProfileResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getUserProfile/Fault/FaxException")
	})
	public UserProfile getUserProfile(
			@WebParam(name = "userId", targetNamespace = "")
					String userId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "markAsUndeleted", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsUndeleted")
	@ResponseWrapper(localName = "markAsUndeletedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsUndeletedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/markAsUndeletedRequest", output = "http://ws.xm.faxserver.com/FaxService/markAsUndeletedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/markAsUndeleted/Fault/FaxException")
	})
	public void markAsUndeleted(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param senderInfo
	 * @param attachment
	 * @param subject
	 * @param recipient
	 * @param coverPage
	 * @param comment
	 * @param faxOptions
	 * @param delayUntil
	 * @param userId
	 * @return returns com.clifton.fax.xmedius.api.FaxResult
	 */
	@WebMethod
	@WebResult(name = "result", targetNamespace = "")
	@RequestWrapper(localName = "sendFaxLater", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SendFaxLater")
	@ResponseWrapper(localName = "sendFaxLaterResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SendFaxLaterResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/sendFaxLaterRequest", output = "http://ws.xm.faxserver.com/FaxService/sendFaxLaterResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/sendFaxLater/Fault/FaxException")
	})
	public FaxResult sendFaxLater(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "subject", targetNamespace = "")
					String subject,
			@WebParam(name = "comment", targetNamespace = "")
					String comment,
			@WebParam(name = "recipient", targetNamespace = "")
					List<FaxRecipient> recipient,
			@WebParam(name = "attachment", targetNamespace = "")
					List<AttachedFile> attachment,
			@WebParam(name = "coverPage", targetNamespace = "")
					String coverPage,
			@WebParam(name = "senderInfo", targetNamespace = "")
					SenderInfo senderInfo,
			@WebParam(name = "faxOptions", targetNamespace = "")
					FaxOptions faxOptions,
			@WebParam(name = "delayUntil", targetNamespace = "")
					Object delayUntil)
			throws FaxException_Exception
	;


	/**
	 * @param startIndex
	 * @param sortInfo
	 * @param userId
	 * @param maxCount
	 * @return returns com.clifton.fax.xmedius.api.InboundUserFaxResponse
	 */
	@WebMethod
	@WebResult(name = "response", targetNamespace = "")
	@RequestWrapper(localName = "findInboundFaxes", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindInboundFaxes")
	@ResponseWrapper(localName = "findInboundFaxesResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindInboundFaxesResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/findInboundFaxesRequest", output = "http://ws.xm.faxserver.com/FaxService/findInboundFaxesResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/findInboundFaxes/Fault/FaxException")
	})
	public InboundUserFaxResponse findInboundFaxes(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "startIndex", targetNamespace = "")
					int startIndex,
			@WebParam(name = "maxCount", targetNamespace = "")
					int maxCount,
			@WebParam(name = "sortInfo", targetNamespace = "")
					InboundSortInfo sortInfo)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 * @return returns com.clifton.fax.xmedius.api.InboundUserFaxItemFull
	 */
	@WebMethod
	@WebResult(name = "fax", targetNamespace = "")
	@RequestWrapper(localName = "getInboundFax", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetInboundFax")
	@ResponseWrapper(localName = "getInboundFaxResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetInboundFaxResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getInboundFaxRequest", output = "http://ws.xm.faxserver.com/FaxService/getInboundFaxResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getInboundFax/Fault/FaxException")
	})
	public InboundUserFaxItemFull getInboundFax(
			@WebParam(name = "faxId", targetNamespace = "")
					String faxId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 * @return returns com.clifton.fax.xmedius.api.OutgoingFaxItemFull
	 */
	@WebMethod
	@WebResult(name = "fax", targetNamespace = "")
	@RequestWrapper(localName = "getOutgoingFax", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetOutgoingFax")
	@ResponseWrapper(localName = "getOutgoingFaxResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetOutgoingFaxResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getOutgoingFaxRequest", output = "http://ws.xm.faxserver.com/FaxService/getOutgoingFaxResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getOutgoingFax/Fault/FaxException")
	})
	public OutgoingFaxItemFull getOutgoingFax(
			@WebParam(name = "faxId", targetNamespace = "")
					String faxId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 * @param destination
	 * @return returns java.util.List<com.clifton.fax.xmedius.api.FaxResult>
	 */
	@WebMethod
	@WebResult(targetNamespace = "")
	@RequestWrapper(localName = "resubmitTo", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.ResubmitTo")
	@ResponseWrapper(localName = "resubmitToResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.ResubmitToResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/resubmitToRequest", output = "http://ws.xm.faxserver.com/FaxService/resubmitToResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/resubmitTo/Fault/FaxException")
	})
	public List<FaxResult> resubmitTo(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId,
			@WebParam(name = "destination", targetNamespace = "")
					String destination)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "markAsUnviewed", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsUnviewed")
	@ResponseWrapper(localName = "markAsUnviewedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsUnviewedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/markAsUnviewedRequest", output = "http://ws.xm.faxserver.com/FaxService/markAsUnviewedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/markAsUnviewed/Fault/FaxException")
	})
	public void markAsUnviewed(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 * @return returns com.clifton.fax.xmedius.api.OutboundUserFaxItemFull
	 */
	@WebMethod
	@WebResult(name = "fax", targetNamespace = "")
	@RequestWrapper(localName = "getOutboundFax", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetOutboundFax")
	@ResponseWrapper(localName = "getOutboundFaxResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetOutboundFaxResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getOutboundFaxRequest", output = "http://ws.xm.faxserver.com/FaxService/getOutboundFaxResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getOutboundFax/Fault/FaxException")
	})
	public OutboundUserFaxItemFull getOutboundFax(
			@WebParam(name = "faxId", targetNamespace = "")
					String faxId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "markAsViewed", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsViewed")
	@ResponseWrapper(localName = "markAsViewedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsViewedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/markAsViewedRequest", output = "http://ws.xm.faxserver.com/FaxService/markAsViewedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/markAsViewed/Fault/FaxException")
	})
	public void markAsViewed(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 * @param destination
	 * @param delayUntil
	 * @return returns java.util.List<com.clifton.fax.xmedius.api.FaxResult>
	 */
	@WebMethod
	@WebResult(targetNamespace = "")
	@RequestWrapper(localName = "resubmitLater", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.ResubmitLater")
	@ResponseWrapper(localName = "resubmitLaterResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.ResubmitLaterResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/resubmitLaterRequest", output = "http://ws.xm.faxserver.com/FaxService/resubmitLaterResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/resubmitLater/Fault/FaxException")
	})
	public List<FaxResult> resubmitLater(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId,
			@WebParam(name = "destination", targetNamespace = "")
					String destination,
			@WebParam(name = "delayUntil", targetNamespace = "")
					Object delayUntil)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "markAsDeleted", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsDeleted")
	@ResponseWrapper(localName = "markAsDeletedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.MarkAsDeletedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/markAsDeletedRequest", output = "http://ws.xm.faxserver.com/FaxService/markAsDeletedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/markAsDeleted/Fault/FaxException")
	})
	public void markAsDeleted(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param searchQuery
	 * @param userId
	 * @param maxCount
	 * @return returns com.clifton.fax.xmedius.api.PhonebookSearchResponse
	 */
	@WebMethod
	@WebResult(targetNamespace = "")
	@RequestWrapper(localName = "findContacts", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindContacts")
	@ResponseWrapper(localName = "findContactsResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindContactsResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/findContactsRequest", output = "http://ws.xm.faxserver.com/FaxService/findContactsResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/findContacts/Fault/FaxException")
	})
	public PhonebookSearchResponse findContacts(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "searchQuery", targetNamespace = "")
					String searchQuery,
			@WebParam(name = "maxCount", targetNamespace = "")
					int maxCount)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 * @return returns com.clifton.fax.xmedius.api.AuditLogResponse
	 */
	@WebMethod
	@WebResult(name = "response", targetNamespace = "")
	@RequestWrapper(localName = "getAuditLogs", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetAuditLogs")
	@ResponseWrapper(localName = "getAuditLogsResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetAuditLogsResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getAuditLogsRequest", output = "http://ws.xm.faxserver.com/FaxService/getAuditLogsResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getAuditLogs/Fault/FaxException")
	})
	public AuditLogResponse getAuditLogs(
			@WebParam(name = "faxId", targetNamespace = "")
					String faxId)
			throws FaxException_Exception
	;


	/**
	 * @param contact
	 * @param userId
	 */
	@WebMethod
	@RequestWrapper(localName = "addContact", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.AddContact")
	@ResponseWrapper(localName = "addContactResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.AddContactResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/addContactRequest", output = "http://ws.xm.faxserver.com/FaxService/addContactResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/addContact/Fault/FaxException")
	})
	public void addContact(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "contact", targetNamespace = "")
					PhonebookPerson contact)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 * @return returns java.util.List<com.clifton.fax.xmedius.api.FaxResult>
	 */
	@WebMethod
	@WebResult(targetNamespace = "")
	@RequestWrapper(localName = "resubmit", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.Resubmit")
	@ResponseWrapper(localName = "resubmitResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.ResubmitResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/resubmitRequest", output = "http://ws.xm.faxserver.com/FaxService/resubmitResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/resubmit/Fault/FaxException")
	})
	public List<FaxResult> resubmit(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "retrySendingNow", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.RetrySendingNow")
	@ResponseWrapper(localName = "retrySendingNowResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.RetrySendingNowResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/retrySendingNowRequest", output = "http://ws.xm.faxserver.com/FaxService/retrySendingNowResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/retrySendingNow/Fault/FaxException")
	})
	public void retrySendingNow(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "cancelBroadcast", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.CancelBroadcast")
	@ResponseWrapper(localName = "cancelBroadcastResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.CancelBroadcastResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/cancelBroadcastRequest", output = "http://ws.xm.faxserver.com/FaxService/cancelBroadcastResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/cancelBroadcast/Fault/FaxException")
	})
	public void cancelBroadcast(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param senderInfo
	 * @param attachment
	 * @param subject
	 * @param recipient
	 * @param coverPage
	 * @param comment
	 * @param faxOptions
	 * @param userId
	 * @return returns com.clifton.fax.xmedius.api.FaxResult
	 */
	@WebMethod
	@WebResult(name = "result", targetNamespace = "")
	@RequestWrapper(localName = "sendFax", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SendFax")
	@ResponseWrapper(localName = "sendFaxResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SendFaxResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/sendFaxRequest", output = "http://ws.xm.faxserver.com/FaxService/sendFaxResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/sendFax/Fault/FaxException")
	})
	public FaxResult sendFax(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "subject", targetNamespace = "")
					String subject,
			@WebParam(name = "comment", targetNamespace = "")
					String comment,
			@WebParam(name = "recipient", targetNamespace = "")
					List<FaxRecipient> recipient,
			@WebParam(name = "attachment", targetNamespace = "")
					List<AttachedFile> attachment,
			@WebParam(name = "coverPage", targetNamespace = "")
					String coverPage,
			@WebParam(name = "senderInfo", targetNamespace = "")
					SenderInfo senderInfo,
			@WebParam(name = "faxOptions", targetNamespace = "")
					FaxOptions faxOptions)
			throws FaxException_Exception
	;


	/**
	 * @param note
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "saveNote", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SaveNote")
	@ResponseWrapper(localName = "saveNoteResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.SaveNoteResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/saveNoteRequest", output = "http://ws.xm.faxserver.com/FaxService/saveNoteResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/saveNote/Fault/FaxException")
	})
	public void saveNote(
			@WebParam(name = "faxId", targetNamespace = "")
					String faxId,
			@WebParam(name = "note", targetNamespace = "")
					String note)
			throws FaxException_Exception
	;


	/**
	 * @param startIndex
	 * @param userId
	 * @param maxCount
	 * @return returns com.clifton.fax.xmedius.api.OutgoingFaxResponse
	 */
	@WebMethod
	@WebResult(name = "response", targetNamespace = "")
	@RequestWrapper(localName = "findOutgoingFaxes", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutgoingFaxes")
	@ResponseWrapper(localName = "findOutgoingFaxesResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutgoingFaxesResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/findOutgoingFaxesRequest", output = "http://ws.xm.faxserver.com/FaxService/findOutgoingFaxesResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/findOutgoingFaxes/Fault/FaxException")
	})
	public OutgoingFaxResponse findOutgoingFaxes(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "startIndex", targetNamespace = "")
					int startIndex,
			@WebParam(name = "maxCount", targetNamespace = "")
					int maxCount)
			throws FaxException_Exception
	;


	/**
	 * @param faxId
	 */
	@WebMethod
	@RequestWrapper(localName = "deletePermanently", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.DeletePermanently")
	@ResponseWrapper(localName = "deletePermanentlyResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.DeletePermanentlyResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/deletePermanentlyRequest", output = "http://ws.xm.faxserver.com/FaxService/deletePermanentlyResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/deletePermanently/Fault/FaxException")
	})
	public void deletePermanently(
			@WebParam(name = "faxId", targetNamespace = "")
					List<String> faxId)
			throws FaxException_Exception
	;


	/**
	 * @param password
	 * @param otp
	 * @param userId
	 * @return returns boolean
	 */
	@WebMethod
	@WebResult(name = "verification", targetNamespace = "")
	@RequestWrapper(localName = "verifyUserPassword", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.VerifyUserPassword")
	@ResponseWrapper(localName = "verifyUserPasswordResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.VerifyUserPasswordResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/verifyUserPasswordRequest", output = "http://ws.xm.faxserver.com/FaxService/verifyUserPasswordResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/verifyUserPassword/Fault/FaxException")
	})
	public boolean verifyUserPassword(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "password", targetNamespace = "")
					String password,
			@WebParam(name = "otp", targetNamespace = "")
					String otp)
			throws FaxException_Exception
	;


	/**
	 * @param startIndex
	 * @param sortInfo
	 * @param userId
	 * @param maxCount
	 * @return returns com.clifton.fax.xmedius.api.OutboundUserFaxResponse
	 */
	@WebMethod
	@WebResult(name = "response", targetNamespace = "")
	@RequestWrapper(localName = "findOutboundFaxes", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutboundFaxes")
	@ResponseWrapper(localName = "findOutboundFaxesResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutboundFaxesResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/findOutboundFaxesRequest", output = "http://ws.xm.faxserver.com/FaxService/findOutboundFaxesResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/findOutboundFaxes/Fault/FaxException")
	})
	public OutboundUserFaxResponse findOutboundFaxes(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "startIndex", targetNamespace = "")
					int startIndex,
			@WebParam(name = "maxCount", targetNamespace = "")
					int maxCount,
			@WebParam(name = "sortInfo", targetNamespace = "")
					OutboundSortInfo sortInfo)
			throws FaxException_Exception
	;


	/**
	 * @param faxFormat
	 * @param bannerLocation
	 * @param faxId
	 * @param timezone
	 * @param maxPages
	 * @param locale
	 * @return returns com.clifton.fax.xmedius.api.OutboundUserFaxItemFull
	 */
	@WebMethod
	@WebResult(name = "fax", targetNamespace = "")
	@RequestWrapper(localName = "getOutboundFaxAdvanced", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetOutboundFaxAdvanced")
	@ResponseWrapper(localName = "getOutboundFaxAdvancedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetOutboundFaxAdvancedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getOutboundFaxAdvancedRequest", output = "http://ws.xm.faxserver.com/FaxService/getOutboundFaxAdvancedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getOutboundFaxAdvanced/Fault/FaxException")
	})
	public OutboundUserFaxItemFull getOutboundFaxAdvanced(
			@WebParam(name = "faxId", targetNamespace = "")
					String faxId,
			@WebParam(name = "faxFormat", targetNamespace = "")
					FaxFormat faxFormat,
			@WebParam(name = "maxPages", targetNamespace = "")
					Integer maxPages,
			@WebParam(name = "bannerLocation", targetNamespace = "")
					BannerLocation bannerLocation,
			@WebParam(name = "locale", targetNamespace = "")
					String locale,
			@WebParam(name = "timezone", targetNamespace = "")
					String timezone)
			throws FaxException_Exception
	;


	/**
	 * @param faxFormat
	 * @param bannerLocation
	 * @param faxId
	 * @param timezone
	 * @param maxPages
	 * @param locale
	 * @return returns com.clifton.fax.xmedius.api.InboundUserFaxItemFull
	 */
	@WebMethod
	@WebResult(name = "fax", targetNamespace = "")
	@RequestWrapper(localName = "getInboundFaxAdvanced", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetInboundFaxAdvanced")
	@ResponseWrapper(localName = "getInboundFaxAdvancedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetInboundFaxAdvancedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getInboundFaxAdvancedRequest", output = "http://ws.xm.faxserver.com/FaxService/getInboundFaxAdvancedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getInboundFaxAdvanced/Fault/FaxException")
	})
	public InboundUserFaxItemFull getInboundFaxAdvanced(
			@WebParam(name = "faxId", targetNamespace = "")
					String faxId,
			@WebParam(name = "faxFormat", targetNamespace = "")
					FaxFormat faxFormat,
			@WebParam(name = "maxPages", targetNamespace = "")
					Integer maxPages,
			@WebParam(name = "bannerLocation", targetNamespace = "")
					BannerLocation bannerLocation,
			@WebParam(name = "locale", targetNamespace = "")
					String locale,
			@WebParam(name = "timezone", targetNamespace = "")
					String timezone)
			throws FaxException_Exception
	;


	/**
	 * @param startIndex
	 * @param keywordInfo
	 * @param sortInfo
	 * @param searchInfo
	 * @param userId
	 * @param maxCount
	 * @return returns com.clifton.fax.xmedius.api.InboundUserFaxResponse
	 */
	@WebMethod
	@WebResult(name = "response", targetNamespace = "")
	@RequestWrapper(localName = "findInboundFaxesAdvanced", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindInboundFaxesAdvanced")
	@ResponseWrapper(localName = "findInboundFaxesAdvancedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindInboundFaxesAdvancedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/findInboundFaxesAdvancedRequest", output = "http://ws.xm.faxserver.com/FaxService/findInboundFaxesAdvancedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/findInboundFaxesAdvanced/Fault/FaxException")
	})
	public InboundUserFaxResponse findInboundFaxesAdvanced(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "startIndex", targetNamespace = "")
					int startIndex,
			@WebParam(name = "maxCount", targetNamespace = "")
					int maxCount,
			@WebParam(name = "sortInfo", targetNamespace = "")
					InboundSortInfo sortInfo,
			@WebParam(name = "searchInfo", targetNamespace = "")
					InboundUserSearchInfo searchInfo,
			@WebParam(name = "keywordInfo", targetNamespace = "")
					InboundUserKeywordInfo keywordInfo)
			throws FaxException_Exception
	;


	/**
	 * @param startIndex
	 * @param keywordInfo
	 * @param sortInfo
	 * @param searchInfo
	 * @param userId
	 * @param maxCount
	 * @return returns com.clifton.fax.xmedius.api.OutboundUserFaxResponse
	 */
	@WebMethod
	@WebResult(name = "response", targetNamespace = "")
	@RequestWrapper(localName = "findOutboundFaxesAdvanced", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutboundFaxesAdvanced")
	@ResponseWrapper(localName = "findOutboundFaxesAdvancedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutboundFaxesAdvancedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/findOutboundFaxesAdvancedRequest", output = "http://ws.xm.faxserver.com/FaxService/findOutboundFaxesAdvancedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/findOutboundFaxesAdvanced/Fault/FaxException")
	})
	public OutboundUserFaxResponse findOutboundFaxesAdvanced(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "startIndex", targetNamespace = "")
					int startIndex,
			@WebParam(name = "maxCount", targetNamespace = "")
					int maxCount,
			@WebParam(name = "sortInfo", targetNamespace = "")
					OutboundSortInfo sortInfo,
			@WebParam(name = "searchInfo", targetNamespace = "")
					OutboundUserSearchInfo searchInfo,
			@WebParam(name = "keywordInfo", targetNamespace = "")
					OutboundUserKeywordInfo keywordInfo)
			throws FaxException_Exception
	;


	/**
	 * @param startIndex
	 * @param searchInfo
	 * @param userId
	 * @param maxCount
	 * @return returns com.clifton.fax.xmedius.api.OutgoingFaxResponse
	 */
	@WebMethod
	@WebResult(name = "response", targetNamespace = "")
	@RequestWrapper(localName = "findOutgoingFaxesAdvanced", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutgoingFaxesAdvanced")
	@ResponseWrapper(localName = "findOutgoingFaxesAdvancedResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.FindOutgoingFaxesAdvancedResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/findOutgoingFaxesAdvancedRequest", output = "http://ws.xm.faxserver.com/FaxService/findOutgoingFaxesAdvancedResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/findOutgoingFaxesAdvanced/Fault/FaxException")
	})
	public OutgoingFaxResponse findOutgoingFaxesAdvanced(
			@WebParam(name = "userId", targetNamespace = "")
					String userId,
			@WebParam(name = "startIndex", targetNamespace = "")
					int startIndex,
			@WebParam(name = "maxCount", targetNamespace = "")
					int maxCount,
			@WebParam(name = "searchInfo", targetNamespace = "")
					OutgoingSearchInfo searchInfo)
			throws FaxException_Exception
	;


	/**
	 * @param groupId
	 * @return returns java.util.List<com.clifton.fax.xmedius.api.PhonebookPerson>
	 */
	@WebMethod
	@WebResult(targetNamespace = "")
	@RequestWrapper(localName = "getPersonsInGroupRecursive", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetPersonsInGroupRecursive")
	@ResponseWrapper(localName = "getPersonsInGroupRecursiveResponse", targetNamespace = "http://ws.xm.faxserver.com/", className = "com.clifton.fax.xmedius.api.GetPersonsInGroupRecursiveResponse")
	@Action(input = "http://ws.xm.faxserver.com/FaxService/getPersonsInGroupRecursiveRequest", output = "http://ws.xm.faxserver.com/FaxService/getPersonsInGroupRecursiveResponse", fault = {
			@FaultAction(className = FaxException_Exception.class, value = "http://ws.xm.faxserver.com/FaxService/getPersonsInGroupRecursive/Fault/FaxException")
	})
	public List<PhonebookPerson> getPersonsInGroupRecursive(
			@WebParam(name = "groupId", targetNamespace = "")
					List<String> groupId)
			throws FaxException_Exception
	;
}
