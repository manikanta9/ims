package com.clifton.fax.xmedius.converter;

import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fax.FaxMessageSearchForm;
import com.clifton.fax.xmedius.api.OutboundSortInfo;
import com.clifton.fax.xmedius.api.OutboundSortProperty;
import com.clifton.fax.xmedius.api.OutboundStatus;
import com.clifton.fax.xmedius.api.OutboundUserKeywordInfo;
import com.clifton.fax.xmedius.api.OutboundUserSearchInfo;
import com.clifton.fax.xmedius.search.OutboundXmediusSearch;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;

import java.util.Date;
import java.util.Map;


public class FaxMessageSearchFormOutboundXmediusSearchConverter implements Converter<FaxMessageSearchForm, OutboundXmediusSearch> {

	@Override
	public OutboundXmediusSearch convert(FaxMessageSearchForm from) {
		OutboundXmediusSearch xmediusSearch = new OutboundXmediusSearch();
		xmediusSearch.setMaxCount(from.getLimit());
		xmediusSearch.setStartIndex(from.getStart());
		xmediusSearch.setOutboundUserSearchInfo(getOutboundUserSearchInfo(from));
		xmediusSearch.setOutboundSortInfo(getOutboundSortInfo(from));
		xmediusSearch.setOutboundUserKeywordInfo(getOutboundUserKeywordInfo(from));
		return xmediusSearch;
	}


	private OutboundUserSearchInfo getOutboundUserSearchInfo(FaxMessageSearchForm from) {
		OutboundUserSearchInfo userSearchInfo = new OutboundUserSearchInfo();
		//Even though appears unused, the configurer modifies the form that is passed in to populate/parse values in the restriction list
		new HibernateSearchFormConfigurer(from);

		if (!StringUtils.isEmpty(from.getUserId())) {
			userSearchInfo.setUserId(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getUserId()));
		}

		if (!StringUtils.isEmpty(from.getStatus())) {
			userSearchInfo.setStatus(OutboundStatus.fromValue(from.getStatus()));
		}

		if (!StringUtils.isEmpty(from.getFaxId())) {
			userSearchInfo.setTransactionId(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getFaxId()));
		}

		if (from.getNumberOfPages() != null) {
			userSearchInfo.setPagesSubmitted(from.getNumberOfPages().longValue());
		}

		if (!StringUtils.isEmpty(from.getRecipientName())) {
			userSearchInfo.setRecipientName(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getRecipientName()));
		}

		if (!StringUtils.isEmpty(from.getRecipientNumber())) {
			userSearchInfo.setOriginalDestination(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getRecipientNumber()));
		}

		if (!StringUtils.isEmpty(from.getSubject())) {
			userSearchInfo.setSubject(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getSubject()));
		}

		if (from.getErrorCode() != null) {
			userSearchInfo.setErrorCode(from.getErrorCode().longValue());
		}

		if (!CollectionUtils.isEmpty(from.getRestrictionList())) {
			Map<String, Date> startDateRestrictions = FaxMessageXmediusUtils.processDateRestrictions(SearchUtils.getSearchRestrictionsForField(from.getRestrictionList(), "startDate"));
			if (!CollectionUtils.isEmpty(startDateRestrictions)) {
				userSearchInfo.setSubmittedTime(FaxMessageXmediusUtils.getDateTimeRange(startDateRestrictions.get("start"), startDateRestrictions.get("end")));
			}

			Map<String, Date> endDateRestrictions = FaxMessageXmediusUtils.processDateRestrictions(SearchUtils.getSearchRestrictionsForField(from.getRestrictionList(), "endDate"));
			if (!CollectionUtils.isEmpty(endDateRestrictions)) {
				userSearchInfo.setCompletedTime(FaxMessageXmediusUtils.getDateTimeRange(endDateRestrictions.get("start"), endDateRestrictions.get("end")));
			}
		}

		return userSearchInfo;
	}


	private OutboundSortInfo getOutboundSortInfo(FaxMessageSearchForm from) {
		OutboundSortInfo sortInfo = null;
		if (!StringUtils.isEmpty(from.getOrderBy())) {
			//Split on after asc or desc if there is another field set after
			String[] ordering = from.getOrderBy().split("(?<=:(asc|desc))#");
			ValidationUtils.assertTrue(ordering.length < 2, "Cannot sort in more than 1 direction at this time.");
			String[] orderSet = ordering[0].split(":");
			String[] fields = orderSet[0].split("#");
			ValidationUtils.assertTrue(fields.length < 2, "Cannot sort on more than 1 field at this time.");
			String sortDirection = orderSet[1];
			try {
				String field = fields[0];
				switch (field) {
					case "faxId":
						field = "TransactionId";
						break;
					case "numberOfPages":
						field = "PagesSubmitted";
						break;
					case "recipientNumber":
						field = "OriginalDestination";
						break;
					case "startDate":
						field = "SubmittedTime";
						break;
					case "endDate":
						field = "CompletedTime";
						break;
					default:
						break;
				}
				OutboundSortProperty sortProperty = OutboundSortProperty.fromValue(field);
				sortInfo = new OutboundSortInfo();
				sortInfo.setSortProperty(sortProperty);
				sortInfo.setAscending("asc".equalsIgnoreCase(sortDirection));
			}
			catch (IllegalArgumentException iae) {
				throw new ValidationException("Cannot find Outbound Sort Property to sort on: " + fields[0]);
			}
		}
		return sortInfo;
	}


	private OutboundUserKeywordInfo getOutboundUserKeywordInfo(FaxMessageSearchForm from) {
		return null;
	}
}
