package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutboundUserFaxItem complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OutboundUserFaxItem">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}OutboundFaxItem">
 *       &lt;sequence>
 *         &lt;element name="isViewed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OutboundUserFaxItem", propOrder = {
		"isViewed",
		"note"
})
@XmlSeeAlso({
		OutboundUserFaxItemFull.class
})
public class OutboundUserFaxItem
		extends OutboundFaxItem {

	private Boolean isViewed;
	private String note;


	/**
	 * Gets the value of the isViewed property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getIsViewed() {
		return this.isViewed;
	}


	/**
	 * Sets the value of the isViewed property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setIsViewed(Boolean value) {
		this.isViewed = value;
	}


	/**
	 * Gets the value of the note property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getNote() {
		return this.note;
	}


	/**
	 * Sets the value of the note property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setNote(String value) {
		this.note = value;
	}
}
