package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InboundUserSearchInfo complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InboundUserSearchInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}InboundSearchInfo">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isDeleted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InboundUserSearchInfo", propOrder = {
		"userId",
		"isDeleted",
		"note"
})
public class InboundUserSearchInfo
		extends InboundSearchInfo {

	private String userId;
	private Boolean isDeleted;
	private String note;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the isDeleted property.
	 *
	 * @return possible object is
	 * {@link Boolean }
	 */
	public Boolean getIsDeleted() {
		return this.isDeleted;
	}


	/**
	 * Sets the value of the isDeleted property.
	 *
	 * @param value allowed object is
	 *              {@link Boolean }
	 */
	public void setIsDeleted(Boolean value) {
		this.isDeleted = value;
	}


	/**
	 * Gets the value of the note property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getNote() {
		return this.note;
	}


	/**
	 * Sets the value of the note property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setNote(String value) {
		this.note = value;
	}
}
