package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutgoingStatus.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OutgoingStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSpecified"/>
 *     &lt;enumeration value="Preprocessing"/>
 *     &lt;enumeration value="Authorizing"/>
 *     &lt;enumeration value="Ready to send"/>
 *     &lt;enumeration value="Sending"/>
 *     &lt;enumeration value="Waiting to retry"/>
 *     &lt;enumeration value="Sent"/>
 *     &lt;enumeration value="Failed"/>
 *     &lt;enumeration value="Delayed"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "OutgoingStatus")
@XmlEnum
public enum OutgoingStatus {

	@XmlEnumValue("NotSpecified")
	NOT_SPECIFIED("NotSpecified"),
	@XmlEnumValue("Preprocessing")
	PREPROCESSING("Preprocessing"),
	@XmlEnumValue("Authorizing")
	AUTHORIZING("Authorizing"),
	@XmlEnumValue("Ready to send")
	READY_TO_SEND("Ready to send"),
	@XmlEnumValue("Sending")
	SENDING("Sending"),
	@XmlEnumValue("Waiting to retry")
	WAITING_TO_RETRY("Waiting to retry"),
	@XmlEnumValue("Sent")
	SENT("Sent"),
	@XmlEnumValue("Failed")
	FAILED("Failed"),
	@XmlEnumValue("Delayed")
	DELAYED("Delayed");
	private final String value;


	OutgoingStatus(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static OutgoingStatus fromValue(String v) {
		for (OutgoingStatus c : OutgoingStatus.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
