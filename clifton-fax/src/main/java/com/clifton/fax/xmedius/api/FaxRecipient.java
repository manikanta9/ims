package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaxRecipient complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaxRecipient">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="billingCode" type="{http://ws.xm.faxserver.com/}BillingCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxRecipient", propOrder = {
		"number",
		"name",
		"company",
		"billingCode"
})
public class FaxRecipient {

	@XmlElement(required = true)
	private String number;
	private String name;
	private String company;
	private BillingCode billingCode;


	/**
	 * Gets the value of the number property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getNumber() {
		return this.number;
	}


	/**
	 * Sets the value of the number property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setNumber(String value) {
		this.number = value;
	}


	/**
	 * Gets the value of the name property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * Sets the value of the name property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setName(String value) {
		this.name = value;
	}


	/**
	 * Gets the value of the company property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCompany() {
		return this.company;
	}


	/**
	 * Sets the value of the company property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCompany(String value) {
		this.company = value;
	}


	/**
	 * Gets the value of the billingCode property.
	 *
	 * @return possible object is
	 * {@link BillingCode }
	 */
	public BillingCode getBillingCode() {
		return this.billingCode;
	}


	/**
	 * Sets the value of the billingCode property.
	 *
	 * @param value allowed object is
	 *              {@link BillingCode }
	 */
	public void setBillingCode(BillingCode value) {
		this.billingCode = value;
	}
}
