package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOutgoingFaxResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getOutgoingFaxResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fax" type="{http://ws.xm.faxserver.com/}OutgoingFaxItemFull" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOutgoingFaxResponse", propOrder = {
		"fax"
})
public class GetOutgoingFaxResponse {

	private OutgoingFaxItemFull fax;


	/**
	 * Gets the value of the fax property.
	 *
	 * @return possible object is
	 * {@link OutgoingFaxItemFull }
	 */
	public OutgoingFaxItemFull getFax() {
		return this.fax;
	}


	/**
	 * Sets the value of the fax property.
	 *
	 * @param value allowed object is
	 *              {@link OutgoingFaxItemFull }
	 */
	public void setFax(OutgoingFaxItemFull value) {
		this.fax = value;
	}
}
