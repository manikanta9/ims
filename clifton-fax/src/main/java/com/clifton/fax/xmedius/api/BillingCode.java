package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillingCode complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="BillingCode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingCode", propOrder = {
		"code",
		"subCode"
})
public class BillingCode {

	@XmlElement(required = true)
	private String code;
	@XmlElement(required = true)
	private String subCode;


	/**
	 * Gets the value of the code property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getCode() {
		return this.code;
	}


	/**
	 * Sets the value of the code property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setCode(String value) {
		this.code = value;
	}


	/**
	 * Gets the value of the subCode property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getSubCode() {
		return this.subCode;
	}


	/**
	 * Sets the value of the subCode property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setSubCode(String value) {
		this.subCode = value;
	}
}
