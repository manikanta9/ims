package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getVersionResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getVersionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="version" type="{http://ws.xm.faxserver.com/}Version" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getVersionResponse", propOrder = {
		"version"
})
public class GetVersionResponse {

	private Version version;


	/**
	 * Gets the value of the version property.
	 *
	 * @return possible object is
	 * {@link Version }
	 */
	public Version getVersion() {
		return this.version;
	}


	/**
	 * Sets the value of the version property.
	 *
	 * @param value allowed object is
	 *              {@link Version }
	 */
	public void setVersion(Version value) {
		this.version = value;
	}
}
