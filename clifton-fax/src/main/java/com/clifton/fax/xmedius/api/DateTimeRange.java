package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DateTimeRange complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DateTimeRange">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="begin" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DateTimeRange", propOrder = {
		"value",
		"begin",
		"end"
})
public class DateTimeRange {

	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar value;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar begin;
	@XmlSchemaType(name = "dateTime")
	private XMLGregorianCalendar end;


	/**
	 * Gets the value of the value property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getValue() {
		return this.value;
	}


	/**
	 * Sets the value of the value property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setValue(XMLGregorianCalendar value) {
		this.value = value;
	}


	/**
	 * Gets the value of the begin property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getBegin() {
		return this.begin;
	}


	/**
	 * Sets the value of the begin property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setBegin(XMLGregorianCalendar value) {
		this.begin = value;
	}


	/**
	 * Gets the value of the end property.
	 *
	 * @return possible object is
	 * {@link XMLGregorianCalendar }
	 */
	public XMLGregorianCalendar getEnd() {
		return this.end;
	}


	/**
	 * Sets the value of the end property.
	 *
	 * @param value allowed object is
	 *              {@link XMLGregorianCalendar }
	 */
	public void setEnd(XMLGregorianCalendar value) {
		this.end = value;
	}
}
