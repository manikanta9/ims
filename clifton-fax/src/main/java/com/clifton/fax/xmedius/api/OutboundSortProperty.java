package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OutboundSortProperty.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OutboundSortProperty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="UserId"/>
 *     &lt;enumeration value="CompletedTime"/>
 *     &lt;enumeration value="Duration"/>
 *     &lt;enumeration value="ModifiedDestination"/>
 *     &lt;enumeration value="Status"/>
 *     &lt;enumeration value="ErrorCode"/>
 *     &lt;enumeration value="SenderBillingCode"/>
 *     &lt;enumeration value="SenderSubBillingCode"/>
 *     &lt;enumeration value="RecipientBillingCode"/>
 *     &lt;enumeration value="SenderSubBillingCode"/>
 *     &lt;enumeration value="RetryDelay"/>
 *     &lt;enumeration value="RemoteCsid"/>
 *     &lt;enumeration value="RecipientName"/>
 *     &lt;enumeration value="Priority"/>
 *     &lt;enumeration value="PagesSubmitted"/>
 *     &lt;enumeration value="PagesSent"/>
 *     &lt;enumeration value="OriginalTransactionId"/>
 *     &lt;enumeration value="OriginalDestination"/>
 *     &lt;enumeration value="NoOfRetries"/>
 *     &lt;enumeration value="ErrorDescription"/>
 *     &lt;enumeration value="ChannelNumber"/>
 *     &lt;enumeration value="SubmittedTime"/>
 *     &lt;enumeration value="BroadcastId"/>
 *     &lt;enumeration value="AttemptCount"/>
 *     &lt;enumeration value="ArchivedTime"/>
 *     &lt;enumeration value="Speed"/>
 *     &lt;enumeration value="Subject"/>
 *     &lt;enumeration value="TransactionId"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "OutboundSortProperty")
@XmlEnum
public enum OutboundSortProperty {

	@XmlEnumValue("UserId")
	USER_ID("UserId"),
	@XmlEnumValue("CompletedTime")
	COMPLETED_TIME("CompletedTime"),
	@XmlEnumValue("Duration")
	DURATION("Duration"),
	@XmlEnumValue("ModifiedDestination")
	MODIFIED_DESTINATION("ModifiedDestination"),
	@XmlEnumValue("Status")
	STATUS("Status"),
	@XmlEnumValue("ErrorCode")
	ERROR_CODE("ErrorCode"),
	@XmlEnumValue("SenderBillingCode")
	SENDER_BILLING_CODE("SenderBillingCode"),
	@XmlEnumValue("SenderSubBillingCode")
	SENDER_SUB_BILLING_CODE("SenderSubBillingCode"),
	@XmlEnumValue("RecipientBillingCode")
	RECIPIENT_BILLING_CODE("RecipientBillingCode"),
	@XmlEnumValue("RetryDelay")
	RETRY_DELAY("RetryDelay"),
	@XmlEnumValue("RemoteCsid")
	REMOTE_CSID("RemoteCsid"),
	@XmlEnumValue("RecipientName")
	RECIPIENT_NAME("RecipientName"),
	@XmlEnumValue("Priority")
	PRIORITY("Priority"),
	@XmlEnumValue("PagesSubmitted")
	PAGES_SUBMITTED("PagesSubmitted"),
	@XmlEnumValue("PagesSent")
	PAGES_SENT("PagesSent"),
	@XmlEnumValue("OriginalTransactionId")
	ORIGINAL_TRANSACTION_ID("OriginalTransactionId"),
	@XmlEnumValue("OriginalDestination")
	ORIGINAL_DESTINATION("OriginalDestination"),
	@XmlEnumValue("NoOfRetries")
	NO_OF_RETRIES("NoOfRetries"),
	@XmlEnumValue("ErrorDescription")
	ERROR_DESCRIPTION("ErrorDescription"),
	@XmlEnumValue("ChannelNumber")
	CHANNEL_NUMBER("ChannelNumber"),
	@XmlEnumValue("SubmittedTime")
	SUBMITTED_TIME("SubmittedTime"),
	@XmlEnumValue("BroadcastId")
	BROADCAST_ID("BroadcastId"),
	@XmlEnumValue("AttemptCount")
	ATTEMPT_COUNT("AttemptCount"),
	@XmlEnumValue("ArchivedTime")
	ARCHIVED_TIME("ArchivedTime"),
	@XmlEnumValue("Speed")
	SPEED("Speed"),
	@XmlEnumValue("Subject")
	SUBJECT("Subject"),
	@XmlEnumValue("TransactionId")
	TRANSACTION_ID("TransactionId");
	private final String value;


	OutboundSortProperty(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static OutboundSortProperty fromValue(String v) {
		for (OutboundSortProperty c : OutboundSortProperty.values()) {
			if (c.value.equalsIgnoreCase(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
