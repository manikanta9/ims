package com.clifton.fax.xmedius;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fax.FaxMessage;
import com.clifton.fax.FaxMessageSearchForm;
import com.clifton.fax.FaxMessageService;
import com.clifton.fax.xmedius.api.Fax;
import com.clifton.fax.xmedius.api.FaxException;
import com.clifton.fax.xmedius.api.FaxException_Exception;
import com.clifton.fax.xmedius.api.FaxResult;
import com.clifton.fax.xmedius.api.FaxService;
import com.clifton.fax.xmedius.api.InboundUserFaxItem;
import com.clifton.fax.xmedius.api.InboundUserFaxItemFull;
import com.clifton.fax.xmedius.api.InboundUserFaxResponse;
import com.clifton.fax.xmedius.api.InboundUserSearchInfo;
import com.clifton.fax.xmedius.api.OutboundUserFaxItem;
import com.clifton.fax.xmedius.api.OutboundUserFaxItemFull;
import com.clifton.fax.xmedius.api.OutboundUserFaxResponse;
import com.clifton.fax.xmedius.api.OutboundUserSearchInfo;
import com.clifton.fax.xmedius.api.OutgoingFaxItem;
import com.clifton.fax.xmedius.api.OutgoingFaxResponse;
import com.clifton.fax.xmedius.api.Version;
import com.clifton.fax.xmedius.search.InboundXmediusSearch;
import com.clifton.fax.xmedius.search.OutboundXmediusSearch;
import com.clifton.fax.xmedius.search.OutgoingXmediusSearch;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;
import org.springframework.stereotype.Service;

import javax.xml.ws.BindingProvider;
import java.io.File;
import java.util.List;
import java.util.Map;


/**
 * Fax service implementation using the XMedius API
 */
@Service("faxMessageService")
public class FaxMessageXmediusServiceImpl implements FaxMessageService {

	private Map<String, Converter<FaxMessageSearchForm, Object>> xmediusSearchConverterMap;
	private Map<String, Converter<Object, FaxMessage>> xmediusFaxItemConverterMap;

	private static final String WILDCARD = "%";

	private String serviceAddress;
	private String webServiceUserName;
	private String webServiceUserPassword;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FaxMessage sendFax(FaxMessage faxMessage) {
		FaxMessage result = new FaxMessage();
		FaxService faxService = setupFaxServiceRequest();
		try {
			FaxResult serverResult = sendFax(faxService, faxMessage);
			/*
			  For XMedius a given fax is unique per transaction and attachments are tied to broadcastids
			  where a broadcast is in the initial command and a unique transaction is generated for each destination/recipient.

			  Because we are only sending to one recipient at a time at this point (looped in IntegrationExportHandlerFaxImpl) there should
			  only be one transactionId returned.
			 */
			result.setFaxServerIdentifier(CollectionUtils.getFirstElementStrict(serverResult.getTransactionId()));
		}
		catch (FaxException_Exception fee) {
			result.setException(fee);
		}

		return result;
	}


	/**
	 * Get a fax message using the TransactionID
	 */
	@Override
	public FaxMessage getFax(String faxIdentifier) {
		FaxService faxService = setupFaxServiceRequest();
		FaxMessage faxMessage = getOutboundFax(faxIdentifier, faxService, false);
		if (faxMessage == null) {
			faxMessage = getInboundFax(faxIdentifier, faxService, false);
		}
		return faxMessage;
	}


	@Override
	public FileWrapper downloadFaxAttachment(String faxIdentifier) {
		FaxService faxService = setupFaxServiceRequest();
		FaxMessage faxMessage = getOutboundFax(faxIdentifier, faxService, true);
		if (faxMessage == null) {
			faxMessage = getInboundFax(faxIdentifier, faxService, true);
		}
		if (faxMessage != null) {
			List<FileWrapper> attachments = faxMessage.getAttachments();
			if (attachments.size() > 1) {
				String filename = FileContainerFactory.getFileContainer(attachments.get(0).getFile()).getParent() + File.separator + attachments.get(0).getFileName() + ".zip";
				return new FileWrapper(FilePath.forPath(ZipUtils.zipFiles(attachments, filename).getPath()), filename, true);
			}
			else if (attachments.size() == 1) {
				return CollectionUtils.getFirstElement(attachments);
			}
		}
		return null;
	}


	@Override
	public PagingArrayList<FaxMessage> getFaxList(FaxMessageSearchForm searchForm) {
		FaxService faxService = setupFaxServiceRequest();
		try {
			switch (searchForm.getFaxMessageType()) {
				case OUTGOING:
					return getOutgoingFaxes(searchForm, faxService);
				case OUTBOUND:
					return getOutboundFaxes(searchForm, faxService);
				case INBOUND:
					return getInboundFaxes(searchForm, faxService);
			}
		}
		catch (FaxException_Exception fee) {
			throw new RuntimeException("Error occurred retrieving faxes", fee);
		}
		return null;
	}


	@Override
	public boolean isFaxServerRunning() {
		return checkFaxServerRunning(null);
	}


	/////////////////////////////////////////////////////////////////////////////////////


	private PagingArrayList<FaxMessage> getOutboundFaxes(FaxMessageSearchForm searchForm, FaxService faxService) throws FaxException_Exception {
		OutboundXmediusSearch xmediusSearch = (OutboundXmediusSearch) (getXmediusSearchConverterMap().get(searchForm.getFaxMessageType().toString())).convert(searchForm);
		OutboundUserFaxResponse response = faxService.findOutboundFaxesAdvanced(xmediusSearch.getUserSearchId(),
				xmediusSearch.getStartIndex(),
				xmediusSearch.getMaxCount() + 1,
				xmediusSearch.getOutboundSortInfo(),
				xmediusSearch.getOutboundUserSearchInfo(),
				xmediusSearch.getOutboundUserKeywordInfo());

		PagingArrayList<FaxMessage> results = new PagingArrayList<>(xmediusSearch.getStartIndex(), xmediusSearch.getMaxCount());
		for (OutboundUserFaxItem faxItem : response.getFax()) {
			results.add(convertToFaxMessage(faxItem));
		}
		results.setTotalElementCount(Math.toIntExact(response.getTotalItems()));
		return results;
	}


	private PagingArrayList<FaxMessage> getOutgoingFaxes(FaxMessageSearchForm searchForm, FaxService faxService) throws FaxException_Exception {
		OutgoingXmediusSearch xmediusSearch = (OutgoingXmediusSearch) (getXmediusSearchConverterMap().get(searchForm.getFaxMessageType().toString())).convert(searchForm);
		OutgoingFaxResponse response = faxService.findOutgoingFaxesAdvanced(xmediusSearch.getUserSearchId(),
				xmediusSearch.getStartIndex(),
				xmediusSearch.getMaxCount() + 1,
				xmediusSearch.getOutgoingSearchInfo());

		PagingArrayList<FaxMessage> results = new PagingArrayList<>(xmediusSearch.getStartIndex(), xmediusSearch.getMaxCount());
		for (OutgoingFaxItem faxItem : response.getFax()) {
			results.add(convertToFaxMessage(faxItem));
		}
		results.setTotalElementCount(Math.toIntExact(response.getTotalItems()));
		return results;
	}


	private PagingArrayList<FaxMessage> getInboundFaxes(FaxMessageSearchForm searchForm, FaxService faxService) throws FaxException_Exception {
		InboundXmediusSearch xmediusSearch = (InboundXmediusSearch) (getXmediusSearchConverterMap().get(searchForm.getFaxMessageType().toString())).convert(searchForm);
		InboundUserFaxResponse response = faxService.findInboundFaxesAdvanced(xmediusSearch.getUserSearchId(),
				xmediusSearch.getStartIndex(),
				xmediusSearch.getMaxCount() + 1,
				xmediusSearch.getInboundSortInfo(),
				xmediusSearch.getInboundUserSearchInfo(),
				xmediusSearch.getInboundUserKeywordInfo());

		PagingArrayList<FaxMessage> results = new PagingArrayList<>(xmediusSearch.getStartIndex(), xmediusSearch.getMaxCount());
		for (InboundUserFaxItem faxItem : response.getFax()) {
			results.add(convertToFaxMessage(faxItem));
		}
		results.setTotalElementCount(Math.toIntExact(response.getTotalItems()));
		return results;
	}


	private FaxMessage convertToFaxMessage(Object xmediusObject) {
		return (getXmediusFaxItemConverterMap().get(xmediusObject.getClass().getName()).convert(xmediusObject));
	}


	private boolean checkFaxServerRunning(FaxService faxService) {
		if (faxService == null) {
			faxService = setupFaxServiceRequest();
		}

		try {
			checkXMediusServerStatus(faxService);
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}


	private FaxMessage getInboundFax(String transactionId, FaxService faxService, boolean fullyPopulated) {
		try {
			InboundUserSearchInfo searchInfo = new InboundUserSearchInfo();
			searchInfo.setTransactionId(transactionId);
			InboundUserFaxResponse response = faxService.findInboundFaxesAdvanced(WILDCARD, 0, 2, null, searchInfo, null);
			//There should only be one fax item for a given transaction id
			InboundUserFaxItem faxItem = CollectionUtils.getOnlyElement(response.getFax());
			if (faxItem != null) {
				if (fullyPopulated) {
					return getInboundFaxFull(faxItem.getId(), faxService);
				}
				else {
					return (getXmediusFaxItemConverterMap().get(faxItem.getClass().getName()).convert(faxItem));
				}
			}
			return null;
		}
		catch (FaxException_Exception fee) {
			throw new RuntimeException("Error occurred retrieving inbound faxes for transactionID: " + transactionId, fee);
		}
	}


	private FaxMessage getInboundFaxFull(String faxId, FaxService faxService) {
		try {
			InboundUserFaxItemFull inboundUserFaxItemFull = faxService.getInboundFax(faxId);
			return (getXmediusFaxItemConverterMap().get(inboundUserFaxItemFull.getClass().getName()).convert(inboundUserFaxItemFull));
		}
		catch (FaxException_Exception fee) {
			FaxException exception = fee.getFaultInfo();
			if (!(exception.getCode() == 599 && exception.getVendorCode().contains("InvalidId"))) {
				throw new RuntimeException("An error occured getting inbound fax with faxId" + faxId, fee);
			}
		}
		return null;
	}


	private FaxMessage getOutboundFax(String transactionId, FaxService faxService, boolean fullyPopulated) {
		try {
			OutboundUserSearchInfo searchInfo = new OutboundUserSearchInfo();
			searchInfo.setTransactionId(transactionId);
			OutboundUserFaxResponse response = faxService.findOutboundFaxesAdvanced(WILDCARD, 0, 2, null, searchInfo, null);
			//There should only be one fax item for a given transaction id
			OutboundUserFaxItem faxItem = CollectionUtils.getOnlyElement(response.getFax());
			if (faxItem != null) {
				if (fullyPopulated) {
					return getOutboundFaxFull(faxItem.getId(), faxService);
				}
				else {
					return (getXmediusFaxItemConverterMap().get(faxItem.getClass().getName()).convert(faxItem));
				}
			}
			return null;
		}
		catch (FaxException_Exception fee) {
			throw new RuntimeException("Error occurred retrieving outbound faxes for transactionID: " + transactionId, fee);
		}
	}


	private FaxMessage getOutboundFaxFull(String faxId, FaxService faxService) {
		try {
			OutboundUserFaxItemFull outboundFax = faxService.getOutboundFax(faxId);
			return (getXmediusFaxItemConverterMap().get(outboundFax.getClass().getName()).convert(outboundFax));
		}
		catch (FaxException_Exception fee) {
			FaxException exception = fee.getFaultInfo();
			if (!(exception.getCode() == 599 && exception.getVendorCode().contains("InvalidId"))) {
				throw new RuntimeException("An error occured getting outbound fax with faxId" + faxId, fee);
			}
		}
		return null;
	}


	private FaxResult sendFax(FaxService faxService, FaxMessage faxMessage) throws FaxException_Exception {
		return faxService.sendFax(faxMessage.getSenderEmail(),
				faxMessage.getSubject(),
				faxMessage.getComments(),
				FaxMessageXmediusUtils.getFaxRecipients(faxMessage),
				FaxMessageXmediusUtils.getFaxAttachments(faxMessage),
				FaxMessageXmediusUtils.getCoverPage(faxMessage),
				FaxMessageXmediusUtils.getSenderInfo(faxMessage),
				FaxMessageXmediusUtils.getFaxOptions(faxMessage));
	}


	private FaxService setupFaxServiceRequest() {
		Fax faxDefinition = new Fax();
		FaxService faxService = faxDefinition.getFaxServicePort();
		BindingProvider bindingProvider = (BindingProvider) faxService;
		bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getServiceAddress());
		setCredentials(bindingProvider);
		return faxService;
	}


	private void checkXMediusServerStatus(FaxService faxService) {
		Version version = faxService.getVersion();
		ValidationUtils.assertNotNull(version, "XMedius Fax Server version check failed.");
	}


	/**
	 * To set the credentials for web service client.
	 */
	private void setCredentials(BindingProvider bp) {
		Map<String, Object> map = bp.getRequestContext();
		map.put(BindingProvider.USERNAME_PROPERTY, getWebServiceUserName());
		map.put(BindingProvider.PASSWORD_PROPERTY, getWebServiceUserPassword());
	}


	/////////////////////////////////////////////////////////////////////////////////////


	public String getServiceAddress() {
		return this.serviceAddress;
	}


	public void setServiceAddress(String serviceAddress) {
		this.serviceAddress = serviceAddress;
	}


	public String getWebServiceUserName() {
		return this.webServiceUserName;
	}


	public void setWebServiceUserName(String webServiceUserName) {
		this.webServiceUserName = webServiceUserName;
	}


	public String getWebServiceUserPassword() {
		return this.webServiceUserPassword;
	}


	public void setWebServiceUserPassword(String webServiceUserPassword) {
		this.webServiceUserPassword = webServiceUserPassword;
	}


	public Map<String, Converter<FaxMessageSearchForm, Object>> getXmediusSearchConverterMap() {
		return this.xmediusSearchConverterMap;
	}


	public void setXmediusSearchConverterMap(Map<String, Converter<FaxMessageSearchForm, Object>> xmediusSearchConverterMap) {
		this.xmediusSearchConverterMap = xmediusSearchConverterMap;
	}


	public Map<String, Converter<Object, FaxMessage>> getXmediusFaxItemConverterMap() {
		return this.xmediusFaxItemConverterMap;
	}


	public void setXmediusFaxItemConverterMap(Map<String, Converter<Object, FaxMessage>> xmediusFaxItemConverterMap) {
		this.xmediusFaxItemConverterMap = xmediusFaxItemConverterMap;
	}
}
