package com.clifton.fax.xmedius.converter;

import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.fax.FaxMessageSearchForm;
import com.clifton.fax.xmedius.api.InboundSortInfo;
import com.clifton.fax.xmedius.api.InboundStatus;
import com.clifton.fax.xmedius.api.InboundUserKeywordInfo;
import com.clifton.fax.xmedius.api.InboundUserSearchInfo;
import com.clifton.fax.xmedius.search.InboundXmediusSearch;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;

import java.util.Date;
import java.util.Map;


public class FaxMessageSearchFormInboundXmediusSearchConverter implements Converter<FaxMessageSearchForm, InboundXmediusSearch> {

	@Override
	public InboundXmediusSearch convert(FaxMessageSearchForm from) {
		InboundXmediusSearch xmediusSearch = new InboundXmediusSearch();
		xmediusSearch.setStartIndex(from.getStart());
		xmediusSearch.setMaxCount(from.getLimit());
		xmediusSearch.setInboundSortInfo(getInboundSortInfo(from));
		xmediusSearch.setInboundUserKeywordInfo(getInboundUserKeywordInfo(from));
		xmediusSearch.setInboundUserSearchInfo(getInboundUserSearchInfo(from));
		return xmediusSearch;
	}


	private InboundUserSearchInfo getInboundUserSearchInfo(FaxMessageSearchForm from) {
		InboundUserSearchInfo searchInfo = new InboundUserSearchInfo();

		if (!StringUtils.isEmpty(from.getFaxId())) {
			searchInfo.setTransactionId(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getFaxId()));
		}

		if (!StringUtils.isEmpty(from.getUserId())) {
			searchInfo.setUserId(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getUserId()));
		}

		if (!StringUtils.isEmpty(from.getSenderNumber())) {
			searchInfo.setAni(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getSenderNumber()));
		}

		if (!StringUtils.isEmpty(from.getStatus())) {
			searchInfo.setStatus(InboundStatus.fromValue(from.getStatus()));
		}
		if (!StringUtils.isEmpty(from.getSenderName())) {
			searchInfo.setRemoteCsid(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getSenderName()));
		}

		if (from.getErrorCode() != null) {
			searchInfo.setErrorCode(from.getErrorCode().longValue());
		}

		if (from.getNumberOfPages() != null) {
			searchInfo.setPagesReceived(from.getNumberOfPages().longValue());
		}

		if (!CollectionUtils.isEmpty(from.getRestrictionList())) {
			Map<String, Date> startDateRestrictions = FaxMessageXmediusUtils.processDateRestrictions(SearchUtils.getSearchRestrictionsForField(from.getRestrictionList(), "startDate"));
			if (!CollectionUtils.isEmpty(startDateRestrictions)) {
				searchInfo.setReceivedTime(FaxMessageXmediusUtils.getDateTimeRange(startDateRestrictions.get("start"), startDateRestrictions.get("end")));
			}
		}
		return searchInfo;
	}


	private InboundSortInfo getInboundSortInfo(FaxMessageSearchForm from) {
		return null;
	}


	private InboundUserKeywordInfo getInboundUserKeywordInfo(FaxMessageSearchForm from) {
		return null;
	}
}
