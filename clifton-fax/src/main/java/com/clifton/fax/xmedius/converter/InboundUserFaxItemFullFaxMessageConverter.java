package com.clifton.fax.xmedius.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.fax.FaxMessage;
import com.clifton.fax.xmedius.api.FaxImage;
import com.clifton.fax.xmedius.api.InboundStatus;
import com.clifton.fax.xmedius.api.InboundUserFaxItemFull;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;

import java.util.HashSet;
import java.util.Set;


public class InboundUserFaxItemFullFaxMessageConverter implements Converter<InboundUserFaxItemFull, FaxMessage> {

	@Override
	public FaxMessage convert(InboundUserFaxItemFull from) {
		Set<String> propertiesToExclude = new HashSet<>();
		FaxMessage message = new FaxMessage();
		message.setFaxServerIdentifier((String) FaxMessageXmediusUtils.getPropertyValue(from, "transactionId", propertiesToExclude));
		message.setSenderName((String) FaxMessageXmediusUtils.getPropertyValue(from, "senderNasme", propertiesToExclude));
		message.setSenderNumber((String) FaxMessageXmediusUtils.getPropertyValue(from, "ani", propertiesToExclude));
		message.setComments((String) FaxMessageXmediusUtils.getPropertyValue(from, "note", propertiesToExclude));
		if (from.getErrorCode() != null && !StringUtils.isEmpty(from.getErrorDescription())) {
			message.setException(new Exception((String) FaxMessageXmediusUtils.getPropertyValue(from, "errorDescription", propertiesToExclude)));
		}
		message.setAttachments(FaxMessageXmediusUtils.getAttachments((FaxImage) FaxMessageXmediusUtils.getPropertyValue(from, "image", propertiesToExclude)));
		message.setStatus(((InboundStatus) FaxMessageXmediusUtils.getPropertyValue(from, "status", propertiesToExclude)).value());
		message.setAdditionalProperties(FaxMessageXmediusUtils.getAdditionalPropertiesList(BeanUtils.describe(from), propertiesToExclude));
		return message;
	}
}
