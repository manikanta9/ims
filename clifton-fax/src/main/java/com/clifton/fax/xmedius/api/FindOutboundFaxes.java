package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for findOutboundFaxes complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="findOutboundFaxes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startIndex" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="maxCount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="sortInfo" type="{http://ws.xm.faxserver.com/}OutboundSortInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findOutboundFaxes", propOrder = {
		"userId",
		"startIndex",
		"maxCount",
		"sortInfo"
})
public class FindOutboundFaxes {

	private String userId;
	private int startIndex;
	private int maxCount;
	private OutboundSortInfo sortInfo;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the startIndex property.
	 */
	public int getStartIndex() {
		return this.startIndex;
	}


	/**
	 * Sets the value of the startIndex property.
	 */
	public void setStartIndex(int value) {
		this.startIndex = value;
	}


	/**
	 * Gets the value of the maxCount property.
	 */
	public int getMaxCount() {
		return this.maxCount;
	}


	/**
	 * Sets the value of the maxCount property.
	 */
	public void setMaxCount(int value) {
		this.maxCount = value;
	}


	/**
	 * Gets the value of the sortInfo property.
	 *
	 * @return possible object is
	 * {@link OutboundSortInfo }
	 */
	public OutboundSortInfo getSortInfo() {
		return this.sortInfo;
	}


	/**
	 * Sets the value of the sortInfo property.
	 *
	 * @param value allowed object is
	 *              {@link OutboundSortInfo }
	 */
	public void setSortInfo(OutboundSortInfo value) {
		this.sortInfo = value;
	}
}
