package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for BillingGroup complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="BillingGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="billingCodeDisclose" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="billingCodeRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="billingCodeEnforced" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="billingCodes" type="{http://ws.xm.faxserver.com/}BillingCode" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BillingGroup", propOrder = {
		"name",
		"billingCodeDisclose",
		"billingCodeRequired",
		"billingCodeEnforced",
		"billingCodes"
})
public class BillingGroup {

	@XmlElement(required = true)
	private String name;
	private boolean billingCodeDisclose;
	private boolean billingCodeRequired;
	private boolean billingCodeEnforced;
	private List<BillingCode> billingCodes;


	/**
	 * Gets the value of the name property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * Sets the value of the name property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setName(String value) {
		this.name = value;
	}


	/**
	 * Gets the value of the billingCodeDisclose property.
	 */
	public boolean isBillingCodeDisclose() {
		return this.billingCodeDisclose;
	}


	/**
	 * Sets the value of the billingCodeDisclose property.
	 */
	public void setBillingCodeDisclose(boolean value) {
		this.billingCodeDisclose = value;
	}


	/**
	 * Gets the value of the billingCodeRequired property.
	 */
	public boolean isBillingCodeRequired() {
		return this.billingCodeRequired;
	}


	/**
	 * Sets the value of the billingCodeRequired property.
	 */
	public void setBillingCodeRequired(boolean value) {
		this.billingCodeRequired = value;
	}


	/**
	 * Gets the value of the billingCodeEnforced property.
	 */
	public boolean isBillingCodeEnforced() {
		return this.billingCodeEnforced;
	}


	/**
	 * Sets the value of the billingCodeEnforced property.
	 */
	public void setBillingCodeEnforced(boolean value) {
		this.billingCodeEnforced = value;
	}


	/**
	 * Gets the value of the billingCodes property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the billingCodes property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getBillingCodes().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link BillingCode }
	 */
	public List<BillingCode> getBillingCodes() {
		if (this.billingCodes == null) {
			this.billingCodes = new ArrayList<>();
		}
		return this.billingCodes;
	}
}
