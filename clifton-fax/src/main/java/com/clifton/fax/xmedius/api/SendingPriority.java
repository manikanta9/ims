package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SendingPriority.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SendingPriority">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSpecified"/>
 *     &lt;enumeration value="Low"/>
 *     &lt;enumeration value="Normal"/>
 *     &lt;enumeration value="High"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "SendingPriority")
@XmlEnum
public enum SendingPriority {

	@XmlEnumValue("NotSpecified")
	NOT_SPECIFIED("NotSpecified"),
	@XmlEnumValue("Low")
	LOW("Low"),
	@XmlEnumValue("Normal")
	NORMAL("Normal"),
	@XmlEnumValue("High")
	HIGH("High");
	private final String value;


	SendingPriority(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static SendingPriority fromValue(String v) {
		for (SendingPriority c : SendingPriority.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
