package com.clifton.fax.xmedius.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.fax.FaxMessage;
import com.clifton.fax.xmedius.api.OutgoingFaxItem;
import com.clifton.fax.xmedius.api.OutgoingStatus;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;

import java.util.HashSet;
import java.util.Set;


public class OutgoingFaxItemFaxMessageConverter implements Converter<OutgoingFaxItem, FaxMessage> {

	@Override
	public FaxMessage convert(OutgoingFaxItem from) {
		Set<String> propertiesToExclude = new HashSet<>();
		FaxMessage message = new FaxMessage();
		message.setFaxServerIdentifier((String) FaxMessageXmediusUtils.getPropertyValue(from, "transactionId", propertiesToExclude));
		message.setSubject((String) FaxMessageXmediusUtils.getPropertyValue(from, "subject", propertiesToExclude));
		message.setRecipientName((String) FaxMessageXmediusUtils.getPropertyValue(from, "recipientName", propertiesToExclude));
		message.setRecipientNumber((String) FaxMessageXmediusUtils.getPropertyValue(from, "originalDestination", propertiesToExclude));
		message.setSenderEmail((String) FaxMessageXmediusUtils.getPropertyValue(from, "userId", propertiesToExclude));
		if (from.getErrorCode() != null && !StringUtils.isEmpty(from.getErrorDescription())) {
			message.setException(new Exception((String) FaxMessageXmediusUtils.getPropertyValue(from, "errorDescription", propertiesToExclude)));
		}
		message.setStatus(((OutgoingStatus) FaxMessageXmediusUtils.getPropertyValue(from, "status", propertiesToExclude)).value());
		message.setAdditionalProperties(FaxMessageXmediusUtils.getAdditionalPropertiesList(BeanUtils.describe(from), propertiesToExclude));
		return message;
	}
}
