package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InboundKeywordProperty.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InboundKeywordProperty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Ani"/>
 *     &lt;enumeration value="DnisOrDid"/>
 *     &lt;enumeration value="Dtmf"/>
 *     &lt;enumeration value="RemoteCsid"/>
 *     &lt;enumeration value="ErrorCode"/>
 *     &lt;enumeration value="SiteName"/>
 *     &lt;enumeration value="TransactionId"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "InboundKeywordProperty")
@XmlEnum
public enum InboundKeywordProperty {

	@XmlEnumValue("All")
	ALL("All"),
	@XmlEnumValue("Ani")
	ANI("Ani"),
	@XmlEnumValue("DnisOrDid")
	DNIS_OR_DID("DnisOrDid"),
	@XmlEnumValue("Dtmf")
	DTMF("Dtmf"),
	@XmlEnumValue("RemoteCsid")
	REMOTE_CSID("RemoteCsid"),
	@XmlEnumValue("ErrorCode")
	ERROR_CODE("ErrorCode"),
	@XmlEnumValue("SiteName")
	SITE_NAME("SiteName"),
	@XmlEnumValue("TransactionId")
	TRANSACTION_ID("TransactionId");
	private final String value;


	InboundKeywordProperty(String v) {
		this.value = v;
	}


	public String value() {
		return this.value;
	}


	public static InboundKeywordProperty fromValue(String v) {
		for (InboundKeywordProperty c : InboundKeywordProperty.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}
}
