package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for InboundUserFaxResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InboundUserFaxResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.xm.faxserver.com/}FaxResponse">
 *       &lt;sequence>
 *         &lt;element name="fax" type="{http://ws.xm.faxserver.com/}InboundUserFaxItem" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InboundUserFaxResponse", propOrder = {
		"fax"
})
public class InboundUserFaxResponse
		extends FaxResponse {

	@XmlElement(nillable = true)
	private List<InboundUserFaxItem> fax;


	/**
	 * Gets the value of the fax property.
	 *
	 * <p>
	 * This accessor method returns a reference to the live list,
	 * not a snapshot. Therefore any modification you make to the
	 * returned list will be present inside the JAXB object.
	 * This is why there is not a <CODE>set</CODE> method for the fax property.
	 *
	 * <p>
	 * For example, to add a new item, do as follows:
	 * <pre>
	 *    getFax().add(newItem);
	 * </pre>
	 *
	 *
	 * <p>
	 * Objects of the following type(s) are allowed in the list
	 * {@link InboundUserFaxItem }
	 */
	public List<InboundUserFaxItem> getFax() {
		if (this.fax == null) {
			this.fax = new ArrayList<>();
		}
		return this.fax;
	}
}
