package com.clifton.fax.xmedius.search;

import com.clifton.fax.xmedius.api.OutgoingSearchInfo;


public class OutgoingXmediusSearch extends XmediusSearch {

	private OutgoingSearchInfo outgoingSearchInfo;


	public OutgoingSearchInfo getOutgoingSearchInfo() {
		return this.outgoingSearchInfo;
	}


	public void setOutgoingSearchInfo(OutgoingSearchInfo outgoingSearchInfo) {
		this.outgoingSearchInfo = outgoingSearchInfo;
	}
}
