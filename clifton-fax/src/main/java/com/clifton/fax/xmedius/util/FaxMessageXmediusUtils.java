package com.clifton.fax.xmedius.util;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fax.FaxMessage;
import com.clifton.fax.FaxProperty;
import com.clifton.fax.xmedius.api.AttachedFile;
import com.clifton.fax.xmedius.api.DateTimeRange;
import com.clifton.fax.xmedius.api.FaxImage;
import com.clifton.fax.xmedius.api.FaxOptions;
import com.clifton.fax.xmedius.api.FaxRecipient;
import com.clifton.fax.xmedius.api.SenderInfo;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class FaxMessageXmediusUtils {

	private static final String TEMP_FILE_PREFIX = "fax";
	private static final String FILE_IMAGE_MIME_TYPE = "image/tiff";
	private static final String FILE_IMAGE_EXTENSION = "tif";
	private static final String DEFAULT_COVER_SHEET = "ParametricMPLS.cse";

	private static final Integer TIME_ZONE_ID = 99;

	private static DatatypeFactory dataTypeFactory;


	public static SenderInfo getSenderInfo(FaxMessage faxMessage) {
		SenderInfo senderInfo = new SenderInfo();
		senderInfo.setFax(faxMessage.getSenderNumber());
		senderInfo.setLastName(faxMessage.getSenderName());
		return senderInfo;
	}


	public static List<FaxRecipient> getFaxRecipients(FaxMessage faxMessage) {
		List<FaxRecipient> recipients = new ArrayList<>();
		recipients.add(getFaxRecipient(faxMessage));
		return recipients;
	}


	public static FaxRecipient getFaxRecipient(FaxMessage faxMessage) {
		FaxRecipient recipient = new FaxRecipient();
		recipient.setNumber(faxMessage.getRecipientNumber());
		recipient.setName(faxMessage.getRecipientName());
		return recipient;
	}


	/**
	 * Converts the list of FileWrappers on the FaxMessage to a list of AttachedFiles to send to Xmedius
	 */
	public static List<AttachedFile> getFaxAttachments(FaxMessage faxMessage) {
		List<AttachedFile> attachedFiles = new ArrayList<>();
		try {
			for (FileWrapper fileWrapper : faxMessage.getAttachments()) {
				AttachedFile file = new AttachedFile();
				file.setName(fileWrapper.getFileName());
				file.setContents(FileUtils.readFileBytes(fileWrapper.getFile()));
				attachedFiles.add(file);
			}
		}
		catch (IOException ioe) {
			throw new RuntimeException("Error occured converting Files to AttachedFiles.", ioe);
		}
		return attachedFiles;
	}


	/**
	 * Get the name of the coversheet to use
	 */
	public static String getCoverPage(FaxMessage faxMessage) {
		return !StringUtils.isEmpty(faxMessage.getCoverPage()) ? faxMessage.getCoverPage() : DEFAULT_COVER_SHEET;
	}


	public static FaxOptions getFaxOptions(FaxMessage faxMessage) {
		//TODO: future enhancement to include advanced options
		return new FaxOptions();
	}


	public static List<FileWrapper> getAttachments(FaxImage image) {
		List<FileWrapper> files = new ArrayList<>();
		ValidationUtils.assertTrue(StringUtils.isEqual(image.getMimeType(), FILE_IMAGE_MIME_TYPE), "Expected MIME type is " + FILE_IMAGE_MIME_TYPE + " but was : " + image.getMimeType());
		File tempFile = FileUtils.convertInputStreamToFile(TEMP_FILE_PREFIX, FILE_IMAGE_EXTENSION, new ByteArrayInputStream(image.getContent()));
		files.add(new FileWrapper(tempFile, tempFile.getName(), true));
		return files;
	}


	/**
	 * Gets the property value from the object and adds the property name to the properties added set
	 */
	public static Object getPropertyValue(Object from, String propertyName, Set<String> propertiesAdded) {
		Object value = BeanUtils.getPropertyValue(from, propertyName);
		propertiesAdded.add(propertyName);
		return value;
	}


	/**
	 * Convert the objectPropertyMap into a map where the value is a string
	 * <p>
	 * Also, exclude any properties that are specified in the propertiesToExclude Set
	 */
	public static List<FaxProperty> getAdditionalPropertiesList(Map<String, Object> objectMap, Set<String> propertiesToExclude) {
		List<FaxProperty> additionalProperties = new ArrayList<>();
		for (Map.Entry<String, Object> row : objectMap.entrySet()) {
			if (!propertiesToExclude.contains(row.getKey()) && row.getValue() != null) {
				String rowValue = row.getValue().toString();
				if (!StringUtils.isEmpty(rowValue)) {
					additionalProperties.add(new FaxProperty(row.getKey(), rowValue));
				}
			}
		}
		return additionalProperties;
	}


	/**
	 * Creates a DateTimeRange object using the start and end date parameters
	 */
	public static DateTimeRange getDateTimeRange(Date startDate, Date endDate) {
		DateTimeRange range = new DateTimeRange();
		if (startDate != null) {
			XMLGregorianCalendar startCalendar = getXmlGregorianCalendarForDate(startDate);
			range.setBegin(startCalendar);
		}
		if (endDate != null) {
			XMLGregorianCalendar endCalendar = getXmlGregorianCalendarForDate(endDate);
			range.setEnd(endCalendar);
		}
		return range;
	}


	/**
	 * Convert a Date object to a XMLGregorianCalendar object
	 */
	public static XMLGregorianCalendar getXmlGregorianCalendarForDate(Date date) {
		return getDataTypeFactory().newXMLGregorianCalendar(DateUtils.getYear(date),
				DateUtils.getMonthOfYear(date),
				DateUtils.getDayOfMonth(date),
				DateUtils.getHours(date),
				DateUtils.getMinutes(date),
				DateUtils.getSeconds(date),
				DateUtils.getMilliseconds(date),
				TIME_ZONE_ID
		);
	}


	/**
	 * Converts an XMLGregorianCalendar to a Date object
	 */
	public static Date getDateFromXmlGregorianCalendar(XMLGregorianCalendar calendar) {
		return calendar.toGregorianCalendar().getTime();
	}


	private static DatatypeFactory getDataTypeFactory() {
		if (dataTypeFactory == null) {
			try {
				dataTypeFactory = DatatypeFactory.newInstance();
			}
			catch (Exception e) {
				throw new RuntimeException("Exception occurred when instantiating data type factory", e);
			}
		}
		return dataTypeFactory;
	}


	/**
	 * Takes a list of restrictions for the same date field and returns a map of start and end dates
	 */
	public static Map<String, Date> processDateRestrictions(List<SearchRestriction> restrictions) {
		if (!CollectionUtils.isEmpty(restrictions)) {
			Map<String, Date> map = new HashMap<>();
			for (SearchRestriction restriction : CollectionUtils.getIterable(restrictions)) {
				Date restrictionDate = DateUtils.clearTime(DateUtils.toDate((String) restriction.getValue()));
				switch (restriction.getComparison()) {
					case EQUALS:
						map.put("start", restrictionDate);
						//Set end to 2359hrs
						map.put("end", DateUtils.addMinutes(restrictionDate, 1439));
						break;
					case LESS_THAN:
						map.put("end", restrictionDate);
						break;
					case GREATER_THAN:
						map.put("start", restrictionDate);
						break;
				}
			}
			return map;
		}
		return null;
	}


	/**
	 * Prepends and appends wildcard search characters to the string parameter
	 */
	public static String addWildcardSearchCharacters(String searchValue) {
		return "%" + searchValue + "%";
	}
}
