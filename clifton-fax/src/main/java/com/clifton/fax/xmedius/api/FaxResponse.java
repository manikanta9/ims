package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FaxResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="FaxResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalItems" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FaxResponse", propOrder = {
		"totalItems"
})
@XmlSeeAlso({
		InboundUserFaxResponse.class,
		OutgoingFaxResponse.class,
		OutboundUserFaxResponse.class
})
public class FaxResponse {

	@XmlSchemaType(name = "unsignedInt")
	private long totalItems;


	/**
	 * Gets the value of the totalItems property.
	 */
	public long getTotalItems() {
		return this.totalItems;
	}


	/**
	 * Sets the value of the totalItems property.
	 */
	public void setTotalItems(long value) {
		this.totalItems = value;
	}
}
