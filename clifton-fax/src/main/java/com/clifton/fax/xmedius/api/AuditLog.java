package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuditLog complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AuditLog">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="faxBoxId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actionDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="isSuccess" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuditLog", propOrder = {
		"userId",
		"userType",
		"faxBoxId",
		"action",
		"actionDetails",
		"time",
		"isSuccess"
})
public class AuditLog {

	private String userId;
	private String userType;
	private String faxBoxId;
	private String action;
	private String actionDetails;
	private int time;
	private boolean isSuccess;


	/**
	 * Gets the value of the userId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserId() {
		return this.userId;
	}


	/**
	 * Sets the value of the userId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserId(String value) {
		this.userId = value;
	}


	/**
	 * Gets the value of the userType property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getUserType() {
		return this.userType;
	}


	/**
	 * Sets the value of the userType property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setUserType(String value) {
		this.userType = value;
	}


	/**
	 * Gets the value of the faxBoxId property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getFaxBoxId() {
		return this.faxBoxId;
	}


	/**
	 * Sets the value of the faxBoxId property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setFaxBoxId(String value) {
		this.faxBoxId = value;
	}


	/**
	 * Gets the value of the action property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getAction() {
		return this.action;
	}


	/**
	 * Sets the value of the action property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setAction(String value) {
		this.action = value;
	}


	/**
	 * Gets the value of the actionDetails property.
	 *
	 * @return possible object is
	 * {@link String }
	 */
	public String getActionDetails() {
		return this.actionDetails;
	}


	/**
	 * Sets the value of the actionDetails property.
	 *
	 * @param value allowed object is
	 *              {@link String }
	 */
	public void setActionDetails(String value) {
		this.actionDetails = value;
	}


	/**
	 * Gets the value of the time property.
	 */
	public int getTime() {
		return this.time;
	}


	/**
	 * Sets the value of the time property.
	 */
	public void setTime(int value) {
		this.time = value;
	}


	/**
	 * Gets the value of the isSuccess property.
	 */
	public boolean isIsSuccess() {
		return this.isSuccess;
	}


	/**
	 * Sets the value of the isSuccess property.
	 */
	public void setIsSuccess(boolean value) {
		this.isSuccess = value;
	}
}
