package com.clifton.fax.xmedius.converter;

import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.fax.FaxMessageSearchForm;
import com.clifton.fax.xmedius.api.OutgoingSearchInfo;
import com.clifton.fax.xmedius.search.OutgoingXmediusSearch;
import com.clifton.fax.xmedius.util.FaxMessageXmediusUtils;

import java.util.Date;
import java.util.Map;


public class FaxMessageSearchFormOutgoingXmediusSearchConverter implements Converter<FaxMessageSearchForm, OutgoingXmediusSearch> {

	@Override
	public OutgoingXmediusSearch convert(FaxMessageSearchForm from) {
		OutgoingXmediusSearch xmediusSearch = new OutgoingXmediusSearch();
		xmediusSearch.setMaxCount(from.getLimit());
		xmediusSearch.setStartIndex(from.getStart());
		xmediusSearch.setOutgoingSearchInfo(getOutgoingSearchInfo(from));
		return xmediusSearch;
	}


	private OutgoingSearchInfo getOutgoingSearchInfo(FaxMessageSearchForm from) {
		OutgoingSearchInfo searchInfo = new OutgoingSearchInfo();
		if (!StringUtils.isEmpty(from.getUserId())) {
			searchInfo.setUserId(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getUserId()));
		}

		if (!StringUtils.isEmpty(from.getFaxId())) {
			searchInfo.setTransactionId(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getFaxId()));
		}

		if (!StringUtils.isEmpty(from.getRecipientNumber())) {
			searchInfo.setOriginalDestination(FaxMessageXmediusUtils.addWildcardSearchCharacters(from.getRecipientNumber()));
		}

		if (!CollectionUtils.isEmpty(from.getRestrictionList())) {
			Map<String, Date> startDateRestrictions = FaxMessageXmediusUtils.processDateRestrictions(SearchUtils.getSearchRestrictionsForField(from.getRestrictionList(), "startDate"));
			if (!CollectionUtils.isEmpty(startDateRestrictions)) {
				searchInfo.setSubmittedTime(FaxMessageXmediusUtils.getDateTimeRange(startDateRestrictions.get("start"), startDateRestrictions.get("end")));
			}
		}

		return searchInfo;
	}
}
