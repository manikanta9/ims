package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getAuditLogsResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getAuditLogsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="response" type="{http://ws.xm.faxserver.com/}AuditLogResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAuditLogsResponse", propOrder = {
		"response"
})
public class GetAuditLogsResponse {

	private AuditLogResponse response;


	/**
	 * Gets the value of the response property.
	 *
	 * @return possible object is
	 * {@link AuditLogResponse }
	 */
	public AuditLogResponse getResponse() {
		return this.response;
	}


	/**
	 * Sets the value of the response property.
	 *
	 * @param value allowed object is
	 *              {@link AuditLogResponse }
	 */
	public void setResponse(AuditLogResponse value) {
		this.response = value;
	}
}
