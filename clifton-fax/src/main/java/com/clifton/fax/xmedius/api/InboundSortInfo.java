package com.clifton.fax.xmedius.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InboundSortInfo complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InboundSortInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sortProperty" type="{http://ws.xm.faxserver.com/}InboundSortProperty"/>
 *         &lt;element name="ascending" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InboundSortInfo", propOrder = {
		"sortProperty",
		"ascending"
})
public class InboundSortInfo {

	@XmlElement(required = true)
	@XmlSchemaType(name = "string")
	private InboundSortProperty sortProperty;
	private boolean ascending;


	/**
	 * Gets the value of the sortProperty property.
	 *
	 * @return possible object is
	 * {@link InboundSortProperty }
	 */
	public InboundSortProperty getSortProperty() {
		return this.sortProperty;
	}


	/**
	 * Sets the value of the sortProperty property.
	 *
	 * @param value allowed object is
	 *              {@link InboundSortProperty }
	 */
	public void setSortProperty(InboundSortProperty value) {
		this.sortProperty = value;
	}


	/**
	 * Gets the value of the ascending property.
	 */
	public boolean isAscending() {
		return this.ascending;
	}


	/**
	 * Sets the value of the ascending property.
	 */
	public void setAscending(boolean value) {
		this.ascending = value;
	}
}
