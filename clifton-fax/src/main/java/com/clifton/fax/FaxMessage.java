package com.clifton.fax;

import com.clifton.core.dataaccess.file.FileWrapper;

import java.util.Date;
import java.util.List;


/**
 * The <code>FaxMessage</code> represents a Fax object
 *
 * @author theodorez
 */
public class FaxMessage {

	/**
	 * Identifier that is assigned to a fax when it is processed by the fax server
	 */
	private String faxServerIdentifier;

	private String senderName;
	private String senderEmail;
	private String senderNumber;

	private String recipientName;
	private String recipientNumber;

	private List<FileWrapper> attachments;

	private String coverPage;
	private String subject;
	private String comments;

	private Exception exception;

	private List<FaxProperty> additionalProperties;

	/**
	 * The date the Fax server received the fax
	 */
	private Date startDate;

	/**
	 * The date the Fax server finished sending or receiving the fax
	 */
	private Date endDate;

	private Integer numberOfPages;

	private String status;


	public FaxMessage() {
		//empty default
	}


	public FaxMessage(String faxServerIdentifier) {
		this.setFaxServerIdentifier(faxServerIdentifier);
	}


	public String getFaxServerIdentifier() {
		return this.faxServerIdentifier;
	}


	public void setFaxServerIdentifier(String faxServerIdentifier) {
		this.faxServerIdentifier = faxServerIdentifier;
	}


	public String getSenderName() {
		return this.senderName;
	}


	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}


	public String getSenderEmail() {
		return this.senderEmail;
	}


	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}


	public String getSenderNumber() {
		return this.senderNumber;
	}


	public void setSenderNumber(String senderNumber) {
		this.senderNumber = senderNumber;
	}


	public String getRecipientName() {
		return this.recipientName;
	}


	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}


	public String getRecipientNumber() {
		return this.recipientNumber;
	}


	public void setRecipientNumber(String recipientNumber) {
		this.recipientNumber = recipientNumber;
	}


	public List<FileWrapper> getAttachments() {
		return this.attachments;
	}


	public void setAttachments(List<FileWrapper> attachment) {
		this.attachments = attachment;
	}


	public String getCoverPage() {
		return this.coverPage;
	}


	public void setCoverPage(String coverPage) {
		this.coverPage = coverPage;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getComments() {
		return this.comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}


	public Exception getException() {
		return this.exception;
	}


	public void setException(Exception exception) {
		this.exception = exception;
	}


	public List<FaxProperty> getAdditionalProperties() {
		return this.additionalProperties;
	}


	public void setAdditionalProperties(List<FaxProperty> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Integer getNumberOfPages() {
		return this.numberOfPages;
	}


	public void setNumberOfPages(Integer numberOfPages) {
		this.numberOfPages = numberOfPages;
	}


	public String getStatus() {
		return this.status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
}
