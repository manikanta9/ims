package com.clifton.fax;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.dataaccess.PagingArrayList;


/**
 * The <code>FaxMessageService</code> defines the methods for retrieving and sending faxes
 *
 * @author theodorez
 */
public interface FaxMessageService {


	/**
	 * Retrieve a fax object from the fax server.
	 */
	@SecureMethod(securityResource = "Fax")
	public FaxMessage getFax(String faxIdentifier);


	/**
	 * Get the attachment that was sent from the fax server
	 */
	@SecureMethod(securityResource = "Fax")
	public FileWrapper downloadFaxAttachment(String faxIdentifier);


	/**
	 * Submit a fax object to the fax server to send.
	 */
	@DoNotAddRequestMapping
	public FaxMessage sendFax(FaxMessage faxMessage);


	/**
	 * Get a list of faxes from the fax server.
	 */
	@SecureMethod(securityResource = "Fax")
	public PagingArrayList<FaxMessage> getFaxList(FaxMessageSearchForm searchForm);


	/**
	 * Check if the fax server is up
	 *
	 * @return true if server is running and accepting requests
	 */
	@SecureMethod(securityResource = "Fax")
	public boolean isFaxServerRunning();
}
