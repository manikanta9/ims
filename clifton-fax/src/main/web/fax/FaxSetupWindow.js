Clifton.fax.FaxSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'faxSetupWindow',
	title: 'Fax Setup',
	iconCls: 'fax',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Outbound Faxes',
				items: [{
					xtype: 'fax-grid',
					faxMessageType: 'OUTBOUND'
				}]
			}
		]
	}]
});
