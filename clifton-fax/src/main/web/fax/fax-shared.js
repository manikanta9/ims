Ext.ns('Clifton.fax');

Clifton.fax.FaxGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'faxListFind',
	xtype: 'gridpanel',
	remoteSort: true,
	hideToolsMenu: true,
	appendStandardColumns: false,
	faxMessageType: 'OUTBOUND',
	columns: [
		{header: 'Fax Identifier', width: 80, dataIndex: 'faxServerIdentifier', hidden: true},
		{
			header: '', width: 10,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('view', '', 'Download Fax', 'DOWNLOAD_FAX');
			}
		},
		{header: 'Recipient Name', width: 75, dataIndex: 'recipientName'},
		{header: 'Recipient Number', width: 35, dataIndex: 'recipientNumber'},
		{header: 'Sender', width: 50, dataIndex: 'senderEmail', filter: {searchFieldName: 'userId'}},
		{header: 'Subject', width: 100, dataIndex: 'subject'},
		{header: 'Comments', width: 150, dataIndex: 'comments', hidden: true},
		{header: 'Status', width: 20, dataIndex: 'status'},
		{header: 'Pages', width: 20, dataIndex: 'numberOfPages', type: 'int'},
		{header: 'Started', width: 40, dataIndex: 'startDate'},
		{header: 'Ended', width: 40, dataIndex: 'endDate'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fax.FaxWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.json.faxServerIdentifier;
		}
	},
	gridConfig: {
		listeners: {
			rowclick: function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					if (eventName === 'DOWNLOAD_FAX') {
						const url = 'faxAttachmentDownload.json?faxIdentifier=' + row.json.faxServerIdentifier;
						TCG.downloadFile(url, null, grid);
					}
				}
			}
		}
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('endDate', {'after': new Date().add(Date.DAY, -30)});
		}
		return {faxMessageType: this.faxMessageType};
	}
});
Ext.reg('fax-grid', Clifton.fax.FaxGrid);

