Clifton.fax.FaxWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Fax Detail',
	height: 600,
	width: 800,

	items: [{
		xtype: 'formwithtabs',
		url: 'fax.json',
		readOnly: true,
		getLoadParams: function(win) {
			return {faxIdentifier: win.params.id};
		},
		items: [{
			xtype: 'tabpanel',
			items: [

				{
					title: 'Overview',
					tbar: [
						{
							text: 'Download Fax',
							tooltip: 'Download the fax attachments',
							iconCls: 'arrow-down-green',
							handler: function() {
								const fp = TCG.getParentFormPanel(this);
								const url = 'faxAttachmentDownload.json?faxIdentifier=' + fp.getForm().findField('faxServerIdentifier').getValue();
								TCG.downloadFile(url, null, this);
							}
						}
					],
					items: [{
						xtype: 'formfragment',
						items: [
							{fieldLabel: 'Fax Identifier', name: 'faxServerIdentifier'},
							{fieldLabel: 'Subject', name: 'subject'},
							{fieldLabel: 'Recipient Name', name: 'recipientName'},
							{fieldLabel: 'Recipient Number', name: 'recipientNumber'},
							{fieldLabel: 'Sender', name: 'senderEmail'},
							{fieldLabel: 'Comments', name: 'comments'},
							{fieldLabel: 'Status', name: 'status'},
							{fieldLabel: 'Pages', name: 'numberOfPages'},
							{fieldLabel: 'Date Submitted', name: 'startDate'},
							{fieldLabel: 'Date Completed', name: 'endDate'},
							{
								xtype: 'formgrid',
								title: 'Additional Properties',
								storeRoot: 'additionalProperties',
								dtoClass: 'com.clifton.fax.FaxProperty',
								frame: false,
								readOnly: true,
								columnsConfig: [
									{header: 'Property Name', dataIndex: 'faxPropertyName', width: 165},
									{header: 'Property Value', dataIndex: 'faxPropertyValue', width: 575}
								]
							}
						]
					}]
				}
			]
		}]
	}]
});
