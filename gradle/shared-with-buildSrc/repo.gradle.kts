// Since this file is shared with the build for the buildSrc project, types within buildSrc cannot be used, so we manually reference "extra" properties by name
repositories {
    clear()
    maven {
        name = "${extra["repo.name"]}"
        setUrl("${extra["repo.host"]}/${extra["repo.path"]}")
        credentials {
            username = "${extra["repo.username"]}"
            password = "${extra["repo.password"]}"
        }
    }
}
