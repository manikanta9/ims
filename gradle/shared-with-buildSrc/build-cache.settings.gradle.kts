// Note: This script must only be applied to a Settings object

// buildSrc sources cannot be accessed during settings processing; "Extra" properties must be accessed by using their keys directly.
val enableBuildCache: String by extra
val enableBuildCacheLocal: String by extra
val enableBuildCacheRemote: String by extra
val gradleEnterpriseHost: String by extra

// Configure build cache
val isCiServer = System.getenv().containsKey("bamboo_repository_name")
buildCache {
	local {
		isEnabled = enableBuildCache.toBoolean() && enableBuildCacheLocal.toBoolean() && !isCiServer
	}
	remote<HttpBuildCache> {
		isEnabled = enableBuildCache.toBoolean() && enableBuildCacheRemote.toBoolean()
		setUrl("$gradleEnterpriseHost/cache/")
		isAllowUntrustedServer = true
		isPush = isCiServer
	}
}

// Re-run all tasks without using the cache for force-clean builds
if (System.getenv("bamboo_FORCE_CLEAN")?.toBoolean() == true) {
	gradle.startParameter.isRerunTasks = true
}
