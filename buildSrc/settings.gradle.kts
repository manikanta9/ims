// Plugin repository configuration must be manually configured in settings scripts; see https://docs.gradle.org/current/userguide/upgrading_version_5.html#the_pluginmanagement_block_in_settings_scripts_is_now_isolated
pluginManagement {
    repositories {
        clear()
        maven {
            name = "${extra["repo.name"]}"
            setUrl("${extra["repo.host"]}/${extra["repo.path"]}")
            credentials {
                username = "${extra["repo.username"]}"
                password = "${extra["repo.password"]}"
            }
        }
    }
}

// Note: buildSrc sources cannot be accessed from within the settings.gradle.kts file. "Extra" properties must be accessed by using their keys directly.
val buildRootPath: String by extra

// Apply shared settings
apply(from = "../$buildRootPath/shared-with-buildSrc/logging.settings.gradle.kts")
// Note: This import *should* be unnecessary, and Gradle 6.0 now uses the top-level build cache configuration for buildSrc projects:
// https://docs.gradle.org/6.0.1/userguide/upgrading_version_5.html#buildsrc_projects_automatically_use_build_cache_configuration
apply(from = "../$buildRootPath/shared-with-buildSrc/build-cache.settings.gradle.kts")
