package com.clifton.gradle

import com.clifton.gradle.util.ExtensionProperties
import com.clifton.gradle.util.MappingExtraPropertyValueDelegateProvider
import com.clifton.gradle.util.resolve
import org.gradle.api.Project
import org.gradle.api.plugins.ExtraPropertiesExtension
import org.gradle.api.provider.Provider

/*
 * Internal inline functions should be placed here. By marking these functions as internal, they are removed from the public API, allowing Gradle's Kotlin DSL script compilation
 * avoidance to be enabled. This file is primarily intended for the use of *reified* inline functions, since reification can only be performed with inline functions. See
 * https://docs.gradle.org/6.8.3/release-notes.html#kotlin-dsl-script-compilation-improvements for more information.
 */


/**
 * Creates an [ExtensionProperties] object.
 */
internal inline fun <reified T : Any> ExtensionProperties.Companion.create(project: Project, finalizeOnRead: Boolean = true) = create(project, T::class.java, finalizeOnRead)


/**
 * Maps any discovered [ExtraPropertiesExtension] value from a [String] to an object of the target type via the given mapping function. The resulting value is saved in place. The
 * target type must not be identical to the source type ([String]).
 */
internal inline fun <reified T> ExtraPropertiesExtension.mapping(noinline mappingFn: (String) -> T): MappingExtraPropertyValueDelegateProvider<String, T> = MappingExtraPropertyValueDelegateProvider(extra = this, mappingFn = mappingFn, sourceType = String::class.java, targetType = T::class.java)
internal inline fun <reified T> ExtraPropertiesExtension.mapping(name: String, noinline mappingFn: (String) -> T): MappingExtraPropertyValueDelegateProvider<String, T> = MappingExtraPropertyValueDelegateProvider(extra = this, name = name, mappingFn = mappingFn, sourceType = String::class.java, targetType = T::class.java)


/**
 * Resolves the given argument using providers, lists, and other available live collections.
 *
 * Resolution performed by this function simply flattens collections and [Provider] instances. Built-in resolvers such as [Project.files] should be preferred when available.
 */
internal inline fun <reified T : Any> resolve(arg: Any?, flatten: Boolean = true, noinline cases: ((Any?) -> T?)? = null): List<T> = resolve(arg, T::class, flatten, cases)
