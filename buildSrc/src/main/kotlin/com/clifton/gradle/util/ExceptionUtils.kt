package com.clifton.gradle.util


/**
 * Attempts to execute the given function, returning `true` if the function succeeded or `false` if an exception was thrown.
 *
 * This can be useful for determining the output of functions from external APIs, such as determining the presence of a class via the [ClassLoader.loadClass] method.
 */
fun <T : Any?> tryOrFalse(fn: () -> T): Boolean {
	return try {
		fn()
		true
	}
	catch (e: Exception) {
		false
	}
}
