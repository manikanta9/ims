package com.clifton.gradle.util

import org.gradle.api.Plugin
import org.gradle.api.Project


/**
 * Asserts that the given plugin project is the root project. This is helpful for plugins which can only be applied to the root project.
 */
fun Plugin<Project>.requireRootProject(project: Project) {
	require(project == project.rootProject) {
		"Error applying plugin [${this::class.simpleName}] to project [${project.name}]. The plugin can only be applied to the root project ([${project.rootProject.name}])."
	}
}
