package com.clifton.gradle.util

import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.provider.Provider
import java.io.File


/**
 * Returns the given file, expanding the file into a [org.gradle.api.file.FileTree] if it is an archive.
 *
 * This can be used to more easily handle file references such as dependencies which may or may not be packed into individual archives.
 */
fun Project.expandArchive(file: File): Any {
	if (!file.isFile) {
		return file
	}
	return when (file.extension.toLowerCase()) {
		"zip", "rar", "7z", "jar", "war", "ear" -> project.zipTree(file)
		"tar", "gz", "xz", "tgz", "txz" -> project.tarTree(file)
		else -> file
	}
}


/**
 * Maps the [Configuration] provider to a provider of files for which the given transform function is applied.
 */
fun Provider<Configuration>.mapFiles(transform: (File) -> Any): Provider<List<Any>> {
	return this.map { config -> config.map(transform) }
}
