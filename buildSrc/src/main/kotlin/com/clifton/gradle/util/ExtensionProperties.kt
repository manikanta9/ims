@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.util

import org.gradle.api.Project
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider

/**
 * The [ExtensionProperties] type provides by-name [Property] management. The API is roughly similar to that of [Map], but customized for the use of [Property] objects.
 *
 * Contents may be given [conventions][Property.convention], [values][Property.value], and may be used as [providers][Provider]. Each property is automatically [finalized]
 * [Property.finalizeValue] on read, at which point subsequent mutation attempts will throw exception, guaranteeing that the read value is the final value. Additionally, this
 * property holder is finalized by default when calling any read operation, guaranteeing that no mutations may occur after any read. This can be disabled with the [finalizeOnRead]
 * property.
 *
 * This type is adds flexibility not provided by [MapProperty]. [MapProperty] is functionally very similar to any other [Property] in that it can only hold a single real value and
 * a single convention value. [ExtensionProperties] enhances this by producing a map of [Property] values, each of which can have its own real value and its own convention value.
 * This allows conventions to be set for individual map values by key.
 *
 * @author MikeH
 * @see Property
 * @see MapProperty
 */
class ExtensionProperties<T : Any> private constructor(
	private val project: Project,
	private val valueType: Class<T>,
	private val finalizeOnRead: Boolean
) {

	companion object {
		fun <T : Any> create(project: Project, valueType: Class<T>, finalizeOnRead: Boolean) = ExtensionProperties(project, valueType, finalizeOnRead)
	}

	private var finalized = false
	private val backingProperties = mutableMapOf<String, Property<T>>()
	private val finalizedProperties = InitOnce<Map<String, T>>()


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	operator fun get(name: String): T {
		if (finalizeOnRead) {
			finalize()
		}
		return if (finalized) {
			getFinalizedMap().getValue(name)
		}
		else {
			getProperty(name).get()
		}
	}


	fun getOrNull(name: String): T? {
		if (finalizeOnRead) {
			finalize()
		}
		return if (finalized) {
			getFinalizedMap()[name]
		}
		else {
			getProperty(name).orNull
		}
	}


	operator fun contains(name: String): Boolean {
		if (finalizeOnRead) {
			finalize()
		}
		return if (finalized) {
			name in getFinalizedMap()
		}
		else {
			hasProperty(name)
		}
	}


	operator fun set(name: String, value: T) {
		assertCanMutate()
		getProperty(name).set(value)
	}


	operator fun set(name: String, valueProvider: Provider<T>) {
		assertCanMutate()
		getProperty(name).set(valueProvider)
	}


	operator fun plusAssign(map: Map<String, T>) {
		assertCanMutate()
		map.forEach { (key, value) -> this[key] = value }
	}


	fun setConvention(name: String, value: T) {
		assertCanMutate()
		@Suppress("UnstableApiUsage")
		getProperty(name).convention(value)
	}


	fun setConvention(name: String, valueProvider: Provider<T>) {
		assertCanMutate()
		@Suppress("UnstableApiUsage")
		getProperty(name).convention(valueProvider)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	fun asMap(): Map<String, T> {
		if (finalizeOnRead) {
			finalize()
		}
		return if (finalized) {
			getFinalizedMap()
		}
		else {
			computePropertyMap()
		}
	}


	fun hasProperty(name: String): Boolean {
		return backingProperties.containsKey(name)
	}


	fun getProperty(name: String): Property<T> {
		return if (finalized) {
			backingProperties.getValue(name)
		}
		else {
			backingProperties.computeIfAbsent(name) {
				// Individual properties are always finalized on read
				project.objects.property(valueType).apply { finalizeValueOnRead() }
			}
		}
	}


	fun getPropertyMap(): Map<String, Property<T>> {
		if (finalizeOnRead) {
			finalize()
		}
		return backingProperties
	}


	fun finalize() {
		if (!finalized) {
			finalized = true
			backingProperties.values.forEach { it.finalizeValue() }
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun computePropertyMap(): Map<String, T> {
		return backingProperties
				.filterValues { it.isPresent }
				.mapValues { it.value.get() }
	}


	private fun getFinalizedMap(): Map<String, T> {
		check(finalized) { "The map is not finalized." }
		return finalizedProperties.getOrSet { computePropertyMap() }
	}


	private fun assertCanMutate() {
		check(!finalized) { "Unable to perform mutation operations. The properties object has been finalized." }
	}
}
