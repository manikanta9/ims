package com.clifton.gradle.util

import org.gradle.api.plugins.ExtraPropertiesExtension
import org.gradle.kotlin.dsl.InitialValueExtraPropertyDelegate
import org.gradle.kotlin.dsl.MutablePropertyDelegate
import org.gradle.kotlin.dsl.invoke
import org.gradle.kotlin.dsl.provideDelegate
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


/**
 * Maps a delegated property by a given specified name, rather than the name of the property itself. This can be useful for shortening property names or for using property names
 * with special symbols which are not allowed in variable names.
 */
fun ExtraPropertiesExtension.named(propertyName: String): NamedExtraPropertyValueDelegate = NamedExtraPropertyValueDelegate(this, propertyName)

class NamedExtraPropertyValueDelegate(
	private val extra: ExtraPropertiesExtension,
	private val propertyName: String
) {

	operator fun <T> setValue(receiver: Any, property: KProperty<*>, value: T) = extra.set(propertyName, value)

	@Suppress("unchecked_cast")
	operator fun <T> getValue(receiver: Any, property: KProperty<*>): T = extra.get(propertyName)!! as T
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * Maps to an [ExtraPropertiesExtension] property, using the default value if and only if no value (or a blank string) exists.
 */
fun <T> ExtraPropertiesExtension.withDefault(defaultValue: T): DefaultExtraPropertyValueDelegateProvider<T> = DefaultExtraPropertyValueDelegateProvider(this) { defaultValue }

@Suppress("unused")
fun <T> ExtraPropertiesExtension.withDefaultSupplier(defaultValueSupplier: () -> T): DefaultExtraPropertyValueDelegateProvider<T> = DefaultExtraPropertyValueDelegateProvider(this, defaultValueSupplier)

class DefaultExtraPropertyValueDelegateProvider<T>(
	private val extra: ExtraPropertiesExtension,
	private val defaultValueSupplier: () -> T
) {

	operator fun provideDelegate(thisRef: Any?, property: KProperty<*>): InitialValueExtraPropertyDelegate<T> {
		val existingValue = if (extra.has(property.name)) extra.get(property.name) else null

		@Suppress("UNCHECKED_CAST")
		val initialValue = existingValue?.takeIf { it !is String || it.isNotBlank() } as T
				?: defaultValueSupplier()
		return extra(initialValue).provideDelegate(extra, property)
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


class MappingExtraPropertyValueDelegateProvider<S, T>(
	private val extra: ExtraPropertiesExtension,
	private val name: String? = null,
	private val mappingFn: (S) -> T,
	sourceType: Class<S>,
	private val targetType: Class<T>
) : ReadWriteProperty<Any, T> {

	private lateinit var backingDelegate: MutablePropertyDelegate
	private lateinit var propertyDelegate: KProperty<Any>

	init {
		require(sourceType != targetType) { "The [${this::class.simpleName}] type can only be used to map delegated values from one type to another. The source type [${sourceType.simpleName}] and the target type [${targetType.simpleName}] may not be the same type." }
	}

	operator fun provideDelegate(thisRef: Any, property: KProperty<*>): ReadWriteProperty<Any, T> {
		@Suppress("UNCHECKED_CAST")
		propertyDelegate = object : KProperty<Any> by (property as KProperty<Any>) {
			override val name: String
				get() = this@MappingExtraPropertyValueDelegateProvider.name ?: property.name
		}
		backingDelegate = extra.provideDelegate(extra, propertyDelegate)
		return this
	}

	@Suppress("UNCHECKED_CAST")
	override fun getValue(thisRef: Any, property: KProperty<*>): T {
		return backingDelegate.getValue<S>(extra, propertyDelegate)
				.also { if (it == null) throw NullPointerException("Unexpected missing property [${propertyDelegate.name}]. The type must be non-null.") }
				.let { currentValue -> if (!targetType.isInstance(currentValue)) mappingFn(currentValue).also { setValue(thisRef, propertyDelegate, it) } else currentValue as T }
	}

	override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
		return backingDelegate.setValue(extra, propertyDelegate, value)
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * Optionally gets the named property if it exists.
 */
fun ExtraPropertiesExtension.getOptional(property: String) = if (this.has(property)) this[property] else null
