package com.clifton.gradle.util

import org.gradle.api.Task
import org.gradle.api.internal.TaskOutputsInternal
import org.gradle.kotlin.dsl.support.serviceOf
import org.gradle.language.base.internal.tasks.StaleOutputCleaner
import java.io.File


/**
 * Cleans any stale outputs for this task. This will find and delete all outputs previously produced by this task. If the designated output for the task is not a single directory,
 * then the [directoryToClean] value must be provided.
 */
fun Task.cleanStaleOutputs(directoryToClean: File = outputs.files.singleFile) {
	require(outputs is TaskOutputsInternal) { "Unable to clean stale outputs for task [$name]. The task output type for stale outputs must be [${TaskOutputsInternal::class.simpleName}], but the discovered type was [${outputs::class.simpleName}]." }
	StaleOutputCleaner.cleanOutputs(project.serviceOf(), (outputs as TaskOutputsInternal).previousOutputFiles, directoryToClean)
}
