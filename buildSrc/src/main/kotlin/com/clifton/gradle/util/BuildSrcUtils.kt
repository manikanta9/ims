package com.clifton.gradle.util

import org.gradle.api.provider.Provider
import java.util.*
import kotlin.reflect.KClass


/**
 * @see [com.clifton.gradle.resolve]
 */
fun <T : Any> resolve(arg: Any?, type: KClass<T>, flatten: Boolean = true, cases: ((Any?) -> T?)? = null): List<T> {
	val result = mutableListOf<T>()
	val stack = ArrayDeque<Any?>()
	arg?.let { stack.push(it) }
	while (stack.isNotEmpty()) {
		val element = requireNotNull(stack.pop())
		when {
			type.isInstance(element) -> @Suppress("UNCHECKED_CAST") result.add(element as T)
			element is Provider<*> -> element.get()?.let { stack.push(it) }
			flatten && element is Iterable<*> -> element.reversed().forEach { nullableEl -> nullableEl?.let { stack.push(it) } }
			else -> cases?.invoke(element)?.let { stack.push(it) }
					?: error("Unable to resolve argument of type [${element::class.qualifiedName}] and value [$element].")
		}
	}
	return result
}
