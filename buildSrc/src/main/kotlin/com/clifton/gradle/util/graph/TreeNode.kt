package com.clifton.gradle.util.graph

import java.util.*


/**
 * The [TreeNode] type implements a tree structure with a [root] element and [children] nodes.
 */
class TreeNode<T>(
	@Suppress("MemberVisibilityCanBePrivate") val root: T,
	@Suppress("MemberVisibilityCanBePrivate") val children: List<TreeNode<T>> = emptyList()
) {

	/**
	 * Iterates the tree in depth-first order, starting at the root node and traversing down each branch.
	 *
	 * Note: This assumes that the tree is immutable and performs no checks for concurrent mutation.
	 *
	 * @param reversed if `true`, the depth-first order will be reversed, meaning that iteration will begin at the last leaf
	 * @param excludeSelf if `true`, the root node is not included as part of the resulting sequence
	 * @param distinct if `true`, redundant nodes will be skipped (**Note: There are no guarantees for order or distinct characteristics when processing sequence elements
	 * concurrently.**)
	 */
	fun iterateDepthFirst(reversed: Boolean = false, excludeSelf: Boolean = false, distinct: Boolean = false): Sequence<T> {
		return doIterateDepthFirst(reversed, excludeSelf, distinct, visitedSet = mutableSetOf())
	}


	private fun doIterateDepthFirst(reversed: Boolean, excludeSelf: Boolean, distinct: Boolean, visitedSet: MutableSet<T>): Sequence<T> = sequence {
		if (distinct && root in visitedSet) {
			return@sequence
		}
		if (!reversed) {
			if (!excludeSelf) {
				yield(root)
				visitedSet.add(root)
			}
			children.forEach { yieldAll(it.doIterateDepthFirst(reversed, false, distinct, visitedSet = visitedSet)) }
		}
		else {
			children.reversed().forEach { yieldAll(it.doIterateDepthFirst(reversed, false, distinct, visitedSet = visitedSet)) }
			if (!excludeSelf) {
				yield(root)
				visitedSet.add(root)
			}
		}
	}


	/**
	 * Iterates the tree in breadth-first order, starting at the root node and then traversing across each node at any given level of depth before continuing to the nodes at the
	 * next level of depth.
	 */
	fun iterateBreadthFirst(): Sequence<T> = sequence {
		val sequenceDeque = ArrayDeque<TreeNode<T>>()
		sequenceDeque.add(this@TreeNode)
		while (sequenceDeque.isNotEmpty()) {
			val node = sequenceDeque.pop()
			yield(node.root)
			sequenceDeque.addAll(node.children)
		}
	}
}
