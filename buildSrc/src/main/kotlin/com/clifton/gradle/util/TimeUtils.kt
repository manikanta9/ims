package com.clifton.gradle.util

import org.gradle.api.Project
import org.gradle.api.logging.LogLevel
import java.io.OutputStream
import java.text.DecimalFormat


private val NUMBER_FORMAT_SECONDS_PRECISE = DecimalFormat("#,##0.000")


/**
 * Executes the given [action] and prints the amount of time taken (in nanoseconds) to the provided output stream, or to the standard output stream if no stream is provided.
 */
fun <T : Any?> time(label: String, outputStream: OutputStream = System.out, action: () -> T): T {
	val t0 = System.nanoTime()
	val result = action()
	val t1 = System.nanoTime()
	val elapsed = (t1 - t0).toBigDecimal().movePointLeft(9) // Convert to seconds
	val elapsedStr = NUMBER_FORMAT_SECONDS_PRECISE.format(elapsed)
	outputStream.writer().use { it.appendln("[${label}] Execution time: [$elapsedStr] seconds") }
	return result
}


/**
 * Executes the given [action] and prints the amount of time taken (in nanoseconds) using the configured [LogLevel].
 */
fun <T : Any?> Project.time(label: String, logLevel: LogLevel = LogLevel.DEBUG, action: () -> T): T {
	// Guard-clause: Ignore time tracking
	if (!this.logger.isEnabled(logLevel)) {
		return action()
	}
	val t0 = System.nanoTime()
	val result = action()
	val t1 = System.nanoTime()
	val elapsed = (t1 - t0).toBigDecimal().movePointLeft(9) // Convert to seconds
	val elapsedStr = NUMBER_FORMAT_SECONDS_PRECISE.format(elapsed)
	this.logger.log(logLevel, "[${label}] Execution time: [$elapsedStr] seconds")
	return result
}
