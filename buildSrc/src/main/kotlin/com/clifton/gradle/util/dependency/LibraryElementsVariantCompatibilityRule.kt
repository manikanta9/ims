package com.clifton.gradle.util.dependency

import org.gradle.api.attributes.AttributeCompatibilityRule
import org.gradle.api.attributes.CompatibilityCheckDetails
import org.gradle.api.attributes.LibraryElements
import org.gradle.api.internal.ReusableAction
import javax.inject.Inject


/**
 * The [AttributeCompatibilityRule] type for allowing a [LibraryElements] attribute value ([compatibleProducerName]) to satisfy variant-aware requests which require the
 * [requestedFeatureName] library element type.
 *
 * Semantically, this compatibility rule states that the artifact of the "compatible" variant contains and supersedes the contents of the "requested" variant artifact. This is
 * necessary during variant selection for determining a compatible <em>primary</em> variant for dependencies. Since the configuration dependency tree is built off of primary
 * dependencies, the primary variant must be matched first. The secondary variant, which is used to select the requested resources directly, can then be used during artifact
 * dependency resolution. This allows tasks to depend solely on the secondary variant without requiring the entirety of the primary variant.
 *
 * These compatibility rules can be used for such functionality as creating a dependency solely on the schema mapping resources for an upstream project. See
 * [CliftonSchemaPlugin][com.clifton.gradle.plugin.artifact.CliftonSchemaPlugin] for more details.
 *
 * Example usage:
 * ```
 * // Create a configuration which requests a specific variant feature
 * project.configurations.register("myConfiguration") {
 *     attributes {
 *         attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE, project.objects.named(LibraryElements::class, "mySecondaryVariantLibraryElementsAttribute"))
 *     }
 * }
 * // Create a secondary variant which provides the requested feature; since the primary variant for the configuration produces the JAR artifact, this secondary variant should be
 * // made to be compatible with the JAR library elements type
 * project.configurations.named("runtimeApiElements") {
 *     outgoing {
 *         variants {
 *             create("mySecondaryVariant") {
 *                 artifact(myTask)
 *                 attributes {
 *                     attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE, project.objects.named(LibraryElements::class, "mySecondaryVariantLibraryElementsAttribute"))
 *                 }
 *             }
 *         }
 *     }
 * }
 * // Configure the compatibility rule to allow the primary variant's JAR feature to satisfy the secondary variant request
 * project.dependencies {
 *     attributesSchema {
 *         attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE) {
 *             compatibilityRules.add(LibraryElementsVariantCompatibilityRule::class) {
 *                 params("mySecondaryVariantLibraryElementsAttribute", LibraryElements.JAR)
 *             }
 *         }
 *     }
 * }
 * ```
 *
 * See [https://docs.gradle.org/current/userguide/variant_model.html#sec:variant-aware-matching] for more information on variant-aware matching.
 *
 * See [org.gradle.api.internal.artifacts.JavaEcosystemSupport.LibraryElementsCompatibilityRules] for the default compatibility rule which shows how the variant for the JAR
 * artifact is configured to supersede the contents of the "classes" variant artifact, very similarly to what is done in this compatibility rule.
 *
 * See [org.gradle.api.internal.artifacts.ivyservice.resolveengine.artifact.BuildDependenciesOnlyVisitedArtifactSet.select] for secondary variant selection.
 *
 * See the `outgoingVariants` task for a listing of primary and secondary variants for any project.
 *
 * @author MikeH
 */
class LibraryElementsVariantCompatibilityRule @Inject constructor(
	/**
	 * The requested feature name. When a variant-aware request for this feature name is executed, a primary variant using the [compatibleProducerName] value will satisfy.
	 */
	private val requestedFeatureName: String,
	/**
	 * The compatible producer feature name. This is the [LibraryElements] value for the variant which supersedes the [requestedFeatureName] variant. A common value for this is
	 * [LibraryElements.JAR].
	 */
	private val compatibleProducerName: String
) : AttributeCompatibilityRule<LibraryElements>, ReusableAction {

	override fun execute(details: CompatibilityCheckDetails<LibraryElements>) {
		val consumerValue = details.consumerValue
		val producerValue = details.producerValue ?: error("Unexpected null value found for producer value during compatibility check for details [$details].")
		if (consumerValue == null) {
			// Consumer didn't express any preferences; everything fits
			details.compatible()
			return
		}
		// Mark produced "compatible" artifacts as compatible for consumers requesting the given feature
		if (consumerValue.name == requestedFeatureName && producerValue.name == compatibleProducerName) {
			details.compatible()
		}
	}
}
