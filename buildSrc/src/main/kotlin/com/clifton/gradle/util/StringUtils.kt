package com.clifton.gradle.util

import java.util.*


/**
 * Tokenizes the given string into a list using the provided delimiter set.
 */
@Suppress("UNCHECKED_CAST")
fun String.tokenize(delimiters: String = " \t\n\r\u000C"): List<String> {
	return StringTokenizer(this, delimiters).toList() as List<String>
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * Converts the string from its current form to *camelCase*. See [WORD_DELIMITER_REGEX] for more information on word splits.
 */
fun String.toCamelCase() = this.split(WORD_DELIMITER_REGEX).joinToString("") { it.toLowerCase().capitalize() }.decapitalize()


/**
 * Converts the string from its current form to *PascalCase*. See [WORD_DELIMITER_REGEX] for more information on word splits.
 */
fun String.toPascalCase() = this.split(WORD_DELIMITER_REGEX).joinToString("") { it.toLowerCase().capitalize() }


/**
 * Converts the string from its current form to *snake_case*. See [WORD_DELIMITER_REGEX] for more information on word splits.
 */
fun String.toSnakeCase() = this.split(WORD_DELIMITER_REGEX).joinToString("_") { it.toLowerCase() }


/**
 * Converts the string from its current form to *UPPER_SNAKE_CASE*. See [WORD_DELIMITER_REGEX] for more information on word splits.
 */
fun String.toUpperSnakeCase() = this.split(WORD_DELIMITER_REGEX).joinToString("_") { it.toUpperCase() }


/**
 * Converts the string from its current form to *kebab-case*. See [WORD_DELIMITER_REGEX] for more information on word splits.
 */
fun String.toKebabCase() = this.split(WORD_DELIMITER_REGEX).joinToString("-") { it.toLowerCase() }


/**
 * The regular expression which matches suitable word delimiters for case conversions. Matches the following patterns and boundaries:
 * - Any sequence of non-alpha-numeric characters
 * - After any lowercase letter followed by a number
 * - After any lowercase letter or number followed by an uppercase letter
 * - After any number followed by a letter
 */
private val WORD_DELIMITER_REGEX = Regex("""[^\p{Alnum}]+|(?<=[a-z0-9])(?=[A-Z])|(?<=[a-z])(?=[0-9])|(?<=[0-9])(?=[a-zA-Z])""")
