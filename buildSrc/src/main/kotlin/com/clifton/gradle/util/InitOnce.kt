package com.clifton.gradle.util

/**
 * A wrapper object for values which should be initialized only a single time. This can be used as a set-if-null utility for shared volatile properties.
 *
 * This differs from [lazy] in that the supplier for the property is provided upon first use, rather than upon creation. This allows the supplier to use other properties which may
 * only be available after field initialization.
 *
 * Example:
 * ```
 * class MyClass {
 *
 *     private val myExpensiveValue = InitOnce<String>()
 *
 *     fun getMyExpensiveValue(name: String) {
 *         this.myExpensiveValue.getOrSet { expensiveOperation(name) }
 *     }
 *
 *     private fun expensiveOperation(name: String): String {
 *         Thread.sleep(10000)
 *         return "Hello, $name"
 *     }
 * }
 * ```
 */
@Suppress("unused") // This class is currently not used, but is saved as a utility class for any later needs
class InitOnce<T : Any> {

	@Volatile
	private lateinit var property: T
	private val lock = Object()

	@Suppress("MemberVisibilityCanBePrivate") // Intentionally public
	val isInitialized: Boolean
		get() = ::property.isInitialized

	fun getOrSet(supplier: () -> T): T {
		// Double-checked locking
		if (!isInitialized) {
			synchronized(lock) {
				if (!isInitialized) {
					property = supplier()
				}
			}
		}
		return property
	}
}
