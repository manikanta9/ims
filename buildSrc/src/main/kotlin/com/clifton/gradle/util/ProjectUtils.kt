package com.clifton.gradle.util

import com.clifton.gradle.util.graph.TreeNode
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.execution.taskgraph.TaskExecutionGraphInternal
import org.gradle.kotlin.dsl.extra
import org.gradle.process.ExecResult
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.jar.Attributes
import java.util.jar.Manifest
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import kotlin.collections.set


/**
 * Executes a process from the given list of arguments, delimiting each list element as a single argument and returning the trimmed standard output text. Exceptions will be thrown
 * on non-zero exit values. This is preferred over the standard [String] [Project.executeSimple] when elements may contain characters that would cause issues with argument
 * delimiting, such as spaces, double quotes, or escape characters.
 *
 * @param arguments the list of arguments to execute
 * @param outputStream if provided, then the resulting output will also be written directly to this stream, even in the event of an exception
 * @param captureStdErr if `true`, capture stderr in the provided output stream; otherwise, forward directly to [System.err]
 */
fun Project.execute(vararg arguments: String, outputStream: OutputStream? = null, captureStdErr: Boolean = false): String {
	ByteArrayOutputStream().use { out ->
		try {
			exec {
				commandLine(*arguments)
				standardOutput = out
				errorOutput = if (captureStdErr) out else System.err
			}
			return@execute out.toString().trim()
		}
		finally {
			// Write to the given output stream, if provided
			outputStream?.write(out.toByteArray())
		}
	}
}

/**
 * Executes the given string, using the space character as the argument delimiter and returning the trimmed standard output text. Exceptions will be thrown on non-zero exit values.
 */
fun Project.executeSimple(command: String): String = this.execute(*command.tokenize().toTypedArray())

/**
 * Executes a process from the given list of arguments, delimiting each list element as a single argument and returning the trimmed standard output text. This provides more fine-
 * grained results than the standard [Project.execute] functions, allowing the exit code and standard and error output to be processed independently.
 *
 * @param arguments the arguments to execute
 */
fun Project.executeWithResult(vararg arguments: String): ExecuteResult {
	ByteArrayOutputStream().use { standardOutput ->
		ByteArrayOutputStream().use { errorOutput ->
			val execResult = exec {
				commandLine(*arguments)
				isIgnoreExitValue = true
				this.standardOutput = standardOutput
				this.errorOutput = errorOutput
			}
			return ExecuteResult(
					execResult,
					standardOutput.toString().trim(),
					errorOutput.toString().trim()
			)
		}
	}
}

/**
 * An execution result data object including the exit code, standard output, and error output from the command.
 */
data class ExecuteResult(val result: ExecResult, val standardOutput: String, val errorOutput: String)

/**
 * Generates a JAR file with the given class path included as the manifest class path. This can be used to address command line overflow issues during Java execution. The generated
 * file will be marked for automatic deletion on build completion or on JVM termination, which is typically when Gradle or the Gradle Daemon terminates.
 */
fun Project.classpathJar(classpathElements: Iterable<String>, file: File? = null): File {
	val jarFile = file ?: File.createTempFile("gradle-classpath-jar", ".jar").also {
		this.gradle.buildFinished { it.delete() }
		it.deleteOnExit() // Delete on JVM exit if the process is terminated during a build
	}
	ZipOutputStream(FileOutputStream(jarFile)).use { zipStream ->
		zipStream.putNextEntry(ZipEntry("META-INF/MANIFEST.MF"))
		Manifest().apply {
			mainAttributes[Attributes.Name.MANIFEST_VERSION] = "1.0"
			mainAttributes[Attributes.Name.CLASS_PATH] = classpathElements.joinToString(" ")
		}.write(zipStream)
	}
	return jarFile
}

/**
 * @see classpathJar
 */
fun Project.classpathJar(vararg files: Iterable<File>, file: File? = null): File {
	val classpathElements = files.flatMap { it }.map { it.toURI().toString().removePrefix("file:") }
	return this.classpathJar(classpathElements, file)
}

/**
 * The list of requested task instances for the current execution.
 *
 * <em>Note: This method has the same constraints as [getTaskDependencyGraph]. Retrieving its value before the task dependency graph has been determined will result in execution-
 * time errors. [TaskExecutionGraph.whenReady][org.gradle.api.execution.TaskExecutionGraph.whenReady] can be used (via `project.gradle.taskGraph.whenReady`) to defer execution
 * until the task graph is ready, if desired.</em>
 *
 * @see [getTaskDependencyGraph]
 */
val Project.requestedTasks: Collection<Task>
	get() = (gradle.taskGraph as TaskExecutionGraphInternal).let { /* Ensure graph is finalized */ it.allTasks; it.requestedTasks }

/**
 * The list of requested task names for the current execution. This includes task names only and excludes task paths. For example, the execution `"./gradlew
 * -Dorg.gradle.parallel=true :project-a:classes :project-b:test --debug"` will produce the list `["classes", "test"]`.
 */
val Project.requestedTaskNames: List<String>
	get() {
		return if (this.rootProject.extra.has("requestedTaskNames")) {
			@Suppress("UNCHECKED_CAST")
			this.rootProject.extra["requestedTaskNames"] as List<String>
		}
		else {
			val requestedTaskNames = this.rootProject.gradle.startParameter.taskNames
					.filter { !it.startsWith('-') }
					.map { it.substringAfterLast(':') }
			this.rootProject.extra.set("requestedTaskNames", requestedTaskNames)
			requestedTaskNames
		}
	}

/**
 * The list of requested task arguments. These are the raw commandline arguments provided for task execution. This will include task option arguments, such as `--debug`. For
 * example, the execution `"./gradlew -Dorg.gradle.parallel=true :project-a:classes :project-b:test --debug"` will produce the list `[":project-a:classes", ":project-b:test",
 * "--debug"]`.
 */
var Project.requestedTaskArguments: List<String>
	get() = this.rootProject.gradle.startParameter.taskNames
	set(taskArguments) = this.rootProject.gradle.startParameter.setTaskNames(taskArguments)

/**
 * The global lock object used to avoid concurrency conflicts in [getTaskDependencyGraph]. This lock is enforced on a global level to ensure that deadlocks cannot occur when
 * retrieving task dependency graphs.
 */
private val TASK_DEPENDENCY_LIST_LOCK = Object()

/**
 * Gets the full dependency graph for the given task. This includes all task dependencies for the current execution. Dependency graphs for the provided task and each of its
 * upstream task dependencies are cached on first retrieval.
 *
 * <em>Note: This must only be executed <b>after</b> the [configuration phase][https://docs.gradle.org/current/userguide/build_lifecycle.html#sec:build_phases] has completed. I.e.,
 * may only be executed during the execution phase, at which point the task dependency graph has been determined.
 * [TaskExecutionGraph.whenReady][org.gradle.api.execution.TaskExecutionGraph.whenReady] can be used (via `project.gradle.taskGraph.whenReady`) to defer execution until the task
 * graph is ready, if desired.</em>
 */
fun Project.getTaskDependencyGraph(task: Task): TreeNode<Task> {
	val dependencyTreePropertyName = "taskDependencyTree_${task.name}"
	// Double-checked locking
	if (!this.extra.has(dependencyTreePropertyName)) {
		synchronized(TASK_DEPENDENCY_LIST_LOCK) {
			if (!this.extra.has(dependencyTreePropertyName)) {
				this.extra[dependencyTreePropertyName] = project.time("${project.path}: getTaskDependencyList(${task.path})") {
					val children = task.project.gradle.taskGraph.getDependencies(task)
							.map { it.project.getTaskDependencyGraph(it) }
					TreeNode(task, children)
				}
			}
		}
	}
	@Suppress("UNCHECKED_CAST")
	return this.extra[dependencyTreePropertyName] as TreeNode<Task>

	// TODO: Recursive ConcurrentHashMap#computeIfAbsent calls on the same instance cause infinite loops in Java 8. Replace the above method body with the below CHM usage after
	//  upgrading to Java 9. See https://bugs.openjdk.java.net/browse/JDK-8062841.
	// val projectTaskDependencyGraphMap by this.extra.withDefaultSupplier { ConcurrentHashMap<String, TreeNode<Task>>() }
	// // Deadlocks must be avoided here; as long as task dependencies are not recursive (which would raise an exception elsewhere), then deadlocks should not occur
	// return projectTaskDependencyGraphMap.computeIfAbsent(task.name) { taskName ->
	// 	project.time("${project.path}: getTaskDependencyList($taskName)") {
	// 		val children = task.project.gradle.taskGraph.getDependencies(task)
	// 				.map { task -> task.project.getTaskDependencyGraph(task) }
	// 		TreeNode(task, children)
	// 	}
	// }
}

/**
 * Gets the project hierarchy up to and including the current project.
 */
fun Project.getProjectHierarchy(): List<Project> {
	val projectList = mutableListOf<Project>()
	var currentProject: Project? = this
	while (currentProject != null) {
		projectList.add(currentProject)
		currentProject = currentProject.parent
	}
	return projectList.reversed()
}
