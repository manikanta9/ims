package com.clifton.gradle.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project


/**
 * The raw Gradle [Plugin]. This is intentionally a no-op plugin. Projects which do not need any plugin functionality can apply this plugin to gain access to the plugin class path.
 * This can be used to access plugin objects and utilities.
 *
 * @author MikeH
 */
@Suppress("unused") // Class is registered as a plugin
class CliftonRawPlugin : Plugin<Project> {

	override fun apply(target: Project) {
		// Do nothing
	}
}
