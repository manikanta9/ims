@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.build

import com.clifton.gradle.plugin.buildSrc
import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.provider.MapProperty
import org.gradle.api.tasks.PathSensitivity
import org.gradle.api.tasks.TaskProvider
import org.gradle.kotlin.dsl.*
import org.jfrog.artifactory.client.Artifactory
import org.jfrog.artifactory.client.ArtifactoryClientBuilder
import org.openapitools.generator.gradle.plugin.tasks.GenerateTask
import java.io.File
import java.util.*


/**
 * The Gradle [Plugin] for applying OpenAPI build logic. See https://openapi-generator.tech/docs/generators/ for a list of available
 * generators and their associated parameters.
 *
 * @author lnaylor
 */
class CliftonOpenApiPlugin : Plugin<Project> {

	companion object {
		const val JAVA_SERVER = "javaServer"
		const val JAVA_CLIENT = "javaClient"
		const val DOCUMENTATION = "documentation"
	}

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	fun applyFeatures(rootProject: Project) {
		rootProject.subprojects {
			if (project.projectConfig.isOpenApiProject) {
				applyOpenApiFeatures(this)
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private fun applyOpenApiFeatures(project: Project) {
		project.extensions.add("cliftonOpenApi", CliftonOpenApiExtension(project))
		project.afterEvaluate {
			project.the<CliftonOpenApiExtension>().generatorSet.get().filterNotNull().forEach {
				when (it) {
					CliftonOpenApiExtension.OpenApiGeneratorTypes.JAVA_CLIENT -> configureJavaClient(project)
					CliftonOpenApiExtension.OpenApiGeneratorTypes.JAVA_SERVER -> configureJavaServer(project)
					CliftonOpenApiExtension.OpenApiGeneratorTypes.CSHARP_NET -> configureCSharpNet(project)
					CliftonOpenApiExtension.OpenApiGeneratorTypes.DOCUMENTATION -> configureDocumentation(project)
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun configureJavaClient(project: Project) {
		val javaClientVariant = project.registerVariant(JAVA_CLIENT, upstreamForTests = false)
		val openApiExtension = project.the<CliftonOpenApiExtension>()
		val outputDirectoryName = "java-client"

		val generateJavaClient = registerOpenApiGeneratorTask(project,
				"generateOpenApiJavaClient",
				"java") {
			apiPackage.set(openApiExtension.javaPackageName.map { "${it}.api.client" })
			invokerPackage.set(openApiExtension.javaPackageName.map { "${it}.api.client.invoker" })
			modelPackage.set(openApiExtension.javaPackageName.map { "${it}.api.client.model" })
			configOptions.set(mapOf(
					"java8" to "true",
					"dateLibrary" to "java8",
					"serializationLibrary" to "jackson",
					"library" to "resttemplate",
					"useBeanValidation" to "false",
					"enableBuilderSupport" to "true"
			))
			globalProperties.set(mapOf("modelDocs" to "true"))
			outputDir.set("${project.buildDir}/${outputDirectoryName}")
			id.set("${project.name}-${outputDirectoryName}")
		}

		javaClientVariant.sourceSet.java.srcDir("${project.buildDir}/${outputDirectoryName}/src/main/java")
		project.dependencies {
			javaClientVariant.sourceSet.implementationConfigurationName("io.swagger:swagger-annotations:1.5.22")
			javaClientVariant.sourceSet.compileOnlyConfigurationName("com.google.code.findbugs:jsr305:3.0.2")
			javaClientVariant.sourceSet.implementationConfigurationName("org.springframework:spring-web")
			javaClientVariant.sourceSet.implementationConfigurationName("org.springframework:spring-context")
			javaClientVariant.sourceSet.implementationConfigurationName("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
		}
		project.tasks.named(javaClientVariant.sourceSet.compileJavaTaskName) { dependsOn(generateJavaClient) }

		val buildJavaClient = project.tasks.register("buildOpenApiJavaClient") {
			dependsOn(generateJavaClient)
			val generateDir = generateJavaClient.map { it.outputs.files.asPath }
			inputs.files(generateDir.map { project.fileTree(it).exclude("build/**", "**.jar", ".gradle/**") })
					.withPathSensitivity(PathSensitivity.RELATIVE)
					.ignoreEmptyDirectories()
					.skipWhenEmpty()
			outputs.file(generateDir.zip(openApiExtension.apiVersion) { left, right -> "${left}/build/libs/${project.name}-${outputDirectoryName}-${right}.jar" })

			doLast {
				project.exec {
					workingDir(project.rootDir)
					if (System.getProperty("os.name").toLowerCase(Locale.ROOT).contains("windows")) {
						commandLine("cmd", "/c", "gradlew.bat", "build", "-p", generateDir.get())
					}
					else {
						commandLine("sh", "gradlew", "build", "-p", generateDir.get())
					}
				}
			}
		}

		project.tasks.register("publishOpenApiJavaClient") {
			dependsOn(buildJavaClient)
			doLast {
				val artifactory = openApiExtension.artifactory.get()
				val inputDir = buildJavaClient.map { it.outputs.files.asPath }.get()
				val artifact = File(inputDir)

				val artifactName = "${project.name}-${outputDirectoryName}"
				val targetDir = "${project.buildConfig.rootProjectGroup.split(".").joinToString("/")}/${artifactName}/${openApiExtension.apiVersion.get()}"
				val jarFileName = "${artifactName}-${openApiExtension.apiVersion.get()}.jar"
				artifactory.repository(project.buildConfig.artifactoryJavaRepo)
						.upload("${targetDir}/${jarFileName}", artifact)
						.doUpload()
				val pomFile = generateJavaClient.map { it -> it.outputs.files.asFileTree.single { it.name.contains("pom.xml") } }.get()
				val upload = artifactory.repository(project.buildConfig.artifactoryJavaRepo)
						.upload("${targetDir}/${artifactName}-${openApiExtension.apiVersion.get()}.pom", pomFile)
						.doUpload()
				project.logger.quiet("Artifact uploaded to: " + upload.downloadUri.substringBeforeLast("/"))
			}
		}
	}

	private fun configureJavaServer(project: Project) {
		val javaServerVariant = project.registerVariant(JAVA_SERVER, upstreamForTests = false)
		val openApiExtension = project.the<CliftonOpenApiExtension>()
		val outputDirectoryName = "java-server"

		val generateServerCode = registerOpenApiGeneratorTask(project, "generateOpenApiJavaServer", "spring") {
			val templatesFile = project.buildSrc.resources("templates/java/**").files.elementAtOrNull(0)
			templateDir.set(templatesFile?.path?.substring(0, templatesFile.path.lastIndexOf(File.separatorChar)))
			apiPackage.set(openApiExtension.javaPackageName.map { "${it}.api.server" })
			modelPackage.set(openApiExtension.javaPackageName.map { "${it}.api.server.model" })
			modelNameSuffix.set("Model") //naming conflicts?
			configOptions.set(mapOf(
					"java8" to "true",
					"dateLibrary" to "java8",
					"serializationLibrary" to "jackson",
					"library" to "spring-boot",
					"useBeanValidation" to "true",
					"interfaceOnly" to "true",
					"serializableModel" to "true",
					"useTags" to "true",
					"enableBuilderSupport" to "true",
					"skipDefaultInterface" to "true",
					"openApiNullable" to "false"
			))
			outputDir.set("${project.buildDir}/${outputDirectoryName}")
			id.set("${project.name}-${outputDirectoryName}")
		}

		javaServerVariant.sourceSet.java.srcDir("${project.buildDir}/${outputDirectoryName}/src/main/java")
		project.dependencies {
			javaServerVariant.sourceSet.implementationConfigurationName("org.springframework:spring-web")
			javaServerVariant.sourceSet.implementationConfigurationName("org.springframework:spring-context")
			javaServerVariant.sourceSet.implementationConfigurationName("javax.validation:validation-api")
			javaServerVariant.sourceSet.implementationConfigurationName("javax.servlet:javax.servlet-api")
			javaServerVariant.sourceSet.implementationConfigurationName("io.swagger:swagger-annotations:1.5.22")
			javaServerVariant.sourceSet.implementationConfigurationName("com.fasterxml.jackson.core:jackson-databind")
		}
		project.tasks.named(javaServerVariant.sourceSet.compileJavaTaskName) { dependsOn(generateServerCode) }
	}

	private fun configureCSharpNet(project: Project) {
		val openApiExtension = project.the<CliftonOpenApiExtension>()
		val packageName = openApiExtension.dotnetPackageName
		val outputDirectoryName = "csharp-client"

		val generateCSharpNetClient = registerOpenApiGeneratorTask(project,
				"generateOpenApiCSharpNetClient",
				"csharp-netcore") {
			val templatesFile = project.buildSrc.resources("templates/dotnet/**").files.elementAtOrNull(0)
			templateDir.set(templatesFile?.path?.substring(0, templatesFile.path.lastIndexOf(File.separatorChar)))
			invokerPackage.set(null)
			modelPackage.set("Model")
			val configOptionsMap: MapProperty<String, String> = project.objects.mapProperty(String::class, String::class)
			configOptionsMap.put("packageName", packageName)
			configOptionsMap.put("packageVersion", openApiExtension.apiVersion)
			configOptionsMap.put("useComparableNetObjects", "true")
			configOptionsMap.put("targetFramework", "netstandard2.0")
			configOptionsMap.put("modelPropertyNaming", "camelCase")
			configOptionsMap.put("validatable", "false")
			configOptions.set(configOptionsMap)
			globalProperties.set(mapOf(
					"apiTests" to "false",
					"modelTests" to "false"))

			outputDir.set("${project.buildDir}/${outputDirectoryName}")
			id.set("${project.name}-${outputDirectoryName}")
		}

		//requires dotnet on the PATH
		val buildCSharpNetClient = project.tasks.register("buildOpenApiCSharpNetClient") {
			dependsOn(generateCSharpNetClient)
			val generateDir = generateCSharpNetClient.map { it.outputs.files.asPath }
			inputs.files(generateDir.map { project.fileTree(it).exclude("**/bin/**", "**/obj/**") })
					.withPathSensitivity(PathSensitivity.RELATIVE)
					.ignoreEmptyDirectories()
					.skipWhenEmpty()
			outputs.file(generateDir.zip(packageName) { left, right -> "${left}/src/${right}/bin/Debug/${right}" }.zip(openApiExtension.apiVersion) { left, right -> "${left}.${right}.nupkg" })

			doLast {
				project.exec {
					workingDir(generateDir.get())
					commandLine("dotnet", "pack") //this will fail if dotnet is not available
				}
			}
		}

		project.tasks.register("publishOpenApiCSharpNetClient") {
			dependsOn(buildCSharpNetClient)
			doLast {
				val artifactory = openApiExtension.artifactory.get()
				val inputDir = buildCSharpNetClient.map { it.outputs.files.asPath }.get()
				val artifact = File(inputDir)
				val upload = artifactory.repository(project.buildConfig.artifactoryNetcoreRepo)
						.upload("${openApiExtension.dotnetPackageName.get()}/${packageName.get()}.${openApiExtension.apiVersion.get()}.nupkg", artifact)
						.doUpload()
				project.logger.quiet("Artifact uploaded to: " + upload.downloadUri)
			}
		}
	}

	private fun configureDocumentation(project: Project) {
		val documentationVariant = project.registerVariant(DOCUMENTATION, upstreamForTests = false)
		val outputDirectoryName = "documentation"
		documentationVariant.sourceSet.resources.srcDir("${project.buildDir}/${outputDirectoryName}/jar/resources")

		val generateDocumentation = registerOpenApiGeneratorTask(project, "generateOpenApiDocumentation", "openapi-yaml") {
			outputDir.set("${project.buildDir}/${outputDirectoryName}")
			doLast {
				project.copy {
					from("${project.buildDir}/${outputDirectoryName}/openapi")
					into("${project.buildDir}/${outputDirectoryName}/jar/resources/META-INF/resources")
				}
			}
		}

		project.dependencies {
			documentationVariant.sourceSet.apiConfigurationName("org.webjars:swagger-ui:3.48.0")
		}
		project.tasks.named(documentationVariant.sourceSet.compileJavaTaskName) { dependsOn(generateDocumentation) }
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun registerOpenApiGeneratorTask(project: Project,
	                                         name: String,
	                                         generatorNameParam: String,
	                                         configure: Action<GenerateTask>): TaskProvider<GenerateTask> {
		val openApiExtension = project.the<CliftonOpenApiExtension>()
		return project.tasks.register(name, GenerateTask::class) {
			generatorName.set(generatorNameParam)
			inputSpec.set(openApiExtension.projectYml)
			groupId.set(project.group.toString())
			version.set(openApiExtension.apiVersion)
			apiPackage.set("api")
			invokerPackage.set("invoker")
			modelPackage.set("model")
			enablePostProcessFile.set(true)
			skipOverwrite.set(false)
			outputs.cacheIf { true }
			configure(this)
		}
	}
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized Open API configuration. This extension provides properties for use with Open API project tasks.
 */
open class CliftonOpenApiExtension(val project: Project) {

	enum class OpenApiGeneratorTypes { JAVA_CLIENT, JAVA_SERVER, CSHARP_NET, DOCUMENTATION }

	/**
	 * The package name used to generate and publish Open API Java clients.
	 */
	val javaPackageName = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.provider {
				val projectNameSuffix = project.name
						.substringAfterLast("${project.buildConfig.companyName}-")
						.substringBeforeLast("-${project.buildConfig.apiProjectSuffix}")
				val projectPackage = "${project.buildConfig.rootProjectGroup}.${projectNameSuffix.replace('-', '.')}"
				projectPackage
			})

	/**
	 * The package name used to generate and publish Open API dotnet clients.
	 */
	val dotnetPackageName = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.provider {
				val projectNameSuffix = project.name
						.substringAfterLast("${project.buildConfig.companyName}-")
						.substringBeforeLast("-${project.buildConfig.apiProjectSuffix}")
				val packageName = "PPA.${projectNameSuffix.split("-").joinToString("") { it.capitalize() }}.API"
				packageName
			})

	/**
	 * The location of the project yml file used for OpenAPI configuration.
	 */
	val projectYml = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention("${project.layout.projectDirectory.dir("api")}/${project.name}.yml")

	/**
	 * The version number to be associated with the generated artifacts.
	 */
	val apiVersion = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.apiVersion)

	/**
	 * The generator types that will be used by the project.
	 */
	val generatorSet = project.objects.setProperty<OpenApiGeneratorTypes>()
			.apply { finalizeValueOnRead() }
			.convention(setOf(OpenApiGeneratorTypes.JAVA_CLIENT, OpenApiGeneratorTypes.JAVA_SERVER, OpenApiGeneratorTypes.CSHARP_NET, OpenApiGeneratorTypes.DOCUMENTATION))

	/**
	 * The artifactory where artifacts should be published.
	 */
	val artifactory = project.objects.property(Artifactory::class)
			.apply { finalizeValueOnRead() }
			.convention(project.provider {
				ArtifactoryClientBuilder.create()
						.setUrl(project.buildConfig.artifactoryUrl)
						.addInterceptorLast { request, _ -> request.addHeader("X-JFrog-Art-Api", project.buildConfig.apiKey) }
						.build()
			})
}
