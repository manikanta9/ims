package com.clifton.gradle.plugin.artifact

import com.clifton.gradle.plugin.build.CliftonJavaPlugin
import com.clifton.gradle.plugin.buildSrc
import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.plugin.verification.CliftonTestPlugin
import com.clifton.gradle.util.*
import com.clifton.gradle.util.dependency.LibraryElementsVariantCompatibilityRule
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.attributes.LibraryElements
import org.gradle.api.file.FileCollection
import org.gradle.api.file.RelativePath
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.tasks.*
import org.gradle.internal.component.external.model.TestFixturesSupport
import org.gradle.kotlin.dsl.*
import java.io.File
import kotlin.properties.Delegates


/**
 * The Gradle [Plugin] for registering and configuring tasks for working with database schemas.
 */
class CliftonSchemaPlugin : Plugin<Project> {

	companion object {

		const val LIBRARY_ELEMENTS_SCHEMA = "schema"
		const val SCHEMA_VARIANT_NAME = "schema"

		const val DB_MIGRATE_TASK_NAME = "dbMigrate"
		const val DB_MIGRATE_PRINT_TASK_NAME = "dbMigratePrint"
		const val DB_MIGRATE_RESTORE_TASK_NAME = "dbMigrateRestore"
		const val DB_MIGRATE_RESTORE_PRINT_TASK_NAME = "dbMigrateRestorePrint"
		const val DB_REBUILD_MIGRATE_TASK_NAME = "dbRebuildMigrate"
		const val DB_REBUILD_MIGRATE_PRINT_TASK_NAME = "dbRebuildMigratePrint"
		const val DB_BACKUP_TASK_NAME = "dbBackup"
		const val DB_BACKUP_MIGRATE_TASK_NAME = "dbBackupMigrate"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.pluginManager.apply(CliftonTestPlugin::class)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyPerProjectFeatures(project: Project) {
		applySchemaLogConfigurations(project)
		applySchemaConfiguration(project)
		// Apply database migration scripts to appropriate projects
		if (project.projectConfig.isAppProject || project.projectConfig.isServiceProject) {
			applyMigrationTasks(project)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applySchemaLogConfigurations(project: Project) {
		val loggingConfigurationDirectory = project.buildDir.resolve("tmp/migration")
		project.tasks.register("copyMigrationLoggingConfig", Copy::class) {
			doFirst { project.delete(loggingConfigurationDirectory) }
			from(project.buildSrc.resources("migration/log4j2*.xml")) {
				// Remove "migration" directory prefix (see https://docs.gradle.org/current/userguide/working_with_files.html#sec:unpacking_archives_example)
				eachFile { this.relativePath = RelativePath(true, *relativePath.segments.drop(1).toTypedArray()) }
				includeEmptyDirs = false
				if (project.buildConfig.isResourceFilteringEnabled) {
					project.projectConfig.applyPropertyReplacements(this, this@register)
				}
			}
			into(loggingConfigurationDirectory)
		}
		// Make sure copyMigrationLoggingConfig executes during a build and/or during test execution. Each are common with generateSchemaMapping.
		project.tasks.named("jar") { dependsOn("copyMigrationLoggingConfig") }
		project.tasks.named("processTestResources") { inputs.files(project.tasks.named("copyMigrationLoggingConfig")).withPathSensitivity(PathSensitivity.RELATIVE) }

		project.configurations {
			// Logging configuration file for DB migrations for app/service projects and Bamboo deployments, see log4j2-migration.xml
			register("migrationLogConfiguration")
			// Logging configuration file for schema generation, see log4j2-schemaGeneration.xml
			register("schemaGenerationLogConfiguration")
		}

		project.dependencies {
			"migrationLogConfiguration"(project.fileTree(loggingConfigurationDirectory) { include("log4j2-migration.xml") })
			"schemaGenerationLogConfiguration"(project.fileTree(loggingConfigurationDirectory) { include("log4j2-schemaGeneration.xml") })
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applySchemaConfiguration(project: Project) {
		val sourceSets = project.the<SourceSetContainer>()
		sourceSets.configureEach {
			// Do not provide schema generation capabilities for test source sets (build performance optimization)
			if (this.name == SourceSet.TEST_SOURCE_SET_NAME) {
				return@configureEach
			}
			val task = registerGenerateSchemaMappingsTask(project, this)

			/*
			 * Add task to source set outputs. Semantically, this indicates that the source set produces the schema resources. Functionally, this causes the project JAR artifact to
			 * require this task and to include its outputs.
			 */
			val schemaOutputs = task.map { it.outputDir }
			output.dir(mapOf("builtBy" to task), schemaOutputs)

			// Create the configuration and corresponding variant for schema resources
			registerSchemaClasspathConfiguration(project, this, schemaOutputs)
			registerSchemaVariants(project, this, schemaOutputs)
		}

		// Configure compatibility rules for variant selection
		project.dependencies {
			attributesSchema {
				attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE) {
					compatibilityRules.add(LibraryElementsVariantCompatibilityRule::class) {
						params(LIBRARY_ELEMENTS_SCHEMA, LibraryElements.JAR)
					}
				}
			}
		}
	}


	private fun registerGenerateSchemaMappingsTask(project: Project, sourceSet: SourceSet): TaskProvider<GenerateSchemaMappings> {
		val sourceSetPrefixName = sourceSet.name.takeIf { it != SourceSet.MAIN_SOURCE_SET_NAME } ?: ""
		val taskName = "generate${sourceSetPrefixName.capitalize()}SchemaMappings"
		return project.tasks.register(taskName, GenerateSchemaMappings::class) {
			description = "Generates schema and Hibernate mapping files for the ${sourceSet.name} source set."
			sourceSetName = sourceSet.name
			outputDir = project.file("${project.buildDir}/schema/${sourceSet.name}")
			outputs.dirs(project.file(outputDir))

			// Depend on preparation of compile class path
			dependsOn("copyMigrationLoggingConfig", sourceSet.compileClasspath)

			// Set up class path
			/*
			 * Note: The dependencies here must be carefully selected to be complete while also avoiding recursive dependencies. Gradle will let us know early on if infinite
			 * recursion occurs, but it will not inform us if the inputs are incomplete. "sourceSet.output" and "sourceSet.runtimeClasspath" cannot be used as inputs, as these
			 * would include the outputs from this task.
			 */
			classpath += project.files(
					project.tasks.named(sourceSet.compileJavaTaskName),
					project.tasks.named(sourceSet.processResourcesTaskName),
					project.files(project.configurations.named(sourceSet.schemaClasspathConfigurationName))
			)

			// Include upstream dependencies as monitored or unmonitored task inputs
			inputs.property("Include upstream changes", project.buildConfig.schemaIncludeUpstreamChanges)
			if (project.buildConfig.schemaIncludeUpstreamChanges) {
				classpath += sourceSet.compileClasspath // Monitored for dirty checking
			}
			else {
				transientClasspath += sourceSet.compileClasspath // Not monitored for dirty checking
			}

			// Handle special source set cases
			var migrationFileSourceSet = sourceSet
			when (sourceSet.name) {
				SourceSet.MAIN_SOURCE_SET_NAME -> {
					// No customization necessary
				}
				TestFixturesSupport.TEST_FIXTURE_SOURCESET_NAME -> {
					// Produce in-memory Hibernate mappings only
					mappingTypes = listOf("HIBERNATE_IN_MEMORY")
					/*
					 * For in-memory mapping generation, do not load the mapping files generated for the same project's main source set. We want to create alternate mappings
					 * instead. Additionally, we want to include the main source set migration resources on the class path. This applies only to library projects, as non-library
					 * projects will include the project JAR itself on the compile class path, thus already including the migration files.
					 */
					val mainSourceSet = project.the<SourceSetContainer>()[SourceSet.MAIN_SOURCE_SET_NAME]
					excludeIntraProjectSchemas = listOf(mainSourceSet.name)
					if (project.projectConfig.isLibraryProject) {
						classpath += project.files(project.tasks.named(mainSourceSet.processResourcesTaskName))
					}
					migrationFileSourceSet = mainSourceSet
				}
				else -> {
					// Attach variant to project name
					effectiveProjectName = "${project.name}-${sourceSet.name}"
				}
			}
			migrationFiles = getSchemaFileCollection(project, effectiveProjectName, migrationFileSourceSet)

			// Clean stale outputs
			doFirst { cleanStaleOutputs() }
		}
	}


	/**
	 * Registers the schema class path configuration. This configuration is only for resolving resources required for schema mapping generation.
	 */
	private fun registerSchemaClasspathConfiguration(project: Project, sourceSet: SourceSet, artifactNotation: Any) {
		val configurationName = sourceSet.schemaClasspathConfigurationName
		project.configurations.register(configurationName) {
			description = "Schema class path dependencies for the source set \"${sourceSet.name}\"."
			extendsFrom(project.configurations[sourceSet.implementationConfigurationName])
			isCanBeResolved = true
			isCanBeConsumed = false
			isTransitive = true
			isVisible = false
			attributes {
				attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE, project.objects.named(LibraryElements::class, LIBRARY_ELEMENTS_SCHEMA))
			}
		}
		project.artifacts.add(configurationName, artifactNotation)
	}


	/**
	 * Registers the outgoing variants for schema artifacts. This allows downstream projects to explicitly reference the schema resource outputs themselves, rather than requiring
	 * the project artifact. This also allows downstream projects to identify the explicit schema artifacts by name, rather than attempting to automatically discover the schema
	 * resource outputs from dependent projects. This advantage is especially helpful since schema resource output names may vary for each source set variant.
	 */
	private fun registerSchemaVariants(project: Project, sourceSet: SourceSet, artifactNotation: Any) {
		// Use matching because some source sets will not produce one or more of the eligible configurations (e.g., "test" does not produce the "testApiElements" configuration)
		project.configurations.matching { it.name == sourceSet.apiElementsConfigurationName || it.name == sourceSet.runtimeElementsConfigurationName }.configureEach {
			outgoing {
				variants {
					// Variants must be eagerly created so that they exist during configuration; otherwise, Gradle will fail with a late configuration exception
					create(SCHEMA_VARIANT_NAME) {
						artifact(artifactNotation)
						attributes {
							attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE, project.objects.named(LibraryElements::class, LIBRARY_ELEMENTS_SCHEMA))
						}
					}
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyMigrationTasks(project: Project) {
		project.tasks.register(DB_MIGRATE_TASK_NAME, MigrationExec::class) {
			description = "Run database migrations."
			action = "MIGRATE"
		}

		project.tasks.register(DB_MIGRATE_PRINT_TASK_NAME, MigrationExec::class) {
			description = "Print current database migrations without actions."
			action = "PRINT"
		}

		project.tasks.register(DB_MIGRATE_RESTORE_TASK_NAME, MigrationExec::class) {
			description = "Restore database from backup and run database migrations."
			action = "RESTORE_MIGRATE"
		}

		project.tasks.register(DB_MIGRATE_RESTORE_PRINT_TASK_NAME, MigrationExec::class) {
			description = "Restore database from backup and print database migrations."
			action = "RESTORE_PRINT"
		}

		project.tasks.register(DB_REBUILD_MIGRATE_TASK_NAME, MigrationExec::class) {
			description = "Rebuild Database from Scratch and Run Migrations."
			action = "REBUILD_MIGRATE"
		}

		project.tasks.register(DB_REBUILD_MIGRATE_PRINT_TASK_NAME, MigrationExec::class) {
			description = "Rebuild Database from Scratch and Print Migrations."
			action = "REBUILD_PRINT"
		}

		project.tasks.register(DB_BACKUP_TASK_NAME, MigrationExec::class) {
			description = "Create a backup of database."
			action = "BACKUP"
		}

		project.tasks.register(DB_BACKUP_MIGRATE_TASK_NAME, MigrationExec::class) {
			description = "Create a backup of database and runs migrations."
			action = "BACKUP_MIGRATE"
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun getSchemaFileCollection(project: Project, effectiveProjectName: String, sourceSet: SourceSet): FileCollection {
		return project.files(sourceSet.resources.srcDirs.map { srcDir ->
			project.fileTree("$srcDir/META-INF/schema")
					.apply { include("**/$effectiveProjectName-*/**") }
					.filter { it.isFile }
		})
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Task] type for generating schema mapping files. The resulting files are used by the application for schema metadata and by Hibernate as ORM mapping files.
 */
@CacheableTask
open class GenerateSchemaMappings : JavaExec() {

	// Configuration parameters
	@get:Input
	var sourceSetName by Delegates.notNull<String>()

	/**
	 * The effective project name to use during migrations. This is the base name which will be used when searching for migration resources and when producing output artifacts.
	 * This can be modified to avoid naming conflicts, such as when producing mapping files for different variants within the same project.
	 */
	@get:Input
	var effectiveProjectName = project.name

	/**
	 * The list of source sets within the same project which should *not* be treated as dependent schemas. For example, when generating in-memory database Hibernate
	 * mappings for tests, we may wish to use produce mapping files tangential to the primary ("main") source set mappings. In this case, we do not want to include the generated
	 * mappings for the primary source set as a dependent schema because its inclusion would typically conflict with the new schema. Execution can be expected to fail in such
	 * cases.
	 */
	@get:Input
	var excludeIntraProjectSchemas = emptyList<String>()

	/**
	 * The class path which should not be monitored for dirty-checking. Changes to contents within this class path will not be incorporated when computing cache keys and will not
	 * trigger re-executions of otherwise up-to-date tasks. This field is intended for improving execution performance in environments where build consistency is not critical.
	 * Ideal contents for this file collection include resources from upstream projects which typically do not impact task results for the current project.
	 */
	@get:Internal
	var transientClasspath: FileCollection = project.files()

	// Execution parameters
	@get:Input
	var mainClassName = "com.clifton.core.dataaccess.migrate.MigrationConverterRunner"

	@get:Input
	var mappingTypes = listOf("HIBERNATE", "SCHEMA_METADATA")

	@get:OutputDirectory
	var outputDir by Delegates.notNull<File>()

	// Other parameters
	@get:InputFiles
	@get:SkipWhenEmpty
	@get:PathSensitive(PathSensitivity.RELATIVE)
	var migrationFiles: FileCollection = project.files()


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	init {
		mainClass.convention(project.provider { mainClassName })
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun exec() {
		classpath = project.files(project.classpathJar(classpath, transientClasspath))
		jvmArgs("-Dlog4j.configurationFile=${project.configurations["schemaGenerationLogConfiguration"].singleFile.path}")
		args(effectiveProjectName, outputDir.path, mappingTypes.joinToString(","), *getSchemaDependencyList().toTypedArray())
		super.exec()
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the list of dependent schemas for the current task. The resulting list is generated in reversed depth-first order, meaning that each dependency should be
	 * satisfied by all previous dependencies, and thus the dependent schemas may be loaded in the order returned.
	 */
	private fun getSchemaDependencyList(): List<String> {
		return project.time("${this.path}: getProjectDependencyList()") {
			project.getTaskDependencyGraph(this)
					.iterateDepthFirst(reversed = true, excludeSelf = true, distinct = true)
					.filterIsInstance<GenerateSchemaMappings>()
					// Exclude dependent mappings generated for specified intra-project dependencies (typically variant-specific dependencies)
					.filter { !(project == it.project && it.sourceSetName in excludeIntraProjectSchemas) }
					.flatMap { it.outputs.files.files.asSequence() }
					.flatMap { it.walk() }
					.filter { it.isFile && it.name.startsWith("schema-clifton-") }
					.map { it.nameWithoutExtension.substringAfter("schema-clifton-") }
					.toList()
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Task] type for executing database migration actions. Tasks of this type execute the migration runner, performing the specified type of migration [action].
 */
open class MigrationExec : JavaExec() {

	@Input
	lateinit var action: String

	// Prevent execution when no migration context exists
	@get:InputFiles
	@get:SkipWhenEmpty
	@get:PathSensitive(PathSensitivity.RELATIVE)
	var migrationContextFiles: FileCollection = project.files(project.the<SourceSetContainer>()[SourceSet.MAIN_SOURCE_SET_NAME].resources.srcDirs
			.map { srcDir -> project.file("$srcDir/META-INF/schema/migration-context.xml") }
			.filter { it.isFile })

	init {
		this.main = "com.clifton.core.dataaccess.migrate.MigrationRunner"
		this.classpath = this.project.the<SourceSetContainer>()["main"].runtimeClasspath
		this.dependsOn(JavaBasePlugin.BUILD_TASK_NAME)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun exec() {
		// provide support for spring instrumentation and log overriding
		this.jvmArgs(
				"-javaagent:${this.project.configurations[CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION].singleFile}",
				"-Dlog4j.configurationFile=${this.project.configurations["migrationLogConfiguration"].singleFile.path}"
		)
		this.args = listOf(action, "classpath:/META-INF/schema/migration-context.xml")
		this.classpath = this.project.files(this.project.classpathJar(this.classpath))
		super.exec()
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


val SourceSet.schemaClasspathConfigurationName
	get() = if (this.name == SourceSet.MAIN_SOURCE_SET_NAME) "schemaClasspath" else "${this.name}SchemaClasspath"
