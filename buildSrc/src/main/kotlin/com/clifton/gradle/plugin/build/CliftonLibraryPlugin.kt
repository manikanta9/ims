package com.clifton.gradle.plugin.build

import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.plugin.verification.CliftonTestPlugin
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.ProjectDependency
import org.gradle.api.plugins.FeatureSpec
import org.gradle.api.plugins.JavaLibraryPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.internal.component.external.model.TestFixturesSupport
import org.gradle.kotlin.dsl.*


/**
 * The Gradle [Plugin] for applying features to "library" projects. Projects are considered "library" projects based on the
 * [com.clifton.gradle.plugin.util.CliftonProjectConfiguration.isLibraryProject] property.
 *
 * Library projects have special configurations for dependency management.
 */
class CliftonLibraryPlugin : Plugin<Project> {

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.pluginManager.apply(CliftonTestPlugin::class) // The test plugin is required in order to modify test fixtures dependencies
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyPerProjectFeatures(project: Project) {
		// Guard-clause: Only apply features to appropriate projects
		if (!project.projectConfig.isLibraryProject) {
			return
		}

		project.pluginManager.apply(JavaLibraryPlugin::class)

		// TODO: Ideally, all project dependencies could be explicitly stated (use "implementation" instead of "api") to reduce coupling. This is a little difficult right now with
		//  how schema mappings are generated. Once we are able to make schema mapping generation work with the runtime classpath (i.e., including transitive dependencies through
		//  the "implementation" configuration), we should re-enable these warnings and then change all project dependencies to be "implementation" dependencies.
		// /*
		//  * Warn on transitive project dependencies used for library projects. Project dependencies should be declared explicitly to reduce coupling. Most of these cases will likely
		//  * be caused by a use of the "api" dependency configuration rather than the "implementation" configuration, and the fix is simply to replace "api" with "implementation".
		//  */
		// project.afterEvaluate {
		// 	project.configurations.named("api") {
		// 		val configuration = this
		// 		withDependencies {
		// 			val projectDependencies = this.filterIsInstance<ProjectDependency>()
		// 			if (projectDependencies.isNotEmpty()) {
		// 				project.logger.warn("Project dependencies should not be used for the library project [${project.name}] and transitive configuration [${configuration.name}]: ${projectDependencies.joinToString("\n\t- ", "\n\t- ") { it.dependencyProject.name }}")
		// 			}
		// 		}
		// 	}
		// }
	}
}


////////////////////////////////////////////////////////////////////////////
////////            Extension Functions                             ////////
////////////////////////////////////////////////////////////////////////////


/**
 * The type representing a newly registered feature variant. This provides convenience properties for declaring dependencies for the registered feature variant.
 *
 * @see registerVariant
 */
@Suppress("unused") // Intended to be available for consumers
class FeatureVariant(
	val name: String,
	val sourceSet: SourceSet,
	private val project: Project
) {

	/**
	 * Returns the feature variant API configuration name. This can be used within `dependencies { }` blocks to declare API dependencies for this feature variant.
	 */
	val api = sourceSet.apiConfigurationName

	/**
	 * Returns the feature variant implementation configuration name. This can be used within `dependencies { }` blocks to declare implementation dependencies for this feature
	 * variant.
	 */
	val implementation = sourceSet.implementationConfigurationName

	/**
	 * Returns the feature variant runtime-only configuration name. This can be used within `dependencies { }` blocks to declare runtime-only dependencies for this feature variant.
	 */
	val runtimeOnly = sourceSet.runtimeOnlyConfigurationName

	/**
	 * Returns the feature variant annotation processor configuration name. This can be used within `dependencies { }` blocks to declare annotation processor dependencies for this
	 * feature variant.
	 */
	val annotationProcessor = sourceSet.annotationProcessorConfigurationName


	/**
	 * Generates a dependency on the feature variant.
	 */
	operator fun invoke() = project.dependencies.project(project.path).usingVariant(name)
}


/**
 * Registers a new [feature variant][https://docs.gradle.org/current/userguide/feature_variants.html] of the given name for the current project. Registered feature variants will
 * also automatically have a new matching source set and corresponding configurations registered.
 *
 * Feature variants can be used to grow the dependency graph without requiring additional projects. By default, there is no explicit relationship between any feature variants,
 * including the main variant. Relationships must be manually added through declared project dependencies if desired. Parameters may be used to automatically configure certain
 * feature variant relationships when registering features.
 *
 * See [usingVariant] for more usage details.
 *
 * @param name the name of the feature (and the subsequent source set) to be created
 * @param primaryCapability indicates that the feature provides the primary capability for the project. Only one such feature may be simultaneously referenced by consumers. This
 * can be used to prevent consumers from using multiple conflicting features. Only one feature from the current producer with this value equal to `true` may be depended upon by any
 * given consumer.
 * @param dependsOnMain indicates that the feature variant depends on the main project variant
 * @param upstreamForMain indicates that the main project variant depends on the feature variant
 * @param upstreamForTests indicates that the project test fixtures variant depends on the feature variant.
 * @param configureFeature the action to execute on the registered [FeatureSpec]
 */
fun Project.registerVariant(
	name: String,
	primaryCapability: Boolean = false,
	dependsOnMain: Boolean = false,
	upstreamForMain: Boolean = false,
	upstreamForTests: Boolean = true,
	configureFeature: Action<FeatureSpec>? = null
): FeatureVariant {
	if (dependsOnMain && upstreamForMain) {
		throw IllegalArgumentException("The \"dependsOnMain\" and \"upstreamForMain\" options cannot be enabled simultaneously.")
	}

	// Create feature variant
	val project = this
	val sourceSets = the<SourceSetContainer>()
	val sourceSet = sourceSets.create(name)
	the<JavaPluginExtension>().registerFeature(name) {
		usingSourceSet(sourceSet)
		// Declare custom capabilities to impose unique capability constraints
		if (primaryCapability) {
			capability(project.group.toString(), "${project.name}-primary", project.version.toString())
			capability(project.group.toString(), "${project.name}-$name", project.version.toString())
		}
		configureFeature?.execute(this)
	}

	// Configure dependencies
	val featureVariant = FeatureVariant(name, sourceSet, project)
	dependencies {
		// Add main as a dependency for the feature variant
		if (dependsOnMain) {
			featureVariant.api(project)
		}
		// Add the feature variant as a dependency for main
		if (upstreamForMain) {
			sourceSets[SourceSet.MAIN_SOURCE_SET_NAME].apiConfigurationName(featureVariant())
		}
		// Allow tests to access the feature variant
		if (upstreamForTests) {
			sourceSets[TestFixturesSupport.TEST_FIXTURE_SOURCESET_NAME].apiConfigurationName(featureVariant())
		}
	}
	return featureVariant
}


/**
 * Configures the current dependency to select the variant which provides the feature of the given name. This is intended to be used in tandem with [Project.registerVariant] to
 * select a single registered feature variant.
 *
 * Unless the registered feature depends on the main variant for the dependency project, then this does <em>not</em> implicitly add a main variant (i.e., default) dependency. If
 * this is desired, then a separate dependency must be added for this.
 *
 * Example:
 * ```
 * // Project A (build.gradle.kts)
 * val apiVariant = registerVariant("api", upstreamForMain = true)
 * val clientVariant = registerVariant("client", dependsOnMain = true)
 * val serverVariant = registerVariant("server", dependsOnMain = true)
 *
 * dependencies {
 *     apiVariant.implementation("group:artifact-1:version")
 *     implementation("group:artifact-2:version")
 *     clientVariant.implementation("group:artifact-3:version")
 *     serverVariant.implementation("group:artifact-4:version")
 * }
 *
 * // Project B (build.gradle.kts)
 * dependencies {
 *     // Pulls in "api" only
 *     implementation(project(":project-a")) { usingVariant("api") }
 * }
 *
 * // Project C (build.gradle.kts)
 * dependencies {
 *     // Pulls in "api" and "main"
 *     implementation(project(":project-a"))
 * }
 *
 * // Project D (build.gradle.kts)
 * dependencies {
 *     // Pulls in "api", "main", and "client"
 *     implementation(project(":project-a")) { usingVariant("client") }
 * }
 *
 * // Project E (build.gradle.kts)
 * dependencies {
 *     // Pulls in "api", "main", and "server"
 *     implementation(project(":project-a")) { usingVariant("server") }
 * }
 * ```
 *
 * @see registerVariant
 */
fun <T : ProjectDependency> T.usingVariant(variantName: String): T {
	val group = this.dependencyProject.group
	val name = this.dependencyProject.name
	val capability = "$group:$name-$variantName"
	capabilities {
		requireCapabilities(capability)
	}
	return this
}
