@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.build

import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.expandArchive
import com.clifton.gradle.util.mapFiles
import com.clifton.gradle.util.requireRootProject
import com.google.cloud.tools.jib.gradle.JibExtension
import com.google.cloud.tools.jib.gradle.JibPlugin
import com.google.cloud.tools.jib.gradle.JibTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.kotlin.dsl.*
import java.text.SimpleDateFormat
import java.util.*


/**
 * The Gradle [Plugin] for applying containerization logic.
 *
 * @author lnaylor
 */
class CliftonContainerizationPlugin : Plugin<Project> {

	companion object {
		const val CACERTS_CONFIGURATION = "cacerts"
	}

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.subprojects {
			if (project.projectConfig.isContainerProject) {
				applyContainerizationFeatures(this)
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private fun applyContainerizationFeatures(project: Project) {
		project.extensions.add("cliftonContainerization", CliftonContainerizationExtension(project))
		project.pluginManager.apply(JibPlugin::class)

		project.configurations {
			register(CACERTS_CONFIGURATION)
		}

		project.dependencies {
			CACERTS_CONFIGURATION("com.clifton:cacerts:1.0.0@CACERTS")
		}

		val jibExtra = project.layout.buildDirectory.dir("jib-extra")

		val jibCopyExtraDirectories = project.tasks.register("jibCopyExtraDirectories", Copy::class) {
			val mainSourceSet = project.the<SourceSetContainer>()[SourceSet.MAIN_SOURCE_SET_NAME]
			val dependencyStaticAssets = project.configurations.named(mainSourceSet.javascriptClasspathConfigurationName).mapFiles { project.expandArchive(it) }

			into(jibExtra)

			from(dependencyStaticAssets) {
				into("app/resources/static")
			}
			from(project.configurations.named(CACERTS_CONFIGURATION)) {
				into("opt/java/openjdk/lib/security")
				rename(".*", "cacerts")
			}
		}

		project.tasks.withType(JibTask::class) {
			dependsOn(jibCopyExtraDirectories)
		}

		project.afterEvaluate {
			val containerizationExtension = project.the<CliftonContainerizationExtension>()
			val jibExtension = project.the<JibExtension>()

			jibExtension.from.auth.username = containerizationExtension.fromAuthUsername.get()
			jibExtension.from.auth.password = containerizationExtension.fromAuthPassword.get()
			jibExtension.to.auth.username = containerizationExtension.toAuthUsername.get()
			jibExtension.to.auth.password = containerizationExtension.toAuthPassword.get()

			jibExtension.container.creationTime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
					.apply { timeZone = TimeZone.getTimeZone("UTC") }
					.format(Date())
			jibExtension.container.jvmFlags = listOf("-javaagent:app/libs/${project.configurations[CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION].singleFile.name}")
			jibExtension.to.image = containerizationExtension.toImage.get()
			jibExtension.to.tags = containerizationExtension.toTags.get().toMutableSet()
			jibExtension.from.image = containerizationExtension.fromImage.get()

			jibExtension.extraDirectories {
				paths {
					path {
						setFrom(jibExtra)
					}
				}
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized containerization configuration. This extension provides properties for use with containerization project tasks.
 */
open class CliftonContainerizationExtension(val project: Project) {

	/**
	 * The username to be used for authentication to the base image registry.
	 */
	val fromAuthUsername = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.container.fromAuthUsername)

	/**
	 * The password to be used for authentication to the base image registry.
	 */
	val fromAuthPassword = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.container.fromAuthPassword)

	/**
	 * The base image for the containerized project.
	 */
	val fromImage = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.container.fromImage)

	/**
	 * The username to be used for authentication to the container registry.
	 */
	val toAuthUsername = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.container.toAuthUsername)

	/**
	 * The password to be used for authentication to the container registry.
	 */
	val toAuthPassword = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.container.toAuthPassword)

	/**
	 * The image name for the project, including the container registry domain.
	 */
	val toImage = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.container.toImage)

	/**
	 * The image tags for the containerized project.
	 */
	val toTags = project.objects.listProperty<String>()
			.apply { finalizeValueOnRead() }
			.convention(project.buildConfig.container.toTags)
}
