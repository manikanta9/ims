package com.clifton.gradle.plugin.artifact

import com.clifton.gradle.plugin.build.CliftonJavaPlugin
import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.Sync
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.bundling.Zip
import org.gradle.api.tasks.bundling.ZipEntryCompression
import org.gradle.kotlin.dsl.*
import java.io.File


/**
 * The Gradle [Plugin] for tasks requiring artifact deployment. This typically only applies to projects which are
 * [application projects][com.clifton.gradle.plugin.util.CliftonProjectConfiguration.isAppProject] or
 * [service projects][com.clifton.gradle.plugin.util.CliftonProjectConfiguration.isServiceProject].
 *
 * The tasks provided by this plugin relate to build artifacts. These tasks facilitate decoupling between the resource filtering and migration process and the build plan artifact
 * generation.
 */
class CliftonArtifactPlugin : Plugin<Project> {

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.pluginManager.apply(CliftonServicePlugin::class)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyPerProjectFeatures(project: Project) {
		// Guard-clause: Only apply features to appropriate projects
		if (!project.projectConfig.isAppProject && !project.projectConfig.isServiceProject) {
			return
		}

		/*
		 * The servlet-api jar is included in the compile classpath and excluded from our war WEB-INF/lib directory.
		 * Since the artifact deployment process migration uses the war"s lib directory for the classpath, we need to
		 * make sure the servlet-api.jar is included. The action migration initializes some of the servlet/filter classes
		 * because they are references in the Spring context files.
		 */
		project.configurations.register("servletApiJar")
		project.dependencies {
			"servletApiJar"("javax.servlet:javax.servlet-api")
		}

		// Prepare artifact migration properties
		val migrationConfig = project.projectConfig.artifactMigration
		project.tasks.register("printArtifactProperties") {
			doLast { migrationConfig.printPropertyConfig() }
		}

		// This task will unzip the artifact if it is a war file or unzip the service zip. This is needed to filter the resources and run migrations
		project.tasks.register("unzipArtifact", Sync::class) {
			dependsOn("printArtifactProperties")
			val zipFile = project.file(migrationConfig.artifactFile)
			onlyIf { zipFile.exists() }
			from(project.zipTree(zipFile))
			into(project.file(buildString {
				append(migrationConfig.artifactBuildPath)
				if (!project.buildConfig.buildProjectPrefixedDistribution) {
					append("/${project.name}")
				}
			}))
		}

		project.tasks.register("filterArtifactResources", Sync::class) {
			description = "Processes all property replacements within the artifact resources. The results are placed into a temporary directory."
			inputs.files(project.tasks.named("unzipArtifact"))
			if (project.buildConfig.isResourceFilteringEnabled) {
				project.projectConfig.applyPropertyReplacements(this)
			}
			from(project.file(migrationConfig.artifactContentsPath)) {
				include(project.buildConfig.fileTypesToFilter.map { "**/*$it" })
			}
			// External resources are dynamically generated based on properties and must always be recreated during artifact migration
			if (CliftonServicePlugin.PROCESS_EXTERNAL_RESOURCES_TASK_NAME in project.tasks.names) {
				from(project.tasks.named(CliftonServicePlugin.PROCESS_EXTERNAL_RESOURCES_TASK_NAME))
				doLast { project.delete(project.tasks.named(CliftonServicePlugin.PROCESS_EXTERNAL_RESOURCES_TASK_NAME)) }
			}
			into(project.file("${migrationConfig.artifactTmpPath}/$name"))
		}

		/*
		 * This is necessary for cases in which the project resources are included in the distribution as an archive (JAR) file. This is incorporated as a separate task since
		 * Gradle does not have a built-in way to extract, process, and re-archive a JAR artifact within a "Sync" or "Copy" task.
		 *
		 * This is a [Zip] task type rather than a [Jar] task type in order to allow reuse of the existing manifest rather than re-creating a manifest.
		 */
		project.tasks.register("filterArtifactJar", Zip::class) {
			description = "Extracts the project JAR file and processes its contents for all property replacements. The results are placed into a temporary directory."
			dependsOn("unzipArtifact")
			onlyIf { migrationConfig.artifactProjectRelativeJarPath != null }
			val artifactArchiveType = "jar"
			from(project.zipTree("${migrationConfig.artifactContentsPath}/${migrationConfig.artifactProjectRelativeJarPath}/${project.name}.$artifactArchiveType"))
			destinationDirectory.set(project.file("${migrationConfig.artifactTmpPath}/$name"))
			archiveExtension.set(artifactArchiveType)
			setMetadataCharset(Charsets.UTF_8.name()) // Enforce safe encoding for Java archives
			if (project.buildConfig.isResourceFilteringEnabled) {
				project.projectConfig.applyPropertyReplacements(this)
			}
			if (project.projectConfig.isSpringBootProject) {
				// Spring Boot fat JAR archives must not be compressed; nested archives cannot be loaded in compressed artifacts
				entryCompression = ZipEntryCompression.STORED
			}
		}

		project.tasks.register("filterArtifact", Sync::class) {
			description = "Processes property replacements for all artifact contents. The results are placed into a temporary directory."
			dependsOn("filterArtifactResources", "filterArtifactJar")
			into("${migrationConfig.artifactTmpPath}/$name")
			from(project.tasks.named("filterArtifactResources"))
			from(project.tasks.named("filterArtifactJar")) {
				into("${migrationConfig.artifactProjectRelativeJarPath}")
			}
			doLast {
				project.delete(project.tasks.named("filterArtifactResources"))
				project.delete(project.tasks.named("filterArtifactJar"))
			}
		}

		// Gradle cannot do in place file filtering; attempting in-place file filtering can conflict with incremental build optimizers
		project.tasks.register("copyFilteredArtifactResources", Copy::class) {
			description = "Copies all processed artifact resources back into ${project.file(migrationConfig.artifactContentsPath).relativeTo(project.projectDir)} after property replacements have occured. This overwrites any existing files in the distribution directory with their processed replacements."
			from(project.tasks.named("filterArtifact"))
			into(project.file(migrationConfig.artifactContentsPath))
			doLast { project.delete(project.tasks.named("filterArtifact")) }
		}

		// Zip back up the filtered war file or zip up the exploded service
		project.tasks.register("zipArtifact", Zip::class) {
			dependsOn("copyFilteredArtifactResources")
			onlyIf {
				val zipExists = project.file(migrationConfig.artifactFile).exists()
				if (project.buildConfig.buildProjectPrefixedDistribution) !zipExists else zipExists
			}

			from(project.file(migrationConfig.artifactContentsPath))
			if (project.buildConfig.buildProjectPrefixedDistribution) {
				// add the prefix to the rebuilt package
				into(project.name)
			}
			destinationDirectory.set(project.file(migrationConfig.artifactBuildPath))
			archiveExtension.set(migrationConfig.artifactFileType)
		}

		// Our migration runner looks for all schema files on the classpath relative to the META-INF directory. Since migration is decoupled we need to jar up root level project specific schema files
		project.tasks.register("jarArtifactSchemaFiles", Jar::class) {
			dependsOn("zipArtifact")
			onlyIf { migrationConfig.isArtifactMigrationEnabled && File(migrationConfig.artifactClassPath).exists() }
			from(project.files(migrationConfig.artifactClassPath))
			destinationDirectory.set(project.file("${project.buildDir}/tmp"))
		}

		// A task that will generically execute our migration runner
		project.tasks.register("migrateArtifact", MigrationExec::class) {
			description = "Runs migration command on an independent artifact"
			enabled = migrationConfig.isArtifactMigrationEnabled && migrationConfig.migrateType.isNotBlank()
			dependsOn("jarArtifactSchemaFiles", "copyMigrationLoggingConfig")

			val classpathTree = project.fileTree(migrationConfig.artifactFullClassPath) {
				exclude("**/*.kjb", "**/*.ktr", "**/*.json", "**/*.properties", "**/*.xml", "**/*.class", "**/*.ftl", "**/*.sql", "**/schema/*clifton*/*.xml", "**/*-launcher.jar")
			}

			// Schema files for main project to be processed with classpath resolvers
			val zipFileName = "${project.buildDir}/tmp/${project.name}.jar"

			action = migrationConfig.migrateType
			classpath = project.files(classpathTree, File(zipFileName), project.configurations["servletApiJar"].singleFile)
			if (project.projectConfig.isSpringBootProject) {
				/*
				 * The MigrationRunner class is only available through the Spring Boot class loader; use PropertiesLauncher to specify this entry point explicitly. See
				 * https://stackoverflow.com/questions/50367647/run-non-main-class-of-spring-boot-application-through-command-line.
				 */
				jvmArgs("-Dloader.main=${this.main}")
				// Update main after it is used to define the loader.main class.
				main = "org.springframework.boot.loader.PropertiesLauncher"
				jvmArgs("-Dloader.path=WEB-INF/classes,WEB-INF/lib")
			}
		}

		// Clean up filtering remnants
		project.tasks.register("cleanArtifactExplodedDir", Delete::class) {
			dependsOn("migrateArtifact")
			delete(migrationConfig.artifactContentsPath)
		}

		project.tasks.register("deleteArtifactDestDir", Delete::class) {
			dependsOn("cleanArtifactExplodedDir")
			onlyIf { migrationConfig.artifactDestinationPath != null }
			delete(migrationConfig.artifactDestinationPath)
		}

		// The last step of the artifact process is to copy the final filtered and migrated artifact to a central destination directory.  This location is defined in the build properties.
		project.tasks.register("copyArtifactToDestDir") {
			dependsOn("deleteArtifactDestDir")
			val artifact = project.file(migrationConfig.artifactFile)
			onlyIf { migrationConfig.artifactDestinationPath != null && artifact.exists() }
			// copy artifact and it"s exploded representation to the destination directory.
			// Use Ant to copy because Gradle copy does not work on linux when copying to a mounted CIFS/Windows file share (attempts to chmod files, which is not supported).
			doLast {
				ant.withGroovyBuilder {
					"copy"("file" to artifact, "toDir" to migrationConfig.artifactDestinationPath)
				}
				// exploded
				val suffixedPath = buildString {
					append(migrationConfig.artifactDestinationPath)
					if (!project.buildConfig.buildProjectPrefixedDistribution) {
						append("/${project.name}")
					}
				}
				ant.withGroovyBuilder {
					"unzip"("src" to artifact, "dest" to suffixedPath)
				}
			}
		}

		// Create a master task that will filter and migrate a built artifact
		project.tasks.register("filterAndMigrateArtifact") {
			dependsOn("copyArtifactToDestDir")
		}
	}
}
