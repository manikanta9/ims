@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.verification

import com.clifton.gradle.plugin.build.CliftonJavaPlugin
import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.requireRootProject
import com.github.spotbugs.SpotBugsExtension
import com.github.spotbugs.SpotBugsPlugin
import com.github.spotbugs.SpotBugsTask
import org.cyclonedx.gradle.CycloneDxPlugin
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.testing.Test
import org.gradle.kotlin.dsl.*
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.sonarqube.gradle.SonarQubeExtension
import org.sonarqube.gradle.SonarQubePlugin
import org.sonarqube.gradle.SonarQubeTask
import java.io.File
import java.util.*


/**
 * The Gradle [Plugin] for registering inspections, such as components for SonarQube or CycloneDX.
 */
class CliftonInspectionsPlugin : Plugin<Project> {

	companion object {
		const val SONAR_PROPERTIES_FILE_NAME = "sonar-project.properties"

		// Analysis narrowing properties (https://docs.sonarqube.org/7.9/project-administration/narrowing-the-focus/)
		const val SONAR_SOURCES = "sonar.sources"
		const val SONAR_TESTS = "sonar.tests"

		// Java analysis properties (https://docs.sonarqube.org/7.9/analysis/languages/java/)
		const val SONAR_JAVA_LIBRARIES = "sonar.java.libraries"
		const val SONAR_JAVA_BINARIES = "sonar.java.binaries"
		const val SONAR_JAVA_TEST_LIBRARIES = "sonar.java.test.libraries"
		const val SONAR_JAVA_TEST_BINARIES = "sonar.java.test.binaries"

		// Test coverage and execution properties (https://docs.sonarqube.org/7.9/analysis/coverage/)
		const val SONAR_JACOCO_REPORT_PATHS = "sonar.coverage.jacoco.xmlReportPaths"
		const val SONAR_JUNIT_REPORT_PATHS = "sonar.junit.reportPaths"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		applyCycloneDxFeatures(rootProject)
		applySonarQubeFeatures(rootProject)
		applySpotBugsFeatures(rootProject)
	}


	private fun applyCycloneDxFeatures(rootProject: Project) {
		rootProject.subprojects {
			if (this.projectConfig.isAppProject || this.projectConfig.isServiceProject) {
				this.pluginManager.apply(CycloneDxPlugin::class)
			}
		}
	}


	private fun applySonarQubeFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(SonarQubePlugin::class)
		CliftonSonarQubeExtension.create(rootProject)
		// Apply default values eagerly to allow for overrides
		applySonarQubeDefaultConfig(rootProject)
		// Wait until after evaluation so that the caller can configure the plugin
		rootProject.afterEvaluate { applySonarQubeCustomConfig(rootProject, rootProject.the()) }
	}


	private fun applySpotBugsFeatures(rootProject: Project) {
		// Guard-clause: SpotBugs enabled; SpotBugs is available to us, but is not typically used
		if (!rootProject.buildConfig.enableSpotBugs) {
			return
		}

		rootProject.subprojects {
			val project = this
			project.pluginManager.apply(SpotBugsPlugin::class)
			project.configure<SpotBugsExtension> {
				toolVersion = "3.1.12"
				isIgnoreFailures = true
				reportsDir = project.file("${rootProject.buildDir}/findbugsReports/${project.name}")
				effort = "max"
				reportLevel = "high"
			}

			project.tasks.withType<SpotBugsTask>().configureEach {
				reports.xml.isEnabled = false
				reports.html.isEnabled = true
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method applies the default properties to be used during scanner analysis. These properties can be subsequently overridden with environment parameters (under the
	 * [org.sonarsource.scanner.api.Utils.SONARQUBE_SCANNER_PARAMS] environment variable), with system properties, or by [any manual configurations]
	 * [CliftonSonarQubeExtension.workspace].
	 */
	private fun applySonarQubeDefaultConfig(rootProject: Project) {
		// Apply root-level SonarQube configurations
		val rootSonarqube = rootProject.the<SonarQubeExtension>()
		rootSonarqube.properties {
			/*
			 * Occasionally, properties provided through the command line require excessively long commands, producing operating system errors such as "Argument list too long".
			 * To work around this, property overrides may be appended to the properties file. These will supersede default properties and earlier properties listed in the
			 * properties file, but will not supersede system properties.
			 *
			 * Order of precedence for properties (last-in wins):
			 * - Properties file (last entry wins when duplicates exist)
			 * - Programmatically mapped properties (such as those from CliftonSonarQubeExtension actions)
			 * - System properties
			 */
			val sonarqubeProperties = Properties().apply {
				rootProject.file(SONAR_PROPERTIES_FILE_NAME)
						.takeIf { it.isFile }
						?.inputStream()?.use { load(it) }
			}

			@Suppress("UNCHECKED_CAST")
			properties(sonarqubeProperties.toMap() as Map<String, *>)
		}

		// Apply project-level SonarQube configurations
		rootProject.subprojects {
			configure<SonarQubeExtension> {
				properties {
					val sourceSets = this@subprojects.the<SourceSetContainer>()
					val productionSourceSets = sourceSets.matching { it.name !in rootProject.buildConfig.testSourceSets && it.name !in rootProject.buildConfig.ignoredSourceSets }
					val testSourceSets = sourceSets.matching { it.name in rootProject.buildConfig.testSourceSets }
					fun Collection<SourceSet>.getFiles(transform: (SourceSet) -> Collection<File>): Set<String> {
						val files = this.asSequence()
								.flatMap { transform(it).asSequence() }
								.filter { it.exists() }
								.sorted()
								.toList()
						// Remove duplicate files
						return files.filter { file -> files.first { file.startsWith(it) } == file }
								.map { it.toString() }
								.toSet()
					}
					// Include additional variants (by default, the SonarQube plugin only incorporates the "main" and "test" variants)
					property(SONAR_SOURCES, productionSourceSets.getFiles { it.allSource.srcDirs })
					property(SONAR_TESTS, testSourceSets.getFiles { it.allSource.srcDirs })
					property(SONAR_JAVA_BINARIES, productionSourceSets.getFiles { it.output.classesDirs.files })
					property(SONAR_JAVA_TEST_BINARIES, testSourceSets.getFiles { it.output.classesDirs.files })
					/*
					 * It is important that the JAR artifacts for upstream projects are referenced rather than the class files themselves. Sonar Findbugs automatically scans all
					 * unpackaged class files included within the libraries paths. This causes excessive redundant scanning if upstream project class files are included rather than
					 * the upstream project JAR files and has a dramatic impact on scan duration. See https://github.com/spotbugs/sonar-findbugs/issues/41.
					 */
					val javaHome = System.getProperty("java.home")
					property(SONAR_JAVA_LIBRARIES, productionSourceSets.getFiles { it.runtimeClasspath.files + it.annotationProcessorPath.files }
							+ (properties[SONAR_JAVA_LIBRARIES] as Collection<*>).filter { it.toString().startsWith(javaHome) }) // Retain existing JDK library entries
					property(SONAR_JAVA_TEST_LIBRARIES, testSourceSets.getFiles { it.runtimeClasspath.files + it.annotationProcessorPath.files }
							+ (properties[SONAR_JAVA_TEST_LIBRARIES] as Collection<*>).filter { it.toString().startsWith(javaHome) }) // Retain existing JDK library entries
				}
			}
		}
		if (rootProject.buildConfig.enableCodeCoverage) {
			/*
			 * Apply JUnit and JaCoCo report paths. This defers report-generating task configuration until SonarQube task configuration (i.e., we prevent configuration of the
			 * report-generating tasks via resolution of the "withType" task collections if the SonarQube task is not targeted). These must be applied both on a per-project basis
			 * and on the root project in order to be properly submitted to SonarQube.
			 */
			rootProject.tasks.named("sonarqube") {
				// Ensure reports are generated first
				mustRunAfter(rootProject.allprojects.map { it.tasks.withType(JacocoReport::class) })
				// Apply paths to sub-projects
				rootProject.subprojects {
					configure<SonarQubeExtension> {
						properties {
							val project = this@subprojects
							property(SONAR_JUNIT_REPORT_PATHS, project.tasks.withType(Test::class).joinToString(",") { it.reports.junitXml.destination.toRelativeString(project.projectDir) })
							property(SONAR_JACOCO_REPORT_PATHS, project.tasks.withType(JacocoReport::class).joinToString(",") { it.reports.xml.outputLocation.asFile.get().toRelativeString(project.projectDir) })
						}
					}
				}
				// Apply paths to root project
				rootSonarqube.properties {
					property(SONAR_JUNIT_REPORT_PATHS, rootProject.subprojects
							.flatMap { it.tasks.withType(Test::class) }
							.joinToString { it.reports.junitXml.destination.toRelativeString(rootProject.projectDir) })
					property(SONAR_JACOCO_REPORT_PATHS, rootProject.subprojects
							.flatMap { it.tasks.withType(JacocoReport::class) }
							.joinToString(",") { it.reports.xml.outputLocation.asFile.get().toRelativeString(rootProject.projectDir) })
				}
			}
		}
	}


	/**
	 * Applies properties to the [SonarQubeExtension] based off of the given [customConfig].
	 */
	private fun applySonarQubeCustomConfig(rootProject: Project, customConfig: CliftonSonarQubeExtension) {
		/*
		 * By default, SonarQube has dependencies on several tasks which produce relevant output for analysis, such as compile tasks, test tasks, and code coverage tasks. Due to
		 * our monorepo approach, we require customizations to these task dependencies and provide our own set instead. See the "Task dependencies" section here for more details:
		 * https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-gradle/#header-5.
		 */
		rootProject.tasks.named<SonarQubeTask>("sonarqube") { setDependsOn(listOf(customConfig.taskDependencies)) }

		// Apply configured actions to root project and sub-projects
		customConfig.workspaceActionList.forEach { action -> action.execute(rootProject.the()) }
		rootProject.subprojects { customConfig.subprojectActionList.forEach { action -> action.invoke(this.the(), this) } }
	}
}


////////////////////////////////////////////////////////////////////////////
////////            Supplementary Types                             ////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized inspection configuration. This extension provides fields for configuring plugin-specific inspection
 * customizations and also allows for configuring the [SonarQubeExtension] on a workspace and sub-project level.
 *
 * Example usage:
 * ```
 * cliftonSonarQube {
 *     taskDependencies.add("jacocoTestReport")
 *     workspace {
 *         properties {
 *             property("sonar.project.name", "My Project")
 *         }
 *     }
 *     subprojects { project ->
 *         isSkipProject = !project.projectConfig.isAppProject
 *     }
 * }
 * ```
 */
open class CliftonSonarQubeExtension(private val project: Project) {

	companion object {

		fun create(project: Project): CliftonSonarQubeExtension {
			return project.extensions.create("cliftonSonarQube", CliftonSonarQubeExtension::class, project)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The list of SonarQube task dependencies. This accepts all argument types accepted by [org.gradle.api.Task.dependsOn].
	 */
	val taskDependencies = project.objects.listProperty(Any::class)
			.apply { finalizeValueOnRead() }
			.apply {
				// Compile and package all sources before running scans
				add(project.rootProject.subprojects.map { it.tasks.named(CliftonJavaPlugin.ASSEMBLE_ALL_TASK_NAME) })
				// Since some source sets (such as tests) do not produce artifacts, include the "classes" task for each source set as well
				add(project.rootProject.subprojects.map { it.tasks.named(CliftonJavaPlugin.ALL_CLASSES_TASK_NAME) })
			}
	internal val workspaceActionList: MutableList<Action<SonarQubeExtension>> = mutableListOf()
	internal val subprojectActionList: MutableList<SonarQubeExtension.(Project) -> Unit> = mutableListOf()


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	fun workspace(action: Action<SonarQubeExtension>) {
		workspaceActionList += action
	}


	fun subprojects(action: SonarQubeExtension.(Project) -> Unit) {
		subprojectActionList += action
	}
}
