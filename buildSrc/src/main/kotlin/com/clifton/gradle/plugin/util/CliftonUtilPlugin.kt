@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.util

import com.clifton.gradle.plugin.build.CliftonJavaPlugin
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Buildable
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.*
import org.gradle.api.attributes.Attribute
import org.gradle.api.attributes.AttributeContainer
import org.gradle.api.internal.artifacts.dsl.LazyPublishArtifact
import org.gradle.api.internal.artifacts.publish.DecoratingPublishArtifact
import org.gradle.api.internal.tasks.AbstractTaskDependencyResolveContext
import org.gradle.api.internal.tasks.TaskDependencyContainer
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.diagnostics.ProjectBasedReportTask
import org.gradle.api.tasks.diagnostics.internal.ReportRenderer
import org.gradle.api.tasks.diagnostics.internal.TextReportRenderer
import org.gradle.api.tasks.options.Option
import org.gradle.internal.deprecation.DeprecatableConfiguration
import org.gradle.internal.graph.GraphRenderer
import org.gradle.internal.logging.text.StyledTextOutput.Style
import org.gradle.kotlin.dsl.apply
import org.gradle.kotlin.dsl.register
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.isAccessible
import kotlin.reflect.jvm.javaField


/**
 * The Gradle [Plugin] for registering generic utility scripts.
 *
 * @author MikeH
 */
class CliftonUtilPlugin : Plugin<Project> {

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		applyRootProjectFeatures(rootProject)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyRootProjectFeatures(rootProject: Project) {
		rootProject.tasks.register("cleanLocalBuildCache") {
			description = "Deletes the full contents of the local build cache. As an alternative, the \"--rerun-tasks\" argument may be included during task execution to clear and repopulate the build cache for only the executed tasks."
			doLast {
				val cacheDir = File(project.gradle.gradleUserHomeDir, "caches/build-cache-1")
				if (!cacheDir.exists()) {
					project.logger.warn("The Gradle cache directory {} could not be found.", cacheDir)
					return@doLast
				}
				project.logger.error("""
					================================================================================
					====================                WARNING                ====================
					================================================================================
					The Gradle Build Cache should never need to be deleted. If you are experiencing
					an issue with stale cache entries, please notify Mike Hill or Nick Kirsch so
					that the issue can be appropriately documented and resolved.
					https://wiki.paraport.com/display/IT/Gradle+Enterprise
					================================================================================
				""".trimIndent())
				project.logger.warn("Deleting Gradle cache directory: {}", cacheDir)
				cacheDir.deleteRecursively()
			}
		}
	}


	private fun applyPerProjectFeatures(project: Project) {
		project.tasks.register("printConfigurations", PrintConfigurationsTask::class) {
			description = "Prints debug information for each of the configurations for the project, including artifacts and configuration inheritance."
		}

		project.tasks.register("downloadDependencies") {
			description = "Downloads all dependencies for the project. This is a utility task which can be used to automatically populate the dependency cache. If the built-in Gradle command line parameter \"--refresh-dependencies\" is provided, then all dependencies will be re-resolved from the available repositories."
			doLast {
				val fileCount = project.configurations
						.filter { it.isCanBeResolved && !(it is DeprecatableConfiguration && it.resolutionAlternatives != null) }
						.map { it.files.size } // Resolution takes place during the "files" property access
						.sum()
				project.logger.info("Downloaded all dependencies ([{}] files).", fileCount)
			}
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


private class SimpleTaskDependencyResolveContext : AbstractTaskDependencyResolveContext() {
	val dependencyList = mutableListOf<Any>()


	override fun add(dependency: Any) {
		dependencyList.add(dependency)
	}


	override fun getTask(): Task? {
		error("This method is not implemented.")
	}
}


private val LAZY_PUBLISH_ARTIFACT_PROVIDER = LazyPublishArtifact::class.memberProperties
		.find { it.name == "provider" }
		?.apply { isAccessible = true }
		?.javaField!!


/**
 * A [Task] type which prints [Configuration] objects and their associated [artifacts][PublishArtifact] and [dependencies][Dependency].
 */
open class PrintConfigurationsTask : ProjectBasedReportTask() {

	@Internal
	@set:Option(option = "external", description = "Includes external dependencies in the dependency list.")
	var includeExternalDependencies = false

	private val renderer = TextReportRenderer()


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun generate(project: Project) {
		GraphRenderer(renderer.textOutput).run {
			startChildren()
			render(createProjectReport(project), this, true)
			completeChildren()
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun render(tree: ReportTextTree, graphRenderer: GraphRenderer, lastChild: Boolean) {
		graphRenderer.visit({
			tree.textBlocks.forEach {
				style(it.style ?: tree.defaultStyle)
				text(it.text)
			}
		}, lastChild)
		if (tree.children.isNotEmpty()) {
			graphRenderer.startChildren()
			tree.children.forEachIndexed { index, child -> render(child, graphRenderer, index == tree.children.size - 1) }
			graphRenderer.completeChildren()
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun createProjectReport(project: Project): ReportTextTree {
		return ReportTextTree("Project: ", Style.Header).apply {
			text(project.name, Style.Identifier)
			project.configurations.forEach { addConfiguration(it) }
		}
	}


	private fun ReportTextTree.addConfiguration(config: Configuration) {
		child("Configuration: ", Style.Header) {
			text(config.name, Style.Identifier)
			child("Description: ", Style.Header) {
				text(config.description ?: "[N/A]", Style.Description)
			}
			if (config.extendsFrom.isNotEmpty()) {
				child("Extends From: ", Style.Header) {
					text(config.extendsFrom.joinToString(", ") { it.name }, Style.Description)
				}
			}
			child("Properties:", Style.Header) {
				child("isCanBeResolved: ", Style.Header) { text("${config.isCanBeResolved}", Style.Description) }
				child("isCanBeConsumed: ", Style.Header) { text("${config.isCanBeConsumed}", Style.Description) }
				child("isTransitive: ", Style.Header) { text("${config.isTransitive}", Style.Description) }
				child("isVisible: ", Style.Header) { text("${config.isVisible}", Style.Description) }
			}
			config.allDependencies.takeIf { it.isNotEmpty() }?.let { dependencies ->
				child("Dependencies: ", Style.Header) {
					dependencies.forEach { addDependency(it) }
				}
			}
			config.allArtifacts.takeIf { it.isNotEmpty() }?.let { artifacts ->
				child("Artifacts: ", Style.Header) {
					artifacts.forEach { addArtifact(it) }
				}
			}
			config.attributes.takeIf { it.keySet().isNotEmpty() }?.let { attributes ->
				child("Attributes: ", Style.Header) {
					attributes.keySet().forEach { addAttribute(attributes, it) }
				}
			}
			addPublications(config.outgoing)
		}
	}


	private fun ReportTextTree.addDependency(dependency: Dependency) {
		when {
			dependency is ExternalModuleDependency && !includeExternalDependencies -> Unit // Ignore this dependency
			dependency is FileCollectionDependency -> {
				child("Dependency: ", Style.Header) {
					text(dependency.name.takeIf { it != "unspecified" } ?: "[Unnamed]", Style.Description)
					getBuildDependencies(dependency).takeIf { it.isNotEmpty() }?.let { buildDependencies ->
						child("Tasks: ", Style.Header) {
							text(buildDependencies.joinToString(", "), Style.Description)
						}
					}
					child("Files: ", Style.Header) {
						text(dependency.files.joinToString(", ") { it.path }, Style.Description)
					}
				}
			}
			else -> {
				child("Dependency: ", Style.Header) {
					text("${dependency.name} (${dependency})", Style.Description)
				}
			}
		}
	}


	private fun ReportTextTree.addArtifact(artifact: PublishArtifact) {
		child("Artifact: ", Style.Header) {
			text(artifact.name, Style.Description)
			getBuildDependencies(artifact).takeIf { it.isNotEmpty() }?.let { buildDependencies ->
				child("Tasks: ", Style.Header) {
					text(buildDependencies.joinToString(", "), Style.Description)
				}
			}
			child("Files: ", Style.Header) {
				text(artifact.file.path, Style.Description)
			}
		}
	}


	private fun ReportTextTree.addPublications(publications: ConfigurationPublications) {
		val publicationsTree = ReportTextTree("Outgoing:", Style.Header).apply {
			publications.capabilities.takeIf { it.isNotEmpty() }?.let { capabilities ->
				child("Outgoing Capabilities:", Style.Header) {
					capabilities.forEach { child("${it.group}:${it.name}:${it.version}", Style.Description) }
				}
			}
			publications.artifacts.takeIf { it.isNotEmpty() }?.let { artifacts ->
				child("Outgoing Artifacts:", Style.Header) {
					artifacts.forEach { addArtifact(it) }
				}
			}
			publications.variants.takeIf { it.isNotEmpty() }?.let { variants ->
				child("Outgoing Variants:", Style.Header) {
					variants.forEach { variant ->
						child("Variant: ", Style.Header) {
							text(variant.name, Style.Description)
							variant.artifacts.forEach { addArtifact(it) }
							variant.attributes.keySet().forEach { addAttribute(variant.attributes, it) }
						}
					}
				}
			}
			publications.attributes.takeIf { it.keySet().isNotEmpty() }?.let { attributes ->
				child("Outgoing Attributes:", Style.Header) {
					attributes.keySet().forEach { addAttribute(attributes, it) }
				}
			}
		}
		if (publicationsTree.children.isNotEmpty()) {
			child(publicationsTree)
		}
	}


	private fun ReportTextTree.addAttribute(attributes: AttributeContainer, key: Attribute<*>) {
		child("Attribute: ", Style.Header) {
			text("${key.type.simpleName} (${key.name}): ${attributes.getAttribute(key)}", Style.Description)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun getBuildDependencies(source: Any, iterations: AtomicInteger = AtomicInteger(0)): List<Any> {
		val objectQueue = ArrayDeque(listOf(source))
		val buildDependencyList = mutableListOf<Any>()
		while (objectQueue.isNotEmpty()) {
			check(iterations.incrementAndGet() <= 100_000) { "An excessive number of iterations have occurred. Terminating evaluation." }
			when (val currentObj = objectQueue.pop()) {
				is Task -> buildDependencyList.add(currentObj.name)
				is TaskProvider<*> -> buildDependencyList.add(currentObj.name)
				is PublishArtifact -> getTaskProviderFromArtifact(currentObj)?.let { objectQueue.add(it) }
				is TaskDependencyContainer -> objectQueue.addAll(getTaskDependencyContainerTaskList(currentObj, iterations))
				is Buildable -> objectQueue.add(currentObj.buildDependencies)
				else -> buildDependencyList.add(currentObj)
			}
		}
		return buildDependencyList
	}


	private fun getTaskProviderFromArtifact(artifact: PublishArtifact): Any? {
		return artifact.let { if (it is DecoratingPublishArtifact) it.publishArtifact else it }
				.let { if (it is LazyPublishArtifact) LAZY_PUBLISH_ARTIFACT_PROVIDER.get(it) else null }
	}


	private fun getTaskDependencyContainerTaskList(taskDependencyContainer: TaskDependencyContainer, iterations: AtomicInteger = AtomicInteger(0)): List<Any> {
		val resolveContext = SimpleTaskDependencyResolveContext()
		taskDependencyContainer.visitDependencies(resolveContext)
		return resolveContext.dependencyList.flatMap { getBuildDependencies(it, iterations) }
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun getRenderer(): ReportRenderer = this.renderer
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/* Report text tree types */

@DslMarker
private annotation class ReportTextTreeDsl

@ReportTextTreeDsl
private class ReportTextTree(text: String, style: Style = Style.Normal) {

	val defaultStyle = style
	val textBlocks = mutableListOf(ReportTextPart(text, style))
	val children = mutableListOf<ReportTextTree>()

	fun text(textPart: ReportTextPart) =
			this.textBlocks.add(textPart)

	fun text(text: String, style: Style? = null) =
			ReportTextPart(text, style ?: defaultStyle).also { text(it) }

	fun child(textTree: ReportTextTree) =
			this.children.add(textTree)

	fun child(text: String, style: Style? = null, init: ReportTextTree.() -> Unit = {}) =
			ReportTextTree(text, style ?: defaultStyle).also { it.init(); child(it) }
}

private class ReportTextPart(val text: String, val style: Style?)
