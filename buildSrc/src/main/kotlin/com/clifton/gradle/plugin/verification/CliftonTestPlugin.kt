@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.verification

import com.clifton.gradle.plugin.build.CliftonJavaPlugin
import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.internal.tasks.testing.filter.DefaultTestFilter
import org.gradle.api.internal.tasks.testing.filter.TestSelectionMatcher
import org.gradle.api.plugins.JavaTestFixturesPlugin
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.StopExecutionException
import org.gradle.api.tasks.testing.*
import org.gradle.internal.component.external.model.TestFixturesSupport
import org.gradle.kotlin.dsl.*
import org.gradle.testing.jacoco.plugins.JacocoPlugin
import org.gradle.testing.jacoco.plugins.JacocoTaskExtension
import org.gradle.testing.jacoco.tasks.JacocoReport
import org.gradle.testretry.TestRetryPlugin
import java.io.File
import java.lang.reflect.Modifier
import java.net.URLClassLoader
import kotlin.reflect.jvm.jvmName


// Test profiles
private const val TEST_PROFILE_MEMORY_DB = "memory-db"
private const val TEST_PROFILE_DATABASE = "database"


/**
 * The Gradle [Plugin] for configuring test tasks.
 */
class CliftonTestPlugin : Plugin<Project> {

	companion object {
		const val INTEG_TEST_TASK_NAME = "integTest"
		const val MEMORY_DB_TEST_TASK_NAME = "memoryDbTest"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		applyRootProjectFeatures(rootProject)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyRootProjectFeatures(rootProject: Project) {
		/*
		 * Manually add upstream test tasks to encourage IntelliJ to attach the debugger to these, even when only the primary test task is executed.
		 *
		 * As of version 2020.3, IntelliJ attempts to only attach the debugger to explicitly-requested tasks (see https://youtrack.jetbrains.com/issue/IDEA-200192). This reduces
		 * usability for testing, as our test sequence includes multiple tasks which are intended to automatically be executed together. The IntelliJ script (ijresolvers1.gradle)
		 * filters requested tasks via the "taskNames" field. This segment updates the value of this field to encourage IntelliJ to consider our upstream test tasks as explicitly-
		 * requested task targets.
		 */
		val testTaskRegex = Regex("""(?<=(^|:))test$""")
		val testTaskList = rootProject.requestedTaskArguments.filter { it.contains(testTaskRegex) }
		if (testTaskList.isNotEmpty()) {
			val memoryDbTaskList = testTaskList
					.map { it.replace(testTaskRegex, MEMORY_DB_TEST_TASK_NAME) }
					.filter { !rootProject.requestedTaskArguments.contains(it) }
			rootProject.requestedTaskArguments += memoryDbTaskList
		}
	}


	private fun applyPerProjectFeatures(project: Project) {
		configureTestExtension(project)
		if (project.buildConfig.enableTestRetry) {
			configureTestRetryPlugin(project)
		}
		configureTestFixturesPlugin(project)
		configureTestCoverage(project)
		configureMemoryDbTests(project)
		if (project.projectConfig.isAppProject) {
			configureIntegrationTests(project)
		}

		project.configure<SourceSetContainer> {
			// Allow us to include test resources (e.g., context XML files) alongside sources
			named(SourceSet.TEST_SOURCE_SET_NAME) {
				resources {
					srcDirs(this@named.java.srcDirs)
				}
			}
		}

		// The aggregate task for running all tests together
		project.tasks.named("test", Test::class) {
			//Allows us to set the junit profile to execute under and integration test url override
			systemProperty("ims-url", System.getProperty("ims-url"))
		}

		// Apply common properties to test tasks
		project.tasks.withType(Test::class).configureEach {
			/* Configure inputs/outputs */
			inputs.property("includeTags", includeTags)
			inputs.property("excludeTags", excludeTags)
			// Disable local caching to allow test re-runs
			outputs.upToDateWhen { project.buildConfig.isCiServer }
			outputs.cacheIf { project.buildConfig.isCiServer }

			/* Configure test JVM parameters */
			minHeapSize = "128m"
			maxHeapSize = "1024m"
			maxParallelForks = project.buildConfig.maxProjectTestThreads // number of concurrent threads running tests for a project
			jvmArgs("-javaagent:${project.configurations[CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION].singleFile}")
			// Use a JAR manifest for classpaths to prevent command line overflows (the "CLASSPATH" environment variable cannot be configured for Test tasks)
			doFirst { classpath = project.files(project.classpathJar(classpath)) }

			/* Configure test output */
			// Show standard out and standard error of the test JVM(s) on the console
			testLogging.showStandardStreams = true
			ignoreFailures = true
			testLogging { events("passed", "skipped", "failed") }
			systemProperty("log4j.configurationFile", "log4j2-test.xml") // Specify test logging configuration file
			reports.html.isEnabled = false

			/* Configure test execution */
			filter.isFailOnNoMatchingTests = false
			getIncludedTestPatternList(project).singleOrNull()?.let { setTestNameIncludePatterns(listOf(it)) }
			// Jupiter is used for JUnit 5 tests; Vintage is used for JUnit 4 tests
			useJUnitPlatform {
				includeEngines("junit-jupiter", "junit-vintage")
				includeTags = this@configureEach.includeTags
				excludeTags = this@configureEach.excludeTags
			}
		}

		// Skip local test execution when not directly executing test tasks
		project.tasks.named("check") {
			val executeBuildChecks = project.buildConfig.isCiServer || project.requestedTaskNames.any { project.tasks.findByName(it) is AbstractTestTask }
			inputs.property("executeBuildChecks", executeBuildChecks)
			if (!executeBuildChecks) {
				dependsOn.clear()
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun configureTestExtension(project: Project): CliftonTestExtension {
		return project.extensions.create("cliftonTest", CliftonTestExtension::class, project)
	}


	private fun configureTestRetryPlugin(project: Project) {
		project.pluginManager.apply(TestRetryPlugin::class)
		project.tasks.withType(Test::class).configureEach {
			/* Configure test retries to account for flaky tests */
			retry {
				maxRetries.set(3)
				maxFailures.set(20)
				failOnPassedAfterRetry.set(false)
			}
			// Exclude flaky failures from JUnit failed test reports
			reports.junitXml.mergeReruns.set(true)
		}
	}


	private fun configureTestFixturesPlugin(project: Project) {
		// Apply the test-fixtures plugin to allow test-specific sources to be distinguished from test classes
		project.pluginManager.apply(JavaTestFixturesPlugin::class)
		project.configure<SourceSetContainer> {
			// Allow us to include test resources (e.g., context XML files) alongside sources
			named(TestFixturesSupport.TEST_FIXTURE_SOURCESET_NAME) {
				resources {
					srcDirs(this@named.java.srcDirs)
				}
			}
		}

		// Include all implementation dependencies in the test fixtures path
		/*
		 * NOTE: Using the API test fixtures configuration causes implementation dependencies to deeply traverse throughout the text fixtures dependency tree. Once the IntelliJ
		 * bug causing this issue has been fixed (https://youtrack.jetbrains.com/issue/IDEA-231870), this should be changed to alter "testFixturesImplementation" instead.
		 */
		project.configurations["testFixturesApi"].extendsFrom(project.configurations["implementation"])

		// IntelliJ (as of 2019.3.2) erroneously treats testFixturesImplementation dependencies as transient in several contexts; warn on any declared dependencies
		// See https://youtrack.jetbrains.com/issue/IDEA-231870
		project.afterEvaluate {
			project.configurations["testFixturesImplementation"].withDependencies {
				if (this.isNotEmpty()) {
					val dependencyListStr = this.joinToString("\n- ", "\n- ")
					project.logger.error("[${project.name}] Warning: Declared dependencies for the \"testFixturesImplementation\" configuration may cause errors with IntelliJ. See https://youtrack.jetbrains.com/issue/IDEA-231870 for more details. Detected dependencies: $dependencyListStr")
				}
			}
		}
	}


	private fun configureTestCoverage(project: Project) {
		project.pluginManager.apply(JacocoPlugin::class)

		// Configure JaCoCo instrumentation
		val testExtension = project.the<CliftonTestExtension>()
		val testTasks = project.tasks.withType(Test::class)
		// Do not instrument test classes to avoid impacts on reflection
		testTasks.configureEach { configure<JacocoTaskExtension> { excludes!! += listOf("*.Test*", "*\$Test*", "*Tests") } }
		// Allow user configuration for disabling test coverage instrumentation
		project.afterEvaluate {
			testTasks.configureEach {
				val task = this
				configure<JacocoTaskExtension> {
					isEnabled = testExtension.testCoverageIncludedTasks.get().let { it.isEmpty() || it.contains(task.name) }
							&& task.name !in testExtension.testCoverageExcludedTasks.get()
				}
			}
		}


		// Configure reports
		project.tasks.named("test", Test::class) { finalizedBy("jacocoTestReport") }
		project.tasks.named("jacocoTestReport", JacocoReport::class) {
			enabled = project.buildConfig.enableCodeCoverage
			// Include coverage results from all test tasks, rather than just the default "test" task
			/*
			 * Note: executionData(project.tasks.withType(Test::class)) fails here with mutation guards due to eager task configuration. mustRunAfter and setFrom are the lazy
			 * equivalents of the executionData(...) call.
			 */
			val testTaskCollection = project.tasks.withType(Test::class)
			mustRunAfter(testTaskCollection)
			executionData.setFrom(project.provider { testTaskCollection.mapNotNull { task -> task.extensions.findByType(JacocoTaskExtension::class)?.destinationFile?.takeIf { it.exists() } } })

			// Include non-standard source sets
			sourceSets(*project.the<SourceSetContainer>().matching { it.name !in project.buildConfig.testSourceSets && it.name !in project.buildConfig.ignoredSourceSets }.toTypedArray())
			reports { xml.isEnabled = true }

			// Enable HTML reports for local builds
			if (!project.buildConfig.isCiServer) {
				reports {
					html.isEnabled = true
					doLast { println("Code coverage report generated to directory: ${html.entryPoint.absoluteFile.toPath().toUri()}") }
				}
			}
		}
	}


	private fun configureMemoryDbTests(project: Project) {
		// Test Profile related project properties
		val memoryDbPropertySourceMap = mapOf(
				"clifton-fix" to ":app-clifton-fix",
				"clifton-fix-quickfix" to ":app-clifton-fix",
				"clifton-fix-quickfix-server" to ":app-clifton-fix",
				"clifton-fix-server" to ":app-clifton-fix",
				"clifton-swift" to ":service-clifton-swift",
				"clifton-integration" to ":service-clifton-integration",
				"clifton-portal" to ":app-clifton-client-portal",
				"clifton-reconciliation" to ":app-clifton-reconciliation",
				"clifton-order" to ":app-clifton-oms",
				"clifton-order-base" to ":app-clifton-oms",
				"clifton-order-management" to ":app-clifton-oms"
		)

		// Gradle task for exporting data for in memory db tests
		project.tasks.register("exportMemoryDbTestData", JavaExec::class) {
			// Skip projects which do not depend on core
			onlyIf { task ->
				task as JavaExec
				val classpathUrls = task.classpath.map { it.toURI().toURL() }.toTypedArray()
				URLClassLoader.newInstance(classpathUrls).use { tryOrFalse { it.loadClass(task.main) } }
			}
			main = "com.clifton.core.dataaccess.export.InMemoryDatabaseDataExportRunner"
			classpath = project.the<SourceSetContainer>()[SourceSet.TEST_SOURCE_SET_NAME].runtimeClasspath
			jvmArgs(
					"-javaagent:${project.configurations[CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION].singleFile}",
					"-Dlog4j.configurationFile=log4j2-test.xml"
			)
			doFirst {
				val propertySourceProject = memoryDbPropertySourceMap[project.name]
				val imsProperties = project.project(propertySourceProject ?: ":app-clifton-ims").projectConfig.buildProperties
				systemProperties("jdbc.driverClassName" to imsProperties["dataSource.driverClassName"])
				systemProperties("jdbc.url" to "jdbc:sqlserver://${imsProperties["dataSource.databaseServerName"]}:${imsProperties["dataSource.port"]};databaseName=${imsProperties["dataSource.databaseName"]}")
				systemProperties("jdbc.username" to imsProperties["dataSource.username"])
				systemProperties("jdbc.password" to imsProperties["dataSource.password"])
				systemProperties("hibernate.dialect" to imsProperties["hibernate.dialect"])
				systemProperties("hibernate.show_sql" to imsProperties["hibernate.show_sql"])
				systemProperties("hibernate.format_sql" to imsProperties["hibernate.format_sql"])
				systemProperties("hibernate.generate_statistics" to imsProperties["hibernate.generate_statistics"])
				classpath = project.files(project.classpathJar(classpath))
			}
		}

		// Replace task registration during IntelliJ task querying to hide this task from the test context menu drop-down
		if (project.buildConfig.isIdeaGradleSync) {
			project.tasks.register(MEMORY_DB_TEST_TASK_NAME) {
				description = "Run in-memory database tests. This version of this task is a placeholder for use with IntelliJ tooling. It is intentionally not marked as a test task type so that IntelliJ will not provide it as an alternative option when attempting to execute tests, as execution of the default test task will include in-memory DB tests. This placeholder exists for the event in which the user wishes to manually run only the in-memory database tests for the project. The runtime task type of this task will be a test task."
			}
		} else {
			/*
			 * Note: This task could theoretically be merged with the "test" task once separate per-class class loaders are enabled in JUnit 5 and scoped for version 5.7 (#201, #806,
			 * and #2104, via https://github.com/junit-team/junit5/issues/2104#issuecomment-557072890). The class loader issue is caused by weaving, where in-memory database tests
			 * require weaving to be enabled, but other tests will not succeed when weaving is enabled.
			 */
			project.tasks.register(MEMORY_DB_TEST_TASK_NAME, Test::class) {
				description = "Run in-memory database tests. This exists as a separate task in order to segregate the JVM from the other test JVMs, as class-weaving is enabled for these tests."
				onlyIf { !project.buildConfig.skipMemoryDbTests && project.buildConfig.testProfiles.let { it.isEmpty() || TEST_PROFILE_MEMORY_DB in it } }
				includeTags += TEST_PROFILE_MEMORY_DB
			}
			/*
			 * Modify test filters *after* the include patterns are set by ijtestinit.gradle (taskGraph#whenReady) but *before* the debugger attach process is initiated by
			 * ijresolvers.gradle (task#doFirst).
			 */
			project.gradle.taskGraph.whenReady {
				project.tasks.named(MEMORY_DB_TEST_TASK_NAME, Test::class) {
					doFirst {
						// Include test files only; this prevents JUnit from pre-maturely loading any other classes during test discovery and preventing them from being appropriately woven
						/*
						 * If "includePatterns" is non-empty, assume it should be used as command-line test params instead. IntelliJ's test init script (ijtestinit.gradle) pre-sets
						 * the "includePatterns" field using the selected tests. This should use "commandLineIncludePatterns" instead, since the command-line patterns take
						 * precedence over the "includePatterns" field.
						 */
						if (filter.includePatterns.isNotEmpty()) {
							(filter as DefaultTestFilter).commandLineIncludePatterns += filter.includePatterns
							filter.includePatterns.clear()
						}
						val testClassList = filterTestClassesForMemoryDbTests(project)
						filter.setIncludePatterns(*testClassList.toTypedArray())
						/*
						 * As of IntelliJ 2020.3, attempting to debug non-terminal test tasks with no matching tests causes IntelliJ to hang indefinitely. As a workaround, make a
						 * best-effort attempt to determine whether any tests would be executed. If no tests would be executed, terminate task execution before the debugger signal-
						 * sending action is reached. See https://youtrack.jetbrains.com/issue/IDEA-260282 for more details.
						 */
						addTestListener(NoMatchingTestsLoggingReporter(this@named, "The test task [$name] was executed, but no matching tests were found. This can cause IntelliJ to hang indefinitely when debugging is enabled. A pre-execution StopExecutionException condition may need to be added or enhanced to cover these cases. See https://youtrack.jetbrains.com/issue/IDEA-260282 for more details."))
						val testMatcher = TestSelectionMatcher(filter.includePatterns, filter.excludePatterns, (filter as DefaultTestFilter).commandLineIncludePatterns)
						if (testClassList.none { testMatcher.mayIncludeClass(it) }) {
							throw StopExecutionException("No qualifying test classes were found matching the given test filter patterns.")
						}
					}
				}
			}
		}

		project.tasks.named("test", Test::class) {
			// Ensure that in-memory tests are executed alongside normal test execution
			dependsOn(MEMORY_DB_TEST_TASK_NAME)
			//Allows us to override the default memory based cookie store with a persistent cookie store for integration tests
			systemProperty("usePersistentCookieStore", System.getProperty("usePersistentCookieStore"))

			/* Configure test selection */
			excludeTags += TEST_PROFILE_MEMORY_DB
			if (project.buildConfig.testProfiles.isEmpty()) {
				// Default test selection
				excludeTags += TEST_PROFILE_DATABASE
			} else {
				includeTags += project.buildConfig.testProfiles
				includeTags -= TEST_PROFILE_MEMORY_DB // Always exclude in-memory DB tests, even if they were included in the testProfiles property
			}
		}
	}


	private fun configureIntegrationTests(project: Project) {
		// Create integration test source set
		val sourceSets = project.the<SourceSetContainer>()
		val mainSourceSet = sourceSets[SourceSet.MAIN_SOURCE_SET_NAME]
		val integrationTestSourceSet = sourceSets.create("integTest") {
			compileClasspath += mainSourceSet.output
			runtimeClasspath += mainSourceSet.output
			resources {
				srcDirs(this@create.java.srcDirs)
			}
		}
		project.configurations[integrationTestSourceSet.implementationConfigurationName].extendsFrom(project.configurations[mainSourceSet.implementationConfigurationName])
		project.configurations[integrationTestSourceSet.runtimeOnlyConfigurationName].extendsFrom(project.configurations[mainSourceSet.runtimeOnlyConfigurationName])
		// Register integration test task
		project.tasks.register(INTEG_TEST_TASK_NAME, Test::class) {
			description = "Runs integration tests."
			group = "verification"
			// Disable integration test caching since inputs are highly variable and cannot be configured
			outputs.cacheIf { false }
			shouldRunAfter("test")
			classpath = integrationTestSourceSet.runtimeClasspath
			testClassesDirs = integrationTestSourceSet.output.classesDirs
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the list of test patterns included in the run. This is an aggregation of all non-JVM arguments prefixed with `--tests`.
	 *
	 * Example:
	 * ```
	 * $> ./gradlew :clifton-compliance:test --tests 'com.clifton.compliance.*' :clifton-system:test --tests 'com.clifton.system.*
	 * # Resulting list of test patterns: ["com.clifton.compliance.*", "com.clifton.system.*"]
	 * ```
	 *
	 * For reference, built-in command line parsing can be found at [org.gradle.execution.commandline.CommandLineTaskConfigurer.configureTasksNow].
	 */
	private fun getIncludedTestPatternList(project: Project): List<String> {
		val includedTestPatternList by project.rootProject.extra.withDefaultSupplier {
			// Parse all "tests" options
			val testPatternList = mutableListOf<String>()
			for (taskRequest in project.gradle.startParameter.taskRequests) {
				val argIterator = taskRequest.args.iterator()
				while (argIterator.hasNext()) {
					val arg = argIterator.next()
					if (arg == "--tests") {
						// Options only support a single argument, so the pattern is always the next argument
						require(argIterator.hasNext()) { "A --tests parameter was found with no arguments. Full arguments list: [${taskRequest.args.joinToString(", ")}]." }
						testPatternList.add(argIterator.next())
					}
				}
			}
			testPatternList
		}
		return includedTestPatternList
	}


	private fun filterTestClassesForMemoryDbTests(project: Project): List<String> {
		// Get all in-memory database test class names
		val projectSourceSets = project.the<SourceSetContainer>()
		val cwd = File(System.getProperty("user.dir"))
		val testClassNames = projectSourceSets[SourceSet.TEST_SOURCE_SET_NAME].output.classesDirs
				.filter { it.exists() }
				.map { it.relativeTo(cwd) }
				.flatMap { classDir ->
					val rootPath = "${classDir.path}${File.separatorChar}"
					classDir.walk()
							.onEnter { it.isDirectory || it.absolutePath.endsWith(".class") }
							.filter { !it.isDirectory } // Some process appears to hold locks to class files at some points, causing isFile to erroneously return false
							.map { it.path.removePrefix(rootPath).replace(File.separatorChar, '.').removeSuffix(".class") }
							.toList()
				}
				.takeIf { it.isNotEmpty() } ?: return emptyList()
		// Filter to applicable in-memory database test types
		val urlList = projectSourceSets[SourceSet.TEST_SOURCE_SET_NAME].runtimeClasspath.map { it.toURI().toURL() }.toTypedArray()
		URLClassLoader.newInstance(urlList).use { classLoader ->
			@Suppress("UNCHECKED_CAST")
			val tagClass = classLoader.loadClass("org.junit.jupiter.api.Tag") as Class<out Annotation>? ?: return emptyList()
			val tagValueMethod = tagClass.methods.find { it.name == "value" } ?: error("Unable to find expected method [value] for type [${tagClass.name}].")
			return testClassNames.filter { testClassName ->
				val testClass = classLoader.loadClass(testClassName)
				!Modifier.isAbstract(testClass.modifiers)
						&& testClass.getAnnotationsByType(tagClass).any { tagValueMethod.invoke(it) == "memory-db" }
						&& "org.junit.jupiter.api.Disabled" !in testClass.annotations.map { it.annotationClass.jvmName }
			}
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////            Extension Functions                             ////////
////////////////////////////////////////////////////////////////////////////


val Test.includeTags: MutableSet<String>
	get() {
		val includeTags: MutableSet<String> by this.extra.withDefaultSupplier { mutableSetOf() }
		return includeTags
	}
val Test.excludeTags: MutableSet<String>
	get() {
		val excludeTags: MutableSet<String> by this.extra.withDefaultSupplier { mutableSetOf() }
		return excludeTags
	}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


open class CliftonTestExtension(val project: Project) {
	/**
	 * The list of tasks included for code coverage. Leave empty to include all tasks.
	 */
	val testCoverageIncludedTasks = project.objects.listProperty(String::class)
			.apply { finalizeValueOnRead() }

	/**
	 * The list of tasks excluded for code coverage.
	 */
	val testCoverageExcludedTasks = project.objects.listProperty(String::class)
			.convention(listOf(CliftonTestPlugin.INTEG_TEST_TASK_NAME))
			.apply { finalizeValueOnRead() }
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * Reports test executions for which no matching tests exist. This is similar to `NoMatchingTestsReporter` (removed as of v6.8.0;
 * [commit](https://github.com/gradle/gradle/commit/5aca1dc8e4cdafb17ee7cf9cabe680c7ba93c1e6#diff-76b9e40a9015084dbf360a33febba172a33c92e3de1b08197caf42481a819dc3)) but only logs a
 * warning rather than failing execution when no matching tests were found. This is intended to inform the user when unexpected behavior may occur.
 */
private class NoMatchingTestsLoggingReporter(private val task: Task, private val message: String) : TestListener {

	override fun beforeSuite(suite: TestDescriptor?) {
		/* Do nothing */
	}

	override fun afterSuite(suite: TestDescriptor, result: TestResult) {
		if (suite.parent == null && result.testCount == 0L && result.resultType != TestResult.ResultType.FAILURE) {
			task.logger.warn(message)
		}
	}

	override fun beforeTest(testDescriptor: TestDescriptor?) {
		/* Do nothing */
	}

	override fun afterTest(testDescriptor: TestDescriptor?, result: TestResult?) {
		/* Do nothing */
	}
}
