@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.artifact

import com.clifton.gradle.create
import com.clifton.gradle.plugin.build.CliftonJavaPlugin
import com.clifton.gradle.plugin.build.CliftonSpringBootExtension
import com.clifton.gradle.plugin.buildSrc
import com.clifton.gradle.plugin.util.CliftonProjectConfiguration
import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.ExtensionProperties
import com.clifton.gradle.util.requireRootProject
import com.clifton.gradle.util.tokenize
import org.apache.tools.ant.filters.ConcatFilter
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.distribution.DistributionContainer
import org.gradle.api.plugins.ApplicationPlugin
import org.gradle.api.plugins.ApplicationPluginConvention
import org.gradle.api.tasks.*
import org.gradle.api.tasks.application.CreateStartScripts
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.bundling.Zip
import org.gradle.kotlin.dsl.*
import org.gradle.language.jvm.tasks.ProcessResources
import java.io.File
import java.util.jar.Attributes


/**
 * The Gradle [Plugin] for configuring service-specific tasks and configurations. Projects are typically considered "service" projects based on the
 * [com.clifton.gradle.plugin.util.CliftonProjectConfiguration.isServiceProject] property.
 */
class CliftonServicePlugin : Plugin<Project> {

	companion object {

		const val EXTERNAL_RESOURCES_CONFIGURATION = "externalResources"
		const val UNIX_SERVICE_CONFIGURATION = "unixService"
		const val WINDOWS_WRAPPER_CONFIGURATION = "windowsWrapper"

		const val PROCESS_EXTERNAL_RESOURCES_TASK_NAME = "processExternalResources"

		// Special project properties used for service artifact generation
		const val JAVA_AGENT_PROPERTY = "service.java.agent"
		const val JAVA_OPTS_PROPERTY = "service.java.opts"
		const val UNIX_RUNNER_CLASS_PROPERTY = "service.unix.runner.class"
		const val WINDOWS_RUNNER_CLASS_PROPERTY = "service.windows.runner.class"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyPerProjectFeatures(project: Project) {
		// Guard-clause: Only apply features to appropriate projects
		if (!project.projectConfig.isServiceProject && !project.projectConfig.isSpringBootProject) {
			return
		}
		applySharedProjectFeatures(project)
		if (project.projectConfig.isSpringBootProject) {
			applySpringBootProjectConfiguration(project)
		}
		else if (project.projectConfig.isServiceProject) {
			applyServiceProjectConfiguration(project)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applySharedProjectFeatures(project: Project) {
		// Apply the application plugin for distribution tasks
		project.pluginManager.apply(ApplicationPlugin::class)

		// Mark the external resources directory as a source directory for tooling purposes
		project.configure<SourceSetContainer> {
			named("main") {
				java {
					srcDir(project.file(project.buildConfig.externalResourcesDirectoryName))
				}
			}
		}

		project.configurations {
			register(EXTERNAL_RESOURCES_CONFIGURATION)
			register(UNIX_SERVICE_CONFIGURATION)
			register(WINDOWS_WRAPPER_CONFIGURATION)
			named("runtimeOnly") { extendsFrom(project.configurations[CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION]) }
		}

		project.dependencies {
			UNIX_SERVICE_CONFIGURATION(project.buildSrc.resources("service/unix/**"))
			WINDOWS_WRAPPER_CONFIGURATION("winrun4j:service:0.4.5@exe")
			WINDOWS_WRAPPER_CONFIGURATION(project.buildSrc.resources("service/windows/**"))
		}

		val cliftonService = registerServiceExtension(project)
		project.tasks.register(PROCESS_EXTERNAL_RESOURCES_TASK_NAME, ProcessResources::class) {
			description = "Processes all supplementary resources which should be made accessible on the filesystem, such as scripts and configuration files."

			// Copy processed static resources to target directory
			into(project.file("${project.buildDir}/externalResources"))
			from(project.files(project.buildConfig.externalResourcesDirectoryName))
			from(project.configurations.named(EXTERNAL_RESOURCES_CONFIGURATION))

			// Additional resources generated based on project properties; only generate when properties are activated
			if (project.buildConfig.isResourceFilteringEnabled) {
				// Add scripts for Unix execution
				if (cliftonService.buildUnixService) {
					from(project.configurations.named(UNIX_SERVICE_CONFIGURATION)) {
						// Consume special properties
						project.projectConfig.applyPropertyReplacements(this, this@register, cliftonService.unixServiceProperties.get().asMap())
					}
				}

				// Add WinRun4J wrapper for Windows execution
				if (cliftonService.buildWindowsWrapper) {
					// Include Windows resources
					from(project.configurations.named(WINDOWS_WRAPPER_CONFIGURATION)) {
						exclude("service.ini")
						rename("""service-[.0-9]+\.exe""", "${project.name}.exe")
					}
					// Note: The .ini file must be handled in distinct CopySpec in order to appropriately segregate filter actions (https://github.com/gradle/gradle/issues/11257)
					inputs.property("Windows wrapper properties", cliftonService.windowsWrapperProperties.map { it.asMap() })
					from(project.configurations.named(WINDOWS_WRAPPER_CONFIGURATION)) {
						include("service.ini")
						rename(""".*""", "${project.name}.ini")
						// Append custom properties
						val wrapperPropertiesFile = project.resources.text.fromString(cliftonService.windowsWrapperProperties.get().asMap()
								.map { (key, value) -> "$key=$value" }
								.sorted()
								.joinToString("\n"))
								.asFile()
						filter(ConcatFilter::class, "append" to wrapperPropertiesFile)
					}
				}
			}
		}

		// Copy application artifact to location for deployment - package will have project prefix within directory
		project.tasks.register("copyServiceDistribution", Copy::class) {
			dependsOn(cliftonService.zipDistributionTask)
			from("${project.buildDir}/distributions") {
				include("${project.name}.zip")
			}
			into(project.file("${project.buildDir}/install").apply { mkdirs() })
		}

		// Default - Repackage the application artifact without the project name prefix for deployment
		// installDist extracts the distZip distribution into project/build/install so we can package without the prefix
		project.tasks.register("zipServiceDistribution", Zip::class) {
			dependsOn(cliftonService.zipDistributionTask, cliftonService.installDistributionTask)
			destinationDirectory.set(project.file("${project.buildDir}/install"))
			from("${project.buildDir}/install/${project.name}")
		}

		if (project.buildConfig.buildCompressedDistribution) {
			project.tasks.named("build") {
				dependsOn(if (project.buildConfig.buildProjectPrefixedDistribution) "copyServiceDistribution" else "zipServiceDistribution")
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyServiceProjectConfiguration(project: Project) {
		// Apply extension conventions
		val cliftonService = project.the<CliftonServiceExtension>()
		val buildProperties = project.projectConfig.buildProperties
		cliftonService.mainClass.convention(
				buildProperties.getProperty(UNIX_RUNNER_CLASS_PROPERTY)
						.orElse(buildProperties.getProperty(WINDOWS_RUNNER_CLASS_PROPERTY))
						.orElse(project.provider { error("The main class could not be derived. Either specify a main class manually in the ${CliftonServiceExtension::class.simpleName} extension or use the \"${UNIX_RUNNER_CLASS_PROPERTY}\" or \"${WINDOWS_RUNNER_CLASS_PROPERTY}\" properties.") })
		)
		cliftonService.javaAgent.convention(buildProperties.getProperty(JAVA_AGENT_PROPERTY).map { project.buildDir.resolve(it) }
				.orElse(project.provider { project.configurations.named(CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION) }.flatMap { it }.map { it.singleFile }))
		cliftonService.windowsWrapperProperties {
			setConvention("classpath.1", project.tasks.named("serviceLauncherJar").map { "lib\\${it.outputs.files.singleFile.name}" })
		}
		cliftonService.zipDistributionTask.convention(project.tasks.named("distZip", Zip::class))
		cliftonService.installDistributionTask.convention(project.tasks.named("installDist", Sync::class))

		project.tasks.register("serviceLauncherJar", Jar::class) {
			description = "Creates a launcher JAR which uses a manifest class path, intended especially for environments in which the class path is too long for command-line execution."
			inputs.files(project.configurations.named("runtimeClasspath"))
					.withNormalizer(ClasspathNormalizer::class)
					.withPropertyName("Runtime class path")
			archiveAppendix.set("launcher")
			doFirst {
				manifest {
					val runtimeClasspathConfig = project.configurations["runtimeClasspath"]
					val classpathJarFiles = runtimeClasspathConfig.allArtifacts.files + runtimeClasspathConfig.files
					val relativeClasspath = classpathJarFiles.joinToString(" ") { it.name }
					attributes(Attributes.Name.CLASS_PATH.toString() to relativeClasspath)
				}
			}
		}

		project.tasks.named("startScripts", CreateStartScripts::class) {
			// Replace full class path with manifest class path
			classpath = project.files(project.tasks.getByName("serviceLauncherJar", Jar::class).archiveFile.get().asFile)
		}

		project.afterEvaluate { // Delay property resolution
			project.configure<ApplicationPluginConvention> {
				mainClassName = cliftonService.mainClass.get()
				// Include additional distribution assets
				applicationDistribution.from(project.tasks.named(PROCESS_EXTERNAL_RESOURCES_TASK_NAME))
				applicationDistribution.from(project.tasks.named("serviceLauncherJar")) {
					into("lib")
				}
			}


			project.tasks.named("run", JavaExec::class) {
				// Include the Spring Instrument Java agent
				jvmArgs(cliftonService.javaAgent.map { "-javaagent:$it" }.get())
				jvmArgs(cliftonService.javaOpts.get())
			}
		}
	}


	private fun applySpringBootProjectConfiguration(project: Project) {
		// Apply extension conventions
		val cliftonService = project.the<CliftonServiceExtension>()
		val buildProperties = project.projectConfig.buildProperties
		cliftonService.mainClass.convention(project.provider { project.the<CliftonSpringBootExtension>() }.flatMap { it.mainClass })
		cliftonService.javaAgent.convention(buildProperties.getProperty(JAVA_AGENT_PROPERTY).map { project.buildDir.resolve(it) }
				.orElse(project.tasks.named("bootJar").map { it.outputs.files.singleFile }))
		cliftonService.windowsWrapperProperties {
			setConvention("classpath.1", project.tasks.named("bootJar").map { "lib\\${it.outputs.files.singleFile.name}" })
			setConvention("arg.1", cliftonService.mainClass)
		}
		cliftonService.zipDistributionTask.convention(project.tasks.named("bootDistZip", Zip::class))
		cliftonService.installDistributionTask.convention(project.tasks.named("installBootDist", Sync::class))

		// Effectively disable main distribution
		val mainDistTaskNames = arrayOf(
				ApplicationPlugin.TASK_RUN_NAME,
				ApplicationPlugin.TASK_START_SCRIPTS_NAME,
				ApplicationPlugin.TASK_DIST_TAR_NAME,
				ApplicationPlugin.TASK_DIST_ZIP_NAME
		)
		mainDistTaskNames.forEach { project.tasks.named(it) { enabled = false } }

		// Modify distribution contents
		project.configure<DistributionContainer> {
			named("boot") {
				distributionBaseName.set(project.name) // Use main distribution name
				contents {
					from(project.tasks.named(PROCESS_EXTERNAL_RESOURCES_TASK_NAME))
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun registerServiceExtension(project: Project): CliftonServiceExtension {
		val cliftonService = CliftonServiceExtension.create(project)
		// Apply conventions
		cliftonService.javaOpts.convention(project.projectConfig.buildProperties.getProperty(JAVA_OPTS_PROPERTY).map { it.tokenize(";") }.orElse(emptyList()))
		cliftonService.unixServiceProperties {
			setConvention(JAVA_AGENT_PROPERTY, cliftonService.javaAgent.map { "lib/${it.name}" })
			setConvention(JAVA_OPTS_PROPERTY, cliftonService.javaOpts.map { it.joinToString(" ") })
		}
		cliftonService.windowsWrapperProperties {
			val javaAgentOpt = cliftonService.javaAgent.map { "-javaagent:lib\\${it.name}" }.get()
			val javaArgs = (listOf(javaAgentOpt) + cliftonService.javaOpts.get())
			for ((index, arg) in javaArgs.withIndex()) {
				setConvention("vmarg.${index + 1}", arg)
			}
		}
		return cliftonService
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customizing service features. This extension provides properties for configuring service artifacts and
 * deployments.
 */
open class CliftonServiceExtension(project: Project) {

	companion object {

		/**
		 * The property prefix for additional [Windows wrapper][buildWindowsWrapper] properties. All [project properties][CliftonProjectConfiguration] prefixed with this string
		 * will be appended to the WinRun4J wrapper configuration file. See http://winrun4j.sourceforge.net/ for recognized properties.
		 */
		const val SERVICE_WRAPPER_PROPERTY_PREFIX = "service.windows.wrapper."

		fun create(project: Project): CliftonServiceExtension {
			return project.extensions.create("cliftonService", CliftonServiceExtension::class, project)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The entry point class used for distributions. Distribution scripts and execution tasks (e.g., `run`) will use this class as the entry point class during execution.
	 */
	val mainClass = project.objects.property<String>().apply { finalizeValueOnRead() }

	/**
	 * The Java Agent to use during execution.
	 */
	val javaAgent = project.objects.property<File>().apply { finalizeValueOnRead() }

	/**
	 * The list of additional Java command line Java options to use during execution.
	 */
	val javaOpts = project.objects.listProperty<String>().apply { finalizeValueOnRead() }

	/**
	 * If `true`, Windows-specific content will be added to project distributions. This uses [WinRun4J][http://winrun4j.sourceforge.net/] to generate a Windows- compatible wrapper.
	 * WinRun4J provides several benefits, such as automatic log management and a distinct process executable.
	 */
	var buildWindowsWrapper = false

	/**
	 * If `true`, Unix-specific content will be added to project distributions. This includes content such as service-generation scripts and log-management resources.
	 */
	var buildUnixService = true

	// Selected distribution tasks to be used by downstream ("build") tasks
	var zipDistributionTask = project.objects.property<Zip>().apply { finalizeValueOnRead() }
	var installDistributionTask = project.objects.property<Sync>().apply { finalizeValueOnRead() }

	/**
	 * A set of additional properties to be used during [property replacements][CliftonProjectConfiguration.applyPropertyReplacements] for Unix service files.
	 */
	internal val unixServiceProperties = project.provider {
		ExtensionProperties.create<String>(project).also { properties -> unixServicePropertiesActions.forEach { properties.it() } }
	}

	/**
	 * A set of additional properties to be added to the WinRun4J configuration file.
	 * @see [SERVICE_WRAPPER_PROPERTY_PREFIX]
	 */
	internal val windowsWrapperProperties = project.provider {
		ExtensionProperties.create<String>(project).also { properties -> windowsWrapperPropertiesActions.forEach { properties.it() } }
	}

	private val unixServicePropertiesActions = mutableListOf<ExtensionProperties<String>.() -> Unit>()
	private val windowsWrapperPropertiesActions = mutableListOf<ExtensionProperties<String>.() -> Unit>()


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	init {
		// Pull properties from build configuration
		windowsWrapperProperties {
			val properties = this
			project.projectConfig.buildProperties.getPropertyMap()
					.filterKeys { it.startsWith(SERVICE_WRAPPER_PROPERTY_PREFIX) }
					.mapKeys { (key, _) -> key.removePrefix(SERVICE_WRAPPER_PROPERTY_PREFIX) }
					.forEach { (key, value) -> properties[key] = value }
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	fun unixServiceProperties(action: ExtensionProperties<String>.() -> Unit) {
		unixServicePropertiesActions.add(action)
	}


	fun windowsWrapperProperties(action: ExtensionProperties<String>.() -> Unit) {
		windowsWrapperPropertiesActions.add(action)
	}
}
