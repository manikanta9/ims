@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.build

import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.getOptional
import com.clifton.gradle.util.requestedTasks
import com.clifton.gradle.util.requireRootProject
import com.clifton.gradle.util.tokenize
import com.github.gradle.node.NodeExtension
import com.github.gradle.node.NodePlugin
import com.github.gradle.node.npm.task.NpmSetupTask
import com.github.gradle.node.npm.task.NpmTask
import com.github.gradle.node.task.NodeSetupTask
import com.github.gradle.node.task.NodeTask
import com.github.gradle.node.yarn.task.YarnSetupTask
import com.github.gradle.node.yarn.task.YarnTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.ExtraPropertiesExtension
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.PathSensitivity
import org.gradle.api.tasks.TaskProvider
import org.gradle.api.tasks.bundling.Zip
import org.gradle.api.tasks.testing.Test
import org.gradle.kotlin.dsl.*
import org.gradle.language.base.plugins.LifecycleBasePlugin


/**
 * The Gradle [Plugin] for applying Node features to projects. These features are used for running Node-based compilation and artifact publishing.
 */
class CliftonNodePlugin : Plugin<Project> {

	companion object {

		const val JS_NODE_CONFIGURATION = "jsNode"
		const val CLEAN_NODE_TASK_NAME = "cleanNode"
		const val NODE_ENVIRONMENTS_PROPERTY_NAME = "node.environments"
		const val NODE_DEPLOY_DIR_PROPERTY_NAME = "node.deploy.dir"

		/**
		 * The property which, if set to `true` within a [Task]'s [ExtraPropertiesExtension], indicates that the associated task is a Node-related task. If any such task is
		 * requested, then the [CliftonNodeExtension] execution avoidance measures will be disabled and all other Node-related tasks will be allowed to run.
		 *
		 * @see [CliftonNodeExtension.buildNodeArtifacts]
		 */
		val NODE_TASK_PROPERTY = "${CliftonNodePlugin::class.simpleName}_nodeTask"
		val NODE_TASK_TYPE_LIST = listOf(NpmSetupTask::class, NpmTask::class, NodeTask::class, NodeSetupTask::class, YarnTask::class)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		registerNodeExtension(rootProject)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyPerProjectFeatures(project: Project) {
		// Apply configuration to all projects to allow transitive dependencies
		project.configurations {
			register(JS_NODE_CONFIGURATION)
		}

		// Guard-clause: Only apply features to appropriate projects
		if (!project.file("${project.projectDir}/package.json").exists()) {
			return
		}

		project.pluginManager.apply(NodePlugin::class)

		// Configure the Node plugin
		val cliftonNode = registerNodeExtension(project)
		project.configure<NodeExtension> {
			version.set("9.10.1")
			// npmVersion = "5.0.3"
			yarnVersion.set("1.5.1")
			distBaseUrl.set("${project.buildConfig.repository.host}/node-js/")
			download.set(true)
		}

		// Force npm to use private registry when downloading the package manager
		project.tasks.named("yarnSetup", YarnSetupTask::class) {
			args.set(listOf("--registry", "${project.buildConfig.repository.host}/npm/"))
		}

		project.tasks.register(CLEAN_NODE_TASK_NAME, Delete::class) {
			description = "Delete all Node.js build artifacts and assets, including build assets such as node_modules dependencies and any downloaded build tools."
			group = "node"
			val node = project.the<NodeExtension>()
			delete(node.workDir, node.npmWorkDir, node.yarnWorkDir, node.nodeProjectDir.map { "${it}/node_modules" })
		}
		if (project.buildConfig.isCiServer) {
			project.tasks.named(LifecycleBasePlugin.CLEAN_TASK_NAME) { dependsOn(CLEAN_NODE_TASK_NAME) }
		}

		// For each configured environment to be built, configure the appropriate gradle tasks
		val environmentsStr = project.projectConfig.buildProperties[NODE_ENVIRONMENTS_PROPERTY_NAME]
		val nodeEnvironments = environmentsStr.tokenize()
		nodeEnvironments.forEach {
			val nodeTasks = registerNodeTasks(project, it)
			// Create a new configuration and artifact for each set of tasks so that the desired artifact can be built and retrieved independently
			val environmentConfigurationName = "${JS_NODE_CONFIGURATION}-$it"
			project.configurations.register(environmentConfigurationName)
			project.artifacts.add(environmentConfigurationName, nodeTasks.zipNodeTask)
		}

		project.tasks.register("testNode", YarnTask::class) {
			dependsOn("yarn")
			description = "Runs the Node project tests using the npm script \"test\"."
			group = "node"
			mustRunAfter(CLEAN_NODE_TASK_NAME)
			inputs.files(project.fileTree("src/node"))
					.withPathSensitivity(PathSensitivity.RELATIVE)
			args.set(listOf("run", "test"))
		}

		project.tasks.named("test", Test::class).configure { dependsOn("testNode") }

		project.tasks.withType(YarnTask::class).configureEach {
			onlyIf { project.file("${project.projectDir}/package.json").exists() }
			inputs.files(".angular-cli.json", "tsconfig.json", "package.json", "package-lock.json", "yarn.lock")
					.withPathSensitivity(PathSensitivity.RELATIVE)
		}

		// Enforce execution avoidance rules for Node tasks
		project.tasks.matching { task -> NODE_TASK_TYPE_LIST.any { it.isInstance(task) } }.configureEach { extra.set(NODE_TASK_PROPERTY, true) }
		project.tasks.matching { task -> task.extra.getOptional(NODE_TASK_PROPERTY) == true }.configureEach { onlyIf { cliftonNode.buildNodeArtifacts.get() } }
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun registerNodeExtension(project: Project): CliftonNodeExtension {
		return CliftonNodeExtension.create(project).apply {
			// By convention, only generate Node artifacts for CI builds or when at least one Node-related task is explicitly targeted
			buildNodeArtifacts.convention(
					project.provider {
						project.buildConfig.buildNodeArtifacts
								|| project.buildConfig.isCiServer
								|| project.requestedTasks.any { it.extra.getOptional(NODE_TASK_PROPERTY) == true }
					}
			)
		}
	}


	/**
	 * Registers the standard Node tasks for the given environment.
	 * <p>
	 * The standard tasks are <tt>cleanNode-${envName}</tt>, <tt>compileNode-${envName}</tt>, and
	 * <tt>zipNode-${envName}</tt>.
	 * <p>
	 * For each configured environment, the npm script <tt>build:${envName}</tt> must be present. This is the script which
	 * will be used to build the environment when <tt>compileNode-${envName}</tt> is called. It is also expected that the
	 * given build script will output its result to <tt>${project.buildDir}/node-${envName}</tt>. This is the directory
	 * that shall be cleaned during the <tt>cleanNode-${envName}</tt> task and zipped during the
	 * <tt>zipNode-${envName}</tt> task.
	 *
	 * @param project the project for which to create the tasks
	 * @param envName the environment name for which to create the tasks
	 * @return a tuple of the clean task, the compile task, and the zip task, in order
	 */
	private fun registerNodeTasks(project: Project, envName: String): NodeTasks {
		val compileNodeTask = project.tasks.register("compileNode-$envName", YarnTask::class) {
			dependsOn("yarn")
			description = "Compiles the Node project for environment $envName."
			group = "node"
			extra.set(NODE_TASK_PROPERTY, true)
			inputs.files(project.fileTree("src/node"))
					.withPathSensitivity(PathSensitivity.RELATIVE)
			outputs.dir("${project.buildDir}/node-$envName")
			outputs.cacheIf { true }
			args.set(listOf("run", "build:$envName"))
		}
		val zipNodeTask = project.tasks.register("zipNode-$envName", Zip::class) {
			description = "Produce compiled Node artifact."
			group = "node"
			extra.set(NODE_TASK_PROPERTY, true)
			from(compileNodeTask)
			archiveFileName.set("$name-node-js-$envName.zip")
			onlyIf { project.the<CliftonNodeExtension>().buildNodeArtifacts.get() }
		}
		val cleanNodeTask = project.tasks.register("cleanNode-$envName", Delete::class) {
			description = "Cleans the Node project for environment $envName."
			group = "node"
			delete(
					compileNodeTask.map { it.outputs.files },
					zipNodeTask.map { it.outputs.files }
			)
		}
		project.tasks.named(LifecycleBasePlugin.CLEAN_TASK_NAME) { dependsOn(cleanNodeTask) }
		project.tasks.named(CLEAN_NODE_TASK_NAME) { dependsOn(cleanNodeTask) }
		compileNodeTask.configure { mustRunAfter(cleanNodeTask, CLEAN_NODE_TASK_NAME) }
		return NodeTasks(compileNodeTask, zipNodeTask, cleanNodeTask)
	}


	data class NodeTasks(
		val compileNodeTask: TaskProvider<out Task>,
		val zipNodeTask: TaskProvider<out Task>,
		val cleanNodeTask: TaskProvider<out Task>
	)
}


////////////////////////////////////////////////////////////////////////////
////////            Supplementary Types                             ////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized Node.js build configuration. This extension provides properties for configuring Node artifacts and
 * builds.
 */
open class CliftonNodeExtension(project: Project) {

	companion object {

		fun create(project: Project): CliftonNodeExtension {
			return project.extensions.create("cliftonNode", CliftonNodeExtension::class, project)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If `true`, then the Node-related tasks will be executed. Otherwise, these tasks will be skipped. This is used as a work-avoidance flag, since Node-related build steps are
	 * not required during development in most cases.
	 */
	val buildNodeArtifacts = project.objects.property<Boolean>().apply { finalizeValueOnRead() }
}
