@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.build

import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Action
import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.plugins.JavaBasePlugin
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.JavaCompile
import org.gradle.internal.deprecation.DeprecatableConfiguration
import org.gradle.kotlin.dsl.*
import org.gradle.language.jvm.tasks.ProcessResources
import org.gradle.process.JavaForkOptions


/**
 * The Gradle [Plugin] for Java projects. This plugin imports and configures the [Java Plugin][https://docs.gradle.org/current/userguide/java_plugin.html] and its tasks.
 */
class CliftonJavaPlugin : Plugin<Project> {

	companion object {

		const val ALL_CLASSES_TASK_NAME = "allClasses"
		const val ASSEMBLE_ALL_TASK_NAME = "assembleAll"
		const val BUILD_VARIANTS_TASK_NAME = "buildVariants"

		const val SPRING_INSTRUMENT_CONFIGURATION = "springInstrument"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.subprojects {
			applyJavaFeatures(this)
			applyDependencyManagementFeatures(this)
		}
	}


	private fun applyJavaFeatures(project: Project) {
		project.pluginManager.apply(JavaPlugin::class)

		// Add configurable extension
		project.extensions.create("cliftonJava", CliftonJavaExtension::class, project)

		project.java {
			sourceCompatibility = JavaVersion.VERSION_1_8
			targetCompatibility = JavaVersion.VERSION_1_8
		}

		project.configurations {
			register(SPRING_INSTRUMENT_CONFIGURATION)
		}

		project.dependencies {
			SPRING_INSTRUMENT_CONFIGURATION("org.springframework:spring-instrument")
		}

		project.sourceSets {
			named("main") {
				java {
					/*
					 * Mark web resources as "Java" sources rather than "resources"; this prevents web resources from being included in JARs and allows them to be manually included
					 * in specific paths for WAR distributions. If and when it is decided that we will serve static resources from the class path (e.g., via Spring Boot, or in the
					 * same way that webjars are served) rather than the WAR root (via the default servlet), then we may want to change this to "resources" so that they are made
					 * available on the class path.
					 */
					srcDir(project.buildConfig.webDirectoryName)
				}
			}
		}

		// Exclude third-party dependency source files from being included in build distributions
		project.tasks.withType(JavaCompile::class).configureEach {
			options.sourcepath = project.files() // avoid user classpath scanning for classes to compile
			options.compilerArgs.addAll(listOf(
					"-parameters", // store method parameter names in compiled classes
					"-Xlint:deprecation" // show deprecation use details during compile
			))
			options.isIncremental = project.buildConfig.useIncrementalBuild // optimizes the gradle compile to only recompile modified files
		}

		project.tasks.withType(ProcessResources::class).configureEach {
			// Configure resource filtering for property-placeholder replacements
			if (project.buildConfig.isResourceFilteringEnabled && project.projectConfig.isStandardPropertyReplacementsEnabled()) {
				outputs.cacheIf { true } // Enable caching due to the cost of filtering resources
				outputs.doNotCacheIfSpecs.removeIf { spec -> spec.displayName == "Has custom actions" } // Disable do-not-cache spec for custom actions to allow caching
				project.projectConfig.applyPropertyReplacements(this)
			}
		}

		// Support configurable debug targets during CI builds
		project.tasks.matching { it is JavaForkOptions && it.path in project.buildConfig.debugTasks }.configureEach {
			this as JavaForkOptions
			debug = true
		}

		// Setup comprehensive build tasks
		project.tasks.register(ALL_CLASSES_TASK_NAME) {
			description = "Assembles classes for all registered feature variants and source sets for the project."
			dependsOn(project.sourceSets.map { it.classesTaskName })
		}

		project.tasks.register(ASSEMBLE_ALL_TASK_NAME) {
			/*
			 * This task works around an apparent bug in org.gradle.api.plugins.BasePlugin.configureConfigurations in which the selected artifacts are oftentimes queries too early,
			 * preventing them from being included in the defaultArtifacts artifact set and thus from being included in the assemble task dependencies.
			 */
			description = "Assembles all output artifacts for the current project. This differs from the default \"assemble\" task such that it includes artifacts from all non-standard configurations."
			dependsOn(project.configurations.flatMap { it.artifacts }.distinct())
		}

		project.tasks.register(BUILD_VARIANTS_TASK_NAME) {
			description = "Builds all registered feature variants and source sets for the project."
			dependsOn(ASSEMBLE_ALL_TASK_NAME)
			dependsOn(JavaBasePlugin.BUILD_TASK_NAME)
		}
	}


	private fun applyDependencyManagementFeatures(project: Project) {
		// Apply registered BOMs to all configurations
		val javaExtension = project.the<CliftonJavaExtension>()
		project.configurations
				.matching { (it as? DeprecatableConfiguration)?.declarationAlternatives?.isEmpty() ?: true } // Skip configurations not intended for declaration
				.configureEach {
					withDependencies {
						if (isNotEmpty()) {
							javaExtension.bomList.get().forEach { add(project.dependencies.platform(it)) }
						}
					}
				}

		project.configurations.all {
			// Disallow dynamic versions, since these can change at any time and can difficult-to-diagnose issues
			resolutionStrategy.failOnNonReproducibleResolution()
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////            Extension Functions                             ////////
////////////////////////////////////////////////////////////////////////////


/* Extension functions intended for use within the buildSrc project (where generated accessors do not yet exist) */

@Suppress("unused")
private fun Project.java(action: Action<JavaPluginExtension>) = action.execute(the())

@Suppress("unused")
private val Project.java: JavaPluginExtension
	get() = the()

@Suppress("unused")
private fun Project.sourceSets(action: Action<SourceSetContainer>) = action.execute(the())

@Suppress("unused")
private val Project.sourceSets: SourceSetContainer
	get() = the()


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized Java configuration. This extension provides properties for use with Java projects.
 */
open class CliftonJavaExtension(project: Project) {

	/**
	 * The list of BOMs to be applied to all declaration-compatible project [configurations][Configuration] which have one or more declared dependencies.
	 */
	val bomList = project.objects.listProperty<String>().apply { finalizeValueOnRead() }
}
