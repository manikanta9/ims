package com.clifton.gradle.plugin.util

import com.clifton.gradle.util.execute
import com.clifton.gradle.util.executeSimple
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Plugin
import org.gradle.api.Project


/**
 * The Gradle [Plugin] for registering git utility scripts.
 */
class CliftonGitPlugin : Plugin<Project> {

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.tasks.register("cleanMergedLocalBranches") {
			description = "Deletes all local branches which have been merged into the upstream origin/master."
			doLast {
				// Find merged local branches
				rootProject.executeSimple("git fetch --prune")
				val branchList = rootProject.executeSimple("git branch --merged origin/master").split("\n").map { it.trim() }

				// Filter to applicable branches
				val curBranch = rootProject.executeSimple("git rev-parse --abbrev-ref HEAD")
				val filteredBranchList = branchList
						.map { it.trim() }
						.filter { !it.matches("^(master|release)(/.+)?$".toRegex()) } // Skip master and release branches
						.filter { !it.matches("^[+*].*".toRegex()) } // Skip currently-checked out branches (worktree branches are prefixed with "+" as of git 2.23.0)
						.filter { it != curBranch }

				// Delete merged branches
				if (filteredBranchList.isNotEmpty()) {
					try {
						rootProject.execute(
								"git", "branch", "-D", *filteredBranchList.toTypedArray(),
								outputStream = System.out
						)
					}
					catch (e: Exception) {
						// Often, these errors will be insignificant, such as failures due to qualifying branches checked out in one or more worktrees
						rootProject.logger.info("An error occurred during branch deletion.", e)
					}
				}
			}
		}
	}
}
