package com.clifton.gradle.plugin.util

import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlatformPlugin
import org.gradle.kotlin.dsl.apply
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.invoke
import org.gradle.plugins.ide.idea.IdeaPlugin
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.springframework.boot.gradle.plugin.SpringBootPlugin


/**
 * The Gradle [Plugin] for applying IntelliJ IDE features for Gradle. This applies the [IDEA Plugin][https://docs.gradle.org/current/userguide/idea_plugin.html] and configures
 * properties which are used by the IDE during Gradle project import. These customizations enable the IDE to seamlessly integrate with Gradle configurations as needed.
 */
class CliftonIntelliJPlugin : Plugin<Project> {

	companion object {

		const val LOCAL_KEYSTORE_CONFIGURATION = "localKeystore"
		const val LOCAL_SPRING_INSTRUMENT_CONFIGURATION = "localSpringInstrument"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		applyRootProjectFeatures(rootProject)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyRootProjectFeatures(project: Project) {
		// Allow the root project to consume and produce dependency platforms as needed, such as when consuming the Spring BOM
		project.pluginManager.apply(JavaPlatformPlugin::class)

		project.configurations {
			register(LOCAL_KEYSTORE_CONFIGURATION)
			register(LOCAL_SPRING_INSTRUMENT_CONFIGURATION)
		}

		project.dependencies {
			LOCAL_KEYSTORE_CONFIGURATION("com.clifton:certificates:${project.buildConfig.keystoreVersion}@KEYSTORE")
			LOCAL_SPRING_INSTRUMENT_CONFIGURATION(platform(SpringBootPlugin.BOM_COORDINATES))
			LOCAL_SPRING_INSTRUMENT_CONFIGURATION("org.springframework:spring-instrument") // Version is pulled from Spring BOM
		}

		project.tasks.register("setupCertificateKeystore") {
			description = "Downloads and deploys the SSL certificate keystore. This is used for local machines only. This is necessary for Tomcat SSL configurations, which require a filesystem reference to the keystore."
			// Use doLast with manual copy task to prevent input/output snapshotting
			doLast {
				project.copy {
					from(project.configurations.named(LOCAL_KEYSTORE_CONFIGURATION))
					into("${project.rootDir}/..")
					rename(".*", project.buildConfig.keystoreFileName)
				}
			}
		}

		project.tasks.register("setupSpringInstrumentJar") {
			description = "Downloads and deploys the Spring Instrument JAR locally. This is used for local machines only. This is necessary for a small number of cases, such as for Spring Boot application IntelliJ run configurations, which require an explicit filesystem reference to the instrument JAR to run as a Java agent."
			// Use doLast with manual copy task to prevent input/output snapshotting
			doLast {
				project.copy {
					from(project.configurations.named(LOCAL_SPRING_INSTRUMENT_CONFIGURATION))
					into("${project.rootDir}/..")
					rename(".*", project.buildConfig.springInstrumentFileName)
				}
			}
		}
	}


	private fun applyPerProjectFeatures(project: Project) {
		project.pluginManager.apply(IdeaPlugin::class)
		project.configure<IdeaModel> {
			module {
				// If you prefer different SDK than the one inherited from IDEA project
				jdkName = "1.8"
				isDownloadJavadoc = true
				isDownloadSources = true
				/*
				 * Note: Output directories ("outputDir" and "testOutputDir") may be set here in some circumstances to force IntelliJ to use different compilation output
				 * directories than Gradle. This can prevent conflicts that can occur when both applications (IntelliJ and Gradle) attempt to hold locks on these files
				 * simultaneously. These conflicts are greatly reduced by disabling the IntelliJ "Build project automatically" compilation setting, but some users may wish to keep
				 * this setting enabled.
				 *
				 * Leaving these properties as their defaults causes IntelliJ to share the directories which Gradle designates for any source set.
				 *
				 * See the following for more information:
				 * - https://github.com/gradle/gradle/issues/2315
				 * - https://youtrack.jetbrains.com/issue/IDEA-175172 (primary ticket for issue)
				 * - https://youtrack.jetbrains.com/issue/IDEA-189063 (suggested fix for issue)
				 */
			}
		}
	}
}
