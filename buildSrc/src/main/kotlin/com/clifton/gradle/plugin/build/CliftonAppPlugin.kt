@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.build

import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileCopyDetails
import org.gradle.api.plugins.ApplicationPlugin
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.tasks.*
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.bundling.Zip
import org.gradle.kotlin.dsl.*
import org.springframework.boot.gradle.dsl.SpringBootExtension
import org.springframework.boot.gradle.plugin.SpringBootPlugin
import org.springframework.boot.gradle.tasks.bundling.BootJar
import org.springframework.boot.gradle.tasks.run.BootRun
import java.io.File


/**
 * The Gradle [Plugin] for applying features to "application" projects. Projects are considered "application" projects based on the
 * [com.clifton.gradle.plugin.util.CliftonProjectConfiguration.isAppProject] property.
 *
 * Application projects have special configurations for tasks relating to builds, JS, Node, and packaging.
 */
class CliftonAppPlugin : Plugin<Project> {

	companion object {

		const val COPY_NODE_TASK_NAME = "copyNode"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.pluginManager.apply(CliftonJavaScriptPlugin::class)
		rootProject.pluginManager.apply(CliftonNodePlugin::class)
		rootProject.subprojects { applyPerProjectFeatures(this) }
	}


	private fun applyPerProjectFeatures(project: Project) {
		// Guard-clause: Only apply features to appropriate projects
		if (!project.projectConfig.isAppProject) {
			return
		}

		if (project.projectConfig.isSpringBootProject) {
			applySpringBootApplicationFeatures(project)
		}
		else {
			applyStandardApplicationFeatures(project)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyStandardApplicationFeatures(project: Project) {
		val sourceSets = project.the<SourceSetContainer>()
		val mainSourceSet = sourceSets[SourceSet.MAIN_SOURCE_SET_NAME]

		project.tasks.register("copyWeb", Copy::class) {
			description = "Copies all web resources to distribution directory. This typically includes assets such as static JavaScript, images, CSS, and HTML files. This may also include limited webapp resources such as JSP and web.xml files. This does not include compiled code and libraries."
			mustRunAfter("cleanWarDir")

			into("${project.buildDir}/dist/${project.name}")
			from(project.files(project.buildConfig.webDirectoryName))
			// Pull JavaScript dependencies, expanding all archives
			from(project.configurations.named(mainSourceSet.javascriptClasspathConfigurationName).mapFiles { project.expandArchive(it) })

			/*
			 * When running task directly, reduce cost by only copying files which have been modified. This is an attempt to work around inefficient copy behavior built into
			 * Gradle. The Ant copy task, alternatively, does this up-to-date checking automatically. However, the Ant copy task presents additional difficulties when attempting to
			 * select and work with Gradle dependencies in a fluid manner.
			 *
			 * Related links:
			 * - https://discuss.gradle.org/t/using-war-copyspec-for-ant-copy-task/23711/5
			 * - https://github.com/gradle/gradle/issues/1643#issuecomment-589864718
			 * - https://issues.gradle.org/browse/GRADLE-1264
			 */
			if (!project.buildConfig.isCiServer && name in project.requestedTaskNames) {
				// Since this references the source file, this must be declared before any property replacement filters, as these filters drop the source file reference
				eachFile {
					val targetFile = File(destinationDir, path)
					if (file.name.toLowerCase().let { filename -> project.buildConfig.fileTypesToFilter.none { filename.endsWith(it) } }
							&& lastModified == targetFile.lastModified()
							&& file.length() == targetFile.length()) {
						exclude()
					}
				}
			}

			// Retain timestamps from source files (see https://github.com/gradle/gradle/issues/1252#issuecomment-421744595)
			// This should take place after all exclusion filters in order to prevent unnecessary timestamp updates to unmodified files
			val copyDetails = mutableListOf<FileCopyDetails>()
			eachFile {
				copyDetails += this
				project.logger.info("Copying resource file from [{}] to [{}/{}].", file.path, destinationDir, path)
			}
			doLast {
				copyDetails.forEach { details ->
					File(destinationDir, details.path).takeIf { it.exists() }?.setLastModified(details.lastModified)
				}
			}

			if (project.buildConfig.isResourceFilteringEnabled) {
				project.projectConfig.applyPropertyReplacements(this)
			}
		}

		// Register Node artifact tasks
		val hasNodeArtifact = project.projectConfig.buildProperties.hasProperty(CliftonNodePlugin.NODE_DEPLOY_DIR_PROPERTY_NAME)
		if (hasNodeArtifact) {
			project.tasks.register(COPY_NODE_TASK_NAME, Copy::class) {
				description = "Copy node changes to \"${CliftonNodePlugin.NODE_DEPLOY_DIR_PROPERTY_NAME}\" within the dist directory."
				extra.set(CliftonNodePlugin.NODE_TASK_PROPERTY, true)
				onlyIf { project.buildConfig.isCiServer || project.requestedTaskNames.contains(name) }
				onlyIf { hasNodeArtifact }
				dependsOn(project.configurations.named(CliftonNodePlugin.JS_NODE_CONFIGURATION))
				mustRunAfter("cleanWarDir")
				from(project.configurations.named(CliftonNodePlugin.JS_NODE_CONFIGURATION).mapFiles { project.expandArchive(it) })
				into({ "${project.buildDir}/dist/${project.name}/${project.projectConfig.buildProperties.getOrNull(CliftonNodePlugin.NODE_DEPLOY_DIR_PROPERTY_NAME)}" })
				doFirst { cleanStaleOutputs() }
			}
		}

		project.tasks.register("syncWebInf", Sync::class) {
			dependsOn("compileJava")
			mustRunAfter("cleanWarDir")
			into("build/dist/${project.name}/WEB-INF")
			/*
			 * Follow the pattern used by org.gradle.api.tasks.bundling.War, which assumes that dependencies are all provided as artifacts (files) and current-project resources
			 * are all provided as directories.
			 */
			from(mainSourceSet.runtimeClasspath.filter { it.isFile }) { into("lib") }
			from(mainSourceSet.runtimeClasspath.filter { it.isDirectory }) { into("classes") }
			// Do not delete any existing web.xml, as this is typically migrated over alongside static assets
			preserve { include("web.xml") }
		}

		project.tasks.register("prepareWar") {
			dependsOn("copyWeb", "syncWebInf")
			if (hasNodeArtifact) {
				dependsOn(COPY_NODE_TASK_NAME)
			}
		}

		project.tasks.register("war", Zip::class) {
			dependsOn("prepareWar")
			from("${project.buildDir}/dist/${project.name}")
			archiveExtension.set("war")
		}

		project.tasks.register("cleanWarDir", Delete::class) {
			delete("${project.buildDir}/dist/${project.name}")
		}

		project.tasks.named("build") {
			dependsOn("prepareWar")
			if (project.buildConfig.buildCompressedDistribution) {
				dependsOn("war")
			}
		}
	}


	private fun applySpringBootApplicationFeatures(project: Project) {
		project.pluginManager.apply(ApplicationPlugin::class)
		project.pluginManager.apply(SpringBootPlugin::class)

		// Apply configurable extension
		project.extensions.add("cliftonSpringBoot", CliftonSpringBootExtension(project))
		project.afterEvaluate {
			project.configure<SpringBootExtension> {
				mainClassName = project.the<CliftonSpringBootExtension>().mainClass.get()
			}
		}

		project.dependencies {
			// Include keystore in development environments
			if (!project.buildConfig.isCiServer) {
				JavaPlugin.RUNTIME_ONLY_CONFIGURATION_NAME("com.clifton:certificates:${project.buildConfig.keystoreVersion}")
			}
		}

		// Fix primary application JAR not being provided for test compilation and execution (https://github.com/gradle/gradle/issues/11696#issuecomment-636916754)
		project.tasks.named<Jar>(JavaPlugin.JAR_TASK_NAME) {
			enabled = true
			archiveClassifier.set("library")
		}

		/*
		 * Include client-side resources for the main source set directly. This impacts actions such as the IntelliJ Spring Boot run configuration which attempt to reference class
		 * and resource files directly via the main source set, as well as any other tasks which include the main source set in their class paths (e.g., "bootRun").
		 */
		val mainSourceSet = project.the<SourceSetContainer>()[SourceSet.MAIN_SOURCE_SET_NAME]
		mainSourceSet.runtimeClasspath += project.configurations.named(mainSourceSet.javascriptClasspathConfigurationName).get()

		// Register Node artifact tasks
		val hasNodeArtifact = project.projectConfig.buildProperties.hasProperty(CliftonNodePlugin.NODE_DEPLOY_DIR_PROPERTY_NAME)
		if (hasNodeArtifact) {
			val copyNodeTask = project.tasks.register(COPY_NODE_TASK_NAME, Copy::class) {
				description = "Copy node changes to \"node.deploy.dir\" within the build directory."
				extra.set(CliftonNodePlugin.NODE_TASK_PROPERTY, true)
				onlyIf { project.projectConfig.buildProperties.hasProperty(CliftonNodePlugin.NODE_DEPLOY_DIR_PROPERTY_NAME) }
				dependsOn(project.configurations.named(CliftonNodePlugin.JS_NODE_CONFIGURATION))
				from(project.configurations.named(CliftonNodePlugin.JS_NODE_CONFIGURATION).mapFiles { project.expandArchive(it) })
				into(project.file("${project.buildDir}/node"))
				doFirst { cleanStaleOutputs() }
				eachFile { path = "META-INF/resources/${project.projectConfig.buildProperties.getOrNull(CliftonNodePlugin.NODE_DEPLOY_DIR_PROPERTY_NAME)}/$path" }
			}
			// Include Node.js client-side resources for the main source set directly
			mainSourceSet.resources.srcDir(copyNodeTask)
		}

		// Configure development execution
		project.tasks.named("bootRun", BootRun::class) {
			doFirst { classpath = project.files(project.classpathJar(classpath)) }
			jvmArgs("-javaagent:${project.configurations[CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION].singleFile.absolutePath}")
			// Include full class path when searching for static assets; allows assets to referenced directly on the class path without requiring a /META-INF/resources path prefix
			args("--spring.resources.static-locations=classpath:/,classpath:/META-INF/resources/,/")
		}

		// Configure artifact builds
		project.tasks.named("bootJar", BootJar::class) {
			// Exclude static assets from the artifact "classes" directory
			this.classpath -= project.configurations.named(mainSourceSet.javascriptClasspathConfigurationName).get()

			// Manually pull static assets from all dependencies to include in resource directory, expanding all archives
			val dependencyStaticAssets = project.configurations.named(mainSourceSet.javascriptClasspathConfigurationName).mapFiles { project.expandArchive(it) }
			from(dependencyStaticAssets) { into("META-INF/resources") }

			// Allow the archive to be executed as a Java Agent
			from(project.configurations.named(CliftonJavaPlugin.SPRING_INSTRUMENT_CONFIGURATION).map { project.zipTree(it.singleFile) }) { exclude("META-INF/**") }
			manifest { attributes["Premain-Class"] = "org.springframework.instrument.InstrumentationSavingAgent" }
		}

		// TODO: Delete me once build plans no longer require this task for other branches
		project.tasks.register("cleanWarDir") {
			// Do nothing; this is a placeholder to allow side-by-side builds on the same build plan for old branches and Spring Boot branches
			enabled = false
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized Spring Boot configuration. This extension provides properties for use with Spring Boot project tasks.
 */
open class CliftonSpringBootExtension(val project: Project) {

	/**
	 * The Spring Boot entry point class. This is the main class for the application, typically annotated with `@SpringBootApplication`. All builds and execution tasks will use
	 * this class to run the application.
	 */
	val mainClass = project.objects.property(String::class)
			.apply { finalizeValueOnRead() }
			.convention(project.provider {
				val projectNameSuffix = project.name.substringAfterLast("${project.buildConfig.companyName}-")
				val projectPackage = "${project.buildConfig.rootProjectGroup}.${projectNameSuffix.replace('-', '.')}.app"
				val projectClassName = "${projectNameSuffix.toPascalCase()}Application"
				"${projectPackage}.${projectClassName}"
			})
}
