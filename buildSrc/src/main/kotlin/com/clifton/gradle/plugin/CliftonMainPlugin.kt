package com.clifton.gradle.plugin

import com.clifton.gradle.plugin.artifact.CliftonArtifactPlugin
import com.clifton.gradle.plugin.artifact.CliftonSchemaPlugin
import com.clifton.gradle.plugin.artifact.CliftonServicePlugin
import com.clifton.gradle.plugin.build.*
import com.clifton.gradle.plugin.util.*
import com.clifton.gradle.plugin.verification.CliftonBenchmarkPlugin
import com.clifton.gradle.plugin.verification.CliftonInspectionsPlugin
import com.clifton.gradle.plugin.verification.CliftonTestPlugin
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.gradle.api.tasks.wrapper.Wrapper
import org.gradle.execution.taskgraph.TaskExecutionGraphInternal
import org.gradle.kotlin.dsl.apply
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.named
import org.gradle.kotlin.dsl.the
import java.io.File
import java.net.URLClassLoader


/**
 * The main Gradle [Plugin]. This plugin is used to apply shared root features in addition to applying each of the common root plugins.
 *
 * @author MikeH
 */
@Suppress("unused") // Class is registered as a plugin
class CliftonMainPlugin : Plugin<Project> {

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyPrePluginFeatures(rootProject)
		applyPlugins(rootProject)
		applyPostPluginFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyPrePluginFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonBuildConfigPlugin::class) // Allow configurations to be used in all plugins
		applyProjectConventions(rootProject)
		applyBuildSrcResourceExtension(rootProject)
	}


	private fun applyPostPluginFeatures(rootProject: Project) {
		applyWrapperSettings(rootProject)
		applyTaskCategories(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyBuildSrcResourceExtension(rootProject: Project) {
		// Get buildSrc jar
		val classLoader = (Thread.currentThread().contextClassLoader as? URLClassLoader)
				?: error("The classloader is of an unexpected type. Expected: [${URLClassLoader::class.qualifiedName}]. Actual: ${Thread.currentThread().contextClassLoader::class.qualifiedName}].")
		val buildSrcJar = classLoader.urLs
				.map { File(it.file) }
				.filter { it.name == BUILD_SRC_JAR }
				.also { check(it.size == 1) { "An unexpected number of classloader URLs matched name [$BUILD_SRC_JAR]. Expected a single match. Matching URLs: ${it.joinToString(separator = "\n\t", prefix = "\n\t", limit = 10)}" } }
				.first()
		// Apply extension to all projects
		rootProject.allprojects { BuildSrcResourceExtension.create(this, buildSrcJar) }
	}


	private fun applyProjectConventions(rootProject: Project) {
		val groupName = rootProject.buildConfig.rootProjectGroup
		rootProject.allprojects {
			group = groupName
		}
	}


	private fun applyPlugins(rootProject: Project) {
		// Order-dependent plugins
		rootProject.pluginManager.apply(CliftonBuildScanPlugin::class) // Allow build-scans to be deployed even if remaining plugins fail during configuration

		/*
		 * Apply all root plugins. The order should not matter here, assuming dependencies are properly set up. If ordering issues are found, then please add additional cross-
		 * plugin imports as needed.
		 */
		rootProject.pluginManager.apply(CliftonAppPlugin::class)
		rootProject.pluginManager.apply(CliftonArtifactPlugin::class)
		rootProject.pluginManager.apply(CliftonBenchmarkPlugin::class)
		rootProject.pluginManager.apply(CliftonGitPlugin::class)
		rootProject.pluginManager.apply(CliftonInspectionsPlugin::class)
		rootProject.pluginManager.apply(CliftonIntelliJPlugin::class)
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.pluginManager.apply(CliftonJavaScriptPlugin::class)
		rootProject.pluginManager.apply(CliftonLibraryPlugin::class)
		rootProject.pluginManager.apply(CliftonNodePlugin::class)
		rootProject.pluginManager.apply(CliftonSchemaPlugin::class)
		rootProject.pluginManager.apply(CliftonServicePlugin::class)
		rootProject.pluginManager.apply(CliftonTestPlugin::class)
		rootProject.pluginManager.apply(CliftonUtilPlugin::class)
		rootProject.pluginManager.apply(CliftonOpenApiPlugin::class)
		rootProject.pluginManager.apply(CliftonContainerizationPlugin::class)
	}


	private fun applyWrapperSettings(rootProject: Project) {
		rootProject.tasks.named<Wrapper>("wrapper") {
			// Retrieve "-all" distribution rather than "-bin" distribution in order to include Gradle source code (includes JavaDocs)
			distributionType = Wrapper.DistributionType.ALL
		}
	}


	private fun applyTaskCategories(rootProject: Project) {
		rootProject.allprojects {
			val project = this

			// Enable dynamic inclusion/exclusion of tasks for the IDE view.  Only show what is important.
			project.afterEvaluate {
				project.tasks.configureEach {
					group = if (name in project.buildConfig.taskNameExpressionIncludes && enabled) project.buildConfig.companyName else null
				}
			}

			/*
			 * Check for any default skipped tasks (Usually we skip test by default in the gradle.properties file for development)
			 * Will only execute those tasks that were provided for execution in the command and those not in our excluded task list.
			 */
			(project.gradle.taskGraph as TaskExecutionGraphInternal).useFilter { task -> task.name !in project.buildConfig.defaultExcludeTask || project.gradle.startParameter.taskNames.any { it == task.name } }
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


const val BUILD_SRC_JAR = "buildSrc.jar"
internal val Project.buildSrc get() = the<BuildSrcResourceExtension>()

/**
 * The plugin extension type for accessing buildSrc resources. This allows references to buildSrc resources within tasks and other logic.
 *
 * Example usage:
 * ```
 * tasks.register<Copy>("myCopyTask") {
 *     from(project.buildSrc("myCopyTaskSources/" + "**")) // String is only split to work around KDoc's lack of escape characters here
 *     into("${project.buildDir}/dist/myCopyTaskOutput")
 * }
 * ```
 *
 * @author MikeH
 */
@Suppress("unused")
open class BuildSrcResourceExtension constructor(private val project: Project, private val buildSrcJar: File) {

	companion object {

		fun create(project: Project, buildSrcJar: File): BuildSrcResourceExtension {
			return project.extensions.create("buildSrc", BuildSrcResourceExtension::class, project, buildSrcJar)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	fun resources(vararg resources: String): FileTree {
		return project.zipTree(buildSrcJar).matching {
			resources.forEach { include(it) }
		}
	}
}
