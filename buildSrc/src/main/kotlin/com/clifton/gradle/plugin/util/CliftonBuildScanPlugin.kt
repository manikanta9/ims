@file:Suppress("UnstableApiUsage")

package com.clifton.gradle.plugin.util

import com.clifton.gradle.util.executeSimple
import com.clifton.gradle.util.requestedTaskNames
import com.clifton.gradle.util.requireRootProject
import com.gradle.scan.plugin.BuildScanExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.internal.GradleInternal
import org.gradle.kotlin.dsl.create
import org.gradle.kotlin.dsl.property
import org.gradle.kotlin.dsl.the
import java.net.URLEncoder


/**
 * The Gradle [Plugin] for applying build-scan features to the project. This plugin attaches several convention-based tags, links, and other features to build scans.
 *
 * @author MikeH
 */
class CliftonBuildScanPlugin : Plugin<Project> {

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		check((rootProject.gradle as GradleInternal).settings.pluginManager.hasPlugin("com.gradle.enterprise")) { "The Gradle Build Scan plugin must be applied before the Clifton Build Scan plugin is applied." }
		val buildScan = CliftonBuildScanExtension.create(rootProject)
		// Wait until after configuration so that the caller can configure the custom extension properties
		buildScan.afterFinalized {
			if (!rootProject.buildConfig.enableBuildScan || !buildScan.enabled.get()) return@afterFinalized
			applyHostConfiguration(rootProject, buildScan)
			applyStandardAttributes(rootProject, buildScan)
			applyIdeAttributes(buildScan)
			applyCiAttributes(buildScan)
			applyBuildFinished(buildScan)
			applyGitAttributes(rootProject, buildScan)
		}
		rootProject.afterEvaluate {
			if (!buildScan.finalized) {
				rootProject.logger.warn("Build scan finalization has not occurred. A build scan will not be taken. Consider adding a cliftonBuildPlugin.finalize() statement to apply any existing configuration and silence this message.")
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Apply host and upload configurations.
	 */
	private fun applyHostConfiguration(project: Project, buildScan: CliftonBuildScanExtension) {
		buildScan.publishAlwaysIf(buildScan.publish.get())
		buildScan.server = project.buildConfig.gradleEnterpriseHost
		buildScan.allowUntrustedServer = true
		buildScan.isCaptureTaskInputFiles = true
	}


	/**
	 * Apply scan tags which apply to all builds.
	 */
	private fun applyStandardAttributes(project: Project, buildScan: CliftonBuildScanExtension) {
		buildScan.tag(System.getProperty("os.name"))
		if (project.gradle.startParameter.isRerunTasks) {
			buildScan.tag("Rerun Tasks")
		}
		if (project.gradle.startParameter.isBuildCacheEnabled) {
			buildScan.tag("Cached")
		}
		buildScan.value("Project Directory", project.gradle.startParameter.projectDir.toString())
		buildScan.value("Task Request(s)", project.gradle.startParameter.taskRequests.joinToString("\n"))
	}


	/**
	 * Apply scan tags for the IDE.
	 */
	private fun applyIdeAttributes(buildScan: CliftonBuildScanExtension) {
		// IntelliJ attributes
		if (listOf("idea.registered", "idea.active", "idea.paths.selector").any { System.getProperty(it) != null }) {
			buildScan.tag("IDEA")
			System.getProperty("idea.paths.selector")?.let { ideaVersion -> buildScan.value("IDEA version", ideaVersion) }
		}
	}


	/**
	 * Apply scan tags for the CI server.
	 */
	private fun applyCiAttributes(buildScan: CliftonBuildScanExtension) {
		val ciServerType: CiServerType = when {
			System.getenv().containsKey("bamboo_repository_name") -> CiServerType.BAMBOO
			else -> CiServerType.NONE
		}
		@Suppress("REDUNDANT_ELSE_IN_WHEN")
		when (ciServerType) {
			CiServerType.BAMBOO -> {
				buildScan.tag("CI")
				buildScan.tag("Bamboo")
				if (System.getenv().containsKey("bamboo_buildNumber")) {
					val buildNumber = System.getenv("bamboo_buildNumber")
					buildScan.value("Build Number", buildNumber)
					if (buildNumber == "1") {
						buildScan.tag("Initial Build")
					}
				}
				if (System.getenv().containsKey("bamboo_resultsUrl")) {
					buildScan.link("Bamboo Build", System.getenv("bamboo_resultsUrl"))
				}
			}
			CiServerType.NONE -> {
				buildScan.tag("Local")
			}
			else -> error("Unrecognized CI server type [$ciServerType].")
		}
	}


	/**
	 * Apply scan tags for git environments.
	 */
	private fun applyGitAttributes(project: Project, buildScan: CliftonBuildScanExtension) {
		// Guard-clause: Check enabled flag
		if (!buildScan.detectGitAttributes.get()) {
			return
		}
		// Long-running attribute processing
		buildScan.background {
			// Commit details
			project.executeSimple("git rev-parse --verify HEAD").let { commitId ->
				this.link("Commit", "https://stash.paraport.com/projects/IMS/repos/ims/commits/$commitId")
				this.value("Commit", commitId)
				this.link("Commit Scans", "${this.server}/scans?search.names=Commit&search.values=${URLEncoder.encode(commitId, "UTF-8")}")
			}
			project.executeSimple("git log -1 --pretty=format:%aE").let { authorEmail ->
				if (authorEmail.isNotEmpty()) {
					this.value("Author", authorEmail)
				}
			}
			// Branch details
			project.executeSimple("git rev-parse --abbrev-ref HEAD").let { branchName ->
				if (branchName != "HEAD") {
					this.link("Branch", "https://stash.paraport.com/projects/IMS/repos/ims/compare/commits?sourceBranch=$branchName&targetBranch=refs%2Fheads%2Fmaster")
					this.value("Branch", branchName)
					this.link("Branch Scans", "${this.server}/scans?search.names=Branch&search.values=${URLEncoder.encode(branchName, "UTF-8")}")
				}
			}
			// Workspace status
			project.executeSimple("git status --porcelain").let { status ->
				if (status.isNotEmpty()) {
					this.tag("Dirty")
					this.value("Git Status", status)
				}
			}
		}
	}


	/**
	 * Apply actions to be taken when the build is completed.
	 */
	private fun applyBuildFinished(buildScan: CliftonBuildScanExtension) {
		buildScan.buildFinished {
			if (this.failure != null) {
				buildScan.value("Failed with", this.failure.message)
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private enum class CiServerType { BAMBOO, NONE }
}


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized build scan configuration. This extension includes all features incorporated in the standard
 * [BuildScanExtension] by delegation.
 *
 * This extension can be configured in projects which apply this plugin as follows:
 * ```
 * cliftonBuildScan {
 *     publishAlways()
 *     detectGitAttributes = true
 *     tag("Clifton Build")
 *     link("Build Server", "https://bamboo.paraport.com")
 *     // ...
 * }
 * ```
 */
open class CliftonBuildScanExtension constructor(project: Project, buildScan: BuildScanExtension) : BuildScanExtension by buildScan {

	companion object {

		fun create(project: Project): CliftonBuildScanExtension {
			return project.extensions.create("cliftonBuildScan", CliftonBuildScanExtension::class, project, project.the<BuildScanExtension>())
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If `true`, all build scan configuration will be enabled. Otherwise, build scans will be disabled.
	 */
	val enabled = project.objects.property<Boolean>()
			.apply { finalizeValueOnRead() }
			.convention(project.provider { project.requestedTaskNames.any { it !in project.buildConfig.buildScanSkipTasks } })

	/**
	 * If `true`, build scans will be published. Otherwise, build scans (even if generated) will not be published.
	 */
	val publish = project.objects.property<Boolean>()
			.apply { finalizeValueOnRead() }
			.convention(true)

	/**
	 * If `true`, the build scan will pull in properties from git for the build scan. These operations run as a background task, but if git operations run slowly then this can
	 * extend the duration of the build.
	 */
	val detectGitAttributes = project.objects.property<Boolean>()
			.apply { finalizeValueOnRead() }
			.convention(project.provider { !project.buildConfig.isQuickTaskExecution })

	/**
	 * If `true`, the build scan extension has been finalized. The build scan plugin is configured on finalization, and finalization can only occur a single time.
	 */
	var finalized = false
		private set

	private val finalizedActions = mutableListOf<(CliftonBuildScanExtension) -> Unit>()


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	fun afterFinalized(action: (CliftonBuildScanExtension) -> Unit) {
		check(!finalized) { "The extension has already been finalized. Finalized actions can no longer be added." }
		finalizedActions += action
	}


	fun finalize() {
		check(!finalized) { "The extension has already been finalized and cannot be finalized more than once." }
		finalized = true
		finalizedActions.forEach { it(this) }
	}
}
