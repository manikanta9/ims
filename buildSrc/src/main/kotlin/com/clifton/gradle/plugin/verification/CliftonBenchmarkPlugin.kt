package com.clifton.gradle.plugin.verification

import com.clifton.gradle.plugin.build.*
import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.plugin.util.projectConfig
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.*
import org.gradle.kotlin.dsl.*
import java.io.File
import kotlin.properties.Delegates


/**
 * The Gradle [Plugin] for registering benchmarking components, such as JMH.
 *
 * @author MikeH
 */
@Suppress("unused")
class CliftonBenchmarkPlugin : Plugin<Project> {

	companion object {
		const val JMH_TASK_NAME = "jmh"
		const val JMH_SOURCE_SET_NAME = "jmh"
		const val JMH_FEATURE_NAME = "jmh"
	}


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.pluginManager.apply(CliftonLibraryPlugin::class)
		rootProject.subprojects {
			applyJmhFeatures(this)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Applies JMH functionality to the project.
	 *
	 * This adds a source set (as well as an exported [feature variant][registerVariant] for library projects) named "jmh". JMH benchmarks can be created within this source set.
	 * ```
	 * val jmhSourceSet = cliftonJmh.enable()
	 * ```
	 * Shared benchmarking source can be placed in the JMH variant of a library project and then depended upon via the [usingVariant] dependency functionality for variants.
	 * ```
	 * dependencies {
	 *     jmhSourceSet.implementationConfigurationName(project(":clifton-core")) { usingVariant("jmh") }
	 * }
	 * ```
	 */
	private fun applyJmhFeatures(project: Project) {
		CliftonJmhExtension.create(project) {            // Create JMH source set
			val sourceSets = project.the<SourceSetContainer>()
			val jmhSourceSet = if (project.projectConfig.isLibraryProject) {
				// Library project: Create variant which can be used by downstream consumers
				val jmhVariant: FeatureVariant = project.registerVariant(JMH_FEATURE_NAME, upstreamForTests = false)
				project.configurations.named(jmhVariant.implementation) { extendsFrom(project.configurations[sourceSets[SourceSet.TEST_SOURCE_SET_NAME].runtimeClasspathConfigurationName]) }
				jmhVariant.sourceSet
			}
			else {
				// Non-library project: Create source set
				sourceSets.create(JMH_SOURCE_SET_NAME).apply {
					compileClasspath += sourceSets[SourceSet.TEST_SOURCE_SET_NAME].runtimeClasspath
					runtimeClasspath += sourceSets[SourceSet.TEST_SOURCE_SET_NAME].runtimeClasspath
				}
			}

			// Add JMH dependencies
			project.dependencies {
				jmhSourceSet.implementationConfigurationName("org.openjdk.jmh:jmh-core:${project.buildConfig.dependencies.jmhVersion}")
				jmhSourceSet.annotationProcessorConfigurationName("org.openjdk.jmh:jmh-generator-annprocess:${project.buildConfig.dependencies.jmhVersion}")
			}

			// Configure JMH task
			project.tasks.register(JMH_TASK_NAME, JmhExec::class) {
				main = "org.openjdk.jmh.Main"
				classpath = jmhSourceSet.runtimeClasspath
				benchmarkSource = jmhSourceSet.allSource.asFileTree.filter { it.isFile }
				resultFile = project.buildDir.resolve("reports/jmh/result.json")
				args(
						"-foe", "true", // Fail on error
						"-v", "NORMAL", // Verbosity level
						"-jvmArgsPrepend", "-Xms3G", // Min heap size
						"-jvmArgsPrepend", "-Xmx3G", // Max heap size
						"-rf", "json", // Result format
						"-rff", resultFile // Result file
				)
			}
			jmhSourceSet
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Task] type for executing JMH benchmarks. This executes all benchmarks in the given project.
 */
open class JmhExec : JavaExec() {

	/**
	 * The sources used for benchmark evaluation. If empty, execution will be skipped.
	 */
	@get:InputFiles
	@get:SkipWhenEmpty
	@get:PathSensitive(PathSensitivity.RELATIVE)
	var benchmarkSource: FileCollection = project.files()

	/**
	 * The output file for the benchmark results.
	 */
	@get:OutputFile
	var resultFile: File by Delegates.notNull<File>()
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * The [Extension][org.gradle.api.plugins.ExtensionContainer] for customized jmh configuration. This extension provides fields for configuring plugin-specific jmh
 * customizations.
 *
 * Example usage:
 * ```
 * val jmhSourceSet = cliftonJmh.enable()
 * ```
 */
open class CliftonJmhExtension(private val project: Project, private val enableFn: () -> SourceSet) {

	companion object {

		fun create(project: Project, enableFn: () -> SourceSet): CliftonJmhExtension {
			return project.extensions.create("cliftonJmh", CliftonJmhExtension::class, project, enableFn)
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private val jmhSourceSet by lazy { enableFn() }

	fun enable(): SourceSet {
		return jmhSourceSet
	}
}
