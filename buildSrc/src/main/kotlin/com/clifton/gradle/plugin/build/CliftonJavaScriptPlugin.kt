package com.clifton.gradle.plugin.build

import com.clifton.gradle.plugin.util.buildConfig
import com.clifton.gradle.util.requireRootProject
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.artifacts.ModuleDependency
import org.gradle.api.attributes.LibraryElements
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.internal.deprecation.DeprecatableConfiguration
import org.gradle.kotlin.dsl.apply
import org.gradle.kotlin.dsl.get
import org.gradle.kotlin.dsl.named
import org.gradle.kotlin.dsl.the


/**
 * The Gradle [Plugin] for configuring JavaScript tasks and resources.
 */
class CliftonJavaScriptPlugin : Plugin<Project> {

	companion object {
		const val LIBRARY_ELEMENTS_JAVASCRIPT = "javascript"
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		rootProject.pluginManager.apply(CliftonJavaPlugin::class)
		rootProject.subprojects { applyPerProjectFeatures(project) }
	}


	private fun applyPerProjectFeatures(project: Project) {
		setupJavascriptConfigurations(project)

		// Exclude transitive dependencies of Webjars. Because these dependencies are not resolved consistently, explicit resolution is required for determinant builds.
		project.afterEvaluate {
			configurations
					.flatMap { it.dependencies }
					.filter { it.group?.startsWith("org.webjars") == true && it.name != "webjars-locator" }
					.forEach { (it as ModuleDependency).isTransitive = false }
		}
	}


	private fun setupJavascriptConfigurations(project: Project) {
		val sourceSets = project.the<SourceSetContainer>()
		val mainSourceSet = sourceSets[SourceSet.MAIN_SOURCE_SET_NAME]
		// Dependency-declaring configuration
		project.configurations.register(mainSourceSet.javascriptConfigurationName) {
			description = "JavaScript dependencies for the source set \"${mainSourceSet.name}\"."
			isCanBeResolved = false
			isCanBeConsumed = false
			isTransitive = true
			isVisible = false
		}
		// Internally resolvable configuration
		project.configurations.register(mainSourceSet.javascriptClasspathConfigurationName) {
			extendsFrom(project.configurations[mainSourceSet.javascriptConfigurationName])
			(this as DeprecatableConfiguration).deprecateForDeclaration(mainSourceSet.javascriptConfigurationName)
			description = "JavaScript class path for the source set \"${mainSourceSet.name}\". This is the resolvable configuration which designates the JavaScript resources to be used during builds."
			isCanBeResolved = true
			isCanBeConsumed = false
			isTransitive = true
			isVisible = false
			attributes {
				attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE, project.objects.named(LibraryElements::class, LIBRARY_ELEMENTS_JAVASCRIPT))
			}
		}
		// Outgoing configuration
		project.configurations.register(mainSourceSet.javascriptElementsConfigurationName) {
			extendsFrom(project.configurations[mainSourceSet.javascriptConfigurationName])
			(this as DeprecatableConfiguration).deprecateForDeclaration(mainSourceSet.javascriptConfigurationName)
			description = "JavaScript elements for the source set \"${mainSourceSet.name}\". These are the JavaScript elements which are made available to other projects."
			isCanBeResolved = false
			isCanBeConsumed = true
			isTransitive = true
			isVisible = false
			attributes {
				attribute(LibraryElements.LIBRARY_ELEMENTS_ATTRIBUTE, project.objects.named(LibraryElements::class, LIBRARY_ELEMENTS_JAVASCRIPT))
			}
		}
		project.artifacts.add(mainSourceSet.javascriptElementsConfigurationName, project.file(project.buildConfig.webDirectoryName))
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


val SourceSet.javascriptConfigurationName
	get() = if (this.name == SourceSet.MAIN_SOURCE_SET_NAME) "javascript" else "${this.name}Javascript"

val SourceSet.javascriptClasspathConfigurationName
	get() = if (this.name == SourceSet.MAIN_SOURCE_SET_NAME) "javascriptClasspath" else "${this.name}JavascriptClasspath"

val SourceSet.javascriptElementsConfigurationName
	get() = if (this.name == SourceSet.MAIN_SOURCE_SET_NAME) "javascriptElements" else "${this.name}JavascriptElements"
