@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package com.clifton.gradle.plugin.util

import com.clifton.gradle.create
import com.clifton.gradle.mapping
import com.clifton.gradle.util.*
import org.apache.tools.ant.filters.FixCrLfFilter
import org.apache.tools.ant.filters.ReplaceTokens
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.file.CopySpec
import org.gradle.kotlin.dsl.extra
import org.gradle.kotlin.dsl.filter
import org.gradle.kotlin.dsl.provideDelegate
import org.gradle.kotlin.dsl.the
import java.util.*
import kotlin.collections.component1
import kotlin.collections.component2


/**
 * The Gradle [Plugin] for adding shared configuration properties for all plugins and scripts. This plugin adds extension properties to all projects so that they have access to
 * both [root properties][BUILD_CONFIG_EXTENSION] as well as [project-specific properties][projectConfig].
 *
 * @author MikeH
 */
class CliftonBuildConfigPlugin : Plugin<Project> {

	override fun apply(rootProject: Project) {
		requireRootProject(rootProject)
		applyFeatures(rootProject)
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private fun applyFeatures(rootProject: Project) {
		val cliftonBuildConfiguration = CliftonBuildConfiguration(rootProject)
		rootProject.allprojects { this.extensions.add("buildConfig", cliftonBuildConfiguration) }
		rootProject.subprojects { this.extensions.add("projectConfig", CliftonProjectConfiguration(this)) }
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


const val BUILD_CONFIG_EXTENSION = "buildConfig"
val Project.buildConfig get() = this.the<CliftonBuildConfiguration>()

/**
 * The build configuration object. This object stores the majority of configurable properties used to configure a build.
 *
 * The majority of these properties are stored in `gradle.properties` and can be overridden by using command-line project properties (e.g., `./gradlew
 * -PmyBuildProperties=build.qa.properties :app-clifton-ims:build`) or system properties (e.g., `./gradlew -Dorg.gradle.project.myBuildProperties=build.qa.properties
 * :app-clifton-ims:build`). See the Gradle [Build Environment][https://docs.gradle.org/current/userguide/build_environment.html] documentation page for more details on setting
 * build properties.
 *
 * See [CliftonProjectConfiguration] for per-project configurable and computed properties.
 *
 * @author MikeH
 * @see CliftonProjectConfiguration
 */
class CliftonBuildConfiguration internal constructor(internal val rootProject: Project) {

	// Build environment settings
	val isCiServer: Boolean by rootProject.extra.mapping { if (it.isNotBlank()) it.toBoolean() else System.getenv().containsKey("bamboo_repository_name") }
	val isIdeaGradleSync: Boolean by rootProject.extra.withDefault(System.getProperty("idea.sync.active")?.toBoolean() ?: false)

	// Paths
	val buildRootPath: String by rootProject.extra
	val defaultBuildProperties: String by rootProject.extra
	val myGlobalBuildProperties: String by rootProject.extra
	val myBuildProperties: String by rootProject.extra
	val webDirectoryName: String by rootProject.extra
	val externalResourcesDirectoryName: String by rootProject.extra
	val springInstrumentFileName: String by rootProject.extra

	// Conventions
	val companyName: String by rootProject.extra
	val serviceProjectPrefix: String by rootProject.extra
	val appProjectPrefix: String by rootProject.extra
	val apiProjectSuffix: String by rootProject.extra

	// Task configuration
	val defaultExcludeTask: List<String> by rootProject.extra.mapping { it.tokenize() }
	val taskNameExpressionIncludes: List<String> by rootProject.extra.mapping { it.tokenize() }
	val buildScanSkipTasks: List<String> by rootProject.extra.mapping { it.tokenize() }
	val buildScanQuickTasks: List<String> by rootProject.extra.mapping { it.tokenize() }
	val isQuickTaskExecution: Boolean by rootProject.extra.mapping { if (it.isNotBlank()) it.toBoolean() else rootProject.requestedTaskNames.all { taskName -> taskName in this.buildScanQuickTasks } }
	val enableSpotBugs: Boolean by rootProject.extra.mapping { it.toBoolean() }

	// Gradle settings
	val enableBuildScan: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val enableBuildCache: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val enableBuildCacheLocal: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val enableBuildCacheRemote: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val useIncrementalBuild: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val gradleEnterpriseHost: String by rootProject.extra

	// Test settings
	val enableTestRetry: Boolean by rootProject.extra.mapping { if (it.isNotBlank()) it.toBoolean() else this.isCiServer }
	val enableCodeCoverage: Boolean by rootProject.extra.mapping { if (it.isNotBlank()) it.toBoolean() else rootProject.requestedTaskNames.any { taskName -> taskName in this.codeCoverageTasks } }
	val codeCoverageTasks: List<String> by rootProject.extra.mapping { it.tokenize() }
	val skipMemoryDbTests: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val testProfiles: List<String> by rootProject.extra.mapping { it.tokenize() }
	val maxProjectTestThreads: Int by rootProject.extra.mapping { it.toInt() }

	// Other settings
	val debugTasks: List<String> by rootProject.extra.mapping { it.tokenize() }
	val artifactMigrationEnabled: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val buildCompressedDistribution: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val buildProjectPrefixedDistribution: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val buildNodeArtifacts: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val fileTypesToFilter: List<String> by rootProject.extra.mapping { it.tokenize() }
	val isResourceFilteringEnabled: Boolean by rootProject.extra.mapping { it.toBoolean() }
	val resourceFilteringSystemProperties: List<String> by rootProject.extra.mapping { it.tokenize() }
	val keystoreFileName: String by rootProject.extra
	val keystoreVersion: String by rootProject.extra
	val migrateType: String by rootProject.extra
	val propertiesFilterPrefix: String by rootProject.extra
	val rootProjectGroup: String by rootProject.extra
	val schemaIncludeUpstreamChanges: Boolean by rootProject.extra.mapping { if (it.isNotBlank()) it.toBoolean() else this.isCiServer }
	val testSourceSets: List<String> by rootProject.extra.mapping { it.tokenize() }
	val ignoredSourceSets: List<String> by rootProject.extra.mapping { it.tokenize() }
	val springBootProjects: List<String> by rootProject.extra.mapping { it.tokenize() }
	val containerProjects: List<String> by rootProject.extra.mapping { it.tokenize() }

	// Artifactory properties
	val apiKey: String by rootProject.extra.named("artifactory.apikey")
	val artifactoryUrl: String by rootProject.extra.named("artifactory.url")
	val artifactoryNetcoreRepo: String by rootProject.extra.named("artifactory.netcore.targetRepo")
	val artifactoryJavaRepo: String by rootProject.extra.named("artifactory.java.targetRepo")
	val apiVersion: String by rootProject.extra

	// Custom properties
	val fullBuildRootPath: String = "${rootProject.rootDir}/$buildRootPath"


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	val dependencies = DependencyConfiguration()
	val repository = RepositoryConfiguration()
	val container = ContainerConfiguration()

	inner class DependencyConfiguration {
		val jmhVersion: String by rootProject.extra
	}

	inner class RepositoryConfiguration {
		val name: String by rootProject.extra.named("repo.name")
		val username: String by rootProject.extra.named("repo.username")
		val password: String by rootProject.extra.named("repo.password")
		val host: String by rootProject.extra.named("repo.host")
		val path: String by rootProject.extra.named("repo.path")
	}

	inner class ContainerConfiguration {
		val fromAuthUsername: String by rootProject.extra.named("container.from.auth.username")
		val fromAuthPassword: String by rootProject.extra.named("container.from.auth.password")
		val fromImage: String by rootProject.extra.named("container.from.image")
		val toAuthUsername: String by rootProject.extra.named("container.to.auth.username")
		val toAuthPassword: String by rootProject.extra.named("container.to.auth.password")
		val toImage: String by rootProject.extra.named("container.to.image")
		val toTags: List<String> by rootProject.extra.mapping("container.to.tags") { it.tokenize() }
	}
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


/**
 * @see [CliftonProjectConfiguration]
 */
val Project.projectConfig get() = this.the<CliftonProjectConfiguration>()

/**
 * The project configuration object. This object stores configurable project-specific properties. Most notably, this object stores the [buildProperties] object, which is an
 * aggregation of all of the build properties retrieved from files such as `build.default.properties`.
 *
 * @author MikeH
 */
class CliftonProjectConfiguration(val project: Project) {

	/**
	 * `true` if the project is an "application project", or `false` otherwise. An application project is a project which is intended be run as a fully-fledged application. These
	 * projects are identified via naming conventions.
	 */
	val isAppProject = project.name.startsWith(project.buildConfig.appProjectPrefix)

	/**
	 * `true` if the project is a "service project", or `false` otherwise. A service project is a project which is intended to be run as a daemon service with no provided UI. These
	 * projects are identified via naming conventions.
	 */
	val isServiceProject = project.name.startsWith(project.buildConfig.serviceProjectPrefix)

	/**
	 * `true` if the project is a "library project", or `false` otherwise. A library project is a project which is configured to be usable by downstream dependencies.
	 */
	val isLibraryProject = !isAppProject && !isServiceProject

	/**
	 * `true` if the project is a Spring Boot project, or `false` otherwise. These projects are manually declared via the [global build configuration]
	 * [CliftonBuildConfiguration.springBootProjects].
	 */
	val isSpringBootProject = project.name in project.buildConfig.springBootProjects

	/**
	 * `true` if the project uses OpenAPI, or `false` otherwise. These projects are identified via naming conventions.
	 */
	val isOpenApiProject = project.name.endsWith(project.buildConfig.apiProjectSuffix)

	/**
	 * `true` if the project should be containerized, or `false` otherwise. These projects are manually declared via the [global build configuration]
	 * [CliftonBuildConfiguration.containerProjects].
	 */
	val isContainerProject = project.name in project.buildConfig.containerProjects

	/**
	 * The aggregation of build properties for this project. These properties are retrieved from the [CliftonBuildConfiguration.defaultBuildProperties] file and from the
	 * [CliftonBuildConfiguration.myBuildProperties]. These properties are used primarily for compile-time [property placeholder replacements][applyPropertyReplacements].
	 */
	val buildProperties: ExtensionProperties<String> by lazy { generateBuildProperties() }


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates the build properties for the project.
	 */
	private fun generateBuildProperties(): ExtensionProperties<String> {
		/*
		 * A few project properties are read early during the configuration phase, e.g. those used to determine which tasks to register, and subsequent unrelated modifications
		 * cannot be blocked. This prevents us from finalizing properties before or during applyPropertyReplacements execution.
		 */
		return ExtensionProperties.create<String>(project, finalizeOnRead = false).apply {
			// Apply no properties if all properties files are absent
			val propertyFileList = listOf(project.file(project.buildConfig.defaultBuildProperties), project.rootProject.file(project.buildConfig.myGlobalBuildProperties), project.file(project.buildConfig.myBuildProperties))
					.filter { it.isFile }
			if (propertyFileList.isEmpty()) {
				return@apply
			}
			// Apply standard conventions
			setConvention("project.name", project.name)
			// Apply system properties
			System.getProperties()
					.filterKeys { project.buildConfig.resourceFilteringSystemProperties.contains(it) }
					.forEach { (key, value) -> this[key as String] = value as String }
			// Load properties files
			Properties()
					.apply { propertyFileList.forEach { propertiesFile -> propertiesFile.inputStream().use { load(it) } } } // Load the default properties file and its overrides
					.let { it.forEach { (key, value) -> this[key as String] = value as String } }
			// Add any additional properties with the designated properties filter prefix
			project.properties
					.filter { it.key.startsWith(project.buildConfig.propertiesFilterPrefix) }
					.forEach { (k, v) -> this[k.removePrefix(project.buildConfig.propertiesFilterPrefix)] = v.toString() }
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * See [applyPropertyReplacements].
	 */
	fun <T> applyPropertyReplacements(copySpecTask: T, properties: Map<String, Any?> = project.projectConfig.buildProperties.asMap()) where T : CopySpec, T : Task {
		applyPropertyReplacements(copySpecTask, copySpecTask, properties)
	}


	/**
	 * Applies property replacements to the given [CopySpec]. This will use the key-value pairs in [buildProperties] to replace any [ReplaceTokens] candidates in the [CopySpec]
	 * inputs with their mapped values during execution. [ReplaceTokens] candidates are any pieces of text surrounded by "`@`" characters, such as "`@dataSource.databaseName@`".
	 *
	 * Property replacements are applied _twice_, meaning that recursive definitions up to two levels deep are allowed. This allows for property placeholder values which reference
	 * other property placeholders (e.g., `graylog.input.application=@application.name@`).
	 *
	 * Example:
	 * ```
	 * // Source properties:
	 * application.websocket.enabled=true
	 * application.websocket.servlet.path=/ws
	 * application.websocket.stomp.path=/stomp
	 * application.websocket.stomp.prefix.subscription=/topic,/queue
	 * application.websocket.stomp.prefix.message=/app
	 *
	 * // Source resource:
	 * <bean id="webSocketConfigurationProperties" class="com.clifton.websocket.config.WebSocketConfigurationProperties" autowire="no">
	 *     <constructor-arg name="enabled" value="@application.websocket.enabled@" />
	 *     <constructor-arg name="servletPath" value="@application.websocket.servlet.path@" />
	 *     <constructor-arg name="stompEndpoints" value="@application.websocket.stomp.path@" />
	 *     <constructor-arg name="stompMessagePrefixes" value="@application.websocket.stomp.prefix.message@" />
	 *     <constructor-arg name="stompSubscriptionPrefixes" value="@application.websocket.stomp.prefix.subscription@" />
	 * </bean>
	 *
	 * // Resulting resource:
	 * <bean id="webSocketConfigurationProperties" class="com.clifton.websocket.config.WebSocketConfigurationProperties" autowire="no">
	 *     <constructor-arg name="enabled" value="true" />
	 *     <constructor-arg name="servletPath" value="/ws" />
	 *     <constructor-arg name="stompEndpoints" value="/stomp" />
	 *     <constructor-arg name="stompMessagePrefixes" value="/app" />
	 *     <constructor-arg name="stompSubscriptionPrefixes" value="/topic,/queue" />
	 * </bean>
	 * ```
	 */
	// fun applyPropertyReplacements(copySpec: CopySpec, task: Task? = null, properties: Map<String, Any?> = project.projectConfig.buildProperties.run { finalize(); asMap() }) {
	fun applyPropertyReplacements(copySpec: CopySpec, task: Task? = null, properties: Map<String, Any?> = project.projectConfig.buildProperties.asMap()) {
		// Guard-clause: Only convert line endings if properties map is empty
		if (properties.isEmpty()) {
			return
		}
		// Assign properties as task inputs
		if (task != null) {
			task.inputs.property("property replacements: enabled", true)
			task.inputs.property("property replacements: processed file types", project.buildConfig.fileTypesToFilter)
			properties.forEach { (prop, value) -> task.inputs.property("property replacements: \"$prop\"", value) }
		}
		// Apply property-placeholder replacements during copy
		copySpec.filesMatching(project.buildConfig.fileTypesToFilter.map { "**/*$it" }) {
			/*
			 * Replace tokens twice because because we need two substitutions.
			 * Example:
			 * first: dataSource.url=@dataSource.url@ -> dataSource.url=jdbc:sqlserver://@dataSource.databaseServerName@:1433;databaseName=@dataSource.databaseName@
			 * second: dataSource.url=jdbc:sqlserver://@dataSource.databaseServerName@:1433;databaseName=@dataSource.databaseName@ -> dataSource.url=jdbc:sqlserver://localhost:1433;databaseName=CliftonIMS
			 */
			val replaceTokensProperties = Hashtable(properties) // ReplaceTokens accepts Hashtables only
			filter(ReplaceTokens::class, "tokens" to replaceTokensProperties)
			filter(ReplaceTokens::class, "tokens" to replaceTokensProperties)
			// Convert CRLF to LF
			filter(FixCrLfFilter::class, "eol" to FixCrLfFilter.CrLf.newInstance("lf"))
		}
	}


	/**
	 * Returns `true` if any property replacements are active, or `false` otherwise. This can be used to wrap logic around [applyPropertyReplacements] to conditionally perform an
	 * action if and only if property replacements might occur, such as enabling or disabling caching.
	 */
	fun isStandardPropertyReplacementsEnabled(): Boolean {
		return project.projectConfig.buildProperties.asMap().isNotEmpty()
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	val artifactMigration by lazy { ArtifactMigrationConfiguration() }

	/**
	 * The configuration object for encapsulating artifact migration properties. This object contains configuration values used during the resource-filtering and migration steps
	 * for artifact migration.
	 */
	inner class ArtifactMigrationConfiguration {

		/**
		 * Indicates whether or not artifact migration is enabled.
		 */
		val isArtifactMigrationEnabled = project.buildConfig.artifactMigrationEnabled

		/**
		 * The destination directory for the generated artifact. This is the directory in which the generated artifact will be placed.
		 */
		val artifactDestinationPath = project.projectConfig.buildProperties.getOrNull("app.dest.dir")
				?: project.projectConfig.buildProperties.getOrNull("service.dest.dir")

		/**
		 * The directory in which the source artifact exists. This is also the directory into which the source artifact will be exploded prior to processing.
		 */
		val artifactBuildPath = "${project.buildDir}/${if (project.projectConfig.isServiceProject || project.projectConfig.isSpringBootProject) "install" else "dist"}"

		/**
		 * The directory in which the exploded artifact contents exist. This directory contains the primary artifact contents after it has been exploded.
		 */
		val artifactContentsPath = "$artifactBuildPath/${project.name}"

		/**
		 * The file extension for the original and produced artifact. This is the extension that will be used for both the source artifact file and the generated artifact file.
		 */
		val artifactFileType = if (project.projectConfig.isServiceProject || project.projectConfig.isSpringBootProject) "zip" else "war"

		/**
		 * The full file path for the original and produced artifact. This is the path that will be used for both the source artifact file and the generated artifact file.
		 */
		val artifactFile = "$artifactBuildPath/${project.name}.$artifactFileType"

		/**
		 * The temporary directory to use for intermediate artifact migration steps. This is the directory in which the processed resource files will be placed.
		 */
		val artifactTmpPath = "${project.buildDir}/tmp/artifact"

		/**
		 * The [artifactContentsPath] subdirectory which includes all classes and resources provided directly by the end project. This does not include resources from dependencies.
		 */
		val artifactClassPath = "$artifactContentsPath/${if (project.projectConfig.isServiceProject || project.projectConfig.isSpringBootProject) "main" else "WEB-INF/classes"}"

		/**
		 * The [artifactContentsPath] subdirectory which can be used as a classpath when executing migrations for an artifact.
		 */
		val artifactFullClassPath = "$artifactContentsPath/${if (project.projectConfig.isServiceProject || project.projectConfig.isSpringBootProject) "lib" else "WEB-INF"}"

		/**
		 * The relative directory within the project artifact under which the artifact JAR exists. This is only relevant for projects which place the archived JAR in the
		 * distribution, such as service projects which use the [org.gradle.api.plugins.ApplicationPlugin].
		 */
		val artifactProjectRelativeJarPath = if (project.projectConfig.isServiceProject || project.projectConfig.isSpringBootProject) "lib" else null

		/**
		 * The database migration operation type to execute after resource processing has completed.
		 */
		val migrateType = project.buildConfig.migrateType


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		fun printPropertyConfig() {
			println("""
				Artifact processing properties:
					artifactMigrationEnabled=$isArtifactMigrationEnabled
					artifactDestinationPath=$artifactDestinationPath
					artifactBuildPath=$artifactBuildPath
					artifactContentsPath=$artifactContentsPath
					artifactFileType=$artifactFileType
					artifactFile=$artifactFile
					artifactTmpPath=$artifactTmpPath
					artifactClassPath=$artifactClassPath
					artifactFullClassPath=$artifactFullClassPath
					artifactProjectRelativeJarPath=$artifactProjectRelativeJarPath
					migrateType=$migrateType
			""".trimIndent())
		}
	}
}
