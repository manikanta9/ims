#!/bin/bash

SERVICE_NAME="@project.name@"
SERVICE_DIRECTORY="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SERVICE_LOG_DIRECTORY="@service.log.base.path@"
SERVICE_USER="@service.username@"

# Check user existence
if id -u "$SERVICE_USER" >/dev/null 2>&1; then
  echo "Using user $SERVICE_USER"
else
  echo "User does not exist $SERVICE_USER"
  exit 0
fi

# Create service log directory if it does not exist
if [ ! -d "$SERVICE_LOG_DIRECTORY" ]; then
  echo "Creating directory $SERVICE_LOG_DIRECTORY"
  mkdir -m 777 -p "$SERVICE_LOG_DIRECTORY"
fi
# Change end of lines in case it has not been - only if dos2unix command exists
if hash dos2unix 2>/dev/null; then
  find "$SERVICE_DIRECTORY" -type f -exec dos2unix -k -s -o -q {} ';'
else
  echo "Skipping eol conversion because dos2unix is not installed."
fi

# Make sure script is executable
chmod -R 755 "$SERVICE_DIRECTORY"
chown -R "$SERVICE_USER" "$SERVICE_DIRECTORY"

# Create a symbolic link to the logrotate configuration in /etc/logrotate.d
ln -s "$SERVICE_DIRECTORY"/service-logrotate /etc/logrotate.d/"$SERVICE_NAME"
# Add daily logrotate cron job
cat >/etc/cron.daily/"$SERVICE_NAME"-logrotate <<EOT
#!/bin/bash

/usr/sbin/logrotate /etc/logrotate.d/"$SERVICE_NAME"
exit 0
EOT
chmod 755 /etc/cron.daily/"$SERVICE_NAME"-logrotate
chown "$SERVICE_USER" /etc/cron.daily/"$SERVICE_NAME"-logrotate

# Create a symbolic link to the service start/stop script
ln -s "$SERVICE_DIRECTORY"/"service-template.sh" /usr/sbin/"$SERVICE_NAME"
# Register systemd service - init scripts do not work for new Centos versions
cp "$SERVICE_DIRECTORY"/"service-template".service /etc/systemd/system/"$SERVICE_NAME".service
chmod 755 /etc/systemd/system/"$SERVICE_NAME".service
systemctl daemon-reload
systemctl enable "$SERVICE_NAME"
systemctl start "$SERVICE_NAME"
