#!/bin/bash

### BEGIN INIT INFO
# Provides: @project.name@
# Required-Start: $local_fs $network $remote_fs
# Required-Stop: $local_fs $network $remote_fs
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: @service.name@
# Description: @service.name@
### END INIT INFO

SERVICE_LONG_NAME="@service.name@"
SERVICE_NAME="@project.name@"
SERVICE_USER="@service.username@"
SERVICE_DIRECTORY="$(dirname "$(readlink -f "$0")")"
SERVICE_JAVA_AGENT="@service.java.agent@"
if [ -n "$SERVICE_JAVA_AGENT" ] ; then
    SERVICE_JAVA_AGENT="-javaagent:$SERVICE_JAVA_AGENT"
fi
# Export JAVA_OPTS for use in the service template script
export JAVA_OPTS="$SERVICE_JAVA_AGENT @service.java.opts@"
SERVICE_LOG_DIRECTORY="@service.log.base.path@"
PID_FILE="$SERVICE_LOG_DIRECTORY/$SERVICE_NAME.pid"
SERVICE_LOG_FILE="$SERVICE_LOG_DIRECTORY/wrapper.log"

# Change to the service directory so the launcher jar's relative classpath entries work.
cd "$SERVICE_DIRECTORY"
# Verify user existence
if id -u "$SERVICE_USER" > /dev/null 2>&1; then
	echo "Using user $SERVICE_USER"
else
	echo "User does not exist $SERVICE_USER"
	exit 0
fi

isProgramRunning() {
	# If PID file exists and the PID matches a running process, return true (0 = true, 1 = false)
	if [ ! -f "$PID_FILE" ] ; then
		return 1
	elif [ $(echo -n $(ps -p $(cat "$PID_FILE") -o comm=) | wc -c) -eq 0 ]; then
		# PID file exists and process is not active. The process' command length is 0
		return 1
	fi
	return 0
}

start() {
	if isProgramRunning; then
		echo "$SERVICE_LONG_NAME: RUNNING"
	else
		echo "$SERVICE_LONG_NAME: Starting ..."
		# Start service appending output to the log file
		if [ $(echo $(whoami)) == "$SERVICE_USER" ] ; then
			nohup "$SERVICE_DIRECTORY/bin/$SERVICE_NAME" >> "$SERVICE_LOG_FILE" 2>&1 & echo $! > "$PID_FILE"
		else
			EXEC_COMMAND="nohup \"$SERVICE_DIRECTORY/bin/$SERVICE_NAME\" >> \"$SERVICE_LOG_FILE\" 2>&1 & echo \$! > \"$PID_FILE\""
			su -p "$SERVICE_USER" -c "$EXEC_COMMAND"
		fi
		echo "$SERVICE_LONG_NAME: STARTED"
	fi
}

stop() {
	if isProgramRunning; then
		PID=$(cat "$PID_FILE");
		echo "$SERVICE_LONG_NAME: Stopping process with ID $PID ..."
		kill "$PID";
		echo "$SERVICE_LONG_NAME: STOPPED"
		rm "$PID_FILE"
	else
		echo "$SERVICE_LONG_NAME: NOT RUNNING"
	fi
}

case $1 in
	start)
		start
	;;
	stop)
		stop
	;;
	restart)
		stop
		start
	;;
	status)
		if isProgramRunning; then echo "$SERVICE_LONG_NAME: RUNNING"; else echo "$SERVICE_LONG_NAME: NOT RUNNING"; fi
	;;
esac

exit 0
