#!/bin/bash

SERVICE_NAME="@project.name@"

# Stop the service
if [ -f "/etc/init.d/$SERVICE_NAME" ]; then
  # init service
  # Stop the service
  /sbin/service "$SERVICE_NAME" stop
  # Deregister Service
  chkconfig --del "$SERVICE_NAME"
  # Remove link in /ect/init.d
  rm -f /etc/init.d/"$SERVICE_NAME"
else
  # systemd service
  # Stop systemd service
  systemctl stop "$SERVICE_NAME"
  # Deregister systemd Service
  systemctl disable "$SERVICE_NAME"
  # Remove link in start/stop script
  rm -f /usr/sbin/"$SERVICE_NAME"
  # Remove systemd service link
  rm -f /etc/systemd/system/"$SERVICE_NAME".service
fi

# Remove logrotate cron job
rm -f /etc/cron.daily/"$SERVICE_NAME"-logrotate
# Remove link in /etc/logrotate.d
rm -f /etc/logrotate.d/"$SERVICE_NAME"
