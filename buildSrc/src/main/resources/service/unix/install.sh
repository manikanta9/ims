#!/bin/bash
name=$1
if [ -n "$name" ]; then
  if [ -f "/opt/deploy/$name.zip" ]; then
    if [ -d "/opt/$name" ]; then
      echo "Backing up $name"
      cd "/opt/$name"
      /bin/bash "./deregister-service.sh"
      cd /opt
      mkdir -p /opt/backup
      jar cfM "/opt/backup/$name-$(date +"%Y%m%d_%H%M%S").bak.zip" "$name"
      rm -rf "$name"
      # Configure and apply artifact backup retention period
      declare -i retentionDays;
      retentionDays='@service.unix.retention.days@'
      echo "Removing backups older than $retentionDays old"
      find '/opt/backup/' -maxdepth 1 -name "$name-*" -type f -mtime "+$retentionDays" -exec sh -c 'artifact="$0"; echo "- Removing $artifact"; rm "$artifact";' {} \;
    fi

    echo "Installing $name"
    mkdir "/opt/$name"
    cd "/opt/$name"
    jar xf "/opt/deploy/$name.zip"
    rm -rf "/opt/deploy/$name.zip"
    /bin/bash "./register-service.sh"
  fi
else
  echo "Please provide a service name as an argument"
fi
