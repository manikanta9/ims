package com.clifton.gradle.util

import org.gradle.BuildResult
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.testfixtures.ProjectBuilder
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.io.TempDir
import java.io.File
import java.util.jar.Attributes
import java.util.jar.Manifest
import java.util.zip.ZipFile

/**
 * The test class for project utility functions.
 *
 * @author MikeH
 */
class ProjectUtilsKtTest {

	@Suppress("ProtectedInFinal")
	@TempDir
	protected lateinit var projectDir: File
	private lateinit var project: ProjectInternal


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	fun setup() {
		project = ProjectBuilder.builder()
				.withProjectDir(projectDir)
				.build() as ProjectInternal
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	fun `create the class path manifest jar`() {
		// Create classpath jar
		val originalClasspath = listOf(
				File("/lib"),
				File("/src/abc.java"),
				File("/src/file1.java"),
				File("/src/file2.java"),
				File("/src/file3.java"),
				File("/src/file4.java"),
				File("/src/file5.java"),
				File("/src/package1/file1.java"),
				File("/src/package1/file2.java"),
				File("/src/package1/file3.java"),
				File("/src/package1/file4.java"),
				File("/src/package1/file5.java"),
				File("/src/package1/package1-b/file1.java"),
				File("/src/package1/package1-b/file2.java"),
				File("/src/package1/package1-b/file3.java"),
				File("/src/package1/package1-b/file4.java"),
				File("/src/package1/package1-b/file5.java"),
				File("/src/pkg2/file1.java"),
				File("/src/pkg2/file2.java"),
				File("/src/pkg2/file3.java"),
				File("/src/pkg2/file4.java"),
				File("/src/pkg2/file5.java"),
				File("/src/pkg2/pkg2-b/file1.java"),
				File("/src/pkg2/pkg2-b/file2.java"),
				File("/src/pkg2/pkg2-b/file3.java"),
				File("/src/pkg2/pkg2-b/file4.java"),
				File("/src/pkg2/pkg2-b/file5.java")
		)
		val classpathJar = project.classpathJar(originalClasspath)

		// Validate Jar contents
		Assertions.assertTrue(classpathJar.exists())
		Assertions.assertEquals(listOf("META-INF/MANIFEST.MF"), ZipFile(classpathJar).use { zip -> zip.entries().asSequence().map { it.name }.toList() })

		// Validate manifest attributes
		val manifest = ZipFile(classpathJar).use { zip -> Manifest(zip.getInputStream(zip.getEntry("META-INF/MANIFEST.MF"))) }
		val expectedClasspath = originalClasspath.joinToString(" ") { it.toURI().toString().removePrefix("file:") }
		Assertions.assertEquals("1.0", manifest.mainAttributes[Attributes.Name.MANIFEST_VERSION])
		Assertions.assertEquals(expectedClasspath, manifest.mainAttributes[Attributes.Name.CLASS_PATH])
	}


	@Test
	fun `remove the class path jar on build finished`() {
		val classpathJar = project.classpathJar(project.files("/test1", "/test2"))
		Assertions.assertTrue(classpathJar.exists())
		project.gradle.buildListenerBroadcaster.buildFinished(BuildResult(null, null))
		Assertions.assertFalse(classpathJar.exists())
	}
}
