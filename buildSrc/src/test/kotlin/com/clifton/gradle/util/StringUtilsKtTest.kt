package com.clifton.gradle.util

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test


/**
 * The test class for string utility functions.
 *
 * @author MikeH
 */
class StringUtilsKtTest {

	@Test
	fun testTokenize() {
		Assertions.assertEquals(listOf<String>(), "".tokenize())
		Assertions.assertEquals(listOf<String>(), "    ".tokenize())
		Assertions.assertEquals(listOf<String>(), "  \t  \r  \u000c  ".tokenize())
		Assertions.assertEquals(listOf("a"), "a".tokenize())
		Assertions.assertEquals(listOf("ab"), "ab".tokenize())
		Assertions.assertEquals(listOf("abcdefABCDEF_!@#$%^&*()_+-=[]{}\\|;:'\",<.>/?`~"), "abcdefABCDEF_!@#$%^&*()_+-=[]{}\\|;:'\",<.>/?`~".tokenize())
		Assertions.assertEquals(listOf("a", "b", "c"), "a b c".tokenize())
		Assertions.assertEquals(listOf("abc", "def", "ghi"), "abc    def\tghi".tokenize())
	}


	@Test
	fun testToCamelCase() {
		Assertions.assertEquals("", "".toCamelCase())
		Assertions.assertEquals("c", "c".toCamelCase())
		Assertions.assertEquals("c", "C".toCamelCase())
		Assertions.assertEquals("camelCaseStr123", "camelCaseStr123".toCamelCase())
		Assertions.assertEquals("camelCaseStr123", "camel case str 123".toCamelCase())
		Assertions.assertEquals("camelCaseStr123", "CamelCaseStr_123".toCamelCase())
		Assertions.assertEquals("camelCaseStr123", "CamelCaseStr123".toCamelCase())
		Assertions.assertEquals("camelCaseStr123", "camel-case-str-123".toCamelCase())
		Assertions.assertEquals("camelCaseStr123", "camel_case_str_123".toCamelCase())
		Assertions.assertEquals("camelCaseStr123", "CAMEL_CASE_STR_123".toCamelCase())
		Assertions.assertEquals("camelcasestr123", "CAMELCASESTR123".toCamelCase())
	}


	@Test
	fun testToPascalCase() {
		Assertions.assertEquals("", "".toPascalCase())
		Assertions.assertEquals("P", "p".toPascalCase())
		Assertions.assertEquals("P", "P".toPascalCase())
		Assertions.assertEquals("PascalCaseStr123", "pascalCaseStr123".toPascalCase())
		Assertions.assertEquals("PascalCaseStr123", "pascal case str 123".toPascalCase())
		Assertions.assertEquals("PascalCaseStr123", "PascalCaseStr_123".toPascalCase())
		Assertions.assertEquals("PascalCaseStr123", "PascalCaseStr123".toPascalCase())
		Assertions.assertEquals("PascalCaseStr123", "pascal-case-str-123".toPascalCase())
		Assertions.assertEquals("PascalCaseStr123", "pascal_case_str_123".toPascalCase())
		Assertions.assertEquals("PascalCaseStr123", "PASCAL_CASE_STR_123".toPascalCase())
		Assertions.assertEquals("Pascalcasestr123", "PASCALCASESTR123".toPascalCase())
	}


	@Test
	fun testToSnakeCase() {
		Assertions.assertEquals("", "".toSnakeCase())
		Assertions.assertEquals("s", "s".toSnakeCase())
		Assertions.assertEquals("s", "S".toSnakeCase())
		Assertions.assertEquals("snake_case_str_123", "snakeCaseStr123".toSnakeCase())
		Assertions.assertEquals("snake_case_str_123", "snake case str 123".toSnakeCase())
		Assertions.assertEquals("snake_case_str_123", "SnakeCaseStr_123".toSnakeCase())
		Assertions.assertEquals("snake_case_str_123", "SnakeCaseStr123".toSnakeCase())
		Assertions.assertEquals("snake_case_str_123", "snake-case-str-123".toSnakeCase())
		Assertions.assertEquals("snake_case_str_123", "snake_case_str_123".toSnakeCase())
		Assertions.assertEquals("snake_case_str_123", "SNAKE_CASE_STR_123".toSnakeCase())
		Assertions.assertEquals("snakecasestr123", "SNAKECASESTR123".toSnakeCase())
	}


	@Test
	fun testToKebabCase() {
		Assertions.assertEquals("", "".toKebabCase())
		Assertions.assertEquals("k", "k".toKebabCase())
		Assertions.assertEquals("k", "K".toKebabCase())
		Assertions.assertEquals("kebab-case-str-123", "kebabCaseStr123".toKebabCase())
		Assertions.assertEquals("kebab-case-str-123", "kebab case str 123".toKebabCase())
		Assertions.assertEquals("kebab-case-str-123", "KebabCaseStr_123".toKebabCase())
		Assertions.assertEquals("kebab-case-str-123", "KebabCaseStr123".toKebabCase())
		Assertions.assertEquals("kebab-case-str-123", "kebab-case-str-123".toKebabCase())
		Assertions.assertEquals("kebab-case-str-123", "kebab_case_str_123".toKebabCase())
		Assertions.assertEquals("kebab-case-str-123", "KEBAB_CASE_STR_123".toKebabCase())
		Assertions.assertEquals("kebabcasestr123", "KEBABCASESTR123".toKebabCase())
	}


	@Test
	fun testToUpperSnakeCase() {
		Assertions.assertEquals("", "".toUpperSnakeCase())
		Assertions.assertEquals("U", "u".toUpperSnakeCase())
		Assertions.assertEquals("U", "U".toUpperSnakeCase())
		Assertions.assertEquals("UPPER_SNAKE_CASE_STR_123", "upperSnakeCaseStr123".toUpperSnakeCase())
		Assertions.assertEquals("UPPER_SNAKE_CASE_STR_123", "upper snake case str 123".toUpperSnakeCase())
		Assertions.assertEquals("UPPER_SNAKE_CASE_STR_123", "UpperSnakeCaseStr_123".toUpperSnakeCase())
		Assertions.assertEquals("UPPER_SNAKE_CASE_STR_123", "UpperSnakeCaseStr123".toUpperSnakeCase())
		Assertions.assertEquals("UPPER_SNAKE_CASE_STR_123", "upper-snake-case-str-123".toUpperSnakeCase())
		Assertions.assertEquals("UPPER_SNAKE_CASE_STR_123", "upper_snake_case_str_123".toUpperSnakeCase())
		Assertions.assertEquals("UPPER_SNAKE_CASE_STR_123", "UPPER_SNAKE_CASE_STR_123".toUpperSnakeCase())
		Assertions.assertEquals("UPPERSNAKECASESTR123", "UPPERSNAKECASESTR123".toUpperSnakeCase())
	}
}
