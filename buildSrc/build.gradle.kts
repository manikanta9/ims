import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
	`kotlin-dsl`
}

val buildRootPath: String by extra
apply(from = "../$buildRootPath/shared-with-buildSrc/repo.gradle.kts")

// Disable the warning for the use of experimental features in `kotlin-dsl` (see https://docs.gradle.org/5.6.2/userguide/kotlin_dsl.html#sec:kotlin-dsl_plugin)
kotlinDslPluginOptions {
	experimentalWarning.set(false)
}

dependencies {
	// Plugins
	implementation("com.cyclonedx:cyclonedx-gradle-plugin:1.2.1") {
		/*
		 * The Log4J SLF4J implementation module includes an SLF4J binding. We prefer to use the Gradle API SLF4J binding. We exclude this transitive dependency to prevent the
		 * on-build warning message "Class path contains multiple SLF4J bindings".
		 */
		exclude(group = "org.apache.logging.log4j", module = "log4j-slf4j-impl")
		/*
		 * The included transitive dependency is not hosted on Maven Central. However, an identical artifact is hosted under separate Maven coordinates for organizations which
		 * restrict themselves to Maven Central. See https://github.com/everit-org/json-schema/issues/375 for more details.
		 */
		exclude(group = "com.github.everit-org.json-schema", module = "org.everit.json.schema")
	}
	runtimeOnly("com.github.erosb:everit-json-schema:1.12.1") // Fulfill transitive dependency requirement for CycloneDX
	implementation("com.github.node-gradle:gradle-node-plugin:3.0.0-rc5")
	implementation("com.github.spotbugs:spotbugs-gradle-plugin:3.0.0")
	implementation("com.gradle:gradle-enterprise-gradle-plugin:3.6.2")
	implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.8")
	/*
	 * The spring-boot-gradle-plugin module uses a version of jmustache that causes a stack overflow error when using openapitools to
	 * generate dotnet code for a schema that uses inheritance. The  openapi-generator-gradle-plugin uses jmustache version 1.14, which
	 * does not cause this error so we exclude it here so that version 1.14 will be included below.
	 */
	implementation("org.springframework.boot:spring-boot-gradle-plugin:2.3.1.RELEASE") {
		exclude(group = "com.samskivert", module = "jmustache")
	}
	implementation("org.gradle:test-retry-gradle-plugin:1.2.1")

	/*
	 * The openapi-generator-gradle-plugin uses a version of swagger-parser that causes an error when generating OpenAPI documentation
	 * for a spec that uses a relative server URL (https://github.com/OpenAPITools/openapi-generator/issues/8266). The 2.0.20 version
	 * does not have this bug, so we force that version here.
	 */
	implementation("org.openapitools:openapi-generator-gradle-plugin:5.1.1") {
		exclude(group = "io.swagger.parser.v3", module = "swagger-parser")
	}
	implementation("io.swagger.parser.v3:swagger-parser:2.0.20")


	implementation("org.jfrog.artifactory.client:artifactory-java-client-services:2.9.1")

	//Needed for Jib; both spring-boot-gradle-plugin and openapitools use earlier versions of guava that will cause an error when running Jib
	implementation("com.google.guava:guava:30.1-jre")
	implementation("gradle.plugin.com.google.cloud.tools:jib-gradle-plugin:3.1.2")

	// Test dependencies
	testImplementation("org.junit.jupiter:junit-jupiter-api:5.5.2")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.5.2")
}

tasks {
	test {
		useJUnitPlatform()
		testLogging {
			// Display errors in console
			showExceptions = true
			showStandardStreams = true
			exceptionFormat = TestExceptionFormat.FULL
			events("passed", "skipped", "failed")
		}
	}
}


gradlePlugin {
	plugins {
		// Auto-register all plugins
		for (pluginDetail in AutoLoadPluginUtils.getAutoLoadedPluginDetails(project)) {
			project.logger.info("""Registering plugin from project [{}]: Name = "{}". ID = "{}". Class = "{}".""", project.name, pluginDetail.name, pluginDetail.id, pluginDetail.clazz)
			register(pluginDetail.name) {
				id = pluginDetail.id
				implementationClass = pluginDetail.clazz
			}
		}
	}
}


////////////////////////////////////////////////////////////////////////////
////////            Plugin Auto-Registration                        ////////
////////////////////////////////////////////////////////////////////////////

object AutoLoadPluginUtils {

	private val pluginNameRegex = Regex("""Clifton(\w+)Plugin""")
	private val pluginFileRegex = Regex("""${pluginNameRegex.pattern}\.(?:kt|java)""")
	private val kebabCaseRegex = Regex("(?<!^)(?=\\p{Upper}\\p{Lower})")


	fun getAutoLoadedPluginDetails(project: Project): List<PluginDetails> {
		// Find all plugins, mapping plugin names from PascalCase to kebab-case
		val sources = project.sourceSets["main"].allSource
		val srcDirs = sources.srcDirs.map { it.relativeTo(project.rootDir).path }
		return sources.asFileTree
				.asSequence()
				.filter { it.isFile && it.name matches pluginFileRegex }
				.map { sourceFile ->
					val relativePath = removeMatchingSrcDir(sourceFile.relativeTo(project.rootDir).path, srcDirs)
					val pluginClass = relativePath.removeSuffix(".java").removeSuffix(".kt").replace(File.separatorChar, '.')
					val (pluginNameCapitalized) = pluginNameRegex.find(pluginClass)!!.destructured
					val pluginName = pluginNameCapitalized.split(kebabCaseRegex).joinToString("-") { it.toLowerCase() } // Kebab-case plugin name
					val pluginId = "com.clifton.gradle.$pluginName"
					PluginDetails(pluginName, pluginId, pluginClass)
				}
				.toList()
	}


	private fun removeMatchingSrcDir(path: String, srcDirList: Iterable<String>): String {
		val matchingSrcDir = srcDirList
				.filter { path.startsWith(it) }
				.maxBy { it.length }!!
		return path.removePrefix("$matchingSrcDir${File.separatorChar}")
	}


	class PluginDetails(val name: String, val id: String, val clazz: String)
}
