package com.clifton.trade.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.accounting.commission.TradeCommissionOverride;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.trade.marketdata.TradeMarketDataFieldService;
import com.clifton.trade.marketdata.TradeMarketDataValue;
import com.clifton.trade.marketdata.search.TradeMarketDataValueSearchForm;
import com.clifton.trade.quote.TradeQuote;
import com.clifton.trade.quote.TradeQuoteService;
import com.clifton.trade.quote.search.TradeQuoteSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class TradeRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private InvestmentInstructionService investmentInstructionService;

	private RuleViolationService ruleViolationService;

	private SystemNoteService systemNoteService;

	private TradeService tradeService;
	private TradeCommissionService tradeCommissionService;
	private TradeMarketDataFieldService tradeMarketDataFieldService;
	private TradeQuoteService tradeQuoteService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof Trade;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		Trade trade = (Trade) entity;

		List<TradeFill> tradeFills = getTradeService().getTradeFillListByTrade(trade.getId());
		if (!CollectionUtils.isEmpty(tradeFills)) {
			relatedEntityList.addAll(tradeFills);
		}

		List<TradeCommissionOverride> commissionOverrides = getTradeCommissionService().getTradeCommissionOverrideListForTrade(trade.getId());
		if (!CollectionUtils.isEmpty(commissionOverrides)) {
			relatedEntityList.addAll(commissionOverrides);
		}

		TradeMarketDataValueSearchForm marketDataValueSearchForm = new TradeMarketDataValueSearchForm();
		marketDataValueSearchForm.setTradeId(trade.getId());
		List<TradeMarketDataValue> tradeValues = getTradeMarketDataFieldService().getTradeMarketDataValueList(marketDataValueSearchForm);
		if (!CollectionUtils.isEmpty(tradeValues)) {
			relatedEntityList.addAll(tradeValues);
		}

		List<InvestmentInstructionItem> instructionItems = getInvestmentInstructionService().getInvestmentInstructionItemListForEntityObject(trade);
		if (!CollectionUtils.isEmpty(instructionItems)) {
			relatedEntityList.addAll(instructionItems);
		}

		TradeQuoteSearchForm searchForm = new TradeQuoteSearchForm();
		searchForm.setTradeId(trade.getId());
		List<TradeQuote> tradeQuotes = getTradeQuoteService().getTradeQuoteList(searchForm);
		if (!CollectionUtils.isEmpty(tradeQuotes)) {
			relatedEntityList.addAll(tradeQuotes);
		}

		// Add in Rule Violations - should be a more generic way to do this (check tables for rule categories?)
		List<RuleViolation> ruleViolations = getRuleViolationService().getRuleViolationListByLinkedEntity(Trade.TABLE_NAME, trade.getId());
		if (!CollectionUtils.isEmpty(ruleViolations)) {
			relatedEntityList.addAll(ruleViolations);
		}

		// If part of a group, add it
		if (trade.getTradeGroup() != null) {
			relatedEntityList.add(trade.getTradeGroup());
		}

		// Notes - should be a more generic way to do this (check tables for note types?)
		SystemNoteSearchForm noteSearchForm = new SystemNoteSearchForm();
		noteSearchForm.setEntityTableName(Trade.TABLE_NAME);
		noteSearchForm.setFkFieldId(BeanUtils.getIdentityAsLong(trade));
		// Not adding for linked entities - could be messy if we have notes on accounts
		relatedEntityList.addAll(getSystemNoteService().getSystemNoteListForEntity(noteSearchForm));

		return relatedEntityList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionService getInvestmentInstructionService() {
		return this.investmentInstructionService;
	}


	public void setInvestmentInstructionService(InvestmentInstructionService investmentInstructionService) {
		this.investmentInstructionService = investmentInstructionService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}


	public TradeMarketDataFieldService getTradeMarketDataFieldService() {
		return this.tradeMarketDataFieldService;
	}


	public void setTradeMarketDataFieldService(TradeMarketDataFieldService tradeMarketDataFieldService) {
		this.tradeMarketDataFieldService = tradeMarketDataFieldService;
	}


	public TradeQuoteService getTradeQuoteService() {
		return this.tradeQuoteService;
	}


	public void setTradeQuoteService(TradeQuoteService tradeQuoteService) {
		this.tradeQuoteService = tradeQuoteService;
	}
}
