package com.clifton.trade.lifecycle;

import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.trade.TradeFill;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class TradeFillRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private AccountingJournalService accountingJournalService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof TradeFill;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		TradeFill fill = (TradeFill) entity;

		relatedEntityList.add(fill.getTrade());

		if (fill.getBookingDate() != null) {
			relatedEntityList.add(getAccountingJournalService().getAccountingJournalBySource(TradeFill.TABLE_NAME, fill.getId()));
		}

		return relatedEntityList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////

	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}

}
