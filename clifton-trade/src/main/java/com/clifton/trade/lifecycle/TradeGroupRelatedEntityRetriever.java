package com.clifton.trade.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.trade.group.TradeGroup;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class TradeGroupRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof TradeGroup;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();
		TradeGroup tradeGroup = (TradeGroup) entity;
		if (tradeGroup != null && tradeGroup.getSourceFkFieldId() != null) {
			ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(tradeGroup.getTradeGroupType().getSourceSystemTable().getName());
			IdentityObject linkedEntity = dao.findByPrimaryKey(tradeGroup.getSourceFkFieldId());
			if (linkedEntity != null) {
				relatedEntityList.add(linkedEntity);
			}
		}
		return relatedEntityList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
