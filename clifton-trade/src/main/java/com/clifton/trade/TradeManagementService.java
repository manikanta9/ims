package com.clifton.trade;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>TradeManagementService</code> defines methods for managing {@link Trade}s (e.g. reassigning trades).
 *
 * @author michaelm
 */
public interface TradeManagementService {

	////////////////////////////////////////////////////////////////////////////
	////////             Trade Action Execution Methods                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Performs a trade action based on the TradeListActionTypes value of the command parameter.
	 */
	@ModelAttribute("data")
	@RequestMapping("tradeListActionCommandExecute")
	@SecureMethod(dtoClass = Trade.class)
	public Status executeTradeListActionCommand(TradeListActionCommand command);
}
