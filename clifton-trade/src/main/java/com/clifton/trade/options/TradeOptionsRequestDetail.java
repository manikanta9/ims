package com.clifton.trade.options;


import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.TradeSource;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.execution.TradeExecutionType;

import java.math.BigDecimal;


/**
 * The <code>TradeOptionsRequestDetail</code> represents a single request detail for processing an option exercise or expiration. Specifically, it represents
 * a single option position and an associated price and optional commission.
 *
 * @author jgommels
 */
public class TradeOptionsRequestDetail {

	/**
	 * Optional {@link TradeSource} to be applied to a {@link com.clifton.trade.Trade} generated from this detail defining where it is being generated from.
	 * If the source is not defined here, it will inherit from {@link TradeOptionsRequest#getTradeSource()}.
	 */
	private TradeSource tradeSource;

	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentSecurity investmentSecurity;
	private BusinessCompany executingBroker;
	private TradeDestination tradeDestination;
	private TradeExecutionType tradeExecutionType;
	private BigDecimal limitPrice;

	private BigDecimal tradeQuantity;
	private BigDecimal averageUnitPrice;
	private BigDecimal commissionPerUnit;

	/**
	 * BTIC (Basis Trade at Index Close) trades have a different commission schedule than other Option trades. BTIC commissions
	 * are applied the same as Block Trades. Thus, this flag is used to define whether the trade generated from this detail
	 * should have it's block trade flag set.
	 */
	private boolean btic;

	/**
	 * The Option to roll a current position into to extend the position to a later expiration.
	 * Used for {@link com.clifton.trade.group.TradeGroupType.GroupTypes#OPTION_ROLL_FORWARD} TradeGroupTypes.
	 */
	private InvestmentSecurity rollSecurity;
	/**
	 * The quantity to roll. The client can provide a roll quantity or provide a ratio to calculate the quantity.
	 * Used for {@link com.clifton.trade.group.TradeGroupType.GroupTypes#OPTION_ROLL_FORWARD} TradeGroupTypes.
	 */
	private BigDecimal rollQuantity;
	/**
	 * The ratio to use for the rolled forward position.
	 * Used for {@link com.clifton.trade.group.TradeGroupType.GroupTypes#OPTION_ROLL_FORWARD} TradeGroupTypes.
	 */
	private BigDecimal rollRatio;
	/**
	 * When rolling options positions for client accounts using a service type of {@link com.clifton.business.service.ServiceProcessingTypes#SECURITY_TARGET},
	 * target utilization is validated to ensure the calculated roll trade quantity is <= the available quantity towards the underlying security target.
	 * All usage quantities (covered, frozen, and pending) are included in the available quantity. In some cases, the frozen quantity can be
	 * used to trade more than what would be possible otherwise.
	 */
	private boolean rollUsingFrozen;
	/**
	 * The quantity of the underlying security to trade for a roll of an option position. Only applicable if {@link #isTradeUnderlying()} is true.
	 */
	private BigDecimal underlyingTradeQuantity;
	/**
	 * Indicates whether the underlying trade is a buy or sell for the quantity of {@link #getUnderlyingTradeQuantity()}. Only applicable if {@link #isTradeUnderlying()} is true.
	 */
	private boolean underlyingTradeBuy;
	/**
	 * When rolling option positions for a client account. It is possible to trade the underlying security of the options to increase the client's available cash to facilitate the roll.
	 * When true and the quantities for rolling a position are present, create a trade for the underlying according to {@link #getUnderlyingTradeQuantity()}.
	 */
	private boolean tradeUnderlying;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeSource getTradeSource() {
		return this.tradeSource;
	}


	public void setTradeSource(TradeSource tradeSource) {
		this.tradeSource = tradeSource;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BusinessCompany getExecutingBroker() {
		return this.executingBroker;
	}


	public void setExecutingBroker(BusinessCompany executingBroker) {
		this.executingBroker = executingBroker;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public TradeExecutionType getTradeExecutionType() {
		return this.tradeExecutionType;
	}


	public void setTradeExecutionType(TradeExecutionType tradeExecutionType) {
		this.tradeExecutionType = tradeExecutionType;
	}


	public BigDecimal getLimitPrice() {
		return this.limitPrice;
	}


	public void setLimitPrice(BigDecimal limitPrice) {
		this.limitPrice = limitPrice;
	}


	/**
	 * @return {@link #tradeQuantity}
	 */

	public BigDecimal getTradeQuantity() {
		return this.tradeQuantity;
	}


	/**
	 * Sets {@link #tradeQuantity}.
	 */

	public void setTradeQuantity(BigDecimal tradeQuantity) {
		this.tradeQuantity = tradeQuantity;
	}


	public BigDecimal getAverageUnitPrice() {
		return this.averageUnitPrice;
	}


	public void setAverageUnitPrice(BigDecimal averageUnitPrice) {
		this.averageUnitPrice = averageUnitPrice;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public boolean isBtic() {
		return this.btic;
	}


	public void setBtic(boolean btic) {
		this.btic = btic;
	}


	public InvestmentSecurity getRollSecurity() {
		return this.rollSecurity;
	}


	public void setRollSecurity(InvestmentSecurity rollSecurity) {
		this.rollSecurity = rollSecurity;
	}


	public BigDecimal getRollQuantity() {
		return this.rollQuantity;
	}


	public void setRollQuantity(BigDecimal rollQuantity) {
		this.rollQuantity = rollQuantity;
	}


	public BigDecimal getRollRatio() {
		return this.rollRatio;
	}


	public void setRollRatio(BigDecimal rollRatio) {
		this.rollRatio = rollRatio;
	}


	public boolean isRollUsingFrozen() {
		return this.rollUsingFrozen;
	}


	public void setRollUsingFrozen(boolean rollUsingFrozen) {
		this.rollUsingFrozen = rollUsingFrozen;
	}


	public BigDecimal getUnderlyingTradeQuantity() {
		return this.underlyingTradeQuantity;
	}


	public void setUnderlyingTradeQuantity(BigDecimal underlyingTradeQuantity) {
		this.underlyingTradeQuantity = underlyingTradeQuantity;
	}


	public boolean isUnderlyingTradeBuy() {
		return this.underlyingTradeBuy;
	}


	public void setUnderlyingTradeBuy(boolean underlyingTradeBuy) {
		this.underlyingTradeBuy = underlyingTradeBuy;
	}


	public boolean isTradeUnderlying() {
		return this.tradeUnderlying;
	}


	public void setTradeUnderlying(boolean tradeUnderlying) {
		this.tradeUnderlying = tradeUnderlying;
	}
}
