package com.clifton.trade.options;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.group.TradeGroupType;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * <code>TradeOptionsContext</code> is an object that contains request details and context
 * information while processing trade actions for options position(s).
 *
 * @author NickK
 */
public class TradeOptionsContext {

	private final TradeOptionsRequest request;
	private final Date tradeDate;
	private final TradeOptionsOpenCloseTypeFunction tradeOpenCloseTypeFunction;
	private final TradeOptionsStrategy strategy;

	// Cache that can be populated for the Options used while processing this request
	private final Set<InvestmentSecurity> optionSet = new HashSet<>();
	// Cache that can be populated for the Big to Mini securities of an Option's underlying security
	private final Map<InvestmentSecurity, InvestmentSecurity> optionUnderlyingBigToMiniMap = new HashMap<>();

	///////////////////////////////////////////////////////////////////////////


	private TradeOptionsContext(Builder builder) {
		this.request = builder.request;
		this.tradeDate = builder.tradeDate;
		this.tradeOpenCloseTypeFunction = builder.tradeOpenCloseTypeFunction;
		this.strategy = builder.strategy;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeOptionsRequest getRequest() {
		return this.request;
	}


	public TradeGroupType getTradeGroupType() {
		return getRequest().getTradeGroupType();
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public Set<InvestmentSecurity> getOptionSet() {
		return this.optionSet;
	}


	public Map<InvestmentSecurity, InvestmentSecurity> getOptionUnderlyingBigToMiniMap() {
		return this.optionUnderlyingBigToMiniMap;
	}


	public TradeOptionsOpenCloseTypeFunction getTradeOpenCloseTypeFunction() {
		return this.tradeOpenCloseTypeFunction;
	}


	public TradeOptionsStrategy getStrategy() {
		return this.strategy;
	}


	public static Builder of(TradeOptionsRequest request, Date tradeDate) {
		return new Builder(request, tradeDate);
	}


	/**
	 * <code>Builder</code> is a builder for constructing a {@link TradeOptionsContext}
	 */
	public static class Builder {

		private final TradeOptionsRequest request;
		private final Date tradeDate;

		private TradeOptionsOpenCloseTypeFunction tradeOpenCloseTypeFunction;
		private TradeOptionsStrategy strategy;


		private Builder(TradeOptionsRequest request, Date tradeDate) {
			this.request = request;
			this.tradeDate = tradeDate;
		}


		public Builder setTradeOpenCloseTypeFunction(TradeOptionsOpenCloseTypeFunction tradeOpenCloseTypeFunction) {
			this.tradeOpenCloseTypeFunction = tradeOpenCloseTypeFunction;
			return this;
		}


		public Builder setStrategy(TradeOptionsStrategy strategy) {
			this.strategy = strategy;
			return this;
		}


		public TradeOptionsContext build() {
			ValidationUtils.assertNotNull(this.tradeOpenCloseTypeFunction, "Unable to create Options Processing Context without a Trade Open Close Type Function defined.");
			ValidationUtils.assertNotNull(this.strategy, "Unable to create Options Processing Context without a Option Processing Strategy defined.");
			return new TradeOptionsContext(this);
		}
	}

	/**
	 * <code>TradeOptionsOpenCloseTypeFunction</code> is a function to calculate the {@link TradeOpenCloseType} of a trade
	 * based on an {@link TradeOptionsPositionBalance} and when it is open or close.
	 */
	public static interface TradeOptionsOpenCloseTypeFunction {

		/**
		 * Returns the {@link TradeOpenCloseType} for the trade according to the provided position. The trade
		 * is generally for the Option of the position or the Options's underlying to perform Option actions
		 * such as closing, expiration, delivery, exercising, assignment, etc.
		 * <p>
		 * In some cases, an Option position is extended to a new Option with a later expiration date. Refer
		 * to {@link #apply(TradeOptionsPositionBalance, Trade, boolean)} in this case with an open value of true.
		 */
		default public TradeOpenCloseType apply(TradeOptionsPositionBalance position, Trade trade) {
			return apply(position, trade, false);
		}


		/**
		 * Returns the {@link TradeOpenCloseType} for the trade according to the provided position, and considers
		 * the open flag for an Option security that does not match the one used on the position. This is used
		 * primarily for rolling an option position forward.
		 */
		public TradeOpenCloseType apply(TradeOptionsPositionBalance position, Trade trade, boolean open);
	}
}
