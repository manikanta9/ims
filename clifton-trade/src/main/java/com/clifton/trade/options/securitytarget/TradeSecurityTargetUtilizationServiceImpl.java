package com.clifton.trade.options.securitytarget;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreeze;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetFreezeSearchForm;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import com.clifton.trade.options.securitytarget.search.TradeSecurityTargetUtilizationCommand;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTarget;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTargetUtilHandler;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author NickK
 */
@Service
public class TradeSecurityTargetUtilizationServiceImpl implements TradeSecurityTargetUtilizationService {

	private static final int TEN_HOURS_IN_SECONDS = 60 * 60 * 10;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingBalanceService accountingBalanceService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;
	private MarketDataLiveService marketDataLiveService;
	private TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<TradeSecurityTargetUtilization> getTradeSecurityTargetUtilizationList(TradeSecurityTargetUtilizationCommand command) {
		command.validate();

		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetList(command);
		return getTradeSecurityTargetUtilizationListForSecurityTargetList(command, securityTargetList);
	}


	@Override
	@Transactional(readOnly = true)
	public TradeSecurityTargetUtilization getTradeSecurityTargetUtilization(Integer investmentAccountSecurityTargetId, Date balanceDate, boolean usePriorPriceDate) {
		InvestmentAccountSecurityTarget securityTarget = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTarget(investmentAccountSecurityTargetId);
		ValidationUtils.assertNotNull(securityTarget, "Client Investment Security Target is not found.");

		// Get positions for client
		TradeSecurityTargetUtilizationCommand searchForm = new TradeSecurityTargetUtilizationCommand();
		searchForm.setClientInvestmentAccountId(securityTarget.getClientInvestmentAccount().getId());
		searchForm.setTargetUnderlyingSecurityId(securityTarget.getTargetUnderlyingSecurity().getId());
		searchForm.setBalanceDate(balanceDate);
		List<AccountingBalance> accountingBalanceList = getAccountingPositionListForClientAccountIds(searchForm, securityTarget.getClientInvestmentAccount().getId());

		// Get applicable security target freeze list
		List<InvestmentAccountSecurityTargetFreeze> securityTargetFreezeList = getInvestmentAccountSecurityTargetFreezeListForSecurityTargets(balanceDate, new Integer[]{securityTarget.getClientInvestmentAccount().getId()}, new Integer[]{securityTarget.getTargetUnderlyingSecurity().getId()});

		TargetUtilizationContext context = new TargetUtilizationContext(securityTarget, balanceDate, usePriorPriceDate ? DateUtils.getPreviousWeekday(balanceDate) : balanceDate)
				.withAccountingBalanceList(accountingBalanceList)
				.withSecurityTargetFreezeList(securityTargetFreezeList);

		return createSecurityTargetUtilization(context);
	}


	@Override
	@Transactional(readOnly = true)
	public List<TradeSecurityTargetUtilization> getTradeSecurityTargetUtilizationListForDateRange(int clientInvestmentAccountId, Integer underlyingSecurityId, Date rangeStartDate, Date rangeEndDate) {
		ValidationUtils.assertTrue(rangeStartDate != null && rangeEndDate != null, "Both Range Start Date and Range End Date are required to process historic utilization.");
		ValidationUtils.assertTrue(DateUtils.isDateAfter(rangeEndDate, rangeStartDate), "The range end date must be after the range start date.");
		ValidationUtils.assertFalse(DateUtils.getMonthsDifference(rangeEndDate, rangeStartDate) > 12, "Only able to view the past 12 months of usage history at a time.");

		InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
		securityTargetSearchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		securityTargetSearchForm.setTargetUnderlyingSecurityId(underlyingSecurityId);
		securityTargetSearchForm.setActiveOnDateRangeStartDate(rangeStartDate);
		securityTargetSearchForm.setActiveOnDateRangeEndDate(rangeEndDate);
		securityTargetSearchForm.setOrderBy("startDate:ASC");
		List<InvestmentAccountSecurityTarget> historicSecurityTargetList = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(securityTargetSearchForm);

		List<TradeSecurityTargetUtilization> results = new ArrayList<>();
		if (!CollectionUtils.isEmpty(historicSecurityTargetList)) {
			Date balanceDate = DateUtils.clearTime(rangeStartDate);

			// Map security targets by underlying security, then create a list iterator for each entry value
			Map<Integer, List<InvestmentAccountSecurityTarget>> securityToTargetListMap = BeanUtils.getBeansMap(historicSecurityTargetList, target -> target.getTargetUnderlyingSecurity().getId());
			List<ListIterator<InvestmentAccountSecurityTarget>> securityTargetIteratorList = CollectionUtils.getConverted(securityToTargetListMap.values(), List::listIterator);
			// traverse each underlying security target iterator for balance date in provided range adding usage details
			while (DateUtils.isDateBefore(balanceDate, rangeEndDate, false)) {
				List<InvestmentAccountSecurityTarget> applicableSecurityTargetList = new ArrayList<>();
				for (ListIterator<InvestmentAccountSecurityTarget> securityTargetIterator : securityTargetIteratorList) {
					if (!securityTargetIterator.hasNext()) {
						// no more security targets active in range, continue
						continue;
					}
					InvestmentAccountSecurityTarget securityTarget = securityTargetIterator.next();
					if (DateUtils.isDateBetween(balanceDate, securityTarget.getStartDate(), securityTarget.getEndDate(), false)) {
						applicableSecurityTargetList.add(securityTarget);
					}
					// reset cursor for next pass and continue
					securityTargetIterator.previous();
				}

				if (!applicableSecurityTargetList.isEmpty()) {
					TradeSecurityTargetUtilizationCommand command = new TradeSecurityTargetUtilizationCommand();
					command.setBalanceDate(balanceDate);
					command.setExcludeCashBalance(true);
					command.setExcludePendingActivity(true);
					results.addAll(getTradeSecurityTargetUtilizationListForSecurityTargetList(command, applicableSecurityTargetList));
				}
				balanceDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(balanceDate), 1);
			}
		}
		return results;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(TradeSecurityTargetUtilizationCommand command) {
		InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
		BeanUtils.copyProperties(command, securityTargetSearchForm);
		securityTargetSearchForm.setActiveOnDate(command.getBalanceDate());
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(securityTargetSearchForm);
	}


	private List<AccountingBalance> getAccountingPositionListForClientAccountIds(TradeSecurityTargetUtilizationCommand command, Integer... clientAccountIds) {
		AccountingBalanceSearchForm accountingBalanceSearchForm = AccountingBalanceSearchForm.onTransactionDate(command.getBalanceDate());
		accountingBalanceSearchForm.setUnderlyingSecurityId(command.getTargetUnderlyingSecurityId());
		if (clientAccountIds != null && clientAccountIds.length == 1) {
			accountingBalanceSearchForm.setClientInvestmentAccountId(clientAccountIds[0]);
		}
		else {
			accountingBalanceSearchForm.setClientInvestmentAccountIds(clientAccountIds);
		}
		accountingBalanceSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_COLLATERAL_AND_RECEIVABLE);
		if (!command.isExcludePendingActivity()) {
			accountingBalanceSearchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_PREVIEW);
		}
		return getAccountingBalanceService().getAccountingBalanceList(accountingBalanceSearchForm);
	}


	private List<InvestmentAccountSecurityTargetFreeze> getInvestmentAccountSecurityTargetFreezeListForSecurityTargets(Date balanceDate, Integer[] clientInvestmentAccountIds, Integer[] underlyingSecurityIds) {
		InvestmentAccountSecurityTargetFreezeSearchForm freezeSearchForm = new InvestmentAccountSecurityTargetFreezeSearchForm();
		if (clientInvestmentAccountIds != null) {
			if (clientInvestmentAccountIds.length == 1) {
				freezeSearchForm.setClientInvestmentAccountId(clientInvestmentAccountIds[0]);
			}
			else {
				freezeSearchForm.setClientInvestmentAccountIds(clientInvestmentAccountIds);
			}
		}
		if (underlyingSecurityIds != null) {
			if (underlyingSecurityIds.length == 1) {
				freezeSearchForm.setTargetUnderlyingSecurityId(underlyingSecurityIds[0]);
			}
			else {
				freezeSearchForm.setTargetUnderlyingSecurityIds(underlyingSecurityIds);
			}
		}
		freezeSearchForm.setActiveOnDate(balanceDate);
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetFreezeList(freezeSearchForm);
	}


	private List<TradeSecurityTargetUtilization> getTradeSecurityTargetUtilizationListForSecurityTargetList(TradeSecurityTargetUtilizationCommand command, List<InvestmentAccountSecurityTarget> securityTargetList) {
		if (CollectionUtils.isEmpty(securityTargetList)) {
			return Collections.emptyList();
		}

		// Map security targets by client account
		Map<InvestmentAccount, List<InvestmentAccountSecurityTarget>> clientSecurityTargetListMap = BeanUtils.getBeansMap(securityTargetList, InvestmentAccountSecurityTarget::getClientInvestmentAccount);
		Integer[] clientInvestmentAccountIds = BeanUtils.getBeanIdentityArray(clientSecurityTargetListMap.keySet(), Integer.class);

		// Get positions for applicable client accounts; pending included
		List<AccountingBalance> positionList = getAccountingPositionListForClientAccountIds(command, clientInvestmentAccountIds);
		Map<InvestmentAccount, List<AccountingBalance>> clientPositionListMap = BeanUtils.getBeansMap(positionList, AccountingBalance::getClientInvestmentAccount);

		// Get security target frozen allocations
		Integer[] underlyingSecurityIds = command.getTargetUnderlyingSecurityId() != null ? new Integer[]{command.getTargetUnderlyingSecurityId()}
				: securityTargetList.stream().map(target -> target.getTargetUnderlyingSecurity().getId()).toArray(Integer[]::new);
		List<InvestmentAccountSecurityTargetFreeze> securityTargetFreezeList = getInvestmentAccountSecurityTargetFreezeListForSecurityTargets(command.getBalanceDate(), clientInvestmentAccountIds, underlyingSecurityIds);
		Map<Integer, List<InvestmentAccountSecurityTargetFreeze>> clientSecurityTargetFreezeListMap = new HashMap<>();
		securityTargetList.forEach(target -> {
			List<InvestmentAccountSecurityTargetFreeze> freezesForCurrentTarget = new ArrayList<>();
			securityTargetFreezeList.forEach(freeze -> {
				if (target.getTargetUnderlyingSecurity().getId().equals(freeze.getTargetUnderlyingSecurity().getId()) && target.getClientInvestmentAccount().getId().equals(freeze.getClientInvestmentAccount().getId())) {
					freezesForCurrentTarget.add(freeze);
				}
			});
			clientSecurityTargetFreezeListMap.put(target.getId(), freezesForCurrentTarget);
		});

		Date priceDate = command.isUsePriorPriceDate()
				? DateUtils.getPreviousWeekday(command.getBalanceDate())
				: command.getBalanceDate();

		List<TradeSecurityTargetUtilization> utilizationList = new ArrayList<>();

		AccountingBalanceCommand accountingBalanceCommand = AccountingBalanceCommand.forClientAccountsOnTransactionDate(clientInvestmentAccountIds, command.getBalanceDate());
		accountingBalanceCommand.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS);

		Map<Integer, BigDecimal> clientCashBalanceMap = command.isExcludeCashBalance() ? Collections.emptyMap() : getAccountingBalanceService().getAccountingCashBalanceMap(accountingBalanceCommand);
		clientSecurityTargetListMap.forEach((clientAccount, clientSecurityTargetList) -> {
			List<AccountingBalance> clientPositionList = clientPositionListMap.get(clientAccount);
			// Group positions by underlying security for security target matching
			Map<Integer, List<AccountingBalance>> underlyingSecurityIdToAccountingBalanceListMap = CollectionUtils.getStream(clientPositionList)
					.filter(balance -> balance.getInvestmentSecurity().getUnderlyingSecurity() != null)
					.collect(Collectors.groupingBy(balance -> balance.getInvestmentSecurity().getUnderlyingSecurity().getId()));

			CollectionUtils.getStream(clientSecurityTargetList)
					.forEach(securityTarget -> {
						TargetUtilizationContext context = new TargetUtilizationContext(securityTarget, command.getBalanceDate(), priceDate)
								.withCashBalance(clientCashBalanceMap.get(clientAccount.getId()))
								.withAccountingBalanceList(underlyingSecurityIdToAccountingBalanceListMap.get(securityTarget.getTargetUnderlyingSecurity().getId()))
								.withSecurityTargetFreezeList(clientSecurityTargetFreezeListMap.get(securityTarget.getId()))
								.withIncludeMarketData(command.isIncludeMarketData())
								.withHoldingAccountPurpose(command.getHoldingAccountPurpose());
						utilizationList.add(createSecurityTargetUtilization(context));
					});
		});


		return utilizationList;
	}


	private TradeSecurityTargetUtilization createSecurityTargetUtilization(TargetUtilizationContext context) {
		InvestmentAccountSecurityTarget securityTarget = context.securityTarget;
		TradeSecurityTarget tradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDate(securityTarget, context.priceDate);

		TradeSecurityTargetUtilization.Builder utilizationBuilder = TradeSecurityTargetUtilization.newBuilder(securityTarget, context.balanceDate)
				.withUnderlyingSecurityPrice(tradeSecurityTarget.getUnderlyingSecurityPrice())
				.withUnderlyingSecurityTargetQuantity(tradeSecurityTarget.getTargetUnderlyingQuantity())
				.withUnderlyingSecurityTargetNotional(tradeSecurityTarget.getTargetUnderlyingNotional())
				.withCashBalance(context.getCashBalance());

		populateTradeSecurityTargetUtilizationBuilderWithHoldingAccount(context, utilizationBuilder);

		context.getApplicableSecuritySet().forEach(security -> {
			List<AccountingBalance> balanceList = context.getAccountingBalanceListForSecurity(security);
			List<InvestmentAccountSecurityTargetFreeze> freezeList = context.getSecurityTargetFreezeListForSecurity(security);

			// calculate utilization quantities from positions and frozen allocations
			BigDecimal securityCoveredQuantity = BigDecimal.ZERO;
			BigDecimal securityPendingQuantity = BigDecimal.ZERO;
			for (AccountingBalance securityBalance : CollectionUtils.getIterable(balanceList)) {
				if (securityBalance.isPendingActivity()) {
					securityPendingQuantity = MathUtils.add(securityPendingQuantity, securityBalance.getQuantity());
				}
				else {
					securityCoveredQuantity = MathUtils.add(securityCoveredQuantity, securityBalance.getQuantity());
				}
			}
			BigDecimal underlyingFrozenQuantity = CoreMathUtils.sumProperty(freezeList, InvestmentAccountSecurityTargetFreeze::getFreezeUnderlyingQuantity);

			// populate utilization details
			TradeSecurityTargetUtilizationDetail detail = TradeSecurityTargetUtilizationDetail.of(security);
			if (!MathUtils.isNullOrZero(securityCoveredQuantity)) {
				BigDecimal underlyingCoveredQuantity = MathUtils.multiply(securityCoveredQuantity, security.getPriceMultiplier());
				if (!MathUtils.isNullOrZero(underlyingFrozenQuantity)) {
					// Set covered quantity and adjust the frozen to the difference from covered
					detail.withCoveredQuantity(underlyingCoveredQuantity)
							.withFrozenQuantity(underlyingFrozenQuantity);
				}
				else {
					detail.withCoveredQuantity(underlyingCoveredQuantity);
				}
			}
			else if (!MathUtils.isNullOrZero(underlyingFrozenQuantity)) {
				detail.withFrozenQuantity(underlyingFrozenQuantity);
			}

			// pending will always be applied in addition to covered and/or frozen
			if (!MathUtils.isNullOrZero(securityPendingQuantity)) {
				detail.withPendingQuantity(MathUtils.multiply(securityPendingQuantity, security.getPriceMultiplier()));
			}

			utilizationBuilder.addDetail(detail);
		});

		BigDecimal additionalFreezeQuantity = context.getSecurityTargetFreezeQuantityWithNoCauseSecurity();
		if (!MathUtils.isNullOrZero(additionalFreezeQuantity)) {
			utilizationBuilder.addDetail(TradeSecurityTargetUtilizationDetail.ofFrozenUnderlyingQuantity(additionalFreezeQuantity));
		}

		if (context.isIncludeMarketData()) {
			populateUnderlyingPriceChangeValues(utilizationBuilder);
		}

		return utilizationBuilder.build();
	}


	private void populateTradeSecurityTargetUtilizationBuilderWithHoldingAccount(TargetUtilizationContext context, TradeSecurityTargetUtilization.Builder utilizationBuilder) {
		InvestmentAccount clientAccount = context.securityTarget.getClientInvestmentAccount();
		Optional.ofNullable(context.getHoldingAccountPurpose())
				.map(NamedEntityWithoutLabel::getName)
				.filter(purposeName -> !StringUtils.isEmpty(purposeName))
				.ifPresent(purposeName -> {
					InvestmentAccount holdingAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurpose(clientAccount.getId(), purposeName, context.balanceDate);
					utilizationBuilder.withHoldingInvestmentAcocunt(holdingAccount);
				});
	}


	private void populateUnderlyingPriceChangeValues(TradeSecurityTargetUtilization.Builder targetUtilizationBuilder) {
		InvestmentSecurity targetSecurity = targetUtilizationBuilder.getTargetUnderlyingSecurity();

		MarketDataLiveCommand command = new MarketDataLiveCommand();
		command.setSecurityIds(targetSecurity.getId());
		String[] fieldNames = getMarketDataFieldNames(targetSecurity);
		command.setFieldNames(fieldNames);
		command.setMaxAgeInSeconds(TEN_HOURS_IN_SECONDS);
		command.setProviderOverride(MarketDataProviderOverrides.BPIPE);

		List<MarketDataLive> liveValueList = getMarketDataLiveService().getMarketDataLiveFieldValuesWithCommand(command);
		for (MarketDataLive liveValue : liveValueList) {
			switch (liveValue.getFieldName()) {
				case MarketDataField.FIELD_PRICE_CHANGE_PERCENT_1DAY: {
					targetUtilizationBuilder.withPriceChangePercentDaily(liveValue.asNumericValue());
					break;
				}
				case MarketDataField.FIELD_PRICE_CHANGE_PERCENT_YTD: {
					targetUtilizationBuilder.withPriceChangePercentYTD(liveValue.asNumericValue());
					break;
				}
				case MarketDataField.FIELD_EARNINGS_REPORT_DATE: {
					targetUtilizationBuilder.withEarningsDate((Date) liveValue.getValue());
					break;
				}
				default: {
					// should not get here; unknown, ignore
				}
			}
		}
	}


	private String[] getMarketDataFieldNames(InvestmentSecurity security) {
		return isEarningsDateApplicableForSecurity(security)
				? new String[]{MarketDataField.FIELD_PRICE_CHANGE_PERCENT_1DAY, MarketDataField.FIELD_PRICE_CHANGE_PERCENT_YTD, MarketDataField.FIELD_EARNINGS_REPORT_DATE}
				: new String[]{MarketDataField.FIELD_PRICE_CHANGE_PERCENT_1DAY, MarketDataField.FIELD_PRICE_CHANGE_PERCENT_YTD};
	}


	private boolean isEarningsDateApplicableForSecurity(InvestmentSecurity security) {
		return InvestmentUtils.isSecurityOfType(security, InvestmentType.STOCKS);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * <code>TargetUtilizationContext</code> represents a data holder for positions and freeze allocations applicable to a
	 * {@link InvestmentAccountSecurityTarget} for a specific date and is used to calculate covered, frozen, and pending utilization amounts.
	 */
	private static class TargetUtilizationContext {

		private final InvestmentAccountSecurityTarget securityTarget;
		private final Date balanceDate;
		private final Date priceDate;
		private InvestmentAccountRelationshipPurpose holdingAccountPurpose;
		private BigDecimal cashBalance;
		private Map<InvestmentSecurity, List<AccountingBalance>> securityToAccountingBalanceListMap = new HashMap<>();
		private Map<InvestmentSecurity, List<InvestmentAccountSecurityTargetFreeze>> securityToSecurityTargetFreezeListMap = new HashMap<>();
		private List<InvestmentAccountSecurityTargetFreeze> underlyingFreezeListWithNoCauseSecurity = new ArrayList<>();
		private Set<InvestmentSecurity> securitySet = new HashSet<>();
		private boolean includeMarketData;


		TargetUtilizationContext(InvestmentAccountSecurityTarget securityTarget, Date balanceDate, Date priceDate) {
			this.securityTarget = securityTarget;
			this.balanceDate = balanceDate;
			this.priceDate = priceDate;
		}


		TargetUtilizationContext withHoldingAccountPurpose(InvestmentAccountRelationshipPurpose holdingAccountPurpose) {
			this.holdingAccountPurpose = holdingAccountPurpose;
			return this;
		}


		TargetUtilizationContext withCashBalance(BigDecimal cashBalance) {
			this.cashBalance = cashBalance;
			return this;
		}


		TargetUtilizationContext withIncludeMarketData(boolean includeMarketData) {
			this.includeMarketData = includeMarketData;
			return this;
		}


		TargetUtilizationContext withAccountingBalanceList(List<AccountingBalance> balanceList) {
			this.securityToAccountingBalanceListMap.putAll(BeanUtils.getBeansMap(balanceList, AccountingBalance::getInvestmentSecurity));
			this.securitySet.addAll(this.securityToAccountingBalanceListMap.keySet());
			return this;
		}


		TargetUtilizationContext withSecurityTargetFreezeList(List<InvestmentAccountSecurityTargetFreeze> freezeList) {
			for (InvestmentAccountSecurityTargetFreeze freeze : CollectionUtils.getIterable(freezeList)) {
				if (freeze.getCauseInvestmentSecurity() == null) {
					this.underlyingFreezeListWithNoCauseSecurity.add(freeze);
				}
				else {
					CollectionUtils.getValue(this.securityToSecurityTargetFreezeListMap, freeze.getCauseInvestmentSecurity(), ArrayList::new).add(freeze);
				}
			}
			this.securitySet.addAll(this.securityToSecurityTargetFreezeListMap.keySet());
			return this;
		}


		Set<InvestmentSecurity> getApplicableSecuritySet() {
			return this.securitySet;
		}


		List<AccountingBalance> getAccountingBalanceListForSecurity(InvestmentSecurity security) {
			return this.securityToAccountingBalanceListMap.get(security);
		}


		List<InvestmentAccountSecurityTargetFreeze> getSecurityTargetFreezeListForSecurity(InvestmentSecurity security) {
			return this.securityToSecurityTargetFreezeListMap.get(security);
		}


		BigDecimal getSecurityTargetFreezeQuantityWithNoCauseSecurity() {
			return CoreMathUtils.sumProperty(this.underlyingFreezeListWithNoCauseSecurity, InvestmentAccountSecurityTargetFreeze::getFreezeUnderlyingQuantity);
		}


		BigDecimal getCashBalance() {
			return this.cashBalance;
		}


		InvestmentAccountRelationshipPurpose getHoldingAccountPurpose() {
			return this.holdingAccountPurpose;
		}


		boolean isIncludeMarketData() {
			return this.includeMarketData;
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public TradeSecurityTargetUtilHandler getTradeSecurityTargetUtilHandler() {
		return this.tradeSecurityTargetUtilHandler;
	}


	public void setTradeSecurityTargetUtilHandler(TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler) {
		this.tradeSecurityTargetUtilHandler = tradeSecurityTargetUtilHandler;
	}
}
