package com.clifton.trade.options;


import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>TradeOptionsPositionBalance</code> represents an option position.
 *
 * @author jgommels
 */
public class TradeOptionsPositionBalance {

	/**
	 * The balance information of the option position.
	 */
	private final AccountingBalance positionBalance;

	private final BusinessCompany executingBroker;

	/**
	 * The current size of the position. Will include any current trades (even if they aren't executed yet). In other words, this value reflects the number of contracts left that have not been
	 * processed for expiration, and the sign indicates whether it is a long or short position.
	 */
	private final BigDecimal positionQuantity;

	/**
	 * The number of contracts left that have not been processed for delivery. This field is not applicable for cash-settled options, and will be <code>null</code> when this is the case.
	 */
	private final BigDecimal numRemainingContractsToDeliver;

	/**
	 * The number of contracts left that have not been processed to offset the delivery. This field is not applicable for cash-settled options, and will be <code>null</code> when this is the case.
	 */
	private final BigDecimal numRemainingContractsToOffsetDelivery;


	// The strike price for the option of this position balance.
	private final BigDecimal optionStrikePrice;

	// The type of the option of this position balance (PUT or CALL).
	private final InvestmentSecurityOptionTypes optionType;


	public TradeOptionsPositionBalance(AccountingBalance positionBalance, BigDecimal numRemainingContractsToExpire, BigDecimal numRemainingContractsToDeliver,
	                                   BigDecimal numRemainingContractsToOffsetDelivery) {
		this(positionBalance, numRemainingContractsToExpire, numRemainingContractsToDeliver, numRemainingContractsToOffsetDelivery, null);
	}


	public TradeOptionsPositionBalance(AccountingBalance positionBalance, BigDecimal numRemainingContractsToExpire, BigDecimal numRemainingContractsToDeliver,
	                                   BigDecimal numRemainingContractsToOffsetDelivery, BusinessCompany defaultExecutingBroker) {
		this.positionBalance = positionBalance;
		this.executingBroker = defaultExecutingBroker == null ? positionBalance.getHoldingInvestmentAccount().getIssuingCompany() : defaultExecutingBroker;
		this.positionQuantity = numRemainingContractsToExpire;
		this.numRemainingContractsToDeliver = numRemainingContractsToDeliver;
		this.numRemainingContractsToOffsetDelivery = numRemainingContractsToOffsetDelivery;
		this.optionStrikePrice = positionBalance.getInvestmentSecurity().getOptionStrikePrice();
		this.optionType = positionBalance.getInvestmentSecurity().getOptionType();
	}


	/**
	 * @return the number of contracts needed to be traded to close the position
	 */
	public BigDecimal getTradeQuantityForExpire() {
		return this.positionQuantity.abs();
	}


	/**
	 * @return the remaining trade quantity needed to deliver or offset the delivery (whichever is higher - used as a default in the UI)
	 */
	public BigDecimal getTradeQuantityForDeliveryOrOffset() {
		return MathUtils.max(this.numRemainingContractsToDeliver, this.numRemainingContractsToOffsetDelivery);
	}


	/**
	 * @return {@link #positionBalance}
	 */
	public AccountingBalance getPositionBalance() {
		return this.positionBalance;
	}


	/**
	 * @return {@link #executingBroker}
	 */

	public BusinessCompany getExecutingBroker() {
		return this.executingBroker;
	}


	/**
	 * @return {@link #positionQuantity}
	 */
	public BigDecimal getPositionQuantity() {
		return this.positionQuantity;
	}


	/**
	 * @return {@link #numRemainingContractsToDeliver}
	 */
	public BigDecimal getNumRemainingContractsToDeliver() {
		return this.numRemainingContractsToDeliver;
	}


	/**
	 * @return {@link #numRemainingContractsToOffsetDelivery}
	 */
	public BigDecimal getNumRemainingContractsToOffsetDelivery() {
		return this.numRemainingContractsToOffsetDelivery;
	}


	/**
	 * Returns the strike price for the security of this position balance.
	 */
	public BigDecimal getOptionStrikePrice() {
		return this.optionStrikePrice;
	}


	/**
	 * Returns the option type of the security of this position balance (PUT or CALL).
	 */
	public InvestmentSecurityOptionTypes getOptionType() {
		return this.optionType;
	}


	@Override
	public String toString() {
		return "TradeOptionsPositionBalance [positionBalance=" + this.positionBalance + ", executingBroker=" + this.executingBroker + ", positionQuantity=" + this.positionQuantity
				+ ", numRemainingContractsToDeliver=" + this.numRemainingContractsToDeliver + ", numRemainingContractsToOffsetDelivery=" + this.numRemainingContractsToOffsetDelivery + "]";
	}
}
