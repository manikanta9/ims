package com.clifton.trade.options.impl;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.options.TradeOptionsContext;
import com.clifton.trade.options.TradeOptionsPositionBalance;
import com.clifton.trade.options.TradeOptionsRequestDetail;
import com.clifton.trade.options.TradeOptionsStrategy;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * <code>AbstractTradeOptionsStrategy</code> is an abstract {@link TradeOptionsStrategy} containing
 * commonly used services and methods to assist in processing for a strategy.
 *
 * @author NickK
 */
public abstract class AbstractTradeOptionsStrategy implements TradeOptionsStrategy {

	private TradeService tradeService;
	private InvestmentCalculatorService investmentCalculatorService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	///////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isOptionUnderlyingBigToMiniMapRequired() {
		return false;
	}


	@Override
	public void validateTradeList(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		boolean valid;
		if (isSingleTradePerPosition()) {
			Trade trade = CollectionUtils.getOnlyElementStrict(tradeList);
			valid = isSingleTradeForPositionValid(position, trade);
			if (valid) {
				applySingleTradeModificationsForPosition(context, position, trade);
			}
		}
		else {
			valid = isTradeListForPositionValid(position, tradeList);
			if (valid) {
				applyTradeListModificationsForPosition(context, position, tradeList);
			}
		}

		ValidationUtils.assertTrue(valid, "Could not process the selected option position(s) because the associated trade(s) have already been created and are in a Pending state.");
	}


	@Override
	public InvestmentSecurity getTradeGroupSecondaryInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return null;
	}


	///////////////////////////////////////////////////////////////////////////
	///////       Strategy With Single Trade Validation Methods       /////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if this strategy generates a single Trade for a trade Option position.
	 * <p>
	 * Returns true by default.
	 */
	protected boolean isSingleTradePerPosition() {
		return true;
	}


	/**
	 * Validates a single generated trade against a {@link TradeOptionsPositionBalance} when {@link #isSingleTradePerPosition()} is true.
	 * <p>
	 *
	 * @throws UnsupportedOperationException by default to force override
	 */
	@SuppressWarnings("unused")
	protected boolean isSingleTradeForPositionValid(TradeOptionsPositionBalance position, Trade trade) {
		throw new UnsupportedOperationException("A single Trade was generated for an Option position and requires validation.");
	}


	/**
	 * Applies any changes to a single generated trade when {@link #isSingleTradePerPosition()} is true.
	 * Each trade's {@link com.clifton.trade.TradeOpenCloseType} must be defined at a minimum.
	 * <p>
	 *
	 * @throws UnsupportedOperationException by default to force override
	 */
	protected void applySingleTradeModificationsForPosition(TradeOptionsContext context, TradeOptionsPositionBalance position, Trade trade) {
		throw new UnsupportedOperationException("A single Trade was generated for an Option position and requires the Trade Open Close Type to be defined.");
	}

	///////////////////////////////////////////////////////////////////////////
	//////       Strategy With Multiple Trades Validation Methods      ////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Validates a list of generated trades against a {@link TradeOptionsPositionBalance} when {@link #isSingleTradePerPosition()} is false.
	 * <p>
	 *
	 * @throws UnsupportedOperationException by default to force override
	 */
	@SuppressWarnings("unused")
	protected boolean isTradeListForPositionValid(TradeOptionsPositionBalance position, List<Trade> tradeList) {
		throw new UnsupportedOperationException("A Trade list was generated for an Option position and each requires validation.");
	}


	/**
	 * Applies any changes to a list of generated trades when {@link #isSingleTradePerPosition()} is false.
	 * Each trade's {@link com.clifton.trade.TradeOpenCloseType} must be defined at a minimum.
	 * <p>
	 *
	 * @throws UnsupportedOperationException by default to force override
	 */
	protected void applyTradeListModificationsForPosition(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		throw new UnsupportedOperationException("A Trade list was generated for an Option position and each requires the Trade Open Close Type to be defined.");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a Trade using information from the arguments.
	 */
	protected Trade generateTrade(TradeOptionsContext context, TradeOptionsRequestDetail detail, InvestmentSecurity security, BigDecimal price) {
		InvestmentSecurity settlementCurrency = InvestmentUtils.getSettlementCurrency(security);

		Trade trade = new Trade();
		trade.setTradeType(getTradeService().getTradeTypeForSecurity(security.getId()));
		trade.setTradeSource(detail.getTradeSource());
		trade.setClientInvestmentAccount(detail.getClientInvestmentAccount());
		trade.setHoldingInvestmentAccount(detail.getHoldingInvestmentAccount());
		trade.setInvestmentSecurity(security);
		trade.setExecutingBrokerCompany(detail.getExecutingBroker());
		trade.setTradeExecutionType(detail.getTradeExecutionType());
		trade.setLimitPrice(detail.getLimitPrice());
		trade.setPayingSecurity(settlementCurrency);
		trade.setTradeDestination(ObjectUtils.coalesce(detail.getTradeDestination(), context.getRequest().getTradeDestination()));
		trade.setQuantityIntended(detail.getTradeQuantity());
		trade.setAverageUnitPrice(price);
		trade.setCommissionPerUnit(detail.getCommissionPerUnit());
		trade.setTradeDate(context.getTradeDate());
		trade.setSettlementDate(getInvestmentCalculatorService().getInvestmentSecuritySettlementDate(security.getId(), settlementCurrency.getId(), context.getTradeDate()));
		trade.setTraderUser(context.getRequest().getTraderUser());
		trade.setBlockTrade(detail.isBtic());

		return trade;
	}


	/**
	 * Returns true if the security of the position is a Call Option.
	 */
	protected boolean isPositionCallOption(TradeOptionsPositionBalance position) {
		AssertUtils.assertNotNull(position.getPositionBalance().getInvestmentSecurity().getOptionType(), "The 'Put or Call' value for the instrument of " + position.getPositionBalance().getInvestmentSecurity().getLabel() + " is not properly set.");
		return position.getPositionBalance().getInvestmentSecurity().isCallOption();
	}


	/**
	 * Returns an underlying trade (if isTradeUnderlying property in the associated with the option trade in the detail entity.
	 */
	protected List<Trade> addUnderlyingTrade(TradeOptionsContext context, TradeOptionsRequestDetail detail, List<Trade> tradeList) {
		if (MathUtils.isGreaterThan(detail.getUnderlyingTradeQuantity(), BigDecimal.ZERO)) {
			Trade underlyingTrade = generateTrade(context, detail, detail.getInvestmentSecurity().getUnderlyingSecurity(), null);
			underlyingTrade.setBuy(detail.isUnderlyingTradeBuy());
			underlyingTrade.setQuantityIntended(detail.getUnderlyingTradeQuantity());
			// Clear out not applicable values
			underlyingTrade.setTradeExecutionType(null);
			underlyingTrade.setLimitPrice(null);
			tradeList.add(underlyingTrade);
		}
		return tradeList;
	}


	/**
	 * Returns a map of trades partitioned by Option trades vs. other types of trades, such as Stock trades.
	 */
	protected Map<Boolean, List<Trade>> getOptionTradeListMap(List<Trade> tradeList) {
		List<Trade> validTrades = CollectionUtils.getFiltered(tradeList, Objects::nonNull);
		Map<Boolean, List<Trade>> optionTradeListMap = CollectionUtils.getPartitioned(validTrades, trade -> InvestmentUtils.isOption(trade.getInvestmentSecurity()));
		return optionTradeListMap;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}
}
