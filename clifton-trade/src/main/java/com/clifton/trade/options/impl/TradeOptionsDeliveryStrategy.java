package com.clifton.trade.options.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.options.TradeOptionsContext;
import com.clifton.trade.options.TradeOptionsPositionBalance;
import com.clifton.trade.options.TradeOptionsRequestDetail;
import com.clifton.trade.options.TradeOptionsStrategy;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;


/**
 * <code>TradeOptionsDeliveryStrategy</code> is a {@link TradeOptionsStrategy} for processing physical delivery
 * of Option positions expiring In the Money.
 *
 * @author NickK
 */
public class TradeOptionsDeliveryStrategy extends AbstractTradeOptionsStrategy {

	@Override
	public List<Trade> generateTradeList(TradeOptionsContext context, TradeOptionsRequestDetail detail) {
		InvestmentSecurity security = detail.getInvestmentSecurity();
		BigDecimal strikePrice = security.getOptionStrikePrice();
		return MathUtils.isNullOrZero(detail.getTradeQuantity()) ? Collections.emptyList()
				: CollectionUtils.createList(generateTrade(context, detail, security.getUnderlyingSecurity(), strikePrice));
	}


	@Override
	protected boolean isSingleTradeForPositionValid(TradeOptionsPositionBalance position, Trade trade) {
		return MathUtils.isLessThanOrEqual(trade.getQuantityIntended(), position.getNumRemainingContractsToDeliver());
	}


	@Override
	protected void applySingleTradeModificationsForPosition(TradeOptionsContext context, TradeOptionsPositionBalance position, Trade trade) {
		// Use the position balance option and quantity for the buy condition because it includes the sign for direction
		// Option Physical Delivery results in a buy if the position is a long call or a short put.
		boolean isCallOption = isPositionCallOption(position);
		BigDecimal positionQuantity = position.getPositionBalance().getQuantity();
		trade.setBuy(isCallOption ? MathUtils.isPositive(positionQuantity) : MathUtils.isNegative(positionQuantity));
		trade.setOpenCloseType(context.getTradeOpenCloseTypeFunction().apply(position, trade));
	}


	@Override
	public InvestmentSecurity getTradeGroupInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return CollectionUtils.getOnlyElementStrict(tradeList).getInvestmentSecurity();
	}
}
