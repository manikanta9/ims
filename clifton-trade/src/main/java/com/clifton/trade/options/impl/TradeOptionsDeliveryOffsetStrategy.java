package com.clifton.trade.options.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.options.TradeOptionsContext;
import com.clifton.trade.options.TradeOptionsPositionBalance;
import com.clifton.trade.options.TradeOptionsRequestDetail;
import com.clifton.trade.options.TradeOptionsStrategy;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;


/**
 * <code>TradeOptionsDeliveryOffsetStrategy</code> is a {@link TradeOptionsStrategy} for processing offsetting
 * trades to remove the positions created by the physical delivery of Option positions expiring In the Money.
 *
 * @author NickK
 */
public class TradeOptionsDeliveryOffsetStrategy extends AbstractTradeOptionsStrategy {

	@Override
	public boolean isOptionUnderlyingBigToMiniMapRequired() {
		return true;
	}


	@Override
	public List<Trade> generateTradeList(TradeOptionsContext context, TradeOptionsRequestDetail detail) {
		return MathUtils.isNullOrZero(detail.getTradeQuantity()) ? Collections.emptyList()
				: CollectionUtils.createList(generateTrade(context, detail, detail.getInvestmentSecurity().getUnderlyingSecurity(), detail.getAverageUnitPrice()));
	}


	@Override
	protected boolean isSingleTradeForPositionValid(TradeOptionsPositionBalance position, Trade trade) {
		return MathUtils.isLessThanOrEqual(trade.getQuantityIntended(), position.getNumRemainingContractsToOffsetDelivery());
	}


	@Override
	protected void applySingleTradeModificationsForPosition(TradeOptionsContext context, TradeOptionsPositionBalance position, Trade trade) {
		//If the trade group type is Delivery Offset and there is a corresponding "mini" security, trade the mini instead and adjust the quantity
		if (context.getOptionUnderlyingBigToMiniMap().containsKey(trade.getInvestmentSecurity())) {
			InvestmentSecurity big = trade.getInvestmentSecurity();
			InvestmentSecurity mini = context.getOptionUnderlyingBigToMiniMap().get(big);
			trade.setInvestmentSecurity(mini);
			BigDecimal adjustFactor = MathUtils.divide(big.getPriceMultiplier(), mini.getPriceMultiplier());
			trade.setQuantityIntended(MathUtils.multiply(trade.getQuantityIntended(), adjustFactor));
		}

		// Use the position balance option and quantity for the buy condition because it includes the sign for direction
		// Option Delivery Offset results in a buy if the position is a short call or a long put. The result should be the opposite of the Delivery.
		boolean isCallOption = isPositionCallOption(position);
		BigDecimal positionQuantity = position.getPositionBalance().getQuantity();
		trade.setBuy(isCallOption ? MathUtils.isNegative(positionQuantity) : MathUtils.isPositive(positionQuantity));
		trade.setOpenCloseType(context.getTradeOpenCloseTypeFunction().apply(position, trade));
	}


	@Override
	public InvestmentSecurity getTradeGroupInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return CollectionUtils.getOnlyElementStrict(tradeList).getInvestmentSecurity();
	}


	@Override
	public InvestmentSecurity getTradeGroupSecondaryInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return position.getPositionBalance().getInvestmentSecurity();
	}
}
