package com.clifton.trade.options.combination;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


/**
 * The default implementation for {@link TradeOptionCombinationHandler}.
 *
 * @author MikeH
 */
@Component
public class TradeOptionCombinationHandlerImpl implements TradeOptionCombinationHandler {

	/**
	 * The standard comparator for {@link TradeOptionLeg} entities. Legs are conventionally ordered by both expiration date and strike price.
	 */
	private static final Comparator<TradeOptionLeg> COMBINATION_LEG_COMPARATOR = Comparator
			.comparing(TradeOptionLeg::getExpirationDate)
			.thenComparing(TradeOptionLeg::getStrikePrice);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<TradeOptionCombination> getTradeOptionCombinationList(List<AccountingPosition> positionList, TradeOptionCombinationTypes... combinationTypes) {
		validateOptionPositionList(positionList);
		List<TradeOptionLeg> optionLegList = getOptionLegList(positionList);
		Collection<TradeOptionCombination> combinationList = getFullOptionCombinationListForLegList(optionLegList, combinationTypes);
		return combinationList;
	}


	@Override
	public BigDecimal getOptionCombinationDownsidePotential(TradeOptionCombination optionCombination) {
		if (optionCombination.getCombinationType() == null) {
			throw new UnsupportedOperationException(String.format("Downside risk calculation is only supported for combinations with uniform expiration dates. No combination type was determined for expiration date [%s]. A cause for this could be a leg of the combination was traded.", DateUtils.fromDate(optionCombination.getExpiryDate(), DateUtils.DATE_FORMAT_ISO)));
		}
		if (!optionCombination.getCombinationType().isSameExpiry()) {
			throw new UnsupportedOperationException(String.format("Downside risk calculation is only supported for combinations with uniform expiration dates. Combinations of type [%s] are not supported.", BeanUtils.getLabel(optionCombination.getCombinationType())));
		}

		// Build map of potential gain/loss magnitudes sorted by increasing strike
		Collection<TradeOptionLeg> optionLegList = optionCombination.getOptionLegList();
		NavigableMap<TradeOptionLeg, BigDecimal> optionLegGainMagnitudeMap = new TreeMap<>(Comparator.comparing(TradeOptionLeg::getStrikePrice));
		optionCombination.getOptionLegList().forEach(optionLeg -> optionLegGainMagnitudeMap.put(optionLeg, BigDecimal.ZERO));

		// Compute gain/loss magnitudes, including the trend if expiration is below the lowest strike or above the greatest strike
		BigDecimal lowerGainTrend = BigDecimal.ZERO;
		BigDecimal upperGainTrend = BigDecimal.ZERO;
		for (TradeOptionLeg optionLeg : optionLegList) {
			BigDecimal gainMultiplier = optionLeg.getUnderlyingRemainingQuantity();
			switch (optionLeg.getLegType()) {
				case LONG_CALL:
					increaseGainAbove(optionLegGainMagnitudeMap, optionLeg, gainMultiplier);
					upperGainTrend = MathUtils.add(upperGainTrend, gainMultiplier);
					break;
				case SHORT_CALL:
					decreaseGainAbove(optionLegGainMagnitudeMap, optionLeg, gainMultiplier);
					upperGainTrend = MathUtils.subtract(upperGainTrend, gainMultiplier);
					break;
				case LONG_PUT:
					increaseGainBelow(optionLegGainMagnitudeMap, optionLeg, gainMultiplier);
					lowerGainTrend = MathUtils.add(lowerGainTrend, gainMultiplier);
					break;
				case SHORT_PUT:
					decreaseGainBelow(optionLegGainMagnitudeMap, optionLeg, gainMultiplier);
					lowerGainTrend = MathUtils.subtract(lowerGainTrend, gainMultiplier);
					break;
			}
		}

		// Determine maximum downside risk
		final BigDecimal downsideRisk;
		if (MathUtils.isNegative(lowerGainTrend) || MathUtils.isNegative(upperGainTrend)) {
			// Infinite loss potential
			downsideRisk = null;
		}
		else {
			// Maximum downside
			downsideRisk = MathUtils.min(BigDecimal.ZERO, CoreMathUtils.min(optionLegGainMagnitudeMap.values()));
		}
		return MathUtils.abs(downsideRisk);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void increaseGainAbove(NavigableMap<TradeOptionLeg, BigDecimal> optionLegGainMagnitudeMap, TradeOptionLeg optionLeg, BigDecimal multiplier) {
		Collection<TradeOptionLeg> modifiedLegList = optionLegGainMagnitudeMap.tailMap(optionLeg, false).keySet();
		modifiedLegList.forEach(higherLeg -> {
			BigDecimal strikeDifference = MathUtils.subtract(higherLeg.getStrikePrice(), optionLeg.getStrikePrice());
			BigDecimal gainMagnitudeAdjustment = MathUtils.multiply(multiplier, strikeDifference);
			optionLegGainMagnitudeMap.compute(higherLeg, (k, magnitude) -> MathUtils.add(magnitude, gainMagnitudeAdjustment));
		});
	}


	private void decreaseGainAbove(NavigableMap<TradeOptionLeg, BigDecimal> optionLegGainMagnitudeMap, TradeOptionLeg optionLeg, BigDecimal multiplier) {
		Collection<TradeOptionLeg> modifiedLegList = optionLegGainMagnitudeMap.tailMap(optionLeg, false).keySet();
		modifiedLegList.forEach(higherLeg -> {
			BigDecimal strikeDifference = MathUtils.subtract(higherLeg.getStrikePrice(), optionLeg.getStrikePrice());
			BigDecimal gainMagnitudeAdjustment = MathUtils.multiply(multiplier, strikeDifference);
			optionLegGainMagnitudeMap.compute(higherLeg, (k, magnitude) -> MathUtils.subtract(magnitude, gainMagnitudeAdjustment));
		});
	}


	private void increaseGainBelow(NavigableMap<TradeOptionLeg, BigDecimal> optionLegGainMagnitudeMap, TradeOptionLeg optionLeg, BigDecimal multiplier) {
		Collection<TradeOptionLeg> modifiedLegList = optionLegGainMagnitudeMap.headMap(optionLeg, false).keySet();
		modifiedLegList.forEach(lowerLeg -> {
			BigDecimal strikeDifference = MathUtils.subtract(optionLeg.getStrikePrice(), lowerLeg.getStrikePrice());
			BigDecimal gainMagnitudeAdjustment = MathUtils.multiply(multiplier, strikeDifference);
			optionLegGainMagnitudeMap.compute(lowerLeg, (k, magnitude) -> MathUtils.add(magnitude, gainMagnitudeAdjustment));
		});
	}


	private void decreaseGainBelow(NavigableMap<TradeOptionLeg, BigDecimal> optionLegGainMagnitudeMap, TradeOptionLeg optionLeg, BigDecimal multiplier) {
		Collection<TradeOptionLeg> modifiedLegList = optionLegGainMagnitudeMap.headMap(optionLeg, false).keySet();
		modifiedLegList.forEach(lowerLeg -> {
			BigDecimal strikeDifference = MathUtils.subtract(optionLeg.getStrikePrice(), lowerLeg.getStrikePrice());
			BigDecimal gainMagnitudeAdjustment = MathUtils.multiply(multiplier, strikeDifference);
			optionLegGainMagnitudeMap.compute(lowerLeg, (k, magnitude) -> MathUtils.subtract(magnitude, gainMagnitudeAdjustment));
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Converts the given position list into a list of {@link TradeOptionLeg} objects. All positions with identical securities will be grouped into the same leg to effectively form
	 * aggregate positions.
	 */
	private List<TradeOptionLeg> getOptionLegList(List<AccountingPosition> positionList) {
		Map<InvestmentSecurity, List<AccountingPosition>> positionListBySecurity = BeanUtils.getBeansMap(positionList, AccountingPosition::getInvestmentSecurity);
		return CollectionUtils.getConverted(positionListBySecurity.entrySet(), securityToPositionListEntry -> new TradeOptionLeg(
				securityToPositionListEntry.getKey(),
				securityToPositionListEntry.getValue()
		));
	}


	/**
	 * Retrieves the full list of {@link TradeOptionCombination option combinations} for the given list of {@link TradeOptionLeg legs} and the given eligible {@link TradeOptionCombinationTypes
	 * combination types}.
	 * <p>
	 * This method will attempt to group legs into {@link TradeOptionCombination combinations} using the provided order of {@link TradeOptionCombinationTypes}. For example, if the
	 * provided list of types is <code>[{@link TradeOptionCombinationTypes#IRON_CONDOR Iron Condor}, {@link TradeOptionCombinationTypes#SHORT_CALL Short Call}]</code> then legs
	 * will first be grouped into iron condor combinations as eligible and then any remaining short call legs will be grouped into short call combinations.
	 * <p>
	 * Any legs that could not be grouped using any of the provided types will be grouped into a special {@link TradeOptionCombination#unknownCombination(TradeOptionLeg) unknown
	 * combination} type.
	 */
	private Collection<TradeOptionCombination> getFullOptionCombinationListForLegList(List<TradeOptionLeg> optionLegList, TradeOptionCombinationTypes... combinationTypes) {
		// Conservatively partition legs into combination types; combination type precedence is indicated by the order of the types provided
		List<TradeOptionLeg> unprocessedOptionLegList = new ArrayList<>(CollectionUtils.createSorted(optionLegList, COMBINATION_LEG_COMPARATOR));
		Collection<TradeOptionCombination> combinationList = new ArrayList<>();
		for (TradeOptionCombinationTypes combinationType : combinationTypes) {
			Collection<TradeOptionCombination> combinationListForType = getOptionCombinationListOfTypeForLegList(combinationType, unprocessedOptionLegList);
			// Remove accepted legs from running list and add combinations
			unprocessedOptionLegList.removeAll(CollectionUtils.getConvertedFlattened(combinationListForType, TradeOptionCombination::getOptionLegList));
			combinationList.addAll(combinationListForType);
		}

		// Create unknown combination objects for unassigned legs
		combinationList.addAll(CollectionUtils.getConverted(unprocessedOptionLegList, TradeOptionCombination::unknownCombination));
		return combinationList;
	}


	/**
	 * Retrieves the list of {@link TradeOptionCombination option combinations} of the given type from the provided leg list. Legs which cannot be processed into the given {@link
	 * TradeOptionCombinationTypes combination type} will be ignored.
	 */
	private Collection<TradeOptionCombination> getOptionCombinationListOfTypeForLegList(TradeOptionCombinationTypes combinationType, List<TradeOptionLeg> optionLegList) {
		// Find legs of eligible types for the combination
		List<TradeOptionLeg> eligibleOptionLegList = CollectionUtils.getFiltered(optionLegList, leg -> ArrayUtils.contains(combinationType.getOptionLegTypes(), leg.getLegType()));

		// Group legs into combinations
		final List<TradeOptionCombination> combinationListForType;
		if (combinationType.getOptionLegTypes().length == 1) {
			// Trivial case: All legs are single combinations
			combinationListForType = CollectionUtils.getConverted(eligibleOptionLegList, optionLeg -> new TradeOptionCombination(combinationType, Collections.singletonList(optionLeg)));
		}
		else {
			// Group option legs based on eligible pairing criteria from selected combination type
			Collection<List<TradeOptionLeg>> optionLegGroupList = BeanUtils.getBeansMap(eligibleOptionLegList, leg -> StringUtils.generateKey(
					leg.getInvestmentSecurity().getUnderlyingSecurity().getId(),
					combinationType.isSameExpiry() ? leg.getExpirationDate().getTime() : "",
					combinationType.isSameUnderlyingQuantity() ? leg.getUnderlyingRemainingQuantity() : ""
			)).values();

			// Find leg groups matching combination type
			combinationListForType = CollectionUtils.getConvertedFlattened(optionLegGroupList, legGroup -> getOptionCombinationListForLegListGroup(combinationType, legGroup));
		}
		return combinationListForType;
	}


	/**
	 * Generates a collection of {@link TradeOptionCombination} objects of the given {@link TradeOptionCombinationTypes combination type} from the provided list of similar {@link
	 * TradeOptionLeg option legs}.
	 * <p>
	 * This method assumes that each leg in the provided list of legs is eligible for grouping into the same {@link TradeOptionCombination}. Multiple {@link TradeOptionCombination}
	 * objects will be created if the number of legs is a non-singular multiple of the expected number of legs in the {@link TradeOptionCombinationTypes}.
	 */
	private Collection<TradeOptionCombination> getOptionCombinationListForLegListGroup(TradeOptionCombinationTypes combinationType, List<TradeOptionLeg> similarOptionLegList) {
		// Seek the option leg list for valid combinations until list is exhausted
		List<TradeOptionCombination> optionCombinationList = new ArrayList<>();
		List<TradeOptionLeg> remainingOptionLegList = new ArrayList<>(similarOptionLegList);
		while (!remainingOptionLegList.isEmpty()) {
			// Seek another option combination
			Iterator<TradeOptionLeg> remainingOptionLegIterator = remainingOptionLegList.iterator();
			List<TradeOptionLeg> combinationLegList = getMatchingTradeOptionLegList(combinationType, remainingOptionLegIterator);
			if (combinationLegList != null) {
				// Leg discovery succeeded; generate combination
				optionCombinationList.add(new TradeOptionCombination(combinationType, combinationLegList));
			}
			else {
				// Leg discovery failed (no more leg groups can be found); terminate seek
				break;
			}
		}
		return optionCombinationList;
	}


	/**
	 * Seeks through the given iterator to find the first set of {@link TradeOptionLeg legs} which match the given {@link TradeOptionCombinationTypes combination type}.
	 * <p>
	 * Legs will automatically be removed from the iterator with the {@link Iterator#remove()} operation once processed.
	 * <p>
	 * This method will return <code>null</code> if no additional groupings for the given {@link TradeOptionCombinationTypes combination type} can be made.
	 */
	private List<TradeOptionLeg> getMatchingTradeOptionLegList(TradeOptionCombinationTypes combinationType, Iterator<TradeOptionLeg> remainingOptionLegIterator) {
		List<TradeOptionLeg> combinationLegList = new ArrayList<>();
		boolean missingTypesExist = false;
		for (TradeOptionLegTypes seekLegType : combinationType.getOptionLegTypes()) {
			// Find matching leg for current type
			TradeOptionLeg matchingLeg = getMatchingTradeOptionLeg(seekLegType, remainingOptionLegIterator);
			if (matchingLeg != null) {
				combinationLegList.add(matchingLeg);
			}
			else {
				// No matching leg found, so no remaining combinations exist; terminate remaining scan
				missingTypesExist = true;
				break;
			}
		}
		return missingTypesExist ? null : combinationLegList;
	}


	/**
	 * Seeks through the given iterator to find the next {@link TradeOptionLeg} which satisfies the given {@link TradeOptionLegTypes seek type}.
	 * <p>
	 * If such a leg is found, it will automatically be removed from the given iterator with the {@link Iterator#remove()} operation. If no such leg is found, this method will
	 * return <code>null</code>
	 */
	private TradeOptionLeg getMatchingTradeOptionLeg(TradeOptionLegTypes seekLegType, Iterator<TradeOptionLeg> remainingOptionLegIterator) {
		TradeOptionLeg matchingLeg = null;
		while (remainingOptionLegIterator.hasNext()) {
			TradeOptionLeg optionLeg = remainingOptionLegIterator.next();
			if (optionLeg.getLegType() == seekLegType) {
				matchingLeg = optionLeg;
				remainingOptionLegIterator.remove();
				break;
			}
		}
		return matchingLeg;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateOptionPositionList(List<AccountingPosition> positionList) {
		List<AccountingPosition> nonOptionPositionList = CollectionUtils.getFiltered(positionList, position -> !InvestmentUtils.isSecurityOfType(position.getInvestmentSecurity(), InvestmentType.OPTIONS));
		ValidationUtils.assertEmpty(nonOptionPositionList, () -> String.format("Non-option securities found when building option combinations: %s.", CollectionUtils.toString(CollectionUtils.getConverted(nonOptionPositionList, position -> position.getInvestmentSecurity().getLabel()), 5)));
	}
}
