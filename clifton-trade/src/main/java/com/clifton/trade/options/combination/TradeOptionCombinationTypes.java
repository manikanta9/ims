package com.clifton.trade.options.combination;

import com.clifton.core.beans.LabeledObject;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


/**
 * The {@link TradeOptionCombinationTypes} enumerator lists all supported {@link TradeOptionCombination} types and their structures.
 * <p>
 * Each type includes a list of {@link TradeOptionLegTypes leg types} in order of increasing strike. Each type also includes {@link #combinationFlags combination flags} to indicate
 * constraints that apply to the combination type, such as whether all legs must have the same {@link #isSameUnderlyingQuantity() underlying quantity} or {@link #isSameExpiry()
 * expiration date}.
 *
 * @author MikeH
 */
public enum TradeOptionCombinationTypes implements LabeledObject {

	/* Single options strategies */
	LONG_CALL("Long Call", 0, TradeOptionLegTypes.LONG_CALL),
	SHORT_CALL("Short Call", 0, TradeOptionLegTypes.SHORT_CALL),
	LONG_PUT("Long Put", 0, TradeOptionLegTypes.LONG_PUT),
	SHORT_PUT("Short Put", 0, TradeOptionLegTypes.SHORT_PUT),

	/* Spread strategies */
	BEAR_CALL_SPREAD("Bear Call Spread", CombinationFlags.SAME_UNDERLYING_QUANTITY | CombinationFlags.SAME_EXPIRY, TradeOptionLegTypes.SHORT_CALL, TradeOptionLegTypes.LONG_CALL),
	BULL_CALL_SPREAD("Bull Call Spread", CombinationFlags.SAME_UNDERLYING_QUANTITY | CombinationFlags.SAME_EXPIRY, TradeOptionLegTypes.LONG_CALL, TradeOptionLegTypes.SHORT_CALL),
	BEAR_PUT_SPREAD("Bear Put Spread", CombinationFlags.SAME_UNDERLYING_QUANTITY | CombinationFlags.SAME_EXPIRY, TradeOptionLegTypes.SHORT_PUT, TradeOptionLegTypes.LONG_PUT),
	BULL_PUT_SPREAD("Bull Put Spread", CombinationFlags.SAME_UNDERLYING_QUANTITY | CombinationFlags.SAME_EXPIRY, TradeOptionLegTypes.LONG_PUT, TradeOptionLegTypes.SHORT_PUT),

	/* Other strategies */
	IRON_CONDOR("Iron Condor", CombinationFlags.SAME_UNDERLYING_QUANTITY | CombinationFlags.SAME_EXPIRY, TradeOptionLegTypes.LONG_PUT, TradeOptionLegTypes.SHORT_PUT, TradeOptionLegTypes.SHORT_CALL, TradeOptionLegTypes.LONG_CALL),
	/**
	 * An options strategy that utilizes a long put at a lower strike price and a short call at a higher strike price, typically with a long position in the underlying.
	 */
	COLLAR("Collar", CombinationFlags.SAME_UNDERLYING_QUANTITY | CombinationFlags.SAME_EXPIRY, TradeOptionLegTypes.LONG_PUT, TradeOptionLegTypes.SHORT_CALL);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final String label;
	private final int combinationFlags;
	/**
	 * The list of {@link TradeOptionLegTypes} for each leg in order from lowest to highest strike.
	 */
	private final TradeOptionLegTypes[] optionLegTypes;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	TradeOptionCombinationTypes(String label, int combinationFlags, TradeOptionLegTypes... optionLegTypes) {
		this.label = label;
		this.combinationFlags = combinationFlags;
		this.optionLegTypes = optionLegTypes;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the {@link TradeOptionCombinationTypes combination type} that matches the given list of legs. If no such type is found, then <code>null</code> will be returned.
	 */
	public static TradeOptionCombinationTypes fromLegList(List<TradeOptionLeg> optionLegList) {
		TradeOptionLegTypes[] optionLegTypes = optionLegList.stream()
				.sorted(Comparator.comparing(TradeOptionLeg::getExpirationDate).thenComparing(TradeOptionLeg::getStrikePrice))
				.map(TradeOptionLeg::getLegType)
				.toArray(TradeOptionLegTypes[]::new);
		return Arrays.stream(TradeOptionCombinationTypes.values())
				.filter(spreadType -> Arrays.equals(spreadType.getOptionLegTypes(), optionLegTypes))
				.findAny()
				.orElse(null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSameUnderlyingQuantity() {
		return this.getOptionLegTypes().length == 1 || (this.combinationFlags & CombinationFlags.SAME_UNDERLYING_QUANTITY) != 0;
	}


	public boolean isSameExpiry() {
		return this.getOptionLegTypes().length == 1 || (this.combinationFlags & CombinationFlags.SAME_EXPIRY) != 0;
	}


	/**
	 * The set of flags used to indicate different {@link TradeOptionCombinationTypes option combination type} constraints, such as the constraint that all legs must have the same
	 * underlying quantity or the same expiration date.
	 */
	private static class CombinationFlags {

		private static final int SAME_UNDERLYING_QUANTITY = 1 << 1;
		private static final int SAME_EXPIRY = 1 << 2;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return this.label;
	}


	public int getCombinationFlags() {
		return this.combinationFlags;
	}


	public TradeOptionLegTypes[] getOptionLegTypes() {
		return this.optionLegTypes;
	}
}
