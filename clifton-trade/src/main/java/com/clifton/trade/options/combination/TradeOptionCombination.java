package com.clifton.trade.options.combination;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link TradeOptionCombination} represents a position in an option combination.
 * <p>
 * The term "combination" refers to any number of options strategies which involve multiple option positions on the same underlying asset. These option positions, or {@link
 * TradeOptionLeg legs}, may differ in strike price, expiration date, option type, trade direction, position size, or any combination of these properties. Option combinations are
 * categorized into {@link TradeOptionCombinationTypes}.
 * <p>
 * Investment strategies which use options may specify an {@link TradeOptionCombinationTypes option combination type} or a set of combination types which are eligible for trading
 * within the strategy.
 *
 * @author MikeH
 * @see <a href="https://www.investopedia.com/terms/c/combination.asp">Investopedia - Combination</a>
 * @see <a href="https://wiki.paraport.com/display/IT/Options+Strategies">PPA Wiki - Options Strategies</a>
 */
public class TradeOptionCombination implements LabeledObject {

	/**
	 * The type of combination represented by this object, or <code>null</code> if the leg(s) within this combination do not match an eligible {@link TradeOptionCombinationTypes
	 * option combination type}.
	 *
	 * @see #unknownCombination(TradeOptionLeg)
	 */
	private final TradeOptionCombinationTypes combinationType;
	private final InvestmentSecurity underlyingSecurity;
	private final Collection<TradeOptionLeg> optionLegList;
	private final Lazy<BigDecimal> lazyUnderlyingRemainingQuantity = new Lazy<>(this::calculateUnderlyingRemainingQuantity);
	private final Lazy<Date> lazyExpiryDate = new Lazy<>(this::calculateExpiryDate);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOptionCombination(TradeOptionCombinationTypes combinationType, Collection<TradeOptionLeg> optionLegList) {
		this.combinationType = combinationType;
		this.underlyingSecurity = getUniqueUnderlyingSecurity(optionLegList);
		this.optionLegList = optionLegList;
		validate();
	}


	private TradeOptionCombination(TradeOptionLeg optionLeg) {
		// Generate an option combination object for an unknown combination type
		this.combinationType = null;
		this.underlyingSecurity = optionLeg.getInvestmentSecurity().getUnderlyingSecurity();
		this.optionLegList = Collections.singletonList(optionLeg);
	}


	/**
	 * Creates an unknown combination type from the given leg. An unknown combination type is a placeholder for a leg which could not be adequately grouped into a permitted {@link
	 * TradeOptionCombinationTypes combination type}.
	 * <p>
	 * Unknown combination types have no set {@link #combinationType} and are indicated by the {@link #isUnknownType()} flag.
	 */
	public static TradeOptionCombination unknownCombination(TradeOptionLeg optionLeg) {
		return new TradeOptionCombination(optionLeg);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder combinationLabel = new StringBuilder();
		combinationLabel.append(isUnknownType() ? "Unknown Combination" : BeanUtils.getLabel(getCombinationType())).append(": ");
		combinationLabel.append(getUnderlyingSecurity().getSymbol());
		if (isUnknownType() || getCombinationType().isSameExpiry()) {
			combinationLabel.append(' ').append(DateUtils.fromDateShort(getExpiryDate()));
		}
		return combinationLabel.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurity getUniqueUnderlyingSecurity(Collection<TradeOptionLeg> optionLegList) {
		// Get unique underlying security
		List<InvestmentSecurity> distinctUnderlyingSecurityList = optionLegList.stream()
				.map(leg -> leg.getInvestmentSecurity().getUnderlyingSecurity())
				.distinct()
				.collect(Collectors.toList());
		AssertUtils.assertTrue(distinctUnderlyingSecurityList.size() == 1, () ->
				String.format("All legs in option combinations must have the same underlying security. Conflicting underlying securities found: %s. Legs: %s.",
						CollectionUtils.toString(distinctUnderlyingSecurityList, 5),
						CollectionUtils.toString(CollectionUtils.getConverted(optionLegList, TradeOptionLeg::getInvestmentSecurity), 5)));
		return CollectionUtils.getOnlyElementStrict(distinctUnderlyingSecurityList);
	}


	private void validate() {
		if (getOptionLegList().size() > 1 && getCombinationType().isSameExpiry()) {
			Map<TradeOptionLeg, Date> legToExpiryMap = CollectionUtils.getMapped(getOptionLegList(), TradeOptionLeg::getExpirationDate);
			List<Date> distinctExpiryList = CollectionUtils.getDistinct(legToExpiryMap.values());
			AssertUtils.assertEquals(1, distinctExpiryList.size(), "Conflicting leg expiration dates found in option combination. Expiration dates: [%s].", legToExpiryMap);
		}
		if (getOptionLegList().size() > 1 && getCombinationType().isSameUnderlyingQuantity()) {
			Map<TradeOptionLeg, BigDecimal> legToUnderlyingQuantityMap = CollectionUtils.getMapped(getOptionLegList(), TradeOptionLeg::getUnderlyingRemainingQuantity);
			List<BigDecimal> distinctQuantityList = CollectionUtils.getDistinct(legToUnderlyingQuantityMap.values());
			AssertUtils.assertEquals(1, distinctQuantityList.size(), "Conflicting leg quantities found in option combination. Quantities: [%s].", legToUnderlyingQuantityMap);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BigDecimal calculateUnderlyingRemainingQuantity() {
		if (!(isUnknownType() || getCombinationType().isSameUnderlyingQuantity())) {
			throw new UnsupportedOperationException(String.format("The underlying quantity property is not supported for option combinations of type [%s].", BeanUtils.getLabel(getCombinationType())));
		}
		return getOptionLegList().stream()
				.map(TradeOptionLeg::getUnderlyingRemainingQuantity)
				.findAny()
				.orElse(null);
	}


	private Date calculateExpiryDate() {
		if (!(isUnknownType() || getCombinationType().isSameExpiry())) {
			throw new UnsupportedOperationException(String.format("The expiry date property is not supported for option combinations of type [%s].", BeanUtils.getLabel(getCombinationType())));
		}
		return getOptionLegList().stream()
				.map(TradeOptionLeg::getExpirationDate)
				.findAny()
				.orElse(null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getUnderlyingRemainingQuantity() {
		return this.lazyUnderlyingRemainingQuantity.get();
	}


	public Date getExpiryDate() {
		return this.lazyExpiryDate.get();
	}


	public boolean isUnknownType() {
		return getCombinationType() == null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "TradeOptionCombination{" +
				"combinationType=" + this.combinationType +
				", underlyingSecurity=" + this.underlyingSecurity +
				", optionLegList=" + this.optionLegList +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOptionCombinationTypes getCombinationType() {
		return this.combinationType;
	}


	public InvestmentSecurity getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public Collection<TradeOptionLeg> getOptionLegList() {
		return this.optionLegList;
	}
}
