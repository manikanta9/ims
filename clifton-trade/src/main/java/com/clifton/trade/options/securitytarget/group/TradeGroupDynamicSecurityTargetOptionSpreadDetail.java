package com.clifton.trade.options.securitytarget.group;

import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.dynamic.TradeGroupDynamicDetail;

import java.math.BigDecimal;
import java.util.List;


/**
 * <code>TradeGroupDynamicSecurityTargetOptionSpreadDetail</code> is a {@link TradeGroupDynamicDetail} for OARS and RPS which
 * outlines the trade details for expiring tranche option positions and new tranche trades.
 * <p>
 * OARS trades both a put spread (long put and short put) and call spread (short call and long call)
 * RPS trades just a put spread (long put and short put)
 * <p>
 * Option strike prices are in order according to (long put, short put, short call, long call)
 *
 * @author NickK
 */
public class TradeGroupDynamicSecurityTargetOptionSpreadDetail extends TradeGroupDynamicDetail {

	/**
	 * Quantity for an expiring {@link InvestmentAccountSecurityTarget} tranche Put/Call Spread on {@link TradeGroupDynamicSecurityTargetOptionSpread#balanceDate}
	 * The quantity is the same for each trade involved in the spread (long put, short put, short call, long call).
	 */
	private BigDecimal expiringQuantity;
	/**
	 * Calculated quantity to trade for the Put/Call spread for a particular {@link InvestmentAccountSecurityTarget} tranche.
	 * This quantity will be used for each spread trade.
	 */
	private BigDecimal tradeQuantity;

	private InvestmentAccountSecurityTarget securityTarget;
	private BigDecimal securityTargetNotional;
	/**
	 * Client account configured Value At Risk. It is the amount of target notional to put at risk for the strategy.
	 */
	private BigDecimal valueAtRiskPercent;
	private BigDecimal averageTrancheValueAtRisk;
	private BigDecimal averageTrancheNotional;


	private Trade longCallTrade;
	private Trade shortCallTrade;
	private Trade longPutTrade;
	private Trade shortPutTrade;

	private BigDecimal putSpreadExposure;
	private BigDecimal callSpreadExposure;

	/**
	 * Executing broker for this client account spread detail. If the holding account is a directed brokerage, this value will be set by the system.
	 * It can remain null if the detail is not set up for directed brokerage, and will be set from the option spread's defined executing broker.
	 */
	private BusinessCompany executingBrokerCompany;


	/**
	 * The trade destination for this client. This may remain null, but can be set if this particular client must trade with a different destination or
	 * manually.  If set, this trade destination will override the trade destination set at the group level.
	 */
	private TradeDestination tradeDestination;


	/**
	 * Items associated to security target utilization pertaining to target quantity, unexpired covered positions quantity, and quantity available to trade.
	 * Option Spreads can be for multiple legs with long and/or short positions. Rather than switch signs depending on which leg is used, values should be in absolute value.
	 */
	private BigDecimal targetUnderlyingQuantity;
	private BigDecimal unexpiredCoveredUnderlyingQuantity;
	/**
	 * Difference between the above two quantities.
	 */
	private BigDecimal availableUnderlyingQuantity;
	/**
	 * Available underlying quantity adjusted to option quantity using price multiplier.
	 */
	private BigDecimal availableQuantity;

	/**
	 * A list of validation notes for this client account to relay back to the UI for trader review.
	 * Note are used to prevent hard stops on trading activity, when performing actions across accounts.
	 */
	private List<String> validationNoteList;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BigDecimal getExpiringQuantity() {
		return this.expiringQuantity;
	}


	public void setExpiringQuantity(BigDecimal expiringQuantity) {
		this.expiringQuantity = expiringQuantity;
	}


	public BigDecimal getTradeQuantity() {
		return this.tradeQuantity;
	}


	public void setTradeQuantity(BigDecimal tradeQuantity) {
		this.tradeQuantity = tradeQuantity;
	}


	public InvestmentAccountSecurityTarget getSecurityTarget() {
		return this.securityTarget;
	}


	public void setSecurityTarget(InvestmentAccountSecurityTarget securityTarget) {
		this.securityTarget = securityTarget;
	}


	public BigDecimal getSecurityTargetNotional() {
		return this.securityTargetNotional;
	}


	public void setSecurityTargetNotional(BigDecimal securityTargetNotional) {
		this.securityTargetNotional = securityTargetNotional;
	}


	public BigDecimal getValueAtRiskPercent() {
		return this.valueAtRiskPercent;
	}


	public void setValueAtRiskPercent(BigDecimal valueAtRiskPercent) {
		this.valueAtRiskPercent = valueAtRiskPercent;
	}


	public BigDecimal getAverageTrancheValueAtRisk() {
		return this.averageTrancheValueAtRisk;
	}


	public void setAverageTrancheValueAtRisk(BigDecimal averageTrancheValueAtRisk) {
		this.averageTrancheValueAtRisk = averageTrancheValueAtRisk;
	}


	public BigDecimal getAverageTrancheNotional() {
		return this.averageTrancheNotional;
	}


	public void setAverageTrancheNotional(BigDecimal averageTrancheNotional) {
		this.averageTrancheNotional = averageTrancheNotional;
	}


	public Trade getLongCallTrade() {
		return this.longCallTrade;
	}


	public void setLongCallTrade(Trade longCallTrade) {
		this.longCallTrade = longCallTrade;
	}


	public Trade getShortCallTrade() {
		return this.shortCallTrade;
	}


	public void setShortCallTrade(Trade shortCallTrade) {
		this.shortCallTrade = shortCallTrade;
	}


	public Trade getLongPutTrade() {
		return this.longPutTrade;
	}


	public void setLongPutTrade(Trade longPutTrade) {
		this.longPutTrade = longPutTrade;
	}


	public Trade getShortPutTrade() {
		return this.shortPutTrade;
	}


	public void setShortPutTrade(Trade shortPutTrade) {
		this.shortPutTrade = shortPutTrade;
	}


	public BigDecimal getPutSpreadExposure() {
		return this.putSpreadExposure;
	}


	public void setPutSpreadExposure(BigDecimal putSpreadExposure) {
		this.putSpreadExposure = putSpreadExposure;
	}


	public BigDecimal getCallSpreadExposure() {
		return this.callSpreadExposure;
	}


	public void setCallSpreadExposure(BigDecimal callSpreadExposure) {
		this.callSpreadExposure = callSpreadExposure;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public BigDecimal getTargetUnderlyingQuantity() {
		return this.targetUnderlyingQuantity;
	}


	public void setTargetUnderlyingQuantity(BigDecimal targetUnderlyingQuantity) {
		this.targetUnderlyingQuantity = targetUnderlyingQuantity;
	}


	public BigDecimal getUnexpiredCoveredUnderlyingQuantity() {
		return this.unexpiredCoveredUnderlyingQuantity;
	}


	public void setUnexpiredCoveredUnderlyingQuantity(BigDecimal unexpiredCoveredUnderlyingQuantity) {
		this.unexpiredCoveredUnderlyingQuantity = unexpiredCoveredUnderlyingQuantity;
	}


	public BigDecimal getAvailableUnderlyingQuantity() {
		return this.availableUnderlyingQuantity;
	}


	public void setAvailableUnderlyingQuantity(BigDecimal availableUnderlyingQuantity) {
		this.availableUnderlyingQuantity = availableUnderlyingQuantity;
	}


	public BigDecimal getAvailableQuantity() {
		return this.availableQuantity;
	}


	public void setAvailableQuantity(BigDecimal availableQuantity) {
		this.availableQuantity = availableQuantity;
	}


	public List<String> getValidationNoteList() {
		return this.validationNoteList;
	}


	public void setValidationNoteList(List<String> validationNoteList) {
		this.validationNoteList = validationNoteList;
	}
}
