package com.clifton.trade.options.combination;

import com.clifton.accounting.gl.position.AccountingPosition;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;


/**
 * The {@link TradeOptionCombinationHandler} provides methods for working with {@link TradeOptionCombination} objects.
 * <p>
 * Included methods provide tools for {@link #getTradeOptionCombinationList(List, TradeOptionCombinationTypes...) option combination creation} and for property calculation, such as
 * {@link #getOptionCombinationDownsidePotential(TradeOptionCombination) maximum downside risk}.
 *
 * @author MikeH
 */
public interface TradeOptionCombinationHandler {

	/**
	 * Gets the list of {@link TradeOptionCombination option combinations} from the given list of {@link AccountingPosition positions} using the provided list of eligible {@link
	 * TradeOptionCombinationTypes}.
	 * <p>
	 * The order of the list of {@link TradeOptionCombinationTypes} is significant. This order indicates precedence for the types that will be inferred from the given position
	 * list. For example, if the provided list of types is <code>[{@link TradeOptionCombinationTypes#IRON_CONDOR Iron Condor}, {@link TradeOptionCombinationTypes#SHORT_CALL Short
	 * Call}]</code> then positions will first be grouped into iron condor combinations as eligible and then any remaining short call legs will be grouped into short call
	 * combinations.
	 * <p>
	 * Any legs that could not be grouped using any of the provided types will be grouped into a special {@link TradeOptionCombination#unknownCombination(TradeOptionLeg) unknown
	 * combination} type.
	 *
	 * @param positionList     the list of positions to group into {@link TradeOptionCombination} objects
	 * @param combinationTypes the list of eligible {@link TradeOptionCombinationTypes} in order of decreasing precedence
	 * @return the list of grouped {@link TradeOptionCombination} objects and {@link TradeOptionCombination#unknownCombination(TradeOptionLeg) unknown combination} objects, if any exist
	 */
	public Collection<TradeOptionCombination> getTradeOptionCombinationList(List<AccountingPosition> positionList, TradeOptionCombinationTypes... combinationTypes);


	/**
	 * Calculates the maximum downside risk for the given {@link TradeOptionCombination}. The downside risk is defined as the maximum possible loss at assignment.
	 * <p>
	 * A <code>null</code> return value indicates an unlimited downside risk. If no downside risk exists, then <code>0</code> will be returned.
	 * <p>
	 * This method assumes that the price of the underlying is unbounded on both ends and can both decrease to negative infinity and increase to positive infinity. This
	 * significance of this assumption is that the underlying price at assignment is not bounded by zero, and thus the downside risk for {@link TradeOptionLegTypes#SHORT_PUT short
	 * puts} is infinite when not hedged.
	 * <p>
	 * Downside risk calculation is only supported for combinations with the {@link TradeOptionCombinationTypes#isSameExpiry() same expiration date}.
	 *
	 * @param optionCombination the option combination for which the downside risk should be calculated
	 * @return the maximum downside risk, or <code>null</code> if the downside risk is infinite
	 * @throws UnsupportedOperationException if the given combination does not enforce {@link TradeOptionCombinationTypes#isSameExpiry() same expiry} constraints
	 */
	public BigDecimal getOptionCombinationDownsidePotential(TradeOptionCombination optionCombination);
}
