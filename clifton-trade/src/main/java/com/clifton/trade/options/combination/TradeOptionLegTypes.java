package com.clifton.trade.options.combination;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;


/**
 * The enumeration of possible leg types for option trades.
 * <p>
 * These leg types are used to describe {@link TradeOptionLeg} objects, which are typically used as components of {@link TradeOptionCombination} objects.
 *
 * @author MikeH
 */
public enum TradeOptionLegTypes {

	LONG_CALL(true, true), SHORT_CALL(false, true),
	LONG_PUT(true, false), SHORT_PUT(false, false);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final boolean isLong;
	private final boolean isCall;


	TradeOptionLegTypes(boolean isLong, boolean isCall) {
		this.isLong = isLong;
		this.isCall = isCall;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isShort() {
		return !isLong();
	}


	public boolean isPut() {
		return !isCall();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isLong() {
		return this.isLong;
	}


	public boolean isCall() {
		return this.isCall;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeOptionLegTypes fromSource(boolean isLong, InvestmentSecurity security) {
		InvestmentSecurityOptionTypes optionType = security.getOptionType();
		ValidationUtils.assertNotNull(optionType, "Security " + security + " is missing a defined Option Type");
		switch (optionType) {
			case CALL:
				return isLong ? LONG_CALL : SHORT_CALL;
			case PUT:
				return isLong ? LONG_PUT : SHORT_PUT;
			default:
				throw new IllegalArgumentException(String.format("Unrecognized option type for trade: [%s].", optionType));
		}
	}
}
