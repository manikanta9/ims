package com.clifton.trade.options.impl;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.options.TradeOptionsContext;
import com.clifton.trade.options.TradeOptionsPositionBalance;
import com.clifton.trade.options.TradeOptionsRequestDetail;
import com.clifton.trade.options.TradeOptionsStrategy;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * <code>TradeOptionsAssignmentStrategy</code> is a {@link TradeOptionsStrategy} for processing assignment of an Option position.
 * Two trades will be generated from a position including: a close of the Option position and the delivery of the underlying security of the Option.
 *
 * @author NickK
 */
public class TradeOptionsAssignmentStrategy extends AbstractTradeOptionsStrategy {

	@Override
	public List<Trade> generateTradeList(TradeOptionsContext context, TradeOptionsRequestDetail detail) {
		InvestmentSecurity option = detail.getInvestmentSecurity();
		if (MathUtils.isNullOrZero(detail.getTradeQuantity())) {
			return Collections.emptyList();
		}

		Trade underlyingTrade = generateTrade(context, detail, option.getUnderlyingSecurity(), option.getOptionStrikePrice());
		underlyingTrade.setQuantityIntended(MathUtils.multiply(underlyingTrade.getQuantityIntended(), option.getPriceMultiplier()));

		List<Trade> tradeList = CollectionUtils.createList(generateTrade(context, detail, option, BigDecimal.ZERO), underlyingTrade);

		if (detail.isTradeUnderlying()) {
			tradeList = addUnderlyingTrade(context, detail, tradeList);
		}

		return tradeList;
	}


	@Override
	protected boolean isSingleTradePerPosition() {
		return false;
	}


	@Override
	protected boolean isTradeListForPositionValid(TradeOptionsPositionBalance position, List<Trade> tradeList) {

		Map<Boolean, List<Trade>> optionTradeListMap = getOptionTradeListMap(tradeList);
		List<Trade> optionTradeList = optionTradeListMap.get(Boolean.TRUE);
		ValidationUtils.assertTrue(CollectionUtils.getSize(optionTradeList) == 1 && optionTradeList.get(0) != null, "A single closing option trade is required for this assignment.");

		Trade optionTrade = optionTradeList.get(0);
		ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(optionTrade.getQuantityIntended(), position.getTradeQuantityForExpire()), "Quantity for the option trade must be less than or equal to the position's expiring quantity");

		// Check underlying trades for a maximum of 2 trades one of them must match requirements for an assignment.
		List<Trade> underlyingTradeList = optionTradeListMap.get(Boolean.FALSE);
		int underlyingTradeCount = CollectionUtils.getSize(underlyingTradeList);
		ValidationUtils.assertTrue(underlyingTradeCount > 0 && underlyingTradeCount <= 2, "At least one trade for the underlying security is required to process an assignment. A maximum of two underlying trades is allowed.");

		boolean requiredUnderlyingTradeQuantityFound = false;
		boolean mismatchedUnderlyingSecurityFound = false;
		BigDecimal expectedUnderlyingTradeQuantity = MathUtils.multiply(optionTrade.getQuantityIntended(), optionTrade.getInvestmentSecurity().getPriceMultiplier());
		for (Trade underlyingTrade : CollectionUtils.getIterable(underlyingTradeList)) {
			if (MathUtils.isEqual(expectedUnderlyingTradeQuantity, underlyingTrade.getQuantityIntended())) {
				requiredUnderlyingTradeQuantityFound = true;
			}

			if (!underlyingTrade.getInvestmentSecurity().getId().equals(optionTrade.getInvestmentSecurity().getUnderlyingSecurity().getId())) {
				mismatchedUnderlyingSecurityFound = true;
			}
		}

		ValidationUtils.assertTrue(requiredUnderlyingTradeQuantityFound, "Expected a trade for the Option's underlying security with quantity " + expectedUnderlyingTradeQuantity + " for an Assignment.");
		ValidationUtils.assertFalse(mismatchedUnderlyingSecurityFound, "All underlying trades for this assignment must trade investment security: " + optionTrade.getInvestmentSecurity().getUnderlyingSecurity().getLabel());

		return true;
	}


	@Override
	protected void applyTradeListModificationsForPosition(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		Map<InvestmentSecurity, Trade> securityToTradeMap = BeanUtils.getBeanMap(tradeList, Trade::getInvestmentSecurity);
		Trade optionTrade = securityToTradeMap.get(position.getPositionBalance().getInvestmentSecurity());
		boolean buyToClose = MathUtils.isNegative(position.getPositionBalance().getQuantity());
		// trade the inverse of the position to close it
		optionTrade.setBuy(buyToClose);
		optionTrade.setOpenCloseType(context.getTradeOpenCloseTypeFunction().apply(position, optionTrade));

		// The trade list may have two underlying trades.  Find the underlying trade corresponding to the assignment (should be the second trade, but search by underlying and quantity to be sure).
		BigDecimal expectedUnderlyingQuantity = MathUtils.multiply(optionTrade.getQuantityIntended(), optionTrade.getInvestmentSecurity().getPriceMultiplier());
		for (Trade trade : tradeList) {
			if (MathUtils.isEqual(expectedUnderlyingQuantity, trade.getQuantityIntended()) && trade.getInvestmentSecurity().equals(optionTrade.getInvestmentSecurity().getUnderlyingSecurity())) {
				trade.setBuy(!buyToClose);
				trade.setOpenCloseType(context.getTradeOpenCloseTypeFunction().apply(position, trade));
				break;
			}
		}
	}


	@Override
	public InvestmentSecurity getTradeGroupInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return position.getPositionBalance().getInvestmentSecurity().getUnderlyingSecurity();
	}


	@Override
	public InvestmentSecurity getTradeGroupSecondaryInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return position.getPositionBalance().getInvestmentSecurity();
	}
}

