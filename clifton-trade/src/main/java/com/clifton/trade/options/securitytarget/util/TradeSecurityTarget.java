package com.clifton.trade.options.securitytarget.util;

import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * <code>TradeSecurityTarget</code> is an enhanced {@link InvestmentAccountSecurityTarget} for  trading to include computed
 * and retrieved values necessary for perform trading activity for a client account's security target.
 * <p>
 * The adjusted values apply the target multiplier to the quantity and notional.  ABS is useful because the multiplier is negative, but when valuing or reporting on the target value we use positive values
 *
 * @author NickK
 */
public class TradeSecurityTarget {

	private final InvestmentAccountSecurityTarget securityTarget;

	private BigDecimal underlyingSecurityPrice;

	private BigDecimal targetUnderlyingQuantity;
	private BigDecimal targetUnderlyingNotional;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeSecurityTarget(InvestmentAccountSecurityTarget securityTarget) {
		this.securityTarget = securityTarget;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BigDecimal getTargetUnderlyingQuantityAdjusted() {
		return MathUtils.multiply(getSecurityTarget().getTargetUnderlyingMultiplier(), getTargetUnderlyingQuantity());
	}


	public BigDecimal getTargetUnderlyingQuantityAdjustedAbs() {
		return MathUtils.abs(getTargetUnderlyingQuantityAdjusted());
	}


	public BigDecimal getTargetUnderlyingNotionalAdjusted() {
		return MathUtils.multiply(getSecurityTarget().getTargetUnderlyingMultiplier(), getTargetUnderlyingNotional());
	}


	public BigDecimal getTargetUnderlyingNotionalAdjustedAbs() {
		return MathUtils.abs(getTargetUnderlyingNotionalAdjusted());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTarget getSecurityTarget() {
		return this.securityTarget;
	}


	public BigDecimal getUnderlyingSecurityPrice() {
		return this.underlyingSecurityPrice;
	}


	public void setUnderlyingSecurityPrice(BigDecimal underlyingSecurityPrice) {
		this.underlyingSecurityPrice = underlyingSecurityPrice;
	}


	public BigDecimal getTargetUnderlyingQuantity() {
		return this.targetUnderlyingQuantity;
	}


	public void setTargetUnderlyingQuantity(BigDecimal targetUnderlyingQuantity) {
		this.targetUnderlyingQuantity = targetUnderlyingQuantity;
	}


	public BigDecimal getTargetUnderlyingNotional() {
		return this.targetUnderlyingNotional;
	}


	public void setTargetUnderlyingNotional(BigDecimal targetUnderlyingNotional) {
		this.targetUnderlyingNotional = targetUnderlyingNotional;
	}
}
