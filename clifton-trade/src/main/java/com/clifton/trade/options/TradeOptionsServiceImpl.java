package com.clifton.trade.options;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.collections.BigDecimalMap;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyService;
import com.clifton.trade.execution.TradeExecutionTypeService;
import com.clifton.trade.execution.search.TradeExecutingBrokerSearchCommand;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.options.impl.TradeOptionsAssignmentStrategy;
import com.clifton.trade.options.impl.TradeOptionsDeliveryOffsetStrategy;
import com.clifton.trade.options.impl.TradeOptionsDeliveryStrategy;
import com.clifton.trade.options.impl.TradeOptionsExpirationStrategy;
import com.clifton.trade.options.impl.TradeOptionsRollForwardStrategy;
import com.clifton.trade.search.TradeSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
public class TradeOptionsServiceImpl implements TradeOptionsService {

	private ApplicationContextService applicationContextService;

	private AccountingBalanceService accountingBalanceService;
	private BusinessCompanyService businessCompanyService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private SecurityUserService securityUserService;
	private TradeDestinationService tradeDestinationService;
	private TradeService tradeService;
	private TradeExecutionTypeService tradeExecutionTypeService;
	private TradeGroupService tradeGroupService;
	private TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<TradeOptionsPositionBalance> getTradeOptionPositionBalanceList(AccountingBalanceSearchForm searchForm) {
		searchForm.setInvestmentTypeName(InvestmentType.OPTIONS);
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
		//Set the transaction date to today (balances of positions should be current)
		searchForm.setTransactionDate(DateUtils.clearTime(new Date()));
		//Get the balances of the option positions
		List<AccountingBalance> positionBalances = getAccountingBalanceService().getAccountingBalanceList(searchForm);

		//Get all pending trades so that the number of remaining option contracts to be processed can be calculated
		List<Trade> pendingTrades = getApplicablePendingTradeList(positionBalances);

		OptionPositionBalanceQuantityCalculator optionPositionBalanceQuantityCalculator = new OptionPositionBalanceQuantityCalculator();
		optionPositionBalanceQuantityCalculator.applyPendingTradeListForPositionAdjustments(pendingTrades);

		return CollectionUtils.getStream(positionBalances)
				.map(optionPositionBalanceQuantityCalculator::getRemainingPositionBalances)
				.collect(Collectors.toList());
	}


	@Override
	@Transactional(timeout = 180)
	public List<TradeGroup> generateTradeGroupListForTradeOptionsRequest(TradeOptionsRequest request) {
		return createTradeGroups(request);
	}

	///////////////////////////////////////////////////////////////////////////
	//////////                Private helper methods              /////////////
	///////////////////////////////////////////////////////////////////////////


	private List<Trade> getApplicablePendingTradeList(List<AccountingBalance> positionBalanceList) {
		if (CollectionUtils.isEmpty(positionBalanceList)) {
			return Collections.emptyList();
		}

		Set<Integer> clientAccountIdSet = new HashSet<>();
		Set<Integer> securityIdSet = new HashSet<>();
		Set<Integer> underlyingSecurityIdSet = new HashSet<>();
		CollectionUtils.getStream(positionBalanceList).forEach(position -> {
			clientAccountIdSet.add(position.getClientInvestmentAccount().getId());
			securityIdSet.add(position.getInvestmentSecurity().getId());
			if (position.getInvestmentSecurity().getUnderlyingSecurity() != null) {
				underlyingSecurityIdSet.add(position.getInvestmentSecurity().getUnderlyingSecurity().getId());
			}
		});

		if (!underlyingSecurityIdSet.isEmpty()) {
			// add underlying IDs
			securityIdSet.addAll(underlyingSecurityIdSet);
			List<InvestmentSecurity> miniSecurityList = getMiniSecurityListForBigSecurityIds(underlyingSecurityIdSet.toArray(new Integer[0]));
			CollectionUtils.getStream(miniSecurityList).forEach(miniSecurity -> securityIdSet.add(miniSecurity.getId()));
		}

		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setExcludeWorkflowStatusName(TradeService.TRADES_CLOSED_STATUS_NAME);
		tradeSearchForm.setSecurityIds(securityIdSet.toArray(new Integer[0]));
		tradeSearchForm.setClientInvestmentAccountIds(clientAccountIdSet.toArray(new Integer[0]));
		return getTradeService().getTradeList(tradeSearchForm);
	}


	private List<InvestmentSecurity> getMiniSecurityListForBigSecurityIds(Integer[] bigSecurityIds) {
		SecuritySearchForm littleSecuritySearchForm = new SecuritySearchForm();
		littleSecuritySearchForm.setBigSecurityIdList(bigSecurityIds);
		return getInvestmentInstrumentService().getInvestmentSecurityList(littleSecuritySearchForm);
	}


	/**
	 * Creates trades for the options or the underlying securities for those options with the specified settings. Trades are grouped into {@link TradeGroup}s by underlying the
	 * securities of the options.
	 *
	 * @param request the request from the user
	 */
	private List<TradeGroup> createTradeGroups(TradeOptionsRequest request) {
		ValidationUtils.assertTrue(request.getTradeDestination() != null && request.getTradeDestination().getId() != null, "A Trade Destination is required for creating Option trade groups.");
		request.setTradeDestination(getTradeDestinationService().getTradeDestination(request.getTradeDestination().getId()));
		ValidationUtils.assertTrue(request.getTradeGroupType() != null && request.getTradeGroupType().getId() != null, "A Trade Group Type is required for creating Option trade groups.");
		request.setTradeGroupType(getTradeGroupService().getTradeGroupType(request.getTradeGroupType().getId()));
		ValidationUtils.assertTrue(request.getTraderUser() != null && request.getTraderUser().getId() != null, "A Trader User is required for creating Option trade groups.");
		request.setTraderUser(getSecurityUserService().getSecurityUser(request.getTraderUser().getId()));

		TradeOptionsContext context = TradeOptionsContext.of(request, DateUtils.clearTime(new Date()))
				.setTradeOpenCloseTypeFunction(getTradeOpenCloseTypeFunction())
				.setStrategy(getTradeOptionsStrategyForTradeGroupType(request.getTradeGroupType()))
				.build();

		// Maps a (client account, holding account, security) String key to a newly built trades
		Map<String, List<Trade>> tradeMap = createTradeMapForTradeOptionsProcessingCommand(context);
		// the following must be executed after the trade map is created because the context's Option set gets populated
		populateBigToMiniSecurityMapOnTradeOptionsProcessingCommand(context);

		//Retrieve the position balances for the securities that are being expired
		List<TradeOptionsPositionBalance> optionBalances = getOptionPositionBalances(context.getOptionSet());
		Map<String, TradeGroup> tradeGroupMap = new HashMap<>();

		/*
		 * Iterate through all the retrieved position balances and cross-reference them with the tradeMap that contains
		 * the Trade objects built so far. Using the information from the position balances, set the remaining
		 * information on the trades.
		 */
		for (TradeOptionsPositionBalance optionBalance : CollectionUtils.getIterable(optionBalances)) {
			AccountingBalance balance = optionBalance.getPositionBalance();
			String key = BeanUtils.createKeyFromBeans(balance.getClientInvestmentAccount(), balance.getHoldingInvestmentAccount(), balance.getInvestmentSecurity());
			List<Trade> tradeList = tradeMap.get(key);
			if (!CollectionUtils.isEmpty(tradeList)) {
				context.getStrategy().validateTradeList(context, optionBalance, tradeList);

				// Add the trades to the applicable trade group. If a trade group is not available for the trade criteria, create a new group.
				InvestmentSecurity tradeGroupSecurity = context.getStrategy().getTradeGroupInvestmentSecurity(context, optionBalance, tradeList);
				InvestmentSecurity tradeGroupSecondarySecurity = context.getStrategy().getTradeGroupSecondaryInvestmentSecurity(context, optionBalance, tradeList);
				String tradeGroupKey = getTradeGroupKey(context.getTradeGroupType(), tradeGroupSecurity, tradeGroupSecondarySecurity);
				TradeGroup tradeGroup = CollectionUtils.getValue(tradeGroupMap, tradeGroupKey,
						() -> createTradeGroup(context, tradeGroupSecurity, tradeGroupSecondarySecurity));
				tradeGroup.getTradeList().addAll(tradeList);
			}
		}

		// Save the trade groups
		List<TradeGroup> tradeGroups = new ArrayList<>();
		tradeGroupMap.values().forEach(tradeGroup -> {
			if (context.getTradeGroupType().getGroupType() == TradeGroupType.GroupTypes.OPTION_TRADE_TO_CLOSE) {
				tradeGroups.addAll(createTradeToCloseTradeGroupList(tradeGroup));
			}
			else {
				tradeGroups.add(getTradeGroupService().saveTradeGroup(tradeGroup));
			}
		});

		return tradeGroups;
	}


	/**
	 * Returns a {@link TradeOptionsStrategy} for the provided {@link TradeGroupType}.
	 */
	private TradeOptionsStrategy getTradeOptionsStrategyForTradeGroupType(TradeGroupType tradeGroupType) {
		TradeOptionsStrategy strategy;
		switch (tradeGroupType.getGroupType()) {
			case OPTION_EXPIRATION:
			case OPTION_TRADE_TO_CLOSE: {
				strategy = new TradeOptionsExpirationStrategy();
				break;
			}
			case OPTION_PHYSICAL_DELIVERY: {
				strategy = new TradeOptionsDeliveryStrategy();
				break;
			}
			case OPTION_DELIVERY_OFFSET: {
				strategy = new TradeOptionsDeliveryOffsetStrategy();
				break;
			}
			case OPTION_ASSIGNMENT: {
				strategy = new TradeOptionsAssignmentStrategy();
				break;
			}
			case OPTION_ROLL_FORWARD: {
				strategy = new TradeOptionsRollForwardStrategy();
				break;
			}
			default: {
				throw new ValidationException("No Option Processing Strategy for Trade Group Type: " + tradeGroupType.getGroupType().name());
			}
		}

		getApplicationContextService().autowireBean(strategy);
		return strategy;
	}


	private TradeOptionsContext.TradeOptionsOpenCloseTypeFunction getTradeOpenCloseTypeFunction() {
		TradeOpenCloseType buy = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.BUY.getName());
		TradeOpenCloseType sell = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL.getName());
		TradeOpenCloseType buyToClose = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.BUY_CLOSE.getName());
		TradeOpenCloseType sellToClose = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL_CLOSE.getName());
		TradeOpenCloseType buyToOpen = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.BUY_OPEN.getName());
		TradeOpenCloseType sellToOpen = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL_OPEN.getName());
		return (position, trade, open) -> {
			if (position.getPositionBalance().getInvestmentSecurity().equals(trade.getInvestmentSecurity())) {
				if (MathUtils.isPositive(position.getPositionBalance().getQuantity())) {
					// position is positive; buy is opening, sell is closing
					return trade.isBuy() ? buyToOpen : sellToClose;
				}
				// position is negative; buy is closing, sell is opening
				return trade.isBuy() ? buyToClose : sellToOpen;
			}
			else if (InvestmentUtils.isOption(trade.getInvestmentSecurity())) {
				if (open) {
					return trade.isBuy() ? buyToOpen : sellToOpen;
				}
				return trade.isBuy() ? buyToClose : sellToClose;
			}
			return trade.isBuy() ? buy : sell;
		};
	}


	/**
	 * Returns a Map of trade identifier to List of {@link Trade}s for each {@link TradeOptionsRequestDetail}
	 * of the context's {@link TradeOptionsRequest}. The trades are generated by the context's {@link TradeGroupType}.
	 * <p>
	 * The trade identifier key is composed of Client Account, Holding Account and Option Security, which can be
	 * mapped to an Option position for validation.
	 * <p>
	 * Each detail's DTOs will be hydrated and the {@link TradeOptionsContext} will be populated with each traded Option security.
	 *
	 * @param context the {@link TradeOptionsContext} containing the Option processing request details
	 */
	private Map<String, List<Trade>> createTradeMapForTradeOptionsProcessingCommand(TradeOptionsContext context) {
		//Maps a (client account, holding account, security) String key to a newly built trade
		Map<String, List<Trade>> tradeMap = new HashMap<>();

		// Returns true if the provided identity object is not null and has a non null identity
		Predicate<IdentityObject> detailValidationFunction = identityObject -> identityObject != null && identityObject.getIdentity() != null;

		/*
		 * First iterate through all the TradeOptionsRequestDetail objects, generate a trade based on each one, and put
		 * each trade into the map so that they're grouped by the client account, holding account, and security.
		 */
		CollectionUtils.getStream(context.getRequest().getDetails())
				.forEach(detail -> {
					// validate detail properties
					ValidationUtils.assertTrue(detailValidationFunction.test(detail.getClientInvestmentAccount()), "Each Option detail to process must have a Client Investment Account defined.");
					ValidationUtils.assertTrue(detailValidationFunction.test(detail.getHoldingInvestmentAccount()), "Each Option detail to process must have a Holding Investment Account defined.");
					ValidationUtils.assertTrue(detailValidationFunction.test(detail.getInvestmentSecurity()), "Each Option detail to process must have an Investment Security defined.");
					BusinessCompany executingBroker = ObjectUtils.coalesce(detail.getExecutingBroker(), context.getRequest().getExecutingBroker());
					ValidationUtils.assertTrue(detailValidationFunction.test(executingBroker), "Each Option detail to process must have an Executing Broker defined.");
					// hydrate details
					detail.setClientInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(detail.getClientInvestmentAccount().getId()));
					detail.setHoldingInvestmentAccount(getInvestmentAccountService().getInvestmentAccount(detail.getHoldingInvestmentAccount().getId()));
					detail.setInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(detail.getInvestmentSecurity().getId()));
					detail.setExecutingBroker(getBusinessCompanyService().getBusinessCompany(executingBroker.getId()));
					if (BeanUtils.getBeanIdentity(detail.getTradeExecutionType()) != null) {
						detail.setTradeExecutionType(getTradeExecutionTypeService().getTradeExecutionType(detail.getTradeExecutionType().getId()));
					}
					if (BeanUtils.getBeanIdentity(detail.getRollSecurity()) != null) {
						detail.setRollSecurity(getInvestmentInstrumentService().getInvestmentSecurity(detail.getRollSecurity().getId()));
					}
					Short tradeSourceId = BeanUtils.getBeanIdentity(ObjectUtils.coalesce(detail.getTradeSource(), context.getRequest().getTradeSource()));
					if (tradeSourceId != null) {
						detail.setTradeSource(getTradeService().getTradeSource(tradeSourceId));
					}
					// populate context option sets
					context.getOptionSet().add(detail.getInvestmentSecurity());

					String key = BeanUtils.createKeyFromBeans(detail.getClientInvestmentAccount(), detail.getHoldingInvestmentAccount(), detail.getInvestmentSecurity());
					tradeMap.put(key, context.getStrategy().generateTradeList(context, detail));
				});

		return tradeMap;
	}


	/**
	 * If the option processing strategy requires the "big" to "mini" security mapping, populate it from the traded options' underlying security.
	 * We do this all at once so that only one database call is needed.
	 */
	private void populateBigToMiniSecurityMapOnTradeOptionsProcessingCommand(TradeOptionsContext context) {
		/*
		 * If the option processing strategy requires the "big" to "mini" security mapping, populate it from the traded options' underlying security.
		 * We do this all at once so that only one database call is needed.
		 */
		if (context.getStrategy().isOptionUnderlyingBigToMiniMapRequired()) {
			Integer[] optionUnderlyingSecurityIds = CollectionUtils.getStream(context.getOptionSet())
					.map(option -> option.getUnderlyingSecurity().getId())
					.toArray(Integer[]::new);
			CollectionUtils.getStream(getMiniSecurityListForBigSecurityIds(optionUnderlyingSecurityIds))
					.filter(miniSecurity -> miniSecurity.getBigSecurity() != null)
					.forEach(miniSecurity -> context.getOptionUnderlyingBigToMiniMap().put(miniSecurity.getBigSecurity(), miniSecurity));
		}
	}


	private List<TradeOptionsPositionBalance> getOptionPositionBalances(Collection<InvestmentSecurity> options) {
		AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
		searchForm.setInvestmentSecurityIds(BeanUtils.getBeanIdentityArray(options, Integer.class));
		return getTradeOptionPositionBalanceList(searchForm);
	}


	private String getTradeGroupKey(TradeGroupType groupType, InvestmentSecurity groupInvestmentSecurity, InvestmentSecurity originalPositionOptionSecurity) {
		if (groupType.getGroupType() == TradeGroupType.GroupTypes.OPTION_DELIVERY_OFFSET) {
			return BeanUtils.createKeyFromBeans(groupInvestmentSecurity, originalPositionOptionSecurity);
		}
		return BeanUtils.createKeyFromBeans(groupInvestmentSecurity);
	}


	private TradeGroup createTradeGroup(TradeOptionsContext context, InvestmentSecurity groupInvestmentSecurity, InvestmentSecurity groupSecondaryInvestmentSecurity) {
		TradeGroup tradeGroup = new TradeGroup();
		tradeGroup.setTradeGroupType(context.getTradeGroupType());
		tradeGroup.setTraderUser(context.getRequest().getTraderUser());
		tradeGroup.setTradeDate(context.getTradeDate());
		tradeGroup.setInvestmentSecurity(groupInvestmentSecurity);
		tradeGroup.setSecondaryInvestmentSecurity(groupSecondaryInvestmentSecurity);
		tradeGroup.setTradeList(new ArrayList<>());
		return tradeGroup;
	}


	/**
	 * Creates at least one {@link TradeGroup} for an Option Trade To Close type. Option Trade To Close trade groups can be strangled, so they can only process Put/Call
	 * combinations with the following criteria. <br>- a group can contain long or short trades, not both <br>- a group can contain Trades for options of the same underlying
	 * instrument <br>- a group can only contain Trades for one put option and one call option, thus, Trades for put options with different strike prices must be in separate
	 * groups
	 */
	private List<TradeGroup> createTradeToCloseTradeGroupList(TradeGroup tradeGroupWithTradesToPartition) {
		List<TradeGroup> tradeGroupList = new ArrayList<>();
		// Process long and short trades individually.
		Map<Boolean, List<Trade>> buySellTradeMap = BeanUtils.getBeansMap(tradeGroupWithTradesToPartition.getTradeList(), Trade::isBuy);

		// support stock trades
		List<Trade> underlyingTradeList = CollectionUtils.getFiltered(tradeGroupWithTradesToPartition.getTradeList(), trade -> trade.getTradeType().getName().equals(TradeType.STOCKS) || trade.getTradeType().getName().equals(TradeType.FUNDS));
		Map<InvestmentSecurity, List<Trade>> securityTradeListMap = BeanUtils.getBeansMap(underlyingTradeList, Trade::getInvestmentSecurity);
		securityTradeListMap.forEach((security, tradeList) -> {
			if (!CollectionUtils.isEmpty(tradeList)) {
				TradeGroup stockTradeGroup = new TradeGroup();
				stockTradeGroup.setInvestmentSecurity(security);
				stockTradeGroup.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.MULTI_CLIENT.getTypeName()));
				stockTradeGroup.setTradeDate(tradeGroupWithTradesToPartition.getTradeDate());
				stockTradeGroup.setTraderUser(tradeGroupWithTradesToPartition.getTraderUser());
				stockTradeGroup.setTradeList(tradeList);
				tradeGroupList.add(getTradeGroupService().saveTradeGroup(stockTradeGroup));
			}
		});
		buySellTradeMap.forEach((buy, buySellTradeList) -> {
			// Group trades by option so they can be grouped with one put type and one call type per group.
			Map<InvestmentSecurity, List<Trade>> optionSecurityTradeMap = BeanUtils.getBeansMap(buySellTradeList, Trade::getInvestmentSecurity);
			// group securities by option type (put/call)
			Map<InvestmentSecurityOptionTypes, List<InvestmentSecurity>> putCallOptionSecurityMap = BeanUtils.getBeansMap(optionSecurityTradeMap.keySet(), InvestmentSecurity::getOptionType);
			Iterator<InvestmentSecurity> putSecurityIterator = CollectionUtils.asNonNullList(putCallOptionSecurityMap.get(InvestmentSecurityOptionTypes.PUT)).iterator();
			Iterator<InvestmentSecurity> callSecurityIterator = CollectionUtils.asNonNullList(putCallOptionSecurityMap.get(InvestmentSecurityOptionTypes.CALL)).iterator();
			while (putSecurityIterator.hasNext() || callSecurityIterator.hasNext()) {
				List<Trade> groupTradeList = new ArrayList<>();
				// Add trades for a Put option if there is another.
				if (putSecurityIterator.hasNext()) {
					groupTradeList.addAll(optionSecurityTradeMap.get(putSecurityIterator.next()));
				}
				// Add trades for a Call option if there is another.
				if (callSecurityIterator.hasNext()) {
					groupTradeList.addAll(optionSecurityTradeMap.get(callSecurityIterator.next()));
				}
				// Create a group.
				if (!CollectionUtils.isEmpty(groupTradeList)) {
					TradeGroup toSave = new TradeGroup();
					BeanUtils.copyProperties(tradeGroupWithTradesToPartition, toSave);
					toSave.setTradeList(groupTradeList);
					tradeGroupList.add(getTradeGroupService().saveTradeGroup(toSave));
				}
			}
		});
		return tradeGroupList;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////                 Getters and Setters                /////////////
	///////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public TradeExecutingBrokerCompanyService getTradeExecutingBrokerCompanyService() {
		return this.tradeExecutingBrokerCompanyService;
	}


	public void setTradeExecutingBrokerCompanyService(TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService) {
		this.tradeExecutingBrokerCompanyService = tradeExecutingBrokerCompanyService;
	}


	public TradeExecutionTypeService getTradeExecutionTypeService() {
		return this.tradeExecutionTypeService;
	}


	public void setTradeExecutionTypeService(TradeExecutionTypeService tradeExecutionTypeService) {
		this.tradeExecutionTypeService = tradeExecutionTypeService;
	}

///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * <code>OptionPositionBalanceQuantityCalculator</code> is a calculator that applies pending trades to {@link AccountingBalance} to calculate the remaining positions for
	 * expiring and delivery of expiring Options.
	 */
	private class OptionPositionBalanceQuantityCalculator {

		//Get the option-related trade group types
		private final TradeGroupType optionExpireGroupType = getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_EXPIRATION.getTypeName());
		private final TradeGroupType optionTradeToCloseGroupType = getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_TRADE_TO_CLOSE.getTypeName());
		private final TradeGroupType optionAssignmentGroupType = getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_ASSIGNMENT.getTypeName());
		private final TradeGroupType optionRollForwardGroupType = getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_ROLL_FORWARD.getTypeName());
		private final TradeGroupType optionDeliveryGroupType = getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_PHYSICAL_DELIVERY.getTypeName());
		private final TradeGroupType optionDeliveryOffsetGroupType = getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_DELIVERY_OFFSET.getTypeName());

		//Maps a big security to its corresponding mini security
		private final Map<InvestmentSecurity, InvestmentSecurity> bigToMiniMap = new HashMap<>();

		//Maps a (client account, holding account, security, trade group type) String key to the number of existing option "processing" trades
		private final BigDecimalMap<String> pendingOptionContractProcessingQuantityMap = new BigDecimalMap<>();


		/**
		 * Populates a local cache of pending Option trade quantities in this calculator for making adjustmensts to an open Option position.
		 */
		void applyPendingTradeListForPositionAdjustments(List<Trade> pendingTradeList) {
			for (Trade trade : CollectionUtils.getIterable(pendingTradeList)) {
				TradeGroup group = getTradeOriginalTradeGroup(trade);
				/*
				 * All trades that we are concerned with here should have a trade group specified (Option Expiration, Option
				 * Physical Delivery, or Option Delivery Offset). If a group is not specified, we automatically know that
				 * the trade is not relevant to options processing.
				 */
				if (group != null) {
					InvestmentSecurity tradedSecurity = trade.getInvestmentSecurity();
					if (tradedSecurity.getBigSecurity() != null) {
						this.bigToMiniMap.put(tradedSecurity.getBigSecurity(), tradedSecurity);
					}

					//Create a key to uniquely identify the primary components of the trade. This key will be used when comparing with Option positions.
					List<Object> keyComponents = new ArrayList<>();
					keyComponents.add(trade.getClientInvestmentAccount());
					keyComponents.add(trade.getHoldingInvestmentAccount());
					keyComponents.add(tradedSecurity);
					keyComponents.add(group.getTradeGroupType());
					keyComponents.add(trade.isBuy());
					if (group.getTradeGroupType().equals(this.optionDeliveryGroupType) ||
							(group.getTradeGroupType().equals(this.optionAssignmentGroupType) && MathUtils.isGreaterThan(trade.getAverageUnitPrice(), BigDecimal.ZERO))) {
						keyComponents.add(CoreMathUtils.formatNumber(trade.getAverageUnitPrice(), null));
					}
					else if (group.getTradeGroupType().equals(this.optionDeliveryOffsetGroupType)) {
						keyComponents.add(group.getSecondaryInvestmentSecurity());
					}

					String key = BeanUtils.createKeyFromBeans(keyComponents.toArray());
					this.pendingOptionContractProcessingQuantityMap.add(key, trade.getQuantityIntended());
				}
			}
		}


		/**
		 * Returns the parent, original, {@link TradeGroup} for a trade. Some trade groups are capable of producing child groups.
		 * For example, the Trade To Close can produce strangle and strangle tail child groups.
		 * <p>
		 * Returns null if the trade is not a member of a trade group.
		 */
		private TradeGroup getTradeOriginalTradeGroup(Trade trade) {
			TradeGroup group = trade.getTradeGroup();
			while (group != null && group.getParent() != null) {
				group = group.getParent();
			}
			return group;
		}


		/**
		 * Applies the pending trades provided at construction to the provided {@link AccountingBalance} to build the {@link TradeOptionsPositionBalance} containing the remaining
		 * expiration and delivery quantities.
		 * <p>
		 * To do this, the following values that need to be calculated by comparing existing pending trades with an option position: <br/>1. Number of remaining contracts to expire
		 * <br/>2. Number of remaining contracts to deliver <br/>3. Number of remaining contracts to offset delivery
		 * <p>
		 * Note that #1 involves looking up trades that are trading the actual option security itself, whereas #2 and #3 involve looking up trades with the underlying of the option
		 * security.
		 */
		TradeOptionsPositionBalance getRemainingPositionBalances(AccountingBalance accountingBalance) {
			InvestmentSecurity option = accountingBalance.getInvestmentSecurity();
			BigDecimal positionQuantity = getRemainingExpirationQuantity(accountingBalance, option);
			BigDecimal numRemainingToDeliver = null, numRemainingToOffsetDelivery = null;
			if (option.getInstrument().isDeliverable() && option.getUnderlyingSecurity() != null) {
				BigDecimal positionUnderlyingDeliverableQuantity = getPositionUnderlyingDeliverableQuantity(accountingBalance);
				numRemainingToDeliver = getRemainingDeliveryQuantity(accountingBalance, positionUnderlyingDeliverableQuantity);
				numRemainingToOffsetDelivery = getRemainingDeliveryOffsetQuantity(accountingBalance, positionUnderlyingDeliverableQuantity);
			}
			return new TradeOptionsPositionBalance(accountingBalance, positionQuantity, numRemainingToDeliver, numRemainingToOffsetDelivery, getDefaultExecutingBroker(accountingBalance));
		}


		/**
		 * Determines and returns the default executing broker considering broker restrictions to select a valid
		 * broker.
		 */
		private BusinessCompany getDefaultExecutingBroker(AccountingBalance accountingBalance) {
			BusinessCompany proposedExecutingBroker = accountingBalance.getHoldingInvestmentAccount().getIssuingCompany();
			if (accountingBalance.getHoldingInvestmentAccount().getType().isExecutionWithIssuerRequired()) {
				return proposedExecutingBroker;
			}

			Date currentDate = DateUtils.clearTime(new Date());

			// make sure the executing broker is on the account's available broker list
			TradeExecutingBrokerSearchCommand command = new TradeExecutingBrokerSearchCommand();
			command.setUsePagingArray(false);
			command.setSecurityId(accountingBalance.getInvestmentSecurity().getId());
			command.setClientInvestmentAccountId(accountingBalance.getClientInvestmentAccount().getId());
			command.setActiveOnDate(currentDate);

			List<BusinessCompany> availableBrokerList = getTradeExecutingBrokerCompanyService().getTradeExecutingBrokerCompanyList(command);
			if (availableBrokerList.contains(proposedExecutingBroker)) {
				return proposedExecutingBroker;
			}

			return availableBrokerList.isEmpty() ? null : availableBrokerList.get(0);
		}


		private BigDecimal getRemainingExpirationQuantity(AccountingBalance accountingBalance, InvestmentSecurity optionSecurity) {
			// trade direction for trades of expiration, buy to close, and assignments are in the opposite direction of the position
			boolean buy = MathUtils.isNegative(accountingBalance.getQuantity());
			// apply expiration trades
			String key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), optionSecurity, this.optionExpireGroupType, buy);
			BigDecimal pendingContracts = this.pendingOptionContractProcessingQuantityMap.get(key);
			// apply trade to close trades
			key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), optionSecurity, this.optionTradeToCloseGroupType, buy);
			pendingContracts = MathUtils.add(pendingContracts, this.pendingOptionContractProcessingQuantityMap.get(key));
			// apply assignment trades
			key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), optionSecurity, this.optionAssignmentGroupType, buy);
			pendingContracts = MathUtils.add(pendingContracts, this.pendingOptionContractProcessingQuantityMap.get(key));
			// apply roll forward trades
			key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), optionSecurity, this.optionRollForwardGroupType, buy);
			pendingContracts = MathUtils.add(pendingContracts, this.pendingOptionContractProcessingQuantityMap.get(key));

			return MathUtils.reduceAbsValue(accountingBalance.getQuantity(), pendingContracts);
		}


		private BigDecimal getRemainingDeliveryQuantity(AccountingBalance accountingBalance, BigDecimal positionUnderlyingDeliverableQuantity) {
			InvestmentSecurity optionSecurity = accountingBalance.getInvestmentSecurity();
			// Calculate number of remaining contracts to be delivered.
			boolean buy = optionSecurity.isCallOption() ? MathUtils.isPositive(accountingBalance.getQuantity()) : MathUtils.isNegative(accountingBalance.getQuantity());
			String strikePriceString = CoreMathUtils.formatNumber(optionSecurity.getOptionStrikePrice(), null);
			// apply physical delivery trades
			String key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), optionSecurity.getUnderlyingSecurity(), this.optionDeliveryGroupType, buy, strikePriceString);
			BigDecimal numPendingContracts = this.pendingOptionContractProcessingQuantityMap.get(key);
			// apply assignment trades
			key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), optionSecurity.getUnderlyingSecurity(), this.optionAssignmentGroupType, buy, strikePriceString);
			numPendingContracts = MathUtils.add(numPendingContracts, this.pendingOptionContractProcessingQuantityMap.get(key));

			return MathUtils.subtract(positionUnderlyingDeliverableQuantity, numPendingContracts);
		}


		private BigDecimal getRemainingDeliveryOffsetQuantity(AccountingBalance accountingBalance, BigDecimal positionUnderlyingDeliverableQuantity) {
			InvestmentSecurity optionSecurity = accountingBalance.getInvestmentSecurity();
			// Calculate number of remaining contracts to be offset for delivery
			boolean buy = optionSecurity.isCallOption() ? MathUtils.isNegative(accountingBalance.getQuantity()) : MathUtils.isPositive(accountingBalance.getQuantity());
			String key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), optionSecurity.getUnderlyingSecurity(), this.optionDeliveryOffsetGroupType, buy, optionSecurity);
			BigDecimal numPendingContracts = this.pendingOptionContractProcessingQuantityMap.get(key);
			BigDecimal numRemainingToOffsetDelivery = MathUtils.subtract(positionUnderlyingDeliverableQuantity, numPendingContracts);

			InvestmentSecurity mini = this.bigToMiniMap.get(optionSecurity.getUnderlyingSecurity());
			if (mini != null) {
				key = BeanUtils.createKeyFromBeans(accountingBalance.getClientInvestmentAccount(), accountingBalance.getHoldingInvestmentAccount(), mini, this.optionDeliveryOffsetGroupType, buy, optionSecurity);
				numPendingContracts = this.pendingOptionContractProcessingQuantityMap.get(key);

				//Subtract the number of offsetting delivery trades in the mini security (in the adjusted quantity) from the number of remaining contracts to offset
				BigDecimal quantityAdjustFactor = MathUtils.divide(mini.getPriceMultiplier(), optionSecurity.getUnderlyingSecurity().getPriceMultiplier());
				numRemainingToOffsetDelivery = MathUtils.subtract(numRemainingToOffsetDelivery, MathUtils.multiply(numPendingContracts, quantityAdjustFactor));
			}
			return numRemainingToOffsetDelivery;
		}


		/**
		 * Returns the underlying quantity that can be delivered for the provided position.
		 * <p>
		 * The underlying quantity for an option on delivery is not always one to one. We need to compare the price multipliers of the option to that of the underlying.
		 * The multiplier on a security helps derive the value of the ultimate underlying. If the multipliers are the same, the quantities are the same. If they differ,
		 * we need to calculate the underlying quantity using the option's multiplier.
		 * <p>
		 * For example, if we have an SP option, the underlying SP Future and the option both have a multiplier of 250 because each contract represents a quantity of 250 SPX.
		 * This is true for nearly all options on futures (option quantity = future delivery quantity). It is not the same for options on stocks or funds, where the 1 contract
		 * of the option often represents a quantity of 100 of the underlying stock/future.
		 */
		private BigDecimal getPositionUnderlyingDeliverableQuantity(AccountingBalance accountingBalance) {
			InvestmentSecurity optionSecurity = accountingBalance.getInvestmentSecurity();
			BigDecimal accountingBalanceUnderlyingQuantity = accountingBalance.getQuantity().abs();
			if (!MathUtils.isEqual(optionSecurity.getPriceMultiplier(), optionSecurity.getUnderlyingSecurity().getPriceMultiplier())) {
				accountingBalanceUnderlyingQuantity = MathUtils.multiply(accountingBalanceUnderlyingQuantity, optionSecurity.getPriceMultiplier());
			}
			return accountingBalanceUnderlyingQuantity;
		}
	}
}
