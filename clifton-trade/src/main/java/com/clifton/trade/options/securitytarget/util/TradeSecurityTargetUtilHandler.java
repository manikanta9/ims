package com.clifton.trade.options.securitytarget.util;

import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>TradeSecurityTargetUtilHandler</code> is a utility for interacting with an {@link InvestmentAccountSecurityTarget} for trading purposes.
 *
 * @author NickK
 */
public interface TradeSecurityTargetUtilHandler {

	/**
	 * Returns a {@link TradeSecurityTarget} for the provided {@link InvestmentAccountSecurityTarget} for the date specified.
	 * The date is used to obtain the underlying security price of the security target, and any other necessary date specified details (e.g. manager balance value).
	 * <p>
	 * Since manager balances and closing prices are typically not yet available for the current date, if <code>date</code> is the current date or in the future then all values
	 * will be flexibly retrieved and the latest available data will be used.
	 */
	public TradeSecurityTarget getTradeSecurityTargetForDate(InvestmentAccountSecurityTarget securityTarget, Date date);


	/**
	 * Returns a {@link TradeSecurityTarget} for the provided {@link InvestmentAccountSecurityTarget} for the date specified.
	 * If the price date is outside of the securityTarget start and end date it will adjust either the contracts (adjustPrice = false) or price (adjustPrice = true) to the comparable value (occurs if there is a split)
	 * by comparing the market value adjustment factor of the two dates. (i.e. give me today's target using price from last week - if a split occured during that time we need to adjust)
	 * <p>
	 * Since manager balances and closing prices are typically not yet available for the current date, if <code>date</code> is the current date or in the future then all values
	 * will be flexibly retrieved and the latest available data will be used.
	 */
	public TradeSecurityTarget getTradeSecurityTargetForDateWithPriceDate(InvestmentAccountSecurityTarget securityTarget, Date date, Date priceDate);


	/**
	 * Returns a {@link TradeSecurityTarget} for the provided {@link InvestmentAccountSecurityTarget} for the date specified using the provided underlying security price.
	 * The date is used to obtain necessary date specified details (e.g. manager balance value).
	 * <p>
	 * Since manager balances and closing prices are typically not yet available for the current date, if <code>date</code> is the current date or in the future then all values
	 * will be flexibly retrieved and the latest available data will be used.
	 */
	public TradeSecurityTarget getTradeSecurityTargetWithUnderlyingSecurityPriceForDate(InvestmentAccountSecurityTarget securityTarget, Date date, BigDecimal underlyingSecurityPrice);
}
