package com.clifton.trade.options.impl;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.options.TradeOptionsContext;
import com.clifton.trade.options.TradeOptionsPositionBalance;
import com.clifton.trade.options.TradeOptionsRequestDetail;
import com.clifton.trade.options.TradeOptionsStrategy;

import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * <code>TradeOptionsExpirationStrategy</code> is a {@link TradeOptionsStrategy} for processing Option expirations.
 *
 * @author NickK
 */
public class TradeOptionsExpirationStrategy extends AbstractTradeOptionsStrategy {

	@Override
	public List<Trade> generateTradeList(TradeOptionsContext context, TradeOptionsRequestDetail detail) {
		List<Trade> tradeList = MathUtils.isNullOrZero(detail.getTradeQuantity()) ? Collections.emptyList()
				: CollectionUtils.createList(generateTrade(context, detail, detail.getInvestmentSecurity(), detail.getAverageUnitPrice()));

		if (detail.isTradeUnderlying()) {
			tradeList = addUnderlyingTrade(context, detail, tradeList);
		}

		return tradeList;
	}


	@Override
	protected boolean isSingleTradePerPosition() {
		// may optionally contain an underlying trade.
		return false;
	}


	@Override
	public void validateTradeList(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		Map<Boolean, List<Trade>> optionTradeListMap = getOptionTradeListMap(tradeList);
		List<Trade> optionTradeList = optionTradeListMap.get(Boolean.TRUE);
		ValidationUtils.assertTrue(CollectionUtils.getSize(optionTradeList) == 1,
				"For closing an Option position, only one trade is expected.");

		optionTradeList.forEach(trade -> {
			if (trade != null) {
				if (trade.getInvestmentSecurity().equals(position.getPositionBalance().getInvestmentSecurity())) {
					// This is the closing trade. Validate the quantity and set the buy and openCloseType properties.
					ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(trade.getQuantityIntended(), position.getTradeQuantityForExpire()),
							"The Trade for closing the Option position must be of the same quantity as the remaining position value [" + position.getTradeQuantityForExpire() + "]. There may be pending trades.");
					boolean buyToClose = MathUtils.isNegative(position.getPositionBalance().getQuantity());
					// trade the inverse of the position to close it
					trade.setBuy(buyToClose);
					trade.setOpenCloseType(context.getTradeOpenCloseTypeFunction().apply(position, trade));
				}
			}
		});

		List<Trade> underlyingTradeList = optionTradeListMap.get(Boolean.FALSE);
		if (!CollectionUtils.isEmpty(underlyingTradeList)) {
			ValidationUtils.assertTrue(underlyingTradeList.size() == 1, "For closing trades, only one trade for the underlying security is expected.");
			Trade underlyingTrade = CollectionUtils.getFirstElement(underlyingTradeList);
			if (underlyingTrade != null) {
				ValidationUtils.assertEquals(underlyingTrade.getInvestmentSecurity(), optionTradeList.get(0).getInvestmentSecurity().getUnderlyingSecurity(), "The underlying security traded should be the same as the underlying security of the traded Options.");
			}
		}
	}


	@Override
	public InvestmentSecurity getTradeGroupInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return position.getPositionBalance().getInvestmentSecurity().getUnderlyingSecurity();
	}
}
