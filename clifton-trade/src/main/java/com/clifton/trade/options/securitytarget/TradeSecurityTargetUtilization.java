package com.clifton.trade.options.securitytarget;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * <code>TradeSecurityTargetUtilization</code> contains covered, pending, and frozen quantities for
 * showing utilization of a {@link InvestmentAccountSecurityTarget}. This utilization object will contain
 * a {@link TradeSecurityTargetUtilizationDetail} for each security applicable to the target.
 *
 * @author NickK
 */
@NonPersistentObject
public class TradeSecurityTargetUtilization {

	private final InvestmentAccountSecurityTarget securityTarget;
	/**
	 * The date this utilization is valid for.
	 */
	private final Date balanceDate;

	/**
	 * Optional holding {@link InvestmentAccount} that can be populated based on a purpose provided in the service request's
	 * {@link com.clifton.trade.options.securitytarget.search.TradeSecurityTargetUtilizationCommand}. It is acceptable for this value to be null
	 * if the purpose was not included in the request, or more than one holding account exists for the purpose.
	 * <p>
	 * For example, for trading options for DeltaShift, the "Trading: Options" purpose can be provided to default the holding accounts in the usage grid.
	 */
	private final InvestmentAccount holdingInvestmentAccount;

	private final List<TradeSecurityTargetUtilizationDetail> detailList;

	/**
	 * The original target quantity from the {@link InvestmentAccountSecurityTarget}.
	 * <p>
	 * If the target is defined by quantity, this value will be that value. If the target is a notional, this value
	 * is calculated using the target's notional value and specified underlying security price.
	 */
	private final BigDecimal underlyingSecurityTargetQuantity;
	/**
	 * The quantity target from the client account underlying security target adjusted by the target multiplier.
	 */
	private final BigDecimal underlyingSecurityTargetQuantityAdjusted;

	/**
	 * The original target notional from the {@link InvestmentAccountSecurityTarget}.
	 * <p>
	 * If the target is defined by notional, this value will be that value. If the target is defined for quantity,
	 * this value will be the calculated value using the target quantity and specified underlying security price.
	 */
	private final BigDecimal underlyingSecurityTargetNotional;
	/**
	 * The notional target from the client account underlying security target adjusted by the target multiplier.
	 */
	private final BigDecimal underlyingSecurityTargetNotionalAdjusted;

	private final BigDecimal underlyingSecurityPrice;

	/**
	 * The details define only covered, pending, and frozen quantities. Both quantities and notionals calculated using
	 * {@link #getUnderlyingSecurityPrice()} are available here in order to show utilization for either value.
	 */
	private final BigDecimal coveredQuantity;
	private final BigDecimal frozenQuantity;
	private final BigDecimal pendingQuantity;

	private final BigDecimal coveredNotional;
	private final BigDecimal frozenNotional;
	private final BigDecimal pendingNotional;

	/**
	 * Client Account base currency cash balance
	 */
	private final BigDecimal cashBalance;

	/**
	 * MarketData fields that may be retrieved
	 */
	private final BigDecimal priceChangePercentYTD;
	private final BigDecimal priceChangePercentDaily;
	private final Date earningsDate;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private TradeSecurityTargetUtilization(Builder builder) {
		this.securityTarget = builder.securityTarget;
		this.balanceDate = builder.balanceDate;
		this.holdingInvestmentAccount = builder.holdingInvestmentAccount;
		this.detailList = builder.detailList;

		this.underlyingSecurityTargetQuantity = builder.underlyingSecurityTargetQuantity;
		this.underlyingSecurityTargetQuantityAdjusted = MathUtils.multiply(this.underlyingSecurityTargetQuantity, this.securityTarget.getTargetUnderlyingMultiplier());
		this.underlyingSecurityTargetNotional = builder.underlyingSecurityTargetNotional;
		this.underlyingSecurityTargetNotionalAdjusted = MathUtils.multiply(this.underlyingSecurityTargetNotional, this.securityTarget.getTargetUnderlyingMultiplier());

		this.underlyingSecurityPrice = builder.underlyingSecurityPrice;

		this.coveredQuantity = builder.coveredQuantity;
		this.frozenQuantity = builder.frozenQuantity;
		this.pendingQuantity = builder.pendingQuantity;
		this.coveredNotional = MathUtils.multiply(this.underlyingSecurityPrice, this.coveredQuantity);
		this.frozenNotional = MathUtils.multiply(this.underlyingSecurityPrice, this.frozenQuantity);
		this.pendingNotional = MathUtils.multiply(this.underlyingSecurityPrice, this.pendingQuantity);

		this.cashBalance = builder.cashBalance;

		this.priceChangePercentYTD = builder.priceChangePercentYTD;
		this.priceChangePercentDaily = builder.priceChangePercentDaily;
		this.earningsDate = builder.earningsDate;
	}


	public InvestmentAccountSecurityTarget getSecurityTarget() {
		return this.securityTarget;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public BigDecimal getUnderlyingSecurityTargetQuantity() {
		return this.underlyingSecurityTargetQuantity;
	}


	public BigDecimal getUnderlyingSecurityTargetQuantityAdjusted() {
		return this.underlyingSecurityTargetQuantityAdjusted;
	}


	public BigDecimal getUnderlyingSecurityTargetNotional() {
		return this.underlyingSecurityTargetNotional;
	}


	public BigDecimal getUnderlyingSecurityTargetNotionalAdjusted() {
		return this.underlyingSecurityTargetNotionalAdjusted;
	}


	public BigDecimal getUnderlyingSecurityPrice() {
		return this.underlyingSecurityPrice;
	}


	public BigDecimal getCoveredQuantity() {
		return this.coveredQuantity;
	}


	public BigDecimal getFrozenQuantity() {
		return this.frozenQuantity;
	}


	public BigDecimal getPendingQuantity() {
		return this.pendingQuantity;
	}


	public BigDecimal getTotalUsageQuantity() {
		return CoreMathUtils.sum(getCoveredQuantity(), getFrozenQuantity());
	}


	public BigDecimal getCoveredNotional() {
		return this.coveredNotional;
	}


	public BigDecimal getFrozenNotional() {
		return this.frozenNotional;
	}


	public BigDecimal getPendingNotional() {
		return this.pendingNotional;
	}


	public BigDecimal getTotalUsageNotional() {
		return CoreMathUtils.sum(getCoveredNotional(), getFrozenNotional());
	}


	public List<TradeSecurityTargetUtilizationDetail> getDetailList() {
		return this.detailList;
	}


	public BigDecimal getCashBalance() {
		return this.cashBalance;
	}


	public BigDecimal getPriceChangePercentYTD() {
		return this.priceChangePercentYTD;
	}


	public BigDecimal getPriceChangePercentDaily() {
		return this.priceChangePercentDaily;
	}


	public Date getEarningsDate() {
		return this.earningsDate;
	}


	public static Builder newBuilder(InvestmentAccountSecurityTarget securityTarget, Date balanceDate) {
		return new Builder(securityTarget, balanceDate);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static class Builder {

		private final InvestmentAccountSecurityTarget securityTarget;
		private final Date balanceDate;

		public final List<TradeSecurityTargetUtilizationDetail> detailList = new ArrayList<>();

		private InvestmentAccount holdingInvestmentAccount;

		private BigDecimal underlyingSecurityPrice;

		private BigDecimal underlyingSecurityTargetQuantity;
		private BigDecimal underlyingSecurityTargetNotional;

		private BigDecimal coveredQuantity;
		private BigDecimal frozenQuantity;
		private BigDecimal pendingQuantity;

		private BigDecimal cashBalance = BigDecimal.ZERO;

		private BigDecimal priceChangePercentYTD;
		private BigDecimal priceChangePercentDaily;
		private Date earningsDate;


		private Builder(InvestmentAccountSecurityTarget securityTarget, Date balanceDate) {
			this.securityTarget = securityTarget;
			this.balanceDate = balanceDate;
		}


		public InvestmentSecurity getTargetUnderlyingSecurity() {
			return this.securityTarget.getTargetUnderlyingSecurity();
		}


		public Builder withHoldingInvestmentAcocunt(InvestmentAccount holdingInvestmentAccount) {
			this.holdingInvestmentAccount = holdingInvestmentAccount;
			return this;
		}


		public Builder withUnderlyingSecurityPrice(BigDecimal underlyingSecurityPrice) {
			this.underlyingSecurityPrice = underlyingSecurityPrice;
			return this;
		}


		public Builder addDetail(TradeSecurityTargetUtilizationDetail detail) {
			if (detail != null) {
				this.detailList.add(detail);
				this.coveredQuantity = MathUtils.add(this.coveredQuantity, detail.getCoveredQuantity());
				this.frozenQuantity = MathUtils.add(this.frozenQuantity, detail.getFrozenQuantity());
				this.pendingQuantity = MathUtils.add(this.pendingQuantity, detail.getPendingQuantity());
			}
			return this;
		}


		public Builder withUnderlyingSecurityTargetQuantity(BigDecimal underlyingSecurityTargetQuantity) {
			this.underlyingSecurityTargetQuantity = underlyingSecurityTargetQuantity;
			return this;
		}


		public Builder withUnderlyingSecurityTargetNotional(BigDecimal underlyingSecurityTargetNotional) {
			this.underlyingSecurityTargetNotional = underlyingSecurityTargetNotional;
			return this;
		}


		public Builder withCashBalance(BigDecimal cashBalance) {
			this.cashBalance = cashBalance;
			return this;
		}


		public Builder withPriceChangePercentYTD(BigDecimal priceChangePercentYTD) {
			this.priceChangePercentYTD = priceChangePercentYTD;
			return this;
		}


		public Builder withPriceChangePercentDaily(BigDecimal priceChangePercentDaily) {
			this.priceChangePercentDaily = priceChangePercentDaily;
			return this;
		}


		public Builder withEarningsDate(Date earningsDate) {
			this.earningsDate = earningsDate;
			return this;
		}


		public TradeSecurityTargetUtilization build() {
			return new TradeSecurityTargetUtilization(this);
		}
	}
}
