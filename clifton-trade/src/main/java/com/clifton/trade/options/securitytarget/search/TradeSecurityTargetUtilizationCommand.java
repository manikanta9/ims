package com.clifton.trade.options.securitytarget.search;

import com.clifton.core.dataaccess.search.form.BaseRestrictiveSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;

import java.util.Date;


/**
 * <code>TradeSecurityTargetUtilizationCommand</code> defines parameters for calculating {@link com.clifton.trade.options.securitytarget.TradeSecurityTargetUtilization}s
 * with covered, pending, and frozen quantities for applicable {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget}s.
 *
 * @author NickK
 */
@SearchForm(hasOrmDtoClass = false)
public class TradeSecurityTargetUtilizationCommand extends BaseRestrictiveSearchForm {

	private static final String CLIENT_ACCOUNT_ID_RESTRICTION_KEY = "clientInvestmentAccountId";
	private static final String ACCOUNT_GROUP_ID_RESTRICTION_KEY = "investmentAccountGroupId";
	private static final String UNDERLYING_SECURITY_ID_RESTRICTION_KEY = "targetUnderlyingSecurityId";

	/**
	 * Date to use for calculating utilization for covered, frozen, and pending quantities.
	 */
	private Date balanceDate;

	/**
	 * If <code>true</code> then the prior price date will be used. This is useful when retrieving security targets for the current date, as balance values and the current day
	 * closing prices will not yet be available. Otherwise, the given {@link #balanceDate} will be used.
	 */
	private boolean usePriorPriceDate;

	private Integer clientInvestmentAccountId;
	private Integer[] clientInvestmentAccountIds;

	private Integer investmentAccountGroupId;

	private Integer targetUnderlyingSecurityId;
	private Integer[] targetUnderlyingSecurityIds;
	/**
	 * Optional purpose to use for defaulting holding {@link com.clifton.investment.account.InvestmentAccount} on
	 * {@link com.clifton.trade.options.securitytarget.TradeSecurityTargetUtilization} returned.
	 */
	private InvestmentAccountRelationshipPurpose holdingAccountPurpose;
	/**
	 * Optional flag to disable/enable MarketData inclusion in the results.
	 */
	private boolean includeMarketData;
	/**
	 * Optional flag to exclude getting the cash balance for each unique client investment account. The cash balance is retrieved by default.
	 */
	private boolean excludeCashBalance;
	/**
	 * Optional flag to exclude pending activity. Pending activity is included by default.
	 */
	private boolean excludePendingActivity;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (getBalanceDate() == null) {
			setBalanceDate(DateUtils.clearTime(new Date()));
		}
		if (getClientInvestmentAccountId() == null && containsSearchRestriction(CLIENT_ACCOUNT_ID_RESTRICTION_KEY)) {
			setClientInvestmentAccountId(Integer.valueOf((String) getSearchRestriction(CLIENT_ACCOUNT_ID_RESTRICTION_KEY).getValue()));
			removeSearchRestriction(CLIENT_ACCOUNT_ID_RESTRICTION_KEY);
		}
		if (getInvestmentAccountGroupId() == null && containsSearchRestriction(ACCOUNT_GROUP_ID_RESTRICTION_KEY)) {
			setInvestmentAccountGroupId(Integer.valueOf((String) getSearchRestriction(ACCOUNT_GROUP_ID_RESTRICTION_KEY).getValue()));
			removeSearchRestriction(ACCOUNT_GROUP_ID_RESTRICTION_KEY);
		}
		if (getTargetUnderlyingSecurityId() == null && containsSearchRestriction(UNDERLYING_SECURITY_ID_RESTRICTION_KEY)) {
			setTargetUnderlyingSecurityId(Integer.valueOf((String) getSearchRestriction(UNDERLYING_SECURITY_ID_RESTRICTION_KEY).getValue()));
			removeSearchRestriction(UNDERLYING_SECURITY_ID_RESTRICTION_KEY);
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public boolean isUsePriorPriceDate() {
		return this.usePriorPriceDate;
	}


	public void setUsePriorPriceDate(boolean usePriorPriceDate) {
		this.usePriorPriceDate = usePriorPriceDate;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Integer getTargetUnderlyingSecurityId() {
		return this.targetUnderlyingSecurityId;
	}


	public void setTargetUnderlyingSecurityId(Integer targetUnderlyingSecurityId) {
		this.targetUnderlyingSecurityId = targetUnderlyingSecurityId;
	}


	public Integer[] getTargetUnderlyingSecurityIds() {
		return this.targetUnderlyingSecurityIds;
	}


	public void setTargetUnderlyingSecurityIds(Integer[] targetUnderlyingSecurityIds) {
		this.targetUnderlyingSecurityIds = targetUnderlyingSecurityIds;
	}


	public InvestmentAccountRelationshipPurpose getHoldingAccountPurpose() {
		return this.holdingAccountPurpose;
	}


	public void setHoldingAccountPurpose(InvestmentAccountRelationshipPurpose holdingAccountPurpose) {
		this.holdingAccountPurpose = holdingAccountPurpose;
	}


	public boolean isIncludeMarketData() {
		return this.includeMarketData;
	}


	public void setIncludeMarketData(boolean includeMarketData) {
		this.includeMarketData = includeMarketData;
	}


	public boolean isExcludeCashBalance() {
		return this.excludeCashBalance;
	}


	public void setExcludeCashBalance(boolean excludeCashBalance) {
		this.excludeCashBalance = excludeCashBalance;
	}


	public boolean isExcludePendingActivity() {
		return this.excludePendingActivity;
	}


	public void setExcludePendingActivity(boolean excludePendingActivity) {
		this.excludePendingActivity = excludePendingActivity;
	}
}
