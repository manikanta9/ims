package com.clifton.trade.options;


import com.clifton.business.company.BusinessCompany;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.TradeSource;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.TradeGroupType;

import java.util.List;


/**
 * The <code>TradeOptionsRequest</code> represents a request to process an Option or set of Options for
 * expiration and/or exercise.
 *
 * @author jgommels
 */
public class TradeOptionsRequest {

	private SecurityUser traderUser;
	private TradeDestination tradeDestination;
	private BusinessCompany executingBroker;
	private TradeGroupType tradeGroupType;
	/**
	 * Optional {@link TradeSource} to be applied to the {@link TradeOptionsRequestDetail}s of this object defining where the resulting trades are generated from.
	 */
	private TradeSource tradeSource;
	private List<TradeOptionsRequestDetail> details;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public SecurityUser getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public BusinessCompany getExecutingBroker() {
		return this.executingBroker;
	}


	public void setExecutingBroker(BusinessCompany executingBroker) {
		this.executingBroker = executingBroker;
	}


	public TradeGroupType getTradeGroupType() {
		return this.tradeGroupType;
	}


	public void setTradeGroupType(TradeGroupType tradeGroupType) {
		this.tradeGroupType = tradeGroupType;
	}


	public TradeSource getTradeSource() {
		return this.tradeSource;
	}


	public void setTradeSource(TradeSource tradeSource) {
		this.tradeSource = tradeSource;
	}


	public List<TradeOptionsRequestDetail> getDetails() {
		return this.details;
	}


	public void setDetails(List<TradeOptionsRequestDetail> details) {
		this.details = details;
	}
}
