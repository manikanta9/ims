package com.clifton.trade.options.securitytarget.group.util;

import com.clifton.accounting.gl.position.AccountingPositionBalance;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.options.combination.TradeOptionCombinationTypes;
import com.clifton.trade.options.combination.TradeOptionLegTypes;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * A calculator to determine the number of tranches executed over a given date range.
 *
 * @author davidi
 */
@Component
public class TradeGroupTradeCycleUtilHandlerImpl implements TradeGroupTradeCycleUtilHandler {

	private static final String CLIENT_ACCOUNT_MAX_OPTION_SPREAD_COLUMN_NAME = BusinessServiceProcessingType.MAX_OPTION_SPREAD_PERCENT;

	/**
	 * A list of day offsets from the cycle starting date used to derive a future date for excess contract distributions.
	 * The first element is the day offset for the first excess contract, the second element is the day offset for the second excess contract, etc.
	 * This entry is valid for trading cycles of 4 weeks with up to and including 12 tranches.
	 * <p>
	 * The strategy allocates the first 4 excess contracts on Fridays, the second 4 excess contracts on Wednesdays, and the final 3 excess contracts on
	 * Mondays.  The distribution occurs on an interleaved pattern week 2, week 4, week 1, week 3 to even out contracts over the trading cycle.  The reasons
	 * for the distribution ordering of Fridays, Wednesdays, and Mondays are:
	 * (a) Certain accounts are only traded on Fridays (4 tranches), so its mandatory that the first 4 excess contracts coincide with these trading dates.  Similarly,
	 * accounts with lower notional values that amount to 8 out of 12 trades per cycle are preferably traded on Wednesdays and Fridays. Thus Wednesdays and Fridays will
	 * receive the first 8 excess contract distributions.
	 * (b) On Mondays, FLEX options are traded.  The investment staff prefers trading listed options (on Wednesdays and Fridays) over FLEX Options.  Thus, the excess contracts
	 * are preferably distributed to listed options sold on Wednesdays and Fridays, with only the final three excess contracts being applied to the FLEX option trade quantities.
	 * <p>
	 * <p>
	 * More of these lists can be added in the future to support different week cycles / tranche combinations.
	 */
	private static final List<Integer> DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES = CollectionUtils.createList(11, 25, 4, 18, 9, 23, 2, 16, 7, 21, 0);
	private static final List<Integer> DAY_OFFSET_3_WEEK_CYCLE_LIST_3_TRANCHES = CollectionUtils.createList(18, 4);

	private InvestmentAccountUtilHandler investmentAccountUtilHandler;

	private SystemColumnValueService systemColumnValueService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getStartOfCurrentCycleDate(Date balanceDate, int cycleDuration) {

		ValidationUtils.assertNotNull(balanceDate, "The \"balanceDate\" parameter must be defined (cannot be null).");
		ValidationUtils.assertTrue(cycleDuration > 0, "The \"cycleDuration\" parameter must have a value greater than 0.");

		final int daysPerWeek = 7;

		int weekCyclesInDays = -daysPerWeek * (getWeekCycle(balanceDate, cycleDuration));
		Date firstDayOfWeek = DateUtils.getFirstDayOfWeek(DateUtils.addDays(balanceDate, weekCyclesInDays));
		// Cycle starts on Mondays
		return DateUtils.addDays(firstDayOfWeek, 1);
	}


	@Override
	public BigDecimal getUnexpiredContractCount(InvestmentAccountSecurityTarget securityTarget, List<AccountingPositionBalance> accountingPositionBalanceList, Date balanceDate) {

		String tradeOpenCloseTypeName;
		InvestmentSecurityOptionTypes optionType;
		BusinessServiceProcessingType processingType = securityTarget.getClientInvestmentAccount().getServiceProcessingType();
		AssertUtils.assertNotNull(processingType, "No business service processing type found for account: \"" + securityTarget.getClientInvestmentAccount().getLabel() + "\"");
		TradeOptionLegTypes unexpiredContractFilterType = getUnexpiredContractFilterType(processingType);

		// Configure parameters to filter query results based on business service
		switch (unexpiredContractFilterType) {
			case LONG_CALL:
				tradeOpenCloseTypeName = TradeOpenCloseTypes.BUY_OPEN.getName();
				optionType = InvestmentSecurityOptionTypes.CALL;
				break;

			case LONG_PUT:
				tradeOpenCloseTypeName = TradeOpenCloseTypes.BUY_OPEN.getName();
				optionType = InvestmentSecurityOptionTypes.PUT;
				break;

			case SHORT_CALL:
				tradeOpenCloseTypeName = TradeOpenCloseTypes.SELL_OPEN.getName();
				optionType = InvestmentSecurityOptionTypes.CALL;
				break;

			case SHORT_PUT:
				tradeOpenCloseTypeName = TradeOpenCloseTypes.SELL_OPEN.getName();
				optionType = InvestmentSecurityOptionTypes.PUT;
				break;

			default:
				throw new ValidationException("Unsupported UnexpiredContractFilterType \"" + unexpiredContractFilterType + "\" found.");
		}


		// Filter on positions with InvestmentSecurity expiration after the balance date, as well as the side of the position:
		// negative quantities are short, positive quantities are long.  Sum up all absolute values of these quantities -- this is the contract count for spread.
		BigDecimal totalContractsCount = CollectionUtils.getStream(accountingPositionBalanceList)
				.filter(accountingPositionBalance -> positionFilterOnOptionType(accountingPositionBalance, optionType))
				.filter(accountingPositionBalance -> DateUtils.isDateAfter(accountingPositionBalance.getInvestmentSecurity().getLastDeliveryOrEndDate(), balanceDate))
				.filter(accountingPositionBalance -> tradeOpenCloseTypeName.equals(TradeOpenCloseTypes.BUY_OPEN.getName()) ? MathUtils.isGreaterThan(accountingPositionBalance.getRemainingQuantity(), BigDecimal.ZERO) : MathUtils.isLessThan(accountingPositionBalance.getRemainingQuantity(), BigDecimal.ZERO))
				.map(accountingPositionBalance -> MathUtils.abs(accountingPositionBalance.getRemainingQuantity()))
				.reduce(BigDecimal.ZERO, MathUtils::add);

		return totalContractsCount;
	}


	@Override
	public BigDecimal calculateExcessContractDistributionForCurrentTrade(InvestmentAccountSecurityTarget accountSecurityTarget, Date balanceDate, BigDecimal excessContracts) {

		AssertUtils.assertNotNull(accountSecurityTarget, "A value must be provided for the accountSecurityTarget parameter (cannot be null).");
		AssertUtils.assertNotNull(balanceDate, "A value must be provided for the balanceDate parameter (cannot be null).");

		Short trancheCount = getTrancheCount(accountSecurityTarget);
		Short cycleDurationWeeks = getCycleDurationWeeks(accountSecurityTarget);

		AssertUtils.assertNotNull(trancheCount, "A tranche count must be defined on the security target or business service processing type.");
		AssertUtils.assertNotNull(cycleDurationWeeks, "A cycle duration (weeks) value must be defined on the security target or the business service processing type.");
		AssertUtils.assertTrue(MathUtils.isLessThan(excessContracts, trancheCount), "Excess contracts for account '" + accountSecurityTarget.getClientInvestmentAccount().getLabel() + "' cannot exceed " + (trancheCount - 1) + " contracts.");

		if (MathUtils.isLessThanOrEqual(excessContracts, BigDecimal.ZERO)) {
			return BigDecimal.ZERO;
		}

		List<Integer> dayOffsetList = Collections.emptyList();
		// choose an appropriate DAY_OFFSET_LIST, and extract the entries corresponding to the excess contracts up-to and including the current excess contract.
		// (e.g. for 5 excess contracts we want to derive distribution dates for 1, 2, 3, 4, and 5 excess contracts (corresponding to index range 0 to 4).
		// Note: to support other combinations, define a DAY_OFFSET list, and choose it based on tranche count and cycle duration as shown below.
		if (cycleDurationWeeks == 4 && trancheCount <= 12) {
			dayOffsetList = DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.subList(0, excessContracts.intValue());
		}
		else if (cycleDurationWeeks == 3 && trancheCount <= 3) {
			dayOffsetList = DAY_OFFSET_3_WEEK_CYCLE_LIST_3_TRANCHES.subList(0, excessContracts.intValue());
		}

		if (!CollectionUtils.isEmpty(dayOffsetList)) {
			Date cycleStartDate = getStartOfCurrentCycleDate(balanceDate, cycleDurationWeeks);
			// look for a an entry that generates an excess contract distribution date that matches the current date.  If the dates match, 1 excess contract will be returned
			// for addition to the trade quantity.
			for (Integer dayOffset : dayOffsetList) {
				if (DateUtils.isEqualWithoutTime(DateUtils.addDays(cycleStartDate, dayOffset), balanceDate)) {
					return BigDecimal.ONE;
				}
			}
		}

		return BigDecimal.ZERO;
	}


	@Override
	public Short getTrancheCount(InvestmentAccountSecurityTarget securityTarget) {
		AssertUtils.assertNotNull(securityTarget, "InvestmentAccountSecurityTarget cannot be null.");

		Short trancheCount = securityTarget.getTrancheCount();
		if (trancheCount != null) {
			return trancheCount;
		}

		BusinessServiceProcessingType processingType = securityTarget.getClientInvestmentAccount().getServiceProcessingType();
		if (processingType == null) {
			return null;
		}

		Integer integerObj = (Integer) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.TRANCHE_COUNT);
		return integerObj.shortValue();
	}


	@Override
	public Short getCycleDurationWeeks(InvestmentAccountSecurityTarget securityTarget) {
		AssertUtils.assertNotNull(securityTarget, "InvestmentAccountSecurityTarget cannot be null.");

		Short cycleDurationWeeks = securityTarget.getCycleDurationWeeks();
		if (cycleDurationWeeks != null) {
			return cycleDurationWeeks;
		}

		BusinessServiceProcessingType processingType = securityTarget.getClientInvestmentAccount().getServiceProcessingType();
		if (processingType == null) {
			return null;
		}

		Integer integerObj = (Integer) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.CYCLE_DURATION_WEEKS);
		return integerObj == null ? null : integerObj.shortValue();
	}


	@Override
	public Date getEstimatedCycleEndDate(Date startDate, InvestmentAccountSecurityTarget securityTarget) {
		Short cycleDurationWeeks = getCycleDurationWeeks(securityTarget);
		if (cycleDurationWeeks == null) {
			return null;
		}

		return DateUtils.addWeekDays(startDate, cycleDurationWeeks * 5);
	}


	@Override
	public Short getAllowedOptionExpirationCycleDeviationDays(BusinessServiceProcessingType processingType) {
		Integer integerObj = (Integer) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.ALLOWED_OPTION_EXPIRATION_CYCLE_DEVIATION_DAYS);
		return integerObj == null ? null : integerObj.shortValue();
	}


	@Override
	public TradeOptionLegTypes getUnexpiredContractFilterType(BusinessServiceProcessingType processingType) {
		String stringObj = (String) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.TRADE_CYCLE_UNEXPIRED_CONTRACT_FILTER_TYPE);
		return TradeOptionLegTypes.valueOf(stringObj);
	}


	@Override
	public Short getTranchePositionCount(BusinessServiceProcessingType processingType) {
		Integer integerObj = (Integer) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.TRANCHE_POSITION_COUNT);
		return integerObj.shortValue();
	}


	@Override
	public boolean isUsesValueAtRisk(BusinessServiceProcessingType processingType) {
		Boolean booleanObj = (Boolean) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.USES_VALUE_AT_RISK);
		return booleanObj != null && booleanObj;
	}


	@Override
	public TradeOptionCombinationTypes getOptionCombinationType(BusinessServiceProcessingType processingType) {
		String optionCombinationTypeValue = (String) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.OPTION_COMBINATION_TYPE);
		return TradeOptionCombinationTypes.valueOf(optionCombinationTypeValue);
	}


	@Override
	public BigDecimal getDeltaMidRange(BusinessServiceProcessingType processingType, String optionDeltaName) {
		return (BigDecimal) getCustomPropertyValueFromProcessingType(processingType, optionDeltaName);
	}


	@Override
	public BigDecimal getAllowedDeltaDeviation(BusinessServiceProcessingType processingType) {
		BigDecimal allowedDeltaDeviation = (BigDecimal) getCustomPropertyValueFromProcessingType(processingType, BusinessServiceProcessingType.ALLOWED_DELTA_DEVIATION);
		return allowedDeltaDeviation != null ? allowedDeltaDeviation : BigDecimal.ZERO;
	}


	@Override
	public BigDecimal getMaxAllowedOptionSpreadFromUnderlyingPricePercent(InvestmentAccount clientAccount) {
		Object maxSpreadPercent = getInvestmentAccountUtilHandler().getClientInvestmentAccountPortfolioSetupCustomColumnValue(clientAccount, CLIENT_ACCOUNT_MAX_OPTION_SPREAD_COLUMN_NAME);

		if (maxSpreadPercent == null) {
			// if not defined on the client account, look to the processing type
			maxSpreadPercent = getCustomPropertyValueFromProcessingType(clientAccount.getServiceProcessingType(), BusinessServiceProcessingType.MAX_OPTION_SPREAD_PERCENT);
		}

		return (BigDecimal) maxSpreadPercent;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A filter used to determine if the security associated with an AccountingPosition entity is of the specified option type.
	 */
	private boolean positionFilterOnOptionType(AccountingPositionBalance accountingPositionBalance, InvestmentSecurityOptionTypes optionType) {
		return optionType == accountingPositionBalance.getInvestmentSecurity().getOptionType();
	}


	/**
	 * A function that returns the week cycle in the trading period.  This value is 0 based, so
	 * week cycle 0 represents the first week of trading.
	 *
	 * @param balanceDate   - Date for which the week cycle should be calculated
	 * @param cycleDuration - duration (in weeks) of the service's trading cycle
	 * @return weekCycle - an integer representing the week within the trading period.
	 */
	private int getWeekCycle(Date balanceDate, int cycleDuration) {
		final Date weekCycleBaseDate = DateUtils.toDate("1/1/1900");

		int weeksDifference = DateUtils.getWeeksDifference(balanceDate, weekCycleBaseDate);
		return weeksDifference % cycleDuration;
	}


	/**
	 * Returns the value for the BusinessServiceProcessingType custom column with the specified column name.
	 *
	 * @param processingType BusinessServiceProcessingType instance.
	 * @param columnName     column name (String)
	 * @return value for the custom column, or null if no value is found.
	 */
	private Object getCustomPropertyValueFromProcessingType(BusinessServiceProcessingType processingType, String columnName) {
		AssertUtils.assertNotNull(processingType, "A valid BusinessServiceProcessingType is required (cannot be null).");
		AssertUtils.assertNotEmpty(columnName, "A valid column name for the BusinessServiceProcessingType custom column must be specified (cannot be null or blank).");
		return getSystemColumnValueService().getSystemColumnCustomValue(processingType.getId(), BusinessServiceProcessingType.BUSINESS_SERVICE_PROCESSING_TYPE_COLUMN_GROUP_NAME, columnName);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountUtilHandler getInvestmentAccountUtilHandler() {
		return this.investmentAccountUtilHandler;
	}


	public void setInvestmentAccountUtilHandler(InvestmentAccountUtilHandler investmentAccountUtilHandler) {
		this.investmentAccountUtilHandler = investmentAccountUtilHandler;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}
}
