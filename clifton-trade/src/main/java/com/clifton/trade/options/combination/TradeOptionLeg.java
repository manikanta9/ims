package com.clifton.trade.options.combination;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;


/**
 * The {@link TradeOptionLeg} class represents a single leg for an {@link TradeOptionCombination option combination}. This object encapsulates relevant information about the leg
 * such as the {@link TradeOptionLegTypes leg type}, security, expiration date, and strike price.
 *
 * @author MikeH
 */
public class TradeOptionLeg {

	private final TradeOptionLegTypes legType;
	private final InvestmentSecurity investmentSecurity;
	private final Date expirationDate;
	private final BigDecimal strikePrice;
	private final Collection<AccountingPosition> positionList;

	/**
	 * The remaining quantity for all positions that compose this leg.
	 */
	private final BigDecimal remainingQuantity;
	/**
	 * The remaining quantity of units of the underlying security represented by this leg. For options, this is typically the number of remaining contracts multiplied by 100.
	 */
	private final BigDecimal underlyingRemainingQuantity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOptionLeg(InvestmentSecurity investmentSecurity, Collection<AccountingPosition> positionList) {
		BigDecimal calculatedRemainingQuantity = CoreMathUtils.sumProperty(positionList, AccountingPosition::getRemainingQuantity);
		TradeOptionLegTypes inferredLegType = TradeOptionLegTypes.fromSource(MathUtils.isGreaterThanOrEqual(calculatedRemainingQuantity, BigDecimal.ZERO), investmentSecurity);
		BigDecimal calculatedUnderlyingRemainingQuantity = MathUtils.multiply(calculatedRemainingQuantity, investmentSecurity.getPriceMultiplier());

		this.legType = inferredLegType;
		this.investmentSecurity = investmentSecurity;
		this.expirationDate = investmentSecurity.getEndDate();
		this.strikePrice = investmentSecurity.getOptionStrikePrice();
		this.positionList = positionList;
		this.remainingQuantity = calculatedRemainingQuantity;
		this.underlyingRemainingQuantity = inferredLegType.isShort() ? MathUtils.negate(calculatedUnderlyingRemainingQuantity) : calculatedUnderlyingRemainingQuantity;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "TradeOptionLeg{" +
				"legType=" + this.legType +
				", investmentSecurity=" + this.investmentSecurity +
				", expirationDate=" + this.expirationDate +
				", strikePrice=" + this.strikePrice +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOptionLegTypes getLegType() {
		return this.legType;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public Date getExpirationDate() {
		return this.expirationDate;
	}


	public BigDecimal getStrikePrice() {
		return this.strikePrice;
	}


	public Collection<AccountingPosition> getPositionList() {
		return this.positionList;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public BigDecimal getUnderlyingRemainingQuantity() {
		return this.underlyingRemainingQuantity;
	}
}
