package com.clifton.trade.options.securitytarget;

import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;


/**
 * <code>TradeSecurityTargetUtilizationDetail</code> contains covered, pending, and frozen quantities for a unique
 * {@link InvestmentSecurity} applicable to a {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget}.
 *
 * @author NickK
 */
public class TradeSecurityTargetUtilizationDetail {

	private final InvestmentSecurity investmentSecurity;

	/**
	 * Following values are defined in quantity of the {@link #investmentSecurity}.
	 * If investmentSecurity is null, the quantity is a frozen amount in the quantity of the underlying security defined
	 * on the {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget} this detail applies to.
	 */
	private BigDecimal coveredQuantity;
	private BigDecimal frozenQuantity;
	private BigDecimal pendingQuantity;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private TradeSecurityTargetUtilizationDetail(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public static TradeSecurityTargetUtilizationDetail of(InvestmentSecurity investmentSecurity) {
		return new TradeSecurityTargetUtilizationDetail(investmentSecurity);
	}


	public static TradeSecurityTargetUtilizationDetail ofFrozenUnderlyingQuantity(BigDecimal frozenUnderlyingQuantity) {
		return new TradeSecurityTargetUtilizationDetail(null)
				.withFrozenQuantity(frozenUnderlyingQuantity);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public BigDecimal getCoveredQuantity() {
		return this.coveredQuantity;
	}


	public TradeSecurityTargetUtilizationDetail withCoveredQuantity(BigDecimal coveredQuantity) {
		setCoveredQuantity(coveredQuantity);
		return this;
	}


	public void setCoveredQuantity(BigDecimal coveredQuantity) {
		this.coveredQuantity = coveredQuantity;
	}


	public BigDecimal getFrozenQuantity() {
		return this.frozenQuantity;
	}


	public TradeSecurityTargetUtilizationDetail withFrozenQuantity(BigDecimal frozenQuantity) {
		setFrozenQuantity(frozenQuantity);
		return this;
	}


	public void setFrozenQuantity(BigDecimal frozenQuantity) {
		this.frozenQuantity = frozenQuantity;
	}


	public BigDecimal getPendingQuantity() {
		return this.pendingQuantity;
	}


	public TradeSecurityTargetUtilizationDetail withPendingQuantity(BigDecimal pendingQuantity) {
		setPendingQuantity(pendingQuantity);
		return this;
	}


	public void setPendingQuantity(BigDecimal pendingQuantity) {
		this.pendingQuantity = pendingQuantity;
	}
}
