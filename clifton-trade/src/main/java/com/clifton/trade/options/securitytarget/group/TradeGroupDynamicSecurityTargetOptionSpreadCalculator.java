package com.clifton.trade.options.securitytarget.group;

import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionBalance;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.AccountingPositionUtils;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationSearchForm;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.dynamic.calculators.BaseTradeGroupDynamicCalculator;
import com.clifton.trade.options.combination.TradeOptionCombination;
import com.clifton.trade.options.combination.TradeOptionCombinationHandler;
import com.clifton.trade.options.combination.TradeOptionCombinationTypes;
import com.clifton.trade.options.combination.TradeOptionLegTypes;
import com.clifton.trade.options.securitytarget.group.util.TradeGroupTradeCycleUtilHandler;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTarget;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTargetUtilHandler;
import com.clifton.trade.restriction.TradeRestrictionBroker;
import com.clifton.trade.restriction.TradeRestrictionBrokerService;
import com.clifton.trade.restriction.search.TradeRestrictionBrokerSearchForm;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.clifton.trade.group.TradeGroup.TradeGroupActions.SAVE_TRADE;
import static com.clifton.trade.group.TradeGroup.TradeGroupActions.UPDATE_FILLS;


/**
 * <code>TradeGroupDynamicSecurityTargetOptionSpreadCalculator</code> is a {@link com.clifton.trade.group.dynamic.calculators.TradeGroupDynamicCalculator} for OARS and RPS
 * used to calculate the tranche trade quantities. This calculator uses applicable {@link InvestmentAccountSecurityTarget}s to calculate new trade quantities for
 * Option Put and Call spreads. The Put and Call spread options are provided and used to validate and calculate new tranche trade quantities.
 *
 * @author NickK
 */
public class TradeGroupDynamicSecurityTargetOptionSpreadCalculator extends BaseTradeGroupDynamicCalculator<TradeGroupDynamicSecurityTargetOptionSpread, TradeGroupDynamicSecurityTargetOptionSpreadDetail> {

	private static final String CLIENT_ACCOUNT_VALUE_AT_RISK_PERCENT_COLUMN_NAME = "Value At Risk Percent";

	private static final BigDecimal FIVE_PERCENT = new BigDecimal("0.05");

	private final Set<TradeOptionCombinationTypes> supportedOptionCombinationTypeSet = CollectionUtils.createHashSet(TradeOptionCombinationTypes.BULL_PUT_SPREAD, TradeOptionCombinationTypes.IRON_CONDOR,
			TradeOptionCombinationTypes.SHORT_CALL, TradeOptionCombinationTypes.SHORT_PUT, TradeOptionCombinationTypes.COLLAR);

	private AccountingPositionService accountingPositionService;

	private InvestmentAccountUtilHandler investmentAccountUtilHandler;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private MarketDataRetriever marketDataRetriever;

	private SystemColumnValueService systemColumnValueService;
	private TradeRestrictionBrokerService tradeRestrictionBrokerService;
	private TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler;
	private TradeService tradeService;
	private TradeDestinationService tradeDestinationService;

	private TradeGroupTradeCycleUtilHandler tradeGroupTradeCycleUtilHandler;
	private TradeOptionCombinationHandler tradeOptionCombinationHandler;

	/**
	 * First access - looks up TradeType by Name = "Options" and
	 * sets it.  Could make this configurable, however the calculator itself is Options specific
	 */
	private TradeType tradeType;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public TradeGroupDynamicSecurityTargetOptionSpread getTradeGroupDynamic(TradeGroup tradeGroup) {
		TradeGroupDynamicSecurityTargetOptionSpread optionSpread = new TradeGroupDynamicSecurityTargetOptionSpread();
		BeanUtils.copyProperties(tradeGroup, optionSpread);
		populateDynamicPropertyValuesFromTradeGroupNote(optionSpread);
		populateOptionSpreadTradeGroupDefaults(optionSpread);
		populateTradeGroupDynamicEntry(optionSpread);
		return optionSpread;
	}


	@Override
	public TradeGroupDynamicSecurityTargetOptionSpread populateTradeGroupDynamicEntry(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
		Date balanceDate = optionSpread.getBalanceDate();
		if (balanceDate == null) {
			// if balance date is null, use current day
			optionSpread.setBalanceDate(DateUtils.clearTime(new Date()));
		}

		TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext = createOptionSpreadContext(optionSpread);

		if (optionSpread.isNewBean()) {
			populateOptionSpreadDetailListForEntry(optionSpreadContext);
		}
		else {
			populateOptionSpreadDetailListFromTradeList(optionSpreadContext);
		}

		return optionSpread;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateTradeGroupForSavingNewTradeGroupDynamic(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
		// Default Settlement Date If Blank
		if (optionSpread.getSettlementDate() == null) {
			// Neither of these should ever happen because the screen requires these values
			ValidationUtils.assertNotNull(optionSpread.getTradeDate(), "Trade Date is required.", "tradeDate");
			InvestmentSecurity option = optionSpread.getLongPutSecurity();
			ValidationUtils.assertNotNull(option, "Unable to determine Option for setting Settlement date");
			// The settlement date would be the same for the call and the put because they both are associated with the same instrument.
			optionSpread.setSettlementDate(getInvestmentCalculator().calculateSettlementDate(option, null, optionSpread.getTradeDate()));
		}
		populateTradeGroupDynamicTradeListFromDetailList(optionSpread);
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnEntrySave(TradeGroupDynamicSecurityTargetOptionSpread optionSpread, TradeGroupDynamicSecurityTargetOptionSpreadDetail spreadDetail) {
		BiConsumer<Trade, InvestmentSecurity> tradeGroupTradePopulator = (trade, option) -> {
			if (trade != null) {
				BigDecimal quantity = trade.getQuantityIntended() != null ? trade.getQuantityIntended() : spreadDetail.getTradeQuantity();
				populateOptionSpreadDetailTrade(optionSpread, spreadDetail, trade, option, quantity);
			}
			else if (option != null) {
				trade = createOptionSpreadDetailTrade(optionSpread, spreadDetail, option, spreadDetail.getTradeQuantity());
			}
			if (trade != null) {
				optionSpread.addTrade(trade);
			}
		};

		tradeGroupTradePopulator.accept(spreadDetail.getLongPutTrade(), optionSpread.getLongPutSecurity());
		tradeGroupTradePopulator.accept(spreadDetail.getShortPutTrade(), optionSpread.getShortPutSecurity());
		tradeGroupTradePopulator.accept(spreadDetail.getShortCallTrade(), optionSpread.getShortCallSecurity());
		tradeGroupTradePopulator.accept(spreadDetail.getLongCallTrade(), optionSpread.getLongCallSecurity());
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(TradeGroupDynamicSecurityTargetOptionSpread optionSpread, TradeGroupDynamicSecurityTargetOptionSpreadDetail spreadDetail) {
		Consumer<Trade> tradeConsumer = trade -> {
			if (trade != null && !trade.isCancelled() && !optionSpread.getTradeList().contains(trade)) {
				optionSpread.addTrade(trade);
			}
		};

		BusinessServiceProcessingType processingType = spreadDetail.getClientAccount().getServiceProcessingType();
		TradeOptionCombinationTypes combinationType = getTradeGroupTradeCycleUtilHandler().getOptionCombinationType(processingType);

		boolean hasLongPutCommission = optionSpread.getLongPutCommissionPerUnit() != null;
		boolean hasShortPutCommission = optionSpread.getShortPutCommissionPerUnit() != null;
		boolean hasShortCallCommission = optionSpread.getShortCallCommissionPerUnit() != null;
		boolean hasLongCallCommission = optionSpread.getLongCallCommissionPerUnit() != null;

		boolean hasLongPutFillPrice = optionSpread.getLongPutFillPrice() != null;
		boolean hasShortPutFillPrice = optionSpread.getShortPutFillPrice() != null;
		boolean hasShortCallFillPrice = optionSpread.getShortCallFillPrice() != null;
		boolean hasLongCallFillPrice = optionSpread.getLongCallFillPrice() != null;

		boolean hasTradeDestination = optionSpread.getTradeDestination() != null;
		boolean hasLongPutSecurity = optionSpread.getLongPutSecurity() != null;
		boolean hasShortPutSecurity = optionSpread.getShortPutSecurity() != null;
		boolean hasShortCallSecurity = optionSpread.getShortCallSecurity() != null;
		boolean hasLongCallSecurity = optionSpread.getLongCallSecurity() != null;

		for (TradeOptionLegTypes optionLegType : CollectionUtils.getIterable(CollectionUtils.createList(combinationType.getOptionLegTypes()))) {
			final TradeGroup.TradeGroupActions tradeGroupAction = optionSpread.getTradeGroupAction();

			switch (optionLegType) {
				case LONG_PUT:
					if (tradeGroupAction == SAVE_TRADE) {
						if (hasLongPutCommission) {
							spreadDetail.getLongPutTrade().setCommissionPerUnit(optionSpread.getLongPutCommissionPerUnit());
							spreadDetail.getLongPutTrade().setEnableCommissionOverride(true);
						}
						if (hasLongPutSecurity) {
							spreadDetail.getLongPutTrade().setInvestmentSecurity(optionSpread.getLongPutSecurity());
						}
						if (hasTradeDestination) {
							spreadDetail.getLongPutTrade().setTradeDestination(optionSpread.getTradeDestination());
						}
					}
					else if (hasLongPutFillPrice && tradeGroupAction == UPDATE_FILLS) {
						spreadDetail.getLongPutTrade().setFillPrice(optionSpread.getLongPutFillPrice());
						spreadDetail.getLongPutTrade().setFillQuantity(spreadDetail.getLongPutTrade().getQuantityIntended());
					}

					tradeConsumer.accept(spreadDetail.getLongPutTrade());
					break;
				case SHORT_PUT:
					if (tradeGroupAction == SAVE_TRADE) {
						if (hasShortPutCommission) {
							spreadDetail.getShortPutTrade().setCommissionPerUnit(optionSpread.getShortPutCommissionPerUnit());
							spreadDetail.getShortPutTrade().setEnableCommissionOverride(true);
						}
						if (hasShortPutSecurity) {
							spreadDetail.getShortPutTrade().setInvestmentSecurity(optionSpread.getShortPutSecurity());
						}
						if (hasTradeDestination) {
							spreadDetail.getShortPutTrade().setTradeDestination(optionSpread.getTradeDestination());
						}
					}
					else if (hasShortPutFillPrice && tradeGroupAction == UPDATE_FILLS) {
						spreadDetail.getShortPutTrade().setFillPrice(optionSpread.getShortPutFillPrice());
						spreadDetail.getShortPutTrade().setFillQuantity(spreadDetail.getShortPutTrade().getQuantityIntended());
					}

					tradeConsumer.accept(spreadDetail.getShortPutTrade());
					break;
				case SHORT_CALL:
					if (tradeGroupAction == SAVE_TRADE) {
						if (hasShortCallCommission) {
							spreadDetail.getShortCallTrade().setCommissionPerUnit(optionSpread.getShortCallCommissionPerUnit());
							spreadDetail.getShortCallTrade().setEnableCommissionOverride(true);
						}
						if (hasShortCallSecurity) {
							spreadDetail.getShortCallTrade().setInvestmentSecurity(optionSpread.getShortCallSecurity());
						}
						if (hasTradeDestination) {
							spreadDetail.getShortCallTrade().setTradeDestination(optionSpread.getTradeDestination());
						}
					}
					else if (hasShortCallFillPrice && tradeGroupAction == UPDATE_FILLS) {
						spreadDetail.getShortCallTrade().setFillPrice(optionSpread.getShortCallFillPrice());
						spreadDetail.getShortCallTrade().setFillQuantity(spreadDetail.getShortCallTrade().getQuantityIntended());
					}

					tradeConsumer.accept(spreadDetail.getShortCallTrade());
					break;
				case LONG_CALL:
					if (tradeGroupAction == SAVE_TRADE) {
						if (hasLongCallCommission) {
							spreadDetail.getLongCallTrade().setCommissionPerUnit(optionSpread.getLongCallCommissionPerUnit());
							spreadDetail.getLongCallTrade().setEnableCommissionOverride(true);
						}
						if (hasLongCallSecurity) {
							spreadDetail.getLongCallTrade().setInvestmentSecurity(optionSpread.getLongCallSecurity());
						}
						if (hasTradeDestination) {
							spreadDetail.getLongCallTrade().setTradeDestination(optionSpread.getTradeDestination());
						}
					}
					else if (hasLongCallFillPrice && tradeGroupAction == UPDATE_FILLS) {
						spreadDetail.getLongCallTrade().setFillPrice(optionSpread.getLongCallFillPrice());
						spreadDetail.getLongCallTrade().setFillQuantity(spreadDetail.getLongCallTrade().getQuantityIntended());
					}
					tradeConsumer.accept(spreadDetail.getLongCallTrade());
					break;
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void populateOptionSpreadTradeGroupDefaults(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
		if (optionSpread.getClientAccountGroup() != null && optionSpread.getClientAccountGroup().getId() != null) {
			optionSpread.setClientAccountGroup(getInvestmentAccountGroupService().getInvestmentAccountGroup(optionSpread.getClientAccountGroup().getId()));
		}
		if (optionSpread.getOptionUnderlyingSecurity() != null && optionSpread.getOptionUnderlyingSecurity().getId() != null) {
			optionSpread.setOptionUnderlyingSecurity(getInvestmentInstrumentService().getInvestmentSecurity(optionSpread.getOptionUnderlyingSecurity().getId()));
		}
	}


	private TradeGroupDynamicSecurityTargetOptionSpreadContext createOptionSpreadContext(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
		ValidationUtils.assertNotNull(optionSpread.getOptionUnderlyingSecurity(), "Option Underlying Security is required.");
		BigDecimal underlyingSecurityPrice;
		if (optionSpread.isLivePrices()) {
			MarketDataLive underlyingSecurityLivePrice = getMarketDataRetriever().getLivePrice(optionSpread.getOptionUnderlyingSecurity());
			ValidationUtils.assertNotNull(underlyingSecurityLivePrice, String.format("Unable to obtain live price for Underlying Security %s", optionSpread.getOptionUnderlyingSecurity()));
			underlyingSecurityPrice = underlyingSecurityLivePrice.asNumericValue();
		}
		else {
			underlyingSecurityPrice = getMarketDataRetriever().getPriceFlexible(optionSpread.getOptionUnderlyingSecurity(), optionSpread.getBalanceDate(), true);
		}

		// get security targets
		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetList(optionSpread);
		ValidationUtils.assertNotEmpty(securityTargetList, String.format("No Client Account Security Targets are active on %tD for Underlying Security %s", optionSpread.getBalanceDate(), optionSpread.getOptionUnderlyingSecurity()));
		Map<InvestmentAccount, List<InvestmentAccountSecurityTarget>> clientSecurityTargetListMap = BeanUtils.getBeansMap(securityTargetList, InvestmentAccountSecurityTarget::getClientInvestmentAccount);

		// Get positions for applicable client accounts; pending included
		Integer[] clientInvestmentAccountIds = BeanUtils.getBeanIdentityArray(clientSecurityTargetListMap.keySet(), Integer.class);
		List<AccountingPosition> accountingPositionList = getAccountingPositionListForClientAccountIds(optionSpread, clientInvestmentAccountIds);
		Map<InvestmentAccount, List<AccountingPosition>> clientAccountingPositionListMap = BeanUtils.getBeansMap(accountingPositionList, AccountingPosition::getClientInvestmentAccount);
		Map<InvestmentAccount, List<AccountingPositionBalance>> clientAccountingPositionBalanceListMap = getAccountingPositionBalanceListMap(accountingPositionList);

		return new TradeGroupDynamicSecurityTargetOptionSpreadContext(optionSpread)
				.withUnderlyingSecurityPrice(underlyingSecurityPrice)
				.withClientSecurityTargetListMap(clientSecurityTargetListMap)
				.withClientAccountingPositionListMap(clientAccountingPositionListMap)
				.withClientAccountingPositionBalanceListMap(clientAccountingPositionBalanceListMap);
	}


	private List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
		InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
		Optional.ofNullable(optionSpread.getClientAccountGroup()).ifPresent(accountGroup -> securityTargetSearchForm.setInvestmentAccountGroupId(accountGroup.getId()));
		securityTargetSearchForm.setTargetUnderlyingSecurityId(optionSpread.getOptionUnderlyingSecurity().getId());
		securityTargetSearchForm.setActiveOnDate(optionSpread.getBalanceDate());
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(securityTargetSearchForm);
	}


	private List<AccountingPosition> getAccountingPositionListForClientAccountIds(TradeGroupDynamicSecurityTargetOptionSpread optionSpread, Integer... clientAccountIds) {
		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(optionSpread.getBalanceDate());
		positionCommand.setUnderlyingSecurityId(optionSpread.getOptionUnderlyingSecurity().getId());

		if (clientAccountIds != null && clientAccountIds.length == 1) {
			positionCommand.setClientInvestmentAccountId(clientAccountIds[0]);
		}
		else {
			positionCommand.setClientInvestmentAccountIds(clientAccountIds);
		}
		positionCommand.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_MERGE);
		return getAccountingPositionService().getAccountingPositionListUsingCommand(positionCommand);
	}


	private Map<InvestmentAccount, List<AccountingPositionBalance>> getAccountingPositionBalanceListMap(List<AccountingPosition> accountingPositionList) {
		List<AccountingPositionBalance> positionBalanceList = AccountingPositionUtils.mergeAccountingPositionList(accountingPositionList);
		return BeanUtils.getBeansMap(positionBalanceList, AccountingPositionBalance::getClientInvestmentAccount);
	}


	private void populateOptionSpreadDetailListForEntry(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext) {
		TradeGroupDynamicSecurityTargetOptionSpread optionSpread = optionSpreadContext.getOptionSpread();
		ValidationUtils.assertTrue(optionSpread.getClientAccountGroup() != null && optionSpread.getClientAccountGroup().getId() != null, "Client Account Group is required.");
		ValidationUtils.assertNotNull(optionSpread.getBalanceDate(), "Balance Date is required.");
		validateOptionSpreadSecurities(optionSpreadContext);

		if (optionSpread.getDetailList() == null) {
			optionSpread.setDetailList(new ArrayList<>());
		}

		optionSpreadContext.getSecurityTargetListMap().forEach((clientAccount, clientSecurityTargetList) -> {
					InvestmentAccountSecurityTarget clientSecurityTarget = CollectionUtils.getOnlyElement(clientSecurityTargetList);
					if (clientSecurityTarget != null) {
						TradeGroupDynamicSecurityTargetOptionSpreadDetail detail = createOptionSpreadDetail(optionSpreadContext, clientSecurityTarget, true);

						int expectedAccountingBalanceCount = getClientAccountExpectedTrancheAccountingPositionCount(detail);
						if (expectedAccountingBalanceCount > 0) {

							if (optionSpread.isValidateExpiringPositions()) {
								List<AccountingPositionBalance> expiringOptionAccountingBalanceList = getExpiringOptionAccountingPositionList(optionSpreadContext, clientAccount);
								if (CollectionUtils.getSize(expiringOptionAccountingBalanceList) != expectedAccountingBalanceCount) {
									addOptionSpreadDetailNote(detail, String.format("Expected %d expiring positions, but found %d", expectedAccountingBalanceCount, expiringOptionAccountingBalanceList.size()));
								}
							}

							TradeOptionCombinationTypes optionCombinationType = getTradeGroupTradeCycleUtilHandler().getOptionCombinationType(detail.getClientAccount().getServiceProcessingType());
							ValidationUtils.assertTrue(getSupportedOptionCombinationTypeSet().contains(optionCombinationType), "The option structure type: " + optionCombinationType + " is not supported by this calculator.");
							ValidationUtils.assertTrue(isOptionSpreadStructureValid(optionSpread, optionCombinationType), "The selected options to not conform to the options required by the structural type: " + optionCombinationType + ".");

							Short trancheCount = getTradeGroupTradeCycleUtilHandler().getTrancheCount(clientSecurityTarget);
							int securityCount = (int) Stream.of(optionSpread.getLongPutSecurity(), optionSpread.getShortPutSecurity(), optionSpread.getShortCallSecurity(), optionSpread.getLongCallSecurity())
									.filter(Objects::nonNull)
									.count();
							if (securityCount != expectedAccountingBalanceCount) {
								addOptionSpreadDetailNote(detail, String.format("Expected %d Options to Trade, but found %d", expectedAccountingBalanceCount, securityCount));
							}

							if (trancheCount != null) {
								if (getTradeGroupTradeCycleUtilHandler().getCycleDurationWeeks(clientSecurityTarget) == null) {
									addOptionSpreadDetailNote(detail, String.format("Client Security Target %s is missing Cycle Duration in order to correctly calculation trade quantities.", clientSecurityTarget));
								}
								else {
									// We derive an intermediate value representing the trancheNotional / optionSecurityPriceMultiplier, which reduces the notional for calculations in terms of option contracts.
									BigDecimal optionSecurityPriceMultiplier = getOptionSpreadOptionPriceMultiplier(optionSpread);
									ValidationUtils.assertNotNull(optionSecurityPriceMultiplier, "Cannot obtain the price multiplier from any of the selected options.");

									BigDecimal trancheNotional = detail.getAverageTrancheNotional();
									BigDecimal reducedTrancheNotional = MathUtils.divide(trancheNotional, optionSecurityPriceMultiplier);

									// The quantity (in contracts) to trade is reducedTrancheNotional / underlyingSecurityPrice
									BigDecimal tradeQuantity = MathUtils.divide(reducedTrancheNotional, optionSpreadContext.getUnderlyingSecurityPrice());

									// Round down the trade quantity to an integer quantity

									tradeQuantity = MathUtils.round(tradeQuantity, 0, RoundingMode.FLOOR);

									// Use previously-calculated reducedTrancheNotional to derive reduced notional for full trade cycle (intermediate value use for calculating contract count for full cycle)
									BigDecimal reducedNotionalForCycle = MathUtils.multiply(reducedTrancheNotional, trancheCount);

									// Calculate total quantity for trade cycle:  reducedNotionalForCycle / underlyingSecurityPrice
									BigDecimal totalQuantityForCycle = MathUtils.divide(reducedNotionalForCycle, optionSpreadContext.getUnderlyingSecurityPrice());

									// Round down the trade quantity to an integer quantity
									totalQuantityForCycle = MathUtils.round(totalQuantityForCycle, 0, RoundingMode.FLOOR);

									// Scale up the quantity per cycle (which was rounded down in floor mode) by a factor of trancheCount
									// this will provide us with a scaled quantity that, when subtracted from the totalQuantityForCycle will be the excess contract count
									BigDecimal scaledTrancheTradeQuantity = MathUtils.multiply(tradeQuantity, trancheCount);

									BigDecimal excessContractsForCycle =
											MathUtils.subtract(
													totalQuantityForCycle,
													scaledTrancheTradeQuantity);

									BigDecimal excessContractForCurrentTrade = getTradeGroupTradeCycleUtilHandler().calculateExcessContractDistributionForCurrentTrade(clientSecurityTarget, optionSpreadContext.getBalanceDate(), excessContractsForCycle);
									tradeQuantity = MathUtils.add(tradeQuantity, excessContractForCurrentTrade);

									// Safety check to ensure the trade quantity does not exceed the net available contracts
									BigDecimal unexpiredContractCount = getTradeGroupTradeCycleUtilHandler().getUnexpiredContractCount(clientSecurityTarget, optionSpreadContext.getAccountingPositionBalanceListForClientAccount(clientSecurityTarget.getClientInvestmentAccount()), optionSpreadContext.getBalanceDate());
									BigDecimal netAvailableQuantity = MathUtils.subtract(totalQuantityForCycle, unexpiredContractCount);
									tradeQuantity = MathUtils.max(BigDecimal.ZERO, MathUtils.min(tradeQuantity, netAvailableQuantity));

									populateOptionSpreadDetailExposure(optionSpreadContext, detail, tradeQuantity);

									// Secondary check that may adjust down the trade quantity if the resulting trade would cause the aggregate VAR to exceed the configured limit.
									adjustQuantityForVar(optionSpreadContext, detail, optionCombinationType);
								}
							}
						}

						optionSpread.getDetailList().add(detail);
					}
				}
		);
	}


	/**
	 * Checks the four option components of a spread to ensure the spread or option grouping has the proper layout.
	 */
	private boolean isOptionSpreadStructureValid(InvestmentSecurity longPut, InvestmentSecurity shortPut, InvestmentSecurity shortCall, InvestmentSecurity longCall, TradeOptionCombinationTypes optionCombinationType) {
		if (null == optionCombinationType) {
			return false;
		}

		// check for required option legs that are missing
		Set<TradeOptionLegTypes> optionLegTypesSet = CollectionUtils.createHashSet(optionCombinationType.getOptionLegTypes());

		if (optionLegTypesSet.contains(TradeOptionLegTypes.LONG_PUT) && longPut == null) {
			return false;
		}

		if (optionLegTypesSet.contains(TradeOptionLegTypes.SHORT_PUT) && shortPut == null) {
			return false;
		}

		if (optionLegTypesSet.contains(TradeOptionLegTypes.SHORT_CALL) && shortCall == null) {
			return false;
		}

		if (optionLegTypesSet.contains(TradeOptionLegTypes.LONG_CALL) && longCall == null) {
			return false;
		}

		// check for non-required option legs that are configured
		if (!optionLegTypesSet.contains(TradeOptionLegTypes.LONG_PUT) && longPut != null) {
			return false;
		}

		if (!optionLegTypesSet.contains(TradeOptionLegTypes.SHORT_PUT) && shortPut != null) {
			return false;
		}

		if (!optionLegTypesSet.contains(TradeOptionLegTypes.SHORT_CALL) && shortCall != null) {
			return false;
		}

		if (!optionLegTypesSet.contains(TradeOptionLegTypes.LONG_CALL) && longCall != null) {
			return false;
		}

		return true;
	}


	/**
	 * Checks a TradeGroupDynamicSecurityTargetOptionSpread entity against an OptionStructureType to ensure the spread or grouping has the proper layout.
	 */
	private boolean isOptionSpreadStructureValid(TradeGroupDynamicSecurityTargetOptionSpread optionSpread, TradeOptionCombinationTypes optionCombinationType) {
		return isOptionSpreadStructureValid(optionSpread.getLongPutSecurity(), optionSpread.getShortPutSecurity(), optionSpread.getShortCallSecurity(), optionSpread.getLongCallSecurity(), optionCombinationType);
	}


	private void populateOptionSpreadDetailListFromTradeList(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext) {
		TradeGroupDynamicSecurityTargetOptionSpread optionSpread = optionSpreadContext.getOptionSpread();
		List<Trade> tradeList = optionSpread.getTradeList();

		if (!CollectionUtils.isEmpty(tradeList)) {
			clearOptionSpreadDetailList(optionSpreadContext.getOptionSpread());

			if (optionSpread.getDetailList() == null) {
				optionSpread.setDetailList(new ArrayList<>());
			}

			Map<InvestmentAccount, List<Trade>> clientTradeMap = tradeList.stream()
					.filter(trade -> !trade.isCancelled())
					.collect(Collectors.groupingBy(Trade::getClientInvestmentAccount));
			// For each client trade list
			clientTradeMap.forEach((clientAccount, clientTradeList) -> {
				InvestmentAccountSecurityTarget clientSecurityTarget = CollectionUtils.getOnlyElement(optionSpreadContext.getSecurityTargetListForClientAccount(clientAccount));
				TradeGroupDynamicSecurityTargetOptionSpreadDetail detail;

				if (clientSecurityTarget == null) {
					detail = new TradeGroupDynamicSecurityTargetOptionSpreadDetail();
					detail.setClientAccount(clientAccount);
					addOptionSpreadDetailNote(detail, String.format("Unable to find Security Target for Underlying Security %s on %tD ", optionSpread.getOptionUnderlyingSecurity(), optionSpread.getBalanceDate()));
				}
				else {
					detail = createOptionSpreadDetail(optionSpreadContext, clientSecurityTarget, false);
					Map<InvestmentSecurity, Trade> securityListMap = BeanUtils.getBeanMap(clientTradeList, Trade::getInvestmentSecurity);
					populateOptionSpreadForDetail(getTradeGroupTradeCycleUtilHandler().getOptionCombinationType(clientAccount.getServiceProcessingType()), securityListMap.values(), detail);

					// populate option spread group's securities if they are not already
					if (optionSpread.getLongPutSecurity() == null && optionSpread.getShortPutSecurity() == null && optionSpread.getShortCallSecurity() == null && optionSpread.getLongCallSecurity() == null) {
						if (detail.getLongPutTrade() != null) {
							optionSpread.setLongPutSecurity(detail.getLongPutTrade().getInvestmentSecurity());
						}
						if (detail.getShortPutTrade() != null) {
							optionSpread.setShortPutSecurity(detail.getShortPutTrade().getInvestmentSecurity());
						}
						if (detail.getShortCallTrade() != null) {
							optionSpread.setShortCallSecurity(detail.getShortCallTrade().getInvestmentSecurity());
						}
						if (detail.getLongCallTrade() != null) {
							optionSpread.setLongCallSecurity(detail.getLongCallTrade().getInvestmentSecurity());
						}
					}

					// populate the trades on the detail based on matching security
					securityListMap.values().forEach(trade -> {
						if (detail.getHoldingAccount() == null) {
							detail.setHoldingAccount(trade.getHoldingInvestmentAccount());
						}
						if (detail.getExecutingBrokerCompany() == null) {
							detail.setExecutingBrokerCompany(trade.getExecutingBrokerCompany());
						}
					});

					Trade trade = ObjectUtils.coalesce(detail.getLongPutTrade(), detail.getShortPutTrade(), detail.getLongCallTrade(), detail.getShortCallTrade());
					if (trade != null) {
						populateOptionSpreadDetailExposure(optionSpreadContext, detail, trade.getQuantityIntended());
					}
				}

				optionSpread.getDetailList().add(detail);
			});
		}
	}


	/**
	 * Populates the option trades for the {@link TradeGroupDynamicSecurityTargetOptionSpreadDetail} based on the trades and {@link TradeOptionCombinationTypes}.
	 */
	private void populateOptionSpreadForDetail(TradeOptionCombinationTypes combinationType, Collection<Trade> tradeCollection, TradeGroupDynamicSecurityTargetOptionSpreadDetail detail) {
		int tradeSize = CollectionUtils.getSize(tradeCollection);
		if (combinationType.getOptionLegTypes().length != tradeSize) {
			addOptionSpreadDetailNote(detail, String.format("Found Option Combination mismatch; found %d of expected %d trades", tradeSize, combinationType.getOptionLegTypes().length));
		}
		for (TradeOptionLegTypes legType : combinationType.getOptionLegTypes()) {
			for (Trade trade : tradeCollection) {
				boolean isLong = trade.isBuy();
				boolean isShort = !isLong;
				boolean isCall = trade.getInvestmentSecurity().isCallOption();
				boolean isPut = !isCall;
				boolean found = false;

				switch (legType) {
					case LONG_PUT:
						if (isLong && isPut) {
							found = true;
							detail.setLongPutTrade(trade);
						}
						break;
					case SHORT_PUT:
						if (isShort && isPut) {
							found = true;
							detail.setShortPutTrade(trade);
						}
						break;
					case SHORT_CALL:
						if (isShort && isCall) {
							found = true;
							detail.setShortCallTrade(trade);
						}
						break;
					case LONG_CALL:
						if (isLong && isCall) {
							found = true;
							detail.setLongCallTrade(trade);
						}
						break;
				}

				// if we found a match, break out of inner loop early
				if (found) {
					break;
				}
			}
		}
	}


	private void clearOptionSpreadDetailList(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
		if (optionSpread.getDetailList() == null) {
			optionSpread.setDetailList(new ArrayList<>());
		}
		else if (!optionSpread.getDetailList().isEmpty()) {
			optionSpread.getDetailList().clear();
		}
	}


	private void validateOptionSpreadSecurities(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext) {
		// Get and Verify BusinessServiceProcessingType for group
		Set<InvestmentAccount> clientInvestmentAccountList = optionSpreadContext.getSecurityTargetListMap().keySet();
		Set<BusinessServiceProcessingType> businessServiceProcessingTypeSet = CollectionUtils.getStream(clientInvestmentAccountList)
				.map(InvestmentAccount::getServiceProcessingType)
				.collect(Collectors.toSet());

		ValidationUtils.assertNotEmpty(businessServiceProcessingTypeSet, "No BusinessServiceProcessingType found for accounts in this group.");
		ValidationUtils.assertFalse(businessServiceProcessingTypeSet.contains(null), "One or more accounts are missing a BusinessServiceProcessingType.");
		ValidationUtils.assertTrue(businessServiceProcessingTypeSet.size() == 1, "Accounts with differing processing service processing types found in this group.");

		BusinessServiceProcessingType businessServiceProcessingType = CollectionUtils.getFirstElement(businessServiceProcessingTypeSet);

		TradeGroupDynamicSecurityTargetOptionSpread optionSpread = optionSpreadContext.getOptionSpread();
		InvestmentSecurity longPut = optionSpread.getLongPutSecurity();
		InvestmentSecurity shortPut = optionSpread.getShortPutSecurity();
		InvestmentSecurity shortCall = optionSpread.getShortCallSecurity();
		InvestmentSecurity longCall = optionSpread.getLongCallSecurity();

		// Check spread for required structures
		TradeOptionCombinationTypes optionCombinationType = getTradeGroupTradeCycleUtilHandler().getOptionCombinationType(businessServiceProcessingType);
		ValidationUtils.assertTrue(isOptionSpreadStructureValid(longPut, shortPut, shortCall, longCall, optionCombinationType), "The option combination does not conform to a the expected combination for: " + BeanUtils.getLabel(optionCombinationType) + ".");

		// all use same expiration date
		// expiration date is normally 26-30 days from balance date
		Set<Date> expirationDateSet = Stream.of(longPut, shortPut, shortCall, longCall)
				.filter(Objects::nonNull)
				.map(InvestmentSecurity::getLastDeliveryDate)
				.collect(Collectors.toSet());
		if (CollectionUtils.isEmpty(expirationDateSet)) {
			// no securities to validate
			return;
		}
		if (optionSpread.isValidateExpiringPositions()) {
			ValidationUtils.assertTrue(CollectionUtils.getSize(expirationDateSet) == 1, "All Spread Options to trade are expected to have the same Expiration Date.");
			validateOptionExpirationDateForBusinessServiceProcessingType(optionSpreadContext, CollectionUtils.getFirstElementStrict(expirationDateSet), businessServiceProcessingType);
		}

		BigDecimal underlyingSecurityPrice = optionSpreadContext.getUnderlyingSecurityPrice();
		Set<TradeOptionLegTypes> optionLegTypesSet = CollectionUtils.createHashSet(optionCombinationType.getOptionLegTypes());

		// Validate option deltas in range and strikes (short put strike < underlying price; short call strike > underlying price)
		BigDecimal shortCallStrike = null;
		BigDecimal shortPutStrike = null;
		if (optionLegTypesSet.contains(TradeOptionLegTypes.LONG_PUT)) {
			validateOptionDelta(longPut, businessServiceProcessingType, BusinessServiceProcessingType.LONG_PUT_DELTA, optionSpreadContext);
		}
		if (optionLegTypesSet.contains(TradeOptionLegTypes.SHORT_PUT)) {
			validateOptionDelta(shortPut, businessServiceProcessingType, BusinessServiceProcessingType.SHORT_PUT_DELTA, optionSpreadContext);
			shortPutStrike = shortPut.getOptionStrikePrice();
			ValidationUtils.assertTrue(MathUtils.isLessThan(shortPutStrike, underlyingSecurityPrice), String.format("Short Put Strike Price: %s should be less than the current underlying price: %s", CoreMathUtils.formatNumberMoney(shortPutStrike), CoreMathUtils.formatNumberMoney(underlyingSecurityPrice)));
		}
		if (optionLegTypesSet.contains(TradeOptionLegTypes.SHORT_CALL)) {
			validateOptionDelta(shortCall, businessServiceProcessingType, BusinessServiceProcessingType.SHORT_CALL_DELTA, optionSpreadContext);
			shortCallStrike = shortCall.getOptionStrikePrice();
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(shortCallStrike, underlyingSecurityPrice), String.format("Short Call Strike Price: %s should be greater than the current underlying price: %s", CoreMathUtils.formatNumberMoney(shortCallStrike), CoreMathUtils.formatNumberMoney(underlyingSecurityPrice)));
		}
		if (optionLegTypesSet.contains(TradeOptionLegTypes.LONG_CALL)) {
			validateOptionDelta(longCall, businessServiceProcessingType, BusinessServiceProcessingType.LONG_CALL_DELTA, optionSpreadContext);
		}

		// Validate the option spreads by spread types.
		BigDecimal actualPutSpreadStrikeDifference = BigDecimal.ZERO;
		BigDecimal actualCallSpreadStrikeDifference = BigDecimal.ZERO;
		// both put spread options; strikes are spread by strike difference; Short Put Strike > Long Put Strike
		if (optionCombinationType == TradeOptionCombinationTypes.BULL_PUT_SPREAD || optionCombinationType == TradeOptionCombinationTypes.IRON_CONDOR) {
			boolean samePutType = longPut.getOptionType() == InvestmentSecurityOptionTypes.PUT && shortPut.getOptionType() == InvestmentSecurityOptionTypes.PUT;
			ValidationUtils.assertTrue(samePutType, "The Put Spread Options must both be Put Options.");
			actualPutSpreadStrikeDifference = MathUtils.subtract(shortPutStrike, longPut.getOptionStrikePrice());
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(actualPutSpreadStrikeDifference, BigDecimal.ZERO), "The Short Put Strike Price should be greater than the Long Put Strike Price.");
		}

		// both call spread options; strikes are spread by strike difference; Short Call Strike < Long Call Strike
		if ((optionCombinationType == TradeOptionCombinationTypes.BEAR_CALL_SPREAD || optionCombinationType == TradeOptionCombinationTypes.IRON_CONDOR)) {
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(MathUtils.subtract(shortCallStrike, shortPutStrike), BigDecimal.ZERO), "The Short Call Strike Price should be greater than the Short Put Strike Price.");
			boolean sameCallType = shortCall.getOptionType() == InvestmentSecurityOptionTypes.CALL && longCall.getOptionType() == InvestmentSecurityOptionTypes.CALL;
			ValidationUtils.assertTrue(sameCallType, "The Call Spread Options must both be Call Options.");
			actualCallSpreadStrikeDifference = MathUtils.subtract(longCall.getOptionStrikePrice(), shortCallStrike);
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(actualCallSpreadStrikeDifference, BigDecimal.ZERO), "The Long Call Strike Price should be greater than the Short Call Strike Price.");
		}

		// Validate option spreads to underlying price rounded down to nearest whole number for the max spread
		// as configured by the processing type, or client account override.
		Set<BigDecimal> maxSpreadPercentSet = new HashSet<>();
		for (InvestmentAccount clientAccount : clientInvestmentAccountList) {
			BigDecimal maxSpreadPercent = getTradeGroupTradeCycleUtilHandler().getMaxAllowedOptionSpreadFromUnderlyingPricePercent(clientAccount);
			if (maxSpreadPercentSet.add(maxSpreadPercent)) {
				BigDecimal maxSpreadStrikeDifference = MathUtils.round(MathUtils.getPercentageOf(maxSpreadPercent, underlyingSecurityPrice, true), 0, RoundingMode.DOWN);

				// both put spread options; strikes are spread by strike difference; Short Put Strike > Long Put Strike
				if (optionCombinationType == TradeOptionCombinationTypes.BULL_PUT_SPREAD || optionCombinationType == TradeOptionCombinationTypes.IRON_CONDOR) {
					ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(actualPutSpreadStrikeDifference, maxSpreadStrikeDifference), String.format("The Put Spread Strike Prices are expected to be spread less than or equal to %s for Client Account %s", maxSpreadStrikeDifference, clientAccount));
				}

				// both call spread options; strikes are spread by strike difference; Short Call Strike < Long Call Strike
				if ((optionCombinationType == TradeOptionCombinationTypes.BEAR_CALL_SPREAD || optionCombinationType == TradeOptionCombinationTypes.IRON_CONDOR)) {
					ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(actualCallSpreadStrikeDifference, maxSpreadStrikeDifference), String.format("The Call Spread Strike Prices are expected to be spread less than or equal to %s for Client Account %s", maxSpreadStrikeDifference, clientAccount));
				}
			}
		}
	}


	private void validateOptionExpirationDateForBusinessServiceProcessingType(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, Date optionExpirationDate, BusinessServiceProcessingType businessServiceProcessingType) {
		Short cycleDeviationDays = getTradeGroupTradeCycleUtilHandler().getAllowedOptionExpirationCycleDeviationDays(businessServiceProcessingType);
		if (cycleDeviationDays != null) {
			List<InvestmentAccountSecurityTarget> securityTargetList = CollectionUtils.getFlattened(optionSpreadContext.getSecurityTargetListMap().values());
			// Cycle Duration Weeks is defined on the processing type but can be overridden by security targets.
			// Look for unique estimated cycle end dates and find client accounts that are affected.
			Map<Date, List<InvestmentAccount>> cycleEndDateToClientAccountListMap = new HashMap<>();
			for (InvestmentAccountSecurityTarget securityTarget : securityTargetList) {
				Date estimatedCycleEndDate = getTradeGroupTradeCycleUtilHandler().getEstimatedCycleEndDate(optionSpreadContext.getBalanceDate(), securityTarget);
				if (estimatedCycleEndDate != null) {
					cycleEndDateToClientAccountListMap.computeIfAbsent(estimatedCycleEndDate, key -> new ArrayList<>()).add(securityTarget.getClientInvestmentAccount());
				}
			}
			List<InvestmentAccount> invalidExpirationDateClientList = cycleEndDateToClientAccountListMap.entrySet().stream()
					.map(entry -> {
						Date minimumDate = DateUtils.addWeekDays(entry.getKey(), -cycleDeviationDays);
						Date maximumDate = DateUtils.addWeekDays(entry.getKey(), cycleDeviationDays);
						return DateUtils.isDateBetween(optionExpirationDate, minimumDate, maximumDate, false) ? Collections.<InvestmentAccount>emptyList() : entry.getValue();
					})
					.flatMap(CollectionUtils::getStream)
					.collect(Collectors.toList());
			ValidationUtils.assertEmpty(invalidExpirationDateClientList, () -> {
				List<String> clientAccountLabelList = CollectionUtils.getConverted(invalidExpirationDateClientList, InvestmentAccount::getLabel);
				return "The Expiration Date of the Option(s) to trade does not fall within the configured cycle date range for Client Accounts: <br/>- " + StringUtils.collectionToDelimitedString(clientAccountLabelList, "<br/>- ");
			});
		}
	}


	/**
	 * Validates the provided option's delta value against the {@link BusinessServiceProcessingType}'s configured value. If there is no configured value, no validation is done.
	 * Live delta values are looking up according to the {@link TradeGroupDynamicSecurityTargetOptionSpread#isLivePrices()}.
	 */
	private void validateOptionDelta(InvestmentSecurity option, BusinessServiceProcessingType businessServiceProcessingType, String deltaType, TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext) {
		if (InvestmentUtils.isListedOption(option)) {
			ValidationUtils.assertNotNull(option, "The validateOptionDelta function requires a valid Option security to validate.");
			ValidationUtils.assertNotNull(businessServiceProcessingType, "The validateOptionDelta function requires a BusinessServiceProcessingType");
			ValidationUtils.assertNotEmpty(deltaType, "The validateOptionDelta function requires a delta type.");
			String decimalFormat = "0.0000";
			BigDecimal deltaMid = getTradeGroupTradeCycleUtilHandler().getDeltaMidRange(businessServiceProcessingType, deltaType);
			if (deltaMid != null) {
				BigDecimal allowedDeltaDeviation = getTradeGroupTradeCycleUtilHandler().getAllowedDeltaDeviation(businessServiceProcessingType);
				BigDecimal minDelta = MathUtils.max(BigDecimal.ZERO, MathUtils.subtract(deltaMid, allowedDeltaDeviation));
				BigDecimal maxDelta = MathUtils.min(BigDecimal.ONE, MathUtils.add(deltaMid, allowedDeltaDeviation));
				BigDecimal delta;
				if (optionSpreadContext.getOptionSpread().isLivePrices()) {
					MarketDataLive liveDelta = getMarketDataRetriever().getLiveDelta(option);
					ValidationUtils.assertNotNull(liveDelta, "Unable to obtain live delta value for option: " + option.getSymbol());
					delta = liveDelta.asNumericValue();
				}
				else {
					delta = getMarketDataRetriever().getDeltaFlexible(option, optionSpreadContext.getOptionSpread().getBalanceDate(), true);
				}
				ValidationUtils.assertTrue(MathUtils.isBetween(delta, minDelta, maxDelta), String.format("%s: %s must fall between %s and %s.", deltaType, CoreMathUtils.formatNumber(delta, decimalFormat), CoreMathUtils.formatNumber(minDelta, decimalFormat), CoreMathUtils.formatNumber(maxDelta, decimalFormat)));
			}
		}
	}


	/**
	 * Adjusts the tradeQuantity to a lower value in the TradeGroupDynamicSecurityTargetOptionSpreadDetail  if the resulting trade would result in an aggregate value at risk that
	 * exceeds the allowed risk limit.
	 */
	private void adjustQuantityForVar(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, TradeGroupDynamicSecurityTargetOptionSpreadDetail detail, TradeOptionCombinationTypes optionCombinationType) {
		if (MathUtils.isGreaterThan(detail.getTradeQuantity(), BigDecimal.ZERO)) {
			final BigDecimal maxAllowedValueAtRisk = MathUtils.multiply(detail.getValueAtRiskPercent(), detail.getSecurityTargetNotional());

			// filter out options expiring today that still have positions
			final List<AccountingPosition> accountingPositionList = CollectionUtils.getStream(optionSpreadContext.getAccountingPositionListForClientAccount(detail.getClientAccount()))
					.filter(accountingPosition -> DateUtils.isDateAfter(accountingPosition.getInvestmentSecurity().getLastDeliveryOrEndDate(), optionSpreadContext.getBalanceDate()))
					.collect(Collectors.toList());

			final Collection<TradeOptionCombination> tradeOptionCombinationList = getTradeOptionCombinationHandler().getTradeOptionCombinationList(accountingPositionList, optionCombinationType);
			final BigDecimal existingValueAtRisk = CoreMathUtils.sumProperty(tradeOptionCombinationList, x -> getTradeOptionCombinationHandler().getOptionCombinationDownsidePotential(x));

			final BigDecimal remainingVar = MathUtils.subtract(maxAllowedValueAtRisk, existingValueAtRisk);
			BigDecimal maxExposure = MathUtils.max(detail.getPutSpreadExposure(), detail.getCallSpreadExposure());
			if (maxExposure == null) {
				maxExposure = BigDecimal.ZERO;
			}

			if (MathUtils.isGreaterThan(maxExposure, remainingVar)) {
				final BigDecimal originalTradeQuantity = detail.getTradeQuantity();
				BigDecimal reducingRatio = MathUtils.isEqual(maxExposure, BigDecimal.ZERO) ? BigDecimal.ZERO : MathUtils.divide(remainingVar, maxExposure);
				reducingRatio = MathUtils.max(reducingRatio, BigDecimal.ZERO);
				final BigDecimal revisedTradeQuantity = MathUtils.multiply(reducingRatio, originalTradeQuantity).setScale(0, RoundingMode.FLOOR);

				addOptionSpreadDetailNote(detail, "Reducing trade quantity from " + originalTradeQuantity + " to " + revisedTradeQuantity + " due to maximum VAR reached for account.");
				populateOptionSpreadDetailExposure(optionSpreadContext, detail, revisedTradeQuantity);
			}
		}
	}


	/**
	 * Returns the number of positions expected to be in each tranche for the client {@link InvestmentAccount} of the provided detail.
	 * <p>
	 * Validates the client's {@link BusinessServiceProcessingType} used for determining the count
	 * <br/>- OARS (4 positions of same quantity; long put, short put, short call, long call)
	 * <br/>- RPS (2 positions of same quantity; long put, short put)
	 * <p>
	 * Returns -1 if the client's business service is not defined or not applicable to this trading calculator.
	 */
	private int getClientAccountExpectedTrancheAccountingPositionCount(TradeGroupDynamicSecurityTargetOptionSpreadDetail detail) {

		BusinessServiceProcessingType processingType = detail.getClientAccount().getServiceProcessingType();

		if (processingType == null) {
			addOptionSpreadDetailNote(detail, String.format("No Business Service Processing Type is defined for Client Account: %s", detail.getClientAccount()));
			return -1;
		}

		Short expectedAccountingPositionCount = getTradeGroupTradeCycleUtilHandler().getTranchePositionCount(processingType);
		if (expectedAccountingPositionCount == null) {
			addOptionSpreadDetailNote(detail, String.format("No value for BusinessServicePropertyType property \"Tranche Position Count\" for account: %s", detail.getClientAccount()));
			return -1;
		}
		return expectedAccountingPositionCount;
	}


	private TradeGroupDynamicSecurityTargetOptionSpreadDetail createOptionSpreadDetail(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, InvestmentAccountSecurityTarget clientSecurityTarget, boolean defaultHoldingAccount) {
		InvestmentAccount clientAccount = clientSecurityTarget.getClientInvestmentAccount();

		Short trancheCount = getTradeGroupTradeCycleUtilHandler().getTrancheCount(clientSecurityTarget);
		TradeSecurityTarget tradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetWithUnderlyingSecurityPriceForDate(clientSecurityTarget, optionSpreadContext.getBalanceDate(), optionSpreadContext.getUnderlyingSecurityPrice());
		BigDecimal targetNotional = tradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs();

		BigDecimal clientValueAtRiskPercent = getClientAccountValueAtRiskPercent(clientAccount);

		TradeGroupDynamicSecurityTargetOptionSpreadDetail detail = new TradeGroupDynamicSecurityTargetOptionSpreadDetail();
		detail.setClientAccount(clientAccount);
		detail.setSecurityTarget(clientSecurityTarget);
		detail.setValueAtRiskPercent(clientValueAtRiskPercent);
		detail.setSecurityTargetNotional(targetNotional);

		populateOptionSpreadDetailUsageDetails(detail, optionSpreadContext, tradeSecurityTarget);

		if (trancheCount == null || trancheCount == 0) {
			addOptionSpreadDetailNote(detail, String.format("Client Security Target %s is missing Tranche Count in order to correctly calculation trade quantities", clientSecurityTarget));
		}
		else {
			BigDecimal clientTrancheAverageNotional = MathUtils.divide(targetNotional, trancheCount);
			detail.setAverageTrancheValueAtRisk(MathUtils.multiply(clientTrancheAverageNotional, clientValueAtRiskPercent));
			detail.setAverageTrancheNotional(getValueAtRiskAdjustedValue(clientAccount, clientTrancheAverageNotional, clientValueAtRiskPercent));
		}
		detail.setExpiringQuantity(getClientAccountExpiringOptionQuantity(optionSpreadContext, clientAccount));

		if (defaultHoldingAccount) {
			populateOptionSpreadDetailDefaultHoldingAccount(optionSpreadContext, detail);
			if (detail.getExecutingBrokerCompany() == null) {
				detail.setExecutingBrokerCompany(getExecutingBrokerCompanyDefaultForOptionSpreadDetail(detail));
			}

			// if the detail has an executing broker company set, populate the TradeDestination if there is a single trade destination that does not match the group's trade destination.
			if (detail.getExecutingBrokerCompany() != null) {
				List<TradeDestination> detailTradeDestinationList = getTradeDestinationList(detail, optionSpreadContext.getBalanceDate());
				TradeDestination optionSpreadTradeDestination = optionSpreadContext.getOptionSpread().getTradeDestination();
				if (detailTradeDestinationList.size() == 1 && (optionSpreadTradeDestination == null || !optionSpreadTradeDestination.getId().equals(detailTradeDestinationList.get(0).getId()))) {
					detail.setTradeDestination(detailTradeDestinationList.get(0));
				}
			}
		}

		return detail;
	}


	/**
	 * Returns the configured Value At Risk Percent value for the provided client {@link InvestmentAccount}.
	 * The VAR is configured in Portfolio Setup custom field group for the client.
	 */
	private BigDecimal getClientAccountValueAtRiskPercent(InvestmentAccount clientAccount) {
		BigDecimal clientValueAtRiskPercent = (BigDecimal) getInvestmentAccountUtilHandler().getClientInvestmentAccountPortfolioSetupCustomColumnValue(clientAccount, CLIENT_ACCOUNT_VALUE_AT_RISK_PERCENT_COLUMN_NAME);
		if (getTradeGroupTradeCycleUtilHandler().isUsesValueAtRisk(clientAccount.getServiceProcessingType())) {
			ValidationUtils.assertNotNull(clientValueAtRiskPercent, String.format("Unable to obtain %s value for client account: %s", CLIENT_ACCOUNT_VALUE_AT_RISK_PERCENT_COLUMN_NAME, clientAccount));
		}

		return clientValueAtRiskPercent != null ? MathUtils.divide(clientValueAtRiskPercent, MathUtils.BIG_DECIMAL_ONE_HUNDRED) : BigDecimal.ZERO;
	}


	private BigDecimal getValueAtRiskAdjustedValue(InvestmentAccount clientAccount, BigDecimal quantityOrNotional, BigDecimal valueAtRiskPercentage) {
		if (getTradeGroupTradeCycleUtilHandler().isUsesValueAtRisk(clientAccount.getServiceProcessingType())) {
			// averageTrancheValueAtRisk -- the value at risk for one tranche. ClientValueAtRiskPercent is configured at the account level and is usually 5% or 10%.
			// By dividing by 5%, we calculate a notional value for this tranche.  This calculation doubles the trancheNotional if averageTrancheValueAtRisk represents a 10% risk level of the instead of 5%.
			BigDecimal varAdjustedTargetUnderlyingQuantity = MathUtils.multiply(quantityOrNotional, valueAtRiskPercentage);
			return MathUtils.divide(varAdjustedTargetUnderlyingQuantity, FIVE_PERCENT);
		}
		return quantityOrNotional;
	}


	private void populateOptionSpreadDetailUsageDetails(TradeGroupDynamicSecurityTargetOptionSpreadDetail detail, TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, TradeSecurityTarget tradeSecurityTarget) {
		BigDecimal targetUnderlyingQuantity = getValueAtRiskAdjustedValue(detail.getClientAccount(), tradeSecurityTarget.getTargetUnderlyingQuantityAdjustedAbs(), detail.getValueAtRiskPercent());
		detail.setTargetUnderlyingQuantity(targetUnderlyingQuantity);
		detail.setUnexpiredCoveredUnderlyingQuantity(getClientAccountUnexpiredCoveredQuantity(optionSpreadContext, tradeSecurityTarget.getSecurityTarget()));
		detail.setAvailableUnderlyingQuantity(MathUtils.subtract(detail.getTargetUnderlyingQuantity(), detail.getUnexpiredCoveredUnderlyingQuantity()));
		BigDecimal priceMultiplier = getOptionSpreadOptionPriceMultiplier(optionSpreadContext.getOptionSpread());
		detail.setAvailableQuantity(MathUtils.round(MathUtils.divide(detail.getAvailableUnderlyingQuantity(), priceMultiplier), 0, BigDecimal.ROUND_FLOOR));
	}


	/**
	 * Returns the price multiplier for a populated option spread option.
	 */
	private BigDecimal getOptionSpreadOptionPriceMultiplier(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
		BigDecimal priceMultiplier = MathUtils.BIG_DECIMAL_ONE_HUNDRED;
		InvestmentSecurity tradedOption = ObjectUtils.coalesce(optionSpread.getLongPutSecurity(), optionSpread.getShortPutSecurity(), optionSpread.getShortCallSecurity(), optionSpread.getLongCallSecurity());
		if (tradedOption != null) {
			priceMultiplier = tradedOption.getPriceMultiplier();
		}
		return priceMultiplier;
	}


	private void populateOptionSpreadDetailDefaultHoldingAccount(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, TradeGroupDynamicSecurityTargetOptionSpreadDetail detail) {
		if (detail.getHoldingAccount() == null) {
			// If Only 1, Set It
			List<InvestmentAccount> list = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(detail.getClientAccount().getId(), null, getTradeType().getHoldingAccountPurpose().getName(), optionSpreadContext.getOptionSpread().getTradeDate());
			if (CollectionUtils.getSize(list) == 1) {
				detail.setHoldingAccount(list.get(0));
			}
			// If there are multiple see if there is only one of the holding accounts that actually holds positions
			else if (CollectionUtils.getSize(list) > 1) {
				List<InvestmentAccount> holdingAccountList = CollectionUtils.getConverted(optionSpreadContext.getAccountingPositionBalanceListForClientAccount(detail.getClientAccount()), AccountingPositionBalance::getHoldingInvestmentAccount);
				if (CollectionUtils.getSize(holdingAccountList) == 1) {
					InvestmentAccount positionHoldingAccount = holdingAccountList.get(0);
					for (InvestmentAccount account : list) {
						if (positionHoldingAccount.equals(account)) {
							detail.setHoldingAccount(account);
							break;
						}
					}
				}
			}
		}
	}


	/**
	 * Returns the list of expiring option AccountingPositions for a client account
	 */
	private List<AccountingPositionBalance> getExpiringOptionAccountingPositionList(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, InvestmentAccount clientAccount) {
		return CollectionUtils.getStream(optionSpreadContext.getAccountingPositionBalanceListForClientAccount(clientAccount))
				.filter(positionBalance -> DateUtils.isEqualWithoutTime(positionBalance.getInvestmentSecurity().getLastDeliveryDate(), optionSpreadContext.getBalanceDate()))
				.filter(positionBalance -> InvestmentUtils.isOption(positionBalance.getInvestmentSecurity()))
				.collect(Collectors.toList());
	}


	/**
	 * Returns the underlying quantity equivalent for unexpired option positions pertaining to a security target.
	 */
	private BigDecimal getClientAccountUnexpiredCoveredQuantity(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, InvestmentAccountSecurityTarget clientSecurityTarget) {
		TradeOptionCombinationTypes optionCombination = getTradeGroupTradeCycleUtilHandler().getOptionCombinationType(clientSecurityTarget.getClientInvestmentAccount().getServiceProcessingType());
		if (optionCombination == null || optionCombination.getOptionLegTypes()[0] == null) {
			BigDecimal unexpiredOptionContractCount = getTradeGroupTradeCycleUtilHandler().getUnexpiredContractCount(clientSecurityTarget, optionSpreadContext.getAccountingPositionBalanceListForClientAccount(clientSecurityTarget.getClientInvestmentAccount()), optionSpreadContext.getBalanceDate());
			BigDecimal priceMultiplier = getOptionSpreadOptionPriceMultiplier(optionSpreadContext.getOptionSpread());
			return MathUtils.multiply(unexpiredOptionContractCount, priceMultiplier);
		}
		InvestmentSecurityOptionTypes optionTypeValue = optionCombination.getOptionLegTypes()[0].isCall() ? InvestmentSecurityOptionTypes.CALL : InvestmentSecurityOptionTypes.PUT;
		return CollectionUtils.getStream(optionSpreadContext.getAccountingPositionBalanceListForClientAccount(clientSecurityTarget.getClientInvestmentAccount()))
				.filter(positionBalance -> InvestmentUtils.isOption(positionBalance.getInvestmentSecurity()))
				.filter(positionBalance -> clientSecurityTarget.getTargetUnderlyingSecurity().equals(positionBalance.getInvestmentSecurity().getUnderlyingSecurity()))
				.filter(positionBalance -> optionTypeValue == positionBalance.getInvestmentSecurity().getOptionType())
				.filter(positionBalance -> optionCombination.getOptionLegTypes()[0].isLong() ? MathUtils.isPositive(positionBalance.getRemainingQuantity()) : MathUtils.isNegative(positionBalance.getRemainingQuantity()))
				.filter(positionBalance -> DateUtils.isDateAfter(positionBalance.getInvestmentSecurity().getLastDeliveryDate(), optionSpreadContext.getBalanceDate()))
				.map(positionBalance -> MathUtils.multiply(positionBalance.getRemainingQuantity().abs(), positionBalance.getInvestmentSecurity().getPriceMultiplier()))
				.reduce(BigDecimal.ZERO, MathUtils::add);
	}


	private BigDecimal getClientAccountExpiringOptionQuantity(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, InvestmentAccount clientAccount) {
		List<AccountingPositionBalance> expiringOptionAccountingPositionList = getExpiringOptionAccountingPositionList(optionSpreadContext, clientAccount);

		if (CollectionUtils.isEmpty(expiringOptionAccountingPositionList)) {
			return BigDecimal.ZERO;
		}

		Set<BigDecimal> expiringOptionAccountingPositionQuantitySet = expiringOptionAccountingPositionList.stream()
				.map(position -> position.getRemainingQuantity().abs())
				.collect(Collectors.toSet());
		if (optionSpreadContext.getOptionSpread().isValidateExpiringPositions()) {
			ValidationUtils.assertTrue(expiringOptionAccountingPositionQuantitySet.size() == 1, String.format("Expected all expiring positions to be of the same quantity; found {%s}", StringUtils.collectionToCommaDelimitedString(expiringOptionAccountingPositionQuantitySet)));
		}
		return expiringOptionAccountingPositionQuantitySet.stream().findFirst()
				.orElse(null); // result should never be null as it was previously validated
	}


	private void populateOptionSpreadDetailExposure(TradeGroupDynamicSecurityTargetOptionSpreadContext optionSpreadContext, TradeGroupDynamicSecurityTargetOptionSpreadDetail detail, BigDecimal tradeQuantity) {
		detail.setTradeQuantity(tradeQuantity);
		TradeOptionCombinationTypes optionCombination = getTradeGroupTradeCycleUtilHandler().getOptionCombinationType(detail.getClientAccount().getServiceProcessingType());
		if (optionCombination == TradeOptionCombinationTypes.IRON_CONDOR || optionCombination == TradeOptionCombinationTypes.BULL_PUT_SPREAD || optionCombination == TradeOptionCombinationTypes.BEAR_CALL_SPREAD) {
			TradeGroupDynamicSecurityTargetOptionSpread optionSpread = optionSpreadContext.getOptionSpread();
			Function<Boolean, BigDecimal> optionSpreadExposureFunction = putSpread -> {
				BigDecimal longOptionStrike;
				BigDecimal shortOptionStrike;

				if (putSpread) {
					longOptionStrike = optionSpread.getLongPutSecurity() != null ? optionSpread.getLongPutSecurity().getOptionStrikePrice() : null;
					shortOptionStrike = optionSpread.getShortPutSecurity() != null ? optionSpread.getShortPutSecurity().getOptionStrikePrice() : null;
				}
				else {
					longOptionStrike = optionSpread.getLongCallSecurity() != null ? optionSpread.getLongCallSecurity().getOptionStrikePrice() : null;
					shortOptionStrike = optionSpread.getShortCallSecurity() != null ? optionSpread.getShortCallSecurity().getOptionStrikePrice() : null;
				}

				BigDecimal strikeDifference = MathUtils.abs(MathUtils.subtract(longOptionStrike, shortOptionStrike));
				return MathUtils.multiply(MathUtils.multiply(getOptionSpreadOptionPriceMultiplier(optionSpread), strikeDifference), tradeQuantity);
			};

			detail.setPutSpreadExposure(optionSpreadExposureFunction.apply(Boolean.TRUE));
			detail.setCallSpreadExposure(optionSpreadExposureFunction.apply(Boolean.FALSE));
		}
	}


	private TradeType getTradeType() {
		if (this.tradeType == null) {
			this.tradeType = getTradeService().getTradeTypeByName(TradeType.OPTIONS);
		}
		return this.tradeType;
	}


	private Trade createOptionSpreadDetailTrade(TradeGroupDynamicSecurityTargetOptionSpread optionSpread, TradeGroupDynamicSecurityTargetOptionSpreadDetail detail, InvestmentSecurity security, BigDecimal quantity) {
		if (security == null) {
			return null;
		}
		return populateOptionSpreadDetailTrade(optionSpread, detail, new Trade(), security, quantity);
	}


	private Trade populateOptionSpreadDetailTrade(TradeGroupDynamicSecurityTargetOptionSpread optionSpread, TradeGroupDynamicSecurityTargetOptionSpreadDetail detail, Trade trade, InvestmentSecurity security, BigDecimal quantity) {
		if (MathUtils.isNullOrZero(quantity)) {
			return null;
		}

		populateIfNullOrDifferent(trade, Trade::getTradeType, Trade::setTradeType, getTradeType());
		populateIfNullOrDifferent(trade, Trade::getClientInvestmentAccount, Trade::setClientInvestmentAccount, detail.getClientAccount());
		populateIfNullOrDifferent(trade, Trade::getHoldingInvestmentAccount, Trade::setHoldingInvestmentAccount, detail.getHoldingAccount());
		populateIfNullOrDifferent(trade, Trade::getTraderUser, Trade::setTraderUser, optionSpread.getTraderUser());
		populateIfNullOrDifferent(trade, Trade::getTradeDate, Trade::setTradeDate, optionSpread.getTradeDate());
		populateIfNullOrDifferent(trade, Trade::getSettlementDate, Trade::setSettlementDate, optionSpread.getSettlementDate());
		TradeDestination tradeDestination = ObjectUtils.coalesce(detail.getTradeDestination(), optionSpread.getTradeDestination());
		populateIfNullOrDifferent(trade, Trade::getTradeDestination, Trade::setTradeDestination, tradeDestination);
		BusinessCompany executingBrokerCompany = ObjectUtils.coalesce(detail.getExecutingBrokerCompany(), optionSpread.getExecutingBrokerCompany());
		populateIfNullOrDifferent(trade, Trade::getExecutingBrokerCompany, Trade::setExecutingBrokerCompany, executingBrokerCompany);
		populateIfNullOrDifferent(trade, Trade::getInvestmentSecurity, Trade::setInvestmentSecurity, security);
		populateIfNullOrDifferent(trade, Trade::getPayingSecurity, Trade::setPayingSecurity, security.getInstrument().getTradingCurrency());
		populateIfNullOrDifferent(trade, Trade::getQuantityIntended, Trade::setQuantityIntended, quantity);
		// Option Spread trades will always be opening. The closing is handled by TradeToClose and Expiration.
		boolean buy = security.equals(optionSpread.getLongPutSecurity()) || security.equals(optionSpread.getLongCallSecurity());
		TradeOpenCloseTypes optionCloseType = buy ? TradeOpenCloseTypes.BUY_OPEN : TradeOpenCloseTypes.SELL_OPEN;
		populateIfNullOrDifferent(trade, Trade::getOpenCloseType, Trade::setOpenCloseType, getTradeService().getTradeOpenCloseTypeByName(optionCloseType.getName()));
		return trade;
	}


	private <R> void populateIfNullOrDifferent(Trade trade, Function<Trade, R> valueGetter, BiConsumer<Trade, R> valueSetter, R newValue) {
		R value = valueGetter.apply(trade);
		if (value == null || !value.equals(newValue)) {
			valueSetter.accept(trade, newValue);
		}
	}


	private void addOptionSpreadDetailNote(TradeGroupDynamicSecurityTargetOptionSpreadDetail detail, String note) {
		if (detail.getValidationNoteList() == null) {
			detail.setValidationNoteList(new ArrayList<>());
		}
		detail.getValidationNoteList().add(note);
	}


	private BusinessCompany getExecutingBrokerCompanyDefaultForOptionSpreadDetail(TradeGroupDynamicSecurityTargetOptionSpreadDetail detail) {
		// return holding account issuing company if directed broker
		if (detail.getHoldingAccount() != null && detail.getHoldingAccount().getType() != null
				&& detail.getHoldingAccount().getType().isExecutionWithIssuerRequired()) {
			return detail.getHoldingAccount().getIssuingCompany();
		}
		// return single broker restriction for client account
		TradeRestrictionBrokerSearchForm brokerSearchForm = TradeRestrictionBrokerSearchForm.ofClientAccount(detail.getClientAccount(), true);
		List<TradeRestrictionBroker> brokerList = getTradeRestrictionBrokerService().getTradeRestrictionBrokerList(brokerSearchForm);
		if (CollectionUtils.getSize(brokerList) == 1) {
			return brokerList.get(0).getBrokerCompany();
		}
		// return null so it can be defaulted from the option spread upon trade save
		return null;
	}


	private List<TradeDestination> getTradeDestinationList(TradeGroupDynamicSecurityTargetOptionSpreadDetail detail, Date balanceDate) {
		TradeDestinationSearchForm searchForm = new TradeDestinationSearchForm();
		searchForm.setTradeTypeNameEquals(TradeType.OPTIONS);
		searchForm.setExecutingBrokerCompanyId(detail.getExecutingBrokerCompany().getId());
		searchForm.setActiveOnDate(balanceDate != null ? balanceDate : new Date());
		return getTradeDestinationService().getTradeDestinationList(searchForm);
	}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


	/**
	 * Private member class used to hold data for populating details for an option spread.
	 */
	private static final class TradeGroupDynamicSecurityTargetOptionSpreadContext {

		private final TradeGroupDynamicSecurityTargetOptionSpread optionSpread;
		private final Map<InvestmentAccount, List<InvestmentAccountSecurityTarget>> clientSecurityTargetListMap = new HashMap<>();
		private final Map<InvestmentAccount, List<AccountingPositionBalance>> clientAccountingPositionBalanceListMap = new HashMap<>();
		private final Map<InvestmentAccount, List<AccountingPosition>> clientAccountingPositionListMap = new HashMap<>();
		private BigDecimal underlyingSecurityPrice;


		private TradeGroupDynamicSecurityTargetOptionSpreadContext(TradeGroupDynamicSecurityTargetOptionSpread optionSpread) {
			this.optionSpread = optionSpread;
		}


		private TradeGroupDynamicSecurityTargetOptionSpreadContext withUnderlyingSecurityPrice(BigDecimal securityPrice) {
			this.underlyingSecurityPrice = securityPrice;
			return this;
		}


		private TradeGroupDynamicSecurityTargetOptionSpreadContext withClientSecurityTargetListMap(Map<InvestmentAccount, List<InvestmentAccountSecurityTarget>> clientSecurityTargetListMap) {
			if (clientSecurityTargetListMap != null) {
				this.clientSecurityTargetListMap.clear();
				this.clientSecurityTargetListMap.putAll(clientSecurityTargetListMap);
			}
			return this;
		}


		private TradeGroupDynamicSecurityTargetOptionSpreadContext withClientAccountingPositionListMap(Map<InvestmentAccount, List<AccountingPosition>> clientAccountingPositionListMap) {
			if (clientAccountingPositionListMap != null) {
				this.clientAccountingPositionListMap.clear();
				this.clientAccountingPositionListMap.putAll(clientAccountingPositionListMap);
			}
			return this;
		}


		private TradeGroupDynamicSecurityTargetOptionSpreadContext withClientAccountingPositionBalanceListMap(Map<InvestmentAccount, List<AccountingPositionBalance>> clientAccountingPositionBalanceListMap) {
			if (clientAccountingPositionBalanceListMap != null) {
				this.clientAccountingPositionBalanceListMap.clear();
				this.clientAccountingPositionBalanceListMap.putAll(clientAccountingPositionBalanceListMap);
			}
			return this;
		}


		private TradeGroupDynamicSecurityTargetOptionSpread getOptionSpread() {
			return this.optionSpread;
		}


		private BigDecimal getUnderlyingSecurityPrice() {
			return this.underlyingSecurityPrice;
		}


		private Date getBalanceDate() {
			return getOptionSpread().getBalanceDate();
		}


		private Map<InvestmentAccount, List<InvestmentAccountSecurityTarget>> getSecurityTargetListMap() {
			return this.clientSecurityTargetListMap;
		}


		private List<InvestmentAccountSecurityTarget> getSecurityTargetListForClientAccount(InvestmentAccount clientAccount) {
			return this.clientSecurityTargetListMap.get(clientAccount);
		}


		private List<AccountingPosition> getAccountingPositionListForClientAccount(InvestmentAccount clientAccount) {
			return this.clientAccountingPositionListMap.get(clientAccount);
		}


		private List<AccountingPositionBalance> getAccountingPositionBalanceListForClientAccount(InvestmentAccount clientAccount) {
			return this.clientAccountingPositionBalanceListMap.get(clientAccount);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Set<TradeOptionCombinationTypes> getSupportedOptionCombinationTypeSet() {
		return this.supportedOptionCombinationTypeSet;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public InvestmentAccountUtilHandler getInvestmentAccountUtilHandler() {
		return this.investmentAccountUtilHandler;
	}


	public void setInvestmentAccountUtilHandler(InvestmentAccountUtilHandler investmentAccountUtilHandler) {
		this.investmentAccountUtilHandler = investmentAccountUtilHandler;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public TradeRestrictionBrokerService getTradeRestrictionBrokerService() {
		return this.tradeRestrictionBrokerService;
	}


	public void setTradeRestrictionBrokerService(TradeRestrictionBrokerService tradeRestrictionBrokerService) {
		this.tradeRestrictionBrokerService = tradeRestrictionBrokerService;
	}


	public TradeSecurityTargetUtilHandler getTradeSecurityTargetUtilHandler() {
		return this.tradeSecurityTargetUtilHandler;
	}


	public void setTradeSecurityTargetUtilHandler(TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler) {
		this.tradeSecurityTargetUtilHandler = tradeSecurityTargetUtilHandler;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeGroupTradeCycleUtilHandler getTradeGroupTradeCycleUtilHandler() {
		return this.tradeGroupTradeCycleUtilHandler;
	}


	public void setTradeGroupTradeCycleUtilHandler(TradeGroupTradeCycleUtilHandler tradeGroupTradeCycleUtilHandler) {
		this.tradeGroupTradeCycleUtilHandler = tradeGroupTradeCycleUtilHandler;
	}


	public TradeOptionCombinationHandler getTradeOptionCombinationHandler() {
		return this.tradeOptionCombinationHandler;
	}


	public void setTradeOptionCombinationHandler(TradeOptionCombinationHandler tradeOptionCombinationHandler) {
		this.tradeOptionCombinationHandler = tradeOptionCombinationHandler;
	}
}
