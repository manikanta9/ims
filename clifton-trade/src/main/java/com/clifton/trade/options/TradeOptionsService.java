package com.clifton.trade.options;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupType;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>OptionExpirationService</code> handles the retrieval and processing of expiring options.
 *
 * @author jgommels
 * @see <a href="http://wiki/pages/viewpage.action?pageId=2326656">Options (Puts and Calls)</a>
 * @see <a href="http://wiki/display/IT/Options+Transactions">Options Transactions</a>
 */
public interface TradeOptionsService {

	/**
	 * Returns a list of {@link AccountingBalance}s for option positions that match the search form. This method modifies the search form
	 * that is passed to filter down to position {@link AccountingAccount}s with an investment type of "Options" and a current transaction date.
	 *
	 * @param searchForm the search form for retrieving option position balances. To retrieve option position balances expiring on a date, {@link AccountingBalanceSearchForm#setLastDeliveryDate(java.util.Date)}
	 *                   should be set.
	 */
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public List<TradeOptionsPositionBalance> getTradeOptionPositionBalanceList(AccountingBalanceSearchForm searchForm);


	/**
	 * Generates trade(s) for {@link TradeOptionsPositionBalance}s. The trades are processed according to the {@link TradeGroupType} defined on the request.
	 * <p>
	 * Here are the support trade group types and the actions performed for each:
	 * <br/>- <b>Option Expiration</b>: generates trade(s) to close the option position(s); trades are grouped by underlying security
	 * <br/>- <b>Option Trade To Close</b>: generates trade(s) to close the option position(s); trades are group by underlying security with a single Put and Call pair for strangle execution
	 * <br/>- <b>Option Physical Delivery</b>: generates trade(s) to deliver the underlying security of an 'In the Money' option; trades are group by underlying security
	 * <br/>- <b>Option Delivery Offset</b>: generates trade(s) to offset the delivery of the underlying security of an 'In the Money' option; trades are grouped by underlying security;
	 * if the underlying is a 'big' security, the associated 'mini' will be traded; trades can be created for REDI execution and for BTIC
	 *
	 * @param request
	 */
	@ModelAttribute("tradeGroupList")
	@SecureMethod(dtoClass = Trade.class)
	public List<TradeGroup> generateTradeGroupListForTradeOptionsRequest(TradeOptionsRequest request);
}
