package com.clifton.trade.options.securitytarget;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>TradeSecurityTargetLastTradeDateUpdatingJob</code> is a {@link Task} that can be used in a Batch Job for updating
 * The last buy and last sell trade dates for {@link InvestmentAccountSecurityTarget}s applicable to defined property criteria.
 *
 * @author NickK
 */
public class TradeSecurityTargetLastTradeDateUpdatingJob implements Task, StatusHolderObjectAware<Status> {

	private StatusHolderObject<Status> statusHolderObject;

	/**
	 * Date to look for active security targets to update. Defaults to the current day.
	 */
	private Date activeOnDate;

	private Integer clientInvestmentAccountId;
	private Integer clientInvestmentAccountGroupId;
	private Integer underlyingSecurityId;

	// 1 means trades from 1 business day before and including activeOnDate
	private Integer tradeDateDaysBeforeActiveOnDate = 1;
	// 1 means trades for 1 business day after and including activeOnDate
	private Integer tradeDateDaysAfterActiveOnDate = 1;

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;
	private TradeService tradeService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();

		Date activeOn = getActiveOnDate() != null ? getActiveOnDate() : DateUtils.clearTime(new Date());

		int clientAccountCount = 0;
		int securityTargetCount = 0;
		int updatedSecurityTargetCount = 0;
		int failureCount = 0;
		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetList(activeOn);
		if (!CollectionUtils.isEmpty(securityTargetList)) {
			// Map security targets by client account
			Map<InvestmentAccount, List<InvestmentAccountSecurityTarget>> clientSecurityTargetListMap = BeanUtils.getBeansMap(securityTargetList, InvestmentAccountSecurityTarget::getClientInvestmentAccount);
			Integer[] clientInvestmentAccountIds = BeanUtils.getBeanIdentityArray(clientSecurityTargetListMap.keySet(), Integer.class);

			List<Trade> tradeList = getBookedTradeListToProcess(clientInvestmentAccountIds, activeOn);
			Map<InvestmentAccount, List<Trade>> clientAccountTradeListMap = BeanUtils.getBeansMap(tradeList, Trade::getClientInvestmentAccount);
			// partition each client account trade list to get the last buy and sell trade for each underlying security
			Map<InvestmentAccount, Map<InvestmentSecurity, Map<Boolean, Trade>>> clientAccountLastBuyTradeMap = new HashMap<>();
			clientAccountTradeListMap.forEach((clientAccount, clientTradeList) -> {
				// Map the trade list by underlying security
				Map<InvestmentSecurity, List<Trade>> underlyingSecurityToTradeListMap = CollectionUtils.getStream(clientTradeList)
						.filter(trade -> trade.getInvestmentSecurity().getUnderlyingSecurity() != null)
						.collect(Collectors.groupingBy(trade -> trade.getInvestmentSecurity().getUnderlyingSecurity()));
				// Determine the last buy and sell trade for each underlying security
				underlyingSecurityToTradeListMap.forEach((underlyingSecurity, underlyingTradeList) -> {
					Map<Boolean, List<Trade>> buyTradeListMap = BeanUtils.getBeansMap(underlyingTradeList, Trade::isBuy);
					Map<Boolean, Trade> openTradeMap = clientAccountLastBuyTradeMap.computeIfAbsent(clientAccount, key -> new HashMap<>())
							.computeIfAbsent(underlyingSecurity, key -> new HashMap<>());
					// The trades should still be in order since they are ordered by trade date from the database.
					// Take the first item from each buy and sell trade list as the latest trade for each
					openTradeMap.put(Boolean.TRUE, CollectionUtils.getFirstElement(buyTradeListMap.get(Boolean.TRUE)));
					openTradeMap.put(Boolean.FALSE, CollectionUtils.getFirstElement(buyTradeListMap.get(Boolean.FALSE)));
				});
			});

			for (Map.Entry<InvestmentAccount, List<InvestmentAccountSecurityTarget>> clientSecurityTargetListEntry : clientSecurityTargetListMap.entrySet()) {
				List<InvestmentAccountSecurityTarget> clientSecurityTargetList = clientSecurityTargetListEntry.getValue();
				status.setMessage(String.format("Processing %d Security Targets for Client Account {%s}", CollectionUtils.getSize(clientSecurityTargetList), clientSecurityTargetListEntry.getKey().getLabel()));
				clientAccountCount++;
				for (InvestmentAccountSecurityTarget clientSecurityTarget : CollectionUtils.getIterable(clientSecurityTargetList)) {
					try {
						securityTargetCount++;
						Map<Boolean, Trade> clientSecurityTargetBuyTradeMap = clientAccountLastBuyTradeMap.getOrDefault(clientSecurityTarget.getClientInvestmentAccount(), Collections.emptyMap())
								.getOrDefault(clientSecurityTarget.getTargetUnderlyingSecurity(), Collections.emptyMap());
						Trade lastBuyTrade = clientSecurityTargetBuyTradeMap.get(Boolean.TRUE);
						Trade lastSellTrade = clientSecurityTargetBuyTradeMap.get(Boolean.FALSE);
						if (updateSecurityTargetForLastBuySellTrade(clientSecurityTarget, lastBuyTrade, lastSellTrade)) {
							updatedSecurityTargetCount++;
						}
					}
					catch (Exception e) {
						status.addError(String.format("Failed to process Security Target for Client Account %s and Underlying Security %s: %s", clientSecurityTarget.getClientInvestmentAccount().getLabel(), clientSecurityTarget.getTargetUnderlyingSecurity(), ExceptionUtils.getDetailedMessage(e)));
						failureCount++;
					}
				}
			}
		}

		status.setMessage(String.format("Update %d of %d Security Targets across %d Client Accounts with %d failures", updatedSecurityTargetCount, securityTargetCount, clientAccountCount, failureCount));
		return status;
	}


	private List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(Date activeOnDate) {
		InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
		securityTargetSearchForm.setInvestmentAccountGroupId(getClientInvestmentAccountGroupId());
		securityTargetSearchForm.setClientInvestmentAccountId(getClientInvestmentAccountId());
		securityTargetSearchForm.setTargetUnderlyingSecurityId(getUnderlyingSecurityId());
		securityTargetSearchForm.setActiveOnDate(activeOnDate);
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(securityTargetSearchForm);
	}


	private List<Trade> getBookedTradeListToProcess(Integer[] investmentAccountIds, Date activeOnDate) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setClientInvestmentAccountIds(investmentAccountIds);
		searchForm.setUnderlyingSecurityId(getUnderlyingSecurityId());
		searchForm.setWorkflowStateName(TradeService.TRADE_BOOKED_STATE_NAME);

		if (getTradeDateDaysBeforeActiveOnDate() != null) {
			Date startDate = (getTradeDateDaysBeforeActiveOnDate() == 0) ? DateUtils.clearTime(Date.from(Instant.EPOCH)) : getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(activeOnDate), -getTradeDateDaysBeforeActiveOnDate());
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		}
		if (getTradeDateDaysAfterActiveOnDate() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(activeOnDate), getTradeDateDaysAfterActiveOnDate())));
		}

		// sorting to have the latest trade first
		searchForm.setOrderBy("clientInvestmentAccountId:asc#tradeDate:desc");

		return getTradeService().getTradeList(searchForm);
	}


	private boolean updateSecurityTargetForLastBuySellTrade(InvestmentAccountSecurityTarget clientSecurityTarget, Trade lastBuyTrade, Trade lastSellTrade) {
		boolean save = false;
		if (lastBuyTrade != null && (clientSecurityTarget.getLastBuyTradeDate() == null || DateUtils.isDateAfter(lastBuyTrade.getTradeDate(), clientSecurityTarget.getLastBuyTradeDate()))) {
			clientSecurityTarget.setLastBuyTradeDate(lastBuyTrade.getTradeDate());
			save = true;
		}
		if (lastSellTrade != null && (clientSecurityTarget.getLastSellTradeDate() == null || DateUtils.isDateAfter(lastSellTrade.getTradeDate(), clientSecurityTarget.getLastSellTradeDate()))) {
			clientSecurityTarget.setLastSellTradeDate(lastSellTrade.getTradeDate());
			save = true;
		}
		if (save) {
			getInvestmentAccountSecurityTargetService().saveInvestmentAccountSecurityTarget(clientSecurityTarget);
		}
		return save;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public Integer getTradeDateDaysBeforeActiveOnDate() {
		return this.tradeDateDaysBeforeActiveOnDate;
	}


	public void setTradeDateDaysBeforeActiveOnDate(Integer tradeDateDaysBeforeActiveOnDate) {
		this.tradeDateDaysBeforeActiveOnDate = tradeDateDaysBeforeActiveOnDate;
	}


	public Integer getTradeDateDaysAfterActiveOnDate() {
		return this.tradeDateDaysAfterActiveOnDate;
	}


	public void setTradeDateDaysAfterActiveOnDate(Integer tradeDateDaysAfterActiveOnDate) {
		this.tradeDateDaysAfterActiveOnDate = tradeDateDaysAfterActiveOnDate;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
