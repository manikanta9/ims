package com.clifton.trade.options.securitytarget.group.util;

import com.clifton.accounting.gl.position.AccountingPositionBalance;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.trade.options.combination.TradeOptionCombinationTypes;
import com.clifton.trade.options.combination.TradeOptionLegTypes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * A utility to aid in the calculation of the starting date of a trading cycle, unexpired contracts, and
 * determination of the number of excess contracts to add a given trade based on the number of excess contracts available and the balance date for the trade.
 *
 * @author davidi
 */
public interface TradeGroupTradeCycleUtilHandler {

	/**
	 * A method used to calculate the starting date of the current trading cycle
	 *
	 * @param balanceDate   a date within the current cycle
	 * @param cycleDuration the duration of the trading cycle in weeks
	 * @return the date representing the start of the current trading cycle.
	 */
	public Date getStartOfCurrentCycleDate(Date balanceDate, int cycleDuration);


	/**
	 * A method to calculate contracts for all unexpired option spreads (excluding those that expire on the trade date).
	 *
	 * @param securityTarget                underlying security target for option spreads
	 * @param accountingPositionBalanceList a list of accounting positions to use to attain the number of unexpired contracts.
	 * @param balanceDate                   date for which the unexpired contract count will be calculated.
	 * @return sum of all contracts for tranches.
	 */
	public BigDecimal getUnexpiredContractCount(InvestmentAccountSecurityTarget securityTarget, List<AccountingPositionBalance> accountingPositionBalanceList, Date balanceDate);


	/**
	 * Calculates  and returns the at most 1 excess contract to be allocated on the balance date based on the current number of excess contracts.
	 *
	 * @param accountSecurityTarget the {@link InvestmentAccountSecurityTarget} for the account.
	 * @param balanceDate           the date for which the calculation should be performed (usually same as trade date).
	 * @param excessContracts       total excess contracts calculated for the trade cycle
	 * @return number of excess contracts to add to current trade for the balanceDate (1 or 0)
	 */
	public BigDecimal calculateExcessContractDistributionForCurrentTrade(InvestmentAccountSecurityTarget accountSecurityTarget, Date balanceDate, BigDecimal excessContracts);


	/**
	 * Returns the tranche count from the InvestmentAccountSecurityTarget.  If the security target does not contain a value, this function will return
	 * the default value from the client account's BusinessServiceProcessingType.
	 */
	public Short getTrancheCount(InvestmentAccountSecurityTarget securityTarget);


	/**
	 * Returns the cycle duration value (in weeks) from the InvestmentAccountSecurityTarget.  If the security target does not contain a value, this function will return
	 * the default value from the client account's BusinessServiceProcessingType.
	 */
	public Short getCycleDurationWeeks(InvestmentAccountSecurityTarget securityTarget);


	/**
	 * Returns the calculated cycle end date for the provided start date. Is calculated by calculating weekdays from the start date for {@link #getCycleDurationWeeks(InvestmentAccountSecurityTarget)} weeks.
	 * The end date should match the same day of the week as the start date. Only weekdays are considered.
	 */
	public Date getEstimatedCycleEndDate(Date startDate, InvestmentAccountSecurityTarget securityTarget);


	/**
	 * Returns the number of days a traded Option's Expiration Date can deviate from a calculated cycle end date.
	 */
	public Short getAllowedOptionExpirationCycleDeviationDays(BusinessServiceProcessingType processingType);


	/**
	 * Returns the {@link TradeOptionLegTypes leg type} for unexpired contract querying for the specified BusinessServiceProcessingType.
	 */
	public TradeOptionLegTypes getUnexpiredContractFilterType(BusinessServiceProcessingType processingType);


	/**
	 * Returns the number of positions per account per tranche generated by the business service''s trading process. For example, OARS trades generate 4 positions for each spread traded.
	 */
	public Short getTranchePositionCount(BusinessServiceProcessingType processingType);


	/**
	 * Returns true if the processingType uses Value at Risk for compliance verification.
	 */
	public boolean isUsesValueAtRisk(BusinessServiceProcessingType processingType);


	/**
	 * Returns the {@link TradeOptionCombinationTypes option combination type} for the specified business service processing type.
	 */
	public TradeOptionCombinationTypes getOptionCombinationType(BusinessServiceProcessingType processingType);


	/**
	 * Looks up and returns the mid-range delta value for an option direction / type combination
	 *
	 * @param processingType  the BusinessServiceProcessingType for which the delta mid range should be looked up
	 * @param optionDeltaName a constant from the Delta value constants defined in BusinessServiceProcessingType (e.g. LONG_PUT_DELTA, SHORT_CALL_DELTA)
	 * @return The mid-range delta value if defined, otherwise null.
	 */
	public BigDecimal getDeltaMidRange(BusinessServiceProcessingType processingType, String optionDeltaName);


	/**
	 * Returns the allowed deviation for a mid-range delta value for the specified BusinessServiceProcessingType.
	 */
	public BigDecimal getAllowedDeltaDeviation(BusinessServiceProcessingType processingType);


	/**
	 * Returns the max allowed option spread percent difference from the underlying price.
	 * <p>
	 * It is configured on the {@link BusinessServiceProcessingType} as a default and can be overridden
	 * on the client account.
	 * <p>
	 * The spread is determined from the difference between the underlying price and the option's strike price.
	 */
	public BigDecimal getMaxAllowedOptionSpreadFromUnderlyingPricePercent(InvestmentAccount clientAccount);
}
