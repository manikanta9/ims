package com.clifton.trade.options.securitytarget.group;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.group.dynamic.TradeGroupDynamic;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * <code>TradeGroupDynamicSecurityTargetOptionSpread</code> is a {@link TradeGroupDynamic} used for OARS and RPS, which relies on
 * client accounts' {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget}s to trade Put and Call Option Spreads
 * in tranches.
 *
 * @author NickK
 */
public class TradeGroupDynamicSecurityTargetOptionSpread extends TradeGroupDynamic<TradeGroupDynamicSecurityTargetOptionSpreadDetail> {

	private InvestmentAccountGroup clientAccountGroup;

	/**
	 * Underlying {@link InvestmentSecurity} to use for finding applicable {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget}s to trade.
	 */
	private InvestmentSecurity optionUnderlyingSecurity;
	/**
	 * Date to use for active {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget}s for the underlying security.
	 */
	private Date balanceDate;

	private InvestmentSecurity longPutSecurity;
	private InvestmentSecurity shortPutSecurity;
	private InvestmentSecurity shortCallSecurity;
	private InvestmentSecurity longCallSecurity;
	/**
	 * Flag to enable checking for expiring positions and there are the same number of positions expiring as those being traded.
	 */
	private boolean validateExpiringPositions;

	private boolean livePrices;

	private BigDecimal longPutCommissionPerUnit;
	private BigDecimal shortPutCommissionPerUnit;
	private BigDecimal shortCallCommissionPerUnit;
	private BigDecimal longCallCommissionPerUnit;

	private BigDecimal longPutFillPrice;
	private BigDecimal shortPutFillPrice;
	private BigDecimal shortCallFillPrice;
	private BigDecimal longCallFillPrice;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getSavePropertiesAsTradeGroupNote() {
		return CollectionUtils.createList("clientAccountGroup.id", "balanceDate", "optionUnderlyingSecurity.id");
	}


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getClientAccountGroup() != null) {
			sb.append(getClientAccountGroup().getName()).append(' ');
		}
		if (getOptionUnderlyingSecurity() != null) {
			sb.append("Targets for ").append(getOptionUnderlyingSecurity().getLabel()).append(' ');
		}
		if (getBalanceDate() != null) {
			sb.append("on ").append(DateUtils.fromDateShort(getBalanceDate()));
		}
		return sb.toString();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroup getClientAccountGroup() {
		return this.clientAccountGroup;
	}


	public void setClientAccountGroup(InvestmentAccountGroup clientAccountGroup) {
		this.clientAccountGroup = clientAccountGroup;
	}


	public InvestmentSecurity getOptionUnderlyingSecurity() {
		return this.optionUnderlyingSecurity;
	}


	public void setOptionUnderlyingSecurity(InvestmentSecurity optionUnderlyingSecurity) {
		this.optionUnderlyingSecurity = optionUnderlyingSecurity;
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public InvestmentSecurity getLongCallSecurity() {
		return this.longCallSecurity;
	}


	public void setLongCallSecurity(InvestmentSecurity longCallSecurity) {
		this.longCallSecurity = longCallSecurity;
	}


	public InvestmentSecurity getShortCallSecurity() {
		return this.shortCallSecurity;
	}


	public void setShortCallSecurity(InvestmentSecurity shortCallSecurity) {
		this.shortCallSecurity = shortCallSecurity;
	}


	public InvestmentSecurity getLongPutSecurity() {
		return this.longPutSecurity;
	}


	public void setLongPutSecurity(InvestmentSecurity longPutSecurity) {
		this.longPutSecurity = longPutSecurity;
	}


	public InvestmentSecurity getShortPutSecurity() {
		return this.shortPutSecurity;
	}


	public void setShortPutSecurity(InvestmentSecurity shortPutSecurity) {
		this.shortPutSecurity = shortPutSecurity;
	}


	public boolean isValidateExpiringPositions() {
		return this.validateExpiringPositions;
	}


	public void setValidateExpiringPositions(boolean validateExpiringPositions) {
		this.validateExpiringPositions = validateExpiringPositions;
	}


	public boolean isLivePrices() {
		return this.livePrices;
	}


	public void setLivePrices(boolean livePrices) {
		this.livePrices = livePrices;
	}


	public BigDecimal getLongPutCommissionPerUnit() {
		return this.longPutCommissionPerUnit;
	}


	public void setLongPutCommissionPerUnit(BigDecimal longPutCommissionPerUnit) {
		this.longPutCommissionPerUnit = longPutCommissionPerUnit;
	}


	public BigDecimal getShortPutCommissionPerUnit() {
		return this.shortPutCommissionPerUnit;
	}


	public void setShortPutCommissionPerUnit(BigDecimal shortPutCommissionPerUnit) {
		this.shortPutCommissionPerUnit = shortPutCommissionPerUnit;
	}


	public BigDecimal getShortCallCommissionPerUnit() {
		return this.shortCallCommissionPerUnit;
	}


	public void setShortCallCommissionPerUnit(BigDecimal shortCallCommissionPerUnit) {
		this.shortCallCommissionPerUnit = shortCallCommissionPerUnit;
	}


	public BigDecimal getLongCallCommissionPerUnit() {
		return this.longCallCommissionPerUnit;
	}


	public void setLongCallCommissionPerUnit(BigDecimal longCallCommissionPerUnit) {
		this.longCallCommissionPerUnit = longCallCommissionPerUnit;
	}


	public BigDecimal getLongPutFillPrice() {
		return this.longPutFillPrice;
	}


	public void setLongPutFillPrice(BigDecimal longPutFillPrice) {
		this.longPutFillPrice = longPutFillPrice;
	}


	public BigDecimal getShortPutFillPrice() {
		return this.shortPutFillPrice;
	}


	public void setShortPutFillPrice(BigDecimal shortPutFillPrice) {
		this.shortPutFillPrice = shortPutFillPrice;
	}


	public BigDecimal getShortCallFillPrice() {
		return this.shortCallFillPrice;
	}


	public void setShortCallFillPrice(BigDecimal shortCallFillPrice) {
		this.shortCallFillPrice = shortCallFillPrice;
	}


	public BigDecimal getLongCallFillPrice() {
		return this.longCallFillPrice;
	}


	public void setLongCallFillPrice(BigDecimal longCallFillPrice) {
		this.longCallFillPrice = longCallFillPrice;
	}
}
