package com.clifton.trade.options;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;

import java.util.List;


/**
 * <code>TradeOptionsStrategy</code> is an interface that defines methods for generating trade groups and trades
 * while processing {@link TradeOptionsRequest}s for expiring Options positions.
 *
 * @author NickK
 */
public interface TradeOptionsStrategy {

	/**
	 * Returns true if this strategy requires 'Big' to 'Mini' security mappings to be populated in order to generate or validate
	 * trades for an {@link TradeOptionsPositionBalance}.
	 */
	public boolean isOptionUnderlyingBigToMiniMapRequired();


	/**
	 * Returns a list of Trades for the provided {@link TradeOptionsRequestDetail}. The returned trades should be used
	 * for validating them with position via {@link #validateTradeList(TradeOptionsContext, TradeOptionsPositionBalance, List)}.
	 *
	 * @param context the context containing the request, trade group type, and trade date
	 * @param detail  the option detail to create trade(s) for
	 */
	public List<Trade> generateTradeList(TradeOptionsContext context, TradeOptionsRequestDetail detail);


	/**
	 * Validates the provided trades against the provided {@link TradeOptionsPositionBalance}. The trades can be updated in place
	 * to adjust properties such as the security, quantity, buy, and tradeOpenCloseType.
	 *
	 * @param context   the context containing the request, trade group type, and trade date
	 * @param position  the option position in the system
	 * @param tradeList the trade(s) to validate
	 */
	public void validateTradeList(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList);


	/**
	 * Returns the security to use as the generated TradeGroup's primary security.
	 */
	public InvestmentSecurity getTradeGroupInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList);


	/**
	 * Returns the security to use as the generated TradeGroup's secondary security.
	 */
	public InvestmentSecurity getTradeGroupSecondaryInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList);
}
