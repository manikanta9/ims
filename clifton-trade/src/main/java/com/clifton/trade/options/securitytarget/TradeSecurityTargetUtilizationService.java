package com.clifton.trade.options.securitytarget;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;
import com.clifton.trade.options.securitytarget.search.TradeSecurityTargetUtilizationCommand;

import java.util.Date;
import java.util.List;


/**
 * <code>TradeSecurityTargetUtilizationService</code> is a service for creating real-time utilization of defined
 * {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget}s. The utilization includes held positions,
 * pending transactions, and frozen quantities.
 *
 * @author NickK
 */
public interface TradeSecurityTargetUtilizationService {

	/**
	 * Returns a list of {@link TradeSecurityTargetUtilization}s applicable to the provided {@link TradeSecurityTargetUtilizationCommand}.
	 * <p>
	 * The command's balance date is required to use for loading necessary data. If a balance date is not provided, the current day is used.
	 */
	@SecureMethod(dtoClass = Trade.class)
	public List<TradeSecurityTargetUtilization> getTradeSecurityTargetUtilizationList(TradeSecurityTargetUtilizationCommand command);


	/**
	 * Returns a {@link TradeSecurityTargetUtilization}s applicable to the provide {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget} ID and balance date.
	 */
	@SecureMethod(dtoClass = Trade.class)
	public TradeSecurityTargetUtilization getTradeSecurityTargetUtilization(Integer investmentAccountSecurityTargetId, Date balanceDate, boolean usePriorPriceDate);


	/**
	 * Returns a list of historic {@link TradeSecurityTargetUtilization}s applicable to the client account ID, underlying security ID, and range dates.
	 *
	 * @param clientInvestmentAccountId the required client {@link com.clifton.investment.account.InvestmentAccount} ID
	 * @param underlyingSecurityId      the optional underlying {@link com.clifton.investment.instrument.InvestmentSecurity} ID, if not provided all applicable security targets will be used
	 * @param rangeStartDate            the required range start date
	 * @param rangeEndDate              the required range end date
	 */
	@SecureMethod(dtoClass = Trade.class)
	public List<TradeSecurityTargetUtilization> getTradeSecurityTargetUtilizationListForDateRange(int clientInvestmentAccountId, Integer underlyingSecurityId, Date rangeStartDate, Date rangeEndDate);
}
