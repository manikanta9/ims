package com.clifton.trade.options.impl;

import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.options.TradeOptionsContext;
import com.clifton.trade.options.TradeOptionsPositionBalance;
import com.clifton.trade.options.TradeOptionsRequestDetail;
import com.clifton.trade.options.TradeOptionsStrategy;
import com.clifton.trade.options.securitytarget.TradeSecurityTargetUtilization;
import com.clifton.trade.options.securitytarget.TradeSecurityTargetUtilizationService;
import com.clifton.trade.options.securitytarget.search.TradeSecurityTargetUtilizationCommand;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * <code>TradeOptionsRollForwardStrategy</code> is a {@link TradeOptionsStrategy} for extending an Option position to a later expiration.
 * Two trades will be generated from a position including: a close of the Option position and an open to a new Option of the same underlying security with a later expiration.
 *
 * @author NickK
 */
public class TradeOptionsRollForwardStrategy extends AbstractTradeOptionsStrategy {

	private TradeSecurityTargetUtilizationService tradeSecurityTargetUtilizationService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<Trade> generateTradeList(TradeOptionsContext context, TradeOptionsRequestDetail detail) {
		ValidationUtils.assertNotNull(detail.getRollSecurity(), "Must provide an Option to roll the position to.");
		ValidationUtils.assertNotNull(ObjectUtils.coalesce(detail.getRollQuantity(), detail.getRollRatio()), "Must provide either a roll quantity or a roll ratio to calculate the roll quantity for the Option position.");
		ValidationUtils.assertFalse(detail.getRollSecurity().equals(detail.getInvestmentSecurity()), "Cannot roll to the same Option of the position being closed.");

		BigDecimal rollQuantity = getRollQuantity(detail, context.getTradeDate());
		if (MathUtils.isNullOrZero(rollQuantity) && MathUtils.isNullOrZero(detail.getTradeQuantity())) {
			return Collections.emptyList();
		}
		ValidationUtils.assertFalse(MathUtils.isNullOrZero(rollQuantity) || MathUtils.isNullOrZero(detail.getTradeQuantity()),
				"Both a trade quantity to close the position and to roll into a new security are required.");

		Trade rollTrade = generateTrade(context, detail, detail.getRollSecurity(), null);
		rollTrade.setQuantityIntended(rollQuantity);

		List<Trade> tradeList = CollectionUtils.createList(
				generateTrade(context, detail, detail.getInvestmentSecurity(), null),
				rollTrade
		);

		if (detail.isTradeUnderlying()) {
			tradeList = addUnderlyingTrade(context, detail, tradeList);
		}

		return tradeList;
	}


	@Override
	public void validateTradeList(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		Map<Boolean, List<Trade>> optionTradeListMap = getOptionTradeListMap(tradeList);
		List<Trade> optionTradeList = optionTradeListMap.get(Boolean.TRUE);
		ValidationUtils.assertTrue(CollectionUtils.getSize(optionTradeList) == 2,
				"Two trades are expected. One for closing the Option position and the other for extending the expiration.");

		optionTradeList.forEach(trade -> {
			if (trade != null) {
				if (trade.getInvestmentSecurity().equals(position.getPositionBalance().getInvestmentSecurity())) {
					// This is the closing trade. Validate the quantity and set the buy and openCloseType properties.
					ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(trade.getQuantityIntended(), position.getTradeQuantityForExpire()),
							"The Trade for closing the Option position must be of the same quantity as the remaining position value [" + position.getTradeQuantityForExpire() + "]. There may be pending trades.");
					boolean buyToClose = MathUtils.isNegative(position.getPositionBalance().getQuantity());
					// trade the inverse of the position to close it
					trade.setBuy(buyToClose);
					trade.setOpenCloseType(context.getTradeOpenCloseTypeFunction().apply(position, trade));
				}
				else {
					// This is the roll trade. Validate the security and quantity and set the buy and openCloseType properties.
					ValidationUtils.assertTrue(CompareUtils.isEqual(position.getPositionBalance().getInvestmentSecurity().getUnderlyingSecurity(), trade.getInvestmentSecurity().getUnderlyingSecurity()),
							"The rolled position must be for an Option with the same underlying security as the closed/expiring position.");
					ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(trade.getInvestmentSecurity().getLastDeliveryDate(), position.getPositionBalance().getInvestmentSecurity().getLastDeliveryDate()),
							"The rolled position must be for an Option with the same or a later expiration date than that of the Option of the closed/expiring position.");

					boolean buyToOpen = MathUtils.isPositive(position.getPositionBalance().getQuantity());
					// roll trade should be the same direction of the position closed
					trade.setBuy(buyToOpen);
					trade.setOpenCloseType(context.getTradeOpenCloseTypeFunction().apply(position, trade, true));
				}
			}
		});

		List<Trade> underlyingTradeList = optionTradeListMap.get(Boolean.FALSE);
		if (!CollectionUtils.isEmpty(underlyingTradeList)) {
			ValidationUtils.assertTrue(underlyingTradeList.size() == 1, "Only one trade for the underlying security is expected.");
			Trade underlyingTrade = CollectionUtils.getFirstElement(underlyingTradeList);
			if (underlyingTrade != null) {
				ValidationUtils.assertEquals(underlyingTrade.getInvestmentSecurity(), optionTradeList.get(0).getInvestmentSecurity().getUnderlyingSecurity(), "The underlying security traded should be the same as the underlying security of the traded Options.");
			}
		}
	}


	@Override
	public InvestmentSecurity getTradeGroupInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		return position.getPositionBalance().getInvestmentSecurity();
	}


	@Override
	public InvestmentSecurity getTradeGroupSecondaryInvestmentSecurity(TradeOptionsContext context, TradeOptionsPositionBalance position, List<Trade> tradeList) {
		Optional<InvestmentSecurity> rollSecurity = tradeList.stream()
				.map(Trade::getInvestmentSecurity)
				.filter(security -> !position.getPositionBalance().getInvestmentSecurity().equals(security))
				.findFirst();
		return rollSecurity.orElse(null);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the roll quantity from the detail. If the client account uses a security target, the roll quantity may be adjusted to avoid
	 * surpassing the available quantity towards the target.
	 */
	private BigDecimal getRollQuantity(TradeOptionsRequestDetail detail, Date balanceDate) {
		// Use the rollQuantity if defined, otherwise calculate it form the ratio
		BigDecimal rollQuantity = detail.getRollQuantity() != null ? detail.getRollQuantity()
				: MathUtils.round(MathUtils.multiply(detail.getTradeQuantity(), detail.getRollRatio()), 0);

		TradeSecurityTargetUtilization underlyingUtilization = getTradeSecurityTargetUtilizationForClientAccount(detail, balanceDate);
		if (underlyingUtilization != null) {
			// If the client account is a service using security targets, determine if the rollQuantity needs to be adjusted according to the security target's available quantity
			BigDecimal availableUnderlyingQuantity = MathUtils.subtract(underlyingUtilization.getUnderlyingSecurityTargetQuantityAdjusted(), underlyingUtilization.getTotalUsageQuantity()).abs();
			// adjust available quantity with the position being closed; the current position to close will be in the covered quantity of the total and can be added back
			BigDecimal closingTradeUnderlyingQuantity = MathUtils.multiply(detail.getInvestmentSecurity().getPriceMultiplier(), detail.getTradeQuantity());
			availableUnderlyingQuantity = MathUtils.add(availableUnderlyingQuantity, closingTradeUnderlyingQuantity);
			BigDecimal rollUnderlyingQuantity = MathUtils.multiply(rollQuantity, detail.getRollSecurity().getPriceMultiplier());
			if (MathUtils.isGreaterThan(rollUnderlyingQuantity, availableUnderlyingQuantity)) {
				if (detail.isRollUsingFrozen()) {
					BigDecimal rollUnderlyingQuantityDifference = MathUtils.subtract(rollUnderlyingQuantity, availableUnderlyingQuantity);
					BigDecimal frozenUnderlyingQuantity = underlyingUtilization.getFrozenQuantity().abs();
					if (MathUtils.isGreaterThan(rollUnderlyingQuantityDifference, frozenUnderlyingQuantity)) {
						// adjust the roll quantity to the value of available and frozen
						rollUnderlyingQuantity = MathUtils.add(availableUnderlyingQuantity, frozenUnderlyingQuantity);
					}
					// else use rollUnderlyingQuantity as is because the frozen quantity is sufficient
				}
				else {
					rollUnderlyingQuantity = availableUnderlyingQuantity;
				}
			}
			rollQuantity = MathUtils.round(MathUtils.divide(rollUnderlyingQuantity, detail.getRollSecurity().getPriceMultiplier()), 0, RoundingMode.FLOOR);
		}
		return rollQuantity;
	}


	/**
	 * Looks up and returns a {@link TradeSecurityTargetUtilization} for the client account and traded security's underlying, on the balance date.
	 * If the client is not using a {@link BusinessService} of {@link ServiceProcessingTypes#SECURITY_TARGET}, null is returned.
	 *
	 * @throws ValidationException if 0 or more than one security target utilization is returned
	 */
	private TradeSecurityTargetUtilization getTradeSecurityTargetUtilizationForClientAccount(TradeOptionsRequestDetail detail, Date balanceDate) {
		BusinessServiceProcessingType clientServiceProcessingType = detail.getClientInvestmentAccount().getServiceProcessingType();
		if (clientServiceProcessingType == null || clientServiceProcessingType.getProcessingType() != ServiceProcessingTypes.SECURITY_TARGET) {
			return null;
		}

		TradeSecurityTargetUtilizationCommand utilizationCommand = new TradeSecurityTargetUtilizationCommand();
		utilizationCommand.setClientInvestmentAccountId(detail.getClientInvestmentAccount().getId());
		utilizationCommand.setTargetUnderlyingSecurityId(detail.getRollSecurity().getUnderlyingSecurity().getId());
		utilizationCommand.setBalanceDate(balanceDate);

		List<TradeSecurityTargetUtilization> utilizationList = getTradeSecurityTargetUtilizationService().getTradeSecurityTargetUtilizationList(utilizationCommand);
		ValidationUtils.assertTrue(CollectionUtils.getSize(utilizationList) == 1, String.format("One and only one Security Target is required for Client Account %s on %tD", detail.getClientInvestmentAccount(), balanceDate));
		return utilizationList.get(0);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeSecurityTargetUtilizationService getTradeSecurityTargetUtilizationService() {
		return this.tradeSecurityTargetUtilizationService;
	}


	public void setTradeSecurityTargetUtilizationService(TradeSecurityTargetUtilizationService tradeSecurityTargetUtilizationService) {
		this.tradeSecurityTargetUtilizationService = tradeSecurityTargetUtilizationService;
	}
}

