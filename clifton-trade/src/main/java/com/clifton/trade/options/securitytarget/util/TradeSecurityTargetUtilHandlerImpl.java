package com.clifton.trade.options.securitytarget.util;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author NickK
 */
@Component
public class TradeSecurityTargetUtilHandlerImpl implements TradeSecurityTargetUtilHandler {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private MarketDataRetriever marketDataRetriever;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public TradeSecurityTarget getTradeSecurityTargetForDate(InvestmentAccountSecurityTarget securityTarget, Date date) {
		return getTradeSecurityTargetWithUnderlyingSecurityPriceForDate(securityTarget, date, null);
	}


	@Override
	public TradeSecurityTarget getTradeSecurityTargetForDateWithPriceDate(InvestmentAccountSecurityTarget securityTarget, Date date, Date priceDate) {
		if (securityTarget == null) {
			return null;
		}
		MarketDataValueHolder priceDateValueHolder = getUnderlyingSecurityPriceObjectForDate(securityTarget, priceDate);
		if (!DateUtils.isDateBetween(priceDate, securityTarget.getStartDate(), securityTarget.getEndDate(), false)) {
			MarketDataValueHolder dateValueHolder = getUnderlyingSecurityPriceObjectForDate(securityTarget, date);
			BigDecimal underlyingPrice = priceDateValueHolder.getMeasureValue();
			if (!CompareUtils.isEqual(dateValueHolder.getMeasureValueAdjustmentFactor(), priceDateValueHolder.getMeasureValueAdjustmentFactor())) {
				underlyingPrice = MathUtils.multiply(underlyingPrice, MathUtils.divide(priceDateValueHolder.getMeasureValueAdjustmentFactor(), dateValueHolder.getMeasureValueAdjustmentFactor()));
			}
			return getTradeSecurityTargetWithUnderlyingSecurityPriceForDate(securityTarget, date, underlyingPrice);
		}
		return getTradeSecurityTargetWithUnderlyingSecurityPriceForDate(securityTarget, date, priceDateValueHolder.getMeasureValue());
	}


	@Override
	public TradeSecurityTarget getTradeSecurityTargetWithUnderlyingSecurityPriceForDate(InvestmentAccountSecurityTarget securityTarget, Date date, BigDecimal underlyingSecurityPrice) {
		if (securityTarget == null) {
			return null;
		}
		TradeSecurityTarget tradeSecurityTarget = new TradeSecurityTarget(securityTarget);
		BigDecimal underlyingSecurityPriceSafe = underlyingSecurityPrice;
		if (underlyingSecurityPriceSafe == null) {
			underlyingSecurityPriceSafe = getUnderlyingSecurityPriceObjectForDate(securityTarget, date).getMeasureValue();
		}
		tradeSecurityTarget.setUnderlyingSecurityPrice(underlyingSecurityPriceSafe);

		if (securityTarget.getTargetUnderlyingQuantity() != null) {
			tradeSecurityTarget.setTargetUnderlyingQuantity(securityTarget.getTargetUnderlyingQuantity());
			tradeSecurityTarget.setTargetUnderlyingNotional(MathUtils.multiply(underlyingSecurityPriceSafe, securityTarget.getTargetUnderlyingQuantity()));
		}
		else {
			BigDecimal targetNotional;
			if (securityTarget.getTargetUnderlyingNotional() != null) {
				targetNotional = securityTarget.getTargetUnderlyingNotional();
			}
			else {
				// Notional is from the manager account balance
				InvestmentManagerAccountBalance balance = getManagerBalanceForDate(securityTarget, date);
				targetNotional = balance.getAdjustedTotalValue();
			}
			tradeSecurityTarget.setTargetUnderlyingNotional(targetNotional);
			tradeSecurityTarget.setTargetUnderlyingQuantity(MathUtils.divide(targetNotional, underlyingSecurityPriceSafe));
		}
		return tradeSecurityTarget;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataValueHolder getUnderlyingSecurityPriceObjectForDate(InvestmentAccountSecurityTarget securityTarget, Date date) {
		// Use flexible look-ups for current and future dates since current-day data (closing price) is typically not yet available
		boolean currentOrFutureDate = DateUtils.isDateAfterOrEqual(date, DateUtils.clearTime(new Date()));
		// If security starts after price look up date - it's ok if it's missing just return 0
		boolean exceptionIfMissing = (DateUtils.isDateBeforeOrEqual(securityTarget.getTargetUnderlyingSecurity().getStartDate(), date, false));
		MarketDataValueHolder valueHolder = getMarketDataRetriever().getPriceMarketDataValueHolder(securityTarget.getTargetUnderlyingSecurity(), date, currentOrFutureDate, exceptionIfMissing, null);
		if (valueHolder == null) {
			// If we get here it's OK that the value is NULL will return a 0 price
			valueHolder = new MarketDataValueHolder(null);
			valueHolder.setMeasureDate(date);
			valueHolder.setMeasureValueAdjusted(BigDecimal.ZERO);
			valueHolder.setMeasureValue(BigDecimal.ZERO);
			valueHolder.setMeasureValueAdjustmentFactor(BigDecimal.ONE);
		}
		return valueHolder;
	}


	private InvestmentManagerAccountBalance getManagerBalanceForDate(InvestmentAccountSecurityTarget securityTarget, Date date) {
		InvestmentManagerAccount managerAccount = securityTarget.getTargetUnderlyingManagerAccount();
		ValidationUtils.assertNotNull(managerAccount, () -> String.format("The Security Target %s does not have a target Quantity, Notional, or Manager Account defined.", securityTarget));
		ValidationUtils.assertFalse(managerAccount.isInactive(), () -> String.format("The Manager Account %s is inactive.", securityTarget.getTargetUnderlyingManagerAccount()));
		InvestmentManagerAccountBalance balance = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceByManagerAndDateFlexible(managerAccount.getId(), date, false);

		// If balance exists and matches date - return it
		if (balance != null && DateUtils.isEqualWithoutTime(date, balance.getBalanceDate())) {
			return balance;
		}

		if (DateUtils.isDateAfterOrEqual(date, DateUtils.clearTime(new Date()))) {
			// Retrieval is flexible for current and future dates; accept flexible balance date
			ValidationUtils.assertNotNull(balance, () -> String.format("Unable to find a manager balance for manager %s on or before [%tD] for client account %s.", managerAccount, date, securityTarget.getClientInvestmentAccount()));
		}
		else {
			// Validate balance date on expected date
			Date expectedBalanceDate = getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(date)) ? date : getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(date), -1);
			ValidationUtils.assertTrue(balance != null && DateUtils.isEqualWithoutTime(balance.getBalanceDate(), expectedBalanceDate), () -> String.format("Unable to find a manager balance for manager %s on business day [%tD] for client account %s.", managerAccount, expectedBalanceDate, securityTarget.getClientInvestmentAccount()));
		}
		return balance;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
