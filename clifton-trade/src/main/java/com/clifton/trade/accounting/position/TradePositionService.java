package com.clifton.trade.accounting.position;


import com.clifton.accounting.AccountingBean;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The TradePositionService interface defines various calculations on trade positions that are generally used by Accounting.
 */
public interface TradePositionService {

	/**
	 * Gets total trades for Client/Holding/Security pending & actual not including this trade
	 */
	public BigDecimal getSharesBeforeTrade(AccountingBean bean, boolean useSettlementDate);


	/**
	 * Gets total trades for Client/Holding/Security pending & actual including this trade
	 * <p>
	 * if sharesBeforeTrade is null, will recalculate it
	 */
	public BigDecimal getSharesAfterTrade(AccountingBean bean, boolean useSettlementDate, BigDecimal sharesBeforeTrade);


	/**
	 * Gets total accounting notional for Client/Holding/Security pending & actual not including this trade
	 */
	public BigDecimal getAccountingNotionalBeforeTrade(AccountingBean bean, boolean useSettlementDate);


	/**
	 * Gets total accounting notional for Client/Holding/Security pending & actual not including this trade
	 * <p>
	 * if accountingNotionalBeforeTrade is null, will recalculate it
	 */
	public BigDecimal getAccountingNotionalAfterTrade(AccountingBean bean, boolean useSettlementDate, BigDecimal accountingNotionalBeforeTrade);


	@SecureMethod(dtoClass = Trade.class)
	public BigDecimal getTradeNetPaymentAmount(int tradeId);


	/**
	 * Returns Accounting Notional in local currency that would be closed by the specified trade.
	 * In order to do this, we need to go through the booking process to identify lots being closed.
	 * The method uses "preview" functionality without actually booking the trade.
	 * <p>
	 * Note: this can be used to calculate Accrued Interest for TRS early termination trades.
	 */
	public BigDecimal getTradeAccountingNotionalBeingClosed(Trade trade);


	/**
	 * Returns accrued interest (sum of accrual 1 and 2) in local currency calculated for the specified trade.
	 * Different types of securities (CDS, IRS, TRS, Bonds) use different logic to calculate accrued interest.
	 */
	public BigDecimal getTradeAccrualAmount(Trade trade);


	/**
	 * Returns accrual amount only for the first accrual. If second accrual is applicable, it will be excluded.
	 */
	public BigDecimal getTradeAccrualAmount1(Trade trade);


	/**
	 * Returns accrual amount only for the second accrual. First accrual is always excluded.
	 */
	public BigDecimal getTradeAccrualAmount2(Trade trade);


	/**
	 * Returns accrued interest in local currency calculated for the specified trade.
	 * Use this method if you need to separate accruals for each leg (only applies to IRS).
	 * The method will throw an exception if both includeEvent1 and includeEvent 2 are false.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = Trade.class)
	public BigDecimal getTradeAccrualAmountAdvanced(Trade trade, boolean includeEvent1, boolean includeEvent2);


	/**
	 * Returns the date when accrual should end for accrued interest calculation for the specified trade.
	 * The interest is included on this date (End Of Day convention).
	 */
	public Date getTradeAccrualUpToDate(Trade trade);
}
