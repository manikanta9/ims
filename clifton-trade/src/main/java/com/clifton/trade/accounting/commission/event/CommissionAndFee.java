package com.clifton.trade.accounting.commission.event;

import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The CommissionAndFee class holds commission and fee fields and is used to sum them across multiple journal details and trade fills.
 *
 * @author vgomelsky
 */
public class CommissionAndFee {

	private BigDecimal feeAmount;
	private BigDecimal commissionAmount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void add(CommissionAndFee commissionAndFee) {
		this.feeAmount = MathUtils.add(this.feeAmount, commissionAndFee.getFeeAmount());
		this.commissionAmount = MathUtils.add(this.commissionAmount, commissionAndFee.getCommissionAmount());
	}


	public void addCommissionOrFee(AccountingJournalDetailDefinition detail) {
		if (detail.getAccountingAccount().isFee()) {
			this.feeAmount = MathUtils.subtract(this.feeAmount, detail.getLocalDebitCredit()); // need to negate expenses
		}
		else {
			this.commissionAmount = MathUtils.subtract(this.commissionAmount, detail.getLocalDebitCredit()); // need to negate expenses
		}
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}
}
