package com.clifton.trade.accounting.booking;


import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;

import java.util.Date;


/**
 * The <code>TradeBookingCommand</code> class allows to define filters that will limit the trades
 * that should be booked and posted to the General Ledger.
 * <p/>
 * The command can also specify how to execute bookings: delayBetweenBookingsInMilliseconds, bookingCountBeforeDelay, etc.
 *
 * @author vgomelsky
 */
public class TradeBookingCommand {

	private Integer[] tradeIds;

	private Integer securityId;
	private Short investmentGroupId;

	private Integer clientInvestmentAccountId;
	private Integer clientInvestmentAccountGroupId;

	/**
	 * Optionally limits trades to a particular workflow state.  By default, the following workflow states will be included:
	 * TradeService.TRADE_EXECUTED_STATE_NAME, TradeService.TRADE_UNBOOKED_STATE_NAME
	 * However, one may want to restrict to Executed state only in order to skip unbooked trades.
	 */
	private String workflowStateName;

	private Date fromTradeDate;
	private Date toTradeDate;

	private Boolean reconciled;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * Number of milliseconds to wait before booking next batch of trades.  Zero means never wait.
	 * Use this to spread the load on the system when a lot of trades are booked together.
	 * Delays will allow other system processes to run without being effected by this job.
	 */
	private long delayBetweenBookingsInMilliseconds = 0;
	/**
	 * Number of trades to book before next delay.
	 */
	private int bookingCountBeforeDelay = 1;

	////////////////////////////////////////////////////////////////////////////

	// allows to send "live" updates of booking status back to the caller (assuming the caller sets this field)
	private StatusHolderObject<Status> statusHolder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public Integer[] getTradeIds() {
		return this.tradeIds;
	}


	public void setTradeIds(Integer[] tradeIds) {
		this.tradeIds = tradeIds;
	}


	public long getDelayBetweenBookingsInMilliseconds() {
		return this.delayBetweenBookingsInMilliseconds;
	}


	public void setDelayBetweenBookingsInMilliseconds(long delayBetweenBookingsInMilliseconds) {
		this.delayBetweenBookingsInMilliseconds = delayBetweenBookingsInMilliseconds;
	}


	public int getBookingCountBeforeDelay() {
		return this.bookingCountBeforeDelay;
	}


	public void setBookingCountBeforeDelay(int bookingCountBeforeDelay) {
		this.bookingCountBeforeDelay = bookingCountBeforeDelay;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public Date getFromTradeDate() {
		return this.fromTradeDate;
	}


	public void setFromTradeDate(Date fromTradeDate) {
		this.fromTradeDate = fromTradeDate;
	}


	public Date getToTradeDate() {
		return this.toTradeDate;
	}


	public void setToTradeDate(Date toTradeDate) {
		this.toTradeDate = toTradeDate;
	}


	public StatusHolderObject<Status> getStatusHolder() {
		return this.statusHolder;
	}


	public void setStatusHolder(StatusHolderObject<Status> statusHolder) {
		this.statusHolder = statusHolder;
	}
}
