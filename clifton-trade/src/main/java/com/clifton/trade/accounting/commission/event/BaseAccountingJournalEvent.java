package com.clifton.trade.accounting.commission.event;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.Event;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.core.util.MathUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * The BaseAccountingJournalEvent class is a helper class that contains common methods used by Posting/Unposting event listeners.
 *
 * @author vgomelsky
 */
public abstract class BaseAccountingJournalEvent<E extends Event<?, ?>> extends BaseEventListener<E> {

	private UpdatableDAO<Trade> tradeDAO; // need this here in order to by-pass validation

	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected Map<Integer, CommissionAndFee> getFillToCommissionMap(AccountingJournal journal) {
		// skip trade journals: updates are covered by booking workflow action
		Map<Integer, CommissionAndFee> fillToCommissionMap = new HashMap<>();
		if (journal != null && !journal.getJournalType().getName().equals(AccountingJournalType.TRADE_JOURNAL)) {
			for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(journal.getJournalDetailList())) {
				// only look at commissions or fees that adjust existing trade
				if (detail.getAccountingAccount().isCommission() && !MathUtils.isNullOrZero(detail.getLocalDebitCredit())) {
					AccountingTransaction parent = detail.getParentTransaction();
					if (parent != null && TradeFill.TABLE_NAME.equals(parent.getSystemTable().getName())) {
						CommissionAndFee commissionAndFee = fillToCommissionMap.computeIfAbsent(parent.getFkFieldId(), k -> new CommissionAndFee());
						commissionAndFee.addCommissionOrFee(detail);
					}
				}
			}
		}
		return fillToCommissionMap;
	}


	protected Map<Trade, CommissionAndFee> getTradeToCommissionMap(Map<Integer, CommissionAndFee> fillToCommissionMap) {
		Map<Trade, CommissionAndFee> tradeToCommissionMap = new HashMap<>();

		// get trades and group fills by trades
		fillToCommissionMap.forEach((fillId, commissionAndFee) -> {
			Trade trade = getTradeService().getTradeFill(fillId).getTrade();
			if (tradeToCommissionMap.containsKey(trade)) {
				tradeToCommissionMap.get(trade).add(commissionAndFee);
			}
			else {
				tradeToCommissionMap.put(trade, commissionAndFee);
			}
		});

		return tradeToCommissionMap;
	}


	protected void updateTrades(Map<Trade, CommissionAndFee> tradeCommissionMap, boolean add) {
		tradeCommissionMap.forEach((trade, commissionAndFee) -> {
			if (add) {
				trade.setFeeAmount(MathUtils.add(trade.getFeeAmount(), commissionAndFee.getFeeAmount()));
				trade.setCommissionAmount(MathUtils.add(trade.getCommissionAmount(), commissionAndFee.getCommissionAmount()));
			}
			else {
				trade.setFeeAmount(MathUtils.subtract(trade.getFeeAmount(), commissionAndFee.getFeeAmount()));
				trade.setCommissionAmount(MathUtils.subtract(trade.getCommissionAmount(), commissionAndFee.getCommissionAmount()));
			}
			if (!MathUtils.isNullOrZero(trade.getQuantityIntended())) {
				// negate because our convention is to have negative total commission and positive commission per unit when the client pays commission (almost always the case)
				trade.setCommissionPerUnit(MathUtils.divide(trade.getCommissionAmount().negate(), trade.getQuantityIntended(), DataTypes.DECIMAL_PRECISE.getPrecision()));
			}

			DaoUtils.executeWithAllObserversDisabled(() -> getTradeDAO().save(trade));
		});
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public UpdatableDAO<Trade> getTradeDAO() {
		return this.tradeDAO;
	}


	public void setTradeDAO(UpdatableDAO<Trade> tradeDAO) {
		this.tradeDAO = tradeDAO;
	}
}
