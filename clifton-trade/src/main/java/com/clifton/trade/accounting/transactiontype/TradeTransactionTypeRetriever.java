package com.clifton.trade.accounting.transactiontype;


import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeRetriever;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>TradeTransactionTypeRetriever</code> handles the retrieval of {@link AccountingTransactionType}
 * based on the source entity ID for journal entries that are trade journals.
 *
 * @author jgommels
 */
@Component
public class TradeTransactionTypeRetriever implements AccountingTransactionTypeRetriever<TradeFill> {

	private TradeService tradeService;
	private AccountingTransactionTypeService accountingTransactionTypeService;


	@Override
	public AccountingTransactionType getAccountingTransactionType(AccountingJournalDetailDefinition journalDetail, TradeFill fill) {
		Date tradeDate = fill.getTransactionDate();
		Date securityEndDate = fill.getInvestmentSecurity().getEndDate();

		if (fill.getTrade().isBlockTrade()) {
			return getTypeByName(AccountingTransactionType.BLOCK_TRADE);
		}

		//If the trade date is the same date as the security end date, and the notional unit price is 0, then this is a Maturity
		if (DateUtils.compare(tradeDate, securityEndDate, false) == 0 && MathUtils.isEqual(fill.getNotionalUnitPrice(), BigDecimal.ZERO)) {
			return getTypeByName(AccountingTransactionType.MATURITY);
		}

		TradeGroup tradeGroup = fill.getTrade().getTradeGroup();
		if (tradeGroup != null) {
			switch (tradeGroup.getTradeGroupType().getGroupType()) {
				//If the TradeGroupType is equal to OPTION_EXPIRATION, then this is a Maturity.
				case OPTION_EXPIRATION: {
					if (MathUtils.isGreaterThan(fill.getTrade().getAverageUnitPrice(), BigDecimal.ZERO)) {
						// Cash settled Option expiring In the Money
						return getTypeByName(AccountingTransactionType.ASSIGNMENT);
					}
					if (fill.getInvestmentSecurity().getInstrument().isDeliverable()) {
						// look for a matching delivery offset trade to determine if the expiration is an assignment
						// the delivery offset group will have the originating option security as teh secondary security
						TradeSearchForm tradeSearchForm = new TradeSearchForm();
						tradeSearchForm.setIncludeTradeGroupTypeName(TradeGroupType.GroupTypes.OPTION_DELIVERY_OFFSET.getTypeName());
						tradeSearchForm.setClientInvestmentAccountId(fill.getClientInvestmentAccount().getId());
						tradeSearchForm.setHoldingInvestmentAccountId(fill.getHoldingInvestmentAccount().getId());
						tradeSearchForm.setTradeDate(fill.getTrade().getTradeDate());
						List<Trade> potentialMatchingDeliveryOffsetTradeList = getTradeService().getTradeList(tradeSearchForm);
						potentialMatchingDeliveryOffsetTradeList = BeanUtils.filter(potentialMatchingDeliveryOffsetTradeList, trade -> fill.getInvestmentSecurity().equals(trade.getTradeGroup().getSecondaryInvestmentSecurity()));
						if (CollectionUtils.getSize(potentialMatchingDeliveryOffsetTradeList) == 1) {
							return getTypeByName(AccountingTransactionType.ASSIGNMENT);
						}
					}
					return getTypeByName(AccountingTransactionType.MATURITY);
				}
				case OPTION_PHYSICAL_DELIVERY: {
					return getTypeByName(AccountingTransactionType.DELIVERY); // should fall back on TRADE (future schedule)
				}
				case OPTION_ASSIGNMENT: {
					if (InvestmentUtils.isOption(fill.getInvestmentSecurity())) {
						return getTypeByName(AccountingTransactionType.ASSIGNMENT);
					}
					else {
						return getTypeByName(AccountingTransactionType.DELIVERY);
					}
				}
				//If the TradeGroupType is equal to EFP, then this is an Exchange For Physical.
				case EFP:
					return getTypeByName(AccountingTransactionType.EXCHANGE_FOR_PHYSICAL);
			}
		}

		//If the hierarchy is OTC and the position is being closed and the trade date is earlier than the security end date, then this is an early termination
		InvestmentInstrumentHierarchy hierarchy = fill.getInvestmentSecurity().getInstrument().getHierarchy();
		if (hierarchy.isOtc() && journalDetail.isClosing() && DateUtils.getDaysDifference(tradeDate, securityEndDate) < 0) {
			return getTypeByName(AccountingTransactionType.EARLY_TERMINATION);
		}

		return getTypeByName(AccountingTransactionType.TRADE); // default
	}


	/**
	 * Gets the TransactionType by name
	 */
	private AccountingTransactionType getTypeByName(String typeName) {
		return getAccountingTransactionTypeService().getAccountingTransactionTypeByName(typeName);
	}


	@Override
	public String getJournalTypeName() {
		return AccountingJournalType.TRADE_JOURNAL;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AccountingTransactionTypeService getAccountingTransactionTypeService() {
		return this.accountingTransactionTypeService;
	}


	public void setAccountingTransactionTypeService(AccountingTransactionTypeService accountingTransactionTypeService) {
		this.accountingTransactionTypeService = accountingTransactionTypeService;
	}
}
