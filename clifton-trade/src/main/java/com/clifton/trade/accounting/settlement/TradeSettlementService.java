package com.clifton.trade.accounting.settlement;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.trade.Trade;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


public interface TradeSettlementService {

	@RequestMapping("tradeFinalSettlement")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public Status tradeFinalSettlement(Integer[] tradeIds);


	@SecureMethod(dtoClass = Trade.class)
	public Status saveTradeActualSettlementDate(Integer[] tradeIds, Date actualSettlementDate);
}
