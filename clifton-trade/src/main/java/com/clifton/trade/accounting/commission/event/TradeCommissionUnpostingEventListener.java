package com.clifton.trade.accounting.commission.event;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.event.AccountingJournalUnpostingEvent;
import com.clifton.trade.Trade;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * The TradeCommissionUnpostingEventListener class listens to GL Unpost events and if Commission or Fee GL Accounts
 * are unposted that are linked to a trade from a different journal, updates trade's commission and fee fields to reflect this change.
 *
 * @author vgomelsky
 */
@Component
public class TradeCommissionUnpostingEventListener extends BaseAccountingJournalEvent<AccountingJournalUnpostingEvent> {

	@Override
	public String getEventName() {
		return AccountingJournalUnpostingEvent.EVENT_NAME;
	}


	@Override
	public void onEvent(AccountingJournalUnpostingEvent event) {
		AccountingJournal journal = event.getTarget();

		Map<Integer, CommissionAndFee> fillToCommissionMap = getFillToCommissionMap(journal);
		Map<Trade, CommissionAndFee> tradeCommissionMap = getTradeToCommissionMap(fillToCommissionMap);

		// subtract when unposting
		updateTrades(tradeCommissionMap, false);
	}
}
