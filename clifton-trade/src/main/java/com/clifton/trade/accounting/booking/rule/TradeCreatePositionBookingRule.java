package com.clifton.trade.accounting.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.accounting.booking.TradeBookingUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;


/**
 * The <code>TradeCreatePositionBookingRule</code> adds a "Position" journal detail for each trade fill.
 * At this time, all positions are considered to be opens. Another rule (splitter) will reconcile these positions
 * against existing positions and will change to a full or partial close if necessary.
 * <p>
 * Asset backed securities (ABS) that are open at reduced face (trade factor is less than 1) will result in 2 position
 * entries: one that opens position at Original Face and one that immediately closes a portion of this position to Current Face.
 * <p>
 * This rule also adds a Cash or Currency payment/proceeds journal detail for each trade fill.
 * Currency record is used vs Cash if security and client account have different currency denominations.
 *
 * @author vgomelsky
 */
public class TradeCreatePositionBookingRule extends AccountingCommonBookingRule<TradeFill> implements AccountingBookingRule<TradeFill> {

	private InvestmentCalculator investmentCalculator;
	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<TradeFill> session) {
		AccountingJournal journal = session.getJournal();
		TradeFill fill = session.getBookableEntity();
		Trade trade = fill.getTrade();

		AccountingJournalDetail positionRecord;
		if (!MathUtils.isEqual(trade.getOriginalFace(), trade.getQuantityIntended()) && trade.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null) {
			// Asset backed bonds that are not open at Original Face generate 2 records: open at Original Face and reduce it to Current Face
			// other securities may have factor changes too: Credit Default Swaps
			positionRecord = createPositionOpenUsingFactorOfOne(fill);
			AccountingJournalDetail reductionToCurrentFaceRecord = createPositionReductionToCurrentFactor(fill);
			reductionToCurrentFaceRecord.setParentDefinition(positionRecord);

			journal.addJournalDetail(positionRecord);
			journal.addJournalDetail(createCashCurrencyRecord(positionRecord, getSettlementCurrency(trade)));

			journal.addJournalDetail(reductionToCurrentFaceRecord);
			journal.addJournalDetail(createCashCurrencyRecord(reductionToCurrentFaceRecord, getSettlementCurrency(trade)));
		}
		else {
			positionRecord = createPositionRecord(fill);

			journal.addJournalDetail(positionRecord);
			journal.addJournalDetail(createCashCurrencyRecord(positionRecord, getSettlementCurrency(trade)));
		}

		if (fill.isLotSpecificClosing()) {
			loadSpecificLotClosingPositions(fill, positionRecord, session);
		}
	}


	private void loadSpecificLotClosingPositions(TradeFill fill, AccountingJournalDetail positionRecord, BookingSession<TradeFill> session) {
		Trade trade = fill.getTrade();
		ValidationUtils.assertFalse(InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.CURRENCY), "Specific lot closings are not supported by currency trades.");
		ValidationUtils.assertFalse(trade.getInvestmentSecurity().getInstrument().getHierarchy().isCloseOnMaturityOnly(), "Specific lot closings are not supported for securities that are closed on maturity only [" + trade.getInvestmentSecurity().getLabel() + "].");

		// get any positions that were opened at the openingPrice on the openingDate
		List<BookingPosition> positionList = getSpecificLotClosingPositions(fill, positionRecord, session);


		BigDecimal positionTotalQuantity = CoreMathUtils.sumProperty(positionList, position -> position.getPosition().getRemainingQuantity());
		// there are enough open lot to book the fill
		ValidationUtils.assertTrue(MathUtils.isGreaterThanOrEqual(MathUtils.abs(positionTotalQuantity), fill.getQuantity()), new StringBuilder("Cannot book trade fill [").append(fill.getId()).append("] because only [")
				.append(positionTotalQuantity).append("] of [").append(fill.getTrade().getInvestmentSecurity().getLabel())
				.append("] were opened on [").append(DateUtils.fromDate(fill.getOpeningDate(), DateUtils.DATE_FORMAT_INPUT)).append("] at [").append(fill.getOpeningPrice()).append("].").toString());

		// the open lots are in the opposite direction
		ValidationUtils.assertTrue(!MathUtils.isSameSignum(positionTotalQuantity, fill.getTrade().isBuy() ? fill.getQuantity() : fill.getQuantity().negate()), new StringBuilder("Cannot close lots opened on [").append(DateUtils.fromDate(fill.getOpeningDate(), DateUtils.DATE_FORMAT_INPUT)).append("] at [")
				.append(fill.getOpeningPrice()).append("] because they are in the same direction as the current trade.").toString());

		// add the positions to the booking session
		replaceCurrentOpenPositionListToBookingSession(session, positionList, positionRecord);
	}


	/**
	 * Get the positions that were opened on the fill open date and the fill open price.
	 */
	private List<BookingPosition> getSpecificLotClosingPositions(TradeFill fill, AccountingJournalDetail positionRecord, BookingSession<TradeFill> session) {
		List<BookingPosition> bookingPositionList = getCurrentOpenPositions(session, positionRecord);

		List<BookingPosition> result = BeanUtils.filter(bookingPositionList, bookingPosition -> DateUtils.isEqualWithoutTime(fill.getOpeningDate(), bookingPosition.getPosition().getOriginalTransactionDate()));
		if ((result.size() > 1) && BeanUtils.getBeansMap(bookingPositionList, bookingPosition -> bookingPosition.getPosition().getPrice()).size() > 1) {
			result = BeanUtils.filter(bookingPositionList, bookingPosition -> MathUtils.isEqual(fill.getOpeningPrice(), bookingPosition.getPosition().getPrice()));
		}
		return result;
	}


	private AccountingJournalDetail createPositionRecord(TradeFill fill) {
		AccountingJournalDetail result = populateCommonFieldsFromFill(fill);

		Trade trade = fill.getTrade();
		result.setQuantity(trade.isBuy() ? fill.getQuantity() : fill.getQuantity().negate());
		result.setLocalDebitCredit(TradeBookingUtils.normalizeAmountSign(trade, fill.getPaymentTotalPrice()));
		result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), result.getExchangeRateToBase(), result.getClientInvestmentAccount()));
		result.setPositionCostBasis(TradeBookingUtils.normalizeAmountSign(trade, fill.getNotionalTotalPrice()));
		result.setOpening(true); // splitter may change this to closing (buy can be a closing when going short)

		// validate notional calculation
		InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(trade.getInvestmentSecurity().getInstrument().getTradingCurrency());
		BigDecimal notionalMultiplier = getInvestmentCalculator().getNotionalMultiplier(result.getInvestmentSecurity(), result.getTransactionDate(), result.getSettlementDate(), false);
		BigDecimal calculatedCostBasis = getInvestmentCalculator().calculateNotional(result.getInvestmentSecurity(), result.getPrice(), result.getQuantity(), notionalMultiplier, roundingCalculator);
		if (InvestmentCalculatorUtils.getNotionalCalculator(trade.getInvestmentSecurity()).isNegativeQuantity()) {
			calculatedCostBasis = MathUtils.negate(calculatedCostBasis);
		}

		// allow manual correction for penny rounding
		BigDecimal diff = MathUtils.subtract(calculatedCostBasis, result.getPositionCostBasis()).abs();
		ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(diff, BigDecimal.valueOf(0.01)), "Cost Basis calculated during booking " + CoreMathUtils.formatNumberMoney(calculatedCostBasis)
				+ " must equal Cost Basis of the fill " + CoreMathUtils.formatNumberMoney(result.getPositionCostBasis()));

		return result;
	}


	private AccountingJournalDetail createPositionOpenUsingFactorOfOne(TradeFill fill) {
		AccountingJournalDetail result = populateCommonFieldsFromFill(fill);

		Trade trade = fill.getTrade();
		result.setQuantity(trade.isBuy() ? trade.getOriginalFace() : trade.getOriginalFace().negate());
		result.setLocalDebitCredit(TradeBookingUtils.normalizeAmountSign(trade,
				getInvestmentCalculator().calculateNotional(result.getInvestmentSecurity(), result.getPrice(), result.getQuantity(), trade.getNotionalMultiplier())));
		result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), result.getExchangeRateToBase(), trade.getClientInvestmentAccount()));
		result.setPositionCostBasis(result.getLocalDebitCredit());
		result.setDescription("Position opening at " + InvestmentUtils.getUnadjustedQuantityFieldName(trade.getInvestmentSecurity()) + " for " + trade.getInvestmentSecurity().getSymbol());
		result.setOpening(true);

		return result;
	}


	private AccountingJournalDetail createPositionReductionToCurrentFactor(TradeFill fill) {
		AccountingJournalDetail result = populateCommonFieldsFromFill(fill);

		Trade trade = fill.getTrade();
		BigDecimal quantity = trade.getOriginalFace().subtract(fill.getQuantity()); // quantity reduces Original Face to Current Face
		result.setQuantity(trade.isBuy() ? quantity.negate() : quantity);
		result.setLocalDebitCredit(TradeBookingUtils.normalizeAmountSign(trade,
				getInvestmentCalculator().calculateNotional(result.getInvestmentSecurity(), result.getPrice(), result.getQuantity(), trade.getNotionalMultiplier())).negate());
		result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), result.getExchangeRateToBase(), trade.getClientInvestmentAccount()));
		result.setPositionCostBasis(result.getLocalDebitCredit());
		result.setDescription("Reducing " + InvestmentUtils.getUnadjustedQuantityFieldName(trade.getInvestmentSecurity()) +
				" to " + InvestmentUtils.getQuantityFieldName(trade.getInvestmentSecurity()) + " for " + trade.getInvestmentSecurity().getSymbol());
		result.setOpening(false);

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the settlement currency for this trade. If necessary converts into system specific form industry convention.
	 * <p>
	 * There are 2 different ways to buy or sell a forward and it's up to the trader to determine this.
	 * For example, USD denominated forward for EUR can be traded by "BUY EUR" or "SELL USD"
	 * From systems perspective we either BUY or SELL the forward and we always need to convert back to this convention based on "Settlement Currency"
	 */
	private InvestmentSecurity getSettlementCurrency(Trade trade) {
		InvestmentSecurity result = trade.getSettlementCurrency();
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			result = trade.getInvestmentSecurity().getInstrument().getTradingCurrency();
		}
		return result;
	}


	/**
	 * Populates the fields that are the same regardless of the type of position.
	 */
	private AccountingJournalDetail populateCommonFieldsFromFill(TradeFill fill) {
		AccountingJournalDetail result = new AccountingJournalDetail();

		Trade trade = fill.getTrade();
		result.setFkFieldId(fill.getId());
		result.setInvestmentSecurity(trade.getInvestmentSecurity());
		result.setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
		result.setClientInvestmentAccount(trade.getClientInvestmentAccount());
		result.setPrice(fill.getNotionalUnitPrice());
		result.setExchangeRateToBase(trade.getExchangeRateToBase());

		result.setTransactionDate(trade.getTradeDate());
		result.setOriginalTransactionDate(trade.getTradeDate());
		result.setSettlementDate(trade.getSettlementDate());

		result.setExecutingCompany(trade.getExecutingBrokerCompany());
		result.setPositionCommission(trade.getCommissionPerUnit() == null ? BigDecimal.ZERO : trade.getCommissionPerUnit().multiply(fill.getQuantity()).abs().negate());

		result.setSystemTable(getSystemSchemaService().getSystemTableByName(TradeFill.TABLE_NAME));
		result.setAccountingAccount(trade.isCollateralTrade() ? getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL)
				: getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION));

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
