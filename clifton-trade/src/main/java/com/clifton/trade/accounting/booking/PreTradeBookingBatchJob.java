package com.clifton.trade.accounting.booking;


import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


/**
 * The <code>PreTradeBookingBatchJob</code> is a batch job that will book trades
 * with destination "Pre-Trade Entry" in state "Awaiting Fills" on a specific trade date
 * using the price of the underlying security.
 *
 * @author mwacker
 */
public class PreTradeBookingBatchJob implements Task, StatusHolderObjectAware<Status> {

	/**
	 * Number of days to go back to run the FTP export.
	 */
	private Integer daysFromToday = 1;
	/**
	 * The date generation strategy.
	 */
	private DateGenerationOptions dateGenerationOption;


	private StatusHolderObject<Status> statusHolderObject;

	////////////////////////////////////////////////////////////////////////////

	private TradeService tradeService;
	private InvestmentCalculator investmentCalculator;
	private TradeBookingService tradeBookingService;
	private MarketDataRetriever marketDataRetriever;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;


	//////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Date tradeDate = getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());

		Collection<Integer> tradeIds = new HashSet<>();
		updateTradePrices(tradeDate, tradeIds, this.statusHolderObject.getStatus());

		if (!CollectionUtils.isEmpty(tradeIds)) {
			TradeBookingCommand command = new TradeBookingCommand();
			command.setStatusHolder(this.statusHolderObject);
			Integer[] tradeIdArray = new Integer[tradeIds.size()];
			tradeIdArray = tradeIds.toArray(tradeIdArray);
			command.setTradeIds(tradeIdArray);

			getTradeBookingService().tradeBookAndPost(command);
		}

		return this.statusHolderObject.getStatus();
	}


	private void updateTradePrices(Date tradeDate, Collection<Integer> tradeIdsToBook, Status status) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setWorkflowStateName(TradeService.TRADE_AWAITING_FILLS_STATE_NAME);
		searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, tradeDate));
		searchForm.setTradeDestination(TradeDestination.PRE_TRADE_ENTRY);

		int errorCount = 0;
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);
		Map<InvestmentSecurity, BigDecimal> underlyingPriceMap = new HashMap<>();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			try {
				BigDecimal price = getUnderlyingPrice(trade.getInvestmentSecurity(), trade.getTradeDate(), underlyingPriceMap);
				updateTrade(trade, price);

				tradeIdsToBook.add(trade.getId());
			}
			catch (Throwable e) {
				errorCount++;
				String errorMessage = ExceptionUtils.getOriginalMessage(e);
				status.addError(errorMessage);
				LogUtils.error(getClass(), "Error booking trade " + trade + ": " + errorMessage, e);
			}
		}

		status.setMessage("Finished updating trades: Total = " + tradeList.size() + ", Errors = " + errorCount);
	}


	@Transactional
	protected void updateTrade(Trade trade, BigDecimal price) {
		trade.setAverageUnitPrice(price);

		if (!MathUtils.isNullOrZero(trade.getAccountingNotional())) {
			trade.setQuantityIntended(getInvestmentCalculator().calculateQuantityFromNotional(trade.getInvestmentSecurity(), price, trade.getAccountingNotional(), trade.getNotionalMultiplier()));
		}
		else {
			trade.setAccountingNotional(getInvestmentCalculator().calculateNotional(trade.getInvestmentSecurity(), price, trade.getQuantityIntended(), trade.getNotionalMultiplier()));
		}

		// save the trade
		getTradeService().saveTrade(trade);

		// create the fill
		TradeFill fill = new TradeFill();
		fill.setTrade(trade);
		fill.setQuantity(trade.getQuantityIntended());
		fill.setNotionalUnitPrice(trade.getAverageUnitPrice());
		// other values will be calculated by the save method
		getTradeService().saveTradeFill(fill);

		// set the workflow state
		transitionTradeToExecute(trade);
	}


	@Transactional
	protected void transitionTradeToExecute(Trade trade) {
		WorkflowState bookedState = getWorkflowDefinitionService().getWorkflowStateByName(trade.getWorkflowState().getWorkflow().getId(), TradeService.TRADE_EXECUTED_VALID_STATE_NAME);
		getWorkflowTransitionService().executeWorkflowTransition(Trade.class.getSimpleName(), trade.getId(), bookedState.getId());
	}


	private BigDecimal getUnderlyingPrice(InvestmentSecurity security, Date tradeDate, Map<InvestmentSecurity, BigDecimal> cache) {
		if (cache.containsKey(security)) {
			return cache.get(security);
		}
		InvestmentSecurity underlying = security.getUnderlyingSecurity();
		BigDecimal price = getMarketDataRetriever().getPriceFlexible(underlying, tradeDate, true);
		cache.put(security, price);
		return price;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public TradeBookingService getTradeBookingService() {
		return this.tradeBookingService;
	}


	public void setTradeBookingService(TradeBookingService tradeBookingService) {
		this.tradeBookingService = tradeBookingService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
