package com.clifton.trade.accounting.booking;


import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>TradeBookingService</code> interface defines methods for booking/unbooking trades.
 *
 * @author vgomelsky
 */
public interface TradeBookingService {

	/**
	 * Books and posts to the General Ledger trades identified by the specified command.
	 * Limits to trades in "Executed" or "Unbooked" workflow state: only one transition away from Booked.
	 */
	@RequestMapping("tradeBookAndPost")
	@SecureMethod(dtoClass = AccountingTransaction.class)
	public Status tradeBookAndPost(TradeBookingCommand command);
}
