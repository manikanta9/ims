package com.clifton.trade.accounting.commission.event;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.event.AccountingJournalPostingEvent;
import com.clifton.trade.Trade;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * The TradeCommissionPostingEventListener class listens to GL Post events and if Commission or Fee GL Accounts
 * are posted that are linked to a trade from a different journal, updates trade's commission and fee fields to reflect this change.
 *
 * @author vgomelsky
 */
@Component
public class TradeCommissionPostingEventListener extends BaseAccountingJournalEvent<AccountingJournalPostingEvent> {

	@Override
	public String getEventName() {
		return AccountingJournalPostingEvent.EVENT_NAME;
	}


	@Override
	public void onEvent(AccountingJournalPostingEvent event) {
		AccountingJournal journal = event.getTarget();

		Map<Integer, CommissionAndFee> fillToCommissionMap = getFillToCommissionMap(journal);
		Map<Trade, CommissionAndFee> tradeCommissionMap = getTradeToCommissionMap(fillToCommissionMap);

		// add when posting
		updateTrades(tradeCommissionMap, true);
	}
}
