package com.clifton.trade.accounting.eventjournal;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.PopulatorCommand;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;

import java.math.BigDecimal;


/**
 * The <code>TradeAssumedFactorBuyAccountingEventJournalDetailPopulator</code> class populates {@link AccountingEventJournalDetail} fields
 * specific to Factor Change - Assumed Buy event.
 * <p/>
 * Calculates the change in Interest Expense due to Factor Change.
 *
 * @author vgomelsky
 */
public class TradeAssumedFactorBuyAccountingEventJournalDetailPopulator implements AccountingEventJournalDetailPopulator {

	private InvestmentCalculator investmentCalculator;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		// get the opening trade
		AccountingPosition openPosition = command.getOpenPosition();
		int fillId = openPosition.getOpeningTransaction().getFkFieldId();
		TradeFill fill = getTradeService().getTradeFill(fillId);
		ValidationUtils.assertNotNull(fill, "Cannot find TradeFill for id = " + fillId);
		Trade trade = fill.getTrade();

		// calculate new Current Face and face reduction
		BigDecimal currentFace = MathUtils.multiply(openPosition.getQuantity(), command.getEventData().getAfterEventValue(), 2);
		AccountingEventJournalDetail detail = command.getJournalDetail();
		detail.setAffectedQuantity(MathUtils.subtract(trade.getQuantityIntended(), currentFace));

		// calculate Interest Expense reduction
		BigDecimal interestExpense = getInvestmentCalculator().calculateAccruedInterest(openPosition.getInvestmentSecurity(), AccrualBasis.of(currentFace), null, AccrualDates.ofUpToDate(openPosition.getSettlementDate()));
		detail.setTransactionAmount(MathUtils.subtract(interestExpense, trade.getAccrualAmount()));

		// calculate Cost Basis Reduction
		BigDecimal newRemainingCostBasis = getInvestmentCalculator().calculateNotional(openPosition.getInvestmentSecurity(), openPosition.getPrice(), currentFace, trade.getNotionalMultiplier());
		detail.setAffectedCost(MathUtils.subtract(openPosition.getRemainingCostBasis(), newRemainingCostBasis));

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
