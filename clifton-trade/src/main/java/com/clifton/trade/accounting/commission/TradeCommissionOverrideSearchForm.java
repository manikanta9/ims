package com.clifton.trade.accounting.commission;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class TradeCommissionOverrideSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "trade.id")
	private Integer tradeId;

	@SearchField(searchField = "accountingAccount.id")
	private Short accountingAccountId;

	@SearchField(searchField = "trade.createDate")
	private Date tradeCreateDate;


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public Short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(Short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public Date getTradeCreateDate() {
		return this.tradeCreateDate;
	}


	public void setTradeCreateDate(Date tradeCreateDate) {
		this.tradeCreateDate = tradeCreateDate;
	}
}
