package com.clifton.trade.accounting.settlement;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;


@Service
public class TradeSettlementServiceImpl implements TradeSettlementService {

	public TradeService tradeService;
	public WorkflowDefinitionService workflowDefinitionService;
	public WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status tradeFinalSettlement(Integer[] tradeIds) {
		List<Trade> tradeList = Collections.emptyList();
		if (!ArrayUtils.isEmpty(tradeIds)) {
			TradeSearchForm searchForm = new TradeSearchForm();
			searchForm.setIds(tradeIds);
			tradeList = getTradeService().getTradeList(searchForm);
		}

		FinalSettlementCounts results = new FinalSettlementCounts(CollectionUtils.getSize(tradeList));
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			results.doFinalSettlement(trade, this::doFinalSettlement);
		}

		return results.getFinalStatus();
	}


	@Override
	public Status saveTradeActualSettlementDate(Integer[] tradeIds, Date actualSettlementDate) {
		Status status = Status.ofEmptyMessage();

		List<Trade> tradeList = Collections.emptyList();
		if (!ArrayUtils.isEmpty(tradeIds)) {
			TradeSearchForm searchForm = new TradeSearchForm();
			searchForm.setIds(tradeIds);
			tradeList = getTradeService().getTradeList(searchForm);
		}

		int updatedCount = 0;
		int skippedCount = 0;

		for (Trade trade : tradeList) {
			try {
				if (actualSettlementDate == null) {
					status.addError("The actual settlement date is required.");
					skippedCount = tradeList.size();
					break;
				}
				if (trade.getSettlementDate() == null) {
					status.addSkipped(trade + ": " + "Settlement Date is required.");
					skippedCount++;
					continue;
				}
				if (trade.getSettlementDate() != null && DateUtils.isDateBefore(actualSettlementDate, trade.getSettlementDate(), false)) {
					status.addSkipped(trade + ": " + String.format("Actual Settlement Date [%1$tm-%1$te-%1$tY] cannot be before the Settlement Date [%2$tm-%2$te-%2$tY].", actualSettlementDate, trade.getSettlementDate()));
					skippedCount++;
					continue;
				}
				trade.setActualSettlementDate(actualSettlementDate);
				getTradeService().saveTrade(trade);
				updatedCount++;
			}
			catch (Throwable e) {
				String errorMessage = ExceptionUtils.getOriginalMessage(e);
				status.addError(trade + ": " + errorMessage);
			}
		}

		String message = "Finished updating: Total = " + tradeList.size() + ", Updated = " + updatedCount + ", Errors = " + status.getErrorCount() + ", Skipped = " + skippedCount;
		status.setMessage(message);
		status.setActionPerformed(updatedCount != 0);

		return status;
	}


	@Transactional(timeout = 30)
	protected void doFinalSettlement(Trade trade) {
		WorkflowState bookedState = getWorkflowDefinitionService().getWorkflowStateByName(trade.getWorkflowState().getWorkflow().getId(), TradeService.TRADE_BOOKED_STATE_NAME);
		getWorkflowTransitionService().executeWorkflowTransition(Trade.class.getSimpleName(), trade.getId(), bookedState.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class FinalSettlementCounts {

		private final Status status = Status.ofEmptyMessage();

		private final int totalTrades;
		private int settledCount = 0;

		private long totalSettlementTimeMillis = 0;
		private long minSettlementTimeMillis = 0;
		private long maxSettlementTimeMillis = 0;


		public FinalSettlementCounts(int totalTrades) {
			this.totalTrades = totalTrades;
		}


		public void doFinalSettlement(Trade trade, Consumer<Trade> settler) {
			try {
				long start = System.currentTimeMillis();

				boolean isValid = isValid(trade);
				if (isValid) {
					settler.accept(trade);
				}

				long bookingTimeMillis = (System.currentTimeMillis() - start);
				this.totalSettlementTimeMillis += bookingTimeMillis;
				if (bookingTimeMillis > this.maxSettlementTimeMillis) {
					this.maxSettlementTimeMillis = bookingTimeMillis;
				}
				if (this.minSettlementTimeMillis == 0 || this.minSettlementTimeMillis > bookingTimeMillis) {
					this.minSettlementTimeMillis = bookingTimeMillis;
				}
				if (isValid) {
					this.settledCount++;
				}
			}
			catch (Throwable e) {
				String errorMessage = ExceptionUtils.getOriginalMessage(e);
				this.status.addError(trade + ": " + errorMessage);
				LogUtils.error(getClass(), "Error booking trade " + trade + ": " + errorMessage, e);
			}
		}


		private boolean isValid(Trade trade) {
			if (trade.getActualSettlementDate() == null) {
				this.status.addSkipped(String.format("The trade [%s] does not have an actual settlement date.", trade.getId()));
				return false;
			}
			return true;
		}


		public Status getFinalStatus() {
			String message = "Finished settling: Total = " + this.totalTrades + ", Settled = " + this.settledCount + ", Errors = " + this.status.getErrorCount() + ", Skipped = " + this.status.getSkippedCount();
			if (this.settledCount > 0) {
				message += "\nAvg Time = " + (this.totalSettlementTimeMillis / this.settledCount) + "ms, Min Time = " + this.minSettlementTimeMillis + "ms, Max Time = " + this.maxSettlementTimeMillis;
			}
			this.status.setMessage(message);
			this.status.setActionPerformed(this.settledCount != 0);
			return this.status;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}
