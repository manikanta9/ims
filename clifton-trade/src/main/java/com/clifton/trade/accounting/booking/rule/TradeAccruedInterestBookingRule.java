package com.clifton.trade.accounting.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>TradeAccruedInterestBookingRule</code> class adds accrued interest income/expense from the trade (accrualAmount) to the
 * journal.  Proportionally distributes it across positions during closing
 */
public class TradeAccruedInterestBookingRule extends AccountingCommonBookingRule<TradeFill> implements AccountingBookingRule<TradeFill> {

	private AccountingEventJournalService accountingEventJournalService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<TradeFill> bookingSession) {
		Trade trade = bookingSession.getBookableEntity().getTrade();

		if (!MathUtils.isNullOrZero(trade.getAccrualAmount1())) {
			applyAccruals(trade.getAccrualAmount1(), trade, bookingSession, true);
		}
		if (!MathUtils.isNullOrZero(trade.getAccrualAmount2())) {
			applyAccruals(trade.getAccrualAmount2(), trade, bookingSession, false);
		}
	}


	private void applyAccruals(BigDecimal accrualAmount, Trade trade, BookingSession<TradeFill> bookingSession, boolean firstAccrual) {
		// get positions that accrued interest (feeAmount) applies to
		AccountingJournal journal = bookingSession.getJournal();
		List<AccountingJournalDetailDefinition> positionRecords = filterActualPositionDetails(journal.getJournalDetailList());
		int positionCount = CollectionUtils.getSize(positionRecords);
		ValidationUtils.assertFalse(positionCount == 0, "At least one position must be present in order to apply Accrued Interest to trade: " + trade);

		if (positionCount == 1) {
			// no allocation needed: full interest against one position
			AccountingJournalDetailDefinition detail = CollectionUtils.getOnlyElement(positionRecords);
			addAccruedInterestEntries(journal, trade, detail, accrualAmount, firstAccrual);
		}
		else {
			// more than one position (sell closes multiple lots): allocate Accrued Interest proportionally
			BigDecimal currentAllocationAmount = BigDecimal.ZERO;
			BigDecimal currentTradeQuantity = BigDecimal.ZERO;
			BigDecimal tradeQuantity = trade.isBuy() ? trade.getQuantityIntended() : trade.getQuantityIntended().negate();
			for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(positionRecords)) {
				BigDecimal quantity = detail.getQuantity();

				//If the security can have factor changes and this is an opening detail, we need to adjust the quantity to the factored notional amount
				if (detail.isOpening() && detail.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null) {
					quantity = MathUtils.multiply(quantity, trade.getCurrentFactor(), 2);
				}

				currentTradeQuantity = currentTradeQuantity.add(quantity);
				BigDecimal detailAccrualAmount;
				if (MathUtils.isEqual(currentTradeQuantity, tradeQuantity)) {
					// last position: give it remaining Accrued Interest amount
					detailAccrualAmount = accrualAmount.subtract(currentAllocationAmount);
				}
				else {

					BigDecimal proportion = MathUtils.divide(quantity, tradeQuantity);
					detailAccrualAmount = MathUtils.multiply(accrualAmount, proportion, 2);
				}
				currentAllocationAmount = currentAllocationAmount.add(detailAccrualAmount);

				addAccruedInterestEntries(journal, trade, detail, detailAccrualAmount, firstAccrual);
			}

			// this should never happen but just in case
			if (!MathUtils.isEqual(currentTradeQuantity, tradeQuantity)) {
				throw new ValidationException("The sum of position quantities: " + currentTradeQuantity + " must equal trade's quantity intended: " + tradeQuantity + " (Trade ID = " + trade.getId() + ")");
			}
			if (!MathUtils.isEqual(currentAllocationAmount, accrualAmount)) {
				throw new ValidationException("The sum of accrued interest allocations: " + currentAllocationAmount + " must equal amount being allocated: " + accrualAmount + " (Trade ID = " + trade.getId() + ")");
			}
		}
	}


	/**
	 * Generates journal details (accrual and currency) for the specified arguments and adds them to the specified journal.
	 */
	private void addAccruedInterestEntries(AccountingJournal journal, Trade trade, AccountingJournalDetailDefinition detail, BigDecimal accrualAmount, boolean firstAccrual) {
		InvestmentInstrumentHierarchy hierarchy = trade.getInvestmentSecurity().getInstrument().getHierarchy();
		InvestmentSecurityEventType accrualType2 = hierarchy.getAccrualSecurityEventType2();
		InvestmentSecurityEventType accrualType = firstAccrual ? hierarchy.getAccrualSecurityEventType() : accrualType2;

		AccountingAccount accountingAccount;
		if (accrualType2 == null) {
			boolean longPosition = detail.getQuantityNormalized().signum() > -1;
			accountingAccount = longPosition ? trade.getTradeType().getBuyAccrualAccountingAccount() : trade.getTradeType().getSellAccrualAccountingAccount();
		}
		else {
			AccountingEventJournalType accrualJournalType = getAccountingEventJournalTypeForSecurity(accrualType, trade.getInvestmentSecurity());
			accountingAccount = accrualJournalType.getCreditAccount();
		}

		AccountingJournalDetailDefinition accruedInterestDetail = createAccruedInterestRecord(trade, detail, accrualAmount, accountingAccount);
		journal.addJournalDetail(accruedInterestDetail);
		journal.addJournalDetail(createCashCurrencyRecord(accruedInterestDetail, trade.getSettlementCurrency()));
	}


	private AccountingEventJournalType getAccountingEventJournalTypeForSecurity(InvestmentSecurityEventType securityEventType, InvestmentSecurity security) {
		AccountingEventJournalType result = null;
		List<AccountingEventJournalType> journalTypeList = getAccountingEventJournalService().getAccountingEventJournalTypeListByEventType(securityEventType.getId());
		if (CollectionUtils.isEmpty(journalTypeList)) {
			throw new IllegalStateException("At least one AccountingEventJournalType must be defined for security event: " + securityEventType);
		}
		for (AccountingEventJournalType journalType : CollectionUtils.getIterable(journalTypeList)) {
			if (journalType.getEventJournalTypeScope().isSecurityInScope(security)) {
				if (result != null) {
					throw new IllegalStateException("Cannot have more than one AccountingEventJournalType for security event type: " + securityEventType + " for security: " + security);
				}
				result = journalType;
			}
		}
		if (result == null) {
			throw new IllegalStateException("Cannot find AccountingEventJournalType for security event type: " + securityEventType + " for security: " + security);
		}
		return result;
	}


	private AccountingJournalDetail createAccruedInterestRecord(Trade trade, AccountingJournalDetailDefinition positionEntry, BigDecimal accrualAmount, AccountingAccount accountingAccount) {
		boolean longPosition = positionEntry.getQuantityNormalized().signum() > -1;

		AccountingJournalDetail result = new AccountingJournalDetail();
		BeanUtils.copyProperties(positionEntry, result, new String[]{"parentTransaction"});
		result.setParentDefinition(positionEntry);
		result.setPrice(null);
		result.setQuantity(null);
		result.setAccountingAccount(accountingAccount);
		result.setPositionCommission(BigDecimal.ZERO);
		result.setPositionCostBasis(BigDecimal.ZERO);

		if (trade.getTradeType().isAccrualSignAccountsForBuy()) {
			if (!accountingAccount.getAccountType().isGrowingOnDebitSide()) {
				// negative expense because it grows on same side as cash
				accrualAmount = accrualAmount.negate();
			}
		}
		else if (!longPosition) {
			accrualAmount = accrualAmount.negate();
		}

		result.setLocalDebitCredit(accrualAmount);
		result.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(result.getLocalDebitCredit(), result.getExchangeRateToBase(), positionEntry.getClientInvestmentAccount()));
		result.setDescription("Accrued " + accountingAccount.getName() + " for " + positionEntry.getInvestmentSecurity().getSymbol());
		result.setOpening(true);

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                   Getter & Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingEventJournalService getAccountingEventJournalService() {
		return this.accountingEventJournalService;
	}


	public void setAccountingEventJournalService(AccountingEventJournalService accountingEventJournalService) {
		this.accountingEventJournalService = accountingEventJournalService;
	}
}
