package com.clifton.trade.accounting.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;

import java.math.BigDecimal;
import java.util.function.Predicate;


/**
 * The <code>TradeCreateCurrencyPositionBookingRule</code> class is a booking rule that adds 2 details
 * to BookingSession journal: buy one currency and sell the other.
 * <p/>
 * NOTE: at this time one of the currencies must be client account's base currency.
 *
 * @author vgomelsky
 */
public class TradeCreateCurrencyPositionBookingRule extends AccountingCommonBookingRule<TradeFill> implements AccountingBookingRule<TradeFill> {

	private InvestmentCurrencyService investmentCurrencyService;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<TradeFill> session) {
		TradeFill fill = session.getBookableEntity();
		AccountingJournal journal = session.getJournal();

		Trade trade = fill.getTrade();
		if (trade.getExchangeRateToBase() == null) {
			throw new FieldValidationException("Cannot find exchange rate from " + trade.getInvestmentSecurity().getSymbol() + " currency denomination " + trade.getSettlementCurrency().getSymbol()
					+ " to client account base currency " + InvestmentUtils.getClientAccountBaseCurrency(trade).getSymbol() + " on " + DateUtils.fromDateShort(trade.getTradeDate()), "exchangeRate");
		}

		SystemTable allocFillSystemTable = getSystemSchemaService().getSystemTableByName(TradeFill.TABLE_NAME);
		AccountingAccount currencyAccount = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CURRENCY);

		// Trade's exchange rate is to base CCY (settle CCY = base CCY)
		AccountingJournalDetail currencyRecord = createCurrencyRecord(fill);
		currencyRecord.setSystemTable(allocFillSystemTable);
		currencyRecord.setAccountingAccount(currencyAccount);
		journal.addJournalDetail(currencyRecord);

		if (trade.getSettlementCurrency().equals(InvestmentUtils.getClientAccountBaseCurrency(trade))) {
			journal.addJournalDetail(createCashRecord(currencyRecord));
		}
		else {
			// Trade's exchange rate is the rate between CCY traded and settle CCY and not to base CCY (settle CCY differs from base CCY).
			// Look up FX for CCY traded and base CCY to calculate currency details.
			BigDecimal exchangeRateToBase = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(trade.fxSourceCompany(), !trade.fxSourceCompanyOverridden(), trade.getInvestmentSecurity().getSymbol(), trade.getClientInvestmentAccount().getBaseCurrency().getSymbol(), trade.getTradeDate()).flexibleLookup());
			currencyRecord.setExchangeRateToBase(exchangeRateToBase);
			currencyRecord.setBaseDebitCredit(InvestmentCalculatorUtils.calculateBaseAmount(currencyRecord.getLocalDebitCredit(), currencyRecord.getExchangeRateToBase(), trade.getClientInvestmentAccount()));

			journal.addJournalDetail(createSettlementCurrencyRecord(fill, currencyRecord));
		}
	}


	private AccountingJournalDetail createCurrencyRecord(TradeFill fill) {
		Trade trade = fill.getTrade();

		AccountingJournalDetail currencyEntry = new AccountingJournalDetail();
		currencyEntry.setFkFieldId(fill.getId());
		currencyEntry.setInvestmentSecurity(trade.getInvestmentSecurity());
		currencyEntry.setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
		currencyEntry.setClientInvestmentAccount(trade.getClientInvestmentAccount());
		currencyEntry.setExecutingCompany(trade.getExecutingBrokerCompany());
		currencyEntry.setPrice(null);
		currencyEntry.setQuantity(null);
		currencyEntry.setExchangeRateToBase(getExchangeRateToBase(fill));
		currencyEntry.setLocalDebitCredit(trade.isBuy() ? fill.getNotionalTotalPrice() : fill.getNotionalTotalPrice().negate());
		currencyEntry.setBaseDebitCredit(trade.isBuy() ? fill.getPaymentTotalPrice() : fill.getPaymentTotalPrice().negate());
		currencyEntry.setPositionCostBasis(BigDecimal.ZERO);
		currencyEntry.setTransactionDate(trade.getTradeDate());
		currencyEntry.setOriginalTransactionDate(trade.getTradeDate());
		currencyEntry.setSettlementDate(trade.getSettlementDate());
		currencyEntry.setDescription("Converting " + trade.getInvestmentSecurity().getSymbol() + " to " + InvestmentUtils.getClientAccountBaseCurrency(currencyEntry).getSymbol());
		currencyEntry.setPositionCommission(BigDecimal.ZERO);
		currencyEntry.setOpening(true);
		return currencyEntry;
	}


	private AccountingJournalDetail createCashRecord(AccountingJournalDetailDefinition oppositeEntry) {
		AccountingJournalDetail cashCurrencyEntry = new AccountingJournalDetail();
		BeanUtils.copyProperties(oppositeEntry, cashCurrencyEntry, new String[]{"parentTransaction"});
		// if the opposite entries parent is null, it must be the parent itself.  Otherwise, the oppositeEntry is a child, so use its parent as the cash's parent.
		cashCurrencyEntry.setParentDefinition(oppositeEntry.getParentDefinition() == null ? oppositeEntry : oppositeEntry.getParentDefinition());

		cashCurrencyEntry.setExchangeRateToBase(BigDecimal.ONE);
		cashCurrencyEntry.setLocalDebitCredit(BigDecimal.ZERO.subtract(oppositeEntry.getBaseDebitCredit()).setScale(2, BigDecimal.ROUND_HALF_UP));
		cashCurrencyEntry.setBaseDebitCredit(cashCurrencyEntry.getLocalDebitCredit());
		cashCurrencyEntry.setPositionCostBasis(BigDecimal.ZERO);

		cashCurrencyEntry.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_CASH));
		cashCurrencyEntry.setInvestmentSecurity(InvestmentUtils.getClientAccountBaseCurrency(oppositeEntry));
		cashCurrencyEntry.setDescription(getCashCurrencyRecordDescriptionPrefix(cashCurrencyEntry)
				+ (cashCurrencyEntry.getParentDefinition().isOpening() ? OPENING_DESCRIPTION_SEGMENT : CLOSING_DESCRIPTION_SEGMENT) + cashCurrencyEntry.getParentDefinition().getInvestmentSecurity().getSymbol());
		cashCurrencyEntry.setOpening(false);

		return cashCurrencyEntry;
	}


	private AccountingJournalDetail createSettlementCurrencyRecord(TradeFill tradeFill, AccountingJournalDetailDefinition oppositeEntry) {
		AccountingJournalDetail settlementCurrencyEntry = new AccountingJournalDetail();
		BeanUtils.copyProperties(oppositeEntry, settlementCurrencyEntry, new String[]{"parentTransaction"});
		// if the opposite entries parent is null, it must be the parent itself.  Otherwise, the oppositeEntry is a child, so use its parent as the cash's parent.
		settlementCurrencyEntry.setParentDefinition(oppositeEntry.getParentDefinition() == null ? oppositeEntry : oppositeEntry.getParentDefinition());

		Trade trade = tradeFill.getTrade();
		settlementCurrencyEntry.setInvestmentSecurity(trade.getSettlementCurrency());
		// base debit/credit will be the same as the opposite entry and the payment will be the value from the trade
		// We will have to calculate the FX rate from the two values.
		settlementCurrencyEntry.setLocalDebitCredit(trade.isBuy() ? tradeFill.getPaymentTotalPrice().negate() : tradeFill.getPaymentTotalPrice());
		settlementCurrencyEntry.setBaseDebitCredit(settlementCurrencyEntry.getBaseDebitCredit().negate());
		BigDecimal fxRateToBase = MathUtils.divide(settlementCurrencyEntry.getBaseDebitCredit(), settlementCurrencyEntry.getLocalDebitCredit());
		Predicate<BigDecimal> roundingTestCondition = testValue -> MathUtils.isEqual(settlementCurrencyEntry.getBaseDebitCredit(), MathUtils.multiply(testValue, settlementCurrencyEntry.getLocalDebitCredit(), settlementCurrencyEntry.getBaseDebitCredit().scale()));
		fxRateToBase = MathUtils.roundToSmallestPrecision(fxRateToBase, roundingTestCondition);
		settlementCurrencyEntry.setExchangeRateToBase(fxRateToBase);

		settlementCurrencyEntry.setDescription("Converting " + settlementCurrencyEntry.getInvestmentSecurity().getSymbol() + " to " + InvestmentUtils.getClientAccountBaseCurrency(settlementCurrencyEntry).getSymbol());
		return settlementCurrencyEntry;
	}


	/**
	 * Returns FX rate from the trade. "Normalizes" the rate, if necessary, from
	 * industry standard convention to ours that is always based on multiplication.
	 */
	private BigDecimal getExchangeRateToBase(TradeFill fill) {
		Trade trade = fill.getTrade();
		BigDecimal fx = trade.getExchangeRateToBase();

		boolean multiply = getInvestmentCurrencyService().isInvestmentCurrencyMultiplyConvention(trade.getInvestmentSecurity().getSymbol(), trade.getClientInvestmentAccount().getBaseCurrency().getSymbol());
		if (!multiply) {
			// Currency trades should always have the exchange rate defined for the dominant currency of the two on the trade (e.g. USD->GBP and GBP->USD will both use the GBP->USD exchange rate).
			// Convert FX from industry standard convention to ours based on multiplication
			int precision = DataTypes.EXCHANGE_RATE.getPrecision();
			fx = MathUtils.divide(BigDecimal.ONE, fx, precision);
			Predicate<BigDecimal> roundingTestCondition = testValue -> MathUtils.isEqual(fill.getNotionalTotalPrice(), MathUtils.multiply(testValue, fill.getPaymentTotalPrice(), fill.getNotionalTotalPrice().scale()));
			fx = MathUtils.roundToSmallestPrecision(fx, roundingTestCondition);
		}

		return fx;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public InvestmentCurrencyService getInvestmentCurrencyService() {
		return this.investmentCurrencyService;
	}


	public void setInvestmentCurrencyService(InvestmentCurrencyService investmentCurrencyService) {
		this.investmentCurrencyService = investmentCurrencyService;
	}
}
