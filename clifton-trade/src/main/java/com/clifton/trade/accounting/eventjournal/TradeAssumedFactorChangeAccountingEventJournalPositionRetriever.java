package com.clifton.trade.accounting.eventjournal;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.generator.positionretriever.AccountingEventJournalPositionRetriever;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.ArrayList;
import java.util.List;


public class TradeAssumedFactorChangeAccountingEventJournalPositionRetriever implements AccountingEventJournalPositionRetriever {

	/**
	 * Specifies whether retriever is for buys or for sells
	 */
	private boolean buy;

	private AccountingTransactionService accountingTransactionService;
	private InvestmentCalculator investmentCalculator;
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingPosition> getAffectedPositions(@SuppressWarnings("unused") AccountingEventJournalType journalType, InvestmentSecurityEvent securityEvent, Integer clientInvestmentAccountId) {
		List<AccountingPosition> result = new ArrayList<>();

		// get all trades for the specified Factor Change event that were booked at assumed factor
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setBuy(isBuy());
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		searchForm.setAssumedFactorSecurityEventId(securityEvent.getId());
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			// find opening AccountingTransaction: need trade fill first
			List<TradeFill> fillList = getTradeService().getTradeFillListByTrade(trade.getId());
			ValidationUtils.assertTrue(CollectionUtils.getSize(fillList) == 1, "Only 1 fill is supported for Assumed Factor trade with id = " + trade.getId());

			AccountingTransactionSearchForm tranSearchForm = new AccountingTransactionSearchForm();
			tranSearchForm.setFkFieldId(CollectionUtils.getOnlyElement(fillList).getId());
			tranSearchForm.setJournalTypeName(AccountingJournalType.TRADE_JOURNAL);
			tranSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
			// extra filters to improve lookup performance: indexed fields
			tranSearchForm.setInvestmentSecurityId(trade.getInvestmentSecurity().getId());
			tranSearchForm.setClientInvestmentAccountId(trade.getClientInvestmentAccount().getId());
			List<AccountingTransaction> fillTranList = getAccountingTransactionService().getAccountingTransactionList(tranSearchForm);

			int countBefore = CollectionUtils.getSize(result);
			// one trade fill can be closing multiple positions (multiple transactions)
			for (AccountingTransaction t : CollectionUtils.getIterable(fillTranList)) {
				// check if parent is from the same journal: reduction to purchase face
				if (t.getParentTransaction() != null && t.getJournal().equals(t.getParentTransaction().getJournal())) {
					continue;
				}

				// create AccountingPosition being affected by Assumed Factor Change for the opening or closing transaction
				AccountingPosition position = AccountingPosition.forOpeningTransaction(t);
				AccountingTransaction openTran = (t.getParentTransaction() == null) ? t : t.getParentTransaction(); // SELL has parent
				position.setRemainingQuantity(MathUtils.multiply(openTran.getQuantity(), securityEvent.getBeforeEventValue(), 2));
				position.setRemainingCostBasis(getInvestmentCalculator().calculateNotional(openTran.getInvestmentSecurity(), openTran.getPrice(), position.getRemainingQuantity(),
						trade.getNotionalMultiplier()));
				result.add(position);
			}
			ValidationUtils.assertFalse(countBefore == CollectionUtils.getSize(result), "Cannot find AccountingTransaction for trade id = " + trade.getId());
		}

		return CollectionUtils.isEmpty(result) ? null : result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
