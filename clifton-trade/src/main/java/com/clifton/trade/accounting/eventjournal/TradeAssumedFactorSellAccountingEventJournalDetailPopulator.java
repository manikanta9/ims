package com.clifton.trade.accounting.eventjournal;


import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.generator.detailpopulator.AccountingEventJournalDetailPopulator;
import com.clifton.accounting.eventjournal.generator.detailpopulator.PopulatorCommand;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;

import java.math.BigDecimal;


/**
 * The <code>TradeAssumedFactorSellAccountingEventJournalDetailPopulator</code> class populates {@link AccountingEventJournalDetail} fields
 * specific to Factor Change - Assumed Sell event.
 * <p/>
 * Calculates the change in Interest Income due to Factor Change.
 *
 * @author vgomelsky
 */
public class TradeAssumedFactorSellAccountingEventJournalDetailPopulator implements AccountingEventJournalDetailPopulator {

	private InvestmentCalculator investmentCalculator;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean populate(PopulatorCommand command) {
		// get the closing trade
		AccountingPosition closingPosition = command.getOpenPosition();
		int fillId = closingPosition.getOpeningTransaction().getFkFieldId();
		TradeFill fill = getTradeService().getTradeFill(fillId);
		ValidationUtils.assertNotNull(fill, "Cannot find TradeFill for id = " + fillId);
		Trade trade = fill.getTrade();

		// calculate Interest Income decrease
		BigDecimal originalFace = closingPosition.getOpeningTransaction().getParentTransaction().getQuantity();
		BigDecimal currentFace = MathUtils.multiply(originalFace, command.getEventData().getAfterEventValue(), 2);
		BigDecimal interestIncome = getInvestmentCalculator().calculateAccruedInterest(closingPosition.getInvestmentSecurity(), AccrualBasis.of(currentFace), null, AccrualDates.ofUpToDate(closingPosition.getSettlementDate()));
		AccountingEventJournalDetail detail = command.getJournalDetail();
		detail.setTransactionAmount(MathUtils.subtract(interestIncome, calculateLotPortion(trade.getAccrualAmount(), trade, originalFace)));

		// calculate Principal Payment
		detail.setAffectedQuantity(MathUtils.subtract(calculateLotPortion(trade.getQuantityIntended(), trade, originalFace), currentFace));

		// principal payment
		detail.setTransactionPrice(trade.getAverageUnitPrice()); // need to use closing price to calculate gain/loss
		// Gain/Loss = Affected Cost - Affected Quantity
		detail.setAffectedCost(getInvestmentCalculator().calculateNotional(closingPosition.getInvestmentSecurity(), trade.getAverageUnitPrice(), detail.getAffectedQuantity(),
				trade.getNotionalMultiplier()));

		return false;
	}


	/**
	 * A single assumed sell trade maybe closing multiple lots. Returns lot's portion of the specified amount to avoid double counting.
	 */
	private BigDecimal calculateLotPortion(BigDecimal amount, Trade trade, BigDecimal originalFace) {
		return MathUtils.divide(MathUtils.multiply(amount, originalFace), trade.getOriginalFace(), 2);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
