package com.clifton.trade.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.TradeFill;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>TradeCommission</code> is an in-memory representation of a commission associated with a trade. Typically the commission is one of four types: previewed, overridden, actual, or adjusted.
 *
 * @author jgommels
 */
public class TradeCommission<T> {

	private T fkFieldId;
	private String tableName;
	private TradeCommissionSources source;
	private TradeFill tradeFill;
	private AccountingAccount expenseAccountingAccount;
	private InvestmentSecurity commissionCurrency;
	private BigDecimal amountPerUnit;
	private BigDecimal totalAmount;
	private String note;

	// when possible (overrides), these fields will be populated
	private Short createUserId;
	private Date createDate;
	private Short updateUserId;
	private Date updateDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeCommission<Long> createForActualCommission(AccountingTransaction transaction, TradeFill fill) {
		return new TradeCommission<>(transaction.getJournal().getId(), "AccountingJournal", TradeCommissionSources.ACTUAL, transaction.getAccountingAccount(), null, transaction.getPrice(),
				transaction.getLocalDebitCredit().negate(), fill, transaction.getDescription());
	}


	public static TradeCommission<Integer> createForCommissionOverride(TradeCommissionOverride override) {
		InvestmentSecurity currency = override.getCommissionCurrency();
		if (currency == null) {
			currency = override.getTrade().getInvestmentSecurity().getInstrument().getTradingCurrency();
		}

		TradeCommission<Integer> result = new TradeCommission<>(override.getId(), "AccountingCommissionOverride", TradeCommissionSources.OVERRIDE, override.getAccountingAccount(), currency, override.getAmountPerUnit(),
				override.getTotalAmount(), null, override.getOverrideNote());

		result.setCreateUserId(override.getCreateUserId());
		result.setCreateDate(override.getCreateDate());
		result.setUpdateUserId(override.getUpdateUserId());
		result.setUpdateDate(override.getUpdateDate());
		return result;
	}


	public static TradeCommission<Long> createForCommissionAdjustment(AccountingTransaction transaction, TradeFill fill) {
		return new TradeCommission<>(transaction.getJournal().getId(), "AccountingJournal", TradeCommissionSources.ADJUSTMENT, transaction.getAccountingAccount(), null, transaction.getPrice(),
				transaction.getLocalDebitCredit().negate(), fill, transaction.getDescription());
	}


	public static TradeCommission<Object> createForCommissionPreview(AccountingJournalDetailDefinition detail, TradeFill fill, String note) {
		return new TradeCommission<>(null, null, TradeCommissionSources.PREVIEW, detail.getAccountingAccount(), null, detail.getPrice(), detail.getLocalDebitCredit().negate(), fill, note);
	}


	public static TradeCommission<Object> createForCommissionSummary(TradeCommissionSources source, AccountingAccount account, BigDecimal totalAmount) {
		return new TradeCommission<>(null, null, source, account, null, null, totalAmount, null, null);
	}


	public TradeCommission() {
		// no-arg constructor used in tests
	}


	public TradeCommission(T fkFieldId, String tableName, TradeCommissionSources source, AccountingAccount glAccount, InvestmentSecurity currency, BigDecimal amountPerUnit, BigDecimal totalAmount,
	                       TradeFill tradeFill, String note) {
		this.fkFieldId = fkFieldId;
		this.tableName = tableName;
		this.source = source;
		this.tradeFill = tradeFill;
		this.expenseAccountingAccount = glAccount;
		this.commissionCurrency = currency;
		this.amountPerUnit = amountPerUnit;
		this.totalAmount = totalAmount;
		this.note = note;
	}


	@Override
	public String toString() {
		return "TradeCommission [source=" + this.source + ", tradeFill=" + this.tradeFill + ", expenseAccountingAccount=" + this.expenseAccountingAccount + ", amountPerUnit=" + this.amountPerUnit
				+ ", totalAmount=" + this.totalAmount + ", note=" + this.note + "]";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public T getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(T fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public TradeCommissionSources getSource() {
		return this.source;
	}


	public TradeFill getTradeFill() {
		return this.tradeFill;
	}


	public AccountingAccount getExpenseAccountingAccount() {
		return this.expenseAccountingAccount;
	}


	public AccountingAccount getAccountingAccount() {
		return this.expenseAccountingAccount;
	}


	public BigDecimal getAmountPerUnit() {
		return this.amountPerUnit;
	}


	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}


	public String getNote() {
		return this.note;
	}


	public void setSource(TradeCommissionSources source) {
		this.source = source;
	}


	public void setTradeFill(TradeFill tradeFill) {
		this.tradeFill = tradeFill;
	}


	public void setExpenseAccountingAccount(AccountingAccount expenseAccountingAccount) {
		this.expenseAccountingAccount = expenseAccountingAccount;
	}


	public InvestmentSecurity getCommissionCurrency() {
		return this.commissionCurrency;
	}


	public void setCommissionCurrency(InvestmentSecurity commissionCurrency) {
		this.commissionCurrency = commissionCurrency;
	}


	public void setAmountPerUnit(BigDecimal amountPerUnit) {
		this.amountPerUnit = amountPerUnit;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Short getCreateUserId() {
		return this.createUserId;
	}


	public void setCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
	}


	public Date getCreateDate() {
		return this.createDate;
	}


	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public Short getUpdateUserId() {
		return this.updateUserId;
	}


	public void setUpdateUserId(Short updateUserId) {
		this.updateUserId = updateUserId;
	}


	public Date getUpdateDate() {
		return this.updateDate;
	}


	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
