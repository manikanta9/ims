package com.clifton.trade.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionOverride;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;

import java.math.BigDecimal;


/**
 * The <code>TradeCommissionOverride</code> represents a custom commission or fee override for a trade. The override can be a "flat" amount, or a "per unit" amount.
 * If it is a "flat" amount, then any automated commission calculation logic for that GL Account is ignored and the amount is distributed evenly across the lots.
 * If it is a "per unit" amount, then the applicable {@link AccountingCommissionDefinition} rule is retrieved and its value is temporarily replaced with the overridden
 * value, otherwise if no such {@link AccountingCommissionDefinition} record exists then the amount is simply multiplied by the quantity of each lot.
 *
 * @author jgommels
 */
public class TradeCommissionOverride extends BaseEntity<Integer> implements AccountingCommissionOverride {

	private Trade trade;
	private AccountingAccount accountingAccount;
	private String overrideNote;
	private BigDecimal amountPerUnit;
	private BigDecimal totalAmount;

	/**
	 * The currency that the commission is in. If null, this indicates that the currency denomination of the security should be used.
	 */
	private InvestmentSecurity commissionCurrency;


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	@Override
	public AccountingAccount getAccountingAccount() {
		return this.accountingAccount;
	}


	@Override
	public void setAccountingAccount(AccountingAccount accountingAccount) {
		this.accountingAccount = accountingAccount;
	}


	public String getOverrideNote() {
		return this.overrideNote;
	}


	public void setOverrideNote(String overrideNote) {
		this.overrideNote = overrideNote;
	}


	@Override
	public BigDecimal getAmountPerUnit() {
		return this.amountPerUnit;
	}


	@Override
	public void setAmountPerUnit(BigDecimal amountPerUnit) {
		this.amountPerUnit = amountPerUnit;
	}


	public BigDecimal getTotalAmount() {
		return this.totalAmount;
	}


	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}


	public InvestmentSecurity getCommissionCurrency() {
		return this.commissionCurrency;
	}


	public void setCommissionCurrency(InvestmentSecurity commissionCurrency) {
		this.commissionCurrency = commissionCurrency;
	}


	@Override
	public String toString() {
		return "TradeCommissionOverride [trade=" + this.trade + ", accountingAccount=" + this.accountingAccount + ", overrideNote=" + this.overrideNote + ", AmountPerUnit=" + this.amountPerUnit
				+ ", totalAmount=" + this.totalAmount + "]";
	}
}
