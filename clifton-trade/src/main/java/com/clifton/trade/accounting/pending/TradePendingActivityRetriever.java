package com.clifton.trade.accounting.pending;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookingPreviewCommand;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.pending.AccountingPendingActivityRetriever;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.search.GeneralLedgerSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.accounting.booking.TradeBookingUtils;
import com.clifton.trade.search.TradeFillSearchForm;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.trade.util.TradeFillUtilHandler;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;


/**
 * @author mwacker
 */
@Component
public class TradePendingActivityRetriever implements AccountingPendingActivityRetriever {

	private static final Set<String> GL_ACCOUNT_RESTRICTION_KEYS = CollectionUtils.createHashSet("clientInvestmentAccount", "holdingInvestmentAccount");
	private static final Map<String, Supplier<String>> GL_SECURITY_RESTRICTION_KEYS = MapUtils.ofEntries(
			MapUtils.entry("investmentSecurityId", () -> "securityId"),
			MapUtils.entry("investmentTypeId", () -> "investmentTypeId"),
			MapUtils.entry("underlyingSecurityId", () -> "underlyingSecurityId")
	);

	private TradeService tradeService;
	private TradeFillUtilHandler tradeFillUtilHandler;
	private AccountingAccountService accountingAccountService;
	private AccountingBookingService<TradeFill> accountingBookingService;
	private AccountingJournalService accountingJournalService;
	private InvestmentInstrumentService investmentInstrumentService;
	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<AccountingJournalDetailDefinition> getPendingTransactionList(final GeneralLedgerSearchForm searchForm) {
		SystemTable tradeTable = getSystemSchemaService().getSystemTableByName(Trade.class.getSimpleName());
		List<TradeFill> tradeFillList = getTradeFillListFromGeneralLedgerSearchForm(searchForm);
		return CollectionUtils.getConvertedFlattened(tradeFillList, tradeFill -> generateJournalDetailListFromFill(tradeFill, tradeTable, searchForm.getPendingActivityRequest()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Includes actual fills for the specified search parameters as well as "preview" fills for pending trades that do not have a fill yet.
	 */
	private List<TradeFill> getTradeFillListFromGeneralLedgerSearchForm(GeneralLedgerSearchForm searchForm) {
		List<TradeFill> result = new ArrayList<>();

		// need trades so that we can "preview" fills for trades that do not have them yet
		List<Trade> tradeList = getTradeList(searchForm);
		if (!CollectionUtils.isEmpty((tradeList))) {
			// Look up TradeFill for the found Trades. Since we used the search form for the trades, the fills will not be populated.
			Map<Trade, List<TradeFill>> tradeFillMap = getTradeFillListMapForTradeList(tradeList, searchForm);
			for (Trade trade : CollectionUtils.getIterable(tradeList)) {
				List<TradeFill> tradeFillList = tradeFillMap.get(trade);
				if (CollectionUtils.isEmpty(tradeFillList)) {
					TradeFill tradeFill = getTradeFillPreview(trade);
					if (tradeFill != null) {
						result.add(tradeFill);
					}
					else if (searchForm.getPendingActivityRequest().isStrict()) {
						// The trade fill could not be simulated; in strict mode, throw an exception
						throw new RuntimeException(String.format("Simulated trade fills could not be created for trade [%s]. A possible cause is that the trade has no fills and no" +
								" average price from which to derive notional prices.", trade.getLabel()));
					}
				}
				else {
					result.addAll(tradeFillList);
				}
			}
		}
		return result;
	}


	private List<Trade> getTradeList(GeneralLedgerSearchForm searchForm) {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setClientInvestmentAccountGroupId(searchForm.getClientInvestmentAccountGroupId());
		tradeSearchForm.setClientInvestmentAccountId(searchForm.getClientInvestmentAccountId());
		tradeSearchForm.setClientInvestmentAccountIds(searchForm.getClientInvestmentAccountIds());
		tradeSearchForm.setHoldingInvestmentAccountId(searchForm.getHoldingInvestmentAccountId());
		tradeSearchForm.setHoldingInvestmentAccountIds(searchForm.getHoldingInvestmentAccountIds());

		CollectionUtils.getStream(searchForm.getRestrictionList())
				.filter(searchRestriction -> GL_ACCOUNT_RESTRICTION_KEYS.contains(searchRestriction.getField()) && !tradeSearchForm.isSearchRestrictionSet(searchRestriction.getField()))
				.forEach(tradeSearchForm::addSearchRestriction);

		if (isIncludeGeneralLedgerSecurityFilter(searchForm)) {
			// include impact from other (currency, etc.) trades pending activity in corresponding account(s)
			tradeSearchForm.setInvestmentTypeId(searchForm.getInvestmentTypeId());
			tradeSearchForm.setInvestmentGroupId(searchForm.getInvestmentGroupId());
			tradeSearchForm.setInvestmentInstrumentId((searchForm.getInvestmentInstrumentId()));
			tradeSearchForm.setSecurityId(searchForm.getInvestmentSecurityId());
			tradeSearchForm.setUnderlyingSecurityId(searchForm.getUnderlyingSecurityId());

			CollectionUtils.getStream(searchForm.getRestrictionList())
					.filter(searchRestriction -> GL_SECURITY_RESTRICTION_KEYS.containsKey(searchRestriction.getField()))
					.map(searchRestriction -> {
						String fieldName = GL_SECURITY_RESTRICTION_KEYS.get(searchRestriction.getField()).get();
						return tradeSearchForm.isSearchRestrictionSet(fieldName) ? null : searchRestriction.copyForField(fieldName);
					})
					.filter(Objects::nonNull)
					.forEach(tradeSearchForm::addSearchRestriction);
		}
		tradeSearchForm.setExcludeWorkflowStatusName(TradeService.TRADES_CLOSED_STATUS_NAME);
		tradeSearchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, searchForm.getTransactionOrSettlementDate()));
		return getTradeService().getTradeList(tradeSearchForm);
	}


	private Map<Trade, List<TradeFill>> getTradeFillListMapForTradeList(List<Trade> tradeList, GeneralLedgerSearchForm searchForm) {
		Integer[] tradeIds = CollectionUtils.getStream(tradeList)
				.map(Trade::getId)
				.toArray(Integer[]::new);
		List<TradeFill> tradeFillList = getNotBookedTradeFillList(tradeIds, searchForm);
		return BeanUtils.getBeansMap(tradeFillList, TradeFill::getTrade);
	}


	private List<TradeFill> getNotBookedTradeFillList(Integer[] tradeIds, GeneralLedgerSearchForm searchForm) {
		TradeFillSearchForm tradeFillSearchForm = new TradeFillSearchForm();
		tradeFillSearchForm.setTradeIds(tradeIds);
		tradeFillSearchForm.setBookingDateIsNull(true);
		tradeFillSearchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, searchForm.getTransactionOrSettlementDate()));
		return getTradeService().getTradeFillList(tradeFillSearchForm);
	}


	private TradeFill getTradeFillPreview(Trade trade) {
		TradeFill fill = getTradeFillUtilHandler().createTradeFill(trade, true);
		if (fill != null) {
			getTradeFillUtilHandler().validateTradeFill(trade, fill);
		}
		return fill;
	}


	/**
	 * Determines if the {@link GeneralLedgerSearchForm} is filtering for a currency.
	 * Currency securities are unique in they are not positions and can be affected by trades from different types of securities.
	 * Thus, we have to look up applicable trades more generically to get a good pending preview.
	 */
	private boolean isIncludeGeneralLedgerSecurityFilter(GeneralLedgerSearchForm searchForm) {
		if (!searchForm.getPendingActivityRequest().isPositionsOnly() && searchForm.getInvestmentSecurityId() != null) {
			return !getInvestmentInstrumentService().getInvestmentSecurity(searchForm.getInvestmentSecurityId()).isCurrency();
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<AccountingJournalDetailDefinition> generateJournalDetailListFromFill(TradeFill tradeFill, SystemTable tradeTable, PendingActivityRequests pendingActivityRequest) {
		// Generate journal details for fill
		final List<AccountingJournalDetailDefinition> tradeFillDetailList;
		if (pendingActivityRequest.isGeneratedByBookingPreview()) {
			BookingRuleScopes scope = pendingActivityRequest.isPositionsOnly() ? BookingRuleScopes.POSITION_IMPACT : null;
			AccountingJournal journal = getAccountingBookingService().getAccountingJournalPreview(BookingPreviewCommand.ofScope(tradeFill, AccountingJournalType.TRADE_JOURNAL, scope));
			@SuppressWarnings("unchecked")
			List<AccountingJournalDetailDefinition> previewList = (List<AccountingJournalDetailDefinition>) journal.getJournalDetailList();
			if (pendingActivityRequest.isPositionsOnly()) {
				previewList = BeanUtils.filter(previewList, position -> position.getAccountingAccount().isPosition());
			}
			tradeFillDetailList = new ArrayList<>(previewList);
		}
		else {
			tradeFillDetailList = Collections.singletonList(createJournalDetailFromFill(tradeFill));
		}

		// For pseudo-trade-fills, link both journal and journal details directly to trade
		for (AccountingJournalDetailDefinition journalDetail : tradeFillDetailList) {
			if (journalDetail.getFkFieldId() == null && journalDetail.getJournal().getFkFieldId() == null) {
				journalDetail.setSystemTable(tradeTable);
				journalDetail.setFkFieldId(tradeFill.getTrade().getId());
				journalDetail.getJournal().setSystemTable(tradeTable);
				journalDetail.getJournal().setFkFieldId(tradeFill.getTrade().getId());
			}
		}
		return tradeFillDetailList;
	}


	private AccountingJournalDetailDefinition createJournalDetailFromFill(TradeFill fill) {
		AccountingJournalDetail journalDetail = new AccountingJournalDetail();
		journalDetail.setClientInvestmentAccount(fill.getTrade().getClientInvestmentAccount());
		journalDetail.setHoldingInvestmentAccount(fill.getTrade().getHoldingInvestmentAccount());
		journalDetail.setAccountingAccount(getAccountingAccountService().getAccountingAccountByName(fill.getTrade().isCollateral() ? AccountingAccount.ASSET_POSITION_COLLATERAL : AccountingAccount.ASSET_POSITION));
		journalDetail.setInvestmentSecurity(fill.getTrade().getInvestmentSecurity());

		journalDetail.setExchangeRateToBase(fill.getTrade().getExchangeRateToBase());
		journalDetail.setQuantity(fill.getTrade().isBuy() ? fill.getQuantity() : fill.getQuantity().negate());
		journalDetail.setPrice(fill.getNotionalUnitPrice());
		journalDetail.setLocalDebitCredit(TradeBookingUtils.normalizeAmountSign(fill.getTrade(), fill.getPaymentTotalPrice()));
		journalDetail.setBaseDebitCredit(MathUtils.multiply(journalDetail.getLocalDebitCredit(), journalDetail.getExchangeRateToBase(), 2));
		journalDetail.setPositionCostBasis(TradeBookingUtils.normalizeAmountSign(fill.getTrade(), fill.getNotionalTotalPrice()));

		journalDetail.setTransactionDate(fill.getTrade().getTradeDate());
		journalDetail.setSettlementDate(fill.getTrade().getSettlementDate());
		journalDetail.setFkFieldId(fill.getTrade().getId());

		AccountingJournal journal = new AccountingJournal();
		journal.setJournalType(getAccountingJournalService().getAccountingJournalTypeByName(AccountingJournalType.TRADE_JOURNAL));
		journal.setFkFieldId(fill.getTrade().getId());
		journalDetail.setJournal(journal);

		return journalDetail;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeFillUtilHandler getTradeFillUtilHandler() {
		return this.tradeFillUtilHandler;
	}


	public void setTradeFillUtilHandler(TradeFillUtilHandler tradeFillUtilHandler) {
		this.tradeFillUtilHandler = tradeFillUtilHandler;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingBookingService<TradeFill> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<TradeFill> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
