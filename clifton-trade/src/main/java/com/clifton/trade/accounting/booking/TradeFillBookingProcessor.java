package com.clifton.trade.accounting.booking;


import com.clifton.accounting.gl.booking.processor.TypeBasedRulesBookingProcessor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeServiceImpl;
import com.clifton.trade.search.TradeFillSearchForm;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The <code>TradeFillBookingProcessor</code> class is AccountingBookingProcessor for TradeFill objects.
 * <p/>
 * Booking of trades must be done at Fill level in order to support FIFO position closing across multiple
 * trades with multiple fills on the same date.
 *
 * @author vgomelsky
 */
public class TradeFillBookingProcessor extends TypeBasedRulesBookingProcessor<TradeFill> {

	private TradeService tradeService;

	private List<AccountingBookingRule<TradeFill>> accrualReversalBookingRules;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJournalTypeName() {
		return AccountingJournalType.TRADE_JOURNAL;
	}


	@Override
	public TradeFill getBookableEntity(Serializable id) {
		return getTradeService().getTradeFill((Integer) id);
	}


	@Override
	@ValueIgnoringGetter
	public String getTypeProperty() {
		return "trade.tradeType.name";
	}


	@Override
	public List<TradeFill> getUnbookedEntityList() {
		TradeFillSearchForm searchForm = new TradeFillSearchForm();
		searchForm.setExcludeWorkflowStatusName(TradeService.TRADES_CLOSED_STATUS_NAME);
		return getTradeService().getTradeFillList(searchForm);
	}


	@Override
	protected String getBookingEntityType(TradeFill fill) {
		String tradeType = fill.getTrade().getTradeType().getName();

		// check for investment hierarchy specific overrides first
		InvestmentSecurity security = fill.getTrade().getInvestmentSecurity();
		String hierarchySpecificKey = getBookingRulesKeyOverride(tradeType, security);
		if (hierarchySpecificKey != null) {
			return hierarchySpecificKey;
		}

		return tradeType;
	}


	@Override
	public List<AccountingBookingRule<TradeFill>> getBookingRules(BookingSession<TradeFill> bookingSession) {
		if (bookingSession.getBookableEntity().getBookingDate() != null) {
			return getAccrualReversalBookingRules();
		}
		return super.getBookingRules(bookingSession);
	}


	@Override
	public BookingSession<TradeFill> newBookingSession(AccountingJournal journal, TradeFill fill) {
		if (fill == null) {
			fill = getBookableEntity(journal.getFkFieldId());
		}

		if (StringUtils.isEmpty(fill.getTrade().getDescription())) {
			StringBuilder description = new StringBuilder();
			description.append(fill.getTrade().isBuy() ? "BUY " : "SELL ");
			description.append(CoreMathUtils.formatNumberDecimal(fill.getQuantity()));
			description.append(" ");
			description.append(InvestmentUtils.getQuantityFieldName(fill.getTrade().getInvestmentSecurity()));
			description.append(" of ");
			description.append(fill.getTrade().getInvestmentSecurity().getSymbol());
			journal.setDescription(description.toString());
		}
		return new SimpleBookingSession<>(journal, fill);
	}


	@Override
	public TradeFill markBooked(TradeFill fill) {
		ValidationUtils.assertNotNull(fill.getTrade().getExecutingBrokerCompany(), "Cannot book trade fill with id = " + fill.getId() + " because there is no executing broker on the trade.");

		// set booking date bypassing fill save validation
		if (fill.getBookingDate() == null) {
			fill.setBookingDate(new Date());
			return ((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(fill);
		}

		return fill;
	}


	@Override
	public TradeFill markUnbooked(int sourceEntityId) {
		TradeFill fill = getBookableEntity(sourceEntityId);
		ValidationUtils.assertNotNull(fill, "Cannot find trade fill with id = " + sourceEntityId);

		// clear booking date bypassing fill save validation
		if (fill.getBookingDate() != null) {
			fill.setBookingDate(null);
			return ((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(fill);
		}

		return fill;
	}


	@Override
	public void deleteSourceEntity(int sourceEntityId) {
		throw new ValidationException("Cannot delete an existing trade fill: " + sourceEntityId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public List<AccountingBookingRule<TradeFill>> getAccrualReversalBookingRules() {
		return this.accrualReversalBookingRules;
	}


	public void setAccrualReversalBookingRules(List<AccountingBookingRule<TradeFill>> accrualReversalBookingRules) {
		this.accrualReversalBookingRules = accrualReversalBookingRules;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
