package com.clifton.trade.accounting.booking;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class TradeBookingServiceImpl implements TradeBookingService {

	public TradeService tradeService;
	public WorkflowDefinitionService workflowDefinitionService;
	public WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status tradeBookAndPost(TradeBookingCommand command) {
		List<Trade> tradeList = getTradesToBook(command);

		int bookedCount = 0;
		int errorCount = 0;
		int skipCount = 0;
		int delayBatchCount = 0;
		long totalBookingTimeMillis = 0;
		long minBookingTimeMillis = 0;
		long maxBookingTimeMillis = 0;
		int totalTrades = CollectionUtils.getSize(tradeList);
		Set<String> skipSet = new HashSet<>();
		Status status = (command.getStatusHolder() != null) ? command.getStatusHolder().getStatus() : Status.ofEmptyMessage();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			status.setMessage("Total = " + totalTrades + "; Booked = " + bookedCount + "; Skipped = " + skipCount + "; Failed = " + errorCount + "; Current Trade ID = " + trade.getId());
			String skipIdentifier = getTradeSkipIdentifier(trade);
			try {
				if (skipSet.contains(skipIdentifier)) {
					// if a trade failed for selected client account security, the skip remaining trades to avoid out of order problems
					skipCount++;
				}
				else {
					delayBatchCount++;
					long start = System.currentTimeMillis();

					doBookAndPost(trade);

					long bookingTimeMillis = (System.currentTimeMillis() - start);
					totalBookingTimeMillis += bookingTimeMillis;
					if (bookingTimeMillis > maxBookingTimeMillis) {
						maxBookingTimeMillis = bookingTimeMillis;
					}
					if (minBookingTimeMillis == 0 || minBookingTimeMillis > bookingTimeMillis) {
						minBookingTimeMillis = bookingTimeMillis;
					}
					bookedCount++;
				}
			}
			catch (Throwable e) {
				errorCount++;
				// keep going: don't want to prevent other trades from being booked
				skipSet.add(skipIdentifier);
				String errorMessage = ExceptionUtils.getOriginalMessage(e);
				status.addError(trade + ": " + errorMessage);
				LogUtils.error(getClass(), "Error booking trade " + trade + ": " + errorMessage, e);
			}
			if (delayBatchCount >= command.getBookingCountBeforeDelay()) {
				delayBatchCount = 0;
				if (command.getDelayBetweenBookingsInMilliseconds() > 0) {
					try {
						Thread.sleep(command.getDelayBetweenBookingsInMilliseconds());
					}
					catch (InterruptedException e) {
						// should never be interrupted but it's OK for this job
						Thread.currentThread().interrupt();
					}
				}
			}
		}

		String message = "Finished booking: Total = " + tradeList.size() + ", Booked = " + bookedCount + ", Errors = " + errorCount + ", Skipped = " + skipCount;
		if (bookedCount > 0) {
			message += "\nAvg Time = " + (totalBookingTimeMillis / bookedCount) + "ms, Min Time = " + minBookingTimeMillis + "ms, Max Time = " + maxBookingTimeMillis;
		}
		status.setMessage(message);
		status.setActionPerformed(bookedCount != 0);
		return status;
	}


	private List<Trade> getTradesToBook(TradeBookingCommand command) {
		// get all executed trades (fully executed by haven't been booked)
		TradeSearchForm searchForm = new TradeSearchForm();
		if (StringUtils.isEmpty(command.getWorkflowStateName())) {
			searchForm.setWorkflowStateNames(new String[]{TradeService.TRADE_EXECUTED_VALID_STATE_NAME, TradeService.TRADE_UNBOOKED_STATE_NAME});
		}
		else {
			searchForm.setWorkflowStateNameEquals(command.getWorkflowStateName());
		}
		if (command.getReconciled() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("reconciled", ComparisonConditions.EQUALS, command.getReconciled()));
		}
		searchForm.setIds(command.getTradeIds());
		searchForm.setSecurityId(command.getSecurityId());
		searchForm.setInvestmentGroupId(command.getInvestmentGroupId());
		searchForm.setClientInvestmentAccountId(command.getClientInvestmentAccountId());
		searchForm.setClientInvestmentAccountGroupId(command.getClientInvestmentAccountGroupId());
		if (command.getFromTradeDate() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, command.getFromTradeDate()));
		}
		if (command.getToTradeDate() != null) {
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, command.getToTradeDate()));
		}

		// sorting to avoid out of order same date bookings (optional parameters)
		searchForm.setOrderBy("clientInvestmentAccountId#settlementDate#securityId#averageUnitPrice");

		return getTradeService().getTradeList(searchForm);
	}


	@Transactional(timeout = 30)
	protected void doBookAndPost(Trade trade) {
		WorkflowState bookedState = getWorkflowDefinitionService().getWorkflowStateByName(trade.getWorkflowState().getWorkflow().getId(), TradeService.TRADE_BOOKED_STATE_NAME);
		getWorkflowTransitionService().executeWorkflowTransition(Trade.class.getSimpleName(), trade.getId(), bookedState.getId());
	}


	/**
	 * Returns Client Account + Security ID for the specified trade.
	 */
	private String getTradeSkipIdentifier(Trade trade) {
		StringBuilder result = new StringBuilder(16);
		result.append(trade.getClientInvestmentAccount().getId());
		result.append('-');
		result.append(trade.getInvestmentSecurity().getId());
		return result.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}
