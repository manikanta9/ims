package com.clifton.trade.accounting.booking.rule;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.trade.TradeFill;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class TradeAccrualReversalBookingRule extends AccountingCommonBookingRule<TradeFill> implements AccountingBookingRule<TradeFill> {

	private AccountingJournalService accountingJournalService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<TradeFill> bookingSession) {
		TradeFill tradeFill = bookingSession.getBookableEntity();
		if (tradeFill.getInvestmentSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			if (tradeFill.getTrade().getActualSettlementDate() != null) {
				applyReversalRule(bookingSession);
			}
		}
	}


	private void applyReversalRule(BookingSession<com.clifton.trade.TradeFill> bookingSession) {
		final TradeFill tradeFill = bookingSession.getBookableEntity();
		final Date actualSettlementDate = tradeFill.getTrade().getActualSettlementDate();
		if (actualSettlementDate != null) {
			AccountingJournal journal = bookingSession.getJournal();
			AccountingJournal parent = getAccountingJournal(tradeFill);
			if (parent != null) {
				journal.setParent(parent);
				AccountingAccount receivable = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE);
				List<? extends AccountingJournalDetailDefinition> details = getOpenReceivableList(tradeFill);
				for (AccountingJournalDetailDefinition detail : details) {
					AccountingJournalDetail reversalEntry = new AccountingJournalDetail();
					BeanUtils.copyProperties(detail, reversalEntry);
					reversalEntry.setAccountingAccount(receivable);
					reversalEntry.setLocalDebitCredit(detail.getLocalDebitCredit().negate());
					reversalEntry.setBaseDebitCredit(detail.getBaseDebitCredit().negate());
					reversalEntry.setSettlementDate(actualSettlementDate);
					reversalEntry.setTransactionDate(actualSettlementDate);
					reversalEntry.setDescription("Reverse Accrual " + detail.getAccountingAccount().getName());

					AccountingJournalDetail cashEntry = new AccountingJournalDetail();
					BeanUtils.copyProperties(detail, cashEntry);
					cashEntry.setAccountingAccount(getCashAccountingAccount(detail));
					cashEntry.setDescription(detail.getDescription().replaceFirst("Accrual ", ""));
					cashEntry.setSettlementDate(actualSettlementDate);
					cashEntry.setTransactionDate(actualSettlementDate);

					journal.addJournalDetail(reversalEntry);
					journal.addJournalDetail(cashEntry);
				}
			}
		}
	}


	private AccountingJournal getAccountingJournal(TradeFill tradeFill) {
		AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
		searchForm.setSystemTableName(TradeFill.TABLE_NAME);
		searchForm.setFkFieldId(tradeFill.getId());
		searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
		searchForm.setOrderBy("id:asc");
		List<AccountingJournal> journalList = getAccountingJournalService().getAccountingJournalList(searchForm);
		if (CollectionUtils.isEmpty(journalList)) {
			throw new ValidationException("Cannot find original accrual journal for trade journal id = " + tradeFill.getId());
		}
		if (CollectionUtils.getSize(journalList) > 1) {
			throw new ValidationException("Cannot have more than one journal when generating Accrual Reversal journal for trade journal id = " + tradeFill.getId());
		}
		return CollectionUtils.getOnlyElement(journalList);
	}


	private List<? extends AccountingJournalDetailDefinition> getOpenReceivableList(TradeFill tradeFill) {
		AccountingJournal journal = getAccountingJournal(tradeFill);
		journal = getAccountingJournalService().getAccountingJournal(journal.getId());
		List<? extends AccountingJournalDetailDefinition> receivables = CollectionUtils.getStream(journal.getJournalDetailList())
				.filter(d -> AccountingAccount.ASSET_PAYMENT_RECEIVABLE.equals(d.getAccountingAccount().getName()))
				.filter(d -> !MathUtils.isNullOrZero(d.getLocalDebitCredit()))
				.collect(Collectors.toList());
		BigDecimal balance = receivables.stream()
				.map(AccountingJournalDetailDefinition::getLocalDebitCredit)
				.reduce(BigDecimal.ZERO, MathUtils::add);
		if (!MathUtils.isNullOrZero(balance)) {
			return receivables;
		}
		return Collections.emptyList();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}
}
