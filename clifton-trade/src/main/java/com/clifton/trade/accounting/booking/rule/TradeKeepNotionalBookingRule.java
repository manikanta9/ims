package com.clifton.trade.accounting.booking.rule;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.ResetTypes;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.TradeFill;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * The <code>TradeKeepNotionalBookingRule</code> class will adjust the gain loss and correct the cost basis for
 * securities that the have a reset type of KEEP_NOTIONAL.  To do this, if there is a partial close, the remainder
 * will be fully closed and reopened with correct units to adjust the cost basis.  It will also, find any Gain/Loss
 * (and corresponding cash) transaction and adjust it to the correct amount.
 * <p>
 * <b>Gain/Loss Calculation:</b>
 * <p>
 * <b>[Original Cost Basis] * ( 1 - [New Price]/[Old Price])</b>
 * <p>
 * Where the <b>[Original Cost Basis]</b> is the <b>[Price] * [Quantity]</b> of the closing transaction
 * which is also the notionalTotalPrice on the trade fill.
 *
 * @author mwacker
 */
public class TradeKeepNotionalBookingRule implements AccountingBookingRule<TradeFill> {

	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, then existing lookup will use Settlement vs Transaction date. False by default.
	 */
	private boolean lookupUsingSettlementDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<BookingRuleScopes> getRuleScopes() {
		return CollectionUtils.createHashSet(BookingRuleScopes.POSITION_IMPACT);
	}


	@Override
	public void applyRule(BookingSession<TradeFill> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		ValidationUtils.assertNotEmpty(journal.getJournalDetailList(), "At least one detail must be present for journal: " + journal.getLabel());
		List<AccountingJournalDetailDefinition> detailList = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailList)) {
			if (isPartialClose(detail) && isKeepNotional(detail.getInvestmentSecurity())) {
				closeLotRemainder(journal, bookingSession, detail);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void closeLotRemainder(AccountingJournal journal, BookingSession<TradeFill> bookingSession, AccountingJournalDetailDefinition closingDetail) {
		TradeFill tradeFill = bookingSession.getBookableEntity();
		AccountingPosition bookingPositionBeingClosed = getAccountingPosition(closingDetail.getParentTransaction());

		// get the cost basis without the gain loss
		BigDecimal originalDetailCostBasis = MathUtils.isSameSignum(closingDetail.getPositionCostBasis(), tradeFill.getNotionalTotalPrice()) ? tradeFill.getNotionalTotalPrice() : tradeFill.getNotionalTotalPrice().negate();
		// get the correct gain loss for KEEP_NOTIONAL_ADJUST_UNITS
		BigDecimal gainLoss = getActualGainLoss(bookingPositionBeingClosed, closingDetail, originalDetailCostBasis);


		// close remaining position at cost
		AccountingJournalDetail remainder = new AccountingJournalDetail();
		AccountingTransaction t = getAccountingTransactionService().getAccountingTransaction(closingDetail.getParentTransaction().getId());
		BeanUtils.copyProperties(t, remainder);
		remainder.setParentTransaction(t);
		remainder.setSystemTable(closingDetail.getSystemTable());
		remainder.setFkFieldId(closingDetail.getFkFieldId());
		remainder.setTransactionDate(closingDetail.getTransactionDate());
		remainder.setSettlementDate(closingDetail.getSettlementDate());
		remainder.setOriginalTransactionDate(closingDetail.getOriginalTransactionDate());
		remainder.setOpening(false);
		remainder.setDescription("Full close of remaining lot for " + closingDetail.getInvestmentSecurity().getSymbol());

		remainder.setLocalDebitCredit(BigDecimal.ZERO);
		remainder.setBaseDebitCredit(BigDecimal.ZERO);
		remainder.setPrice(t.getPrice());
		remainder.setQuantity(MathUtils.add(closingDetail.getQuantity(), bookingPositionBeingClosed.getRemainingQuantityClean()).negate());
		remainder.setPositionCostBasis(MathUtils.add(originalDetailCostBasis, MathUtils.add(bookingPositionBeingClosed.getRemainingCostBasis(), gainLoss)).negate());

		journal.addJournalDetail(remainder);

		// open a new lot with the correct units
		AccountingJournalDetail opening = new AccountingJournalDetail();
		BeanUtils.copyProperties(remainder, opening);
		opening.setOriginalTransactionDate(closingDetail.getParentTransaction().getOriginalTransactionDate());
		opening.setOpening(true);
		opening.setDescription("Position opening of " + closingDetail.getInvestmentSecurity().getSymbol());
		opening.setParentDefinition(null);
		opening.setParentTransaction(null);

		opening.setPositionCostBasis(MathUtils.add(bookingPositionBeingClosed.getRemainingCostBasis(), originalDetailCostBasis));

		opening.setQuantity(getUnitsScaleBasedOffNotional(MathUtils.divide(opening.getPositionCostBasis(), opening.getPrice()), opening));
		journal.addJournalDetail(opening);


		// adjust the cost basis of the original closing lot
		closingDetail.setPositionCostBasis(MathUtils.add(gainLoss, originalDetailCostBasis));
		adjustGainLossAndCashEntries(bookingSession, closingDetail, gainLoss);
	}


	private void adjustGainLossAndCashEntries(BookingSession<TradeFill> bookingSession, AccountingJournalDetailDefinition detail, BigDecimal gainLoss) {
		InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(detail.getInvestmentSecurity().getInstrument().getTradingCurrency());

		AccountingJournalDetailDefinition gainLossDetail = getGainLossDetail(bookingSession, detail);
		if (gainLossDetail != null) {
			BigDecimal originalLocalGainLoss = gainLossDetail.getLocalDebitCredit();

			gainLossDetail.setLocalDebitCredit(gainLoss.negate());
			gainLossDetail.setBaseDebitCredit(roundingCalculator.round(MathUtils.multiply(gainLossDetail.getLocalDebitCredit(), gainLossDetail.getExchangeRateToBase())));

			AccountingJournalDetailDefinition gainLossCashDetail = getGainLossCashDetail(bookingSession, detail);
			if (gainLossCashDetail != null) {
				gainLossCashDetail.setLocalDebitCredit(MathUtils.add(MathUtils.subtract(gainLossCashDetail.getLocalDebitCredit(), originalLocalGainLoss.negate()), gainLoss));
				gainLossCashDetail.setBaseDebitCredit(roundingCalculator.round(MathUtils.multiply(gainLossCashDetail.getLocalDebitCredit(), gainLossCashDetail.getExchangeRateToBase())));
			}
		}
	}


	private BigDecimal getActualGainLoss(AccountingPosition bookingPositionBeingClosed, AccountingJournalDetailDefinition detail, BigDecimal originalDetailCostBasis) {
		BigDecimal closingPortion = MathUtils.divide(detail.getPrice(), bookingPositionBeingClosed.getPrice());
		BigDecimal closingNotional = MathUtils.multiply(originalDetailCostBasis, closingPortion, 2);
		return MathUtils.subtract(originalDetailCostBasis, closingNotional);
	}


	private AccountingPosition getAccountingPosition(AccountingTransaction transaction) {
		AccountingPositionCommand command = isLookupUsingSettlementDate() ? AccountingPositionCommand.onPositionSettlementDate(transaction.getSettlementDate()) : AccountingPositionCommand.onPositionTransactionDate(transaction.getTransactionDate());
		command.setExistingPositionId(transaction.getId());
		return CollectionUtils.getFirstElementStrict(getAccountingPositionService().getAccountingPositionListUsingCommand(command));
	}


	private boolean isPartialClose(AccountingJournalDetailDefinition detail) {
		AccountingAccount accountingAccount = detail.getAccountingAccount();
		if (accountingAccount.isPosition()) {
			// must be a closing position
			if (detail.getParentTransaction() != null && detail.getInvestmentSecurity().equals(detail.getParentTransaction().getInvestmentSecurity())
					&& !MathUtils.isEqual(detail.getQuantity().abs(), detail.getParentTransaction().getQuantity().abs())) {
				return true;
			}
		}
		return false;
	}


	private boolean isKeepNotional(InvestmentSecurity security) {
		// check if something needs to be adjusted based on reset type
		String resetTypeValue = (String) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
				InvestmentSecurity.CUSTOM_FIELD_RESET_TYPE, false);
		if (resetTypeValue != null) {
			ResetTypes resetType = ResetTypes.valueOf(resetTypeValue.toUpperCase());
			return resetType == ResetTypes.KEEP_NOTIONAL;
		}
		return false;
	}


	private AccountingJournalDetailDefinition getGainLossDetail(BookingSession<TradeFill> bookingSession, AccountingJournalDetailDefinition partialClose) {
		AccountingJournal journal = bookingSession.getJournal();
		List<AccountingJournalDetailDefinition> detailList = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailList)) {
			if (detail.getAccountingAccount().isGainLoss() && detail.getParentDefinition().equals(partialClose)) {
				return detail;
			}
		}
		return null;
	}


	private AccountingJournalDetailDefinition getGainLossCashDetail(BookingSession<TradeFill> bookingSession, AccountingJournalDetailDefinition partialClose) {
		AccountingJournal journal = bookingSession.getJournal();
		List<AccountingJournalDetailDefinition> detailList = new ArrayList<>(journal.getJournalDetailList()); //Copy to prevent concurrent modification

		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(detailList)) {
			if (detail.getAccountingAccount().isCash() && detail.getParentDefinition().equals(partialClose)) {
				return detail;
			}
		}
		return null;
	}


	/**
	 * Calculate the least precise units to get to the correct notional amount.
	 */
	private BigDecimal getUnitsScaleBasedOffNotional(BigDecimal newUnits, AccountingJournalDetailDefinition detail) {
		int precision = 10;
		while (precision >= 0) {
			BigDecimal adjustedUnits = newUnits.setScale(precision, RoundingMode.HALF_UP);
			BigDecimal newNotional = getInvestmentCalculator().calculateNotional(detail.getInvestmentSecurity(), detail.getPrice(), adjustedUnits, BigDecimal.ONE);
			if (!MathUtils.isEqual(newNotional, detail.getPositionCostBasis())) {
				if (precision == 10) {
					throw new ValidationException("New notional " + newNotional + " calculated using new units " + newUnits + " does not match old notional "
							+ detail.getPositionCostBasis());
				}
				break;
			}
			precision--;
		}
		return newUnits.setScale(precision + 1, RoundingMode.HALF_UP);
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public boolean isLookupUsingSettlementDate() {
		return this.lookupUsingSettlementDate;
	}


	public void setLookupUsingSettlementDate(boolean lookupUsingSettlementDate) {
		this.lookupUsingSettlementDate = lookupUsingSettlementDate;
	}
}
