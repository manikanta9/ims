package com.clifton.trade.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookingPreviewCommand;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Service
public class TradeCommissionServiceImpl implements TradeCommissionService {

	private AdvancedUpdatableDAO<TradeCommissionOverride, Criteria> tradeCommissionOverrideDAO;

	private AccountingBookingService<TradeFill> accountingBookingService;
	private AccountingJournalService accountingJournalService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentCalculator investmentCalculator;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeCommissionOverride getTradeCommissionOverride(int id) {
		return getTradeCommissionOverrideDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeCommissionOverride> getTradeCommissionOverrideListForTrade(int tradeId) {
		//Otherwise, return the result from the database
		TradeCommissionOverrideSearchForm searchForm = new TradeCommissionOverrideSearchForm();
		searchForm.setTradeId(tradeId);
		return getTradeCommissionOverrideList(searchForm);
	}


	@Override
	public List<TradeCommissionOverride> getTradeCommissionOverrideList(TradeCommissionOverrideSearchForm searchForm) {
		return getTradeCommissionOverrideDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeCommissionOverride getTradeCommissionOverrideForTradeCommissionAccountingAccount(Trade trade) {
		AccountingAccount commissionAccount = trade.getTradeType().getCommissionAccountingAccount();
		if (commissionAccount != null) {
			return getTradeCommissionOverrideForTradeAndAccountingAccount(trade, commissionAccount);
		}

		return null;
	}


	@Override
	public TradeCommissionOverride getTradeCommissionOverrideForTradeFeeAccountingAccount(Trade trade) {
		AccountingAccount feeAccount = trade.getTradeType().getFeeAccountingAccount();
		if (feeAccount != null) {
			return getTradeCommissionOverrideForTradeAndAccountingAccount(trade, feeAccount);
		}

		return null;
	}


	@Override
	public TradeCommissionOverride getTradeCommissionOverrideForTradeAndAccountingAccount(Trade trade, AccountingAccount accountingAccount) {
		TradeCommissionOverrideSearchForm searchForm = new TradeCommissionOverrideSearchForm();
		searchForm.setTradeId(trade.getId());
		searchForm.setAccountingAccountId(accountingAccount.getId());

		return CollectionUtils.getFirstElement(getTradeCommissionOverrideList(searchForm));
	}


	/*
	 * Transactional is needed so the Trade Commission Override and possible Trade save are grouped as a unit of work by Hibernate.
	 */
	@Override
	@Transactional
	public void saveTradeCommissionOverride(TradeCommissionOverride tradeCommissionOverride) {
		Trade trade = tradeCommissionOverride.getTrade();
		if (saveTradeCommissionOverrideForTrade(trade, tradeCommissionOverride, false)) {
			getTradeService().saveTrade(trade);
		}
	}


	/**
	 * Saves the Trade Commission Override and returns <code>true</code> if the trade was modified
	 * by the override's values.
	 */
	private boolean saveTradeCommissionOverrideForTrade(Trade trade, TradeCommissionOverride tradeCommissionOverride, boolean updateExisting) {
		Integer tradeId = trade.getId();

		BigDecimal amountPerUnit = tradeCommissionOverride.getAmountPerUnit();
		BigDecimal totalAmount = tradeCommissionOverride.getTotalAmount();

		ValidationUtils.assertFalse(getTradeService().isTradeFullyBooked(tradeId), "Cannot save commission override for a trade that is already booked.");

		if (amountPerUnit == null && totalAmount == null) {
			throw new ValidationException("Must set either 'Amount per unit' and 'total amount'.");
		}

		if (amountPerUnit != null && totalAmount != null) {
			throw new ValidationException("'Amount per unit' and 'total amount' cannot both be set.");
		}

		for (TradeCommissionOverride existingOverride : CollectionUtils.getIterable(getTradeCommissionOverrideListForTrade(tradeId))) {
			boolean duplicateFound = !tradeCommissionOverride.equals(existingOverride) && tradeCommissionOverride.getAccountingAccount().equals(existingOverride.getAccountingAccount());
			if (duplicateFound) {
				if (!updateExisting || !existingOverride.getId().equals(tradeCommissionOverride.getId())) {
					throw new ValidationException("A commission override with the same expense accounting account already exists for this trade.");
				}
			}
		}

		//Keep overrides on the Trade itself in sync
		boolean saveTrade = false;
		if (tradeCommissionOverride.getAccountingAccount().equals(trade.getTradeType().getCommissionAccountingAccount())) {
			if (amountPerUnit != null) {
				if (!MathUtils.isEqual(amountPerUnit, trade.getCommissionPerUnit())) {
					trade.setCommissionPerUnit(amountPerUnit);
					saveTrade = true;
					// on update, may need to clear the flat commission amount
					if (!MathUtils.isNullOrZero(trade.getCommissionAmount())) {
						trade.setCommissionAmount(null);
					}
				}
			}
			else {
				if (!MathUtils.isEqual(totalAmount, trade.getCommissionAmount())) {
					trade.setCommissionAmount(totalAmount);
					saveTrade = true;
					// on update, may need to clear commission per unit
					if (!MathUtils.isNullOrZero(trade.getCommissionPerUnit())) {
						trade.setCommissionPerUnit(null);
					}
				}
			}
		}
		else if (tradeCommissionOverride.getAccountingAccount().equals(trade.getTradeType().getFeeAccountingAccount())) {
			if (totalAmount != null && !MathUtils.isEqual(totalAmount, trade.getFeeAmount())) {
				trade.setFeeAmount(totalAmount);
				saveTrade = true;
			}
		}

		getTradeCommissionOverrideDAO().save(tradeCommissionOverride);
		return saveTrade;
	}


	@Override
	@Transactional
	public void saveTradeCommissionOverrideListForTrade(Trade trade, List<TradeCommissionOverride> tradeCommissionOverrideList) {
		boolean tradeModified = false;
		for (TradeCommissionOverride tradeCommissionOverride : CollectionUtils.getIterable(tradeCommissionOverrideList)) {
			boolean commissionModifiedTrade = saveTradeCommissionOverrideForTrade(trade, tradeCommissionOverride, true);
			tradeModified = tradeModified || commissionModifiedTrade;
		}
		if (tradeModified) {
			/*
			 * Copy saved state into provided bean. Thus, the provided bean reference is not changed.
			 * The javadoc comment on the interface, says the trade attributes will be modified. Trade
			 * is row versioned, so getting the saved state is important.
			 */
			BeanUtils.copyProperties(getTradeService().saveTrade(trade), trade);
		}
	}


	@Override
	public TradeCommissionOverride createOrUpdateTradeCommissionOverride(Trade trade, AccountingAccount account, BigDecimal totalAmount, BigDecimal amountPerUnit) {
		if (totalAmount != null && amountPerUnit != null) {
			throw new ValidationException("Either 'totalAmount' or 'amountPerUnit' should be null.");
		}

		TradeCommissionOverride existingOverride = getTradeCommissionOverrideForTradeAndAccountingAccount(trade, account);

		TradeCommissionOverride overrideToSave;
		if (existingOverride != null) {
			overrideToSave = existingOverride;
		}
		else {
			overrideToSave = new TradeCommissionOverride();
			overrideToSave.setTrade(trade);
			overrideToSave.setAccountingAccount(account);
		}

		overrideToSave.setTotalAmount(totalAmount);
		overrideToSave.setAmountPerUnit(amountPerUnit);
		saveTradeCommissionOverride(overrideToSave);

		return overrideToSave;
	}


	@Override
	public void deleteTradeCommissionOverride(int id) {
		TradeCommissionOverride override = getTradeCommissionOverride(id);

		ValidationUtils.assertNotNull(override, "Bean to delete cannot be null.");

		List<TradeFill> fills = getTradeService().getTradeFillListByTrade(override.getTrade().getId());
		for (TradeFill fill : CollectionUtils.getIterable(fills)) {
			ValidationUtils.assertNull(fill.getBookingDate(), "Cannot delete commission override for a trade that is already booked.");
		}

		Trade trade = override.getTrade();
		//Keep overrides on the Trade itself in sync
		boolean saveTrade = false;
		BigDecimal amountPerUnit = override.getAmountPerUnit();
		BigDecimal totalAmount = override.getTotalAmount();
		if (override.getAccountingAccount().equals(trade.getTradeType().getCommissionAccountingAccount())) {
			if (amountPerUnit != null && trade.getCommissionPerUnit() != null) {
				trade.setCommissionPerUnit(null);
				saveTrade = true;
			}
			else if (trade.getCommissionAmount() != null) {
				trade.setCommissionAmount(null);
				saveTrade = true;
			}
		}
		else if (override.getAccountingAccount().equals(trade.getTradeType().getFeeAccountingAccount())) {
			if (totalAmount != null && trade.getFeeAmount() != null) {
				trade.setFeeAmount(null);
				saveTrade = true;
			}
		}

		//Save the trade if changed (Note: We do this before saving the TradeCommissionOverride in case there is an error when saving the trade)
		if (saveTrade) {
			getTradeService().saveTrade(trade);
		}
		getTradeCommissionOverrideDAO().delete(id);
	}


	@Override
	public List<TradeCommission<?>> getTradeCommissionListByTrade(int tradeId) {
		List<TradeCommission<?>> previewedCommissions = getTradeCommissionListExpandedByTrade(tradeId);
		List<TradeCommission<?>> summarizedCommissions = new ArrayList<>();

		Map<TradeCommissionKey, BigDecimal> totals = new HashMap<>();
		for (TradeCommission<?> commission : CollectionUtils.getIterable(previewedCommissions)) {
			if (!MathUtils.isNullOrZero(commission.getTotalAmount()) && commission.getSource() != TradeCommissionSources.OVERRIDE) {
				TradeCommissionKey key = new TradeCommissionKey(commission.getAccountingAccount(), commission.getSource());
				if (totals.containsKey(key)) {
					BigDecimal newTotal = MathUtils.add(totals.get(key), commission.getTotalAmount());
					totals.put(key, newTotal);
				}
				else {
					totals.put(key, commission.getTotalAmount());
				}
			}
			else {
				summarizedCommissions.add(commission);
			}
		}

		for (Map.Entry<TradeCommissionKey, BigDecimal> tradeCommissionKeyBigDecimalEntry : totals.entrySet()) {
			summarizedCommissions.add(TradeCommission.createForCommissionSummary((tradeCommissionKeyBigDecimalEntry.getKey()).source, (tradeCommissionKeyBigDecimalEntry.getKey()).accountingAccount, tradeCommissionKeyBigDecimalEntry.getValue()));
		}

		return summarizedCommissions;
	}


	@Override
	public List<TradeCommission<?>> getTradeCommissionListExpandedByTrade(int tradeId) {
		Trade trade = getTradeService().getTrade(tradeId);
		List<TradeFill> tradeFillList = trade.getTradeFillList();

		//If there is no TradeFill, then we create a "dummy" fill
		if (CollectionUtils.isEmpty(tradeFillList)) {
			if (tradeFillList == null) {
				tradeFillList = new ArrayList<>();
				trade.setTradeFillList(tradeFillList);
			}
			TradeFill fill = createTradeFillForPreview(trade);
			tradeFillList.add(fill);
		}

		return getCommissions(trade);
	}


	private TradeFill createTradeFillForPreview(Trade trade) {
		TradeFill fill = new TradeFill();
		fill.setId(-100);
		fill.setTrade(trade);
		fill.setQuantity(trade.getQuantityIntended());
		fill.setNotionalUnitPrice(trade.getAverageOrExpectedUnitPrice());

		fill.setNotionalTotalPrice(getInvestmentCalculator().calculateNotional(trade.getInvestmentSecurity(), fill.getNotionalUnitPrice(), fill.getQuantity(), trade.getNotionalMultiplier()));
		if (InvestmentUtils.isNoPaymentOnOpen(trade.getInvestmentSecurity())) {
			fill.setPaymentUnitPrice(BigDecimal.ZERO);
			fill.setPaymentTotalPrice(BigDecimal.ZERO);
		}
		else {

			fill.setPaymentUnitPrice(fill.getNotionalUnitPrice());
			fill.setPaymentTotalPrice(fill.getNotionalTotalPrice());
		}

		return fill;
	}


	private List<TradeCommission<?>> getCommissions(Trade trade) {
		List<TradeCommission<?>> tradeCommissions = new ArrayList<>();

		//Add the commission overrides
		List<TradeCommissionOverride> commissionOverrideList = getTradeCommissionOverrideListForTrade(trade.getId());
		addCommissionOverrides(commissionOverrideList, tradeCommissions);

		//For trade fills without a booking date, add the previewed commissions, otherwise add the fill ID to a list of booked fill IDs
		Set<Integer> bookedFillIdList = new HashSet<>();
		for (TradeFill fill : CollectionUtils.getIterable(trade.getTradeFillList())) {
			if (fill.getBookingDate() == null) {
				addPreviewedCommissionsForTradeFill(fill, tradeCommissions, commissionOverrideList);
			}
			else {
				bookedFillIdList.add(fill.getId());
			}
		}

		if (!CollectionUtils.isEmpty(bookedFillIdList)) {
			//Get the journals and their IDs for the fills
			List<AccountingJournal> journalList = getJournalListForTradeFills(bookedFillIdList);
			Long[] journalIds = BeanUtils.getBeanIdentityArray(journalList, Long.class);

			//Add the actual commissions
			addActualCommissions(journalIds, tradeCommissions, commissionOverrideList);

			//Add the commission adjustments
			addCommissionAdjustments(journalIds, tradeCommissions);
		}

		return tradeCommissions;
	}


	private List<AccountingJournal> getJournalListForTradeFills(Collection<Integer> fills) {
		AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
		searchForm.setSystemTableName(TradeFill.TABLE_NAME);
		searchForm.setFkFieldIdList(fills.toArray(new Integer[fills.size()]));
		searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);

		return getAccountingJournalService().getAccountingJournalList(searchForm);
	}


	private List<TradeCommissionOverride> addCommissionOverrides(List<TradeCommissionOverride> commissionOverrideList, List<TradeCommission<?>> commissions) {
		for (TradeCommissionOverride override : CollectionUtils.getIterable(commissionOverrideList)) {
			TradeCommission<Integer> tradeCommission = TradeCommission.createForCommissionOverride(override);
			commissions.add(tradeCommission);
		}

		return commissionOverrideList;
	}


	private void addActualCommissions(Long[] journalIds, List<TradeCommission<?>> commissions, List<TradeCommissionOverride> overrides) {
		Set<AccountingAccount> accountsToSkip = new HashSet<>();
		for (TradeCommissionOverride override : CollectionUtils.getIterable(overrides)) {
			accountsToSkip.add(override.getAccountingAccount());
		}

		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setJournalIdList(journalIds);
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.COMMISSION_ACCOUNTS);
		if (!CollectionUtils.isEmpty(accountsToSkip)) {
			searchForm.setExcludeAccountingAccountIds(BeanUtils.getBeanIdentityArray(accountsToSkip, Short.class));
		}

		List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		for (AccountingTransaction transaction : CollectionUtils.getIterable(transactionList)) {
			TradeFill fill = getTradeService().getTradeFill(transaction.getFkFieldId());
			TradeCommission<Long> commission = TradeCommission.createForActualCommission(transaction, fill);
			commissions.add(commission);
		}
	}


	private void addCommissionAdjustments(Long[] journalIds, List<TradeCommission<?>> commissions) {
		AccountingTransactionSearchForm positionSearchForm = new AccountingTransactionSearchForm();
		positionSearchForm.setJournalIdList(journalIds);
		positionSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);

		List<AccountingTransaction> positions = getAccountingTransactionService().getAccountingTransactionList(positionSearchForm);

		if (!positions.isEmpty()) {
			Long[] positionIds = BeanUtils.getBeanIdentityArray(positions, Long.class);

			AccountingTransactionSearchForm adjustmentSearchForm = new AccountingTransactionSearchForm();
			adjustmentSearchForm.setParentIdList(positionIds);
			adjustmentSearchForm.setExcludeJournalIdList(journalIds);
			adjustmentSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.COMMISSION_ACCOUNTS);

			List<AccountingTransaction> adjustments = getAccountingTransactionService().getAccountingTransactionList(adjustmentSearchForm);

			for (AccountingTransaction adjustment : CollectionUtils.getIterable(adjustments)) {
				TradeFill fill = null;
				if (adjustment.getFkFieldId() != null) {
					fill = getTradeService().getTradeFill(adjustment.getFkFieldId());
				}

				TradeCommission<Long> tradeCommission = TradeCommission.createForCommissionAdjustment(adjustment, fill);
				commissions.add(tradeCommission);
			}
		}
	}


	private void addPreviewedCommissionsForTradeFill(TradeFill fill, List<TradeCommission<?>> commissionListForTrade, List<TradeCommissionOverride> commissionOverrideList) {
		AccountingJournal journal = getAccountingBookingService().getAccountingJournalPreview(BookingPreviewCommand.of(fill, AccountingJournalType.TRADE_JOURNAL));
		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(journal.getJournalDetailList())) {
			AccountingAccount glAccount = detail.getAccountingAccount();
			if (glAccount.isCommission() && !commissionOverrideListContainsAccountingAccount(commissionOverrideList, glAccount)) {
				TradeCommission<Object> previewedCommission = TradeCommission.createForCommissionPreview(detail, fill, "Previewed commission/fee");
				commissionListForTrade.add(previewedCommission);
			}
		}
	}


	private boolean commissionOverrideListContainsAccountingAccount(List<TradeCommissionOverride> commissionOverrideList, AccountingAccount accountingAccount) {
		for (TradeCommissionOverride override : CollectionUtils.getIterable(commissionOverrideList)) {
			if (override.getAccountingAccount().equals(accountingAccount)) {
				return true;
			}
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeCommissionOverride, Criteria> getTradeCommissionOverrideDAO() {
		return this.tradeCommissionOverrideDAO;
	}


	public void setTradeCommissionOverrideDAO(AdvancedUpdatableDAO<TradeCommissionOverride, Criteria> tradeCommissionOverrideDAO) {
		this.tradeCommissionOverrideDAO = tradeCommissionOverrideDAO;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingBookingService<TradeFill> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<TradeFill> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	private static class TradeCommissionKey {

		final AccountingAccount accountingAccount;
		final TradeCommissionSources source;


		public TradeCommissionKey(AccountingAccount accountingAccount, TradeCommissionSources source) {
			this.accountingAccount = accountingAccount;
			this.source = source;
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.accountingAccount == null) ? 0 : this.accountingAccount.hashCode());
			result = prime * result + ((this.source == null) ? 0 : this.source.hashCode());
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			TradeCommissionKey other = (TradeCommissionKey) obj;
			if (this.accountingAccount == null) {
				if (other.accountingAccount != null) {
					return false;
				}
			}
			else if (!this.accountingAccount.equals(other.accountingAccount)) {
				return false;
			}
			return this.source == other.source;
		}
	}
}
