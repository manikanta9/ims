package com.clifton.trade.accounting.position;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.booking.BookingPreviewCommand;
import com.clifton.accounting.gl.booking.rule.BookingRuleScopes;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentTransactionInfo;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.calculator.accrual.AccrualBasis;
import com.clifton.investment.instrument.calculator.accrual.AccrualDates;
import com.clifton.investment.instrument.calculator.accrual.AccrualDatesCommand;
import com.clifton.investment.instrument.calculator.accrual.AccrualHolder;
import com.clifton.investment.instrument.calculator.accrual.date.AccrualDateCalculators;
import com.clifton.investment.instrument.calculator.accrual.sign.AccrualSignCalculators;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * NOTE: most likely need to change the architecture of this service. Should be in accounting and should process
 * self-registered processors from each module (trade, transfer, etc.) that will keep adding to quantities/notionals.
 */
@Service
public class TradePositionServiceImpl implements TradePositionService {

	private AccountingBookingService<TradeFill> accountingBookingService;
	private AccountingPositionService accountingPositionService;
	private AccountingTransactionService accountingTransactionService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;
	private MarketDataRetriever marketDataRetriever;
	private SystemColumnValueHandler systemColumnValueHandler;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getSharesBeforeTrade(AccountingBean bean, boolean useSettlementDate) {
		Integer holdingAccountId = bean.getHoldingInvestmentAccount() != null ? bean.getHoldingInvestmentAccount().getId() : null;
		// Get a list of all open trades for this client account/holding account/security
		List<Trade> openTrades = getTradeService().getTradePendingList(bean.getClientInvestmentAccount().getId(), holdingAccountId, bean.getInvestmentSecurity().getId());

		// Sum the quantity value for each open trade
		BigDecimal openTradePosition = BigDecimal.ZERO;
		for (Trade openTrade : CollectionUtils.getIterable(openTrades)) {
			if (!openTrade.equals(bean) && (bean.isCollateral() == openTrade.isCollateralTrade())) {
				openTradePosition = openTrade.isBuy() ? MathUtils.add(openTradePosition, openTrade.getQuantityNormalized()) : MathUtils.subtract(openTradePosition, openTrade.getQuantityNormalized());
			}
		}

		// Get the Current Accounting Position for this client/holding acct/security
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListUsingCommand(
				AccountingPositionCommand.onDate(useSettlementDate, bean.getTransactionDate(), bean.getSettlementDate())
						.forClientAccount(bean.getClientInvestmentAccount().getId())
						.forHoldingAccount(holdingAccountId)
						.forInvestmentSecurity(bean.getInvestmentSecurity().getId())
						.excludeReceivableGLAccounts()
		);

		// only include collateral or non-collateral trades
		positionList = BeanUtils.filter(positionList, AccountingPosition::isCollateral, bean.isCollateral());

		// If we have a position, add it to the open trade quantity
		if ((positionList != null) && (!positionList.isEmpty())) {
			BigDecimal openPosition = CoreMathUtils.sumProperty(positionList, AccountingPosition::getRemainingQuantity);
			openTradePosition = openTradePosition.add(openPosition);
		}
		// Return the sum of current closed plus pending
		return openTradePosition;
	}


	@Override
	public BigDecimal getSharesAfterTrade(AccountingBean bean, boolean useSettlementDate, BigDecimal sharesBeforeTrade) {
		// Get the sum of shares (closed & pending) - not including this trade.
		if (sharesBeforeTrade == null) {
			sharesBeforeTrade = getSharesBeforeTrade(bean, useSettlementDate);
		}
		// If the booking date is null, then add this trade quantity, otherwise the quantity has already been summed in the 'sharesBeforeTrade' value.
		if (getBookingDate(bean) == null) {
			// NOTE: should we take into account TradeType.signedAmounts field? Only set for CDS and IRS
			return bean.isBuy() ? MathUtils.add(sharesBeforeTrade, bean.getQuantityNormalized()) : MathUtils.subtract(sharesBeforeTrade, bean.getQuantityNormalized());
		}
		return sharesBeforeTrade;
	}


	@Override
	public BigDecimal getAccountingNotionalBeforeTrade(AccountingBean bean, boolean useSettlementDate) {
		BigDecimal shares = getSharesBeforeTrade(bean, useSettlementDate);
		return calculateAccountingNotional(bean, shares);
	}


	@Override
	public BigDecimal getAccountingNotionalAfterTrade(AccountingBean bean, boolean useSettlementDate, BigDecimal accountingNotionalBeforeTrade) {
		if (accountingNotionalBeforeTrade == null) {
			accountingNotionalBeforeTrade = getAccountingNotionalBeforeTrade(bean, useSettlementDate);
		}
		return bean.isBuy() ? MathUtils.add(accountingNotionalBeforeTrade, bean.getAccountingNotional()) : MathUtils.subtract(accountingNotionalBeforeTrade, bean.getAccountingNotional());
	}


	@Override
	@ResponseBody
	public BigDecimal getTradeNetPaymentAmount(int tradeId) {
		Trade trade = getTradeService().getTrade(tradeId);
		ValidationUtils.assertNotNull(trade, "Cannot find trade with id = " + tradeId);

		// check if all fills have been booked
		List<Integer> bookedFillIds = new ArrayList<>();
		for (TradeFill fill : CollectionUtils.getIterable(trade.getTradeFillList())) {
			if (fill.getBookingDate() != null) {
				bookedFillIds.add(fill.getId());
			}
			else {
				// all fills must be booked to enable GL lookup
				bookedFillIds.clear();
				break;
			}
		}

		List<? extends AccountingJournalDetailDefinition> detailList;
		if (bookedFillIds.isEmpty()) {
			// the trade has not been booked: need to preview
			AccountingJournal journal = previewTradeBooking(trade, null);
			detailList = journal.getJournalDetailList();
			detailList = BeanUtils.filter(detailList, detail -> detail.getAccountingAccount().isCurrency() && !detail.getAccountingAccount().isGainLoss());
		}
		else {
			// the trade was booked: lookup results in GL
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setFkFieldIds(bookedFillIds.toArray(new Integer[0]));
			searchForm.setTableName(TradeFill.TABLE_NAME);
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_GAIN_LOSS_ACCOUNTS);
			detailList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		}

		// calculate local currency amount: fx conversion as needed
		InvestmentSecurity localCurrency = trade.getInvestmentSecurity().getInstrument().getTradingCurrency();
		return CoreMathUtils.sumProperty(detailList, detail -> {
			if (localCurrency.equals(detail.getInvestmentSecurity())) {
				return detail.getLocalDebitCredit();
			}
			return InvestmentCalculatorUtils.calculateLocalAmount(detail.getBaseDebitCredit(), trade.getExchangeRateToBase(), localCurrency);
		});
	}


	@Override
	@ResponseBody
	public BigDecimal getTradeAccountingNotionalBeingClosed(Trade trade) {
		return getClosingTradeAccrualHolders(trade, trade.getQuantity()).stream()
				.map(holder -> holder.getAccrualBasis().getAccrualBasis1())
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}


	@Override
	@ResponseBody
	public BigDecimal getTradeAccrualAmount(Trade trade) {
		return getTradeAccrualAmountAdvanced(trade, true, true);
	}


	@Override
	@ResponseBody
	public BigDecimal getTradeAccrualAmount1(Trade trade) {
		return getTradeAccrualAmountAdvanced(trade, true, false);
	}


	@Override
	@ResponseBody
	public BigDecimal getTradeAccrualAmount2(Trade trade) {
		return getTradeAccrualAmountAdvanced(trade, false, true);
	}


	@Override
	public BigDecimal getTradeAccrualAmountAdvanced(Trade trade, boolean includeEvent1, boolean includeEvent2) {
		BigDecimal accrualResult = null;

		// calculate only if not set and security supports accrual
		InvestmentSecurity security = trade.getInvestmentSecurity();
		List<AccrualHolder> accrualHolders = getAccrualHoldersForTrade(trade);
		for (AccrualHolder accrualHolder : accrualHolders) {
			if ((InvestmentUtils.getAccrualEventTypeName(security) != null) && accrualHolder.getAccrualBasis().isAccrualBasisNotZero()) {
				InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();

				BigDecimal notionalMultiplier = trade.getNotionalMultiplier();
				BigDecimal accrual;
				if (includeEvent1) {
					if (includeEvent2) {
						accrual = getInvestmentCalculator().calculateAccruedInterest(security, accrualHolder.getAccrualBasis(), notionalMultiplier, accrualHolder.getAccrualDates());
					}
					else {
						accrual = getInvestmentCalculator().calculateAccruedInterestForEvent1(security, accrualHolder.getAccrualBasis().getAccrualBasis1(), notionalMultiplier, accrualHolder.getAccrualDates().getAccrueUpToDate1(), accrualHolder.getAccrualDates().getAccrueFromDate1());
					}
				}
				else if (includeEvent2) {
					accrual = getInvestmentCalculator().calculateAccruedInterestForEvent2(security, accrualHolder.getAccrualBasis().getAccrualBasis2(), notionalMultiplier, accrualHolder.getAccrualDates().getAccrueUpToDate2(), accrualHolder.getAccrualDates().getAccrueFromDate2());
				}
				else {
					throw new IllegalArgumentException("Both includeEvent1 and includeEvent2 cannot be false for " + trade);
				}

				// when buying a swap (CDS, IRS), one pays accrued interest
				if (trade.getTradeType().isAmountsSignAccountsForBuy() && hierarchy.getAccrualSignCalculator() != null && trade.isBuy()) {
					accrual = MathUtils.negate(accrual);
				}
				accrualResult = MathUtils.add(accrualResult, accrual);
			}
		}

		return accrualResult;
	}


	@Override
	@ResponseBody
	public Date getTradeAccrualUpToDate(Trade trade) {
		return calculateTradeAccrualDatesForTrade(trade).getAccrueUpToDate1();
	}


	private AccrualDates calculateTradeAccrualDatesForJournalDetail(AccountingJournalDetailDefinition journalDetailDefinition) {
		Date transactionDate = journalDetailDefinition.getTransactionDate();
		AccrualDateCalculators dateCalculator = InvestmentCalculatorUtils.getAccrualDateCalculator(journalDetailDefinition.getInvestmentSecurity());
		if (dateCalculator != null && dateCalculator.isAccrualFromPositionOpenSettlement()) {
			journalDetailDefinition = journalDetailDefinition.getParentTransaction() != null ? journalDetailDefinition.getParentTransaction() : journalDetailDefinition;
		}

		return calculateTradeAccrualDatesAdjustedForIRS(journalDetailDefinition, transactionDate);
	}


	/**
	 * The <code>calculateTradeAccrualDatesForTrade</code> method should only be sued to calculate accrual dates for
	 * a trade when there are no preview journal details available for the trade; the <code>calculateTradeAccrualDatesForJournalDetail</code>
	 * method should be used instead.
	 */
	private AccrualDates calculateTradeAccrualDatesForTrade(Trade trade) {
		return calculateTradeAccrualDatesAdjustedForIRS(trade, trade.getTransactionDate());
	}


	private AccrualDates calculateTradeAccrualDatesAdjustedForIRS(InvestmentTransactionInfo transactionInfo, Date transactionDate) {
		InvestmentSecurity security = transactionInfo.getInvestmentSecurity();
		ValidationUtils.assertNotNull(security, "Investment Security selection is required in order to calculate Accrual Up To Date.");

		AccrualDates accrualDates = getInvestmentCalculator().calculateAccrualDates(AccrualDatesCommand.of(transactionInfo, transactionDate));

		// IRS exception: should probably add an option to have a different Accrual Date Calculator for Trades vs valuations/resets/m2m.
		// IRS will also need a new calculator that uses Fixing Cycle because they have Effective Date (accrual start date determined by Fixing Cycle)
		if (InvestmentUtils.isSecurityOfTypeSubType(security, InvestmentTypeSubType.INTEREST_RATE_SWAPS)) {
			Integer fixingCycle = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_FIXING_CYCLE, false);
			Integer fixingCalendarId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_SETTLEMENT_CALENDAR, false);
			Date accrueUpToDate = DateUtils.addWeekDays(transactionDate, fixingCycle);
			while (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(accrueUpToDate, fixingCalendarId.shortValue()))) {
				accrueUpToDate = DateUtils.addWeekDays(accrueUpToDate, 1);
			}
			accrualDates = AccrualDates.of(accrualDates.getAccrueFromDate1(), accrualDates.getAccrueFromDate2(), accrueUpToDate, accrueUpToDate);
		}
		return accrualDates;
	}


	/**
	 * Returns the booking date for the given accounting bean
	 */
	private Date getBookingDate(AccountingBean bean) {
		Date bookingDate = null;
		if (bean instanceof Trade) {
			for (TradeFill tradeFill : CollectionUtils.getIterable(getTradeService().getTradeFillListByTrade(((Trade) bean).getId()))) {
				if (tradeFill.getBookingDate() != null) {
					bookingDate = tradeFill.getBookingDate();
					break;
				}
			}
		}
		if (bean instanceof BookableEntity) {
			bookingDate = ((BookableEntity) bean).getBookingDate();
		}
		return bookingDate;
	}


	private AccountingJournal previewTradeBookingForPositionsOnly(Trade trade) {
		return previewTradeBooking(trade, BookingRuleScopes.POSITION_IMPACT);
	}


	/**
	 * Previews booking of the specified trade to the General Ledger and returns the journal that would have been booked.
	 */
	private AccountingJournal previewTradeBooking(Trade trade, BookingRuleScopes bookingRuleScope) {
		// create single fill for the trade: cannot support multiple because we're not positing to GL (preview only)
		TradeFill fill = new TradeFill();
		fill.setId(-100);
		fill.setTrade(trade);
		fill.setQuantity(trade.getQuantityIntended());
		fill.setNotionalUnitPrice(trade.getAverageUnitPrice());
		fill.setNotionalTotalPrice(getInvestmentCalculator().calculateNotional(trade.getInvestmentSecurity(), fill.getNotionalUnitPrice(), fill.getQuantity(), trade.getNotionalMultiplier()));
		if (InvestmentUtils.isNoPaymentOnOpen(trade.getInvestmentSecurity())) {
			fill.setPaymentUnitPrice(BigDecimal.ZERO);
			fill.setPaymentTotalPrice(BigDecimal.ZERO);
		}
		else {
			fill.setPaymentUnitPrice(fill.getNotionalUnitPrice());
			fill.setPaymentTotalPrice(fill.getNotionalTotalPrice());
		}

		BigDecimal fxRate = trade.getExchangeRateToBase();
		if (fxRate == null) {
			// FX doesn't matter because we are only interested in local amount
			trade.setExchangeRateToBase(BigDecimal.ONE);
		}

		// limit to position rules only to improve performance and avoid infinite recursion (accrual splitting rule recalculates accrual basis for TRS)
		AccountingJournal journal = getAccountingBookingService().getAccountingJournalPreview(BookingPreviewCommand.ofScope(fill, AccountingJournalType.TRADE_JOURNAL, bookingRuleScope));

		trade.setExchangeRateToBase(fxRate); // restore previous value

		return journal;
	}


	/**
	 * For securities that have accrual based on Notional, returns the notional (may return opening notional based on configuration).
	 * For other securities, returns the quantity field value for the specified trade.
	 * Note: inflation linked bonds use Original Face for accrual basis (will be adjusted by index ratio).
	 */
	private List<AccrualHolder> getAccrualHoldersForTrade(Trade trade) {
		InvestmentSecurity security = trade.getInvestmentSecurity();
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();

		BigDecimal quantity = trade.getQuantityIntended();
		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.BONDS)) {
			if (hierarchy.getFactorChangeEventType() == null) {
				// inflation linked securities accrual is based on original face which will be adjusted by index ratio
				// other bonds (including asset backed) have accrual based on remaining face (quantity intended)
				quantity = trade.getOriginalFace();
			}
		}

		List<AccrualHolder> accrualHolders = new ArrayList<>();
		BigDecimal notional = trade.getAccountingNotional();
		if (hierarchy.isTradeAccrualBasedOnOpeningNotional()) {
			// TRS have accrual that's based on opening notional being closed (vs closing notional)
			// Vanilla TRS do not have accrual on opening while iBoxx does
			if (hierarchy.isTradeAccrualOnOpeningAbsent()) {
				accrualHolders = getClosingTradeAccrualHolders(trade, quantity);
			}
			else {
				// aggregate both opening and closing (negated) notionals
				AccrualSignCalculators signCalculator = hierarchy.getAccrualSignCalculator();
				for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(previewTradeBookingForPositionsOnly(trade).getJournalDetailList())) {
					if (detail.getAccountingAccount().isPosition()) {
						if (detail.isClosing() || AccrualSignCalculators.NEGATE_EXCEPT_ON_OPENING_TRADE == signCalculator) {
							notional = MathUtils.negate(detail.getPositionCostBasis()); // need to negate opening cost basis because closing is of the opposite sign
						}
						else {
							notional = detail.getPositionCostBasis();
						}
						accrualHolders.add(new AccrualHolder(AccrualBasis.forHierarchy(hierarchy, notional, quantity), calculateTradeAccrualDatesForJournalDetail(detail)));
					}
				}
			}
		}
		else {
			accrualHolders.add(new AccrualHolder(AccrualBasis.forHierarchy(hierarchy, notional, quantity), calculateTradeAccrualDatesForTrade(trade)));
		}

		// TRS Dividend Leg
		accrualHolders.forEach(holder -> {
			if (trade.getTradeType().isAccrualSignAccountsForBuy() && !trade.getTradeType().isAmountsSignAccountsForBuy() && !MathUtils.isSameSignum(holder.getAccrualBasis().getAccrualBasis1(), holder.getAccrualBasis().getAccrualBasis2())) {
				holder.setAccrualBasis(AccrualBasis.of(holder.getAccrualBasis().getAccrualBasis1(), MathUtils.negate(holder.getAccrualBasis().getAccrualBasis2())));
			}
		});
		return accrualHolders;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                       Helper Methods                       /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates and returns accounting notional amount (cost basis) for this trade.
	 */
	private BigDecimal calculateAccountingNotional(AccountingBean bean, BigDecimal quantity) {
		BigDecimal result = null;
		BigDecimal price = getMarketDataRetriever().getPriceFlexible(bean.getInvestmentSecurity(), bean.getTransactionDate(), true);
		if (price != null) {
			// TODO: need to revisit AccountingBean interface: move this field there? what else will use the interface?
			BigDecimal notionalMultiplier = null;
			if (bean instanceof Trade) {
				notionalMultiplier = ((Trade) bean).getNotionalMultiplier();
			}
			result = getInvestmentCalculator().calculateNotional(bean.getInvestmentSecurity(), price, quantity, notionalMultiplier);
		}
		return result;
	}


	private List<AccrualHolder> getClosingTradeAccrualHolders(Trade trade, BigDecimal quantity) {
		// skip accrual booking rule in order to avoid infinite recursion (TRS has two accrual legs and in order to split them the booking rule needs accrual basis)
		AccountingJournal journal = previewTradeBookingForPositionsOnly(trade);

		List<AccrualHolder> accrualHolders = new ArrayList<>();
		for (AccountingJournalDetailDefinition detail : CollectionUtils.getIterable(journal.getJournalDetailList())) {
			if (detail.getAccountingAccount().isPosition() && detail.isClosing()) {
				BigDecimal notional = MathUtils.negate(detail.getPositionCostBasis()); // need to negate opening cost basis because closing is of the opposite sign
				AccrualHolder accrualHolder = new AccrualHolder(AccrualBasis.of(notional, MathUtils.isNullOrZero(notional) ? BigDecimal.ZERO : quantity), calculateTradeAccrualDatesForJournalDetail(detail));
				accrualHolders.add(accrualHolder);
			}
		}

		if (CollectionUtils.isEmpty(accrualHolders)) {
			accrualHolders.add(new AccrualHolder(AccrualBasis.of(BigDecimal.ZERO), calculateTradeAccrualDatesForTrade(trade)));
		}

		return accrualHolders;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                   Getter & Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public AccountingBookingService<TradeFill> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<TradeFill> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}
}
