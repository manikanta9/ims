package com.clifton.trade.accounting.archive;

import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.archive.AccountingJournalArchiveEntityAdapter;
import com.clifton.accounting.gl.journal.archive.AccountingJournalArchiveEntityAdapterImpl;
import com.clifton.core.beans.IdentityObject;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import org.springframework.stereotype.Component;


/**
 * <code>TradeFillJournalArchiveAdapter</code> represents an {@link AccountingJournalArchiveEntityAdapter}
 * for the {@link AccountingJournalType} responsible for {@link TradeFill}s.
 *
 * @author NickK
 */
@Component
public class TradeFillJournalArchiveAdapter extends AccountingJournalArchiveEntityAdapterImpl<TradeFill> {

	@Override
	public String getJournalTypeName() {
		return AccountingJournalType.TRADE_JOURNAL;
	}


	@Override
	protected IdentityObject getParentIdentityObject(TradeFill tradeFill) {
		return tradeFill.getTrade();
	}


	@Override
	public TradeFill createNewUnpopulatedBookableEntity() {
		TradeFill fill = new TradeFill();
		fill.setTrade(new Trade());
		return fill;
	}
}
