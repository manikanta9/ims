package com.clifton.trade.accounting.booking;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;

import java.util.Date;
import java.util.Map;


/**
 * The <code>TradeBookingBatchJob</code> class is a batch job that books and posts trades identified
 * by job parameters to the General Ledger.
 *
 * @author vgomelsky
 */
public class TradeBookingBatchJob implements Task, StatusHolderObjectAware<Status> {

	private TradeBookingService tradeBookingService;

	private Short investmentGroupId;
	private Integer clientInvestmentAccountGroupId;
	/**
	 * Optionally limits trades to a particular workflow state.  By default, the following workflow states will be included:
	 * TradeService.TRADE_EXECUTED_STATE_NAME, TradeService.TRADE_UNBOOKED_STATE_NAME
	 * However, one may want to restrict to Executed state only in order to skip unbooked trades.
	 */
	private String workflowStateName;
	private Boolean reconciled;

	// 1 means trades after and including yesterday
	private Integer tradeDateFromDaysBackFromToday;
	// 1 means trades before and including yesterday
	private Integer tradeDateToDaysBackFromToday;

	/**
	 * Number of milliseconds to wait before booking next batch of trades.  Zero means never wait.
	 * Use this to spread the load on the system when a lot of trades are booked together.
	 * Delays will allow other system processes to run without being effected by this job.
	 */
	private long delayBetweenBookingsInMilliseconds = 0;
	/**
	 * Number of trades to book before next delay.
	 */
	private int bookingCountBeforeDelay = 1;

	private StatusHolderObject<Status> statusHolder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		TradeBookingCommand command = new TradeBookingCommand();
		command.setInvestmentGroupId(getInvestmentGroupId());
		command.setClientInvestmentAccountGroupId(getClientInvestmentAccountGroupId());
		command.setWorkflowStateName(getWorkflowStateName());
		command.setReconciled(getReconciled());
		command.setDelayBetweenBookingsInMilliseconds(getDelayBetweenBookingsInMilliseconds());
		command.setBookingCountBeforeDelay(getBookingCountBeforeDelay());
		command.setStatusHolder(this.statusHolder);
		Date today = DateUtils.clearTime(new Date());
		if (getTradeDateFromDaysBackFromToday() != null) {
			command.setFromTradeDate(DateUtils.addDays(today, -getTradeDateFromDaysBackFromToday()));
		}
		if (getTradeDateToDaysBackFromToday() != null) {
			command.setToTradeDate(DateUtils.addDays(today, -getTradeDateToDaysBackFromToday()));
			if (getTradeDateFromDaysBackFromToday() != null) {
				if (getTradeDateFromDaysBackFromToday() < getTradeDateToDaysBackFromToday()) {
					throw new IllegalArgumentException("tradeDateFromDaysBackFromToday must be GREATER THAN tradeDateToDaysBackFromToday");
				}
			}
		}

		getTradeBookingService().tradeBookAndPost(command);

		return this.statusHolder.getStatus();
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////////             Getter and Setter Methods            ////////////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeBookingService getTradeBookingService() {
		return this.tradeBookingService;
	}


	public void setTradeBookingService(TradeBookingService tradeBookingService) {
		this.tradeBookingService = tradeBookingService;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public long getDelayBetweenBookingsInMilliseconds() {
		return this.delayBetweenBookingsInMilliseconds;
	}


	public void setDelayBetweenBookingsInMilliseconds(long delayBetweenBookingsInMilliseconds) {
		this.delayBetweenBookingsInMilliseconds = delayBetweenBookingsInMilliseconds;
	}


	public int getBookingCountBeforeDelay() {
		return this.bookingCountBeforeDelay;
	}


	public void setBookingCountBeforeDelay(int bookingCountBeforeDelay) {
		this.bookingCountBeforeDelay = bookingCountBeforeDelay;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public Integer getTradeDateFromDaysBackFromToday() {
		return this.tradeDateFromDaysBackFromToday;
	}


	public void setTradeDateFromDaysBackFromToday(Integer tradeDateFromDaysBackFromToday) {
		this.tradeDateFromDaysBackFromToday = tradeDateFromDaysBackFromToday;
	}


	public Integer getTradeDateToDaysBackFromToday() {
		return this.tradeDateToDaysBackFromToday;
	}


	public void setTradeDateToDaysBackFromToday(Integer tradeDateToDaysBackFromToday) {
		this.tradeDateToDaysBackFromToday = tradeDateToDaysBackFromToday;
	}
}
