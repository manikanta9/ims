package com.clifton.trade.accounting.booking.rule;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.AccountingCommission;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationMethods;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationProperties;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.booking.rule.AccountingCommissionBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.accounting.commission.TradeCommissionOverride;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * The <code>TradeCommissionBookingRule</code> class calculates commissions and fees for the trade and adds them to the journal
 * if no overrides are used.
 * <p/>
 * If commission or fee override is used, applies these overrides and skips commission calculation.
 */
public class TradeCommissionBookingRule extends AccountingCommissionBookingRule<TradeFill> {

	private TradeCommissionService tradeCommissionService;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void applyRule(BookingSession<TradeFill> bookingSession) {
		AccountingJournal journal = bookingSession.getJournal();
		TradeFill fill = bookingSession.getBookableEntity();
		Trade trade = fill.getTrade();

		boolean singleFillTrade = trade.getTradeType().isSingleFillTrade() || MathUtils.isEqual(fill.getQuantityNormalized(), trade.getQuantityNormalized());

		//Get positions that fees/commissions apply to
		List<AccountingJournalDetailDefinition> positionRecords = this.filterActualPositionDetails(journal.getJournalDetailList());

		boolean tradeSettlementCurrencyInSecurityCurrencyDenomination = trade.getSettlementCurrency().equals(trade.getInvestmentSecurity().getInstrument().getTradingCurrency());
		BigDecimal exchangeRateFromSecurityDenominationToBase = tradeSettlementCurrencyInSecurityCurrencyDenomination ? trade.getExchangeRateToBase() : null;

		List<TradeCommissionOverride> commissionOverrides = null;
		Set<AccountingAccount> accountsToSkip = new HashSet<>();

		if (!trade.isNewBean()) {
			//Get the commission/fee overrides for this trade, if any
			commissionOverrides = getTradeCommissionService().getTradeCommissionOverrideListForTrade(trade.getId());

			//AccountingAccount types with an override can be skipped when processing AccountingCommissionDefinitions
			for (TradeCommissionOverride override : CollectionUtils.getIterable(commissionOverrides)) {
				//flip the sign of the amount so that it is positive during processing
				accountsToSkip.addAll(processFlatFeeCommissions(
						getFlatFeeCommissionCommandBuilder()
								.withTotalAmount(override.getTotalAmount() != null ? override.getTotalAmount().negate() : null)
								.withAccount(override.getAccountingAccount())
								.withTrade(trade)
								.withFill(fill)
								.isSingleFillTrade(singleFillTrade)
								.withPositionRecords(positionRecords)
								.withCommissionCurrency(override.getCommissionCurrency())
								.withExchangeRateFromSecurityDenominationToBase(exchangeRateFromSecurityDenominationToBase)
								.build()
						, accountsToSkip
				));
			}
		}


		//Use automated commission definitions for any other fee types
		for (AccountingJournalDetailDefinition positionDetail : CollectionUtils.getIterable(positionRecords)) {
			AccountingCommissionCalculationProperties<TradeFill> calculationProperties = new AccountingCommissionCalculationProperties<>(positionDetail, fill);
			List<AccountingCommissionDefinition> commissionDefinitions = getAccountingCommissionCalculator().getMatchingAccountingCommissionDefinitionList(calculationProperties);

			if (commissionDefinitions != null) {
				for (AccountingCommissionDefinition commissionDefinition : commissionDefinitions) {
					if (commissionDefinition.getCommissionCalculationMethod() == AccountingCommissionCalculationMethods.FLAT_AMOUNT) {
						accountsToSkip.addAll(processFlatFeeCommissions(
								getFlatFeeCommissionCommandBuilder()
										.withTotalAmount(commissionDefinition.getCommissionAmount())
										.withAccount(commissionDefinition.getExpenseAccountingAccount())
										.withTrade(trade)
										.withFill(fill)
										.isSingleFillTrade(singleFillTrade)
										.withPositionRecords(positionRecords)
										.withCommissionCurrency(commissionDefinition.getCommissionCurrency())
										.withExchangeRateFromSecurityDenominationToBase(exchangeRateFromSecurityDenominationToBase)
										.build()
								, accountsToSkip
						));
					}
				}
			}

			calculationProperties.setOverrides(commissionOverrides);
			calculationProperties.setCommissionDefinitions(commissionDefinitions);

			if (positionDetail.isClosing() && positionDetail.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() != null) {
				BigDecimal originalNotional = MathUtils.divide(positionDetail.getQuantity(), fill.getTrade().getCurrentFactor());
				calculationProperties.setOriginalQuantity(originalNotional);
			}

			applyAutomatedCommissions(calculationProperties, exchangeRateFromSecurityDenominationToBase, accountsToSkip);
		}
	}


	/**
	 * Processes the Flat Fee commission definitions and applies the fee appropriately
	 * <p>
	 * NOTE: This logic is here due to legacy reasons (i.e. it was the simplest solution at the time). It should be centralized when the Accounting module is rewritten
	 */
	private Set<AccountingAccount> processFlatFeeCommissions(FlatFeeCommissionCommand command, Set<AccountingAccount> skippedAccounts) {
		Set<AccountingAccount> accountsToSkip = new HashSet<>();

		//Only process overrides with a "flat" fee (as opposed to a "per unit" fee)
		if (!skippedAccounts.contains(command.getAccount()) && command.getTotalAmount() != null) {
			accountsToSkip.add(command.getAccount());

			if (AccountingAccount.EXPENSE_COMMISSION.equals(command.getAccount().getName())) {
				accountsToSkip.add(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.EXECUTING_COMMISSION_EXPENSE));
				accountsToSkip.add(getAccountingAccountService().getAccountingAccountByName(AccountingAccount.CLEARING_COMMISSION_EXPENSE));
			}

			//Only calculate the amount if the override amount is non-zero
			if (!MathUtils.isEqual(command.getTotalAmount(), BigDecimal.ZERO)) {
				BigDecimal totalSoFarForFill = BigDecimal.ZERO;

				//Iterate through all the position details
				Iterator<AccountingJournalDetailDefinition> detailIterator = CollectionUtils.getIterable(command.getPositionRecords()).iterator();
				while (detailIterator.hasNext()) {
					AccountingJournalDetailDefinition positionDetail = detailIterator.next();
					BigDecimal feeAmount;

					//If this is the last position and fill, then the amount is the remainder
					if (!detailIterator.hasNext() && isLastFill(command.getFill())) {

						//If this is a single fill trade, then we simply subtract the total so far from the total commission amount
						if (command.isSingleFillTrade()) {
							feeAmount = MathUtils.subtract(command.getTotalAmount(), totalSoFarForFill);
						}
						//If this is a multi-fill trade, then we have to add the commission total so far for this trade and AccountingAccount, and subtract that from the total commission amount
						else {
							BigDecimal totalSoFarForTrade = getCommissionTotal(command.getTrade(), command.getAccount());
							feeAmount = MathUtils.subtract(command.getTotalAmount(), totalSoFarForTrade);
						}
					}
					//Amounts are divided proportionally across position details. Amount = Total Amount x (Lot Quantity / Trade Quantity)
					else {
						feeAmount = MathUtils.multiply(command.getTotalAmount(), MathUtils.divide(positionDetail.getQuantityNormalized().abs(), command.getTrade().getQuantityNormalized()), 2);
					}

					InvestmentSecurity commissionCurrency = command.getCommissionCurrency() == null ? command.getTrade().getSettlementCurrency() : command.getCommissionCurrency();

					// round the feeAmount correctly for the commission currency
					feeAmount = InvestmentCalculatorUtils.getNotionalCalculator(commissionCurrency).round(feeAmount);

					totalSoFarForFill = MathUtils.add(feeAmount, totalSoFarForFill);

					AccountingCommission commission = new AccountingCommission(command.getAccount(), commissionCurrency, feeAmount);

					createJournalDetails(command.getFill(), positionDetail, commission, command.getExchangeRateFromSecurityDenominationToBase());
				}
			}
		}

		return accountsToSkip;
	}


	private BigDecimal getCommissionTotal(Trade trade, AccountingAccount expenseAccount) {
		List<TradeFill> fills = getTradeService().getTradeFillListByTrade(trade.getId());

		BigDecimal total = BigDecimal.ZERO;

		for (TradeFill fill : fills) {
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setAccountingAccountId(expenseAccount.getId());
			searchForm.setTableName(TradeFill.TABLE_NAME);
			searchForm.setFkFieldId(fill.getId());
			List<AccountingTransaction> transactions = getAccountingTransactionService().getAccountingTransactionList(searchForm);
			for (AccountingTransaction transaction : CollectionUtils.getIterable(transactions)) {
				total = MathUtils.add(total, transaction.getBaseDebitCredit().abs());
			}
		}

		return total;
	}


	private boolean isLastFill(TradeFill fill) {
		Trade trade = fill.getTrade();

		if (trade.getTradeType().isSingleFillTrade() || MathUtils.isEqual(fill.getQuantityNormalized(), trade.getQuantityNormalized())) {
			return true;
		}

		List<TradeFill> fills = getTradeService().getTradeFillListByTrade(trade.getId());
		for (TradeFill otherFill : CollectionUtils.getIterable(fills)) {
			if (otherFill.getBookingDate() == null && !fill.equals(otherFill)) {
				return false;
			}
		}

		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                   Getter & Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}

	////////////////////////////////////////////////////////////////////////////
	///////                        Helper Classes                      /////////
	////////////////////////////////////////////////////////////////////////////


	private FlatFeeCommissionCommandBuilder getFlatFeeCommissionCommandBuilder() {
		return new FlatFeeCommissionCommandBuilder();
	}


	private class FlatFeeCommissionCommand {

		private final BigDecimal totalAmount;
		private final AccountingAccount account;
		private final Trade trade;
		private final TradeFill fill;
		private final boolean singleFillTrade;
		private final List<AccountingJournalDetailDefinition> positionRecords;
		private final InvestmentSecurity commissionCurrency;
		private final BigDecimal exchangeRateFromSecurityDenominationToBase;


		private FlatFeeCommissionCommand(BigDecimal totalAmount, AccountingAccount account, Trade trade, TradeFill fill, boolean singleFillTrade, List<AccountingJournalDetailDefinition> positionRecords, InvestmentSecurity commissionCurrency, BigDecimal exchangeRateFromSecurityDenominationToBase) {
			this.totalAmount = totalAmount;
			this.account = account;
			this.trade = trade;
			this.fill = fill;
			this.singleFillTrade = singleFillTrade;
			this.positionRecords = positionRecords;
			this.commissionCurrency = commissionCurrency;
			this.exchangeRateFromSecurityDenominationToBase = exchangeRateFromSecurityDenominationToBase;
		}


		public BigDecimal getTotalAmount() {
			return this.totalAmount;
		}


		public AccountingAccount getAccount() {
			return this.account;
		}


		public Trade getTrade() {
			return this.trade;
		}


		public TradeFill getFill() {
			return this.fill;
		}


		public boolean isSingleFillTrade() {
			return this.singleFillTrade;
		}


		public List<AccountingJournalDetailDefinition> getPositionRecords() {
			return this.positionRecords;
		}


		public InvestmentSecurity getCommissionCurrency() {
			return this.commissionCurrency;
		}


		public BigDecimal getExchangeRateFromSecurityDenominationToBase() {
			return this.exchangeRateFromSecurityDenominationToBase;
		}
	}

	private class FlatFeeCommissionCommandBuilder {

		private BigDecimal totalAmount;
		private AccountingAccount account;
		private Trade trade;
		private TradeFill fill;
		private boolean singleFillTrade;
		private List<AccountingJournalDetailDefinition> positionRecords;
		private InvestmentSecurity commissionCurrency;
		private BigDecimal exchangeRateFromSecurityDenominationToBase;


		private FlatFeeCommissionCommandBuilder() {
			//do nothing
		}


		public FlatFeeCommissionCommandBuilder withTotalAmount(BigDecimal totalAmount) {
			this.totalAmount = totalAmount;
			return this;
		}


		public FlatFeeCommissionCommandBuilder withAccount(AccountingAccount account) {
			this.account = account;
			return this;
		}


		public FlatFeeCommissionCommandBuilder withTrade(Trade trade) {
			this.trade = trade;
			return this;
		}


		public FlatFeeCommissionCommandBuilder withFill(TradeFill fill) {
			this.fill = fill;
			return this;
		}


		public FlatFeeCommissionCommandBuilder isSingleFillTrade(boolean singleFillTrade) {
			this.singleFillTrade = singleFillTrade;
			return this;
		}


		public FlatFeeCommissionCommandBuilder withPositionRecords(List<AccountingJournalDetailDefinition> positionRecords) {
			this.positionRecords = positionRecords;
			return this;
		}


		public FlatFeeCommissionCommandBuilder withCommissionCurrency(InvestmentSecurity commissionCurrency) {
			this.commissionCurrency = commissionCurrency;
			return this;
		}


		public FlatFeeCommissionCommandBuilder withExchangeRateFromSecurityDenominationToBase(BigDecimal exchangeRateFromSecurityDenominationToBase) {
			this.exchangeRateFromSecurityDenominationToBase = exchangeRateFromSecurityDenominationToBase;
			return this;
		}


		public FlatFeeCommissionCommand build() {
			return new FlatFeeCommissionCommand(this.totalAmount, this.account, this.trade, this.fill, this.singleFillTrade, this.positionRecords, this.commissionCurrency, this.exchangeRateFromSecurityDenominationToBase);
		}
	}
}
