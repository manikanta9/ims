package com.clifton.trade.accounting.booking.rule;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingCommonBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.trade.TradeFill;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class TradeAccrualBookingRule extends AccountingCommonBookingRule<TradeFill> implements AccountingBookingRule<TradeFill> {


	@Override
	public void applyRule(BookingSession<com.clifton.trade.TradeFill> bookingSession) {
		TradeFill tradeFill = bookingSession.getBookableEntity();
		if (tradeFill.getInvestmentSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			applyCashReceivableRule(bookingSession);
		}
	}


	private void applyCashReceivableRule(BookingSession<com.clifton.trade.TradeFill> bookingSession) {
		final TradeFill tradeFill = bookingSession.getBookableEntity();
		final Date actualSettlementDate = tradeFill.getTrade().getActualSettlementDate();
		AccountingAccount receivable = getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_PAYMENT_RECEIVABLE);
		AccountingJournal journal = bookingSession.getJournal();
		List<? extends AccountingJournalDetailDefinition> details = CollectionUtils.getStream(journal.getJournalDetailList())
				.filter(d -> d.getAccountingAccount().isCurrency())
				.filter(d -> !MathUtils.isNullOrZero(d.getLocalDebitCredit()) || !MathUtils.isNullOrZero(d.getBaseDebitCredit()))
				.collect(Collectors.toList());
		for (AccountingJournalDetailDefinition detail : details) {
			if (actualSettlementDate == null) {
				detail.setAccountingAccount(receivable);
				detail.setDescription("Accrual " + detail.getDescription());
			}
			else {
				detail.setSettlementDate(actualSettlementDate);
				detail.setTransactionDate(actualSettlementDate);
			}
		}
	}
}
