package com.clifton.trade.accounting.commission;


/**
 * The <code>TradeCommissionSources</code> is an enum that represents the source from which a trade commission comes from.
 *
 * @author jgommels
 */
public enum TradeCommissionSources {
	/**
	 * The commission is a previewed value and has not actually been applied yet
	 */
	PREVIEW,

	/**
	 * The commission is a manually overridden value
	 */
	OVERRIDE,

	/**
	 * The commission is an actual commission that was applied during booking
	 */
	ACTUAL,

	/**
	 * The commission is an adjusted amount after the trade was booked
	 */
	ADJUSTMENT
}
