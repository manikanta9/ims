package com.clifton.trade.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>TradeCommissionService</code> provides methods for retrieving the {@link TradeCommission}s associated with a particular trade.
 *
 * @author jgommels
 */
public interface TradeCommissionService {

	public TradeCommissionOverride getTradeCommissionOverride(int id);


	public List<TradeCommissionOverride> getTradeCommissionOverrideList(TradeCommissionOverrideSearchForm searchForm);


	public List<TradeCommissionOverride> getTradeCommissionOverrideListForTrade(int tradeId);


	public TradeCommissionOverride getTradeCommissionOverrideForTradeCommissionAccountingAccount(Trade trade);


	public TradeCommissionOverride getTradeCommissionOverrideForTradeFeeAccountingAccount(Trade trade);


	public TradeCommissionOverride getTradeCommissionOverrideForTradeAndAccountingAccount(Trade trade, AccountingAccount accountingAccount);


	public void saveTradeCommissionOverride(TradeCommissionOverride tradeCommissionOverride);


	/**
	 * Saves the {@link TradeCommissionOverride}s for the provided {@link Trade}. The provided trade will be updated in
	 * place and saved if saving one or more of the overrides results in changes to commission or fee values on the trade.
	 */
	@DoNotAddRequestMapping
	public void saveTradeCommissionOverrideListForTrade(Trade trade, List<TradeCommissionOverride> tradeCommissionOverrideList);


	/**
	 * Updates the existing {@link TradeCommissionOverride} with the same <code>trade</code> and <code>account</code>, otherwise creates a new one if
	 * no such override exists. This ensures that there can be at most one {@link TradeCommissionOverride} for a ({@link Trade}, {@link AccountingAccount}) pair.
	 * Only one of 'totalAmount' or 'amountPerUnit' should be set to a non-null value.
	 */
	@DoNotAddRequestMapping
	public TradeCommissionOverride createOrUpdateTradeCommissionOverride(Trade trade, AccountingAccount account, BigDecimal totalAmount, BigDecimal amountPerUnit);


	public void deleteTradeCommissionOverride(int id);


	@SecureMethod(dtoClass = TradeCommissionOverride.class)
	public List<TradeCommission<?>> getTradeCommissionListByTrade(int tradeId);


	@SecureMethod(dtoClass = TradeCommissionOverride.class)
	public List<TradeCommission<?>> getTradeCommissionListExpandedByTrade(int tradeId);
}
