package com.clifton.trade.accounting.booking;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;

import java.math.BigDecimal;


/**
 * @author mwacker
 */
public class TradeBookingUtils {

	/**
	 * Negates the specified amount if necessary depending on the type of the trade and direction of the trade.
	 */
	public static BigDecimal normalizeAmountSign(Trade trade, BigDecimal amount) {
		if (trade.getTradeType().isAmountsSignAccountsForBuy()) {
			ValidationUtils.assertNotNull(trade.getAccountingNotional(), "Accounting Notional field is required for trades where Amounts Sign Accounts for Buy");
			if (trade.getAccountingNotional().signum() == amount.signum()) {
				return amount.negate();
			}
			return amount;
		}
		if (trade.isBuy()) {
			return (amount.signum() < 0) ? amount.negate() : amount;
		}
		return (amount.signum() <= 0) ? amount : amount.negate();
	}
}
