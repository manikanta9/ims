package com.clifton.trade.marketdata.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.trade.Trade;
import com.clifton.trade.marketdata.TradeMarketDataValue;
import org.springframework.stereotype.Component;

import java.util.Comparator;


/**
 * The cache for the latest {@link TradeMarketDataValue} entity for a given {@link MarketDataField} which applies to a given {@link Trade}.
 * <p>
 * This cache stores the most recently applicable {@link TradeMarketDataValue} for a given trade.
 *
 * @author mikeh
 */
@Component
public class TradeMarketDataValueLatestByTradeAndBackingFieldIdCache extends SelfRegisteringCompositeKeyDaoCache<TradeMarketDataValue, Integer, Short> {

	@Override
	protected String getBeanKey1Property() {
		return "referenceOne.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "tradeMarketDataField.marketDataField.id";
	}


	@Override
	protected Integer getBeanKey1Value(TradeMarketDataValue bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getReferenceOne());
		}
		return null;
	}


	@Override
	protected Short getBeanKey2Value(TradeMarketDataValue bean) {
		if (bean != null && bean.getTradeMarketDataField() != null) {
			return BeanUtils.getBeanIdentity(bean.getTradeMarketDataField().getMarketDataField());
		}
		return null;
	}


	@Override
	protected TradeMarketDataValue lookupBean(ReadOnlyDAO<TradeMarketDataValue> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		// Get and return latest value
		return dao.findByFields(getBeanKeyProperties(), keyProperties).stream()
				.max(Comparator.comparing(tradeMarketDataValue -> tradeMarketDataValue.getReferenceTwo().getMeasureDateWithTime()))
				.orElseGet(() -> {
					if (throwExceptionIfNotFound) {
						throw new ValidationException("Unable to find " + StringUtils.splitWords(dao.getConfiguration().getTableName()) + " with properties [" + ArrayUtils.toString(getBeanKeyProperties()) + "] and value(s) [" + ArrayUtils.toString(keyProperties) + "]");
					}
					return null;
				});
	}


	/**
	 * Since we cache most recent, we need to clear on inserts and updates.  Subsequent retrievals need to do a fresh lookup
	 */
	@Override
	protected boolean isClearOnAllKeyChanges() {
		return true;
	}
}
