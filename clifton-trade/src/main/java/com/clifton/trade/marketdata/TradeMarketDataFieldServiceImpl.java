package com.clifton.trade.marketdata;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.marketdata.search.TradeMarketDataFieldMappingSearchForm;
import com.clifton.trade.marketdata.search.TradeMarketDataFieldSearchForm;
import com.clifton.trade.marketdata.search.TradeMarketDataValueSearchForm;
import com.clifton.trade.marketdata.validate.TradeMarketDataFieldValidator;
import com.clifton.workflow.definition.WorkflowState;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @see TradeMarketDataFieldService
 */
@Service
public class TradeMarketDataFieldServiceImpl implements TradeMarketDataFieldService {

	private AdvancedUpdatableDAO<TradeMarketDataField, Criteria> tradeMarketDataFieldDAO;
	private AdvancedUpdatableDAO<TradeMarketDataFieldMapping, Criteria> tradeMarketDataFieldMappingDAO;
	private AdvancedUpdatableDAO<TradeMarketDataValue, Criteria> tradeMarketDataValueDAO;
	private DaoCompositeKeyCache<TradeMarketDataValue, Integer, Short> tradeMarketDataValueLatestByTradeAndBackingFieldIdCache;
	private DaoNamedEntityCache<TradeMarketDataField> tradeMarketDataFieldCache;
	private DaoSingleKeyListCache<TradeMarketDataFieldMapping, Short> tradeMarketDataFieldMappingByTradeTypeCache;

	private InvestmentGroupService investmentGroupService;
	private MarketDataFieldService marketDataFieldService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeMarketDataField getTradeMarketDataField(short id) {
		return getTradeMarketDataFieldDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeMarketDataField getTradeMarketDataFieldByName(String name) {
		return getTradeMarketDataFieldCache().getBeanForKeyValueStrict(getTradeMarketDataFieldDAO(), name);
	}


	@Override
	public List<TradeMarketDataField> getTradeMarketDataFieldList(TradeMarketDataFieldSearchForm searchForm) {
		return getTradeMarketDataFieldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<TradeMarketDataField> getTradeMarketDataFieldListForTradeAndEndState(Trade trade, WorkflowState endState) {
		List<TradeMarketDataFieldMapping> fieldMappingList = getTradeMarketDataFieldMappingByTradeTypeCache().getBeanListForKeyValue(getTradeMarketDataFieldMappingDAO(), trade.getTradeType().getId());
		return CollectionUtils.getStream(fieldMappingList)
				.filter(mapping -> mapping.getTradeMarketDataField().isActive())
				.filter(mapping -> endState == null || Objects.equals(endState, mapping.getTradeMarketDataField().getTradeWorkflowEndState()))
				.filter(mapping -> mapping.getBusinessService() == null || Objects.equals(mapping.getBusinessService(), trade.getClientInvestmentAccount().getBusinessService()))
				.filter(mapping -> mapping.getInvestmentGroup() == null || getInvestmentGroupService().isInvestmentInstrumentInGroup(mapping.getInvestmentGroup().getName(), trade.getInvestmentSecurity().getInstrument().getId()))
				.map(TradeMarketDataFieldMapping::getTradeMarketDataField)
				.distinct() // Remove duplicates in the event that redundant mappings exist
				.collect(Collectors.toList());
	}


	/**
	 * see {@link TradeMarketDataFieldValidator} for validation details
	 */
	@Override
	public TradeMarketDataField saveTradeMarketDataField(TradeMarketDataField bean) {
		return getTradeMarketDataFieldDAO().save(bean);
	}


	/**
	 * see {@link TradeMarketDataFieldValidator} for validation details
	 */
	@Override
	@Transactional
	public void deleteTradeMarketDataField(short id) {
		TradeMarketDataFieldMappingSearchForm searchForm = new TradeMarketDataFieldMappingSearchForm();
		searchForm.setTradeMarketDataFieldId(id);
		List<TradeMarketDataFieldMapping> mappings = getTradeMarketDataFieldMappingList(searchForm);
		if (!CollectionUtils.isEmpty(mappings)) {
			deleteTradeMarketDataFieldMappingList(mappings);
		}
		getTradeMarketDataFieldDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeMarketDataFieldMapping getTradeMarketDataFieldMapping(int id) {
		return getTradeMarketDataFieldMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeMarketDataFieldMapping> getTradeMarketDataFieldMappingList(TradeMarketDataFieldMappingSearchForm searchForm) {
		return getTradeMarketDataFieldMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeMarketDataFieldMapping saveTradeMarketDataFieldMapping(TradeMarketDataFieldMapping bean) {
		return getTradeMarketDataFieldMappingDAO().save(bean);
	}


	@Override
	public void deleteTradeMarketDataFieldMapping(int id) {
		getTradeMarketDataFieldMappingDAO().delete(id);
	}


	@Override
	public void deleteTradeMarketDataFieldMappingList(List<TradeMarketDataFieldMapping> valueList) {
		getTradeMarketDataFieldMappingDAO().deleteList(valueList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeMarketDataValue getTradeMarketDataValue(int id) {
		TradeMarketDataValue result = getTradeMarketDataValueDAO().findByPrimaryKey(id);
		return result != null ? getUserTradeMarketDataValueView(result) : null;
	}


	@Override
	public TradeMarketDataValue getTradeMarketDataValueLatestForTradeAndBackingFieldUnfiltered(Trade trade, MarketDataField marketDataField) {
		return getTradeMarketDataValueLatestByTradeAndBackingFieldIdCache().getBeanForKeyValues(getTradeMarketDataValueDAO(), trade.getId(), marketDataField.getId());
	}


	@Override
	public List<TradeMarketDataValue> getTradeMarketDataValueList(TradeMarketDataValueSearchForm searchForm) {
		// Get value list filtered based on access conditions
		List<TradeMarketDataValue> tmdvList = getTradeMarketDataValueListUnfiltered(searchForm);
		// Include null return values in order to comply with convention tests requiring null responses when XML-based DAOs are used
		if (tmdvList == null) {
			return tmdvList;
		}
		List<TradeMarketDataValue> filteredTmdvList = CollectionUtils.getConverted(tmdvList, this::getUserTradeMarketDataValueView);
		return CollectionUtils.toPagingArrayList(filteredTmdvList, tmdvList);
	}


	@Override
	public List<TradeMarketDataValue> getTradeMarketDataValueListUnfiltered(TradeMarketDataValueSearchForm searchForm) {
		return getTradeMarketDataValueDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public TradeMarketDataValue saveTradeMarketDataValue(TradeMarketDataValue bean) {
		// Populate market data value if not present
		if (bean.getReferenceTwo() != null && bean.getReferenceTwo().isNewBean()) {
			MarketDataValue mdv = bean.getReferenceTwo();
			mdv.setDataField(bean.getTradeMarketDataField().getMarketDataField());
			mdv.setInvestmentSecurity(bean.getReferenceOne().getInvestmentSecurity());
			mdv.setDataSource(bean.getTradeMarketDataField().getMarketDataSource());
			MarketDataValue newMdv = getMarketDataFieldService().saveMarketDataValue(mdv);
			bean.setReferenceTwo(newMdv);
		}
		return getTradeMarketDataValueDAO().save(bean);
	}


	@Override
	public void deleteTradeMarketDataValue(int id) {
		getTradeMarketDataValueDAO().delete(id);
	}


	@Override
	public void deleteTradeMarketDataValueList(List<TradeMarketDataValue> valueList) {
		getTradeMarketDataValueDAO().deleteList(valueList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the {@link TradeMarketDataValue} view for the current user. If the user does not have access to the market data then the backing market data value will be censored,
	 * providing minimal visible information for the user. This is used to comply with entitlement constraints.
	 */
	private TradeMarketDataValue getUserTradeMarketDataValueView(TradeMarketDataValue tmdv) {
		boolean granted = tmdv.getTradeMarketDataField().getValueReadCondition() == null
				|| getSystemConditionEvaluationHandler().isConditionTrue(tmdv.getTradeMarketDataField().getValueReadCondition(), tmdv);
		TradeMarketDataValue userTmdv;
		if (granted) {
			// Access granted; return original value
			userTmdv = tmdv;
		}
		else {
			// User does not have access; censor market data value
			userTmdv = new TradeMarketDataValue();
			userTmdv.setId(tmdv.getId());
			userTmdv.setReferenceOne(tmdv.getReferenceOne());
			userTmdv.setTradeMarketDataField(tmdv.getTradeMarketDataField());
			MarketDataValue mdv = new MarketDataValue();
			mdv.setDataSource(tmdv.getTradeMarketDataField().getMarketDataSource());
			mdv.setDataField(tmdv.getTradeMarketDataField().getMarketDataField());
			mdv.setInvestmentSecurity(tmdv.getReferenceOne().getInvestmentSecurity());
			mdv.setMeasureDate(tmdv.getReferenceTwo().getMeasureDate());
			mdv.setMeasureTime(tmdv.getReferenceTwo().getMeasureTime());
			userTmdv.setReferenceTwo(mdv);
		}
		return userTmdv;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeMarketDataField, Criteria> getTradeMarketDataFieldDAO() {
		return this.tradeMarketDataFieldDAO;
	}


	public void setTradeMarketDataFieldDAO(AdvancedUpdatableDAO<TradeMarketDataField, Criteria> tradeMarketDataFieldDAO) {
		this.tradeMarketDataFieldDAO = tradeMarketDataFieldDAO;
	}


	public AdvancedUpdatableDAO<TradeMarketDataFieldMapping, Criteria> getTradeMarketDataFieldMappingDAO() {
		return this.tradeMarketDataFieldMappingDAO;
	}


	public void setTradeMarketDataFieldMappingDAO(AdvancedUpdatableDAO<TradeMarketDataFieldMapping, Criteria> tradeMarketDataFieldMappingDAO) {
		this.tradeMarketDataFieldMappingDAO = tradeMarketDataFieldMappingDAO;
	}


	public AdvancedUpdatableDAO<TradeMarketDataValue, Criteria> getTradeMarketDataValueDAO() {
		return this.tradeMarketDataValueDAO;
	}


	public void setTradeMarketDataValueDAO(AdvancedUpdatableDAO<TradeMarketDataValue, Criteria> tradeMarketDataValueDAO) {
		this.tradeMarketDataValueDAO = tradeMarketDataValueDAO;
	}


	public DaoCompositeKeyCache<TradeMarketDataValue, Integer, Short> getTradeMarketDataValueLatestByTradeAndBackingFieldIdCache() {
		return this.tradeMarketDataValueLatestByTradeAndBackingFieldIdCache;
	}


	public void setTradeMarketDataValueLatestByTradeAndBackingFieldIdCache(DaoCompositeKeyCache<TradeMarketDataValue, Integer, Short> tradeMarketDataValueLatestByTradeAndBackingFieldIdCache) {
		this.tradeMarketDataValueLatestByTradeAndBackingFieldIdCache = tradeMarketDataValueLatestByTradeAndBackingFieldIdCache;
	}


	public DaoNamedEntityCache<TradeMarketDataField> getTradeMarketDataFieldCache() {
		return this.tradeMarketDataFieldCache;
	}


	public void setTradeMarketDataFieldCache(DaoNamedEntityCache<TradeMarketDataField> tradeMarketDataFieldCache) {
		this.tradeMarketDataFieldCache = tradeMarketDataFieldCache;
	}


	public DaoSingleKeyListCache<TradeMarketDataFieldMapping, Short> getTradeMarketDataFieldMappingByTradeTypeCache() {
		return this.tradeMarketDataFieldMappingByTradeTypeCache;
	}


	public void setTradeMarketDataFieldMappingByTradeTypeCache(DaoSingleKeyListCache<TradeMarketDataFieldMapping, Short> tradeMarketDataFieldMappingByTradeTypeCache) {
		this.tradeMarketDataFieldMappingByTradeTypeCache = tradeMarketDataFieldMappingByTradeTypeCache;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
