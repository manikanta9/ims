package com.clifton.trade.marketdata;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.FunctionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import com.clifton.trade.Trade;
import com.clifton.trade.marketdata.cache.TradeMarketDataLiveRequestContextCache;
import com.clifton.trade.marketdata.search.TradeMarketDataValueSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;


@Service
public class TradeMarketDataLiveServiceImpl implements TradeMarketDataLiveService {

	public static final int MARKET_DATA_MAX_AGE_IN_SECONDS = 15;

	private MarketDataFieldService marketDataFieldService;
	private MarketDataLiveService marketDataLiveService;
	private TradeMarketDataFieldService tradeMarketDataFieldService;
	private TradeMarketDataLiveRequestContextCache tradeMarketDataLiveRequestContextCache;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<TradeMarketDataValue> createTradeMarketDataValuesForTrade(TradeMarketDataLiveCommand tradeMarketDataLiveCommand) {
		List<TradeMarketDataValue> tradeMarketDataValueList = retrieveTradeMarketDataValuesForTrade(tradeMarketDataLiveCommand);
		return CollectionUtils.getConverted(tradeMarketDataValueList, this::saveTradeMarketDataValueAndBackingValue);
	}


	@Override
	public List<TradeMarketDataValue> retrieveTradeMarketDataValuesForTrade(TradeMarketDataLiveCommand tradeMarketDataLiveCommand) {
		// Partition fields by security
		Map<InvestmentSecurity, List<TradeMarketDataField>> fieldListBySecurity = BeanUtils.getBeansMap(tradeMarketDataLiveCommand.getFieldList(), field -> field.isUnderlyingSecurity()
				? tradeMarketDataLiveCommand.getTrade().getInvestmentSecurity().getUnderlyingSecurity()
				: tradeMarketDataLiveCommand.getTrade().getInvestmentSecurity());

		// Retrieve and save market data values
		List<TradeMarketDataValue> tradeMarketDataValueList = new ArrayList<>();
		StringJoiner errorMessageSj = new StringJoiner(StringUtils.NEW_LINE + StringUtils.TAB);
		for (Map.Entry<InvestmentSecurity, List<TradeMarketDataField>> fieldListBySecurityEntry : fieldListBySecurity.entrySet()) {
			InvestmentSecurity security = fieldListBySecurityEntry.getKey();
			List<TradeMarketDataField> tradeMarketDataFieldList = fieldListBySecurityEntry.getValue();
			try {
				Map<TradeMarketDataField, MarketDataValue> marketDataValueByFieldMap = retrieveMarketDataValueListForTradeUsingSecurity(tradeMarketDataLiveCommand, security, tradeMarketDataFieldList);
				List<TradeMarketDataValue> savedTradeMarketValueList = marketDataValueByFieldMap.entrySet().stream()
						.filter(valueByFieldEntry -> valueByFieldEntry.getValue() != null)
						.map(valueByFieldEntry -> createTradeMarketDataValue(tradeMarketDataLiveCommand.getTrade(), valueByFieldEntry.getKey(), valueByFieldEntry.getValue()))
						.collect(Collectors.toList());
				tradeMarketDataValueList.addAll(savedTradeMarketValueList);
			}
			catch (Exception e) {
				LogUtils.error(LogCommand.ofThrowable(TradeMarketDataLiveServiceImpl.class, e));
				errorMessageSj.add(String.format("Security: [%s]. Field(s): %s.", security.getLabel(), CollectionUtils.getConverted(tradeMarketDataFieldList, NamedEntityWithoutLabel::getName)));
				tradeMarketDataFieldList.forEach(tradeMarketDataField -> getTradeMarketDataLiveRequestContextCache().putErrorForFieldAndSecurity(tradeMarketDataField.getMarketDataField().getName(), security.getId()));
			}
		}

		// Report errors
		if (errorMessageSj.length() != 0) {
			String errorMessage = "An error occurred while retrieving market data values. Failures:" + StringUtils.NEW_LINE + StringUtils.TAB + errorMessageSj;
			if (tradeMarketDataLiveCommand.isIgnoreFailures()) {
				LogUtils.error(TradeMarketDataLiveServiceImpl.class, errorMessage);
			}
			else {
				throw new RuntimeException(errorMessage);
			}
		}
		return tradeMarketDataValueList;
	}


	// Transactional is used here to enhance performance (rather than opening a new transaction for each save) and to reduce superfluous data when secondary saves fail
	@Transactional
	@Override
	public TradeMarketDataValue saveTradeMarketDataValueAndBackingValue(TradeMarketDataValue tradeMarketDataValue) {
		getMarketDataFieldService().saveMarketDataValue(tradeMarketDataValue.getReferenceTwo());
		return getTradeMarketDataFieldService().saveTradeMarketDataValue(tradeMarketDataValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<TradeMarketDataField, MarketDataValue> retrieveMarketDataValueListForTradeUsingSecurity(TradeMarketDataLiveCommand tradeMarketDataLiveCommand, InvestmentSecurity security, List<TradeMarketDataField> tradeMarketDataFieldList) {
		// Filter out "Only Retrieve Once" fields which already have values
		List<TradeMarketDataField> eligibleTradeMarketDataFieldList = getEligibleTradeMarketDataFieldList(tradeMarketDataLiveCommand.getTrade(), tradeMarketDataFieldList);
		// Get market data values
		List<MarketDataLive> marketDataLiveList = getMarketDataLiveListForSecurityAndFieldNames(tradeMarketDataLiveCommand, security, eligibleTradeMarketDataFieldList);

		// Map trade market data fields to market data values
		Map<String, MarketDataLive> marketDataLiveByFieldName = BeanUtils.getBeanMap(marketDataLiveList, MarketDataLive::getFieldName);
		Map<TradeMarketDataField, MarketDataValue> marketDataValueByTradeMarketDataField = CollectionUtils.getMapped(tradeMarketDataFieldList, tradeMarketDataField -> {
			MarketDataLive marketDataLive = marketDataLiveByFieldName.get(tradeMarketDataField.getMarketDataField().getName());
			final MarketDataValue marketDataValue;
			if (marketDataLive != null) {
				marketDataValue = convertMarketDataLiveToValue(marketDataLive, security, tradeMarketDataField);
			}
			else {
				LogUtils.warn(getClass(), String.format("Market Data Live values cannot be looked up for [%s] and security [%s].", tradeMarketDataField.getMarketDataField().getName(), security.getName()));
				marketDataValue = null;
			}
			return marketDataValue;
		});

		return marketDataValueByTradeMarketDataField;
	}


	private List<TradeMarketDataField> getEligibleTradeMarketDataFieldList(Trade trade, List<TradeMarketDataField> tradeMarketDataFieldList) {
		Collection<Short> onlyRetrieveOnceFieldIdList = tradeMarketDataFieldList.stream()
				.filter(TradeMarketDataField::isOnlyRetrieveOnce)
				.map(BaseSimpleEntity::getId)
				.distinct()
				.collect(Collectors.toList());
		final Collection<TradeMarketDataField> previouslyExistingOnlyRetrieveOnceFieldList;
		if (onlyRetrieveOnceFieldIdList.isEmpty()) {
			previouslyExistingOnlyRetrieveOnceFieldList = Collections.emptyList();
		}
		else {
			TradeMarketDataValueSearchForm searchForm = new TradeMarketDataValueSearchForm();
			searchForm.setTradeId(trade.getId());
			searchForm.setTradeMarketDataFieldIds(CollectionUtils.toArray(onlyRetrieveOnceFieldIdList, Short.class));
			previouslyExistingOnlyRetrieveOnceFieldList = getTradeMarketDataFieldService().getTradeMarketDataValueList(searchForm).stream()
					.map(TradeMarketDataValue::getTradeMarketDataField)
					.collect(Collectors.toSet());
		}
		return CollectionUtils.getFiltered(tradeMarketDataFieldList, tradeMarketDataField -> !previouslyExistingOnlyRetrieveOnceFieldList.contains(tradeMarketDataField));
	}


	/**
	 * Returns a list of {@link MarketDataLive}s for the provided fields for the provided security.
	 *
	 * @param tradeMarketDataLiveCommand the command containing additional request parameters to use
	 * @param security                   the security to get field values for
	 * @param fieldList                  the list of fields to retrieve
	 */
	private List<MarketDataLive> getMarketDataLiveListForSecurityAndFieldNames(TradeMarketDataLiveCommand tradeMarketDataLiveCommand, InvestmentSecurity security, List<TradeMarketDataField> fieldList) {
		// Guard-clause: Trivial case
		if (CollectionUtils.isEmpty(fieldList)) {
			return Collections.emptyList();
		}

		// Get cached values
		final List<MarketDataLive> marketDataLiveList = new ArrayList<>();
		List<String> remainingFieldNameList = CollectionUtils.getConverted(fieldList, field -> field.getMarketDataField().getName());
		if (tradeMarketDataLiveCommand.isUseRequestCache()) {
			List<MarketDataLive> cachedMarketDataLiveList = remainingFieldNameList.stream()
					.map(fieldName -> getTradeMarketDataLiveRequestContextCache().getMarketDataLiveForFieldAndSecurity(fieldName, security.getId()))
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
			marketDataLiveList.addAll(cachedMarketDataLiveList);
			// Filter remaining field list to all non-cached fields, excluding cached field which are marked as failures
			remainingFieldNameList = remainingFieldNameList.stream()
					.filter(FunctionUtils.negate(CollectionUtils.getConverted(cachedMarketDataLiveList, MarketDataLive::getFieldName)::contains))
					.filter(fieldName -> !getTradeMarketDataLiveRequestContextCache().isErrorForFieldAndSecurity(fieldName, security.getId()))
					.collect(Collectors.toList());
		}

		// Retrieve new values
		if (!remainingFieldNameList.isEmpty()) {
			MarketDataLiveCommand marketDataLiveCommand = new MarketDataLiveCommand();
			marketDataLiveCommand.setSecurityIds(security.getId());
			marketDataLiveCommand.setFieldNames(CollectionUtils.toArray(remainingFieldNameList, String.class));
			marketDataLiveCommand.setMaxAgeInSeconds(MARKET_DATA_MAX_AGE_IN_SECONDS);
			marketDataLiveCommand.setProviderOverride(tradeMarketDataLiveCommand.isUseTerminal() ? MarketDataProviderOverrides.TERMINAL : MarketDataProviderOverrides.BPIPE);
			marketDataLiveCommand.setProviderTimeoutOverride(tradeMarketDataLiveCommand.getProviderTimeoutOverride());
			List<MarketDataLive> retrievedMarketDataLiveList = getMarketDataLiveService().getMarketDataLiveFieldValuesWithCommand(marketDataLiveCommand);
			retrievedMarketDataLiveList.forEach(marketDataLive -> getTradeMarketDataLiveRequestContextCache().putMarketDataLiveForFieldAndSecurity(marketDataLive.getFieldName(), security.getId(), marketDataLive));
			marketDataLiveList.addAll(retrievedMarketDataLiveList);
		}
		return marketDataLiveList;
	}


	private MarketDataValue convertMarketDataLiveToValue(MarketDataLive marketDataLive, InvestmentSecurity security, TradeMarketDataField tradeMarketDataField) {
		MarketDataValue marketDataValue = new MarketDataValue();
		marketDataValue.setDataField(tradeMarketDataField.getMarketDataField());
		marketDataValue.setInvestmentSecurity(security);
		marketDataValue.setMeasureDate(marketDataLive.getMeasureDate());
		marketDataValue.setMeasureTime(Time.valueOf(marketDataLive.getMeasureDate()));
		marketDataValue.setMeasureValue(marketDataLive.asNumericValue());
		marketDataValue.setDataSource(tradeMarketDataField.getMarketDataSource());
		return marketDataValue;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeMarketDataValue createTradeMarketDataValue(Trade trade, TradeMarketDataField tradeMarketDataField, MarketDataValue marketDataValue) {
		TradeMarketDataValue tradeMarketDataValue = new TradeMarketDataValue();
		tradeMarketDataValue.setReferenceOne(trade);
		tradeMarketDataValue.setReferenceTwo(marketDataValue);
		tradeMarketDataValue.setTradeMarketDataField(tradeMarketDataField);
		return tradeMarketDataValue;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public TradeMarketDataFieldService getTradeMarketDataFieldService() {
		return this.tradeMarketDataFieldService;
	}


	public void setTradeMarketDataFieldService(TradeMarketDataFieldService tradeMarketDataFieldService) {
		this.tradeMarketDataFieldService = tradeMarketDataFieldService;
	}


	public TradeMarketDataLiveRequestContextCache getTradeMarketDataLiveRequestContextCache() {
		return this.tradeMarketDataLiveRequestContextCache;
	}


	public void setTradeMarketDataLiveRequestContextCache(TradeMarketDataLiveRequestContextCache tradeMarketDataLiveRequestContextCache) {
		this.tradeMarketDataLiveRequestContextCache = tradeMarketDataLiveRequestContextCache;
	}
}
