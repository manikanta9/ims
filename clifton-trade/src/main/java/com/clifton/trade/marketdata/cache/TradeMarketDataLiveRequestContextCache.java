package com.clifton.trade.marketdata.cache;

import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.trade.marketdata.TradeMarketDataValue;


/**
 * The {@link TradeMarketDataLiveRequestContextCache} interface declares caching methods for data management of backing {@link MarketDataLive} values for usage during {@link
 * TradeMarketDataValue} entity creation.
 *
 * @author mikeh
 */
public interface TradeMarketDataLiveRequestContextCache {

	/**
	 * Retrieves the {@link MarketDataLive} retrieved during the current request for the given market data field name and security ID.
	 *
	 * @param fieldName  the market data field name for the value to get from the cache
	 * @param securityId the security ID for the value to get from the cache
	 * @return the value that was retrieved within the current request context, or <code>null</code> if the value was not found in the cache
	 */
	public MarketDataLive getMarketDataLiveForFieldAndSecurity(String fieldName, int securityId);


	/**
	 * Stores the given {@link MarketDataLive} for the given market data field name and security ID in the current request context cache.
	 *
	 * @param fieldName      the market data field name for the value to store in the cache
	 * @param securityId     the security ID for the value to store in the cache
	 * @param marketDataLive the market data value to store
	 */
	public void putMarketDataLiveForFieldAndSecurity(String fieldName, int securityId, MarketDataLive marketDataLive);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns <code>true</code> if an error has been stored for the given market data field name and security ID.
	 *
	 * @param fieldName  the market data field name for which to check for a cached error
	 * @param securityId the security ID for which to check for a cached error
	 * @return <code>true</code> if an error has been stored in the cache for the given arguments, or <code>false</code> otherwise
	 */
	public boolean isErrorForFieldAndSecurity(String fieldName, int securityId);


	/**
	 * Stores an error in the cache for the given market data field name and security ID. This can be used to indicate that retrievals are failing for a given field name and
	 * security ID combination.
	 *
	 * @param fieldName  the market data field name for which store an error
	 * @param securityId the security ID for which store an error
	 */
	public void putErrorForFieldAndSecurity(String fieldName, int securityId);
}
