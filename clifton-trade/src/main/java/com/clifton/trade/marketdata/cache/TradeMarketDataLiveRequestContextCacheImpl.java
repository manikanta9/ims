package com.clifton.trade.marketdata.cache;

import com.clifton.core.cache.request.AbstractRequestContextCache;
import com.clifton.marketdata.live.MarketDataLive;
import org.springframework.stereotype.Component;

import java.util.Objects;


/**
 * The {@link TradeMarketDataLiveRequestContextCacheImpl} is the default implementation for the {@link TradeMarketDataLiveRequestContextCache} interface.
 *
 * @author mikeh
 */
@Component
public class TradeMarketDataLiveRequestContextCacheImpl
		extends AbstractRequestContextCache<TradeMarketDataLiveRequestContextCacheImpl.RequestContextKey, TradeMarketDataLiveRequestContextCacheImpl.MarketDataLiveValueHolder>
		implements TradeMarketDataLiveRequestContextCache {

	/**
	 * The immutable constant placeholder for {@link #putErrorForFieldAndSecurity(String, int) stored errors}. This is used to mark a value as irretrievable.
	 */
	private static final MarketDataLiveValueHolder ERROR_VALUE_HOLDER = new MarketDataLiveValueHolder(null);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataLive getMarketDataLiveForFieldAndSecurity(String fieldName, int securityId) {
		MarketDataLiveValueHolder valueHolder = get(new RequestContextKey(fieldName, securityId));
		return valueHolder != null ? valueHolder.getMarketDataLive() : null;
	}


	@Override
	public void putMarketDataLiveForFieldAndSecurity(String fieldName, int securityId, MarketDataLive marketDataLive) {
		put(new RequestContextKey(fieldName, securityId), new MarketDataLiveValueHolder(marketDataLive));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isErrorForFieldAndSecurity(String fieldName, int securityId) {
		return get(new RequestContextKey(fieldName, securityId)) == ERROR_VALUE_HOLDER;
	}


	@Override
	public void putErrorForFieldAndSecurity(String fieldName, int securityId) {
		put(new RequestContextKey(fieldName, securityId), ERROR_VALUE_HOLDER);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The cache key type.
	 */
	protected static class RequestContextKey {

		private final String fieldName;
		private final int securityId;


		public RequestContextKey(String fieldName, int securityId) {
			this.fieldName = fieldName;
			this.securityId = securityId;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			RequestContextKey that = (RequestContextKey) o;
			return this.securityId == that.securityId &&
					this.fieldName.equals(that.fieldName);
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.fieldName, this.securityId);
		}


		public String getFieldName() {
			return this.fieldName;
		}


		public int getSecurityId() {
			return this.securityId;
		}
	}


	/**
	 * The cache value type.
	 */
	protected static class MarketDataLiveValueHolder {

		private final MarketDataLive marketDataLive;


		public MarketDataLiveValueHolder(MarketDataLive marketDataLive) {
			this.marketDataLive = marketDataLive;
		}


		public MarketDataLive getMarketDataLive() {
			return this.marketDataLive;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
}
