package com.clifton.trade.marketdata;

import com.clifton.business.service.BusinessService;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.trade.TradeType;


/**
 * <code>TradeMarketDataFieldMapping</code> Provides {@link TradeMarketDataField} selection criteria.
 *
 * @author tstebner
 */
public class TradeMarketDataFieldMapping extends BaseEntity<Integer> {

	/**
	 * The underlying Trade Data Field for which this selection criteria applies.
	 */
	private TradeMarketDataField tradeMarketDataField;

	/**
	 * Determine which trade may use this field based on the Trade Type. (Required)
	 */
	private TradeType tradeType;

	/**
	 * Optional selection criteria based on the trade security instrument group.
	 */
	private InvestmentGroup investmentGroup;

	/**
	 * Optional selection criteria based on the trade client account business service.
	 */
	private BusinessService businessService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeMarketDataField getTradeMarketDataField() {
		return this.tradeMarketDataField;
	}


	public void setTradeMarketDataField(TradeMarketDataField tradeMarketDataField) {
		this.tradeMarketDataField = tradeMarketDataField;
	}


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}


	public InvestmentGroup getInvestmentGroup() {
		return this.investmentGroup;
	}


	public void setInvestmentGroup(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
	}


	public BusinessService getBusinessService() {
		return this.businessService;
	}


	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}
}
