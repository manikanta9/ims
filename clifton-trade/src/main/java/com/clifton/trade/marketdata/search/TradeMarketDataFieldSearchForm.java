package com.clifton.trade.marketdata.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * <code>TradeMarketDataFieldSearchForm</code> provides search criteria for TradeMarketDataField entities.
 *
 * @author tstebner
 */
public class TradeMarketDataFieldSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "marketDataField.id")
	private Short marketDataFieldId;

	@SearchField(searchField = "marketDataField.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String marketDataFieldNameEquals;

	@SearchField(searchField = "marketDataSource.id")
	private Short marketDataSourceId;

	@SearchField(searchField = "valueReadCondition.id")
	private Integer valueReadConditionId;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean underlyingSecurity;

	@SearchField
	private Boolean onlyRetrieveOnce;

	@SearchField(searchField = "tradeWorkflowEndState.id")
	private Short tradeWorkflowEndStateId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getMarketDataFieldId() {
		return this.marketDataFieldId;
	}


	public void setMarketDataFieldId(Short marketDataFieldId) {
		this.marketDataFieldId = marketDataFieldId;
	}


	public String getMarketDataFieldNameEquals() {
		return this.marketDataFieldNameEquals;
	}


	public void setMarketDataFieldNameEquals(String marketDataFieldNameEquals) {
		this.marketDataFieldNameEquals = marketDataFieldNameEquals;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public Integer getValueReadConditionId() {
		return this.valueReadConditionId;
	}


	public void setValueReadConditionId(Integer valueReadConditionId) {
		this.valueReadConditionId = valueReadConditionId;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(Boolean underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public Boolean getOnlyRetrieveOnce() {
		return this.onlyRetrieveOnce;
	}


	public void setOnlyRetrieveOnce(Boolean onlyRetrieveOnce) {
		this.onlyRetrieveOnce = onlyRetrieveOnce;
	}


	public Short getTradeWorkflowEndStateId() {
		return this.tradeWorkflowEndStateId;
	}


	public void setTradeWorkflowEndStateId(Short tradeWorkflowEndStateId) {
		this.tradeWorkflowEndStateId = tradeWorkflowEndStateId;
	}
}
