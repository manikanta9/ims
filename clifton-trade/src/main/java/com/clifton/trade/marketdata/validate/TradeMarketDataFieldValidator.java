package com.clifton.trade.marketdata.validate;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.CoreValidationUtils;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import com.clifton.trade.marketdata.TradeMarketDataField;
import com.clifton.trade.marketdata.TradeMarketDataFieldMapping;
import com.clifton.trade.marketdata.TradeMarketDataFieldService;
import com.clifton.trade.marketdata.search.TradeMarketDataFieldMappingSearchForm;
import com.clifton.trade.marketdata.search.TradeMarketDataValueSearchForm;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionAction;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * <code>TradeMarketDataFieldValidator</code> Do not allow system defined fields to be inserted or deleted.
 *
 * @author tstebner
 */
@Component
public class TradeMarketDataFieldValidator extends SelfRegisteringDaoValidator<TradeMarketDataField> {

	public static final String TRADE_MARKET_DATA_WORKFLOW_TRANSITION_BEAN_NAME = "Trade Market Data Field Value Workflow Action";

	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;
	private TradeMarketDataFieldService tradeMarketDataFieldService;
	private WorkflowTransitionService workflowTransitionService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(TradeMarketDataField field, DaoEventTypes config) {
		validateMarketDataField(field, config);
		validateSystemDefined(field, config);
		validateActiveCriteria(field, config);
		if (config.isDelete()) {
			TradeMarketDataValueSearchForm searchForm = new TradeMarketDataValueSearchForm();
			searchForm.setTradeMarketDataFieldName(field.getName());
			// don't allow deletion when there is data
			ValidationUtils.assertTrue(CollectionUtils.isEmpty(getTradeMarketDataFieldService().getTradeMarketDataValueList(searchForm)),
					String.format("Cannot delete Trade Market Data Fields with data [%s].", field.getName()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateMarketDataField(TradeMarketDataField field, DaoEventTypes config) {
		if (config.isUpdate() || config.isInsert()) {
			// Validate compatible market data field properties
			ValidationUtils.assertTrue(field.getMarketDataField().isTimeSensitive(), "Market Data Field requires [Capture the time stamp of the valuation time = true].");
			ValidationUtils.assertFalse(field.getMarketDataField().isUpToOnePerDay(), "Market Data Field requires [Allow no more than one value per data source per day = false]");
			ValidationUtils.assertFalse(field.getMarketDataField().isLatestValueOnly(), "Market Data Field requires [Do not keep history but overwrite latest value = false]");

			// Validate that the market data field has the correct tag assigned
			List<SystemHierarchyLink> marketDataFieldHierarchyLinkList = getSystemHierarchyAssignmentService().getSystemHierarchyLinkList("MarketDataField", field.getMarketDataField().getId(), MarketDataField.MARKET_DATA_TAG_CATEGORY_NAME);
			boolean tradePricesMarketDataField = marketDataFieldHierarchyLinkList.stream()
					.map(SystemHierarchyLink::getHierarchy)
					.map(NamedEntityWithoutLabel::getName)
					.anyMatch(TradeMarketDataField.TRADE_FIELD_TAG_NAME::equals);
			ValidationUtils.assertTrue(tradePricesMarketDataField, () -> String.format("The referenced Market Data Field for Trade Market Data Fields must have the \"%s\" tag attached.", TradeMarketDataField.TRADE_FIELD_TAG_NAME), "marketDataField");
		}
	}


	private void validateSystemDefined(TradeMarketDataField field, DaoEventTypes config) {
		if (config.isInsert()) {
			ValidationUtils.assertFalse(field.isSystemDefined(), "Adding System Defined Trade Market Data Fields is not allowed.");
		}
		if (config.isUpdate()) {
			CoreValidationUtils.assertDisallowedModifiedFields(field, getOriginalBean(field), "systemDefined");
			if (field.isSystemDefined()) {
				CoreValidationUtils.assertDisallowedModifiedFields(field, getOriginalBean(field), "name");
			}
		}
		if (config.isDelete()) {
			ValidationUtils.assertFalse(field.isSystemDefined(), "System Defined fields cannot be deleted.");
		}
	}


	private void validateActiveCriteria(TradeMarketDataField field, DaoEventTypes config) {
		if (config.isInsert() || config.isUpdate()) {
			if (field.isActive()) {
				// Validate that each transition to the end state includes an action to execute market data retrieval
				List<WorkflowTransition> transitionToStateList = getWorkflowTransitionService().getWorkflowTransitionListToEndState(field.getTradeWorkflowEndState());
				List<WorkflowTransition> unmatchedTransitionList = CollectionUtils.getFiltered(transitionToStateList, transition -> {
					List<WorkflowTransitionAction> transitionActionList = getWorkflowTransitionService().getWorkflowTransitionActionListForTransitionId(transition.getId());
					return !CollectionUtils.anyMatch(transitionActionList, action -> TRADE_MARKET_DATA_WORKFLOW_TRANSITION_BEAN_NAME.equals(action.getActionSystemBean().getName()));
				});
				ValidationUtils.assertTrue(unmatchedTransitionList.isEmpty(), () -> String.format("The Trade Market Data Field [%s] is missing a workflow transition action for transitions to the ending state [%s] from the start state(s): %s", field.getName(), field.getTradeWorkflowEndState().getName(), CollectionUtils.getConverted(unmatchedTransitionList, transition -> BeanUtils.getLabel(transition.getStartWorkflowState()))));

				// Validate that at least one selection criteria mapping exists
				TradeMarketDataFieldMappingSearchForm fieldMappingSearchForm = new TradeMarketDataFieldMappingSearchForm();
				fieldMappingSearchForm.setTradeMarketDataFieldId(field.getId());
				List<TradeMarketDataFieldMapping> mappingList = getTradeMarketDataFieldService().getTradeMarketDataFieldMappingList(fieldMappingSearchForm);
				ValidationUtils.assertFalse(mappingList.isEmpty(), String.format("The Trade Market Data Field [%s] does not have any selection criteria mappings.", field.getName()));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public TradeMarketDataFieldService getTradeMarketDataFieldService() {
		return this.tradeMarketDataFieldService;
	}


	public void setTradeMarketDataFieldService(TradeMarketDataFieldService tradeMarketDataFieldService) {
		this.tradeMarketDataFieldService = tradeMarketDataFieldService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
