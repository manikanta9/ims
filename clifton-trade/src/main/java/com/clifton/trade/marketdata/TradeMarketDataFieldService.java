package com.clifton.trade.marketdata;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.trade.Trade;
import com.clifton.trade.marketdata.search.TradeMarketDataFieldMappingSearchForm;
import com.clifton.trade.marketdata.search.TradeMarketDataFieldSearchForm;
import com.clifton.trade.marketdata.search.TradeMarketDataValueSearchForm;
import com.clifton.workflow.definition.WorkflowState;

import java.util.List;


/**
 * <code>TradeMarketDataFieldService</code> Provides business logic service methods for fetching, saving, deleting and updating TradeMarketDataField and TradeMarketDataValue entities.
 * Read only access to TradeMarketDataValue entities is also determined by the associated Trade Market Data Field's valueReadCondition SystemCondition.  Currently read permissions require
 * a user's membership in the Bloomberg System.
 *
 * @author tstebner
 */
public interface TradeMarketDataFieldService {

	public TradeMarketDataField getTradeMarketDataField(short id);


	public TradeMarketDataField getTradeMarketDataFieldByName(String name);


	public List<TradeMarketDataField> getTradeMarketDataFieldList(TradeMarketDataFieldSearchForm searchForm);


	/**
	 * Retrieves the list of {@link TradeMarketDataField} entities which apply to the given trade for the given workflow end state.
	 * <p>
	 * This filters the full available list of fields to those who are active and whose mappings match the given trade.
	 *
	 * @param trade    the trade for which fields should be retrieved
	 * @param endState the workflow end state to which fields shall apply, or <code>null</code> to exclude end state filtering
	 * @return the list of applicable fields for the trade
	 */
	public List<TradeMarketDataField> getTradeMarketDataFieldListForTradeAndEndState(Trade trade, WorkflowState endState);


	public TradeMarketDataField saveTradeMarketDataField(TradeMarketDataField bean);


	public void deleteTradeMarketDataField(short id);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Return the TradeMarketDataValue entity based on whether the user has read permissions determined by the associated TradeMarketDataField entity's valueReadCondition
	 * SystemCondition evaluation
	 */
	public TradeMarketDataValue getTradeMarketDataValue(int id);


	/**
	 * Returns the latest {@link TradeMarketDataValue} for the given {@link MarketDataField} which applies to the given {@link Trade}. In most cases, this is the most relevant
	 * market data value when used for evaluation, as previous values are stored for tracking purposes only.
	 * <p>
	 * This method returns <i>unfiltered</i> values, meaning that per-field security access is not evaluated. As such, it is marked with {@link DoNotAddRequestMapping}.
	 */
	@DoNotAddRequestMapping
	public TradeMarketDataValue getTradeMarketDataValueLatestForTradeAndBackingFieldUnfiltered(Trade trade, MarketDataField marketDataField);


	/**
	 * Return the filtered list of TradeMarketDataValue entities based on whether the user has read permissions determined by the associated TradeMarketDataField entity's
	 * valueReadCondition SystemCondition evaluation
	 */
	public List<TradeMarketDataValue> getTradeMarketDataValueList(TradeMarketDataValueSearchForm searchForm);


	/**
	 * Returns the list of {@link TradeMarketDataValue} entities for the provided search form. The list returned from this method will <i>not</i> be filtered according to the field
	 * {@link TradeMarketDataField#valueReadCondition read condition}.
	 */
	@DoNotAddRequestMapping
	public List<TradeMarketDataValue> getTradeMarketDataValueListUnfiltered(TradeMarketDataValueSearchForm searchForm);


	public TradeMarketDataValue saveTradeMarketDataValue(TradeMarketDataValue bean);


	public void deleteTradeMarketDataValue(int id);


	public void deleteTradeMarketDataValueList(List<TradeMarketDataValue> valueList);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = TradeMarketDataField.class)
	public TradeMarketDataFieldMapping getTradeMarketDataFieldMapping(int id);


	@SecureMethod(dtoClass = TradeMarketDataField.class)
	public List<TradeMarketDataFieldMapping> getTradeMarketDataFieldMappingList(TradeMarketDataFieldMappingSearchForm searchForm);


	@SecureMethod(dtoClass = TradeMarketDataField.class)
	public TradeMarketDataFieldMapping saveTradeMarketDataFieldMapping(TradeMarketDataFieldMapping bean);


	@SecureMethod(dtoClass = TradeMarketDataField.class)
	public void deleteTradeMarketDataFieldMapping(int id);


	@SecureMethod(dtoClass = TradeMarketDataField.class)
	public void deleteTradeMarketDataFieldMappingList(List<TradeMarketDataFieldMapping> valueList);
}
