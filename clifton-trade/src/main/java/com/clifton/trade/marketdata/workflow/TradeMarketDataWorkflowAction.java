package com.clifton.trade.marketdata.workflow;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.marketdata.TradeMarketDataField;
import com.clifton.trade.marketdata.TradeMarketDataFieldService;
import com.clifton.trade.marketdata.TradeMarketDataLiveCommand;
import com.clifton.trade.marketdata.TradeMarketDataLiveService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * <code>TradeMarketDataWorkflowAction</code> records TradeMarketDataValues upon a workflow transition.
 *
 * @author tstebner
 */
public class TradeMarketDataWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private static final String RUNNER_THREAD_NAME = "TRADE_MARKET_DATA_FIELD_VALUES";

	private ContextHandler contextHandler;
	private RunnerHandler runnerHandler;
	private SecurityImpersonationHandler securityImpersonationHandler;
	private SecurityUserService securityUserService;
	private TradeMarketDataFieldService tradeMarketDataFieldService;
	private TradeMarketDataLiveService tradeMarketDataLiveService;

	/**
	 * The timeout (in milliseconds) for synchronous retrieval attempts.
	 */
	private int synchronousTimeout;
	/**
	 * If <code>true</code>, then the {@link MarketDataProviderOverrides#TERMINAL Bloomberg terminal} will be used instead of {@link MarketDataProviderOverrides#BPIPE B-Pipes} for
	 * market data retrieval.
	 * <p>
	 * TODO: We would like to remove uses of the terminal. Remove this property.
	 */
	private boolean useTerminal;
	/**
	 * If <code>true</code> and a request is active, then the request context cache will be used. This will cache retrieved values across the duration of the request, preventing
	 * redundant requests.
	 */
	private boolean useRequestCache;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade processAction(Trade trade, WorkflowTransition transition) {
		// Guard-clause: Trivial case
		List<TradeMarketDataField> tradeMarketDataFieldList = getTradeMarketDataFieldService().getTradeMarketDataFieldListForTradeAndEndState(trade, transition.getEndWorkflowState());
		if (tradeMarketDataFieldList.isEmpty()) {
			return trade;
		}

		// Add the client IP address for context overriding in order to use BPipe reference data lookups
		// If the transition is initiated by the system and not a user, the client IP will not be defined
		Object clientIp = getContextHandler().getBean(Context.REQUEST_CLIENT_ADDRESS);
		Map<String, Object> contextOverridesMap;
		if (clientIp == null) {
			contextOverridesMap = Collections.emptyMap();
		}
		else {
			contextOverridesMap = MapUtils.of(Context.REQUEST_CLIENT_ADDRESS, getContextHandler().getBean(Context.REQUEST_CLIENT_ADDRESS));
		}

		// Process trade market data fields
		Map<Boolean, List<TradeMarketDataField>> fieldListMapBySynchronous = CollectionUtils.getPartitioned(tradeMarketDataFieldList, TradeMarketDataField::isSynchronous);
		processAsyncTradeMarketDataFieldList(fieldListMapBySynchronous.get(false), trade, contextOverridesMap);
		processSyncTradeMarketDataFieldList(fieldListMapBySynchronous.get(true), trade, contextOverridesMap);
		return trade;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void processAsyncTradeMarketDataFieldList(List<TradeMarketDataField> fieldList, Trade trade, Map<String, Object> contextOverridesMap) {
		// Guard-clause: No fields to process
		if (fieldList.isEmpty()) {
			return;
		}

		// Create each value using singular runners
		String runnerName = "TRADE_" + trade.getId() + "_FIELDS_" + CollectionUtils.getConverted(fieldList, NamedEntityWithoutLabel::getName);
		getRunnerHandler().runNow(new AbstractStatusAwareRunner(RUNNER_THREAD_NAME, runnerName, new Date(), "Trade Market Data value capture.") {
			@Override
			public void run() {
				createTradeMarketDataValueList(fieldList, trade, getStatus(), contextOverridesMap);
			}
		});
	}


	private void processSyncTradeMarketDataFieldList(List<TradeMarketDataField> fieldList, Trade trade, Map<String, Object> contextOverridesMap) {
		// Guard-clause: No fields to process
		if (fieldList.isEmpty()) {
			return;
		}

		// Create trade market data values
		createTradeMarketDataValueList(fieldList, trade, null, contextOverridesMap);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Fetches and saves a live value for each {@link TradeMarketDataField} for the provided {@link Trade}. The values are looked up by impersonating the trade's trader user
	 * and any context property overrides will be set while for the execution of the task.
	 * <p>
	 * One potential context override includes the IP address depending on the {@link com.clifton.marketdata.provider.MarketDataProvider} used to get live data values.
	 * For example, Bloomberg BPipe requires the user and IP address to determine entitlements.
	 */
	private void createTradeMarketDataValueList(List<TradeMarketDataField> fieldList, Trade trade, Status status, Map<String, Object> contextOverridesMap) {
		getSecurityImpersonationHandler().runAsSecurityUser(trade.getTraderUser(), () -> {
			try {
				TradeMarketDataLiveCommand tradeMarketDataLiveCommand = new TradeMarketDataLiveCommand();
				tradeMarketDataLiveCommand.setTrade(trade);
				tradeMarketDataLiveCommand.setFieldList(fieldList);
				tradeMarketDataLiveCommand.setUseTerminal(isUseTerminal());
				tradeMarketDataLiveCommand.setUseRequestCache(isUseRequestCache());
				tradeMarketDataLiveCommand.setProviderTimeoutOverride(getSynchronousTimeout());
				tradeMarketDataLiveCommand.setIgnoreFailures(true);
				getTradeMarketDataLiveService().createTradeMarketDataValuesForTrade(tradeMarketDataLiveCommand);
			}
			// log then swallow all exceptions; does not require all-or-nothing.
			catch (Exception e) {
				List<String> fieldNameList = CollectionUtils.getConverted(fieldList, NamedEntityWithoutLabel::getName);
				String errorMessage = String.format("Unable to save trade market data for trade [%s] and field(s) %s.", trade.getId(), fieldNameList);
				LogUtils.error(LogCommand.ofThrowableAndMessage(TradeMarketDataWorkflowAction.class, e, errorMessage));
				if (status != null) {
					status.addError(errorMessage);
				}
			}
		}, contextOverridesMap);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public SecurityImpersonationHandler getSecurityImpersonationHandler() {
		return this.securityImpersonationHandler;
	}


	public void setSecurityImpersonationHandler(SecurityImpersonationHandler securityImpersonationHandler) {
		this.securityImpersonationHandler = securityImpersonationHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TradeMarketDataFieldService getTradeMarketDataFieldService() {
		return this.tradeMarketDataFieldService;
	}


	public void setTradeMarketDataFieldService(TradeMarketDataFieldService tradeMarketDataFieldService) {
		this.tradeMarketDataFieldService = tradeMarketDataFieldService;
	}


	public TradeMarketDataLiveService getTradeMarketDataLiveService() {
		return this.tradeMarketDataLiveService;
	}


	public void setTradeMarketDataLiveService(TradeMarketDataLiveService tradeMarketDataLiveService) {
		this.tradeMarketDataLiveService = tradeMarketDataLiveService;
	}


	public int getSynchronousTimeout() {
		return this.synchronousTimeout;
	}


	public void setSynchronousTimeout(int synchronousTimeout) {
		this.synchronousTimeout = synchronousTimeout;
	}


	public boolean isUseTerminal() {
		return this.useTerminal;
	}


	public void setUseTerminal(boolean useTerminal) {
		this.useTerminal = useTerminal;
	}


	public boolean isUseRequestCache() {
		return this.useRequestCache;
	}


	public void setUseRequestCache(boolean useRequestCache) {
		this.useRequestCache = useRequestCache;
	}
}
