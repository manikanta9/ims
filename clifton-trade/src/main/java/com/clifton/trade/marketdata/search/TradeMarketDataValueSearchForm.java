package com.clifton.trade.marketdata.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.date.Time;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>TradeMarketDataValueSearchForm</code> provides search criteria for the related TradeMarketDataValue, MarketDataValue and the linkage table.
 *
 * @author tstebner
 */
public class TradeMarketDataValueSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer tradeId;

	@SearchField(searchField = "referenceOne.id")
	private Integer[] tradeIds;

	@SearchField(searchField = "tradeMarketDataField.id")
	private Short tradeMarketDataFieldId;

	@SearchField(searchField = "tradeMarketDataField.id")
	private Short[] tradeMarketDataFieldIds;

	@SearchField(searchField = "tradeMarketDataField.name")
	private String tradeMarketDataFieldName;

	@SearchField(searchField = "tradeMarketDataField.marketDataSource.id")
	private Short marketDataSourceId;

	@SearchField(searchField = "tradeMarketDataField.marketDataSource.name")
	private String marketDataSourceName;

	@SearchField(searchField = "tradeMarketDataField.marketDataField.id")
	private Short marketDataFieldId;

	@SearchField(searchField = "tradeMarketDataField.marketDataField.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String marketDataFieldNameEquals;

	@SearchField(searchField = "referenceTwo.id")
	private Integer marketDataValueId;

	@SearchField(searchField = "referenceTwo.measureDate")
	private Date measureDate;

	@SearchField(searchField = "referenceTwo.measureTime")
	private Time measureTime;

	@SearchField(searchField = "referenceTwo.measureValue")
	private BigDecimal measureValue;

	@SearchField(searchField = "tradeMarketDataField.tradeWorkflowEndState.id")
	private Short tradeWorkflowEndStateId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public Integer[] getTradeIds() {
		return this.tradeIds;
	}


	public void setTradeIds(Integer[] tradeIds) {
		this.tradeIds = tradeIds;
	}


	public Short getTradeMarketDataFieldId() {
		return this.tradeMarketDataFieldId;
	}


	public void setTradeMarketDataFieldId(Short tradeMarketDataFieldId) {
		this.tradeMarketDataFieldId = tradeMarketDataFieldId;
	}


	public Short[] getTradeMarketDataFieldIds() {
		return this.tradeMarketDataFieldIds;
	}


	public void setTradeMarketDataFieldIds(Short[] tradeMarketDataFieldIds) {
		this.tradeMarketDataFieldIds = tradeMarketDataFieldIds;
	}


	public String getTradeMarketDataFieldName() {
		return this.tradeMarketDataFieldName;
	}


	public void setTradeMarketDataFieldName(String tradeMarketDataFieldName) {
		this.tradeMarketDataFieldName = tradeMarketDataFieldName;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public String getMarketDataSourceName() {
		return this.marketDataSourceName;
	}


	public void setMarketDataSourceName(String marketDataSourceName) {
		this.marketDataSourceName = marketDataSourceName;
	}


	public Short getMarketDataFieldId() {
		return this.marketDataFieldId;
	}


	public void setMarketDataFieldId(Short marketDataFieldId) {
		this.marketDataFieldId = marketDataFieldId;
	}


	public String getMarketDataFieldNameEquals() {
		return this.marketDataFieldNameEquals;
	}


	public void setMarketDataFieldNameEquals(String marketDataFieldNameEquals) {
		this.marketDataFieldNameEquals = marketDataFieldNameEquals;
	}


	public Integer getMarketDataValueId() {
		return this.marketDataValueId;
	}


	public void setMarketDataValueId(Integer marketDataValueId) {
		this.marketDataValueId = marketDataValueId;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Time getMeasureTime() {
		return this.measureTime;
	}


	public void setMeasureTime(Time measureTime) {
		this.measureTime = measureTime;
	}


	public BigDecimal getMeasureValue() {
		return this.measureValue;
	}


	public void setMeasureValue(BigDecimal measureValue) {
		this.measureValue = measureValue;
	}


	public Short getTradeWorkflowEndStateId() {
		return this.tradeWorkflowEndStateId;
	}


	public void setTradeWorkflowEndStateId(Short tradeWorkflowEndStateId) {
		this.tradeWorkflowEndStateId = tradeWorkflowEndStateId;
	}
}
