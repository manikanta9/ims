package com.clifton.trade.marketdata;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.trade.Trade;


/**
 * The <code>TradeFieldValue</code> Create a reference between a Trade and a MarketDataValue.
 * Values are persisted after workflow transitions by TradeMarketDataWorkflowAction.
 * Permissions are determined by membership to the group associated with the resource [Trade Market Data / Trade Data Field Values]
 * and access to the Bloomberg Security System.
 *
 * @author tstebner
 */
public class TradeMarketDataValue extends ManyToManyEntity<Trade, MarketDataValue, Integer> {

	/**
	 * Required link to the TradeMarketData Field.
	 */
	private TradeMarketDataField tradeMarketDataField;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeMarketDataField getTradeMarketDataField() {
		return this.tradeMarketDataField;
	}


	public void setTradeMarketDataField(TradeMarketDataField tradeMarketDataField) {
		this.tradeMarketDataField = tradeMarketDataField;
	}
}
