package com.clifton.trade.marketdata.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * <code>TradeMarketDataFieldMappingSearchForm</code>
 *
 * @author tstebner
 */
public class TradeMarketDataFieldMappingSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "tradeMarketDataField.id")
	private Short tradeMarketDataFieldId;

	@SearchField(searchField = "tradeType.id")
	private Short tradeTypeId;

	@SearchField(searchField = "investmentGroup.id")
	private Short investmentGroupId;

	@SearchField(searchField = "businessService.id")
	private Short businessServiceId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getTradeMarketDataFieldId() {
		return this.tradeMarketDataFieldId;
	}


	public void setTradeMarketDataFieldId(Short tradeMarketDataFieldId) {
		this.tradeMarketDataFieldId = tradeMarketDataFieldId;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}
}
