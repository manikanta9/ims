package com.clifton.trade.marketdata;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.trade.Trade;

import java.util.List;


/**
 * <code>TradeMarketDataLiveService</code> query for the latest {@link com.clifton.marketdata.field.MarketDataValue} and create the {@link TradeMarketDataValue} entity with
 * links to the returned {@link com.clifton.marketdata.field.MarketDataValue}.
 *
 * @author tstebner
 */
public interface TradeMarketDataLiveService {

	/**
	 * Fetches and saves live market data for the provided {@link Trade} and {@link TradeMarketDataField market data field} list. The resulting {@link TradeMarketDataValue}
	 * entities will be returned.
	 */
	@DoNotAddRequestMapping
	public List<TradeMarketDataValue> createTradeMarketDataValuesForTrade(TradeMarketDataLiveCommand tradeMarketDataLiveCommand);


	/**
	 * Fetches live market data for the provided {@link Trade} and {@link TradeMarketDataField market data field} list. The resulting {@link TradeMarketDataValue} are not saved.
	 */
	@DoNotAddRequestMapping
	public List<TradeMarketDataValue> retrieveTradeMarketDataValuesForTrade(TradeMarketDataLiveCommand tradeMarketDataLiveCommand);


	/**
	 * Saves the given {@link TradeMarketDataValue} and its backing {@link MarketDataValue}.
	 */
	@DoNotAddRequestMapping
	public TradeMarketDataValue saveTradeMarketDataValueAndBackingValue(TradeMarketDataValue tradeMarketDataValue);
}
