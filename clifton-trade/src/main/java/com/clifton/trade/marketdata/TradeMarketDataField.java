package com.clifton.trade.marketdata;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import com.clifton.workflow.definition.WorkflowState;


/**
 * The <code>TradeMarketDataField</code> Makes market data field values available for capture at specific times during a trade's execution.
 * The Market Data Values are linked to NOT copied; before linking to a market data field value, make sure the data retention settings will
 * keep the data value indefinitely.
 * <p>
 * Permissions are determined by membership to the group associated with the resource [Trade Market Data / Trade Market Data Setup]
 *
 * @author tstebner
 */
@CacheByName
public class TradeMarketDataField extends NamedEntityWithoutLabel<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	/**
	 * The name of the {@link SystemHierarchy} (a.k.a. tag) which should apply to linked {@link MarketDataField} entities.
	 */
	public static final String TRADE_FIELD_TAG_NAME = "Trade Prices";

	/**
	 * The underlying Market Data field validations:
	 * - IsUpToOnePerDay = false
	 * - IsLatestValueOnly = false
	 * - IsCaptureChangesOnly = false
	 * - IsTimeSensitive = true
	 * These requirements guarantee the market data value is never overwritten.
	 */
	private MarketDataField marketDataField;

	/**
	 * The data source for fetching the market data values, probably Bloomberg.
	 */
	private MarketDataSource marketDataSource;

	/**
	 * Determine who has read permissions to the associated field values.
	 */
	private SystemCondition valueReadCondition;

	/**
	 * System defined fields cannot be edited.
	 */
	private boolean systemDefined;

	/**
	 * Attempt to use the Underlying Security if it exists when looking up the field value.
	 */
	private boolean underlyingSecurity;

	/**
	 * Enable the data capture for this field.
	 */
	private boolean active;


	/**
	 * Query only once for the data for this field.
	 */
	private boolean onlyRetrieveOnce;


	/**
	 * If <code>true</code>, market data retrieval will be performed synchronously. This is helpful in pre-validation steps where market data is required for validation. Otherwise,
	 * market data retrieval will be done asynchronously.
	 * <p>
	 * This flag is used for performance optimization.
	 */
	private boolean synchronous;

	/**
	 * Required link to the WorkflowState of the associated trade at the point the data values are captured.
	 * This is the transition END state.
	 */
	private WorkflowState tradeWorkflowEndState;

	/**
	 * Used to limit who has access to make edits to this {@link TradeMarketDataField}.
	 */
	private SystemCondition entityModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isEntityModifyConditionRequiredForNonAdmins() {
		return isSystemDefined();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataField getMarketDataField() {
		return this.marketDataField;
	}


	public void setMarketDataField(MarketDataField marketDataField) {
		this.marketDataField = marketDataField;
	}


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public SystemCondition getValueReadCondition() {
		return this.valueReadCondition;
	}


	public void setValueReadCondition(SystemCondition valueReadCondition) {
		this.valueReadCondition = valueReadCondition;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(boolean underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public boolean isOnlyRetrieveOnce() {
		return this.onlyRetrieveOnce;
	}


	public void setOnlyRetrieveOnce(boolean onlyRetrieveOnce) {
		this.onlyRetrieveOnce = onlyRetrieveOnce;
	}


	public WorkflowState getTradeWorkflowEndState() {
		return this.tradeWorkflowEndState;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public void setTradeWorkflowEndState(WorkflowState tradeWorkflowEndState) {
		this.tradeWorkflowEndState = tradeWorkflowEndState;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}
}
