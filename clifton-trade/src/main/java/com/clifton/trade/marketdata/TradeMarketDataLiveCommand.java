package com.clifton.trade.marketdata;

import com.clifton.trade.Trade;

import java.util.List;


/**
 * The {@link TradeMarketDataLiveCommand} is a parameter object for {@link TradeMarketDataValue} retrieval operations. This contains fields used to determine what to retrieve and
 * how to retrieve it.
 */
public class TradeMarketDataLiveCommand {

	/**
	 * The {@link Trade} for which to retrieve market data. Market data will be attached to this trade as {@link TradeMarketDataValue} entities.
	 */
	private Trade trade;
	/**
	 * The list of {@link TradeMarketDataField market data fields} to retrieve.
	 */
	private List<TradeMarketDataField> fieldList;
	/**
	 * If <code>true</code, the Bloomberg terminal will be used for reference requests.
	 */
	private boolean useTerminal;
	/**
	 * If <code>true</code> and a request is active, then the request context cache will be used. This will cache retrieved values across the duration of the request, preventing
	 * redundant requests.
	 */
	private boolean useRequestCache;
	/**
	 * The provider timeout (in milliseconds) to use when executing requests.
	 */
	private Integer providerTimeoutOverride;
	/**
	 * If <code>true</code>, failures during retrieval will be ignored.
	 */
	private boolean ignoreFailures;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	public List<TradeMarketDataField> getFieldList() {
		return this.fieldList;
	}


	public void setFieldList(List<TradeMarketDataField> fieldList) {
		this.fieldList = fieldList;
	}


	public boolean isUseTerminal() {
		return this.useTerminal;
	}


	public void setUseTerminal(boolean useTerminal) {
		this.useTerminal = useTerminal;
	}


	public boolean isUseRequestCache() {
		return this.useRequestCache;
	}


	public void setUseRequestCache(boolean useRequestCache) {
		this.useRequestCache = useRequestCache;
	}


	public Integer getProviderTimeoutOverride() {
		return this.providerTimeoutOverride;
	}


	public void setProviderTimeoutOverride(Integer providerTimeoutOverride) {
		this.providerTimeoutOverride = providerTimeoutOverride;
	}


	public boolean isIgnoreFailures() {
		return this.ignoreFailures;
	}


	public void setIgnoreFailures(boolean ignoreFailures) {
		this.ignoreFailures = ignoreFailures;
	}
}
