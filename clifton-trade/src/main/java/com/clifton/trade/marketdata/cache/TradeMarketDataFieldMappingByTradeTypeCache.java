package com.clifton.trade.marketdata.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.trade.TradeType;
import com.clifton.trade.marketdata.TradeMarketDataFieldMapping;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeMarketDataFieldMappingByTradeTypeCache</code> type is a self-registering cache to cache {@link TradeMarketDataFieldMapping} entities by {@link TradeType}.
 * <p>
 * Caching TradeMarketDataFieldMapping entities by trade type allows for a performant determination of whether TradeMarketDataValues
 * need to be captured during a trade's execution.
 *
 * @author tstebner
 */
@Component
public class TradeMarketDataFieldMappingByTradeTypeCache extends SelfRegisteringSingleKeyDaoListCache<TradeMarketDataFieldMapping, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "tradeType.id";
	}


	@Override
	protected Short getBeanKeyValue(TradeMarketDataFieldMapping bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getTradeType());
		}
		return null;
	}
}
