package com.clifton.trade;


import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.trade.accounting.commission.TradeCommissionOverride;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.trade.accounting.position.TradePositionService;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.search.TradeFillSearchForm;
import com.clifton.trade.search.TradeOpenCloseTypeSearchForm;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.trade.search.TradeSearchFormConfigurer;
import com.clifton.trade.search.TradeSourceSearchForm;
import com.clifton.trade.search.TradeTimeInForceTypeSearchForm;
import com.clifton.trade.search.TradeTypeSearchForm;
import com.clifton.trade.util.TradeFillUtilHandler;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.history.WorkflowHistoryService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * The <code>TradeServiceImpl</code> class provides basic implementation of the TradeService interface.
 */
@Service
public class TradeServiceImpl implements TradeService {

	private AdvancedUpdatableDAO<TradeType, Criteria> tradeTypeDAO;
	private AdvancedUpdatableDAO<TradeOpenCloseType, Criteria> tradeOpenCloseTypeDAO;
	private AdvancedUpdatableDAO<TradeSource, Criteria> tradeSourceDAO;
	private AdvancedUpdatableDAO<Trade, Criteria> tradeDAO;
	private AdvancedUpdatableDAO<TradeFill, Criteria> tradeFillDAO;
	private AdvancedUpdatableDAO<BusinessCompany, Criteria> businessCompanyDAO;
	private AdvancedUpdatableDAO<TradeTimeInForceType, Criteria> tradeTimeInForceTypeDAO;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentGroupService investmentGroupService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataService marketDataService;
	private TradeCommissionService tradeCommissionService;
	// NOTE: Circular dependency - need position service to calculate accrued interest
	private TradeFillUtilHandler tradeFillUtilHandler;
	// NOTE: Circular dependency - need group service to look up types by name for faster searches for trades
	private TradeGroupService tradeGroupService;
	private TradePositionService tradePositionService;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowHistoryService workflowHistoryService;

	private DaoNamedEntityCache<TradeType> tradeTypeCache;
	private DaoNamedEntityCache<TradeOpenCloseType> tradeOpenCloseTypeCache;
	private DaoNamedEntityCache<TradeSource> tradeSourceCache;
	private DaoNamedEntityCache<TradeTimeInForceType> tradeTimeInForceTypeCache;
	// Caches list of TradeTypes by InvestmentType.ID value
	private DaoSingleKeyListCache<TradeType, Short> tradeTypeListByInvestmentTypeCache;

	////////////////////////////////////////////////////////////////////////////
	//////                   Trade Type Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeType getTradeType(short id) {
		return getTradeTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeType getTradeTypeByName(String name) {
		return getTradeTypeCache().getBeanForKeyValue(getTradeTypeDAO(), name);
	}


	private List<TradeType> getTradeTypeListByInvestmentType(short investmentTypeId) {
		return getTradeTypeListByInvestmentTypeCache().getBeanListForKeyValue(getTradeTypeDAO(), investmentTypeId);
	}


	@Override
	public TradeType getTradeTypeForSecurity(int investmentSecurityId) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(investmentSecurityId);
		List<TradeType> tradeTypeList = getTradeTypeListByInvestmentType(security.getInstrument().getHierarchy().getInvestmentType().getId());

		TradeType type = null;
		for (TradeType tt : CollectionUtils.getIterable(tradeTypeList)) {
			if (tt.getInvestmentGroup() == null) {
				// Only set default if not already found a group specific one
				if (type == null) {
					type = tt;
				}
			}
			else {
				if (getInvestmentGroupService().isInvestmentSecurityInGroup(tt.getInvestmentGroup().getName(), investmentSecurityId)) {
					// Set group specific if none found yet, or default only found (i.e. no group)
					if (type == null || type.getInvestmentGroup() == null) {
						type = tt;
					}
					// Otherwise found multiple trade types assigned to different groups that this security falls under...don't know which one is correct
					else {
						throw new ValidationException("Found at least two trade types with investment groups that apply to security [" + security.getLabel() + "]: " + tt.getName()
								+ " (Investment Group: " + tt.getInvestmentGroup().getName() + "), " + type.getName() + " (Investment Group: " + type.getInvestmentGroup().getName() + "). ");
					}
				}
			}
		}

		if (type == null) {
			throw new ValidationException("Cannot determine trade type for security [" + security.getLabel() + "].  There is no default trade type assigned to investment type ["
					+ security.getInstrument().getHierarchy().getInvestmentType().getName() + "].");
		}
		return type;
	}


	@Override
	public List<TradeType> getTradeTypeList(TradeTypeSearchForm searchForm) {
		return getTradeTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	//Used so in memory database migrations can save a trade type.
	@Override
	public void saveTradeType(TradeType tradeType) {
		getTradeTypeDAO().save(tradeType);
	}

	////////////////////////////////////////////////////////////////////////////
	//////               Trade Open Close Type Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeOpenCloseType getTradeOpenCloseType(short id) {
		return getTradeOpenCloseTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeOpenCloseType getTradeOpenCloseTypeByName(String name) {
		return getTradeOpenCloseTypeCache().getBeanForKeyValue(getTradeOpenCloseTypeDAO(), name);
	}


	@Override
	public List<TradeOpenCloseType> getTradeOpenCloseTypeList(TradeOpenCloseTypeSearchForm searchForm) {
		TradeType tradeType = null;
		if (searchForm.getTradeTypeId() != null) {
			tradeType = getTradeType(searchForm.getTradeTypeId());
			if (!tradeType.isExplicitOpenCloseSelectionAllowed() && !tradeType.isExplicitOpenCloseSelectionRequired()) {
				searchForm.setOpen(true);
				searchForm.setClose(true);
			}
		}
		final TradeType finalTradeType = tradeType;
		return getTradeOpenCloseTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (finalTradeType != null && finalTradeType.isExplicitOpenCloseSelectionRequired()) {
					criteria.add(Restrictions.neProperty("open", "close"));
				}
			}
		});
	}

	////////////////////////////////////////////////////////////////////////////
	//////                     Trade Source Methods                      ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeSource getTradeSource(short id) {
		return getTradeSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeSource getTradeSourceByName(String name) {
		return getTradeSourceCache().getBeanForKeyValue(getTradeSourceDAO(), name);
	}


	@Override
	public TradeSource saveTradeSource(TradeSource tradeSource) {
		return getTradeSourceDAO().save(tradeSource);
	}


	@Override
	public List<TradeSource> getTradeSourceList(TradeSourceSearchForm searchForm) {
		return getTradeSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	//////               Trade Time In Force Type Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeTimeInForceType getTradeTimeInForceType(short id) {
		return getTradeTimeInForceTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeTimeInForceType getTradeTimeInForceTypeByName(String name) {
		return getTradeTimeInForceTypeCache().getBeanForKeyValue(getTradeTimeInForceTypeDAO(), name);
	}


	@Override
	public TradeTimeInForceType saveTradeTimeInForceType(TradeTimeInForceType tradeTimeInForceType) {
		return getTradeTimeInForceTypeDAO().save(tradeTimeInForceType);
	}


	@Override
	public List<TradeTimeInForceType> getTradeTimeInForceTypeList(TradeTimeInForceTypeSearchForm searchForm) {
		return getTradeTimeInForceTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	//////                        Trade Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public Trade getTrade(int id) {
		Trade trade = getTradeDAO().findByPrimaryKey(id);
		if (trade != null) {
			trade.setTradeFillList(getTradeFillListByTrade(id));
		}
		return trade;
	}


	@Override
	public Trade getOpeningTradeForAccountingPosition(AccountingPosition position) {
		AccountingJournal journal = position.getOpeningTransaction().getJournal();
		AssertUtils.assertTrue(AccountingJournalType.TRADE_JOURNAL.equals(journal.getJournalType().getName()), "An unexpected opening transaction journal type was found when attempting to retrieve position trade data: [%s].", journal.getJournalType().getName());

		// Support pseudo-positions pointing directly at trades instead of trade fills
		final Trade trade;
		switch (journal.getSystemTable().getName()) {
			case "TradeFill":
				TradeFill tradeFill = getTradeFill(journal.getFkFieldId());
				trade = tradeFill.getTrade();
				break;
			case "Trade":
				trade = getTrade(journal.getFkFieldId());
				break;
			default:
				throw new RuntimeException(String.format("Unable to derive journal source from specified table. Unsupported table for journal sources: [%s].", journal.getSystemTable().getName()));
		}
		return trade;
	}


	@Override
	public List<Trade> getTradeList(TradeSearchForm searchForm) {
		// TODO TradeGroupService/TradeService circular dependency
		// Same concept for Trade Group Types (Exclude adds special criteria handling, so the exclude id instead of name is done in the impl method)
		if (!StringUtils.isEmpty(searchForm.getIncludeTradeGroupTypeName())) {
			searchForm.setTradeGroupTypeId(getTradeGroupService().getTradeGroupTypeByName(searchForm.getIncludeTradeGroupTypeName()).getId());
			searchForm.setIncludeTradeGroupTypeName(null);
		}
		if (!StringUtils.isEmpty(searchForm.getTradeSourceNameEquals())) {
			Optional.ofNullable(getTradeSourceByName(searchForm.getTradeSourceNameEquals()))
					.map(BaseSimpleEntity::getId)
					.ifPresent(searchForm::setTradeSourceId);
			searchForm.setTradeSourceNameEquals(null);
		}

		return getTradeDAO().findBySearchCriteria(
				new TradeSearchFormConfigurer(searchForm, getInvestmentInstrumentService(), getTradeGroupService(), getWorkflowDefinitionService(), getInvestmentAccountRelationshipService(), getInvestmentAccountService()));
	}


	@Override
	public List<Trade> getTradePendingList(Integer clientInvestmentAccountId, Integer holdingInvestmentAccountId, Integer securityId) {
		TradeSearchForm searchForm = new TradeSearchForm();
		if (clientInvestmentAccountId != null) {
			searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		}
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		if (holdingInvestmentAccountId != null) {
			searchForm.setHoldingInvestmentAccountId(holdingInvestmentAccountId);
		}
		if (securityId != null) {
			searchForm.setSecurityId(securityId);
		}
		searchForm.setExcludeWorkflowStatusName(TRADES_CLOSED_STATUS_NAME);
		return getTradeList(searchForm);
	}


	@Override
	@Transactional
	public Trade saveTrade(Trade trade) {
		resolveTradeOpenCloseType(trade);

		// populate the fills if they are not all ready populated
		// this is need for the accountingNotional calculation
		if (!trade.isNewBean() && CollectionUtils.isEmpty(trade.getTradeFillList())) {
			trade.setTradeFillList(getTradeFillListByTrade(trade.getId()));
		}

		populateTradeQuantityProperties(trade);

		// set default values and perform basic validation: additional validation is done by {@link TradeObserver}
		InvestmentInstrumentHierarchy hierarchy = trade.getInvestmentSecurity().getInstrument().getHierarchy();
		if (trade.getCurrentFactor() == null) {
			trade.setCurrentFactor(BigDecimal.ONE);
		}
		else if (hierarchy.getFactorChangeEventType() == null) {
			ValidationUtils.assertTrue(MathUtils.isEqual(trade.getCurrentFactor(), BigDecimal.ONE), "Current Factor must be 1 for securities without Factor Change Event.", "currentFactor");
		}
		if (trade.getNotionalMultiplier() == null) {
			trade.setNotionalMultiplier(BigDecimal.ONE);
		}
		else if (hierarchy.getNotionalMultiplierRetriever() == null) {
			ValidationUtils.assertTrue(MathUtils.isEqual(trade.getNotionalMultiplier(), BigDecimal.ONE), "Notional Multiplier must be 1 for non Index Ration Adjusted securities.", "notionalMultiplier");
		}

		if ((trade.getTradeDestination() == null) || !trade.getTradeDestination().isOptionalQuantityOrNotionalEntry()) {
			trade.setAccountingNotional(calculateAccountingNotional(trade));
		}

		if (trade.getAccrualAmount1() != null) {
			ValidationUtils.assertNotNull(hierarchy.getAccrualSecurityEventType(), "Accrual Amount 1 is not allowed for trade's security.", "accrualAmount1");
		}
		else if (hierarchy.getAccrualSecurityEventType() != null) {
			trade.setAccrualAmount1(getTradePositionService().getTradeAccrualAmountAdvanced(trade, true, false));
		}
		if (trade.getAccrualAmount2() != null) {
			ValidationUtils.assertNotNull(hierarchy.getAccrualSecurityEventType2(), "Accrual Amount 2 is not allowed for trade's security.", "accrualAmount2");
		}
		else if (hierarchy.getAccrualSecurityEventType2() != null) {
			trade.setAccrualAmount2(getTradePositionService().getTradeAccrualAmountAdvanced(trade, false, true));
		}

		if (trade.isNovation()) {
			ValidationUtils.assertTrue(hierarchy.isOtc(), "Only OTC trades can be marked as novation.");
		}

		if (trade.getAssignmentCompany() != null) {
			ValidationUtils.assertTrue(trade.isNovation(), "Trade must be marked as novation if a counterparty is assigned.");
		}

		validateTradeExecutionType(trade);
		validateActualSettlementDate(trade);

		return getTradeDAO().save(trade);
	}


	@Override
	@Transactional
	public Trade saveTradeSettlementCompany(int id, int settlementCompanyId) {
		final Trade trade = getTradeDAO().findByPrimaryKey(id);
		ValidationUtils.assertNotNull(trade, "Trade could not be found [" + id + "].");
		BusinessCompany settlementCompany = getBusinessCompanyDAO().findByPrimaryKey(settlementCompanyId);
		ValidationUtils.assertNotNull(settlementCompany, "Settlement Company could not be found [" + settlementCompanyId + "].");
		trade.setSettlementCompany(settlementCompany);
		return DaoUtils.executeWithAllObserversDisabled(() -> getTradeDAO().save(trade));
	}


	@Override
	@Transactional
	public Trade[] splitTrade(Trade tradeToSplit, BigDecimal quantityToSplit) {
		ValidationUtils.assertFalse(tradeToSplit.isNewBean(), "Cannot split an unsaved Trade.");
		ValidationUtils.assertTrue(MathUtils.isGreaterThan(tradeToSplit.getQuantityIntended(), quantityToSplit), "Quantity to split must be less than the Trade to split's Quantity Intended.");
		ValidationUtils.assertTrue(CollectionUtils.isEmpty(getTradeFillListByTrade(tradeToSplit.getId())), "Cannot split a Trade with Fills.");

		// clear values that can be defaulted during save
		tradeToSplit.setCurrentFactor(null);
		tradeToSplit.setAccountingNotional(null);
		tradeToSplit.setAccrualAmount1(null);
		tradeToSplit.setAccrualAmount2(null);

		// Cloning the Trade produces a new Trade without ID set for saving.
		Trade newTrade = BeanUtils.cloneBean(tradeToSplit, false, false);
		newTrade.setQuantityIntended(quantityToSplit);
		newTrade.setDescription("Split from Trade: " + tradeToSplit + ".");
		// Commission and Fee amounts will be defaulted on save and updated when they are copied.
		newTrade.setCommissionPerUnit(null);
		newTrade.setCommissionAmount(null);
		newTrade.setFeeAmount(null);
		// Update the existing trade's quantity after the new trade values are set
		tradeToSplit.setQuantityIntended(MathUtils.subtract(tradeToSplit.getQuantityIntended(), quantityToSplit));

		/*
		 * Execute Trade saves without observers so workflow transitions do not execute.
		 * The workflow history and commission overrides will be copied.
		 */
		Trade[] savedTrades = DaoUtils.executeWithAllObserversDisabled(() -> new Trade[]{saveTrade(tradeToSplit), saveTrade(newTrade)});
		// Copy the original Trade's Workflow History to the new one.
		DaoUtils.executeWithPostUpdateFlushEnabled(() -> getWorkflowHistoryService().copyWorkflowHistoryList(savedTrades[0], savedTrades[1]));
		/*
		 * Copy Commission Overrides - Observers disabled in case TradeCommissionOverride attempts to save the Trade
		 * because commission or fee amounts were modified.
		 */
		DaoUtils.executeWithAllObserversDisabled(() -> copyTradeCommissionOverrideListForSplitTrades(savedTrades[0], savedTrades[1]));
		return savedTrades;
	}


	@Override
	public boolean isTradeFullyBooked(int tradeId) {
		List<TradeFill> fills = getTradeFillListByTrade(tradeId);
		if (CollectionUtils.isEmpty(fills)) {
			return false;
		}

		for (TradeFill fill : CollectionUtils.getIterable(fills)) {
			if (fill.getBookingDate() == null) {
				return false;
			}
		}

		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////          Trade Fill Methods          //////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeFill getTradeFill(int id) {
		return getTradeFillDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeFill getTradeFillForAccountingPosition(AccountingPosition accountingPosition) {
		AccountingJournal journal = accountingPosition.getOpeningTransaction().getJournal();
		AssertUtils.assertTrue(AccountingJournalType.TRADE_JOURNAL.equals(journal.getJournalType().getName()), "An unexpected opening transaction journal type was found  when attempting to retrieve position trade data: [%s].", journal.getJournalType().getName());
		return getTradeFill(journal.getFkFieldId());
	}


	@Override
	public List<TradeFill> getTradeFillListByTrade(int tradeId) {
		return getTradeFillDAO().findByField("trade.id", tradeId);
	}


	@Override
	public List<TradeFill> getTradeFillList(final TradeFillSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getClientInvestmentAccountGroupId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "group");
					sub.createAlias("referenceTwo", "acct");
					sub.add(Restrictions.eqProperty("acct.id", super.getPathAlias("trade", criteria) + ".clientInvestmentAccount.id"));
					sub.add(Restrictions.eq("group.id", searchForm.getClientInvestmentAccountGroupId()));
					criteria.add(Subqueries.exists(sub));
				}

				if (searchForm.getHoldingInvestmentAccountGroupId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "group");
					sub.createAlias("referenceTwo", "acct");
					sub.add(Restrictions.eqProperty("acct.id", super.getPathAlias("trade", criteria) + ".holdingInvestmentAccount.id"));
					sub.add(Restrictions.eq("group.id", searchForm.getHoldingInvestmentAccountGroupId()));
					criteria.add(Subqueries.exists(sub));
				}
			}
		};

		return getTradeFillDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	@Transactional
	public TradeFill saveTradeFill(TradeFill fill) {
		List<Trade> tradeList = new ArrayList<>();
		TradeFill result = doSaveTradeFill(fill, tradeList);
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			saveTrade(trade);
		}
		return result;
	}


	@Override
	@Transactional
	public void saveTradeFillList(List<TradeFill> tradeFillList) {
		List<Trade> tradeList = new ArrayList<>();
		for (TradeFill fill : CollectionUtils.getIterable(tradeFillList)) {
			doSaveTradeFill(fill, tradeList);
		}
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			saveTrade(trade);
		}
	}


	@Override
	public void deleteTradeFill(int id) {
		TradeFill fill = getTradeFill(id);
		ValidationUtils.assertNotNull(fill, "Cannot find trade fill to delete with id = " + id);
		ValidationUtils.assertFalse(TRADES_CLOSED_STATUS_NAME.equals(fill.getTrade().getWorkflowStatus().getName()), "Cannot delete a fill for a trade in 'Closed' workflow status.");

		getTradeFillDAO().delete(fill);
	}


	@Override
	public void deleteTradeFillList(List<TradeFill> tradeFillList) {
		for (TradeFill fill : CollectionUtils.getIterable(tradeFillList)) {
			deleteTradeFill(fill.getId());
		}
	}


	/**
	 * Add the specified fill to the trade and updates trade's average price.
	 * Before adding the fill performs a lot of validation:
	 * - amounts calculated correctly, the trade is not over-filled, etc.
	 */
	@Transactional
	protected TradeFill doSaveTradeFill(TradeFill fill, List<Trade> tradesToBeUpdatedList) {
		// if the trade is already listed to be updated, use that object
		Trade trade = tradesToBeUpdatedList != null && tradesToBeUpdatedList.contains(fill.getTrade()) ? tradesToBeUpdatedList.get(tradesToBeUpdatedList.indexOf(fill.getTrade()))
				: getTrade(fill.getTrade().getId());

		getTradeFillUtilHandler().validateTradeFill(trade, fill);

		if (fill.getQuantity() != null) {
			// use the fill list from the trade to be sure that the list contains any unsaved fills
			List<TradeFill> existingFills = trade.getTradeFillList();

			// make sure we're not over-filling
			BigDecimal existingFillListQuantity = CollectionUtils.getStream(existingFills)
					.filter(existingFill -> !fill.equals(existingFill))
					.map(TradeFill::getQuantity)
					.reduce(BigDecimal.ZERO, MathUtils::add);
			BigDecimal filledQty = MathUtils.add(fill.getQuantity(), existingFillListQuantity);
			ValidationUtils.assertFalse(MathUtils.isGreaterThan(filledQty, trade.getQuantityIntended()), "Intended trade quantity of " + trade.getQuantityIntended() + " cannot be over-filled: " + filledQty);

			// update trade average price, if necessary
			BigDecimal avgPrice = fill.getNotionalUnitPrice();
			if (!CollectionUtils.isEmpty(existingFills)) {
				BigDecimal fillTotalPrice = fill.getQuantity().multiply(fill.getNotionalUnitPrice());
				BigDecimal existingFillListTotalPrice = CollectionUtils.getStream(existingFills)
						.filter(existingFill -> !fill.equals(existingFill))
						.map(existingFill -> existingFill.getQuantity().multiply(existingFill.getNotionalUnitPrice()))
						.reduce(BigDecimal.ZERO, MathUtils::add);

				avgPrice = MathUtils.divide(MathUtils.add(existingFillListTotalPrice, fillTotalPrice), filledQty, DataTypes.PRICE.getPrecision());
			}
			if (MathUtils.isNotEqual(trade.getAverageUnitPrice(), avgPrice)) {
				trade.setAverageUnitPrice(avgPrice);
				if (tradesToBeUpdatedList == null) {
					saveTrade(trade);
				}
				else {
					// add the trade to update list if it's not already there
					if (!tradesToBeUpdatedList.contains(trade)) {
						tradesToBeUpdatedList.add(trade);
					}
				}
			}
			// add the fill to trade for the calculations on the next fill
			if (tradesToBeUpdatedList != null) {
				if (!fill.isNewBean()) {
					// remove the current fill if it is not new so the fill list has the updated version without duplicate
					trade.getTradeFillList().remove(fill);
				}
				trade.addFill(fill);
			}
		}

		if (fill.getOpenCloseType() == null) {
			fill.setOpenCloseType(trade.getOpenCloseType());
		}

		return getTradeFillDAO().save(fill);
	}


	@Override
	@ResponseBody
	public BigDecimal getTradeAmountForTradeSettlementCurrency(Trade trade, BigDecimal value) {
		if (value == null || trade == null) {
			return value;
		}

		InvestmentSecurity security = trade.getInvestmentSecurity();
		// support currency exchange rates: Security.denominationCCY to Trade.settlementCurrency
		BigDecimal fxRate = BigDecimal.ONE;
		InvestmentSecurity securityCurrencyDenomination = InvestmentUtils.getSecurityCurrencyDenomination(security);
		if (securityCurrencyDenomination != null && trade.getSettlementCurrency() != null && !securityCurrencyDenomination.equals(trade.getSettlementCurrency())) {
			ClientAccount clientAccount = trade.getClientInvestmentAccount() != null ? trade.getClientInvestmentAccount().toClientAccount() : null;
			HoldingAccount holdingAccount = trade.getHoldingInvestmentAccount() != null ? trade.getHoldingInvestmentAccount().toHoldingAccount() : null;
			FxRateLookupCommand command = FxRateLookupCommand.forClientAccount(clientAccount, securityCurrencyDenomination.getSymbol(), trade.getSettlementCurrency().getSymbol(), trade.getSettlementDate()).withHoldingAccount(holdingAccount).flexibleLookup(true);
			BigDecimal lookupFxRate = getMarketDataExchangeRatesApiService().getExchangeRate(command);
			if (lookupFxRate != null) {
				fxRate = lookupFxRate;
			}
		}

		if (MathUtils.isEqual(fxRate, 1)) {
			return value;
		}

		return InvestmentCalculatorUtils.calculateBaseAmount(value, fxRate, trade.getSettlementCurrency());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////                 Helper Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////


	private void resolveTradeOpenCloseType(Trade trade) {
		if (trade.getOpenCloseType() == null) {
			TradeOpenCloseTypes openCloseType = TradeOpenCloseTypes.of(trade.isBuy(), true, true);
			trade.setOpenCloseType(getTradeOpenCloseTypeByName(openCloseType.getName()));
		}
		else {
			trade.setBuy(trade.getOpenCloseType().isBuy());
		}
	}


	private BigDecimal calculateAccountingNotional(Trade trade) {
		// do not change if quantity is not set: some trades (currency exchange) do not use quantity and set the notional directly
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.CURRENCY)) {
			return trade.getAccountingNotional();
		}
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			// for currency forwards, it's valid to specify notional instead of quantityIntended: quantity will be calculated once the price (forward rate) is known)
			return updateForwardAccountingNotionalAndQuantityIntended(trade);
		}

		// if the trade is fully filled, sum the notional of each fill to calculate the notional
		if (!CollectionUtils.isEmpty(trade.getTradeFillList())) {
			List<TradeFill> existingFills = trade.getTradeFillList();
			BigDecimal filledQty = CoreMathUtils.sumProperty(existingFills, TradeFill::getQuantity);
			if (MathUtils.isEqual(trade.getQuantityIntended(), filledQty)) {
				BigDecimal accountingNotional = CoreMathUtils.sumProperty(existingFills, TradeFill::getNotionalTotalPrice);
				if (accountingNotional != null && isTradeAccountingNotionalNegated(trade)) {
					accountingNotional = accountingNotional.negate();
				}
				return accountingNotional;
			}
		}

		return calculateQuantityOrNotional(trade, trade.getQuantityIntended(), true);
	}


	private BigDecimal updateForwardAccountingNotionalAndQuantityIntended(Trade trade) {
		boolean useAccountNotional = false;
		BigDecimal quantity;
		if (!trade.getSettlementCurrency().equals(trade.getInvestmentSecurity().getInstrument().getTradingCurrency())) {
			// Round the accounting notional according to the denominating currency of the forward, so the quantity can be correctly calculated.
			BigDecimal correctlyRoundedValue = InvestmentCalculatorUtils.getNotionalCalculator(trade.getInvestmentSecurity().getInstrument().getTradingCurrency())
					.round(trade.getAccountingNotional());
			trade.setAccountingNotional(correctlyRoundedValue);
			quantity = correctlyRoundedValue;
			useAccountNotional = true;
		}
		else {
			// Round the notional according to the numerating currency of the forward, so accounting notional can be correctly calculated.
			BigDecimal correctlyRoundedValue = InvestmentCalculatorUtils.getNotionalCalculator(trade.getInvestmentSecurity().getUnderlyingSecurity())
					.round(trade.getQuantityIntended());
			trade.setQuantityIntended(correctlyRoundedValue);
			trade.setOriginalFace(correctlyRoundedValue);
			quantity = correctlyRoundedValue;
		}

		BigDecimal notional = calculateQuantityOrNotional(trade, quantity, !useAccountNotional);

		if (useAccountNotional) {
			if (notional != null) {
				notional = InvestmentCalculatorUtils.getNotionalCalculator(trade.getSettlementCurrency()).round(notional);
			}
			trade.setQuantityIntended(notional);
			trade.setOriginalFace(notional);
			return trade.getAccountingNotional();
		}
		return notional;
	}


	private BigDecimal calculateQuantityOrNotional(Trade trade, BigDecimal value, boolean valueIsQuantity) {
		BigDecimal result = null;

		populateExpectedUnitPrice(trade);
		BigDecimal price = trade.getAverageOrExpectedUnitPrice();
		if (price != null) {
			InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(trade.getInvestmentSecurity().getInstrument().getTradingCurrency());
			result = valueIsQuantity ? getInvestmentCalculator().calculateNotional(trade.getInvestmentSecurity(), price, value, trade.getNotionalMultiplier(), roundingCalculator)
					: getInvestmentCalculator().calculateQuantityFromNotional(trade.getInvestmentSecurity(), price, value, trade.getNotionalMultiplier(), roundingCalculator);
		}

		if (result != null) {
			if (isTradeAccountingNotionalNegated(trade)) {
				result = result.negate();
			}

			// allow manual correction for penny rounding
			if (trade.getAccountingNotional() != null) {
				if (MathUtils.isLessThanOrEqual(result.subtract(trade.getAccountingNotional()).abs(), BigDecimal.valueOf(0.01))) {
					result = trade.getAccountingNotional();
				}
			}
		}

		return result;
	}


	/**
	 * The accounting notional on some trade types (CDS, IRS) have the opposite sign of the calculated value.  This method
	 * will return true if the accounting notion needs to be negated.
	 */
	private boolean isTradeAccountingNotionalNegated(Trade trade) {
		if (trade.getTradeType().isAmountsSignAccountsForBuy() && trade.isBuy()) {
			if (!InvestmentCalculatorUtils.getNotionalCalculator(trade.getInvestmentSecurity()).isNegativeQuantity()) {
				return true;
			}
		}
		return false;
	}


	/**
	 * On initial Trade entry and there is no expected price: look one up and set it on the trade.
	 */
	private void populateExpectedUnitPrice(Trade trade) {
		if (trade.getExpectedUnitPrice() == null && trade.isNewBean() && !trade.getInvestmentSecurity().isCurrency()) {
			BigDecimal expectedUnitPrice = getMarketDataService().getMarketDataPriceFlexibleForSecurity(trade.getInvestmentSecurity().getId(), trade.getTradeDate(), false);
			trade.setExpectedUnitPrice(expectedUnitPrice);
		}
	}


	/**
	 * Populates the trade with quantityIntended and originalFace as necessary.
	 */
	private void populateTradeQuantityProperties(Trade trade) {
		applyFactorAdjustment(trade);

		if (trade.getQuantityIntended() == null) {
			populateCalculatedQuantityIntended(trade);
		}

		if (trade.getQuantityIntended() != null) {
			// ensure quantity does not have greater precision than allowed.
			Short decimalPrecision = InvestmentUtils.getQuantityDecimalPrecision(trade.getInvestmentSecurity());
			if (trade.getQuantityIntended().stripTrailingZeros().scale() > decimalPrecision.intValue()) {
				InvestmentSecurity securityForRounding = InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)
						? trade.getInvestmentSecurity().getUnderlyingSecurity()
						: trade.getInvestmentSecurity();
				InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(securityForRounding);
				trade.setQuantityIntended(MathUtils.round(trade.getQuantityIntended(), decimalPrecision.intValue(), roundingCalculator.getRoundingMode()));
			}
		}

		if (trade.getOriginalFace() == null || trade.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() == null) {
			trade.setOriginalFace(trade.getQuantityIntended());
		}
	}


	/**
	 * Updates the trade based on factor adjustment and the {@link InvestmentInstrumentHierarchy} of the {@link Trade#investmentSecurity}
	 */
	private void applyFactorAdjustment(Trade trade) {
		InvestmentSecurity security = trade.getInvestmentSecurity();
		InvestmentInstrumentHierarchy hierarchy = trade.getInvestmentSecurity().getInstrument().getHierarchy();

		if (hierarchy.getFactorChangeEventType() != null) {
			if (trade.getCurrentFactor() == null) {
				InvestmentSecurityEvent factorEvent = getInvestmentSecurityEventService().getInvestmentSecurityEventWithLatestExDate(security.getId(), hierarchy.getFactorChangeEventType().getName(), trade.getTradeDate());
				if (factorEvent != null && factorEvent.getAfterEventValue() != null) {
					trade.setCurrentFactor(factorEvent.getAfterEventValue());
				}
				else if (trade.getQuantityIntended() != null && trade.getOriginalFace() != null) {
					trade.setCurrentFactor(MathUtils.divide(trade.getOriginalFace(), trade.getQuantityIntended()));
				}
				else {
					trade.setCurrentFactor(BigDecimal.ONE);
				}
			}

			if (trade.getOriginalFace() != null && trade.getQuantityIntended() == null) {
				trade.setQuantityIntended(MathUtils.multiply(trade.getOriginalFace(), trade.getCurrentFactor()));
			}
			else if (trade.getQuantityIntended() != null && trade.getOriginalFace() == null) {
				trade.setOriginalFace(MathUtils.divide(trade.getQuantityIntended(), trade.getCurrentFactor()));
			}
		}

		if (trade.getCurrentFactor() == null) {
			trade.setCurrentFactor(BigDecimal.ONE);
		}
	}


	/**
	 * Calculates and sets the QuantityIntended value for the Trade. For Currency Forward Trades, the quantity is
	 * calculated and correctly rounded based on the underlying rounding calculator. For other Trade types, the
	 * quantity is set to the value of OriginalFace.
	 */
	private void populateCalculatedQuantityIntended(Trade trade) {
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			/*
			 * Correctly calculate and round QuantityIntended if we have a price, otherwise default to the the behavior of other Trade types.
			 * Price will be defined if specified during trade creation and after execution has occurred.
			 */
			BigDecimal price = trade.getAverageOrExpectedUnitPrice();
			if (price != null) {
				BigDecimal calculatedQuantity = getInvestmentCalculator().calculateQuantityFromNotional(trade.getInvestmentSecurity(), price, trade.getAccountingNotional(), trade.getNotionalMultiplier());
				InvestmentNotionalCalculatorTypes roundingCalculator = trade.getInvestmentSecurity().getInstrument().getUnderlyingInstrument().getCostCalculator();
				trade.setQuantityIntended(roundingCalculator.round(calculatedQuantity));
				trade.setOriginalFace(trade.getQuantityIntended());
				return;
			}
		}
		trade.setQuantityIntended(trade.getOriginalFace());
	}


	/**
	 * Copies the TradeCommissionOverrides available on the source Trade to the destination Trade. If an override is defined
	 * with a total amount, it will be proportionally distributed across the two trades. The Trades will be saved if they are
	 * modified based on the TradeCommissionOverride saves.
	 */
	private void copyTradeCommissionOverrideListForSplitTrades(Trade source, Trade destination) {
		List<TradeCommissionOverride> commissionOverrideList = getTradeCommissionService().getTradeCommissionOverrideListForTrade(source.getId());
		List<TradeCommissionOverride> sourceModifiedCommissionOverrides = new ArrayList<>();
		List<TradeCommissionOverride> destinationCommissionOverrides = new ArrayList<>();
		BigDecimal sourceProportion = null;
		for (TradeCommissionOverride override : CollectionUtils.getIterable(commissionOverrideList)) {
			TradeCommissionOverride clone = BeanUtils.cloneBean(override, false, false);
			clone.setTrade(destination);
			clone.setOverrideNote("Copied from TradeCommissionOverride: {id=" + override.getId());
			if (override.getTotalAmount() != null) {
				/*
				 * Need to proportionally split the total across the Trades for total amount.
				 * sourceAmount = overrideTotal * (sourceQuantity/(sourceQuantity + destinationQuantity))
				 */
				if (sourceProportion == null) {
					sourceProportion = MathUtils.divide(source.getQuantityIntended(), MathUtils.add(source.getQuantityIntended(), destination.getQuantityIntended()));
				}
				BigDecimal sourceTotalAmount = MathUtils.round(MathUtils.multiply(override.getTotalAmount(), sourceProportion), 2, BigDecimal.ROUND_HALF_UP).stripTrailingZeros();
				clone.setTotalAmount(MathUtils.subtract(override.getTotalAmount(), sourceTotalAmount));
				override.setTotalAmount(sourceTotalAmount);
				sourceModifiedCommissionOverrides.add(override);
			}
			destinationCommissionOverrides.add(clone);
		}

		getTradeCommissionService().saveTradeCommissionOverrideListForTrade(source, sourceModifiedCommissionOverrides);
		getTradeCommissionService().saveTradeCommissionOverrideListForTrade(destination, destinationCommissionOverrides);
	}


	/**
	 * Verifies that the trade is in an allowable configuration with respect to the TradeExecutionType and LimitPrice.
	 */
	private void validateTradeExecutionType(Trade trade) {

		if (trade == null) {
			return;
		}

		if (!trade.getTradeType().isTradeExecutionTypeSupported() && trade.getTradeExecutionType() != null) {
			throw new ValidationException("Trade '" + trade.getLabel() + "' of type '" + trade.getTradeType().getName() +
					"' does not support trade execution type: '" + trade.getTradeExecutionType().getName() + "'.");
		}

		if (!trade.getTradeType().isTradeExecutionTypeSupported() && trade.getLimitPrice() != null) {
			throw new ValidationException("Trade '" + trade.getLabel() + "' of type '" + trade.getTradeType().getName() +
					"' does not support limit prices: '" + trade.getTradeExecutionType().getName() + "'.");
		}
	}


	private void validateActualSettlementDate(Trade trade) {
		if (trade.getSettlementDate() == null && trade.getActualSettlementDate() != null) {
			throw new ValidationException("Actual Settlement Date requires a Settlement Date.");
		}
		if (trade.getSettlementDate() != null && trade.getActualSettlementDate() != null && DateUtils.isDateBefore(trade.getActualSettlementDate(), trade.getSettlementDate(), false)) {
			throw new ValidationException(String.format("Actual Settlement Date [%1$tm-%1$te-%1$tY] cannot be before the Settlement Date [%2$tm-%2$te-%2$tY].", trade.getActualSettlementDate(), trade.getSettlementDate()));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeType, Criteria> getTradeTypeDAO() {
		return this.tradeTypeDAO;
	}


	public void setTradeTypeDAO(AdvancedUpdatableDAO<TradeType, Criteria> tradeTypeDAO) {
		this.tradeTypeDAO = tradeTypeDAO;
	}


	public AdvancedUpdatableDAO<TradeOpenCloseType, Criteria> getTradeOpenCloseTypeDAO() {
		return this.tradeOpenCloseTypeDAO;
	}


	public void setTradeOpenCloseTypeDAO(AdvancedUpdatableDAO<TradeOpenCloseType, Criteria> tradeOpenCloseTypeDAO) {
		this.tradeOpenCloseTypeDAO = tradeOpenCloseTypeDAO;
	}


	public AdvancedUpdatableDAO<TradeSource, Criteria> getTradeSourceDAO() {
		return this.tradeSourceDAO;
	}


	public void setTradeSourceDAO(AdvancedUpdatableDAO<TradeSource, Criteria> tradeSourceDAO) {
		this.tradeSourceDAO = tradeSourceDAO;
	}


	public AdvancedUpdatableDAO<Trade, Criteria> getTradeDAO() {
		return this.tradeDAO;
	}


	public void setTradeDAO(AdvancedUpdatableDAO<Trade, Criteria> tradeDAO) {
		this.tradeDAO = tradeDAO;
	}


	public AdvancedUpdatableDAO<TradeFill, Criteria> getTradeFillDAO() {
		return this.tradeFillDAO;
	}


	public void setTradeFillDAO(AdvancedUpdatableDAO<TradeFill, Criteria> tradeFillDAO) {
		this.tradeFillDAO = tradeFillDAO;
	}


	public AdvancedUpdatableDAO<BusinessCompany, Criteria> getBusinessCompanyDAO() {
		return this.businessCompanyDAO;
	}


	public void setBusinessCompanyDAO(AdvancedUpdatableDAO<BusinessCompany, Criteria> businessCompanyDAO) {
		this.businessCompanyDAO = businessCompanyDAO;
	}


	public AdvancedUpdatableDAO<TradeTimeInForceType, Criteria> getTradeTimeInForceTypeDAO() {
		return this.tradeTimeInForceTypeDAO;
	}


	public void setTradeTimeInForceTypeDAO(AdvancedUpdatableDAO<TradeTimeInForceType, Criteria> tradeTimeInForceTypeDAO) {
		this.tradeTimeInForceTypeDAO = tradeTimeInForceTypeDAO;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}


	public TradeFillUtilHandler getTradeFillUtilHandler() {
		return this.tradeFillUtilHandler;
	}


	public void setTradeFillUtilHandler(TradeFillUtilHandler tradeFillUtilHandler) {
		this.tradeFillUtilHandler = tradeFillUtilHandler;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public TradePositionService getTradePositionService() {
		return this.tradePositionService;
	}


	public void setTradePositionService(TradePositionService tradePositionService) {
		this.tradePositionService = tradePositionService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public DaoNamedEntityCache<TradeType> getTradeTypeCache() {
		return this.tradeTypeCache;
	}


	public void setTradeTypeCache(DaoNamedEntityCache<TradeType> tradeTypeCache) {
		this.tradeTypeCache = tradeTypeCache;
	}


	public DaoNamedEntityCache<TradeOpenCloseType> getTradeOpenCloseTypeCache() {
		return this.tradeOpenCloseTypeCache;
	}


	public void setTradeOpenCloseTypeCache(DaoNamedEntityCache<TradeOpenCloseType> tradeOpenCloseTypeCache) {
		this.tradeOpenCloseTypeCache = tradeOpenCloseTypeCache;
	}


	public DaoNamedEntityCache<TradeSource> getTradeSourceCache() {
		return this.tradeSourceCache;
	}


	public void setTradeSourceCache(DaoNamedEntityCache<TradeSource> tradeSourceCache) {
		this.tradeSourceCache = tradeSourceCache;
	}


	public DaoNamedEntityCache<TradeTimeInForceType> getTradeTimeInForceTypeCache() {
		return this.tradeTimeInForceTypeCache;
	}


	public void setTradeTimeInForceTypeCache(DaoNamedEntityCache<TradeTimeInForceType> tradeTimeInForceTypeCache) {
		this.tradeTimeInForceTypeCache = tradeTimeInForceTypeCache;
	}


	public DaoSingleKeyListCache<TradeType, Short> getTradeTypeListByInvestmentTypeCache() {
		return this.tradeTypeListByInvestmentTypeCache;
	}


	public void setTradeTypeListByInvestmentTypeCache(DaoSingleKeyListCache<TradeType, Short> tradeTypeListByInvestmentTypeCache) {
		this.tradeTypeListByInvestmentTypeCache = tradeTypeListByInvestmentTypeCache;
	}
}
