package com.clifton.trade;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeTypeListByInvestmentTypeCache</code> stores a list of trade types for an {@link InvestmentType} id
 * <p/>
 * Most Trade types are set without an {@link InvestmentGroup} - only swaps use groups, so
 * most of the time, finding the trade type for a security will result in 0 db look ups
 *
 * @author manderson
 */
@Component
public class TradeTypeListByInvestmentTypeCache extends SelfRegisteringSingleKeyDaoListCache<TradeType, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "investmentType.id";
	}


	@Override
	protected Short getBeanKeyValue(TradeType bean) {
		if (bean.getInvestmentType() != null) {
			return bean.getInvestmentType().getId();
		}
		return null;
	}
}
