package com.clifton.trade;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.security.user.SecurityUser;

import java.math.BigDecimal;
import java.util.List;


/**
 * A command used with the {@link com.clifton.trade.TradeService} executeTradeActionByCommand method
 * to perform trade actions on a list of trades.  An example of a trade action type is "FILL", which fills
 * the list of trades with a specified fill price.
 * <p>
 * See {@link com.clifton.trade.TradeListActionTypes}
 *
 * @author davidi
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class TradeListActionCommand extends BeanListCommand<Trade> {

	private TradeListActionTypes actionType;
	private BigDecimal fillPrice;
	private SecurityUser newAssignee;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeListActionCommand forTradeList(List<Trade> tradeList) {
		TradeListActionCommand command = new TradeListActionCommand();
		command.setBeanList(tradeList);
		return command;
	}


	public static TradeListActionCommand forReassignWithAssigneeAndTradeList(SecurityUser newAssignee, List<Trade> tradeList) {
		TradeListActionCommand command = new TradeListActionCommand();
		command.setNewAssignee(newAssignee);
		command.setBeanList(tradeList);
		command.setActionType(TradeListActionTypes.REASSIGN);
		return command;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeListActionTypes getActionType() {
		return this.actionType;
	}


	public void setActionType(TradeListActionTypes actionType) {
		this.actionType = actionType;
	}


	public BigDecimal getFillPrice() {
		return this.fillPrice;
	}


	public void setFillPrice(BigDecimal fillPrice) {
		this.fillPrice = fillPrice;
	}


	public SecurityUser getNewAssignee() {
		return this.newAssignee;
	}


	public void setNewAssignee(SecurityUser newAssignee) {
		this.newAssignee = newAssignee;
	}
}
