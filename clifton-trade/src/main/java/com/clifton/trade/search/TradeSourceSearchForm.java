package com.clifton.trade.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>TradeSourceSearchForm</code> class defines search criteria for TradeSource objects.
 *
 * @author NickK
 */
public class TradeSourceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean bypassDualApprovalAllowed;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getBypassDualApprovalAllowed() {
		return this.bypassDualApprovalAllowed;
	}


	public void setBypassDualApprovalAllowed(Boolean bypassDualApprovalAllowed) {
		this.bypassDualApprovalAllowed = bypassDualApprovalAllowed;
	}
}
