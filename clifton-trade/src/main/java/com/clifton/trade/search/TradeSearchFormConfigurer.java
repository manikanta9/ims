package com.clifton.trade.search;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.hibernate.type.StringType;

import java.util.List;


/**
 * The <code>TradeSearchFormConfigurer</code> class configures TradeSearchForm objects.
 * Too much code to be an inner class.
 *
 * @author vgomelsky
 */
public class TradeSearchFormConfigurer extends WorkflowAwareSearchFormConfigurer {

	private final InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private final InvestmentAccountService investmentAccountService;
	private final InvestmentInstrumentService investmentInstrumentService;
	private final TradeGroupService tradeGroupService;


	public TradeSearchFormConfigurer(TradeSearchForm searchForm, InvestmentInstrumentService investmentInstrumentService, TradeGroupService tradeGroupService,
	                                 WorkflowDefinitionService workflowDefinitionService, InvestmentAccountRelationshipService investmentAccountRelationshipService, InvestmentAccountService investmentAccountService) {
		super(searchForm, workflowDefinitionService);
		this.investmentInstrumentService = investmentInstrumentService;
		this.tradeGroupService = tradeGroupService;
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
		this.investmentAccountService = investmentAccountService;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		TradeSearchForm searchForm = (TradeSearchForm) getSortableSearchForm();

		// Search Pattern can be a comma delimited list of different search values
		// Trade ID, Client Account #/Name, Holding Account #/Name, Security Symbol/Name
		// i.e. Search for 227000,TUM3 to find trades for Client Account 227000 for Security TUM3
		if (!StringUtils.isEmpty(searchForm.getSearchPattern())) {
			String[] searchValues = searchForm.getSearchPattern().split(",");
			appendSearchPatternRestriction(criteria, searchValues);
		}

		if (!StringUtils.isEmpty(searchForm.getExcludeTradeGroupTypeName()) || searchForm.getExcludeTradeGroupTypeIds() != null) {
			Object[] excludeTradeGroupTypeIds = searchForm.getExcludeTradeGroupTypeIds();

			// TODO TradeGroupService/TradeService circular dependency
			if (!StringUtils.isEmpty(searchForm.getExcludeTradeGroupTypeName())) {
				TradeGroupType tgt = getTradeGroupService().getTradeGroupTypeByName(searchForm.getExcludeTradeGroupTypeName());
				if (excludeTradeGroupTypeIds == null) {
					excludeTradeGroupTypeIds = new Short[]{tgt.getId()};
				}
				else {
					excludeTradeGroupTypeIds = ArrayUtils.add(excludeTradeGroupTypeIds, tgt.getId());
				}
			}
			String pathAlias = getPathAlias("tradeGroup", criteria, JoinType.LEFT_OUTER_JOIN);
			criteria.add(Restrictions.or(Restrictions.isNull(pathAlias + ".id"), Restrictions.not(Restrictions.in(pathAlias + ".tradeGroupType.id", excludeTradeGroupTypeIds))));
		}

		if (searchForm.getClientInvestmentAccountIdOrSubAccount() != null) {
			List<InvestmentAccountRelationship> relList = this.investmentAccountRelationshipService.getInvestmentAccountRelationshipListForPurposeActive(
					searchForm.getClientInvestmentAccountIdOrSubAccount(), InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME, true);
			if (CollectionUtils.isEmpty(relList)) {
				criteria.add(Restrictions.eq("clientInvestmentAccount.id", searchForm.getClientInvestmentAccountIdOrSubAccount()));
			}
			else {
				Object[] accountIds = BeanUtils.getPropertyValues(relList, "referenceTwo.id");
				accountIds = ArrayUtils.add(accountIds, searchForm.getClientInvestmentAccountIdOrSubAccount());
				criteria.add(Restrictions.in("clientInvestmentAccount.id", accountIds));
			}
		}

		if (searchForm.getClientInvestmentAccountGroupId() != null || searchForm.getClientInvestmentAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceOne", "group");
			sub.createAlias("referenceTwo", "acct");
			sub.add(Restrictions.eqProperty("acct.id", getPathAlias("clientInvestmentAccount", criteria) + ".id"));
			if (searchForm.getClientInvestmentAccountGroupId() != null) {
				sub.add(Restrictions.eq("group.id", searchForm.getClientInvestmentAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("group.id", searchForm.getClientInvestmentAccountGroupIds()));
			}
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getHoldingInvestmentAccountGroupId() != null || searchForm.getHoldingInvestmentAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "hag");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			if (searchForm.getHoldingInvestmentAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getHoldingInvestmentAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getHoldingInvestmentAccountGroupIds()));
			}
			criteria.add(Subqueries.exists(sub));
		}

		if (searchForm.getExcludeHoldingInvestmentAccountGroupId() != null || searchForm.getExcludeHoldingInvestmentAccountGroupIds() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "aga");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			if (searchForm.getExcludeHoldingInvestmentAccountGroupId() != null) {
				sub.add(Restrictions.eq("referenceOne.id", searchForm.getExcludeHoldingInvestmentAccountGroupId()));
			}
			else {
				sub.add(Restrictions.in("referenceOne.id", searchForm.getExcludeHoldingInvestmentAccountGroupIds()));
			}
			criteria.add(Subqueries.notExists(sub));
		}

		if (!ArrayUtils.isEmpty(searchForm.getBrokerOrIssuingCompanyIds())) {
			String issuingCompanyPath = getPathAlias("holdingInvestmentAccount.issuingCompany", criteria);
			criteria.add(Restrictions.or(Restrictions.in("executingBrokerCompany.id", searchForm.getBrokerOrIssuingCompanyIds()), Restrictions.in(issuingCompanyPath + ".id", searchForm.getBrokerOrIssuingCompanyIds())));
		}

		if (searchForm.getAssumedFactorSecurityEventId() != null) {
			// use SQL directly because of limitations in Hibernate's Criteria (need expressions and more than 1 join to same table)
			String sqlRestriction = "{alias}.TradeID IN (SELECT t.TradeID FROM Trade t " + //
					"INNER JOIN WorkflowState ws ON t.WorkflowStateID = ws.WorkflowStateID AND ws.WorkflowStateName != 'Cancelled' " + //
					"INNER JOIN WorkflowStatus ws2 ON t.WorkflowStatusID = ws2.WorkflowStatusID AND ws2.WorkflowStatusName = 'Closed' " + //
					"INNER JOIN InvestmentSecurityEvent ce ON t.InvestmentSecurityID = ce.InvestmentSecurityID " + //
					"INNER JOIN InvestmentSecurityEventType ct ON ce.InvestmentSecurityEventTypeID = ct.InvestmentSecurityEventTypeID " + //
					"INNER JOIN InvestmentSecurityEvent fe ON  t.InvestmentSecurityID = fe.InvestmentSecurityID " + //
					"INNER JOIN InvestmentSecurityEventType ft ON fe.InvestmentSecurityEventTypeID = ft.InvestmentSecurityEventTypeID " + //
					"WHERE " + // find Coupon period for trade (sometimes available before factor)
					"ct.EventTypeName = 'Cash Coupon Payment' AND t.SettlementDate >= ce.EventDeclareDate AND t.SettlementDate < ce.EventExDate " + //
					"AND ft.EventTypeName = 'Factor Change' AND fe.InvestmentSecurityEventID = " + searchForm.getAssumedFactorSecurityEventId() + " " + // find Factor period for month matching Coupon accrual Start Date
					"AND (CONVERT(VARCHAR(3), ce.EventDeclareDate, 0) + CONVERT(VARCHAR(2), ce.EventDeclareDate, 12)) = fe.EventDescription " + // 3 letter month + 2 digit year
					"AND ABS(t.CurrentFactor - fe.AfterEventValue) > 0.0000001 " + // ignore rounding
					"AND NOT EXISTS ( " + // skip assumed factor change trades (legacy migration)
					"SELECT * FROM Trade t2 " + //
					"WHERE t2.TradeDate <> t.TradeDate AND t2.InvestmentSecurityID = t.InvestmentSecurityID " + //
					"AND t2.HoldingInvestmentAccountID = t.HoldingInvestmentAccountID AND t2.IsBuy <> t.IsBuy " + //
					"AND t2.OriginalFace = t.OriginalFace AND t2.AccountingNotional = t.AccountingNotional))";
			criteria.add(Restrictions.sqlRestriction(sqlRestriction));
		}

		if (searchForm.getIncludeBigTradesInvestmentInstrumentId() != null) {
			InvestmentInstrument ins = getInvestmentInstrumentService().getInvestmentInstrument(searchForm.getIncludeBigTradesInvestmentInstrumentId());
			if (ins.getBigInstrument() != null) {
				criteria.add(Restrictions.or(Restrictions.eq(getPathAlias("investmentSecurity.instrument", criteria) + ".id", ins.getId()),
						Restrictions.eq(getPathAlias("investmentSecurity.instrument", criteria) + ".id", ins.getBigInstrument().getId())));
			}
			else {
				criteria.add(Restrictions.eq(getPathAlias("investmentSecurity.instrument", criteria) + ".id", ins.getId()));
			}
		}

		if (searchForm.getCustodianCompanyId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountRelationship.class, "r");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceOne", "mainAccount");
			sub.createAlias("referenceTwo", "relatedAccount");
			sub.createAlias("purpose", "rp");
			sub.createAlias("relatedAccount.issuingCompany", "custodian");

			sub.add(Restrictions.or(Restrictions.eq("rp.name", InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME), Restrictions.eq("rp.name", InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME)));
			sub.add(Restrictions.eq("custodian.id", searchForm.getCustodianCompanyId()));
			sub.add(Restrictions.eqProperty("mainAccount.id", getPathAlias("holdingInvestmentAccount", criteria) + ".id"));
			criteria.add(Restrictions.or(
					Restrictions.and(
							Restrictions.eq(getPathAlias("holdingInvestmentAccount", criteria) + ".issuingCompany.id", searchForm.getCustodianCompanyId()),
							Restrictions.eq(getPathAlias("holdingInvestmentAccount", criteria) + ".type.id", getInvestmentAccountService().getInvestmentAccountTypeByName(InvestmentAccountType.CUSTODIAN).getId())
					),
					Subqueries.exists(sub)
			));
		}

		if (searchForm.getAssignmentOrExecutingBrokerCompanyId() != null) {
			criteria.add(
					Restrictions.or(
							Restrictions.and(Restrictions.isNull("assignmentCompany"), Restrictions.eq("executingBrokerCompany.id", searchForm.getAssignmentOrExecutingBrokerCompanyId())),
							Restrictions.and(Restrictions.isNotNull("assignmentCompany"), Restrictions.eq("assignmentCompany.id", searchForm.getAssignmentOrExecutingBrokerCompanyId()))
					)
			);
		}

		applyRollCriterion(searchForm, criteria);
		applyImportCriterion(searchForm, criteria);
	}


	private void appendSearchPatternRestriction(Criteria criteria, String[] searchValues) {
		String securityAlias = getPathAlias("investmentSecurity", criteria);
		String clientAccountAlias = getPathAlias("clientInvestmentAccount", criteria);
		String holdingAccountAlias = getPathAlias("clientInvestmentAccount", criteria);
		for (String v : searchValues) {
			if (!StringUtils.isEmpty(v)) {
				// in case users do [, ] instead of just [,] between search patterns
				v = v.trim();
				// if v starts with = just search on Trade ID
				if (v.startsWith("=")) {
					v = v.substring(1);
					if (!StringUtils.isEmpty(v)) {
						if (MathUtils.isNumber(v)) {
							criteria.add(Restrictions.sqlRestriction("{alias}.TradeID LIKE ?", v + "%", StringType.INSTANCE));
						}
						else {
							throw new ValidationException("When using = prefix in search pattern, will attempt to find a Trade with that ID - value entered " + v + " is not a valid numerical value.");
						}
					}
				}
				else {
					criteria.add(Restrictions.disjunction()
							.add(Restrictions.like(clientAccountAlias + ".number", v, MatchMode.ANYWHERE))
							.add(Restrictions.like(clientAccountAlias + ".name", v, MatchMode.ANYWHERE))
							.add(Restrictions.like(holdingAccountAlias + ".number", v, MatchMode.ANYWHERE))
							.add(Restrictions.like(holdingAccountAlias + ".name", v, MatchMode.ANYWHERE))
							.add(Restrictions.like(securityAlias + ".symbol", v, MatchMode.ANYWHERE))
							.add(Restrictions.like(securityAlias + ".name", v, MatchMode.ANYWHERE)));
				}
			}
		}
	}


	private void applyRollCriterion(TradeSearchForm tradeSearchForm, Criteria criteria) {
		if (tradeSearchForm.getRoll() != null) {
			addTradeGroupTypeExistsCriterion(criteria, "roll", tradeSearchForm.getRoll());
		}
	}


	private void applyImportCriterion(TradeSearchForm tradeSearchForm, Criteria criteria) {
		if (tradeSearchForm.getTradeImport() != null) {
			addTradeGroupTypeExistsCriterion(criteria, "tradeImport", tradeSearchForm.getTradeImport());
		}
	}


	private void addTradeGroupTypeExistsCriterion(Criteria criteria, String groupTypeProperty, Object groupTypePropertyValue) {
		DetachedCriteria groupTypeSubquery = DetachedCriteria.forClass(TradeGroupType.class, "gt").setProjection(Projections.id())
				.add(Restrictions.eq("gt." + groupTypeProperty, groupTypePropertyValue));

		DetachedCriteria groupSubquery = DetachedCriteria.forClass(TradeGroup.class, "g").setProjection(Projections.id())
				.add(Restrictions.eqProperty("g.id", criteria.getAlias() + ".tradeGroup.id"))
				.add(Subqueries.propertyIn("g.tradeGroupType.id", groupTypeSubquery));

		criteria.add(Subqueries.exists(groupSubquery));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}
}
