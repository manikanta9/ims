package com.clifton.trade.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The <code>TradeSearchForm</code> class defines search configuration for Trade objects.
 *
 * @author vgomelsky
 */
public class TradeSearchForm extends BaseWorkflowAwareSearchForm {

	// Custom Search Field
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] ids;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Integer excludeId;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Integer[] excludeIds;

	@SearchField(searchField = "tradeType.id", sortField = "tradeType.name")
	private Short tradeTypeId;

	@SearchField(searchField = "tradeType.id", sortField = "tradeType.name", comparisonConditions = ComparisonConditions.IN)
	private Short[] tradeTypeIds;

	@SearchField(searchFieldPath = "tradeType", searchField = "name")
	private String tradeTypeName;

	@SearchField(searchFieldPath = "tradeType", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] tradeTypeNames;

	@SearchField(searchField = "openCloseType.id", sortField = "openCloseType.name")
	private Short tradeOpenCloseTypeId;

	@SearchField(searchFieldPath = "openCloseType", searchField = "name")
	private String tradeOpenCloseTypeName;

	@SearchField(searchFieldPath = "openCloseType", searchField = "label")
	private String tradeOpenCloseTypeLabel;

	@SearchField(searchField = "tradeSource.id")
	private Short tradeSourceId;

	@SearchField(searchFieldPath = "tradeSource", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String tradeSourceNameEquals;

	@SearchField(searchField = "clientRelationship.id", searchFieldPath = "clientInvestmentAccount.businessClient", sortField = "clientRelationship.name")
	private Integer businessClientRelationshipId;

	@SearchField(searchField = "name", searchFieldPath = "clientInvestmentAccount.businessClient.clientRelationship")
	private String businessClientRelationshipName;

	@SearchField(searchField = "businessClient.id", searchFieldPath = "clientInvestmentAccount", sortField = "businessClient.name")
	private Integer businessClientId;

	@SearchField(searchField = "name", searchFieldPath = "clientInvestmentAccount.businessClient")
	private String businessClientName;

	@SearchField(searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchField = "clientInvestmentAccount.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Integer excludeClientInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Integer[] excludeClientInvestmentAccountIds;

	// Custom Search Field = Find Trades for a Specific Account or any of its SubAccounts
	private Integer clientInvestmentAccountIdOrSubAccount;

	@SearchField(searchField = "businessService.id", searchFieldPath = "clientInvestmentAccount")
	private Short businessServiceId;

	// custom search filter: account business service or any of its parent services
	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "clientInvestmentAccount.businessService.id,clientInvestmentAccount.businessService.parent.id,clientInvestmentAccount.businessService.parent.parent.id,clientInvestmentAccount.businessService.parent.parent.parent.id", leftJoin = true, sortField = "clientInvestmentAccount.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer[] holdingInvestmentAccountIds;

	@SearchField(searchField = "holdingInvestmentAccount.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean holdingInvestmentAccountPopulated;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingInvestmentAccount", comparisonConditions = ComparisonConditions.IN)
	private Integer[] holdingAccountIssuingCompanyIds;

	@SearchField(searchFieldPath = "holdingInvestmentAccount.type", searchField = "name")
	private String holdingAccountTypeName;

	@SearchField(searchFieldPath = "holdingInvestmentAccount", searchField = "type.id", sortField = "type.name")
	private Short holdingAccountTypeId;

	@SearchField(searchField = "executingBrokerCompany.id", sortField = "executingBrokerCompany.name")
	private Integer executingBrokerId;

	@SearchField(searchField = "executingBrokerCompany.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] executingBrokerIds;

	@SearchField(searchField = "executingSponsorCompany.id", sortField = "executingSponsorCompany.name")
	private Integer executingSponsorCompanyId;

	@SearchField(searchField = "settlementCompany.id", sortField = "settlementCompany.name")
	private Integer settlementCompanyId;

	@SearchField(searchField = "payingSecurity.id", sortField = "payingSecurity.symbol")
	private Integer payingSecurityId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "investmentSecurity.instrument", sortField = "tradingCurrency.symbol", comparisonConditions = {ComparisonConditions.EQUALS,
			ComparisonConditions.NOT_EQUALS})
	private Integer tradingCurrencyId;

	@SearchField(searchField = "investmentSecurity.id", sortField = "investmentSecurity.symbol")
	private Integer securityId;

	@SearchField(searchField = "investmentSecurity.id", sortField = "investmentSecurity.symbol", comparisonConditions = {ComparisonConditions.IN})
	private Integer[] securityIds;

	@SearchField(searchField = "name", searchFieldPath = "investmentSecurity", sortField = "name")
	private String investmentSecurityName;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "investmentSecurity", sortField = "underlyingSecurity.symbol")
	private Integer underlyingSecurityId;

	@SearchField(searchField = "instrument.id", searchFieldPath = "investmentSecurity", sortField = "instrument.identifierPrefix")
	private Integer investmentInstrumentId;

	// Custom Search Filter that looks up trades for the instrument and instrument's big instrument
	private Integer includeBigTradesInvestmentInstrumentId;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "investmentSecurity.instrument")
	private Short investmentHierarchyId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "investmentSecurity.instrument.hierarchy", sortField = "investmentType.name")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "investmentSecurity.instrument.hierarchy", sortField = "investmentType.name", comparisonConditions = {ComparisonConditions.IN})
	private Short[] investmentTypeIds;

	@SearchField(searchField = "priceMultiplierOverride,instrument.priceMultiplier", searchFieldPath = "investmentSecurity", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal priceMultiplier;

	@SearchField(searchField = "traderUser.id", sortField = "traderUser.displayName")
	private Short traderId;

	@SearchField(searchField = "teamSecurityGroup.id", searchFieldPath = "clientInvestmentAccount", sortField = "teamSecurityGroup.name")
	private Short teamSecurityGroupId;

	@SearchField
	private Boolean buy;

	@SearchField(searchField = "fixTradeOnly", searchFieldPath = "tradeDestination.type")
	private Boolean fixTrade;

	@SearchField(searchField = "tradeDestination.id", sortField = "tradeDestination.name")
	private Short tradeDestinationId;

	@SearchField(searchField = "name", searchFieldPath = "tradeDestination")
	private String tradeDestination;

	@SearchField
	private Boolean collateralTrade;

	@SearchField
	private Boolean blockTrade;

	@SearchField
	private BigDecimal exchangeRateToBase;

	@SearchField
	private BigDecimal expectedUnitPrice;

	@SearchField
	private BigDecimal averageUnitPrice;

	@SearchField
	private BigDecimal limitPrice;

	@SearchField
	private BigDecimal commissionPerUnit;

	@SearchField
	private BigDecimal commissionAmount;

	@SearchField
	private BigDecimal feeAmount;

	@SearchField
	private BigDecimal accrualAmount1;

	@SearchField
	private BigDecimal accrualAmount2;

	@SearchField
	private Boolean novation;

	@SearchField(searchField = "assignmentCompany.id")
	private Integer assignmentCompanyId;

	// Custom Search Filter that looks up trades with priority given to assignmentCompany then Executing Broker
	private Integer assignmentOrExecutingBrokerCompanyId;

	@SearchField
	private BigDecimal accountingNotional;

	@SearchField
	private BigDecimal quantityIntended;

	@SearchField
	private BigDecimal originalFace;

	@SearchField
	private BigDecimal notionalMultiplier;

	@SearchField
	private BigDecimal currentFactor;

	@SearchField
	private Date tradeDate;

	@SearchField(searchField = "tradeDate", comparisonConditions = {ComparisonConditions.GREATER_THAN})
	private Date tradeDateGreaterThan;

	@SearchField(searchField = "tradeDate", comparisonConditions = {ComparisonConditions.LESS_THAN})
	private Date tradeDateLessThan;

	@SearchField(searchField = "tradeDate", comparisonConditions = {ComparisonConditions.GREATER_THAN_OR_EQUALS})
	private Date tradeDateGreaterThanOrEquals;

	@SearchField(searchField = "tradeDate", comparisonConditions = {ComparisonConditions.LESS_THAN_OR_EQUALS})
	private Date tradeDateLessThanOrEquals;

	@SearchField
	private Date settlementDate;

	@SearchField
	private Date actualSettlementDate;

	@SearchField
	private String description;

	// Custom Search Field
	private String excludeTradeGroupTypeName;

	// Custom Search Field
	private Short[] excludeTradeGroupTypeIds;

	@SearchField(searchFieldPath = "tradeGroup.tradeGroupType", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String includeTradeGroupTypeName;

	@SearchField(searchFieldPath = "tradeGroup", searchField = "tradeGroupType.id", sortField = "tradeGroupType.name")
	private Short tradeGroupTypeId;

	@SearchField(searchField = "tradeGroup.id")
	private Integer tradeGroupId;

	@SearchField(searchField = "tradeGroup.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] tradeGroupIds;

	@SearchField(searchFieldPath = "tradeGroup", searchField = "sourceFkFieldId")
	private Integer tradeGroupSourceFkFieldId;

	@SearchField(searchField = "tradeGroup.parent.id")
	private Integer tradeGroupParentId;

	/**
	 * Note: Should use orderIdentifier and remove this,
	 * however there are places that use this in UI and to prevent issues, will leave this as a search form field for now
	 */
	@SearchField(searchField = "orderIdentifier")
	private Integer tradeOrderId;

	@SearchField
	private Integer orderIdentifier;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.id", searchFieldPath = "investmentSecurity", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentSecurityGroupId;

	@SearchField(searchField = "tradeExecutionType.name")
	private String tradeExecutionTypeName;

	@SearchField(searchField = "tradeExecutionType.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String tradeExecutionTypeNameEquals;

	@SearchField(searchField = "tradeExecutionType.id")
	private Short tradeExecutionTypeId;

	@SearchField(searchField = "limitPrice", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean limitTrade;

	@SearchField(searchField = "tradeExecutionType.marketOnCloseTrade")
	private Boolean marketOnCloseTrade;

	@SearchField(searchField = "tradeTimeInForceType.id")
	private Short tradeTimeInForceTypeId;

	@SearchField
	private Date timeInForceExpirationDate;

	@SearchField
	private String externalTradeIdentifier;

	@SearchField(searchField = "externalTradeIdentifier", comparisonConditions = ComparisonConditions.EQUALS)
	private String externalTradeIdentifierEquals;

	// Custom Search Filters
	private Integer clientInvestmentAccountGroupId;
	private Integer[] clientInvestmentAccountGroupIds;
	private Integer holdingInvestmentAccountGroupId;
	private Integer[] holdingInvestmentAccountGroupIds;
	private Integer excludeHoldingInvestmentAccountGroupId;
	private Integer[] excludeHoldingInvestmentAccountGroupIds;
	private Integer assumedFactorSecurityEventId; // trades for the specified Factor Change event that were traded at assumed factor
	private Integer custodianCompanyId;
	private Integer[] brokerOrIssuingCompanyIds; //Trades where the broker or issuing company is in the specified list
	/**
	 * Custom IN filter for roll trades to avoid joins on tradeGroup and tradeGroupType (trades member of roll trade group type)
	 */
	private Boolean roll;
	/**
	 * Custom IN filter for imported trades to avoid joins on tradeGroup and tradeGroupType (trades member of import trade group type)
	 */
	private Boolean tradeImport;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		Set<String> populatedFields = SearchUtils.validateRequiredFilter(this, "buy", "excludeTradeGroupTypeName");

		SearchUtils.validateInvalidValuesForSingleRestriction(this, populatedFields, MapUtils.of("workflowStateName", "booked"));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Integer getExcludeId() {
		return this.excludeId;
	}


	public void setExcludeId(Integer excludeId) {
		this.excludeId = excludeId;
	}


	public Integer[] getExcludeIds() {
		return this.excludeIds;
	}


	public void setExcludeIds(Integer[] excludeIds) {
		this.excludeIds = excludeIds;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public Short[] getTradeTypeIds() {
		return this.tradeTypeIds;
	}


	public void setTradeTypeIds(Short[] tradeTypeIds) {
		this.tradeTypeIds = tradeTypeIds;
	}


	public String getTradeTypeName() {
		return this.tradeTypeName;
	}


	public void setTradeTypeName(String tradeTypeName) {
		this.tradeTypeName = tradeTypeName;
	}


	public String[] getTradeTypeNames() {
		return this.tradeTypeNames;
	}


	public void setTradeTypeNames(String[] tradeTypeNames) {
		this.tradeTypeNames = tradeTypeNames;
	}


	public Short getTradeOpenCloseTypeId() {
		return this.tradeOpenCloseTypeId;
	}


	public void setTradeOpenCloseTypeId(Short tradeOpenCloseTypeId) {
		this.tradeOpenCloseTypeId = tradeOpenCloseTypeId;
	}


	public String getTradeOpenCloseTypeName() {
		return this.tradeOpenCloseTypeName;
	}


	public void setTradeOpenCloseTypeName(String tradeOpenCloseTypeName) {
		this.tradeOpenCloseTypeName = tradeOpenCloseTypeName;
	}


	public String getTradeOpenCloseTypeLabel() {
		return this.tradeOpenCloseTypeLabel;
	}


	public void setTradeOpenCloseTypeLabel(String tradeOpenCloseTypeLabel) {
		this.tradeOpenCloseTypeLabel = tradeOpenCloseTypeLabel;
	}


	public Short getTradeSourceId() {
		return this.tradeSourceId;
	}


	public void setTradeSourceId(Short tradeSourceId) {
		this.tradeSourceId = tradeSourceId;
	}


	public String getTradeSourceNameEquals() {
		return this.tradeSourceNameEquals;
	}


	public void setTradeSourceNameEquals(String tradeSourceNameEquals) {
		this.tradeSourceNameEquals = tradeSourceNameEquals;
	}


	public Integer getBusinessClientRelationshipId() {
		return this.businessClientRelationshipId;
	}


	public void setBusinessClientRelationshipId(Integer businessClientRelationshipId) {
		this.businessClientRelationshipId = businessClientRelationshipId;
	}


	public String getBusinessClientRelationshipName() {
		return this.businessClientRelationshipName;
	}


	public void setBusinessClientRelationshipName(String businessClientRelationshipName) {
		this.businessClientRelationshipName = businessClientRelationshipName;
	}


	public Integer getBusinessClientId() {
		return this.businessClientId;
	}


	public void setBusinessClientId(Integer businessClientId) {
		this.businessClientId = businessClientId;
	}


	public String getBusinessClientName() {
		return this.businessClientName;
	}


	public void setBusinessClientName(String businessClientName) {
		this.businessClientName = businessClientName;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Integer getExcludeClientInvestmentAccountId() {
		return this.excludeClientInvestmentAccountId;
	}


	public void setExcludeClientInvestmentAccountId(Integer excludeClientInvestmentAccountId) {
		this.excludeClientInvestmentAccountId = excludeClientInvestmentAccountId;
	}


	public Integer[] getExcludeClientInvestmentAccountIds() {
		return this.excludeClientInvestmentAccountIds;
	}


	public void setExcludeClientInvestmentAccountIds(Integer[] excludeClientInvestmentAccountIds) {
		this.excludeClientInvestmentAccountIds = excludeClientInvestmentAccountIds;
	}


	public Integer getClientInvestmentAccountIdOrSubAccount() {
		return this.clientInvestmentAccountIdOrSubAccount;
	}


	public void setClientInvestmentAccountIdOrSubAccount(Integer clientInvestmentAccountIdOrSubAccount) {
		this.clientInvestmentAccountIdOrSubAccount = clientInvestmentAccountIdOrSubAccount;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer[] getHoldingInvestmentAccountIds() {
		return this.holdingInvestmentAccountIds;
	}


	public void setHoldingInvestmentAccountIds(Integer[] holdingInvestmentAccountIds) {
		this.holdingInvestmentAccountIds = holdingInvestmentAccountIds;
	}


	public Boolean getHoldingInvestmentAccountPopulated() {
		return this.holdingInvestmentAccountPopulated;
	}


	public void setHoldingInvestmentAccountPopulated(Boolean holdingInvestmentAccountPopulated) {
		this.holdingInvestmentAccountPopulated = holdingInvestmentAccountPopulated;
	}


	public Integer getHoldingAccountIssuingCompanyId() {
		return this.holdingAccountIssuingCompanyId;
	}


	public void setHoldingAccountIssuingCompanyId(Integer holdingAccountIssuingCompanyId) {
		this.holdingAccountIssuingCompanyId = holdingAccountIssuingCompanyId;
	}


	public Integer[] getHoldingAccountIssuingCompanyIds() {
		return this.holdingAccountIssuingCompanyIds;
	}


	public void setHoldingAccountIssuingCompanyIds(Integer[] holdingAccountIssuingCompanyIds) {
		this.holdingAccountIssuingCompanyIds = holdingAccountIssuingCompanyIds;
	}


	public String getHoldingAccountTypeName() {
		return this.holdingAccountTypeName;
	}


	public void setHoldingAccountTypeName(String holdingAccountTypeName) {
		this.holdingAccountTypeName = holdingAccountTypeName;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Integer getExecutingBrokerId() {
		return this.executingBrokerId;
	}


	public void setExecutingBrokerId(Integer executingBrokerId) {
		this.executingBrokerId = executingBrokerId;
	}


	public Integer[] getExecutingBrokerIds() {
		return this.executingBrokerIds;
	}


	public void setExecutingBrokerIds(Integer[] executingBrokerIds) {
		this.executingBrokerIds = executingBrokerIds;
	}


	public Integer getExecutingSponsorCompanyId() {
		return this.executingSponsorCompanyId;
	}


	public void setExecutingSponsorCompanyId(Integer executingSponsorCompanyId) {
		this.executingSponsorCompanyId = executingSponsorCompanyId;
	}


	public Integer getSettlementCompanyId() {
		return this.settlementCompanyId;
	}


	public void setSettlementCompanyId(Integer settlementCompanyId) {
		this.settlementCompanyId = settlementCompanyId;
	}


	public Integer getPayingSecurityId() {
		return this.payingSecurityId;
	}


	public void setPayingSecurityId(Integer payingSecurityId) {
		this.payingSecurityId = payingSecurityId;
	}


	public Integer getTradingCurrencyId() {
		return this.tradingCurrencyId;
	}


	public void setTradingCurrencyId(Integer tradingCurrencyId) {
		this.tradingCurrencyId = tradingCurrencyId;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer[] getSecurityIds() {
		return this.securityIds;
	}


	public void setSecurityIds(Integer[] securityIds) {
		this.securityIds = securityIds;
	}


	public String getInvestmentSecurityName() {
		return this.investmentSecurityName;
	}


	public void setInvestmentSecurityName(String investmentSecurityName) {
		this.investmentSecurityName = investmentSecurityName;
	}


	public Integer getUnderlyingSecurityId() {
		return this.underlyingSecurityId;
	}


	public void setUnderlyingSecurityId(Integer underlyingSecurityId) {
		this.underlyingSecurityId = underlyingSecurityId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Integer getIncludeBigTradesInvestmentInstrumentId() {
		return this.includeBigTradesInvestmentInstrumentId;
	}


	public void setIncludeBigTradesInvestmentInstrumentId(Integer includeBigTradesInvestmentInstrumentId) {
		this.includeBigTradesInvestmentInstrumentId = includeBigTradesInvestmentInstrumentId;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short[] getInvestmentTypeIds() {
		return this.investmentTypeIds;
	}


	public void setInvestmentTypeIds(Short[] investmentTypeIds) {
		this.investmentTypeIds = investmentTypeIds;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public Short getTraderId() {
		return this.traderId;
	}


	public void setTraderId(Short traderId) {
		this.traderId = traderId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}


	public Boolean getFixTrade() {
		return this.fixTrade;
	}


	public void setFixTrade(Boolean fixTrade) {
		this.fixTrade = fixTrade;
	}


	public Short getTradeDestinationId() {
		return this.tradeDestinationId;
	}


	public void setTradeDestinationId(Short tradeDestinationId) {
		this.tradeDestinationId = tradeDestinationId;
	}


	public String getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(String tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public Boolean getCollateralTrade() {
		return this.collateralTrade;
	}


	public void setCollateralTrade(Boolean collateralTrade) {
		this.collateralTrade = collateralTrade;
	}


	public Boolean getBlockTrade() {
		return this.blockTrade;
	}


	public void setBlockTrade(Boolean blockTrade) {
		this.blockTrade = blockTrade;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getExpectedUnitPrice() {
		return this.expectedUnitPrice;
	}


	public void setExpectedUnitPrice(BigDecimal expectedUnitPrice) {
		this.expectedUnitPrice = expectedUnitPrice;
	}


	public BigDecimal getAverageUnitPrice() {
		return this.averageUnitPrice;
	}


	public void setAverageUnitPrice(BigDecimal averageUnitPrice) {
		this.averageUnitPrice = averageUnitPrice;
	}


	public BigDecimal getLimitPrice() {
		return this.limitPrice;
	}


	public void setLimitPrice(BigDecimal limitPrice) {
		this.limitPrice = limitPrice;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}


	public BigDecimal getAccrualAmount1() {
		return this.accrualAmount1;
	}


	public void setAccrualAmount1(BigDecimal accrualAmount1) {
		this.accrualAmount1 = accrualAmount1;
	}


	public BigDecimal getAccrualAmount2() {
		return this.accrualAmount2;
	}


	public void setAccrualAmount2(BigDecimal accrualAmount2) {
		this.accrualAmount2 = accrualAmount2;
	}


	public Boolean getNovation() {
		return this.novation;
	}


	public void setNovation(Boolean novation) {
		this.novation = novation;
	}


	public Integer getAssignmentCompanyId() {
		return this.assignmentCompanyId;
	}


	public void setAssignmentCompanyId(Integer assignmentCompanyId) {
		this.assignmentCompanyId = assignmentCompanyId;
	}


	public Integer getAssignmentOrExecutingBrokerCompanyId() {
		return this.assignmentOrExecutingBrokerCompanyId;
	}


	public void setAssignmentOrExecutingBrokerCompanyId(Integer assignmentOrExecutingBrokerCompanyId) {
		this.assignmentOrExecutingBrokerCompanyId = assignmentOrExecutingBrokerCompanyId;
	}


	public BigDecimal getAccountingNotional() {
		return this.accountingNotional;
	}


	public void setAccountingNotional(BigDecimal accountingNotional) {
		this.accountingNotional = accountingNotional;
	}


	public BigDecimal getQuantityIntended() {
		return this.quantityIntended;
	}


	public void setQuantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getNotionalMultiplier() {
		return this.notionalMultiplier;
	}


	public void setNotionalMultiplier(BigDecimal notionalMultiplier) {
		this.notionalMultiplier = notionalMultiplier;
	}


	public BigDecimal getCurrentFactor() {
		return this.currentFactor;
	}


	public void setCurrentFactor(BigDecimal currentFactor) {
		this.currentFactor = currentFactor;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getTradeDateGreaterThan() {
		return this.tradeDateGreaterThan;
	}


	public void setTradeDateGreaterThan(Date tradeDateGreaterThan) {
		this.tradeDateGreaterThan = tradeDateGreaterThan;
	}


	public Date getTradeDateLessThan() {
		return this.tradeDateLessThan;
	}


	public void setTradeDateLessThan(Date tradeDateLessThan) {
		this.tradeDateLessThan = tradeDateLessThan;
	}


	public Date getTradeDateGreaterThanOrEquals() {
		return this.tradeDateGreaterThanOrEquals;
	}


	public void setTradeDateGreaterThanOrEquals(Date tradeDateGreaterThanOrEquals) {
		this.tradeDateGreaterThanOrEquals = tradeDateGreaterThanOrEquals;
	}


	public Date getTradeDateLessThanOrEquals() {
		return this.tradeDateLessThanOrEquals;
	}


	public void setTradeDateLessThanOrEquals(Date tradeDateLessThanOrEquals) {
		this.tradeDateLessThanOrEquals = tradeDateLessThanOrEquals;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getActualSettlementDate() {
		return this.actualSettlementDate;
	}


	public void setActualSettlementDate(Date actualSettlementDate) {
		this.actualSettlementDate = actualSettlementDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getExcludeTradeGroupTypeName() {
		return this.excludeTradeGroupTypeName;
	}


	public void setExcludeTradeGroupTypeName(String excludeTradeGroupTypeName) {
		this.excludeTradeGroupTypeName = excludeTradeGroupTypeName;
	}


	public Short[] getExcludeTradeGroupTypeIds() {
		return this.excludeTradeGroupTypeIds;
	}


	public void setExcludeTradeGroupTypeIds(Short[] excludeTradeGroupTypeIds) {
		this.excludeTradeGroupTypeIds = excludeTradeGroupTypeIds;
	}


	public String getIncludeTradeGroupTypeName() {
		return this.includeTradeGroupTypeName;
	}


	public void setIncludeTradeGroupTypeName(String includeTradeGroupTypeName) {
		this.includeTradeGroupTypeName = includeTradeGroupTypeName;
	}


	public Short getTradeGroupTypeId() {
		return this.tradeGroupTypeId;
	}


	public void setTradeGroupTypeId(Short tradeGroupTypeId) {
		this.tradeGroupTypeId = tradeGroupTypeId;
	}


	public Integer getTradeGroupId() {
		return this.tradeGroupId;
	}


	public void setTradeGroupId(Integer tradeGroupId) {
		this.tradeGroupId = tradeGroupId;
	}


	public Integer[] getTradeGroupIds() {
		return this.tradeGroupIds;
	}


	public void setTradeGroupIds(Integer[] tradeGroupIds) {
		this.tradeGroupIds = tradeGroupIds;
	}


	public Integer getTradeGroupSourceFkFieldId() {
		return this.tradeGroupSourceFkFieldId;
	}


	public void setTradeGroupSourceFkFieldId(Integer tradeGroupSourceFkFieldId) {
		this.tradeGroupSourceFkFieldId = tradeGroupSourceFkFieldId;
	}


	public Integer getTradeGroupParentId() {
		return this.tradeGroupParentId;
	}


	public void setTradeGroupParentId(Integer tradeGroupParentId) {
		this.tradeGroupParentId = tradeGroupParentId;
	}


	public Integer getTradeOrderId() {
		return this.tradeOrderId;
	}


	public void setTradeOrderId(Integer tradeOrderId) {
		this.tradeOrderId = tradeOrderId;
	}


	public Integer getOrderIdentifier() {
		return this.orderIdentifier;
	}


	public void setOrderIdentifier(Integer orderIdentifier) {
		this.orderIdentifier = orderIdentifier;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public String getTradeExecutionTypeName() {
		return this.tradeExecutionTypeName;
	}


	public void setTradeExecutionTypeName(String tradeExecutionTypeName) {
		this.tradeExecutionTypeName = tradeExecutionTypeName;
	}


	public Short getTradeExecutionTypeId() {
		return this.tradeExecutionTypeId;
	}


	public void setTradeExecutionTypeId(Short tradeExecutionTypeId) {
		this.tradeExecutionTypeId = tradeExecutionTypeId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer[] getClientInvestmentAccountGroupIds() {
		return this.clientInvestmentAccountGroupIds;
	}


	public void setClientInvestmentAccountGroupIds(Integer[] clientInvestmentAccountGroupIds) {
		this.clientInvestmentAccountGroupIds = clientInvestmentAccountGroupIds;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	public Integer[] getHoldingInvestmentAccountGroupIds() {
		return this.holdingInvestmentAccountGroupIds;
	}


	public void setHoldingInvestmentAccountGroupIds(Integer[] holdingInvestmentAccountGroupIds) {
		this.holdingInvestmentAccountGroupIds = holdingInvestmentAccountGroupIds;
	}


	public Integer getExcludeHoldingInvestmentAccountGroupId() {
		return this.excludeHoldingInvestmentAccountGroupId;
	}


	public void setExcludeHoldingInvestmentAccountGroupId(Integer excludeHoldingInvestmentAccountGroupId) {
		this.excludeHoldingInvestmentAccountGroupId = excludeHoldingInvestmentAccountGroupId;
	}


	public Integer[] getExcludeHoldingInvestmentAccountGroupIds() {
		return this.excludeHoldingInvestmentAccountGroupIds;
	}


	public void setExcludeHoldingInvestmentAccountGroupIds(Integer[] excludeHoldingInvestmentAccountGroupIds) {
		this.excludeHoldingInvestmentAccountGroupIds = excludeHoldingInvestmentAccountGroupIds;
	}


	public Integer getAssumedFactorSecurityEventId() {
		return this.assumedFactorSecurityEventId;
	}


	public void setAssumedFactorSecurityEventId(Integer assumedFactorSecurityEventId) {
		this.assumedFactorSecurityEventId = assumedFactorSecurityEventId;
	}


	public Integer getCustodianCompanyId() {
		return this.custodianCompanyId;
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.custodianCompanyId = custodianCompanyId;
	}


	public Integer[] getBrokerOrIssuingCompanyIds() {
		return this.brokerOrIssuingCompanyIds;
	}


	public void setBrokerOrIssuingCompanyIds(Integer[] brokerOrIssuingCompanyIds) {
		this.brokerOrIssuingCompanyIds = brokerOrIssuingCompanyIds;
	}


	public String getTradeExecutionTypeNameEquals() {
		return this.tradeExecutionTypeNameEquals;
	}


	public void setTradeExecutionTypeNameEquals(String tradeExecutionTypeNameEquals) {
		this.tradeExecutionTypeNameEquals = tradeExecutionTypeNameEquals;
	}


	public Boolean getLimitTrade() {
		return this.limitTrade;
	}


	public void setLimitTrade(Boolean limitTrade) {
		this.limitTrade = limitTrade;
	}


	public Boolean getMarketOnCloseTrade() {
		return this.marketOnCloseTrade;
	}


	public void setMarketOnCloseTrade(Boolean marketOnCloseTrade) {
		this.marketOnCloseTrade = marketOnCloseTrade;
	}


	public Short getTradeTimeInForceTypeId() {
		return this.tradeTimeInForceTypeId;
	}


	public void setTradeTimeInForceTypeId(Short tradeTimeInForceTypeId) {
		this.tradeTimeInForceTypeId = tradeTimeInForceTypeId;
	}


	public Date getTimeInForceExpirationDate() {
		return this.timeInForceExpirationDate;
	}


	public void setTimeInForceExpirationDate(Date timeInForceExpirationDate) {
		this.timeInForceExpirationDate = timeInForceExpirationDate;
	}


	public Boolean getRoll() {
		return this.roll;
	}


	public void setRoll(Boolean roll) {
		this.roll = roll;
	}


	public Boolean getTradeImport() {
		return this.tradeImport;
	}


	public void setTradeImport(Boolean tradeImport) {
		this.tradeImport = tradeImport;
	}


	public String getExternalTradeIdentifier() {
		return this.externalTradeIdentifier;
	}


	public void setExternalTradeIdentifier(String externalTradeIdentifier) {
		this.externalTradeIdentifier = externalTradeIdentifier;
	}


	public String getExternalTradeIdentifierEquals() {
		return this.externalTradeIdentifierEquals;
	}


	public void setExternalTradeIdentifierEquals(String externalTradeIdentifierEquals) {
		this.externalTradeIdentifierEquals = externalTradeIdentifierEquals;
	}
}
