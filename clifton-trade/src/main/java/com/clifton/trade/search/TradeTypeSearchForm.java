package com.clifton.trade.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class TradeTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] nameList;

	@SearchField
	private String description;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentGroup.id")
	private Short investmentGroupId;

	@SearchField(searchField = "holdingAccountPurpose.id")
	private Short holdingAccountPurposeId;

	@SearchField(searchField = "commissionAccountingAccount.id")
	private Short commissionAccountingAccountId;

	@SearchField(searchField = "feeAccountingAccount.id")
	private Short feeAccountingAccountId;

	@SearchField(searchField = "buyAccrualAccountingAccount.id")
	private Short buyAccrualAccountingAccountId;

	@SearchField(searchField = "sellAccrualAccountingAccount.id")
	private Short sellAccrualAccountingAccountId;

	@SearchField(searchField = "tradeReport.id")
	private Integer tradeReportId;

	@SearchField
	private Boolean fixSupported;
	@SearchField
	private Boolean collateralSupported;
	@SearchField
	private Boolean singleFillTrade;
	@SearchField
	private Boolean amountsSignAccountsForBuy;
	@SearchField
	private Boolean accrualSignAccountsForBuy;
	@SearchField
	private Boolean brokerSelectionAtExecutionAllowed;
	@SearchField
	private Boolean holdingAccountSelectionAtExecutionAllowed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getHoldingAccountPurposeId() {
		return this.holdingAccountPurposeId;
	}


	public void setHoldingAccountPurposeId(Short holdingAccountPurposeId) {
		this.holdingAccountPurposeId = holdingAccountPurposeId;
	}


	public Short getCommissionAccountingAccountId() {
		return this.commissionAccountingAccountId;
	}


	public void setCommissionAccountingAccountId(Short commissionAccountingAccountId) {
		this.commissionAccountingAccountId = commissionAccountingAccountId;
	}


	public Short getFeeAccountingAccountId() {
		return this.feeAccountingAccountId;
	}


	public void setFeeAccountingAccountId(Short feeAccountingAccountId) {
		this.feeAccountingAccountId = feeAccountingAccountId;
	}


	public Short getBuyAccrualAccountingAccountId() {
		return this.buyAccrualAccountingAccountId;
	}


	public void setBuyAccrualAccountingAccountId(Short buyAccrualAccountingAccountId) {
		this.buyAccrualAccountingAccountId = buyAccrualAccountingAccountId;
	}


	public Short getSellAccrualAccountingAccountId() {
		return this.sellAccrualAccountingAccountId;
	}


	public void setSellAccrualAccountingAccountId(Short sellAccrualAccountingAccountId) {
		this.sellAccrualAccountingAccountId = sellAccrualAccountingAccountId;
	}


	public Integer getTradeReportId() {
		return this.tradeReportId;
	}


	public void setTradeReportId(Integer tradeReportId) {
		this.tradeReportId = tradeReportId;
	}


	public Boolean getFixSupported() {
		return this.fixSupported;
	}


	public void setFixSupported(Boolean fixSupported) {
		this.fixSupported = fixSupported;
	}


	public Boolean getCollateralSupported() {
		return this.collateralSupported;
	}


	public void setCollateralSupported(Boolean collateralSupported) {
		this.collateralSupported = collateralSupported;
	}


	public Boolean getSingleFillTrade() {
		return this.singleFillTrade;
	}


	public void setSingleFillTrade(Boolean singleFillTrade) {
		this.singleFillTrade = singleFillTrade;
	}


	public Boolean getAmountsSignAccountsForBuy() {
		return this.amountsSignAccountsForBuy;
	}


	public void setAmountsSignAccountsForBuy(Boolean amountsSignAccountsForBuy) {
		this.amountsSignAccountsForBuy = amountsSignAccountsForBuy;
	}


	public Boolean getAccrualSignAccountsForBuy() {
		return this.accrualSignAccountsForBuy;
	}


	public void setAccrualSignAccountsForBuy(Boolean accrualSignAccountsForBuy) {
		this.accrualSignAccountsForBuy = accrualSignAccountsForBuy;
	}


	public Boolean getBrokerSelectionAtExecutionAllowed() {
		return this.brokerSelectionAtExecutionAllowed;
	}


	public void setBrokerSelectionAtExecutionAllowed(Boolean brokerSelectionAtExecutionAllowed) {
		this.brokerSelectionAtExecutionAllowed = brokerSelectionAtExecutionAllowed;
	}


	public Boolean getHoldingAccountSelectionAtExecutionAllowed() {
		return this.holdingAccountSelectionAtExecutionAllowed;
	}


	public void setHoldingAccountSelectionAtExecutionAllowed(Boolean holdingAccountSelectionAtExecutionAllowed) {
		this.holdingAccountSelectionAtExecutionAllowed = holdingAccountSelectionAtExecutionAllowed;
	}


	public String[] getNameList() {
		return this.nameList;
	}


	public void setNameList(String[] nameList) {
		this.nameList = nameList;
	}
}
