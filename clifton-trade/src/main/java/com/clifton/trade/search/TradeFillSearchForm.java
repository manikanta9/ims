package com.clifton.trade.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.workflow.search.WorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>TradeFillSearchForm</code> class defines search configuration for TradeFill objects.
 */
public class TradeFillSearchForm extends BaseAuditableEntitySearchForm implements WorkflowAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] idList;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Integer[] excludeIdList;

	@SearchField(searchFieldPath = "trade", searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchFieldPath = "trade", searchField = "clientInvestmentAccount.id", sortField = "clientInvestmentAccount.number")
	private Integer[] clientInvestmentAccountIds;

	@SearchField(searchFieldPath = "trade", searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchFieldPath = "trade", searchField = "holdingInvestmentAccount.id", sortField = "holdingInvestmentAccount.number")
	private Integer[] holdingInvestmentAccountIds;

	@SearchField(searchFieldPath = "trade.holdingInvestmentAccount", searchField = "issuingCompany.id", sortField = "issuingCompany.name")
	private Integer holdingAccountIssuingCompanyId;

	@SearchField(searchFieldPath = "trade", searchField = "executingBrokerCompany.id", sortField = "executingBrokerCompany.name")
	private Integer executingBrokerCompanyId;

	@SearchField(searchFieldPath = "trade", searchField = "executingSponsorCompany.id", sortField = "executingSponsorCompany.name")
	private Integer executingSponsorCompanyId;

	@SearchField(searchField = "tradingCurrency.id", searchFieldPath = "trade.investmentSecurity.instrument", sortField = "tradingCurrency.symbol")
	private Integer securityCurrencyDenominationId;

	@SearchField(searchField = "investmentSecurity.id", searchFieldPath = "trade", sortField = "investmentSecurity.symbol")
	private Integer securityId;

	@SearchField(searchField = "investmentSecurity.id", searchFieldPath = "trade", sortField = "investmentSecurity.symbol", comparisonConditions = ComparisonConditions.IN)
	private Integer[] securityIds;

	@SearchField(searchField = "instrument.id", searchFieldPath = "trade.investmentSecurity")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "trade.investmentSecurity.instrument")
	private Short investmentHierarchyId;

	@SearchField(searchField = "priceMultiplierOverride,instrument.priceMultiplier", searchFieldPath = "trade.investmentSecurity", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal priceMultiplier;


	@SearchField(searchFieldPath = "trade", searchField = "buy")
	private Boolean buy;

	@SearchField(searchField = "bookingDate", dateFieldIncludesTime = true, comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date bookingDateAfter;

	@SearchField(searchField = "bookingDate", dateFieldIncludesTime = true, comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date bookingDateBefore;

	@SearchField(searchField = "bookingDate", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean bookingDateIsNull;

	@SearchField(searchField = "trade.id")
	private Integer tradeId;

	@SearchField(searchField = "trade.id")
	private Integer[] tradeIds;

	@SearchField(searchField = "tradeGroup.id", searchFieldPath = "trade")
	private Integer tradeGroupId;

	@SearchField(searchField = "tradeGroup.id", searchFieldPath = "trade", comparisonConditions = ComparisonConditions.IN)
	private Integer[] tradeGroupIds;

	@SearchField(searchFieldPath = "trade", searchField = "tradeType.id")
	private Short tradeTypeId;

	@SearchField(searchFieldPath = "trade.tradeType", searchField = "name")
	private String tradeTypeName;

	@SearchField(searchFieldPath = "trade", searchField = "openCloseType.id", sortField = "openCloseType.name")
	private Short tradeOpenCloseTypeId;

	@SearchField(searchFieldPath = "trade.openCloseType", searchField = "name")
	private String tradeOpenCloseTypeName;

	@SearchField(searchFieldPath = "trade.openCloseType", searchField = "label")
	private String tradeOpenCloseTypeLabel;

	@SearchField(searchFieldPath = "trade", searchField = "tradeDestination.id")
	private Short tradeDestinationId;

	@SearchField(searchField = "name", searchFieldPath = "trade.tradeDestination")
	private String tradeDestination;

	@SearchField(searchFieldPath = "trade", searchField = "traderUser.id")
	private Short traderId;

	@SearchField(searchFieldPath = "trade", searchField = "commissionPerUnit")
	private BigDecimal tradeCommissionPerUnit;

	@SearchField(searchFieldPath = "trade", searchField = "commissionAmount")
	private BigDecimal tradeCommissionAmount;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal notionalUnitPrice;

	@SearchField(searchFieldPath = "trade", searchField = "exchangeRateToBase")
	private BigDecimal exchangeRateToBase;

	@SearchField(searchFieldPath = "trade", searchField = "tradeDate")
	private Date tradeDate;

	@SearchField(searchFieldPath = "trade", searchField = "settlementDate")
	private Date settlementDate;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "trade.investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "groupItemList.referenceOne.group.name", searchFieldPath = "trade.investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS_LIKE)
	private String investmentGroupName;

	@SearchField(searchFieldPath = "trade.tradeType", searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField
	private BigDecimal openingPrice;

	@SearchField
	private Date openingDate;


	// Custom Search Filter
	private Integer clientInvestmentAccountGroupId;
	private Integer holdingInvestmentAccountGroupId;

	/////////  Workflow Status Filters - Needs to be explicit because the path is different ///////////////

	@SearchField(searchFieldPath = "trade", searchField = "workflowStatus.id")
	private Short workflowStatusId;

	@SearchField(searchFieldPath = "trade", searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short workflowStatusIdOrNull;

	@SearchField(searchFieldPath = "trade", searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] workflowStatusIds;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowStatus", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowStatusNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowStatus")
	private String workflowStatusName;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowStatus", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL, leftJoin = true)
	private String workflowStatusNameOrNull;

	@SearchField(searchFieldPath = "trade.workflowStatus", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] workflowStatusNames;

	@SearchField(searchFieldPath = "trade", searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeWorkflowStatusId;

	@SearchField(searchFieldPath = "trade", searchField = "workflowStatus.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeWorkflowStatusIds;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowStatus", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeWorkflowStatusName;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowStatus", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeWorkflowStatusNames;


	/////////  Workflow State Filters ///////////////

	@SearchField(searchFieldPath = "trade", searchField = "workflowState.id")
	private Short workflowStateId;

	@SearchField(searchFieldPath = "trade", searchField = "workflowState.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short workflowStateIdOrNull;

	@SearchField(searchFieldPath = "trade", searchField = "workflowState.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Short[] workflowStateIdsOrNull;

	@SearchField(searchFieldPath = "trade", searchField = "workflowState.id")
	private Short[] workflowStateIds;

	@SearchField(searchFieldPath = "trade.workflowState", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String workflowStateNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowState")
	private String workflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowState", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL, leftJoin = true)
	private String workflowStateNameOrNull;

	@SearchField(searchFieldPath = "trade.workflowState", searchField = "name")
	private String[] workflowStateNames;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowState", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL, leftJoin = true)
	private String[] workflowStateNamesOrNull;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowState", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeWorkflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "trade.workflowState", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeWorkflowStateNames;

	@SearchField(searchFieldPath = "trade", searchField = "workflowState.id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Short excludeWorkflowStateId;

	@SearchField(searchFieldPath = "trade", searchField = "workflowState.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeWorkflowStateIds;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIdList() {
		return this.idList;
	}


	public void setIdList(Integer[] idList) {
		this.idList = idList;
	}


	public Integer[] getExcludeIdList() {
		return this.excludeIdList;
	}


	public void setExcludeIdList(Integer[] excludeIdList) {
		this.excludeIdList = excludeIdList;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer[] getClientInvestmentAccountIds() {
		return this.clientInvestmentAccountIds;
	}


	public void setClientInvestmentAccountIds(Integer[] clientInvestmentAccountIds) {
		this.clientInvestmentAccountIds = clientInvestmentAccountIds;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer[] getHoldingInvestmentAccountIds() {
		return this.holdingInvestmentAccountIds;
	}


	public void setHoldingInvestmentAccountIds(Integer[] holdingInvestmentAccountIds) {
		this.holdingInvestmentAccountIds = holdingInvestmentAccountIds;
	}


	public Integer getHoldingAccountIssuingCompanyId() {
		return this.holdingAccountIssuingCompanyId;
	}


	public void setHoldingAccountIssuingCompanyId(Integer holdingAccountIssuingCompanyId) {
		this.holdingAccountIssuingCompanyId = holdingAccountIssuingCompanyId;
	}


	public Integer getExecutingBrokerCompanyId() {
		return this.executingBrokerCompanyId;
	}


	public void setExecutingBrokerCompanyId(Integer executingBrokerCompanyId) {
		this.executingBrokerCompanyId = executingBrokerCompanyId;
	}


	public Integer getExecutingSponsorCompanyId() {
		return this.executingSponsorCompanyId;
	}


	public void setExecutingSponsorCompanyId(Integer executingSponsorCompanyId) {
		this.executingSponsorCompanyId = executingSponsorCompanyId;
	}


	public Integer getSecurityCurrencyDenominationId() {
		return this.securityCurrencyDenominationId;
	}


	public void setSecurityCurrencyDenominationId(Integer securityCurrencyDenominationId) {
		this.securityCurrencyDenominationId = securityCurrencyDenominationId;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer[] getSecurityIds() {
		return this.securityIds;
	}


	public void setSecurityIds(Integer[] securityIds) {
		this.securityIds = securityIds;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public BigDecimal getPriceMultiplier() {
		return this.priceMultiplier;
	}


	public void setPriceMultiplier(BigDecimal priceMultiplier) {
		this.priceMultiplier = priceMultiplier;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}


	public Date getBookingDateAfter() {
		return this.bookingDateAfter;
	}


	public void setBookingDateAfter(Date bookingDateAfter) {
		this.bookingDateAfter = bookingDateAfter;
	}


	public Date getBookingDateBefore() {
		return this.bookingDateBefore;
	}


	public void setBookingDateBefore(Date bookingDateBefore) {
		this.bookingDateBefore = bookingDateBefore;
	}


	public Boolean getBookingDateIsNull() {
		return this.bookingDateIsNull;
	}


	public void setBookingDateIsNull(Boolean bookingDateIsNull) {
		this.bookingDateIsNull = bookingDateIsNull;
	}


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public Integer[] getTradeIds() {
		return this.tradeIds;
	}


	public void setTradeIds(Integer[] tradeIds) {
		this.tradeIds = tradeIds;
	}


	public Integer getTradeGroupId() {
		return this.tradeGroupId;
	}


	public void setTradeGroupId(Integer tradeGroupId) {
		this.tradeGroupId = tradeGroupId;
	}


	public Integer[] getTradeGroupIds() {
		return this.tradeGroupIds;
	}


	public void setTradeGroupIds(Integer[] tradeGroupIds) {
		this.tradeGroupIds = tradeGroupIds;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public String getTradeTypeName() {
		return this.tradeTypeName;
	}


	public void setTradeTypeName(String tradeTypeName) {
		this.tradeTypeName = tradeTypeName;
	}


	public Short getTradeOpenCloseTypeId() {
		return this.tradeOpenCloseTypeId;
	}


	public void setTradeOpenCloseTypeId(Short tradeOpenCloseTypeId) {
		this.tradeOpenCloseTypeId = tradeOpenCloseTypeId;
	}


	public String getTradeOpenCloseTypeName() {
		return this.tradeOpenCloseTypeName;
	}


	public void setTradeOpenCloseTypeName(String tradeOpenCloseTypeName) {
		this.tradeOpenCloseTypeName = tradeOpenCloseTypeName;
	}


	public String getTradeOpenCloseTypeLabel() {
		return this.tradeOpenCloseTypeLabel;
	}


	public void setTradeOpenCloseTypeLabel(String tradeOpenCloseTypeLabel) {
		this.tradeOpenCloseTypeLabel = tradeOpenCloseTypeLabel;
	}


	public Short getTradeDestinationId() {
		return this.tradeDestinationId;
	}


	public void setTradeDestinationId(Short tradeDestinationId) {
		this.tradeDestinationId = tradeDestinationId;
	}


	public String getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(String tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public Short getTraderId() {
		return this.traderId;
	}


	public void setTraderId(Short traderId) {
		this.traderId = traderId;
	}


	public BigDecimal getTradeCommissionPerUnit() {
		return this.tradeCommissionPerUnit;
	}


	public void setTradeCommissionPerUnit(BigDecimal tradeCommissionPerUnit) {
		this.tradeCommissionPerUnit = tradeCommissionPerUnit;
	}


	public BigDecimal getTradeCommissionAmount() {
		return this.tradeCommissionAmount;
	}


	public void setTradeCommissionAmount(BigDecimal tradeCommissionAmount) {
		this.tradeCommissionAmount = tradeCommissionAmount;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getNotionalUnitPrice() {
		return this.notionalUnitPrice;
	}


	public void setNotionalUnitPrice(BigDecimal notionalUnitPrice) {
		this.notionalUnitPrice = notionalUnitPrice;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public String getInvestmentGroupName() {
		return this.investmentGroupName;
	}


	public void setInvestmentGroupName(String investmentGroupName) {
		this.investmentGroupName = investmentGroupName;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	@Override
	public Short getWorkflowStatusId() {
		return this.workflowStatusId;
	}


	@Override
	public void setWorkflowStatusId(Short workflowStatusId) {
		this.workflowStatusId = workflowStatusId;
	}


	@Override
	public Short getWorkflowStatusIdOrNull() {
		return this.workflowStatusIdOrNull;
	}


	@Override
	public void setWorkflowStatusIdOrNull(Short workflowStatusIdOrNull) {
		this.workflowStatusIdOrNull = workflowStatusIdOrNull;
	}


	@Override
	public Short[] getWorkflowStatusIds() {
		return this.workflowStatusIds;
	}


	@Override
	public void setWorkflowStatusIds(Short[] workflowStatusIds) {
		this.workflowStatusIds = workflowStatusIds;
	}


	@Override
	public String getWorkflowStatusNameEquals() {
		return this.workflowStatusNameEquals;
	}


	@Override
	public void setWorkflowStatusNameEquals(String workflowStatusNameEquals) {
		this.workflowStatusNameEquals = workflowStatusNameEquals;
	}


	@Override
	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	@Override
	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}


	@Override
	public String getWorkflowStatusNameOrNull() {
		return this.workflowStatusNameOrNull;
	}


	@Override
	public void setWorkflowStatusNameOrNull(String workflowStatusNameOrNull) {
		this.workflowStatusNameOrNull = workflowStatusNameOrNull;
	}


	@Override
	public String[] getWorkflowStatusNames() {
		return this.workflowStatusNames;
	}


	@Override
	public void setWorkflowStatusNames(String[] workflowStatusNames) {
		this.workflowStatusNames = workflowStatusNames;
	}


	@Override
	public Short getExcludeWorkflowStatusId() {
		return this.excludeWorkflowStatusId;
	}


	@Override
	public void setExcludeWorkflowStatusId(Short excludeWorkflowStatusId) {
		this.excludeWorkflowStatusId = excludeWorkflowStatusId;
	}


	@Override
	public Short[] getExcludeWorkflowStatusIds() {
		return this.excludeWorkflowStatusIds;
	}


	@Override
	public void setExcludeWorkflowStatusIds(Short[] excludeWorkflowStatusIds) {
		this.excludeWorkflowStatusIds = excludeWorkflowStatusIds;
	}


	@Override
	public String getExcludeWorkflowStatusName() {
		return this.excludeWorkflowStatusName;
	}


	@Override
	public void setExcludeWorkflowStatusName(String excludeWorkflowStatusName) {
		this.excludeWorkflowStatusName = excludeWorkflowStatusName;
	}


	@Override
	public String[] getExcludeWorkflowStatusNames() {
		return this.excludeWorkflowStatusNames;
	}


	@Override
	public void setExcludeWorkflowStatusNames(String[] excludeWorkflowStatusNames) {
		this.excludeWorkflowStatusNames = excludeWorkflowStatusNames;
	}


	@Override
	public Short getWorkflowStateId() {
		return this.workflowStateId;
	}


	@Override
	public void setWorkflowStateId(Short workflowStateId) {
		this.workflowStateId = workflowStateId;
	}


	@Override
	public Short getWorkflowStateIdOrNull() {
		return this.workflowStateIdOrNull;
	}


	@Override
	public void setWorkflowStateIdOrNull(Short workflowStateIdOrNull) {
		this.workflowStateIdOrNull = workflowStateIdOrNull;
	}


	@Override
	public Short[] getWorkflowStateIdsOrNull() {
		return this.workflowStateIdsOrNull;
	}


	@Override
	public void setWorkflowStateIdsOrNull(Short[] workflowStateIdsOrNull) {
		this.workflowStateIdsOrNull = workflowStateIdsOrNull;
	}


	@Override
	public Short[] getWorkflowStateIds() {
		return this.workflowStateIds;
	}


	@Override
	public void setWorkflowStateIds(Short[] workflowStateIds) {
		this.workflowStateIds = workflowStateIds;
	}


	@Override
	public String getWorkflowStateNameEquals() {
		return this.workflowStateNameEquals;
	}


	@Override
	public void setWorkflowStateNameEquals(String workflowStateNameEquals) {
		this.workflowStateNameEquals = workflowStateNameEquals;
	}


	@Override
	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	@Override
	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	@Override
	public String getWorkflowStateNameOrNull() {
		return this.workflowStateNameOrNull;
	}


	@Override
	public void setWorkflowStateNameOrNull(String workflowStateNameOrNull) {
		this.workflowStateNameOrNull = workflowStateNameOrNull;
	}


	@Override
	public String[] getWorkflowStateNames() {
		return this.workflowStateNames;
	}


	@Override
	public void setWorkflowStateNames(String[] workflowStateNames) {
		this.workflowStateNames = workflowStateNames;
	}


	@Override
	public String[] getWorkflowStateNamesOrNull() {
		return this.workflowStateNamesOrNull;
	}


	@Override
	public void setWorkflowStateNamesOrNull(String[] workflowStateNamesOrNull) {
		this.workflowStateNamesOrNull = workflowStateNamesOrNull;
	}


	@Override
	public String getExcludeWorkflowStateName() {
		return this.excludeWorkflowStateName;
	}


	@Override
	public void setExcludeWorkflowStateName(String excludeWorkflowStateName) {
		this.excludeWorkflowStateName = excludeWorkflowStateName;
	}


	@Override
	public String[] getExcludeWorkflowStateNames() {
		return this.excludeWorkflowStateNames;
	}


	@Override
	public void setExcludeWorkflowStateNames(String[] excludeWorkflowStateNames) {
		this.excludeWorkflowStateNames = excludeWorkflowStateNames;
	}


	@Override
	public Short getExcludeWorkflowStateId() {
		return this.excludeWorkflowStateId;
	}


	@Override
	public void setExcludeWorkflowStateId(Short excludeWorkflowStateId) {
		this.excludeWorkflowStateId = excludeWorkflowStateId;
	}


	@Override
	public Short[] getExcludeWorkflowStateIds() {
		return this.excludeWorkflowStateIds;
	}


	@Override
	public void setExcludeWorkflowStateIds(Short[] excludeWorkflowStateIds) {
		this.excludeWorkflowStateIds = excludeWorkflowStateIds;
	}


	public BigDecimal getOpeningPrice() {
		return this.openingPrice;
	}


	public void setOpeningPrice(BigDecimal openingPrice) {
		this.openingPrice = openingPrice;
	}


	public Date getOpeningDate() {
		return this.openingDate;
	}


	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}
}
