package com.clifton.trade.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>TradeTimeInForceTypeSearchForm</code> class defines search criteria for TradeTimeInForceType entities.
 *
 * @author davidi
 */
public class TradeTimeInForceTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,label,description")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField
	private String name;

	@SearchField
	private String label;

	@SearchField
	private String description;

	@SearchField
	private Boolean expirationDateRequired;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getExpirationDateRequired() {
		return this.expirationDateRequired;
	}


	public void setExpirationDateRequired(Boolean expirationDateRequired) {
		this.expirationDateRequired = expirationDateRequired;
	}
}
