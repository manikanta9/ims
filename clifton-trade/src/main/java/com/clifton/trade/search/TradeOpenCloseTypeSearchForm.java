package com.clifton.trade.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author mwacker
 */
public class TradeOpenCloseTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] nameList;

	@SearchField
	private String description;

	@SearchField
	private String label;

	@SearchField
	private Boolean buy;

	@SearchField
	private Boolean open;

	@SearchField
	private Boolean close;

	// Custom Search Field
	private Short tradeTypeId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String[] getNameList() {
		return this.nameList;
	}


	public void setNameList(String[] nameList) {
		this.nameList = nameList;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public Boolean getOpen() {
		return this.open;
	}


	public void setOpen(Boolean open) {
		this.open = open;
	}


	public Boolean getClose() {
		return this.close;
	}


	public void setClose(Boolean close) {
		this.close = close;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}
}
