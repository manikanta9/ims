package com.clifton.trade.assetclass.process;


import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;

import java.util.Date;
import java.util.Map;


/**
 * The <code>TradeAssetClassPositionAllocationProcessorJob</code> ...
 *
 * @author manderson
 */
public class TradeAssetClassPositionAllocationProcessorJob implements Task {

	private TradeAssetClassPositionAllocationProcessService tradeAssetClassPositionAllocationProcessService;

	/**
	 * Leave blank to run as previous business day
	 * Default logic to use for production, override date is for testing/working on QA where latest runs may not be for today
	 */
	private Date tradeDate;


	@Override
	public Status run(Map<String, Object> context) {
		String result = getTradeAssetClassPositionAllocationProcessService().processTradeAssetClassPositionAllocationList(null, getTradeDate());
		return Status.ofMessage(result);
	}


	public TradeAssetClassPositionAllocationProcessService getTradeAssetClassPositionAllocationProcessService() {
		return this.tradeAssetClassPositionAllocationProcessService;
	}


	public void setTradeAssetClassPositionAllocationProcessService(TradeAssetClassPositionAllocationProcessService tradeAssetClassPositionAllocationProcessService) {
		this.tradeAssetClassPositionAllocationProcessService = tradeAssetClassPositionAllocationProcessService;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}
}
