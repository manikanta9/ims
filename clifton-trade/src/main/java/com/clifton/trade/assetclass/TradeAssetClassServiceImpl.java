package com.clifton.trade.assetclass;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>TradeAssetClassServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class TradeAssetClassServiceImpl implements TradeAssetClassService {

	private AdvancedUpdatableDAO<TradeAssetClassPositionAllocation, Criteria> tradeAssetClassPositionAllocationDAO;


	@Override
	public TradeAssetClassPositionAllocation getTradeAssetClassPositionAllocation(int id) {
		return getTradeAssetClassPositionAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeAssetClassPositionAllocation> getTradeAssetClassPositionAllocationList(final TradeAssetClassPositionAllocationSearchForm searchForm) {
		return getTradeAssetClassPositionAllocationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				TradeAssetClassPositionAllocationSearchForm searchForm = (TradeAssetClassPositionAllocationSearchForm) getSortableSearchForm();

				if (searchForm.getClientInvestmentAccountGroupId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "group");
					sub.createAlias("referenceTwo", "acct");
					sub.add(Restrictions.eqProperty("acct.id", getPathAlias("accountAssetClass.account", criteria) + ".id"));
					sub.add(Restrictions.eq("group.id", searchForm.getClientInvestmentAccountGroupId()));
					criteria.add(Subqueries.exists(sub));
				}
			}
		});
	}


	@Override
	public TradeAssetClassPositionAllocation saveTradeAssetClassPositionAllocation(TradeAssetClassPositionAllocation bean) {
		return getTradeAssetClassPositionAllocationDAO().save(bean);
	}


	@Override
	public void saveTradeAssetClassPositionAllocationList(List<TradeAssetClassPositionAllocation> beanList) {
		getTradeAssetClassPositionAllocationDAO().saveList(beanList);
	}


	@Override
	public void deleteTradeAssetClassPositionAllocation(int id) {
		getTradeAssetClassPositionAllocationDAO().delete(id);
	}


	@Override
	public void deleteTradeAssetClassPositionAllocationList(List<TradeAssetClassPositionAllocation> beanList) {
		getTradeAssetClassPositionAllocationDAO().deleteList(beanList);
	}


	///////////////////////////////////////////////////////
	///////       Getter and Setter Methods         ///////
	///////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeAssetClassPositionAllocation, Criteria> getTradeAssetClassPositionAllocationDAO() {
		return this.tradeAssetClassPositionAllocationDAO;
	}


	public void setTradeAssetClassPositionAllocationDAO(AdvancedUpdatableDAO<TradeAssetClassPositionAllocation, Criteria> tradeAssetClassPositionAllocationDAO) {
		this.tradeAssetClassPositionAllocationDAO = tradeAssetClassPositionAllocationDAO;
	}
}
