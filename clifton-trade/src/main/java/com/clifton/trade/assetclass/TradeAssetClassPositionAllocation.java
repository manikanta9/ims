package com.clifton.trade.assetclass;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroupType.GroupTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>TradeAssetClassPositionAllocation</code> class
 * defines information for Trades (or "virtual" trades) and the asset classes the trade is allocated to
 * for contract splitting.
 * <p/>
 * For Portfolio runs, this table is used to allocate pending and current contracts on the trade creation screen.
 * <p/>
 * These entries are then used to update/manage {@link InvestmentAccountAssetClassPositionAllocation} table which is used
 * to split the actual contracts on the follow day's run.
 *
 * @author manderson
 */
public class TradeAssetClassPositionAllocation extends BaseEntity<Integer> {

	private Trade trade;

	/**
	 * For cases where we end up with a virtual trade - i.e. Buy 3, sell 3 of the same security,
	 * we still store the asset class information in this table with trade date so we know how to update the {@link InvestmentAccountAssetClassPositionAllocation} table
	 */
	private Date tradeDate;

	private InvestmentSecurity security;

	private InvestmentAccountAssetClass accountAssetClass;

	/**
	 * Used for contract splitting within the same asset class
	 */
	private InvestmentReplication replication;

	/**
	 * How much of the Trade quantity should be allocated to this asset class/replication
	 */
	private BigDecimal allocationQuantity;

	/**
	 * Once the entry is processed for quantity changes to {@link InvestmentAccountAssetClassPositionAllocation}
	 * this flag is set.  This is used for live contract splitting on trade creation so we know which entries we need
	 * to look at.
	 */
	private boolean processed;


	///////////////////////////////////////////////////////


	public String getTradeLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getAccountAssetClass() != null) {
			sb.append(getAccountAssetClass().getAccount().getNumber());
			sb.append(": ");
		}
		if (getTrade() != null) {
			sb.append(getTrade().getLabelShort());
		}
		else {
			sb.append("Virtual Trade");
			if (getSecurity() != null) {
				sb.append(" of ");
				sb.append(getSecurity().getSymbol());
			}
			if (getTradeDate() != null) {
				sb.append(" on ");
				sb.append(DateUtils.fromDateShort(getTradeDate()));
			}
		}
		return sb.toString();
	}


	/**
	 * Pending = Trade exists and not booked yet
	 * OR Virtual Trade and Not Processed
	 */
	public BigDecimal getPendingQuantity() {
		if (isPending()) {
			return getAllocationQuantity();
		}
		return null;
	}


	/**
	 * Actual = Trade exists and booked
	 * OR Virtual Trade and Processed
	 */
	public BigDecimal getActualQuantity() {
		if (isActual()) {
			return getAllocationQuantity();
		}
		return null;
	}


	public BigDecimal getAllocationQuantityAdjusted() {
		if (isTradeCanceled()) {
			return BigDecimal.ZERO;
		}
		return getAllocationQuantity();
	}


	public boolean isPending() {
		if (!isTradeCanceled()) {
			if (getTrade() != null) {
				if (!TradeService.TRADES_CLOSED_STATUS_NAME.equals(getTrade().getWorkflowStatus().getName())) {
					return true;
				}
			}
			else if (!isProcessed()) {
				return true;
			}
		}
		return false;
	}


	public boolean isActual() {
		if (!isTradeCanceled()) {
			if (getTrade() != null) {
				if (TradeService.TRADES_CLOSED_STATUS_NAME.equals(getTrade().getWorkflowStatus().getName())) {
					return true;
				}
			}
			else if (isProcessed()) {
				return true;
			}
		}
		return false;
	}


	public boolean isTradeCanceled() {
		if (getTrade() != null) {
			return TradeService.TRADES_CANCELLED_STATE_NAME.equals(getTrade().getWorkflowState().getName());
		}
		return false;
	}


	public boolean isRollFromTrade() {
		if (isRollTrade()) {
			return getSecurity().equals(getTrade().getTradeGroup().getInvestmentSecurity());
		}
		return false;
	}


	public boolean isRollToTrade() {
		if (isRollTrade()) {
			return getSecurity().equals(getTrade().getTradeGroup().getSecondaryInvestmentSecurity());
		}
		return false;
	}


	public boolean isRollTrade() {
		if (getTrade() != null && getTrade().getTradeGroup() != null && GroupTypes.ROLL == getTrade().getTradeGroup().getTradeGroupType().getGroupType()) {
			return true;
		}
		return false;
	}


	///////////////////////////////////////////////////////


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}


	public InvestmentReplication getReplication() {
		return this.replication;
	}


	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}


	public BigDecimal getAllocationQuantity() {
		return this.allocationQuantity;
	}


	public void setAllocationQuantity(BigDecimal allocationQuantity) {
		this.allocationQuantity = allocationQuantity;
	}


	public boolean isProcessed() {
		return this.processed;
	}


	public void setProcessed(boolean processed) {
		this.processed = processed;
	}
}
