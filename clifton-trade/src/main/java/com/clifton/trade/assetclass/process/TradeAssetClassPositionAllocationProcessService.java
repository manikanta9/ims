package com.clifton.trade.assetclass.process;


import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;


/**
 * The <code>TradeAssetClassPositionAllocationProcessService</code> ...
 *
 * @author manderson
 */
public interface TradeAssetClassPositionAllocationProcessService {

	/**
	 * Processes the {@link TradeAssetClassPositionAllocation} list (where not already processed)
	 * for updated into {@link InvestmentAccountAssetClassPositionAllocation} lists
	 *
	 * @param clientAccountId - Ability to process for a specific account, although most cases would just process all
	 * @param date            - Leave blank to default to previous business day - most common, but for back dating for tests or QA trading would like to be able to back date
	 *                        If date >= today, will also go back to previous business day
	 */
	@ModelAttribute("result")
	public String processTradeAssetClassPositionAllocationList(Integer clientAccountId, Date date);
}
