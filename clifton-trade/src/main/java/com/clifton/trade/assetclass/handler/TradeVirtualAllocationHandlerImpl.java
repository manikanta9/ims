package com.clifton.trade.assetclass.handler;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassSecurityAllowance;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassSearchForm;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeVirtualAllocation;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author NickK
 */
@Component
public class TradeVirtualAllocationHandlerImpl implements TradeVirtualAllocationHandler {

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;


	private TradeGroupService tradeGroupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(timeout = 180)
	public TradeGroup processTradeGroupVirtualTradeList(TradeGroup tradeGroup, Date balanceDate) {
		List<Trade> tradeList = tradeGroup.getTradeList();
		Map<String, Trade> tradeMap = new HashMap<>();
		List<TradeAssetClassPositionAllocation> tradeAssetClassPositionAllocationList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(tradeList)) {
			tradeList.forEach(trade -> {
				if (trade != null) {
					BigDecimal tradeQuantity = (trade.isBuy() ? trade.getQuantityIntended() : MathUtils.negate(trade.getQuantityIntended()));
					String uId = getUniqueIdentifierForTrade(trade);

					Trade saveTrade = tradeMap.get(uId);
					if (saveTrade == null) {
						if (trade instanceof TradeVirtualAllocation) {
							// Copy the trade so it can be saved
							saveTrade = new Trade();
							BeanUtils.copyProperties(trade, saveTrade);
							tradeMap.put(uId, saveTrade);
						}
						else {
							tradeMap.put(uId, trade);
						}
					}
					else {
						if (saveTrade.isBuy() != trade.isBuy()) {
							BigDecimal finalQuantity = MathUtils.add(saveTrade.isBuy() ? saveTrade.getQuantityIntended() : MathUtils.negate(saveTrade.getQuantityIntended()), tradeQuantity);
							if (MathUtils.isNullOrZero(finalQuantity)) {
								// No trade
								tradeMap.remove(uId);
							}
							else {
								// if the quantity changes side, set the buy/sell of the saveTrade to that of the trade making the side change
								if (saveTrade.isBuy() != MathUtils.isGreaterThan(finalQuantity, BigDecimal.ZERO)) {
									saveTrade.setBuy(trade.isBuy());
									saveTrade.setOpenCloseType(
											InvestmentUtils.isSecurityOfType(saveTrade.getInvestmentSecurity(), InvestmentType.OPTIONS)
													? trade.getOpenCloseType() // rely on the Option replication trade with highest quantity
													: null); // let it default for non-Options
								}
								saveTrade.setQuantityIntended(MathUtils.abs(finalQuantity));
								tradeMap.put(uId, saveTrade);
							}
						}
						else {
							saveTrade.setQuantityIntended(MathUtils.add(saveTrade.getQuantityIntended(), trade.getQuantityIntended()));
							tradeMap.put(uId, saveTrade);
						}
					}
					TradeAssetClassPositionAllocation tradeAssetClass = populateTradeAssetClassPositionAllocationForTrade(trade, tradeQuantity);
					if (tradeAssetClass != null) {
						tradeAssetClassPositionAllocationList.add(tradeAssetClass);
					}
				}
			});
		}

		// validate and prepare allocations to save
		List<TradeAssetClassPositionAllocation> tradeAssetClassPositionAllocationListToSave = processTradeAssetClassPositionAllocationList(tradeAssetClassPositionAllocationList, tradeMap, balanceDate);

		// Update the tradeGroup's trade list to the netted trades and apply not for allocations
		tradeGroup.setTradeList(new ArrayList<>(tradeMap.values()));
		if (!CollectionUtils.isEmpty(tradeAssetClassPositionAllocationListToSave)) {
			String joinedClientAccountNumbersForDisplay = tradeAssetClassPositionAllocationListToSave.stream()
					.map(allocation -> allocation.getAccountAssetClass().getClientInvestmentAccount().getNumber())
					.distinct().collect(Collectors.joining(","));
			String note = (StringUtils.isEmpty(tradeGroup.getNote()) ? "" : tradeGroup.getNote() + " - ")
					+ "Trade asset class allocations created for client account" + ((tradeAssetClassPositionAllocationListToSave.size() > 1) ? "s [" : " [")
					+ joinedClientAccountNumbersForDisplay + "]";
			tradeGroup.setNote(note);
		}

		// Save the trade group, and also the trade allocations
		// Called in same method so that the Validate Trade transition is disabled until allocations are saved
		TradeGroup savedTradeGroup = getTradeGroupService().saveTradeGroupWithTradeAllocations(tradeGroup, tradeAssetClassPositionAllocationListToSave);

		// Return null if the trade group was not saved.
		return (savedTradeGroup != null && CollectionUtils.isEmpty(savedTradeGroup.getTradeList())) ? null : savedTradeGroup;
	}


	private String getUniqueIdentifierForTrade(Trade trade) {
		return trade.getInvestmentSecurity().getId() + "_" + trade.getClientInvestmentAccount().getId() + "_" + (trade.getHoldingInvestmentAccount() != null ? trade.getHoldingInvestmentAccount().getId() : "NA") + "_" + trade.isFixTrade() + "_"
				+ (trade.getExecutingBrokerCompany() != null ? trade.getExecutingBrokerCompany().getId() : "NA");
	}


	/**
	 * If the provided trade is a {@link TradeVirtualAllocation}, a {@link TradeAssetClassPositionAllocation}
	 * is created for it. Null is returned otherwise.
	 */
	private TradeAssetClassPositionAllocation populateTradeAssetClassPositionAllocationForTrade(Trade trade, BigDecimal quantity) {
		if (trade instanceof TradeVirtualAllocation) {
			TradeVirtualAllocation tradeAllocation = (TradeVirtualAllocation) trade;
			if (tradeAllocation.getReplication() != null && tradeAllocation.getAccountAssetClass() != null) {
				InvestmentAccountAssetClass investmentAccountAssetClass = tradeAllocation.getAccountAssetClass();
				if (investmentAccountAssetClass.isNewBean()) {
					InvestmentAccountAssetClassSearchForm assetClassSearchForm = new InvestmentAccountAssetClassSearchForm();
					assetClassSearchForm.setAccountId(tradeAllocation.getClientInvestmentAccount().getId());
					assetClassSearchForm.setAssetClassNameEquals(tradeAllocation.getAccountAssetClass().getAssetClass().getName());
					assetClassSearchForm.setInactive(Boolean.FALSE);
					investmentAccountAssetClass = CollectionUtils.getOnlyElement(getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassList(assetClassSearchForm));

					if (investmentAccountAssetClass == null) {
						throw new ValidationException("Unable to find an Account Asset Class for Client Account " + tradeAllocation.getClientInvestmentAccount() + " and Name [" + tradeAllocation.getAccountAssetClass().getAssetClass().getName() + "].");
					}
				}

				InvestmentReplication investmentReplication = tradeAllocation.getReplication();
				if (investmentReplication.isNewBean()) {
					List<InvestmentReplication> accountAssetClassReplicationList = getInvestmentAccountAssetClassService().getInvestmentReplicationListForAccountAssetClass(investmentAccountAssetClass.getId());
					for (InvestmentReplication replication : accountAssetClassReplicationList) {
						if (replication.getName().equals(investmentReplication.getName())) {
							investmentReplication = replication;
							break;
						}
					}
					if (investmentReplication.isNewBean()) {
						throw new ValidationException("Unable to find a Replication with Name [" + tradeAllocation.getReplication().getName() + "] applicable for Asset Class [" + investmentAccountAssetClass.getAssetClass().getName() + "].");
					}
				}

				TradeAssetClassPositionAllocation alloc = new TradeAssetClassPositionAllocation();
				alloc.setAccountAssetClass(investmentAccountAssetClass);
				alloc.setReplication(investmentReplication);
				alloc.setSecurity(trade.getInvestmentSecurity());
				alloc.setTrade(trade);
				alloc.setTradeDate(trade.getTradeDate());
				alloc.setAllocationQuantity(quantity);
				return alloc;
			}
		}
		return null;
	}


	/**
	 * If the provided {@link TradeAssetClassPositionAllocation} list is not empty, the allocations are grouped by client account
	 * to validate the allocations based on the client's configured asset classes and replications. Each valid trade asset class
	 * allocation will be populated with an associated trade from the provided map of trades.
	 *
	 * @param tradeAssetClassPositionAllocationList the allocation list to process
	 * @param tradeMap                              the map of available trades to assign to an applicable allocation
	 * @return a list of valid trade asset class allocations to save
	 */
	private List<TradeAssetClassPositionAllocation> processTradeAssetClassPositionAllocationList(List<TradeAssetClassPositionAllocation> tradeAssetClassPositionAllocationList, Map<String, Trade> tradeMap, Date balanceDate) {
		if (CollectionUtils.isEmpty(tradeAssetClassPositionAllocationList)) {
			return Collections.emptyList();
		}

		// Split up allocations by client accounts so we can validate by each account
		Map<InvestmentAccount, List<TradeAssetClassPositionAllocation>> clientAccountAllocationListMap = BeanUtils.getBeansMap(
				tradeAssetClassPositionAllocationList, allocation -> allocation.getAccountAssetClass().getClientInvestmentAccount());
		List<TradeAssetClassPositionAllocation> allocationsToSave = new ArrayList<>();
		clientAccountAllocationListMap.forEach((clientAccount, clientAllocationList) -> {
			List<TradeAssetClassPositionAllocation> savedAllocationList = processTradeAssetClassPositionAllocationListForClient(clientAccount, clientAllocationList, tradeMap, balanceDate);
			if (!savedAllocationList.isEmpty()) {
				allocationsToSave.addAll(savedAllocationList);
			}
		});
		return allocationsToSave;
	}


	/**
	 * If the provided {@link TradeAssetClassPositionAllocation} list for a specific client account is not empty, the allocations
	 * are validated against the client's configured asset classes and replications. Each valid trade asset class allocation will
	 * be populated with an associated trade from the provided map of trades.
	 *
	 * @param clientAccount             the client account of the trade allocations
	 * @param clientTradeAllocationList the allocation list to process
	 * @param tradeMap                  the map of available trades to assign to an applicable allocation
	 * @return a list of valid trade asset class allocations to save
	 */
	private List<TradeAssetClassPositionAllocation> processTradeAssetClassPositionAllocationListForClient(InvestmentAccount clientAccount, List<TradeAssetClassPositionAllocation> clientTradeAllocationList, Map<String, Trade> tradeMap, Date balanceDate) {
		if (CollectionUtils.isEmpty(clientTradeAllocationList)) {
			return Collections.emptyList();
		}

		List<TradeAssetClassPositionAllocation> saveList = new ArrayList<>();
		// Add records where the same security shows up multiple times
		Set<Integer> securityIdCheckedList = new HashSet<>();

		for (int i = 0; i < clientTradeAllocationList.size(); i++) {
			TradeAssetClassPositionAllocation bean = clientTradeAllocationList.get(i);
			if (!securityIdCheckedList.add(bean.getSecurity().getId())) {
				continue;
			}
			// get asset class security allowance for client account and security for validating allocation assignments
			InvestmentAccountAssetClassSecurityAllowance assetClassSecurityAllowance = getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassSecurityAllowance(
					clientAccount.getId(), bean.getSecurity().getId(), balanceDate);

			// Add the current allocation.
			if (isTradeAssetClassPositionAllocationValidForSave(bean, assetClassSecurityAllowance, tradeMap)) {
				saveList.add(bean);
			}
			// Add any other allocations for the same security.
			for (int j = i + 1; j < clientTradeAllocationList.size(); j++) {
				// When we find the same security, we add it to the list, but only if there is a quantity assigned
				TradeAssetClassPositionAllocation bean2 = clientTradeAllocationList.get(j);
				if (bean.getSecurity().equals(bean2.getSecurity())) {
					if (isTradeAssetClassPositionAllocationValidForSave(bean2, assetClassSecurityAllowance, tradeMap)) {
						saveList.add(bean2);
					}
				}
			}
		}

		return saveList;
	}


	/**
	 * Returns true if the {@link TradeAssetClassPositionAllocation}
	 * <br/>-has an allocation quantity not equal to null or 0,
	 * <br/>-the defined {@link InvestmentAccountAssetClass} and {@link InvestmentReplication} are valid for the client account and security,
	 * <br/>-and the {@link InvestmentAccountAssetClass} has a split security.
	 * <p>
	 * If the allocation is valid and does not have a defined {@link Trade}, the trade will be set with the applicable trade from the provided trade map.
	 */
	private boolean isTradeAssetClassPositionAllocationValidForSave(TradeAssetClassPositionAllocation tradeAssetClassPositionAllocation, InvestmentAccountAssetClassSecurityAllowance assetClassSecurityAllowance, Map<String, Trade> tradeMap) {
		if (!MathUtils.isNullOrZero(tradeAssetClassPositionAllocation.getAllocationQuantity())
				&& assetClassSecurityAllowance.isValidSelection(tradeAssetClassPositionAllocation.getAccountAssetClass(), tradeAssetClassPositionAllocation.getReplication())
				&& assetClassSecurityAllowance.isSecuritySplit(tradeAssetClassPositionAllocation.getAccountAssetClass())) {
			if (tradeAssetClassPositionAllocation.getTrade() != null) {
				tradeAssetClassPositionAllocation.setTrade(tradeMap.get(getUniqueIdentifierForTrade(tradeAssetClassPositionAllocation.getTrade())));
			}
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}
}
