package com.clifton.trade.assetclass;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.trade.Trade;


/**
 * <code>TradeVirtualAllocation</code> represents an unsaved virtual {@link Trade} containing the
 * {@link InvestmentAccountAssetClass} and {@link InvestmentReplication} the trade should be allocated to.
 * <p>
 * This trade is used by trade uploads and portfolio run trade creation to assist in handling virtual
 * contracts and creating {@link TradeAssetClassPositionAllocation}s.
 *
 * @author NickK
 */
@NonPersistentObject
public class TradeVirtualAllocation extends Trade {

	private InvestmentAccountAssetClass accountAssetClass;
	private InvestmentReplication replication;


	public InvestmentAccountAssetClass getAccountAssetClass() {
		return this.accountAssetClass;
	}


	public void setAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
	}


	public InvestmentReplication getReplication() {
		return this.replication;
	}


	public void setReplication(InvestmentReplication replication) {
		this.replication = replication;
	}
}
