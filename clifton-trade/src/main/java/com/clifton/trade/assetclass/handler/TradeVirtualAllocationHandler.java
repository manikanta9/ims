package com.clifton.trade.assetclass.handler;

import com.clifton.trade.Trade;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.group.TradeGroup;

import java.util.Date;


/**
 * <code>TradeVirtualAllocationHandler</code> processes a list of {@link Trade}s by netting virtual
 * trade contracts and creating {@link TradeAssetClassPositionAllocation}s
 *
 * @author NickK
 */
public interface TradeVirtualAllocationHandler {

	/**
	 * Processes the list of {@link Trade}s of the provided {@link TradeGroup} by netting the trade quantity
	 * for similar trades and generating any potential {@link TradeAssetClassPositionAllocation}s.
	 * <p>
	 * The tradeGroup provided should not have been previously saved because the trade list may change as
	 * virtual trades are processed. The trade group, trades, and any trade asset class allocations will be
	 * saved in the process due to the relationship dependencies of the objects (e.g. allocation references
	 * a trade, which must exist).
	 *
	 * @param tradeGroup  the trade group containing the list of virtual trades to process
	 * @param balanceDate for security asset class allowances - if submitting from a specific run the balance date of the run is passed in, otherwise null (trade uploads) and it uses most current date (i.e. previous weekday)
	 * @return The resulting saved trade group or null if the group was not saved due to lack of trades. One or
	 * more TradeAssetClassPositionAllocations may have been saved on a null response.
	 */
	public TradeGroup processTradeGroupVirtualTradeList(TradeGroup tradeGroup, Date balanceDate);
}
