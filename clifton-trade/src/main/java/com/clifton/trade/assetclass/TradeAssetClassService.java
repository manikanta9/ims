package com.clifton.trade.assetclass;


import java.util.List;


/**
 * The <code>TradeAssetClassService</code> ...
 *
 * @author manderson
 */
public interface TradeAssetClassService {

	public TradeAssetClassPositionAllocation getTradeAssetClassPositionAllocation(int id);


	public List<TradeAssetClassPositionAllocation> getTradeAssetClassPositionAllocationList(TradeAssetClassPositionAllocationSearchForm searchForm);


	public TradeAssetClassPositionAllocation saveTradeAssetClassPositionAllocation(TradeAssetClassPositionAllocation bean);


	public void saveTradeAssetClassPositionAllocationList(List<TradeAssetClassPositionAllocation> beanList);


	public void deleteTradeAssetClassPositionAllocation(int id);


	public void deleteTradeAssetClassPositionAllocationList(List<TradeAssetClassPositionAllocation> beanList);
}
