package com.clifton.trade.assetclass.process;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationSearchForm;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationService;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocationSearchForm;
import com.clifton.trade.assetclass.TradeAssetClassService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>TradeAssetClassPositionAllocationProcessServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class TradeAssetClassPositionAllocationProcessServiceImpl implements TradeAssetClassPositionAllocationProcessService {

	private TradeAssetClassService tradeAssetClassService;
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;


	/**
	 * Processes the {@link TradeAssetClassPositionAllocation} list (where not already processed)
	 * for updated into {@link InvestmentAccountAssetClassPositionAllocation} lists
	 *
	 * @param date - Leave blank to default to previous weekday - most common, but for back dating for tests or QA trading would like to be able to back date
	 *             If date >= today, will also go back to previous week day
	 */
	@Override
	@Transactional
	public String processTradeAssetClassPositionAllocationList(Integer clientAccountId, Date date) {
		if (date == null || DateUtils.compare(date, new Date(), false) >= 0) {
			date = DateUtils.getPreviousWeekday(new Date());
		}
		List<TradeAssetClassPositionAllocation> tradeAssetClassList = getUnprocessedTradeAssetClassPositionAllocationList(clientAccountId, date);
		Map<String, InvestmentAccountAssetClassPositionAllocation> positionAllocationMap = getInvestmentAccountAssetClassPositionAllocationMap(clientAccountId, date);
		// Because of validation, we need to control the order of saves - so updates first, then new so that there are no validation issues
		// For example, if there is a change, we end the old and start the new...if the new one is saved first it throws a validation exception
		// because of an overlap in dates with existing allocation.
		List<InvestmentAccountAssetClassPositionAllocation> updatePositionAllocationList = new ArrayList<>();
		List<InvestmentAccountAssetClassPositionAllocation> newPositionAllocationList = new ArrayList<>();

		int updateCount = 0;

		for (Map.Entry<String, InvestmentAccountAssetClassPositionAllocation> stringInvestmentAccountAssetClassPositionAllocationEntry : positionAllocationMap.entrySet()) {
			InvestmentAccountAssetClassPositionAllocation posAllocation = stringInvestmentAccountAssetClassPositionAllocationEntry.getValue();

			List<TradeAssetClassPositionAllocation> matchingAllocations = BeanUtils.filter(tradeAssetClassList, TradeAssetClassPositionAllocation::getAccountAssetClass, posAllocation.getAccountAssetClass());
			matchingAllocations = BeanUtils.filter(matchingAllocations, TradeAssetClassPositionAllocation::getSecurity, posAllocation.getSecurity());
			if (posAllocation.getReplication() != null) {
				matchingAllocations = BeanUtils.filter(matchingAllocations, TradeAssetClassPositionAllocation::getReplication, posAllocation.getReplication());
			}

			if (!CollectionUtils.isEmpty(matchingAllocations)) {
				BigDecimal quantityChange = BigDecimal.ZERO;
				Integer newSecurityId = null;
				boolean rollFrom = false;

				for (TradeAssetClassPositionAllocation tradeAllocation : matchingAllocations) {
					quantityChange = MathUtils.add(quantityChange, tradeAllocation.getAllocationQuantityAdjusted());

					// Check if it is a roll - if it is, see if new security is already in the existing position allocations
					// if it is - don't set newSecurityId so we don't process it again
					// if it's not, we set new security id so we know to add those allocations
					if (tradeAllocation.isRollFromTrade() && !tradeAllocation.isTradeCanceled()) {
						rollFrom = true;
						newSecurityId = tradeAllocation.getTrade().getTradeGroup().getSecondaryInvestmentSecurity().getId();
						if (positionAllocationMap.containsKey(generateKeyForPositionAllocation(posAllocation.getAccountAssetClass().getId(), newSecurityId,
								posAllocation.getReplication() != null ? posAllocation.getReplication().getId() : null))) {
							newSecurityId = null;
						}
					}
				}

				if (!MathUtils.isNullOrZero(quantityChange)) {
					updateCount++;
					BigDecimal newQuantity = MathUtils.add(posAllocation.getAllocationQuantity(), quantityChange);
					// Don't end the old one if it started on the same date - just update quantity
					if (DateUtils.compare(posAllocation.getStartDate(), date, false) == 0) {
						posAllocation.setAllocationQuantity(newQuantity);
						updatePositionAllocationList.add(posAllocation);
					}
					else {
						// If new quantity is not zero (or it's not from a roll), end the old and start the new one
						// So, if it used to be explicit 1, trade allocations now make it explicit at zero - we want to keep that 0 going forward
						// The only exception is when rolling out of a contract
						if (!MathUtils.isNullOrZero(newQuantity) || !rollFrom) {
							InvestmentAccountAssetClassPositionAllocation newAllocation = BeanUtils.cloneBean(posAllocation, false, false);
							newAllocation.setId(null);
							newAllocation.setStartDate(date);
							newAllocation.setAllocationQuantity(newQuantity);
							newPositionAllocationList.add(newAllocation);
						}
						// Otherwise just end it
						posAllocation.setEndDate(DateUtils.addDays(date, -1));
						updatePositionAllocationList.add(posAllocation);
					}
				}

				if (newSecurityId != null) {
					matchingAllocations = BeanUtils.filter(tradeAssetClassList, TradeAssetClassPositionAllocation::getAccountAssetClass, posAllocation.getAccountAssetClass());
					matchingAllocations = BeanUtils.filter(matchingAllocations, tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getSecurity().getId(), newSecurityId);
					if (posAllocation.getReplication() != null) {
						matchingAllocations = BeanUtils.filter(matchingAllocations, TradeAssetClassPositionAllocation::getReplication, posAllocation.getReplication());
					}
					if (!CollectionUtils.isEmpty(matchingAllocations)) {
						BigDecimal newQuantity = CoreMathUtils.sumProperty(matchingAllocations, TradeAssetClassPositionAllocation::getAllocationQuantityAdjusted);
						// If new quantity is not zero, end the old and start the new one
						// Unless the original quantity was zero.  In this case a security may be been explicitly set to 0 so the other asset class always gets the rest
						// in this case we want to keep that 0 quantity
						if (!MathUtils.isNullOrZero(newQuantity) || MathUtils.isNullOrZero(posAllocation.getAllocationQuantity())) {
							updateCount++;
							InvestmentAccountAssetClassPositionAllocation newAllocation = BeanUtils.cloneBean(posAllocation, false, false);
							newAllocation.setId(null);
							newAllocation.setStartDate(date);
							newAllocation.setEndDate(null);
							newAllocation.setSecurity(matchingAllocations.get(0).getSecurity());
							newAllocation.setAllocationQuantity(newQuantity);
							newPositionAllocationList.add(newAllocation);
						}
					}
				}
			}
		}

		saveAssetClassPositionAllocationChanges(tradeAssetClassList, updatePositionAllocationList, newPositionAllocationList);
		return "Processing Complete.  Processed [" + positionAllocationMap.size() + "] explicit position allocations and [" + CollectionUtils.getSize(tradeAssetClassList)
				+ "] trade asset class allocations, which resulted in [" + updateCount + "] position allocation updates.";
	}


	//////////////////////////////////////////////////////////////
	/////////                Helper Methods              /////////
	//////////////////////////////////////////////////////////////


	private void saveAssetClassPositionAllocationChanges(List<TradeAssetClassPositionAllocation> tradeAssetClassList,
	                                                     List<InvestmentAccountAssetClassPositionAllocation> updatePositionAllocationList, List<InvestmentAccountAssetClassPositionAllocation> newPositionAllocationList) {
		for (TradeAssetClassPositionAllocation tradeAlloc : CollectionUtils.getIterable(tradeAssetClassList)) {
			tradeAlloc.setProcessed(true);
		}

		getTradeAssetClassService().saveTradeAssetClassPositionAllocationList(tradeAssetClassList);
		getInvestmentAccountAssetClassPositionAllocationService().saveInvestmentAccountAssetClassPositionAllocationList(updatePositionAllocationList);
		getInvestmentAccountAssetClassPositionAllocationService().saveInvestmentAccountAssetClassPositionAllocationList(newPositionAllocationList);
	}


	private List<TradeAssetClassPositionAllocation> getUnprocessedTradeAssetClassPositionAllocationList(Integer clientAccountId, Date date) {
		TradeAssetClassPositionAllocationSearchForm searchForm = new TradeAssetClassPositionAllocationSearchForm();
		searchForm.setAccountId(clientAccountId);
		searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, date));
		searchForm.setProcessed(false);
		return getTradeAssetClassService().getTradeAssetClassPositionAllocationList(searchForm);
	}


	private Map<String, InvestmentAccountAssetClassPositionAllocation> getInvestmentAccountAssetClassPositionAllocationMap(Integer clientAccountId, Date date) {
		InvestmentAccountAssetClassPositionAllocationSearchForm searchForm = new InvestmentAccountAssetClassPositionAllocationSearchForm();
		searchForm.setAccountId(clientAccountId);
		searchForm.setActiveOnDate(date);
		// Only update it if it is latest - if it already has an end date - ignore the allocation for updates.
		searchForm.setEndDate(null);
		List<InvestmentAccountAssetClassPositionAllocation> list = getInvestmentAccountAssetClassPositionAllocationService().getInvestmentAccountAssetClassPositionAllocationList(searchForm);
		Map<String, InvestmentAccountAssetClassPositionAllocation> map = new HashMap<>();
		for (InvestmentAccountAssetClassPositionAllocation posAlloc : CollectionUtils.getIterable(list)) {
			map.put(getKeyForInvestmentAccountAssetClassPositionAllocation(posAlloc), posAlloc);
		}
		return map;
	}


	private String getKeyForInvestmentAccountAssetClassPositionAllocation(InvestmentAccountAssetClassPositionAllocation posAlloc) {
		return generateKeyForPositionAllocation(posAlloc.getAccountAssetClass().getId(), posAlloc.getSecurity().getId(), (posAlloc.getReplication() != null ? posAlloc.getReplication().getId() : null));
	}


	private String generateKeyForPositionAllocation(Integer accountAssetClassId, Integer securityId, Integer replicationId) {
		StringBuilder sb = new StringBuilder(16);
		sb.append(accountAssetClassId);
		sb.append("_");
		sb.append(securityId);
		if (replicationId != null) {
			sb.append("_");
			sb.append(replicationId);
		}
		return sb.toString();
	}


	////////////////////////////////////////////////////////
	///////       Getter and Setter Methods        /////////
	////////////////////////////////////////////////////////


	public TradeAssetClassService getTradeAssetClassService() {
		return this.tradeAssetClassService;
	}


	public void setTradeAssetClassService(TradeAssetClassService tradeAssetClassService) {
		this.tradeAssetClassService = tradeAssetClassService;
	}


	public InvestmentAccountAssetClassPositionAllocationService getInvestmentAccountAssetClassPositionAllocationService() {
		return this.investmentAccountAssetClassPositionAllocationService;
	}


	public void setInvestmentAccountAssetClassPositionAllocationService(InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService) {
		this.investmentAccountAssetClassPositionAllocationService = investmentAccountAssetClassPositionAllocationService;
	}
}
