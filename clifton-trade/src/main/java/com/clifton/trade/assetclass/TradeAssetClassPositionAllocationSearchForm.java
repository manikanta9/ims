package com.clifton.trade.assetclass;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>TradeAssetClassPositionAllocationSearchForm</code> ...
 *
 * @author manderson
 */
public class TradeAssetClassPositionAllocationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "trade.id")
	private Integer tradeId;

	/**
	 * Used to exclude Cancelled trades
	 */
	@SearchField(searchFieldPath = "trade.workflowState", searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS_OR_IS_NULL, leftJoin = true)
	private String excludeTradeWorkflowStateName;

	@SearchField(searchFieldPath = "trade", searchField = "tradeGroup.id")
	private Integer tradeGroupId;

	@SearchField(searchFieldPath = "trade", searchField = "tradeGroup.id")
	private Integer[] tradeGroupIds;

	@SearchField
	private Boolean processed;

	@SearchField(searchField = "accountAssetClass.id")
	private Integer accountAssetClassId;

	@SearchField(searchField = "replication.id")
	private Integer replicationId;

	@SearchField(searchFieldPath = "accountAssetClass", searchField = "account.id")
	private Integer accountId;

	@SearchField(searchField = "security.id")
	private Integer securityId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "security.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchFieldPath = "accountAssetClass.assetClass", searchField = "name")
	private String assetClassName;

	@SearchField(searchFieldPath = "replication", searchField = "name")
	private String replicationName;

	@SearchField(searchFieldPath = "security", searchField = "symbol,name", sortField = "symbol")
	private String securityLabel;

	@SearchField
	private Date tradeDate;

	@SearchField
	private BigDecimal allocationQuantity;

	@SearchField(searchField = "trade.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean virtualTradesOnly;

	// Custom Search Filters
	private Integer clientInvestmentAccountGroupId;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public Integer getAccountAssetClassId() {
		return this.accountAssetClassId;
	}


	public void setAccountAssetClassId(Integer accountAssetClassId) {
		this.accountAssetClassId = accountAssetClassId;
	}


	public Integer getReplicationId() {
		return this.replicationId;
	}


	public void setReplicationId(Integer replicationId) {
		this.replicationId = replicationId;
	}


	public Integer getAccountId() {
		return this.accountId;
	}


	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public String getAssetClassName() {
		return this.assetClassName;
	}


	public void setAssetClassName(String assetClassName) {
		this.assetClassName = assetClassName;
	}


	public String getReplicationName() {
		return this.replicationName;
	}


	public void setReplicationName(String replicationName) {
		this.replicationName = replicationName;
	}


	public String getSecurityLabel() {
		return this.securityLabel;
	}


	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public BigDecimal getAllocationQuantity() {
		return this.allocationQuantity;
	}


	public void setAllocationQuantity(BigDecimal allocationQuantity) {
		this.allocationQuantity = allocationQuantity;
	}


	public Boolean getProcessed() {
		return this.processed;
	}


	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}


	public String getExcludeTradeWorkflowStateName() {
		return this.excludeTradeWorkflowStateName;
	}


	public void setExcludeTradeWorkflowStateName(String excludeTradeWorkflowStateName) {
		this.excludeTradeWorkflowStateName = excludeTradeWorkflowStateName;
	}


	public Integer getTradeGroupId() {
		return this.tradeGroupId;
	}


	public void setTradeGroupId(Integer tradeGroupId) {
		this.tradeGroupId = tradeGroupId;
	}


	public Integer[] getTradeGroupIds() {
		return this.tradeGroupIds;
	}


	public void setTradeGroupIds(Integer[] tradeGroupIds) {
		this.tradeGroupIds = tradeGroupIds;
	}


	public Boolean getVirtualTradesOnly() {
		return this.virtualTradesOnly;
	}


	public void setVirtualTradesOnly(Boolean virtualTradesOnly) {
		this.virtualTradesOnly = virtualTradesOnly;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}
}
