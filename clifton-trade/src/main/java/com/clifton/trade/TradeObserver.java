package com.clifton.trade;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingObjectInfo;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.trade.accounting.commission.TradeCommissionOverride;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.util.TradeUtilHandler;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;


/**
 * The <code>TradeObserver</code> class is MODIFY events observer for Trade DAO.
 * <p>
 * If Workflow Transition occurs and Trade is part of a Group - check Group Workflow and update if necessary
 *
 * @author Mary Anderson
 */
@Component
public class TradeObserver extends SelfRegisteringDaoObserver<Trade> {

	// system defined system condition to customize the field validations
	private static final String TRADE_FIELD_MODIFICATION_CONDITION = "Trade Field Modification Condition";

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private TradeCommissionService tradeCommissionService;
	private TradeGroupService tradeGroupService;
	private TradeService tradeService;
	private TradeUtilHandler tradeUtilHandler;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private AccountingJournalService accountingJournalService;
	private AccountingTransactionService accountingTransactionService;
	private SystemConditionService systemConditionService;


	public TradeObserver() {
		super();
		// Setting order to below default so this Observer runs before those at default.
		setOrder(-100);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<Trade> dao, DaoEventTypes event, Trade trade) {

		// top-level modification condition.
		SystemCondition tradeFieldModifyCondition = getSystemConditionService().getSystemConditionByName(TRADE_FIELD_MODIFICATION_CONDITION);
		if (tradeFieldModifyCondition != null) {
			EvaluationResult tradeFieldModifyResults = getSystemConditionEvaluationHandler().evaluateCondition(tradeFieldModifyCondition, trade);
			if (tradeFieldModifyResults.isFalse()) {
				throw new ValidationException(tradeFieldModifyResults.getMessage());
			}
		}

		// First, check modify condition on the new bean
		if (trade.getTradeDestination().getTradeModifySystemCondition() != null) {
			EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(trade.getTradeDestination().getTradeModifySystemCondition(), trade);
			if (result.isFalse()) {
				throw new AccessDeniedException("User does not have permission to " + event + " [" + trade.getLabel() + "] because " + result.getMessage());
			}
		}

		Lazy<Trade> originalTrade = new Lazy<>(() -> getOriginalBean(dao, trade));
		if (event.isInsert() || event.isUpdate()) {
			if (MathUtils.isNullOrZero(trade.getExchangeRateToBase())) {
				populateExchangeRate(trade);
			}
			else if (event.isUpdate() && !originalTrade.get().getInvestmentSecurity().equals(trade.getInvestmentSecurity())) {
				populateExchangeRate(trade);
			}
			getTradeUtilHandler().validateTrade(trade);
		}
		// If it's not a new bean, check modify condition on original bean
		if (!event.isInsert()) {
			if (originalTrade.get() != null) {
				TradeDestination originalDestination = originalTrade.get().getTradeDestination();
				if (originalDestination.getTradeModifySystemCondition() != null) {
					EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(originalDestination.getTradeModifySystemCondition(), originalTrade.get());
					if (result.isFalse()) {
						throw new AccessDeniedException("User does not have permission to " + event + " [" + trade.getLabel() + "] because " + result.getMessage());
					}
				}
			}
		}
	}


	@Override
	public void configurePersistedInstance(Trade persistedInstance, Trade originalInstance) {
		if (persistedInstance != null) {
			/*
			 * If an exception is thrown during update, the persistedInstance will be null. Ignore
			 * setting enableCommissionOverride to avoid getting a NPE.
			 */
			persistedInstance.setEnableCommissionOverride(originalInstance.isEnableCommissionOverride());

			// Set the lazy bag of fills if they are not on the persisted instance (post save) and the original has them.
			// This allows 'after' workflow transition actions to have access to the fills without having to look them up again.
			if (CollectionUtils.isEmpty(persistedInstance.getTradeFillList()) && !CollectionUtils.isEmpty(originalInstance.getTradeFillList())
					&& originalInstance.getTradeFillList().stream().noneMatch(BaseSimpleEntity::isNewBean)) {
				persistedInstance.setTradeFillList(originalInstance.getTradeFillList());
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<Trade> dao, DaoEventTypes event, Trade bean, Throwable e) {
		if (e == null) {
			Trade original = null;
			if (event.isUpdate()) {
				original = getOriginalBean(dao, bean);
			}

			// validate the average price
			validateAveragePrice(bean, original);

			if (original != null && !DateUtils.isEqual(bean.getActualSettlementDate(), original.getActualSettlementDate())) {
				ValidationUtils.assertFalse(isTradeFullyBooked(bean), "Actual Settlement Date cannot be updated when the trade is fully posted (including cash).");
			}

			createOrUpdateTradeCommissionOverrides(bean, original);

			if (isTradeGroupWorkflowRequired(bean)) {
				// If an insert, or change in workflow state
				if (original == null || !original.getWorkflowState().equals(bean.getWorkflowState())) {
					syncTradeWorkflowStateWithTradeGroup(bean);
				}
			}
		}
	}


	/**
	 * Calculated the average price of the fills and compare it to average price on the trade if it changed.
	 */
	private void validateAveragePrice(Trade trade, Trade original) {
		// validate the average price
		if ((original != null) && MathUtils.isNotEqual(trade.getAverageUnitPrice(), original.getAverageUnitPrice())) {
			// validate average price
			List<TradeFill> existingFills = trade.getTradeFillList();
			if (existingFills == null) {
				existingFills = getTradeService().getTradeFillListByTrade(trade.getId());
			}
			if (!CollectionUtils.isEmpty(existingFills)) {
				BigDecimal averagePrice = calculateAveragePrice(existingFills);
				ValidationUtils.assertTrue(MathUtils.isEqual(averagePrice, trade.getAverageUnitPrice()), "Trade average unit price of [" + CoreMathUtils.formatNumberDecimal(trade.getAverageUnitPrice())
						+ "] does not equal the average price of the fills [" + CoreMathUtils.formatNumberDecimal(averagePrice) + "]");
			}
		}
	}


	/**
	 * Determine if a trade's fills are fully booked and posted, including realized cash.
	 * Assumption: booking and posting of all trade fills is atomic.
	 */
	private boolean isTradeFullyBooked(Trade trade) {
		List<TradeFill> existingFills = trade.getTradeFillList();
		if (existingFills == null) {
			existingFills = getTradeService().getTradeFillListByTrade(trade.getId());
		}
		if (CollectionUtils.isEmpty(existingFills)) {
			return false;
		}
		if (CollectionUtils.getStream(existingFills).map(TradeFill::getBookingDate).anyMatch(Objects::isNull)) {
			return false;
		}
		List<AccountingJournal> journalList = getTradeJournalList(existingFills);
		if (journalList.stream().map(AccountingJournal::getPostingDate).anyMatch(Objects::isNull)) {
			return false;
		}
		if (hasReceivables(journalList)) {
			return false;
		}

		return true;
	}


	private List<AccountingJournal> getTradeJournalList(List<TradeFill> tradeFillList) {
		AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
		searchForm.setSystemTableName(TradeFill.TABLE_NAME);
		searchForm.setFkFieldIdList(CollectionUtils.getStream(tradeFillList).map(TradeFill::getId).toArray(Integer[]::new));
		searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
		return CollectionUtils.asNonNullList(getAccountingJournalService().getAccountingJournalList(searchForm));
	}


	private boolean hasReceivables(List<AccountingJournal> journalList) {
		return journalList.stream()
				.flatMap(j -> CollectionUtils.getStream(getAccountingTransactionService().getAccountingTransactionListByJournal(j.getId())))
				.map(AccountingObjectInfo::getAccountingAccount)
				.anyMatch(AccountingAccount::isReceivable);
	}


	private BigDecimal calculateAveragePrice(List<TradeFill> fillList) {
		BigDecimal sumShares = BigDecimal.ZERO;
		BigDecimal totalCost = BigDecimal.ZERO;
		for (TradeFill fill : CollectionUtils.getIterable(fillList)) {
			sumShares = sumShares.add(fill.getQuantity());
			totalCost = totalCost.add(MathUtils.multiply(fill.getNotionalUnitPrice(), fill.getQuantity()));
		}
		return MathUtils.divide(totalCost, sumShares, DataTypes.PRICE.getPrecision());
	}


	/**
	 * If there is a commission or fee set on the Trade itself, and there is already an existing {@link TradeCommissionOverride} with the same
	 * {@link AccountingAccount} set as the trade's commission or fee account, then that override is updated, otherwise a new override is created.
	 * In other words, this ensures that the override on the Trade table and any associated {@link TradeCommissionOverride}s stay in sync.
	 *
	 * @param bean     the Trade being created or updated
	 * @param original the original Trade, if there is one
	 */
	private void createOrUpdateTradeCommissionOverrides(Trade bean, Trade original) {
		if (original == null || bean.isEnableCommissionOverride()) {
			/*
			 * Create/Update the commission override if the trade is new or 'enableCommissionOverride' is set.
			 * The 'enableCommissionOverride' is an indicator that any value changes to the commission/fee can be considered an override (because the change originated from the UI).
			 * Need to check for original being null because not all new trades will come from the UI (e.g. trade import).
			 */
			validateAndUpdateTradeCommissionOverride(bean, original);
			validateAndUpdateTradeFeeOverride(bean, original);

			//Reset commission override flag to false.
			bean.setEnableCommissionOverride(false);
		}
	}


	private void validateAndUpdateTradeCommissionOverride(Trade bean, Trade original) {
		AccountingAccount commissionAccount = bean.getTradeType().getCommissionAccountingAccount();
		BigDecimal commissionPerUnit = bean.getCommissionPerUnit();
		BigDecimal commissionAmount = bean.getCommissionAmount();

		BigDecimal originalCommissionPerUnit;
		BigDecimal originalCommissionAmount;
		if (original == null) {
			originalCommissionPerUnit = null;
			originalCommissionAmount = null;
		}
		else {
			originalCommissionPerUnit = original.getCommissionPerUnit();
			originalCommissionAmount = original.getCommissionAmount();
		}

		if (MathUtils.isNotEqual(commissionPerUnit, originalCommissionPerUnit)) {
			if (commissionAmount != null) {
				/*
				 * The commission override is updated to per unit, so clear the flat commission amount.
				 * Updating here avoids another pass through here saving the commission override.
				 */
				bean.setCommissionAmount(null);
			}
			validateAndUpdateTradeCommissionOverride(bean, commissionAccount, commissionAmount, commissionPerUnit);
		}
		else if (MathUtils.isNotEqual(commissionAmount, originalCommissionAmount)) {
			if (commissionPerUnit != null) {
				/*
				 *The commission override is updated to flat commission amount, so clear the per unit amount.
				 * Updating here avoids another pass through here saving the commission override.
				 */
				bean.setCommissionPerUnit(null);
			}
			validateAndUpdateTradeCommissionOverride(bean, commissionAccount, commissionAmount, commissionPerUnit);
		}
	}


	private void validateAndUpdateTradeFeeOverride(Trade bean, Trade original) {
		AccountingAccount feeAccount = bean.getTradeType().getFeeAccountingAccount();
		BigDecimal feeAmount = bean.getFeeAmount();
		BigDecimal originalFeeAmount = original != null ? original.getFeeAmount() : null;
		if (MathUtils.isNotEqual(feeAmount, originalFeeAmount)) {
			validateAndUpdateTradeCommissionOverride(bean, feeAccount, feeAmount, null);
		}
	}


	private void validateAndUpdateTradeCommissionOverride(Trade trade, AccountingAccount accountingAccount, BigDecimal totalAmount, BigDecimal amountPerUnit) {
		TradeCommissionOverride existingOverride = getTradeCommissionOverrideForTradeAndAccountingAccount(trade, accountingAccount);

		if (existingOverride != null) {
			if (totalAmount == null && amountPerUnit == null) {
				getTradeCommissionService().deleteTradeCommissionOverride(existingOverride.getId());
			}
			else {
				updateTradeCommissionOverride(existingOverride, totalAmount, amountPerUnit);
			}
		}
		else {
			createTradeCommissionOverride(trade, accountingAccount, totalAmount, amountPerUnit);
		}
	}


	private void createTradeCommissionOverride(Trade trade, AccountingAccount accountingAccount, BigDecimal totalAmount, BigDecimal amountPerUnit) {
		if (trade != null && accountingAccount != null && (totalAmount != null || amountPerUnit != null)) {
			TradeCommissionOverride commissionOverride = new TradeCommissionOverride();
			commissionOverride.setTrade(trade);
			commissionOverride.setAccountingAccount(accountingAccount);

			//Ensure only per unit or total is set, not both.
			if (amountPerUnit != null) {
				commissionOverride.setAmountPerUnit(amountPerUnit);
			}
			else {
				commissionOverride.setTotalAmount(totalAmount);
			}

			getTradeCommissionService().saveTradeCommissionOverride(commissionOverride);
		}
	}


	private TradeCommissionOverride getTradeCommissionOverrideForTradeAndAccountingAccount(Trade trade, AccountingAccount accountingAccount) {
		if (trade != null && accountingAccount != null) {
			return getTradeCommissionService().getTradeCommissionOverrideForTradeAndAccountingAccount(trade, accountingAccount);
		}
		return null;
	}


	private void updateTradeCommissionOverride(TradeCommissionOverride override, BigDecimal totalAmount, BigDecimal amountPerUnit) {
		if (override != null) {
			BigDecimal existingTotalAmount = override.getTotalAmount();
			BigDecimal existingAmountPerUnit = override.getAmountPerUnit();

			//Only update the existing TradeCommissionOverride if the fee amount(s) changed.
			if (MathUtils.isNotEqual(existingTotalAmount, totalAmount) || MathUtils.isNotEqual(existingAmountPerUnit, amountPerUnit)) {
				//Ensure only per unit or total is set, not both.
				if (amountPerUnit != null) {
					override.setAmountPerUnit(amountPerUnit);
					override.setTotalAmount(null);
				}
				else {
					override.setAmountPerUnit(null);
					override.setTotalAmount(totalAmount);
				}
				getTradeCommissionService().saveTradeCommissionOverride(override);
			}
		}
	}


	private boolean isTradeGroupWorkflowRequired(Trade trade) {
		if (trade == null) {
			return false;
		}
		TradeGroup group = trade.getTradeGroup();
		if (group == null) {
			return false;
		}
		TradeGroupType type = group.getTradeGroupType();
		if (type == null) {
			return false;
		}
		return type.isWorkflowRequired();
	}


	private void syncTradeWorkflowStateWithTradeGroup(Trade bean) {
		// If new workflow state is different than group's state, reset group to the min
		if (bean.getWorkflowState().getOrder() != null
				&& (bean.getTradeGroup().getWorkflowState() == null || MathUtils.isNotEqual(bean.getTradeGroup().getWorkflowState().getOrder(), bean.getWorkflowState().getOrder()))) {
			TradeGroup group = getTradeGroupService().getTradeGroup(bean.getTradeGroup().getId());
			// Get a Trade with the Min Workflow State Order, if after the Group Workflow, update the Group
			Trade minTrade = CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(group.getTradeList(), trade -> trade.getWorkflowState().getOrder(), true));
			if (minTrade != null && (group.getWorkflowState() == null || MathUtils.isNotEqual(minTrade.getWorkflowState().getOrder(), group.getWorkflowState().getOrder()))) {
				updateTradeGroupWorkflowState(group, minTrade.getWorkflowState());
			}
		}
	}


	private void updateTradeGroupWorkflowState(TradeGroup group, WorkflowState state) {
		group.setWorkflowState(state);
		group.setWorkflowStatus(state.getStatus());
		group = getTradeGroupService().saveTradeGroupWithoutTradeUpdates(group);
		TradeGroup parent = group.getParent();
		if (parent != null) {
			parent = getTradeGroupService().getTradeGroup(parent.getId());
			if (CollectionUtils.isEmpty(parent.getTradeList())) {
				updateTradeGroupWorkflowState(parent, state);
			}
		}
	}


	private void populateExchangeRate(Trade trade) {
		String baseCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(trade);
		String localCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(trade);
		InvestmentSecurity settlementCurrency = trade.getSettlementCurrency();
		ValidationUtils.assertNotNull(settlementCurrency, "Trade Settlement Currency is missing. It is usually the same as currency denomination for security.");
		ValidationUtils.assertNotNull(localCurrency, "Unable to lookup an exchange rate. Currency Denomination for security is not defined.");
		ValidationUtils.assertNotNull(baseCurrency, "Unable to lookup an exchange rate. Trade Client Account [" + trade.getClientInvestmentAccount().getLabel() + "] is missing a base currency");
		if (localCurrency.equals(baseCurrency)) {
			trade.setExchangeRateToBase(BigDecimal.ONE);
		}
		else if (trade.getInvestmentSecurity().isCurrency() && !trade.isFixTrade()) {
			throw new FieldValidationException("Exchange Rate must be populated for non-electronically executed currency trades.", "exchangeRate");
		}
		else {
			// Lookup latest FX rate from clearing company. Currency securities must always use the dominate currency's
			// exchange rate in order for the defined currency conventions to work properly.
			BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(
					FxRateLookupCommand.forFxSource(trade.fxSourceCompany(), !trade.fxSourceCompanyOverridden(), localCurrency, baseCurrency, trade.getTradeDate())
							.flexibleLookup().dominantCurrency(trade.getTradeType().isCurrencyConventionFollowed())
			);
			trade.setExchangeRateToBase(fxRate);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public TradeUtilHandler getTradeUtilHandler() {
		return this.tradeUtilHandler;
	}


	public void setTradeUtilHandler(TradeUtilHandler tradeUtilHandler) {
		this.tradeUtilHandler = tradeUtilHandler;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}
}
