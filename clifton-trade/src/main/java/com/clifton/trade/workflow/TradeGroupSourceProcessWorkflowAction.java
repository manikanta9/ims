package com.clifton.trade.workflow;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>TradeGroupSourceProcessWorkflowAction</code> performs for a trade
 * associated with a group that has a source table and fkField id any automatic transitions for that entity.
 * <p/>
 * Example.  On Trade Approvals for Trades that are associated with a run - Once all of those trades are approved
 * the run is also approved.
 *
 * @author manderson
 */
public class TradeGroupSourceProcessWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private DaoLocator daoLocator;

	private WorkflowTransitionService workflowTransitionService;


	@Override
	public Trade processAction(Trade bean, WorkflowTransition transition) {
		if (bean != null && bean.getTradeGroup() != null) {
			TradeGroup group = bean.getTradeGroup();
			if (group.getTradeGroupType().getSourceSystemTable() != null && group.getSourceFkFieldId() != null) {
				final String sourceTableName = group.getTradeGroupType().getSourceSystemTable().getName();
				final Integer sourceFkFieldId = group.getSourceFkFieldId();

				UpdatableDAO<IdentityObject> dao = (UpdatableDAO<IdentityObject>) getDaoLocator().locate(sourceTableName);
				IdentityObject daoBean = dao.findByPrimaryKey(sourceFkFieldId);
				if (daoBean instanceof WorkflowAware) {
					if (getWorkflowTransitionService().getWorkflowTransitionNext((WorkflowAware) daoBean, new StringBuilder()) != null) {
						// If a next auto transition is available, save the bean so that the transition is applied.
						dao.save(daoBean);
					}
				}
			}
		}
		return bean;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
