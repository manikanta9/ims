package com.clifton.trade.workflow;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.util.TradeFillUtilHandler;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>TradeFillWorkflowAction</code> class is a workflow transition action that creates TradeFill
 * objects for the trade.
 * <p>
 * Note: it only creates TradeFill(s) if they don't already exist and average price is populated.
 *
 * @author vgomelsky
 */
public class TradeFillWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private TradeService tradeService;
	private TradeFillUtilHandler tradeFillUtilHandler;

	private boolean singleFillTradeOnly;
	private boolean allowUpdates;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade processAction(Trade bean, WorkflowTransition transition) {
		List<TradeFill> tradeFillList = getTradeService().getTradeFillListByTrade(bean.getId());
		if (!CompareUtils.isEqual(bean.getTradeFillList(), tradeFillList)) {
			bean.setTradeFillList(tradeFillList);
		}
		if (!isSingleFillTradeOnly() || bean.getTradeType().isSingleFillTrade()) {
			if (CollectionUtils.isEmpty(bean.getTradeFillList())) {
				createTradeFill(bean);
			}
			else if (isAllowUpdates()) {
				if (CollectionUtils.getSize(bean.getTradeFillList()) == 1) {
					bean.getTradeFillList().stream().findFirst().ifPresent(fill -> {
						if (fill.getTrade() == null) {
							fill.setTrade(bean);
						}
						/*
						 * The fill will be validated and updated if necessary by
						 * the TradeService and TradeFillUtilHandler.
						 */
						getTradeService().saveTradeFill(fill);
					});
				}
				// There is no support for validating multiple fills
			}
		}
		return bean;
	}


	private TradeFill createTradeFill(Trade trade) {
		TradeFill fill = getTradeFillUtilHandler().createTradeFill(trade, false);
		return fill == null ? fill : getTradeService().saveTradeFill(fill);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeFillUtilHandler getTradeFillUtilHandler() {
		return this.tradeFillUtilHandler;
	}


	public void setTradeFillUtilHandler(TradeFillUtilHandler tradeFillUtilHandler) {
		this.tradeFillUtilHandler = tradeFillUtilHandler;
	}


	public boolean isSingleFillTradeOnly() {
		return this.singleFillTradeOnly;
	}


	public void setSingleFillTradeOnly(boolean singleFillTradeOnly) {
		this.singleFillTradeOnly = singleFillTradeOnly;
	}


	public boolean isAllowUpdates() {
		return this.allowUpdates;
	}


	public void setAllowUpdates(boolean allowUpdates) {
		this.allowUpdates = allowUpdates;
	}
}
