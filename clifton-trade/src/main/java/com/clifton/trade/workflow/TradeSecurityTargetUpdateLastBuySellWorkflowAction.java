package com.clifton.trade.workflow;

import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.List;


/**
 * <code>TradeSecurityTargetUpdateLastBuySellWorkflowAction</code> is a {@link com.clifton.workflow.transition.WorkflowTransitionAction}
 * applicable to booking and unbooking transitions for updating the LastBuyTradeDate/LastSellTradeDate on {@link InvestmentAccountSecurityTarget} entries.
 * <p>
 * This action is executed only if the booked or unbooked trade is for a client account using a business service processing type matching the defined criteria.
 *
 * @author DavidI
 */
public class TradeSecurityTargetUpdateLastBuySellWorkflowAction extends BaseTradeSecurityTargetWorkflowAction {

	@Override
	protected void processBookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget) {
		// Update only if applicable security target trade date is null or if the trade date is greater than the security target's applicable trade date (buy or sell)
		if (trade.isBuy() && (securityTarget.getLastBuyTradeDate() == null || DateUtils.isDateAfter(trade.getTradeDate(), securityTarget.getLastBuyTradeDate())) ||
				!trade.isBuy() && (securityTarget.getLastSellTradeDate() == null || DateUtils.isDateAfter(trade.getTradeDate(), securityTarget.getLastSellTradeDate()))) {
			setLastBuyOrSellTradeDateAndSave(trade, securityTarget);
		}
	}


	@Override
	protected void processUnbookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget) {
		// use the most recent trade for updating security target.  Prevents use of historical trade dates when security target has more recent trade dates set.
		Trade latestBookedTrade = getLatestBookedTrade(trade);

		// if no most recent trade, make a proxy trade with null trade date to pass to setLastBuyOrSellTradeDateAndSave(...)
		if (latestBookedTrade == null) {
			latestBookedTrade = new Trade();
			latestBookedTrade.setBuy(trade.isBuy());
		}

		// Update only if applicable trade date on the security target is not equal to the previous trade's trade date
		if (trade.isBuy() && !DateUtils.isEqualWithoutTime(latestBookedTrade.getTradeDate(), securityTarget.getLastBuyTradeDate()) ||
				!trade.isBuy() && !DateUtils.isEqualWithoutTime(latestBookedTrade.getTradeDate(), securityTarget.getLastSellTradeDate())) {
			setLastBuyOrSellTradeDateAndSave(latestBookedTrade, securityTarget);
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Sets the security target's LastBuyTradeDate or LastSellTradeDate properties (depending on whether trade is Buy / Sell) to
	 * the trade's trade date.  Will not perform update if trade date matches the target's LastBuyTradeDate or LastSellTradeDate values.
	 */
	private void setLastBuyOrSellTradeDateAndSave(Trade trade, InvestmentAccountSecurityTarget securityTarget) {
		if (trade.isBuy()) {
			securityTarget.setLastBuyTradeDate(trade.getTradeDate());
		}
		else {
			securityTarget.setLastSellTradeDate(trade.getTradeDate());
		}

		getInvestmentAccountSecurityTargetService().saveInvestmentAccountSecurityTarget(securityTarget);
	}


	/**
	 * A function that returns the latest booked trade for this security target (that is similar to the current trade).
	 *
	 * @param currentTrade trade that is currently being processed.
	 * @return latest booked trade that is similar to this trade (e.g. same underlyingSecurity, same clientAccount, same investmentType, same buy flag).
	 * returns null if no matching previous trade was found.
	 */
	private Trade getLatestBookedTrade(Trade currentTrade) {
		TradeSearchForm searchForm = new TradeSearchForm();

		searchForm.setExcludeId(currentTrade.getId());
		searchForm.setClientInvestmentAccountId(currentTrade.getClientInvestmentAccount().getId());
		searchForm.setBuy(currentTrade.isBuy());
		searchForm.setWorkflowStateName(TradeService.TRADE_BOOKED_STATE_NAME);
		searchForm.setInvestmentTypeId(currentTrade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getId());
		searchForm.setUnderlyingSecurityId(currentTrade.getInvestmentSecurity().getUnderlyingSecurity().getId());
		searchForm.setLimit(1);
		searchForm.setOrderBy("tradeDate:desc");

		// Most recent trade will be first due to order by on SearchForm
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);

		return !tradeList.isEmpty() ? tradeList.get(0) : null;
	}
}
