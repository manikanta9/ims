package com.clifton.trade.workflow;

import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * <code>BaseTradeSecurityTargetWorkflowAction</code> is an abstract base class to facilitate implementations of subclasses of {@link com.clifton.workflow.transition.WorkflowTransitionAction}
 * that operate on or depend on {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget}.
 *
 * @author DavidI
 */
public abstract class BaseTradeSecurityTargetWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;
	private TradeService tradeService;

	/**
	 * If true a trade is being booked, if false, a trade is being unbooked.
	 */
	private boolean booking;

	/**
	 * If this list is populated, the processing is limited to client accounts with specific processing types {@link com.clifton.business.service.BusinessServiceProcessingType}, otherwise processing applies to any
	 * client accounts with a {@link com.clifton.business.service.BusinessServiceProcessingType} that has a processingType of SECURITY_TARGET.
	 */
	private List<Short> businessServiceProcessingTypeIds;


	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Trade processAction(Trade trade, WorkflowTransition transition) {
		if (isTradeApplicable(trade, transition)) {
			InvestmentAccountSecurityTarget securityTarget = getInvestmentAccountSecurityTargetForTrade(trade);
			if (securityTarget != null) {
				if (isBooking()) {
					// is booking, validate Booked state
					if (TradeService.TRADE_BOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
						processBookingAction(trade, securityTarget);
					}
				}
				// is not booking, validate Unbooked state
				else if (TradeService.TRADE_UNBOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
					processUnbookingAction(trade, securityTarget);
				}
			}
		}
		return trade;
	}


	protected boolean isTradeApplicable(Trade trade, WorkflowTransition transition) {
		return isTradeApplicableToBusinessServiceProcessingType(trade) && isTradeApplicableToInvestmentType(trade);
	}


	protected abstract void processBookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget);


	protected abstract void processUnbookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget);


	/**
	 * Determines if the client account {@link com.clifton.investment.account.InvestmentAccount} associated with the trade has a {@link com.clifton.business.service.BusinessServiceProcessingType} that is configured for
	 * use in this {@link com.clifton.workflow.transition.WorkflowTransitionAction}.
	 *
	 * @return true if client account's BusinessServiceProcessingType is supported, false if not.
	 */
	protected boolean isTradeApplicableToBusinessServiceProcessingType(Trade trade) {
		boolean result = false;
		if (trade.getClientInvestmentAccount().getServiceProcessingType() != null) {
			if (!CollectionUtils.isEmpty(getBusinessServiceProcessingTypeIds())) {
				result = getBusinessServiceProcessingTypeIds().contains(trade.getClientInvestmentAccount().getServiceProcessingType().getId());
			}
			else {
				result = ServiceProcessingTypes.SECURITY_TARGET == trade.getClientInvestmentAccount().getServiceProcessingType().getProcessingType();
			}
		}
		return result;
	}


	/**
	 * Determines if the trade's {@link com.clifton.investment.instrument.InvestmentSecurity} is an Option.
	 *
	 * @return true if investmentSecurity is an option, false if not.
	 */
	protected boolean isTradeApplicableToInvestmentType(Trade trade) {
		return InvestmentUtils.isOption(trade.getInvestmentSecurity());
	}


	/**
	 * Returns the {@link InvestmentAccountSecurityTarget} associated with the trade's client account for the trade's trade date.
	 */
	protected InvestmentAccountSecurityTarget getInvestmentAccountSecurityTargetForTrade(Trade trade) {
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(), trade.getInvestmentSecurity().getUnderlyingSecurity().getId(), trade.getTradeDate());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public boolean isBooking() {
		return this.booking;
	}


	public void setBooking(boolean booking) {
		this.booking = booking;
	}


	public List<Short> getBusinessServiceProcessingTypeIds() {
		return this.businessServiceProcessingTypeIds;
	}


	public void setBusinessServiceProcessingTypeIds(List<Short> businessServiceProcessingTypeIds) {
		this.businessServiceProcessingTypeIds = businessServiceProcessingTypeIds;
	}
}








