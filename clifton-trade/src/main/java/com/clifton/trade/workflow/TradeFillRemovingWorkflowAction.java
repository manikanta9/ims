package com.clifton.trade.workflow;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>TradeFillRemovingWorkflowAction</code> class is a workflow action that deletes the only trade fill
 * that a "singleFillTrade" has.  If trade type supports multiple fills then nothing is done.
 * <p/>
 * The action is necessary because some trade types (swaps) don't really have a concept of fills and may not have
 * UI to delete the fill.
 *
 * @author vgomelsky
 */
public class TradeFillRemovingWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private TradeService tradeService;


	@Override
	public Trade processAction(Trade bean, WorkflowTransition transition) {
		// get fully populated trade
		Trade trade = getTradeService().getTrade(bean.getId());
		int fillCount = CollectionUtils.getSize(trade.getTradeFillList());
		if (fillCount > 0 && trade.getTradeType().isSingleFillTrade()) {
			ValidationUtils.assertTrue(fillCount == 1, "Single fill trade cannot have more than 1 fill: " + trade);
			getTradeService().deleteTradeFill(trade.getTradeFillList().get(0).getId());
		}
		return trade;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
