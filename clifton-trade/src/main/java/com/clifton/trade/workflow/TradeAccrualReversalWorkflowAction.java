package com.clifton.trade.workflow;

import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


public class TradeAccrualReversalWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private TradeService tradeService;
	private AccountingBookingService<TradeFill> accountingBookingService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade processAction(Trade trade, WorkflowTransition transition) {
		if (trade.getActualSettlementDate() == null) {
			return trade;
		}

		if (!trade.getInvestmentSecurity().getInstrument().getHierarchy().isIncludeAccrualReceivables()) {
			return trade;
		}
		List<TradeFill> fillList = getTradeService().getTradeFillListByTrade(trade.getId());
		for (TradeFill fill : CollectionUtils.getIterable(fillList)) {
			if (fill.getBookingDate() != null) {
				getAccountingBookingService().bookAccountingJournal(AccountingJournalType.TRADE_JOURNAL, fill, true);
			}
		}
		return trade;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AccountingBookingService<TradeFill> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<TradeFill> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}
}
