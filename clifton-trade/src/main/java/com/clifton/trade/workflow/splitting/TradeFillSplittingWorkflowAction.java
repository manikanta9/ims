package com.clifton.trade.workflow.splitting;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionCommand;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeServiceImpl;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.transition.WorkflowTransition;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * <code>TradeFillSplittingWorkflowAction</code> Splits TradeFills for securities that are allowed to cross zero (trade can create a short position),
 * based on cumulative closing and opening position amounts.
 *
 * @author TerryS
 */
public class TradeFillSplittingWorkflowAction extends BaseTradeFillSplittingWorkflowAction {


	@Override
	public Trade processAction(Trade bean, WorkflowTransition transition) {
		if (isSplittable(bean)) {
			// loads trade fills too - make sure trade and fills are not stale.
			final Trade trade = getTradeService().getTrade(bean.getId());
			// sorting the list makes the splitting process determinant
			List<TradeFill> fillList = CollectionUtils.getStream(trade.getTradeFillList())
					.sorted(Comparator.comparing(TradeFill::getQuantity))
					.collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(fillList)) {
				return doProcessAction(trade, fillList);
			}
			return trade;
		}
		return bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Trade doProcessAction(final Trade trade, final List<TradeFill> fillList) {
		BigDecimal totalOpenPositionsQuantity = getTotalOpenPositionsQuantity(trade);
		for (TradeFill tradeFill : fillList) {
			if (!MathUtils.isNullOrZero(totalOpenPositionsQuantity)) {
				BigDecimal originalQuantityToTrade = trade.isBuy() ? tradeFill.getQuantity() : tradeFill.getQuantity().negate();
				if (!MathUtils.isSameSignum(totalOpenPositionsQuantity, originalQuantityToTrade)) {
					if (tradeFill.getQuantity().abs().compareTo(totalOpenPositionsQuantity.abs()) > 0) {
						// need more than what's available
						BigDecimal openQuantity = originalQuantityToTrade.abs().subtract(totalOpenPositionsQuantity.abs());
						BigDecimal closeQuantity = totalOpenPositionsQuantity.abs();

						// the new fill is the open
						TradeFill openFill = createOpeningPosition(tradeFill, openQuantity);
						// bypass validations
						((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(openFill);
						// not the same list being iterated
						trade.getTradeFillList().add(openFill);

						// the original fill is the close
						tradeFill.setOpenCloseType(getExplicitOpenCloseType(trade.isBuy(), false));
						updateQuantities(tradeFill, closeQuantity);
						// bypass validations
						((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(tradeFill);
						// no need to update the fill list on the trade (session-shared entity)
						// all open position quantities have been used.
						totalOpenPositionsQuantity = BigDecimal.ZERO;
					}
					else {
						// need <= what's available
						tradeFill.setOpenCloseType(getExplicitOpenCloseType(trade.isBuy(), false));
						((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(tradeFill);
						totalOpenPositionsQuantity = MathUtils.reduceAbsValue(totalOpenPositionsQuantity, tradeFill.getQuantity());
					}
				}
			}
			// set non-explicit fills to open
			if (!tradeFill.getOpenCloseType().isExplicitOpenClose()) {
				tradeFill.setOpenCloseType(getExplicitOpenCloseType(tradeFill.getTrade().isBuy(), true));
				((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(tradeFill);
			}
		}
		return trade;
	}


	private BigDecimal getTotalOpenPositionsQuantity(Trade trade) {
		final AccountingAccount positionAccount = trade.isCollateralTrade() ? getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL)
				: getAccountingAccountService().getAccountingAccountByName(AccountingAccount.ASSET_POSITION);
		AccountingBookingPositionCommand command = new AccountingBookingPositionCommand(trade, getPositionOrder(trade), true, trade.getSettlementDate(),
				trade.getTransactionDate(), trade.getClientInvestmentAccount().getId(),
				Optional.ofNullable(trade.getHoldingInvestmentAccount()).map(InvestmentAccount::getId).orElse(null), trade.getInvestmentSecurity().getId(), positionAccount, true);
		List<BookingPosition> openPositions = getAccountingBookingPositionRetriever().getAccountingBookingOpenPositionList(command);
		return openPositions.stream().map(BookingPosition::getRemainingQuantity).reduce(BigDecimal.ZERO, MathUtils::add);
	}


	private TradeFill createOpeningPosition(TradeFill tradeFill, BigDecimal openQuantity) {
		TradeFill openFill = new TradeFill();
		BeanUtils.copyProperties(tradeFill, openFill);
		openFill.setId(null);
		openFill.setOpenCloseType(getExplicitOpenCloseType(tradeFill.getTrade().isBuy(), true));
		updateQuantities(openFill, openQuantity);

		return openFill;
	}


	private AccountingPositionOrders getPositionOrder(Trade trade) {
		AccountingPositionOrders closingOrder = null;
		AccountingClosingMethods closingMethod = getAccountingClosingMethod(trade);
		if (closingMethod != null) {
			closingOrder = AccountingPositionOrders.valueOf(closingMethod.name());
		}
		return closingOrder;
	}


	private AccountingClosingMethods getAccountingClosingMethod(Trade trade) {
		InvestmentAccount clientAccount = trade.getClientInvestmentAccount();
		if (clientAccount != null) {
			return clientAccount.getAccountingClosingMethod();
		}
		return AccountingClosingMethods.FIFO;
	}
}
