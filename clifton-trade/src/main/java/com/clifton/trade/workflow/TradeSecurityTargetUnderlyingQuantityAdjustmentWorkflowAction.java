package com.clifton.trade.workflow;

import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.util.InvestmentAccountSecurityTargetAdjustmentUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * <code>TradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction</code> is a {@link com.clifton.workflow.transition.WorkflowTransitionAction}
 * applicable to booking and unbooking transitions for adjusting the TargetUnderlyingQuantity value on {@link InvestmentAccountSecurityTarget} entries.
 * <p>
 * This action is executed only if the booked or unbooked trade is for a client account using a business service processing type matching the defined criteria.
 *
 * @author DavidI
 */
public class TradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction extends BaseTradeSecurityTargetWorkflowAction {

	private InvestmentAccountSecurityTargetAdjustmentUtilHandler investmentAccountSecurityTargetAdjustmentUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void processBookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget) {
		if (securityTarget != null && trade.getInvestmentSecurity().getId().equals(securityTarget.getTargetUnderlyingSecurity().getId())) {
			BigDecimal signedTradeQuantity = getSignedQuantityForTrade(trade);
			updateAndSaveSecurityTarget(securityTarget, signedTradeQuantity, trade.getAverageUnitPrice());
		}
	}


	@Override
	protected void processUnbookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget) {
		if (securityTarget != null && trade.getInvestmentSecurity().getId().equals(securityTarget.getTargetUnderlyingSecurity().getId())) {
			BigDecimal signedTradeQuantity = getSignedQuantityForTrade(trade);
			// negate since this is for unbooking a trade
			signedTradeQuantity = MathUtils.negate(signedTradeQuantity);
			updateAndSaveSecurityTarget(securityTarget, signedTradeQuantity, trade.getAverageUnitPrice());
		}
	}


	/**
	 * Returns the {@link InvestmentAccountSecurityTarget} associated with the trade's client account. If the security target is no longer active, no adjustments should be made.
	 * Historic corrections via unbooking/booking requires manual security target modifications since we cannot accurately adjust historic entries.
	 */
	@Override
	protected InvestmentAccountSecurityTarget getInvestmentAccountSecurityTargetForTrade(Trade trade) {
		// In this case, the trade's investment security is the InvestmentAccountSecurityTarget's underlying security
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(), trade.getInvestmentSecurity().getId(), trade.getTradeDate());
	}


	@Override
	protected boolean isTradeApplicableToInvestmentType(Trade trade) {
		InvestmentInstrument instrument = trade.getInvestmentSecurity().getInstrument();
		return InvestmentUtils.isInstrumentOfType(instrument, InvestmentType.STOCKS) || InvestmentUtils.isInstrumentOfType(instrument, InvestmentType.FUNDS);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates a signed trade quantity based on whether or not the trade is buy or sell.
	 * For Buy trades, the quantity will be positive, for sell trades returns a negative quantity.
	 *
	 * @param trade the trade to evaluate
	 * @return A signed BigDecimal value indicating the direction of the trade
	 */
	private BigDecimal getSignedQuantityForTrade(Trade trade) {

		// sell
		if (!trade.isBuy()) {
			return MathUtils.negate(trade.getQuantity());
		}

		return trade.getQuantity();
	}


	/**
	 * Adjusts and saves a quantity or notional adjustment (depending on the InvestmentAccountSecurityTarget type) using a signed quantity and average price for
	 * the trade.
	 */
	private void updateAndSaveSecurityTarget(InvestmentAccountSecurityTarget securityTarget, BigDecimal signedQuantity, BigDecimal averageUnitPrice) {
		switch (securityTarget.getTargetType()) {
			case InvestmentAccountSecurityTarget.TARGET_TYPE_NOTIONAL:
				getInvestmentAccountSecurityTargetAdjustmentUtilHandler().adjustInvestmentAccountSecurityTargetNotionalFromQuantity(securityTarget, signedQuantity, averageUnitPrice);
				break;

			case InvestmentAccountSecurityTarget.TARGET_TYPE_QUANTITY:
				getInvestmentAccountSecurityTargetAdjustmentUtilHandler().adjustInvestmentAccountSecurityTargetQuantity(securityTarget, signedQuantity);
				break;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetAdjustmentUtilHandler getInvestmentAccountSecurityTargetAdjustmentUtilHandler() {
		return this.investmentAccountSecurityTargetAdjustmentUtilHandler;
	}


	public void setInvestmentAccountSecurityTargetAdjustmentUtilHandler(InvestmentAccountSecurityTargetAdjustmentUtilHandler investmentAccountSecurityTargetAdjustmentUtilHandler) {
		this.investmentAccountSecurityTargetAdjustmentUtilHandler = investmentAccountSecurityTargetAdjustmentUtilHandler;
	}
}
