package com.clifton.trade.workflow;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreeze;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetFreezeType;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetFreezeSearchForm;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.investment.instrument.ExpiryTimeValues;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.workflow.transition.WorkflowTransition;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;


/**
 * <code>TradeSecurityTargetFreezeWorkflowAction</code> is a {@link com.clifton.workflow.transition.WorkflowTransitionAction}
 * applicable to booking and unbooking transitions for entering and removing {@link InvestmentAccountSecurityTargetFreeze} entries.
 * <p>
 * This action is executed only if the booked or unbooked trade is for a client account using a business service processing type matching the defined criteria.
 *
 * @author NickK
 */
public class TradeSecurityTargetFreezeWorkflowAction extends BaseTradeSecurityTargetWorkflowAction {

	private static final String FREEZE_EARLY_CLOSE_DAYS_COLUMN_NAME = "Freeze Early Close Days";
	private static final String TRADE_TABLE_NAME = "Trade";

	private AccountingJournalService accountingJournalService;
	private InvestmentAccountUtilHandler investmentAccountUtilHandler;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	private SystemSchemaService systemSchemaService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected boolean isTradeApplicable(Trade trade, WorkflowTransition transition) {
		return super.isTradeApplicable(trade, transition) && trade.getOpenCloseType().isClose();
	}


	@Override
	protected void processBookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget) {
		Date freezeExpirationDate = getEarlyCloseDateForTrade(trade);
		ExpiryTimeValues optionExpiry = trade.getInvestmentSecurity().getSettlementExpiryTime();
		Predicate<Date> condition = optionExpiry == ExpiryTimeValues.PM
				? tradeDate -> DateUtils.isDateBeforeOrEqual(tradeDate, freezeExpirationDate, false)
				: tradeDate -> DateUtils.isDateBefore(tradeDate, freezeExpirationDate, false);
		if (condition.test(trade.getTradeDate()) && isTradeClosingForProfit(trade)) {
			// Create a frozen allocation for the trade's quantity and option security when the option's expiration is > 10 days in the future of the trade date
			// and we made a profit. Closing trades of a "Roll Forward" group will include a trade to close an existing position at a loss to avoid further losses,
			// and should not result in a frozen allocation being generated.
			createSecurityTargetFreezeForTrade(securityTarget, trade, freezeExpirationDate);
		}
	}


	@Override
	protected void processUnbookingAction(Trade trade, InvestmentAccountSecurityTarget securityTarget) {
		removeSecurityTargetFreezeForTrade(securityTarget, trade);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private Date getEarlyCloseDateForTrade(Trade trade) {
		Integer freezeEarlyCloseDays = (Integer) getInvestmentAccountUtilHandler().getClientInvestmentAccountPortfolioSetupCustomColumnValue(trade.getClientInvestmentAccount(), FREEZE_EARLY_CLOSE_DAYS_COLUMN_NAME);
		return DateUtils.addDays(trade.getInvestmentSecurity().getFirstDeliveryDate(), freezeEarlyCloseDays * -1);
	}


	private BigDecimal normalizeOptionTradeFreezeQuantity(Trade trade, boolean convertToUnderlying) {
		// The trade is closing, so if the trade is a buy, negate the quantity
		BigDecimal quantity = trade.isBuy() ? trade.getQuantityIntended().negate() : trade.getQuantityIntended();
		return convertToUnderlying ? MathUtils.multiply(quantity, trade.getInvestmentSecurity().getPriceMultiplier()) : quantity;
	}


	private void createSecurityTargetFreezeForTrade(InvestmentAccountSecurityTarget securityTarget, Trade trade, Date freezeExpirationDate) {
		InvestmentAccountSecurityTargetFreezeType freezeType = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetFreezeTypeByName(InvestmentAccountSecurityTargetFreezeType.SECURITY_TARGET_TYPE_EARLY_CLOSE);
		SystemTable tradeTable = getSystemSchemaService().getSystemTableByName(TRADE_TABLE_NAME);

		InvestmentAccountSecurityTargetFreeze freeze = new InvestmentAccountSecurityTargetFreeze();
		freeze.setClientInvestmentAccount(securityTarget.getClientInvestmentAccount());
		freeze.setTargetUnderlyingSecurity(securityTarget.getTargetUnderlyingSecurity());
		freeze.setFreezeType(freezeType);
		freeze.setCauseSystemTable(tradeTable);
		freeze.setCauseFKFieldId(trade.getId());
		freeze.setCauseInvestmentSecurity(trade.getInvestmentSecurity());
		freeze.setFreezeCauseSecurityQuantity(normalizeOptionTradeFreezeQuantity(trade, false));
		freeze.setStartDate(trade.getTradeDate());
		freeze.setEndDate(freezeExpirationDate);
		getInvestmentAccountSecurityTargetService().saveInvestmentAccountSecurityTargetFreeze(freeze);
	}


	private List<InvestmentAccountSecurityTargetFreeze> getSecurityTargetFreezeListForTrade(InvestmentAccountSecurityTarget securityTarget, Trade trade) {
		InvestmentAccountSecurityTargetFreezeType freezeType = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetFreezeTypeByName(InvestmentAccountSecurityTargetFreezeType.SECURITY_TARGET_TYPE_EARLY_CLOSE);
		SystemTable tradeTable = getSystemSchemaService().getSystemTableByName(TRADE_TABLE_NAME);

		InvestmentAccountSecurityTargetFreezeSearchForm freezeSearchForm = new InvestmentAccountSecurityTargetFreezeSearchForm();
		freezeSearchForm.setClientInvestmentAccountId(securityTarget.getClientInvestmentAccount().getId());
		freezeSearchForm.setTargetUnderlyingSecurityId(securityTarget.getTargetUnderlyingSecurity().getId());
		freezeSearchForm.setFreezeTypeId(freezeType.getId());
		freezeSearchForm.setCauseSystemTableId(tradeTable.getId());
		freezeSearchForm.setCauseFKFieldId(trade.getId());
		return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetFreezeList(freezeSearchForm);
	}


	private void removeSecurityTargetFreezeForTrade(InvestmentAccountSecurityTarget securityTarget, Trade trade) {
		List<InvestmentAccountSecurityTargetFreeze> freezeList = getSecurityTargetFreezeListForTrade(securityTarget, trade);
		for (InvestmentAccountSecurityTargetFreeze freeze : CollectionUtils.getIterable(freezeList)) {
			getInvestmentAccountSecurityTargetService().deleteInvestmentAccountSecurityTargetFreeze(freeze.getId());
		}
	}


	private boolean isTradeClosingForProfit(Trade trade) {
		List<TradeFill> tradeFillList = getTradeFillListForTrade(trade);

		BigDecimal closingRevenue = CollectionUtils.getStream(tradeFillList)
				.map(fill -> getAccountingJournalService().getAccountingJournalBySource(TradeFill.TABLE_NAME, fill.getId()))
				.map(AccountingJournal::getJournalDetailList)
				.flatMap(CollectionUtils::getStream)
				.map(AccountingTransaction.class::cast)
				.distinct()
				.filter(transaction -> transaction.getAccountingAccount().isGainLoss())
				.map(AccountingTransaction::getLocalDebitCredit)
				.reduce(BigDecimal.ZERO, MathUtils::add);

		// localDebitCredit is negative if credit and positive if debit; profiting if a credit.
		return MathUtils.isLessThan(closingRevenue, BigDecimal.ZERO);
	}


	private List<TradeFill> getTradeFillListForTrade(Trade trade) {
		if (!CollectionUtils.isEmpty(trade.getTradeFillList())) {
			return trade.getTradeFillList();
		}

		return getTradeService().getTradeFillListByTrade(trade.getId());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public InvestmentAccountUtilHandler getInvestmentAccountUtilHandler() {
		return this.investmentAccountUtilHandler;
	}


	public void setInvestmentAccountUtilHandler(InvestmentAccountUtilHandler investmentAccountUtilHandler) {
		this.investmentAccountUtilHandler = investmentAccountUtilHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
