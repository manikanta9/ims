package com.clifton.trade.workflow.splitting;

import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionRetriever;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.math.BigDecimal;


/**
 * <code>BaseTradeFillSplittingWorkflowAction</code> functionality shared by the trade fill splitter upon booking and
 * merging fills upon unbooking.
 *
 * @author TerryS
 */
public abstract class BaseTradeFillSplittingWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private TradeService tradeService;
	private AccountingBookingPositionRetriever accountingBookingPositionRetriever;
	private AccountingAccountService accountingAccountService;
	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void updateQuantities(TradeFill tradeFill, BigDecimal quantity) {
		tradeFill.setQuantity(quantity);
		tradeFill.setNotionalTotalPrice(getInvestmentCalculator().calculateNotional(tradeFill.getInvestmentSecurity(), tradeFill.getNotionalUnitPrice(), MathUtils.abs(quantity),
				tradeFill.getTrade().getNotionalMultiplier()));
		if (InvestmentUtils.isNoPaymentOnOpen(tradeFill.getInvestmentSecurity())) {
			tradeFill.setPaymentUnitPrice(BigDecimal.ZERO);
			tradeFill.setPaymentTotalPrice(BigDecimal.ZERO);
		}
		else {
			tradeFill.setPaymentUnitPrice(tradeFill.getNotionalUnitPrice());
			tradeFill.setPaymentTotalPrice(tradeFill.getNotionalTotalPrice());
		}
	}


	protected boolean isSplittable(Trade trade) {
		return trade.getTradeType().isCrossingZeroAllowed() && !trade.getTradeType().isSingleFillTrade();
	}


	protected TradeOpenCloseType getExplicitOpenCloseType(boolean buy, boolean open) {
		TradeOpenCloseTypes tradeOpenCloseTypes = TradeOpenCloseTypes.of(buy, open, !open);
		return getTradeService().getTradeOpenCloseTypeByName(tradeOpenCloseTypes.getName());
	}


	protected TradeOpenCloseType getImplicitTradeOpenCloseType(boolean buy) {
		TradeOpenCloseTypes tradeOpenCloseTypes = TradeOpenCloseTypes.of(buy, true, true);
		return getTradeService().getTradeOpenCloseTypeByName(tradeOpenCloseTypes.getName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AccountingBookingPositionRetriever getAccountingBookingPositionRetriever() {
		return this.accountingBookingPositionRetriever;
	}


	public void setAccountingBookingPositionRetriever(AccountingBookingPositionRetriever accountingBookingPositionRetriever) {
		this.accountingBookingPositionRetriever = accountingBookingPositionRetriever;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
