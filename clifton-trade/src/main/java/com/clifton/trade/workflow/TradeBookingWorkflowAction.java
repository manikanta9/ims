package com.clifton.trade.workflow;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeExchangeRateDateSelectors;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeServiceImpl;
import com.clifton.trade.TradeType;
import com.clifton.trade.util.TradeUtilHandler;
import com.clifton.trade.workflow.splitting.TradeFillSplittingWorkflowAction;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>TradeBookingWorkflowAction</code> class is a workflow transition action that books passed trades
 * into accounting system.
 * <p/>
 * Note: the action only books trades that do not have "bookingDate" field set. It only books in addition to workflow transition.
 * If booking process is initiated directly (not via workflow) then this action will not do anything in order to avoid double booking.
 *
 * @author vgomelsky
 */
public class TradeBookingWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private boolean doNotPostWhenBooking;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private AccountingBookingService<TradeFill> accountingBookingService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private TradeService tradeService;
	private TradeUtilHandler tradeUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/*
	 * Note: Since this is executed during workflow transitions, saves within this method may cause a org.hibernate.StaleObjectStateException: Row was updated or deleted by another
	 * transaction (or unsaved-value mapping was incorrect). For this reason, rather than saving the trade within this method, we mutate the trade directly and expect the trade to
	 * be saved by the caller. This workflow action must be a "before" (WorkflowTransitionAction#beforeTransition) action to function correctly.
	 *
	 * It is also possible, though less explicit, for these mutations to be persisted in "after" transition actions that do not use separate transactions, thanks to the Hibernate
	 * dirty-checking strategy which will persist mutated entities on session flush.
	 */
	@Override
	public Trade processAction(Trade trade, WorkflowTransition transition) {
		// get fills if not set on the trade - always get from db to be sure the list of fills is complete
		List<TradeFill> fillList = getTradeService().getTradeFillListByTrade(trade.getId());
		ValidationUtils.assertNotEmpty(fillList, "Cannot book a trade with id = " + trade.getId() + " because it has no fills.");

		// validate the security allows trading
		ValidationUtils.assertFalse(trade.getInvestmentSecurity().getInstrument().getHierarchy().isTradingDisallowed(), "Cannot book a trade with security [" + trade.getInvestmentSecurity().getLabel() + "] because trading is disallowed.");

		// validate the trade type for case when the trade can have an invalid security until after execution
		getTradeUtilHandler().validateTradeTypeForBooking(trade);

		// sort fills so that lowest priced fill is first (all brokers use FIFO when closing)
		fillList.sort((o1, o2) -> CompareUtils.compare(o1.getNotionalUnitPrice(), o2.getNotionalUnitPrice()));

		BigDecimal cumulativeQuantity = BigDecimal.ZERO;
		BigDecimal cumulativeCommissionAmount = BigDecimal.ZERO;
		BigDecimal cumulativeFeeAmount = BigDecimal.ZERO;

		// need to update FX Rate before booking
		populateExchangeRateIfNeeded(trade);

		Map<TradeFill, AccountingJournal> tradeFillToJournalMap = new HashMap<>();

		// book each unbooked fill separately
		for (TradeFill fill : fillList) {
			if (fill.getBookingDate() == null) {
				if (fill.getQuantity() != null) {
					cumulativeQuantity = cumulativeQuantity.add(fill.getQuantity().abs());
				}
				AccountingJournal journal = getAccountingBookingService().bookAccountingJournal(AccountingJournalType.TRADE_JOURNAL, fill, !isDoNotPostWhenBooking());
				tradeFillToJournalMap.put(fill, journal);

				// from the journal that was just created, calculate its commission and fees
				for (AccountingJournalDetailDefinition journalEntry : CollectionUtils.getIterable(journal.getJournalDetailList())) {
					if (journalEntry.getAccountingAccount().isCommission()) {
						//If the journal entry accounting account is a commission and NOT a fee (i.e. it is a commission specific GL account)
						if (!journalEntry.getAccountingAccount().isFee()) {
							cumulativeCommissionAmount = cumulativeCommissionAmount.subtract(journalEntry.getLocalDebitCredit()); // keep commission negative
						}
						//Otherwise, if 'isCommission' is true (but it doesn't match the "commission" AccountingAccount of the trade type), then we consider it a Fee
						else {
							cumulativeFeeAmount = cumulativeFeeAmount.subtract(journalEntry.getLocalDebitCredit()); // keep fee negative
						}
					}
				}
			}
		}

		// update commission and fee fields if necessary
		if (MathUtils.isNotEqual(trade.getFeeAmount(), cumulativeFeeAmount)) {
			trade.setFeeAmount(cumulativeFeeAmount);
		}

		if (MathUtils.isNotEqual(trade.getCommissionAmount(), cumulativeCommissionAmount)) {
			trade.setCommissionAmount(cumulativeCommissionAmount);
		}
		if (!MathUtils.isNullOrZero(cumulativeQuantity)) {
			// negate because our convention is to have negative commission amount
			BigDecimal commissionPerUnit = MathUtils.divide(cumulativeCommissionAmount, cumulativeQuantity, DataTypes.DECIMAL_PRECISE.getPrecision()).negate();
			if (MathUtils.isNotEqual(trade.getCommissionPerUnit(), commissionPerUnit)) {
				trade.setCommissionPerUnit(commissionPerUnit);
			}
		}

		handleTradeOpenClose(trade, tradeFillToJournalMap);
		return trade;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void populateExchangeRateIfNeeded(Trade trade) {
		TradeExchangeRateDateSelectors dateSelector = trade.getTradeType().getExchangeRateDateSelector();
		if (dateSelector != null) {
			// need to update FX on trade: is is always from Local Currency to Base Currency
			String localCurrency = InvestmentUtils.getSecurityCurrencyDenominationSymbol(trade);
			String baseCurrency = InvestmentUtils.getClientAccountBaseCurrencySymbol(trade);
			if (!localCurrency.equals(baseCurrency)) {
				Date fxDate = dateSelector.getExchangeRateDate(trade);
				BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(trade.fxSourceCompany(), !trade.fxSourceCompanyOverridden(), localCurrency, baseCurrency, fxDate).doNotFallbackToDefaultDataSource());
				if (!MathUtils.isEqual(fxRate, trade.getExchangeRateToBase())) {
					trade.setExchangeRateToBase(fxRate);
				}
			}
		}
	}


	/**
	 * Validate the Open/Close type on the trade matches the transactions.  Also, if the openCloseType is Buy or Sell update it with Open/Close if possible.
	 */
	private void handleTradeOpenClose(Trade trade, Map<TradeFill, AccountingJournal> tradeFillToJournalMap) {
		OpenCloseCount openCloseCount = getOpenAndClosingTransactions(tradeFillToJournalMap);
		updateTradeOpenClose(trade, openCloseCount);
		validateTradeOpenClose(trade, openCloseCount);
		updateTradeFillOpenClose(trade.getOpenCloseType(), tradeFillToJournalMap.keySet());
		validateTradeFillOpenClose(tradeFillToJournalMap);
	}


	private void updateTradeFillOpenClose(TradeOpenCloseType tradeOpenCloseType, Set<TradeFill> fillList) {
		// Trade does not cross zero (explicit), update the related trade fills with the open / close flag
		if (tradeOpenCloseType.isExplicitOpenClose()) {
			for (TradeFill tradeFill : fillList) {
				if (tradeFill.getOpenCloseType() != tradeOpenCloseType) {
					tradeFill.setOpenCloseType(tradeOpenCloseType);
					// bypass validations
					((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(tradeFill);
				}
			}
		}
	}


	/**
	 * Validate positions have not changed since the {@link TradeFillSplittingWorkflowAction} set the open/close state.
	 */
	private void validateTradeFillOpenClose(Map<TradeFill, AccountingJournal> tradeFillToJournalMap) {
		for (Map.Entry<TradeFill, AccountingJournal> entry : CollectionUtils.getIterable(tradeFillToJournalMap.entrySet())) {
			if (entry.getKey().getOpenCloseType() != null) {
				OpenCloseCount result = new OpenCloseCount();
				List<? extends AccountingJournalDetailDefinition> journalDetailList = BeanUtils.filter(entry.getValue().getJournalDetailList(), this::isValidPositionTransaction);
				updateOpenCloseCount(result, journalDetailList);
				if (result.isCrossingZero() && isSplittableTrade(entry.getKey().getTrade())) {
					AssertUtils.assertTrue(entry.getKey().getOpenCloseType().isExplicitOpenClose(),
							() -> String.format("The trade fill [%s] on trade [%s] should have an explicit open close but is [%s]",
									entry.getKey().getIdentity(), entry.getKey().getTrade().getIdentity(), entry.getKey().getOpenCloseType()));
				}
			}
			else {
				LogUtils.warn(this.getClass(), String.format("Expected an Open Close Type on the Trade Fill %s", entry.getKey().getIdentity()));
			}
		}
	}


	protected boolean isSplittableTrade(Trade trade) {
		return trade.getTradeType().isCrossingZeroAllowed() && !trade.getTradeType().isSingleFillTrade();
	}


	private void updateTradeOpenClose(Trade trade, OpenCloseCount openCloseCount) {
		TradeOpenCloseType openCloseType = trade.getOpenCloseType();
		if (!openCloseType.isExplicitOpenClose()) {
			if (!openCloseCount.isCrossingZero()) {
				TradeOpenCloseType newOpenCloseType = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.of(trade.isBuy(), openCloseCount.getOpenCount() > 0, openCloseCount.getCloseCount() > 0).getName());
				if (newOpenCloseType != null) {
					trade.setOpenCloseType(newOpenCloseType);
				}
			}
		}
	}


	private void validateTradeOpenClose(Trade trade, OpenCloseCount openCloseCount) {
		validateCrossingZero(trade, openCloseCount);
		validateOpenCloseRequired(trade);
		validateOpenCloseMatchesTransactions(trade, openCloseCount);
	}


	private void validateOpenCloseMatchesTransactions(Trade trade, OpenCloseCount openCloseCount) {
		TradeOpenCloseType openCloseType = trade.getOpenCloseType();
		if (openCloseType.isExplicitOpenClose()) {
			ValidationUtils.assertFalse(openCloseCount.isCrossingZero(), "[" + trade.getOpenCloseType() + "] does not support booking fills that take a position from long to short or vice versa");

			if (trade.getOpenCloseType().isOpen()) {
				ValidationUtils.assertTrue(openCloseCount.getOpenCount() > 0 && openCloseCount.getCloseCount() <= 0, "Trade [" + trade.getLabel() + "] requires opening transactions and there are [" + openCloseCount.getCloseCount() + "] closing transactions.");
			}
			else {
				ValidationUtils.assertTrue(openCloseCount.getOpenCount() <= 0 && openCloseCount.getCloseCount() > 0, "Trade [" + trade.getLabel() + "] requires closing transactions and there are [" + openCloseCount.getOpenCount() + "] opening transactions.");
			}
		}
	}


	private void validateOpenCloseRequired(Trade trade) {
		TradeType tradeType = trade.getTradeType();
		if (tradeType.isExplicitOpenCloseSelectionRequired()) {
			ValidationUtils.assertFalse(trade.getOpenCloseType().isOpen() == trade.getOpenCloseType().isClose(), "[" + trade.getOpenCloseType() + "] is not valid for trade type [" + trade.getTradeType().getName() + "] which requires Open/Close to be explicitly set.");
		}
	}


	private void validateCrossingZero(Trade trade, OpenCloseCount openCloseCount) {
		if (!trade.getTradeType().isCrossingZeroAllowed()) {
			ValidationUtils.assertTrue(!openCloseCount.isCrossingZero(), "Trade type [" + trade.getTradeType().getName() + "] does not allow booking a trade fill that takes a position from short to long or vice versa.");
		}
		if (openCloseCount.isCrossingZero()) {
			ValidationUtils.assertTrue(trade.getOpenCloseType().isOpen() == trade.getOpenCloseType().isClose(), "[" + trade.getOpenCloseType() + "] does not allow booking trades with take a position from short to long or vice versa.");
		}
	}


	private OpenCloseCount getOpenAndClosingTransactions(Map<TradeFill, AccountingJournal> tradeFillToJournalMap) {
		OpenCloseCount result = new OpenCloseCount();
		for (AccountingJournal journal : tradeFillToJournalMap.values()) {
			List<? extends AccountingJournalDetailDefinition> journalDetailList = BeanUtils.filter(journal.getJournalDetailList(), this::isValidPositionTransaction);
			updateOpenCloseCount(result, journalDetailList);
		}
		return result;
	}


	private void updateOpenCloseCount(OpenCloseCount result, List<? extends AccountingJournalDetailDefinition> journalDetailList) {
		for (AccountingJournalDetailDefinition journalDetail : CollectionUtils.getIterable(journalDetailList)) {
			result.increment(journalDetail.isOpening());
		}
	}


	private boolean isValidPositionTransaction(AccountingJournalDetailDefinition detail) {
		AccountingAccount accountingAccount = detail.getAccountingAccount();
		if (accountingAccount.isPosition()) {
			if (detail.getParentTransaction() == null || !isReductionOfCurrentFace(detail, detail.getParentTransaction())) {
				if (detail.getParentDefinition() == null || !isReductionOfCurrentFace(detail, detail.getParentDefinition())) {
					if (!detail.isFactorChangeQuantityAdjustment()) {
						return true;
					}
				}
			}
		}
		return false;
	}


	private boolean isReductionOfCurrentFace(AccountingJournalDetailDefinition detail, AccountingJournalDetailDefinition parent) {
		return detail.getInvestmentSecurity().equals(parent.getInvestmentSecurity())
				&& detail.getJournal().equals(parent.getJournal())
				&& detail.getAccountingAccount().equals(parent.getAccountingAccount());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static final class OpenCloseCount {

		private int openCount = 0;
		private int closeCount = 0;


		public void increment(boolean opening) {
			if (opening) {
				this.openCount++;
			}
			else {
				this.closeCount++;
			}
		}


		public boolean isCrossingZero() {
			return this.openCount > 0 && this.closeCount > 0;
		}


		public int getOpenCount() {
			return this.openCount;
		}


		public int getCloseCount() {
			return this.closeCount;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingService<TradeFill> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<TradeFill> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public boolean isDoNotPostWhenBooking() {
		return this.doNotPostWhenBooking;
	}


	public void setDoNotPostWhenBooking(boolean doNotPostWhenBooking) {
		this.doNotPostWhenBooking = doNotPostWhenBooking;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeUtilHandler getTradeUtilHandler() {
		return this.tradeUtilHandler;
	}


	public void setTradeUtilHandler(TradeUtilHandler tradeUtilHandler) {
		this.tradeUtilHandler = tradeUtilHandler;
	}
}
