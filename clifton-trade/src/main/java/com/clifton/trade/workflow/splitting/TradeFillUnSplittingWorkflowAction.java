package com.clifton.trade.workflow.splitting;

import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeServiceImpl;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.transition.WorkflowTransition;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>TradeFillUnSplittingWorkflowAction</code> merge trade fills that have been split by the {@link TradeFillSplittingWorkflowAction}
 *
 * @author TerryS
 */
public class TradeFillUnSplittingWorkflowAction extends BaseTradeFillSplittingWorkflowAction {

	@Override
	public Trade processAction(Trade bean, WorkflowTransition transition) {
		// loads trade fills too - make sure trade and fills are not stale.
		Trade trade = getTradeService().getTrade(bean.getId());
		if (isSplittable(trade)) {
			// prevent concurrent modifications
			List<TradeFill> deletionList = new ArrayList<>();
			for (Map<Boolean, List<TradeFill>> map : getCrossingZeroMappings(trade.getTradeFillList())) {
				// merge openings into closings
				List<TradeFill> openings = map.get(Boolean.TRUE);
				// the total quantity from all opening fills
				BigDecimal openingQuantity = CollectionUtils.getStream(openings).map(TradeFill::getQuantity)
						.reduce(MathUtils::add)
						.orElse(BigDecimal.ZERO);
				if (!MathUtils.isNullOrZero(openingQuantity)) {
					deletionList.addAll(openings);
					// sort by quantity to make process determinant.
					TradeFill closingFill = map.get(Boolean.FALSE).stream()
							.min(Comparator.comparing(TradeFill::getQuantity))
							.orElseThrow(() -> new RuntimeException("Expected at an open fill for trade " + trade.getIdentity()));
					// merge total opening quantity with close fill quantity
					updateQuantities(closingFill, MathUtils.add(openingQuantity, closingFill.getQuantity()));
					// fill is no longer explicit
					closingFill.setOpenCloseType(getImplicitTradeOpenCloseType(trade.isBuy()));
					// bypass validations
					((TradeServiceImpl) getTradeService()).getTradeFillDAO().save(closingFill);
				}
			}
			if (!deletionList.isEmpty()) {
				trade.getTradeFillList().removeAll(deletionList);
				getTradeService().deleteTradeFillList(deletionList);
			}
		}
		return trade;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<Map<Boolean, List<TradeFill>>> getCrossingZeroMappings(List<TradeFill> fillList) {
		List<Map<Boolean, List<TradeFill>>> results = new ArrayList<>();

		// fills with the same notional unit price can be merged, group fills further by the open / close.
		final Map<BigDecimal, Map<TradeOpenCloseType, List<TradeFill>>> map = CollectionUtils.getStream(fillList)
				.collect(Collectors.groupingBy(TradeFill::getNotionalUnitPrice, Collectors.groupingBy(TradeFill::getOpenCloseType)));
		// return only those fills with the same notional unit price and have both (explicit) opening and closing fills.
		for (Map.Entry<BigDecimal, Map<TradeOpenCloseType, List<TradeFill>>> entry : map.entrySet()) {
			// only merge those fill with open / close type
			Map<Boolean, List<TradeFill>> candidateMap = CollectionUtils.getStream(entry.getValue().entrySet())
					.filter(e -> e.getKey() != null)
					.filter(e -> e.getKey().isExplicitOpenClose())
					.collect(Collectors.toMap(e -> e.getKey().isOpen(), Map.Entry::getValue));
			// open and close at the same notional unit price
			if (candidateMap.containsKey(Boolean.TRUE) && !candidateMap.get(Boolean.TRUE).isEmpty()
					&& candidateMap.containsKey(Boolean.FALSE) && !candidateMap.get(Boolean.FALSE).isEmpty()) {
				results.add(candidateMap);
			}
		}
		return results;
	}
}
