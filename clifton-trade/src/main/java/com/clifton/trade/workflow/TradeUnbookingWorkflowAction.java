package com.clifton.trade.workflow;


import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.search.AccountingJournalSearchForm;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.trade.accounting.commission.TradeCommissionOverride;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>TradeUnbookingWorkflowAction</code> class is a workflow transition action that un-posts and un-books
 * passed trades from the accounting system.
 * <p/>
 * Note: the action only un-books trades that have "bookingDate" field set. It only un-books in addition to workflow transition.
 * If un-booking process is initiated directly (not via workflow) then this action will not do anything in order to avoid double un-booking.
 *
 * @author vgomelsky
 */
public class TradeUnbookingWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private AccountingJournalService accountingJournalService;
	private AccountingPostingService accountingPostingService;
	private TradeService tradeService;
	private TradeCommissionService tradeCommissionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade processAction(Trade trade, WorkflowTransition transition) {
		//Retrieve existing commission override if there is one
		TradeCommissionOverride commissionOverride = getTradeCommissionService().getTradeCommissionOverrideForTradeCommissionAccountingAccount(trade);

		//Set the trade's commission to null if there is no override
		if (commissionOverride != null) {
			if (commissionOverride.getAmountPerUnit() == null) {
				trade.setCommissionPerUnit(null);
			}
			else {
				trade.setCommissionAmount(null);
			}
		}
		else {
			trade.setCommissionPerUnit(null);
			trade.setCommissionAmount(null);
		}

		// reset the open/close type on the trade
		updateTradeOpenClose(trade);

		//Retrieve existing fee override if there is one
		TradeCommissionOverride feeOverride = getTradeCommissionService().getTradeCommissionOverrideForTradeFeeAccountingAccount(trade);

		//Set the trade's fee to null if there is no override
		if (feeOverride == null) {
			trade.setFeeAmount(null);
		}

		// get fills
		List<TradeFill> fillList = trade.getTradeFillList();
		if (CollectionUtils.isEmpty(fillList)) {
			fillList = getTradeService().getTradeFillListByTrade(trade.getId());
		}
		ValidationUtils.assertNotEmpty(fillList, "Cannot unbook a trade with id = " + trade.getId() + " because it has no fills.");

		// sort fills so that they are unbooked using LIFO (reverse of booking to make same lot closings work correctly)
		List<Long> journalList = new ArrayList<>();
		for (TradeFill fill : fillList) {
			if (fill.getBookingDate() != null) {
				AccountingJournalSearchForm searchForm = new AccountingJournalSearchForm();
				searchForm.setSystemTableName(TradeFill.TABLE_NAME);
				searchForm.setFkFieldId(fill.getId());
				searchForm.setExcludeJournalStatusName(AccountingJournalStatus.STATUS_DELETED);
				List<AccountingJournal> journals = getAccountingJournalService().getAccountingJournalList(searchForm);
				ValidationUtils.assertNotEmpty(journals, "Cannot find accounting journal for trade fill with id = " + fill.getId());
				journalList = CollectionUtils.getStream(journals).map(AccountingJournal::getId).collect(Collectors.toList());
			}
		}

		// LIFO
		Collections.sort(journalList);
		Collections.reverse(journalList);

		// unbook each booked fill separately
		for (Long journalId : journalList) {
			getAccountingPostingService().unpostAccountingJournal(journalId);
		}
		return trade;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Update trades that are allowed to cross zero to either BUY or SELL without any open/close indicator.
	 */
	private void updateTradeOpenClose(Trade trade) {
		if (trade.getTradeType().isCrossingZeroAllowed()) {
			trade.setOpenCloseType(getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.ofBuy(trade.getOpenCloseType().isBuy()).getName()));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingPostingService getAccountingPostingService() {
		return this.accountingPostingService;
	}


	public void setAccountingPostingService(AccountingPostingService accountingPostingService) {
		this.accountingPostingService = accountingPostingService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}
}
