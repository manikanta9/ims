package com.clifton.trade.quote.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class TradeQuoteSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "quoteRequest.id")
	private Integer quoteRequestId;

	@SearchField(searchField = "clientInvestmentAccount.id", searchFieldPath = "quoteRequest", sortField = "clientInvestmentAccount.number")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "investmentSecurity.id", searchFieldPath = "quoteRequest", sortField = "investmentSecurity.symbol")
	private Integer investmentSecurityId;

	@SearchField(searchField = "underlyingInstrument.id", searchFieldPath = "quoteRequest.investmentSecurity.instrument", sortField = "underlyingInstrument.identifierPrefix")
	private Integer underlyingInstrumentId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "quoteRequest.investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchFieldPath = "quoteRequest")
	private Date tradeDate;

	@SearchField(searchField = "trade.id", searchFieldPath = "quoteRequest")
	private Integer tradeId;

	@SearchField(searchField = "quoteRequest.trade.tradeType.id")
	private Short tradeTypeId;

	@SearchField(searchField = "executingBroker.id", sortField = "executingBroker.name")
	private Integer executingBrokerId;

	@SearchField
	private BigDecimal bidPrice;

	@SearchField
	private BigDecimal askPrice;

	@SearchField
	private BigDecimal commissionAmount;

	@SearchField
	private BigDecimal feeAmount;

	@SearchField
	private BigDecimal financingSpread;

	@SearchField
	private BigDecimal independentAmount;

	@SearchField
	private String note;

	@SearchField
	private Short documentFileCount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getQuoteRequestId() {
		return this.quoteRequestId;
	}


	public void setQuoteRequestId(Integer quoteRequestId) {
		this.quoteRequestId = quoteRequestId;
	}


	public Integer getExecutingBrokerId() {
		return this.executingBrokerId;
	}


	public void setExecutingBrokerId(Integer executingBrokerId) {
		this.executingBrokerId = executingBrokerId;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}


	public BigDecimal getIndependentAmount() {
		return this.independentAmount;
	}


	public void setIndependentAmount(BigDecimal independentAmount) {
		this.independentAmount = independentAmount;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Integer getUnderlyingInstrumentId() {
		return this.underlyingInstrumentId;
	}


	public void setUnderlyingInstrumentId(Integer underlyingInstrumentId) {
		this.underlyingInstrumentId = underlyingInstrumentId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public BigDecimal getFinancingSpread() {
		return this.financingSpread;
	}


	public void setFinancingSpread(BigDecimal financingSpread) {
		this.financingSpread = financingSpread;
	}


	public BigDecimal getBidPrice() {
		return this.bidPrice;
	}


	public void setBidPrice(BigDecimal bidPrice) {
		this.bidPrice = bidPrice;
	}


	public BigDecimal getAskPrice() {
		return this.askPrice;
	}


	public void setAskPrice(BigDecimal askPrice) {
		this.askPrice = askPrice;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}
}
