package com.clifton.trade.quote.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class TradeQuoteRequestSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "trade.id")
	private Integer tradeId;

	@SearchField(searchField = "trade.tradeType.id")
	private Short tradeTypeId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField
	private Date tradeDate;

	@SearchField
	private String note;


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}
}
