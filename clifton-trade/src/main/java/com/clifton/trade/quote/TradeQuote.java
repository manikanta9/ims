package com.clifton.trade.quote;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;


/**
 * The <code>TradeQuote</code> class represents a single broker quote.
 * We usually solicit quotes from multiple brokers that are grouped into TradeQuoteRequest.
 * A new trade is then created from corresponding quote request by usually selecting the best quote.
 *
 * @author vgomelsky
 */
public class TradeQuote extends BaseEntity<Integer> implements DocumentFileCountAware {

	private TradeQuoteRequest quoteRequest;

	private BusinessCompany executingBroker; // or counter party for OTC

	private BigDecimal bidPrice; // depending on quote/trade type this could be quoted as Financing Spread, etc.
	private BigDecimal askPrice; // we often get both prices in order not to disclose which way we are going
	private BigDecimal financingSpread; // for securities that have it: TRS, etc.
	private BigDecimal independentAmount;
	private BigDecimal commissionAmount; // total commission amount charged for this trade
	private BigDecimal feeAmount; // total fee for the trade
	private String note;

	/**
	 * Total number of attachments tied to the quote
	 */
	private Short documentFileCount;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isNewBean() {
		if (getId() == null) {
			return true;
		}
		return getId() < 0; // template uses -10, -11, etc. for new beans
	}


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public String getLabel() {
		StringBuilder result = new StringBuilder();
		if (getExecutingBroker() != null) {
			result.append(getExecutingBroker().getLabel()).append(": ");
		}
		if (getBidPrice() != null) {
			result.append("Bid=").append(CoreMathUtils.formatNumberDecimal(getBidPrice())).append(' ');
		}
		if (getAskPrice() != null) {
			result.append("Ask=").append(CoreMathUtils.formatNumberDecimal(getAskPrice())).append(' ');
		}
		if (getFinancingSpread() != null) {
			result.append("Spread=").append(CoreMathUtils.formatNumberDecimal(getFinancingSpread())).append(' ');
		}
		if (getIndependentAmount() != null) {
			result.append("Independent Amount=").append(CoreMathUtils.formatNumberDecimal(getIndependentAmount())).append(' ');
		}
		if (getCommissionAmount() != null) {
			result.append("Commission=").append(CoreMathUtils.formatNumberDecimal(getCommissionAmount())).append(' ');
		}
		if (getFeeAmount() != null) {
			result.append("Fee=").append(CoreMathUtils.formatNumberDecimal(getFeeAmount())).append(' ');
		}

		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getExecutingBroker() {
		return this.executingBroker;
	}


	public void setExecutingBroker(BusinessCompany executingBroker) {
		this.executingBroker = executingBroker;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}


	public BigDecimal getIndependentAmount() {
		return this.independentAmount;
	}


	public void setIndependentAmount(BigDecimal independentAmount) {
		this.independentAmount = independentAmount;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public TradeQuoteRequest getQuoteRequest() {
		return this.quoteRequest;
	}


	public void setQuoteRequest(TradeQuoteRequest quoteRequest) {
		this.quoteRequest = quoteRequest;
	}


	public BigDecimal getFinancingSpread() {
		return this.financingSpread;
	}


	public void setFinancingSpread(BigDecimal financingSpread) {
		this.financingSpread = financingSpread;
	}


	public BigDecimal getBidPrice() {
		return this.bidPrice;
	}


	public void setBidPrice(BigDecimal bidPrice) {
		this.bidPrice = bidPrice;
	}


	public BigDecimal getAskPrice() {
		return this.askPrice;
	}


	public void setAskPrice(BigDecimal askPrice) {
		this.askPrice = askPrice;
	}
}
