package com.clifton.trade.quote;


import com.clifton.trade.quote.search.TradeQuoteRequestSearchForm;
import com.clifton.trade.quote.search.TradeQuoteSearchForm;

import java.util.List;


/**
 * The <code>TradeQuoteService</code> interface defines methods for working with competitive broker quotes.
 *
 * @author vgomelsky
 */
public interface TradeQuoteService {

	//////////////////////////////////////////////////////////////////////////// 
	//////                   Trade Quote Methods                         /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeQuote getTradeQuote(int id);


	public List<TradeQuote> getTradeQuoteList(TradeQuoteSearchForm searchForm);


	/**
	 * Returns a List of TradeQuote object for the specified tradeId.
	 * If no quotes are found for the trade, retrieve all Executing Brokers that
	 * are allowed for this trade and returns an empty list of new TradeQuote objects
	 * with Executing Brokers populated (template).
	 *
	 * @param tradeId
	 */
	public List<TradeQuote> getTradeQuoteListOrTemplateForTrade(int tradeId);


	public TradeQuote saveTradeQuote(TradeQuote bean);


	public void deleteTradeQuote(int id);


	//////////////////////////////////////////////////////////////////////////// 
	//////               Trade Quote Request Methods                     /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeQuoteRequest getTradeQuoteRequest(int id);


	public List<TradeQuoteRequest> getTradeQuoteRequestList(TradeQuoteRequestSearchForm searchForm);
}
