package com.clifton.trade.quote;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyService;
import com.clifton.trade.execution.search.TradeExecutingBrokerSearchCommand;
import com.clifton.trade.quote.search.TradeQuoteRequestSearchForm;
import com.clifton.trade.quote.search.TradeQuoteSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class TradeQuoteServiceImpl implements TradeQuoteService {

	private AdvancedUpdatableDAO<TradeQuote, Criteria> tradeQuoteDAO;
	private AdvancedUpdatableDAO<TradeQuoteRequest, Criteria> tradeQuoteRequestDAO;

	private TradeService tradeService;
	private TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService;

	////////////////////////////////////////////////////////////////////////////
	//////                   Trade Quote Methods                         ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeQuote getTradeQuote(int id) {
		return getTradeQuoteDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeQuote> getTradeQuoteList(TradeQuoteSearchForm searchForm) {
		return getTradeQuoteDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<TradeQuote> getTradeQuoteListOrTemplateForTrade(int tradeId) {
		// first try to get existing quotes for the specified trade
		TradeQuoteSearchForm searchForm = new TradeQuoteSearchForm();
		searchForm.setTradeId(tradeId);
		List<TradeQuote> result = getTradeQuoteList(searchForm);
		if (CollectionUtils.isEmpty(result)) {
			// didn't find existing quotes: return a list of quote templates for each allowed executing broker
			Trade trade = getTradeService().getTrade(tradeId);
			if (trade != null) {
				TradeExecutingBrokerSearchCommand command = new TradeExecutingBrokerSearchCommand();
				command.setTradeTypeId(trade.getTradeType().getId());
				command.setSecurityId(trade.getInvestmentSecurity().getId());
				command.setClientInvestmentAccountId(trade.getClientInvestmentAccount().getId());
				command.setActiveOnDate(trade.getTradeDate());
				List<BusinessCompany> brokerList = getTradeExecutingBrokerCompanyService().getTradeExecutingBrokerCompanyList(command);
				if (!CollectionUtils.isEmpty(brokerList)) {
					result = new ArrayList<>();
					int id = -10;
					TradeQuoteRequest quoteRequest = new TradeQuoteRequest();
					quoteRequest.setTrade(trade);
					for (BusinessCompany broker : brokerList) {
						TradeQuote quote = new TradeQuote();
						quote.setId(id--);
						quote.setQuoteRequest(quoteRequest);
						quote.setExecutingBroker(broker);
						result.add(quote);
					}
				}
			}
		}
		return result;
	}


	@Override
	@Transactional
	public TradeQuote saveTradeQuote(TradeQuote bean) {
		ValidationUtils.assertTrue(bean.getBidPrice() != null || bean.getAskPrice() != null || bean.getFinancingSpread() != null,
				"Bid/Ask Prices and Financing Spread fields cannot be empty at the same time.");

		TradeQuoteRequest request = bean.getQuoteRequest();
		if (bean.isNewBean()) {
			Trade trade = request.getTrade();
			if (trade != null) {
				// check if this trade already has other quotes: same quote request
				TradeQuoteRequest existingRequest = getTradeQuoteRequestDAO().findOneByField("trade.id", trade.getId());
				if (existingRequest != null) {
					request = existingRequest;
				}
				else {
					request.setClientInvestmentAccount(trade.getClientInvestmentAccount());
					request.setInvestmentSecurity(trade.getInvestmentSecurity());
					request.setTradeDate(trade.getTradeDate());
					request = getTradeQuoteRequestDAO().save(request);
				}
				bean.setQuoteRequest(request);
			}
			else {
				throw new ValidationException("A Trade must be associated with a new quote.");
			}
		}

		Trade trade = request.getTrade();
		if (trade != null) {
			if ((bean.getFinancingSpread() == null) && !InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
				if (trade.isBuy()) {
					ValidationUtils.assertTrue(bean.getAskPrice() != null, "Ask price is required on a quote for a buy trade.");
				}
				else {
					ValidationUtils.assertTrue(bean.getBidPrice() != null, "Bid price is required on a quote for a sel trade.");
				}
			}
			ValidationUtils.assertEquals(trade.getInvestmentSecurity(), request.getInvestmentSecurity(), "A trade must be for the same investment security as the quote.");
			if (request.getClientInvestmentAccount() != null) {
				ValidationUtils.assertEquals(trade.getClientInvestmentAccount(), request.getClientInvestmentAccount(), "A trade must be for the same client account as the quote.");
			}
		}

		return getTradeQuoteDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteTradeQuote(int id) {
		// when last quote is deleted, also delete corresponding request
		TradeQuote quote = getTradeQuote(id);
		TradeQuoteRequest request = getTradeQuoteRequest(quote.getQuoteRequest().getId());
		boolean deleteRequest = (CollectionUtils.getSize(request.getQuoteList()) == 1);

		getTradeQuoteDAO().delete(quote);
		if (deleteRequest) {
			getTradeQuoteRequestDAO().delete(request);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////               Trade Quote Request Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeQuoteRequest getTradeQuoteRequest(int id) {
		TradeQuoteRequest request = getTradeQuoteRequestDAO().findByPrimaryKey(id);
		if (request != null) {
			request.setQuoteList(getTradeQuoteDAO().findByField("quoteRequest.id", id));
		}
		return request;
	}


	@Override
	public List<TradeQuoteRequest> getTradeQuoteRequestList(TradeQuoteRequestSearchForm searchForm) {
		return getTradeQuoteRequestDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	//////                   Getters and Setters                         ///////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeQuote, Criteria> getTradeQuoteDAO() {
		return this.tradeQuoteDAO;
	}


	public void setTradeQuoteDAO(AdvancedUpdatableDAO<TradeQuote, Criteria> tradeQuoteDAO) {
		this.tradeQuoteDAO = tradeQuoteDAO;
	}


	public AdvancedUpdatableDAO<TradeQuoteRequest, Criteria> getTradeQuoteRequestDAO() {
		return this.tradeQuoteRequestDAO;
	}


	public void setTradeQuoteRequestDAO(AdvancedUpdatableDAO<TradeQuoteRequest, Criteria> tradeQuoteRequestDAO) {
		this.tradeQuoteRequestDAO = tradeQuoteRequestDAO;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeExecutingBrokerCompanyService getTradeExecutingBrokerCompanyService() {
		return this.tradeExecutingBrokerCompanyService;
	}


	public void setTradeExecutingBrokerCompanyService(TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService) {
		this.tradeExecutingBrokerCompanyService = tradeExecutingBrokerCompanyService;
	}
}
