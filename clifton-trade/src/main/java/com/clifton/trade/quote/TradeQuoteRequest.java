package com.clifton.trade.quote;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;

import java.util.Date;
import java.util.List;


/**
 * The <code>TradeQuoteRequest</code> class represents a request for competitive quotes.  These quotes
 * are usually obtained from 2 or more executing brokers (counter parties). Details of each quote are
 * stored in corresponding TradeQuote objects associated with this TradeQuoteRequest.
 * <p/>
 * Client Investment Account is optional because a single quote may be an aggregate of multiple accounts.
 *
 * @author vgomelsky
 */
public class TradeQuoteRequest extends BaseEntity<Integer> {

	/**
	 * Links quotes from this request to a trade. Can be null if a trade is never created from a quote.
	 */
	private Trade trade;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentSecurity investmentSecurity;

	private Date tradeDate;
	private String note;

	private List<TradeQuote> quoteList;


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public List<TradeQuote> getQuoteList() {
		return this.quoteList;
	}


	public void setQuoteList(List<TradeQuote> quoteList) {
		this.quoteList = quoteList;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}
}
