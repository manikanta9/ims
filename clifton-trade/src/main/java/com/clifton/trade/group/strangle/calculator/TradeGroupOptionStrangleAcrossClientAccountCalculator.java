package com.clifton.trade.group.strangle.calculator;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.strangle.TradeGroupStrangle;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>TradeGroupOptionStrangleAcrossClientAccountCalculator</code> is a {@link TradeGroupStrangleCalculator} for options
 * that strangles trades across client accounts. The strangle will only have one tail that includes the excess of total put/call
 * quantities. The excess put/call quantity is proportionally redistributed among the included client accounts' trades so the
 * included client accounts can share in the strangle cost savings.
 *
 * @author NickK
 */
public class TradeGroupOptionStrangleAcrossClientAccountCalculator extends BaseTradeGroupOptionStrangleCalculator {

	@Override
	protected TradeGroupStrangle calculateTradeGroupOptionStrangle(StrangleTradeList strangleTradeList) {
		Map<String, List<Trade>> strangleListMap = new HashMap<>(3);
		strangleListMap.put(STRANGLE_TRADE_LIST_KEY, new ArrayList<>());
		strangleListMap.put(CALL_TAIL_TRADE_LIST_KEY, new ArrayList<>());
		strangleListMap.put(PUT_TAIL_TRADE_LIST_KEY, new ArrayList<>());

		boolean putHeavy = strangleTradeList.isPutHeavy();
		BigDecimal[] clientAccountTailDistribution = getProportionTailQuantityForEachClientAccount(strangleTradeList, putHeavy);

		int index = 0;
		for (InvestmentAccount account : strangleTradeList) {
			List<Trade>[] strangleLists = splitAndGroupTrades(strangleTradeList.getClientAccountTradeList(account), putHeavy, clientAccountTailDistribution[index], false);
			if (putHeavy) {
				strangleListMap.get(PUT_TAIL_TRADE_LIST_KEY).addAll(strangleLists[0]);
			}
			else {
				strangleListMap.get(CALL_TAIL_TRADE_LIST_KEY).addAll(strangleLists[0]);
			}
			strangleListMap.get(STRANGLE_TRADE_LIST_KEY).addAll(strangleLists[1]);
			index++;
		}
		TradeGroup callTradeGroup = populateTailTradeGroup(strangleListMap.get(CALL_TAIL_TRADE_LIST_KEY));
		TradeGroup putTradeGroup = populateTailTradeGroup(strangleListMap.get(PUT_TAIL_TRADE_LIST_KEY));
		return populateStrangleTradeGroup(strangleListMap.get(STRANGLE_TRADE_LIST_KEY), strangleTradeList, callTradeGroup, putTradeGroup);
	}


	private BigDecimal[] getProportionTailQuantityForEachClientAccount(StrangleTradeList strangleTradeList, boolean putHeavy) {
		BigDecimal[] tailQuantityArray = new BigDecimal[strangleTradeList.getClientAccountSize()];
		BigDecimal totalTailQuantity = MathUtils.abs(MathUtils.subtract(strangleTradeList.getStranglePutQuantity(), strangleTradeList.getStrangleCallQuantity()));
		BigDecimal ratio = putHeavy ? MathUtils.divide(strangleTradeList.getStrangleCallQuantity(), strangleTradeList.getStranglePutQuantity())
				: MathUtils.divide(strangleTradeList.getStranglePutQuantity(), strangleTradeList.getStrangleCallQuantity());
		int index = 0;
		BigDecimal calculatedTailQuantity = BigDecimal.ZERO;
		BigDecimal largestTail = BigDecimal.ZERO;
		int indexWithLargestTail = 0;
		for (InvestmentAccount account : strangleTradeList) {
			ClientAccountTradeList clientAccountTradeList = strangleTradeList.getClientAccountTradeList(account);
			BigDecimal quantityFactor = putHeavy ? clientAccountTradeList.getPutQuantity() : clientAccountTradeList.getCallQuantity();
			tailQuantityArray[index] = MathUtils.subtract(quantityFactor, MathUtils.round(MathUtils.multiply(quantityFactor, ratio), 0));
			calculatedTailQuantity = MathUtils.add(calculatedTailQuantity, tailQuantityArray[index]);
			if (MathUtils.isGreaterThan(tailQuantityArray[index], largestTail)) {
				largestTail = tailQuantityArray[index];
				indexWithLargestTail = index;
			}
			index++;
		}
		BigDecimal difference = MathUtils.subtract(totalTailQuantity, calculatedTailQuantity);
		if (MathUtils.isNotEqual(difference, BigDecimal.ZERO)) {
			tailQuantityArray[indexWithLargestTail] = MathUtils.add(tailQuantityArray[indexWithLargestTail], difference);
		}
		return tailQuantityArray;
	}
}
