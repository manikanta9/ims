package com.clifton.trade.group;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeSource;
import com.clifton.trade.TradeTimeInForceType;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.execution.TradeExecutionType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>TradeEntry</code> class is used to create multiple trades for the same security, etc. together.
 * This object is not persisted but is used to create multiple Trade object: one for each TradeEntryDetail.
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class TradeEntry {

	private TradeType tradeType;
	/**
	 * Optional {@link TradeSource} to be applied to the {@link TradeEntryDetail}s of this object defining where the resulting trades are generated from.
	 */
	private TradeSource tradeSource;

	private InvestmentSecurity investmentSecurity;
	private InvestmentSecurity payingSecurity;

	private BusinessCompany executingBrokerCompany;
	private TradeDestination tradeDestination; // specifies the destination of the trade i.e. Goldman Sachs via FIX or Internal Clifton processing

	/**
	 * An optional value that identifies how the trade should be executed (e.g. limit order, or MOC).
	 */
	private TradeExecutionType tradeExecutionType;

	/**
	 * Optional time in force value to define duration of the trade order.
	 */
	private TradeTimeInForceType tradeTimeInForceType;

	/**
	 * A Date with time (in UTC) indicating when a GTD order expires.  This should remain null unless the tradeTimeInForceType is of a type that requires the expiration date (GTD).
	 */
	private Date timeInForceExpirationDate;


	/**
	 * An optional value for limit price
	 */
	private BigDecimal limitPrice;

	private SecurityUser traderUser;

	private boolean buy;
	private TradeOpenCloseType openCloseType;

	private boolean fixTrade; // specifies whether the trade should be executed using FIX protocol
	private boolean collateralTrade; // specifies whether position from this trade is used for collateral
	/**
	 * Specifies whether trades of this entry are to be block trades {@link com.clifton.trade.Trade#blockTrade}.
	 */
	private boolean blockTrade;

	private BigDecimal quantityIntended;
	/**
	 * Usually the same as quantityIntended but for bonds that had factor changes prior to this trade (current factor <> 1),
	 * stores Original Face value while quantityIntended will store Purchase Face.
	 */
	private BigDecimal originalFace;
	/**
	 * Some securities may adjust accounting notional by this multiplier that is tied to something.
	 * TIPS use it to adjust for changes in inflation: Index Ratio.
	 */
	private BigDecimal notionalMultiplier = BigDecimal.ONE;

	/**
	 * Purchase Factor that applies to bonds. Must be set to 1 for all other securities meaning no factor change.
	 */
	private BigDecimal currentFactor;
	private BigDecimal exchangeRateToBase;
	private BigDecimal expectedUnitPrice;
	private BigDecimal averageUnitPrice;
	private BigDecimal accrualAmount;
	private BigDecimal commissionPerUnit;
	private BigDecimal feeAmount;
	/**
	 * Trading notional can be different because it's adjusted based on client's benchmark duration
	 */
	private BigDecimal accountingNotional;

	/**
	 * Field to indicate if trade is novation - if this field is true, then a counterparty can be specified via the assignmentCompany field.  While the trade can be created and booked without
	 * a counterparty assigned, there is a post-process rule to verify that a counterparty is assigned for novation trades by the time they get to that stage.
	 */
	private boolean novation;

	/**
	 * If trade is novation, a counterparty can be specified.  This field cannot be specified if novation is false.
	 */
	private BusinessCompany assignmentCompany;

	private Date tradeDate;
	private Date settlementDate;
	private Date bookingDate;

	private Integer groupIdentifier; // optional identifier that can link this trade to other trades that were created together
	private String description;

	private List<TradeEntryDetail> tradeDetailList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}


	public TradeSource getTradeSource() {
		return this.tradeSource;
	}


	public void setTradeSource(TradeSource tradeSource) {
		this.tradeSource = tradeSource;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentSecurity getPayingSecurity() {
		return this.payingSecurity;
	}


	public void setPayingSecurity(InvestmentSecurity payingSecurity) {
		this.payingSecurity = payingSecurity;
	}


	public SecurityUser getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
	}


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public boolean isFixTrade() {
		return this.fixTrade;
	}


	public void setFixTrade(boolean fixTrade) {
		this.fixTrade = fixTrade;
	}


	public BigDecimal getQuantityIntended() {
		return this.quantityIntended;
	}


	public void setQuantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
	}


	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getExpectedUnitPrice() {
		return this.expectedUnitPrice;
	}


	public void setExpectedUnitPrice(BigDecimal expectedUnitPrice) {
		this.expectedUnitPrice = expectedUnitPrice;
	}


	public BigDecimal getAverageUnitPrice() {
		return this.averageUnitPrice;
	}


	public void setAverageUnitPrice(BigDecimal averageUnitPrice) {
		this.averageUnitPrice = averageUnitPrice;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}


	public BigDecimal getAccountingNotional() {
		return this.accountingNotional;
	}


	public void setAccountingNotional(BigDecimal accountingNotional) {
		this.accountingNotional = accountingNotional;
	}


	public boolean isNovation() {
		return this.novation;
	}


	public void setNovation(boolean novation) {
		this.novation = novation;
	}


	public BusinessCompany getAssignmentCompany() {
		return this.assignmentCompany;
	}


	public void setAssignmentCompany(BusinessCompany assignmentCompany) {
		this.assignmentCompany = assignmentCompany;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getBookingDate() {
		return this.bookingDate;
	}


	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getGroupIdentifier() {
		return this.groupIdentifier;
	}


	public void setGroupIdentifier(Integer groupIdentifier) {
		this.groupIdentifier = groupIdentifier;
	}


	public List<TradeEntryDetail> getTradeDetailList() {
		return this.tradeDetailList;
	}


	public void setTradeDetailList(List<TradeEntryDetail> tradeDetailList) {
		this.tradeDetailList = tradeDetailList;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getCurrentFactor() {
		return this.currentFactor;
	}


	public void setCurrentFactor(BigDecimal currentFactor) {
		this.currentFactor = currentFactor;
	}


	public BigDecimal getNotionalMultiplier() {
		return this.notionalMultiplier;
	}


	public void setNotionalMultiplier(BigDecimal notionalMultiplier) {
		this.notionalMultiplier = notionalMultiplier;
	}


	public boolean isCollateralTrade() {
		return this.collateralTrade;
	}


	public void setCollateralTrade(boolean collateralTrade) {
		this.collateralTrade = collateralTrade;
	}


	public boolean isBlockTrade() {
		return this.blockTrade;
	}


	public void setBlockTrade(boolean blockTrade) {
		this.blockTrade = blockTrade;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public TradeExecutionType getTradeExecutionType() {
		return this.tradeExecutionType;
	}


	public void setTradeExecutionType(TradeExecutionType tradeExecutionType) {
		this.tradeExecutionType = tradeExecutionType;
	}


	public TradeTimeInForceType getTradeTimeInForceType() {
		return this.tradeTimeInForceType;
	}


	public void setTradeTimeInForceType(TradeTimeInForceType tradeTimeInForceType) {
		this.tradeTimeInForceType = tradeTimeInForceType;
	}


	public Date getTimeInForceExpirationDate() {
		return this.timeInForceExpirationDate;
	}


	public void setTimeInForceExpirationDate(Date timeInForceExpirationDate) {
		this.timeInForceExpirationDate = timeInForceExpirationDate;
	}


	public BigDecimal getLimitPrice() {
		return this.limitPrice;
	}


	public void setLimitPrice(BigDecimal limitPrice) {
		this.limitPrice = limitPrice;
	}


	public BigDecimal getAccrualAmount() {
		return this.accrualAmount;
	}


	public void setAccrualAmount(BigDecimal accrualAmount) {
		this.accrualAmount = accrualAmount;
	}


	public TradeOpenCloseType getOpenCloseType() {
		return this.openCloseType;
	}


	public void setOpenCloseType(TradeOpenCloseType openCloseType) {
		this.openCloseType = openCloseType;
	}
}
