package com.clifton.trade.group.strangle;

import com.clifton.trade.group.TradeGroup;


/**
 * <code>TradeGroupStrangle</code> represents a group of trades created to improve execution or
 * lower commissions from the executing broker. Each strangle will have a 0, 1 or 2 tails associated
 * with it.
 *
 * @author NickK
 */
public class TradeGroupStrangle extends TradeGroup {

	// TODO create a list of tails with tradeGroup types for put/call?
	private TradeGroup callTailGroup;
	private TradeGroup putTailGroup;

	////////////////////////////////////////////////////////////////////////////
	//////               Trade Group Strangle Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	public boolean hasCallTail() {
		return getCallTailGroup() != null;
	}


	public boolean hasPutTail() {
		return getPutTailGroup() != null;
	}

	////////////////////////////////////////////////////////////////////////////
	//////                  Getter and Setter Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeGroup getCallTailGroup() {
		return this.callTailGroup;
	}


	public void setCallTailGroup(TradeGroup tailGroup) {
		this.callTailGroup = tailGroup;
	}


	public TradeGroup getPutTailGroup() {
		return this.putTailGroup;
	}


	public void setPutTailGroup(TradeGroup tailGroup) {
		this.putTailGroup = tailGroup;
	}
}
