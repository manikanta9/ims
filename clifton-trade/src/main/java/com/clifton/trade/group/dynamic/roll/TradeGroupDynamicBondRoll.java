package com.clifton.trade.group.dynamic.roll;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturity;

import java.util.List;
import java.util.Map;


/**
 * <code>TradeGroupDynamicBondRoll</code> is a type of TradeGroupDynamic that is used for rolling maturing Bonds, particularly Treasuries.
 * The maturing positions can be rolled to later maturing Bond.
 * <p>
 * Customized properties are saved on the TradeGroup as the Note field as
 * propertyName:propertyValue
 * propertyName2:propertyValue2, etc.
 *
 * @author NickK
 */
public class TradeGroupDynamicBondRoll extends BaseTradeGroupDynamicMaturity<TradeGroupDynamicBondRollDetail> {

	private InvestmentTypeSubType investmentTypeSubType;
	private InvestmentTypeSubType2 investmentTypeSubType2;

	// Optional filter used to get a subset of client accounts that support bond trading (e.g. only those that support futures trading)
	private String clientAccountTradingPurposeName;

	private Boolean collateralOnly;

	// List of securities with a weight/percentage allocations of a rolled face amount to create trades against
	private List<TradeGroupDynamicBondRollAllocation> allocationList;

	// local cache used to create trade groups for each allocation
	private Map<String, TradeGroup> allocationTradeGroupMap;

	////////////////////////////////////////////////////////////////////////////////
	//////////              TradeGroupDynamicBondRoll Items             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getSavePropertiesAsTradeGroupNote() {
		List<String> parentProperties = super.getSavePropertiesAsTradeGroupNote();
		parentProperties.addAll(CollectionUtils.createList("investmentTypeSubType.id", "investmentTypeSubType2.id", "collateralOnly"));
		return parentProperties;
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public String getClientAccountTradingPurposeName() {
		return this.clientAccountTradingPurposeName;
	}


	public void setClientAccountTradingPurposeName(String clientAccountTradingPurposeName) {
		this.clientAccountTradingPurposeName = clientAccountTradingPurposeName;
	}


	public Boolean getCollateralOnly() {
		return this.collateralOnly;
	}


	public void setCollateralOnly(Boolean collateralOnly) {
		this.collateralOnly = collateralOnly;
	}


	public List<TradeGroupDynamicBondRollAllocation> getAllocationList() {
		return this.allocationList;
	}


	public void setAllocationList(List<TradeGroupDynamicBondRollAllocation> allocationList) {
		this.allocationList = allocationList;
	}


	public Map<String, TradeGroup> getAllocationTradeGroupMap() {
		return this.allocationTradeGroupMap;
	}


	public void setAllocationTradeGroupMap(Map<String, TradeGroup> allocationTradeGroupMap) {
		this.allocationTradeGroupMap = allocationTradeGroupMap;
	}
}
