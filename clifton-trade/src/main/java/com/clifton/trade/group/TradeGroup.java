package com.clifton.trade.group;


import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.trade.Trade;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>TradeGroup</code> represents a group of trades created for a specific purpose:
 * Rolls, Tails, PIOS, Multi-Client
 *
 * @author Mary Anderson
 */
public class TradeGroup extends HierarchicalEntity<TradeGroup, Integer> implements LabeledObject {

	public enum TradeGroupActions {
		CANCEL("Cancelled"),
		VALIDATE("Validated"),
		APPROVE("Approved", true),
		FILL(""), // Creates a TradeGroups trades' fills with no workflow changes.
		DELETE_FILLS(""), // Deletes a TradeGroups trades' fills with no workflow changes.
		UPDATE_FILLS(""),
		CANNOT_FILL("Cannot Fill"),
		EXECUTE("Executed"),
		FILL_EXECUTE("Executed"),
		UNBOOK("Unbooked"),
		BOOK("Booked"),
		SAVE_TRADE(""), // Saves updates to a TradeGroup's trades with no workflow changes (e.g. commission overrides).
		REJECT("Rejected");

		private final String stateName;
		// Similar to Trade Workflow - for "Approval" action we allow Read Access Only
		private final boolean readAccessSufficientToExecute;


		TradeGroupActions(String stateName) {
			this(stateName, false);
		}


		TradeGroupActions(String stateName, boolean readAccessSufficientToExecute) {
			this.stateName = stateName;
			this.readAccessSufficientToExecute = readAccessSufficientToExecute;
		}


		public String getStateName() {
			return this.stateName;
		}


		public boolean isReadAccessSufficientToExecute() {
			return this.readAccessSufficientToExecute;
		}
	}

	private TradeGroupType tradeGroupType;

	private Date tradeDate;
	private SecurityUser traderUser;

	private InvestmentSecurity investmentSecurity;
	private InvestmentSecurity secondaryInvestmentSecurity;

	private String note;

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	/**
	 * When SourceSystemTable is set on the trade group this field can be set
	 * for UI drill down into the source window.
	 * <p>
	 * i.e. Portfolio Run Trade Group type is linked to the ProductOverlayRun table
	 * The fkFieldId for these trade groups can be set to the ProductOverlayRunID so
	 * can link from the Trade Group window back to the PIOS run the trade group was created from
	 */
	@SoftLinkField(tableBeanPropertyName = "tradeGroupType.sourceSystemTable")
	private Integer sourceFkFieldId;

	private List<Trade> tradeList;

	// Used to execute actions on a Trade Group - either all or a selected list
	@NonPersistentField
	private TradeGroupActions tradeGroupAction;

	// String documenting the results of executing a Trade Group Action
	@NonPersistentField
	private String tradeGroupActionResult;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getNote() != null) {
			return getNote();
		}

		StringBuilder sb = new StringBuilder(16);
		if (getInvestmentSecurity() != null) {
			if (getTradeGroupType() != null) {
				sb.append(getTradeGroupType().getName());
				sb.append(": ");
			}

			sb.append(getInvestmentSecurity().getSymbol());
			if (getSecondaryInvestmentSecurity() != null) {
				sb.append(" - ");
				sb.append(getSecondaryInvestmentSecurity().getSymbol());
			}
		}
		else {
			if (getTradeGroupType() != null) {
				sb.append(getTradeGroupType().getName());
			}
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}


	// Convenience Method
	public void addTrade(Trade trade) {
		if (this.tradeList == null) {
			this.tradeList = new ArrayList<>();
		}
		this.tradeList.add(trade);
		if (getTradeDate() == null) {
			setTradeDate(trade.getTradeDate());
		}
		if (getTraderUser() == null) {
			setTraderUser(trade.getTraderUser());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeGroupType getTradeGroupType() {
		return this.tradeGroupType;
	}


	public void setTradeGroupType(TradeGroupType tradeGroupType) {
		this.tradeGroupType = tradeGroupType;
	}


	public SecurityUser getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentSecurity getSecondaryInvestmentSecurity() {
		return this.secondaryInvestmentSecurity;
	}


	public void setSecondaryInvestmentSecurity(InvestmentSecurity secondaryInvestmentSecurity) {
		this.secondaryInvestmentSecurity = secondaryInvestmentSecurity;
	}


	public List<Trade> getTradeList() {
		return this.tradeList;
	}


	public void setTradeList(List<Trade> tradeList) {
		this.tradeList = tradeList;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public TradeGroupActions getTradeGroupAction() {
		return this.tradeGroupAction;
	}


	public void setTradeGroupAction(TradeGroupActions tradeGroupAction) {
		this.tradeGroupAction = tradeGroupAction;
	}


	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public Integer getSourceFkFieldId() {
		return this.sourceFkFieldId;
	}


	public void setSourceFkFieldId(Integer sourceFkFieldId) {
		this.sourceFkFieldId = sourceFkFieldId;
	}


	public String getTradeGroupActionResult() {
		return this.tradeGroupActionResult;
	}


	public void setTradeGroupActionResult(String tradeGroupActionResult) {
		this.tradeGroupActionResult = tradeGroupActionResult;
	}
}
