package com.clifton.trade.group;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.TradeSource;
import com.clifton.trade.destination.TradeDestination;

import java.math.BigDecimal;


/**
 * The <code>TradeEntryDetail</code> class represents a single Trade and is used with TradeEntry object
 * in order to create multiple trades together.
 * <p/>
 * FIXME REFACTOR AND REMOVE - JUST USE TRADE OBJECT DIRECTLY - NOT NECESSARY TO HAVE ANOTHER OBJECT
 */
public class TradeEntryDetail extends BaseSimpleEntity<Integer> {

	/**
	 * Optional {@link TradeSource} to be applied to a {@link com.clifton.trade.Trade} generated from this detail defining where it is being generated from.
	 * If the source is not defined here, it will inherit from {@link TradeEntry#getTradeSource()}.
	 */
	private TradeSource tradeSource;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	/**
	 * An optional field that can be used to override the entry defined executing broker for this client account
	 */
	private BusinessCompany executingBrokerCompany;
	/**
	 * An optional TradeDestinationMapping that, if defined, is used to override the entry defined TradeDestination for this client account.
	 */
	private TradeDestination tradeDestination;

	private BigDecimal originalFace;
	private BigDecimal quantityIntended;

	private BigDecimal expectedUnitPrice;
	private BigDecimal commissionPerUnit;
	private BigDecimal accrualAmount;
	private BigDecimal feeAmount;

	/**
	 * Accounting notional has been added because it is defined on currency trades, and so is need for TradeEntry's of type currency
	 */
	private BigDecimal accountingNotional;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeSource getTradeSource() {
		return this.tradeSource;
	}


	public void setTradeSource(TradeSource tradeSource) {
		this.tradeSource = tradeSource;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public BigDecimal getQuantityIntended() {
		return this.quantityIntended;
	}


	public void setQuantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
	}


	public BigDecimal getExpectedUnitPrice() {
		return this.expectedUnitPrice;
	}


	public void setExpectedUnitPrice(BigDecimal expectedUnitPrice) {
		this.expectedUnitPrice = expectedUnitPrice;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getAccrualAmount() {
		return this.accrualAmount;
	}


	public void setAccrualAmount(BigDecimal accrualAmount) {
		this.accrualAmount = accrualAmount;
	}


	public BigDecimal getAccountingNotional() {
		return this.accountingNotional;
	}


	public void setAccountingNotional(BigDecimal accountingNotional) {
		this.accountingNotional = accountingNotional;
	}
}
