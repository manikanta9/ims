package com.clifton.trade.group.strangle.calculator;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.strangle.TradeGroupStrangle;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>TradeGroupOptionStrangleByClientAccountCalculator</code> is a {@link TradeGroupStrangleCalculator} for options
 * that strangles trades by client accounts. The strangle will have 0, 1, or 2 tails that includes the excess of put/call
 * quantities calculated for each client account. The excess put/call quantity for each client account will be added to the
 * strangle tail for the put or call security respectively.
 *
 * @author NickK
 */
public class TradeGroupOptionStrangleByClientAccountCalculator extends BaseTradeGroupOptionStrangleCalculator {

	@Override
	protected TradeGroupStrangle calculateTradeGroupOptionStrangle(StrangleTradeList strangleTradeList) {
		Map<String, List<Trade>> strangleListMap = new HashMap<>(3);
		strangleListMap.put(STRANGLE_TRADE_LIST_KEY, new ArrayList<>());
		strangleListMap.put(CALL_TAIL_TRADE_LIST_KEY, new ArrayList<>());
		strangleListMap.put(PUT_TAIL_TRADE_LIST_KEY, new ArrayList<>());
		for (InvestmentAccount investmentAccount : strangleTradeList) {
			ClientAccountTradeList clientAccountTradeList = strangleTradeList.getClientAccountTradeList(investmentAccount);
			TradeGroupStrangle clientAccountStrangle = calculateClientAccountStrangleGrouping(strangleTradeList, clientAccountTradeList);
			if (clientAccountStrangle.hasCallTail()) {
				strangleListMap.get(CALL_TAIL_TRADE_LIST_KEY).addAll(clientAccountStrangle.getCallTailGroup().getTradeList());
			}
			else if (clientAccountStrangle.hasPutTail()) {
				strangleListMap.get(PUT_TAIL_TRADE_LIST_KEY).addAll(clientAccountStrangle.getPutTailGroup().getTradeList());
			}
			strangleListMap.get(STRANGLE_TRADE_LIST_KEY).addAll(clientAccountStrangle.getTradeList());
		}
		TradeGroup callTradeGroup = populateTailTradeGroup(strangleListMap.get(CALL_TAIL_TRADE_LIST_KEY));
		TradeGroup putTradeGroup = populateTailTradeGroup(strangleListMap.get(PUT_TAIL_TRADE_LIST_KEY));
		return populateStrangleTradeGroup(strangleListMap.get(STRANGLE_TRADE_LIST_KEY), strangleTradeList, callTradeGroup, putTradeGroup);
	}


	private TradeGroupStrangle calculateClientAccountStrangleGrouping(StrangleTradeList strangleTradeList, ClientAccountTradeList clientAccountTradeList) {
		boolean putHeavy = clientAccountTradeList.isPutHeavy();
		BigDecimal strangleQuantity = putHeavy ? clientAccountTradeList.getCallQuantity() : clientAccountTradeList.getPutQuantity();
		List<Trade>[] strangleLists = splitAndGroupTrades(clientAccountTradeList, putHeavy, strangleQuantity, true);
		TradeGroup tailTradeGroup = populateTailTradeGroup(strangleLists[1]);
		return putHeavy ? populateStrangleTradeGroup(strangleLists[0], strangleTradeList, null, tailTradeGroup)
				: populateStrangleTradeGroup(strangleLists[0], strangleTradeList, tailTradeGroup, null);
	}
}
