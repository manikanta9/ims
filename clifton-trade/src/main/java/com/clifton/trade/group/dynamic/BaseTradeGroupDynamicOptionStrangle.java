package com.clifton.trade.group.dynamic;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.group.strangle.TradeGroupStrangle;
import com.clifton.trade.group.strangle.TradeGroupStrangleCalculatorMethods;

import java.math.BigDecimal;


/**
 * <code>BaseTradeGroupDynamicOptionStrangle</code> is a base {@link TradeGroupDynamic} that
 * contains common functionality for Strangle grouping of Option Trades.
 *
 * @author NickK
 */
public abstract class BaseTradeGroupDynamicOptionStrangle<D extends TradeGroupDynamicOptionStrangleDetail> extends TradeGroupDynamic<D> {

	private TradeGroupStrangle strangleGroup;

	private TradeGroupStrangleCalculatorMethods strangleCalculatorMethod;

	boolean longPositions;

	InvestmentSecurity putSecurity;
	InvestmentSecurity callSecurity;

	/**
	 * Trade FillPrices. The client will pass all four parameters with the Trade Group Action for
	 * FILL_TRADES. The (put|call)FillPrice will be applied to all respective Trades of the Option Roll,
	 * which should include the child strangle or tail/outright trades. The tail trades will inherit the
	 * fill price of (put|call)FillPrice if the tail amount is not provided.
	 */
	private BigDecimal putFillPrice;
	private BigDecimal callFillPrice;
	private BigDecimal putTailFillPrice;
	private BigDecimal callTailFillPrice;

	/**
	 * Trade Commissions. The client will pass in all four parameters with the Trade Group Action for
	 * SAVE_TRADES. The (put|call)CommissionPerUnit will be applied to all respective Trades of the Option Roll,
	 * which should include the child strangle or tail/outright trades. The tail trades will inherit the
	 * commission amount of (put|call)CommissionPerUnit if the tail amount is not provided.
	 */
	private BigDecimal putCommissionPerUnit;
	private BigDecimal callCommissionPerUnit;
	private BigDecimal putTailCommissionPerUnit;
	private BigDecimal callTailCommissionPerUnit;
	// Trade updates can be made for Securities, Executing Brokers, and Commission Overrides. The commission overrides
	// only get updated if the value is non-null. This flag enables removal of commission overrides from the UI.
	private boolean enableCommissionRemoval;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeGroupStrangle getStrangleGroup() {
		return this.strangleGroup;
	}


	public void setStrangleGroup(TradeGroupStrangle strangleGroup) {
		this.strangleGroup = strangleGroup;
	}


	public TradeGroupStrangleCalculatorMethods getStrangleCalculatorMethod() {
		return this.strangleCalculatorMethod;
	}


	public void setStrangleCalculatorMethod(TradeGroupStrangleCalculatorMethods strangleCalculatorMethod) {
		this.strangleCalculatorMethod = strangleCalculatorMethod;
	}


	public boolean isLongPositions() {
		return this.longPositions;
	}


	public void setLongPositions(boolean longPositions) {
		this.longPositions = longPositions;
	}


	public InvestmentSecurity getPutSecurity() {
		return this.putSecurity;
	}


	public void setPutSecurity(InvestmentSecurity putSecurity) {
		this.putSecurity = putSecurity;
	}


	public InvestmentSecurity getCallSecurity() {
		return this.callSecurity;
	}


	public void setCallSecurity(InvestmentSecurity callSecurity) {
		this.callSecurity = callSecurity;
	}


	public BigDecimal getPutFillPrice() {
		return this.putFillPrice;
	}


	public void setPutFillPrice(BigDecimal fillPrice) {
		this.putFillPrice = fillPrice;
	}


	public BigDecimal getCallFillPrice() {
		return this.callFillPrice;
	}


	public void setCallFillPrice(BigDecimal fillPrice) {
		this.callFillPrice = fillPrice;
	}


	public BigDecimal getPutTailFillPrice() {
		return this.putTailFillPrice;
	}


	public void setPutTailFillPrice(BigDecimal fillPrice) {
		this.putTailFillPrice = fillPrice;
	}


	public BigDecimal getCallTailFillPrice() {
		return this.callTailFillPrice;
	}


	public void setCallTailFillPrice(BigDecimal fillPrice) {
		this.callTailFillPrice = fillPrice;
	}


	public BigDecimal getPutCommissionPerUnit() {
		return this.putCommissionPerUnit;
	}


	public void setPutCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.putCommissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getCallCommissionPerUnit() {
		return this.callCommissionPerUnit;
	}


	public void setCallCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.callCommissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getPutTailCommissionPerUnit() {
		return this.putTailCommissionPerUnit;
	}


	public void setPutTailCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.putTailCommissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getCallTailCommissionPerUnit() {
		return this.callTailCommissionPerUnit;
	}


	public void setCallTailCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.callTailCommissionPerUnit = commissionPerUnit;
	}


	public boolean isEnableCommissionRemoval() {
		return this.enableCommissionRemoval;
	}


	public void setEnableCommissionRemoval(boolean enableCommissionRemoval) {
		this.enableCommissionRemoval = enableCommissionRemoval;
	}
}
