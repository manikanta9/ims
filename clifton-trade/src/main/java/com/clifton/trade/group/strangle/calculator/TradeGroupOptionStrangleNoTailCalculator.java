package com.clifton.trade.group.strangle.calculator;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;
import com.clifton.trade.group.strangle.TradeGroupStrangle;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <code>TradeGroupOptionStrangleNoTailCalculator</code> is a {@link TradeGroupStrangleCalculator} for options
 * that includes all trades in a group as if no strangle is being created.
 *
 * @author NickK
 */
public class TradeGroupOptionStrangleNoTailCalculator extends BaseTradeGroupOptionStrangleCalculator {

	@Override
	protected TradeGroupStrangle calculateTradeGroupOptionStrangle(StrangleTradeList strangleTradeList) {
		List<Trade> strangleList = new ArrayList<>();
		for (InvestmentAccount investmentAccount : strangleTradeList) {
			ClientAccountTradeList clientAccountTradeList = strangleTradeList.getClientAccountTradeList(investmentAccount);
			List<Trade> tradeList = clientAccountTradeList.getTradeList().stream().map(ClientAccountTrade::getTrade).collect(Collectors.toList());
			strangleList.addAll(tradeList);
		}
		return populateStrangleTradeGroup(strangleList, strangleTradeList, null, null);
	}
}
