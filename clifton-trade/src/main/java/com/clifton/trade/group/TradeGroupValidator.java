package com.clifton.trade.group;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeGroupValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class TradeGroupValidator extends SelfRegisteringDaoValidator<TradeGroup> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(TradeGroup bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		TradeGroupType type = bean.getTradeGroupType();
		if (type.isSecurityRequired()) {
			ValidationUtils.assertNotNull(bean.getInvestmentSecurity(), "Trade Groups of Type [" + type.getName() + "] require an investment security.", "investmentSecurity.id");
		}
		if (type.isSecondarySecurityRequired()) {
			ValidationUtils.assertNotNull(bean.getSecondaryInvestmentSecurity(), "Trade Groups of Type [" + type.getName() + "] require a secondary investment security.",
					"secondaryInvestmentSecurity.id");
		}

		if (type.isParentGroupRequired()) {
			ValidationUtils.assertNotNull(bean.getParent(), "Trade Groups of Type [" + type.getName() + "] require a parent group.  There is no parent group assigned to this group.");
		}
	}
}
