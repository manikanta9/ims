package com.clifton.trade.group.dynamic.calculators;

import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.execution.TradeExecutionTypeService;
import com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRoll;
import com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRollDetail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;


/**
 * <code>TradeGroupDynamicForwardRollCalculator</code> is used for Rolling maturing Currency Forward positions.
 * This calculator looks up Forwards positions for the criteria specified in the provided {@link TradeGroupDynamicForwardRoll}
 * that can be used for creating trades, reviewing trade impact, and performing TradeGroup actions.
 *
 * @author NickK
 */
public class TradeGroupDynamicForwardRollCalculator extends BaseTradeGroupDynamicMaturityCalculator<TradeGroupDynamicForwardRoll, TradeGroupDynamicForwardRollDetail> {

	private TradeService tradeService;
	private TradeExecutionTypeService tradeExecutionTypeService;
	private TradeDestinationService tradeDestinationService;


	////////////////////////////////////////////////////////////////////////////////
	/////////////   BaseTradeGroupDynamicMaturityCalculator Methods   //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected TradeGroupDynamicForwardRoll createTradeGroupDynamicMaturity() {
		return new TradeGroupDynamicForwardRoll();
	}


	@Override
	protected void populateTradeType(TradeGroupDynamicForwardRoll rollCommand) {
		TradeType tradeType = rollCommand.getTradeType();
		if (tradeType == null || tradeType.getId() == null) {
			rollCommand.setTradeType(getTradeService().getTradeTypeByName(TradeType.FORWARDS));
		}
	}


	@Override
	protected AccountingBalanceSearchForm createAccountingBalanceSearchFormTemplate(TradeGroupDynamicForwardRoll rollCommand) {
		AccountingBalanceSearchForm searchForm = super.createAccountingBalanceSearchFormTemplate(rollCommand);
		if (rollCommand.isIncludePendingTransactions()) {
			searchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_MERGE);
		}
		return searchForm;
	}


	@Override
	protected TradeGroupDynamicForwardRollDetail createTradeGroupDynamicDetailFromAccountingBalanceValue(TradeGroupDynamicForwardRoll rollCommand, AccountingBalanceValue accountingBalanceValue, Map<Object, Object> detailProcessingCache) {
		TradeGroupDynamicForwardRollDetail detail = new TradeGroupDynamicForwardRollDetail();
		populateForwardRollDetailFromAccountingBalanceValue(detail, accountingBalanceValue);
		populateForwardRollClosePositionDetails(detail);
		populateForwardRollNewPositionDetails(rollCommand, detail, detailProcessingCache);
		return detail;
	}


	@Override
	protected void populateTradeGroupDynamicDetailFromTrades(TradeGroupDynamicForwardRoll rollCommand, List<Trade> tradeList) {
		// Map trades by client and collect filter criterion for closing trade positions, so one db lookup can gather all positions
		Set<Integer> clientAccountIdSet = new HashSet<>();
		Set<Integer> holdingAccountIdSet = new HashSet<>();
		Set<Integer> closingSecurityIdSet = new HashSet<>();
		Map<InvestmentAccount, Map<Boolean, List<Trade>>> clientCloseRollTradeListMap = new HashMap<>();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			Map<Boolean, List<Trade>> clientCloseRollMap = clientCloseRollTradeListMap.computeIfAbsent(trade.getClientInvestmentAccount(), key -> new HashMap<>());
			Boolean close = isSecurityBetweenMaturityRange(trade.getInvestmentSecurity(), rollCommand);
			clientCloseRollMap.computeIfAbsent(close, key -> new ArrayList<>()).add(trade);
			if (close) {
				clientAccountIdSet.add(trade.getClientInvestmentAccount().getId());
				if (trade.getHoldingInvestmentAccount() != null) {
					// pending trades may not have a holding account populated
					holdingAccountIdSet.add(trade.getHoldingInvestmentAccount().getId());
				}
				closingSecurityIdSet.add(trade.getInvestmentSecurity().getId());
			}
		}

		// get closing trade position balances and group by unique keys for detail population
		AccountingBalanceSearchForm searchForm = createAccountingBalanceSearchFormTemplate(rollCommand);
		searchForm.setClientInvestmentAccountIds(clientAccountIdSet.toArray(new Integer[0]));
		searchForm.setInvestmentSecurityIds(closingSecurityIdSet.toArray(new Integer[0]));
		if (!holdingAccountIdSet.isEmpty()) {
			searchForm.setHoldingInvestmentAccountIds(holdingAccountIdSet.toArray(new Integer[0]));
		}
		List<AccountingBalanceValue> maturingPositionList = getAccountingValuationService().getAccountingBalanceValueList(searchForm);
		Map<String, AccountingBalanceValue> clientMaturingPositionMap = BeanUtils.getBeanMap(maturingPositionList,
				position -> BeanUtils.createKeyFromBeans(position.getClientInvestmentAccount(), position.getInvestmentSecurity(), position.getHoldingInvestmentAccount()));
		Function<Trade, String> tradeMaturityPositionFunction = trade -> BeanUtils.createKeyFromBeans(trade.getClientInvestmentAccount(), trade.getInvestmentSecurity(), trade.getHoldingInvestmentAccount());

		for (Map.Entry<InvestmentAccount, Map<Boolean, List<Trade>>> clientTradeEntry : CollectionUtils.getIterable(clientCloseRollTradeListMap.entrySet())) {
			Map<Boolean, List<Trade>> clientMaturingForwardTradeMap = clientTradeEntry.getValue();
			populateForwardRollDetailListWithClosingLegTrades(rollCommand, clientMaturingForwardTradeMap.get(Boolean.TRUE), tradeMaturityPositionFunction, clientMaturingPositionMap);
			populateForwardRollDetailListWithRollLegTrades(rollCommand, clientMaturingForwardTradeMap.get(Boolean.FALSE));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////       BaseTradeGroupDynamicCalculator Methods       //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateTradeGroupForSavingNewTradeGroupDynamic(TradeGroupDynamicForwardRoll rollCommand) {
		populateTradeType(rollCommand);
		populateTradeGroupDynamicTradeListFromDetailList(rollCommand);
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnEntrySave(TradeGroupDynamicForwardRoll rollCommand, TradeGroupDynamicForwardRollDetail detail) {
		Trade closingTrade = populateForwardRollWithForwardRollDetailTrade(rollCommand, detail, true);
		Trade rollTrade = populateForwardRollWithForwardRollDetailTrade(rollCommand, detail, false);
		ValidationUtils.assertFalse(rollTrade != null && closingTrade == null, "Cannot roll to a new Currency Forward without a closing Trade on the maturing position.");
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(TradeGroupDynamicForwardRoll rollCommand, TradeGroupDynamicForwardRollDetail detail) {
		if (detail.getCloseTrade() != null) {
			rollCommand.getTradeList().add(detail.getCloseTrade());
		}
		if (detail.getRollTrade() != null) {
			rollCommand.getTradeList().add(detail.getRollTrade());
		}
	}


	@Override
	protected boolean isAccountingBalanceValueApplicable(TradeGroupDynamicForwardRoll maturityCommand, AccountingBalanceValue accountingBalanceValue) {
		// True conditions are:  The required account type for TradeExecutionType matches the holding account type of the balance value OR
		// the required account type value is null (indicating non-CLS), and the holding account type is not OTC CLS
		TradeExecutionType executionType = maturityCommand.getTradeExecutionType();
		boolean requireCls = executionType != null && executionType.getRequiredHoldingInvestmentAccountType() != null
				&& InvestmentAccountType.OTC_CLS.equals(executionType.getRequiredHoldingInvestmentAccountType().getName());
		InvestmentAccount holdingAccount = accountingBalanceValue.getHoldingInvestmentAccount();
		boolean holdingAccountTypeCls = holdingAccount != null && holdingAccount.getType() != null && InvestmentAccountType.OTC_CLS.equals(holdingAccount.getType().getName());

		if (requireCls == holdingAccountTypeCls) {
			return super.isAccountingBalanceValueApplicable(maturityCommand, accountingBalanceValue);
		}

		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////            Forward Roll Calculator Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	private TradeGroupDynamicForwardRollDetail populateForwardRollDetailFromAccountingBalanceValue(TradeGroupDynamicForwardRollDetail forwardRollDetail, AccountingBalanceValue accountingBalanceValue) {
		forwardRollDetail.setClientAccount(accountingBalanceValue.getClientInvestmentAccount());
		forwardRollDetail.setHoldingAccount(accountingBalanceValue.getHoldingInvestmentAccount());
		forwardRollDetail.setMaturingSecurity(accountingBalanceValue.getInvestmentSecurity());
		forwardRollDetail.setMaturingAmount(accountingBalanceValue.getQuantity());
		forwardRollDetail.setSettlementAmount(accountingBalanceValue.getLocalNotional());
		forwardRollDetail.setMaturingPrice(accountingBalanceValue.getPrice());
		forwardRollDetail.setPending(accountingBalanceValue.isPendingActivity());
		return forwardRollDetail;
	}


	private void populateForwardRollClosePositionDetails(TradeGroupDynamicForwardRollDetail forwardRollDetail) {
		forwardRollDetail.setCloseGivenCurrency(forwardRollDetail.getMaturingSecurity().getInstrument().getUnderlyingInstrument().getTradingCurrency());
		forwardRollDetail.setCloseGivenAmount(MathUtils.abs(forwardRollDetail.getMaturingAmount()));
		forwardRollDetail.setCloseSecurity(forwardRollDetail.getMaturingSecurity());
		forwardRollDetail.setCloseBuy(!forwardRollDetail.isLongPosition());
	}


	private void populateForwardRollNewPositionDetails(TradeGroupDynamicForwardRoll rollCommand, TradeGroupDynamicForwardRollDetail forwardRollDetail, Map<Object, Object> detailProcessingCache) {
		// look up new forward
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setActiveOnDate(rollCommand.getTradeDate());
		securitySearchForm.setInstrumentId(forwardRollDetail.getMaturingSecurity().getInstrument().getId());
		securitySearchForm.setEndDate(rollCommand.getNewMaturityDate());

		Function<List<InvestmentSecurity>, InvestmentSecurity> securitySelector = securityList -> {
			// get first future maturing after specified maturing range, use null if there is not one
			for (InvestmentSecurity rollSecurity : CollectionUtils.getIterable(securityList)) {
				if (DateUtils.isDateAfter(rollSecurity.getEndDate(), rollCommand.getEndMaturityDate())) {
					return rollSecurity;
				}
			}
			return null;
		};
		InvestmentSecurity defaultNewCurrencyForward = super.getInvestmentSecurity(securitySearchForm, detailProcessingCache, securitySelector);

		if (defaultNewCurrencyForward != null) {
			forwardRollDetail.setRollSecurity(defaultNewCurrencyForward);
		}
		else {
			// populate roll given currency with currencies of the maturing security.
			defaultNewCurrencyForward = forwardRollDetail.getMaturingSecurity();
		}
		if (BooleanUtils.isTrue(rollCommand.isDefaultRollToBaseCurrency())) {
			forwardRollDetail.setRollGivenCurrency(defaultNewCurrencyForward.getInstrument().getTradingCurrency());
			forwardRollDetail.setRollBuy(!forwardRollDetail.isLongPosition());
		}
		else {
			forwardRollDetail.setRollGivenCurrency(defaultNewCurrencyForward.getInstrument().getUnderlyingInstrument().getTradingCurrency());
			forwardRollDetail.setRollBuy(forwardRollDetail.isLongPosition());
		}

		populateRollTradeHoldingAccount(rollCommand, forwardRollDetail);
	}


	private void populateRollTradeHoldingAccount(TradeGroupDynamicForwardRoll rollCommand, TradeGroupDynamicForwardRollDetail forwardRollDetail) {
		InvestmentAccountSearchForm holdingAccountSearchForm = new InvestmentAccountSearchForm();
		holdingAccountSearchForm.setOurAccount(Boolean.FALSE);
		holdingAccountSearchForm.setMainPurpose(TradeService.TRADING_PURPOSE_NAME_PREFIX + forwardRollDetail.getMaturingSecurity().getInstrument().getHierarchy().getInvestmentType().getName());
		holdingAccountSearchForm.setMainPurposeActiveOnDate(rollCommand.getTradeDate());
		holdingAccountSearchForm.setMainAccountId(forwardRollDetail.getClientAccount().getId());
		if (forwardRollDetail.getHoldingAccount() != null) {
			// pending trades may not have a holding account populated
			holdingAccountSearchForm.setIssuingCompanyId(forwardRollDetail.getHoldingAccount().getIssuingCompany().getId());
		}
		holdingAccountSearchForm.setOrderBy("workflowStateEffectiveStartDate:DESC");
		List<InvestmentAccount> holdingAccountList = getInvestmentAccountService().getInvestmentAccountList(holdingAccountSearchForm);
		if (!CollectionUtils.isEmpty(holdingAccountList)) {
			if (forwardRollDetail.getRollTrade() == null) {
				forwardRollDetail.setRollTrade(new Trade());
			}
			forwardRollDetail.getRollTrade().setHoldingInvestmentAccount(holdingAccountList.get(0));
		}
	}


	private void populateForwardRollDetailListWithClosingLegTrades(TradeGroupDynamicForwardRoll rollCommand, List<Trade> closingTradeList, Function<Trade, String> tradeMaturityPositionFunction, Map<String, AccountingBalanceValue> maturingPositionMap) {
		for (Trade closingTrade : CollectionUtils.getIterable(closingTradeList)) {
			TradeGroupDynamicForwardRollDetail detail = new TradeGroupDynamicForwardRollDetail();
			AccountingBalanceValue maturingPosition = maturingPositionMap.get(tradeMaturityPositionFunction.apply(closingTrade));
			if (maturingPosition != null) {
				populateForwardRollDetailFromAccountingBalanceValue(detail, maturingPosition);
			}
			else {
				populateForwardRollDetailFromTrade(detail, closingTrade);
			}
			detail.setCloseTrade(closingTrade);
			rollCommand.getDetailList().add(detail);
		}
	}


	/**
	 * In the event that a position has been closed, the closing leg Trade may not find a matching open position for the maturing Currency Forward
	 * that can be used for populating the Forward Roll Detail. Thus, use the Trade to populate the details.
	 * <p>
	 * An example of this would be after the closing leg Trade is booked and the original maturing position has been closed.
	 */
	private TradeGroupDynamicForwardRollDetail populateForwardRollDetailFromTrade(TradeGroupDynamicForwardRollDetail forwardRollDetail, Trade trade) {
		forwardRollDetail.setClientAccount(trade.getClientInvestmentAccount());
		forwardRollDetail.setHoldingAccount(trade.getHoldingInvestmentAccount());
		forwardRollDetail.setMaturingSecurity(trade.getInvestmentSecurity());
		forwardRollDetail.setMaturingAmount(trade.isBuy() ? MathUtils.negate(trade.getQuantity()) : trade.getQuantity());
		forwardRollDetail.setSettlementAmount(trade.isBuy() ? MathUtils.negate(trade.getAccountingNotional()) : trade.getAccountingNotional());
		return forwardRollDetail;
	}


	private void populateForwardRollDetailListWithRollLegTrades(TradeGroupDynamicForwardRoll rollCommand, List<Trade> rollTradeList) {
		/*
		 * Iterate the Roll Trades to match them to a detail row. Remove those that find a match, so Trades left
		 * can be applied by a followup attempt.
		 *
		 * Attempt to match Roll Trades with a detail row twice. The first attempt will match the Roll Trade's Client Account,
		 * Security Instrument, and Holding Account with the Closing Trade of the maturing position, and the second attempt
		 * matching the Client Account and Security Instrument only.
		 */
		populateForwardRollDetailListWithRollLegTrades(rollCommand, rollTradeList, true);
		populateForwardRollDetailListWithRollLegTrades(rollCommand, rollTradeList, false);
		/*
		 * Rather than throwing a validation exception if not all Roll Trades find a matching detail, generate a detail for each to allow the UI to load.
		 * This state can occur if a closing leg trade is cancelled or a position was double traded.
		 */
		for (Trade rollTrade : CollectionUtils.getIterable(rollTradeList)) {
			TradeGroupDynamicForwardRollDetail detail = new TradeGroupDynamicForwardRollDetail();
			detail.setClientAccount(rollTrade.getClientInvestmentAccount());
			detail.setHoldingAccount(rollTrade.getHoldingInvestmentAccount());
			detail.setRollTrade(rollTrade);
			rollCommand.getDetailList().add(detail);
		}
	}


	/**
	 * Trades that match a Detail Row of the Forward Roll will be added to the matching Detail Row and removed from the provided Roll Trade List.
	 * <p>
	 * If requested with Holding Account matching, a Trade's Investment Security Instrument and Holding Account will need to match a Detail Row.
	 * Otherwise, only Investment Security Instrument will be required for matching a Detail Row.
	 */
	private void populateForwardRollDetailListWithRollLegTrades(TradeGroupDynamicForwardRoll rollCommand, List<Trade> rollTradeList, boolean matchHoldingAccount) {
		Iterator<Trade> rollForwardTradeIterator = CollectionUtils.getIterable(rollTradeList).iterator();
		while (rollForwardTradeIterator.hasNext()) {
			Trade rollForwardTrade = rollForwardTradeIterator.next();
			for (TradeGroupDynamicForwardRollDetail detail : rollCommand.getDetailList()) {
				if (doesForwardRollDetailMatchRollTrade(detail, rollForwardTrade, matchHoldingAccount)) {
					ValidationUtils.assertNull(detail.getRollTrade(), "Encountered more than one Roll Trade that matched a Maturing Currency Forward Roll Position [" + detail + "]");
					detail.setRollTrade(rollForwardTrade);
					rollForwardTradeIterator.remove();
					break;
				}
			}
		}
	}


	private boolean doesForwardRollDetailMatchRollTrade(TradeGroupDynamicForwardRollDetail forwardRollDetail, Trade rollTrade, boolean matchHoldingAccount) {
		if (rollTrade.getClientInvestmentAccount().equals(forwardRollDetail.getClientAccount())) {
			Trade closeTrade = forwardRollDetail.getCloseTrade();
			if (closeTrade.getInvestmentSecurity().getInstrument().equals(rollTrade.getInvestmentSecurity().getInstrument())) {
				return !matchHoldingAccount || closeTrade.getHoldingInvestmentAccount().equals(rollTrade.getHoldingInvestmentAccount());
			}
		}
		return false;
	}


	/**
	 * Returns a populated {@link Trade} for the close or roll trade if one is determined to be added to the Forward roll trade group. Null
	 * is returned if a trade was not added due to no given amount specified for the close or roll trade.
	 */
	private Trade populateForwardRollWithForwardRollDetailTrade(TradeGroupDynamicForwardRoll rollCommand, TradeGroupDynamicForwardRollDetail forwardRollDetail, boolean close) {
		if (!MathUtils.isNullOrZero(close ? forwardRollDetail.getCloseGivenAmount() : forwardRollDetail.getRollGivenAmount())) {
			Trade trade = createTradeFromForwardRollDetail(rollCommand, forwardRollDetail, close);
			if (trade != null) {
				rollCommand.addTrade(trade);
				return trade;
			}
		}
		return null;
	}


	private Trade createTradeFromForwardRollDetail(TradeGroupDynamicForwardRoll rollCommand, TradeGroupDynamicForwardRollDetail forwardRollDetail, boolean close) {
		Trade trade;
		if (close) {
			if (MathUtils.isNullOrZero(forwardRollDetail.getCloseGivenAmount())) {
				return null;
			}
			trade = forwardRollDetail.getCloseTrade() == null ? new Trade() : forwardRollDetail.getCloseTrade();
			trade.setAverageUnitPrice(forwardRollDetail.getMaturingPrice());
			populateTradeDirectionAndAmount(trade, forwardRollDetail.getCloseSecurity(), forwardRollDetail, close);
		}
		else {
			if (MathUtils.isNullOrZero(forwardRollDetail.getRollGivenAmount())) {
				return null;
			}
			ValidationUtils.assertFalse(isSecurityBetweenMaturityRange(forwardRollDetail.getRollSecurity(), rollCommand), "Cannot roll to a Currency Forward that is within the specified Maturity Range.");
			trade = forwardRollDetail.getRollTrade() == null ? new Trade() : forwardRollDetail.getRollTrade();
			populateTradeDirectionAndAmount(trade, forwardRollDetail.getRollSecurity(), forwardRollDetail, close);
		}
		trade.setTradeExecutionType(rollCommand.getTradeExecutionType());
		trade.setTradeType(rollCommand.getTradeType());
		trade.setTraderUser(rollCommand.getTraderUser());
		trade.setTradeDate(rollCommand.getTradeDate());
		trade.setSettlementDate(rollCommand.getTradeDate()); // taken from Currency Forward trade creation window
		trade.setClientInvestmentAccount(forwardRollDetail.getClientAccount());
		trade.setTradeDestination(rollCommand.getTradeDestination());
		if (close || rollCommand.isDefaultRollToSameBroker()) {
			// Use the Holding Account and Executing Broker of the Maturing Position for the Close and New Position Trades.
			if (trade.getHoldingInvestmentAccount() == null) {
				trade.setHoldingInvestmentAccount(forwardRollDetail.getHoldingAccount());
			}
			// CLS trades should not get executing broker set.
			if (trade.getExecutingBrokerCompany() == null && !trade.getTradeExecutionType().isClsTrade()) {
				trade.setExecutingBrokerCompany(forwardRollDetail.getHoldingAccount().getIssuingCompany());
			}
			if (trade.getTradeDestination() == null) {
				trade.setTradeDestination(getTradeDestination(trade.getInvestmentSecurity(), trade.getExecutingBrokerCompany(), trade.getTradeDate()));
			}
		}
		return trade;
	}


	private void populateTradeDirectionAndAmount(Trade trade, InvestmentSecurity currencyForward, TradeGroupDynamicForwardRollDetail forwardRollDetail, boolean close) {
		InvestmentSecurity givenCurrency;
		BigDecimal givenAmount;
		boolean givenDirection;
		if (close) {
			givenCurrency = forwardRollDetail.getCloseGivenCurrency();
			givenAmount = forwardRollDetail.getCloseGivenAmount();
			givenDirection = forwardRollDetail.getCloseBuy();
		}
		else {
			givenCurrency = forwardRollDetail.getRollGivenCurrency();
			givenAmount = forwardRollDetail.getRollGivenAmount();
			givenDirection = forwardRollDetail.getRollBuy();
		}

		trade.setInvestmentSecurity(currencyForward);
		/*
		 * Trade direction is entered in relation to the given currency. If the given currency is the base currency,
		 * the trade direction of the Forward being traded will be the opposite of the given direction.
		 */
		if (currencyForward.getInstrument().getUnderlyingInstrument().getTradingCurrency().equals(givenCurrency)) {
			trade.setBuy(givenDirection);
			trade.setQuantityIntended(givenAmount);
			trade.setPayingSecurity(currencyForward.getInstrument().getTradingCurrency());
		}
		else {
			trade.setBuy(!givenDirection);
			trade.setAccountingNotional(givenAmount);
			trade.setPayingSecurity(currencyForward.getInstrument().getUnderlyingInstrument().getTradingCurrency());
		}
	}


	private TradeDestination getTradeDestination(InvestmentSecurity security, BusinessCompany executingBroker, Date tradeDate) {
		TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand(security.getId(), executingBroker.getId(), tradeDate);
		TradeDestinationMapping tradeDestinationMapping = getTradeDestinationService().getTradeDestinationMappingByCommand(command);
		return tradeDestinationMapping.getTradeDestination();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeExecutionTypeService getTradeExecutionTypeService() {
		return this.tradeExecutionTypeService;
	}


	public void setTradeExecutionTypeService(TradeExecutionTypeService tradeExecutionTypeService) {
		this.tradeExecutionTypeService = tradeExecutionTypeService;
	}
}
