package com.clifton.trade.group.search;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.setup.group.InvestmentGroupItemInstrument;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.Set;


/**
 * <code>TradeGroupSearchFormConfigurer</code> extends {@link WorkflowAwareSearchFormConfigurer}
 * and configures {@link TradeGroupSearchForm}.
 *
 * @author NickK
 */
public class TradeGroupSearchFormConfigurer extends WorkflowAwareSearchFormConfigurer {

	private final TradeGroupService tradeGroupService;


	public TradeGroupSearchFormConfigurer(TradeGroupSearchForm searchForm, WorkflowDefinitionService workflowDefinitionService, TradeGroupService tradeGroupService) {
		super(searchForm, workflowDefinitionService);
		this.tradeGroupService = tradeGroupService;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		TradeGroupSearchForm searchForm = (TradeGroupSearchForm) getSortableSearchForm();
		configureInvestmentGroup(criteria, searchForm);
		configureClientInvestmentAccountGroup(criteria, searchForm);
		configureHoldingInvestmentAccountGroup(criteria, searchForm);
		configureIncludeBeginsWithTradeGroupTypeNames(criteria, searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void configureInvestmentGroup(Criteria criteria, TradeGroupSearchForm searchForm) {
		if (searchForm.getInvestmentGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentGroupItemInstrument.class, "igi");
			sub.setProjection(Projections.property("referenceTwo.id"));
			sub.setProjection(Projections.id());
			sub.createAlias("referenceOne", "groupItem");
			sub.add(Restrictions.eq("groupItem.group.id", searchForm.getInvestmentGroupId()));
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("tradeList.investmentSecurity.instrument", criteria) + ".id"));
			criteria.add(Subqueries.exists(sub));
		}
	}


	private void configureClientInvestmentAccountGroup(Criteria criteria, TradeGroupSearchForm searchForm) {
		if (searchForm.getClientInvestmentAccountGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "iag");
			sub.setProjection(Projections.id());
			sub.createAlias("referenceOne", "group");
			sub.createAlias("referenceTwo", "acct");
			sub.add(Restrictions.eqProperty("acct.id", getPathAlias("tradeList.clientInvestmentAccount", criteria) + ".id"));
			sub.add(Restrictions.eq("group.id", searchForm.getClientInvestmentAccountGroupId()));
			criteria.add(Subqueries.exists(sub));
		}
	}


	private void configureHoldingInvestmentAccountGroup(Criteria criteria, TradeGroupSearchForm searchForm) {
		if (searchForm.getHoldingInvestmentAccountGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "hag");
			sub.setProjection(Projections.id());
			sub.add(Restrictions.eqProperty("referenceTwo.id", getPathAlias("tradeList.holdingInvestmentAccount", criteria) + ".id"));
			sub.add(Restrictions.eq("referenceOne.id", searchForm.getHoldingInvestmentAccountGroupId()));
			criteria.add(Subqueries.exists(sub));
		}
	}


	private void configureIncludeBeginsWithTradeGroupTypeNames(Criteria criteria, TradeGroupSearchForm searchForm) {
		if (searchForm.getIncludeBeginsWithTradeGroupTypeNames() != null) {
			Set<String> prefixSet = CollectionUtils.createHashSet(searchForm.getIncludeBeginsWithTradeGroupTypeNames());
			Short[] tradeGroupTypeIdArray = this.tradeGroupService.getTradeGroupTypeIdListByNamePrefix(prefixSet).toArray(new Short[0]);
			ValidationUtils.assertFalse(tradeGroupTypeIdArray.length == 0, "No trade group types were found for the given names: " + ArrayUtils.toString(searchForm.getIncludeBeginsWithTradeGroupTypeNames()));
			criteria.add(Restrictions.in("tradeGroupType.id", tradeGroupTypeIdArray));
		}
	}
}
