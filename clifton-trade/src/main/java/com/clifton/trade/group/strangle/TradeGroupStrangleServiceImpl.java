package com.clifton.trade.group.strangle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.group.search.TradeGroupSearchForm;
import com.clifton.trade.group.strangle.validate.TradeGroupStrangleTradeValidator;
import com.clifton.trade.search.TradeSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * The <code>TradeGroupStrangleServiceImpl</code> class provides basic implementation of the TradeGroupStrangleService interface.
 *
 * @author NickK
 */
@Service
public class TradeGroupStrangleServiceImpl implements TradeGroupStrangleService {

	private TradeService tradeService;

	private TradeGroupService tradeGroupService;

	private SystemColumnValueHandler systemColumnValueHandler;


	////////////////////////////////////////////////////////////////////////////
	//////               Trade Group Strangle Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeGroupStrangle populateTradeGroupStranglePreview(List<Trade> tradeList, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod) {
		return populateTradeGroupStrangle(new TradeGroupStrangleCalculatorContext(tradeList, tradeValidator), strangleMethod);
	}


	@Override
	public TradeGroupStrangle populateTradeGroupStranglePreviewForTradeGroup(TradeGroup tradeGroup, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod) {
		return populateTradeGroupStrangle(new TradeGroupStrangleCalculatorContext(tradeGroup, tradeValidator), strangleMethod);
	}


	@Override
	public TradeGroupStrangle createTradeGroupStrangle(List<Trade> tradeList, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod) {
		return populateAndSaveTradeGroupStrangle(new TradeGroupStrangleCalculatorContext(tradeList, tradeValidator), strangleMethod);
	}


	@Override
	public TradeGroupStrangle createTradeGroupStrangleForTradeGroup(TradeGroup tradeGroup, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod) {
		return populateAndSaveTradeGroupStrangle(new TradeGroupStrangleCalculatorContext(tradeGroup, tradeValidator), strangleMethod);
	}


	@Override
	public TradeGroupStrangle saveTradeGroupStrangle(TradeGroupStrangle tradeGroupStrangle) {
		validateTradeGroupStrangleHasNoNewTrades(tradeGroupStrangle);
		return saveStrangle(tradeGroupStrangle);
	}


	@Transactional(readOnly = true)
	@Override
	public TradeGroupStrangle getTradeGroupStrangle(int id, boolean populateTailTradeList) {
		TradeGroup tradeGroup = getTradeGroupService().getTradeGroup(id);
		TradeGroupStrangle strangle = new TradeGroupStrangle();
		BeanUtils.copyProperties(tradeGroup, strangle);
		return doPopulateTradeGroupStrangleTails(strangle, populateTailTradeList);
	}


	@Transactional(readOnly = true)
	@Override
	public TradeGroupStrangle getTradeGroupStrangleTailsPopulated(TradeGroupStrangle tradeGroupStrangle, boolean populateTailTradeList) {
		ValidationUtils.assertNotNull(tradeGroupStrangle, "Trade Group Strangle cannot be null");
		return doPopulateTradeGroupStrangleTails(tradeGroupStrangle, populateTailTradeList);
	}


	@Transactional(readOnly = true)
	@Override
	public TradeGroupStrangle getTradeGroupStrangleByParent(TradeGroup parentGroup, boolean populateTailTradeList) {
		ValidationUtils.assertNotNull(parentGroup, "Trade Group cannot be null");
		TradeGroup childGroup = getTradeGroupService().getTradeGroupByParent(parentGroup.getId());
		// Currently the only Strangle group type is OPTION_STRANGLE
		if (childGroup != null && childGroup.getTradeGroupType().getGroupType() == TradeGroupType.GroupTypes.OPTION_STRANGLE) {
			TradeGroupStrangle strangle = new TradeGroupStrangle();
			BeanUtils.copyProperties(childGroup, strangle);
			return doPopulateTradeGroupStrangleTails(strangle, populateTailTradeList);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	//////                        Helper Methods                         ///////
	////////////////////////////////////////////////////////////////////////////


	private TradeGroupStrangle populateTradeGroupStrangle(TradeGroupStrangleCalculatorContext context, TradeGroupStrangleCalculatorMethods strangleMethod) {
		TradeGroupStrangleCalculatorContext strangleContext = populateTradeGroupStrangleCalculatorContext(context);
		return strangleMethod.getStrangleCalculator().populateTradeGroupStrangle(strangleContext);
	}


	private TradeGroupStrangle populateTradeGroupStrangleSavingTradeSplits(TradeGroupStrangleCalculatorContext context, TradeGroupStrangleCalculatorMethods strangleMethod) {
		TradeGroupStrangleCalculatorContext strangleContext = populateTradeGroupStrangleCalculatorContext(context);
		strangleContext.setSaveSplitTrade(true);
		return strangleMethod.getStrangleCalculator().populateTradeGroupStrangle(strangleContext);
	}


	private TradeGroupStrangleCalculatorContext populateTradeGroupStrangleCalculatorContext(TradeGroupStrangleCalculatorContext context) {
		context.setTradeService(getTradeService());
		context.setTradeGroupService(getTradeGroupService());
		context.setSystemColumnValueHandler(getSystemColumnValueHandler());
		return context;
	}


	/**
	 * This method will populate the strangle and save the Trades as they are split. This method is Transactional to ensure the
	 * strangle population and save are treated as a single unit of work.
	 */
	@Transactional
	protected TradeGroupStrangle populateAndSaveTradeGroupStrangle(TradeGroupStrangleCalculatorContext context, TradeGroupStrangleCalculatorMethods strangleMethod) {
		TradeGroupStrangle strangle = populateTradeGroupStrangleSavingTradeSplits(context, strangleMethod);
		return saveStrangle(strangle);
	}


	@Transactional
	protected TradeGroupStrangle saveStrangle(TradeGroupStrangle strangle) {
		// create defensive copy for saving strangle trade group
		TradeGroup tradeGroupSave = new TradeGroup();
		BeanUtils.copyProperties(strangle, tradeGroupSave);
		tradeGroupSave = saveTradeGroup(tradeGroupSave);
		BeanUtils.copyProperties(tradeGroupSave, strangle);
		if (strangle.getPutTailGroup() != null) {
			TradeGroup putTail = strangle.getPutTailGroup();
			putTail.setParent(strangle);
			putTail.setWorkflowState(strangle.getWorkflowState());
			strangle.setPutTailGroup(saveTradeGroup(putTail));
		}
		if (strangle.getCallTailGroup() != null) {
			TradeGroup callTail = strangle.getCallTailGroup();
			callTail.setParent(strangle);
			callTail.setWorkflowState(strangle.getWorkflowState());
			strangle.setCallTailGroup(saveTradeGroup(callTail));
		}
		return strangle;
	}


	private TradeGroup saveTradeGroup(TradeGroup tradeGroup) {
		return getTradeGroupService().saveTradeGroup(tradeGroup);
	}


	private void validateTradeGroupStrangleHasNoNewTrades(TradeGroupStrangle tradeGroupStrangle) {
		boolean newTrades = tradeGroupHasNewTrade(tradeGroupStrangle);
		if (!newTrades && tradeGroupStrangle.getPutTailGroup() != null) {
			newTrades = tradeGroupHasNewTrade(tradeGroupStrangle.getPutTailGroup());
		}
		if (!newTrades && tradeGroupStrangle.getCallTailGroup() != null) {
			newTrades = tradeGroupHasNewTrade(tradeGroupStrangle.getCallTailGroup());
		}
		ValidationUtils.assertFalse(newTrades, "Cannot save a Trade Group Strangle with new Trades. The trades should be split.");
	}


	private boolean tradeGroupHasNewTrade(TradeGroup tradeGroup) {
		for (Trade trade : CollectionUtils.getIterable(tradeGroup.getTradeList())) {
			if (trade.isNewBean()) {
				return true;
			}
		}
		return false;
	}


	private TradeGroupStrangle doPopulateTradeGroupStrangleTails(TradeGroupStrangle strangle, boolean hydrateTailTradeList) {
		if (hydrateTailTradeList) {
			List<Trade> tailTradeList = getTradeListForStrangleTails(strangle);
			if (!CollectionUtils.isEmpty(tailTradeList)) {
				Map<InvestmentSecurity, List<Trade>> tailTradeMap = BeanUtils.getBeansMap(tailTradeList, Trade::getInvestmentSecurity);
				ValidationUtils.assertTrue(tailTradeMap.size() <= 2, "Trade Group Strangle does not support more than two tail groups.");
				List<Trade> putTradeList = tailTradeMap.get(strangle.getInvestmentSecurity());
				if (!CollectionUtils.isEmpty(putTradeList)) {
					Trade trade = putTradeList.get(0);
					TradeGroup putTail = trade.getTradeGroup();
					putTail.setTradeList(putTradeList);
					strangle.setPutTailGroup(putTail);
				}
				List<Trade> callTradeList = tailTradeMap.get(strangle.getSecondaryInvestmentSecurity());
				if (!CollectionUtils.isEmpty(callTradeList)) {
					Trade trade = callTradeList.get(0);
					TradeGroup callTail = trade.getTradeGroup();
					callTail.setTradeList(callTradeList);
					strangle.setCallTailGroup(callTail);
				}
			}
		}
		else {
			TradeGroupSearchForm searchForm = new TradeGroupSearchForm();
			searchForm.setParentId(strangle.getId());
			searchForm.setTradeGroupTypeName(TradeGroupType.GroupTypes.OPTION_TAIL.getTypeName());
			List<TradeGroup> tailGroupList = getTradeGroupService().getTradeGroupList(searchForm);
			if (!CollectionUtils.isEmpty(tailGroupList)) {
				ValidationUtils.assertTrue(tailGroupList.size() <= 2, "Trade Group Strangle does not support more than two tail groups.");
				for (TradeGroup tailGroup : tailGroupList) {
					if (strangle.getInvestmentSecurity() != null && strangle.getInvestmentSecurity().equals(tailGroup.getInvestmentSecurity())) {
						strangle.setPutTailGroup(tailGroup);
					}
					else {
						strangle.setCallTailGroup(tailGroup);
					}
				}
			}
		}
		return strangle;
	}


	private List<Trade> getTradeListForStrangleTails(TradeGroupStrangle strangleTradeGroup) {
		if (strangleTradeGroup.isNewBean()) {
			// strangle is new and has no tail trades
			return Collections.emptyList();
		}
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeGroupParentId(strangleTradeGroup.getId());
		return getTradeService().getTradeList(tradeSearchForm);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                 Getter and Setter Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
