package com.clifton.trade.group;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityFilterableCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassService;
import com.clifton.trade.group.TradeGroup.TradeGroupActions;
import com.clifton.trade.group.search.TradeGroupSearchForm;
import com.clifton.trade.group.search.TradeGroupSearchFormConfigurer;
import com.clifton.trade.group.search.TradeGroupTypeSearchForm;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.WorkflowTransitionUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>TradeGroupServiceImpl</code> class provides basic implementation of the TradeGroupService interface.
 */
@Service
public class TradeGroupServiceImpl implements TradeGroupService {

	private AdvancedUpdatableDAO<TradeGroupType, Criteria> tradeGroupTypeDAO;
	private AdvancedUpdatableDAO<TradeGroup, Criteria> tradeGroupDAO;

	private AccountingPositionService accountingPositionService;

	private TradeAssetClassService tradeAssetClassService;
	private TradeService tradeService;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	private DaoNamedEntityFilterableCache<TradeGroupType> tradeGroupTypeCache;


	////////////////////////////////////////////////////////////////////////////
	//////                Trade Group Type Methods                       ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeGroupType getTradeGroupType(short id) {
		return getTradeGroupTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeGroupType getTradeGroupTypeByName(String name) {
		return getTradeGroupTypeCache().getBeanForKeyValueStrict(getTradeGroupTypeDAO(), name);
	}


	@Override
	public List<TradeGroupType> getTradeGroupTypeList(TradeGroupTypeSearchForm searchForm) {
		return getTradeGroupTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<Short> getTradeGroupTypeIdListByNamePrefix(Collection<String> prefixCollection) {
		return getTradeGroupTypeCache().filter(getTradeGroupTypeDAO(),
				entity -> CollectionUtils.getStream(prefixCollection).anyMatch(prefix -> entity.getName().startsWith(prefix)));
	}


	////////////////////////////////////////////////////////////////////////////
	//////                   Trade Group Methods                         ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeGroup getTradeGroup(int id) {
		TradeGroup bean = getTradeGroupDAO().findByPrimaryKey(id);
		setTradeListForGroup(bean);
		return bean;
	}


	@Override
	public TradeGroup getTradeGroupByParent(int parentId) {
		TradeGroup bean = getTradeGroupDAO().findOneByField("parent.id", parentId);
		setTradeListForGroup(bean);
		return bean;
	}


	private void setTradeListForGroup(TradeGroup bean) {
		if (bean != null) {
			TradeSearchForm searchForm = new TradeSearchForm();
			searchForm.setTradeGroupId(bean.getId());
			bean.setTradeList(getTradeService().getTradeList(searchForm));
		}
	}


	@Override
	public List<TradeGroup> getTradeGroupList(TradeGroupSearchForm searchForm) {
		return getTradeGroupDAO().findBySearchCriteria(new TradeGroupSearchFormConfigurer(searchForm, getWorkflowDefinitionService(), this));
	}


	@Override
	@Transactional(timeout = 180)
	public TradeGroup saveTradeGroup(TradeGroup tradeGroup) {
		List<Trade> tradeList = tradeGroup.getTradeList();
		int size = CollectionUtils.getSize(tradeList);

		// List to track saved Trades. The returned TradeGroup will be updated to this List so the Trades are in their persisted state.
		List<Trade> persistedTradeList = new ArrayList<>(size);

		// One trade in the group - don't create the group, just the trade (unless the group type explicitly allows one trade in the group)
		ValidationUtils.assertNotNull(tradeGroup.getTradeGroupType(), "Trade Group Type is missing from the Trade Group.");
		// No trades in the group - do nothing unless the group type allows it.
		if (size == 0 && !tradeGroup.getTradeGroupType().isEmptyTradeListOnInitialSaveAllowed()) {
			return tradeGroup;
		}
		else if (size == 1 && !tradeGroup.getTradeGroupType().isOneTradeAllowed()) {
			// Clear the Trade Group and save the Trade By Itself
			Trade trade = tradeList.get(0);
			trade.setTradeGroup(null);
			persistedTradeList.add(getTradeService().saveTrade(trade));
			tradeGroup.setTradeList(persistedTradeList);
			return tradeGroup;
		}

		// More than one trade - create the group
		// If trade date is missing on the group - set it to the max
		if (tradeGroup.getTradeDate() == null) {
			tradeGroup.setTradeDate(CollectionUtils.getFirstElementStrict(BeanUtils.sortWithFunction(tradeList, Trade::getTradeDate, false)).getTradeDate());
		}

		TradeGroup resultTradeGroup = tradeGroup;
		// Insert new Groups Only - don't change existing
		if (tradeGroup.isNewBean()) {
			resultTradeGroup = getTradeGroupDAO().save(tradeGroup);
		}

		persistedTradeList.addAll(saveTradeGroupTradeList(resultTradeGroup, tradeList));
		resultTradeGroup.setTradeList(persistedTradeList);
		return resultTradeGroup;
	}


	@Override
	@Transactional(timeout = 180)
	public TradeGroup saveTradeGroupWithTradeAllocations(final TradeGroup tradeGroup, List<TradeAssetClassPositionAllocation> tradeAssetClassPositionAllocationList) {
		if (CollectionUtils.isEmpty(tradeAssetClassPositionAllocationList)) {
			// If no trade asset class allocations, just perform regular save trade group
			return saveTradeGroup(tradeGroup);
		}

		// Save the Trade Group and Trade List with Automatic transitions disabled
		TradeGroup savedTradeGroup = WorkflowTransitionUtils.executeWithAutomaticWorkflowTransitionsDisabledAndReturn(() -> saveTradeGroup(tradeGroup));
		// Save the trade allocations
		getTradeAssetClassService().saveTradeAssetClassPositionAllocationList(tradeAssetClassPositionAllocationList);
		for (Trade trade : CollectionUtils.getIterable(savedTradeGroup.getTradeList())) {
			// Just fire "saveTrade" which should kick off automatic transitions if any apply
			getTradeService().saveTrade(trade);
		}
		return savedTradeGroup;
	}


	@Override
	public TradeGroup saveTradeGroupWithoutTradeUpdates(TradeGroup tradeGroup) {
		ValidationUtils.assertFalse(tradeGroup.isNewBean(), "Can only update a Trade Group that already exists.");
		return getTradeGroupDAO().save(tradeGroup);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Save trades for a trade group
	 *
	 * @param tradeGroup the trade group
	 * @param tradeList  the list of trades that are part of the trade group
	 * @return the list of persisted trades
	 */
	@Transactional // open transaction if not present to improve performance and group updates
	protected List<Trade> saveTradeGroupTradeList(TradeGroup tradeGroup, List<Trade> tradeList) {
		List<Trade> persistedTradeList = new ArrayList<>(tradeList.size());
		boolean efpTrade = tradeGroup.getTradeGroupType().getGroupType() == TradeGroupType.GroupTypes.EFP;
		for (Trade trade : tradeList) {
			trade.setTradeGroup(tradeGroup);
			Trade persistedTrade = efpTrade ? saveExchangeForPhysicalTrade(trade)
					: getTradeService().saveTrade(trade);
			persistedTradeList.add(persistedTrade);
		}
		return persistedTradeList;
	}


	/**
	 * EFP trades follow a slightly different workflow. EFP trades can have fills provided by a broker that result in the correct average price of the trade. In addition to saving
	 * the trade, the EFP trade's fills, if present, will be saved and the trade will be transitioned in the trade workflow.
	 *
	 * @param trade the EFP trade to save
	 * @return the persisted version of the trade
	 */
	private Trade saveExchangeForPhysicalTrade(Trade trade) {
		/*
		 * Must get trade fills before saving the trade because the trade is mutable and we may lose them in the save.
		 */
		List<TradeFill> tradeFills = getExchangeForPhysicalTradeFillsToSave(trade);
		Trade savedTrade = DaoUtils.executeWithPostUpdateFlushEnabled(() -> getTradeService().saveTrade(trade));
		if (!CollectionUtils.isEmpty(tradeFills)) {
			getTradeService().saveTradeFillList(tradeFills);
		}
		return transitionExchangeForPhysicalTradeToValidated(savedTrade);
	}


	/**
	 * An EFP TradeGroup's future trade can be entered with two fills that populate the trade's average price. The fills are provided by the broker that wins the trade.
	 *
	 * @param trade trade that is a member of a saved trade group
	 * @return the list of valid trade fills to be persisted for the provided trade
	 */
	private List<TradeFill> getExchangeForPhysicalTradeFillsToSave(Trade trade) {
		if (trade.isNewBean()) {
			if (!CollectionUtils.isEmpty(trade.getTradeFillList())) {
				// filter fills leaving only those that are valid
				List<TradeFill> validTradeFills = CollectionUtils.getStream(trade.getTradeFillList()).filter(tradeFill -> {
					boolean hasQuantity = tradeFill.getQuantity() != null || tradeFill.getQuantityNormalized() != null;
					boolean hasPrice = (tradeFill.getNotionalTotalPrice() != null || tradeFill.getNotionalUnitPrice() != null)
							|| (tradeFill.getPaymentTotalPrice() != null || tradeFill.getPaymentUnitPrice() != null);
					if (hasQuantity && hasPrice) {
						tradeFill.setTrade(trade);
						return true;
					}
					return false;
				}).collect(Collectors.toList());
				if (CollectionUtils.isEmpty(validTradeFills)) {
					/*
					 * This is needed to avoid passing around invalid trade fills on the entity. IMS attempts
					 * to render the invalid/non-persisted fills upon rendering the result.
					 */
					trade.setTradeFillList(null);
				}
				return validTradeFills;
			}
		}
		return Collections.emptyList();
	}


	/**
	 * Upon creation of an EFP trade group, it's trades stay in in a draft state that allows for entering trade fills. After the fills are persisted, the trades are validated and
	 * will follow the usual trade workflow.
	 *
	 * @param trade the newly created trade
	 * @return the trade with updated workflow state
	 */
	private Trade transitionExchangeForPhysicalTradeToValidated(Trade trade) {
		WorkflowState tradeWorkflowState = trade.getWorkflowState();
		if ("Draft Awaiting Fills".equals(tradeWorkflowState.getName())) {
			WorkflowState transitionToState = getWorkflowDefinitionService().getWorkflowStateByName(tradeWorkflowState.getWorkflow().getId(), TradeGroupActions.VALIDATE.getStateName());
			if (transitionToState != null) {
				return (Trade) getWorkflowTransitionService().executeWorkflowTransitionSystemDefined("Trade", trade.getId(), transitionToState.getId());
			}
		}
		return trade;
	}


	////////////////////////////////////////////////////////////////////////////
	//////                Trade Group Action Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String executeActionOnTrades(TradeGroupActions action, List<Integer> tradeIdList) {
		if (CollectionUtils.isEmpty(tradeIdList)) {
			return null;
		}
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setIds(tradeIdList.toArray(new Integer[0]));
		return executeActionOnTradeList(action, getTradeService().getTradeList(searchForm));
	}


	@Override
	public String executeActionOnTradeList(TradeGroupActions action, List<Trade> tradeList) {
		if (action == null) {
			throw new ValidationException("Unable to process action on Trades.  No action set.");
		}

		String stateName = action.getStateName();
		if (StringUtils.isEmpty(stateName)) {
			return null; // No Workflow Transition for Fills only, or Delete Fills (i.e. action has no workflow state name)
		}

		// Put the trades in order by date, security, and price ascending
		tradeList = sortTradeList(tradeList);

		// Pull the First Trade from the List to get the correct Workflow
		Trade trade = CollectionUtils.getFirstElementStrict(tradeList);
		Workflow workflow = trade.getWorkflowState().getWorkflow();
		WorkflowState state = getWorkflowDefinitionService().getWorkflowStateByName(workflow.getId(), stateName);
		return executeTradeGroupWorkflowTransition(tradeList, action, state);
	}


	private String executeTradeGroupWorkflowTransition(List<Trade> tradeList, TradeGroupActions action, WorkflowState state) {
		int failedCount = 0;
		int skippedCount = 0;
		int inStateCount = 0;
		StringBuilder errors = new StringBuilder("The following Trades failed workflow transition to " + state.getName() + ": " + StringUtils.NEW_LINE);

		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			try {
				// Is trade already in state
				if (trade.getWorkflowState().equals(state)) {
					inStateCount++;
				}
				else {
					// See if a valid transition for the trade, else skip it
					WorkflowTransition transition = getWorkflowTransitionService().getWorkflowTransitionForEntity(trade, trade.getWorkflowState(), state);
					if (transition != null) {
						getWorkflowTransitionService().executeWorkflowTransitionByTransition("Trade", trade.getId(), transition.getId());
					}
					else {
						skippedCount++;
					}
					if (TradeGroupActions.CANNOT_FILL == action) {
						// If Cannot Fill - Update Quantity on the Trade
						trade.setQuantityIntended(CoreMathUtils.sumProperty(trade.getTradeFillList(), TradeFill::getQuantity));
						getTradeService().saveTrade(trade);
					}
				}
			}
			catch (Exception e) {
				failedCount++;
				if (failedCount <= 5) {
					errors.append("[").append(trade.getUniqueLabel()).append("]: ").append(e.getMessage()).append(StringUtils.NEW_LINE);
				}
				LogUtils.errorOrInfo(getClass(), "Error transitioning trade [" + trade.getUniqueLabel() + "] to Workflow State [" + state.getName() + "]: " + e.getMessage(), e);
			}
		}
		String resultMsg = "Total Trades Processed " + CollectionUtils.getSize(tradeList) + ": " + (CollectionUtils.getSize(tradeList) - skippedCount - inStateCount - failedCount) + " Successful";
		if (inStateCount > 0) {
			resultMsg += ", " + inStateCount + " Skipped (already " + state.getName() + ")";
		}
		if (skippedCount > 0) {
			resultMsg += ", " + skippedCount + " Skipped (not a valid transition)";
		}
		if (failedCount > 0) {
			throw new ValidationException(resultMsg + ", " + failedCount + " Failed. " + (failedCount > 5 ? "First 5 " : "") + "Failures: " + StringUtils.NEW_LINE + errors);
		}
		return resultMsg;
	}


	/**
	 * Executes Validate transition on either a Trade Group (all trades in the group) or an individual trade Called after ignoring warnings on trades to also immediately validate
	 * the trade(s).
	 */
	@Override
	public void executeTradeValidateAction(Integer tradeGroupId, Integer tradeId) {
		if (tradeGroupId == null && tradeId == null) {
			return;
		}

		List<Trade> tradeList;
		if (tradeGroupId != null) {
			TradeGroup group = getTradeGroup(tradeGroupId);
			tradeList = group.getTradeList();
		}
		else {
			tradeList = CollectionUtils.createList(getTradeService().getTrade(tradeId));
		}
		executeActionOnTradeList(TradeGroupActions.VALIDATE, tradeList);
	}


	@Override
	public void executeTradeGroupAction(TradeGroup bean) {
		TradeGroupActions action = bean.getTradeGroupAction();
		List<Integer> tradeIdList = new ArrayList<>();
		List<TradeFill> tradeFillList = new ArrayList<>();
		List<TradeFill> tradeDeleteBeforeUpdateFillList = new ArrayList<>();
		List<Trade> saveTradeList = new ArrayList<>();

		// Get the list of trade ids
		for (Trade trade : CollectionUtils.getIterable(bean.getTradeList())) {
			switch (action) {
				case FILL:
				case FILL_EXECUTE: {
					fillTrade(trade, action, tradeFillList, tradeIdList);
					break;
				}
				case DELETE_FILLS: {
					tradeFillList.addAll(getTradeService().getTradeFillListByTrade(trade.getId()));
					break;
				}
				case UPDATE_FILLS: {
					if (trade.getFillPrice() != null && trade.getFillQuantity() != null) {
						tradeDeleteBeforeUpdateFillList.addAll(getTradeService().getTradeFillListByTrade(trade.getId()));
						fillTrade(trade, action, tradeFillList, tradeIdList);
					}
				}
				case SAVE_TRADE: {
					saveTradeList.add(trade);
					break;
				}
				default: {
					// Actions with workflow changes need to include the Trade IDs to process.
					tradeIdList.add(trade.getId());
				}
			}
		}
		if (!CollectionUtils.isEmpty(tradeFillList)) {
			StringBuilder resultBuilder = new StringBuilder();
			if (TradeGroupActions.UPDATE_FILLS == action) {
				getTradeService().deleteTradeFillList(tradeDeleteBeforeUpdateFillList);
				getTradeService().saveTradeFillList(tradeFillList);
				resultBuilder.append("Saved ");
			}
			else if (TradeGroupActions.DELETE_FILLS == action) {
				getTradeService().deleteTradeFillList(tradeFillList);
				resultBuilder.append("Deleted ");
			}
			else {
				getTradeService().saveTradeFillList(tradeFillList);
				resultBuilder.append("Saved ");
			}
			bean.setTradeGroupActionResult(resultBuilder.append(CollectionUtils.getSize(tradeFillList)).append(" Trade Fills for ")
					.append(CollectionUtils.getSize(bean.getTradeList())).append(" Trades").toString());
		}
		else if (!CollectionUtils.isEmpty(saveTradeList)) {
			saveTradeGroupTradeList(bean, saveTradeList);
			bean.setTradeGroupActionResult(new StringBuilder("Total Trades Processed ").append(CollectionUtils.getSize(bean.getTradeList()))
					.append(": ").append(CollectionUtils.getSize(saveTradeList)).append(" Successful").toString());
		}
		String workflowActionExecutionResult = executeActionOnTrades(action, tradeIdList);
		if (workflowActionExecutionResult != null) {
			bean.setTradeGroupActionResult(workflowActionExecutionResult);
		}
		else if (bean.getTradeGroupActionResult() == null) {
			bean.setTradeGroupActionResult("Action performed with no changes.");
		}
	}


	private void fillTrade(Trade trade, TradeGroup.TradeGroupActions action, List<TradeFill> tradeFillList, List<Integer> tradeIdList) {
		TradeFill fill = new TradeFill();
		fill.setTrade(getTradeService().getTrade(trade.getId()));
		// Changed Trade Fill Price check to allow 0 Price Fills - Worthless Trade
		if (trade.getFillPrice() == null || MathUtils.isNullOrZero(trade.getFillQuantity())) {
			throw new ValidationException("Missing Fill Price or Quantity for Trade [" + fill.getTrade().getUniqueLabel() + "].");
		}
		fill.setNotionalUnitPrice(trade.getFillPrice());
		fill.setQuantity(trade.getFillQuantity());
		tradeFillList.add(fill);
		if (action == TradeGroupActions.FILL_EXECUTE) {
			// FILL_EXECUTE includes workflow changes, so add them to the list of Trade IDs to process.
			tradeIdList.add(trade.getId());
		}
	}


	private List<Trade> sortTradeList(List<Trade> tradeList) {
		// Put the trades in order by date, security, and price ascending
		return BeanUtils.sortWithFunctions(tradeList, CollectionUtils.createList(
				Trade::getTradeDate,
				trade -> trade.getInvestmentSecurity().getId(),
				Trade::getAverageUnitPrice),
				CollectionUtils.createList(true, true, true));
	}


	////////////////////////////////////////////////////////////////////////////
	//////                   Trade Entry Methods                         ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(timeout = 180)
	public void saveTradeEntry(TradeEntry tradeEntry) {
		ValidationUtils.assertNotEmpty(tradeEntry.getTradeDetailList(), "The trade must be allocated to at least one client account.");
		TradeType tradeType = tradeEntry.getTradeType();
		ValidationUtils.assertNotNull(tradeType, "Trade Type must be defined for trade entry.");
		if (TradeType.CURRENCY.equals(tradeType.getName())) {
			if (tradeEntry.getAccountingNotional() == null || tradeEntry.getAccountingNotional().equals(BigDecimal.ZERO)) {
				throw new ValidationException("Expected greater than 0 value for for Accounting Notional.");
			}
			BigDecimal totalNotional = CoreMathUtils.sumProperty(tradeEntry.getTradeDetailList(), TradeEntryDetail::getAccountingNotional);
			ValidationUtils.assertTrue(MathUtils.isEqual(totalNotional, tradeEntry.getAccountingNotional()), "Expected total notional for all trades of '" + tradeEntry.getAccountingNotional()
					+ "' but actual sum is '" + totalNotional + "'.");
		}
		else {
			if (tradeEntry.getOriginalFace() == null) {
				BigDecimal totalShares = CoreMathUtils.sumProperty(tradeEntry.getTradeDetailList(), TradeEntryDetail::getQuantityIntended);
				ValidationUtils.assertTrue(MathUtils.isEqual(totalShares, tradeEntry.getQuantityIntended()), "Expected total quantity for all trades of '" + tradeEntry.getQuantityIntended()
						+ "' but actual sum is '" + totalShares + "'.");
			}
			else {
				BigDecimal totalFace = CoreMathUtils.sumProperty(tradeEntry.getTradeDetailList(), TradeEntryDetail::getOriginalFace);
				ValidationUtils.assertTrue(MathUtils.isEqual(totalFace, tradeEntry.getOriginalFace()), "Expected total " + InvestmentUtils.getUnadjustedQuantityFieldName(tradeEntry.getInvestmentSecurity()) + " for all trades of '" + tradeEntry.getOriginalFace()
						+ "' but actual sum is '" + totalFace + "'.");
			}
		}

		// creates a list of Trade objects for each detail item of the group
		Set<String> uniqueTradeIdentifiers = new HashSet<>(); // Track Client/Unique Accounts to ensure same one isn't used multiple times
		List<Trade> tradeList = new ArrayList<>();
		for (TradeEntryDetail detail : tradeEntry.getTradeDetailList()) {
			ValidationUtils.assertFalse(detail.getClientInvestmentAccount() == null || detail.getHoldingInvestmentAccount() == null, "Both A Client Account and A Holding account must be defined.");
			String uniqueTradeIdentifier = detail.getClientInvestmentAccount().getId() + "-" + detail.getHoldingInvestmentAccount().getId();
			if (!uniqueTradeIdentifiers.add(uniqueTradeIdentifier)) {
				throw new ValidationException("Each Client + Holding Account selection for Multi-Client Trade Entry must be unique. The account [" + detail.getClientInvestmentAccount().getLabel()
						+ "]-[" + detail.getHoldingInvestmentAccount().getLabel() + "] has more than one trade entry listed.");
			}

			Trade trade = new Trade();
			BeanUtils.copyProperties(tradeEntry, trade);
			trade.setTradeSource(ObjectUtils.coalesce(detail.getTradeSource(), tradeEntry.getTradeSource()));
			trade.setClientInvestmentAccount(detail.getClientInvestmentAccount());
			trade.setHoldingInvestmentAccount(detail.getHoldingInvestmentAccount());

			if (detail.getExecutingBrokerCompany() != null) {
				trade.setExecutingBrokerCompany(detail.getExecutingBrokerCompany());
			}

			if (detail.getTradeDestination() != null) {
				trade.setTradeDestination(detail.getTradeDestination());
			}

			if (detail.getExpectedUnitPrice() != null) {
				trade.setExpectedUnitPrice(detail.getExpectedUnitPrice());
				trade.setAverageUnitPrice(detail.getExpectedUnitPrice());
			}

			if (detail.getCommissionPerUnit() != null) {
				trade.setCommissionPerUnit(detail.getCommissionPerUnit());
			}

			// proportionally allocate the following fields if set
			trade.setOriginalFace(detail.getOriginalFace());
			if (detail.getQuantityIntended() != null) {
				trade.setQuantityIntended(detail.getQuantityIntended());
			}
			else if (tradeEntry.getQuantityIntended() != null) {
				trade.setQuantityIntended(applyProportionally(tradeEntry.getQuantityIntended(), trade, tradeEntry));
			}
			else if (detail.getOriginalFace() != null) {
				trade.setQuantityIntended(detail.getOriginalFace());
			}
			if (detail.getAccrualAmount() != null) {
				trade.setAccrualAmount1(detail.getAccrualAmount());
			}
			else if (tradeEntry.getAccrualAmount() != null) {
				trade.setAccrualAmount1(applyProportionally(tradeEntry.getAccrualAmount(), trade, tradeEntry));
			}
			if (detail.getFeeAmount() != null) {
				trade.setFeeAmount(detail.getFeeAmount());
			}
			else if (tradeEntry.getFeeAmount() != null) {
				trade.setFeeAmount(applyProportionally(tradeEntry.getFeeAmount(), trade, tradeEntry));
			}
			if (trade.getTradeType().getName().equals(TradeType.CURRENCY)) {
				trade.setAccountingNotional(detail.getAccountingNotional());
			}
			else if (tradeEntry.getAccountingNotional() != null) {
				trade.setAccountingNotional(applyProportionally(tradeEntry.getAccountingNotional(), trade, tradeEntry));
			}

			if (MathUtils.isGreaterThan(trade.getQuantityIntended(), BigDecimal.ZERO) ||
					(trade.getTradeType().getName().equals(TradeType.CURRENCY) && MathUtils.isGreaterThan(trade.getAccountingNotional(), BigDecimal.ZERO))) {
				tradeList.add(trade);
			}
		}

		// fix rounding problems if necessary
		if (tradeEntry.getAccrualAmount() != null) {
			CoreMathUtils.applySumPropertyDifference(tradeList, Trade::getAccrualAmount1, Trade::setAccrualAmount1, tradeEntry.getAccrualAmount(), true);
		}
		if (tradeEntry.getFeeAmount() != null) {
			CoreMathUtils.applySumPropertyDifference(tradeList, Trade::getFeeAmount, Trade::setFeeAmount, tradeEntry.getFeeAmount(), true);
		}
		if (tradeEntry.getOriginalFace() != null) {
			boolean applySumDifference = tradeEntry.getInvestmentSecurity().getInstrument().getHierarchy().getFactorChangeEventType() == null;
			// Reference to identify a trade to adjust. Used for factor adjusted security positions to determine largest non-closing trade
			ObjectWrapper<Trade> tradeToAdjust = new ObjectWrapper<>();
			if (!applySumDifference) {
				// determine if factor adjusted security trades are closing existing positions to avoid penny differences.
				Integer[] clientAccountIds = tradeList.stream().map(trade -> trade.getClientInvestmentAccount().getId()).toArray(Integer[]::new);
				AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(tradeEntry.getTradeDate())
						.forInvestmentSecurity(tradeEntry.getInvestmentSecurity().getId())
						.forClientAccounts(clientAccountIds);
				List<AccountingPosition> existingPositionList = getAccountingPositionService().getAccountingPositionListUsingCommand(positionCommand);

				if (CollectionUtils.isEmpty(existingPositionList)) {
					// no open positions, use sum difference to opening lots
					applySumDifference = true;
				}
				else {
					List<Trade> nonFullCloseTradeList = new ArrayList<>();
					Map<InvestmentAccount, List<AccountingPosition>> clientAccountPositionListMap = BeanUtils.getBeansMap(existingPositionList, AccountingPosition::getClientInvestmentAccount);
					for (Trade trade : CollectionUtils.getIterable(tradeList)) {
						BigDecimal tradeQuantity = trade.isBuy() ? trade.getQuantityIntended() : trade.getQuantityIntended().negate();
						BigDecimal positionQuantityBalance = BigDecimal.ZERO;
						boolean foundClosingPosition = false;
						for (AccountingPosition existingPosition : CollectionUtils.getIterable(clientAccountPositionListMap.get(trade.getClientInvestmentAccount()))) {
							BigDecimal positionQuantity = existingPosition.getRemainingQuantity();
							BigDecimal positionDifference = MathUtils.add(positionQuantity, tradeQuantity);
							if (CoreMathUtils.applyPropertyAdjustment(() -> trade, Trade::getQuantityIntended, Trade::setQuantityIntended, positionDifference, MathUtils.BIG_DECIMAL_PENNY).isValueMatch()) {
								// found existing position lot matching closing trade within a penny and was adjusted.
								foundClosingPosition = true;
								break;
							}
							positionQuantityBalance = MathUtils.add(positionQuantityBalance, positionQuantity);
						}

						if (!foundClosingPosition) {
							// no closing position lot found, determine if trade is closing existing position balance within a penny
							BigDecimal positionDifference = MathUtils.add(positionQuantityBalance, tradeQuantity);
							if (!(CoreMathUtils.applyPropertyAdjustment(() -> trade, Trade::getQuantityIntended, Trade::setQuantityIntended, positionDifference, MathUtils.BIG_DECIMAL_PENNY).isValueMatch())) {
								// trade is not a close of a lot or position balance, add it as a candidate for penny adjustment
								nonFullCloseTradeList.add(trade);
							}
						}
					}

					if (!nonFullCloseTradeList.isEmpty()) {
						applySumDifference = true;
						tradeToAdjust.setObject(CoreMathUtils.getBeanWithMaxProperty(nonFullCloseTradeList, Trade::getQuantityIntended, true, t -> true));
					}
				}
			}

			if (applySumDifference) {
				// apply usual sum difference for non-factor adjusted security trades, and factor adjusted security trades that are non-closing
				if (tradeToAdjust.isPresent()) {
					CoreMathUtils.applySumPropertyDifference(tradeList, Trade::getQuantityIntended, Trade::setQuantityIntended, tradeEntry.getQuantityIntended() == null ? tradeEntry.getOriginalFace() : tradeEntry.getQuantityIntended(), tradeToAdjust::getObject);
				}
				else {
					CoreMathUtils.applySumPropertyDifference(tradeList, Trade::getQuantityIntended, Trade::setQuantityIntended, tradeEntry.getQuantityIntended() == null ? tradeEntry.getOriginalFace() : tradeEntry.getQuantityIntended(), true);
				}
			}
		}
		if (tradeEntry.getAccountingNotional() != null) {
			CoreMathUtils.applySumPropertyDifference(tradeList, Trade::getAccountingNotional, Trade::setAccountingNotional, tradeEntry.getAccountingNotional(), true);
		}

		// Create a new Group Record and set properties and save Trades for the Group
		TradeGroup group = new TradeGroup();
		group.setTradeGroupType(getTradeGroupTypeByName(TradeGroupType.GroupTypes.MULTI_CLIENT.getTypeName()));
		group.setInvestmentSecurity(tradeEntry.getInvestmentSecurity());
		group.setTradeDate(tradeEntry.getTradeDate());
		boolean buy = tradeEntry.getOpenCloseType() == null ? tradeEntry.isBuy() : tradeEntry.getOpenCloseType().isBuy();
		group.setNote((buy ? "Buy " : "Sell ") + CoreMathUtils.formatNumberInteger(tradeEntry.getQuantityIntended()) + " " + group.getInvestmentSecurity().getSymbol() + " for "
				+ CollectionUtils.getSize(tradeList) + " accounts.");
		group.setTraderUser(tradeEntry.getTraderUser());
		group.setTradeList(tradeList);
		saveTradeGroup(group);
	}


	/**
	 * Returns proportion of the specified amount for the specified trade from total tradeEntry. Proportion is based on Original Face if the field is set or on Quantity Intended if
	 * Original Face is not set.
	 */
	protected BigDecimal applyProportionally(BigDecimal amount, Trade trade, TradeEntry tradeEntry) {
		if (tradeEntry.getOriginalFace() != null) {
			return handleRounding(MathUtils.divide(MathUtils.multiply(amount, trade.getOriginalFace()), tradeEntry.getOriginalFace()), trade);
		}
		return handleRounding(MathUtils.divide(MathUtils.multiply(amount, trade.getQuantityIntended()), tradeEntry.getQuantityIntended()), trade);
	}


	// This method exists to account for Test cases where not all the necessary info is defined on the trade object
	private BigDecimal handleRounding(BigDecimal amount, Trade trade) {
		if (trade.getInvestmentSecurity() != null) {
			return InvestmentCalculatorUtils.roundLocalAmount(amount, trade.getInvestmentSecurity());
		}
		else if (trade.getPayingSecurity() != null) {
			return InvestmentCalculatorUtils.roundLocalAmount(amount, trade.getPayingSecurity());
		}
		else if (trade.getClientInvestmentAccount() != null) {
			return InvestmentCalculatorUtils.roundBaseAmount(amount, trade.getClientInvestmentAccount());
		}
		else if (trade.getHoldingInvestmentAccount() != null) {
			return InvestmentCalculatorUtils.roundBaseAmount(amount, trade.getHoldingInvestmentAccount());
		}
		return MathUtils.round(amount, 2);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeGroupType, Criteria> getTradeGroupTypeDAO() {
		return this.tradeGroupTypeDAO;
	}


	public void setTradeGroupTypeDAO(AdvancedUpdatableDAO<TradeGroupType, Criteria> tradeGroupTypeDAO) {
		this.tradeGroupTypeDAO = tradeGroupTypeDAO;
	}


	public AdvancedUpdatableDAO<TradeGroup, Criteria> getTradeGroupDAO() {
		return this.tradeGroupDAO;
	}


	public void setTradeGroupDAO(AdvancedUpdatableDAO<TradeGroup, Criteria> tradeGroupDAO) {
		this.tradeGroupDAO = tradeGroupDAO;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public TradeAssetClassService getTradeAssetClassService() {
		return this.tradeAssetClassService;
	}


	public void setTradeAssetClassService(TradeAssetClassService tradeAssetClassService) {
		this.tradeAssetClassService = tradeAssetClassService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public DaoNamedEntityFilterableCache<TradeGroupType> getTradeGroupTypeCache() {
		return this.tradeGroupTypeCache;
	}


	public void setTradeGroupTypeCache(DaoNamedEntityFilterableCache<TradeGroupType> tradeGroupTypeCache) {
		this.tradeGroupTypeCache = tradeGroupTypeCache;
	}
}
