package com.clifton.trade.group.dynamic.roll;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturityDetail;

import java.math.BigDecimal;


/**
 * <code>TradeGroupDynamicForwardRollDetail</code> contains information for a specific Client's position on Maturing Forward.
 * The information is used to help create Trades for closing the position and, optionally, rolling to a new Currency Forward.
 * <p>
 * The values of this entity are transient and can be manipulated in the UX to get the desired new position upon submission.
 *
 * @author NickK
 */
public class TradeGroupDynamicForwardRollDetail extends BaseTradeGroupDynamicMaturityDetail {

	// Maturing position
	private BigDecimal settlementAmount;
	private BigDecimal maturingPrice;

	// Closing position
	private Trade closeTrade;
	// Items used for creating Closing Trade
	private Boolean closeBuy;
	private BigDecimal closeGivenAmount;
	private InvestmentSecurity closeGivenCurrency;
	private InvestmentSecurity closeSecurity;

	// New Forward position
	private Trade rollTrade;
	// Items used for creating New Trade
	private Boolean rollBuy;
	private BigDecimal rollGivenAmount;
	private InvestmentSecurity rollGivenCurrency;
	private InvestmentSecurity rollSecurity;

	// flag to indicate if this detail includes pending activity/adjustments
	private boolean pending;

	////////////////////////////////////////////////////////////////////////////////
	//////////       TradeGroupDynamicForwardRollDetail methods         ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("TradeGroupDynamicForwardRollDetail [clientAccount=").append(getClientAccount()).append(", holdingAccount=").append(getHoldingAccount());
		if (getMaturingSecurity() != null) {
			builder.append(", maturingForward=").append(getMaturingSecurity());
		}
		if (getCloseTrade() != null) {
			builder.append(", closeTrade=").append(getCloseTrade());
		}
		if (getRollTrade() != null) {
			builder.append(", rollTrade=").append(getRollTrade());
		}
		return builder.append("]").toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getSettlementAmount() {
		return this.settlementAmount;
	}


	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}


	public BigDecimal getMaturingPrice() {
		return this.maturingPrice;
	}


	public void setMaturingPrice(BigDecimal maturingPrice) {
		this.maturingPrice = maturingPrice;
	}


	public Trade getCloseTrade() {
		return this.closeTrade;
	}


	public void setCloseTrade(Trade closeTrade) {
		this.closeTrade = closeTrade;
	}


	public Boolean getCloseBuy() {
		return this.closeBuy;
	}


	public void setCloseBuy(Boolean closeBuy) {
		this.closeBuy = closeBuy;
	}


	public BigDecimal getCloseGivenAmount() {
		return this.closeGivenAmount;
	}


	public void setCloseGivenAmount(BigDecimal closeGivenAmount) {
		this.closeGivenAmount = closeGivenAmount;
	}


	public InvestmentSecurity getCloseGivenCurrency() {
		return this.closeGivenCurrency;
	}


	public void setCloseGivenCurrency(InvestmentSecurity closeGivenCurrency) {
		this.closeGivenCurrency = closeGivenCurrency;
	}


	public InvestmentSecurity getCloseSecurity() {
		return this.closeSecurity;
	}


	public void setCloseSecurity(InvestmentSecurity closeSecurity) {
		this.closeSecurity = closeSecurity;
	}


	public Trade getRollTrade() {
		return this.rollTrade;
	}


	public void setRollTrade(Trade rollTrade) {
		this.rollTrade = rollTrade;
	}


	public Boolean getRollBuy() {
		return this.rollBuy;
	}


	public void setRollBuy(Boolean rollBuy) {
		this.rollBuy = rollBuy;
	}


	public BigDecimal getRollGivenAmount() {
		return this.rollGivenAmount;
	}


	public void setRollGivenAmount(BigDecimal rollGivenAmount) {
		this.rollGivenAmount = rollGivenAmount;
	}


	public InvestmentSecurity getRollGivenCurrency() {
		return this.rollGivenCurrency;
	}


	public void setRollGivenCurrency(InvestmentSecurity rollGivenCurrency) {
		this.rollGivenCurrency = rollGivenCurrency;
	}


	public InvestmentSecurity getRollSecurity() {
		return this.rollSecurity;
	}


	public void setRollSecurity(InvestmentSecurity rollSecurity) {
		this.rollSecurity = rollSecurity;
	}


	public boolean isPending() {
		return this.pending;
	}


	public void setPending(boolean pending) {
		this.pending = pending;
	}
}
