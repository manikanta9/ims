package com.clifton.trade.group.strangle;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.strangle.calculator.TradeGroupStrangleCalculator;
import com.clifton.trade.group.strangle.validate.TradeGroupStrangleTradeValidator;

import java.util.List;


/**
 * The <code>TradeGroupStrangleService</code> interface defines methods for working with strangle TradeGroups.
 *
 * @author NickK
 */
public interface TradeGroupStrangleService {

	/**
	 * Populate a strangle with possible tail TradeGroups for the provided TradeGroup. The strangle returned is a
	 * transient object that is not saved. Use
	 * {@link TradeGroupStrangleService#createTradeGroupStrangleForTradeGroup(TradeGroup, TradeGroupStrangleTradeValidator, TradeGroupStrangleCalculatorMethods)}
	 * to save the strangle.
	 */
	@DoNotAddRequestMapping
	public TradeGroupStrangle populateTradeGroupStranglePreviewForTradeGroup(TradeGroup tradeGroup, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod);


	/**
	 * Populate a strangle with possible tail TradeGroups for the provided Trade list. The strangle returned is a
	 * transient object that is not saved. Use
	 * {@link TradeGroupStrangleService#createTradeGroupStrangle(List, TradeGroupStrangleTradeValidator, TradeGroupStrangleCalculatorMethods)}
	 * to save the strangle.
	 */
	@DoNotAddRequestMapping
	public TradeGroupStrangle populateTradeGroupStranglePreview(List<Trade> tradeList, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod);


	/**
	 * Creates a strangle with possible tail TradeGroups for the provided TradeGroup. The strangle returned, and its Trades,
	 * are saved during creation. Use
	 * {@link TradeGroupStrangleService#populateTradeGroupStranglePreviewForTradeGroup(TradeGroup, TradeGroupStrangleTradeValidator, TradeGroupStrangleCalculatorMethods)}
	 * to preview the strangle.
	 */
	@DoNotAddRequestMapping
	public TradeGroupStrangle createTradeGroupStrangleForTradeGroup(TradeGroup tradeGroup, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod);


	/**
	 * Creates a strangle with possible tail TradeGroups for the provided Trade list. The strangle returned, and its Trades,
	 * are saved during creation. Use
	 * {@link TradeGroupStrangleService#populateTradeGroupStranglePreview(List, TradeGroupStrangleTradeValidator, TradeGroupStrangleCalculatorMethods)}
	 * to preview the strangle.
	 */
	@DoNotAddRequestMapping
	public TradeGroupStrangle createTradeGroupStrangle(List<Trade> tradeList, TradeGroupStrangleTradeValidator tradeValidator, TradeGroupStrangleCalculatorMethods strangleMethod);


	/**
	 * Saves the provided {@link TradeGroupStrangle}. If the provided strangle has new Trades, a {@link ValidationException} will be thrown.
	 * This method is intended to be used in conjunction with {@link TradeGroupStrangleCalculator} where split Trades are saved.
	 */
	public TradeGroupStrangle saveTradeGroupStrangle(TradeGroupStrangle tradeGroupStrangle);


	/**
	 * Lookup and populate a TradeGroupStrangle and its tail groups that has the provided TradeGroup ID.
	 */
	public TradeGroupStrangle getTradeGroupStrangle(int id, boolean populateTailTradeList);


	/**
	 * Lookup and apply the Tail TradeGroups to the provided Strangle.
	 */
	public TradeGroupStrangle getTradeGroupStrangleTailsPopulated(TradeGroupStrangle tradeGroupStrangle, boolean populateTailTradeList);


	/**
	 * Lookup and populate a TradeGroupStrangle and its tail groups that has the provided TradeGroup as a parent.
	 * If the provided parent TradeGroup has no strangle group, null is returned.
	 */
	public TradeGroupStrangle getTradeGroupStrangleByParent(TradeGroup parentGroup, boolean populateTailTradeList);
}
