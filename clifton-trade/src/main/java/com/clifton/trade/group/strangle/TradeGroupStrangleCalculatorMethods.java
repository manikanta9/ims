package com.clifton.trade.group.strangle;

import com.clifton.trade.group.strangle.calculator.TradeGroupOptionStrangleAcrossClientAccountCalculator;
import com.clifton.trade.group.strangle.calculator.TradeGroupOptionStrangleByClientAccountCalculator;
import com.clifton.trade.group.strangle.calculator.TradeGroupOptionStrangleNoTailCalculator;
import com.clifton.trade.group.strangle.calculator.TradeGroupStrangleCalculator;


/**
 * The <code>TradeGroupStrangleCalculatorMethods</code> enum defines ways in which Trade lists can be strangled.
 *
 * @author NickK
 */
public enum TradeGroupStrangleCalculatorMethods {
	OPTIONS_NO_TAIL(new TradeGroupOptionStrangleNoTailCalculator()),
	OPTIONS_BY_ACCOUNT(new TradeGroupOptionStrangleByClientAccountCalculator()),
	OPTIONS_ACROSS_ACCOUNTS(new TradeGroupOptionStrangleAcrossClientAccountCalculator());

	private final TradeGroupStrangleCalculator strangleCalculator;


	TradeGroupStrangleCalculatorMethods(TradeGroupStrangleCalculator strangleCalculator) {
		this.strangleCalculator = strangleCalculator;
	}


	public TradeGroupStrangleCalculator getStrangleCalculator() {
		return this.strangleCalculator;
	}
}
