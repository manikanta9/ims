package com.clifton.trade.group.strangle.validate;

import com.clifton.core.util.validation.ValidationException;
import com.clifton.trade.Trade;


/**
 * <code>TradeGroupStrangleTradeValidator</code> is an an interface that can be used
 * to validate trades during strangle processing. If a trade being processes/strangled
 * fails to validate, a {@link ValidationException} should be thrown.
 *
 * @author NickK
 */
public interface TradeGroupStrangleTradeValidator {

	public void validate(Trade tradeToValidate);
}
