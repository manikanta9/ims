package com.clifton.trade.group.strangle.calculator;

import com.clifton.core.context.Context;
import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.group.strangle.TradeGroupStrangle;
import com.clifton.trade.group.strangle.TradeGroupStrangleCalculatorContext;

import java.math.BigDecimal;


/**
 * <code>BaseTradeGroupStrangleCalculator</code> is the base implementation for {@code TradeGroupStrangleCalculator}.
 * This implementation sets the provided context for concrete instances to access. This class will handle saving
 * the generated {@code TradeGroupStrangle} if necessary.
 *
 * @author NickK
 */
public abstract class BaseTradeGroupStrangleCalculator implements TradeGroupStrangleCalculator {

	private static final Context context = new ThreadLocalContextHandler();

	private static final String CONTEXT_STRANGLE_CONTEXT_KEY = "strangleContext";


	////////////////////////////////////////////////////////////////////////////
	//////            Trade Group Strangle Calculator Methods            ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeGroupStrangle populateTradeGroupStrangle(TradeGroupStrangleCalculatorContext strangleCalculatorContext) {
		try {
			getContext().setBean(CONTEXT_STRANGLE_CONTEXT_KEY, strangleCalculatorContext);
			TradeGroupStrangle strangle = processTradeGroupStrangle();
			if (getStrangleContext().getTradeGroup() != null) {
				strangle.setParent(getStrangleContext().getTradeGroup());
			}
			return strangle;
		}
		finally {
			getContext().removeBean(CONTEXT_STRANGLE_CONTEXT_KEY);
		}
	}


	protected abstract TradeGroupStrangle processTradeGroupStrangle();


	protected Trade[] splitTrade(Trade trade, BigDecimal quantityToSplit) {
		return getStrangleContext().getTradeService().splitTrade(trade, quantityToSplit);
	}


	////////////////////////////////////////////////////////////////////////////
	//////                      Getter Methods                           ///////
	////////////////////////////////////////////////////////////////////////////


	private Context getContext() {
		return context;
	}


	protected TradeGroupStrangleCalculatorContext getStrangleContext() {
		return (TradeGroupStrangleCalculatorContext) getContext().getBean(CONTEXT_STRANGLE_CONTEXT_KEY);
	}
}
