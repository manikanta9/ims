package com.clifton.trade.group.dynamic;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.group.TradeGroup;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The TradeGroupDynamicService exposes methods for working with Trade Groups that are of a type that use a dynamic calculator bean
 * These often have custom properties, display of the details vs. individual trades, etc, and make it easier to view a trade group
 * as it relates to the underlying portfolios.
 *
 * @author manderson
 */
public interface TradeGroupDynamicService<D extends TradeGroupDynamicDetail> {

	////////////////////////////////////////////////////////////////////////////////
	//////    Trade Group Dynamic - Loading/Reviewing Existing Trade Group     /////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For the given TradeGroupID loads the existing group/trades and details
	 * based on the saved Trade Group properties and trades associated with that trade group
	 */
	@SecureMethod(dtoClass = TradeGroup.class)
	public TradeGroupDynamic<D> getTradeGroupDynamic(int id);


	////////////////////////////////////////////////////////////////////////////////
	////////     Trade Group Dynamic Entry - Creating New Trade Groups       ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For the given TradeGroupDynamic properties runs the calculator to generate the list of detail rows
	 * This is used the "load details" on the entry screen based on a new Trade Group that hasn't been created yet.
	 */
	@SecureMethod(dtoClass = TradeGroup.class)
	public TradeGroupDynamic<D> getTradeGroupDynamicEntry(TradeGroupDynamic<D> bean);


	/**
	 * For the given TradeGroupDynamic bean with detail rows populated with trade quantities
	 * creates the actual trade group and list of Trades in the system
	 * Returns the TradeGroup so the group window can automatically open with the trade list
	 */
	@SecureMethod(dtoClass = TradeGroup.class)
	public TradeGroupDynamic<D> saveTradeGroupDynamicEntry(TradeGroupDynamic<D> bean);


	/**
	 * For the given TradeGroupDynamic bean run the calculator to apply the Trade Group Action passed from the
	 * client. This is used to process Trade Group Actions, particularly Trade Workflow transitions and updates
	 * for the TradeGroupDynamic's Trade list.
	 */
	@RequestMapping("tradeGroupDynamicActionExecute")
	@SecureMethod(dtoClass = TradeGroup.class, permissionResolverBeanName = "tradeGroupActionSecureMethodPermissionResolver")
	public TradeGroupDynamic<D> executeTradeGroupDynamicAction(TradeGroupDynamic<D> bean);
}
