package com.clifton.trade.group;

import com.clifton.core.security.authorization.SecureMethodPermissionResolver;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * TradeGroupActionSecureMethodPermissionResolver returns permission level required in order to execute a specific trade group action
 * (where the action enum is passed as a parameter in the request).
 *
 * @author manderson
 */
@Component
public class TradeGroupActionSecureMethodPermissionResolver implements SecureMethodPermissionResolver {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public int getRequiredPermissions(Method method, Map<String, ?> config, String securityResourceName) {
		TradeGroup.TradeGroupActions tradeGroupAction = getTradeGroupAction(config);
		if (tradeGroupAction != null && tradeGroupAction.isReadAccessSufficientToExecute()) {
			return SecurityPermission.PERMISSION_READ;
		}
		return SecurityPermission.PERMISSION_WRITE;
	}

	////////////////////////////////////////////////////////////////////////////////


	private TradeGroup.TradeGroupActions getTradeGroupAction(Map<String, ?> config) {
		TradeGroup.TradeGroupActions tradeGroupAction = null;

		// Two options - some methods pass action as it's own parameter
		String actionName = MapUtils.getParameterAsString("action", config);
		if (StringUtils.isEmpty(actionName)) {
			// Or it could be the tradeGroupAction property on the group itself
			actionName = MapUtils.getParameterAsString("tradeGroupAction", config);
		}
		if (!StringUtils.isEmpty(actionName)) {
			tradeGroupAction = TradeGroup.TradeGroupActions.valueOf(actionName);
		}
		return tradeGroupAction;
	}
}
