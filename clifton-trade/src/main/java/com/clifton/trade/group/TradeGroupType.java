package com.clifton.trade.group;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>TradeGroupType</code> represents the types of trade groups we support
 * Rolls, Tails, Multi-Client, PIOS
 *
 * @author Mary Anderson
 */
@CacheByName(filterable = true)
public class TradeGroupType extends NamedEntity<Short> {

	private boolean securityRequired;
	private boolean secondarySecurityRequired;

	// If false, trade groups created with one trade will actually just create the trade and won't group it
	private boolean oneTradeAllowed;

	// If true, requires the Trade Group to have a parent Trade Group 
	private boolean parentGroupRequired;

	// specifies whether groups of this type use workflow: if false, workflow state/status on the group won't be updated
	private boolean workflowRequired;

	// If true, the trades of the group are roll trades where an existing position is closed/rolled to a new position with a different security
	private boolean roll;

	// If true, the trades of the group are from an imported trade file (e.g. excel files)
	private boolean tradeImport;

	/**
	 * Optional table this group was created for
	 * i.e. Portfolio Run Trade Groups are created from ProductOverlayRun table
	 */
	private SystemTable sourceSystemTable;

	/**
	 * In some cases the drill down into the sourceSystemTable needs to be overridden
	 * i.e. ProductOverlayRun table's detail screen class is the run window
	 * but for trading purposes - want to drill into the Portfolio Trade Creation window
	 */
	private String sourceDetailScreenClass;

	/**
	 * Some groups display the data differently, and a generic list of trades in the group is not helpful
	 * i.e. Roll Trades is good to see the 2 trades (roll out of and roll into) on the same line
	 */
	private String detailScreenClass;


	/**
	 * Trade groups where trade entry/review is done via a system bean that implements {@link com.clifton.trade.group.dynamic.calculators.TradeGroupDynamicCalculator}
	 * Example: Defensive Equity: Portfolio Option Rolls, Defensive Equity: Rebalancing
	 */
	private SystemBean dynamicCalculatorBean;


	/**
	 * Allow trades of this TradeGroupType to be saved with no executing broker, and update it after execution is complete.
	 * <p>
	 * This is used for 'Option Roll', 'Option Strangle', and 'Option Tail' TradeGroupTypes because the trades get quotes
	 * from different brokers post approval, and may be executed in different strangle structures.
	 */
	private boolean brokerSelectionAtExecutionAllowed;


	/**
	 * Some trade groups are hierarchical in nature (e.g. Option Roll, Option Trade To Close, and Option Strangle.
	 * The parent group may have no Trades because they are moved to a child group(s).
	 */
	private boolean emptyTradeListAllowed;

	/**
	 * In some cases with hierarchical groups, the trade list will be empty on the initial save (e.g. Option Strangle).
	 * because the trades are part of a child group.
	 */
	private boolean emptyTradeListOnInitialSaveAllowed;


	/**
	 * Some trade group types can create {@link com.clifton.trade.assetclass.TradeAssetClassPositionAllocation}s
	 * (e.g. Portfolio Run, Roll Trade/Tail Trade, and Trade Import). This flag can be used to determine if
	 * the a group type allows this functionality.
	 */
	private boolean tradeAssetClassAllocationAllowed;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The <code>GroupType</code> is an enum representation of the TradeGroupType
	 * that we can use in code for comparisons
	 *
	 * @author Mary Anderson
	 */
	public enum GroupTypes {
		PORTFOLIO("Portfolio Run"),
		MULTI_CLIENT("Multi-Client Trade"),
		TRADE_IMPORT("Trade Import"),
		ROLL_TRADE_IMPORT("Roll Trade Import"),
		INTEGRATION_TRADE_IMPORT("Integration Trade Import"),
		EFP("Exchange For Physical"),
		// Types related to rolling securities
		ROLL("Roll Trade"), // Futures
		ROLL_TAIL("Roll Tail Trade"), // Futures
		FORWARD_ROLL("Forward Roll"),
		BOND_ROLL("Bond Roll"),
		// Types related to Option trading
		OPTION_EXPIRATION("Option Expiration"),
		OPTION_PHYSICAL_DELIVERY("Option Physical Delivery"),
		OPTION_DELIVERY_OFFSET("Option Delivery Offset"),
		OPTION_TRADE_TO_CLOSE("Option Trade To Close"),
		OPTION_ASSIGNMENT("Option Assignment"),
		OPTION_ROLL_FORWARD("Option Roll Forward"),
		OPTION_ROLL("Option Roll"),
		OPTION_STRANGLE("Option Strangle"), // Strangled Option trades (Put and Call); used by Option Roll and Option Trade To Close
		OPTION_TAIL("Option Tail"), // Outright trades of a strangle; used by Option Roll and Option Trade To Close
		OPTION_CASH_FLOW("Option Cash Flow"), // Portfolio trades for Options
		FUNGE("Funge"), // Corrects positions after delivery of Option underlying

		UNKNOWN("Unknown");


		GroupTypes(String typeName) {
			this.typeName = typeName;
		}


		private final String typeName;


		public String getTypeName() {
			return this.typeName;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public GroupTypes getGroupType() {
		for (GroupTypes type : GroupTypes.values()) {
			if (type.getTypeName().equals(getName())) {
				return type;
			}
		}
		return GroupTypes.UNKNOWN;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                 Getters and Setters                          /////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSecurityRequired() {
		return this.securityRequired;
	}


	public void setSecurityRequired(boolean securityRequired) {
		this.securityRequired = securityRequired;
	}


	public boolean isSecondarySecurityRequired() {
		return this.secondarySecurityRequired;
	}


	public void setSecondarySecurityRequired(boolean secondarySecurityRequired) {
		this.secondarySecurityRequired = secondarySecurityRequired;
	}


	public boolean isOneTradeAllowed() {
		return this.oneTradeAllowed;
	}


	public void setOneTradeAllowed(boolean oneTradeAllowed) {
		this.oneTradeAllowed = oneTradeAllowed;
	}


	public boolean isParentGroupRequired() {
		return this.parentGroupRequired;
	}


	public void setParentGroupRequired(boolean parentGroupRequired) {
		this.parentGroupRequired = parentGroupRequired;
	}


	public boolean isWorkflowRequired() {
		return this.workflowRequired;
	}


	public void setWorkflowRequired(boolean workflowRequired) {
		this.workflowRequired = workflowRequired;
	}


	public boolean isRoll() {
		return this.roll;
	}


	public void setRoll(boolean roll) {
		this.roll = roll;
	}


	public boolean isTradeImport() {
		return this.tradeImport;
	}


	public void setTradeImport(boolean tradeImport) {
		this.tradeImport = tradeImport;
	}


	public SystemTable getSourceSystemTable() {
		return this.sourceSystemTable;
	}


	public void setSourceSystemTable(SystemTable sourceSystemTable) {
		this.sourceSystemTable = sourceSystemTable;
	}


	public String getSourceDetailScreenClass() {
		return this.sourceDetailScreenClass;
	}


	public void setSourceDetailScreenClass(String sourceDetailScreenClass) {
		this.sourceDetailScreenClass = sourceDetailScreenClass;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public SystemBean getDynamicCalculatorBean() {
		return this.dynamicCalculatorBean;
	}


	public void setDynamicCalculatorBean(SystemBean dynamicCalculatorBean) {
		this.dynamicCalculatorBean = dynamicCalculatorBean;
	}


	public boolean isBrokerSelectionAtExecutionAllowed() {
		return this.brokerSelectionAtExecutionAllowed;
	}


	public void setBrokerSelectionAtExecutionAllowed(boolean brokerSelectionAtExecutionAllowed) {
		this.brokerSelectionAtExecutionAllowed = brokerSelectionAtExecutionAllowed;
	}


	public boolean isEmptyTradeListAllowed() {
		return this.emptyTradeListAllowed;
	}


	public void setEmptyTradeListAllowed(boolean emptyTradeListAllowed) {
		this.emptyTradeListAllowed = emptyTradeListAllowed;
	}


	public boolean isEmptyTradeListOnInitialSaveAllowed() {
		return this.emptyTradeListOnInitialSaveAllowed;
	}


	public void setEmptyTradeListOnInitialSaveAllowed(boolean emptyTradeListOnInitialSaveAllowed) {
		this.emptyTradeListOnInitialSaveAllowed = emptyTradeListOnInitialSaveAllowed;
	}


	public boolean isTradeAssetClassAllocationAllowed() {
		return this.tradeAssetClassAllocationAllowed;
	}


	public void setTradeAssetClassAllocationAllowed(boolean tradeAssetClassAllocationAllowed) {
		this.tradeAssetClassAllocationAllowed = tradeAssetClassAllocationAllowed;
	}
}
