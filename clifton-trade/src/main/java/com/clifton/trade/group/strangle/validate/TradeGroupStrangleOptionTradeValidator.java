package com.clifton.trade.group.strangle.validate;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * <code>TradeGroupStrangleOptionTradeValidator</code> is a base implementation for validating
 * options in strangle processing. The trade list for option strangle must meet the following criteria:
 * - all trades must be either a buy or sell (cannot have a mix of each)
 * - all trades must be an option
 * - the options must expire on the same date
 * - the trades must match one of two securities, the put or the call
 *
 * @author NickK
 */
public class TradeGroupStrangleOptionTradeValidator implements TradeGroupStrangleTradeValidator {

	Set<InvestmentSecurity> processedSecurityList = new HashSet<>(2);
	private Boolean buy = null;
	private Date endDate = null;


	@Override
	public void validate(Trade tradeToValidate) {
		validateTradeIsOption(tradeToValidate);
		validateTradeSecurityList(tradeToValidate);
		validateTradeEndDate(tradeToValidate);
		validateTradeBuyStatus(tradeToValidate);
	}


	private void validateTradeSecurityList(Trade trade) {
		if (this.processedSecurityList.add(trade.getInvestmentSecurity())) {
			ValidationUtils.assertTrue(this.processedSecurityList.size() <= 2, () -> "There can only be 2 individual securities (1 put and 1 call) for [OPTIONS_STRANGLE] grouping.");
		}
	}


	private void validateTradeIsOption(Trade trade) {
		ValidationUtils.assertTrue(InvestmentType.OPTIONS.equalsIgnoreCase(trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName()), () -> "["
				+ trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType() + "] is not supported by [OPTIONS_STRANGLE] grouping.");
	}


	private void validateTradeEndDate(Trade trade) {
		if (this.endDate == null) {
			this.endDate = trade.getInvestmentSecurity().getLastDeliveryDate();
		}
		else {
			ValidationUtils.assertTrue(DateUtils.compare(this.endDate, trade.getInvestmentSecurity().getLastDeliveryDate(), false) == 0, () -> "Options must expire on the same day. ["
					+ trade.getInvestmentSecurity().getSymbol() + "] expires on [" + DateUtils.fromDate(trade.getInvestmentSecurity().getLastDeliveryDate(), DateUtils.DATE_FORMAT_INPUT) + "] but ["
					+ DateUtils.fromDate(this.endDate, DateUtils.DATE_FORMAT_INPUT) + "] was expected.");
		}
	}


	private void validateTradeBuyStatus(Trade trade) {
		if (this.buy == null) {
			this.buy = trade.isBuy();
		}
		else {
			ValidationUtils.assertTrue(this.buy == trade.isBuy(), () -> "All trades must be in the same direction for [OPTIONS_STRANGLE] grouping.");
		}
	}
}
