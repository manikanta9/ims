package com.clifton.trade.group.strangle;

import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.strangle.validate.TradeGroupStrangleTradeValidator;

import java.util.List;


/**
 * <code>TradeGroupStrangleCalculatorContext</code> serves as the sole argument for {@code TradeGroupStrangleCalculator}.
 * This class contains the necessary services and information needed during strangle calculation.
 *
 * @author NickK
 */
public class TradeGroupStrangleCalculatorContext {

	private TradeGroup tradeGroup;

	private List<Trade> tradeList;

	private TradeGroupStrangleTradeValidator tradeValidator;

	private TradeService tradeService;

	private TradeGroupService tradeGroupService;

	private SystemColumnValueHandler systemColumnValueHandler;

	// flag to signal new Trades created during strangle should be saved
	private boolean saveSplitTrade;


	public TradeGroupStrangleCalculatorContext(List<Trade> tradeList, TradeGroupStrangleTradeValidator tradeValidator) {
		setTradeList(tradeList);
		setTradeValidator(tradeValidator);
	}


	public TradeGroupStrangleCalculatorContext(TradeGroup tradeGroup, TradeGroupStrangleTradeValidator tradeValidator) {
		setTradeGroup(tradeGroup);
		setTradeValidator(tradeValidator);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                 Getter and Setter Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	public List<Trade> getTradeList() {
		return this.tradeList;
	}


	public void setTradeList(List<Trade> tradeList) {
		this.tradeList = tradeList;
	}


	public TradeGroupStrangleTradeValidator getTradeValidator() {
		return this.tradeValidator;
	}


	public void setTradeValidator(TradeGroupStrangleTradeValidator tradeValidator) {
		this.tradeValidator = tradeValidator;
	}


	public TradeGroup getTradeGroup() {
		return this.tradeGroup;
	}


	public void setTradeGroup(TradeGroup tradeGroup) {
		this.tradeGroup = tradeGroup;
		if (tradeGroup != null) {
			setTradeList(tradeGroup.getTradeList());
		}
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public boolean isSaveSplitTrade() {
		return this.saveSplitTrade;
	}


	public void setSaveSplitTrade(boolean saveSplitTrade) {
		this.saveSplitTrade = saveSplitTrade;
	}
}
