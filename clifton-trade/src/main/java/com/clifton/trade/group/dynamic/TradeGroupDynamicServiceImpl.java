package com.clifton.trade.group.dynamic;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.dynamic.calculators.TradeGroupDynamicCalculator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Service
public class TradeGroupDynamicServiceImpl<D extends TradeGroupDynamicDetail> implements TradeGroupDynamicService<D> {

	private SystemBeanService systemBeanService;

	private TradeGroupService tradeGroupService;

	////////////////////////////////////////////////////////////////////////////////
	//////    Trade Group Dynamic - Loading/Reviewing Existing Trade Group     /////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For the given TradeGroupID loads the existing group/trades and details
	 * based on the saved Trade Group properties and trades associated with that trade group
	 */
	@Override
	@Transactional(readOnly = true) // improves performance by starting transaction only once
	public TradeGroupDynamic<D> getTradeGroupDynamic(int id) {
		TradeGroup group = getTradeGroupService().getTradeGroup(id);
		TradeGroupDynamicCalculator<TradeGroupDynamic<D>, D> calculator = getTradeGroupDynamicCalculator(group);
		TradeGroupDynamic<D> result = calculator.getTradeGroupDynamic(group);
		sortTradeGroupDynamicDetailList(result);
		return convertTradeListTradesToHaveIdOnly(result);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////     Trade Group Dynamic Entry - Creating New Trade Groups       ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * For the given TradeGroupDynamic properties runs the calculator to generate the list of detail rows
	 * This is used the "load details" on the entry screen based on a new Trade Group that hasn't been created yet.
	 */
	@Override
	@Transactional(readOnly = true) // improves performance by starting transaction only once
	public TradeGroupDynamic<D> getTradeGroupDynamicEntry(TradeGroupDynamic<D> bean) {
		TradeGroupDynamicCalculator<TradeGroupDynamic<D>, D> calculator = getTradeGroupDynamicCalculator(bean);
		TradeGroupDynamic<D> result = calculator.populateTradeGroupDynamicEntry(bean);
		sortTradeGroupDynamicDetailList(result);
		return convertTradeListTradesToHaveIdOnly(result);
	}


	/**
	 * For the given TradeGroupDynamic bean with detail rows populated with trade quantities
	 * creates the actual trade group and list of Trades in the system
	 * Returns the TradeGroup so the group window can automatically open with the trade list
	 */
	@Override
	@Transactional(timeout = 180) // matching timeout for trade group save
	public TradeGroupDynamic<D> saveTradeGroupDynamicEntry(TradeGroupDynamic<D> bean) {
		TradeGroupDynamicCalculator<TradeGroupDynamic<D>, D> calculator = getTradeGroupDynamicCalculator(bean);
		TradeGroupDynamic<D> group = calculator.populateTradeGroupFromTradeGroupDynamicEntry(bean);
		// Make defensive copy to avoid issues during persistence.
		TradeGroup saved = new TradeGroup();
		BeanUtils.copyProperties(group, saved);
		saved = getTradeGroupService().saveTradeGroup(saved);
		BeanUtils.copyProperties(saved, bean);
		sortTradeGroupDynamicDetailList(bean);
		return convertTradeListTradesToHaveIdOnly(bean);
	}


	@Override
	// Do not use Transactional, follow behavior of TradeGroupService
	public TradeGroupDynamic<D> executeTradeGroupDynamicAction(TradeGroupDynamic<D> bean) {
		if (bean.isNewBean()) {
			throw new ValidationException("Cannot process a Trade Group Action on a new Trade Group Dynamic.");
		}
		TradeGroupDynamicCalculator<TradeGroupDynamic<D>, D> calculator = getTradeGroupDynamicCalculator(bean);
		calculator.executeTradeGroupDynamicAction(bean);
		return convertTradeListTradesToHaveIdOnly(getTradeGroupDynamic(bean.getId()));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                    Helper Methods                      ////////////
	////////////////////////////////////////////////////////////////////////////////


	private TradeGroupDynamicCalculator<TradeGroupDynamic<D>, D> getTradeGroupDynamicCalculator(TradeGroup bean) {
		if (bean.getTradeGroupType() == null) {
			throw new ValidationException("Missing Trade Group Type");
		}
		if (bean.getTradeGroupType().getDynamicCalculatorBean() == null) {
			throw new ValidationException("Missing dynamic calculator for trade group type " + bean.getTradeGroupType().getName());
		}
		//noinspection unchecked
		return (TradeGroupDynamicCalculator<TradeGroupDynamic<D>, D>) getSystemBeanService().getBeanInstance(bean.getTradeGroupType().getDynamicCalculatorBean());
	}


	private void sortTradeGroupDynamicDetailList(TradeGroupDynamic<D> bean) {
		// Always Return Details Sorted By Client Account Number
		BeanUtils.sortWithFunction(bean.getDetailList(), tradeGroupDynamicDetail -> (tradeGroupDynamicDetail.getClientAccount() == null ? null : tradeGroupDynamicDetail.getClientAccount().getNumber()), true);
	}


	/**
	 * Converts the TradeGroupDynamic's list of fully populated Trades to a list of Trades that
	 * only have a populated ID. This conserves the amount of data serialized between the server
	 * and the UI cutting down the response times significantly if the requested max depth is
	 * greater than five. The UI uses the TradeGroupDynamic's list of details, not the trade list.
	 */
	private TradeGroupDynamic<D> convertTradeListTradesToHaveIdOnly(TradeGroupDynamic<D> tradeGroupDynamic) {
		tradeGroupDynamic.setTradeList(CollectionUtils.getStream(tradeGroupDynamic.getTradeList()).map(trade -> {
			Trade tradeWithIdOnly = new Trade();
			tradeWithIdOnly.setId(trade.getId());
			return tradeWithIdOnly;
		}).collect(Collectors.toList()));
		return tradeGroupDynamic;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}
}
