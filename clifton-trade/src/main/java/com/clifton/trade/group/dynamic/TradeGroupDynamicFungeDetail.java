package com.clifton.trade.group.dynamic;

import com.clifton.trade.Trade;

import java.math.BigDecimal;


/**
 * <code>TradeGroupDynamicFungeDetail</code> represents the position and trade details for a
 * client and holding account for processing funge trades.
 *
 * @author NickK
 */
public class TradeGroupDynamicFungeDetail extends TradeGroupDynamicDetail {

	// Current position quantities to offset
	private BigDecimal bigQuantity;
	private BigDecimal miniQuantity;

	// Trades to offset physical delivery positions
	private Trade bigTrade;
	private Trade miniTrade;

	// Commission override and fill properties
	private BigDecimal commissionPerUnit;
	private BigDecimal fillPrice;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BigDecimal getBigQuantity() {
		return this.bigQuantity;
	}


	public void setBigQuantity(BigDecimal bigQuantity) {
		this.bigQuantity = bigQuantity;
	}


	public BigDecimal getMiniQuantity() {
		return this.miniQuantity;
	}


	public void setMiniQuantity(BigDecimal miniQuantity) {
		this.miniQuantity = miniQuantity;
	}


	public Trade getBigTrade() {
		return this.bigTrade;
	}


	public void setBigTrade(Trade bigTrade) {
		this.bigTrade = bigTrade;
	}


	public Trade getMiniTrade() {
		return this.miniTrade;
	}


	public void setMiniTrade(Trade miniTrade) {
		this.miniTrade = miniTrade;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getFillPrice() {
		return this.fillPrice;
	}


	public void setFillPrice(BigDecimal fillPrice) {
		this.fillPrice = fillPrice;
	}
}
