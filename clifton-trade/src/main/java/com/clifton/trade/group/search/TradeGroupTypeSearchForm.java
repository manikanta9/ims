package com.clifton.trade.group.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author nickk
 */
public class TradeGroupTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Boolean securityRequired;

	@SearchField
	private Boolean secondarySecurityRequired;

	@SearchField
	private Boolean oneTradeAllowed;

	@SearchField
	private Boolean parentGroupRequired;

	@SearchField
	private Boolean workflowRequired;

	@SearchField
	private Boolean roll;
	@SearchField
	private Boolean tradeImport;

	@SearchField(searchField = "sourceSystemTable.id")
	private Short sourceSystemTableId;

	@SearchField(searchField = "name", searchFieldPath = "sourceSystemTable")
	private String sourceSystemTableName;

	@SearchField
	private String sourceDetailScreenClass;

	@SearchField
	private String detailScreenClass;

	@SearchField(searchField = "dynamicCalculatorBean.id")
	private Integer dynamicCalculatorBeanId;

	@SearchField(searchField = "name", searchFieldPath = "dynamicCalculatorBean")
	private String dynamicCalculatorBeanName;

	@SearchField
	private Boolean brokerSelectionAtExecutionAllowed;

	@SearchField
	private Boolean emptyTradeListAllowed;

	@SearchField
	private Boolean emptyTradeListOnInitialSaveAllowed;

	@SearchField
	private Boolean tradeAssetClassAllocationAllowed;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Boolean getSecurityRequired() {
		return this.securityRequired;
	}


	public void setSecurityRequired(Boolean securityRequired) {
		this.securityRequired = securityRequired;
	}


	public Boolean getSecondarySecurityRequired() {
		return this.secondarySecurityRequired;
	}


	public void setSecondarySecurityRequired(Boolean secondarySecurityRequired) {
		this.secondarySecurityRequired = secondarySecurityRequired;
	}


	public Boolean getOneTradeAllowed() {
		return this.oneTradeAllowed;
	}


	public void setOneTradeAllowed(Boolean oneTradeAllowed) {
		this.oneTradeAllowed = oneTradeAllowed;
	}


	public Boolean getParentGroupRequired() {
		return this.parentGroupRequired;
	}


	public void setParentGroupRequired(Boolean parentGroupRequired) {
		this.parentGroupRequired = parentGroupRequired;
	}


	public Boolean getWorkflowRequired() {
		return this.workflowRequired;
	}


	public void setWorkflowRequired(Boolean workflowRequired) {
		this.workflowRequired = workflowRequired;
	}


	public Boolean getRoll() {
		return this.roll;
	}


	public void setRoll(Boolean roll) {
		this.roll = roll;
	}


	public Boolean getTradeImport() {
		return this.tradeImport;
	}


	public void setTradeImport(Boolean tradeImport) {
		this.tradeImport = tradeImport;
	}


	public Short getSourceSystemTableId() {
		return this.sourceSystemTableId;
	}


	public void setSourceSystemTableId(Short sourceSystemTableId) {
		this.sourceSystemTableId = sourceSystemTableId;
	}


	public String getSourceSystemTableName() {
		return this.sourceSystemTableName;
	}


	public void setSourceSystemTableName(String sourceSystemTableName) {
		this.sourceSystemTableName = sourceSystemTableName;
	}


	public String getSourceDetailScreenClass() {
		return this.sourceDetailScreenClass;
	}


	public void setSourceDetailScreenClass(String sourceDetailScreenClass) {
		this.sourceDetailScreenClass = sourceDetailScreenClass;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public Integer getDynamicCalculatorBeanId() {
		return this.dynamicCalculatorBeanId;
	}


	public void setDynamicCalculatorBeanId(Integer dynamicCalculatorBeanId) {
		this.dynamicCalculatorBeanId = dynamicCalculatorBeanId;
	}


	public String getDynamicCalculatorBeanName() {
		return this.dynamicCalculatorBeanName;
	}


	public void setDynamicCalculatorBeanName(String dynamicCalculatorBeanName) {
		this.dynamicCalculatorBeanName = dynamicCalculatorBeanName;
	}


	public Boolean getBrokerSelectionAtExecutionAllowed() {
		return this.brokerSelectionAtExecutionAllowed;
	}


	public void setBrokerSelectionAtExecutionAllowed(Boolean brokerSelectionAtExecutionAllowed) {
		this.brokerSelectionAtExecutionAllowed = brokerSelectionAtExecutionAllowed;
	}


	public Boolean getEmptyTradeListAllowed() {
		return this.emptyTradeListAllowed;
	}


	public void setEmptyTradeListAllowed(Boolean emptyTradeListAllowed) {
		this.emptyTradeListAllowed = emptyTradeListAllowed;
	}


	public Boolean getEmptyTradeListOnInitialSaveAllowed() {
		return this.emptyTradeListOnInitialSaveAllowed;
	}


	public void setEmptyTradeListOnInitialSaveAllowed(Boolean emptyTradeListOnInitialSaveAllowed) {
		this.emptyTradeListOnInitialSaveAllowed = emptyTradeListOnInitialSaveAllowed;
	}


	public Boolean getTradeAssetClassAllocationAllowed() {
		return this.tradeAssetClassAllocationAllowed;
	}


	public void setTradeAssetClassAllocationAllowed(Boolean tradeAssetClassAllocationAllowed) {
		this.tradeAssetClassAllocationAllowed = tradeAssetClassAllocationAllowed;
	}
}
