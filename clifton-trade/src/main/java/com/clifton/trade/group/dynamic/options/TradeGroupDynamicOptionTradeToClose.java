package com.clifton.trade.group.dynamic.options;

import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicOptionStrangle;
import com.clifton.trade.group.dynamic.TradeGroupDynamicOptionStrangleDetail;


/**
 * <code>TradeGroupDynamicOptionTradeToClose</code> defines the implementation of a TradeGroupDynamic of
 * Option Trades that offset an open Option Position for Client Investment Accounts. The trades contained
 * within this group can be strangled to minimize execution costs.
 *
 * @author NickK
 */
public class TradeGroupDynamicOptionTradeToClose extends BaseTradeGroupDynamicOptionStrangle<TradeGroupDynamicOptionStrangleDetail> {
	// Parent abstract class contains all of the necessary information, so this class just defines the generic type.
}
