package com.clifton.trade.group.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.util.Date;


/**
 * The <code>TradeGroupSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class TradeGroupSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchFieldPath = "tradeGroupType", searchField = "name")
	private String tradeGroupTypeName;

	@SearchField(searchField = "name", searchFieldPath = "tradeGroupType", comparisonConditions = {ComparisonConditions.BEGINS_WITH})
	private String beginsWithTradeGroupTypeName;

	// Custom search filter used by Options Blotter to filter on Option related trade groups, names are OR'd together
	private String[] includeBeginsWithTradeGroupTypeNames;

	@SearchField(searchField = "tradeGroupType.id", comparisonConditions = {ComparisonConditions.IN})
	private Short[] includeTradeGroupTypeIds;

	// Used for Options Blotter to exclude child strangle and tail trade groups so only the original groups are visible
	@SearchField(searchField = "tradeGroupType.id", comparisonConditions = {ComparisonConditions.NOT_IN})
	private Short[] excludeTradeGroupTypeIds;

	@SearchField(searchField = "roll", searchFieldPath = "tradeGroupType")
	private Boolean roll;

	@SearchField(searchField = "tradeImport", searchFieldPath = "tradeGroupType")
	private Boolean tradeImport;

	@SearchField
	private Date tradeDate;

	@SearchField(searchField = "traderUser.id")
	private Short traderUserId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchField = "secondaryInvestmentSecurity.id")
	private Integer secondaryInvestmentSecurityId;

	@SearchField(searchField = "parent.id", comparisonConditions = {ComparisonConditions.EQUALS})
	private Integer parentId;

	@SearchField(searchField = "groupList.id", searchFieldPath = "traderUser", comparisonConditions = {ComparisonConditions.EXISTS})
	private Short securityUserGroupId;

	// Custom Search Filters
	private Short investmentGroupId;
	private Integer clientInvestmentAccountGroupId;
	private Integer holdingInvestmentAccountGroupId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getTradeGroupTypeName() {
		return this.tradeGroupTypeName;
	}


	public void setTradeGroupTypeName(String tradeGroupTypeName) {
		this.tradeGroupTypeName = tradeGroupTypeName;
	}


	public String getBeginsWithTradeGroupTypeName() {
		return this.beginsWithTradeGroupTypeName;
	}


	public void setBeginsWithTradeGroupTypeName(String beginsWithTradeGroupTypeName) {
		this.beginsWithTradeGroupTypeName = beginsWithTradeGroupTypeName;
	}


	public String[] getIncludeBeginsWithTradeGroupTypeNames() {
		return this.includeBeginsWithTradeGroupTypeNames;
	}


	public void setIncludeBeginsWithTradeGroupTypeNames(String[] includeBeginsWithTradeGroupTypeNames) {
		this.includeBeginsWithTradeGroupTypeNames = includeBeginsWithTradeGroupTypeNames;
	}


	public Short[] getIncludeTradeGroupTypeIds() {
		return this.includeTradeGroupTypeIds;
	}


	public void setIncludeTradeGroupTypeIds(Short[] includeTradeGroupTypeIds) {
		this.includeTradeGroupTypeIds = includeTradeGroupTypeIds;
	}


	public Short[] getExcludeTradeGroupTypeIds() {
		return this.excludeTradeGroupTypeIds;
	}


	public void setExcludeTradeGroupTypeIds(Short[] excludeTradeGroupTypeIds) {
		this.excludeTradeGroupTypeIds = excludeTradeGroupTypeIds;
	}


	public Boolean getRoll() {
		return this.roll;
	}


	public void setRoll(Boolean roll) {
		this.roll = roll;
	}


	public Boolean getTradeImport() {
		return this.tradeImport;
	}


	public void setTradeImport(Boolean tradeImport) {
		this.tradeImport = tradeImport;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Short getTraderUserId() {
		return this.traderUserId;
	}


	public void setTraderUserId(Short traderUserId) {
		this.traderUserId = traderUserId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getSecondaryInvestmentSecurityId() {
		return this.secondaryInvestmentSecurityId;
	}


	public void setSecondaryInvestmentSecurityId(Integer secondaryInvestmentSecurityId) {
		this.secondaryInvestmentSecurityId = secondaryInvestmentSecurityId;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public Short getSecurityUserGroupId() {
		return this.securityUserGroupId;
	}


	public void setSecurityUserGroupId(Short securityUserGroupId) {
		this.securityUserGroupId = securityUserGroupId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}
}
