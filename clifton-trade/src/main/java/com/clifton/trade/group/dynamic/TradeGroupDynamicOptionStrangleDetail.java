package com.clifton.trade.group.dynamic;

import com.clifton.trade.Trade;


/**
 * <code>TradeGroupDynamicOptionStrangleDetail</code> defines the minimum data required to define a
 * {@link TradeGroupDynamicDetail} of a {@link BaseTradeGroupDynamicOptionStrangle} group.
 *
 * @author NickK
 */
public class TradeGroupDynamicOptionStrangleDetail extends TradeGroupDynamicDetail {

	/**
	 * Trades
	 */
	private Trade putTrade;
	private Trade callTrade;
	private Trade putTailTrade;
	private Trade callTailTrade;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Trade getPutTrade() {
		return this.putTrade;
	}


	public void setPutTrade(Trade putTrade) {
		this.putTrade = putTrade;
	}


	public Trade getPutTailTrade() {
		return this.putTailTrade;
	}


	public void setPutTailTrade(Trade putTailTrade) {
		this.putTailTrade = putTailTrade;
	}


	public Trade getCallTrade() {
		return this.callTrade;
	}


	public void setCallTrade(Trade callTrade) {
		this.callTrade = callTrade;
	}


	public Trade getCallTailTrade() {
		return this.callTailTrade;
	}


	public void setCallTailTrade(Trade callTailTrade) {
		this.callTailTrade = callTailTrade;
	}
}
