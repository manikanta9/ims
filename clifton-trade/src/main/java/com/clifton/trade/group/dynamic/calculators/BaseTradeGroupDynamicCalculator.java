package com.clifton.trade.group.dynamic.calculators;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.dynamic.TradeGroupDynamic;
import com.clifton.trade.group.dynamic.TradeGroupDynamicDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;


/**
 * @author manderson
 */
public abstract class BaseTradeGroupDynamicCalculator<T extends TradeGroupDynamic<D>, D extends TradeGroupDynamicDetail> implements TradeGroupDynamicCalculator<T, D> {

	private TradeGroupService tradeGroupService;


	////////////////////////////////////////////////////////////////////////////////
	/////////////         TradeGroupDynamicCalculator Methods         //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final T populateTradeGroupFromTradeGroupDynamicEntry(T bean) {
		setTradeGroupNoteFromDynamicProperties(bean);
		return populateTradeGroupFromTradeGroupDynamicEntryImpl(bean);
	}


	/**
	 * Populates the TradeGroupDynamic for saving as a TradeGroup. This will
	 * call {@link BaseTradeGroupDynamicCalculator#populateTradeGroupForSavingNewTradeGroupDynamic(TradeGroupDynamic)}
	 * or {@link BaseTradeGroupDynamicCalculator#populateTradeGroupForSavingExistingTradeGroupDynamic(TradeGroupDynamic)}
	 * depending on whether the bean is new.
	 */
	private T populateTradeGroupFromTradeGroupDynamicEntryImpl(T bean) {
		if (bean.isNewBean()) {
			populateTradeGroupForSavingNewTradeGroupDynamic(bean);
		}
		else {
			populateTradeGroupForSavingExistingTradeGroupDynamic(bean);
		}
		return bean;
	}


	/**
	 * Populates the TradeGroupDynamic for saving as a TradeGroup for the initial save.
	 * It is expected that all TradeGroup attributes (e.g. tradeList, tradeGroupType, etc.)
	 * will be populated here.
	 */
	protected abstract void populateTradeGroupForSavingNewTradeGroupDynamic(T bean);


	/**
	 * Populates the TradeGroupDynamic for saving as a TradeGroup as an existing group.
	 * TradeGroups do not generally get saved more than once, thus this method by default
	 * does nothing.
	 * <p>
	 * This serves as a hook to allow the implementation to perform customization of the
	 * TradeGroup's Trade allocation to potential child TradeGroups in the event it is needed.
	 */
	protected void populateTradeGroupForSavingExistingTradeGroupDynamic(@SuppressWarnings("unused") T bean) {
	}


	/**
	 * Populates the TradeGroupDynamic Trade list and then executes the group action.
	 */
	@Override
	public void executeTradeGroupDynamicAction(T bean) {
		populateTradeGroupDynamicTradeListFromDetailList(bean);
		executeTradeGroupAction(bean);
	}


	private void clearTradeGroupDynamicTradeList(T bean) {
		if (!CollectionUtils.isEmpty(bean.getTradeList())) {
			return;
		}
		if (bean.getTradeList() == null) {
			bean.setTradeList(new ArrayList<>());
		}
	}


	/**
	 * Iterates the TradeGroupDynamicDetail for the provided TradeGroupDynamic for populating
	 * the Trades for each detail. The Trade list of the TradeGroupDynamic will be cleared and
	 * reloaded to avoid duplicates. The Trade list will be loaded for initial save or executing
	 * TradeGroup actions based on whether the provided TradeGroupDynamic is a new bean.
	 */
	protected void populateTradeGroupDynamicTradeListFromDetailList(T bean) {
		clearTradeGroupDynamicTradeList(bean);
		Consumer<D> detailPopulator;
		if (bean.isNewBean()) {
			detailPopulator = detail -> populateTradeGroupDynamicTradeListForDetailOnEntrySave(bean, detail);
		}
		else {
			detailPopulator = detail -> populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(bean, detail);
		}
		CollectionUtils.getStream(bean.getDetailList()).forEach(detailPopulator);
	}


	/**
	 * Populate the Trade or Trades for the provided TradeGroupDynamic and TradeGroupDynamicDetail. After the
	 * Trade(s) are populated, they should be added to the TradeGroupDynamic for submission to
	 * {@link TradeGroupService#saveTradeGroup(TradeGroup)}.
	 */
	protected abstract void populateTradeGroupDynamicTradeListForDetailOnEntrySave(T bean, D detail);


	/**
	 * Populate the Trade or Trades for the provided TradeGroupDynamic and TradeGroupDynamicDetail. After the
	 * Trade(s) are populated, they should be added to the TradeGroupDynamic for submission to
	 * {@link TradeGroupService#executeTradeGroupAction(TradeGroup)}.
	 */
	protected abstract void populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(T bean, D detail);


	/**
	 * Submits the provided TradeGroup to {@link TradeGroupService#executeTradeGroupAction(TradeGroup)}.
	 * <p>
	 * If the Trades of the group need to be populated with data based on the action, do so with
	 * {@link BaseTradeGroupDynamicCalculator#populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(TradeGroupDynamic, TradeGroupDynamicDetail)}.
	 */
	protected void executeTradeGroupAction(TradeGroup tradeGroup) {
		getTradeGroupService().executeTradeGroupAction(tradeGroup);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                  Helper Methods                     //////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates TradeGroupNote based on the custom properties that need to be saved for the group
	 * in a format that can be easily retrieved and re-populated on the custom object
	 * propertyName:propertyValue
	 * propertyName2:propertyValue2
	 */
	protected void setTradeGroupNoteFromDynamicProperties(T bean) {
		List<String> propertyNames = bean.getSavePropertiesAsTradeGroupNote();
		if (CollectionUtils.isEmpty(propertyNames)) {
			bean.setNote(null);
		}
		else {
			ObjectToStringConverter converter = new ObjectToStringConverter();

			StringBuilder note = new StringBuilder(128);
			for (String propertyName : propertyNames) {
				Object value = BeanUtils.getPropertyValue(bean, propertyName);
				if (value != null) {
					note.append(propertyName);
					note.append(":");
					note.append(converter.convert(value));
					note.append(StringUtils.NEW_LINE);
				}
			}
			bean.setNote(note.toString());
		}
	}


	/**
	 * If there are Identity Object methods - overriding class should handle that for now...
	 */
	protected void populateDynamicPropertyValuesFromTradeGroupNote(T bean) {
		if (!StringUtils.isEmpty(bean.getNote())) {
			String[] propertyNameValues = bean.getNote().split(StringUtils.NEW_LINE);
			if (propertyNameValues.length > 0) {
				for (String propertyNameValue : propertyNameValues) {
					int index = propertyNameValue.indexOf(":");
					String propertyName = propertyNameValue.substring(0, index);
					String propertyValue = propertyNameValue.substring(index + 1);
					BeanUtils.setPropertyValue(bean, propertyName, propertyValue);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}
}
