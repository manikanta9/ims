package com.clifton.trade.group;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.group.TradeGroup.TradeGroupActions;
import com.clifton.trade.group.search.TradeGroupSearchForm;
import com.clifton.trade.group.search.TradeGroupTypeSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;
import java.util.List;


/**
 * The <code>TradeGroupService</code> interface defines methods for working with TradeGroups & TradeGroupTypes.
 *
 * @author vgomelsky
 */
public interface TradeGroupService {

	//////////////////////////////////////////////////////////////////////////// 
	//////                Trade Group Type Methods                        ////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeGroupType getTradeGroupType(short id);


	public TradeGroupType getTradeGroupTypeByName(String name);


	public List<TradeGroupType> getTradeGroupTypeList(TradeGroupTypeSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<Short> getTradeGroupTypeIdListByNamePrefix(Collection<String> prefixCollection);


	//////////////////////////////////////////////////////////////////////////// 
	//////                   Trade Group Methods                         /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeGroup getTradeGroup(int id);


	public TradeGroup getTradeGroupByParent(int parentId);


	public List<TradeGroup> getTradeGroupList(TradeGroupSearchForm searchForm);


	/**
	 * Saves the provided {@link TradeGroup} and returns the persisted entity.
	 * <p>
	 * If the provided Trade Group has:
	 * - no valid trades, the provided entity will note be saved and will be returned as it was provided.
	 * - one trade and the group's {@link TradeGroupType} does not support one trade, the provided entity will note be saved but the returned entity will included the persisted Trade as its trade list.
	 * - more than one trade, the provided entity will be saved and all member trades will be saved. The returned entity will be the persisted values of the Trade Group and all member Trades.
	 *
	 * @param tradeGroup the group to save
	 * @return the provided entity if it is not an acceptable TradeGroup, or the persisted state of the TradeGroup
	 */
	public TradeGroup saveTradeGroup(TradeGroup tradeGroup);


	/**
	 * Similar to saveTradeGroup however: If tradeAssetClassPositionAllocationList is not empty the will save the trades with automatic transitions disabled (prevent validating the
	 * trade prior to allocations being saved) then will save the allocation list.  Then will manually trigger Validation Trade transition on each trade in the group.
	 */
	@DoNotAddRequestMapping
	public TradeGroup saveTradeGroupWithTradeAllocations(TradeGroup tradeGroup, List<TradeAssetClassPositionAllocation> tradeAssetClassPositionAllocationList);


	/**
	 * This save method is needed to modify an existing TradeGroup without making updates to the list
	 * of trades that are members to the the group. The {@link TradeGroupService#saveTradeGroup(TradeGroup)}
	 * method is used for a new TradeGroup and saves the group's trades. Two needs for this method
	 * include:
	 * <br>- updating the TradeGroup's WorkflowState (TradeGroup is not WorkflowAware)
	 * <br>- updating the group's securities (Option Rolls may need to update the securities to get a different
	 * strike price or look-alike option (OTC)).
	 */
	@DoNotAddRequestMapping
	public TradeGroup saveTradeGroupWithoutTradeUpdates(TradeGroup tradeGroup);


	//////////////////////////////////////////////////////////////////////////// 
	//////                Trade Group Action Methods                     /////// 
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public String executeActionOnTrades(TradeGroupActions action, List<Integer> tradeIdList);


	@DoNotAddRequestMapping
	public String executeActionOnTradeList(TradeGroupActions action, List<Trade> tradeList);


	/**
	 * Executes Validate transition on either a Trade Group (all trades in the group) or an individual trade
	 * Called after ignoring warnings on trades to also immediately validate the trade(s).
	 */
	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeValidateActionExecute")
	public void executeTradeValidateAction(Integer tradeGroupId, Integer tradeId);


	@RequestMapping("tradeGroupActionExecute")
	@SecureMethod(dtoClass = Trade.class, permissionResolverBeanName = "tradeGroupActionSecureMethodPermissionResolver")
	public void executeTradeGroupAction(TradeGroup bean);


	//////////////////////////////////////////////////////////////////////////// 
	//////                   Trade Entry Methods                         /////// 
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = Trade.class)
	public void saveTradeEntry(TradeEntry tradeEntry);
}
