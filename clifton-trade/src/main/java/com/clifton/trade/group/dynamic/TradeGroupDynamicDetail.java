package com.clifton.trade.group.dynamic;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;


/**
 * TradeGroupDynamicDetail is a specific detail line that can be associated with one or more trades
 * for the customized TradeGroupDynamic object.
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class TradeGroupDynamicDetail {

	private Integer id;

	private InvestmentAccount clientAccount;
	private InvestmentAccount holdingAccount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}
}
