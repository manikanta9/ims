package com.clifton.trade.group.strangle.calculator;

import com.clifton.trade.Trade;
import com.clifton.trade.group.strangle.TradeGroupStrangle;
import com.clifton.trade.group.strangle.TradeGroupStrangleCalculatorContext;

import java.math.BigDecimal;


/**
 * <code>TradeGroupStrangleCalculator</code> is a calculator for grouping a list of trades
 * into a strangle with the desired tail allocation.
 *
 * @author NickK
 */
public interface TradeGroupStrangleCalculator {

	/**
	 * Populates a {@code TradeGroupStrangle} based on the provided context. If the context is specified
	 * to save split trades, {@link TradeGroupStrangleCalculatorContext#isSaveSplitTrade()}, the split trades will
	 * be saved using {@link com.clifton.trade.TradeService#splitTrade(Trade, BigDecimal)}.
	 * <p>
	 * The Strangle group(s) will not be saved. Use the {@code TradeGroupStrangleService} to persist the strangle.
	 */
	public TradeGroupStrangle populateTradeGroupStrangle(TradeGroupStrangleCalculatorContext strangleCalculatorContext);
}
