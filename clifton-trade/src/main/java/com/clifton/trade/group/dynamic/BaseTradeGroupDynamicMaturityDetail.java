package com.clifton.trade.group.dynamic;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * <code>BaseTradeGroupDynamicMaturityDetail</code> is a base {@link TradeGroupDynamicDetail} that provides common attributes for
 * {@link BaseTradeGroupDynamicMaturity}s that have a position with a maturing {@link InvestmentSecurity}.
 *
 * @author nickk
 */
public class BaseTradeGroupDynamicMaturityDetail extends TradeGroupDynamicDetail {

	/**
	 * Security for a position that is maturing.
	 */
	private InvestmentSecurity maturingSecurity;
	/**
	 * Position amount (quantity, face) that is maturing
	 */
	private BigDecimal maturingAmount;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isLongPosition() {
		return MathUtils.isGreaterThan(getMaturingAmount(), 0);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getMaturingSecurity() {
		return this.maturingSecurity;
	}


	public void setMaturingSecurity(InvestmentSecurity maturingSecurity) {
		this.maturingSecurity = maturingSecurity;
	}


	public BigDecimal getMaturingAmount() {
		return this.maturingAmount;
	}


	public void setMaturingAmount(BigDecimal maturingAmount) {
		this.maturingAmount = maturingAmount;
	}
}
