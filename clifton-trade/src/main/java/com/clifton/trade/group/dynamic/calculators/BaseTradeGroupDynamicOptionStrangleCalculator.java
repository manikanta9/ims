package com.clifton.trade.group.dynamic.calculators;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicOptionStrangle;
import com.clifton.trade.group.dynamic.TradeGroupDynamicOptionStrangleDetail;
import com.clifton.trade.group.strangle.TradeGroupStrangle;
import com.clifton.trade.group.strangle.TradeGroupStrangleCalculatorMethods;
import com.clifton.trade.group.strangle.TradeGroupStrangleService;
import com.clifton.trade.group.strangle.validate.TradeGroupStrangleOptionTradeValidator;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * <code>BaseTradeGroupDynamicOptionStrangleCalculator</code> is a base {@link TradeGroupDynamicCalculator} containing
 * common functionality for working with strangling Trades of {@link BaseTradeGroupDynamicOptionStrangle} groups.
 *
 * @author NickK
 */
public abstract class BaseTradeGroupDynamicOptionStrangleCalculator<T extends BaseTradeGroupDynamicOptionStrangle<D>, D extends TradeGroupDynamicOptionStrangleDetail> extends BaseTradeGroupDynamicCalculator<T, D> {

	private InvestmentCalculator investmentCalculator;

	private TradeService tradeService;
	private TradeGroupStrangleService tradeGroupStrangleService;

	private SystemColumnValueHandler systemColumnValueHandler;

	/**
	 * First access - looks up TradeType by Name = "Options" and
	 * sets it.  Could make this configurable, however the calculator itself is Options specific
	 */
	private TradeType tradeType;

	////////////////////////////////////////////////////////////////////////////////
	/////////////         TradeGroupDynamicCalculator Methods         //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public T getTradeGroupDynamic(TradeGroup bean) {
		T group = createTradeGroupDynamicOptionStrangle();
		BeanUtils.copyProperties(bean, group);
		populateDynamicPropertyValuesFromTradeGroupNote(group);
		populateTradeGroupDynamicDefaults(group);
		populateTradeGroupDynamicEntry(group);
		// Verify WorkflowState
		syncTradeGroupDynamicOptionStrangleWorkflowStateWithStrangleWorkflowState(group);
		return group;
	}

	///////////////////////////////////////////////////////////////////////////
	///////           BaseTradeGroupDynamicCalculator Methods          ////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateTradeGroupForSavingNewTradeGroupDynamic(T group) {
		InvestmentSecurity rollOption = group.getCallSecurity();
		if (rollOption == null) {
			rollOption = group.getPutSecurity();
		}
		ValidationUtils.assertNotNull(rollOption, "Either a Call or Put Option is required", "callSecurity");

		// Default Settlement Date If Blank
		if (group.getSettlementDate() == null) {
			// Neither of these should ever happen because the screen requires these values
			ValidationUtils.assertNotNull(group.getTradeDate(), "Trade Date is required.", "tradeDate");
			// The settlement date would be the same for the call and the put because they both are associated with the same instrument.
			group.setSettlementDate(getInvestmentCalculator().calculateSettlementDate(rollOption, null, group.getTradeDate()));
		}
		populateTradeGroupDynamicTradeListFromDetailList(group);
	}


	@Override
	protected void populateTradeGroupForSavingExistingTradeGroupDynamic(T group) {
		TradeGroupStrangleCalculatorMethods strangleMethod = group.getStrangleCalculatorMethod();
		// clear the strangle calculator method to prevent the strangle from being populated twice.
		group.setStrangleCalculatorMethod(null);
		// populate the Option Group entries
		populateTradeGroupDynamicEntry(group);

		if (strangleMethod != null) {
			TradeGroupStrangle savedStrangle = getTradeGroupStrangleService().createTradeGroupStrangleForTradeGroup(group, new TradeGroupStrangleOptionTradeValidator(), strangleMethod);
			group.setStrangleGroup(savedStrangle);
			// clear Option Group detail list of trades so they can be repopulated with the strangled trades.
			clearTradeGroupDynamicDetailListTrades(group);

			populateTradeGroupDynamicDetailListFromTradeGroupStrangle(group, savedStrangle);
			/*
			 * Remove the trade list. If we do not, the Strangled Trades will be updated and removed
			 * from the Strangle Group and added to the Option Group.upon Hibernate commit.
			 */
			group.setTradeList(null);
		}
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnEntrySave(T group, D detail) {
		if (detail.getCallTrade() != null && !MathUtils.isNullOrZero(detail.getCallTrade().getQuantityIntended())) {
			group.addTrade(populateTradeDetails(group, detail, group.getCallSecurity(), detail.getCallTrade()));
		}
		if (detail.getPutTrade() != null && !MathUtils.isNullOrZero(detail.getPutTrade().getQuantityIntended())) {
			group.addTrade(populateTradeDetails(group, detail, group.getPutSecurity(), detail.getPutTrade()));
		}
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(T group, D detail) {
		/*
		 * We can populate the trade lists directly to the Option Group as long as we start with
		 * the strangle trades of the detail rows.
		 */
		addTradeToTradeGroupDynamic(group, detail, detail.getPutTrade());
		addTradeToTradeGroupDynamic(group, detail, detail.getCallTrade());
		addTradeToTradeGroupDynamic(group, detail, detail.getPutTailTrade());
		addTradeToTradeGroupDynamic(group, detail, detail.getCallTailTrade());
	}


	@Override
	@SuppressWarnings("unchecked")
	protected void executeTradeGroupAction(TradeGroup tradeGroup) {
		TradeGroup.TradeGroupActions action = tradeGroup.getTradeGroupAction();
		if (CollectionUtils.isEmpty(tradeGroup.getTradeList())) {
			// process the action on the strangle and its tails since the parent group has no trades
			T group = (T) tradeGroup;
			executeTradeGroupDynamicStrangleTradeGroupAction(group);
		}
		else {
			if (action == TradeGroup.TradeGroupActions.SAVE_TRADE) {
				// persist group security updates
				updateTradeGroup(tradeGroup);
			}
			// process the action on the group as it has not been strangled
			super.executeTradeGroupAction(tradeGroup);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////       BaseTradeGroupDynamicOptionStrangleCalculator Methods      ////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Method to be implemented by the concrete class to instantiate new {@link BaseTradeGroupDynamicOptionStrangle}
	 */
	protected abstract T createTradeGroupDynamicOptionStrangle();


	/**
	 * Can be overridden to default properties on a newly {@link BaseTradeGroupDynamicOptionStrangle}
	 */
	protected void populateTradeGroupDynamicDefaults(T bean) {
		// Can be overridden as needed.
	}


	/**
	 * Clears the Trades from the group's detail list for repopulating them based on strangle updates.
	 */
	private void clearTradeGroupDynamicDetailListTrades(T group) {
		for (D detail : CollectionUtils.getIterable(group.getDetailList())) {
			detail.setPutTrade(null);
			detail.setCallTrade(null);
			detail.setPutTailTrade(null);
			detail.setCallTailTrade(null);
		}
	}


	/**
	 * Populates the provided trade with details from the group, detail, and security before the trade is saved.
	 */
	private Trade populateTradeDetails(T group, D detail, InvestmentSecurity security, Trade trade) {
		// Note: Buy/Sell and Quantity Intended are Already Populated from Screen
		populateIfNullOrDifferent(trade, Trade::getTradeType, Trade::setTradeType, getTradeType());
		populateIfNullOrDifferent(trade, Trade::getClientInvestmentAccount, Trade::setClientInvestmentAccount, detail.getClientAccount());
		populateIfNullOrDifferent(trade, Trade::getHoldingInvestmentAccount, Trade::setHoldingInvestmentAccount, detail.getHoldingAccount());
		populateIfNullOrDifferent(trade, Trade::getTraderUser, Trade::setTraderUser, group.getTraderUser());
		populateIfNullOrDifferent(trade, Trade::getTradeDate, Trade::setTradeDate, group.getTradeDate());
		populateIfNullOrDifferent(trade, Trade::getSettlementDate, Trade::setSettlementDate, group.getSettlementDate());
		populateIfNullOrDifferent(trade, Trade::getTradeDestination, Trade::setTradeDestination, group.getTradeDestination());
		populateIfNullOrDifferent(trade, Trade::getExecutingBrokerCompany, Trade::setExecutingBrokerCompany, group.getExecutingBrokerCompany());
		populateIfNullOrDifferent(trade, Trade::getInvestmentSecurity, Trade::setInvestmentSecurity, security);
		populateIfNullOrDifferent(trade, Trade::getPayingSecurity, Trade::setPayingSecurity, security.getInstrument().getTradingCurrency());
		// Option Roll trades will always be opening. The closing is handled by TradeToClose and Expiration.
		populateIfNullOrDifferent(trade, Trade::getOpenCloseType, Trade::setOpenCloseType,
				getTradeService().getTradeOpenCloseTypeByName(group.isLongPositions() ? TradeOpenCloseTypes.BUY_OPEN.getName() : TradeOpenCloseTypes.SELL_OPEN.getName()));
		return trade;
	}


	private <R> void populateIfNullOrDifferent(Trade trade, Function<Trade, R> valueGetter, BiConsumer<Trade, R> valueSetter, R newValue) {
		R value = valueGetter.apply(trade);
		if (value == null || !value.equals(newValue)) {
			valueSetter.accept(trade, newValue);
		}
	}


	protected TradeType getTradeType() {
		if (this.tradeType == null) {
			this.tradeType = getTradeService().getTradeTypeByName(TradeType.OPTIONS);
		}
		return this.tradeType;
	}


	/**
	 * Populates the provided TradeGroupDynamic group's detail items with the appropriate
	 * way depending on the group's strangle status.
	 */
	protected void populateTradeGroupDynamicDetailListWithTrades(T group) {
		List<Trade> rollTradeList = filterCancelledTrades(group.getTradeList());
		if (CollectionUtils.isEmpty(rollTradeList)) {
			// Look up the trades. On first load, the trades may not be populated.
			TradeSearchForm tradeSearchForm = new TradeSearchForm();
			tradeSearchForm.setTradeGroupId(group.getId());
			tradeSearchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
			rollTradeList = getTradeService().getTradeList(tradeSearchForm);
			group.setTradeList(rollTradeList);
		}

		if (CollectionUtils.isEmpty(rollTradeList)) {
			// The Option group has been strangled. Fetch the strangle group.
			TradeGroupStrangle strangleTradeGroup = getTradeGroupStrangleService().getTradeGroupStrangleByParent(group, true);
			populateTradeGroupDynamicDetailListFromTradeGroupStrangle(group, strangleTradeGroup);
		}
		else {
			// clear the detail list to remove previously previewed trades
			group.setDetailList(null);
			if (group.getStrangleCalculatorMethod() != null) {
				// Populate the detail list with a preview of the Strangled Trades.
				TradeGroupStrangle strangle = getTradeGroupStrangleService().populateTradeGroupStranglePreviewForTradeGroup(group, new TradeGroupStrangleOptionTradeValidator(), group.getStrangleCalculatorMethod());
				populateTradeGroupDynamicDetailListFromTradeGroupStrangle(group, strangle);
			}
			else {
				// Populate the generic detail list with the Option Group Trades
				populateTradeGroupDynamicDetailListFromTradeList(group, rollTradeList, false);
			}
		}
	}


	/**
	 * Populates group's detail items with the trades according to the provided strangle. The provided map of security to put/call
	 * string is used as a cache in the event the Trades security's custom column for put/call string needs to be looked up.
	 */
	private void populateTradeGroupDynamicDetailListFromTradeGroupStrangle(T group, TradeGroupStrangle strangleTradeGroup) {
		if (strangleTradeGroup != null) {
			group.setStrangleGroup(strangleTradeGroup);
			populateTradeGroupDynamicDetailListFromTradeList(group, strangleTradeGroup.getTradeList(), false);
			if (strangleTradeGroup.getCallTailGroup() != null) {
				populateTradeGroupDynamicDetailListFromTradeList(group, strangleTradeGroup.getCallTailGroup().getTradeList(), true);
			}
			if (strangleTradeGroup.getPutTailGroup() != null) {
				populateTradeGroupDynamicDetailListFromTradeList(group, strangleTradeGroup.getPutTailGroup().getTradeList(), true);
			}
		}
	}


	/**
	 * Populates group's detail items with the trades according to the provided trade list. The tail boolean signals whether the
	 * trades are part of a strangle tail group. The provided map of security to put/call string is used as a cache in the event
	 * the Trades security's custom column for put/call string needs to be looked up.
	 */
	private void populateTradeGroupDynamicDetailListFromTradeList(T group, List<Trade> tradeList, boolean tail) {
		if (group.getDetailList() == null) {
			group.setDetailList(new ArrayList<>());
		}
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if (group.getTradeDestination() == null) {
				group.setTradeDestination(trade.getTradeDestination());
				group.setExecutingBrokerCompany(trade.getExecutingBrokerCompany());
			}
			boolean found = false;
			for (D detail : CollectionUtils.getIterable(group.getDetailList())) {
				if (trade.getClientInvestmentAccount().equals(detail.getClientAccount()) && trade.getHoldingInvestmentAccount().equals(detail.getHoldingAccount())) {
					found = true;
					populateTradeGroupDynamicDetailEntryWithTrade(group, detail, trade, tail);
					break;
				}
			}
			if (!found) {
				// add a new detail for the trade
				D detail = createTradeGroupDynamicOptionStrangleDetail();
				detail.setClientAccount(trade.getClientInvestmentAccount());
				detail.setHoldingAccount(trade.getHoldingInvestmentAccount());
				populateTradeGroupDynamicDetailEntryWithTrade(group, detail, trade, tail);
				group.getDetailList().add(detail);
			}
		}
	}


	/**
	 * Returns a new instance of the concrete implementation's {@link TradeGroupDynamicOptionStrangleDetail}.
	 */
	protected abstract D createTradeGroupDynamicOptionStrangleDetail();


	/**
	 * Sets the provided trade in the correct position (put or call of the strangle or tail) of the trade group's detail item.
	 */
	private void populateTradeGroupDynamicDetailEntryWithTrade(T group, D detail, Trade trade, boolean tail) {
		// Match by the option strange group's put security or check it by custom column.
		if (trade.getInvestmentSecurity().equals(group.getPutSecurity()) || trade.getInvestmentSecurity().isPutOption()) {
			if (!tail) {
				detail.setPutTrade(trade);
			}
			else {
				detail.setPutTailTrade(trade);
			}
		}
		else {
			if (!tail) {
				detail.setCallTrade(trade);
			}
			else {
				detail.setCallTailTrade(trade);
			}
		}
	}


	/**
	 * Adds the trade to to the group depending on the trade's trade group. This method helps populate the
	 * group in a somewhat reverse engineered way by looking at the trades rather than the groups. This is
	 * necessary because we can avoid looking up the strangle groups with calls to the database because the
	 * groups are already set on the trades due to the binding process of requests.
	 */
	private void addTradeToTradeGroupDynamic(T group, D detail, Trade trade) {
		if (trade != null && !trade.isCancelled()) {
			if (group.getTradeDestination() == null) {
				group.setTradeDestination(trade.getTradeDestination());
				group.setExecutingBrokerCompany(trade.getExecutingBrokerCompany());
			}
			TradeGroup groupFromTrade = trade.getTradeGroup();
			if (groupFromTrade == null) {
				/*
				 * We are populating Trade to associated TradeGroups. If a Trade does not belong to a group,
				 * it is in a Strangle Preview. Make sure we avoid processing transient views. If we continue
				 * to process, we could lose Trade quantities when processing a Trade Group Action.
				 */
				ValidationUtils.assertTrue(group.getTradeGroupAction() == null, "You cannot execute a Trade Group Action because the current Trade Group is in an unsaved Strangle view.");
			}
			else {
				boolean applyTail = false;
				switch (groupFromTrade.getTradeGroupType().getGroupType()) {
					case OPTION_STRANGLE: {
						if (group.getStrangleGroup() == null) {
							TradeGroupStrangle strangleGroup = new TradeGroupStrangle();
							BeanUtils.copyProperties(groupFromTrade, strangleGroup);
							group.setStrangleGroup(strangleGroup);
						}
						group.getStrangleGroup().addTrade(trade);
						break;
					}
					case OPTION_TAIL: {
						if (group.getStrangleGroup() == null) {
							// the strangle may not have any trades if it was strangled and had put only, or call only, trades.
							// Populate the strangle parent.
							TradeGroupStrangle strangleGroup = new TradeGroupStrangle();
							BeanUtils.copyProperties(groupFromTrade.getParent(), strangleGroup);
							strangleGroup.setInvestmentSecurity(group.getPutSecurity());
							strangleGroup.setSecondaryInvestmentSecurity(group.getCallSecurity());
							strangleGroup.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_STRANGLE.getTypeName()));
							group.setStrangleGroup(strangleGroup);
						}
						// In the event the securities are being updated, we cannot rely on the Option Group Put/Call securities.
						// We will match to the strangle's investment security, which is the original Put security.
						if (trade.getInvestmentSecurity().equals(group.getStrangleGroup().getInvestmentSecurity())) {
							if (group.getStrangleGroup().getPutTailGroup() == null) {
								group.getStrangleGroup().setPutTailGroup(groupFromTrade);
							}
							group.getStrangleGroup().getPutTailGroup().addTrade(trade);
						}
						else {
							if (group.getStrangleGroup().getCallTailGroup() == null) {
								group.getStrangleGroup().setCallTailGroup(groupFromTrade);
							}
							group.getStrangleGroup().getCallTailGroup().addTrade(trade);
						}
						applyTail = true;
						break;
					}
					default: {
						group.addTrade(trade);
					}
				}
				populateTradeForTradeGroupAction(group, detail, trade, group.getTradeGroupAction(), applyTail);
			}
		}
	}


	/**
	 * Populates the trade according to the action being processed.
	 */
	private void populateTradeForTradeGroupAction(T group, D detail, Trade trade, TradeGroup.TradeGroupActions action, boolean applyTail) {
		switch (action) {
			case FILL:
			case FILL_EXECUTE: {
				if (trade.getInvestmentSecurity().isPutOption()) {
					if (applyTail && group.getPutTailFillPrice() != null) {
						trade.setFillPrice(group.getPutTailFillPrice());
					}
					else if (group.getPutFillPrice() != null) {
						trade.setFillPrice(group.getPutFillPrice());
					}
				}
				else {
					if (applyTail && group.getCallTailFillPrice() != null) {
						trade.setFillPrice(group.getCallTailFillPrice());
					}
					else if (group.getCallFillPrice() != null) {
						trade.setFillPrice(group.getCallFillPrice());
					}
				}

				trade.setFillQuantity(trade.getQuantityIntended());
				break;
			}
			case SAVE_TRADE: {
				// If Trade Destination is updated, apply the change to the Trades.
				TradeDestination destination = group.getTradeDestination();
				if (destination != null && !destination.isNewBean() && !destination.equals(trade.getTradeDestination())) {
					trade.setTradeDestination(destination);
				}
				// If Executing Broker is updated, apply the change to the Trades.
				BusinessCompany executingBrokerCompany = group.getExecutingBrokerCompany();
				if (executingBrokerCompany != null && !executingBrokerCompany.isNewBean() && !executingBrokerCompany.equals(trade.getExecutingBrokerCompany())) {
					trade.setExecutingBrokerCompany(group.getExecutingBrokerCompany());
				}
				if (!trade.getHoldingInvestmentAccount().equals(detail.getHoldingAccount())) {
					trade.setHoldingInvestmentAccount(detail.getHoldingAccount());
				}
				updateTradeGroupDynamicTradeSecurity(group, trade);

				if (trade.getInvestmentSecurity().isPutOption()) {
					BigDecimal commissionPerUnit = applyTail ? group.getPutTailCommissionPerUnit() : group.getPutCommissionPerUnit();
					if (commissionPerUnit != null || group.isEnableCommissionRemoval()) {
						trade.setCommissionPerUnit(commissionPerUnit);
					}
				}
				else {
					BigDecimal commissionPerUnit = applyTail ? group.getCallTailCommissionPerUnit() : group.getCallCommissionPerUnit();
					if (commissionPerUnit != null || group.isEnableCommissionRemoval()) {
						trade.setCommissionPerUnit(commissionPerUnit);
					}
				}
				// Set flag to allow the TradeObserver to update Commission Overrides
				trade.setEnableCommissionOverride(true);
				break;
			}
		}
	}


	/**
	 * In some cases, the trades' securities are changed to use a different strike price. For groups
	 * that support this functionality, this method can be overridden. It does nothing by default.
	 */
	protected void updateTradeGroupDynamicTradeSecurity(T group, Trade trade) {
		// default is to not support security updates but can be overridden to do so
	}


	/**
	 * Executes the trade group action on the provided group on its child strangle group(s).
	 */
	private void executeTradeGroupDynamicStrangleTradeGroupAction(T group) {
		TradeGroupStrangle strangle = group.getStrangleGroup();
		TradeGroup.TradeGroupActions action = group.getTradeGroupAction();
		strangle.setTradeGroupAction(action);

		// If the securities are updated on the Option Group, all four possible groups (Option Group, Strangle, Put/Call Outright)
		// need to be saved to reflect the updates. The security updates must be saved before the trade group actions can be performed.
		if (action == TradeGroup.TradeGroupActions.SAVE_TRADE && !areParentOptionGroupAndStrangleSecuritiesEqual(group, strangle)) {
			// update the securities for the Option Group and Strangle groups
			updateTradeGroup(group);
			strangle.setInvestmentSecurity(group.getPutSecurity());
			strangle.setSecondaryInvestmentSecurity(group.getCallSecurity());
			updateTradeGroup(strangle);
		}
		// Execute the group action on the strangle. The Option Group has no trades to perform the action on.
		super.executeTradeGroupAction(strangle);

		// Execute the action for the put and call outright groups; making group security updates as needed
		executeStrangleOutrightTradeGroupAction(strangle.getPutTailGroup(), action, strangle.getInvestmentSecurity());
		executeStrangleOutrightTradeGroupAction(strangle.getCallTailGroup(), action, strangle.getSecondaryInvestmentSecurity());
	}


	private boolean areParentOptionGroupAndStrangleSecuritiesEqual(T group, TradeGroupStrangle strangle) {
		return ((group.getPutSecurity() == strangle.getInvestmentSecurity())
				|| (group.getCallSecurity() == strangle.getSecondaryInvestmentSecurity()));
	}


	private void executeStrangleOutrightTradeGroupAction(TradeGroup outrightGroup, TradeGroup.TradeGroupActions action, InvestmentSecurity outrightOption) {
		if (outrightGroup != null) {
			outrightGroup.setTradeGroupAction(action);
			if (outrightGroup.getInvestmentSecurity() != outrightOption) {
				// update the outright group's securities
				outrightGroup.setInvestmentSecurity(outrightOption);
				updateTradeGroup(outrightGroup);
			}
			// execute the action of the group's trades
			super.executeTradeGroupAction(outrightGroup);
		}
	}


	/**
	 * If a Trade of the Option Group has a Workflow transition not on the Option Group Window, the Option Group's WorkflowState
	 * will become out of sync. This method will look at the Option Group's strangle and child groups and update its
	 * WorkflowState if it is behind.
	 */
	private void syncTradeGroupDynamicOptionStrangleWorkflowStateWithStrangleWorkflowState(T group) {
		TradeGroupStrangle tradeGroupStrangle = group.getStrangleGroup();
		if (tradeGroupStrangle != null) {
			// Set Option Group workflow state to the lowest state of the Option Strangle, including it's Option Tail groups.
			WorkflowState strangleState = tradeGroupStrangle.getWorkflowState();
			TradeGroup putTail = tradeGroupStrangle.getPutTailGroup();
			if (putTail != null && putTail.getWorkflowState() != null) {
				if (MathUtils.isLessThan(putTail.getWorkflowState().getOrder(), strangleState.getOrder())) {
					strangleState = putTail.getWorkflowState();
				}
			}
			TradeGroup callTail = tradeGroupStrangle.getCallTailGroup();
			if (callTail != null && callTail.getWorkflowState() != null) {
				if (MathUtils.isLessThan(callTail.getWorkflowState().getOrder(), strangleState.getOrder())) {
					strangleState = callTail.getWorkflowState();
				}
			}
			if (CollectionUtils.isEmpty(tradeGroupStrangle.getTradeList()) && !strangleState.equals(tradeGroupStrangle.getWorkflowState())) {
				// if strangle has no trades and is out of sync with tails' state update it
				updateTradeGroupDynamicOptionStrangleWorkflowState(tradeGroupStrangle, strangleState);
			}
			if (!strangleState.equals(group.getWorkflowState())) {
				updateTradeGroupDynamicOptionStrangleWorkflowState(group, strangleState);
			}
		}
	}


	// To update the WorkflowState in a read-only transaction, we need to use a new Transaction that allows writes.
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void updateTradeGroupDynamicOptionStrangleWorkflowState(TradeGroup group, WorkflowState state) {
		// set roll command values for going back to the client
		group.setWorkflowState(state);
		group.setWorkflowStatus(state.getStatus());
		updateTradeGroup(group);
	}


	/**
	 * Saves the provided TradeGroup (Option Group, Strangle, or Outright) without saving the Trade list.
	 * Use cases are for updating the group's securities or workflow.
	 */
	@SuppressWarnings("unchecked")
	private void updateTradeGroup(TradeGroup group) {
		TradeGroup groupToSave = group;
		if (group instanceof BaseTradeGroupDynamicOptionStrangle || group instanceof TradeGroupStrangle) {
			// Hibernate does not know how to save TradeGroupDynamic or Strange, so convert to TradeGroup.
			groupToSave = new TradeGroup();
			if (group instanceof BaseTradeGroupDynamicOptionStrangle) {
				setTradeGroupNoteFromDynamicProperties((T) group);
			}
			BeanUtils.copyProperties(group, groupToSave);
		}
		getTradeGroupService().saveTradeGroupWithoutTradeUpdates(groupToSave);
	}


	private List<Trade> filterCancelledTrades(List<Trade> tradeList) {
		return CollectionUtils.getStream(tradeList).filter(trade -> trade != null && !trade.isCancelled()).collect(Collectors.toList());
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////            Getter and Setters               /////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeGroupStrangleService getTradeGroupStrangleService() {
		return this.tradeGroupStrangleService;
	}


	public void setTradeGroupStrangleService(TradeGroupStrangleService tradeGroupStrangleService) {
		this.tradeGroupStrangleService = tradeGroupStrangleService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
