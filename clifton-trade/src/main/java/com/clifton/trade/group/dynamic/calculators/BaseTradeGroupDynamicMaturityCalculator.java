package com.clifton.trade.group.dynamic.calculators;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturity;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturityDetail;
import com.clifton.trade.group.dynamic.TradeGroupDynamicDetail;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * <code>BaseTradeGroupDynamicMaturityCalculator</code> is a base {@link TradeGroupDynamicCalculator} that provides
 * common functionality for rolling positions of {@link InvestmentSecurity} that mature for many clients at once.
 *
 * @author NickK
 */
public abstract class BaseTradeGroupDynamicMaturityCalculator<T extends BaseTradeGroupDynamicMaturity<D>, D extends BaseTradeGroupDynamicMaturityDetail> extends BaseTradeGroupDynamicCalculator<T, D> {

	private AccountingValuationService accountingValuationService;

	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////////
	/////////////         TradeGroupDynamicCalculator Methods         //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public T getTradeGroupDynamic(TradeGroup bean) {
		T maturityCommand = createTradeGroupDynamicMaturity();
		BeanUtils.copyProperties(bean, maturityCommand);
		populateDynamicPropertyValuesFromTradeGroupNote(maturityCommand);
		populateTradeGroupDynamicEntry(maturityCommand);
		return maturityCommand;
	}


	/**
	 * Returns a new instance of the concrete implementation's {@link com.clifton.trade.group.dynamic.TradeGroupDynamic}.
	 */
	protected abstract T createTradeGroupDynamicMaturity();


	/**
	 * Populates the provided {@link BaseTradeGroupDynamicMaturity}. If the provided command is new/unsaved, the details will
	 * contain positions for a Security of {@link BaseTradeGroupDynamicMaturity#getTradeType()} and default data for creating
	 * potential Trades. If the provided command has been previously persisted, the details will contain positions and Trade
	 * data according to the Trades of the command's Trade List.
	 */
	@Override
	public T populateTradeGroupDynamicEntry(T maturityCommand) {
		populateTradeGroupDynamicProperties(maturityCommand);
		if (maturityCommand.isNewBean()) {
			populateTradeGroupDynamicMaturityDetailListForEntry(maturityCommand);
		}
		else {
			populateTradeGroupDynamicMaturityDetailListFromTrades(maturityCommand);
		}
		BeanUtils.sortWithFunction(maturityCommand.getDetailList(), detail -> detail.getMaturingSecurity() == null ? null : detail.getMaturingSecurity().getSymbol(), true);
		return maturityCommand;
	}


	/**
	 * Hook for concrete implementations to hydrate the TradeGroupDynamic member properties.
	 */
	protected void populateTradeGroupDynamicProperties(T maturityCommand) {
		if (maturityCommand.getClientAccountGroup() != null && maturityCommand.getClientAccountGroup().getId() != null) {
			maturityCommand.setClientAccountGroup(getInvestmentAccountGroupService().getInvestmentAccountGroup(maturityCommand.getClientAccountGroup().getId()));
		}
		if (maturityCommand.getClientAccount() != null && maturityCommand.getClientAccount().getId() != null) {
			maturityCommand.setClientAccount(getInvestmentAccountService().getInvestmentAccount(maturityCommand.getClientAccount().getId()));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////        BaseTradeGroupDynamicMaturity Methods        //////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates the provided {@link BaseTradeGroupDynamicMaturity} by looking up positions for
	 * Securities relevant to the roll command's fields. The populated details will also contain
	 * defaults that can be used or modified to create Trades.
	 */
	private void populateTradeGroupDynamicMaturityDetailListForEntry(T maturityCommand) {
		AccountingBalanceSearchForm searchFormTemplate = createAccountingBalanceSearchFormTemplate(maturityCommand);
		if (maturityCommand.getClientAccountGroup() != null) {
			searchFormTemplate.setClientInvestmentAccountGroupId(maturityCommand.getClientAccountGroup().getId());
		}
		if (maturityCommand.getClientAccount() != null) {
			searchFormTemplate.setClientInvestmentAccountId(maturityCommand.getClientAccount().getId());
		}
		List<AccountingBalanceValue> positionList = getAccountingValuationService().getAccountingBalanceValueList(searchFormTemplate);

		List<D> providedDetailList = maturityCommand.getDetailList();
		maturityCommand.setDetailList(new ArrayList<>());
		Map<Object, Object> detailProcessingCache = new WeakHashMap<>();
		for (AccountingBalanceValue balance : CollectionUtils.getIterable(positionList)) {
			if (isAccountingBalanceValueApplicable(maturityCommand, balance)) {
				D detail = createTradeGroupDynamicDetailFromAccountingBalanceValue(maturityCommand, balance, detailProcessingCache);
				maturityCommand.getDetailList().add(detail);
			}
		}
		mergeDetailListForEntry(providedDetailList, maturityCommand.getDetailList());
	}


	/**
	 * When preparing a {@link BaseTradeGroupDynamicMaturity} for trade entry, there are cases where
	 * a trader will populate some data and reload the grid. Some data from the request must be merged
	 * into the response's updated detail list.
	 *
	 * @param detailListFromRequest the detail list submitted in the request
	 * @param detailListForResponse the destination detail list that will be returned with the response
	 */
	protected void mergeDetailListForEntry(List<D> detailListFromRequest, List<D> detailListForResponse) {
		for (D requestDetail : CollectionUtils.getIterable(detailListFromRequest)) {
			for (D responseDetail : detailListForResponse) {
				if (isDetailMatchForMerge(requestDetail, responseDetail)) {
					mergeDetailMatchForEntry(requestDetail, responseDetail);
				}
			}
		}
	}


	/**
	 * Tests if a detail submitted in the request is a match to a detail being returned. If they are a match,
	 * the properties from the request detail will be merged into the response detail.
	 *
	 * @param requestDetail  the detail that was provided in the request
	 * @param responseDetail the detail that will be returned in the response
	 * @return true if the details are a match
	 */
	protected boolean isDetailMatchForMerge(D requestDetail, D responseDetail) {
		return responseDetail.getClientAccount().equals(requestDetail.getClientAccount())
				&& responseDetail.getHoldingAccount().equals(requestDetail.getHoldingAccount());
	}


	/**
	 * Allows for merging data from the request detail into the response detail. This method
	 * does not do any merging by default, thus, should be overridden.
	 *
	 * @param requestDetail  the detail that was provided in the request
	 * @param responseDetail the detail that will be returned in the response
	 */
	protected void mergeDetailMatchForEntry(D requestDetail, D responseDetail) {
		// Can be overridden to perform the merge.
	}


	/**
	 * Returns a template {@link AccountingBalanceSearchForm} that can be used for finding maturing positions.
	 */
	protected AccountingBalanceSearchForm createAccountingBalanceSearchFormTemplate(T maturityCommand) {
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(maturityCommand.getTradeDate());
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_NOT_OUR_ACCOUNTS_AND_RECEIVABLE_COLLATERAL);
		populateTradeType(maturityCommand);
		searchForm.setInvestmentTypeId(maturityCommand.getTradeType().getInvestmentType().getId());
		if (maturityCommand.getClientAccountGroup() != null) {
			searchForm.setClientInvestmentAccountGroupId(maturityCommand.getClientAccountGroup().getId());
		}
		if (maturityCommand.getClientAccount() != null) {
			searchForm.setClientInvestmentAccountId(maturityCommand.getClientAccount().getId());
		}
		return searchForm;
	}


	/**
	 * Sets the {@link BaseTradeGroupDynamicMaturity}'s TradeType.
	 */
	protected abstract void populateTradeType(T maturityCommand);


	/**
	 * Returns true if the provided AccountingBalanceValue matches the request criteria of the {@link BaseTradeGroupDynamicMaturity}.
	 */
	protected boolean isAccountingBalanceValueApplicable(T maturityCommand, AccountingBalanceValue accountingBalanceValue) {
		if (MathUtils.isNullOrZero(accountingBalanceValue.getQuantity()) && !accountingBalanceValue.isPendingActivity()) {
			return false;
		}
		if (maturityCommand.getLongPositions() != null) {
			if (maturityCommand.getLongPositions()) {
				if (MathUtils.isLessThan(accountingBalanceValue.getQuantity(), BigDecimal.ZERO)) {
					return false;
				}
			}
			else {
				if (MathUtils.isGreaterThan(accountingBalanceValue.getQuantity(), BigDecimal.ZERO)) {
					return false;
				}
			}
		}
		return isSecurityBetweenMaturityRange(accountingBalanceValue.getInvestmentSecurity(), maturityCommand);
	}


	/**
	 * Returns true if the security's end date is between the maturity date ranges of the provided {@link BaseTradeGroupDynamicMaturity}
	 */
	protected boolean isSecurityBetweenMaturityRange(InvestmentSecurity security, T maturityCommand) {
		return DateUtils.isDateBetween(security.getEndDate(), maturityCommand.getStartMaturityDate(), maturityCommand.getEndMaturityDate(), false);
	}


	/**
	 * Returns a populated {@link TradeGroupDynamicDetail} for the provided {@link BaseTradeGroupDynamicMaturity} and
	 * {@link AccountingBalanceValue}.
	 * <p>
	 * The detailProcessingCache argument is a Map that can be used to store data during the processing of the detail rows to avoid
	 * excessive database access. The detailProcessingCache will be lost after processing of the detail rows has completed.
	 */
	protected abstract D createTradeGroupDynamicDetailFromAccountingBalanceValue(T maturityCommand, AccountingBalanceValue accountingBalanceValue, Map<Object, Object> detailProcessingCache);


	/**
	 * Uses the provided {@link SecuritySearchForm} to find a recent security match in the detailProcessingCache. If the detailProcessingCache does not contain
	 * a match, the search form will be used to get a list of potential securities that will be passed to the securitySelector to choose a security. If a security
	 * cannot be identified, null will be returned.
	 */
	protected InvestmentSecurity getInvestmentSecurity(SecuritySearchForm securitySearchForm, Map<Object, Object> detailProcessingCache, Function<List<InvestmentSecurity>, InvestmentSecurity> securitySelector) {
		String securitySearchFormKey = getSecuritySearchFormKey(securitySearchForm);
		InvestmentSecurity defaultNewSecurity = (InvestmentSecurity) detailProcessingCache.get(securitySearchFormKey);
		if (defaultNewSecurity == null) {
			List<InvestmentSecurity> investmentSecurityList = BeanUtils.sortWithFunction(getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm), InvestmentSecurity::getEndDate, true);
			if (!CollectionUtils.isEmpty(investmentSecurityList)) {
				defaultNewSecurity = securitySelector.apply(investmentSecurityList);
				if (defaultNewSecurity != null) {
					detailProcessingCache.put(securitySearchFormKey, defaultNewSecurity);
				}
			}
		}
		return defaultNewSecurity;
	}


	private String getSecuritySearchFormKey(SecuritySearchForm securitySearchForm) {
		return BeanUtils.createKeyFromBeans(securitySearchForm.getActiveOnDate(), securitySearchForm.getInstrumentId(), securitySearchForm.getHierarchyId(), securitySearchForm.getEndDate());
	}


	/**
	 * Populates the {@link TradeGroupDynamicDetail}s for the provided {@link BaseTradeGroupDynamicMaturity}.
	 */
	protected void populateTradeGroupDynamicMaturityDetailListFromTrades(T maturityCommand) {
		List<Trade> tradeList = CollectionUtils.getStream(maturityCommand.getTradeList())
				.filter(trade -> !trade.isCancelled())
				.collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(tradeList)) {
			clearTradeGroupDynamicDetailList(maturityCommand);
			populateTradeGroupDynamicDetailFromTrades(maturityCommand, tradeList);
		}
	}


	/**
	 * Clears the detail list of the provided TradeGroupDynamic. If the detail list is not initialized, it will be set to a new empty List.
	 */
	protected void clearTradeGroupDynamicDetailList(T maturityCommand) {
		if (maturityCommand.getDetailList() == null) {
			maturityCommand.setDetailList(new ArrayList<>());
		}
		else if (!maturityCommand.getDetailList().isEmpty()) {
			maturityCommand.getDetailList().clear();
		}
	}


	/**
	 * Populates the provided {@link BaseTradeGroupDynamicMaturity}'s {@link TradeGroupDynamicDetail} from the provided {@link Trade} List.
	 */
	protected abstract void populateTradeGroupDynamicDetailFromTrades(T maturityCommand, List<Trade> tradeList);


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}

