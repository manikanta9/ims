package com.clifton.trade.group.dynamic.calculators;

import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.dynamic.TradeGroupDynamic;
import com.clifton.trade.group.dynamic.TradeGroupDynamicDetail;


/**
 * The TradeGroupDynamicCalculator interface defines the methods for working with customized trade groups, used
 * to load the information for the user, as well as populate the real Trade Group properties for saving.
 *
 * @author manderson
 */
public interface TradeGroupDynamicCalculator<T extends TradeGroupDynamic<D>, D extends TradeGroupDynamicDetail> {


	/**
	 * Takes an existing TradeGroup that has been saved and converts it to the
	 * customized TradeGroupDynamic object
	 */
	public T getTradeGroupDynamic(TradeGroup bean);


	/**
	 * Processes a new TradeGroupDynamic object and creates a detail list
	 * for users to apply trades to.  Nothing saved.
	 */
	public T populateTradeGroupDynamicEntry(T bean);


	/**
	 * Processes the Trade Group Dynamic entry and updates the Trade Group Properties
	 * as necessary in order to save the Trades from the quantities the user entered on screen
	 */
	public T populateTradeGroupFromTradeGroupDynamicEntry(T bean);


	/**
	 * Processes the Trade Group Dynamic entry by applying the Trade Group Action passed from the
	 * client. This action is similar to {@link TradeGroupService#executeTradeGroupAction(TradeGroup)}
	 * but processes the action as necessary for the concrete TradeGroupDynamic implementation.
	 */
	public void executeTradeGroupDynamicAction(T bean);
}
