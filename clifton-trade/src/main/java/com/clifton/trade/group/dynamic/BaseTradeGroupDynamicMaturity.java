package com.clifton.trade.group.dynamic;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.TradeType;

import java.util.Date;
import java.util.List;


/**
 * <code>BaseTradeGroupDynamicMaturity</code> is a base {@link TradeGroupDynamic} that provides common
 * attributes used for finding open positions of {@link InvestmentSecurity} that mature.
 *
 * @author NickK
 */
public abstract class BaseTradeGroupDynamicMaturity<D extends BaseTradeGroupDynamicMaturityDetail> extends TradeGroupDynamic<D> {

	private InvestmentAccountGroup clientAccountGroup;
	private InvestmentAccount clientAccount;
	private TradeType tradeType;

	private Date startMaturityDate;
	private Date endMaturityDate;
	private Date newMaturityDate;

	// Condition for loading long (true), short (false), both (null) positions for other criteria.
	private Boolean longPositions;

	////////////////////////////////////////////////////////////////////////////////
	//////////               BaseTradeGroupDynamicMaturity Items            ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getSavePropertiesAsTradeGroupNote() {
		return CollectionUtils.createList("clientAccountGroup.id", "clientAccount.id", "startMaturityDate", "endMaturityDate", "newMaturityDate", "longPositions");
	}


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getClientAccountGroup() != null) {
			sb.append(getClientAccountGroup().getName()).append(" ");
		}
		if (getClientAccount() != null) {
			sb.append(getClientAccount().getName()).append(" ");
		}
		if (getLongPositions() != null) {
			sb.append(getLongPositions() ? "Long " : "Short ");
		}
		sb.append("Positions Maturing ").append(DateUtils.fromDateRange(getStartMaturityDate(), getEndMaturityDate(), true, true));

		return sb.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroup getClientAccountGroup() {
		return this.clientAccountGroup;
	}


	public void setClientAccountGroup(InvestmentAccountGroup clientAccountGroup) {
		this.clientAccountGroup = clientAccountGroup;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}


	public Date getStartMaturityDate() {
		return this.startMaturityDate;
	}


	public void setStartMaturityDate(Date startMaturityDate) {
		this.startMaturityDate = startMaturityDate;
	}


	public Date getEndMaturityDate() {
		return this.endMaturityDate;
	}


	public void setEndMaturityDate(Date endMaturityDate) {
		this.endMaturityDate = endMaturityDate;
	}


	public Date getNewMaturityDate() {
		return this.newMaturityDate;
	}


	public void setNewMaturityDate(Date newMaturityDate) {
		this.newMaturityDate = newMaturityDate;
	}


	public Boolean getLongPositions() {
		return this.longPositions;
	}


	public void setLongPositions(Boolean longPositions) {
		this.longPositions = longPositions;
	}
}
