package com.clifton.trade.group.dynamic.calculators;

import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturity;
import com.clifton.trade.group.dynamic.roll.TradeGroupDynamicBondRoll;
import com.clifton.trade.group.dynamic.roll.TradeGroupDynamicBondRollAllocation;
import com.clifton.trade.group.dynamic.roll.TradeGroupDynamicBondRollDetail;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;


/**
 * <code>TradeGroupDynamicBondRollCalculator</code> is used for Rolling maturing Bond positions. This calculator looks up Bond positions for the criteria specified in the provided
 * {@link TradeGroupDynamicBondRoll} that can be used for creating trades, reviewing trade impact, and performing TradeGroup actions.
 *
 * @author NickK
 */
public class TradeGroupDynamicBondRollCalculator extends BaseTradeGroupDynamicMaturityCalculator<TradeGroupDynamicBondRoll, TradeGroupDynamicBondRollDetail> {

	private Integer rollBondLimit;

	private InvestmentCalculatorService investmentCalculatorService;
	private InvestmentSetupService investmentSetupService;

	private TradeService tradeService;
	private TradeDestinationService tradeDestinationService;

	////////////////////////////////////////////////////////////////////////////////
	//////////      BaseTradeGroupDynamicMaturityCalculator Methods      ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected TradeGroupDynamicBondRoll createTradeGroupDynamicMaturity() {
		return new TradeGroupDynamicBondRoll();
	}


	@Override
	protected void populateTradeGroupDynamicProperties(TradeGroupDynamicBondRoll rollCommand) {
		super.populateTradeGroupDynamicProperties(rollCommand);
		if (rollCommand.getInvestmentTypeSubType() != null && rollCommand.getInvestmentTypeSubType().getId() != null) {
			rollCommand.setInvestmentTypeSubType(getInvestmentSetupService().getInvestmentTypeSubType(rollCommand.getInvestmentTypeSubType().getId()));
		}
		if (rollCommand.getInvestmentTypeSubType2() != null && rollCommand.getInvestmentTypeSubType2().getId() != null) {
			rollCommand.setInvestmentTypeSubType2(getInvestmentSetupService().getInvestmentTypeSubType2(rollCommand.getInvestmentTypeSubType2().getId()));
		}
	}


	@Override
	protected void populateTradeType(TradeGroupDynamicBondRoll rollCommand) {
		TradeType tradeType = rollCommand.getTradeType();
		if (tradeType == null || tradeType.getId() == null) {
			rollCommand.setTradeType(getTradeService().getTradeTypeByName(TradeType.BONDS));
		}
	}


	@Override
	protected AccountingBalanceSearchForm createAccountingBalanceSearchFormTemplate(TradeGroupDynamicBondRoll rollCommand) {
		List<InvestmentAccount> applicableInvestmentAccountList = getApplicableClientInvestmentAccountList(rollCommand);

		AccountingBalanceSearchForm searchForm = super.createAccountingBalanceSearchFormTemplate(rollCommand);
		searchForm.setClientInvestmentAccountIds(CollectionUtils.getStream(applicableInvestmentAccountList).map(InvestmentAccount::getId).toArray(Integer[]::new));
		searchForm.setClientInvestmentAccountGroupId(null); // clear the group since we have the applicable clients
		searchForm.setInvestmentTypeId(rollCommand.getTradeType().getInvestmentType().getId());
		searchForm.setInvestmentTypeSubTypeId(rollCommand.getInvestmentTypeSubType() == null ? null : rollCommand.getInvestmentTypeSubType().getId());
		searchForm.setInvestmentTypeSubType2Id(rollCommand.getInvestmentTypeSubType2() == null ? null : rollCommand.getInvestmentTypeSubType2().getId());
		searchForm.setCollateralAccountingAccount(rollCommand.getCollateralOnly());
		return searchForm;
	}


	@Override
	protected TradeGroupDynamicBondRollDetail createTradeGroupDynamicDetailFromAccountingBalanceValue(TradeGroupDynamicBondRoll rollCommand, AccountingBalanceValue accountingBalanceValue, Map<Object, Object> detailProcessingCache) {
		TradeGroupDynamicBondRollDetail detail = new TradeGroupDynamicBondRollDetail();
		detail.setClientAccount(accountingBalanceValue.getClientInvestmentAccount());
		detail.setHoldingAccount(accountingBalanceValue.getHoldingInvestmentAccount());
		detail.setMaturingSecurity(accountingBalanceValue.getInvestmentSecurity());
		detail.setMaturingAmount(accountingBalanceValue.getQuantity());
		detail.setMaturingCollateral(accountingBalanceValue.isCollateral());
		populateRollDefaultsForDetail(rollCommand, detail, detailProcessingCache);
		return detail;
	}


	/**
	 * Override {@link BaseTradeGroupDynamicMaturityCalculator#isDetailMatchForMerge(com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturityDetail, com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturityDetail))} in order to consider maturing security and
	 * maturing collateral position.
	 */
	@Override
	protected boolean isDetailMatchForMerge(TradeGroupDynamicBondRollDetail requestDetail, TradeGroupDynamicBondRollDetail responseDetail) {
		return super.isDetailMatchForMerge(requestDetail, responseDetail)
				&& responseDetail.getMaturingSecurity().equals(requestDetail.getMaturingSecurity())
				&& responseDetail.getMaturingCollateral().equals(requestDetail.getMaturingCollateral());
	}


	/**
	 * Override {@link BaseTradeGroupDynamicMaturityCalculator#mergeDetailMatchForEntry(com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturityDetail, com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturityDetail)} in order to apply roll amount adjustment
	 * and total values that may have been modified on the UI.
	 */
	@Override
	protected void mergeDetailMatchForEntry(TradeGroupDynamicBondRollDetail requestDetail, TradeGroupDynamicBondRollDetail responseDetail) {
		responseDetail.setRollAmountAdjustment(requestDetail.getRollAmountAdjustment());
		responseDetail.setRollAmountTotal(requestDetail.getRollAmountTotal());
	}


	/**
	 * Override {@link BaseTradeGroupDynamicMaturityCalculator#populateTradeGroupDynamicMaturityDetailListFromTrades(BaseTradeGroupDynamicMaturity)} in order to dynamically load
	 * trades from child trade groups of a parent Bond Roll as needed.
	 */
	@Override
	protected void populateTradeGroupDynamicMaturityDetailListFromTrades(TradeGroupDynamicBondRoll rollCommand) {
		List<Trade> tradeList = rollCommand.getTradeList();
		clearTradeGroupDynamicDetailList(rollCommand);
		if (CollectionUtils.isEmpty(tradeList)) {
			TradeSearchForm tradeSearchForm = new TradeSearchForm();
			tradeSearchForm.setTradeGroupParentId(rollCommand.getId());
			tradeList = getTradeService().getTradeList(tradeSearchForm);
		}
		populateTradeGroupDynamicDetailFromTrades(rollCommand, tradeList);
	}


	@Override
	protected void populateTradeGroupDynamicDetailFromTrades(TradeGroupDynamicBondRoll rollCommand, List<Trade> tradeList) {
		Map<InvestmentAccount, List<Trade>> clientTradeMap = BeanUtils.getBeansMap(tradeList, Trade::getClientInvestmentAccount);
		// set the allocation list; it will be created during trade processing below
		rollCommand.setAllocationList(new ArrayList<>());

		// For each client trade list, we need to match it to a maturing bond position
		clientTradeMap.forEach((clientAccount, clientTradeList) -> {
			Map<Boolean, List<Trade>> collateralTradeMap = BeanUtils.getBeansMap(clientTradeList, Trade::isCollateral);

			AccountingBalanceSearchForm searchFormTemplate = createAccountingBalanceSearchFormTemplate(rollCommand);
			searchFormTemplate.setClientInvestmentAccountId(clientAccount.getId());
			Map<InvestmentAccount, Map<Boolean, List<AccountingBalanceValue>>> clientHoldingCollateralPositionMap = getClientHoldingCollateralPositionMap(rollCommand, searchFormTemplate);
			// Match client trade(s) to positions based on holding account and collateral
			clientHoldingCollateralPositionMap.forEach((holdingAccount, collateralPositionMap) ->
					collateralPositionMap.forEach((collateral, positionValueList) -> {
						List<Trade> collateralTradeList = collateralTradeMap.get(collateral);
						if (!CollectionUtils.isEmpty(collateralTradeList)) {
							positionValueList.forEach(positionValue -> {
								TradeGroupDynamicBondRollDetail detail = new TradeGroupDynamicBondRollDetail();
								detail.setClientAccount(clientAccount);
								detail.setHoldingAccount(holdingAccount);
								detail.setRollTradeList(new ArrayList<>());
								detail.setMaturingAmount(positionValue.getQuantity());
								detail.setMaturingSecurity(positionValue.getInvestmentSecurity());
								detail.setMaturingCollateral(collateral);
								rollCommand.getDetailList().add(detail);

								// Calculate the roll total face by the set of trades for a client's holding account and collateral value.
								// The total is used to determine the security allocation list and corresponding weights/percentages.
								BigDecimal rollFaceTotal = BigDecimal.ZERO;
								for (Trade trade : collateralTradeList) {
									detail.setRollHoldingAccount(trade.getHoldingInvestmentAccount());
									detail.setRollExecutingBroker(trade.getExecutingBrokerCompany());
									detail.setRollTradeDestination(trade.getTradeDestination());
									detail.setRollCollateral(trade.isCollateral());
									detail.getRollTradeList().add(trade);
									rollFaceTotal = MathUtils.add(rollFaceTotal, trade.getOriginalFace());
								}
								detail.setRollAmountTotal(rollFaceTotal);
								if (rollCommand.getAllocationList().isEmpty()) {
									// Calculate and set the allocation list. We can do this from the first set of trades because
									// the same security allocation list is used to create trades for each client account.
									detail.getRollTradeList().forEach(trade -> {
										TradeGroupDynamicBondRollAllocation allocation = new TradeGroupDynamicBondRollAllocation();
										allocation.setSecurity(trade.getInvestmentSecurity());
										allocation.setAllocationPercent(CoreMathUtils.getPercentValue(trade.getOriginalFace(), detail.getRollAmountTotal(), true));
										rollCommand.getAllocationList().add(allocation);
									});
								}
							});
						}
					})
			);
		});
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////       BaseTradeGroupDynamicCalculator Methods       //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateTradeGroupForSavingNewTradeGroupDynamic(TradeGroupDynamicBondRoll rollCommand) {
		populateTradeType(rollCommand);
		ValidationUtils.assertNotEmpty(rollCommand.getAllocationList(), "Please specify at least one security allocation to roll the selected maturing positions to.");
		if (!MathUtils.isNullOrZero(getRollBondLimit())) {
			ValidationUtils.assertTrue(getRollBondLimit() == rollCommand.getAllocationList().size(), () -> "The specified list of security allocations exceeds the defined calculator limit of " + getRollBondLimit() + " securities.");
		}

		// filter out invalid allocations
		List<TradeGroupDynamicBondRollAllocation> allocationList = new ArrayList<>();
		for (TradeGroupDynamicBondRollAllocation allocation : CollectionUtils.getIterable(rollCommand.getAllocationList())) {
			if (allocation != null && allocation.getSecurity() != null) {
				ValidationUtils.assertFalse(MathUtils.isNullOrZero(allocation.getAllocationPercent()), () -> "Allocation for security [" + allocation.getSecurity() + "] must be greater than zero.");
				allocationList.add(allocation);
			}
		}
		rollCommand.setAllocationList(allocationList);
		ValidationUtils.assertNotEmpty(rollCommand.getAllocationList(), "There are no security allocations to roll the new face amount(s) to.");

		// Create temporary cache for trade group creation
		Map<String, TradeGroup> allocationTradeGroupMap = new HashMap<>();
		rollCommand.setAllocationTradeGroupMap(allocationTradeGroupMap);
		populateTradeGroupDynamicTradeListFromDetailList(rollCommand);

		if (allocationTradeGroupMap.size() == 1) {
			allocationTradeGroupMap.values().stream().findFirst().ifPresent(tradeGroup -> rollCommand.setTradeList(tradeGroup.getTradeList()));
		}
		else {
			TradeGroup rollTradeGroup = new TradeGroup();
			BeanUtils.copyProperties(rollCommand, rollTradeGroup);
			rollTradeGroup = getTradeGroupService().saveTradeGroup(rollTradeGroup);
			rollCommand.setId(rollTradeGroup.getId());
			for (TradeGroup tradeGroup : allocationTradeGroupMap.values()) {
				tradeGroup.setParent(rollTradeGroup);
				getTradeGroupService().saveTradeGroup(tradeGroup);
			}
		}
		// set temporary cache to null to avoid returning
		rollCommand.setAllocationTradeGroupMap(null);
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnEntrySave(TradeGroupDynamicBondRoll rollCommand, TradeGroupDynamicBondRollDetail detail) {
		BigDecimal rollAmountFace = detail.getRollAmountTotal();
		if (!MathUtils.isNullOrZero(rollAmountFace)) {
			int allocationCount = 0;
			for (TradeGroupDynamicBondRollAllocation allocation : CollectionUtils.getIterable(rollCommand.getAllocationList())) {
				if (allocation != null && allocation.getSecurity() != null && allocation.getAllocationPercent() != null) {
					ValidationUtils.assertFalse(isSecurityBetweenMaturityRange(allocation.getSecurity(), rollCommand), "Cannot roll to a Bond that is within the specified Maturity Range.");

					String allocationKey = BeanUtils.createKeyFromBeans(allocation, ++allocationCount);
					TradeGroup tradeGroup = CollectionUtils.getValue(rollCommand.getAllocationTradeGroupMap(), allocationKey, () -> {
						TradeGroup group = new TradeGroup();
						BeanUtils.copyProperties(rollCommand, group);
						group.setInvestmentSecurity(allocation.getSecurity());
						group.setTradeList(new ArrayList<>());
						return group;
					});

					Trade trade = new Trade();
					trade.setBuy(true);
					trade.setTradeType(rollCommand.getTradeType());
					trade.setTraderUser(rollCommand.getTraderUser());
					trade.setTradeDate(rollCommand.getTradeDate());
					trade.setSettlementDate(getSettlementDate(allocation.getSecurity(), rollCommand.getTradeDate()));
					trade.setClientInvestmentAccount(detail.getClientAccount());
					trade.setCollateralTrade(BooleanUtils.isTrue(detail.getRollCollateral()));
					trade.setInvestmentSecurity(allocation.getSecurity());
					trade.setPayingSecurity(allocation.getSecurity().getInstrument().getTradingCurrency());
					trade.setOriginalFace(calculateAllocationFaceAmount(rollAmountFace, allocation));
					trade.setHoldingInvestmentAccount(detail.getRollHoldingAccount() != null ? detail.getRollHoldingAccount() : detail.getHoldingAccount());
					trade.setTradeDestination(detail.getRollTradeDestination() != null ? detail.getRollTradeDestination() : rollCommand.getTradeDestination());
					if (!TradeDestination.BLOOMBERG_TICKET.equals(trade.getTradeDestination().getName())) {
						// If using Bloomberg Ticket, the broker will be determined.
						trade.setExecutingBrokerCompany(detail.getRollExecutingBroker() != null ? detail.getRollExecutingBroker() : detail.getHoldingAccount().getIssuingCompany());
					}
					tradeGroup.getTradeList().add(trade);
				}
			}
		}
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(TradeGroupDynamicBondRoll rollCommand, TradeGroupDynamicBondRollDetail detail) {
		if (!CollectionUtils.isEmpty(detail.getRollTradeList())) {
			rollCommand.getTradeList().addAll(detail.getRollTradeList());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////       Bond Roll TradeGroupDynamic Methods       //////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<InvestmentAccount> getApplicableClientInvestmentAccountList(TradeGroupDynamicBondRoll rollCommand) {
		InvestmentAccountSearchForm investmentAccountSearchForm = new InvestmentAccountSearchForm();
		investmentAccountSearchForm.setOurAccount(Boolean.TRUE);
		investmentAccountSearchForm.setWorkflowStatusNameEquals(WorkflowStatus.STATUS_ACTIVE);
		investmentAccountSearchForm.setRelatedPurposeActiveOnDate(rollCommand.getTradeDate());
		// Client Account must support Trading: Bonds or Trading: Bonds Collateral
		String[] tradingPurposeNames = ArrayUtils.createArray(InvestmentAccountRelationshipPurpose.TRADING_BONDS_PURPOSE_NAME, InvestmentAccountRelationshipPurpose.TRADING_BONDS_COLLATERAL_PURPOSE_NAME);
		investmentAccountSearchForm.setRelatedPurposes(tradingPurposeNames);
		if (rollCommand.getClientAccountTradingPurposeName() != null) {
			// Client Account must also allow trading for optional purpose from UI
			investmentAccountSearchForm.setRelatedPurpose(rollCommand.getClientAccountTradingPurposeName());
		}
		if (rollCommand.getClientAccount() != null) {
			investmentAccountSearchForm.setId(rollCommand.getClientAccount().getId());
		}
		else if (rollCommand.getClientAccountGroup() != null) {
			investmentAccountSearchForm.setInvestmentAccountGroupId(rollCommand.getClientAccountGroup().getId());
		}
		return getInvestmentAccountService().getInvestmentAccountList(investmentAccountSearchForm);
	}


	private void populateRollDefaultsForDetail(TradeGroupDynamicBondRoll rollCommand, TradeGroupDynamicBondRollDetail detail, Map<Object, Object> detailProcessingCache) {
		detail.setRollAmount(detail.getMaturingAmount());
		detail.setRollAmountAdjustment(detail.getRollAmountAdjustment());
		detail.setRollAmountTotal(detail.getRollAmountTotal());
		detail.setRollHoldingAccount(detail.getHoldingAccount());
		detail.setRollTradeDestination(rollCommand.getTradeDestination());
		detail.setRollCollateral(BooleanUtils.isTrue(detail.getMaturingCollateral()));

		populateRollSecurityDefaultsForDetail(rollCommand, detail, detailProcessingCache);
	}


	private void populateRollSecurityDefaultsForDetail(TradeGroupDynamicBondRoll rollCommand, TradeGroupDynamicBondRollDetail detail, Map<Object, Object> detailProcessingCache) {
		if (detail.getRollTradeList() == null) {
			detail.setRollTradeList(new ArrayList<>());
		}
		List<TradeGroupDynamicBondRollAllocation> rollSecurityAllocationList = rollCommand.getAllocationList();
		BigDecimal rollFaceAmount = ObjectUtils.coalesce(detail.getRollAmountTotal(), MathUtils.add(detail.getRollAmount(), detail.getRollAmountAdjustment()), detail.getMaturingAmount());
		if (CollectionUtils.isEmpty(rollSecurityAllocationList)) {
			InvestmentSecurity defaultNewBond = getDefaultRollSecurity(rollCommand, detail, detailProcessingCache);
			if (defaultNewBond != null) {
				Trade trade = new Trade();
				trade.setInvestmentSecurity(defaultNewBond);
				trade.setOriginalFace(rollFaceAmount);
				detail.getRollTradeList().add(trade);
			}
		}
		else {
			for (TradeGroupDynamicBondRollAllocation allocation : rollSecurityAllocationList) {
				Trade trade = new Trade();
				trade.setInvestmentSecurity(allocation.getSecurity());
				trade.setOriginalFace(calculateAllocationFaceAmount(rollFaceAmount, allocation));
				detail.getRollTradeList().add(trade);
			}
		}
	}


	private BigDecimal calculateAllocationFaceAmount(BigDecimal totalFaceAmount, TradeGroupDynamicBondRollAllocation allocation) {
		return MathUtils.getPercentageOf(allocation.getAllocationPercent(), totalFaceAmount, true);
	}


	private InvestmentSecurity getDefaultRollSecurity(TradeGroupDynamicBondRoll rollCommand, TradeGroupDynamicBondRollDetail detail, Map<Object, Object> detailProcessingCache) {
		// look up new bond
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setActiveOnDate(rollCommand.getTradeDate());
		securitySearchForm.setHierarchyId(detail.getMaturingSecurity().getInstrument().getHierarchy().getId());

		Function<List<InvestmentSecurity>, InvestmentSecurity> securitySelector = (List<InvestmentSecurity> securityList) -> {
			Predicate<InvestmentSecurity> newMaturityRangePredicate = security -> true;
			if (rollCommand.getNewMaturityDate() != null) {
				Date firstDayOfMonth = DateUtils.getFirstDayOfMonth(rollCommand.getNewMaturityDate());
				Date lastDayOfMonth = DateUtils.getLastDayOfMonth(rollCommand.getNewMaturityDate());
				newMaturityRangePredicate = security -> DateUtils.isDateBetween(security.getEndDate(), firstDayOfMonth, lastDayOfMonth, false);
			}
			// get first security maturing after specified maturing range, use null if there is not one
			Optional<InvestmentSecurity> defaultSecurity = CollectionUtils.getStream(securityList)
					.filter(security -> DateUtils.isDateAfter(security.getEndDate(), rollCommand.getEndMaturityDate()))
					.filter(newMaturityRangePredicate)
					.findFirst();
			return defaultSecurity.orElse(null);
		};
		return super.getInvestmentSecurity(securitySearchForm, detailProcessingCache, securitySelector);
	}


	private Date getSettlementDate(InvestmentSecurity security, Date tradeDate) {
		// taken from Bond trade creation window
		return getInvestmentCalculatorService().getInvestmentSecuritySettlementDate(security.getId(), null, tradeDate);
	}


	/**
	 * Fetches a list of {@link AccountingBalanceValue} positions for the provided search form. The list is then partitioned by holding account and whether or not the position is
	 * collateral.
	 */
	private Map<InvestmentAccount, Map<Boolean, List<AccountingBalanceValue>>> getClientHoldingCollateralPositionMap(TradeGroupDynamicBondRoll rollCommand, AccountingBalanceSearchForm searchFormTemplate) {
		try {
			searchFormTemplate.setInvestmentTypeId(rollCommand.getTradeType().getInvestmentType().getId());
			List<AccountingBalanceValue> valueList = BeanUtils.filter(getAccountingValuationService().getAccountingBalanceValueList(searchFormTemplate), balanceValue -> isAccountingBalanceValueApplicable(rollCommand, balanceValue));
			Map<InvestmentAccount, List<AccountingBalanceValue>> holdingAccountPositionValueMap = BeanUtils.getBeansMap(valueList, AccountingBalanceValue::getHoldingInvestmentAccount);
			Map<InvestmentAccount, Map<Boolean, List<AccountingBalanceValue>>> holdingAccountCollateralPositionValueMap = new HashMap<>();
			holdingAccountPositionValueMap.forEach((holdingAccount, positionValueList) -> holdingAccountCollateralPositionValueMap.computeIfAbsent(holdingAccount, account -> BeanUtils.getBeansMap(positionValueList, AccountingBalanceValue::isCollateral)));
			return holdingAccountCollateralPositionValueMap;
		}
		finally {
			searchFormTemplate.setRestrictionList(null);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getRollBondLimit() {
		return this.rollBondLimit;
	}


	public void setRollBondLimit(Integer rollBondLimit) {
		this.rollBondLimit = rollBondLimit;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}
}
