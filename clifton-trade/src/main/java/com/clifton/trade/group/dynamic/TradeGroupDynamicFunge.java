package com.clifton.trade.group.dynamic;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.trade.TradeType;

import java.util.List;


/**
 * <code>TradeGroupDynamicFunge</code> is a custom {@link com.clifton.trade.group.TradeGroup} for assisting in
 * closing offsetting Big and Mini {@link com.clifton.investment.instrument.InvestmentSecurity} positions often
 * created when Future Options are delivered.
 *
 * @author NickK
 */
public class TradeGroupDynamicFunge extends TradeGroupDynamic<TradeGroupDynamicFungeDetail> {

	// Position filtering criteria
	private InvestmentAccountGroup clientAccountGroup;
	private InvestmentAccount clientAccount;
	private InvestmentInstrument bigInstrument;
	private InvestmentInstrument miniInstrument;

	private TradeType tradeType;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getSavePropertiesAsTradeGroupNote() {
		return CollectionUtils.createList("clientAccountGroup.id", "clientAccount.id", "bigInstrument.id", "miniInstrument.id");
	}


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getClientAccountGroup() != null) {
			sb.append(getClientAccountGroup().getName()).append(" ");
		}
		if (getClientAccount() != null) {
			if (getClientAccountGroup() != null) {
				sb.append("and ");
			}
			sb.append(getClientAccount().getName()).append(" ");
		}
		if (getBigInstrument() != null) {
			sb.append(getBigInstrument().getName()).append(" ");
		}
		if (getMiniInstrument() != null) {
			if (getBigInstrument() != null) {
				sb.append("and ");
			}
			sb.append(getMiniInstrument().getName()).append(" ");
		}
		sb.append("Positions on ").append(DateUtils.fromDateShort(getTradeDate()));

		return sb.toString();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroup getClientAccountGroup() {
		return this.clientAccountGroup;
	}


	public void setClientAccountGroup(InvestmentAccountGroup clientAccountGroup) {
		this.clientAccountGroup = clientAccountGroup;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentInstrument getBigInstrument() {
		return this.bigInstrument;
	}


	public void setBigInstrument(InvestmentInstrument bigInstrument) {
		this.bigInstrument = bigInstrument;
	}


	public InvestmentInstrument getMiniInstrument() {
		return this.miniInstrument;
	}


	public void setMiniInstrument(InvestmentInstrument miniInstrument) {
		this.miniInstrument = miniInstrument;
	}


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}
}
