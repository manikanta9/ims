package com.clifton.trade.group.dynamic.roll;

import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturityDetail;

import java.math.BigDecimal;
import java.util.List;


/**
 * <code>TradeGroupDynamicBondRollDetail</code> contains information for a specific Client's position on a maturing Bond.
 * The information is used to help create Trades for rolling the balance to a new Bond.
 * <p>
 * The values of this entity are transient and can be manipulated in the UX to get the desired new position upon submission.
 *
 * @author NickK
 */
public class TradeGroupDynamicBondRollDetail extends BaseTradeGroupDynamicMaturityDetail {

	// Maturing position
	private Boolean maturingCollateral;

	// Items used for creating new trades
	private Boolean rollCollateral;
	private BigDecimal rollAmount;
	private BigDecimal rollAmountAdjustment;
	private BigDecimal rollAmountTotal;
	private InvestmentAccount rollHoldingAccount;
	private TradeDestination rollTradeDestination;
	private BusinessCompany rollExecutingBroker;
	// new bond trades
	private List<Trade> rollTradeList;

	////////////////////////////////////////////////////////////////////////////////
	//////////         TradeGroupDynamicBondRollDetail methods          ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder("TradeGroupDynamicBondRollDetail [clientAccount=").append(getClientAccount()).append(", holdingAccount=").append(getHoldingAccount());
		if (getMaturingSecurity() != null) {
			builder.append(", maturingBond=").append(getMaturingSecurity());
		}
		return builder.append("]").toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getMaturingCollateral() {
		return this.maturingCollateral;
	}


	public void setMaturingCollateral(Boolean maturingCollateral) {
		this.maturingCollateral = maturingCollateral;
	}


	public Boolean getRollCollateral() {
		return this.rollCollateral;
	}


	public void setRollCollateral(Boolean rollCollateral) {
		this.rollCollateral = rollCollateral;
	}


	public BigDecimal getRollAmount() {
		return this.rollAmount;
	}


	public void setRollAmount(BigDecimal rollAmount) {
		this.rollAmount = rollAmount;
	}


	public BigDecimal getRollAmountAdjustment() {
		return this.rollAmountAdjustment;
	}


	public void setRollAmountAdjustment(BigDecimal rollAmountAdjustment) {
		this.rollAmountAdjustment = rollAmountAdjustment;
	}


	public BigDecimal getRollAmountTotal() {
		return this.rollAmountTotal;
	}


	public void setRollAmountTotal(BigDecimal rollAmountTotal) {
		this.rollAmountTotal = rollAmountTotal;
	}


	public InvestmentAccount getRollHoldingAccount() {
		return this.rollHoldingAccount;
	}


	public void setRollHoldingAccount(InvestmentAccount holdingAccount) {
		this.rollHoldingAccount = holdingAccount;
	}


	public TradeDestination getRollTradeDestination() {
		return this.rollTradeDestination;
	}


	public void setRollTradeDestination(TradeDestination tradeDestination) {
		this.rollTradeDestination = tradeDestination;
	}


	public BusinessCompany getRollExecutingBroker() {
		return this.rollExecutingBroker;
	}


	public void setRollExecutingBroker(BusinessCompany executingBroker) {
		this.rollExecutingBroker = executingBroker;
	}


	public List<Trade> getRollTradeList() {
		return this.rollTradeList;
	}


	public void setRollTradeList(List<Trade> rollTradeList) {
		this.rollTradeList = rollTradeList;
	}
}
