package com.clifton.trade.group.strangle.validate;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;


/**
 * <code>TradeGroupStrangleOptionOrderTradeValidator</code> adds to the
 * <code>TradeGroupStrangleOptionTradeValidator</code> validation to ensure the
 * trade supports a trade destination for order creation.
 *
 * @author NickK
 */
public class TradeGroupStrangleOptionOrderTradeValidator extends TradeGroupStrangleOptionTradeValidator {

	@Override
	public void validate(Trade tradeToValidate) {
		ValidationUtils.assertTrue(tradeToValidate.isCreateOrder(), "Trade destination [" + tradeToValidate.getTradeDestination().getName() + "] does not support order creation.");
		super.validate(tradeToValidate);
	}
}
