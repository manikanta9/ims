package com.clifton.trade.group.dynamic.roll;

import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.group.dynamic.BaseTradeGroupDynamicMaturity;

import java.util.List;


/**
 * <code>TradeGroupDynamicForwardRoll</code> is a type of TradeGroupDynamic that is used for rolling maturing Currency Forwards.
 * The maturing Forwards' positions can be closed and rolled to new Forwards.
 * <p>
 * Customized properties are saved on the TradeGroup as the Note field as
 * propertyName:propertyValue
 * propertyName2:propertyValue2, etc.
 *
 * @author NickK
 */
public class TradeGroupDynamicForwardRoll extends BaseTradeGroupDynamicMaturity<TradeGroupDynamicForwardRollDetail> {

	// If true, new Forward position's Given currency should default to the base currency of the Forward.
	private boolean defaultRollToBaseCurrency;

	// If true, new Forward position's Holding Account and Executing Broker should default to those of the maturing position.
	private boolean defaultRollToSameBroker;

	// If true, include pending transactions in the maturing Forwards positions
	private boolean includePendingTransactions;


	private TradeExecutionType tradeExecutionType;

	////////////////////////////////////////////////////////////////////////////////
	//////////            TradeGroupDynamicForwardRoll Items            ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getSavePropertiesAsTradeGroupNote() {
		List<String> parentProperties = super.getSavePropertiesAsTradeGroupNote();
		parentProperties.addAll(CollectionUtils.createList("defaultRollToBaseCurrency", "defaultRollToSameBroker"));
		return parentProperties;
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and Setters                     ////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isDefaultRollToBaseCurrency() {
		return this.defaultRollToBaseCurrency;
	}


	public void setDefaultRollToBaseCurrency(boolean defaultRollToBaseCurrency) {
		this.defaultRollToBaseCurrency = defaultRollToBaseCurrency;
	}


	public boolean isDefaultRollToSameBroker() {
		return this.defaultRollToSameBroker;
	}


	public void setDefaultRollToSameBroker(boolean defaultRollToSameBroker) {
		this.defaultRollToSameBroker = defaultRollToSameBroker;
	}


	public boolean isIncludePendingTransactions() {
		return this.includePendingTransactions;
	}


	public void setIncludePendingTransactions(boolean includePendingTransactions) {
		this.includePendingTransactions = includePendingTransactions;
	}


	public TradeExecutionType getTradeExecutionType() {
		return this.tradeExecutionType;
	}


	public void setTradeExecutionType(TradeExecutionType tradeExecutionType) {
		this.tradeExecutionType = tradeExecutionType;
	}
}
