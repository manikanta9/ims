package com.clifton.trade.group.dynamic;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.TradeGroup;

import java.util.Date;
import java.util.List;


/**
 * TradeGroupDynamic is a custom object with a list of details used for trade groups that use
 * a dynamic calculator for trade group entry/review
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class TradeGroupDynamic<D extends TradeGroupDynamicDetail> extends TradeGroup {

	// Common Fields Used for Group Entry Only - Not Saved on the Group
	private Date settlementDate;
	private TradeDestination tradeDestination;
	private BusinessCompany executingBrokerCompany;


	// Customized List of Details - can be one or multiple trades per line
	private List<D> detailList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Overridden to define which properties should be saved to the Trade Group Note
	 * as
	 * propertyName:propertyValue
	 * propertyName2:propertyValue2
	 */
	public List<String> getSavePropertiesAsTradeGroupNote() {
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<D> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<D> detailList) {
		this.detailList = detailList;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}
}
