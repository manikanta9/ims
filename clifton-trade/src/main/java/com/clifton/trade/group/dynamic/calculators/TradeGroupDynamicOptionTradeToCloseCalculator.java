package com.clifton.trade.group.dynamic.calculators;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.group.dynamic.TradeGroupDynamicOptionStrangleDetail;
import com.clifton.trade.group.dynamic.options.TradeGroupDynamicOptionTradeToClose;


/**
 * <code>TradeGroupDynamicOptionTradeToCloseCalculator</code> defines the functionality for processing
 * a created {@link TradeGroupDynamicOptionTradeToClose} group.
 *
 * @author NickK
 */
public class TradeGroupDynamicOptionTradeToCloseCalculator extends BaseTradeGroupDynamicOptionStrangleCalculator<TradeGroupDynamicOptionTradeToClose, TradeGroupDynamicOptionStrangleDetail> {

	///////////////////////////////////////////////////////////////////////////
	//////////        BaseTradeGroupDynamicCalculator Methods      ////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public TradeGroupDynamicOptionTradeToClose populateTradeGroupDynamicEntry(TradeGroupDynamicOptionTradeToClose bean) {
		populateTradeGroupDynamicDetailListWithTrades(bean);
		Boolean longPositions = null;
		for (TradeGroupDynamicOptionStrangleDetail detail : CollectionUtils.getIterable(bean.getDetailList())) {
			// Set dynamic trade group securities
			if (bean.getPutSecurity() == null) {
				if (detail.getPutTrade() != null) {
					bean.setPutSecurity(detail.getPutTrade().getInvestmentSecurity());
				}
				else if (detail.getPutTailTrade() != null) {
					bean.setPutSecurity(detail.getPutTailTrade().getInvestmentSecurity());
				}
			}
			if (bean.getCallSecurity() == null) {
				if (detail.getCallTrade() != null) {
					bean.setCallSecurity(detail.getCallTrade().getInvestmentSecurity());
				}
				else if (detail.getCallTailTrade() != null) {
					bean.setCallSecurity(detail.getCallTailTrade().getInvestmentSecurity());
				}
			}
			// validate trade directions
			Boolean detailTradesBuy = areDetailTradesBuy(detail);
			if (detailTradesBuy != null) {
				if (longPositions == null) {
					longPositions = detailTradesBuy;
				}
				else {
					ValidationUtils.assertTrue(longPositions.equals(detailTradesBuy), "Unable to process Trade Group because the Trades of details are not all buy or all sell");
				}
			}
		}
		if (longPositions != null) {
			bean.setLongPositions(longPositions);
		}
		return bean;
	}

	///////////////////////////////////////////////////////////////////////////
	////    Base Trade Group Dynamic Option Strangle Calculator Methods    ////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected TradeGroupDynamicOptionTradeToClose createTradeGroupDynamicOptionStrangle() {
		return new TradeGroupDynamicOptionTradeToClose();
	}


	@Override
	protected TradeGroupDynamicOptionStrangleDetail createTradeGroupDynamicOptionStrangleDetail() {
		return new TradeGroupDynamicOptionStrangleDetail();
	}

	///////////////////////////////////////////////////////////////////////////
	/////////       Option Trade To Close Calculator Methods          /////////
	///////////////////////////////////////////////////////////////////////////


	private Boolean areDetailTradesBuy(TradeGroupDynamicOptionStrangleDetail detail) {
		Boolean tradesAreBuy = populateAndValidateTradeBuy(null, detail.getPutTrade());
		tradesAreBuy = populateAndValidateTradeBuy(tradesAreBuy, detail.getPutTailTrade());
		tradesAreBuy = populateAndValidateTradeBuy(tradesAreBuy, detail.getCallTrade());
		return populateAndValidateTradeBuy(tradesAreBuy, detail.getCallTailTrade());
	}


	private Boolean populateAndValidateTradeBuy(Boolean buy, Trade trade) {
		if (trade != null) {
			if (buy == null) {
				buy = trade.isBuy();
			}
			else {
				ValidationUtils.assertTrue(buy.equals(trade.isBuy()),
						() -> "Unable to process Trade Group because the Trades of detail with Client Account [" + trade.getClientInvestmentAccount()
								+ "] and Holding Account[" + trade.getHoldingInvestmentAccount() + "] are not all buy or all sell");
			}
		}
		return buy;
	}
}
