package com.clifton.trade.group.dynamic.roll;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;


/**
 * <code>TradeGroupDynamicBondRollAllocation</code> defines an allocation proportion of a total rolled face
 * amount for a new bond when rolling maturing bond positions
 *
 * @author NickK
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class TradeGroupDynamicBondRollAllocation {

	private InvestmentSecurity security;
	private BigDecimal allocationPercent;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "Allocate " + getAllocationPercent() + " to " + getSecurity();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public BigDecimal getAllocationPercent() {
		return this.allocationPercent;
	}


	public void setAllocationPercent(BigDecimal allocationPercent) {
		this.allocationPercent = allocationPercent;
	}
}
