package com.clifton.trade.group.strangle.calculator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.group.strangle.TradeGroupStrangle;
import com.clifton.trade.group.strangle.TradeGroupStrangleCalculatorContext;
import com.clifton.trade.group.strangle.validate.TradeGroupStrangleTradeValidator;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;


/**
 * <code>BaseTradeGroupOptionStrangleCalculator</code> is the based implementation of the {@code TradeGroupStrangleCalculator}
 * for options and is responsible for validating the trade list prior to performing any strangle grouping.
 *
 * @author NickK
 */
public abstract class BaseTradeGroupOptionStrangleCalculator extends BaseTradeGroupStrangleCalculator {

	protected static final String STRANGLE_TRADE_LIST_KEY = "strangle";
	protected static final String CALL_TAIL_TRADE_LIST_KEY = "call";
	protected static final String PUT_TAIL_TRADE_LIST_KEY = "put";

	////////////////////////////////////////////////////////////////////////////
	//////                    Calculator Methods                         ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected TradeGroupStrangle processTradeGroupStrangle() {
		StrangleTradeList strangleTradeList = new StrangleTradeList();
		strangleTradeList.prepareTradeList(getStrangleContext());
		return calculateTradeGroupOptionStrangle(strangleTradeList);
	}


	protected abstract TradeGroupStrangle calculateTradeGroupOptionStrangle(StrangleTradeList strangleTradeList);


	/**
	 * Groups the provided client account trade list into two groups. The first group including the trades matching the groupPut argument up to the desiredQuantity. If the
	 * desired group is being populated for a strangle, pass true in for the desiredGroupStrangle argument.
	 * <p>
	 * The response includes an array of trade lists. The first list is the desired grouping, the second is the excess trades that did not match the groupPut argument.
	 */
	@SuppressWarnings("unchecked")
	protected List<Trade>[] splitAndGroupTrades(ClientAccountTradeList clientAccountTradeList, boolean groupPut, BigDecimal desiredQuantity, boolean desiredGroupStrangle) {
		List<Trade> desiredGroup = new ArrayList<>();
		List<Trade> excessGroup = new ArrayList<>();

		BigDecimal sumLimit = BigDecimal.ZERO;
		for (ClientAccountTrade clientAccountTrade : clientAccountTradeList) {
			Trade trade = clientAccountTrade.getTrade();
			if (groupPut == clientAccountTrade.isPut()) {
				BigDecimal newSumLimit = MathUtils.add(sumLimit, trade.getQuantityIntended());
				if (!MathUtils.isGreaterThan(newSumLimit, desiredQuantity)) {
					desiredGroup.add(trade);
					sumLimit = newSumLimit;
				}
				else {
					BigDecimal limitQuantity = MathUtils.subtract(desiredQuantity, sumLimit);
					if (MathUtils.isLessThan(BigDecimal.ZERO, limitQuantity)) {
						BigDecimal excessQuantity = MathUtils.subtract(newSumLimit, desiredQuantity);
						if (MathUtils.isGreaterThan(excessQuantity, BigDecimal.ZERO)) {
							Trade newTrade;
							if (this.getStrangleContext().isSaveSplitTrade()) {
								Trade[] splitTrades = splitTrade(trade, excessQuantity);
								trade = splitTrades[0];
								newTrade = splitTrades[1];
							}
							else {
								newTrade = BeanUtils.cloneBean(trade, false, false);
								newTrade.setId(null);
								// Clear the workflow so the trade can make it's own transitions
								newTrade.setWorkflowState(null);
								newTrade.setWorkflowStatus(null);
								newTrade.setQuantityIntended(excessQuantity);
								trade.setQuantityIntended(limitQuantity);
							}
							excessGroup.add(newTrade);
						}
						desiredGroup.add(trade);
						sumLimit = limitQuantity;
					}
					else {
						excessGroup.add(trade);
						sumLimit = newSumLimit;
					}
				}
			}
			else {
				/*
				 * This method allows for grouping a strangle or tail. If the strangle is being grouped,
				 * the non-matching trades should be included in the tail (excess). If a tail is being grouped,
				 * the non-matching trades should be included in the strangle (excess).
				 */
				if (desiredGroupStrangle) {
					desiredGroup.add(trade);
				}
				else {
					excessGroup.add(trade);
				}
			}
		}
		return new List[]{desiredGroup, excessGroup};
	}


	protected TradeGroup populateTailTradeGroup(List<Trade> tradeList) {
		if (CollectionUtils.isEmpty(tradeList)) {
			return null;
		}
		Trade firstTrade = tradeList.get(0);
		TradeGroup tradeGroup = new TradeGroup();
		tradeGroup.setTradeDate(firstTrade.getTradeDate());
		tradeGroup.setTradeGroupType(getTradeGroupType(TradeGroupType.GroupTypes.OPTION_TAIL));
		tradeGroup.setTradeList(tradeList);
		tradeGroup.setTraderUser(firstTrade.getTraderUser());
		tradeGroup.setInvestmentSecurity(firstTrade.getInvestmentSecurity());
		populateTradeGroupWorkflowState(tradeGroup);
		return tradeGroup;
	}


	private void populateTradeGroupWorkflowState(TradeGroup tradeGroup) {
		TradeGroup parentTradeGroup = getStrangleContext().getTradeGroup();
		if (parentTradeGroup != null) {
			tradeGroup.setWorkflowState(parentTradeGroup.getWorkflowState());
			tradeGroup.setWorkflowStatus(parentTradeGroup.getWorkflowStatus());
		}
	}


	private TradeGroupType getTradeGroupType(TradeGroupType.GroupTypes groupType) {
		return getStrangleContext().getTradeGroupService().getTradeGroupTypeByName(groupType.getTypeName());
	}


	protected TradeGroupStrangle populateStrangleTradeGroup(List<Trade> tradeList, StrangleTradeList strangleTradeList, TradeGroup callTailTradeGroup, TradeGroup putTailTradeGroup) {
		Trade firstTrade = null;
		if (CollectionUtils.isEmpty(tradeList)) {
			if (callTailTradeGroup != null) {
				firstTrade = CollectionUtils.getFirstElement(callTailTradeGroup.getTradeList());
			}
			else if (putTailTradeGroup != null) {
				firstTrade = CollectionUtils.getFirstElement(putTailTradeGroup.getTradeList());
			}
			if (firstTrade == null) {
				throw new NoSuchElementException("A strangle cannot be created with no trades within itself or one of its tail groups.");
			}
		}
		else {
			firstTrade = tradeList.get(0);
		}

		TradeGroupStrangle tradeGroup = new TradeGroupStrangle();
		tradeGroup.setTradeDate(firstTrade.getTradeDate());
		tradeGroup.setTradeGroupType(getTradeGroupType(TradeGroupType.GroupTypes.OPTION_STRANGLE));
		tradeGroup.setTradeList(tradeList);
		tradeGroup.setTraderUser(firstTrade.getTraderUser());
		tradeGroup.setInvestmentSecurity(strangleTradeList.getPutInvestmentSecurity());
		tradeGroup.setSecondaryInvestmentSecurity(strangleTradeList.getCallInvestmentSecurity());
		tradeGroup.setCallTailGroup(callTailTradeGroup);
		tradeGroup.setPutTailGroup(putTailTradeGroup);
		populateTradeGroupWorkflowState(tradeGroup);
		return tradeGroup;
	}


	////////////////////////////////////////////////////////////////////////////
	//////                    Helper Classes                             ///////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Class that prepares a trade list for strangle grouping by mapping the trades
	 * to associated client accounts and security classification.
	 */
	protected class StrangleTradeList implements Iterable<InvestmentAccount> {

		private final Map<InvestmentAccount, ClientAccountTradeList> clientAccountToTrades = new HashMap<>();
		private final ClientAccountTradeCallPutCounter callPutCounter = new ClientAccountTradeCallPutCounter();
		private InvestmentSecurity putSecurity = null;
		private InvestmentSecurity callSecurity = null;


		void prepareTradeList(TradeGroupStrangleCalculatorContext strangleCalculatorContext) {
			TradeGroupStrangleTradeValidator tradeValidator = strangleCalculatorContext.getTradeValidator();
			for (Trade trade : CollectionUtils.getIterable(strangleCalculatorContext.getTradeList())) {
				if (trade != null && !trade.isCancelled()) {
					tradeValidator.validate(trade);
					processTradeForClientAccount(trade);
				}
			}
		}


		private void processTradeForClientAccount(Trade trade) {
			if (trade.getInvestmentSecurity().isPutOption()) {
				if (this.putSecurity == null) {
					this.putSecurity = trade.getInvestmentSecurity();
				}
			}
			else {
				if (this.callSecurity == null) {
					this.callSecurity = trade.getInvestmentSecurity();
				}
			}
			ClientAccountTrade clientInvestmentAccountTrade = this.clientAccountToTrades.computeIfAbsent(trade.getClientInvestmentAccount(), ClientAccountTradeList::new).addTrade(trade, trade.getInvestmentSecurity().isPutOption());
			this.callPutCounter.applyTrade(clientInvestmentAccountTrade);
		}


		public int getClientAccountSize() {
			return this.clientAccountToTrades.size();
		}


		@Override
		public Iterator<InvestmentAccount> iterator() {
			return this.clientAccountToTrades.keySet().iterator();
		}


		protected ClientAccountTradeList getClientAccountTradeList(InvestmentAccount clientAccount) {
			return this.clientAccountToTrades.get(clientAccount);
		}


		protected boolean isPutHeavy() {
			return this.callPutCounter.isPutHeavy();
		}


		protected BigDecimal getStrangleCallQuantity() {
			return this.callPutCounter.getCallQuantity();
		}


		protected BigDecimal getStranglePutQuantity() {
			return this.callPutCounter.getPutQuantity();
		}


		protected InvestmentSecurity getPutInvestmentSecurity() {
			return this.putSecurity;
		}


		protected InvestmentSecurity getCallInvestmentSecurity() {
			return this.callSecurity;
		}
	}

	/**
	 * Class that will track the total quantity of put and call.
	 */
	private class ClientAccountTradeCallPutCounter {

		private BigDecimal callQuantity = BigDecimal.ZERO;
		private BigDecimal putQuantity = BigDecimal.ZERO;


		void applyTrade(ClientAccountTrade clientInvestmentAccountTrade) {
			if (clientInvestmentAccountTrade.isPut()) {
				this.putQuantity = MathUtils.add(this.putQuantity, clientInvestmentAccountTrade.getTrade().getQuantityIntended());
			}
			else {
				this.callQuantity = MathUtils.add(this.callQuantity, clientInvestmentAccountTrade.getTrade().getQuantityIntended());
			}
		}


		boolean isPutHeavy() {
			return MathUtils.isLessThan(getCallQuantity(), getPutQuantity());
		}


		BigDecimal getCallQuantity() {
			return this.callQuantity;
		}


		BigDecimal getPutQuantity() {
			return this.putQuantity;
		}
	}

	/**
	 * Class containing the trades associated for a particular client account.
	 */
	protected class ClientAccountTradeList implements Iterable<ClientAccountTrade> {

		private final InvestmentAccount clientAccount;
		private final List<ClientAccountTrade> tradeList = new ArrayList<>();
		private final ClientAccountTradeCallPutCounter callPutCounter = new ClientAccountTradeCallPutCounter();


		ClientAccountTradeList(InvestmentAccount clientAccount) {
			this.clientAccount = clientAccount;
		}


		ClientAccountTrade addTrade(Trade trade, boolean isPut) {
			ClientAccountTrade accountTrade = new ClientAccountTrade(trade, isPut);
			this.tradeList.add(accountTrade);
			this.callPutCounter.applyTrade(accountTrade);
			return accountTrade;
		}


		@Override
		public Iterator<ClientAccountTrade> iterator() {
			return getTradeList().iterator();
		}


		protected List<ClientAccountTrade> getTradeList() {
			return BeanUtils.sortWithFunction(this.tradeList, clientAccountTrade -> (clientAccountTrade.getTrade() == null ? null : clientAccountTrade.getTrade().getQuantityIntended()), true);
		}


		protected boolean isPutHeavy() {
			return this.callPutCounter.isPutHeavy();
		}


		protected BigDecimal getCallQuantity() {
			return this.callPutCounter.getCallQuantity();
		}


		protected BigDecimal getPutQuantity() {
			return this.callPutCounter.getPutQuantity();
		}


		protected InvestmentAccount getClientAccount() {
			return this.clientAccount;
		}
	}

	/**
	 * Class for a trade of a client account that can be useful in strangle calculation.
	 */
	protected class ClientAccountTrade {

		private final Trade trade;
		private final boolean put;


		ClientAccountTrade(Trade trade, boolean isPut) {
			this.trade = trade;
			this.put = isPut;
		}


		protected Trade getTrade() {
			return this.trade;
		}


		protected boolean isPut() {
			return this.put;
		}
	}
}
