package com.clifton.trade.group.dynamic.calculators;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.dynamic.TradeGroupDynamicFunge;
import com.clifton.trade.group.dynamic.TradeGroupDynamicFungeDetail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;


/**
 * <code>TradeGroupDynamicFungeCalculator</code> is a calculator responsible for looking up client and holding
 * account positions applicable for funge trading. After trades are created, this calculator will use the trades
 * of a Funge {@link TradeGroup} to populate a summary for review.
 *
 * @author NickK
 */
public class TradeGroupDynamicFungeCalculator extends BaseTradeGroupDynamicCalculator<TradeGroupDynamicFunge, TradeGroupDynamicFungeDetail> {

	private AccountingValuationService accountingValuationService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentCalculatorService investmentCalculatorService;
	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataRetriever marketDataRetriever;

	private TradeDestinationService tradeDestinationService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public TradeGroupDynamicFunge getTradeGroupDynamic(TradeGroup bean) {
		TradeGroupDynamicFunge fungeGroup = new TradeGroupDynamicFunge();
		BeanUtils.copyProperties(bean, fungeGroup);
		populateDynamicPropertyValuesFromTradeGroupNote(fungeGroup);
		populateTradeGroupDynamicProperties(fungeGroup);
		populateTradeGroupDynamicEntry(fungeGroup);
		return fungeGroup;
	}


	@Override
	public TradeGroupDynamicFunge populateTradeGroupDynamicEntry(TradeGroupDynamicFunge fungeGroup) {
		fungeGroup.setDetailList(new ArrayList<>());

		AccountingBalanceSearchForm balanceSearchForm = AccountingBalanceSearchForm.onTransactionDate(fungeGroup.getTradeDate());
		balanceSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_NOT_OUR_ACCOUNTS_AND_RECEIVABLE_COLLATERAL);
		Optional.ofNullable(fungeGroup.getTradeType()).ifPresent(tradeType -> balanceSearchForm.setInvestmentTypeId(tradeType.getInvestmentType().getId()));
		Optional.ofNullable(fungeGroup.getClientAccountGroup()).ifPresent(clientAccountGroup -> balanceSearchForm.setClientInvestmentAccountGroupId(clientAccountGroup.getId()));
		Optional.ofNullable(fungeGroup.getClientAccount()).ifPresent(clientAccount -> balanceSearchForm.setClientInvestmentAccountId(clientAccount.getId()));

		if (fungeGroup.isNewBean()) {
			// Populate the details for trade entry from open positions on balance date.
			populateTradeGroupDynamicForTradeEntry(fungeGroup, balanceSearchForm);
		}
		else {
			// Populate the details from the trades associated with an existing funge trade group.
			populateTradeGroupDynamicFromTrades(fungeGroup, balanceSearchForm);
		}
		return fungeGroup;
	}


	@Override
	protected void populateTradeGroupForSavingNewTradeGroupDynamic(TradeGroupDynamicFunge fungeGroup) {
		populateTradeGroupDynamicTradeListFromDetailList(fungeGroup);
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnEntrySave(TradeGroupDynamicFunge fungeGroup, TradeGroupDynamicFungeDetail detail) {
		if (!MathUtils.isNullOrZero(detail.getBigTrade().getQuantityIntended()) && !MathUtils.isNullOrZero(detail.getMiniTrade().getQuantityIntended())) {
			fungeGroup.getTradeList().add(populateTradeForSave(fungeGroup, detail, detail.getBigTrade()));
			fungeGroup.getTradeList().add(populateTradeForSave(fungeGroup, detail, detail.getMiniTrade()));
		}
	}


	@Override
	protected void populateTradeGroupDynamicTradeListForDetailOnTradeGroupAction(TradeGroupDynamicFunge fungeGroup, TradeGroupDynamicFungeDetail detail) {
		// populate trade values for commission overrides or fill price
		if (fungeGroup.getTradeGroupAction() == TradeGroup.TradeGroupActions.SAVE_TRADE && (detail.getCommissionPerUnit() != null)) {
			populateTradeCommissionPerUnitOverrideForTradeGroupActionSave(detail.getBigTrade(), detail.getCommissionPerUnit());
			populateTradeCommissionPerUnitOverrideForTradeGroupActionSave(detail.getMiniTrade(), detail.getCommissionPerUnit());
		}
		else if ((fungeGroup.getTradeGroupAction() == TradeGroup.TradeGroupActions.FILL || fungeGroup.getTradeGroupAction() == TradeGroup.TradeGroupActions.FILL_EXECUTE)
				&& !MathUtils.isNullOrZero(detail.getFillPrice())) {
			populateTradeFillPriceForTradeGroupActionFill(detail.getBigTrade(), detail.getFillPrice());
			populateTradeFillPriceForTradeGroupActionFill(detail.getMiniTrade(), detail.getFillPrice());
		}
		// add trades to the trade group for action execution
		fungeGroup.getTradeList().add(detail.getBigTrade());
		fungeGroup.getTradeList().add(detail.getMiniTrade());
	}


	@Override
	protected void executeTradeGroupAction(TradeGroup tradeGroup) {
		if (tradeGroup.getTradeGroupAction() == TradeGroup.TradeGroupActions.SAVE_TRADE) {
			TradeGroup savableTradeGroup = new TradeGroup();
			BeanUtils.copyProperties(tradeGroup, savableTradeGroup);
			getTradeGroupService().executeTradeGroupAction(savableTradeGroup);
		}
		else {
			getTradeGroupService().executeTradeGroupAction(tradeGroup);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////               Helper Methods                        ///////////
	///////////////////////////////////////////////////////////////////////////


	private void populateTradeGroupDynamicForTradeEntry(TradeGroupDynamicFunge fungeGroup, AccountingBalanceSearchForm balanceSearchForm) {
		populateBigAndMiniInstruments(fungeGroup);
		List<AccountingBalanceValue> positionList = getAccountingValuationService().getAccountingBalanceValueList(balanceSearchForm);
		Map<String, List<AccountingBalanceValue>> clientHoldingInstrumentValueMap = BeanUtils.getBeansMap(positionList, balanceValue -> BeanUtils.createKeyFromBeans(balanceValue.getClientInvestmentAccount(), balanceValue.getHoldingInvestmentAccount()));

		// Consumer used to populate applicable big/mini security positions for funge trading
		// e.g. big security position with offsetting mini security position (long big to short mini or short big to long mini)
		BiConsumer<List<AccountingBalanceValue>, List<AccountingBalanceValue>> offsettingBigMiniInstrumentBalanceValueConsumer = (bigInstrumentBalanceValueList, miniInstrumentBalanceValueList) -> {
			// map instrument positions by security
			Map<InvestmentSecurity, List<AccountingBalanceValue>> bigSecurityBalanceValueMap = BeanUtils.getBeansMap(bigInstrumentBalanceValueList, AccountingBalanceValue::getInvestmentSecurity);
			Map<InvestmentSecurity, List<AccountingBalanceValue>> miniSecurityBalanceValueMap = BeanUtils.getBeansMap(miniInstrumentBalanceValueList, AccountingBalanceValue::getInvestmentSecurity);
			// iterate mini security positions because mini has a link to big security
			miniSecurityBalanceValueMap.forEach((miniSecurity, miniSecurityBalanceValueList) -> {
				if (miniSecurity.getBigSecurity() != null) {
					List<AccountingBalanceValue> bigSecurityBalanceValueList = bigSecurityBalanceValueMap.get(miniSecurity.getBigSecurity());
					BigDecimal bigQuantity = CoreMathUtils.sumProperty(bigSecurityBalanceValueList, AccountingBalanceValue::getQuantity);
					BigDecimal miniQuantity = CoreMathUtils.sumProperty(miniSecurityBalanceValueList, AccountingBalanceValue::getQuantity);
					// add a detail if both big and mini security positions have quantity and are in opposite position direction (long vs short or short vs long)
					if (!MathUtils.isNullOrZero(bigQuantity) && !MathUtils.isNullOrZero(miniQuantity)
							&& (MathUtils.isPositive(MathUtils.negate(bigQuantity)) == MathUtils.isPositive(miniQuantity))) {
						TradeGroupDynamicFungeDetail detail = new TradeGroupDynamicFungeDetail();
						AccountingBalanceValue firstBigBalanceValue = CollectionUtils.getFirstElement(bigSecurityBalanceValueList);
						if (firstBigBalanceValue != null) {
							detail.setClientAccount(firstBigBalanceValue.getClientInvestmentAccount());
							detail.setHoldingAccount(firstBigBalanceValue.getHoldingInvestmentAccount());

							detail.setBigQuantity(bigQuantity);
							detail.setBigTrade(new Trade());
							detail.getBigTrade().setInvestmentSecurity(miniSecurity.getBigSecurity());
							detail.getBigTrade().setTradeDestination(fungeGroup.getTradeDestination());
							detail.getBigTrade().setExecutingBrokerCompany(getDefaultExecutingBrokerCompany(fungeGroup, firstBigBalanceValue));
							detail.getBigTrade().setAverageUnitPrice(getPriceFlexible(firstBigBalanceValue.getInvestmentSecurity(), fungeGroup.getTradeDate()));
							detail.getBigTrade().setCommissionPerUnit(BigDecimal.ZERO); // Funge trades generally have a commission of 0 set on created trades, thus, default it
							detail.getBigTrade().setBuy(MathUtils.isNegative(bigQuantity));
							detail.getBigTrade().setQuantityIntended(MathUtils.abs(detail.getBigQuantity()));

							// The mini trade will have the same price, commission, destination, and executing broker obtained from the
							// big trade when the trades are created. There is no need to set them now.
							detail.setMiniQuantity(miniQuantity);
							detail.setMiniTrade(new Trade());
							detail.getMiniTrade().setInvestmentSecurity(miniSecurity);
							detail.getMiniTrade().setBuy(MathUtils.isNegative(miniQuantity));
							detail.getMiniTrade().setQuantityIntended(MathUtils.abs(MathUtils.multiply(bigQuantity, MathUtils.divide(miniSecurity.getBigSecurity().getPriceMultiplier(), miniSecurity.getPriceMultiplier()))));
							// validate calculated trade quantity to absolute value of miniQuantity because trade quantity is always positive
							ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(detail.getMiniTrade().getQuantityIntended(), MathUtils.abs(miniQuantity)), () -> "Calculated mini funge trade quantity [" + detail.getMiniQuantity() + "] is greater than the actual quantity held [" + miniQuantity + "]");

							fungeGroup.getDetailList().add(detail);
						}
					}
				}
			});
		};

		// for each list of positions for a client and holding account pair, use the above consumer to populate details for funge trading
		clientHoldingInstrumentValueMap.values().forEach(clientHoldingBalanceValueList -> {
			Map<InvestmentInstrument, List<AccountingBalanceValue>> instrumentBalanceValueMap = BeanUtils.getBeansMap(clientHoldingBalanceValueList, balanceValue -> balanceValue.getInvestmentSecurity().getInstrument());
			if (fungeGroup.getBigInstrument() == null && fungeGroup.getMiniInstrument() == null) {
				// if big and mini instruments were not provided, process all big and mini instrument positions
				// look at mini instruments because they have a reference to the big instrument
				instrumentBalanceValueMap.forEach((instrument, instrumentBalanceValueList) -> {
					if (instrument.getBigInstrument() != null &&! instrument.getBigInstrument().isInactive()) {
						offsettingBigMiniInstrumentBalanceValueConsumer.accept(instrumentBalanceValueMap.get(instrument.getBigInstrument()), instrumentBalanceValueList);
					}
				});
			}
			else {
				offsettingBigMiniInstrumentBalanceValueConsumer.accept(instrumentBalanceValueMap.get(fungeGroup.getBigInstrument()), instrumentBalanceValueMap.get(fungeGroup.getMiniInstrument()));
			}
		});
	}


	private void populateTradeGroupDynamicFromTrades(TradeGroupDynamicFunge fungeGroup, AccountingBalanceSearchForm balanceSearchForm) {
		// There should be two trades, one for a big security and the other for the related mini security, for each
		// client and holding account combination.
		Map<String, List<Trade>> clientHoldingTradeMap = BeanUtils.getBeansMap(fungeGroup.getTradeList(), trade -> BeanUtils.createKeyFromBeans(trade.getClientInvestmentAccount(), trade.getHoldingInvestmentAccount()));
		clientHoldingTradeMap.forEach((clientHoldingKey, clientHoldingTradeList) -> {
			Trade firstClientHoldingTrade = CollectionUtils.getFirstElement(clientHoldingTradeList);
			if (firstClientHoldingTrade != null) {
				balanceSearchForm.setClientInvestmentAccountId(firstClientHoldingTrade.getClientInvestmentAccount().getId());
				balanceSearchForm.setHoldingInvestmentAccountId(firstClientHoldingTrade.getHoldingInvestmentAccount().getId());
				// get positions active for the client/holding pair to populate the funge details
				List<AccountingBalanceValue> positionList = getAccountingValuationService().getAccountingBalanceValueList(balanceSearchForm);
				// map the positions by instrument to find related big and mini positions
				Map<InvestmentSecurity, List<AccountingBalanceValue>> securityPositionMap = BeanUtils.getBeansMap(positionList, AccountingBalanceValue::getInvestmentSecurity);
				// Each Security should have one trade for a client account and holding account pair
				Map<InvestmentSecurity, Trade> securityTradeMap = BeanUtils.getBeanMap(clientHoldingTradeList, Trade::getInvestmentSecurity);
				securityTradeMap.forEach((security, trade) -> {
					// look at only trades for the mini security so we can easily get the related big security trade
					if (security.getBigSecurity() != null) {
						Trade bigTrade = securityTradeMap.get(security.getBigSecurity());
						TradeGroupDynamicFungeDetail detail = new TradeGroupDynamicFungeDetail();
						detail.setClientAccount(trade.getClientInvestmentAccount());
						detail.setHoldingAccount(trade.getHoldingInvestmentAccount());
						detail.setBigQuantity(CoreMathUtils.sumProperty(securityPositionMap.get(security.getBigSecurity()), AccountingBalanceValue::getQuantity));
						detail.setMiniQuantity(CoreMathUtils.sumProperty(securityPositionMap.get(security), AccountingBalanceValue::getQuantity));
						detail.setBigTrade(bigTrade);
						detail.setMiniTrade(trade);
						fungeGroup.getDetailList().add(detail);
					}
				});
				balanceSearchForm.setRestrictionList(null); // clear previous restrictions
			}
		});
	}


	/**
	 * Hook for to hydrate the TradeGroupDynamic member properties from their ID.
	 */
	private void populateTradeGroupDynamicProperties(TradeGroupDynamicFunge fungeGroup) {
		if (fungeGroup.getClientAccountGroup() != null && fungeGroup.getClientAccountGroup().getId() != null) {
			fungeGroup.setClientAccountGroup(getInvestmentAccountGroupService().getInvestmentAccountGroup(fungeGroup.getClientAccountGroup().getId()));
		}
		if (fungeGroup.getClientAccount() != null && fungeGroup.getClientAccount().getId() != null) {
			fungeGroup.setClientAccount(getInvestmentAccountService().getInvestmentAccount(fungeGroup.getClientAccount().getId()));
		}
		if (fungeGroup.getBigInstrument() != null && fungeGroup.getBigInstrument().getId() != null) {
			fungeGroup.setBigInstrument(getInvestmentInstrumentService().getInvestmentInstrument(fungeGroup.getBigInstrument().getId()));
		}
		if (fungeGroup.getMiniInstrument() != null && fungeGroup.getMiniInstrument().getId() != null) {
			fungeGroup.setMiniInstrument(getInvestmentInstrumentService().getInvestmentInstrument(fungeGroup.getMiniInstrument().getId()));
		}
	}


	/**
	 * Populates the Big and Mini {@link InvestmentInstrument}s for details populated on the
	 * provided funge trade group. If either the Big or Mini instruments exist, the associated
	 * Mini or Big, respectively, is looked up and populated on the funge trade group. If neither
	 * the Big or Mini instruments are populated, neither will be populated.
	 */
	private void populateBigAndMiniInstruments(TradeGroupDynamicFunge fungeGroup) {
		if (fungeGroup.getBigInstrument() != null) {
			if (fungeGroup.getMiniInstrument() == null) {
				// look up mini
				InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
				instrumentSearchForm.setInactive(Boolean.FALSE);
				instrumentSearchForm.setBigInstrumentId(fungeGroup.getBigInstrument().getId());
				List<InvestmentInstrument> miniInstrumentList = getInvestmentInstrumentService().getInvestmentInstrumentList(instrumentSearchForm);
				ValidationUtils.assertTrue(CollectionUtils.getSize(miniInstrumentList) == 1, () -> "Only expected one mini Instrument for big instrument [" + fungeGroup.getBigInstrument() + "]");
				fungeGroup.setMiniInstrument(CollectionUtils.getFirstElement(miniInstrumentList));
			}
		}
		else if (fungeGroup.getMiniInstrument() != null) {
			// use big from mini
			fungeGroup.setBigInstrument(fungeGroup.getMiniInstrument().getBigInstrument());
		}
	}


	/**
	 * Returns the first applicable {@link BusinessCompany} active on the {@link TradeGroupDynamicFunge}'s
	 * trade date using criteria of an open position (e.g. holding account issuing company and trade
	 * destination and trade type from the funge group).
	 */
	private BusinessCompany getDefaultExecutingBrokerCompany(TradeGroupDynamicFunge fungeGroup, AccountingBalanceValue balanceValue) {
		TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand();
		command.setActiveOnDate(fungeGroup.getTradeDate());
		command.setTradeTypeId(fungeGroup.getTradeType().getId());
		command.setTradeDestinationId(fungeGroup.getTradeDestination().getId());
		command.setExecutingBrokerCompanyId(balanceValue.getHoldingInvestmentAccount().getIssuingCompany().getId());
		return CollectionUtils.getStream(getTradeDestinationService().getTradeDestinationMappingListByCommand(command))
				.map(TradeDestinationMapping::getExecutingBrokerCompany)
				.filter(Objects::nonNull)
				.findFirst()
				.orElse(null);
	}


	/**
	 * Returns the price for the provided {@link InvestmentSecurity}.
	 *
	 * @see MarketDataRetriever#getPriceFlexible(InvestmentSecurity, Date, boolean)
	 */
	private BigDecimal getPriceFlexible(InvestmentSecurity security, Date tradeDate) {
		return getMarketDataRetriever().getPriceFlexible(security, tradeDate, false);
	}


	/**
	 * Populates the provided {@link Trade} with values from the {@link TradeGroupDynamicFunge}
	 * and {@link TradeGroupDynamicFungeDetail} the trade is a part of for saving.
	 */
	private Trade populateTradeForSave(TradeGroupDynamicFunge fungeGroup, TradeGroupDynamicFungeDetail detail, Trade trade) {
		trade.setClientInvestmentAccount(detail.getClientAccount());
		trade.setHoldingInvestmentAccount(detail.getHoldingAccount());
		trade.setTradeType(fungeGroup.getTradeType());
		trade.setTraderUser(fungeGroup.getTraderUser());
		trade.setTradeDate(fungeGroup.getTradeDate());
		trade.setSettlementDate(getInvestmentCalculatorService().getInvestmentSecuritySettlementDate(trade.getInvestmentSecurity().getId(), null, fungeGroup.getTradeDate()));
		trade.setPayingSecurity(trade.getInvestmentSecurity().getInstrument().getTradingCurrency());
		if (trade.getTradeDestination() == null) {
			trade.setTradeDestination(ObjectUtils.coalesce(detail.getBigTrade().getTradeDestination(), fungeGroup.getTradeDestination()));
		}
		if (trade.getExecutingBrokerCompany() == null) {
			trade.setExecutingBrokerCompany(detail.getBigTrade().getExecutingBrokerCompany());
		}
		if (trade.getAverageUnitPrice() == null) {
			trade.setAverageUnitPrice(detail.getBigTrade().getAverageUnitPrice());
		}
		if (trade.getCommissionPerUnit() == null) {
			trade.setCommissionPerUnit(detail.getBigTrade().getCommissionPerUnit());
		}
		return trade;
	}


	/**
	 * Populates the commission override on the provided {@link Trade} and enables the trade to
	 * have a commission override upon save using {@link Trade#setEnableCommissionOverride}
	 */
	private void populateTradeCommissionPerUnitOverrideForTradeGroupActionSave(Trade trade, BigDecimal commissionPerUnit) {
		trade.setCommissionPerUnit(commissionPerUnit);
		trade.setEnableCommissionOverride(true);
	}


	/**
	 * Populates the fill price on the provided {@link Trade} and a fill quantity that is equal to
	 * the trade's intended quantity.
	 */
	private void populateTradeFillPriceForTradeGroupActionFill(Trade trade, BigDecimal fillPrice) {
		trade.setFillPrice(fillPrice);
		trade.setFillQuantity(trade.getQuantityIntended());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////             Getters and Setters             ///////////////
	///////////////////////////////////////////////////////////////////////////


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public TradeDestinationService getTradeDestinationService() {
		return tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}
}
