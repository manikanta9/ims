package com.clifton.trade.rule;

import com.clifton.accounting.AccountingBean;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The TradeDateInFutureRuleEvaluator class checks if {@link AccountingBean} has Transaction Date in the future (after today).
 * If so, it generates a {@link RuleViolation}.
 *
 * @author stevenf
 */
public class TradeDateInFutureRuleEvaluator extends BaseRuleEvaluator<IdentityObject, TradeRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(IdentityObject entity, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			AccountingBean accountingBean = (AccountingBean) entity;
			Integer linkedFkFieldId = (Integer) entity.getIdentity();
			if (DateUtils.isDateAfter(accountingBean.getTransactionDate(), DateUtils.clearTime(new Date()))) {
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, linkedFkFieldId));
			}
		}

		return ruleViolationList;
	}
}
