package com.clifton.trade.rule;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
public class ActiveTradesInOppositeDirectionRuleEvaluator<L extends SystemList> extends BaseRuleEvaluator<Trade, TradeRuleEvaluatorContext> implements ValidationAware {

	private Short[] tradeTypeIds;

	private Short[] investmentTypeIds;

	private Integer[] investmentAccountGroupIds;

	private Integer differentClientAccountListItemId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private InvestmentAccountGroupService investmentAccountGroupService;
	private TradeService tradeService;
	private SystemListService<L> systemListService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getInvestmentAccountGroupIds(), "At least one account group must be provided.");
		ValidationUtils.assertTrue(getInvestmentAccountGroupIds().length != 0, "At least one account group must be provided.");
	}


	@Override
	public List<RuleViolation> evaluateRule(Trade trade, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {

		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {

			if ((!ArrayUtils.isEmpty(getTradeTypeIds()) && !ArrayUtils.contains(getTradeTypeIds(), trade.getTradeType().getId()))
					|| (!ArrayUtils.isEmpty(getInvestmentTypeIds()) && !ArrayUtils.contains(getInvestmentTypeIds(), trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType()))) {
				return ruleViolationList;
			}

			List<InvestmentAccountGroup> accountGroups = Arrays.stream(getInvestmentAccountGroupIds())
					.map(id -> getInvestmentAccountGroupService().getInvestmentAccountGroup(id))
					.filter(group -> getInvestmentAccountGroupService().isInvestmentAccountInGroup(trade.getClientInvestmentAccount().getId(), group.getName()))
					.collect(Collectors.toList());

			if (!CollectionUtils.isEmpty(accountGroups)) {
				TradeSearchForm sf = new TradeSearchForm();
				sf.setSecurityId(trade.getInvestmentSecurity().getId());
				sf.setClientInvestmentAccountGroupIds(getInvestmentAccountGroupIds());
				if (getDifferentClientAccountListItemId() != null) {
					SystemListItem listItem = getSystemListService().getSystemListItem(getDifferentClientAccountListItemId());
					if ("true".equals(listItem.getValue())) {
						sf.setExcludeClientInvestmentAccountIds(new Integer[]{trade.getClientInvestmentAccount().getId()});
					}
					else {
						sf.setClientInvestmentAccountId(trade.getClientInvestmentAccount().getId());
					}
				}
				sf.setBuy(!trade.isBuy());
				sf.setTradeDate(trade.getTradeDate());
				sf.setExcludeWorkflowStatusName("Closed");

				List<Trade> failingTrades = getTradeService().getTradeList(sf);
				if (!CollectionUtils.isEmpty(failingTrades)) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("securityName", trade.getInvestmentSecurity().getLabel());

					StringBuilder tradeLabelList = new StringBuilder();
					tradeLabelList.append("\n");

					failingTrades.forEach(failingTrade -> tradeLabelList.append(failingTrade.getLabel()).append("\n"));

					templateValues.put("tradeList", tradeLabelList);

					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, trade.getId(), templateValues));
				}
			}
		}

		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short[] getTradeTypeIds() {
		return this.tradeTypeIds;
	}


	public void setTradeTypeIds(Short[] tradeTypeIds) {
		this.tradeTypeIds = tradeTypeIds;
	}


	public Short[] getInvestmentTypeIds() {
		return this.investmentTypeIds;
	}


	public void setInvestmentTypeIds(Short[] investmentTypeIds) {
		this.investmentTypeIds = investmentTypeIds;
	}


	public Integer[] getInvestmentAccountGroupIds() {
		return this.investmentAccountGroupIds;
	}


	public void setInvestmentAccountGroupIds(Integer[] investmentAccountGroupIds) {
		this.investmentAccountGroupIds = investmentAccountGroupIds;
	}


	public Integer getDifferentClientAccountListItemId() {
		return this.differentClientAccountListItemId;
	}


	public void setDifferentClientAccountListItemId(Integer differentClientAccountListItemId) {
		this.differentClientAccountListItemId = differentClientAccountListItemId;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public SystemListService<L> getSystemListService() {
		return this.systemListService;
	}


	public void setSystemListService(SystemListService<L> systemListService) {
		this.systemListService = systemListService;
	}
}
