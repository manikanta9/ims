package com.clifton.trade.rule;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * This rule checks whether the price of a Trade Fill deviates from the expected price by more than a threshold.
 * This rule works by taking the Notional Unit Price of the trade fill, and comparing it to the Expected Unit Price of the Trade Fill's Trade.  If the difference between those two
 * values is above the threshold, a rule violation will be created.
 * <p>
 * This rule can also treat the threshold as a percent.  If the percent field is set to true, the threshold/difference will be treated as a percentage of the expected unit price.
 *
 * @author MitchellF
 */
public class TradeFillPriceValidationRuleEvaluator extends BaseRuleEvaluator<IdentityObject, TradeRuleEvaluatorContext> implements ValidationAware {

	/**
	 * The price threshold - This is the maximum DIFFERENCE between expected and actual price
	 */
	private BigDecimal priceThreshold;

	/**
	 * Whether the price threshold should be treated as a percent or as an actual value.
	 */
	private boolean percent;

	private TradeService tradeService;

	/**
	 * List of trade types to be included in the rule evaluation. If left blank, all trade types will be included.  This field and excludedTradeTypeIds are mutually exclusive; a trade type cannot be in both lists. This is enforced by the class validator.
	 */
	private Short[] includedTradeTypeIds;

	/**
	 * List of trade types to be excluded from the rule evaluation. If left blank, no trade types will be excluded.
	 */
	private Short[] excludedTradeTypeIds;


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject entity, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			Trade trade = (Trade) entity;

			if (isTradeIncluded(trade)) {

				Integer linkedFkFieldId = (Integer) entity.getIdentity();

				if (trade.getExpectedUnitPrice() != null) {
					List<TradeFill> fills = getTradeService().getTradeFillListByTrade(trade.getId());
					for (TradeFill tradeFill : CollectionUtils.getIterable(fills)) {
						BigDecimal diff = MathUtils.absoluteDiff(tradeFill.getTrade().getExpectedUnitPrice(), tradeFill.getNotionalUnitPrice());

						if (isPercent()) {
							diff = CoreMathUtils.getPercentValue(diff, tradeFill.getTrade().getExpectedUnitPrice(), true);
						}

						if (MathUtils.isGreaterThan(diff, getPriceThreshold())) {
							Map<String, Object> contextValueMap = Collections.singletonMap("difference", diff);
							ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, linkedFkFieldId, contextValueMap));
							return ruleViolationList;
						}
					}
				}
			}
		}

		return ruleViolationList;
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(MathUtils.isGreaterThan(getPriceThreshold(), 0), "Price threshold cannot be less than zero.");
		if (isPercent()) {
			ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(getPriceThreshold(), 100), "Price threshold must be less than or equal to 100 if used as a percent");
		}

		if (!ArrayUtils.isEmpty(getIncludedTradeTypeIds()) && !ArrayUtils.isEmpty(getExcludedTradeTypeIds())) {
			for (Short id : getIncludedTradeTypeIds()) {
				ValidationUtils.assertFalse(ArrayUtils.contains(getExcludedTradeTypeIds(), id), "Trade type ID [" + id + "] cannot be both included and excluded from rule evaluation.");
			}
		}
	}


	private boolean isTradeIncluded(Trade trade) {
		Short id = trade.getTradeType().getId();

		if (!ArrayUtils.isEmpty(getIncludedTradeTypeIds()) && !ArrayUtils.contains(getIncludedTradeTypeIds(), id)) {
			return false;
		}

		if (!ArrayUtils.isEmpty(getExcludedTradeTypeIds()) && ArrayUtils.contains(getExcludedTradeTypeIds(), id)) {
			return false;
		}

		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPriceThreshold() {
		return this.priceThreshold;
	}


	public void setPriceThreshold(BigDecimal priceThreshold) {
		this.priceThreshold = priceThreshold;
	}


	public boolean isPercent() {
		return this.percent;
	}


	public void setPercent(boolean percent) {
		this.percent = percent;
	}


	public Short[] getIncludedTradeTypeIds() {
		return this.includedTradeTypeIds;
	}


	public void setIncludedTradeTypeIds(Short[] includedTradeTypeIds) {
		this.includedTradeTypeIds = includedTradeTypeIds;
	}


	public Short[] getExcludedTradeTypeIds() {
		return this.excludedTradeTypeIds;
	}


	public void setExcludedTradeTypeIds(Short[] excludedTradeTypeIds) {
		this.excludedTradeTypeIds = excludedTradeTypeIds;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
