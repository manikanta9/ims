package com.clifton.trade.rule;

import com.clifton.core.util.ArrayUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.rule.BaseUnapprovedEventRuleEvaluator;
import com.clifton.trade.Trade;

import java.util.Date;


/**
 * @author mitchellf
 */
public class TradeUnapprovedEventRuleEvaluator extends BaseUnapprovedEventRuleEvaluator<Trade, TradeRuleEvaluatorContext> {

	private Short[] excludedTradeGroupTypeIds;


	@Override
	protected InvestmentAccount getEvaluationObjectClientAccount(Trade trade, TradeRuleEvaluatorContext evaluatorContext) {
		return trade.getClientInvestmentAccount();
	}


	@Override
	protected Date getEntityEvaluationDate(Trade trade, TradeRuleEvaluatorContext evaluatorContext) {
		return trade.getTradeDate();
	}


	@Override
	protected boolean isObjectExcluded(Trade evaluationObject, TradeRuleEvaluatorContext evaluatorContext) {
		return evaluationObject.getTradeGroup() != null && ArrayUtils.contains(getExcludedTradeGroupTypeIds(), evaluationObject.getTradeGroup().getTradeGroupType().getId());
	}


	public Short[] getExcludedTradeGroupTypeIds() {
		return this.excludedTradeGroupTypeIds;
	}


	public void setExcludedTradeGroupTypeIds(Short[] excludedTradeGroupTypeIds) {
		this.excludedTradeGroupTypeIds = excludedTradeGroupTypeIds;
	}
}

