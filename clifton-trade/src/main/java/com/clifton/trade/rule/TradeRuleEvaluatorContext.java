package com.clifton.trade.rule;

import com.clifton.accounting.AccountingBean;
import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;
import com.clifton.trade.accounting.position.TradePositionService;

import java.math.BigDecimal;


/**
 * Used by all Trade Rule evaluation and contains common information (easily stored/retrieved from context) and re-used across various rule evaluations
 *
 * @author stevenf on 7/31/2015.
 */
public class TradeRuleEvaluatorContext extends BaseRuleEvaluatorContext {

	private TradePositionService tradePositionService;

	public static final String SHARES_BEFORE_TRADE_SETTLE = "SHARES_BEFORE_TRADE_USING_SETTLEMENT_DATE";
	public static final String SHARES_BEFORE_TRADE = "SHARES_BEFORE_TRADE_NOT_USING_SETTLEMENT_DATE";

	public static final String NOTIONAL_BEFORE_TRADE_SETTLE = "NOTIONAL_BEFORE_TRADE_USING_SETTLEMENT_DATE";
	public static final String NOTIONAL_BEFORE_TRADE = "NOTIONAL_BEFORE_TRADE_NOT_USING_SETTLEMENT_DATE";

	/////////////////////////////////////////////////////////////////////////////


	public BigDecimal getSharesBeforeTrade(AccountingBean bean, boolean useSettlementDate) {
		BigDecimal beforeTrade = (BigDecimal) getContext().getBean(useSettlementDate ? SHARES_BEFORE_TRADE_SETTLE : SHARES_BEFORE_TRADE);
		if (beforeTrade == null) {
			beforeTrade = getTradePositionService().getSharesBeforeTrade(bean, useSettlementDate);
			getContext().setBean(useSettlementDate ? SHARES_BEFORE_TRADE_SETTLE : SHARES_BEFORE_TRADE, beforeTrade);
		}
		return beforeTrade;
	}


	public BigDecimal getSharesAfterTrade(AccountingBean bean, boolean useSettlementDate) {
		BigDecimal beforeTrade = getSharesBeforeTrade(bean, useSettlementDate);
		return getTradePositionService().getSharesAfterTrade(bean, useSettlementDate, beforeTrade);
	}


	public BigDecimal getNotionalBeforeTrade(AccountingBean bean, boolean useSettlementDate) {
		BigDecimal beforeTrade = (BigDecimal) getContext().getBean(useSettlementDate ? NOTIONAL_BEFORE_TRADE_SETTLE : NOTIONAL_BEFORE_TRADE);
		if (beforeTrade == null) {
			beforeTrade = getTradePositionService().getAccountingNotionalBeforeTrade(bean, useSettlementDate);
			getContext().setBean(useSettlementDate ? NOTIONAL_BEFORE_TRADE_SETTLE : NOTIONAL_BEFORE_TRADE, beforeTrade);
		}
		return beforeTrade;
	}


	public BigDecimal getNotionalAfterTrade(AccountingBean bean, boolean useSettlementDate) {
		BigDecimal beforeTrade = getNotionalBeforeTrade(bean, useSettlementDate);
		return getTradePositionService().getAccountingNotionalAfterTrade(bean, useSettlementDate, beforeTrade);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public TradePositionService getTradePositionService() {
		return this.tradePositionService;
	}


	public void setTradePositionService(TradePositionService tradePositionService) {
		this.tradePositionService = tradePositionService;
	}
}
