package com.clifton.trade.rule;

import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;

import java.util.ArrayList;
import java.util.List;


/**
 * This rule ensures that any trade that has the Novation field set to true has a counterparty assigned.  It will generate a violation if there is a novation trade without the assignmentCompany field populated.
 *
 * @author MitchellF
 */
public class NovationTradeWithoutCounterpartyRuleEvaluator extends BaseRuleEvaluator<Trade, TradeRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(Trade trade, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			if (trade.isNovation() && trade.getAssignmentCompany() == null) {
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, trade.getId()));
			}
		}

		return ruleViolationList;
	}
}
