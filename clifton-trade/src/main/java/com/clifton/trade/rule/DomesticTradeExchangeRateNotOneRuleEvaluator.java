package com.clifton.trade.rule;

import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * @author MitchellF
 * <p>
 * This rule is to validate that no fx rate is applied to a domestic trade. If the security currency is the same as the client acct currency, the fx rate should always be 1
 */
public class DomesticTradeExchangeRateNotOneRuleEvaluator extends BaseRuleEvaluator<Trade, TradeRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(Trade trade, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			// Check currency denomination to indicate domestic trade
			if (InvestmentUtils.isSecurityCurrencyDenominationEqualToClientAccountBaseCurrency(trade) && MathUtils.isNotEqual(trade.getExchangeRateToBase(), BigDecimal.ONE)) {
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, trade.getId()));
			}
		}

		return ruleViolationList;
	}
}
