package com.clifton.trade.rule;

import com.clifton.accounting.AccountingBean;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The TradeBusinessDayRuleEvaluator class checks if {@link AccountingBean} has Transaction Date not on a business day
 * for corresponding security.  If so, it generates a {@link RuleViolation}.
 *
 * @author stevenf
 */
public class TradeBusinessDayRuleEvaluator extends BaseRuleEvaluator<IdentityObject, TradeRuleEvaluatorContext> {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject entity, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			Integer linkedFkFieldId = (Integer) entity.getIdentity();
			AccountingBean accountingBean = (AccountingBean) entity;
			if (accountingBean.getTransactionDate() != null) {
				Calendar securityCalendar = getInvestmentCalculator().getInvestmentSecurityCalendar(accountingBean.getInvestmentSecurity());
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forTrade(accountingBean.getTransactionDate(), securityCalendar.getId()))) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("calendarName", securityCalendar.getName());
					ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, linkedFkFieldId, templateValues));
				}
			}
		}

		return ruleViolationList;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
