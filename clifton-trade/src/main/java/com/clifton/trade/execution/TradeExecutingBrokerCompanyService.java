package com.clifton.trade.execution;

import com.clifton.business.company.BusinessCompany;
import com.clifton.trade.Trade;
import com.clifton.trade.execution.search.TradeExecutingBrokerSearchCommand;

import java.util.List;


/**
 * Provides a list of allowed brokers from all providers for a given trade.
 *
 * @author mwacker
 */
public interface TradeExecutingBrokerCompanyService {

	/**
	 * Will return a list of all allowed brokers.  If there are multiple broker providers, the
	 * list of allowed brokers will be the intersection of all include lists, and the excluded brokers will be the
	 * union of all lists exclude lists.  This method will then remove all excluded brokers, and filter the
	 * remaining to those that are mapped via TradeDestinationMappings.  If all providers return no included or excluded brokers,
	 * then the result of this method will be the brokers mapped via TradeDestinationMappings.
	 *
	 * @param trade
	 */
	public List<BusinessCompany> getTradeExecutingBrokerCompanyListByTrade(Trade trade);


	public List<BusinessCompany> getTradeExecutingBrokerCompanyList(TradeExecutingBrokerSearchCommand command);
}
