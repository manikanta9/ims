package com.clifton.trade.execution.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class TradeExecutionTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "name")
	private String typeName;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String typeNameEquals;

	@SearchField
	private String description;

	@SearchField
	private Short id;


	/**
	 * Search for all TradeExecutionTypes that are limit trades (when set to true)
	 */
	@SearchField
	private Boolean marketOnCloseTrade;


	/**
	 * Search for all TradeExecutionTypes that are BTIC trades (when set to true)
	 */
	@SearchField
	private Boolean bticTrade;

	@SearchField
	private Boolean clsTrade;


	@SearchField(searchField = "name", searchFieldPath = "requiredHoldingInvestmentAccountType")
	private String requiredInvestmentAccountTypeName;


	@SearchField(searchField = "requiredHoldingInvestmentAccountType.id")
	private Short requiredInvestmentAccountTypeId;


	@SearchField(searchField = "tradeTypeList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short tradeTypeId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getTypeNameEquals() {
		return this.typeNameEquals;
	}


	public void setTypeNameEquals(String typeNameEquals) {
		this.typeNameEquals = typeNameEquals;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Boolean getMarketOnCloseTrade() {
		return this.marketOnCloseTrade;
	}


	public void setMarketOnCloseTrade(Boolean marketOnCloseTrade) {
		this.marketOnCloseTrade = marketOnCloseTrade;
	}


	public Boolean getBticTrade() {
		return this.bticTrade;
	}


	public void setBticTrade(Boolean bticTrade) {
		this.bticTrade = bticTrade;
	}


	public Boolean getClsTrade() {
		return this.clsTrade;
	}


	public void setClsTrade(Boolean clsTrade) {
		this.clsTrade = clsTrade;
	}


	public String getRequiredInvestmentAccountTypeName() {
		return this.requiredInvestmentAccountTypeName;
	}


	public void setRequiredInvestmentAccountTypeName(String requiredInvestmentAccountTypeName) {
		this.requiredInvestmentAccountTypeName = requiredInvestmentAccountTypeName;
	}


	public Short getRequiredInvestmentAccountTypeId() {
		return this.requiredInvestmentAccountTypeId;
	}


	public void setRequiredInvestmentAccountTypeId(Short requiredInvestmentAccountTypeId) {
		this.requiredInvestmentAccountTypeId = requiredInvestmentAccountTypeId;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}
}
