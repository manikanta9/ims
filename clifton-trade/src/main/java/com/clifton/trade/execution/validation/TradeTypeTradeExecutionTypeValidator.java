package com.clifton.trade.execution.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.execution.TradeExecutionTypeService;
import com.clifton.trade.execution.TradeTypeTradeExecutionType;
import com.clifton.trade.execution.search.TradeTypeTradeExecutionTypeSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * A validator for the TradeTypeTradeExecutionType entity on a DAO Save event.
 *
 * @author davidi
 */
@Component
public class TradeTypeTradeExecutionTypeValidator extends SelfRegisteringDaoValidator<TradeTypeTradeExecutionType> {

	private TradeExecutionTypeService tradeExecutionTypeService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(TradeTypeTradeExecutionType bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			ValidationUtils.assertNotNull(bean.getReferenceOne(), "The Trade Type must be specified.");
			ValidationUtils.assertNotNull(bean.getReferenceTwo(), "The Trade Execution Type must be specified.");

			// A TradeTypeTradeExecutionType can have one DefaultExecutionType property set to True for a given TradeType.  If you are inserting a new entity, there should be 0 records
			// found that have DefaultExecutionType set to true.  On updates, the retrieved entity (with DefaultExecutionType set to true) must be the same as the entity being updated.
			if (bean.isDefaultExecutionType()) {
				TradeTypeTradeExecutionTypeSearchForm searchForm = new TradeTypeTradeExecutionTypeSearchForm();
				searchForm.setTradeTypeId(bean.getReferenceOne().getId());
				searchForm.setDefaultExecutionType(true);
				List<TradeTypeTradeExecutionType> tradeTypeTradeExecutionTypeList = getTradeExecutionTypeService().getTradeTypeTradeExecutionTypeList(searchForm);

				ValidationUtils.assertTrue(tradeTypeTradeExecutionTypeList.isEmpty() || (tradeTypeTradeExecutionTypeList.size() == 1 && tradeTypeTradeExecutionTypeList.get(0).getId().equals(bean.getId())),
						"Only one Trade Execution Type can be set to be the default Trade Execution Type for Trade Type: " + bean.getReferenceOne() + ".");
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeExecutionTypeService getTradeExecutionTypeService() {
		return this.tradeExecutionTypeService;
	}


	public void setTradeExecutionTypeService(TradeExecutionTypeService tradeExecutionTypeService) {
		this.tradeExecutionTypeService = tradeExecutionTypeService;
	}
}
