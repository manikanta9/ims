package com.clifton.trade.execution.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.execution.TradeTypeTradeExecutionType;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * A cache to store TradeExecutionType lists by TradeType.ID
 *
 * @author davidi
 */
@Component
public class TradeExecutionTypeListByTradeTypeCacheImpl extends SelfRegisteringSimpleDaoCache<TradeTypeTradeExecutionType, Short, List<TradeExecutionType>> implements TradeExecutionTypeListByTradeTypeCache {

	@Override
	public List<TradeExecutionType> getTradeExecutionTypeList(short tradeTypeId) {
		return getCacheHandler().get(getCacheName(), tradeTypeId);
	}


	@Override
	public void setTradeExecutionTypeList(Short tradeTypeId, List<TradeExecutionType> tradeExecutionTypeList) {
		getCacheHandler().put(getCacheName(), tradeTypeId, tradeExecutionTypeList);
	}
}
