package com.clifton.trade.execution.search;

import com.clifton.core.util.validation.ValidationException;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;

import java.util.Date;


/**
 * The <code>TradeExecutingBrokerSearchCommand</code> defines a custom search command.  This is used to search the
 * for the brokers (<code>BusinessCompany</code>) that are associated with a trade destination.
 *
 * @author davidi
 */
public class TradeExecutingBrokerSearchCommand extends TradeDestinationMappingSearchCommand {

	/**
	 * When set to True, this flag indicates that external brokers will be used to build a list of brokers.
	 */
	private boolean externalBrokerProviderListUsed;
	/**
	 * When set to true, the service will throw a {@link ValidationException} when the
	 * executing broker {@link com.clifton.business.company.BusinessCompany} list is empty.
	 */
	private boolean exceptionIfEmptyResult;

	private Short tradeExecutionTypeId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeExecutingBrokerSearchCommand() {
		super();
	}


	public TradeExecutingBrokerSearchCommand(Trade trade) {
		super(trade);
	}


	public TradeExecutingBrokerSearchCommand(Short tradeTypeId, Boolean defaultDestinationMapping, Date activeOnDate) {
		super(tradeTypeId, defaultDestinationMapping, activeOnDate);
	}


	public TradeExecutingBrokerSearchCommand(Integer securityId, Integer executingBrokerCompanyId, Date activeOnDate) {
		super(securityId, executingBrokerCompanyId, activeOnDate);
	}


	public Short getTradeExecutionTypeId() {
		return this.tradeExecutionTypeId;
	}


	public void setTradeExecutionTypeId(Short tradeExecutionTypeId) {
		this.tradeExecutionTypeId = tradeExecutionTypeId;
	}
////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isExternalBrokerProviderListUsed() {
		return this.externalBrokerProviderListUsed;
	}


	public void setExternalBrokerProviderListUsed(boolean externalBrokerProviderListUsed) {
		this.externalBrokerProviderListUsed = externalBrokerProviderListUsed;
	}


	public boolean isExceptionIfEmptyResult() {
		return this.exceptionIfEmptyResult;
	}


	public void setExceptionIfEmptyResult(boolean exceptionIfEmptyResult) {
		this.exceptionIfEmptyResult = exceptionIfEmptyResult;
	}
}
