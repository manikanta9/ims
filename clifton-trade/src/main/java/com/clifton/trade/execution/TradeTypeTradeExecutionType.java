package com.clifton.trade.execution;

import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.trade.TradeType;


/**
 * The <code>TradeTypeTradeExecutionType</code> class links TradeTypes to TradeExecutionTypes to allow us to identify what execution types are allowed
 * for specific trade types.
 *
 * @author mwacker
 */
public class TradeTypeTradeExecutionType extends ManyToManyEntity<TradeType, TradeExecutionType, Short> {

	private boolean defaultExecutionType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isDefaultExecutionType() {
		return this.defaultExecutionType;
	}


	public void setDefaultExecutionType(boolean defaultExecutionType) {
		this.defaultExecutionType = defaultExecutionType;
	}
}
