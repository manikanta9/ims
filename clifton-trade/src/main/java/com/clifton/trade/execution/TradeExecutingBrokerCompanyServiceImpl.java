package com.clifton.trade.execution;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.execution.search.TradeExecutingBrokerSearchCommand;
import com.clifton.trade.restriction.TradeRestrictionBroker;
import com.clifton.trade.restriction.TradeRestrictionBrokerService;
import com.clifton.trade.restriction.search.TradeRestrictionBrokerSearchForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Provides a list of allowed brokers from all providers for a given trade.
 *
 * @author mwacker
 */
@Service
public class TradeExecutingBrokerCompanyServiceImpl implements TradeExecutingBrokerCompanyService {

	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;
	private TradeDestinationService tradeDestinationService;
	private TradeRestrictionBrokerService tradeRestrictionBrokerService;
	private TradeExecutionTypeService tradeExecutionTypeService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private final List<TradeExecutingBrokerCompanyProvider> tradeExecutingBrokerCompanyProviderList;


	public TradeExecutingBrokerCompanyServiceImpl(@Autowired(required = false) List<TradeExecutingBrokerCompanyProvider> tradeExecutingBrokerCompanyProviderList) {
		this.tradeExecutingBrokerCompanyProviderList = ObjectUtils.coalesce(tradeExecutingBrokerCompanyProviderList, Collections.emptyList());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessCompany> getTradeExecutingBrokerCompanyListByTrade(Trade trade) {
		TradeExecutingBrokerSearchCommand command = new TradeExecutingBrokerSearchCommand(trade);
		command.setExternalBrokerProviderListUsed(true);
		command.setClientInvestmentAccountId(trade.getClientInvestmentAccount().getId());
		// exclude executing broker from the trade to get an accurate broker listing for the client account
		command.setExecutingBrokerCompanyId(null);
		// throw validation exception if the result is empty
		command.setExceptionIfEmptyResult(true);
		return getTradeExecutingBrokerCompanyList(command);
	}


	@Override
	public List<BusinessCompany> getTradeExecutingBrokerCompanyList(TradeExecutingBrokerSearchCommand command) {
		// 1. Get the list of all Executing Brokers from the TradeDestinationMappings
		Set<BusinessCompany> mappedBrokerList = getMappedTradeDestinations(command);
		// 2. Get the list of all allowed an excluded brokers from the providers
		TradeExecutingBrokerCompanyProviderResult providerResult = getAllowedExecutingBrokerList(command, mappedBrokerList);

		Set<BusinessCompany> companySet = new HashSet<>(providerResult.getIncludeExecutingBrokerCompanyList());
		// remove exclude trade destinations
		companySet.removeAll(providerResult.getExcludedExecutingBrokerCompanyList());
		ValidationUtils.assertFalse(command.isExceptionIfEmptyResult() && CollectionUtils.isEmpty(companySet), "There are no allowed executing brokers for client account [" + command.getClientInvestmentAccountId() + "].");

		if (command.getTradeExecutionTypeId() != null) {
			TradeExecutionType tradeExecutionType = getTradeExecutionTypeService().getTradeExecutionType(command.getTradeExecutionTypeId());
			// if the TradeExecutionType has a filter, then filter the Set to only contain BusinessCompany entities that meet the filter criteria.
			if (tradeExecutionType.getExecutingBrokerFilter() != null) {
				companySet = CollectionUtils.getStream(companySet)
						.filter(company -> getSystemConditionEvaluationHandler().evaluateCondition(tradeExecutionType.getExecutingBrokerFilter().getId(), company).isResult())
						.collect(Collectors.toSet());
			}
		}

		List<BusinessCompany> result = BeanUtils.sortWithFunction(new ArrayList<>(companySet), BusinessCompany::getName, true);
		if (command.isUsePagingArray()) {
			return CollectionUtils.toPagingArrayList(result, command.getStart(), command.getLimit());
		}
		else {
			return result;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Looks up executing brokers from providers in the system and broker restrictions for the provided command.
	 * <p>
	 * Returns a {@link TradeExecutingBrokerCompanyProviderResult} with an inclusion and exclusion list of brokers.
	 * The provided list of mapped brokers are included in the inclusion list of the result.
	 */
	@SuppressWarnings("unchecked")
	private TradeExecutingBrokerCompanyProviderResult getAllowedExecutingBrokerList(TradeExecutingBrokerSearchCommand command, Set<BusinessCompany> mappedBrokerList) {
		TradeExecutingBrokerCompanyProviderResult result = new TradeExecutingBrokerCompanyProviderResult();

		if (CollectionUtils.isEmpty(mappedBrokerList)) {
			return result;
		}

		InvestmentAccount clientAccount = command.getClientInvestmentAccountId() != null ? getInvestmentAccountService().getInvestmentAccount(command.getClientInvestmentAccountId()) : null;

		// Retrieve provider results
		List<TradeExecutingBrokerCompanyProviderResult> providerResultList = new ArrayList<>();
		if (command.isExternalBrokerProviderListUsed()) {
			ValidationUtils.assertNotNull(command.getClientInvestmentAccountId(), "A client investment account must be specified in order to use executing broker providers.");
			InvestmentSecurity security = command.getTrade() != null ? command.getTrade().getInvestmentSecurity() : getInvestmentInstrumentService().getInvestmentSecurity(command.getSecurityId());
			ValidationUtils.assertNotNull(security, "An investment security must be specified in order to use executing broker providers.");

			providerResultList.addAll(CollectionUtils.getConverted(this.tradeExecutingBrokerCompanyProviderList,
					provider -> provider.getAllowedBrokerCompanyList(clientAccount, security, command.getActiveOnDate())));
		}
		// Add the broker restrictions for the client account
		if (clientAccount != null) {
			providerResultList.add(getTradeRestrictionBrokerCompanySet(clientAccount));
		}

		// Process inclusion and exclusion sets from results -- filter out empty lists to allow intersecting of non-empty lists.
		@SuppressWarnings("rawtypes")
		Collection<BusinessCompany>[] includedCompanyLists = providerResultList.stream()
				.map(TradeExecutingBrokerCompanyProviderResult::getIncludeExecutingBrokerCompanyList)
				.filter(companyList -> !companyList.isEmpty()).toArray(Collection[]::new);
		// Use mapped broker list as inclusion set if no explicit inclusion sets were provided; always include mapped broker list in inclusions
		result.getIncludeExecutingBrokerCompanyList().addAll(includedCompanyLists.length > 0
				? CollectionUtils.getIntersection(ArrayUtils.add(includedCompanyLists, mappedBrokerList))
				: mappedBrokerList);

		Collection<BusinessCompany>[] excludedCompanyLists = CollectionUtils.getConverted(providerResultList,
				TradeExecutingBrokerCompanyProviderResult::getExcludedExecutingBrokerCompanyList)
				.toArray(new Collection[0]);
		result.getExcludedExecutingBrokerCompanyList().addAll(CollectionUtils.combineCollections(excludedCompanyLists));

		return result;
	}


	private Set<BusinessCompany> getMappedTradeDestinations(TradeExecutingBrokerSearchCommand command) {
		TradeExecutingBrokerSearchCommand searchCommand = new TradeExecutingBrokerSearchCommand();
		BeanUtils.copyProperties(command, searchCommand, new String[]{"start", "limit"});
		searchCommand.setExecutingBrokerIsNull(null);
		searchCommand.setExecutingBrokerIsNotNull(true);
		List<TradeDestinationMapping> mappingList = getTradeDestinationService().getTradeDestinationMappingListByCommand(searchCommand);
		return CollectionUtils.getStream(mappingList).filter(mapping -> mapping.getExecutingBrokerCompany() != null).map(TradeDestinationMapping::getExecutingBrokerCompany).collect(Collectors.toSet());
	}


	private TradeExecutingBrokerCompanyProviderResult getTradeRestrictionBrokerCompanySet(InvestmentAccount account) {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountId(account.getId());
		List<TradeRestrictionBroker> restrictionBrokerList = getTradeRestrictionBrokerService().getTradeRestrictionBrokerList(searchForm);

		TradeExecutingBrokerCompanyProviderResult result = new TradeExecutingBrokerCompanyProviderResult();

		CollectionUtils.getStream(restrictionBrokerList).forEach(
				restrictionBroker -> {
					if (restrictionBroker.isInclude()) {
						result.getIncludeExecutingBrokerCompanyList().add(restrictionBroker.getBrokerCompany());
					}
					else {
						result.getExcludedExecutingBrokerCompanyList().add(restrictionBroker.getBrokerCompany());
					}
				}
		);

		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeRestrictionBrokerService getTradeRestrictionBrokerService() {
		return this.tradeRestrictionBrokerService;
	}


	public void setTradeRestrictionBrokerService(TradeRestrictionBrokerService tradeRestrictionBrokerService) {
		this.tradeRestrictionBrokerService = tradeRestrictionBrokerService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public TradeExecutionTypeService getTradeExecutionTypeService() {
		return this.tradeExecutionTypeService;
	}


	public void setTradeExecutionTypeService(TradeExecutionTypeService tradeExecutionTypeService) {
		this.tradeExecutionTypeService = tradeExecutionTypeService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
