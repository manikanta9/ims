package com.clifton.trade.execution.cache;

import com.clifton.trade.execution.TradeExecutionType;

import java.util.List;


/**
 * A cache to store TradeExecutionType lists by TradeType.ID
 *
 * @author davidi
 */
public interface TradeExecutionTypeListByTradeTypeCache {

	public List<TradeExecutionType> getTradeExecutionTypeList(short tradeTypeId);


	public void setTradeExecutionTypeList(Short tradeTypeId, List<TradeExecutionType> tradeExecutionTypeList);
}
