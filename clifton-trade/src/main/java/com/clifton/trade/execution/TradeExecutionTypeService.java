package com.clifton.trade.execution;

import com.clifton.trade.execution.search.TradeExecutionTypeSearchForm;
import com.clifton.trade.execution.search.TradeTypeTradeExecutionTypeSearchForm;

import java.util.List;


public interface TradeExecutionTypeService {

	////////////////////////////////////////////////////////////////////////////
	//////                Trade Execution Type Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeExecutionType getTradeExecutionType(short id);


	public TradeExecutionType getTradeExecutionTypeByName(String name);


	public List<TradeExecutionType> getTradeExecutionTypeList(TradeExecutionTypeSearchForm searchForm);


	public List<TradeExecutionType> getTradeExecutionTypeListForTradeType(short tradeTypeId);


	public TradeExecutionType saveTradeExecutionType(TradeExecutionType tradeExecutionType);


	////////////////////////////////////////////////////////////////////////////
	//////             Trade Type Trade Execution Type Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeTypeTradeExecutionType getTradeTypeTradeExecutionType(short id);


	public List<TradeTypeTradeExecutionType> getTradeTypeTradeExecutionTypeList(TradeTypeTradeExecutionTypeSearchForm searchForm);


	public TradeTypeTradeExecutionType saveTradeTypeTradeExecutionType(TradeTypeTradeExecutionType tradeTypeTradeExecutionType);


	public void deleteTradeTypeTradeExecutionType(short id);
}
