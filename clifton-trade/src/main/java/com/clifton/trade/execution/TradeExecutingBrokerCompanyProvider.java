package com.clifton.trade.execution;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * Provides a list of allowed brokers for given client and/or security.
 *
 * @author mwacker
 */
public interface TradeExecutingBrokerCompanyProvider {


	public TradeExecutingBrokerCompanyProviderResult getAllowedBrokerCompanyList(InvestmentAccount clientAccount, InvestmentSecurity security, Date date);
}
