package com.clifton.trade.execution;

import com.clifton.business.company.BusinessCompany;

import java.util.HashSet;
import java.util.Set;


/**
 * Result class that will provide a list of allow and excluded brokers.  If
 * the exclude list is populated and the include list is not, then the
 * include list will be the populated with all brokers mapped via TradeDestinationMappings.
 *
 * @author mwacker
 */
public class TradeExecutingBrokerCompanyProviderResult {

	private Set<BusinessCompany> includeExecutingBrokerCompanyList = new HashSet<>();

	private Set<BusinessCompany> excludedExecutingBrokerCompanyList = new HashSet<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Set<BusinessCompany> getIncludeExecutingBrokerCompanyList() {
		return this.includeExecutingBrokerCompanyList;
	}


	public void setIncludeExecutingBrokerCompanyList(Set<BusinessCompany> includeExecutingBrokerCompanyList) {
		this.includeExecutingBrokerCompanyList = includeExecutingBrokerCompanyList;
	}


	public Set<BusinessCompany> getExcludedExecutingBrokerCompanyList() {
		return this.excludedExecutingBrokerCompanyList;
	}


	public void setExcludedExecutingBrokerCompanyList(Set<BusinessCompany> excludedExecutingBrokerCompanyList) {
		this.excludedExecutingBrokerCompanyList = excludedExecutingBrokerCompanyList;
	}
}
