package com.clifton.trade.execution.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * SearchForm for TradeTypeTradeExecutionType entity which relates TradeType to TradeExecutionType.
 */
public class TradeTypeTradeExecutionTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "referenceOne.id")
	private Short tradeTypeId;

	@SearchField(searchField = "referenceTwo.id")
	private Short tradeExecutionTypeId;

	@SearchField(searchField = "referenceOne.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] tradeTypeIds;

	@SearchField(searchField = "referenceTwo.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] tradeExecutionTypeIds;

	@SearchField
	private Boolean defaultExecutionType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public Short getTradeExecutionTypeId() {
		return this.tradeExecutionTypeId;
	}


	public void setTradeExecutionTypeId(Short tradeExecutionTypeId) {
		this.tradeExecutionTypeId = tradeExecutionTypeId;
	}


	public Short[] getTradeTypeIds() {
		return this.tradeTypeIds;
	}


	public void setTradeTypeIds(Short[] tradeTypeIds) {
		this.tradeTypeIds = tradeTypeIds;
	}


	public Short[] getTradeExecutionTypeIds() {
		return this.tradeExecutionTypeIds;
	}


	public void setTradeExecutionTypeIds(Short[] tradeExecutionTypeIds) {
		this.tradeExecutionTypeIds = tradeExecutionTypeIds;
	}


	public Boolean getDefaultExecutionType() {
		return this.defaultExecutionType;
	}


	public void setDefaultExecutionType(Boolean defaultExecutionType) {
		this.defaultExecutionType = defaultExecutionType;
	}
}
