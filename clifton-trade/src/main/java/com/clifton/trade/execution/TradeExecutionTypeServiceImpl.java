package com.clifton.trade.execution;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.trade.execution.cache.TradeExecutionTypeListByTradeTypeCache;
import com.clifton.trade.execution.search.TradeExecutionTypeSearchForm;
import com.clifton.trade.execution.search.TradeTypeTradeExecutionTypeSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TradeExecutionTypeServiceImpl implements TradeExecutionTypeService {

	private AdvancedUpdatableDAO<TradeExecutionType, Criteria> tradeExecutionTypeDAO;

	private AdvancedUpdatableDAO<TradeTypeTradeExecutionType, Criteria> tradeTypeTradeExecutionTypeDAO;

	private DaoNamedEntityCache<TradeExecutionType> tradeExecutionTypeCache;

	// Caches list of TradeExecutionTypes by TradeType.ID value
	private TradeExecutionTypeListByTradeTypeCache tradeExecutionTypeListByTradeTypeCache;


	////////////////////////////////////////////////////////////////////////////
	//////                Trade Execution Type Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeExecutionType getTradeExecutionType(short id) {
		return getTradeExecutionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeExecutionType getTradeExecutionTypeByName(String name) {
		return getTradeExecutionTypeCache().getBeanForKeyValue(getTradeExecutionTypeDAO(), name);
	}


	@Override
	public List<TradeExecutionType> getTradeExecutionTypeList(TradeExecutionTypeSearchForm searchForm) {
		return getTradeExecutionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<TradeExecutionType> getTradeExecutionTypeListForTradeType(short tradeTypeId) {
		List<TradeExecutionType> tradeExecutionTypeList = getTradeExecutionTypeListByTradeTypeCache().getTradeExecutionTypeList(tradeTypeId);
		if (tradeExecutionTypeList == null) {
			TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
			searchForm.setTradeTypeId(tradeTypeId);
			tradeExecutionTypeList = getTradeExecutionTypeList(searchForm);
			getTradeExecutionTypeListByTradeTypeCache().setTradeExecutionTypeList(tradeTypeId, tradeExecutionTypeList);
		}
		return tradeExecutionTypeList;
	}


	@Override
	public TradeExecutionType saveTradeExecutionType(TradeExecutionType tradeExecutionType) {
		return getTradeExecutionTypeDAO().save(tradeExecutionType);
	}


	////////////////////////////////////////////////////////////////////////////
	//////                Trade Type Trade Execution Type Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeTypeTradeExecutionType getTradeTypeTradeExecutionType(short id) {
		return getTradeTypeTradeExecutionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeTypeTradeExecutionType> getTradeTypeTradeExecutionTypeList(TradeTypeTradeExecutionTypeSearchForm searchForm) {
		return getTradeTypeTradeExecutionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeTypeTradeExecutionType saveTradeTypeTradeExecutionType(TradeTypeTradeExecutionType tradeTypeTradeExecutionType) {
		return getTradeTypeTradeExecutionTypeDAO().save(tradeTypeTradeExecutionType);
	}


	@Override
	public void deleteTradeTypeTradeExecutionType(short id) {
		getTradeTypeTradeExecutionTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeExecutionType, Criteria> getTradeExecutionTypeDAO() {
		return this.tradeExecutionTypeDAO;
	}


	public void setTradeExecutionTypeDAO(AdvancedUpdatableDAO<TradeExecutionType, Criteria> tradeExecutionTypeDAO) {
		this.tradeExecutionTypeDAO = tradeExecutionTypeDAO;
	}


	public DaoNamedEntityCache<TradeExecutionType> getTradeExecutionTypeCache() {
		return this.tradeExecutionTypeCache;
	}


	public void setTradeExecutionTypeCache(DaoNamedEntityCache<TradeExecutionType> tradeExecutionTypeCache) {
		this.tradeExecutionTypeCache = tradeExecutionTypeCache;
	}


	public AdvancedUpdatableDAO<TradeTypeTradeExecutionType, Criteria> getTradeTypeTradeExecutionTypeDAO() {
		return this.tradeTypeTradeExecutionTypeDAO;
	}


	public void setTradeTypeTradeExecutionTypeDAO(AdvancedUpdatableDAO<TradeTypeTradeExecutionType, Criteria> tradeTypeTradeExecutionTypeDAO) {
		this.tradeTypeTradeExecutionTypeDAO = tradeTypeTradeExecutionTypeDAO;
	}


	public TradeExecutionTypeListByTradeTypeCache getTradeExecutionTypeListByTradeTypeCache() {
		return this.tradeExecutionTypeListByTradeTypeCache;
	}


	public void setTradeExecutionTypeListByTradeTypeCache(TradeExecutionTypeListByTradeTypeCache tradeExecutionTypeListByTradeTypeCache) {
		this.tradeExecutionTypeListByTradeTypeCache = tradeExecutionTypeListByTradeTypeCache;
	}
}
