package com.clifton.trade.execution;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.system.condition.SystemCondition;


/**
 * A DTO class that represents trade execution types, such as Limit Trade.
 *
 * @author davidi
 */
@CacheByName
public class TradeExecutionType extends NamedEntity<Short> {

	public final static String MOC_TRADE = "MOC Trade";
	public final static String BTIC_TRADE = "BTIC Trade";
	public final static String CLS_TRADE = "CLS";
	public final static String NON_CLS_TRADE = "Non-CLS";


	////////////////////////////////////////////////////////////////////////////

	private boolean marketOnCloseTrade;
	private boolean bticTrade;
	private boolean clsTrade;


	/**
	 * Only accounts of this type support the execution type.
	 */
	private InvestmentAccountType requiredHoldingInvestmentAccountType;

	/**
	 * Cannot not be used with accounts of this type.
	 */
	private InvestmentAccountType excludeHoldingInvestmentAccountType;

	/**
	 * Defines a system condition that will include a specific holding account if the result is true otherwise it will exclude it.
	 * <p>
	 * For example, when trading Forwards with execution type CLS, the condition will return true if the broker is a CLS Member and the trade type equal to Forwards.
	 * <p>
	 */
	private SystemCondition executingBrokerFilter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isMarketOnCloseTrade() {
		return this.marketOnCloseTrade;
	}


	public void setMarketOnCloseTrade(boolean marketOnCloseTrade) {
		this.marketOnCloseTrade = marketOnCloseTrade;
	}


	public boolean isBticTrade() {
		return this.bticTrade;
	}


	public void setBticTrade(boolean bticTrade) {
		this.bticTrade = bticTrade;
	}


	public InvestmentAccountType getRequiredHoldingInvestmentAccountType() {
		return this.requiredHoldingInvestmentAccountType;
	}


	public void setRequiredHoldingInvestmentAccountType(InvestmentAccountType requiredHoldingInvestmentAccountType) {
		this.requiredHoldingInvestmentAccountType = requiredHoldingInvestmentAccountType;
	}


	public InvestmentAccountType getExcludeHoldingInvestmentAccountType() {
		return this.excludeHoldingInvestmentAccountType;
	}


	public void setExcludeHoldingInvestmentAccountType(InvestmentAccountType excludeHoldingInvestmentAccountType) {
		this.excludeHoldingInvestmentAccountType = excludeHoldingInvestmentAccountType;
	}


	public SystemCondition getExecutingBrokerFilter() {
		return this.executingBrokerFilter;
	}


	public void setExecutingBrokerFilter(SystemCondition executingBrokerFilter) {
		this.executingBrokerFilter = executingBrokerFilter;
	}


	public boolean isClsTrade() {
		return this.clsTrade;
	}


	public void setClsTrade(boolean clsTrade) {
		this.clsTrade = clsTrade;
	}
}
