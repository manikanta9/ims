package com.clifton.trade.roll.setup;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>TradeRollDate</code> class defines time period in the system when a security will be rolled.
 * The start/end dates for the security roll are required, and a default new security is optional.
 * When populated, if the roll from security is in the type's selected current securities group,
 * it will be automatically updated to the new security on the start date.
 *
 * @author manderson
 */
public class TradeRollDate extends BaseEntity<Integer> implements LabeledObject {

	/**
	 * Each roll date entry applies to a specific type so we can define roll schedules separately
	 */
	private TradeRollType rollType;

	/**
	 * Roll from security is globally unique.
	 */
	private InvestmentSecurity rollFromSecurity;

	/**
	 * This is the default security that we are rolling into.  New security is not required.
	 */
	private InvestmentSecurity defaultNewSecurity;

	/**
	 * The date range when the roll begins and ends. Often can be the same date.
	 */
	private Date startDate;
	private Date endDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		result.append(getRollFromSecurity() == null ? "???" : getRollFromSecurity().getSymbol());
		result.append(" to ");
		result.append(getDefaultNewSecurity() == null ? "???" : getDefaultNewSecurity().getSymbol());
		if (DateUtils.isEqual(getStartDate(), getEndDate())) {
			result.append(" on ");
			result.append(DateUtils.fromDateShort(getStartDate()));
		}
		else {
			result.append(" from ");
			result.append(DateUtils.fromDateShort(getStartDate()));
			result.append(" to ");
			result.append(DateUtils.fromDateShort(getEndDate()));
		}

		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getRollFromSecurity() {
		return this.rollFromSecurity;
	}


	public void setRollFromSecurity(InvestmentSecurity rollFromSecurity) {
		this.rollFromSecurity = rollFromSecurity;
	}


	public InvestmentSecurity getDefaultNewSecurity() {
		return this.defaultNewSecurity;
	}


	public void setDefaultNewSecurity(InvestmentSecurity defaultNewSecurity) {
		this.defaultNewSecurity = defaultNewSecurity;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public TradeRollType getRollType() {
		return this.rollType;
	}


	public void setRollType(TradeRollType rollType) {
		this.rollType = rollType;
	}
}
