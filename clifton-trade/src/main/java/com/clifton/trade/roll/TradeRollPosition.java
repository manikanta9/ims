package com.clifton.trade.roll;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>TradeRollPosition</code> ...
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class TradeRollPosition extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentAccount clientAccount;
	private InvestmentAccount holdingAccount;
	private BusinessCompany executingBrokerCompany;

	// Quantity according to what's booked
	private BigDecimal actualQuantity;
	// Quantity according to what was booked previous weekday.  This is used to allocate rolls/tails to asset classes based on existing position allocations
	// We use previous day's quantity so we know the percentage the asset class had before and what to maintain.  This is so if multiple rolls for the same
	// account and security occur on the same day, those percentages are accurately reflected in the updates.
	private BigDecimal previousQuantity;

	// Pending Trade Quantity
	private BigDecimal pendingQuantity;

	private BigDecimal tailQuantity;
	private BigDecimal rollQuantity;

	// Used During Roll Trade Review
	private Trade rollFromTrade;
	private Trade rollToTrade;
	private Trade tailTrade;


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder();
		sb.append("Client Account [");
		if (getClientAccount() != null) {
			sb.append(getClientAccount().getNumber());
		}
		sb.append("] Holding Account [");
		if (getHoldingAccount() != null) {
			sb.append(getHoldingAccount().getNumber());
		}
		sb.append("] Executing Broker [");
		if (getExecutingBrokerCompany() != null) {
			sb.append(getExecutingBrokerCompany().getName());
		}
		sb.append("]");
		return sb.toString();
	}


	public boolean isLongPosition() {
		return MathUtils.isGreaterThan(getQuantity(), 0);
	}


	public BigDecimal getQuantity() {
		if (MathUtils.isNullOrZero(getPendingQuantity())) {
			return getActualQuantity();
		}
		return MathUtils.add(getActualQuantity(), getPendingQuantity());
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public void setTailQuantity(BigDecimal tailQuantity) {
		this.tailQuantity = tailQuantity;
	}


	public void setRollQuantity(BigDecimal rollQuantity) {
		this.rollQuantity = rollQuantity;
	}


	public BigDecimal getActualQuantity() {
		return this.actualQuantity;
	}


	public void setActualQuantity(BigDecimal actualQuantity) {
		this.actualQuantity = actualQuantity;
	}


	public BigDecimal getPendingQuantity() {
		return this.pendingQuantity;
	}


	public void setPendingQuantity(BigDecimal pendingQuantity) {
		this.pendingQuantity = pendingQuantity;
	}


	public Trade getRollFromTrade() {
		return this.rollFromTrade;
	}


	public void setRollFromTrade(Trade rollFromTrade) {
		this.rollFromTrade = rollFromTrade;
	}


	public Trade getRollToTrade() {
		return this.rollToTrade;
	}


	public void setRollToTrade(Trade rollToTrade) {
		this.rollToTrade = rollToTrade;
	}


	public Trade getTailTrade() {
		return this.tailTrade;
	}


	public void setTailTrade(Trade tailTrade) {
		this.tailTrade = tailTrade;
	}


	public BigDecimal getTailQuantity() {
		return this.tailQuantity;
	}


	public BigDecimal getRollQuantity() {
		return this.rollQuantity;
	}


	public BigDecimal getPreviousQuantity() {
		return this.previousQuantity;
	}


	public void setPreviousQuantity(BigDecimal previousQuantity) {
		this.previousQuantity = previousQuantity;
	}
}
