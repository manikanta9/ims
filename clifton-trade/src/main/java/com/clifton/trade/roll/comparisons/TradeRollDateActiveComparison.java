package com.clifton.trade.roll.comparisons;


import com.clifton.accounting.AccountingBean;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.date.DateUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.comparison.RuleViolationComparison;
import com.clifton.trade.roll.setup.TradeRollSetupService;


/**
 * The <code>TradeRollDateActiveComparison</code> attempts to determine the security/trade date
 * from the bean passed.
 * <p>
 * Supports if passed an
 * {@link AccountingBean} or
 * {@link RuleViolation} where the linked or cause entity is an {@link AccountingBean}
 * <p>
 * If we are rolling TO the security on that date, then returns true
 *
 * @author manderson
 */
public class TradeRollDateActiveComparison extends RuleViolationComparison<IdentityObject> {

	private TradeRollSetupService tradeRollSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		Boolean result;
		String msg;

		AccountingBean accountingBean = null;

		if (bean instanceof RuleViolation) {
			IdentityObject entity = getLinkedEntity((RuleViolation) bean);
			if (entity != null && AccountingBean.class.isAssignableFrom(entity.getClass())) {
				accountingBean = (AccountingBean) entity;
			}
			else {
				entity = getCauseEntity((RuleViolation) bean);
				if (entity != null && AccountingBean.class.isAssignableFrom(entity.getClass())) {
					accountingBean = (AccountingBean) entity;
				}
			}
		}
		else if (AccountingBean.class.isAssignableFrom(bean.getClass())) {
			accountingBean = (AccountingBean) bean;
		}

		if (accountingBean != null) {
			result = getTradeRollSetupService().isTradeRollToSecurityOnDate(accountingBean.getInvestmentSecurity().getId(), accountingBean.getTransactionDate());
			msg = "Security [" + accountingBean.getInvestmentSecurity().getLabel() + "] is " + (result ? "" : "not ") + "rolling on [" + DateUtils.fromDateShort(accountingBean.getTransactionDate());
		}
		else {
			result = false;
			msg = "Cannot determine security or trade date from bean of type [" + bean.getClass().getName()
					+ "] in order to determine if currently rolling. Supported classes are [SystemWarning with a Linked or Cause Entity is an AccountingBean, or AccountingBean].";
		}
		if (context != null) {
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeRollSetupService getTradeRollSetupService() {
		return this.tradeRollSetupService;
	}


	public void setTradeRollSetupService(TradeRollSetupService tradeRollSetupService) {
		this.tradeRollSetupService = tradeRollSetupService;
	}
}
