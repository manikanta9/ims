package com.clifton.trade.roll.setup;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;

import java.util.Date;
import java.util.List;


/**
 * The <code>TradeRollSetupService</code> defines methods for working with the setup of trade rolls
 * i.e. types, roll dates for each type, etc.
 *
 * @author manderson
 */
public interface TradeRollSetupService {

	////////////////////////////////////////////////////////////////////////////////
	//////////////                  Trade Roll Types                 ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeRollType getTradeRollType(short id);


	public TradeRollType getTradeRollTypeByName(String name);


	public List<TradeRollType> getTradeRollTypeListBySecurityGroupId(short investmentSecurityGroupId);


	public List<TradeRollType> getTradeRollTypeList();


	public TradeRollType saveTradeRollType(TradeRollType bean);


	public void deleteTradeRollType(short id);


	////////////////////////////////////////////////////////////////////////////////
	///////////////                 Trade Roll Dates                 ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeRollDate getTradeRollDate(int id);


	public List<TradeRollDate> getTradeRollDateList(TradeRollDateSearchForm searchForm);


	public TradeRollDate saveTradeRollDate(TradeRollDate bean);


	public void deleteTradeRollDate(int id);


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Trade Roll Dates - Uploads            //////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Uploads TradeRollDates into the system
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dtoClass = TradeRollDate.class)
	public void uploadTradeRollDateUploadFile(TradeRollDateUploadCommand uploadCommand);


	////////////////////////////////////////////////////////////////////////////////
	////////////                Trade Roll Security Methods              ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the given security is being rolled FROM on the given date
	 * i.e. TradeRollDate has an entry for the security where date falls between start/end dates
	 */
	@DoNotAddRequestMapping
	public boolean isTradeRollSecurityOnDate(int securityId, Date date);


	/**
	 * Returns true if the given security is being rolled TO on the given date
	 * i.e. TradeRollDate has an entry for the security where date falls between start/end dates
	 */
	@DoNotAddRequestMapping
	public boolean isTradeRollToSecurityOnDate(int securityId, Date date);


	/**
	 * Returns the new security we are rolling into if defined for the rollType
	 * and the given security is being rolled on the given date
	 * only returns if there is only one.
	 * <p/>
	 * i.e. TradeRollDate has an entry for the security where date falls between start/end dates
	 */
	@SecureMethod(dtoClass = Trade.class)
	public InvestmentSecurity getTradeRollDefaultNewSecurityForSecurity(short rollTypeId, int securityId, Date date);


	public List<InvestmentSecurity> getTradeRollTypeCurrentSecurityList(final short rollTypeId, final Date activeOnDate);
}
