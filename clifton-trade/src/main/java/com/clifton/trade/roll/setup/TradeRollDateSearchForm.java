package com.clifton.trade.roll.setup;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>TradeRollDateSearchForm</code> ...
 *
 * @author manderson
 */
public class TradeRollDateSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "rollType.id")
	private Short rollTypeId;

	@SearchField(searchFieldPath = "rollType", searchField = "currentSecuritiesGroup.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean currentSecurityGroupPopulated;

	@SearchField(searchFieldPath = "rollType", searchField = "name")
	private String rollTypeName;

	@SearchField(searchFieldPath = "rollFromSecurity", searchField = "instrument.id")
	private Integer instrumentId;

	@SearchField(searchFieldPath = "rollFromSecurity.instrument", searchField = "identifierPrefix,name", sortField = "identifierPrefix")
	private String instrumentLabel;

	@SearchField(searchField = "rollFromSecurity.id")
	private Integer rollFromSecurityId;

	@SearchField(searchFieldPath = "rollFromSecurity", searchField = "symbol,name", sortField = "symbol")
	private String rollFromSecurityLabel;

	@SearchField(searchField = "defaultNewSecurity.id")
	private Integer defaultNewSecurityId;

	@SearchField(searchFieldPath = "defaultNewSecurity", searchField = "symbol,name", sortField = "symbol")
	private String defaultNewSecurityLabel;

	@SearchField(searchField = "defaultNewSecurity", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean defaultNewSecurityPopulated;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getRollTypeId() {
		return this.rollTypeId;
	}


	public void setRollTypeId(Short rollTypeId) {
		this.rollTypeId = rollTypeId;
	}


	public Boolean getCurrentSecurityGroupPopulated() {
		return this.currentSecurityGroupPopulated;
	}


	public void setCurrentSecurityGroupPopulated(Boolean currentSecurityGroupPopulated) {
		this.currentSecurityGroupPopulated = currentSecurityGroupPopulated;
	}


	public String getRollTypeName() {
		return this.rollTypeName;
	}


	public void setRollTypeName(String rollTypeName) {
		this.rollTypeName = rollTypeName;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public String getInstrumentLabel() {
		return this.instrumentLabel;
	}


	public void setInstrumentLabel(String instrumentLabel) {
		this.instrumentLabel = instrumentLabel;
	}


	public Integer getRollFromSecurityId() {
		return this.rollFromSecurityId;
	}


	public void setRollFromSecurityId(Integer rollFromSecurityId) {
		this.rollFromSecurityId = rollFromSecurityId;
	}


	public String getRollFromSecurityLabel() {
		return this.rollFromSecurityLabel;
	}


	public void setRollFromSecurityLabel(String rollFromSecurityLabel) {
		this.rollFromSecurityLabel = rollFromSecurityLabel;
	}


	public Integer getDefaultNewSecurityId() {
		return this.defaultNewSecurityId;
	}


	public void setDefaultNewSecurityId(Integer defaultNewSecurityId) {
		this.defaultNewSecurityId = defaultNewSecurityId;
	}


	public String getDefaultNewSecurityLabel() {
		return this.defaultNewSecurityLabel;
	}


	public void setDefaultNewSecurityLabel(String defaultNewSecurityLabel) {
		this.defaultNewSecurityLabel = defaultNewSecurityLabel;
	}


	public Boolean getDefaultNewSecurityPopulated() {
		return this.defaultNewSecurityPopulated;
	}


	public void setDefaultNewSecurityPopulated(Boolean defaultNewSecurityPopulated) {
		this.defaultNewSecurityPopulated = defaultNewSecurityPopulated;
	}
}
