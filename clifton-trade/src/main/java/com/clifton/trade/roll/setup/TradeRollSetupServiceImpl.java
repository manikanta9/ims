package com.clifton.trade.roll.setup;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.CoalesceExpression;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.upload.SystemUploadHandler;
import com.clifton.trade.roll.cache.TradeRollTypeCurrentSecurityIdsCache;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.DateType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>TradeRollSetupServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class TradeRollSetupServiceImpl implements TradeRollSetupService {

	private AdvancedUpdatableDAO<TradeRollDate, Criteria> tradeRollDateDAO;
	private AdvancedUpdatableDAO<TradeRollType, Criteria> tradeRollTypeDAO;

	private InvestmentInstrumentService investmentInstrumentService;

	private SystemUploadHandler systemUploadHandler;

	private DaoNamedEntityCache<TradeRollType> tradeRollTypeCache;
	private TradeRollTypeCurrentSecurityIdsCache tradeRollTypeCurrentSecurityIdsCache;


	////////////////////////////////////////////////////////////////////////////////
	//////////////                  Trade Roll Types                 ///////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRollType getTradeRollType(short id) {
		return getTradeRollTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeRollType getTradeRollTypeByName(String name) {
		return getTradeRollTypeCache().getBeanForKeyValueStrict(getTradeRollTypeDAO(), name);
	}


	@Override
	public List<TradeRollType> getTradeRollTypeListBySecurityGroupId(short investmentSecurityGroupId) {
		return getTradeRollTypeDAO().findByField("currentSecuritiesGroup.id", investmentSecurityGroupId);
	}


	@Override
	public List<TradeRollType> getTradeRollTypeList() {
		return getTradeRollTypeDAO().findAll();
	}


	@Override
	public TradeRollType saveTradeRollType(TradeRollType bean) {
		return getTradeRollTypeDAO().save(bean);
	}


	@Override
	public void deleteTradeRollType(short id) {
		getTradeRollTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////                 Trade Roll Dates                 ///////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRollDate getTradeRollDate(int id) {
		return getTradeRollDateDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeRollDate> getTradeRollDateList(final TradeRollDateSearchForm searchForm) {
		return getTradeRollDateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * See {@link TradeRollDateObserver} for validation
	 */
	@Override
	public TradeRollDate saveTradeRollDate(TradeRollDate bean) {
		return getTradeRollDateDAO().save(bean);
	}


	@Override
	public void deleteTradeRollDate(int id) {
		getTradeRollDateDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Trade Roll Dates - Uploads            //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadTradeRollDateUploadFile(TradeRollDateUploadCommand uploadCommand) {
		TradeRollType rollType = uploadCommand.getRollType();
		Date defaultStartDate = uploadCommand.getStartDate();
		Date defaultEndDate = uploadCommand.getEndDate();

		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, true);

		int count = 0;
		int skipCount = 0; // Used to track where new security = current security, i.e. that security is still in play and we aren't rolling
		List<TradeRollDate> saveList = new ArrayList<>();
		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			TradeRollDate trd = (TradeRollDate) obj;

			if (uploadCommand.isSimple()) {
				Map<String, List<InvestmentSecurity>> currentSecurityDateMap = new HashMap<>();
				if (trd.getRollType() == null) {
					trd.setRollType(rollType);
				}

				if (trd.getStartDate() == null) {
					trd.setStartDate(defaultStartDate);
				}
				if (trd.getEndDate() == null) {
					trd.setEndDate(defaultEndDate);
				}
				if (trd.getRollFromSecurity() == null) {
					// Determine Roll From Security Based on the Current Security Populated for the Start Date
					// Throws An Exception if it can't find it
					// Uses map so we get all current securities for a date - usually will be uploaded for one date, but could have more than one
					trd.setRollFromSecurity(calculateRollFromSecurity(trd, currentSecurityDateMap));
				}
				if (trd.getDefaultNewSecurity() != null && trd.getDefaultNewSecurity().equals(trd.getRollFromSecurity())) {
					skipCount++;
					continue;
				}
			}

			saveList.add(trd);
			count++;
		}
		getTradeRollDateDAO().saveList(saveList);
		uploadCommand.getUploadResult().addUploadResults("TradeRollDate", count, false);
		uploadCommand.getUploadResult().addUploadResultsSkipped("TradeRollDate", skipCount, "");
	}


	private InvestmentSecurity calculateRollFromSecurity(TradeRollDate rollDate, Map<String, List<InvestmentSecurity>> currentSecurityDateMap) {
		Date date = rollDate.getStartDate();
		if (rollDate.getDefaultNewSecurity() == null) {
			throw new ValidationException("Unable to calculate roll from security.  Missing Default New Security");
		}
		if (rollDate.getStartDate() == null) {
			throw new ValidationException("Unable to calculate roll from security.  Missing Start Date for Trade Roll Date to Security " + rollDate.getDefaultNewSecurity().getSymbol());
		}
		List<InvestmentSecurity> currentSecurityList;
		if (!currentSecurityDateMap.containsKey(DateUtils.fromDateShort(rollDate.getStartDate()))) {
			currentSecurityList = getTradeRollTypeCurrentSecurityList(rollDate.getRollType().getId(), date);
			currentSecurityDateMap.put(DateUtils.fromDateShort(rollDate.getStartDate()), currentSecurityList);
		}
		else {
			currentSecurityList = currentSecurityDateMap.get(DateUtils.fromDateShort(rollDate.getStartDate()));
		}
		List<InvestmentSecurity> filteredList = BeanUtils.filter(currentSecurityList, InvestmentSecurity::getInstrument, rollDate.getDefaultNewSecurity().getInstrument());
		if (CollectionUtils.getSize(filteredList) == 1) {
			return filteredList.get(0);
		}
		if (CollectionUtils.getSize(filteredList) > 1) {
			throw new ValidationException("Unable to system calculate roll from security.  Found more than one \"current\" security for instrument ["
					+ rollDate.getDefaultNewSecurity().getInstrument().getLabel() + "] on [" + DateUtils.fromDateShort(rollDate.getStartDate()) + "]: "
					+ BeanUtils.getPropertyValues(filteredList, "symbol", ","));
		}
		throw new ValidationException("Unable to system calculate roll from security.  Could not determine current security for instrument ["
				+ rollDate.getDefaultNewSecurity().getInstrument().getLabel() + "] on [" + DateUtils.fromDateShort(rollDate.getStartDate()) + "]");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Trade Roll Security Methods              ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isTradeRollSecurityOnDate(int securityId, Date date) {
		TradeRollDateSearchForm searchForm = new TradeRollDateSearchForm();
		searchForm.setRollFromSecurityId(securityId);
		searchForm.setActiveOnDate(date);
		return !CollectionUtils.isEmpty(getTradeRollDateList(searchForm));
	}


	@Override
	public boolean isTradeRollToSecurityOnDate(int securityId, Date date) {
		TradeRollDateSearchForm searchForm = new TradeRollDateSearchForm();
		searchForm.setDefaultNewSecurityId(securityId);
		searchForm.setActiveOnDate(date);
		return !CollectionUtils.isEmpty(getTradeRollDateList(searchForm));
	}


	@Override
	public InvestmentSecurity getTradeRollDefaultNewSecurityForSecurity(short rollTypeId, int securityId, Date date) {
		TradeRollDateSearchForm searchForm = new TradeRollDateSearchForm();
		searchForm.setRollTypeId(rollTypeId);
		searchForm.setRollFromSecurityId(securityId);
		searchForm.setActiveOnDate(date);
		searchForm.setDefaultNewSecurityPopulated(true);
		List<TradeRollDate> list = getTradeRollDateList(searchForm);
		if (CollectionUtils.getSize(list) == 1) {
			return list.get(0).getDefaultNewSecurity();
		}
		return null;
	}


	@Override
	public List<InvestmentSecurity> getTradeRollTypeCurrentSecurityList(final short rollTypeId, final Date activeOnDate) {
		ValidationUtils.assertNotNull(activeOnDate, "Date is required to determine current security list.");

		List<InvestmentSecurity> currentSecurityList = new ArrayList<>();
		Integer[] currentSecurityIds = getTradeRollTypeCurrentSecurityIdsCache().getCurrentSecurityIdsForTradeRollType(rollTypeId, activeOnDate);
		if (currentSecurityIds == null) {
			// Get Last Roll (Start Date) on or Before Date
			HibernateSearchConfigurer config = criteria -> {
				criteria.add(Restrictions.le("startDate", activeOnDate));
				criteria.add(Restrictions.eq("rollType.id", rollTypeId));
				// Get the last roll that started before Active On Date
				// When using alias in sql restriction need to append 1_ to the alias
				// it's just what it always is
				criteria.createAlias("rollFromSecurity", "rfs").add(Restrictions.sqlRestriction("{alias}.RollStartDate = (SELECT MAX(x.RollStartDate) FROM TradeRollDate x INNER JOIN InvestmentSecurity xs ON x.RollFromSecurityID = xs.InvestmentSecurityID WHERE x.RollStartDate <= ? AND xs.InvestmentInstrumentID = rfs1_.InvestmentInstrumentID)"
						, activeOnDate, DateType.INSTANCE));
				// Only include where roll to security is still active
				criteria.createAlias("defaultNewSecurity", "rts");
				criteria.add(Restrictions.or(Restrictions.isNull("rts.startDate"), Restrictions.le("rts.startDate", activeOnDate)));
				criteria.add(new CoalesceExpression(">=", new String[]{"rts.earlyTerminationDate", "rts.endDate"}, activeOnDate, activeOnDate, false)); // No reason to allow null with a default value present that would never be null
			};
			List<TradeRollDate> trdList = getTradeRollDateDAO().findBySearchCriteria(config);
			for (TradeRollDate trd : CollectionUtils.getIterable(trdList)) {
				if (trd.getDefaultNewSecurity() != null) {
					currentSecurityList.add(trd.getDefaultNewSecurity());
				}
			}
			getTradeRollTypeCurrentSecurityIdsCache().storeCurrentSecurityIdsForTradeRollType(rollTypeId, activeOnDate, currentSecurityList);
		}
		else if (currentSecurityIds.length > 0) {
			currentSecurityList = getInvestmentInstrumentService().getInvestmentSecurityListByIds(Arrays.asList(currentSecurityIds));
		}
		return currentSecurityList;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeRollDate, Criteria> getTradeRollDateDAO() {
		return this.tradeRollDateDAO;
	}


	public void setTradeRollDateDAO(AdvancedUpdatableDAO<TradeRollDate, Criteria> tradeRollDateDAO) {
		this.tradeRollDateDAO = tradeRollDateDAO;
	}


	public AdvancedUpdatableDAO<TradeRollType, Criteria> getTradeRollTypeDAO() {
		return this.tradeRollTypeDAO;
	}


	public void setTradeRollTypeDAO(AdvancedUpdatableDAO<TradeRollType, Criteria> tradeRollTypeDAO) {
		this.tradeRollTypeDAO = tradeRollTypeDAO;
	}


	public DaoNamedEntityCache<TradeRollType> getTradeRollTypeCache() {
		return this.tradeRollTypeCache;
	}


	public void setTradeRollTypeCache(DaoNamedEntityCache<TradeRollType> tradeRollTypeCache) {
		this.tradeRollTypeCache = tradeRollTypeCache;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}


	public TradeRollTypeCurrentSecurityIdsCache getTradeRollTypeCurrentSecurityIdsCache() {
		return this.tradeRollTypeCurrentSecurityIdsCache;
	}


	public void setTradeRollTypeCurrentSecurityIdsCache(TradeRollTypeCurrentSecurityIdsCache tradeRollTypeCurrentSecurityIdsCache) {
		this.tradeRollTypeCurrentSecurityIdsCache = tradeRollTypeCurrentSecurityIdsCache;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
