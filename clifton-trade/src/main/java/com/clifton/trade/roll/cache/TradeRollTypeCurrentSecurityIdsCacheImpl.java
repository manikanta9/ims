package com.clifton.trade.roll.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.roll.setup.TradeRollDate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>TradeRollTypeCurrentSecurityIdsCacheImpl</code> caches the list of TradeRollDates for a given TradeRollType and Active On Date
 *
 * @author manderson
 */
@Component
public class TradeRollTypeCurrentSecurityIdsCacheImpl extends SelfRegisteringSimpleDaoCache<TradeRollDate, Short, Map<Long, Integer[]>> implements TradeRollTypeCurrentSecurityIdsCache {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer[] getCurrentSecurityIdsForTradeRollType(short tradeRollTypeId, Date activeOnDate) {
		Map<Long, Integer[]> activeOnDateMap = getCacheHandler().get(getCacheName(), tradeRollTypeId);
		if (activeOnDateMap == null || !activeOnDateMap.containsKey(activeOnDate.getTime())) {
			return null;
		}
		return activeOnDateMap.get(activeOnDate.getTime());
	}


	@Override
	public void storeCurrentSecurityIdsForTradeRollType(short tradeRollTypeId, Date activeOnDate, List<InvestmentSecurity> securityList) {
		Map<Long, Integer[]> activeOnDateMap = getCacheHandler().get(getCacheName(), tradeRollTypeId);
		if (activeOnDateMap == null) {
			activeOnDateMap = new ConcurrentHashMap<>();
		}
		activeOnDateMap.put(activeOnDate.getTime(), BeanUtils.getPropertyValues(securityList, "id", Integer.class));
		getCacheHandler().put(getCacheName(), tradeRollTypeId, activeOnDateMap);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<TradeRollDate> dao, DaoEventTypes event, TradeRollDate bean) {
		if (event.isUpdate()) { // Only needed for updates in case the key has changed - want to clear the original key so we don't leave bad objects in the cache
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<TradeRollDate> dao, DaoEventTypes event, TradeRollDate bean, Throwable e) {
		if (e == null) {
			// clear all values for the roll type
			getCacheHandler().remove(getCacheName(), bean.getRollType().getId());
			if (event.isUpdate()) {
				// also clear all values for original bean in case roll type changed
				TradeRollDate originalBean = getOriginalBean(dao, bean);
				getCacheHandler().remove(getCacheName(), originalBean.getRollType().getId());
			}
		}
	}
}
