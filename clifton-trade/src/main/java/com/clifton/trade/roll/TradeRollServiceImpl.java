package com.clifton.trade.roll;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationSearchForm;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocationSearchForm;
import com.clifton.trade.assetclass.TradeAssetClassService;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroup.TradeGroupActions;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;


/**
 * The <code>TradeRollServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class TradeRollServiceImpl implements TradeRollService {

	private AccountingPositionDailyService accountingPositionDailyService;
	private AccountingPositionService accountingPositionService;
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;
	private InvestmentInstrumentService investmentInstrumentService;
	private TradeAssetClassService tradeAssetClassService;
	private TradeDestinationService tradeDestinationService;
	private TradeService tradeService;
	private TradeGroupService tradeGroupService;
	private SecurityUserService securityUserService;
	private BusinessCompanyService businessCompanyService;


	////////////////////////////////////////////////////////////////////////////////
	//////////                   Trade Roll Entry                         //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRoll getTradeRollEntry(TradeRoll roll) {
		// IDs are set only Not fully populated from UI - probably because it's a Virtual DTO
		ValidationUtils.assertNotNull(roll.getSecurity(), "Security selection is required to view existing positions", "security.id");
		setTradeRollEntryPositionList(roll);
		return roll;
	}


	@Override
	public void saveTradeRollEntry(TradeRoll roll) {
		if (CollectionUtils.isEmpty(roll.getPositionList())) {
			throw new ValidationException("There are no positions to roll");
		}

		// Because of NonPersistentObject beans aren't populated - just ids
		roll.setSecurity(getInvestmentInstrumentService().getInvestmentSecurity(roll.getSecurity().getId()));
		roll.setNewSecurity(getInvestmentInstrumentService().getInvestmentSecurity(roll.getNewSecurity().getId()));
		roll.setTradeDestination(getTradeDestinationService().getTradeDestination(roll.getTradeDestination().getId()));

		// Validate Roll From/To are different securities
		if (roll.getSecurity().equals(roll.getNewSecurity())) {
			throw new ValidationException("Cannot Roll From/To the same security.  Please select a different security to roll into.");
		}
		// Validate Roll From/To are the same instrument
		if (!roll.getSecurity().getInstrument().equals(roll.getNewSecurity().getInstrument())) {
			throw new ValidationException("Cannot Roll From/To the different instruments.  Please select a security to roll into from the same instrument ["
					+ roll.getSecurity().getInstrument().getIdentifierPrefix() + ".");
		}

		// Look for Allocations on Trade Date - Actual + Pending
		InvestmentAccountAssetClassPositionAllocationSearchForm positionAllocationSearchForm = new InvestmentAccountAssetClassPositionAllocationSearchForm();
		positionAllocationSearchForm.setActiveOnDate(roll.getTradeDate());
		positionAllocationSearchForm.setSecurityId(roll.getSecurity().getId());
		List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList = getInvestmentAccountAssetClassPositionAllocationService().getInvestmentAccountAssetClassPositionAllocationList(positionAllocationSearchForm);

		TradeAssetClassPositionAllocationSearchForm tradePositionAllocationSearchForm = new TradeAssetClassPositionAllocationSearchForm();
		tradePositionAllocationSearchForm.setTradeDate(roll.getTradeDate());
		tradePositionAllocationSearchForm.setSecurityId(roll.getSecurity().getId());
		tradePositionAllocationSearchForm.setProcessed(false);
		List<TradeAssetClassPositionAllocation> pendingTradeAssetClassList = getTradeAssetClassService().getTradeAssetClassPositionAllocationList(tradePositionAllocationSearchForm);

		List<TradeAssetClassPositionAllocation> newTradeAssetClassList = new ArrayList<>();

		roll.setTraderUser(getSecurityUserService().getSecurityUser(roll.getTraderUser().getId()));
		roll.setExecutingBrokerCompany(getBusinessCompanyService().getBusinessCompany(roll.getExecutingBrokerCompany().getId()));
		roll.setTradeType(getTradeService().getTradeTypeForSecurity(roll.getSecurity().getId()));

		roll.setRollTradeGroup(new TradeGroup());
		roll.setTailTradeGroup(new TradeGroup());

		for (TradeRollPosition trp : CollectionUtils.getIterable(roll.getPositionList())) {
			// Validate Tail/Roll <= Total Quantity
			BigDecimal remaining = MathUtils.abs(trp.getQuantity());
			if (!MathUtils.isNullOrZero(trp.getTailQuantity())) {
				remaining = MathUtils.subtract(remaining, trp.getTailQuantity());
			}
			if (!MathUtils.isNullOrZero(trp.getRollQuantity())) {
				remaining = MathUtils.subtract(remaining, trp.getRollQuantity());
			}
			if (MathUtils.isLessThan(remaining, BigDecimal.ZERO)) {
				throw new ValidationException(trp.getClientAccount().getLabel() + ": Cannot enter tail/roll quantities that exceed the total quantity held.");
			}
			addTradesForRollPosition(roll, trp, positionAllocationList, pendingTradeAssetClassList, newTradeAssetClassList);
		}
		saveTradeRollPositionTrades(roll, newTradeAssetClassList);
	}


	@Transactional(timeout = 180)
	protected void saveTradeRollPositionTrades(TradeRoll roll, List<TradeAssetClassPositionAllocation> tradeAssetClassList) {
		TradeGroup rollGroup = roll.getRollTradeGroup();
		if (!CollectionUtils.isEmpty(rollGroup.getTradeList())) {
			rollGroup.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.ROLL.getTypeName()));
			rollGroup.setInvestmentSecurity(roll.getSecurity());
			rollGroup.setSecondaryInvestmentSecurity(roll.getNewSecurity());
			rollGroup.setTradeDate(roll.getTradeDate());
			rollGroup.setNote("Roll " + CoreMathUtils.formatNumberInteger(MathUtils.divide(CoreMathUtils.sumProperty(rollGroup.getTradeList(), Trade::getQuantityIntended), 2)) + " "
					+ rollGroup.getInvestmentSecurity().getSymbol() + (roll.isLongPositions() ? "(Long " : "(Short") + "Positions) to " + rollGroup.getSecondaryInvestmentSecurity().getSymbol()
					+ " for " + (CollectionUtils.getSize(rollGroup.getTradeList()) / 2) + " accounts.");
			rollGroup.setTraderUser(roll.getTraderUser());
			getTradeGroupService().saveTradeGroup(rollGroup);
		}
		else {
			throw new ValidationException("Cannot create tail trades without any rolls.");
		}

		TradeGroup tailGroup = roll.getTailTradeGroup();
		if (!CollectionUtils.isEmpty(tailGroup.getTradeList())) {
			tailGroup.setParent(rollGroup);
			tailGroup.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.ROLL_TAIL.getTypeName()));
			tailGroup.setInvestmentSecurity(roll.getSecurity());
			tailGroup.setTradeDate(roll.getTradeDate());
			tailGroup.setNote((roll.isLongPositions() ? "Sell " : "Buy ") + CoreMathUtils.formatNumberInteger(CoreMathUtils.sumProperty(tailGroup.getTradeList(), Trade::getQuantityIntended)) + " "
					+ tailGroup.getInvestmentSecurity().getSymbol() + " for " + CollectionUtils.getSize(tailGroup.getTradeList()) + " accounts before rolling");
			tailGroup.setTraderUser(roll.getTraderUser());
			getTradeGroupService().saveTradeGroup(tailGroup);
		}

		if (!CollectionUtils.isEmpty(tradeAssetClassList)) {
			getTradeAssetClassService().saveTradeAssetClassPositionAllocationList(tradeAssetClassList);
		}
	}


	private void addTradesForRollPosition(TradeRoll roll, TradeRollPosition trp, List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, List<TradeAssetClassPositionAllocation> pendingTradeAssetClassList, List<TradeAssetClassPositionAllocation> newTradeAssetClassList) {

		BigDecimal tailQuantity = trp.getTailQuantity();
		BigDecimal rollQuantity = trp.getRollQuantity();

		// Only trade if there is a quantity
		if (!MathUtils.isNullOrZero(tailQuantity)) {
			Trade tailTrade = populateTrade(roll, trp, tailQuantity, true);
			tailTrade.setTradeDestination(roll.getTradeDestination());
			tailTrade.setDescription((tailTrade.isBuy() ? "Buy " : "Sell ") + CoreMathUtils.formatNumberInteger(tailQuantity) + " " + roll.getSecurity().getSymbol() + " before rolling.");
			roll.getTailTradeGroup().addTrade(tailTrade);

			addTradeAssetClassPositionAllocationListForTradeRollPosition(trp, tailTrade, null, positionAllocationList, pendingTradeAssetClassList, newTradeAssetClassList);
		}

		if (!MathUtils.isNullOrZero(rollQuantity)) {
			Trade rollFromTrade = populateTrade(roll, trp, rollQuantity, true);
			rollFromTrade.setDescription("(Roll From Trade): Roll " + CoreMathUtils.formatNumberInteger(rollQuantity) + " " + roll.getSecurity().getSymbol() + " to " + roll.getNewSecurity().getSymbol());

			Trade rollToTrade = populateTrade(roll, trp, rollQuantity, false);
			rollToTrade.setDescription("(Roll To Trade): Roll " + CoreMathUtils.formatNumberInteger(rollQuantity) + " " + roll.getSecurity().getSymbol() + " to " + roll.getNewSecurity().getSymbol());

			roll.getRollTradeGroup().addTrade(rollFromTrade);
			roll.getRollTradeGroup().addTrade(rollToTrade);

			addTradeAssetClassPositionAllocationListForTradeRollPosition(trp, rollFromTrade, rollToTrade, positionAllocationList, pendingTradeAssetClassList, newTradeAssetClassList);
		}
	}


	/**
	 * Creates TradeAssetClassPositionAllocations and adds them to the list for the specified TradeRollPosition for either a roll or tail trade.
	 * For tail trades, the toTrade is null, so we know now to roll the allocation, but just adjust it for the old security
	 */
	private void addTradeAssetClassPositionAllocationListForTradeRollPosition(TradeRollPosition tradeRollPosition, Trade fromTrade, Trade toTrade, List<InvestmentAccountAssetClassPositionAllocation> positionAllocationList, List<TradeAssetClassPositionAllocation> pendingTradeAssetClassList, List<TradeAssetClassPositionAllocation> newTradeAssetClassList) {

		List<InvestmentAccountAssetClassPositionAllocation> filteredPositionAllocationList = BeanUtils.filter(positionAllocationList, investmentAccountAssetClassPositionAllocation -> investmentAccountAssetClassPositionAllocation.getAccountAssetClass().getAccount(), fromTrade.getClientInvestmentAccount());
		List<TradeAssetClassPositionAllocation> filteredPendingPositionAllocationList = BeanUtils.filter(pendingTradeAssetClassList, tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getAccountAssetClass().getAccount(), fromTrade.getClientInvestmentAccount());
		if (!CollectionUtils.isEmpty(filteredPositionAllocationList)) {
			// Percentage we are rolling as compared to Current Actual + Pending
			BigDecimal percentage = CoreMathUtils.getPercentValue(fromTrade.getQuantityIntended(), MathUtils.abs(MathUtils.add(tradeRollPosition.getActualQuantity(), tradeRollPosition.getPendingQuantity())));
			// This is the total allocated explicitly
			BigDecimal allocatedQuantity = CoreMathUtils.sumProperty(filteredPositionAllocationList, InvestmentAccountAssetClassPositionAllocation::getAllocationQuantity);
			// Plus total allocated for pending trades (use 'adjusted' which excludes canceled trades)
			BigDecimal pendingAllocatedQuantity = CoreMathUtils.sumProperty(filteredPendingPositionAllocationList, TradeAssetClassPositionAllocation::getAllocationQuantityAdjusted);

			// This the total we should allocate from the roll/tail trade
			BigDecimal rollQuantity = MathUtils.negate(MathUtils.round(MathUtils.getPercentageOf(percentage, MathUtils.add(allocatedQuantity, pendingAllocatedQuantity)), 0));
			List<TradeAssetClassPositionAllocation> rollFromTradeAssetClassList = new ArrayList<>();
			for (InvestmentAccountAssetClassPositionAllocation posAllocation : filteredPositionAllocationList) {
				TradeAssetClassPositionAllocation tradeAlloc = new TradeAssetClassPositionAllocation();
				tradeAlloc.setAccountAssetClass(posAllocation.getAccountAssetClass());
				tradeAlloc.setReplication(posAllocation.getReplication());
				tradeAlloc.setSecurity(fromTrade.getInvestmentSecurity());
				tradeAlloc.setTrade(fromTrade);
				tradeAlloc.setTradeDate(fromTrade.getTradeDate());
				tradeAlloc.setAllocationQuantity(MathUtils.negate(MathUtils.round(MathUtils.getPercentageOf(percentage, posAllocation.getAllocationQuantity()), 0)));
				rollFromTradeAssetClassList.add(tradeAlloc);
			}
			newTradeAssetClassList.addAll(rollFromTradeAssetClassList);
			CoreMathUtils.applySumPropertyDifference(rollFromTradeAssetClassList, TradeAssetClassPositionAllocation::getAllocationQuantity, TradeAssetClassPositionAllocation::setAllocationQuantity, rollQuantity, true);
			if (toTrade != null) {
				// Add Roll To Trade Allocations
				for (TradeAssetClassPositionAllocation rollFromTradeAlloc : rollFromTradeAssetClassList) {
					TradeAssetClassPositionAllocation rollToTradeAlloc = BeanUtils.cloneBean(rollFromTradeAlloc, false, false);
					rollToTradeAlloc.setSecurity(toTrade.getInvestmentSecurity());
					rollToTradeAlloc.setAllocationQuantity(MathUtils.negate(rollToTradeAlloc.getAllocationQuantity()));
					rollToTradeAlloc.setTrade(toTrade);
					newTradeAssetClassList.add(rollToTradeAlloc);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                    Roll Trade Group                        //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRoll getTradeRollGroup(int id) {
		TradeRoll roll = new TradeRoll();
		roll.setRollTradeGroup(getTradeGroupService().getTradeGroup(id));
		roll.setTailTradeGroup(getTradeGroupService().getTradeGroupByParent(roll.getRollTradeGroup().getId()));
		setTradeRollGroupPositionList(roll);
		return roll;
	}


	@Override
	@Transactional
	public void saveTradeRollGroup(TradeRoll bean) {
		TradeGroupActions action = bean.getTradeGroupAction();
		if (TradeGroupActions.SAVE_TRADE == action) {
			saveTradeRollGroupTradeList(bean);
		}
		else {
			List<Integer> tradeIdList = new ArrayList<>();
			List<TradeRollPosition> trpList = new ArrayList<>();

			// Get the list of trade
			for (TradeRollPosition trp : CollectionUtils.getIterable(bean.getPositionList())) {
				if (trp.getRollFromTrade() != null && trp.getRollFromTrade().getId() != null) {
					tradeIdList.add(trp.getRollFromTrade().getId());
					tradeIdList.add(trp.getRollToTrade().getId());

					if (TradeGroupActions.FILL_EXECUTE == action) {
						trp.setRollFromTrade(getTradeService().getTrade(trp.getRollFromTrade().getId()));
						trp.setRollToTrade(getTradeService().getTrade(trp.getRollToTrade().getId()));
						trp.setRollQuantity(trp.getRollFromTrade().getQuantityIntended());
						trpList.add(trp);
					}
				}

				if (TradeGroupActions.CANCEL == action || TradeGroupActions.APPROVE == action || TradeGroupActions.REJECT == action || TradeGroupActions.VALIDATE == action) {
					if (trp.getTailTrade() != null && trp.getTailTrade().getId() != null) {
						tradeIdList.add(trp.getTailTrade().getId());
					}
				}
			}

			if (TradeGroupActions.DELETE_FILLS == action) {
				List<TradeFill> tradeFillList = new ArrayList<>();
				for (Integer tradeId : CollectionUtils.getIterable(tradeIdList)) {
					tradeFillList.addAll(getTradeService().getTradeFillListByTrade(tradeId));
				}
				getTradeService().deleteTradeFillList(tradeFillList);
			}

			if (TradeGroupActions.FILL_EXECUTE == action) {
				BigDecimal maxFill = CoreMathUtils.sumProperty(trpList, TradeRollPosition::getRollQuantity);
				BigDecimal thisFill = bean.getFillQuantity();
				boolean partialFill = false;
				// Update quantities if a partial fill
				if (!MathUtils.isNullOrZero(thisFill) && !MathUtils.isEqual(thisFill, maxFill)) {
					BigDecimal percentage = CoreMathUtils.getPercentValue(thisFill, maxFill, true);
					partialFill = true;
					for (TradeRollPosition trp : CollectionUtils.getIterable(trpList)) {
						trp.setRollQuantity(MathUtils.round(MathUtils.getPercentageOf(percentage, trp.getRollQuantity(), true), 0));
					}
					CoreMathUtils.applySumPropertyDifference(trpList, TradeRollPosition::getRollQuantity, TradeRollPosition::setRollQuantity, thisFill, true);
				}

				List<TradeFill> tradeFillList = new ArrayList<>();
				for (TradeRollPosition trp : CollectionUtils.getIterable(trpList)) {
					TradeFill fromFill = new TradeFill();
					fromFill.setTrade(trp.getRollFromTrade());
					fromFill.setNotionalUnitPrice(bean.getFromSecurityPrice());
					fromFill.setQuantity(trp.getRollQuantity());
					tradeFillList.add(fromFill);

					TradeFill toFill = new TradeFill();
					toFill.setTrade(trp.getRollToTrade());
					toFill.setNotionalUnitPrice(bean.getToSecurityPrice());
					toFill.setQuantity(trp.getRollQuantity());
					tradeFillList.add(toFill);
				}

				getTradeService().saveTradeFillList(tradeFillList);
				if (partialFill) {
					// Update roll trade note for partial fill
					updateRollTradeGroupNoteForPartialFill(bean.getRollTradeGroup().getId(), maxFill, thisFill);
					// Adjust roll trade trades for partial fill
					getTradeGroupService().executeActionOnTrades(TradeGroupActions.CANNOT_FILL, tradeIdList);
				}
				// We can safely return at this point because workflow transitions will be automatically transitioned from Fully Filled to Executed
				return;
			}

			getTradeGroupService().executeActionOnTrades(action, tradeIdList);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////            Trade Roll Position Helper Methods              ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Generate Position List for Roll Trade Entry
	 */
	private void setTradeRollEntryPositionList(TradeRoll roll) {
		Date snapshotDate = roll.getTradeDate();
		if (snapshotDate == null) {
			snapshotDate = new Date();
		}
		AccountingPositionDailyLiveSearchForm liveSearchForm = new AccountingPositionDailyLiveSearchForm();
		liveSearchForm.setSnapshotDate(snapshotDate);
		liveSearchForm.setInvestmentSecurityId(roll.getSecurity().getId());
		List<AccountingPositionDaily> positionList = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(liveSearchForm);

		Map<String, TradeRollPosition> map = new HashMap<>();

		// If only for specific broker, filter out
		Integer holdingCompanyId = roll.getHoldingCompany() != null ? roll.getHoldingCompany().getId() : null;
		if (holdingCompanyId != null) {
			positionList = BeanUtils.filter(positionList, accountingPositionDaily -> accountingPositionDaily.getAccountingTransaction() == null ? null : accountingPositionDaily.getAccountingTransaction().getHoldingInvestmentAccount().getIssuingCompany().getId(), holdingCompanyId);
		}
		for (AccountingPositionDaily pos : CollectionUtils.getIterable(positionList)) {
			String uid = pos.getAccountingTransaction().getClientInvestmentAccount().getId() + "_" + pos.getAccountingTransaction().getHoldingInvestmentAccount().getId();
			TradeRollPosition rp = map.get(uid);
			if (rp == null) {
				rp = new TradeRollPosition();
				rp.setClientAccount(pos.getAccountingTransaction().getClientInvestmentAccount());
				rp.setHoldingAccount(pos.getAccountingTransaction().getHoldingInvestmentAccount());
			}

			rp.setActualQuantity(MathUtils.add(rp.getQuantity(), pos.getRemainingQuantity()));
			rp.setPreviousQuantity(MathUtils.add(rp.getPreviousQuantity(), pos.getPriorQuantity()));
			map.put(uid, rp);
		}

		List<TradeRollPosition> trpList = new ArrayList<>(map.values());
		trpList = BeanUtils.filter(trpList, TradeRollPosition::isLongPosition, roll.isLongPositions());

		if (!CollectionUtils.isEmpty(trpList)) {
			List<Trade> pendingTrades = getTradeService().getTradePendingList(null, null, roll.getSecurity().getId());
			for (TradeRollPosition trp : CollectionUtils.getIterable(trpList)) {
				for (Trade pt : CollectionUtils.getIterable(pendingTrades)) {
					if (trp.getClientAccount().equals(pt.getClientInvestmentAccount()) && trp.getHoldingAccount().equals(pt.getHoldingInvestmentAccount())) {
						BigDecimal pending = (pt.isBuy()) ? pt.getQuantityIntended() : MathUtils.negate(pt.getQuantityIntended());
						trp.setPendingQuantity(MathUtils.add(trp.getPendingQuantity(), pending));
					}
				}
				trp.setRollQuantity(MathUtils.abs(trp.getQuantity()));
			}
		}
		roll.setPositionList(trpList);
	}


	/**
	 * Generate Position List for Roll Review
	 */
	private void setTradeRollGroupPositionList(TradeRoll roll) {
		Map<String, TradeRollPosition> map = new HashMap<>();
		InvestmentSecurity rollFrom = roll.getRollTradeGroup().getInvestmentSecurity();
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForSecurity(roll.getRollTradeGroup().getInvestmentSecurity().getId(),
				roll.getRollTradeGroup().getTradeDate());
		// GO THROUGH TRADES IN ROLL GROUP
		for (Trade rollTrade : CollectionUtils.getIterable(roll.getRollTradeGroup().getTradeList())) {
			String uid = rollTrade.getClientInvestmentAccount().getId() + "_" + rollTrade.getHoldingInvestmentAccount().getId();
			TradeRollPosition rp = map.get(uid);
			if (rp == null) {
				rp = new TradeRollPosition();
				rp.setClientAccount(rollTrade.getClientInvestmentAccount());
				rp.setHoldingAccount(rollTrade.getHoldingInvestmentAccount());
			}
			if (rollTrade.getInvestmentSecurity().equals(rollFrom)) {
				List<AccountingPosition> tradePositionList = BeanUtils.filter(positionList, AccountingPosition::getClientInvestmentAccount, rp.getClientAccount());
				tradePositionList = BeanUtils.filter(tradePositionList, AccountingPosition::getHoldingInvestmentAccount, rp.getHoldingAccount());
				rp.setActualQuantity(CoreMathUtils.sumProperty(tradePositionList, AccountingPosition::getRemainingQuantity));
				rp.setRollQuantity(rollTrade.getQuantityIntended());
				rp.setRollFromTrade(rollTrade);
			}
			else {
				rp.setRollToTrade(rollTrade);
			}
			map.put(uid, rp);
		}
		// GO THROUGH TRADES IN TAIL GROUP
		if (roll.getTailTradeGroup() != null) {
			for (Trade tailTrade : CollectionUtils.getIterable(roll.getTailTradeGroup().getTradeList())) {
				String uid = tailTrade.getClientInvestmentAccount().getId() + "_" + tailTrade.getHoldingInvestmentAccount().getId();
				TradeRollPosition rp = map.get(uid);
				if (rp == null) {
					rp = new TradeRollPosition();
					rp.setClientAccount(tailTrade.getClientInvestmentAccount());
					rp.setHoldingAccount(tailTrade.getHoldingInvestmentAccount());
				}
				rp.setTailQuantity(tailTrade.getQuantityIntended());
				rp.setTailTrade(tailTrade);
				map.put(uid, rp);
			}
		}

		List<TradeRollPosition> trpList = new ArrayList<>(map.values());
		roll.setPositionList(trpList);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Trade Helper Methods                    ////////////
	////////////////////////////////////////////////////////////////////////////////


	private Trade populateTrade(TradeRoll roll, TradeRollPosition trp, BigDecimal quantity, boolean fromSec) {
		Trade trade = new Trade();

		trade.setQuantityIntended(quantity);
		trade.setBuy(fromSec ? !roll.isLongPositions() : roll.isLongPositions());
		trade.setInvestmentSecurity(fromSec ? roll.getSecurity() : roll.getNewSecurity());
		trade.setPayingSecurity(InvestmentUtils.getSettlementCurrency(trade.getInvestmentSecurity()));

		trade.setExecutingBrokerCompany(roll.getExecutingBrokerCompany());
		trade.setClientInvestmentAccount(trp.getClientAccount());
		trade.setHoldingInvestmentAccount(trp.getHoldingAccount());
		trade.setTradeDate(roll.getTradeDate());
		trade.setTraderUser(roll.getTraderUser());
		trade.setTradeDestination(roll.getTradeDestination());
		trade.setTradeType(roll.getTradeType());
		trade.setSettlementDate(roll.getSettlementDate());
		trade.setBlockTrade(roll.isCreateBlockTrades());
		return trade;
	}


	/**
	 * Saves any trade updates to the respective roll and roll tail trade groups of the provided {@link TradeRoll}.
	 * <p>
	 * The trades will be updated to reflect the value of {@link TradeRoll#createBlockTrades}.
	 */
	private void saveTradeRollGroupTradeList(TradeRoll bean) {
		List<Trade> rollTradeList = new ArrayList<>();
		List<Trade> tailTradeList = new ArrayList<>();
		/* Populate the provided list with the provided trade if the trade is not null.
		 * Also sets the trade's blockTrade property. Cannot rely on updating the trades as block trades from the UI
		 * because the trade properties are populated during binding from a hash map. The ID property for hte trade
		 * may be process after the block property causing the block value sent from the UI to be overwritten.
		 */
		BiConsumer<List<Trade>, Trade> tradeListPopulator = (tradeList, trade) -> {
			if (trade != null && !trade.isNewBean()) {
				trade.setBlockTrade(bean.isCreateBlockTrades());
				tradeList.add(trade);
			}
		};
		for (TradeRollPosition tradeRollPosition : CollectionUtils.getIterable(bean.getPositionList())) {
			tradeListPopulator.accept(rollTradeList, tradeRollPosition.getRollFromTrade());
			tradeListPopulator.accept(rollTradeList, tradeRollPosition.getRollToTrade());
			tradeListPopulator.accept(tailTradeList, tradeRollPosition.getTailTrade());
		}
		saveTradeRollGroupTradeList(bean.getRollTradeGroup(), rollTradeList, false);
		saveTradeRollGroupTradeList(bean.getTailTradeGroup(), tailTradeList, true);
	}


	/**
	 * Saves the provided list of trades for the provided trade group. The tailGroup argument is used to
	 * lookup the correct {@link TradeGroupType} if the provided trade group's type is not defined.
	 */
	private void saveTradeRollGroupTradeList(TradeGroup tradeGroup, List<Trade> tradeList, boolean tailGroup) {
		if (!tradeList.isEmpty()) {
			TradeGroup tradeGroupToSave = new TradeGroup();
			BeanUtils.copyProperties(tradeGroup, tradeGroupToSave);
			if (tradeGroupToSave.getTradeGroupType() == null) {
				tradeGroupToSave.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(tailGroup
						? TradeGroupType.GroupTypes.ROLL_TAIL.getTypeName()
						: TradeGroupType.GroupTypes.ROLL.getTypeName()));
			}
			tradeGroupToSave.setTradeGroupAction(TradeGroupActions.SAVE_TRADE);
			tradeGroupToSave.setTradeList(tradeList);
			getTradeGroupService().executeTradeGroupAction(tradeGroupToSave);
		}
	}


	private TradeGroup updateRollTradeGroupNoteForPartialFill(int rollTradeGroupId, BigDecimal maxFill, BigDecimal partialFill) {
		TradeGroup rollGroup = getTradeGroupService().getTradeGroup(rollTradeGroupId);
		String originalFillQuantityString = maxFill.stripTrailingZeros().toPlainString();
		String partialFillQuantityString = partialFill.stripTrailingZeros().toPlainString() + " of " + originalFillQuantityString;
		rollGroup.setNote(rollGroup.getNote().replace(originalFillQuantityString, partialFillQuantityString));
		return getTradeGroupService().saveTradeGroupWithoutTradeUpdates(rollGroup);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////             Getter & Setter Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public InvestmentAccountAssetClassPositionAllocationService getInvestmentAccountAssetClassPositionAllocationService() {
		return this.investmentAccountAssetClassPositionAllocationService;
	}


	public void setInvestmentAccountAssetClassPositionAllocationService(InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService) {
		this.investmentAccountAssetClassPositionAllocationService = investmentAccountAssetClassPositionAllocationService;
	}


	public TradeAssetClassService getTradeAssetClassService() {
		return this.tradeAssetClassService;
	}


	public void setTradeAssetClassService(TradeAssetClassService tradeAssetClassService) {
		this.tradeAssetClassService = tradeAssetClassService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}
}
