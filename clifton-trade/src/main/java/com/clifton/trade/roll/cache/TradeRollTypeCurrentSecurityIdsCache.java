package com.clifton.trade.roll.cache;

import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public interface TradeRollTypeCurrentSecurityIdsCache {


	public Integer[] getCurrentSecurityIdsForTradeRollType(short tradeRollTypeId, Date activeOnDate);


	public void storeCurrentSecurityIdsForTradeRollType(short tradeRollTypeId, Date activeOnDate, List<InvestmentSecurity> securityList);
}
