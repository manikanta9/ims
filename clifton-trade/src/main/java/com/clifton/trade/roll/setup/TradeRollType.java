package com.clifton.trade.roll.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;


/**
 * The <code>TradeRollType</code> ...
 *
 * @author manderson
 */
@CacheByName
public class TradeRollType extends NamedEntity<Short> {

	private InvestmentSecurityGroup currentSecuritiesGroup;

	private InvestmentType investmentType;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityGroup getCurrentSecuritiesGroup() {
		return this.currentSecuritiesGroup;
	}


	public void setCurrentSecuritiesGroup(InvestmentSecurityGroup currentSecuritiesGroup) {
		this.currentSecuritiesGroup = currentSecuritiesGroup;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}
}
