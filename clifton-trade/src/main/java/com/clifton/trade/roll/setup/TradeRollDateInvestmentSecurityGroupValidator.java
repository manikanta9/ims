package com.clifton.trade.roll.setup;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>AccountingTransactionInvestmentSecurityGroupValidator</code> checks to make sure {@link InvestmentSecurityGroup#rebuildSystemBean}s are not removed when the
 * {@link InvestmentSecurityGroup} is referenced by a {@link TradeRollDate}.
 *
 * @author michaelm
 */
@Component
public class TradeRollDateInvestmentSecurityGroupValidator extends SelfRegisteringDaoValidator<InvestmentSecurityGroup> {

	private TradeRollSetupService tradeRollSetupService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE_OR_DELETE;
	}


	@Override
	public void validate(InvestmentSecurityGroup bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		if (config.isUpdate()) {
			if (bean.getRebuildSystemBean() == null) {
				TradeRollType tradeRollType = CollectionUtils.getFirstElement(getTradeRollSetupService().getTradeRollTypeListBySecurityGroupId(bean.getId()));
				if (tradeRollType != null) {
					throw new ValidationException(String.format("Cannot update Investment Security Group [%s] because it is referenced by Trade Roll Type [%s] which requires a Rebuild Bean.", bean.getLabel(), tradeRollType.getLabel()));
				}
			}
		}
		else {
			TradeRollType tradeRollType = CollectionUtils.getFirstElement(getTradeRollSetupService().getTradeRollTypeListBySecurityGroupId(bean.getId()));
			if (tradeRollType != null) {
				throw new ValidationException(String.format("Cannot delete Investment Security Group [%s] because it is being used by Trade Roll Type [%s].", bean.getLabel(), tradeRollType.getLabel()));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRollSetupService getTradeRollSetupService() {
		return this.tradeRollSetupService;
	}


	public void setTradeRollSetupService(TradeRollSetupService tradeRollSetupService) {
		this.tradeRollSetupService = tradeRollSetupService;
	}
}
