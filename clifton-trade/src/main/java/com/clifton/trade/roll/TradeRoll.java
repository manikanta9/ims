package com.clifton.trade.roll;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroup.TradeGroupActions;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>TradeSecurityRoll</code> ...
 *
 * @author Mary Anderson
 */
@NonPersistentObject
public class TradeRoll extends BaseEntity<Integer> {

	// Used As Filters to Load Positions
	private InvestmentSecurity security;
	private BusinessCompany holdingCompany;
	private boolean longPositions;
	// Allows the trades of this roll to be created as Block Trades. A Block Trade is a privately negotiated futures, options or combination transaction
	// that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.
	private boolean createBlockTrades;

	// Used for Trade Generation
	private InvestmentSecurity newSecurity;
	private Date tradeDate;
	private Date settlementDate;
	private SecurityUser traderUser;
	private BusinessCompany executingBrokerCompany;

	// Not Saved on the Roll or Tail, but set on each individual trade created.
	private TradeDestination tradeDestination;

	// Positions that match above filters and where quantities are set, appropriate Trades will be created
	List<TradeRollPosition> positionList;

	// Set for Trading on the backend. Stored here for convenience
	private TradeType tradeType;

	// Used during Roll Trade Review
	private TradeGroup rollTradeGroup;
	private TradeGroup tailTradeGroup;

	private TradeGroupActions tradeGroupAction;

	// Used during Fill Process
	private BigDecimal fillQuantity;
	private BigDecimal fromSecurityPrice;
	private BigDecimal toSecurityPrice;


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public List<TradeRollPosition> getPositionList() {
		return this.positionList;
	}


	public void setPositionList(List<TradeRollPosition> positionList) {
		this.positionList = positionList;
	}


	public InvestmentSecurity getNewSecurity() {
		return this.newSecurity;
	}


	public void setNewSecurity(InvestmentSecurity newSecurity) {
		this.newSecurity = newSecurity;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public boolean isLongPositions() {
		return this.longPositions;
	}


	public void setLongPositions(boolean longPositions) {
		this.longPositions = longPositions;
	}


	public boolean isCreateBlockTrades() {
		return this.createBlockTrades;
	}


	public void setCreateBlockTrades(boolean createBlockTrades) {
		this.createBlockTrades = createBlockTrades;
	}


	public SecurityUser getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public BusinessCompany getHoldingCompany() {
		return this.holdingCompany;
	}


	public void setHoldingCompany(BusinessCompany holdingCompany) {
		this.holdingCompany = holdingCompany;
	}


	public TradeGroup getRollTradeGroup() {
		return this.rollTradeGroup;
	}


	public void setRollTradeGroup(TradeGroup rollTradeGroup) {
		this.rollTradeGroup = rollTradeGroup;
	}


	public TradeGroup getTailTradeGroup() {
		return this.tailTradeGroup;
	}


	public void setTailTradeGroup(TradeGroup tailTradeGroup) {
		this.tailTradeGroup = tailTradeGroup;
	}


	public BigDecimal getFromSecurityPrice() {
		return this.fromSecurityPrice;
	}


	public void setFromSecurityPrice(BigDecimal fromSecurityPrice) {
		this.fromSecurityPrice = fromSecurityPrice;
	}


	public BigDecimal getToSecurityPrice() {
		return this.toSecurityPrice;
	}


	public void setToSecurityPrice(BigDecimal toSecurityPrice) {
		this.toSecurityPrice = toSecurityPrice;
	}


	public TradeGroupActions getTradeGroupAction() {
		return this.tradeGroupAction;
	}


	public void setTradeGroupAction(TradeGroupActions tradeGroupAction) {
		this.tradeGroupAction = tradeGroupAction;
	}


	public BigDecimal getFillQuantity() {
		return this.fillQuantity;
	}


	public void setFillQuantity(BigDecimal fillQuantity) {
		this.fillQuantity = fillQuantity;
	}
}
