package com.clifton.trade.roll.setup;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * Validator for {@link TradeRollType}s.
 *
 * @author michaelm
 */
@Component
public class TradeRollTypeValidator extends SelfRegisteringDaoValidator<TradeRollType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(TradeRollType bean, DaoEventTypes config) throws ValidationException {
		if (bean.getCurrentSecuritiesGroup() != null) {
			ValidationUtils.assertNotNull(bean.getCurrentSecuritiesGroup().getRebuildSystemBean(), "Trade Roll Types can only reference Investment Security Groups that have a Rebuild System Bean.");
		}
	}
}
