package com.clifton.trade.roll.calculator;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.current.BaseInvestmentCurrentSecurityCalculator;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.trade.roll.setup.TradeRollSetupService;
import com.clifton.trade.roll.setup.TradeRollType;

import java.util.Date;
import java.util.List;


/**
 * The <code>TradeRollDateCurrentSecurityCalculator</code> ...
 *
 * @author manderson
 */
public class TradeRollDateCurrentSecurityCalculator extends BaseInvestmentCurrentSecurityCalculator {

	private Short rollTypeId;

	private TradeRollSetupService tradeRollSetupService;


	@Override
	protected void validateImpl(InvestmentInstrument instrument, InvestmentSecurityGroup securityGroup) {
		ValidationUtils.assertNull(securityGroup, "Security group selection is not allowed for the Roll Schedule calculator - doesn't apply.");
		ValidationUtils.assertNotNull(instrument, "Instrument selection is required.");
		TradeRollType trt = getTradeRollSetupService().getTradeRollType(getRollTypeId());
		if (trt.getInvestmentType() != null) {
			ValidationUtils.assertTrue(InvestmentUtils.isInstrumentOfType(instrument, trt.getInvestmentType().getName()), "Trade Roll Type [" + trt.getName() + "] allows only ["
					+ trt.getInvestmentType().getName() + "] type instrument selections");
		}
	}


	@Override
	public InvestmentSecurity calculateImpl(InvestmentInstrument instrument, @SuppressWarnings("unused") InvestmentSecurityGroup securityGroup, InvestmentSecurity underlyingSecurity, Date date) {
		if (getRollTypeId() == null) {
			throw new ValidationException("Trade Roll Type selection is required.");
		}

		List<InvestmentSecurity> list = null;
		TradeRollType trt = getTradeRollSetupService().getTradeRollType(getRollTypeId());

		boolean currentGroupChecked = false;

		Integer underlyingSecurityId = underlyingSecurity != null ? underlyingSecurity.getId() : null;
		if (trt.getCurrentSecuritiesGroup() != null && DateUtils.compare(new Date(), date, false) == 0) {
			currentGroupChecked = true;
			list = getSecuritiesForInstrumentAndGroup(instrument.getId(), trt.getCurrentSecuritiesGroup().getId(), underlyingSecurityId, date);
		}

		// If list is empty - i.e. wasn't today or instrument not in group
		if (CollectionUtils.isEmpty(list)) {
			List<InvestmentSecurity> rollCurrentSecurityList = getTradeRollSetupService().getTradeRollTypeCurrentSecurityList(getRollTypeId(), date);
			list = BeanUtils.filter(rollCurrentSecurityList, InvestmentSecurity::getInstrument, instrument);
			if (CollectionUtils.isEmpty(list) && !currentGroupChecked) {
				list = getSecuritiesForInstrumentAndGroup(instrument.getId(), trt.getCurrentSecuritiesGroup().getId(), underlyingSecurityId, date);
			}
		}

		// Return the first one sorted by end date asc
		if (!CollectionUtils.isEmpty(list)) {
			return CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(list, InvestmentSecurity::getEndDate, true));
		}
		return null;
	}


	private List<InvestmentSecurity> getSecuritiesForInstrumentAndGroup(int instrumentId, short groupId, Integer underlyingSecurityId, Date date) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setSecurityGroupId(groupId);
		searchForm.setInstrumentId(instrumentId);
		ObjectUtils.doIfPresent(underlyingSecurityId, s -> searchForm.setUnderlyingSecurityId(underlyingSecurityId));
		searchForm.setActiveOnDate(date);
		return getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
	}


	public Short getRollTypeId() {
		return this.rollTypeId;
	}


	public void setRollTypeId(Short rollTypeId) {
		this.rollTypeId = rollTypeId;
	}


	public TradeRollSetupService getTradeRollSetupService() {
		return this.tradeRollSetupService;
	}


	public void setTradeRollSetupService(TradeRollSetupService tradeRollSetupService) {
		this.tradeRollSetupService = tradeRollSetupService;
	}
}
