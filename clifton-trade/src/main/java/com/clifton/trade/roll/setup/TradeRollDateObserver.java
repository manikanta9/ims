package com.clifton.trade.roll.setup;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeRollDateValidator</code>
 * <p/>
 * validated Start/End Dates
 * If new security is selected - it is the same instrument as the roll from security
 *
 * @author manderson
 */
@Component
public class TradeRollDateObserver extends SelfRegisteringDaoObserver<TradeRollDate> {

	private InvestmentSecurityGroupService investmentSecurityGroupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<TradeRollDate> dao, @SuppressWarnings("unused") DaoEventTypes event, TradeRollDate bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}

		ValidationUtils.assertNotNull(bean.getRollType(), "Roll Type is required", "rollType.id");
		ValidationUtils.assertNotNull(bean.getRollFromSecurity(), "Roll From Security is required", "rollFromSecurity.id");

		ValidationUtils.assertFalse(bean.getRollFromSecurity().getInstrument().getHierarchy().isOneSecurityPerInstrument(),
				"Roll Dates can only be defined for instruments that are one to many (i.e. NOT one security per instrument).");
		if (bean.getRollType().getInvestmentType() != null) {
			ValidationUtils.assertTrue(bean.getRollType().getInvestmentType().equals(bean.getRollFromSecurity().getInstrument().getHierarchy().getInvestmentType()), "Roll Type ["
					+ bean.getRollType().getName() + "] allows only [" + bean.getRollType().getInvestmentType().getName() + "] securities selected.  Security ["
					+ bean.getRollFromSecurity().getSymbol() + " is of type [" + bean.getRollFromSecurity().getInstrument().getHierarchy().getInvestmentType().getName() + "].");
		}

		ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
		if (bean.getDefaultNewSecurity() != null) {
			ValidationUtils.assertTrue(bean.getRollFromSecurity().getInstrument().equals(bean.getDefaultNewSecurity().getInstrument()), "Roll From Security [" + bean.getRollFromSecurity().getSymbol()
					+ "] belongs to instrument [" + bean.getRollFromSecurity().getInstrument().getLabelShort() + "], but Default New Security [" + bean.getDefaultNewSecurity().getSymbol()
					+ "] belongs to instrument [" + bean.getDefaultNewSecurity().getInstrument().getLabelShort() + "].  Both securities must belong to the same instrument.", "defaultNewSecurity.id");
			ValidationUtils.assertTrue(InvestmentUtils.isSecurityActiveOn(bean.getDefaultNewSecurity(), bean.getStartDate()),
					"Default New Security is invalid because it is not active on the Roll Start date.");
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<TradeRollDate> dao, @SuppressWarnings("unused") DaoEventTypes event, TradeRollDate bean, Throwable e) {
		if (e == null && bean.getRollType().getCurrentSecuritiesGroup() != null && bean.getDefaultNewSecurity() != null && DateUtils.isCurrentDateBetween(bean.getStartDate(), bean.getEndDate(), false)) {
			getInvestmentSecurityGroupService().rebuildInvestmentSecurityGroup(bean.getRollType().getCurrentSecuritiesGroup().getId());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}
}
