package com.clifton.trade.roll;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;


/**
 * The <code>TradeRollService</code> ...
 *
 * @author Mary Anderson
 */
public interface TradeRollService {

	//////////////////////////////////////////////////////////////
	///////              Trade Roll Entry                   //////
	//////////////////////////////////////////////////////////////


	/**
	 * Loads TradeRoll for viewing current positions for a security
	 * for Security Roll
	 *
	 * @param roll
	 */
	@SecureMethod(dtoClass = Trade.class)
	public TradeRoll getTradeRollEntry(TradeRoll roll);


	/**
	 * Saves Trades for Tails & Rolls for a Security Roll
	 *
	 * @param roll
	 */
	@SecureMethod(dtoClass = Trade.class)
	public void saveTradeRollEntry(TradeRoll roll);


	//////////////////////////////////////////////////////////////
	///////              Roll Trade Group                   //////
	//////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = Trade.class)
	public TradeRoll getTradeRollGroup(int id);


	/**
	 * Executes an action on a TradeRollGroup for selected rolls
	 * Cancel, Approve, Fill
	 *
	 * @param bean
	 */
	@SecureMethod(dtoClass = Trade.class, permissionResolverBeanName = "tradeGroupActionSecureMethodPermissionResolver")
	public void saveTradeRollGroup(TradeRoll bean);
}
