package com.clifton.trade;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * <code>TradeSource</code> defines a source that can be added to a {@link Trade} defining where the trade originated.
 * The source adds additional context to a trade that can change workflow and/or processing behavior applied to the trade.
 *
 * @author NickK
 */
@CacheByName
public class TradeSource extends NamedEntity<Short> {

	/**
	 * Flag that is used to bypass dual approval in the trade workflow because there is appropriate validation done client-side, server-side, and via trade/compliance rules.
	 */
	private boolean bypassDualApprovalAllowed;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isBypassDualApprovalAllowed() {
		return this.bypassDualApprovalAllowed;
	}


	public void setBypassDualApprovalAllowed(boolean bypassDualApprovalAllowed) {
		this.bypassDualApprovalAllowed = bypassDualApprovalAllowed;
	}
}
