package com.clifton.trade.destination.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;


public class TradeDestinationMappingSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(leftJoin = true, searchField = "executingBrokerCompany.name,tradeType.name,tradeDestination.name")
	private String searchPattern;


	@SearchField(searchField = "executingBrokerCompany.id")
	private Integer executingBrokerCompanyId;

	@SearchField(searchField = "executingBrokerCompany.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] executingBrokerCompanyIdList;

	@SearchField(searchField = "name,alias", searchFieldPath = "executingBrokerCompany", sortField = "name")
	private String executingBrokerCompanyName;

	@SearchField(searchField = "executingBrokerCompany.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean executingBrokerIsNull;

	@SearchField(searchField = "executingBrokerCompany.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean executingBrokerIsNotNull;


	@SearchField(searchField = "tradeType.id")
	private Short tradeTypeId;

	@SearchField(searchField = "name", searchFieldPath = "tradeType")
	private String tradeTypeName;

	@SearchField(searchField = "name", searchFieldPath = "tradeType", comparisonConditions = ComparisonConditions.EQUALS)
	private String tradeTypeNameEquals;

	@SearchField(searchFieldPath = "tradeType", searchField = "investmentType.id")
	private Short investmentTypeId;


	@SearchField(searchField = "tradeDestination.id")
	private Short tradeDestinationId;

	@SearchField(searchField = "name", searchFieldPath = "tradeDestination")
	private String tradeDestinationName;

	@SearchField(searchField = "destinationOrder", searchFieldPath = "tradeDestination")
	private Integer tradeDestinationOrder;

	@SearchField(searchField = "connection.id", searchFieldPath = "tradeDestination")
	private Short tradeDestinationConnectionId;

	@SearchField(searchField = "overrideConnection.id,tradeDestination.connection.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Short coalesceOverrideConnectionId;


	@SearchField(searchField = "defaultInvestmentGroup.id", sortField = "defaultInvestmentGroup.name")
	private Short defaultInvestmentGroupId;

	@SearchField(searchField = "mappingConfiguration.id")
	private Integer mappingConfigurationId;

	@SearchField
	private Boolean defaultDestinationMapping;

	@SearchField
	private String overrideBrokerCode;

	@SearchField
	private String overrideAllocationBrokerCode;

	@SearchField
	private String fixPostTradeAllocationTopAccountNumber;

	@SearchField(searchField = "orderAccountMappingPurpose.id")
	private Short orderAccountMappingPurposeId;

	@SearchField(searchField = "allocationAccountMappingPurpose.id")
	private Short allocationAccountMappingPurposeId;

	@SearchField
	private String fixGlobalTradeNote;


	// Mapping Configuration Search Fields


	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixPostTradeAllocation;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixDisablePreTradeAllocations;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixTradeGroupingAllowed;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixTradeGroupingByHoldingAccount;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixExecutingAccountUsed;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixAllocationReportExpected;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private CommissionTypes fixCommissionType;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private ProcessCodes fixProcessCode;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private BigDecimal fixDefaultCommissionPerUnit;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private AllocationTypes fixAllocationType;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixAllocationCompleteWhenStatusIsReceived;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixMICExchangeCodeRequiredOnAllocationMessages;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixSecondaryClientOrderIdUsedForAllocations;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixSendTopAccountOnAllocationMessages;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixMICExchangeCodeRequiredOnOrderMessages;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixSendBrokerPartyIdsOnAllocationMessages;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean sendClientAccountWithOrder;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean sendRestrictiveBrokerList;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean brokerRestrictedToRelatedHoldingAccountList;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean searchCompanyHierarchyForBrokerMatch;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixUseSymbolForEquityOrders;

	@SearchField(searchFieldPath = "mappingConfiguration")
	private Boolean fixSendClearingFirmLEIWithAllocation;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getExecutingBrokerCompanyId() {
		return this.executingBrokerCompanyId;
	}


	public void setExecutingBrokerCompanyId(Integer executingBrokerCompanyId) {
		this.executingBrokerCompanyId = executingBrokerCompanyId;
	}


	public Integer[] getExecutingBrokerCompanyIdList() {
		return this.executingBrokerCompanyIdList;
	}


	public void setExecutingBrokerCompanyIdList(Integer[] executingBrokerCompanyIdList) {
		this.executingBrokerCompanyIdList = executingBrokerCompanyIdList;
	}


	public String getExecutingBrokerCompanyName() {
		return this.executingBrokerCompanyName;
	}


	public void setExecutingBrokerCompanyName(String executingBrokerCompanyName) {
		this.executingBrokerCompanyName = executingBrokerCompanyName;
	}


	public Boolean getExecutingBrokerIsNull() {
		return this.executingBrokerIsNull;
	}


	public void setExecutingBrokerIsNull(Boolean executingBrokerIsNull) {
		this.executingBrokerIsNull = executingBrokerIsNull;
	}


	public Boolean getExecutingBrokerIsNotNull() {
		return this.executingBrokerIsNotNull;
	}


	public void setExecutingBrokerIsNotNull(Boolean executingBrokerIsNotNull) {
		this.executingBrokerIsNotNull = executingBrokerIsNotNull;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public String getTradeTypeName() {
		return this.tradeTypeName;
	}


	public void setTradeTypeName(String tradeTypeName) {
		this.tradeTypeName = tradeTypeName;
	}


	public String getTradeTypeNameEquals() {
		return this.tradeTypeNameEquals;
	}


	public void setTradeTypeNameEquals(String tradeTypeNameEquals) {
		this.tradeTypeNameEquals = tradeTypeNameEquals;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getTradeDestinationId() {
		return this.tradeDestinationId;
	}


	public void setTradeDestinationId(Short tradeDestinationId) {
		this.tradeDestinationId = tradeDestinationId;
	}


	public String getTradeDestinationName() {
		return this.tradeDestinationName;
	}


	public void setTradeDestinationName(String tradeDestinationName) {
		this.tradeDestinationName = tradeDestinationName;
	}


	public Integer getTradeDestinationOrder() {
		return this.tradeDestinationOrder;
	}


	public void setTradeDestinationOrder(Integer tradeDestinationOrder) {
		this.tradeDestinationOrder = tradeDestinationOrder;
	}


	public Short getTradeDestinationConnectionId() {
		return this.tradeDestinationConnectionId;
	}


	public void setTradeDestinationConnectionId(Short tradeDestinationConnectionId) {
		this.tradeDestinationConnectionId = tradeDestinationConnectionId;
	}


	public Short getCoalesceOverrideConnectionId() {
		return this.coalesceOverrideConnectionId;
	}


	public void setCoalesceOverrideConnectionId(Short coalesceOverrideConnectionId) {
		this.coalesceOverrideConnectionId = coalesceOverrideConnectionId;
	}


	public Short getDefaultInvestmentGroupId() {
		return this.defaultInvestmentGroupId;
	}


	public void setDefaultInvestmentGroupId(Short defaultInvestmentGroupId) {
		this.defaultInvestmentGroupId = defaultInvestmentGroupId;
	}


	public Integer getMappingConfigurationId() {
		return this.mappingConfigurationId;
	}


	public void setMappingConfigurationId(Integer mappingConfigurationId) {
		this.mappingConfigurationId = mappingConfigurationId;
	}


	public Boolean getDefaultDestinationMapping() {
		return this.defaultDestinationMapping;
	}


	public void setDefaultDestinationMapping(Boolean defaultDestinationMapping) {
		this.defaultDestinationMapping = defaultDestinationMapping;
	}


	public String getOverrideBrokerCode() {
		return this.overrideBrokerCode;
	}


	public void setOverrideBrokerCode(String overrideBrokerCode) {
		this.overrideBrokerCode = overrideBrokerCode;
	}


	public String getOverrideAllocationBrokerCode() {
		return this.overrideAllocationBrokerCode;
	}


	public void setOverrideAllocationBrokerCode(String overrideAllocationBrokerCode) {
		this.overrideAllocationBrokerCode = overrideAllocationBrokerCode;
	}


	public String getFixPostTradeAllocationTopAccountNumber() {
		return this.fixPostTradeAllocationTopAccountNumber;
	}


	public void setFixPostTradeAllocationTopAccountNumber(String fixPostTradeAllocationTopAccountNumber) {
		this.fixPostTradeAllocationTopAccountNumber = fixPostTradeAllocationTopAccountNumber;
	}


	public Short getOrderAccountMappingPurposeId() {
		return this.orderAccountMappingPurposeId;
	}


	public void setOrderAccountMappingPurposeId(Short orderAccountMappingPurposeId) {
		this.orderAccountMappingPurposeId = orderAccountMappingPurposeId;
	}


	public Short getAllocationAccountMappingPurposeId() {
		return this.allocationAccountMappingPurposeId;
	}


	public void setAllocationAccountMappingPurposeId(Short allocationAccountMappingPurposeId) {
		this.allocationAccountMappingPurposeId = allocationAccountMappingPurposeId;
	}


	public String getFixGlobalTradeNote() {
		return this.fixGlobalTradeNote;
	}


	public void setFixGlobalTradeNote(String fixGlobalTradeNote) {
		this.fixGlobalTradeNote = fixGlobalTradeNote;
	}


	public Boolean getFixPostTradeAllocation() {
		return this.fixPostTradeAllocation;
	}


	public void setFixPostTradeAllocation(Boolean fixPostTradeAllocation) {
		this.fixPostTradeAllocation = fixPostTradeAllocation;
	}


	public Boolean getFixDisablePreTradeAllocations() {
		return this.fixDisablePreTradeAllocations;
	}


	public void setFixDisablePreTradeAllocations(Boolean fixDisablePreTradeAllocations) {
		this.fixDisablePreTradeAllocations = fixDisablePreTradeAllocations;
	}


	public Boolean getFixTradeGroupingAllowed() {
		return this.fixTradeGroupingAllowed;
	}


	public void setFixTradeGroupingAllowed(Boolean fixTradeGroupingAllowed) {
		this.fixTradeGroupingAllowed = fixTradeGroupingAllowed;
	}


	public Boolean getFixTradeGroupingByHoldingAccount() {
		return this.fixTradeGroupingByHoldingAccount;
	}


	public void setFixTradeGroupingByHoldingAccount(Boolean fixTradeGroupingByHoldingAccount) {
		this.fixTradeGroupingByHoldingAccount = fixTradeGroupingByHoldingAccount;
	}


	public Boolean getFixExecutingAccountUsed() {
		return this.fixExecutingAccountUsed;
	}


	public void setFixExecutingAccountUsed(Boolean fixExecutingAccountUsed) {
		this.fixExecutingAccountUsed = fixExecutingAccountUsed;
	}


	public Boolean getFixAllocationReportExpected() {
		return this.fixAllocationReportExpected;
	}


	public void setFixAllocationReportExpected(Boolean fixAllocationReportExpected) {
		this.fixAllocationReportExpected = fixAllocationReportExpected;
	}


	public CommissionTypes getFixCommissionType() {
		return this.fixCommissionType;
	}


	public void setFixCommissionType(CommissionTypes fixCommissionType) {
		this.fixCommissionType = fixCommissionType;
	}


	public ProcessCodes getFixProcessCode() {
		return this.fixProcessCode;
	}


	public void setFixProcessCode(ProcessCodes fixProcessCode) {
		this.fixProcessCode = fixProcessCode;
	}


	public BigDecimal getFixDefaultCommissionPerUnit() {
		return this.fixDefaultCommissionPerUnit;
	}


	public void setFixDefaultCommissionPerUnit(BigDecimal fixDefaultCommissionPerUnit) {
		this.fixDefaultCommissionPerUnit = fixDefaultCommissionPerUnit;
	}


	public AllocationTypes getFixAllocationType() {
		return this.fixAllocationType;
	}


	public void setFixAllocationType(AllocationTypes fixAllocationType) {
		this.fixAllocationType = fixAllocationType;
	}


	public Boolean getFixAllocationCompleteWhenStatusIsReceived() {
		return this.fixAllocationCompleteWhenStatusIsReceived;
	}


	public void setFixAllocationCompleteWhenStatusIsReceived(Boolean fixAllocationCompleteWhenStatusIsReceived) {
		this.fixAllocationCompleteWhenStatusIsReceived = fixAllocationCompleteWhenStatusIsReceived;
	}


	public Boolean getFixMICExchangeCodeRequiredOnAllocationMessages() {
		return this.fixMICExchangeCodeRequiredOnAllocationMessages;
	}


	public void setFixMICExchangeCodeRequiredOnAllocationMessages(Boolean fixMICExchangeCodeRequiredOnAllocationMessages) {
		this.fixMICExchangeCodeRequiredOnAllocationMessages = fixMICExchangeCodeRequiredOnAllocationMessages;
	}


	public Boolean getFixSecondaryClientOrderIdUsedForAllocations() {
		return this.fixSecondaryClientOrderIdUsedForAllocations;
	}


	public void setFixSecondaryClientOrderIdUsedForAllocations(Boolean fixSecondaryClientOrderIdUsedForAllocations) {
		this.fixSecondaryClientOrderIdUsedForAllocations = fixSecondaryClientOrderIdUsedForAllocations;
	}


	public Boolean getFixSendTopAccountOnAllocationMessages() {
		return this.fixSendTopAccountOnAllocationMessages;
	}


	public void setFixSendTopAccountOnAllocationMessages(Boolean fixSendTopAccountOnAllocationMessages) {
		this.fixSendTopAccountOnAllocationMessages = fixSendTopAccountOnAllocationMessages;
	}


	public Boolean getFixMICExchangeCodeRequiredOnOrderMessages() {
		return this.fixMICExchangeCodeRequiredOnOrderMessages;
	}


	public void setFixMICExchangeCodeRequiredOnOrderMessages(Boolean fixMICExchangeCodeRequiredOnOrderMessages) {
		this.fixMICExchangeCodeRequiredOnOrderMessages = fixMICExchangeCodeRequiredOnOrderMessages;
	}


	public Boolean getFixSendBrokerPartyIdsOnAllocationMessages() {
		return this.fixSendBrokerPartyIdsOnAllocationMessages;
	}


	public void setFixSendBrokerPartyIdsOnAllocationMessages(Boolean fixSendBrokerPartyIdsOnAllocationMessages) {
		this.fixSendBrokerPartyIdsOnAllocationMessages = fixSendBrokerPartyIdsOnAllocationMessages;
	}


	public Boolean getSendClientAccountWithOrder() {
		return this.sendClientAccountWithOrder;
	}


	public void setSendClientAccountWithOrder(Boolean sendClientAccountWithOrder) {
		this.sendClientAccountWithOrder = sendClientAccountWithOrder;
	}


	public Boolean getSendRestrictiveBrokerList() {
		return this.sendRestrictiveBrokerList;
	}


	public void setSendRestrictiveBrokerList(Boolean sendRestrictiveBrokerList) {
		this.sendRestrictiveBrokerList = sendRestrictiveBrokerList;
	}


	public Boolean getBrokerRestrictedToRelatedHoldingAccountList() {
		return this.brokerRestrictedToRelatedHoldingAccountList;
	}


	public void setBrokerRestrictedToRelatedHoldingAccountList(Boolean brokerRestrictedToRelatedHoldingAccountList) {
		this.brokerRestrictedToRelatedHoldingAccountList = brokerRestrictedToRelatedHoldingAccountList;
	}


	public Boolean getSearchCompanyHierarchyForBrokerMatch() {
		return this.searchCompanyHierarchyForBrokerMatch;
	}


	public void setSearchCompanyHierarchyForBrokerMatch(Boolean searchCompanyHierarchyForBrokerMatch) {
		this.searchCompanyHierarchyForBrokerMatch = searchCompanyHierarchyForBrokerMatch;
	}


	public Boolean getFixUseSymbolForEquityOrders() {
		return this.fixUseSymbolForEquityOrders;
	}


	public void setFixUseSymbolForEquityOrders(Boolean fixUseSymbolForEquityOrders) {
		this.fixUseSymbolForEquityOrders = fixUseSymbolForEquityOrders;
	}


	public Boolean getFixSendClearingFirmLEIWithAllocation() {
		return this.fixSendClearingFirmLEIWithAllocation;
	}


	public void setFixSendClearingFirmLEIWithAllocation(Boolean fixSendClearingFirmLEIWithAllocation) {
		this.fixSendClearingFirmLEIWithAllocation = fixSendClearingFirmLEIWithAllocation;
	}
}
