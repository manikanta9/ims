package com.clifton.trade.destination.connection;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>TradeDestinationConnection</code> defines the properties needed to send an order
 * to a destination.
 *
 * @author mwacker
 */
public class TradeDestinationConnection extends NamedEntity<Short> {


	private String fixOnBehalfOfCompId; // company id for allocation messages


	private TradeDestinationConnection allocationConnection;

	/**
	 * The name of the fix destination; this will be used by the fix application to look up the session information.
	 */
	private String fixDestinationName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationConnection getAllocationConnection() {
		return this.allocationConnection;
	}


	public void setAllocationConnection(TradeDestinationConnection allocationConnection) {
		this.allocationConnection = allocationConnection;
	}


	public String getFixOnBehalfOfCompId() {
		return this.fixOnBehalfOfCompId;
	}


	public void setFixOnBehalfOfCompId(String fixOnBehalfOfCompId) {
		this.fixOnBehalfOfCompId = fixOnBehalfOfCompId;
	}


	public String getFixDestinationName() {
		return this.fixDestinationName;
	}


	public void setFixDestinationName(String fixDestinationName) {
		this.fixDestinationName = fixDestinationName;
	}
}
