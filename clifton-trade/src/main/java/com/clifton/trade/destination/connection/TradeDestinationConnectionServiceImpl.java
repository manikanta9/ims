package com.clifton.trade.destination.connection;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TradeDestinationConnectionServiceImpl implements TradeDestinationConnectionService {

	private AdvancedUpdatableDAO<TradeDestinationConnection, Criteria> tradeDestinationConnectionDAO;


	////////////////////////////////////////////////////////////////////////////
	//////         Trade Destination Connection Methods                  /////// 
	////////////////////////////////////////////////////////////////////////////
	@Override
	public TradeDestinationConnection getTradeDestinationConnection(short id) {
		return getTradeDestinationConnectionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeDestinationConnection> getTradeDestinationConnectionList(TradeDestinationConnectionSearchForm searchForm) {
		return getTradeDestinationConnectionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeDestinationConnection saveTradeDestinationConnection(TradeDestinationConnection bean) {
		return getTradeDestinationConnectionDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public AdvancedUpdatableDAO<TradeDestinationConnection, Criteria> getTradeDestinationConnectionDAO() {
		return this.tradeDestinationConnectionDAO;
	}


	public void setTradeDestinationConnectionDAO(AdvancedUpdatableDAO<TradeDestinationConnection, Criteria> tradeDestinationConnectionDAO) {
		this.tradeDestinationConnectionDAO = tradeDestinationConnectionDAO;
	}
}
