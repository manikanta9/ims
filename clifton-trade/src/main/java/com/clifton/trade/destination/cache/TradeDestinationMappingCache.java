package com.clifton.trade.destination.cache;

import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;

import java.util.List;


/**
 * The <code>TradeDestinationMappingCacheImpl</code> implements a cache for TradeDestinationMapping object.  This
 * cache essentially implements 2 separate caches.  One cache to look up mappings during trade creation, and one to look
 * up mappings based of a broker code, which is typically used during FIX order execution to find a mapping from a code received
 * in a FIX message.
 *
 * @author mwacker
 */
public interface TradeDestinationMappingCache {


	public List<TradeDestinationMapping> getTradeDestinationMappingList(TradeDestinationMappingSearchCommand command);
}
