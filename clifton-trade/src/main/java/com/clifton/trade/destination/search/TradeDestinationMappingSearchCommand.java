package com.clifton.trade.destination.search;


import com.clifton.core.util.StringUtils;
import com.clifton.trade.Trade;

import java.util.Date;


/**
 * The <code>TradeDestinationMappingSearchCommand</code> defines a custom search command.  This is used to search the
 * TradeDestinationMappingCache.
 *
 * @author mwacker
 */
public class TradeDestinationMappingSearchCommand extends TradeDestinationMappingSearchForm {

	/**
	 * The trade whose properties will be used to find a TradeDestinationMapping
	 */
	private Trade trade;

	/**
	 * The broker code used to find the correct TradeDestinationMapping.
	 * <p/>
	 * NOTE:  This will filter on overrideBrokerCode on the TradeDestinationMapping or the MarketDataSourceCompanyMapping.externalCompanyName looked
	 * up using the dataSource from the tradeDestinationConnection.
	 */
	private String brokerCode;

	/**
	 * Provides a security used to get a trade type, and not restricts the result to securities in the defaultInvestmentGroup's
	 */
	private Integer securityId;

	/**
	 * Provides a security used to get a trade type, but does not restrict the result to securities in the defaultInvestmentGroup's
	 */
	private Integer securityIdForTradeType;

	/**
	 * A flag used to return all results in a PagingArrayList instead of a list.
	 */
	private boolean usePagingArray = true;

	/**
	 *
	 */
	private Integer clientInvestmentAccountId;


	public TradeDestinationMappingSearchCommand(Trade trade) {
		this.trade = trade;

		setTradeTypeId(trade.getTradeType() != null ? trade.getTradeType().getId() : null);
		setTradeDestinationId(trade.getTradeDestination() != null ? trade.getTradeDestination().getId() : null);
		if (trade.getExecutingBrokerCompany() != null && !trade.getExecutingBrokerCompany().isNewBean()) {
			setExecutingBrokerCompanyId(trade.getExecutingBrokerCompany().getId());
		}
		else {
			setExecutingBrokerIsNull(true);
		}
		setActiveOnDate(trade.getTradeDate());
	}


	public TradeDestinationMappingSearchCommand(Short tradeTypeId, Boolean defaultDestinationMapping, Date activeOnDate) {
		setTradeTypeId(tradeTypeId);
		setDefaultDestinationMapping(defaultDestinationMapping);
		setActiveOnDate(activeOnDate);
	}


	public TradeDestinationMappingSearchCommand(Integer securityId, Integer executingBrokerCompanyId, Date activeOnDate) {
		setSecurityId(securityId);
		setExecutingBrokerCompanyId(executingBrokerCompanyId);
		setActiveOnDate(activeOnDate);
	}


	public TradeDestinationMappingSearchCommand() {
		//
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public boolean isBrokerFilter() {
		return !StringUtils.isEmpty(getBrokerCode());
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	public String getBrokerCode() {
		return this.brokerCode;
	}


	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer getSecurityIdForTradeType() {
		return this.securityIdForTradeType;
	}


	public void setSecurityIdForTradeType(Integer securityIdForTradeType) {
		this.securityIdForTradeType = securityIdForTradeType;
	}


	public boolean isUsePagingArray() {
		return this.usePagingArray;
	}


	public void setUsePagingArray(boolean usePagingArray) {
		this.usePagingArray = usePagingArray;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}
}
