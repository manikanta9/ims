package com.clifton.trade.destination;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurpose;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.connection.TradeDestinationConnection;

import java.util.Date;


/**
 * The <code>TradeDestinationMapping</code> class maps trade types to corresponding destinations by broker.
 * <p>
 * For example, if 6 brokers are allowed for "Manual" destination for "Futures" trades, then 6 mapping rows will be created.
 * If only 1 broker is used for trade tickets (Goldman Sachs), then only one row will be for "Trade Ticket" destination.
 * If any broker can be used for bonds, then executingBrokerCompany can be null to allow all.
 *
 * @author mwacker
 */
public class TradeDestinationMapping extends BaseEntity<Integer> implements LabeledObject {

	private TradeType tradeType;
	private TradeDestination tradeDestination;
	private BusinessCompany executingBrokerCompany; // optional (if not set, then any broker can be selected)
	private TradeDestinationMappingConfiguration mappingConfiguration;

	private InvestmentGroup defaultInvestmentGroup;
	private TradeDestinationConnection overrideConnection;
	private boolean defaultDestinationMapping;

	private Date startDate;
	private Date endDate;

	/**
	 * The account number sent in FIX tag 1 in post trade allocation instructions
	 */
	private String fixPostTradeAllocationTopAccountNumber;

	/**
	 * The broker code used to identify the broker to counter party on a FIX message.
	 */
	private String overrideBrokerCode;

	/**
	 * The broker code used to override the broker in the party list in AllocationInstruction message.
	 */
	private String overrideAllocationBrokerCode;

	/**
	 * For account to value mapping, this is the purpose for the order account mapping
	 */
	private InvestmentAccountMappingPurpose orderAccountMappingPurpose;

	/**
	 * For account to value mapping, this is the purpose for the allocation account mapping
	 */
	private InvestmentAccountMappingPurpose allocationAccountMappingPurpose;


	private String fixGlobalTradeNote;

	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		if (getTradeType() != null) {
			result.append(getTradeType().getName());
		}
		if (getTradeDestination() != null) {
			result.append(": ").append(getTradeDestination().getName());
		}
		if (getExecutingBrokerCompany() != null) {
			result.append(": ").append(getExecutingBrokerCompany().getName());
		}
		return result.toString();
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public TradeDestinationConnection getConnection() {
		return getOverrideConnection() != null ? getOverrideConnection() : getTradeDestination().getConnection();
	}


	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public boolean isDefaultDestinationMapping() {
		return this.defaultDestinationMapping;
	}


	public void setDefaultDestinationMapping(boolean defaultDestinationMapping) {
		this.defaultDestinationMapping = defaultDestinationMapping;
	}


	public InvestmentGroup getDefaultInvestmentGroup() {
		return this.defaultInvestmentGroup;
	}


	public void setDefaultInvestmentGroup(InvestmentGroup defaultInvestmentGroup) {
		this.defaultInvestmentGroup = defaultInvestmentGroup;
	}


	public TradeDestinationMappingConfiguration getMappingConfiguration() {
		return this.mappingConfiguration;
	}


	public void setMappingConfiguration(TradeDestinationMappingConfiguration mappingConfiguration) {
		this.mappingConfiguration = mappingConfiguration;
	}


	public TradeDestinationConnection getOverrideConnection() {
		return this.overrideConnection;
	}


	public void setOverrideConnection(TradeDestinationConnection overrideConnection) {
		this.overrideConnection = overrideConnection;
	}


	public String getOverrideBrokerCode() {
		return this.overrideBrokerCode;
	}


	public void setOverrideBrokerCode(String overrideBrokerCode) {
		this.overrideBrokerCode = overrideBrokerCode;
	}


	public String getOverrideAllocationBrokerCode() {
		return this.overrideAllocationBrokerCode;
	}


	public void setOverrideAllocationBrokerCode(String overrideAllocationBrokerCode) {
		this.overrideAllocationBrokerCode = overrideAllocationBrokerCode;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getFixPostTradeAllocationTopAccountNumber() {
		return this.fixPostTradeAllocationTopAccountNumber;
	}


	public void setFixPostTradeAllocationTopAccountNumber(String fixPostTradeAllocationTopAccountNumber) {
		this.fixPostTradeAllocationTopAccountNumber = fixPostTradeAllocationTopAccountNumber;
	}


	public InvestmentAccountMappingPurpose getOrderAccountMappingPurpose() {
		return this.orderAccountMappingPurpose;
	}


	public void setOrderAccountMappingPurpose(InvestmentAccountMappingPurpose orderAccountMappingPurpose) {
		this.orderAccountMappingPurpose = orderAccountMappingPurpose;
	}


	public InvestmentAccountMappingPurpose getAllocationAccountMappingPurpose() {
		return this.allocationAccountMappingPurpose;
	}


	public void setAllocationAccountMappingPurpose(InvestmentAccountMappingPurpose allocationAccountMappingPurpose) {
		this.allocationAccountMappingPurpose = allocationAccountMappingPurpose;
	}


	public String getFixGlobalTradeNote() {
		return this.fixGlobalTradeNote;
	}


	public void setFixGlobalTradeNote(String fixGlobalTradeNote) {
		this.fixGlobalTradeNote = fixGlobalTradeNote;
	}
}
