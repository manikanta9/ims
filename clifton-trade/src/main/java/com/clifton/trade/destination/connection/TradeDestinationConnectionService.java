package com.clifton.trade.destination.connection;


import java.util.List;


public interface TradeDestinationConnectionService {

	////////////////////////////////////////////////////////////////////////////
	//////         Trade Destination Connection Methods                  /////// 
	////////////////////////////////////////////////////////////////////////////
	public TradeDestinationConnection getTradeDestinationConnection(short id);


	public List<TradeDestinationConnection> getTradeDestinationConnectionList(TradeDestinationConnectionSearchForm searchForm);


	public TradeDestinationConnection saveTradeDestinationConnection(TradeDestinationConnection bean);
}
