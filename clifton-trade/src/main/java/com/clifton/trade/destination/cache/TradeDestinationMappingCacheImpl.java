package com.clifton.trade.destination.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMapping;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMappingSearchForm;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.trade.TradeService;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>TradeDestinationMappingCacheImpl</code> implements a cache for TradeDestinationMapping object.  This
 * cache essentially implements 2 separate caches.  One cache to look up mappings during trade creation, and one to look
 * up mappings based of a broker code, which is typically used during FIX order execution to find a mapping from a code received
 * in a FIX message.
 *
 * @author mwacker
 */
@Component
public class TradeDestinationMappingCacheImpl extends SelfRegisteringSimpleDaoCache<TradeDestinationMapping, String, Map<String, List<TradeDestinationMappingHolder>>> implements TradeDestinationMappingCache {

	/**
	 * Circular reference needed to build the cache.
	 */
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentGroupService investmentGroupService;
	private MarketDataSourceCompanyService marketDataSourceCompanyService;
	private TradeDestinationService tradeDestinationService;

	private static final String DESTINATION_MAP_CACHED_OBJECT_NAME = "TradeDestinationMappingHolderList";
	private static final String BROKER_DESTINATION_MAP_CACHED_OBJECT_NAME = "TradeDestinationMappingHolderBrokerList";

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return "TradeDestinationMappingCacheImpl";
	}


	@Override
	public List<TradeDestinationMapping> getTradeDestinationMappingList(TradeDestinationMappingSearchCommand command) {
		ValidationUtils.assertNotNull(command.getTradeTypeId(), "[tradeType] is required to look up [TradeDestinationMapping]s from cache.");

		List<TradeDestinationMappingHolder> holderList;
		// get the correct destination mapping list
		if (command.isBrokerFilter()) {
			// get the broker list
			holderList = getTradeDestinationMappingHolderBrokerList(command.getTradeTypeId(), command.getBrokerCode());
		}
		else {
			holderList = getTradeDestinationMappingHolderList(command.getTradeTypeId());
		}

		// run the filters provided by the command
		for (FilterValue filter : CollectionUtils.getIterable(getFilterListFromCommand(command))) {
			holderList = filterList(holderList, filter.getFieldName(), filter.getValue(), filter.isFilterNullValue());
			if (CollectionUtils.isEmpty(holderList)) {
				return new ArrayList<>();
			}
		}

		// additional OTC filter
		if (command.getClientInvestmentAccountId() != null && command.getSecurityId() != null && command.getActiveOnDate() != null) {
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(command.getSecurityId());
			InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
			if (hierarchy.isOtc()) {
				List<InvestmentAccountRelationship> allowedList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurpose(command.getClientInvestmentAccountId(),
						TradeService.TRADING_PURPOSE_NAME_PREFIX + hierarchy.getInvestmentType().getName(), command.getActiveOnDate(), false);
				if (CollectionUtils.isEmpty(allowedList)) {
					return new ArrayList<>();
				}
				Integer[] companyIds = BeanUtils.getPropertyValuesUniqueExcludeNull(allowedList, investmentAccountRelationship -> investmentAccountRelationship.getReferenceTwo().getIssuingCompany().getId(), Integer.class);
				holderList = BeanUtils.filter(holderList, tradeDestinationMappingHolder -> ArrayUtils.contains(companyIds, tradeDestinationMappingHolder.getExecutingBrokerCompanyId()));
			}
		}

		// get the actual TradeDestinationMapping objects using the cached holders
		List<TradeDestinationMapping> result = new ArrayList<>();
		for (TradeDestinationMappingHolder holder : CollectionUtils.getIterable(holderList)) {
			if (isMatch(command, holder)) {
				result.add(getTradeDestinationService().getTradeDestinationMapping(holder.getId()));
			}
		}

		int size = CollectionUtils.getSize(result);
		if (size > 1) {
			// sort the results
			if (!StringUtils.isEmpty(command.getOrderBy())) {
				List<String> sortPropertyNames = new ArrayList<>();
				List<Boolean> sortAscending = new ArrayList<>();
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(command.getOrderBy());
				for (OrderByField field : CollectionUtils.getIterable(orderByList)) {
					sortPropertyNames.add(field.getName());
					if (OrderByDirections.ASC == field.getDirection()) {
						sortAscending.add(true);
					}
					else {
						sortAscending.add(false);
					}
				}
				if (!CollectionUtils.isEmpty(sortPropertyNames)) {
					result = BeanUtils.sortWithPropertyNames(result, sortPropertyNames, sortAscending);
				}
			}
			else {
				// default sort is by group name and then by default
				result = BeanUtils.sortWithFunctions(result, CollectionUtils.createList(
						tradeDestinationMapping -> tradeDestinationMapping.getDefaultInvestmentGroup() == null ? null : tradeDestinationMapping.getDefaultInvestmentGroup().getName(),
						TradeDestinationMapping::isDefaultDestinationMapping),
						CollectionUtils.createList(false, false));
			}
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////////


	private List<FilterValue> getFilterListFromCommand(TradeDestinationMappingSearchCommand command) {
		// validate unsupported search fields
		//ValidationUtils.assertNull(command.getExecutingBrokerCompanyName(), "[TradeDestinationMappingCacheImpl] does not support searchField [executingBrokerCompanyName].");
		ValidationUtils.assertNull(command.getTradeTypeName(), "[TradeDestinationMappingCacheImpl] does not support searchField [tradeTypeName].");
		ValidationUtils.assertNull(command.getTradeDestinationName(), "[TradeDestinationMappingCacheImpl] does not support searchField [tradeDestinationName].");

		List<FilterValue> result = new ArrayList<>();

		if (command.getTradeDestinationId() != null) {
			result.add(new FilterValue("tradeDestinationId", command.getTradeDestinationId(), false));
		}
		if ((command.getExecutingBrokerCompanyId() != null) || ((command.getExecutingBrokerIsNull() != null) && (command.getExecutingBrokerIsNull()))) {
			result.add(new FilterValue("executingBrokerCompanyId", command.getExecutingBrokerCompanyId(), command.getExecutingBrokerIsNull() != null && command.getExecutingBrokerIsNull()));
		}
		if (command.getDefaultDestinationMapping() != null) {
			result.add(new FilterValue("defaultDestinationMapping", command.getDefaultDestinationMapping(), false));
		}
		if (command.getDefaultDestinationMapping() != null) {
			result.add(new FilterValue("tradeDestinationConnectionId", command.getTradeDestinationConnectionId(), false));
		}

		return result;
	}


	/**
	 * Brokers are cached by trade type and code and this method provides the string key for the cache.
	 */
	private String getBrokerCacheKey(Short tradeTypeId, String brokerCode) {
		return tradeTypeId + "-" + brokerCode;
	}


	/**
	 * Applies a filter the a list
	 */
	private List<TradeDestinationMappingHolder> filterList(List<TradeDestinationMappingHolder> holderList, String propertyName, Object value, boolean filterNull) {
		if (value != null || filterNull) {
			return BeanUtils.filterByPropertyName(holderList, propertyName, new Object[]{value}, false, true);
		}
		return holderList;
	}


	/**
	 * Builds or returns the trade destination mapping cache WITHOUT broker codes.
	 */
	private List<TradeDestinationMappingHolder> getTradeDestinationMappingHolderList(Short tradeTypeId) {
		Map<String, List<TradeDestinationMappingHolder>> holderMap = getCacheHandler().get(getCacheName(), DESTINATION_MAP_CACHED_OBJECT_NAME);
		if (holderMap == null) {
			holderMap = new ConcurrentHashMap<>();
			getCacheHandler().put(getCacheName(), DESTINATION_MAP_CACHED_OBJECT_NAME, holderMap);
		}
		List<TradeDestinationMappingHolder> result = holderMap.get(tradeTypeId.toString());
		if (result == null) {
			result = new ArrayList<>();

			TradeDestinationMappingSearchForm searchForm = new TradeDestinationMappingSearchForm();
			searchForm.setTradeTypeId(tradeTypeId);
			searchForm.setOrderBy("defaultDestinationMapping:desc#tradeDestinationOrder:asc#startDate:asc#id:asc");
			List<TradeDestinationMapping> mappingList = getTradeDestinationService().getTradeDestinationMappingList(searchForm);
			for (TradeDestinationMapping mapping : CollectionUtils.getIterable(mappingList)) {
				TradeDestinationMappingHolder holder = new TradeDestinationMappingHolder(mapping, null);
				result.add(holder);
			}

			holderMap.put(tradeTypeId.toString(), result);
		}
		return new ArrayList<>(result);
	}


	/**
	 * Builds or returns the trade destination mapping cache for the broker code.
	 */
	private List<TradeDestinationMappingHolder> getTradeDestinationMappingHolderBrokerList(Short tradeTypeId, String brokerCode) {
		Map<String, List<TradeDestinationMappingHolder>> holderMap = getCacheHandler().get(getCacheName(), BROKER_DESTINATION_MAP_CACHED_OBJECT_NAME);
		if (holderMap == null) {
			holderMap = doTradeDestinationMappingHolderBrokerList();
			getCacheHandler().put(getCacheName(), BROKER_DESTINATION_MAP_CACHED_OBJECT_NAME, holderMap);
		}
		String key = getBrokerCacheKey(tradeTypeId, brokerCode);
		return holderMap.containsKey(key) ? new ArrayList<>(holderMap.get(key)) : new ArrayList<>();
	}


	/**
	 * Returns the trade destination mapping holder list with broker codes.
	 */
	private Map<String, List<TradeDestinationMappingHolder>> doTradeDestinationMappingHolderBrokerList() {
		Map<String, List<TradeDestinationMappingHolder>> result = new ConcurrentHashMap<>();

		TradeDestinationMappingSearchForm sf = new TradeDestinationMappingSearchForm();
		sf.setOrderBy("tradeType.id:asc#defaultDestinationMapping:desc#tradeDestinationOrder:asc");
		List<TradeDestinationMapping> mappingList = getTradeDestinationService().getTradeDestinationMappingList(sf);
		for (TradeDestinationMapping mapping : CollectionUtils.getIterable(mappingList)) {
			if (!StringUtils.isEmpty(mapping.getOverrideBrokerCode())) {
				TradeDestinationMappingHolder holder = new TradeDestinationMappingHolder(mapping, mapping.getOverrideBrokerCode());
				addBrokerMappingHolderToMap(result, holder);
			}
			else if ((mapping.getConnection() != null) && (mapping.getExecutingBrokerCompany() != null)) {
				MarketDataSourceCompanyMappingSearchForm searchForm = new MarketDataSourceCompanyMappingSearchForm();
				searchForm.setMarketDataSourceId(BeanUtils.getBeanIdentity(mapping.getTradeDestination().getMarketDataSource()));
				if (mapping.getTradeDestination().getMarketDataSourcePurpose() == null) {
					searchForm.setMarketDataSourcePurposeIsNull(true);
				}
				else {
					searchForm.setMarketDataSourcePurposeId(BeanUtils.getBeanIdentity(mapping.getTradeDestination().getMarketDataSourcePurpose()));
				}
				searchForm.setBusinessCompanyId(BeanUtils.getBeanIdentity(mapping.getExecutingBrokerCompany()));
				List<MarketDataSourceCompanyMapping> companyMappingList = getMarketDataSourceCompanyService().getMarketDataSourceCompanyMappingList(searchForm);
				for (MarketDataSourceCompanyMapping companyMapping : CollectionUtils.getIterable(companyMappingList)) {
					TradeDestinationMappingHolder holder = new TradeDestinationMappingHolder(mapping, companyMapping.getExternalCompanyName());
					addBrokerMappingHolderToMap(result, holder);
				}
			}
		}
		return result;
	}


	private void addBrokerMappingHolderToMap(Map<String, List<TradeDestinationMappingHolder>> map, TradeDestinationMappingHolder holder) {
		List<TradeDestinationMappingHolder> holderList;
		String key = getBrokerCacheKey(holder.getTradeTypeId(), holder.getBrokerCode());
		if (!map.containsKey(key)) {
			holderList = new ArrayList<>();
			map.put(key, holderList);
		}
		else {
			holderList = map.get(key);
		}
		if (!holderList.contains(holder)) {
			holderList.add(holder);
		}
	}


	/**
	 * Determines if the destination mapping is active on correct date and is part of the correct investment group
	 */
	private boolean isMatch(TradeDestinationMappingSearchCommand source, TradeDestinationMappingHolder holder) {
		// if the source is restricted by a security, then check if the holder has an investment group and if it does, check that the security is in that group.
		if (source.getSecurityId() != null) {
			if (!StringUtils.isEmpty(holder.getInvestmentGroupName()) && !getInvestmentGroupService().isInvestmentSecurityInGroup(holder.getInvestmentGroupName(), source.getSecurityId())) {
				return false;
			}
		}
		if (!StringUtils.isEmpty(source.getExecutingBrokerCompanyName())) {
			if (StringUtils.isEmpty(holder.getExecutingBrokerCompanyName()) || !holder.getExecutingBrokerCompanyName().toLowerCase().contains(source.getExecutingBrokerCompanyName().toLowerCase())) {
				return false;
			}
		}
		Date activeOnDate = (source.getActive() != null && source.getActive().equals(true) && (source.getActiveOnDate() == null)) ? DateUtils.clearTime(new Date()) : source.getActiveOnDate();
		return activeOnDate == null || DateUtils.isDateBetween(activeOnDate, holder.getStartDate(), holder.getEndDate(), false);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public class FilterValue {

		private final String fieldName;
		private final Object value;
		private final boolean filterNullValue;


		public FilterValue(String fieldName, Object value, boolean filterNullValue) {
			this.fieldName = fieldName;
			this.value = value;
			this.filterNullValue = filterNullValue;
		}


		public String getFieldName() {
			return this.fieldName;
		}


		public Object getValue() {
			return this.value;
		}


		public boolean isFilterNullValue() {
			return this.filterNullValue;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public MarketDataSourceCompanyService getMarketDataSourceCompanyService() {
		return this.marketDataSourceCompanyService;
	}


	public void setMarketDataSourceCompanyService(MarketDataSourceCompanyService marketDataSourceCompanyService) {
		this.marketDataSourceCompanyService = marketDataSourceCompanyService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}
}
