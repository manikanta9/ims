package com.clifton.trade.destination;


import com.clifton.trade.destination.search.TradeDestinationMappingConfigurationSearchForm;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchForm;
import com.clifton.trade.destination.search.TradeDestinationSearchForm;

import java.util.List;


public interface TradeDestinationService {

	//////////////////////////////////////////////////////////////////////////// 
	//////                Trade Destination Methods                      /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeDestination getTradeDestination(short id);


	public TradeDestination getTradeDestinationByName(String name);


	public List<TradeDestination> getTradeDestinationList(TradeDestinationSearchForm searchForm);


	public TradeDestination saveTradeDestination(TradeDestination bean);


	public void deleteTradeDestination(short id);


	////////////////////////////////////////////////////////////////////////////
	//////               Trade Destination Type Methods                  /////// 
	////////////////////////////////////////////////////////////////////////////
	public TradeDestinationType getTradeDestinationType(short id);


	public List<TradeDestinationType> getTradeDestinationTypeList();


	////////////////////////////////////////////////////////////////////////////
	//////           Trade Destination Mapping Methods                   /////// 
	////////////////////////////////////////////////////////////////////////////
	public TradeDestinationMapping getTradeDestinationMapping(int id);


	public TradeDestinationMapping getTradeDestinationMappingByCommand(TradeDestinationMappingSearchCommand command);


	public List<TradeDestinationMapping> getTradeDestinationMappingListByCommand(TradeDestinationMappingSearchCommand command);


	public List<TradeDestinationMapping> getTradeDestinationMappingList(TradeDestinationMappingSearchForm searchForm);


	public TradeDestinationMapping saveTradeDestinationMapping(TradeDestinationMapping bean);


	public void deleteTradeDestinationMapping(int id);


	////////////////////////////////////////////////////////////////////////////
	//////         Trade Destination Mapping Configuration Methods       ///////
	////////////////////////////////////////////////////////////////////////////
	public TradeDestinationMappingConfiguration getTradeDestinationMappingConfiguration(int id);


	public List<TradeDestinationMappingConfiguration> getTradeDestinationMappingConfigurationList(TradeDestinationMappingConfigurationSearchForm searchForm);


	public TradeDestinationMappingConfiguration saveTradeDestinationMappingConfiguration(TradeDestinationMappingConfiguration bean);


	public void deleteTradeDestinationMappingConfiguration(int id);
}
