package com.clifton.trade.destination.cache;


import com.clifton.core.util.date.DateUtils;
import com.clifton.trade.destination.TradeDestinationMapping;

import java.util.Date;


public class TradeDestinationMappingHolder {

	private final Integer id;
	private final Date startDate;
	private final Date endDate;
	private final String investmentGroupName;
	private final String brokerCode;
	private final Short tradeTypeId;
	private final Short tradeDestinationId;
	private final Integer executingBrokerCompanyId;
	private final String executingBrokerCompanyName;
	private final boolean defaultDestinationMapping;
	private final Short tradeDestinationConnectionId;


	public TradeDestinationMappingHolder(TradeDestinationMapping mapping, String brokerCode) {
		this.id = mapping.getId();
		this.startDate = DateUtils.clearTime(mapping.getStartDate());
		this.endDate = DateUtils.clearTime(mapping.getEndDate());
		this.investmentGroupName = mapping.getDefaultInvestmentGroup() != null ? mapping.getDefaultInvestmentGroup().getName() : null;
		this.tradeTypeId = mapping.getTradeType().getId();
		this.tradeDestinationId = mapping.getTradeDestination().getId();
		this.executingBrokerCompanyId = mapping.getExecutingBrokerCompany() != null ? mapping.getExecutingBrokerCompany().getId() : null;
		this.executingBrokerCompanyName = mapping.getExecutingBrokerCompany() != null ? mapping.getExecutingBrokerCompany().getName() : null;
		this.defaultDestinationMapping = mapping.isDefaultDestinationMapping();
		this.tradeDestinationConnectionId = mapping.getConnection() != null ? mapping.getConnection().getId() : null;

		this.brokerCode = brokerCode;
	}


	public Integer getId() {
		return this.id;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public String getInvestmentGroupName() {
		return this.investmentGroupName;
	}


	public String getBrokerCode() {
		return this.brokerCode;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public Short getTradeDestinationId() {
		return this.tradeDestinationId;
	}


	public Integer getExecutingBrokerCompanyId() {
		return this.executingBrokerCompanyId;
	}


	public boolean isDefaultDestinationMapping() {
		return this.defaultDestinationMapping;
	}


	public Short getTradeDestinationConnectionId() {
		return this.tradeDestinationConnectionId;
	}


	/**
	 * Returns true of the specified object is an instance of the same class as this object
	 * and if they both have the same id.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !o.getClass().equals(getClass())) {
			return false;
		}
		TradeDestinationMappingHolder entity = (TradeDestinationMappingHolder) o;
		return getId().equals(entity.getId());
	}


	@Override
	public int hashCode() {
		return (this.id == null) ? Integer.MIN_VALUE : this.id;
	}


	public String getExecutingBrokerCompanyName() {
		return this.executingBrokerCompanyName;
	}
}
