package com.clifton.trade.destination.connection;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class TradeDestinationConnectionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "name", searchFieldPath = "allocationConnection")
	private String allocationConnectionName;

	@SearchField(searchField = "allocationConnection.id")
	private Short allocationConnectionId;

	@SearchField
	private String fixDestinationName;

	@SearchField(searchField = "fixDestinationName", comparisonConditions = ComparisonConditions.EQUALS)
	private String fixDestinationNameEquals;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getAllocationConnectionName() {
		return this.allocationConnectionName;
	}


	public void setAllocationConnectionName(String allocationConnectionName) {
		this.allocationConnectionName = allocationConnectionName;
	}


	public Short getAllocationConnectionId() {
		return this.allocationConnectionId;
	}


	public void setAllocationConnectionId(Short allocationConnectionId) {
		this.allocationConnectionId = allocationConnectionId;
	}


	public String getFixDestinationName() {
		return this.fixDestinationName;
	}


	public void setFixDestinationName(String fixDestinationName) {
		this.fixDestinationName = fixDestinationName;
	}


	public String getFixDestinationNameEquals() {
		return this.fixDestinationNameEquals;
	}


	public void setFixDestinationNameEquals(String fixDestinationNameEquals) {
		this.fixDestinationNameEquals = fixDestinationNameEquals;
	}
}
