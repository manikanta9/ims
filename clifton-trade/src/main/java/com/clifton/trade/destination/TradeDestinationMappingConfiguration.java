package com.clifton.trade.destination;


import com.clifton.core.beans.NamedEntity;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;


/**
 * The <code>TradeDestinationMappingFixConfigurationGroup</code> class allows grouping of common combinations of
 * settings to facilitate the configuration of TradeDestinationMappings.
 * <p>
 * For example, if 3 TradeDestinationMappings have a common set of configuration options, these can be added as
 * a group to the TradeDestinationMappingFigConfigurationGroup table and referenced by each TradeDestinationMapping.
 *
 * @author dirizarry
 */
public class TradeDestinationMappingConfiguration extends NamedEntity<Integer> {

	/**
	 * Use the FIX post trade allocation process for this destination.
	 */
	private boolean fixPostTradeAllocation;

	/**
	 * Disable pre-trade allocations from being included in the order.  Pre-trade allocations are automatically
	 * enabled if the fixPostTradeAllocation flag is false.  This flag will provide the ability to disable pre-trade allocations.
	 */
	private boolean fixDisablePreTradeAllocations;

	/**
	 * When creating orders the trades will be grouped by direction (buy/sell), security, executing broker, trader.
	 */
	private boolean fixTradeGroupingAllowed;

	/**
	 * Trades will be grouped into orders by holding account.
	 */
	private boolean fixTradeGroupingByHoldingAccount;


	/**
	 * Indicates that trade will be allocated to an execution account.  This requires that the holding account has a related execution account.
	 * <p>
	 * For example for Stock, in order to execute the holding account needs to related to a custodian account with purpose [Executing Stocks]
	 */
	private boolean fixExecutingAccountUsed;

	/**
	 * The allocation process is not complete until the allocation report is received.  If
	 * this flag is false, the trade will be booked with the average price of the last fill.
	 */
	private boolean fixAllocationReportExpected;

	/**
	 * If an allocation report is expected, we can still force booking with the average price.
	 * <p>
	 * NOTE: This is being done because Morgan Stanley messed up and started average pricing allocations for all trades across accounts each day.
	 * Booking at the average price will help us catch the sooner.
	 */
	private boolean fixForceExecutionAveragePriceWithAllocationReport;

	/**
	 * The commission type for the allocation.  This is currently only used for Stock allocations.
	 * <p>
	 * NOTE: If this value is populated, then commission data will be added to every AllocationInstruction.
	 */
	private CommissionTypes fixCommissionType;

	/**
	 * The commission process code.
	 */
	private ProcessCodes fixProcessCode;

	/**
	 * The default commission per unit.  This is currently only used for Stock allocations.
	 */
	private BigDecimal fixDefaultCommissionPerUnit;

	/**
	 * The allocation type used for FIX execution.  For example, Bloomberg supports CALCULATED or PRELIMINARY, but the field is not
	 * used for Goldman.
	 */
	private AllocationTypes fixAllocationType;

	/**
	 * The allocation process is complete when the an AllocationAcknowledgement message is received with status RECEIVED.
	 */
	private boolean fixAllocationCompleteWhenStatusIsReceived;

	/**
	 * If true, calculates and includes the order quantity for each order in the AllocationInstruction NoOrders group.
	 */
	private boolean fixAllocationOrderQuantityRequiredInNoOrdersGroup;

	/**
	 * If true, the average allocation price will be included in the detail section of the allocation report.  Fix Tag 366.
	 */
	private boolean fixAllocationPriceRequiredInAllocationDetails;

	/**
	 * Include FIX Tag 15 Currency containing the ISO currency code on allocation messages.
	 */
	private boolean fixCurrencyCodeRequiredOnAllocationMessages;

	/**
	 * Add the exchange code to allocation message (use code from MarketDataSourceExchangeMapping if it exists other wise use the 'exchangeCode' on the InvestmentExchange).
	 */
	private boolean fixMICExchangeCodeRequiredOnAllocationMessages;

	/**
	 * Add the CFI Code for the Investment Instrument to allocation messages.
	 */
	private boolean fixCFICodeRequiredOnAllocationMessages;

	/**
	 * Indicates that we should send SecondaryClOrdID (tag 526) from the execution reports instead of our ClOrdID from tag 11.
	 */
	private boolean fixSecondaryClientOrderIdUsedForAllocations;

	/**
	 * Send the post trade allocation top account number on the allocation message and not the order.
	 */
	private boolean fixSendTopAccountOnAllocationMessages;

	/**
	 * Add the exchange code to order message (use code from MarketDataSourceExchangeMapping if it exists other wise use the 'exchangeCode' on the InvestmentExchange).
	 */
	private boolean fixMICExchangeCodeRequiredOnOrderMessages;

	/**
	 * When this is true, each allocation on the fix message will have a party id that indicates who the
	 * clearing broker is.
	 */
	private boolean fixSendBrokerPartyIdsOnAllocationMessages;

	/**
	 * Indicates that the client account will be but in TAG 1 of the FIX message.
	 */
	private boolean sendClientAccountWithOrder;

	/**
	 * Indicated that a restrictive list of broker codes should be sent with the order.
	 * <p>
	 * For example when sending to FxConnect, add a FixParty of type EXECUTING_FIRM with comma delimited list of broker codes.
	 */
	private boolean sendRestrictiveBrokerList;

	/**
	 * When this is true and sendRestrictiveBrokerList is true, then list of brokers that is sent will be issuing companies of
	 * all related holding accounts.
	 */
	private boolean brokerRestrictedToRelatedHoldingAccountList;

	/**
	 * Search for the executing broker based on the issuer of the holding account being a subsidiary at any level of the mapped company.
	 */
	private boolean searchCompanyHierarchyForBrokerMatch;

	/**
	 * If set to true, equity orders will have a ticker symbol such as 'IBM' in field 55 instead of a CUSIP code.
	 */
	private boolean fixUseSymbolForEquityOrders;

	/**
	 * If true, we will send a nested party id with the LEI of the issuing company of the holding account.
	 * <p>
	 * Currently used for sending pre-trade allocations to Bloomberg for Cleared CDS/IRS traded on the SEF.
	 */
	private boolean fixSendClearingFirmLEIWithAllocation;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isFixPostTradeAllocation() {
		return this.fixPostTradeAllocation;
	}


	public void setFixPostTradeAllocation(boolean fixPostTradeAllocation) {
		this.fixPostTradeAllocation = fixPostTradeAllocation;
	}


	public boolean isFixDisablePreTradeAllocations() {
		return this.fixDisablePreTradeAllocations;
	}


	public void setFixDisablePreTradeAllocations(boolean fixDisablePreTradeAllocations) {
		this.fixDisablePreTradeAllocations = fixDisablePreTradeAllocations;
	}


	public boolean isFixTradeGroupingAllowed() {
		return this.fixTradeGroupingAllowed;
	}


	public void setFixTradeGroupingAllowed(boolean fixTradeGroupingAllowed) {
		this.fixTradeGroupingAllowed = fixTradeGroupingAllowed;
	}


	public boolean isFixTradeGroupingByHoldingAccount() {
		return this.fixTradeGroupingByHoldingAccount;
	}


	public void setFixTradeGroupingByHoldingAccount(boolean fixTradeGroupingByHoldingAccount) {
		this.fixTradeGroupingByHoldingAccount = fixTradeGroupingByHoldingAccount;
	}


	public boolean isFixExecutingAccountUsed() {
		return this.fixExecutingAccountUsed;
	}


	public void setFixExecutingAccountUsed(boolean fixExecutingAccountUsed) {
		this.fixExecutingAccountUsed = fixExecutingAccountUsed;
	}


	public boolean isFixAllocationReportExpected() {
		return this.fixAllocationReportExpected;
	}


	public void setFixAllocationReportExpected(boolean fixAllocationReportExpected) {
		this.fixAllocationReportExpected = fixAllocationReportExpected;
	}


	public boolean isFixForceExecutionAveragePriceWithAllocationReport() {
		return this.fixForceExecutionAveragePriceWithAllocationReport;
	}


	public void setFixForceExecutionAveragePriceWithAllocationReport(boolean fixForceExecutionAveragePriceWithAllocationReport) {
		this.fixForceExecutionAveragePriceWithAllocationReport = fixForceExecutionAveragePriceWithAllocationReport;
	}


	public CommissionTypes getFixCommissionType() {
		return this.fixCommissionType;
	}


	public void setFixCommissionType(CommissionTypes fixCommissionType) {
		this.fixCommissionType = fixCommissionType;
	}


	public ProcessCodes getFixProcessCode() {
		return this.fixProcessCode;
	}


	public void setFixProcessCode(ProcessCodes fixProcessCode) {
		this.fixProcessCode = fixProcessCode;
	}


	public BigDecimal getFixDefaultCommissionPerUnit() {
		return this.fixDefaultCommissionPerUnit;
	}


	public void setFixDefaultCommissionPerUnit(BigDecimal fixDefaultCommissionPerUnit) {
		this.fixDefaultCommissionPerUnit = fixDefaultCommissionPerUnit;
	}


	public AllocationTypes getFixAllocationType() {
		return this.fixAllocationType;
	}


	public void setFixAllocationType(AllocationTypes fixAllocationType) {
		this.fixAllocationType = fixAllocationType;
	}


	public boolean isFixAllocationCompleteWhenStatusIsReceived() {
		return this.fixAllocationCompleteWhenStatusIsReceived;
	}


	public void setFixAllocationCompleteWhenStatusIsReceived(boolean fixAllocationCompleteWhenStatusIsReceived) {
		this.fixAllocationCompleteWhenStatusIsReceived = fixAllocationCompleteWhenStatusIsReceived;
	}


	public boolean isFixAllocationOrderQuantityRequiredInNoOrdersGroup() {
		return this.fixAllocationOrderQuantityRequiredInNoOrdersGroup;
	}


	public void setFixAllocationOrderQuantityRequiredInNoOrdersGroup(boolean fixAllocationOrderQuantityRequiredInNoOrdersGroup) {
		this.fixAllocationOrderQuantityRequiredInNoOrdersGroup = fixAllocationOrderQuantityRequiredInNoOrdersGroup;
	}


	public boolean isFixAllocationPriceRequiredInAllocationDetails() {
		return this.fixAllocationPriceRequiredInAllocationDetails;
	}


	public void setFixAllocationPriceRequiredInAllocationDetails(boolean fixAllocationPriceRequiredInAllocationDetails) {
		this.fixAllocationPriceRequiredInAllocationDetails = fixAllocationPriceRequiredInAllocationDetails;
	}


	public boolean isFixCurrencyCodeRequiredOnAllocationMessages() {
		return this.fixCurrencyCodeRequiredOnAllocationMessages;
	}


	public void setFixCurrencyCodeRequiredOnAllocationMessages(boolean fixCurrencyCodeRequiredOnAllocationMessages) {
		this.fixCurrencyCodeRequiredOnAllocationMessages = fixCurrencyCodeRequiredOnAllocationMessages;
	}


	public boolean isFixMICExchangeCodeRequiredOnAllocationMessages() {
		return this.fixMICExchangeCodeRequiredOnAllocationMessages;
	}


	public void setFixMICExchangeCodeRequiredOnAllocationMessages(boolean fixMICExchangeCodeRequiredOnAllocationMessages) {
		this.fixMICExchangeCodeRequiredOnAllocationMessages = fixMICExchangeCodeRequiredOnAllocationMessages;
	}


	public boolean isFixCFICodeRequiredOnAllocationMessages() {
		return this.fixCFICodeRequiredOnAllocationMessages;
	}


	public void setFixCFICodeRequiredOnAllocationMessages(boolean fixCFICodeRequiredOnAllocationMessages) {
		this.fixCFICodeRequiredOnAllocationMessages = fixCFICodeRequiredOnAllocationMessages;
	}


	public boolean isFixSecondaryClientOrderIdUsedForAllocations() {
		return this.fixSecondaryClientOrderIdUsedForAllocations;
	}


	public void setFixSecondaryClientOrderIdUsedForAllocations(boolean fixSecondaryClientOrderIdUsedForAllocations) {
		this.fixSecondaryClientOrderIdUsedForAllocations = fixSecondaryClientOrderIdUsedForAllocations;
	}


	public boolean isFixSendTopAccountOnAllocationMessages() {
		return this.fixSendTopAccountOnAllocationMessages;
	}


	public void setFixSendTopAccountOnAllocationMessages(boolean fixSendTopAccountOnAllocationMessages) {
		this.fixSendTopAccountOnAllocationMessages = fixSendTopAccountOnAllocationMessages;
	}


	public boolean isFixMICExchangeCodeRequiredOnOrderMessages() {
		return this.fixMICExchangeCodeRequiredOnOrderMessages;
	}


	public void setFixMICExchangeCodeRequiredOnOrderMessages(boolean fixMICExchangeCodeRequiredOnOrderMessages) {
		this.fixMICExchangeCodeRequiredOnOrderMessages = fixMICExchangeCodeRequiredOnOrderMessages;
	}


	public boolean isFixSendBrokerPartyIdsOnAllocationMessages() {
		return this.fixSendBrokerPartyIdsOnAllocationMessages;
	}


	public void setFixSendBrokerPartyIdsOnAllocationMessages(boolean fixSendBrokerPartyIdsOnAllocationMessages) {
		this.fixSendBrokerPartyIdsOnAllocationMessages = fixSendBrokerPartyIdsOnAllocationMessages;
	}


	public boolean isSendClientAccountWithOrder() {
		return this.sendClientAccountWithOrder;
	}


	public void setSendClientAccountWithOrder(boolean sendClientAccountWithOrder) {
		this.sendClientAccountWithOrder = sendClientAccountWithOrder;
	}


	public boolean isSendRestrictiveBrokerList() {
		return this.sendRestrictiveBrokerList;
	}


	public void setSendRestrictiveBrokerList(boolean sendRestrictiveBrokerList) {
		this.sendRestrictiveBrokerList = sendRestrictiveBrokerList;
	}


	public boolean isBrokerRestrictedToRelatedHoldingAccountList() {
		return this.brokerRestrictedToRelatedHoldingAccountList;
	}


	public void setBrokerRestrictedToRelatedHoldingAccountList(boolean brokerRestrictedToRelatedHoldingAccountList) {
		this.brokerRestrictedToRelatedHoldingAccountList = brokerRestrictedToRelatedHoldingAccountList;
	}


	public boolean isSearchCompanyHierarchyForBrokerMatch() {
		return this.searchCompanyHierarchyForBrokerMatch;
	}


	public void setSearchCompanyHierarchyForBrokerMatch(boolean searchCompanyHierarchyForBrokerMatch) {
		this.searchCompanyHierarchyForBrokerMatch = searchCompanyHierarchyForBrokerMatch;
	}


	public boolean isFixUseSymbolForEquityOrders() {
		return this.fixUseSymbolForEquityOrders;
	}


	public void setFixUseSymbolForEquityOrders(boolean fixUseSymbolForEquityOrders) {
		this.fixUseSymbolForEquityOrders = fixUseSymbolForEquityOrders;
	}


	public boolean isFixSendClearingFirmLEIWithAllocation() {
		return this.fixSendClearingFirmLEIWithAllocation;
	}


	public void setFixSendClearingFirmLEIWithAllocation(boolean fixSendClearingFirmLEIWithAllocation) {
		this.fixSendClearingFirmLEIWithAllocation = fixSendClearingFirmLEIWithAllocation;
	}
}

