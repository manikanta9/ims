package com.clifton.trade.destination.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class TradeDestinationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeTradeDestinationName;

	@SearchField
	private String description;

	@SearchField(searchField = "connection.id")
	private Short connectionId;

	@SearchField(searchField = "type.id")
	private Short destinationTypeId;

	@SearchField(searchField = "marketDataSource.id")
	private Short marketDataSourceId;

	@SearchField(searchField = "marketDataSourcePurpose.id")
	private Short marketDataSourcePurposeId;

	@SearchField
	private String fixTargetSubId;

	@SearchField
	private String fixHandlingInstructions;

	@SearchField(searchFieldPath = "tradeModifySystemCondition", searchField = "name")
	private String tradeModifySystemConditionName;

	@SearchField
	private Integer destinationOrder;

	@SearchField
	private Boolean allocateAfterDoneForDay;

	@SearchField
	private Boolean optionalQuantityOrNotionalEntry;

	// custom search filter
	private Integer executingBrokerCompanyId;

	// custom search filter
	private Short tradeTypeId;

	// custom search filter - sets trade type id when id is null, but name equals is present
	private String tradeTypeNameEquals;

	// custom search filter: used to lookup a tradeTypeId and is ignored if tradeTypeId is not null.
	private Integer securityIdForTradeType;

	/**
	 * Used in combination with tradeTypeId to join to the TradeDestinationMapping table and filter on mapped trade types.  Or by itself
	 * to filter to destinations that have an active mapping.
	 */
	// custom search filter
	private Date activeOnDate;

	// custom search filter: allows brokerCompanyId to be null
	private Integer defaultOrBrokerCompanyId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getConnectionId() {
		return this.connectionId;
	}


	public void setConnectionId(Short connectionId) {
		this.connectionId = connectionId;
	}


	public Short getDestinationTypeId() {
		return this.destinationTypeId;
	}


	public void setDestinationTypeId(Short destinationTypeId) {
		this.destinationTypeId = destinationTypeId;
	}


	public Integer getExecutingBrokerCompanyId() {
		return this.executingBrokerCompanyId;
	}


	public void setExecutingBrokerCompanyId(Integer executingBrokerCompanyId) {
		this.executingBrokerCompanyId = executingBrokerCompanyId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getDefaultOrBrokerCompanyId() {
		return this.defaultOrBrokerCompanyId;
	}


	public void setDefaultOrBrokerCompanyId(Integer defaultOrBrokerCompanyId) {
		this.defaultOrBrokerCompanyId = defaultOrBrokerCompanyId;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getExcludeTradeDestinationName() {
		return this.excludeTradeDestinationName;
	}


	public void setExcludeTradeDestinationName(String excludeTradeDestinationName) {
		this.excludeTradeDestinationName = excludeTradeDestinationName;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Integer getSecurityIdForTradeType() {
		return this.securityIdForTradeType;
	}


	public void setSecurityIdForTradeType(Integer securityIdForTradeType) {
		this.securityIdForTradeType = securityIdForTradeType;
	}


	public String getTradeTypeNameEquals() {
		return this.tradeTypeNameEquals;
	}


	public void setTradeTypeNameEquals(String tradeTypeNameEquals) {
		this.tradeTypeNameEquals = tradeTypeNameEquals;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public Short getMarketDataSourcePurposeId() {
		return this.marketDataSourcePurposeId;
	}


	public void setMarketDataSourcePurposeId(Short marketDataSourcePurposeId) {
		this.marketDataSourcePurposeId = marketDataSourcePurposeId;
	}


	public String getFixTargetSubId() {
		return this.fixTargetSubId;
	}


	public void setFixTargetSubId(String fixTargetSubId) {
		this.fixTargetSubId = fixTargetSubId;
	}


	public String getFixHandlingInstructions() {
		return this.fixHandlingInstructions;
	}


	public void setFixHandlingInstructions(String fixHandlingInstructions) {
		this.fixHandlingInstructions = fixHandlingInstructions;
	}


	public String getTradeModifySystemConditionName() {
		return this.tradeModifySystemConditionName;
	}


	public void setTradeModifySystemConditionName(String tradeModifySystemConditionName) {
		this.tradeModifySystemConditionName = tradeModifySystemConditionName;
	}


	public Integer getDestinationOrder() {
		return this.destinationOrder;
	}


	public void setDestinationOrder(Integer destinationOrder) {
		this.destinationOrder = destinationOrder;
	}


	public Boolean getAllocateAfterDoneForDay() {
		return this.allocateAfterDoneForDay;
	}


	public void setAllocateAfterDoneForDay(Boolean allocateAfterDoneForDay) {
		this.allocateAfterDoneForDay = allocateAfterDoneForDay;
	}


	public Boolean getOptionalQuantityOrNotionalEntry() {
		return this.optionalQuantityOrNotionalEntry;
	}


	public void setOptionalQuantityOrNotionalEntry(Boolean optionalQuantityOrNotionalEntry) {
		this.optionalQuantityOrNotionalEntry = optionalQuantityOrNotionalEntry;
	}
}
