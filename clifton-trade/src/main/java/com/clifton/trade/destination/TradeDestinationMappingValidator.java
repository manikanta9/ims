package com.clifton.trade.destination;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeDestinationValidator</code> validates there is only one default set
 *
 * @author Mary Anderson
 */
@Component
public class TradeDestinationMappingValidator extends SelfRegisteringDaoValidator<TradeDestinationMapping> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(@SuppressWarnings("unused") TradeDestinationMapping bean, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		// Do nothing - validation needs dao
	}


	@Override
	public void validate(TradeDestinationMapping bean, DaoEventTypes config, ReadOnlyDAO<TradeDestinationMapping> dao) throws ValidationException {
		if (bean.isDefaultDestinationMapping()) {
			TradeDestinationMapping def = dao.findOneByFields(new String[]{"tradeType.id", "tradeDestination.id", "defaultDestinationMapping"}, new Object[]{bean.getTradeType().getId(),
					bean.getTradeDestination().getId(), true});
			if (def != null && (config.isInsert() || !def.equals(bean))) {
				throw new ValidationException(def.getTradeType().getName() + " for " + def.getTradeDestination().getName() + " with " + def.getExecutingBrokerCompany().getLabel()
						+ " is already marked as the default trade destination. Cannot have more than one default.");
			}
		}
	}
}
