package com.clifton.trade.destination;


import com.clifton.core.beans.NamedEntity;


public class TradeDestinationType extends NamedEntity<Short> {

	private boolean fixTradeOnly;
	/**
	 * Indicates that the trade will be part of a TradeOrder.
	 */
	private boolean createOrder;

	private boolean manual;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isManual() {
		return this.manual;
	}


	public void setManual(boolean manual) {
		this.manual = manual;
	}


	public boolean isFixTradeOnly() {
		return this.fixTradeOnly;
	}


	public void setFixTradeOnly(boolean fixTradeOnly) {
		this.fixTradeOnly = fixTradeOnly;
	}


	public boolean isCreateOrder() {
		return this.createOrder;
	}


	public void setCreateOrder(boolean createOrder) {
		this.createOrder = createOrder;
	}
}
