package com.clifton.trade.destination;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.cache.TradeDestinationMappingCache;
import com.clifton.trade.destination.search.TradeDestinationMappingConfigurationSearchForm;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchForm;
import com.clifton.trade.destination.search.TradeDestinationSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class TradeDestinationServiceImpl implements TradeDestinationService {

	private AdvancedUpdatableDAO<TradeDestination, Criteria> tradeDestinationDAO;
	private AdvancedUpdatableDAO<TradeDestinationType, Criteria> tradeDestinationTypeDAO;
	private AdvancedUpdatableDAO<TradeDestinationMapping, Criteria> tradeDestinationMappingDAO;
	private AdvancedUpdatableDAO<TradeDestinationMappingConfiguration, Criteria> tradeDestinationMappingConfigurationDAO;

	private TradeService tradeService;
	private TradeDestinationMappingCache tradeDestinationMappingCache;


	////////////////////////////////////////////////////////////////////////////
	//////                Trade Destination Methods                      ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeDestination getTradeDestination(short id) {
		return getTradeDestinationDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeDestination getTradeDestinationByName(String name) {
		return getTradeDestinationDAO().findOneByField("name", name);
	}


	@Override
	public List<TradeDestination> getTradeDestinationList(final TradeDestinationSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getDefaultOrBrokerCompanyId() != null) {
					criteria.createAlias("tradeDestinationMappingList", "m").add(
							Restrictions.or(Restrictions.isNull("m.executingBrokerCompany.id"), Restrictions.eq("m.executingBrokerCompany.id", searchForm.getDefaultOrBrokerCompanyId())));
				}

				if ((searchForm.getTradeTypeId() != null) || (searchForm.getSecurityIdForTradeType() != null) || (searchForm.getActiveOnDate() != null)
						|| (searchForm.getExecutingBrokerCompanyId() != null) || !StringUtils.isEmpty(searchForm.getTradeTypeNameEquals())) {
					if (searchForm.getTradeTypeId() == null) {
						if (!StringUtils.isEmpty(searchForm.getTradeTypeNameEquals())) {
							TradeType tradeType = getTradeService().getTradeTypeByName(searchForm.getTradeTypeNameEquals());
							ValidationUtils.assertNotNull(tradeType, "No trade type found with name [" + searchForm.getTradeTypeNameEquals() + "].");
							searchForm.setTradeTypeId(tradeType.getId());
						}
						else {
							TradeType tradeType = getTradeService().getTradeTypeForSecurity(searchForm.getSecurityIdForTradeType());
							searchForm.setTradeTypeId(tradeType.getId());
						}
					}

					DetachedCriteria subMapping = DetachedCriteria.forClass(TradeDestinationMapping.class, "m");
					subMapping.setProjection(Projections.property("id"));
					subMapping.add(Restrictions.eqProperty("m.tradeDestination.id", criteria.getAlias() + ".id"));
					if (searchForm.getTradeTypeId() != null) {
						subMapping.add(Restrictions.eq("m.tradeType.id", searchForm.getTradeTypeId()));
					}
					if (searchForm.getActiveOnDate() != null) {
						subMapping.add(ActiveExpressionForDates.forActiveOnDate(true, searchForm.getActiveOnDate()));
					}
					if (searchForm.getExecutingBrokerCompanyId() != null) {
						subMapping.add(Restrictions.eq("m.executingBrokerCompany.id", searchForm.getExecutingBrokerCompanyId()));
					}

					criteria.add(Subqueries.exists(subMapping));
				}
			}
		};

		return getTradeDestinationDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public TradeDestination saveTradeDestination(TradeDestination bean) {
		if (bean.getMarketDataSource() != null) {
			ValidationUtils.assertNotNull(bean.getMarketDataSourcePurpose(), "MarketDataSourcePurpose is required when a MarketDataSource is selected.", "marketDataSourcePurpose");
		}
		return getTradeDestinationDAO().save(bean);
	}


	@Override
	public void deleteTradeDestination(short id) {
		getTradeDestinationDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////               Trade Destination Type Methods                  ///////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public TradeDestinationType getTradeDestinationType(short id) {
		return getTradeDestinationTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeDestinationType> getTradeDestinationTypeList() {
		return getTradeDestinationTypeDAO().findAll();
	}


	////////////////////////////////////////////////////////////////////////////
	//////           Trade Destination Mapping Methods                   ///////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public TradeDestinationMapping getTradeDestinationMapping(int id) {
		return getTradeDestinationMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeDestinationMapping getTradeDestinationMappingByCommand(TradeDestinationMappingSearchCommand command) {
		if (command.getTrade() != null) {
			return getTradeDestinationMappingByTrade(command);
		}
		List<TradeDestinationMapping> tdl = getTradeDestinationMappingListByCommand(command);
		TradeDestinationMapping td = CollectionUtils.getFirstElement(tdl);
		if (td == null && command.getTradeTypeId() != null) {
			td = doGetTradeDestinationMappingDefault(command.getTradeTypeId(), command.getTradeDestinationId());
		}
		return td;
	}


	@Override
	public List<TradeDestinationMapping> getTradeDestinationMappingListByCommand(TradeDestinationMappingSearchCommand command) {
		return doGetTradeDestinationMappingListByCommand(command);
	}


	private List<TradeDestinationMapping> doGetTradeDestinationMappingListByCommand(TradeDestinationMappingSearchCommand command) {
		if (command.getTradeTypeId() == null) {
			if (command.getSecurityId() != null) {
				TradeType tradeType = getTradeService().getTradeTypeForSecurity(command.getSecurityId());
				ValidationUtils.assertNotNull(tradeType, "No trade type found for investment security [" + command.getSecurityId() + "].");
				command.setTradeTypeId(tradeType.getId());
			}
			else if (command.getSecurityIdForTradeType() != null) {
				TradeType tradeType = getTradeService().getTradeTypeForSecurity(command.getSecurityIdForTradeType());
				ValidationUtils.assertNotNull(tradeType, "No trade type found for investment security [" + command.getSecurityId() + "].");
				command.setTradeTypeId(tradeType.getId());
			}
			else if (!StringUtils.isEmpty(command.getTradeTypeNameEquals())) {
				TradeType tradeType = getTradeService().getTradeTypeByName(command.getTradeTypeNameEquals());
				ValidationUtils.assertNotNull(tradeType, "No trade type found with name [" + command.getTradeTypeNameEquals() + "].");
				command.setTradeTypeId(tradeType.getId());
			}
		}
		List<TradeDestinationMapping> result = getTradeDestinationMappingCache().getTradeDestinationMappingList(command);
		if (command.getStart() == 0 && (command.getLimit() == Integer.MAX_VALUE)) {
			return result;
		}
		if (command.isUsePagingArray()) {
			return CollectionUtils.toPagingArrayList(result, command.getStart(), command.getLimit());
		}
		return result;
	}


	private TradeDestinationMapping getTradeDestinationMappingByTrade(TradeDestinationMappingSearchCommand command) {
		ValidationUtils.assertNotNull(command.getTradeDestinationId(), "Cannot retrieve trade destination mapping for a trade that does not have trade destination set.", "tradeDestination");
		TradeDestinationMapping mapping = CollectionUtils.getOnlyElement(getTradeDestinationMappingCache().getTradeDestinationMappingList(command));
		if (mapping == null) {
			throw new ValidationException("Cannot find active Trade Destination Mapping for Trade [" + command.getTrade().getLabel() + "] with type ["
					+ command.getTrade().getTradeType().getLabel() + "] and destination ["
					+ command.getTrade().getTradeDestination().getLabel() + "] and executing broker ["
					+ (command.getTrade().getExecutingBrokerCompany() != null ? command.getTrade().getExecutingBrokerCompany().getLabel() : "null") + "].");
		}
		return mapping;
	}


	/**
	 * Gets the default destination mapping for the tradeType and destination.
	 * <p>
	 * NOTE:  This will be the first object in the list of available mappings order with:
	 * <p>
	 * "defaultDestinationMapping:desc#tradeDestination.destinationOrder:asc#startDate:asc#id:asc"
	 */
	private TradeDestinationMapping doGetTradeDestinationMappingDefault(final short tradeTypeId, final Short tradeDestinationId) {
		TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand();
		command.setTradeDestinationId(tradeDestinationId);
		command.setTradeTypeId(tradeTypeId);
		command.setDefaultDestinationMapping(true);
		command.setActiveOnDate(DateUtils.clearTime(new Date()));
		return CollectionUtils.getFirstElement(getTradeDestinationMappingCache().getTradeDestinationMappingList(command));
	}


	@Override
	public List<TradeDestinationMapping> getTradeDestinationMappingList(final TradeDestinationMappingSearchForm searchForm) {
		return getTradeDestinationMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeDestinationMapping saveTradeDestinationMapping(TradeDestinationMapping bean) {
		if (!bean.getTradeType().isBrokerSelectionAtExecutionAllowed()) {
			ValidationUtils.assertTrue((bean.getExecutingBrokerCompany() != null && bean.getExecutingBrokerCompany().getType().isTradingAllowed()) || bean.getExecutingBrokerCompany() == null,
					"Cannot map destinations to companies of type [" + (bean.getExecutingBrokerCompany() != null ? bean.getExecutingBrokerCompany().getType().getName() : "null")
							+ "] only companies of with types that allow trading.");
		}
		// validate mutually exclusive fields
		ValidationUtils.assertFalse(bean.getMappingConfiguration().isFixAllocationReportExpected() && bean.getMappingConfiguration().isFixAllocationCompleteWhenStatusIsReceived(),
				"[fixAllocationReportExpected] and [fixAllocationReportExpected] cannot both be true.", "fixAllocationReportExpected");
		// validate that only one default destination exists
		if (bean.isDefaultDestinationMapping()) {
			TradeDestinationMappingSearchForm sf = new TradeDestinationMappingSearchForm();
			sf.setTradeDestinationId(bean.getTradeDestination().getId());
			sf.setTradeTypeId(bean.getTradeType().getId());
			if (bean.getExecutingBrokerCompany() != null) {
				sf.setExecutingBrokerCompanyId(bean.getExecutingBrokerCompany().getId());
			}
			else {
				sf.setExecutingBrokerIsNull(true);
			}
			sf.setDefaultDestinationMapping(bean.isDefaultDestinationMapping());
			List<TradeDestinationMapping> existing = getTradeDestinationMappingList(sf);
			ValidationUtils.assertTrue(
					CollectionUtils.isEmpty(existing) || ((existing.size() == 1) && existing.get(0).equals(bean)),
					"Default trade destination already exists for destination [" + bean.getTradeDestination() + "], trade type[" + bean.getTradeType() + "] and broker ["
							+ (bean.getExecutingBrokerCompany() == null ? "null" : bean.getExecutingBrokerCompany()) + "].", "clientInvestmentAccount");
		}

		return getTradeDestinationMappingDAO().save(bean);
	}


	@Override
	public void deleteTradeDestinationMapping(int id) {
		getTradeDestinationMappingDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////         Trade Destination Mapping Configuration Methods       ///////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public TradeDestinationMappingConfiguration getTradeDestinationMappingConfiguration(int id) {
		return getTradeDestinationMappingConfigurationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeDestinationMappingConfiguration> getTradeDestinationMappingConfigurationList(TradeDestinationMappingConfigurationSearchForm searchForm) {
		return getTradeDestinationMappingConfigurationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeDestinationMappingConfiguration saveTradeDestinationMappingConfiguration(TradeDestinationMappingConfiguration bean) {
		return getTradeDestinationMappingConfigurationDAO().save(bean);
	}


	@Override
	public void deleteTradeDestinationMappingConfiguration(int id) {
		getTradeDestinationMappingConfigurationDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////                 Getter and Setter Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeDestination, Criteria> getTradeDestinationDAO() {
		return this.tradeDestinationDAO;
	}


	public void setTradeDestinationDAO(AdvancedUpdatableDAO<TradeDestination, Criteria> tradeDestinationDAO) {
		this.tradeDestinationDAO = tradeDestinationDAO;
	}


	public AdvancedUpdatableDAO<TradeDestinationMapping, Criteria> getTradeDestinationMappingDAO() {
		return this.tradeDestinationMappingDAO;
	}


	public void setTradeDestinationMappingDAO(AdvancedUpdatableDAO<TradeDestinationMapping, Criteria> tradeDestinationMappingDAO) {
		this.tradeDestinationMappingDAO = tradeDestinationMappingDAO;
	}


	public AdvancedUpdatableDAO<TradeDestinationMappingConfiguration, Criteria> getTradeDestinationMappingConfigurationDAO() {
		return this.tradeDestinationMappingConfigurationDAO;
	}


	public void setTradeDestinationMappingConfigurationDAO(AdvancedUpdatableDAO<TradeDestinationMappingConfiguration, Criteria> tradeDestinationMappingConfigurationDAO) {
		this.tradeDestinationMappingConfigurationDAO = tradeDestinationMappingConfigurationDAO;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AdvancedUpdatableDAO<TradeDestinationType, Criteria> getTradeDestinationTypeDAO() {
		return this.tradeDestinationTypeDAO;
	}


	public void setTradeDestinationTypeDAO(AdvancedUpdatableDAO<TradeDestinationType, Criteria> tradeDestinationTypeDAO) {
		this.tradeDestinationTypeDAO = tradeDestinationTypeDAO;
	}


	public TradeDestinationMappingCache getTradeDestinationMappingCache() {
		return this.tradeDestinationMappingCache;
	}


	public void setTradeDestinationMappingCache(TradeDestinationMappingCache tradeDestinationMappingCache) {
		this.tradeDestinationMappingCache = tradeDestinationMappingCache;
	}
}
