package com.clifton.trade.destination;


import com.clifton.core.beans.NamedEntity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.system.condition.SystemCondition;
import com.clifton.trade.destination.connection.TradeDestinationConnection;


/**
 * The <code>TradeDestination</code> defines a list of destinations for trades such as REDI via FIX or normal Clifton processing.
 * <p/>
 * Destination name must be unique per broker company.
 *
 * @author mwacker
 */
public class TradeDestination extends NamedEntity<Short> {

	public static final String MANUAL = "Manual";
	public static final String MANUAL_ORDER = "Manual Order";
	public static final String REDI_TICKET = "REDI Ticket";
	public static final String PRE_TRADE_ENTRY = "Pre-Trade Entry";
	public static final String FX_CONNECT_TICKET = "FX Connect Ticket";
	public static final String BLOOMBERG_TICKET = "Bloomberg Ticket";

	private TradeDestinationType type;
	private TradeDestinationConnection connection;

	private SystemCondition tradeModifySystemCondition;

	/**
	 * The market data sources used to map the securities, broker codes and exchanges code to an order.
	 * <p/>
	 * NOTE: The data source for price multiplier is found using the purpose below and the executing broker
	 * on the order.
	 */
	public MarketDataSource marketDataSource;
	/**
	 * The market data sources used to lookup the securities, broker codes and exchanges code.
	 */
	private MarketDataSourcePurpose marketDataSourcePurpose;

	/**
	 * Default trades for securities that belong to this group to this destination.
	 * Apply smallest order if more than one destination is returned.
	 */
	private int destinationOrder;

	private String fixTargetSubId; // TKTS, GSDESK, etc.
	private String fixHandlingInstructions; // AUTOMATIC, MANUAL, etc.

	private boolean allocateAfterDoneForDay;

	/**
	 * Trades for this destination must have either quantityIntended or AccountingNotional populated but not both.
	 */
	private boolean optionalQuantityOrNotionalEntry;


	/**
	 * Allows for enablement of the Time In Force (TIF) selection via the UI. This allows us to choose which destinations
	 * should allow configuration of this value based on whether or not the associated FIX Platform supports it.  The default TIF value is always "DAY".
	 */
	private boolean enableTimeInForceSelection;

	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////


	public int getDestinationOrder() {
		return this.destinationOrder;
	}


	public void setDestinationOrder(int destinationOrder) {
		this.destinationOrder = destinationOrder;
	}


	public TradeDestinationType getType() {
		return this.type;
	}


	public void setType(TradeDestinationType type) {
		this.type = type;
	}


	public String getFixTargetSubId() {
		return this.fixTargetSubId;
	}


	public void setFixTargetSubId(String fixTargetSubId) {
		this.fixTargetSubId = fixTargetSubId;
	}


	public String getFixHandlingInstructions() {
		return this.fixHandlingInstructions;
	}


	public void setFixHandlingInstructions(String fixHandlingInstructions) {
		this.fixHandlingInstructions = fixHandlingInstructions;
	}


	public TradeDestinationConnection getConnection() {
		return this.connection;
	}


	public void setConnection(TradeDestinationConnection connection) {
		this.connection = connection;
	}


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public MarketDataSourcePurpose getMarketDataSourcePurpose() {
		return this.marketDataSourcePurpose;
	}


	public void setMarketDataSourcePurpose(MarketDataSourcePurpose marketDataSourcePurpose) {
		this.marketDataSourcePurpose = marketDataSourcePurpose;
	}


	public boolean isOptionalQuantityOrNotionalEntry() {
		return this.optionalQuantityOrNotionalEntry;
	}


	public void setOptionalQuantityOrNotionalEntry(boolean optionalQuantityOrNotionalEntry) {
		this.optionalQuantityOrNotionalEntry = optionalQuantityOrNotionalEntry;
	}


	public boolean isAllocateAfterDoneForDay() {
		return this.allocateAfterDoneForDay;
	}


	public void setAllocateAfterDoneForDay(boolean allocateAfterDoneForDay) {
		this.allocateAfterDoneForDay = allocateAfterDoneForDay;
	}


	public SystemCondition getTradeModifySystemCondition() {
		return this.tradeModifySystemCondition;
	}


	public void setTradeModifySystemCondition(SystemCondition tradeModifySystemCondition) {
		this.tradeModifySystemCondition = tradeModifySystemCondition;
	}


	public boolean isEnableTimeInForceSelection() {
		return this.enableTimeInForceSelection;
	}


	public void setEnableTimeInForceSelection(boolean enableTimeInForceSelection) {
		this.enableTimeInForceSelection = enableTimeInForceSelection;
	}
}
