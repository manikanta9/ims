package com.clifton.trade.destination.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;


public class TradeDestinationMappingConfigurationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "name")
	private String configurationName;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String configurationNameEquals;

	@SearchField(searchField = "description")
	private String configurationDescription;

	@SearchField
	private Boolean fixPostTradeAllocation;

	@SearchField
	private Boolean fixDisablePreTradeAllocations;

	@SearchField
	private Boolean fixTradeGroupingAllowed;

	@SearchField
	private Boolean fixTradeGroupingByHoldingAccount;

	@SearchField
	private Boolean fixExecutingAccountUsed;

	@SearchField
	private Boolean fixAllocationReportExpected;

	@SearchField
	private CommissionTypes fixCommissionType;

	@SearchField
	private ProcessCodes fixProcessCode;

	@SearchField
	private BigDecimal fixDefaultCommissionPerUnit;

	@SearchField
	private AllocationTypes fixAllocationType;

	@SearchField
	private Boolean fixAllocationCompleteWhenStatusIsReceived;

	@SearchField
	private Boolean fixAllocationOrderQuantityRequiredInNoOrdersGroup;

	@SearchField
	private Boolean fixAllocationPriceRequiredInAllocationDetails;

	@SearchField
	private Boolean fixCurrencyCodeRequiredOnAllocationMessages;

	@SearchField
	private Boolean fixMICExchangeCodeRequiredOnAllocationMessages;

	@SearchField
	private Boolean fixCFICodeRequiredOnAllocationMessages;

	@SearchField
	private Boolean fixSecondaryClientOrderIdUsedForAllocations;

	@SearchField
	private Boolean fixSendTopAccountOnAllocationMessages;

	@SearchField
	private Boolean fixMICExchangeCodeRequiredOnOrderMessages;

	@SearchField
	private Boolean fixSendBrokerPartyIdsOnAllocationMessages;

	@SearchField
	private Boolean sendClientAccountWithOrder;

	@SearchField
	private Boolean sendRestrictiveBrokerList;

	@SearchField
	private Boolean brokerRestrictedToRelatedHoldingAccountList;

	@SearchField
	private Boolean searchCompanyHierarchyForBrokerMatch;

	@SearchField
	private Boolean fixUseSymbolForEquityOrders;

	@SearchField
	private Boolean fixSendClearingFirmLEIWithAllocation;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getConfigurationName() {
		return this.configurationName;
	}


	public void setConfigurationName(String configurationName) {
		this.configurationName = configurationName;
	}


	public String getConfigurationNameEquals() {
		return this.configurationNameEquals;
	}


	public void setConfigurationNameEquals(String configurationNameEquals) {
		this.configurationNameEquals = configurationNameEquals;
	}


	public String getConfigurationDescription() {
		return this.configurationDescription;
	}


	public void setConfigurationDescription(String configurationDescription) {
		this.configurationDescription = configurationDescription;
	}


	public Boolean getFixPostTradeAllocation() {
		return this.fixPostTradeAllocation;
	}


	public void setFixPostTradeAllocation(Boolean fixPostTradeAllocation) {
		this.fixPostTradeAllocation = fixPostTradeAllocation;
	}


	public Boolean getFixDisablePreTradeAllocations() {
		return this.fixDisablePreTradeAllocations;
	}


	public void setFixDisablePreTradeAllocations(Boolean fixDisablePreTradeAllocations) {
		this.fixDisablePreTradeAllocations = fixDisablePreTradeAllocations;
	}


	public Boolean getFixTradeGroupingAllowed() {
		return this.fixTradeGroupingAllowed;
	}


	public void setFixTradeGroupingAllowed(Boolean fixTradeGroupingAllowed) {
		this.fixTradeGroupingAllowed = fixTradeGroupingAllowed;
	}


	public Boolean getFixTradeGroupingByHoldingAccount() {
		return this.fixTradeGroupingByHoldingAccount;
	}


	public void setFixTradeGroupingByHoldingAccount(Boolean fixTradeGroupingByHoldingAccount) {
		this.fixTradeGroupingByHoldingAccount = fixTradeGroupingByHoldingAccount;
	}


	public Boolean getFixExecutingAccountUsed() {
		return this.fixExecutingAccountUsed;
	}


	public void setFixExecutingAccountUsed(Boolean fixExecutingAccountUsed) {
		this.fixExecutingAccountUsed = fixExecutingAccountUsed;
	}


	public Boolean getFixAllocationReportExpected() {
		return this.fixAllocationReportExpected;
	}


	public void setFixAllocationReportExpected(Boolean fixAllocationReportExpected) {
		this.fixAllocationReportExpected = fixAllocationReportExpected;
	}


	public CommissionTypes getFixCommissionType() {
		return this.fixCommissionType;
	}


	public void setFixCommissionType(CommissionTypes fixCommissionType) {
		this.fixCommissionType = fixCommissionType;
	}


	public ProcessCodes getFixProcessCode() {
		return this.fixProcessCode;
	}


	public void setFixProcessCode(ProcessCodes fixProcessCode) {
		this.fixProcessCode = fixProcessCode;
	}


	public BigDecimal getFixDefaultCommissionPerUnit() {
		return this.fixDefaultCommissionPerUnit;
	}


	public void setFixDefaultCommissionPerUnit(BigDecimal fixDefaultCommissionPerUnit) {
		this.fixDefaultCommissionPerUnit = fixDefaultCommissionPerUnit;
	}


	public AllocationTypes getFixAllocationType() {
		return this.fixAllocationType;
	}


	public void setFixAllocationType(AllocationTypes fixAllocationType) {
		this.fixAllocationType = fixAllocationType;
	}


	public Boolean getFixAllocationCompleteWhenStatusIsReceived() {
		return this.fixAllocationCompleteWhenStatusIsReceived;
	}


	public void setFixAllocationCompleteWhenStatusIsReceived(Boolean fixAllocationCompleteWhenStatusIsReceived) {
		this.fixAllocationCompleteWhenStatusIsReceived = fixAllocationCompleteWhenStatusIsReceived;
	}


	public Boolean getFixAllocationOrderQuantityRequiredInNoOrdersGroup() {
		return this.fixAllocationOrderQuantityRequiredInNoOrdersGroup;
	}


	public void setFixAllocationOrderQuantityRequiredInNoOrdersGroup(Boolean fixAllocationOrderQuantityRequiredInNoOrdersGroup) {
		this.fixAllocationOrderQuantityRequiredInNoOrdersGroup = fixAllocationOrderQuantityRequiredInNoOrdersGroup;
	}


	public Boolean getFixAllocationPriceRequiredInAllocationDetails() {
		return this.fixAllocationPriceRequiredInAllocationDetails;
	}


	public void setFixAllocationPriceRequiredInAllocationDetails(Boolean fixAllocationPriceRequiredInAllocationDetails) {
		this.fixAllocationPriceRequiredInAllocationDetails = fixAllocationPriceRequiredInAllocationDetails;
	}


	public Boolean getFixCurrencyCodeRequiredOnAllocationMessages() {
		return this.fixCurrencyCodeRequiredOnAllocationMessages;
	}


	public void setFixCurrencyCodeRequiredOnAllocationMessages(Boolean fixCurrencyCodeRequiredOnAllocationMessages) {
		this.fixCurrencyCodeRequiredOnAllocationMessages = fixCurrencyCodeRequiredOnAllocationMessages;
	}


	public Boolean getFixMICExchangeCodeRequiredOnAllocationMessages() {
		return this.fixMICExchangeCodeRequiredOnAllocationMessages;
	}


	public void setFixMICExchangeCodeRequiredOnAllocationMessages(Boolean fixMICExchangeCodeRequiredOnAllocationMessages) {
		this.fixMICExchangeCodeRequiredOnAllocationMessages = fixMICExchangeCodeRequiredOnAllocationMessages;
	}


	public Boolean getFixCFICodeRequiredOnAllocationMessages() {
		return this.fixCFICodeRequiredOnAllocationMessages;
	}


	public void setFixCFICodeRequiredOnAllocationMessages(Boolean fixCFICodeRequiredOnAllocationMessages) {
		this.fixCFICodeRequiredOnAllocationMessages = fixCFICodeRequiredOnAllocationMessages;
	}


	public Boolean getFixSecondaryClientOrderIdUsedForAllocations() {
		return this.fixSecondaryClientOrderIdUsedForAllocations;
	}


	public void setFixSecondaryClientOrderIdUsedForAllocations(Boolean fixSecondaryClientOrderIdUsedForAllocations) {
		this.fixSecondaryClientOrderIdUsedForAllocations = fixSecondaryClientOrderIdUsedForAllocations;
	}


	public Boolean getFixSendTopAccountOnAllocationMessages() {
		return this.fixSendTopAccountOnAllocationMessages;
	}


	public void setFixSendTopAccountOnAllocationMessages(Boolean fixSendTopAccountOnAllocationMessages) {
		this.fixSendTopAccountOnAllocationMessages = fixSendTopAccountOnAllocationMessages;
	}


	public Boolean getFixMICExchangeCodeRequiredOnOrderMessages() {
		return this.fixMICExchangeCodeRequiredOnOrderMessages;
	}


	public void setFixMICExchangeCodeRequiredOnOrderMessages(Boolean fixMICExchangeCodeRequiredOnOrderMessages) {
		this.fixMICExchangeCodeRequiredOnOrderMessages = fixMICExchangeCodeRequiredOnOrderMessages;
	}


	public Boolean getFixSendBrokerPartyIdsOnAllocationMessages() {
		return this.fixSendBrokerPartyIdsOnAllocationMessages;
	}


	public void setFixSendBrokerPartyIdsOnAllocationMessages(Boolean fixSendBrokerPartyIdsOnAllocationMessages) {
		this.fixSendBrokerPartyIdsOnAllocationMessages = fixSendBrokerPartyIdsOnAllocationMessages;
	}


	public Boolean getSendClientAccountWithOrder() {
		return this.sendClientAccountWithOrder;
	}


	public void setSendClientAccountWithOrder(Boolean sendClientAccountWithOrder) {
		this.sendClientAccountWithOrder = sendClientAccountWithOrder;
	}


	public Boolean getSendRestrictiveBrokerList() {
		return this.sendRestrictiveBrokerList;
	}


	public void setSendRestrictiveBrokerList(Boolean sendRestrictiveBrokerList) {
		this.sendRestrictiveBrokerList = sendRestrictiveBrokerList;
	}


	public Boolean getBrokerRestrictedToRelatedHoldingAccountList() {
		return this.brokerRestrictedToRelatedHoldingAccountList;
	}


	public void setBrokerRestrictedToRelatedHoldingAccountList(Boolean brokerRestrictedToRelatedHoldingAccountList) {
		this.brokerRestrictedToRelatedHoldingAccountList = brokerRestrictedToRelatedHoldingAccountList;
	}


	public Boolean getSearchCompanyHierarchyForBrokerMatch() {
		return this.searchCompanyHierarchyForBrokerMatch;
	}


	public void setSearchCompanyHierarchyForBrokerMatch(Boolean searchCompanyHierarchyForBrokerMatch) {
		this.searchCompanyHierarchyForBrokerMatch = searchCompanyHierarchyForBrokerMatch;
	}


	public Boolean getFixUseSymbolForEquityOrders() {
		return this.fixUseSymbolForEquityOrders;
	}


	public void setFixUseSymbolForEquityOrders(Boolean fixUseSymbolForEquityOrders) {
		this.fixUseSymbolForEquityOrders = fixUseSymbolForEquityOrders;
	}


	public Boolean getFixSendClearingFirmLEIWithAllocation() {
		return this.fixSendClearingFirmLEIWithAllocation;
	}


	public void setFixSendClearingFirmLEIWithAllocation(Boolean fixSendClearingFirmLEIWithAllocation) {
		this.fixSendClearingFirmLEIWithAllocation = fixSendClearingFirmLEIWithAllocation;
	}
}
