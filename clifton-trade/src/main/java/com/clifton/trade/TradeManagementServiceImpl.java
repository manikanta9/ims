package com.clifton.trade;

import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditUpdateObserver;
import com.clifton.trade.util.TradeFillUtilHandler;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


/**
 * The <code>TradeManagementServiceImpl</code> class provides basic implementation of the {@link TradeManagementService} interface.
 *
 * @author michaelm
 */
@Service
public class TradeManagementServiceImpl implements TradeManagementService {

	protected static final Set<String> REASSIGNABLE_WORKFLOW_STATUS_NAME_LIST = CollectionUtils.createHashSet(WorkflowStatus.STATUS_DRAFT, WorkflowStatus.STATUS_PENDING, WorkflowStatus.STATUS_APPROVED);

	private TradeService tradeService;
	private SecurityAuthorizationService securityAuthorizationService;
	private TradeFillUtilHandler tradeFillUtilHandler;
	private WorkflowTransitionService workflowTransitionService;


	////////////////////////////////////////////////////////////////////////////
	////////////////         Trade Action Methods          /////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(timeout = 180)
	public Status executeTradeListActionCommand(TradeListActionCommand command) {
		Status status = new Status();

		ValidationUtils.assertNotNull(command.getActionType(), "An action type is required.");
		switch (command.getActionType()) {
			case FILL:
			case FILL_EXECUTE:
				fillTradeByCommand(command, status);
				break;
			case REASSIGN:
				executeTradeReassign(command, status);
		}
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A method used to fill one or more trades using an ExecuteTradeActionCommand.
	 *
	 * @param command - ExecuteTradeActionCommand containing an action type of FILL or FILL_EXECUTE, a trade list to fill, and a fill price.
	 *                The FILL action type fills the trade but does not change its workflow state, whereas FILL_EXECUTE will fill the trade and transition it's workflow state to "Executed Valid".
	 * @param status  - A Status entity that will be updated as trades are filled.
	 */
	@Transactional(timeout = 180)
	protected void fillTradeByCommand(TradeListActionCommand command, Status status) {
		validateTradeListForFillAction(command.getBeanList());
		ValidationUtils.assertNotNull(command.getFillPrice(), "A fill price is required when processing the FILL action.");
		for (Trade trade : command.getBeanList()) {
			try {
				trade.setAverageUnitPrice(command.getFillPrice());
				Trade savedTrade = getTradeService().saveTrade(trade);
				TradeFill tradeFill = getTradeFillUtilHandler().createTradeFill(savedTrade, false);
				getTradeFillUtilHandler().validateTradeFill(savedTrade, tradeFill);
				tradeFill = getTradeService().saveTradeFill(tradeFill);
				status.addDetail("Success", "Filled trade with id: " + savedTrade.getId());
				if (command.getActionType() == TradeListActionTypes.FILL_EXECUTE) {
					WorkflowTransition transition = getWorkflowTransitionService().getWorkflowTransitionNext(tradeFill.getTrade(), new StringBuilder());
					getWorkflowTransitionService().executeWorkflowTransitionByTransition("Trade", tradeFill.getTrade().getId(), transition.getId());
				}
			}
			catch (Exception exc) {
				status.addError("Error encountered processing trade fill for trade: " + trade.getId() + ": " + exc.getMessage());
			}
		}
		status.setStatusTitle("Trade Fill Status");
		if (status.getErrorCount() == 0) {
			status.setMessage("Successfully processed " + command.getBeanList().size() + " trade fills.");
		}
		else {
			status.setMessage("Process completed with errors.");
		}
	}


	@Transactional
	protected void executeTradeReassign(TradeListActionCommand command, Status status) {
		List<Trade> tradeList = command.getBeanList();
		CollectionUtils.getIterable(tradeList).forEach(trade -> {
			try {
				reassignTrade(command.getNewAssignee(), trade);
			}
			catch (Exception exc) {
				status.addError("Error encountered while reassigning trade: " + exc.getMessage());
			}
		});

		if (status.getErrorCount() == 0) {
			status.setMessage("Successfully processed " + command.getBeanList().size() + " trade reassignments.");
		}
		else {
			status.setMessage("Process completed with errors.");
		}
	}


	private void validateTradeListForFillAction(List<Trade> tradeList) {
		if (tradeList != null && tradeList.size() > 1) {
			Trade firstTrade = CollectionUtils.getFirstElementStrict(tradeList);
			ValidationUtils.assertEquals(TradeService.TRADE_AWAITING_FILLS_STATE_NAME, firstTrade.getWorkflowState().getName(), "Selected trades must be in \"Awaiting Fills\" workflow state.");
			for (int index = 1; index < tradeList.size(); ++index) {
				ValidationUtils.assertEquals(TradeService.TRADE_AWAITING_FILLS_STATE_NAME, tradeList.get(index).getWorkflowState().getName(), "Selected trades must be in \"Awaiting Fills\" workflow state.");
				ValidationUtils.assertEquals(firstTrade.getInvestmentSecurity(), tradeList.get(index).getInvestmentSecurity(), "Trades submitted for fills must all have the same investment security.");
				ValidationUtils.assertEquals(firstTrade.isBuy(), tradeList.get(index).isBuy(), "Trades submitted for fills must all have the same side (buy or sell).");
			}
		}
	}


	private Trade reassignTrade(SecurityUser newAssignee, Trade trade) {
		validateTradeReassignableToNewAssignee(newAssignee, trade);
		trade.setTraderUser(newAssignee);
		return DaoUtils.executeWithSpecificObserversEnabled(() -> getTradeService().saveTrade(trade), SystemDaoAuditUpdateObserver.class);
	}


	private void validateTradeReassignableToNewAssignee(SecurityUser newAssignee, Trade trade) {
		validateNewAssigneeHasWriteAccess(newAssignee, String.format("Trade [%s]", trade.getLabel()));
		validateWorkflowStatusReassignable(trade);
	}


	private void validateNewAssigneeHasWriteAccess(SecurityUser newAssignee, String entityLabel) {
		ValidationUtils.assertTrue(getSecurityAuthorizationService().isSecurityAccessAllowed(newAssignee.getUserName(), Trade.class.getSimpleName(), SecurityPermission.PERMISSION_WRITE),
				String.format("%s cannot be reassigned to [%s] because they do not have write permissions to [%s].",
						entityLabel, newAssignee.getLabel(), Trade.class.getName()));
	}


	private void validateWorkflowStatusReassignable(Trade trade) {
		String workflowStatusName = trade.getWorkflowStatus().getName();
		ValidationUtils.assertTrue(REASSIGNABLE_WORKFLOW_STATUS_NAME_LIST.contains(workflowStatusName), String.format("Trade [%s] with Workflow Status [%s] cannot be reassigned because Trades can only be reassigned in workflow status [%s].",
				trade.getLabel(), workflowStatusName, StringUtils.join(REASSIGNABLE_WORKFLOW_STATUS_NAME_LIST, ", ")));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public TradeFillUtilHandler getTradeFillUtilHandler() {
		return this.tradeFillUtilHandler;
	}


	public void setTradeFillUtilHandler(TradeFillUtilHandler tradeFillUtilHandler) {
		this.tradeFillUtilHandler = tradeFillUtilHandler;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
