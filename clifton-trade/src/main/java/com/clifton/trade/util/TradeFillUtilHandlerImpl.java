package com.clifton.trade.util;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


/**
 * @author NickK
 * @see TradeFillUtilHandler
 */
@Component
public class TradeFillUtilHandlerImpl implements TradeFillUtilHandler {

	private InvestmentCalculator investmentCalculator;
	private InvestmentCurrencyService investmentCurrencyService;
	private MarketDataRetriever marketDataRetriever;
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////              Trade Fill Handler Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns an auto-generated {@link TradeFill} for the given {@link Trade}. If insufficient data exists then no fill will be generated and {@code null} will be returned.
	 * <p>
	 * {@link TradeFill} generation is based primarily on the {@link Trade#averageUnitPrice average unit price} field, but may also be based on the {@link Trade#accountingNotional
	 * accounting notional} field for currencies. Additional price fallbacks may also be used if designated.
	 *
	 * @param useAdditionalPriceFallbacks if {@code true}, coalesce the {@link Trade#expectedUnitPrice expected unit price} and the latest market data price as fallback options if
	 *                                    the {@link Trade#averageUnitPrice average unit price} is not available
	 * @return the generated {@link TradeFill}, or {@code null} if no fill could be generated
	 */
	@Override
	public TradeFill createTradeFill(Trade bean, boolean useAdditionalPriceFallbacks) {
		// get fully populated trade
		Trade trade = getTradeService().getTrade(bean.getId());
		TradeFill fill = null;
		BigDecimal price = trade.getAverageUnitPrice();
		if (price == null && useAdditionalPriceFallbacks) {
			price = trade.getExpectedUnitPrice();
			if (price == null) {
				price = getMarketDataRetriever().getPriceFlexible(bean.getInvestmentSecurity(), bean.getTradeDate(), false);
			}
		}
		if (trade.getQuantityIntended() != null && price != null) {
			fill = new TradeFill();
			fill.setTrade(trade);
			fill.setQuantity(trade.getQuantityIntended());
			fill.setNotionalUnitPrice(price);
		}
		else if (trade.getQuantityIntended() == null && trade.getAccountingNotional() != null) {
			if (trade.getInvestmentSecurity().isCurrency()) { // currency forwards may also not have quantity before forward rate is known
				// currency trades
				fill = new TradeFill();
				fill.setTrade(trade);
				fill.setNotionalTotalPrice(trade.getAccountingNotional());
				boolean multiply = getInvestmentCurrencyService().isInvestmentCurrencyMultiplyConvention(getSettlementCurrency(trade).getSymbol(), trade.getInvestmentSecurity().getSymbol());
				if (multiply) {
					fill.setPaymentTotalPrice(InvestmentCalculatorUtils.calculateBaseAmount(trade.getAccountingNotional(), MathUtils.divide(BigDecimal.ONE, trade.getExchangeRateToBase()), trade.getClientInvestmentAccount()));
				}
				else {
					fill.setPaymentTotalPrice(InvestmentCalculatorUtils.calculateBaseAmount(trade.getAccountingNotional(), trade.getExchangeRateToBase(), trade.getClientInvestmentAccount()));
				}
			}
		}
		return fill;
	}


	@Override
	public TradeFill validateTradeFill(Trade trade, TradeFill fill) {
		boolean fillUpdateAllowed = !fill.isNewBean() && trade.getTradeType().isSingleFillTrade();

		ValidationUtils.assertTrue(fillUpdateAllowed || (trade.getWorkflowStatus() == null || !TradeService.TRADES_CLOSED_STATUS_NAME.equals(trade.getWorkflowStatus().getName())),
				"Cannot create a fill for a trade in 'Closed' workflow status.");

		if (trade.getQuantityIntended() == null) {
			// currency exchanges don't have quantity
			ValidationUtils.assertNotNull(fill.getNotionalTotalPrice(), "Trade fill notional total price is required for trades with no quantity.");
			ValidationUtils.assertNotNull(fill.getPaymentTotalPrice(), "Trade fill payment total price is required for trades with no quantity.");
		}
		else {
			ValidationUtils.assertNotNull(fill.getQuantity(), "Trade fill quantity is required.");
			ValidationUtils.assertTrue(MathUtils.isGreaterThan(fill.getQuantity(), BigDecimal.ZERO), "Trade fill quantity must be greater than 0.");
			ValidationUtils.assertNotNull(fill.getNotionalUnitPrice(), "Trade fill unit price is required.");

			InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(trade.getInvestmentSecurity().getInstrument().getTradingCurrency());
			BigDecimal notionalTotalPrice = getInvestmentCalculator().calculateNotional(trade.getInvestmentSecurity(), fill.getNotionalUnitPrice(), fill.getQuantity(), trade.getNotionalMultiplier(), roundingCalculator);
			if (fill.getNotionalTotalPrice() == null || fillUpdateAllowed) {
				if (MathUtils.isEqual(trade.getQuantityIntended(), fill.getQuantity()) && trade.getAccountingNotional() != null) {
					// single fill trade: fill notional must equal to trade notional (allow for one penny rounding during validation)
					if (MathUtils.isLessThanOrEqual(notionalTotalPrice.subtract(trade.getAccountingNotional()).abs(), BigDecimal.valueOf(0.01))) {
						notionalTotalPrice = trade.getAccountingNotional();
					}
				}
				fill.setNotionalTotalPrice(notionalTotalPrice);
			}
			else {
				ValidationUtils.assertTrue(MathUtils.isEqual(notionalTotalPrice, fill.getNotionalTotalPrice()), "Notional total price of the fill " + fill.getNotionalTotalPrice()
						+ " must equal to Quantity * ROUNDUP(Multiplier * Unit Price, 2) = " + notionalTotalPrice);
			}

			if (InvestmentUtils.isNoPaymentOnOpen(trade.getInvestmentSecurity())) {
				if (fill.getPaymentUnitPrice() == null) {
					fill.setPaymentUnitPrice(BigDecimal.ZERO);
				}
				if (fill.getPaymentTotalPrice() == null) {
					fill.setPaymentTotalPrice(BigDecimal.ZERO);
				}
				ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, fill.getPaymentUnitPrice()), "Payment unit price must be 0 for securities that have no payment on open");
				ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, fill.getPaymentTotalPrice()), "Payment total price must be 0 for securities that have no payment on open");
			}
			else {
				if (fill.getPaymentUnitPrice() == null) {
					fill.setPaymentUnitPrice(fill.getNotionalUnitPrice());
				}
				if (fill.getPaymentTotalPrice() == null || fillUpdateAllowed) {
					fill.setPaymentTotalPrice(fill.getNotionalTotalPrice());
				}
				ValidationUtils.assertTrue(MathUtils.isEqual(fill.getNotionalUnitPrice(), fill.getPaymentUnitPrice()), "Payment unit price " + fill.getPaymentUnitPrice()
						+ " must equal to notional unit price " + fill.getNotionalUnitPrice() + " for securities that have payment on open");
				ValidationUtils.assertTrue(MathUtils.isEqual(fill.getNotionalTotalPrice(), fill.getPaymentTotalPrice()), "Payment total price " + fill.getPaymentTotalPrice()
						+ " must equal to notional total price " + fill.getNotionalTotalPrice() + " for securities that have payment on open");
			}
		}

		ValidationUtils.assertFalse(!fill.isLotSpecificClosing() && (fill.getOpeningDate() != null || fill.getOpeningPrice() != null), "Cannot save trade fill because [openingDate] and [openingPrice] must both be set or both be empty.", "openingDate");
		return fill;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Helper Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the settlement currency for this trade. If necessary converts into system specific form industry convention.
	 * <p>
	 * There are 2 different ways to buy or sell a forward and it's up to the trader to determine this.
	 * For example, USD denominated forward for EUR can be traded by "BUY EUR" or "SELL USD"
	 * From systems perspective we either BUY or SELL the forward and we always need to convert back to this convention based on "Settlement Currency"
	 */
	private InvestmentSecurity getSettlementCurrency(Trade trade) {
		InvestmentSecurity result = trade.getSettlementCurrency();
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			result = trade.getInvestmentSecurity().getInstrument().getTradingCurrency();
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentCurrencyService getInvestmentCurrencyService() {
		return this.investmentCurrencyService;
	}


	public void setInvestmentCurrencyService(InvestmentCurrencyService investmentCurrencyService) {
		this.investmentCurrencyService = investmentCurrencyService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
