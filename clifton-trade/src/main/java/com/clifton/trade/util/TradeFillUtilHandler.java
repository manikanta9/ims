package com.clifton.trade.util;

import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;


/**
 * <code>TradeFillUtilHandler</code> assists in creating and validating {@link TradeFill}.
 *
 * @author NickK
 */
public interface TradeFillUtilHandler {

	/**
	 * Returns a {@link TradeFill} constructed from the provided {@link Trade};
	 */
	public TradeFill createTradeFill(Trade trade, boolean allowExpectedUnitPrice);


	/**
	 * Validates the provided {@link TradeFill} for completeness and correctness
	 * based on the properties of the fill's provided {@link Trade}.
	 * <p>
	 * Properties of the fill that can be populated will be according to the provided
	 * arguments.
	 */
	public TradeFill validateTradeFill(Trade trade, TradeFill fill);
}
