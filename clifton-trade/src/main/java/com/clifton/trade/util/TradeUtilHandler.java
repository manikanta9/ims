package com.clifton.trade.util;

import com.clifton.trade.Trade;


/**
 * <code>TradeUtilHandler</code> assists in updating and validating {@link Trade}s.
 *
 * @author mwacker
 */
public interface TradeUtilHandler {

	public void validateTrade(Trade trade);


	/**
	 * Validates that the correct trade type is on the trade.
	 */
	public void validateTradeTypeForBooking(Trade trade);
}
