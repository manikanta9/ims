package com.clifton.trade.investment.security.group.rebuild;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAware;
import com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor;
import com.clifton.trade.roll.setup.TradeRollSetupService;
import com.clifton.trade.roll.setup.TradeRollType;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentSecurityGroupTradeRollTypeRebuildAwareExecutor</code> is an {@link EntityGroupRebuildAwareExecutor} bean that
 * looks up the {@link com.clifton.trade.roll.setup.TradeRollType} associated with its {@link InvestmentSecurityGroup} and updates the corresponding list
 * of current {@link InvestmentSecurity}s.
 *
 * @author michaelm
 * @see InvestmentSecurityGroupSecurity
 */
public class InvestmentSecurityGroupTradeRollTypeRebuildAwareExecutor implements EntityGroupRebuildAwareExecutor {

	private static final String TRADE_ROLL_TYPE_REBUILD_EXECUTOR_NAME = "Trade Roll Type Investment Security Group Rebuild Executor";

	private TradeRollSetupService tradeRollSetupService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	@Override
	public void validate(EntityGroupRebuildAware groupEntity) {
		ValidationUtils.assertTrue(groupEntity instanceof InvestmentSecurityGroup, "Rebuild executor applies to InvestmentSecurityGroups only.");
	}


	@Transactional // transactional so all edits to the group are committed/rolled back together.
	@Override
	public Status executeRebuild(EntityGroupRebuildAware groupEntity, Status status) {
		validate(groupEntity);

		InvestmentSecurityGroup investmentSecurityGroup = (InvestmentSecurityGroup) groupEntity;
		List<TradeRollType> tradeRollTypeList = getTradeRollSetupService().getTradeRollTypeListBySecurityGroupId(investmentSecurityGroup.getId());
		TradeRollType tradeRollType = CollectionUtils.getFirstElementStrict(tradeRollTypeList,
				String.format("Unable to rebuild Investment Security Group using [%s] because there are [%d] Trade Roll Types associated with [%s]", TRADE_ROLL_TYPE_REBUILD_EXECUTOR_NAME, CollectionUtils.getSize(tradeRollTypeList), investmentSecurityGroup.getLabel()));
		List<InvestmentSecurity> securitiesToAddList = getRebuildDefaultNewSecurityList(tradeRollType);
		List<InvestmentSecurityGroupSecurity> currentInvestmentSecurityGroupSecurityList = getInvestmentSecurityGroupService().getInvestmentSecurityGroupSecurityListByGroup(investmentSecurityGroup.getId());
		for (InvestmentSecurityGroupSecurity securityGroupSecurity : CollectionUtils.getIterable(currentInvestmentSecurityGroupSecurityList)) {
			/*
			 * Remove the security from securitiesToAddList because it is already included in the security group.
			 * This will avoid adding a duplicate row.
			 */
			if (!securitiesToAddList.remove(securityGroupSecurity.getReferenceTwo())) {
				/*
				 * Delete the security from the security group because it is no longer the new security.
				 */
				getInvestmentSecurityGroupService().deleteInvestmentSecurityGroupSecurity(securityGroupSecurity);
			}
		}
		/*
		 * Add securities to the security group that previously did not exist in the group.
		 */
		getInvestmentSecurityGroupService().linkInvestmentSecurityGroupToSecurityList(investmentSecurityGroup, securitiesToAddList);
		status.addMessage("Executed rebuild of investment security group: " + investmentSecurityGroup.getName());
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<InvestmentSecurity> getRebuildDefaultNewSecurityList(TradeRollType tradeRollType) {
		return CollectionUtils.asNonNullList(getTradeRollSetupService().getTradeRollTypeCurrentSecurityList(tradeRollType.getId(), new Date()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRollSetupService getTradeRollSetupService() {
		return this.tradeRollSetupService;
	}


	public void setTradeRollSetupService(TradeRollSetupService tradeRollSetupService) {
		this.tradeRollSetupService = tradeRollSetupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}
}
