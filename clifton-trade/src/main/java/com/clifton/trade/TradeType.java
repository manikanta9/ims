package com.clifton.trade;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.specificity.InvestmentSpecificityDefinition;
import com.clifton.report.definition.Report;


/**
 * The <code>TradeType</code> class represents a type of a trade (future, stock, option, bond, etc.)
 * and defines meta-data used for trades of this type.
 *
 * @author vgomelsky
 */
@CacheByName
public class TradeType extends NamedEntity<Short> {

	public static final String BONDS = "Bonds";
	public static final String CURRENCY = "Currency";
	public static final String FORWARDS = "Forwards";
	public static final String FUNDS = "Funds";
	public static final String FUTURES = "Futures";
	public static final String NOTES = "Notes";
	public static final String OPTIONS = "Options";
	public static final String STOCKS = "Stocks";

	public static final String SWAPS = "Swaps";
	public static final String CREDIT_DEFAULT_SWAPS = "Swaps: CDS";
	public static final String INTEREST_RATE_SWAPS = "Swaps: IRS";
	public static final String TOTAL_RETURN_SWAPS = "Swaps: TRS";
	public static final String CLEARED_CREDIT_DEFAULT_SWAPS = "Cleared: CDS";
	public static final String CLEARED_INTEREST_RATE_SWAPS = "Cleared: IRS";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Limits securities managed by this Trade Type to those of the specified Investment Type.
	 */
	private InvestmentType investmentType;

	/**
	 * Optional Instrument Group that further limits securities for selected Investment Type.
	 * Used for cases where trade type -> investment type is not 1:1.  i.e Swaps (TRS, Credit Default, etc).
	 */
	private InvestmentGroup investmentGroup;

	/**
	 * Restricts client/holding account selection to active relationships with this purpose.
	 */
	private InvestmentAccountRelationshipPurpose holdingAccountPurpose;

	/**
	 * Optional trade report defined for this trade type.  When defined,
	 * users can pull the report directly from the Trade Match screen.
	 */
	private Report tradeReport;


	/**
	 * Some securities may require Exchange Rate from a very specific date for foreign transactions (Trade Date for Cleared CDS).
	 */
	private TradeExchangeRateDateSelectors exchangeRateDateSelector;

	/**
	 * Specifies whether trades of this type follow industry standard exchange rate currency convention.
	 * If true, then conversion from local currency to base currency will take into account the dominant currency before
	 * deciding whether local amount should be multiplied or divided by exchange rate.
	 * If false, then local currency amount is always multiplied by the exchange rate to get to base currency amount.
	 * Note: virtually all other places in the system always multiply local by FX to get to base currency (do not follow convention).
	 * This is done to simplify calculations and in some rare cases can result in loss of precision: can use source (trade) to look it up.
	 */
	private boolean currencyConventionFollowed;

	/**
	 * If true, all trades of this type will always have 1 fill (currency, swap, etc.).
	 * This also means that the fill doesn't really apply to trades of this type.
	 */
	private boolean singleFillTrade;

	/**
	 * Determines if a trade of this type can create a short position when it's currently long or vice versa.
	 */
	private boolean crossingZeroAllowed;

	/**
	 * Determines if the trade screen will allow the trader to explicitly set the Open/Close on a trade.
	 */
	private boolean explicitOpenCloseSelectionAllowed;

	/**
	 * Determines if the trade screen will require the trader to explicitly set the Open/Close on a trade.
	 */
	private boolean explicitOpenCloseSelectionRequired;

	/**
	 * Specifies whether trades of this type support execution using FIX protocol.
	 */
	private boolean fixSupported;

	/**
	 * For this trade type an execution type is required.  For example, Forward require either CLS or NON-CLS.
	 */
	private boolean executionTypeRequired;

	/**
	 * Specifies whether trades of this type can also be used as Collateral (bonds and stocks).
	 */
	private boolean collateralSupported;

	/**
	 * Allow trades to be saved with no executing broker, and update it after execution is complete.
	 * <p>
	 * This is used for Currency Forwards, Currency and Bond trades where we can execute with multiple brokers and it's not know
	 * which until the time trade is executed.
	 */
	private boolean brokerSelectionAtExecutionAllowed;

	/**
	 * Allow trades to be saved with no holding account, and update it after execution is complete.
	 * <p>
	 * This is used for Currency Forwards and Currency trades where we can execute with multiple brokers and it's not know
	 * which until the time trade is executed.
	 */
	private boolean holdingAccountSelectionAtExecutionAllowed;

	/**
	 * Indicates an order that does not have the final security at the time of creation (such as Cleared IRS) and the security will
	 * be created after the order is executed.
	 */
	private boolean securityCreationAtExecutionAllowed;


	/**
	 * The specificity definition that will be used to retrieve the Freemark FpML of order execution via FIX and
	 * the InvestmentInstrumentHierarchy that will be used to create a new security if needed.
	 */
	private InvestmentSpecificityDefinition executionInvestmentSpecificityDefinition;

	/**
	 * Usually Notional amount field on the trade is positive and the direction is determined by the buy/sell field.
	 * However, some trade types (CDS, IRS) have complex calculations that may have buy or sell amount positive or negative.
	 * In those cases, UI logic will account for signs and buy/sell indicator will be ignored during booking for these fields.
	 * If the field is set then the positive amount means cash in-flow and negative amount means cash out-flow.
	 */
	private boolean amountsSignAccountsForBuy;
	/**
	 * Similar to {@link #amountsSignAccountsForBuy} but for Accrual Amount field. Usually accrual and notional sign logic
	 * is the same.  However, in some cases (TRS has true for accrual but false for notional) it maybe different.
	 */
	private boolean accrualSignAccountsForBuy;


	/**
	 * When Booking trades of this type use this GL Account for {@link Trade#commissionAmount} field
	 */
	private AccountingAccount commissionAccountingAccount;
	/**
	 * When Booking trades of this type use this GL Account for {@link Trade#feeAmount} field
	 */
	private AccountingAccount feeAccountingAccount;

	/**
	 * Different GL Accounts maybe used for accrualAmount on buy vs sell trades.
	 * For example, Interest Income vs Interest Expense for bond trades.
	 * These fields are set only for trade types that support accruals.
	 */
	private AccountingAccount buyAccrualAccountingAccount;
	private AccountingAccount sellAccrualAccountingAccount;

	/**
	 * A flag that indicates whether or not trades of this type support the use of the TradeExecutionType.
	 */
	private boolean tradeExecutionTypeSupported;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingAccount getCommissionAccountingAccount() {
		return this.commissionAccountingAccount;
	}


	public void setCommissionAccountingAccount(AccountingAccount commissionAccountingAccount) {
		this.commissionAccountingAccount = commissionAccountingAccount;
	}


	public AccountingAccount getFeeAccountingAccount() {
		return this.feeAccountingAccount;
	}


	public void setFeeAccountingAccount(AccountingAccount feeAccountingAccount) {
		this.feeAccountingAccount = feeAccountingAccount;
	}


	public boolean isFixSupported() {
		return this.fixSupported;
	}


	public void setFixSupported(boolean fixSupported) {
		this.fixSupported = fixSupported;
	}


	public boolean isCollateralSupported() {
		return this.collateralSupported;
	}


	public void setCollateralSupported(boolean collateralSupported) {
		this.collateralSupported = collateralSupported;
	}


	public InvestmentAccountRelationshipPurpose getHoldingAccountPurpose() {
		return this.holdingAccountPurpose;
	}


	public void setHoldingAccountPurpose(InvestmentAccountRelationshipPurpose holdingAccountPurpose) {
		this.holdingAccountPurpose = holdingAccountPurpose;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public boolean isSingleFillTrade() {
		return this.singleFillTrade;
	}


	public void setSingleFillTrade(boolean singleFillTrade) {
		this.singleFillTrade = singleFillTrade;
	}


	public boolean isCurrencyConventionFollowed() {
		return this.currencyConventionFollowed;
	}


	public void setCurrencyConventionFollowed(boolean currencyConventionFollowed) {
		this.currencyConventionFollowed = currencyConventionFollowed;
	}


	public AccountingAccount getBuyAccrualAccountingAccount() {
		return this.buyAccrualAccountingAccount;
	}


	public void setBuyAccrualAccountingAccount(AccountingAccount buyAccrualAccountingAccount) {
		this.buyAccrualAccountingAccount = buyAccrualAccountingAccount;
	}


	public AccountingAccount getSellAccrualAccountingAccount() {
		return this.sellAccrualAccountingAccount;
	}


	public void setSellAccrualAccountingAccount(AccountingAccount sellAccrualAccountingAccount) {
		this.sellAccrualAccountingAccount = sellAccrualAccountingAccount;
	}


	public boolean isAmountsSignAccountsForBuy() {
		return this.amountsSignAccountsForBuy;
	}


	public void setAmountsSignAccountsForBuy(boolean amountsSignAccountsForBuy) {
		this.amountsSignAccountsForBuy = amountsSignAccountsForBuy;
	}


	public boolean isAccrualSignAccountsForBuy() {
		return this.accrualSignAccountsForBuy;
	}


	public void setAccrualSignAccountsForBuy(boolean accrualSignAccountsForBuy) {
		this.accrualSignAccountsForBuy = accrualSignAccountsForBuy;
	}


	public InvestmentGroup getInvestmentGroup() {
		return this.investmentGroup;
	}


	public void setInvestmentGroup(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
	}


	public boolean isBrokerSelectionAtExecutionAllowed() {
		return this.brokerSelectionAtExecutionAllowed;
	}


	public void setBrokerSelectionAtExecutionAllowed(boolean brokerSelectionAtExecutionAllowed) {
		this.brokerSelectionAtExecutionAllowed = brokerSelectionAtExecutionAllowed;
	}


	public boolean isHoldingAccountSelectionAtExecutionAllowed() {
		return this.holdingAccountSelectionAtExecutionAllowed;
	}


	public void setHoldingAccountSelectionAtExecutionAllowed(boolean holdingAccountSelectionAtExecutionAllowed) {
		this.holdingAccountSelectionAtExecutionAllowed = holdingAccountSelectionAtExecutionAllowed;
	}


	public TradeExchangeRateDateSelectors getExchangeRateDateSelector() {
		return this.exchangeRateDateSelector;
	}


	public void setExchangeRateDateSelector(TradeExchangeRateDateSelectors exchangeRateDateSelector) {
		this.exchangeRateDateSelector = exchangeRateDateSelector;
	}


	public Report getTradeReport() {
		return this.tradeReport;
	}


	public void setTradeReport(Report tradeReport) {
		this.tradeReport = tradeReport;
	}


	public boolean isCrossingZeroAllowed() {
		return this.crossingZeroAllowed;
	}


	public void setCrossingZeroAllowed(boolean crossingZeroAllowed) {
		this.crossingZeroAllowed = crossingZeroAllowed;
	}


	public boolean isExplicitOpenCloseSelectionAllowed() {
		return this.explicitOpenCloseSelectionAllowed;
	}


	public void setExplicitOpenCloseSelectionAllowed(boolean explicitOpenCloseSelectionAllowed) {
		this.explicitOpenCloseSelectionAllowed = explicitOpenCloseSelectionAllowed;
	}


	public boolean isExplicitOpenCloseSelectionRequired() {
		return this.explicitOpenCloseSelectionRequired;
	}


	public void setExplicitOpenCloseSelectionRequired(boolean explicitOpenCloseSelectionRequired) {
		this.explicitOpenCloseSelectionRequired = explicitOpenCloseSelectionRequired;
	}


	public boolean isTradeExecutionTypeSupported() {
		return this.tradeExecutionTypeSupported;
	}


	public void setTradeExecutionTypeSupported(boolean tradeExecutionTypeSupported) {
		this.tradeExecutionTypeSupported = tradeExecutionTypeSupported;
	}


	public boolean isSecurityCreationAtExecutionAllowed() {
		return this.securityCreationAtExecutionAllowed;
	}


	public void setSecurityCreationAtExecutionAllowed(boolean securityCreationAtExecutionAllowed) {
		this.securityCreationAtExecutionAllowed = securityCreationAtExecutionAllowed;
	}


	public InvestmentSpecificityDefinition getExecutionInvestmentSpecificityDefinition() {
		return this.executionInvestmentSpecificityDefinition;
	}


	public void setExecutionInvestmentSpecificityDefinition(InvestmentSpecificityDefinition executionInvestmentSpecificityDefinition) {
		this.executionInvestmentSpecificityDefinition = executionInvestmentSpecificityDefinition;
	}


	public boolean isExecutionTypeRequired() {
		return this.executionTypeRequired;
	}


	public void setExecutionTypeRequired(boolean executionTypeRequired) {
		this.executionTypeRequired = executionTypeRequired;
	}
}
