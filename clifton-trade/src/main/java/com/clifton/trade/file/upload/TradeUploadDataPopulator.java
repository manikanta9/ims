package com.clifton.trade.file.upload;

import com.clifton.trade.Trade;


/**
 * The trade upload data retriever is used by simple and custom trade uploads to populate bean properties with real objects based on the properties populated
 * Example: Uploads usually only require populating the Client Account Number or Security Symbol, we can often try to determine the actual security based on symbol (and trade type if not unique), or try to find the correct holding account if not specified
 *
 * @author manderson
 */
public interface TradeUploadDataPopulator {


	/**
	 * Based on the not fully populated Trade object and upload properties, will populate missing data on the trade
	 * i.e. Client Account, Holding Account, Security, Trade Destination, etc.
	 */
	public void populateTrade(Trade trade, TradeUploadCommand uploadCommand);
}
