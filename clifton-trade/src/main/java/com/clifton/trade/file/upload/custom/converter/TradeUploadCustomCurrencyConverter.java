package com.clifton.trade.file.upload.custom.converter;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.converter.reversable.BooleanReversableConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.execution.TradeExecutionTypeService;
import com.clifton.trade.file.upload.custom.TradeUploadCustomCommand;
import com.clifton.trade.file.upload.custom.TradeUploadCustomCurrency;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>TradeUploadCustomForwardConverter</code> implements a converter for custom forwards uploads.
 *
 * @author mwacker
 */
@Component
public class TradeUploadCustomCurrencyConverter implements TradeUploadCustomConverter<TradeUploadCustomCurrency> {

	private BooleanReversableConverter tradeBuyColumnReversableConverter;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentCalculatorService investmentCalculatorService;
	private TradeDestinationService tradeDestinationService;
	private TradeExecutionTypeService tradeExecutionTypeService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountService investmentAccountService;
	private BusinessCompanyService businessCompanyService;


	@Override
	public Trade convert(TradeUploadCustomCurrency from, TradeUploadCustomCommand<TradeUploadCustomCurrency> uploadCommand) {
		TradeType tradeType = uploadCommand.getTradeUpload().getSimpleTradeType();
		ValidationUtils.assertNotNull(tradeType, "Trade type is required.");
		ValidationUtils.assertTrue(!TradeType.FORWARDS.equals(tradeType.getName()) || !StringUtils.isEmpty(from.getForwardSecuritySymbol()), "Forward security is required for trade type [" + tradeType.getLabel() + "].", "forwardSecuritySymbol");

		Trade trade = new Trade();
		// set the trade destination from the upload
		if ((uploadCommand.getTradeUpload().getSimpleTradeDestination() != null) && (uploadCommand.getTradeUpload().getSimpleTradeDestination().getId() != null)) {
			trade.setTradeDestination(uploadCommand.getTradeUpload().getSimpleTradeDestination());
		}
		trade.setTradeType(uploadCommand.getTradeUpload().getSimpleTradeType());
		// get the trade buy sell
		trade.setBuy(getTradeBuyColumnReversableConverter().convert(from.getBuy()));
		// set the trade date
		if (from.getTradeDate() != null) {
			trade.setTradeDate(from.getTradeDate());
		}
		else {
			trade.setTradeDate(uploadCommand.getTradeUpload().getTradeGroup().getTradeDate());
		}

		InvestmentAccount clientInvestmentAccount = new InvestmentAccount();
		clientInvestmentAccount.setNumber(from.getClientInvestmentAccountNumber());
		trade.setClientInvestmentAccount(clientInvestmentAccount);
		setClientInvestmentAccountForTrade(trade);

		// set the holding account
		String accountNumber = StringUtils.isEmpty(from.getHoldingInvestmentAccountNumber()) ? from.getIsdaInvestmentAccountNumber() : from.getHoldingInvestmentAccountNumber();
		if (!StringUtils.isEmpty(accountNumber)) {
			InvestmentAccount holdingAccount = new InvestmentAccount();
			holdingAccount.setNumber(accountNumber);
			trade.setHoldingInvestmentAccount(holdingAccount);
		}
		setHoldingAccountAndBrokerForTrade(trade);

		// get the currencies
		InvestmentSecurity givenCCY = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(from.getGivenCurrencySymbol(), true);
		InvestmentSecurity settlementCCY = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(from.getSettlementCurrencySymbol(), true);

		trade.setInvestmentSecurity(givenCCY);
		trade.setPayingSecurity(settlementCCY);


		// set the forward rate and calculate the settlement amount
		BigDecimal givenAmount = InvestmentCalculatorUtils.getNotionalCalculator(givenCCY).round(from.getGivenAmount());
		BigDecimal settlementAmount = null;
		if (from.getRate() != null) {
			settlementAmount = from.getSettlementAmount() != null ? InvestmentCalculatorUtils.getNotionalCalculator(settlementCCY).round(from.getSettlementAmount()) : getInvestmentCalculatorService().getInvestmentSecurityNotional(settlementCCY.getId(),
					from.getRate(), givenAmount, null);
		}


		// do the Forwards specific logic
		if (TradeType.FORWARDS.equals(tradeType.getName())) {
			// set the price
			trade.setAverageUnitPrice(from.getRate());

			InvestmentSecurity forwardSecurity = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(from.getForwardSecuritySymbol(), false);
			trade.setInvestmentSecurity(forwardSecurity);
			if (forwardSecurity == null) {
				throw new ValidationException("Cannot find Active Security on " + DateUtils.fromDateShort(trade.getTradeDate()) + " with Symbol [" + from.getForwardSecuritySymbol() + "] and Investment Type ["
						+ trade.getTradeType().getHoldingAccountPurpose().getInvestmentType().getName() + "].");
			}
			// flip the buy/sell is the given currency equals the trading currency
			if (givenCCY.equals(forwardSecurity.getInstrument().getTradingCurrency())) {
				trade.setBuy(!trade.isBuy());
			}
			// set the correct quantity and notional values
			if (settlementCCY.equals(forwardSecurity.getInstrument().getTradingCurrency())) {
				trade.setQuantityIntended(givenAmount);
				trade.setAccountingNotional(settlementAmount);
			}
			else {
				trade.setAccountingNotional(givenAmount);
				trade.setQuantityIntended(settlementAmount);
			}
		}
		else {
			trade.setExchangeRateToBase(from.getRate());
			trade.setAccountingNotional(settlementAmount);
		}

		trade.setRoll(from.isRoll());

		// set the trade destination
		if (!StringUtils.isEmpty(from.getTradeDestinationName())) {
			trade.setTradeDestination(getTradeDestinationService().getTradeDestinationByName(from.getTradeDestinationName()));
		}
		if (!StringUtils.isEmpty(from.getTradeExecutionTypeName())) {
			trade.setTradeExecutionType(getTradeExecutionTypeService().getTradeExecutionTypeByName(from.getTradeExecutionTypeName()));
		}

		// Set executing broker company, which depends on TradeExecutionType
		setExecutingBrokerCompany(trade, uploadCommand.getTradeUpload().getSimpleExecutingBrokerCompany(), from.getExecutingBrokerCompanyName());

		// Set tradeDestination, which depends on executing broker.
		setTradeDestination(trade);

		uploadCommand.getTradeUpload().applyUploadSpecificProperties(trade, null);
		return trade;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setHoldingAccountAndBrokerForTrade(Trade trade) {
		InvestmentAccount account = trade.getHoldingInvestmentAccount();
		InvestmentAccount result = null;

		if (account == null || StringUtils.isEmpty(account.getNumber())) {
			// Try to figure out which holding account to use
			List<InvestmentAccount> accountList = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(trade.getClientInvestmentAccount().getId(), null,
					trade.getTradeType().getHoldingAccountPurpose().getName(), trade.getTradeDate());

			if (!CollectionUtils.isEmpty(accountList) && (accountList.size() == 1)) {
				result = CollectionUtils.getOnlyElement(accountList);
			}
		}
		else {
			InvestmentAccountSearchForm aForm = new InvestmentAccountSearchForm();
			aForm.setNumberEquals(account.getNumber());
			aForm.setOurAccount(false);
			aForm.setClientId(trade.getClientInvestmentAccount().getBusinessClient().getId());

			try {
				result = CollectionUtils.getOnlyElement(getInvestmentAccountService().getInvestmentAccountList(aForm));
			}
			catch (Exception e) {
				throw new ValidationException("Multiple holding accounts with number [" + aForm.getNumber() + "] exist in the system for client ["
						+ trade.getClientInvestmentAccount().getBusinessClient().getName() + "]");
			}
		}

		trade.setHoldingInvestmentAccount(result);
	}


	private void setExecutingBrokerCompany(Trade trade, BusinessCompany simpleExecutingBrokerCompany, String brokerName) {
		BusinessCompany executingBroker = trade.getExecutingBrokerCompany();
		boolean clsTrade = trade.getTradeExecutionType() != null && trade.getTradeExecutionType().isClsTrade();
		boolean manualTrade = trade.getTradeDestination() != null && TradeDestination.MANUAL.equals(trade.getTradeDestination().getName());

		if ((simpleExecutingBrokerCompany != null) && (executingBroker == null || StringUtils.isEmpty(brokerName))) {
			executingBroker = simpleExecutingBrokerCompany;
		}
		else if (!StringUtils.isEmpty(brokerName)) {
			CompanySearchForm bForm = new CompanySearchForm();
			bForm.setNameEquals(brokerName);
			bForm.setTradingAllowed(true);
			executingBroker = CollectionUtils.getOnlyElement(getBusinessCompanyService().getBusinessCompanyList(bForm));
			if (executingBroker == null) {
				throw new ValidationException("Cannot find Broker Company with Name [" + bForm.getName() + "]");
			}
		}
		// Derive broker from holding account except for CLS trades with a trade destination that is not MANUAL.
		else if (!(clsTrade && !manualTrade)) {
			executingBroker = trade.getHoldingInvestmentAccount() != null ? trade.getHoldingInvestmentAccount().getIssuingCompany() : null;
		}

		trade.setExecutingBrokerCompany(executingBroker);
	}


	private void setClientInvestmentAccountForTrade(Trade trade) {
		InvestmentAccount account = trade.getClientInvestmentAccount();
		if (account == null || StringUtils.isEmpty(account.getNumber())) {
			throw new ValidationException("Client Investment Account Number is required.");
		}

		InvestmentAccountSearchForm aForm = new InvestmentAccountSearchForm();
		aForm.setNumberEquals(account.getNumber());
		aForm.setOurAccount(true);

		try {
			account = CollectionUtils.getOnlyElement(getInvestmentAccountService().getInvestmentAccountList(aForm));
		}
		catch (Exception e) {
			throw new ValidationException("Multiple client accounts with number [" + account.getNumber()
					+ "] exist in the system.  Please verify the client account number or use the full Trade import functionality to properly define with client account the trade is for.");
		}

		if (account == null) {
			throw new ValidationException("Cannot find Client Account with Account Number [" + aForm.getNumber() + "]");
		}
		trade.setClientInvestmentAccount(account);
	}


	private void setTradeDestination(Trade trade) {
		if ((trade.getTradeDestination() != null) && (trade.getTradeDestination().getName() != null)) {
			TradeDestinationMappingSearchCommand searchForm = new TradeDestinationMappingSearchCommand();
			searchForm.setTradeDestinationId(trade.getTradeDestination().getId());
			if ((trade.getExecutingBrokerCompany() != null) && (trade.getExecutingBrokerCompany().getId() != null)) {
				searchForm.setExecutingBrokerCompanyId(trade.getExecutingBrokerCompany().getId());
			}
			else {
				searchForm.setExecutingBrokerIsNull(true);
			}
			searchForm.setTradeTypeId(trade.getTradeType().getId());
			if (trade.getTradeDate() != null) {
				searchForm.setActiveOnDate(trade.getTradeDate());
			}

			List<TradeDestinationMapping> mappingList = getTradeDestinationService().getTradeDestinationMappingListByCommand(searchForm);
			ValidationUtils.assertTrue(!mappingList.isEmpty(),
					"No trade destination mapping exists for [" + trade.getTradeDestination().getName() + "] and broker ["
							+ (trade.getExecutingBrokerCompany() == null ? null : trade.getExecutingBrokerCompany().getName()) + "].");
			ValidationUtils.assertTrue(mappingList.size() < 2,
					"More than 1 trade destination mapping exists for [" + trade.getTradeDestination().getName() + "] and broker ["
							+ (trade.getExecutingBrokerCompany() == null ? null : trade.getExecutingBrokerCompany().getName()) + "].");
			trade.setTradeDestination(mappingList.get(0).getTradeDestination());
		}
		else if ((trade.getExecutingBrokerCompany() != null) && (trade.getExecutingBrokerCompany().getId() != null)) {
			TradeDestinationMappingSearchCommand searchForm = new TradeDestinationMappingSearchCommand(trade.getTradeType().getId(), true, trade.getTradeDate());
			if ((trade.getExecutingBrokerCompany() != null) && (trade.getExecutingBrokerCompany().getId() != null)) {
				searchForm.setExecutingBrokerCompanyId(trade.getExecutingBrokerCompany().getId());
				searchForm.setDefaultDestinationMapping(false);
			}
			TradeDestinationMapping mapping = getTradeDestinationService().getTradeDestinationMappingByCommand(searchForm);
			ValidationUtils.assertNotNull(mapping, "No trade destination mapping exists for [" + trade.getTradeType().getName() + "] and broker ["
					+ (trade.getExecutingBrokerCompany() == null ? null : trade.getExecutingBrokerCompany().getName()) + "].");
			trade.setTradeDestination(mapping.getTradeDestination());
		}
		else {
			// do nothing let the standard upload handle it
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BooleanReversableConverter getTradeBuyColumnReversableConverter() {
		return this.tradeBuyColumnReversableConverter;
	}


	public void setTradeBuyColumnReversableConverter(BooleanReversableConverter tradeBuyColumnReversableConverter) {
		this.tradeBuyColumnReversableConverter = tradeBuyColumnReversableConverter;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeExecutionTypeService getTradeExecutionTypeService() {
		return this.tradeExecutionTypeService;
	}


	public void setTradeExecutionTypeService(TradeExecutionTypeService tradeExecutionTypeService) {
		this.tradeExecutionTypeService = tradeExecutionTypeService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}
}
