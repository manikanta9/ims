package com.clifton.trade.file.upload.custom;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TradeUploadCustomBean {

	/**
	 * The list of properties that are required on the file.
	 * <p/>
	 * Could also maybe create a new @RequiredProperty instead of this list.
	 */
	String[] requiredPropertyList();


	/**
	 * The class used to convert the upload class to a trade.
	 */
	Class<?> converterClass() default TradeUploadCustomBean.class;


	/**
	 * The bean name need to retrieve the convert from the spring context.
	 */
	String converterBeanName() default "";
}
