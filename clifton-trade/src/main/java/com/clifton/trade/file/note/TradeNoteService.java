package com.clifton.trade.file.note;


import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * The <code>TradeNoteService</code> defines simplified methods for working with
 * SystemNotes, their file attachments and associating them with trades.
 * <p/>
 * Currently exposed for Trade Groups only
 *
 * @author manderson
 */
public interface TradeNoteService {

	//////////////////////////////////////////////////////////////////////////// 
	//////                Trade Group Note Methods                       /////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Create a note with specified note type for Trades associated with specified group and attaches document to it.
	 * Note is created as one note, linked to each trade in the group.
	 * <p>
	 * Security Resource is hardcoded because it's not dynamically finding the table name and we expect users to have CREATE access to TradeNote security resource, not just WRITE access to Trades
	 */
	@SecureMethod(securityResource = "TradeNote")
	public void uploadTradeNoteForTradeGroup(int tradeGroupId, String noteTypeName, FileUploadWrapper fileWrapper);


	/**
	 * Create a note with specified note type for Trades associated with any group with type source and source Fk Field ID and attaches document to it.
	 * Note is created as one note, linked to each trade in the group(s) that match.
	 * Note: Usually this one be one group, but could be multiple if on, for example, Portfolio Run someone submits trades, then adds more and submits again.
	 * <p>
	 * Security Resource is hardcoded because it's not dynamically finding the table name and we expect users to have CREATE access to TradeNote security resource, not just WRITE access to Trades
	 */
	@SecureMethod(securityResource = "TradeNote")
	public void uploadTradeNoteForTradeGroupBySource(String tradeGroupTypeName, Integer sourceFkFieldId, String noteTypeName, FileUploadWrapper fileWrapper);
}
