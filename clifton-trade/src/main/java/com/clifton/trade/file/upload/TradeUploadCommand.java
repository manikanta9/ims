package com.clifton.trade.file.upload;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.group.TradeGroup;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>TradeUploadCommand</code> extends the System Upload
 * however has some trade specific fields that will be applied
 * to the trades during the insert process.
 *
 * @author Mary Anderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class TradeUploadCommand extends SystemUploadCommand {

	/**
	 * Trades are always inserted as a group
	 */
	private TradeGroup tradeGroup;

	/**
	 * If simple is true, this needs to be set to clarify
	 * security look ups, holding account look ups, etc.
	 */
	private TradeType simpleTradeType;

	/**
	 * If simple is true, this can optionally be set to set the trade destination for all
	 * Trades in the file
	 */
	private TradeDestination simpleTradeDestination;

	/**
	 * If simple is true, this can optionally be set to set the executing broker for all
	 * Trades in the file
	 */
	private BusinessCompany simpleExecutingBrokerCompany;

	/**
	 * If simple is true, this can optionally be set to set the assignment company for all
	 * Trades in the file
	 */
	private BusinessCompany simpleAssignmentCompany;

	/**
	 * Allow setting explicitly and not have the system calculate
	 * So For CCY Forwards users can make it the same as the Trade Date
	 */
	private Date simpleSettlementDate;

	/**
	 * "Settlement CCY" If simple is true, this can optionally be set to set the paying security (settlement ccy) for all
	 * Trades in the file.  Can also be set within the file itself - otherwise defaults to traded security's CCY denomination
	 */
	private InvestmentSecurity simplePayingSecurity;

	/**
	 * Indicates that the upload is custom upload.
	 */
	private boolean custom;


	/**
	 * Used currently for Option trades only - Ability to create the option (if required information is present)
	 * prior to uploading the trades
	 */
	private boolean createMissingSecurities;


	/**
	 * Map Helpers that allow storing previously retrieved information for other rows in the upload.
	 * Besides security symbol map which is used to prevent the same security look up repeatedly, these maps are
	 * currently only used when creating new securities to reduce the number of look ups in the database
	 * Note "clearResults" method has been overridden and when called (just prior to processing the file) sets up these maps for use
	 */
	private final Map<String, InvestmentSecurity> securitySymbolMap = new HashMap<>();
	private final Map<String, InvestmentInstrumentHierarchy> hierarchyNameExpandedMap = new HashMap<>();
	// Maps "Hierarchy_" + HierarchyID + IdentifierPrefix
	// OR "Type_" + TypeID + IdentifierPrefix for each re-use throughout the upload file if the same security is used multiple times
	private final Map<String, InvestmentInstrument> instrumentIdentifierPrefixMap = new HashMap<>();
	// Maps DTC Number + Company Name in the file to the company to use
	private final Map<String, BusinessCompany> businessCompanyDTCMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "Trade";
	}


	@Override
	@ValueIgnoringGetter
	public String getSystemUploadBeanListConverterBeanName() {
		return "tradeUploadConverter";
	}


	@Override
	public void applyUploadSpecificProperties(IdentityObject bean, DataRow row) {
		// Copy properties from TradeUploadCommand bean where null on trade
		Trade trade = (Trade) bean;
		// Clear Workflow State & Status (Shouldn't be set anyway, but just to be sure)
		trade.setWorkflowState(null);
		trade.setWorkflowStatus(null);

		if (trade.getTraderUser() == null && getTradeGroup() != null && getTradeGroup().getTraderUser() != null) {
			trade.setTraderUser(getTradeGroup().getTraderUser());
		}
		if (trade.getTradeDate() == null && getTradeGroup() != null && getTradeGroup().getTradeDate() != null) {
			trade.setTradeDate(getTradeGroup().getTradeDate());
		}

		// If missing on trades, and simple upload values set that are available on screen use them
		if (trade.getTradeType() == null && getSimpleTradeType() != null) {
			trade.setTradeType(getSimpleTradeType());
		}
		if (trade.getTradeDestination() == null && getSimpleTradeDestination() != null) {
			trade.setTradeDestination(getSimpleTradeDestination());
		}
		if (trade.getExecutingBrokerCompany() == null && getSimpleExecutingBrokerCompany() != null) {
			trade.setExecutingBrokerCompany(getSimpleExecutingBrokerCompany());
		}
		if (trade.getSettlementDate() == null && getSimpleSettlementDate() != null) {
			trade.setSettlementDate(getSimpleSettlementDate());
		}
		if (trade.getPayingSecurity() == null && getSimplePayingSecurity() != null) {
			trade.setPayingSecurity(getSimplePayingSecurity());
		}

		// If trade has no description, but trade group note was set on screen - use it
		if (StringUtils.isEmpty(trade.getDescription()) && getTradeGroup() != null && !StringUtils.isEmpty(getTradeGroup().getNote())) {
			trade.setDescription(getTradeGroup().getNote());
		}
	}


	// Overrides - Trade Import is all or nothing, do not insert fk beans, and always inserts, never updates


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		// Note: Security inserts are handled independently through unmapped properties converter
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	@Override
	public void clearResults() {
		super.clearResults();
		this.securitySymbolMap.clear();
		this.hierarchyNameExpandedMap.clear();
		this.instrumentIdentifierPrefixMap.clear();
		this.businessCompanyDTCMap.clear();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeGroup getTradeGroup() {
		return this.tradeGroup;
	}


	public void setTradeGroup(TradeGroup tradeGroup) {
		this.tradeGroup = tradeGroup;
	}


	public TradeType getSimpleTradeType() {
		return this.simpleTradeType;
	}


	public void setSimpleTradeType(TradeType simpleTradeType) {
		this.simpleTradeType = simpleTradeType;
	}


	public BusinessCompany getSimpleExecutingBrokerCompany() {
		return this.simpleExecutingBrokerCompany;
	}


	public void setSimpleExecutingBrokerCompany(BusinessCompany simpleExecutingBrokerCompany) {
		this.simpleExecutingBrokerCompany = simpleExecutingBrokerCompany;
	}


	public BusinessCompany getSimpleAssignmentCompany() {
		return this.simpleAssignmentCompany;
	}


	public void setSimpleAssignmentCompany(BusinessCompany simpleAssignmentCompany) {
		this.simpleAssignmentCompany = simpleAssignmentCompany;
	}


	public TradeDestination getSimpleTradeDestination() {
		return this.simpleTradeDestination;
	}


	public void setSimpleTradeDestination(TradeDestination simpleTradeDestination) {
		this.simpleTradeDestination = simpleTradeDestination;
	}


	public Date getSimpleSettlementDate() {
		return this.simpleSettlementDate;
	}


	public void setSimpleSettlementDate(Date simpleSettlementDate) {
		this.simpleSettlementDate = simpleSettlementDate;
	}


	public InvestmentSecurity getSimplePayingSecurity() {
		return this.simplePayingSecurity;
	}


	public void setSimplePayingSecurity(InvestmentSecurity simplePayingSecurity) {
		this.simplePayingSecurity = simplePayingSecurity;
	}


	public boolean isCustom() {
		return this.custom;
	}


	public void setCustom(boolean custom) {
		this.custom = custom;
	}


	public boolean isCreateMissingSecurities() {
		return this.createMissingSecurities;
	}


	public void setCreateMissingSecurities(boolean createMissingSecurities) {
		this.createMissingSecurities = createMissingSecurities;
	}


	public Map<String, InvestmentSecurity> getSecuritySymbolMap() {
		return this.securitySymbolMap;
	}


	public Map<String, InvestmentInstrumentHierarchy> getHierarchyNameExpandedMap() {
		return this.hierarchyNameExpandedMap;
	}


	public Map<String, InvestmentInstrument> getInstrumentIdentifierPrefixMap() {
		return this.instrumentIdentifierPrefixMap;
	}


	public Map<String, BusinessCompany> getBusinessCompanyDTCMap() {
		return this.businessCompanyDTCMap;
	}
}
