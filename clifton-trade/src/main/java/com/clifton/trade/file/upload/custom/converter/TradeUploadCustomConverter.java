package com.clifton.trade.file.upload.custom.converter;


import com.clifton.trade.Trade;
import com.clifton.trade.file.upload.custom.TradeUploadCustomCommand;


public interface TradeUploadCustomConverter<F> {

	/**
	 * Converts the specified custom upload and returns the trade.
	 */
	public Trade convert(F from, TradeUploadCustomCommand<F> uploadCommand);
}
