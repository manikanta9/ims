package com.clifton.trade.file.note;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.system.note.DocumentSystemNoteService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.search.TradeSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class TradeNoteServiceImpl implements TradeNoteService {

	private DocumentSystemNoteService documentSystemNoteService;

	private SystemNoteService systemNoteService;
	private SystemSchemaService systemSchemaService;

	private TradeService tradeService;
	private TradeGroupService tradeGroupService;


	////////////////////////////////////////////////////////////////////////////
	///////////             Trade Group Note Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadTradeNoteForTradeGroup(int tradeGroupId, String noteTypeName, FileUploadWrapper fileWrapper) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setTradeGroupId(tradeGroupId);
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);
		if (CollectionUtils.isEmpty(tradeList)) {
			throw new ValidationException("Unable create note with attachment.  No trades available.");
		}

		String noteText = tradeList.get(0).getTradeGroup().getNote();
		uploadTradeNoteForTradeList(tradeList, noteTypeName, noteText, fileWrapper, false);
	}


	@Override
	public void uploadTradeNoteForTradeGroupBySource(String tradeGroupTypeName, Integer sourceFkFieldId, String noteTypeName, FileUploadWrapper fileWrapper) {
		// NOTE: This validation is important and although isn't as much a validation exception for the user, but for us
		// This will prevent accidentally attaching a file to all trades.

		ValidationUtils.assertFalse(StringUtils.isEmpty(tradeGroupTypeName), "Trade Group Type is required.");
		ValidationUtils.assertNotNull(sourceFkFieldId, "Source FK Field ID is required.");

		// Find matching Trades
		TradeGroupType type = getTradeGroupService().getTradeGroupTypeByName(tradeGroupTypeName);
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setTradeGroupTypeId(type.getId());
		searchForm.setTradeGroupSourceFkFieldId(sourceFkFieldId);
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);
		if (CollectionUtils.isEmpty(tradeList)) {
			throw new ValidationException("Unable create note with attachment.  No trades available.");
		}

		String noteText = tradeList.get(0).getTradeGroup().getNote();
		uploadTradeNoteForTradeList(tradeList, noteTypeName, noteText, fileWrapper, false);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////////             Trade Note Methods             ///////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @param tradeList
	 * @param noteTypeName
	 * @param noteText
	 * @param fileWrapper
	 * @param validateLimit - When attaching notes through trade groups we don't limit the number of trades the note can be attached to.  The limit is intended to keep from accidentally attaching notes to all trades.
	 */
	@Transactional
	protected void uploadTradeNoteForTradeList(List<Trade> tradeList, String noteTypeName, String noteText, FileUploadWrapper fileWrapper, boolean validateLimit) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(noteTypeName), "Note Type Name is Required.");
		String errorMsgPrefix = "Unable to create [" + noteTypeName + "] note and attachment for selected trades: ";

		ValidationUtils.assertNotNull(fileWrapper, errorMsgPrefix + "Missing File to attach.");

		int size = CollectionUtils.getSize(tradeList);

		if (size == 0) {
			throw new ValidationException("No trades selected.");
		}
		if (validateLimit && size > 200) {
			// Adding validation with the maximum amount of trades that can be attached the same note...
			// A few hundred should be more than enough and would prevent someone from accidentally attaching a note to all trades
			throw new ValidationException("You've selected [" + size + "] trades.  Please select a maximum of 200 trades.");
		}

		Long[] tradeIds = new Long[size];
		for (int i = 0; i < size; i++) {
			tradeIds[i] = BeanUtils.getIdentityAsLong(tradeList.get(i));
		}

		// Validate System Note Type
		SystemTable table = getSystemSchemaService().getSystemTableByName("Trade");
		SystemNoteType noteType = getSystemNoteService().getSystemNoteTypeByTableAndName(table.getId(), noteTypeName);
		ValidationUtils.assertNotNull(noteType, errorMsgPrefix + "Note Type does not exist for Trade table.");
		ValidationUtils.assertTrue(noteType.isLinkToMultipleAllowed(), errorMsgPrefix + "Note Type for Trade table does not support Link to Multiple feature.");
		ValidationUtils.assertFalse(DocumentAttachmentSupportedTypes.NONE == noteType.getAttachmentSupportedType(), errorMsgPrefix + "Selected note type [" + noteType.getName()
				+ "] does not support file attachments.");

		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setLinkedToMultiple(true);
		note.setText(noteText);
		getSystemNoteService().saveSystemNoteWithLinks(note, tradeIds);

		// Attach Document Appropriately
		getDocumentSystemNoteService().saveDocumentSystemNoteAttachment(note, fileWrapper);
	}


	////////////////////////////////////////////////////////
	////////         Getter & Setter Methods         ///////
	////////////////////////////////////////////////////////


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public DocumentSystemNoteService getDocumentSystemNoteService() {
		return this.documentSystemNoteService;
	}


	public void setDocumentSystemNoteService(DocumentSystemNoteService documentSystemNoteService) {
		this.documentSystemNoteService = documentSystemNoteService;
	}
}
