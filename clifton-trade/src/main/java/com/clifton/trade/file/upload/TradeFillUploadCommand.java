package com.clifton.trade.file.upload;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.trade.TradeFill;


/**
 * The <code>TradeFillUploadCommand</code> extends the System Upload
 * however has some trade fill specific fields that will be applied
 * to the trades they are filling during the insert process.
 *
 * @author Mary Anderson
 */
public class TradeFillUploadCommand extends SystemUploadCommand {

	private boolean executeFilledTrades;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return TradeFill.TABLE_NAME;
	}


	// Overrides - Trade Fill Import is all or nothing, do not insert fk beans, and always inserts, never updates


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isExecuteFilledTrades() {
		return this.executeFilledTrades;
	}


	public void setExecuteFilledTrades(boolean executeFilledTrades) {
		this.executeFilledTrades = executeFilledTrades;
	}
}
