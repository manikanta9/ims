package com.clifton.trade.file.upload;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;


/**
 * The <code>TradeUploadService</code> ...
 *
 * @author Mary Anderson
 */
public interface TradeUploadService {

	/**
	 * Uploads Trades into the system as one Trade Import group.
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dtoClass = Trade.class)
	public void uploadTradeUploadFile(TradeUploadCommand uploadCommand);


	@SecureMethod(dtoClass = Trade.class)
	public void uploadTradeFillUploadFile(TradeFillUploadCommand uploadCommand);
}
