package com.clifton.trade.file.upload.custom;


import java.math.BigDecimal;
import java.util.Date;


/**
 * Supports custom upload of Currency and Forwards trades.
 */
@TradeUploadCustomBean(converterBeanName = "tradeUploadCustomCurrencyConverter", requiredPropertyList = {"clientInvestmentAccountNumber", "givenCurrencySymbol",
		"settlementCurrencySymbol", "givenAmount", "buy"})
public class TradeUploadCustomCurrency {

	private String traderUserName;

	private String clientInvestmentAccountNumber;
	private String holdingInvestmentAccountNumber;
	private String isdaInvestmentAccountNumber;

	private String forwardSecuritySymbol;
	private String givenCurrencySymbol;
	private String settlementCurrencySymbol;

	private BigDecimal givenAmount;
	private BigDecimal settlementAmount;

	private BigDecimal rate;

	private String buy;

	private String executingBrokerCompanyName;
	private String tradeDestinationName;
	private String tradeExecutionTypeName;

	private Date tradeDate;

	private boolean roll;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getTraderUserName() {
		return this.traderUserName;
	}


	public void setTraderUserName(String traderUserName) {
		this.traderUserName = traderUserName;
	}


	public String getClientInvestmentAccountNumber() {
		return this.clientInvestmentAccountNumber;
	}


	public void setClientInvestmentAccountNumber(String clientInvestmentAccountNumber) {
		this.clientInvestmentAccountNumber = clientInvestmentAccountNumber;
	}


	public String getHoldingInvestmentAccountNumber() {
		return this.holdingInvestmentAccountNumber;
	}


	public void setHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
	}


	public String getIsdaInvestmentAccountNumber() {
		return this.isdaInvestmentAccountNumber;
	}


	public void setIsdaInvestmentAccountNumber(String isdaInvestmentAccountNumber) {
		this.isdaInvestmentAccountNumber = isdaInvestmentAccountNumber;
	}


	public String getForwardSecuritySymbol() {
		return this.forwardSecuritySymbol;
	}


	public void setForwardSecuritySymbol(String forwardSecuritySymbol) {
		this.forwardSecuritySymbol = forwardSecuritySymbol;
	}


	public String getGivenCurrencySymbol() {
		return this.givenCurrencySymbol;
	}


	public void setGivenCurrencySymbol(String givenCurrencySymbol) {
		this.givenCurrencySymbol = givenCurrencySymbol;
	}


	public String getSettlementCurrencySymbol() {
		return this.settlementCurrencySymbol;
	}


	public void setSettlementCurrencySymbol(String settlementCurrencySymbol) {
		this.settlementCurrencySymbol = settlementCurrencySymbol;
	}


	public BigDecimal getGivenAmount() {
		return this.givenAmount;
	}


	public void setGivenAmount(BigDecimal givenAmount) {
		this.givenAmount = givenAmount;
	}


	public BigDecimal getSettlementAmount() {
		return this.settlementAmount;
	}


	public void setSettlementAmount(BigDecimal settlementAmount) {
		this.settlementAmount = settlementAmount;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public String getBuy() {
		return this.buy;
	}


	public void setBuy(String buy) {
		this.buy = buy;
	}


	public String getExecutingBrokerCompanyName() {
		return this.executingBrokerCompanyName;
	}


	public void setExecutingBrokerCompanyName(String executingBrokerCompanyName) {
		this.executingBrokerCompanyName = executingBrokerCompanyName;
	}


	public String getTradeDestinationName() {
		return this.tradeDestinationName;
	}


	public void setTradeDestinationName(String tradeDestinationName) {
		this.tradeDestinationName = tradeDestinationName;
	}


	public String getTradeExecutionTypeName() {
		return this.tradeExecutionTypeName;
	}


	public void setTradeExecutionTypeName(String tradeExecutionTypeName) {
		this.tradeExecutionTypeName = tradeExecutionTypeName;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public boolean isRoll() {
		return this.roll;
	}


	public void setRoll(boolean roll) {
		this.roll = roll;
	}
}
