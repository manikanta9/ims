package com.clifton.trade.file.upload.custom;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.file.upload.FileUploadCommand;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.trade.file.upload.TradeUploadCommand;


public class TradeUploadCustomCommand<T> extends FileUploadCommand<T> {

	/**
	 * The upload bean.  For example, TradeUploadForward.class for custom forwards upload.
	 */
	private Class<T> uploadBeanClass;

	//private Converter<T, Trade> uploadBeanToTradeConverter;

	private TradeUploadCommand tradeUpload;


	//////////////////////////////////////////////////
	//////////////////////////////////////////////////
	public TradeUploadCustomCommand(TradeUploadCommand tradeUpload) {
		this.tradeUpload = tradeUpload;
	}


	public TradeUploadCustomCommand(TradeUploadCommand tradeUpload, Class<T> uploadBeanClass) {
		this(tradeUpload);
		this.uploadBeanClass = uploadBeanClass;
	}


	//////////////////////////////////////////////////
	//////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	@Override
	public Class<T> getUploadBeanClass() {
		return this.uploadBeanClass;
	}


	@Override
	public String[] getRequiredProperties() {
		TradeUploadCustomBean annotation = AnnotationUtils.getAnnotation(getUploadBeanClass(), TradeUploadCustomBean.class);
		return annotation.requiredPropertyList();
	}


	//////////////////////////////////////////////////
	//////////////////////////////////////////////////


	public TradeUploadCommand getTradeUpload() {
		return this.tradeUpload;
	}


	public void setTradeUpload(TradeUploadCommand tradeUpload) {
		this.tradeUpload = tradeUpload;
	}
}
