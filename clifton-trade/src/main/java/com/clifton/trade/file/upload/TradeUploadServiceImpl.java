package com.clifton.trade.file.upload;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.file.upload.FileUploadHandler;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.upload.SystemUploadHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.assetclass.TradeVirtualAllocation;
import com.clifton.trade.assetclass.handler.TradeVirtualAllocationHandler;
import com.clifton.trade.file.upload.custom.TradeUploadCustomBean;
import com.clifton.trade.file.upload.custom.TradeUploadCustomCommand;
import com.clifton.trade.file.upload.custom.TradeUploadCustomCurrency;
import com.clifton.trade.file.upload.custom.converter.TradeUploadCustomConverter;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroup.TradeGroupActions;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType.GroupTypes;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>TradeUploadServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
@SuppressWarnings("rawtypes")
public class TradeUploadServiceImpl implements TradeUploadService, ApplicationContextAware {

	private InvestmentInstrumentService investmentInstrumentService;

	private SystemUploadHandler systemUploadHandler;
	private FileUploadHandler fileUploadHandler;

	private TradeVirtualAllocationHandler tradeVirtualAllocationHandler;
	private TradeGroupService tradeGroupService;
	private TradeService tradeService;
	private TradeUploadDataPopulator tradeUploadDataPopulator;

	////////////////////////////////////////////////////////////////////////////////

	private Map<String, Class<?>> tradeTypeNameToCustomUploadClass;
	private ApplicationContext applicationContext;


	// these could be in the spring context
	private Map<String, Class<?>> getTradeTypeNameToCustomUploadClass() {
		if (this.tradeTypeNameToCustomUploadClass == null) {
			this.tradeTypeNameToCustomUploadClass = new HashMap<>();
			this.tradeTypeNameToCustomUploadClass.put(TradeType.FORWARDS, TradeUploadCustomCurrency.class);
			this.tradeTypeNameToCustomUploadClass.put(TradeType.CURRENCY, TradeUploadCustomCurrency.class);
		}
		return this.tradeTypeNameToCustomUploadClass;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadTradeUploadFile(TradeUploadCommand uploadCommand) {
		if (uploadCommand.isCustom()) {
			ValidationUtils.assertTrue(uploadCommand.isSimple(), "Custom uploads must be simple uploads.");
		}
		if (uploadCommand.isCreateMissingSecurities()) {
			ValidationUtils.assertTrue(uploadCommand.isSimple(), "Creating new securities prior to uploading Trades is allowed for simple uploads only.");
		}

		TradeGroup tradeGroup = uploadCommand.getTradeGroup() == null ? new TradeGroup() : uploadCommand.getTradeGroup();
		if (!uploadCommand.isSimple() || tradeGroup.getTradeGroupType() == null || tradeGroup.getTradeGroupType().getId() == null) {
			String groupTypeName = (tradeGroup.getTradeGroupType() == null || StringUtils.isEmpty(tradeGroup.getTradeGroupType().getName()))
					? GroupTypes.TRADE_IMPORT.getTypeName() : tradeGroup.getTradeGroupType().getName();
			tradeGroup.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(groupTypeName));
		}

		// Create a reference to a lazily built roll trade group for imports that include a mix of roll and non-roll trades (two groups will be created: Roll Trade Import and Trade Import)
		Lazy<TradeGroup> rollTradeGroup = new Lazy<>(() -> {
			if (tradeGroup.getTradeGroupType().isRoll()) {
				return tradeGroup;
			}
			else {
				TradeGroup result = new TradeGroup();
				BeanUtils.copyProperties(tradeGroup, result, new String[]{"tradeList"});
				result.setTradeGroupType(getTradeGroupService().getTradeGroupTypeByName(GroupTypes.ROLL_TRADE_IMPORT.getTypeName()));
				return result;
			}
		});

		if (uploadCommand.isSimple()) {
			ValidationUtils.assertNotNull(uploadCommand.getSimpleTradeType(), "Please specify the Trade Type for the Trades in the Simple Upload File.");
		}

		List<IdentityObject> beanList = getBeanList(uploadCommand, uploadCommand.getSimpleTradeType());

		boolean processAssetClassAllocations = false;
		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			Trade trade = (Trade) obj;
			ValidationUtils.assertTrue(trade.isNewBean(), "Cannot update existing trades only inserts are allowed.");

			getTradeUploadDataPopulator().populateTrade(trade, uploadCommand);

			if (trade.isRoll() && !tradeGroup.getTradeGroupType().isRoll()) {
				// We will dynamically create a roll trade group for standard uploads based on the Roll column.
				// If the import is overridden to use the Roll Trade Group, ignore the Roll column and classify all trades as roll.
				trade.setTradeGroup(rollTradeGroup.get());
				rollTradeGroup.get().addTrade(trade);
			}
			else {
				trade.setTradeGroup(tradeGroup);
				tradeGroup.addTrade(trade);
			}

			if (trade instanceof TradeVirtualAllocation) {
				processAssetClassAllocations = true;
			}
		}

		if (rollTradeGroup.isComputed()) {
			// had a roll trades included in a standard trade upload; save the roll trade group
			saveTradeUploadResults(uploadCommand, rollTradeGroup.get(), processAssetClassAllocations);
		}

		saveTradeUploadResults(uploadCommand, tradeGroup, processAssetClassAllocations);
	}


	@Transactional
	protected void saveTradeUploadResults(TradeUploadCommand uploadCommand, TradeGroup tradeGroup, boolean processAssetClassAllocations) {
		// Create New Securities First
		if (uploadCommand.isCreateMissingSecurities()) {
			// Check for any securities that we need to create
			int count = 0;
			List<IdentityObject> createSecurityList = new ArrayList<>();
			for (InvestmentSecurity investmentSecurity : uploadCommand.getSecuritySymbolMap().values()) {
				if (investmentSecurity.isNewBean()) {
					createSecurityList.add(investmentSecurity);
				}
			}
			if (!CollectionUtils.isEmpty(createSecurityList)) {
				getSystemUploadHandler().processSystemUploadCustomColumnValueList(createSecurityList);
				for (IdentityObject security : createSecurityList) {
					getInvestmentInstrumentService().saveInvestmentSecurity((InvestmentSecurity) security);
					count++;
				}
			}
			uploadCommand.getUploadResult().addUploadResults("InvestmentSecurity", count, false);
		}

		if (processAssetClassAllocations) {
			// Save trades and asset class allocations
			getTradeVirtualAllocationHandler().processTradeGroupVirtualTradeList(tradeGroup, null);
		}
		else {
			// Save trades
			getTradeGroupService().saveTradeGroup(tradeGroup);
		}
		uploadCommand.getUploadResult().addUploadResults("Trade", CollectionUtils.getSize(tradeGroup.getTradeList()), false);
	}


	/**
	 * Gets the converter for a custom upload.
	 */
	private TradeUploadCustomConverter getCustomConverter(Class<?> clazz) {
		TradeUploadCustomBean annotation = AnnotationUtils.getAnnotation(clazz, TradeUploadCustomBean.class);
		if (!StringUtils.isEmpty(annotation.converterBeanName())) {
			if (getApplicationContext().containsBean(annotation.converterBeanName())) {
				return (TradeUploadCustomConverter) getApplicationContext().getBean(annotation.converterBeanName());
			}
		}
		else if (!TradeUploadCustomBean.class.equals(annotation.converterClass())) {
			return (TradeUploadCustomConverter) BeanUtils.newInstance(annotation.converterClass());
		}
		throw new RuntimeException("No converter for [" + clazz + "] to Trade is specified.");
	}


	@SuppressWarnings("unchecked")
	private List<IdentityObject> getBeanList(TradeUploadCommand uploadCommand, TradeType tradeType) {
		List<IdentityObject> beanList;
		if (uploadCommand.isCustom() && tradeType != null && getTradeTypeNameToCustomUploadClass().containsKey(tradeType.getName())) {
			uploadCommand.clearResults();

			Class<?> clazz = getTradeTypeNameToCustomUploadClass().get(tradeType.getName());
			TradeUploadCustomConverter converter = getCustomConverter(clazz);
			TradeUploadCustomCommand<?> customUpload = BeanUtils.newInstance(TradeUploadCustomCommand.class, uploadCommand, clazz);
			customUpload.setFile(uploadCommand.getFile());

			List<?> beanListCustom = getFileUploadHandler().convertFileUploadFileToBeanList(customUpload);
			beanList = new ArrayList<>();

			for (Object obj : CollectionUtils.getIterable(beanListCustom)) {
				beanList.add(converter.convert(obj, customUpload));
			}
		}
		else {
			beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, !uploadCommand.isSimple());
		}
		return beanList;
	}


	///////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadTradeFillUploadFile(TradeFillUploadCommand uploadCommand) {
		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, true);
		List<Integer> tradeIdList = new ArrayList<>();
		List<TradeFill> tradeFillList = new ArrayList<>();
		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			TradeFill tradeFill = (TradeFill) obj;

			ValidationUtils.assertFalse(tradeFill.getTrade().isNewBean(), "Trade [" + tradeFill.getTrade().getUniqueLabel() + "] is missing in the database.");
			if (uploadCommand.isExecuteFilledTrades() && MathUtils.isEqual(tradeFill.getQuantity(), tradeFill.getTrade().getQuantityIntended())) {
				tradeIdList.add(tradeFill.getTrade().getId());
			}
			tradeFillList.add(tradeFill);
		}
		getTradeService().saveTradeFillList(tradeFillList);
		uploadCommand.getUploadResult().addUploadResults(TradeFill.TABLE_NAME, CollectionUtils.getSize(tradeFillList), false);
		if (!CollectionUtils.isEmpty(tradeIdList)) {
			getTradeGroupService().executeActionOnTrades(TradeGroupActions.EXECUTE, tradeIdList);
			uploadCommand.getUploadResult().addUploadResults("Trade", CollectionUtils.getSize(tradeIdList), true);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}


	public FileUploadHandler getFileUploadHandler() {
		return this.fileUploadHandler;
	}


	public void setFileUploadHandler(FileUploadHandler fileUploadHandler) {
		this.fileUploadHandler = fileUploadHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public TradeVirtualAllocationHandler getTradeVirtualAllocationHandler() {
		return this.tradeVirtualAllocationHandler;
	}


	public void setTradeVirtualAllocationHandler(TradeVirtualAllocationHandler tradeVirtualAllocationHandler) {
		this.tradeVirtualAllocationHandler = tradeVirtualAllocationHandler;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeUploadDataPopulator getTradeUploadDataPopulator() {
		return this.tradeUploadDataPopulator;
	}


	public void setTradeUploadDataPopulator(TradeUploadDataPopulator tradeUploadDataPopulator) {
		this.tradeUploadDataPopulator = tradeUploadDataPopulator;
	}
}
