package com.clifton.trade.file.upload;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.instrument.securitylocator.InvestmentSecurityLocatorService;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.execution.TradeExecutionTypeService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
@Component
public class TradeUploadDataPopulatorImpl implements TradeUploadDataPopulator {

	private BusinessCompanyService businessCompanyService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityLocatorService investmentSecurityLocatorService;

	private MarketDataRetriever marketDataRetriever;

	private SecurityUserService securityUserService;
	private SystemColumnService systemColumnService;

	private TradeDestinationService tradeDestinationService;
	private TradeService tradeService;
	private TradeExecutionTypeService tradeExecutionTypeService;

	private SystemBeanService systemBeanService;
	private MarketDataService marketDataService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void populateTrade(Trade trade, TradeUploadCommand uploadCommand) {
		// Applies only to NOT custom uploads.  Can apply to both simple and regular trade uploads as this information is no longer required on screen
		// and fields are allowed to be set in the file.  Currently, only applies to Westport Option Trade Uploads
		if (!uploadCommand.isCustom()) {
			ValidationUtils.assertNotNull(trade.getTradeDate(), "Trade Date is required for each trade in the file if not selected on the upload screen.");

			if (trade.getTraderUser() == null || (trade.getTraderUser().isNewBean() && StringUtils.isEmpty(trade.getTraderUser().getUserName()))) {
				throw new ValidationException("Trader User is required for each trade in the file if not selected on the upload screen.");
			}
			if (trade.getTraderUser().isNewBean()) {
				SecurityUser traderUser = getSecurityUserService().getSecurityUserByName(trade.getTraderUser().getUserName());
				if (traderUser == null) {
					throw new ValidationException("Cannot find user in the system with user name [" + trade.getTraderUser().getUserName() + "]");
				}
				trade.setTraderUser(traderUser);
			}
		}

		// Process Simple Look-ups - NOTE: Custom imports are responsible for their own look-ups
		if (uploadCommand.isSimple() && !uploadCommand.isCustom()) {
			populateInvestmentAccountsForTrade(trade);
			populateInvestmentSecurityForTrade(trade, uploadCommand);
			populateExecutingBrokerCompanyForTrade(trade, uploadCommand);
			populateAssignmentCompanyForTrade(trade, uploadCommand);
		}

		ValidationUtils.assertFalse(!uploadCommand.isCreateMissingSecurities() && trade.getInvestmentSecurity().isNewBean(), "Security [" + trade.getInvestmentSecurity().getUniqueLabel()
				+ "] is missing in the database.  Securities are unique by Hierarchy, Instrument Identifier Prefix, & Security Symbol.");

		// Trade Type
		if (trade.getTradeType() == null) {
			if (trade.getInvestmentSecurity().isNewBean()) {
				throw new ValidationException("Trade type is required when a new security is being created/used to trade.");
			}
			trade.setTradeType(getTradeService().getTradeTypeForSecurity(trade.getInvestmentSecurity().getId()));
		}

		// Trade Destination
		if (trade.getTradeDestination() == null || trade.getTradeDestination().isNewBean()) {
			populateTradeDestinationForTrade(trade);
		}
		if (trade.getOpenCloseType() != null && trade.getOpenCloseType().isNewBean()) {
			populateTradeOpenCloseTypeForTrade(trade);
		}

		// Paying Security
		if (trade.getPayingSecurity() != null && trade.getPayingSecurity().isNewBean() && !StringUtils.isEmpty(trade.getPayingSecurity().getSymbol())) {
			trade.setPayingSecurity(getInvestmentInstrumentService().getInvestmentSecurityBySymbol(trade.getPayingSecurity().getSymbol(), true));
		}
		// If still not set - default it
		if (trade.getPayingSecurity() == null || trade.getPayingSecurity().isNewBean()) {
			trade.setPayingSecurity(InvestmentUtils.getSettlementCurrency(trade.getInvestmentSecurity()));
		}

		// Calculate Settlement Date Only If Not Specified - Otherwise use value set on screen
		if (trade.getSettlementDate() == null) {
			// Forwards - Should Default to Trade Date which means it is not an early settlement
			if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
				trade.setSettlementDate(trade.getTradeDate());
			}
			else {
				trade.setSettlementDate(getInvestmentCalculator().calculateSettlementDate(trade.getInvestmentSecurity(), trade.getSettlementCurrency(), trade.getTradeDate()));
			}
		}

		// Trade Execution Type
		if (trade.getTradeExecutionType() != null && trade.getTradeExecutionType().isNewBean()) {
			trade.setTradeExecutionType(getTradeExecutionTypeService().getTradeExecutionTypeByName(trade.getTradeExecutionType().getName()));
		}

		// If both expected unit price and average unit price are missing expected to most recent so accounting notional will populate
		// Most cases these will be Desk Orders where the fill price is not yet known
		if (trade.getExpectedUnitPrice() == null && trade.getAverageUnitPrice() == null) {
			trade.setExpectedUnitPrice(getMarketDataRetriever().getPriceFlexible(trade.getInvestmentSecurity(), trade.getTradeDate(), false));
			// If you can't find any price and accounting notional is not populated - throw an exception, otherwise accounting notional cannot be calculated
			if (trade.getAccountingNotional() == null) {
				ValidationUtils.assertNotNull(trade.getExpectedUnitPrice(), "There is no price available for security [" + trade.getInvestmentSecurity().getSymbol() + "] on or before [" + DateUtils.fromDateShort(trade.getTradeDate()) + "]. Please set the accounting notional, expected unit price, or average unit price in the upload, or load prices in the system.");
			}
		}

		// Based on hierarchy information on notional multiplier use and index ratio adjustment, set the notional multiplier
		if (trade.getInvestmentSecurity().getInstrument().getHierarchy().isNotionalMultiplierUsed()) {
			if (trade.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted()) {
				trade.setNotionalMultiplier(getMarketDataService().getMarketDataIndexRatioForSecurity(trade.getInvestmentSecurity().getId(), trade.getSettlementDate(), false));
			}
			else {
				NotionalMultiplierRetriever retriever = (NotionalMultiplierRetriever) getSystemBeanService().getBeanInstance(trade.getInvestmentSecurity().getInstrument().getHierarchy().getNotionalMultiplierRetriever());
				trade.setNotionalMultiplier(retriever.retrieveNotionalMultiplier(trade.getInvestmentSecurity(), trade::getSettlementDate, false));
			}
		}

		if (trade.getAssignmentCompany() != null) {
			trade.setNovation(true);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Population Helper Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	private void populateTradeOpenCloseTypeForTrade(Trade trade) {
		trade.setOpenCloseType(getTradeService().getTradeOpenCloseTypeByName(trade.getOpenCloseType().getName()));
	}


	private void populateInvestmentAccountsForTrade(Trade trade) {
		InvestmentAccount clientAccount = trade.getClientInvestmentAccount();
		InvestmentAccount holdingAccount = trade.getHoldingInvestmentAccount();

		// Client Account Number Specified - Use It
		if (clientAccount != null && !StringUtils.isEmpty(clientAccount.getNumber())) {
			// Will throw an exception if none or more than one found
			clientAccount = getInvestmentAccountByNumber(clientAccount.getNumber(), true, null);
			// Now - Look Up Holding Account Based on Client Account Selected (Unless Specified)
			if (holdingAccount == null || StringUtils.isEmpty(holdingAccount.getNumber())) {
				// Try to figure out which holding account to use
				holdingAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurpose(clientAccount.getId(),
						trade.getTradeType().getHoldingAccountPurpose().getName(), trade.getTradeDate());

				if (holdingAccount == null) {
					throw new ValidationException("There are no active holding account relationships for trade purpose [" + trade.getTradeType().getHoldingAccountPurpose().getName()
							+ "] and client account [" + clientAccount.getNumber() + "] on " + DateUtils.fromDateShort(trade.getTradeDate()));
				}
			}
			else {
				// Will throw an exception if none or more than one found
				holdingAccount = getInvestmentAccountByNumber(holdingAccount.getNumber(), false, trade.getClientInvestmentAccount().getBusinessClient());
			}
		}
		// Else - Holding Account Number must be specified and unique
		else if (holdingAccount != null && !StringUtils.isEmpty(holdingAccount.getNumber())) {
			// Will throw an exception if none or more than one found
			holdingAccount = getInvestmentAccountByNumber(holdingAccount.getNumber(), false, null);
			// Now - Look Up Client Account Based on Holding Account Selected
			clientAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurpose(holdingAccount.getId(),
					trade.getTradeType().getHoldingAccountPurpose().getName(), trade.getTradeDate());

			if (clientAccount == null) {
				throw new ValidationException("There are no active client account relationships for trade purpose [" + trade.getTradeType().getHoldingAccountPurpose().getName()
						+ "] and holding account [" + holdingAccount.getNumber() + "] on " + DateUtils.fromDateShort(trade.getTradeDate()));
			}
		}
		else {
			throw new ValidationException("At least one of Client Account Number or Holding Account Number is required.");
		}

		trade.setClientInvestmentAccount(clientAccount);
		trade.setHoldingInvestmentAccount(holdingAccount);
	}


	private void populateInvestmentSecurityForTrade(Trade trade, TradeUploadCommand uploadCommand) {
		if (trade.getInvestmentSecurity() == null || StringUtils.isEmpty(trade.getInvestmentSecurity().getSymbol())) {
			throw new ValidationException("Security Symbol is required for all Trades.");
		}
		// First check if we've already retrieved this security by symbol
		InvestmentSecurity security = uploadCommand.getSecuritySymbolMap().get(trade.getInvestmentSecurity().getSymbol());
		// If not - look it up
		if (security == null) {
			security = getSecurityForTrade(trade);
			if (security == null) {
				String msg = "Cannot find Active Security on " + DateUtils.fromDateShort(trade.getTradeDate()) + " with Symbol [" + trade.getInvestmentSecurity().getSymbol() + "] and Investment Type ["
						+ trade.getTradeType().getHoldingAccountPurpose().getInvestmentType().getName() + "].";
				if (uploadCommand.isCreateMissingSecurities()) {
					if (InvestmentType.OPTIONS.equals(uploadCommand.getSimpleTradeType().getName())) {
						try {
							security = populateNewInvestmentSecurity(trade, uploadCommand);
						}
						catch (ValidationException e) {
							msg += "Unable to create new security because: [" + e.getMessage() + "]";
						}
					}
					else {
						msg += "Ability to create new securities via simple Trade Uploads is currently supported for Options only.";
					}
				}
				if (security == null) {
					throw new ValidationException(msg);
				}
			}
			uploadCommand.getSecuritySymbolMap().put(security.getSymbol(), security);
		}
		trade.setInvestmentSecurity(security);
	}


	/**
	 * Uses {@link InvestmentInstrumentService#getInvestmentSecurityBySymbol(String, Boolean)} to find the security for the provided Trade.
	 * The found security is validated for being of the correct {@link InvestmentType} and active on the Trade's trade date.
	 */
	private InvestmentSecurity getSecurityForTrade(Trade trade) {
		List<InvestmentSecurity> resultList = getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(trade.getInvestmentSecurity().getSymbol());
		resultList = BeanUtils.filter(resultList, security ->
				security.getInstrument().getHierarchy().getInvestmentType().getId().equals(trade.getTradeType().getHoldingAccountPurpose().getInvestmentType().getId()) && security.isActiveOn(trade.getTradeDate())
		);
		if (CollectionUtils.getSize(resultList) == 1) {
			return resultList.get(0);
		}

		SecuritySearchForm sForm = new SecuritySearchForm();
		try {
			// Look up security with Yellow Key
			InvestmentSecurity security = getInvestmentSecurityLocatorService().getInvestmentSecurityByFullSymbol(trade.getInvestmentSecurity().getSymbol());
			if (security == null) {
				// Look up security by internal identifier (CUSIP). Reuse search form for exception handling message, so remove restrictions and symbol.
				sForm.setCusipEquals(trade.getInvestmentSecurity().getSymbol());
				sForm.setOtc(Boolean.TRUE); // limit to OTC to be more specific and avoid a potential conflict
				sForm.setInvestmentTypeId(trade.getTradeType().getHoldingAccountPurpose().getInvestmentType().getId());
				sForm.setActiveOnDate(trade.getTradeDate());
				security = CollectionUtils.getOnlyElement(getInvestmentInstrumentService().getInvestmentSecurityList(sForm));
			}
			else {
				// Do not return the found security by Yellow Key if it is not active or of the wrong investment type.
				if (!InvestmentUtils.isSecurityActiveOn(security, trade.getTradeDate())) {
					return null;
				}
				if (trade.getTradeType().getHoldingAccountPurpose().getInvestmentType() != null) {
					if (!InvestmentUtils.isSecurityOfType(security, trade.getTradeType().getHoldingAccountPurpose().getInvestmentType().getName())) {
						return null;
					}
				}
			}
			return security;
		}
		// IllegalArgumentException will be thrown if any of the security lists have more than one matching security.
		catch (IllegalArgumentException e) {
			String message = String.format("Multiple [%s] securities with the %s [%s] were found active on %s.  Please verify the security type & symbol or use the full Trade import functionality to properly define with security the trade is for.",
					trade.getTradeType().getHoldingAccountPurpose().getName(), (sForm.getSymbolExact() == null ? "CUSIP" : "symbol"), (sForm.getSymbolExact() == null ? sForm.getCusip() : sForm.getSymbolExact()), DateUtils.fromDateShort(trade.getTradeDate()));
			throw new ValidationException(message);
		}
	}


	private InvestmentSecurity populateNewInvestmentSecurity(Trade trade, TradeUploadCommand uploadCommand) {
		InvestmentSecurity security = BeanUtils.cloneBean(trade.getInvestmentSecurity(), false, false);
		if (trade.getInvestmentSecurity().getInstrument() == null || StringUtils.isEmpty(trade.getInvestmentSecurity().getInstrument().getIdentifierPrefix())) {
			throw new ValidationException("[InvestmentSecurity-Instrument-IdentifierPrefix] is required to create new security.");
		}
		InvestmentInstrument instrument = getInvestmentInstrumentForTrade(trade, uploadCommand);
		security.setInstrument(instrument);
		if (instrument.getUnderlyingInstrument() != null && !instrument.getUnderlyingInstrument().getHierarchy().isOneSecurityPerInstrument()) {
			if (trade.getInvestmentSecurity().getUnderlyingSecurity() == null || StringUtils.isEmpty(trade.getInvestmentSecurity().getUnderlyingSecurity().getSymbol())) {
				throw new ValidationException("[InvestmentSecurity-UnderlyingSecurity-Symbol] is required to create new security. Underlying security should fall under instrument [" + instrument.getLabel() + "].");
			}
			security.setUnderlyingSecurity(getInvestmentSecurityByInstrumentAndSymbol(instrument.getUnderlyingInstrument(), trade.getInvestmentSecurity().getUnderlyingSecurity().getSymbol(), "underlying"));
		}

		if (instrument.getBigInstrument() != null && trade.getInvestmentSecurity().getBigSecurity() != null && !StringUtils.isEmpty(trade.getInvestmentSecurity().getBigSecurity().getSymbol())) {
			security.setBigSecurity(getInvestmentSecurityByInstrumentAndSymbol(instrument.getBigInstrument(), trade.getInvestmentSecurity().getBigSecurity().getSymbol(), "big"));
		}

		if (instrument.getHierarchy().isOtc()) {
			if (trade.getHoldingInvestmentAccount() == null) {
				throw new ValidationException("Holding Account on the trade is Required for OTC securities");
			}
			security.setBusinessCompany(trade.getHoldingInvestmentAccount().getIssuingCompany());
			security.setBusinessContract(trade.getHoldingInvestmentAccount().getBusinessContract());
		}

		if (TradeType.OPTIONS.equals(trade.getTradeType().getName())) {
			if (security.getLastDeliveryDate() != null && security.getFirstDeliveryDate() == null) {
				// This is customized on the Option Security Window
				security.setFirstDeliveryDate(security.getLastDeliveryDate());
			}
		}

		// Custom Fields - Filter the list of custom column values to those that actually apply (because we use unmapped properties and assume they are custom fields, we just ignore what isn't)
		if (!StringUtils.isEmpty(security.getColumnGroupName())) {
			List<SystemColumnCustom> columnList = getSystemColumnService().getSystemColumnCustomListForGroupAndLinkedValue(security.getColumnGroupName(), instrument.getHierarchy().getId() + "", true);
			List<SystemColumnValue> newValueList = new ArrayList<>();
			for (SystemColumnCustom customColumn : CollectionUtils.getIterable(columnList)) {
				// Find (if there was a value entered) in the value List
				for (SystemColumnValue value : CollectionUtils.getIterable(security.getColumnValueList())) {
					if (customColumn.getName().equals(value.getColumn().getName())) {
						value.setColumn(customColumn);
						value.setValue(new ObjectToStringConverter().convert(value.getValue()));
						newValueList.add(value);
					}
				}
			}
			security.setColumnValueList(newValueList);
		}
		return security;
	}


	private void populateExecutingBrokerCompanyForTrade(Trade trade, TradeUploadCommand uploadCommand) {
		BusinessCompany executingBroker = trade.getExecutingBrokerCompany();
		if (executingBroker == null || (StringUtils.isEmpty(executingBroker.getName()) && StringUtils.isEmpty(executingBroker.getDtcNumber()))) {
			executingBroker = uploadCommand.getSimpleExecutingBrokerCompany();
		}
		else if (executingBroker.isNewBean()) {
			if (!StringUtils.isEmpty(executingBroker.getDtcNumber())) {
				executingBroker = getBusinessCompanyWithDTCOrName(uploadCommand, executingBroker.getDtcNumber(), executingBroker.getName());
			}
			else {
				CompanySearchForm bForm = new CompanySearchForm();
				bForm.setNameEquals(executingBroker.getName());
				bForm.setTradingAllowed(true);

				executingBroker = CollectionUtils.getOnlyElement(getBusinessCompanyService().getBusinessCompanyList(bForm));
				if (executingBroker == null) {
					throw new ValidationException("Cannot find Broker Company with Name [" + bForm.getNameEquals() + "]");
				}
			}
		}
		trade.setExecutingBrokerCompany(executingBroker);
	}


	private void populateAssignmentCompanyForTrade(Trade trade, TradeUploadCommand uploadCommand) {
		BusinessCompany assignmentCompany = trade.getAssignmentCompany();
		if (assignmentCompany == null || (StringUtils.isEmpty(assignmentCompany.getName()) && StringUtils.isEmpty(assignmentCompany.getDtcNumber()))) {
			assignmentCompany = uploadCommand.getSimpleAssignmentCompany();
		}
		else if (assignmentCompany.isNewBean()) {
			if (!StringUtils.isEmpty(assignmentCompany.getDtcNumber())) {
				assignmentCompany = getBusinessCompanyWithDTCOrName(uploadCommand, assignmentCompany.getDtcNumber(), assignmentCompany.getName());
			}
			else {
				CompanySearchForm bForm = new CompanySearchForm();
				bForm.setNameEquals(assignmentCompany.getName());
				bForm.setTradingAllowed(true);

				assignmentCompany = CollectionUtils.getOnlyElement(getBusinessCompanyService().getBusinessCompanyList(bForm));
				if (assignmentCompany == null) {
					throw new ValidationException("Cannot find Assignment Company with Name [" + bForm.getNameEquals() + "]");
				}
			}
		}
		trade.setAssignmentCompany(assignmentCompany);
	}


	private BusinessCompany getBusinessCompanyWithDTCOrName(TradeUploadCommand uploadCommand, String dtcNumber, String companyName) {
		String key = dtcNumber + "_" + companyName;
		if (uploadCommand.getBusinessCompanyDTCMap().containsKey(key)) {
			return uploadCommand.getBusinessCompanyDTCMap().get(key);
		}
		CompanySearchForm searchForm = new CompanySearchForm();
		searchForm.setDtcNumberEquals(new ObjectToStringConverter().convert(dtcNumber));
		List<BusinessCompany> dtcBusinessCompanyList = getBusinessCompanyService().getBusinessCompanyList(searchForm);

		BusinessCompany executingBroker = null;
		if (CollectionUtils.isEmpty(dtcBusinessCompanyList)) {
			if (!StringUtils.isEmpty(companyName)) {
				CompanySearchForm companySearchForm = new CompanySearchForm();
				companySearchForm.setNameEquals(companyName);
				companySearchForm.setTradingAllowed(true);
				executingBroker = CollectionUtils.getOnlyElement(getBusinessCompanyService().getBusinessCompanyList(companySearchForm));
				if (executingBroker == null) {
					throw new ValidationException("Cannot find Broker Company with DTC Number [" + dtcNumber + "] or Name [" + companyName + "]");
				}
			}
			else {
				throw new ValidationException("Cannot find Broker Company with DTC Number [" + dtcNumber + "] and no company name provided.");
			}
		}
		else if (dtcBusinessCompanyList.size() == 1) {
			executingBroker = dtcBusinessCompanyList.get(0);
		}
		else {
			if (StringUtils.isEmpty(companyName)) {
				throw new ValidationException("Found multiple companies with DTC Number [" + dtcNumber + "] and no company name provided.  Companies with that DTC Number are: " + StringUtils.collectionToCommaDelimitedString(dtcBusinessCompanyList, BusinessCompany::getName));
			}
			else {
				for (BusinessCompany company : dtcBusinessCompanyList) {
					if (StringUtils.isEqualIgnoreCase(companyName, company.getName())) {
						executingBroker = company;
						break;
					}
				}
				if (executingBroker == null) {
					throw new ValidationException("Found multiple companies with DTC Number [" + dtcNumber + "] and none that match company name [" + companyName + "].  Companies with that DTC Number are: " + StringUtils.collectionToCommaDelimitedString(dtcBusinessCompanyList, BusinessCompany::getName));
				}
			}
		}
		uploadCommand.getBusinessCompanyDTCMap().put(key, executingBroker);
		return executingBroker;
	}


	private void populateTradeDestinationForTrade(Trade trade) {
		if ((trade.getTradeDestination() != null) && (trade.getTradeDestination().getName() != null)) {
			TradeDestinationMappingSearchCommand searchForm = new TradeDestinationMappingSearchCommand();
			if (trade.getTradeDestination().getId() != null) {
				searchForm.setTradeDestinationId(trade.getTradeDestination().getId());
			}
			else {
				TradeDestination destination = getTradeDestinationService().getTradeDestinationByName(trade.getTradeDestination().getName());
				ValidationUtils.assertNotNull(destination, "Cannot find trade destination with name [" + trade.getTradeDestination().getName() + "].");
				searchForm.setTradeDestinationId(destination.getId());
			}
			if (trade.getExecutingBrokerCompany() != null) {
				searchForm.setExecutingBrokerCompanyId(trade.getExecutingBrokerCompany().getId());
			}
			searchForm.setTradeTypeId(trade.getTradeType().getId());
			if (trade.getTradeDate() != null) {
				searchForm.setActiveOnDate(trade.getTradeDate());
			}

			List<TradeDestinationMapping> mappingList = getTradeDestinationService().getTradeDestinationMappingListByCommand(searchForm);
			String messageSuffix = "trade destination mapping exists for [" + trade.getTradeDestination().getName() + "]" + ((trade.getExecutingBrokerCompany() != null) ? " and broker [" +
					trade.getExecutingBrokerCompany().getName() + "]" : "") + ".";
			ValidationUtils.assertTrue(!mappingList.isEmpty(), "No " + messageSuffix);
			ValidationUtils.assertTrue(mappingList.size() < 2, "More than 1 " + messageSuffix);
			trade.setTradeDestination(mappingList.get(0).getTradeDestination());
		}
		else {
			TradeDestinationMapping mapping = getTradeDestinationService().getTradeDestinationMappingByCommand(
					new TradeDestinationMappingSearchCommand(trade.getTradeType().getId(), true, trade.getTradeDate()));
			ValidationUtils.assertNotNull(mapping, "No trade destination mapping exists for trade [" + trade.getLabel() + "].");
			trade.setTradeDestination(mapping.getTradeDestination());
		}
	}

	////////////////////////////////////////////////////////////////////////
	//////////                  Helper Methods                    //////////
	////////////////////////////////////////////////////////////////////////


	private InvestmentAccount getInvestmentAccountByNumber(String accountNumber, boolean ourAccount, BusinessClient client) {
		InvestmentAccountSearchForm accountSearchForm = new InvestmentAccountSearchForm();
		accountSearchForm.setNumberEquals(accountNumber);
		accountSearchForm.setOurAccount(ourAccount);
		if (client != null) {
			accountSearchForm.setClientId(client.getId());
		}

		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(accountSearchForm);
		if (CollectionUtils.isEmpty(accountList)) {
			throw new ValidationException("Cannot find " + (ourAccount ? "client" : "holding") + " account with Account Number [" + accountNumber + "] " + (client != null ? " for Client ["
					+ client.getName() + "]" : ""));
		}
		else if (accountList.size() > 1) {
			throw new ValidationException("Multiple " + (ourAccount ? "client" : "holding") + " accounts with number [" + accountNumber + "] exist in the system" + (client != null ? " for client ["
					+ client.getName() + "]" : ""));
		}
		return accountList.get(0);
	}


	private InvestmentInstrument getInvestmentInstrumentForTrade(Trade trade, TradeUploadCommand uploadCommand) {
		// Start with the Hierarchy - If it's populated
		if (trade.getInvestmentSecurity().getInstrument().getHierarchy() != null && !StringUtils.isEmpty(trade.getInvestmentSecurity().getInstrument().getHierarchy().getName())) {
			InvestmentInstrumentHierarchy hierarchy = uploadCommand.getHierarchyNameExpandedMap().get(trade.getInvestmentSecurity().getInstrument().getHierarchy().getNameExpanded());
			InvestmentInstrument instrument = (hierarchy != null ? uploadCommand.getInstrumentIdentifierPrefixMap().get("Hierarchy_" + hierarchy.getId() + "_" + trade.getInvestmentSecurity().getInstrument().getIdentifierPrefix()) : null);
			if (instrument == null) {
				InstrumentSearchForm searchForm = new InstrumentSearchForm();
				searchForm.setIdentifierPrefixEquals(trade.getInvestmentSecurity().getInstrument().getIdentifierPrefix());
				searchForm.setInvestmentTypeId(uploadCommand.getSimpleTradeType().getInvestmentType().getId());
				if (hierarchy == null) {
					searchForm.setHierarchyNameEquals(trade.getInvestmentSecurity().getInstrument().getHierarchy().getName());
				}
				else {
					searchForm.setHierarchyId(hierarchy.getId());
				}

				List<InvestmentInstrument> instrumentList = getInvestmentInstrumentService().getInvestmentInstrumentList(searchForm);
				// If we didn't know the exact hierarchy, then we only checked the first level - need to verify hierarchy (also in case there is more than one)
				// Otherwise there could only be one (UX constraint)
				if (hierarchy != null) {
					instrument = CollectionUtils.getFirstElement(instrumentList);
				}
				else {
					for (InvestmentInstrument investmentInstrument : CollectionUtils.getIterable(instrumentList)) {
						if (investmentInstrument.getHierarchy().getNameExpanded().equalsIgnoreCase(trade.getInvestmentSecurity().getInstrument().getHierarchy().getNameExpanded())) {
							// Unique Index - Could only have one exact match
							instrument = investmentInstrument;
							break;
						}
					}
				}
				if (instrument == null) {
					throw new ValidationException("Could not find existing instrument with identifier prefix [" + searchForm.getIdentifierPrefixEquals() + "] under hierarchy [" + trade.getInvestmentSecurity().getInstrument().getHierarchy().getNameExpanded() + "].");
				}
				if (hierarchy == null) {
					hierarchy = instrument.getHierarchy();
					uploadCommand.getHierarchyNameExpandedMap().put(hierarchy.getNameExpanded(), hierarchy);
				}
				uploadCommand.getInstrumentIdentifierPrefixMap().put("Hierarchy_" + hierarchy.getId() + "_" + instrument.getIdentifierPrefix(), instrument);
			}
			return instrument;
		}
		// If no Hierarchy, see if we can figure it out based on type and identifier prefix only
		String key = "Type_" + uploadCommand.getSimpleTradeType().getInvestmentType().getId() + "_" + trade.getInvestmentSecurity().getInstrument().getIdentifierPrefix();
		InvestmentInstrument instrument = uploadCommand.getInstrumentIdentifierPrefixMap().get(key);
		if (instrument == null) {
			InstrumentSearchForm searchForm = new InstrumentSearchForm();
			searchForm.setIdentifierPrefixEquals(trade.getInvestmentSecurity().getInstrument().getIdentifierPrefix());
			searchForm.setInvestmentTypeId(uploadCommand.getSimpleTradeType().getInvestmentType().getId());

			List<InvestmentInstrument> instrumentList = getInvestmentInstrumentService().getInvestmentInstrumentList(searchForm);
			if (CollectionUtils.isEmpty(instrumentList)) {
				throw new ValidationException("Could not find existing instrument with identifier prefix [" + searchForm.getIdentifierPrefixEquals() + "] and Investment Type [" + uploadCommand.getSimpleTradeType().getInvestmentType().getName() + "].");
			}
			if (instrumentList.size() > 1) {
				throw new ValidationException("Found multiple instruments with identifier prefix [" + searchForm.getIdentifierPrefixEquals() + "] and Investment Type [" + uploadCommand.getSimpleTradeType().getInvestmentType().getName() + "]. [HierarchyExpanded] column is required to limit to specific hierarchy. ");
			}
			instrument = instrumentList.get(0);
			uploadCommand.getInstrumentIdentifierPrefixMap().put(key, instrument);
		}
		return instrument;
	}


	/**
	 * Used for underlying and big security look-ups to find the one that matches based on instrument and exact symbol
	 */
	private InvestmentSecurity getInvestmentSecurityByInstrumentAndSymbol(InvestmentInstrument instrument, String symbol, String propertyName) {
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(symbol);
		securityList = BeanUtils.filter(securityList, security -> security.getInstrument().getId().equals(instrument.getId()));
		// UX on Instrument ID and Security Symbol - never more than one
		InvestmentSecurity security = CollectionUtils.getOnlyElement(securityList);
		if (security == null) {
			throw new ValidationException("Cannot find " + propertyName + " security with symbol [" + symbol + " under instrument [" + instrument.getLabel() + "].");
		}
		return security;
	}

	////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods              //////////
	////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityLocatorService getInvestmentSecurityLocatorService() {
		return this.investmentSecurityLocatorService;
	}


	public void setInvestmentSecurityLocatorService(InvestmentSecurityLocatorService investmentSecurityLocatorService) {
		this.investmentSecurityLocatorService = investmentSecurityLocatorService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TradeExecutionTypeService getTradeExecutionTypeService() {
		return this.tradeExecutionTypeService;
	}


	public void setTradeExecutionTypeService(TradeExecutionTypeService tradeExecutionTypeService) {
		this.tradeExecutionTypeService = tradeExecutionTypeService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}
}
