package com.clifton.trade.file.upload.converter;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.upload.converter.SystemUploadBeanListConverterImpl;
import com.clifton.trade.Trade;
import com.clifton.trade.assetclass.TradeVirtualAllocation;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>TradeUploadConverter</code> performs additional conversion for customized
 * Trade uploads that also allow:
 * 1. Creation of the missing securities.
 * 2. And/Or Using DTC Number to find the correct Executing Broker Company
 * 3. And/Or Using AssetClass-Name and/or Replication-Name.
 * <p>
 * Unmapped column names include specifically: HierarchyExpanded, AssetType, and ExecutingBrokerCompany-DTC Number
 * The rest of the unmapped columns are expected to be custom column values for the security
 *
 * @author manderson
 */
@Component
public class TradeUploadConverter<L extends SystemList> extends SystemUploadBeanListConverterImpl<Trade> {

	private static final String HIERARCHY_EXPANDED_COLUMN_NAME = "HierarchyExpanded";
	private static final String ASSET_TYPE_COLUMN_NAME = "AssetType";

	private static final String EXECUTING_BROKER_DTC = "ExecutingBrokerCompany-DTC Number";

	private static final String ASSET_CLASS_NAME = "InvestmentAssetClass-Name";
	private static final String REPLICATION_NAME = "InvestmentReplication-Name";

	private static final String ROLL_COLUMN_NAME = "Roll";

	private static final String ASSET_TYPE_IDENTIFIER_PREFIX_GENERATOR_LIST_NAME = "Trade Upload: AssetType-Identifier Prefix";

	////////////////////////////////////////////////////////////////////////////////

	private SystemListService<L> systemListService;
	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the {@link Trade} object with InvestmentSecurity fields properly populated for inserts
	 * <p>
	 * Note: Does not validate real data fields, just set the name property for look ups later
	 */
	@Override
	protected List<Trade> populateBeans(Trade bean, Map<String, Object> unmappedPropertyMap) {
		if (bean.getInvestmentSecurity() == null) {
			bean.setInvestmentSecurity(new InvestmentSecurity());
		}
		if (bean.getInvestmentSecurity().getInstrument() == null) {
			bean.getInvestmentSecurity().setInstrument(new InvestmentInstrument());
		}
		String assetType = null;
		List<SystemColumnValue> valueList = new ArrayList<>();
		Converter<Object, String> objectToStringConverter = new ObjectToStringConverter();
		for (Map.Entry<String, Object> unmappedPropertyEntry : CollectionUtils.getIterable(unmappedPropertyMap.entrySet())) {
			String fieldName = unmappedPropertyEntry.getKey();
			Object value = unmappedPropertyEntry.getValue();
			if (value != null) {
				// Special logic will be applied to generate identifier prefix if supported
				if (ASSET_TYPE_COLUMN_NAME.equalsIgnoreCase(fieldName)) {
					assetType = value.toString();
				}
				// Hierarchy Field
				else if (HIERARCHY_EXPANDED_COLUMN_NAME.equalsIgnoreCase(fieldName)) {
					String[] hierarchyNames = value.toString().split("/");
					InvestmentInstrumentHierarchy lastHierarchy = null;
					for (String hierarchyName : hierarchyNames) {
						InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
						hierarchy.setName(hierarchyName.trim());
						if (lastHierarchy != null) {
							hierarchy.setParent(lastHierarchy);
						}
						lastHierarchy = hierarchy;
					}
					bean.getInvestmentSecurity().getInstrument().setHierarchy(lastHierarchy);
				}
				else if (EXECUTING_BROKER_DTC.equalsIgnoreCase(fieldName)) {
					String valueStr = objectToStringConverter.convert(value);
					// Only add if DTC Number is actually populated
					if (!StringUtils.isEmpty(valueStr)) {
						if (bean.getExecutingBrokerCompany() == null) {
							bean.setExecutingBrokerCompany(new BusinessCompany());
						}
						bean.getExecutingBrokerCompany().setDtcNumber(valueStr);
					}
				}
				else if (fieldName.startsWith("InvestmentSecurity-")) {
					// Nested Property Set
					String[] nestedProperties = fieldName.split("-");
					StringBuilder propertyName = new StringBuilder();
					for (String nestedProperty : nestedProperties) {
						propertyName.append('.').append(Character.toLowerCase(nestedProperty.charAt(0))).append(nestedProperty.substring(1));
					}
					BeanUtils.setPropertyValue(bean, propertyName.substring(1), value);
				}
				else if ((ASSET_CLASS_NAME.equalsIgnoreCase(fieldName)) || (REPLICATION_NAME.equalsIgnoreCase(fieldName))) {
					if (!(bean instanceof TradeVirtualAllocation)) {
						TradeVirtualAllocation assetClassTrade = new TradeVirtualAllocation();
						BeanUtils.copyProperties(bean, assetClassTrade);
						bean = assetClassTrade;
					}
					if (ASSET_CLASS_NAME.equalsIgnoreCase(fieldName)) {
						InvestmentAccountAssetClass accountAssetClass = new InvestmentAccountAssetClass();
						accountAssetClass.setAccount(bean.getClientInvestmentAccount());
						accountAssetClass.setAssetClass(new InvestmentAssetClass());
						accountAssetClass.getAssetClass().setName(objectToStringConverter.convert(value));
						((TradeVirtualAllocation) bean).setAccountAssetClass(accountAssetClass);
					}
					else {
						InvestmentReplication replication = new InvestmentReplication();
						replication.setName(objectToStringConverter.convert(value));
						((TradeVirtualAllocation) bean).setReplication(replication);
					}
				}
				else if (ROLL_COLUMN_NAME.equalsIgnoreCase(fieldName)) {
					bean.setRoll(BooleanUtils.isTrue(value));
				}
				// Custom Values
				// This doesn't verify column names at this point and just sets up the value list
				// See {@link TradeUploadDataPopulator} for additional custom value processing
				else {
					SystemColumnCustom customColumn = new SystemColumnCustom();
					customColumn.setName(fieldName);
					SystemColumnValue customValue = new SystemColumnValue();
					customValue.setColumn(customColumn);
					customValue.setText(objectToStringConverter.convert(value));
					valueList.add(customValue);
				}
			}
			if (!valueList.isEmpty()) {
				bean.getInvestmentSecurity().setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
				bean.getInvestmentSecurity().setColumnValueList(valueList);
			}
		}
		populateIdentifierPrefix(assetType, bean);
		return CollectionUtils.createList(bean);
	}


	private void populateIdentifierPrefix(String assetType, Trade bean) {
		if (StringUtils.isEmpty(bean.getInvestmentSecurity().getInstrument().getIdentifierPrefix())) {
			if (!StringUtils.isEmpty(assetType)) {
				SystemListItem systemListItem = getSystemListService().getSystemListItemByListAndValue(ASSET_TYPE_IDENTIFIER_PREFIX_GENERATOR_LIST_NAME, assetType);
				ValidationUtils.assertNotNull(assetType, "Selected asset type is not a valid option. Available options are: [" + StringUtils.collectionToCommaDelimitedString(getSystemListService().getSystemListItemListByList(getSystemListService().getSystemListByName(ASSET_TYPE_IDENTIFIER_PREFIX_GENERATOR_LIST_NAME).getId()), SystemListItem::getValue));
				TemplateConfig config = new TemplateConfig(systemListItem.getText());
				config.addBeanToContext("bean", bean);
				bean.getInvestmentSecurity().getInstrument().setIdentifierPrefix(getTemplateConverter().convert(config));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemListService<L> getSystemListService() {
		return this.systemListService;
	}


	public void setSystemListService(SystemListService<L> systemListService) {
		this.systemListService = systemListService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
