package com.clifton.trade.file.report;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;


/**
 * The <code>TradeReportService</code> defines methods that are used to download/export Trade Reports
 *
 * @author manderson
 */
public interface TradeReportService {

	////////////////////////////////////////////////////////////////////////////
	////////            Trade Report Business Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = Trade.class)
	public FileWrapper downloadTradeReport(int tradeId);
}
