package com.clifton.trade.file.report;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.report.definition.Report;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;


/**
 * The <code>TradeReportServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class TradeReportServiceImpl implements TradeReportService {

	private ReportExportService reportExportService;

	private SecurityUserService securityUserService;

	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////            Trade Report Business Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadTradeReport(int tradeId) {
		// Get the trade
		Trade trade = getTradeService().getTrade(tradeId);
		// Get which report to run for the trade type
		Report tradeReport = trade.getTradeType().getTradeReport();
		if (tradeReport == null) {
			throw new ValidationException("Unable to generate report for Trade # " + tradeId + ". Trade Type [" + trade.getTradeType().getName() + "] does not have a trade report defined.");
		}
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("TradeID", tradeId);
		parameters.put("RunAsUserID", getSecurityUserService().getSecurityUserCurrent().getId());
		return getReportExportService().getReportPDFFile(new ReportExportConfiguration(tradeReport.getId(), parameters, true, true));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
