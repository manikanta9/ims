package com.clifton.trade.restriction.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithTimeSearchForm;
import com.clifton.trade.restriction.TradeRestriction;

import java.math.BigDecimal;


/**
 * The search form for querying {@link TradeRestriction} entities.
 *
 * @author MikeH
 */
public class TradeRestrictionSearchForm extends BaseAuditableEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "clientInvestmentAccount.name,investmentSecurity.name,restrictionType.name,note", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer[] clientAccountIds;

	@SearchField(searchField = "clientInvestmentAccount.name")
	private String clientAccountName;

	@SearchField(searchField = "clientInvestmentAccount.number")
	private String clientAccountNumber;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer securityId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer[] securityIds;

	@SearchField(searchField = "investmentSecurity.name")
	private String securityName;

	@SearchField(searchField = "investmentSecurity.symbol")
	private String securitySymbol;

	@SearchField(searchField = "restrictionType.id")
	private Short restrictionTypeId;

	@SearchField(searchFieldPath = "restrictionType", searchField = "name")
	private String restrictionTypeName;

	@SearchField(searchFieldPath = "restrictionType", searchField = "category.id")
	private Short restrictionTypeCategoryId;

	@SearchField(searchFieldPath = "restrictionType.category", searchField = "name")
	private String restrictionTypeCategoryName;

	@SearchField(searchFieldPath = "restrictionType", searchField = "causeSystemTable.id")
	private Short restrictionTypeCausedSystemTableId;
	@SearchField
	private Integer causeFkFieldId;
	@SearchField
	private BigDecimal restrictionValue;

	@SearchField
	private String note;

	@SearchField(searchField = "note", comparisonConditions = ComparisonConditions.EQUALS)
	private String noteEquals;

	@SearchField
	private Boolean systemManaged;

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}


	public String getClientAccountName() {
		return this.clientAccountName;
	}


	public void setClientAccountName(String clientAccountName) {
		this.clientAccountName = clientAccountName;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public Integer[] getSecurityIds() {
		return this.securityIds;
	}


	public void setSecurityIds(Integer[] securityIds) {
		this.securityIds = securityIds;
	}


	public String getSecurityName() {
		return this.securityName;
	}


	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}


	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	public void setSecuritySymbol(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	public Short getRestrictionTypeId() {
		return this.restrictionTypeId;
	}


	public void setRestrictionTypeId(Short restrictionTypeId) {
		this.restrictionTypeId = restrictionTypeId;
	}


	public String getRestrictionTypeName() {
		return this.restrictionTypeName;
	}


	public void setRestrictionTypeName(String restrictionTypeName) {
		this.restrictionTypeName = restrictionTypeName;
	}


	public Short getRestrictionTypeCategoryId() {
		return this.restrictionTypeCategoryId;
	}


	public void setRestrictionTypeCategoryId(Short restrictionTypeCategoryId) {
		this.restrictionTypeCategoryId = restrictionTypeCategoryId;
	}


	public String getRestrictionTypeCategoryName() {
		return this.restrictionTypeCategoryName;
	}


	public void setRestrictionTypeCategoryName(String restrictionTypeCategoryName) {
		this.restrictionTypeCategoryName = restrictionTypeCategoryName;
	}


	public BigDecimal getRestrictionValue() {
		return this.restrictionValue;
	}


	public void setRestrictionValue(BigDecimal restrictionValue) {
		this.restrictionValue = restrictionValue;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getNoteEquals() {
		return this.noteEquals;
	}


	public void setNoteEquals(String noteEquals) {
		this.noteEquals = noteEquals;
	}


	public Boolean getSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(Boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public Short getRestrictionTypeCausedSystemTableId() {
		return this.restrictionTypeCausedSystemTableId;
	}


	public void setRestrictionTypeCausedSystemTableId(Short restrictionTypeCausedSystemTableId) {
		this.restrictionTypeCausedSystemTableId = restrictionTypeCausedSystemTableId;
	}


	public Integer getCauseFkFieldId() {
		return this.causeFkFieldId;
	}


	public void setCauseFkFieldId(Integer causeFkFieldId) {
		this.causeFkFieldId = causeFkFieldId;
	}
}
