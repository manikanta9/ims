package com.clifton.trade.restriction;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;
import com.clifton.trade.restriction.rule.TradeRestrictionEvaluator;
import com.clifton.trade.restriction.type.TradeRestrictionType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.StringJoiner;


/**
 * The {@link TradeRestriction} entity is used for applying restrictions on accounts and securities. Restrictions may apply guards against configured trade directions or against
 * configured trade limits.
 * <p>
 * {@link TradeRestriction} constraints are applied using the {@link TradeRestrictionEvaluator}.
 *
 * @author MikeH
 */
public class TradeRestriction extends BaseEntity<Integer> implements LabeledObject, SystemEntityModifyConditionAware {

	private TradeRestrictionType restrictionType;

	// scope: client account or security specific
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentSecurity investmentSecurity;

	@SoftLinkField(tableBeanPropertyName = "restrictionType.causeSystemTable")
	private Integer causeFkFieldId;

	/**
	 * Some restriction types require a corresponding value.  For example, Strike Price for the "S/O Minimum Strike Price" restriction.
	 */
	private BigDecimal restrictionValue;

	private Date startDate;
	private Date endDate;

	private String note;

	/**
	 * System managed restrictions cannot be added or removed by users directly and are usually managed via corresponding Workflow Tasks.
	 */
	private boolean systemManaged;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getNoteWithDefaults() {
		if (StringUtils.isEmpty(getNote())) {
			return DateUtils.isDateBetween(new Date(), getStartDate(), getEndDate(), true) ? getRestrictionType().getDefaultLockNote() : getRestrictionType().getDefaultUnlockNote();
		}
		else {
			return getNote();
		}
	}


	public void setNoteWithDefaults(String note) {
		note = StringUtils.trim(note);
		if (!StringUtils.isEqual(note, getRestrictionType().getDefaultLockNote()) && !StringUtils.isEqual(note, getRestrictionType().getDefaultUnlockNote())) {
			setNote(note);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getRestrictionType().getEntityModifyCondition();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringJoiner assignmentJoiner = new StringJoiner(", ");
		ObjectUtils.map(getClientInvestmentAccount(), InvestmentAccount::getNumber).thenConsume(assignmentJoiner::add);
		ObjectUtils.map(getInvestmentSecurity(), InvestmentSecurity::getSymbol).thenConsume(assignmentJoiner::add);

		StringJoiner dateJoiner = new StringJoiner(" to ");
		ObjectUtils.map(getStartDate(), DateUtils::fromDateSmart).thenConsume(dateJoiner::add);
		ObjectUtils.map(getEndDate(), DateUtils::fromDateSmart).thenConsume(dateJoiner::add);

		String restrictionTypeStr = getRestrictionType().getLabel() + (getRestrictionValue() != null ? " [" + getRestrictionValue().toPlainString() + "]" : "");
		String noteStr = ObjectUtils.map(getNote(), " - "::concat).orElseSupply(() -> "").get();

		// Sample label: 012345, SPX: No Trade (2018-06-02 09:00:00 to 2018-07-01) - Call client before trading
		return String.format("%s: %s (%s)%s", assignmentJoiner, restrictionTypeStr, dateJoiner, noteStr);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionType getRestrictionType() {
		return this.restrictionType;
	}


	public void setRestrictionType(TradeRestrictionType restrictionType) {
		this.restrictionType = restrictionType;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BigDecimal getRestrictionValue() {
		return this.restrictionValue;
	}


	public void setRestrictionValue(BigDecimal restrictionValue) {
		this.restrictionValue = restrictionValue;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public boolean isSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public Integer getCauseFkFieldId() {
		return this.causeFkFieldId;
	}


	public void setCauseFkFieldId(Integer causeFkFieldId) {
		this.causeFkFieldId = causeFkFieldId;
	}
}
