package com.clifton.trade.restriction;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.trade.Trade;
import com.clifton.trade.restriction.search.TradeRestrictionSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The {@link TradeRestrictionService} provides methods for retrieving, creating, and modifying {@link TradeRestriction} entities.
 *
 * @author MikeH
 */
public interface TradeRestrictionService {


	////////////////////////////////////////////////////////////////////////////
	////////            Trade Restriction Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRestriction getTradeRestriction(int id);


	public List<TradeRestriction> getTradeRestrictionList(TradeRestrictionSearchForm searchForm);


	/**
	 * Gets the applicable restriction list for the {@link Trade} based on its {@link Trade#tradeDate}.
	 * <p>
	 * If the trade is for the current date then all restrictions which apply at the current instant in time will be applicable. For all other trades, a conservative approach will
	 * be taken and all restrictions which apply at any moment in time on the trade date will be applicable.
	 */
	public List<TradeRestriction> getTradeRestrictionListForTrade(Trade trade);


	public TradeRestriction saveTradeRestriction(TradeRestriction tradeRestriction);


	/**
	 * Method that allows the modification of System Managed Trade Restrictions
	 */
	@DoNotAddRequestMapping
	public TradeRestriction saveTradeRestrictionSystemManaged(TradeRestriction tradeRestriction);


	/**
	 * Attempts to deactivate the {@link TradeRestriction} with the given ID. If the restriction is active (i.e., the current time is between its start and end date), then the restriction will be
	 * disabled by updating its note with the given disable note and setting itstraderestrictionsetupwindow end date to the current date and time. If the restriction has never been active then it will be deleted.
	 */
	@RequestMapping("tradeRestrictionDeactivate")
	public void deactivateTradeRestriction(int id, String disableNote);


	/**
	 * Method that allows the deactivation/deletion of System Managed Trade Restrictions
	 */
	@DoNotAddRequestMapping
	public void deactivateTradeRestrictionSystemManaged(TradeRestriction tradeRestriction, String disableNote);
}
