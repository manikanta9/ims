package com.clifton.trade.restriction.validation;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.restriction.TradeRestriction;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The {@link TradeRestrictionObserver} applies operations on {@link TradeRestriction} DAO operations.
 * <p>
 * This observer normalizes dates for entities in the following ways:
 * <ul>
 * <li>Dates which are entered for earlier today (such as those with no time attached) are given the current time.
 * <li>The millisecond field is cleared for all dates to facilitate client/server matching.
 * </ul>
 * <p>
 * This observer is expected to run before the primary validation logic in {@link TradeRestrictionValidator}.
 *
 * @author MikeH
 */
@Component
public class TradeRestrictionObserver extends SelfRegisteringDaoObserver<TradeRestriction> {

	@Override
	@ValueIgnoringGetter
	public int getOrder() {
		// Perform value normalization prior to TradeRestrictionValidator
		return -100;
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeTransactionMethodCallImpl(ReadOnlyDAO<TradeRestriction> dao, DaoEventTypes event, TradeRestriction restriction) {
		if (event.isInsert()) {
			processInsert(restriction);
		}
		else if (event.isUpdate()) {
			processUpdate(restriction, dao);
		}

		// Revalidate start and end date equality in case normalization introduces conflicts
		ValidationUtils.assertNotEquals(restriction.getStartDate(), restriction.getEndDate(), "The start and end date values may not be equal.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void processInsert(TradeRestriction restriction) {
		Date currentDate = new Date();

		// Normalize the start and end date to the current time if they are in the past
		if (restriction.getStartDate() == null || DateUtils.isDateAfter(currentDate, restriction.getStartDate())) {
			restriction.setStartDate(currentDate);
		}
		if (restriction.getEndDate() != null && DateUtils.isDateAfter(currentDate, restriction.getEndDate())) {
			restriction.setEndDate(currentDate);
		}
	}


	private void processUpdate(TradeRestriction restriction, ReadOnlyDAO<TradeRestriction> dao) {
		Date currentDate = new Date();
		TradeRestriction originalBean = getOriginalBean(dao, restriction);

		// Normalize changed start and end dates. Dates from the UI will have seconds precision.
		// If the seconds difference between the original and provided date is 0, update the provided with the original's value.
		// Update to the current time if they are in the past.
		if (CompareUtils.compare(originalBean.getStartDate(), restriction.getStartDate()) != 0) {
			if (DateUtils.getSecondsDifference(originalBean.getStartDate(), restriction.getStartDate()) == 0) {
				restriction.setStartDate(originalBean.getStartDate());
			}
			else if (DateUtils.isDateAfter(currentDate, restriction.getStartDate())) {
				restriction.setStartDate(currentDate);
			}
		}
		if (CompareUtils.compare(originalBean.getEndDate(), restriction.getEndDate()) != 0) {
			if (restriction.getEndDate() != null) {
				if (originalBean.getEndDate() != null && DateUtils.getSecondsDifference(originalBean.getEndDate(), restriction.getEndDate()) == 0) {
					restriction.setEndDate(originalBean.getEndDate());
				}
				else if (DateUtils.isDateAfter(currentDate, restriction.getEndDate())) {
					restriction.setEndDate(currentDate);
				}
			}
		}
	}
}
