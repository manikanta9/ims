package com.clifton.trade.restriction.type;

import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.type.search.TradeRestrictionTypeCategorySearchForm;
import com.clifton.trade.restriction.type.search.TradeRestrictionTypeSearchForm;

import java.util.List;


/**
 * The {@link TradeRestrictionTypeService} provides methods for retrieving, creating, and modifying {@link TradeRestriction} entities.
 *
 * @author StevenF
 */
public interface TradeRestrictionTypeService {


	////////////////////////////////////////////////////////////////////////////
	////////          Trade Restriction Type Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionType getTradeRestrictionType(short id);


	public TradeRestrictionType getTradeRestrictionTypeByName(String restrictionTypeName);


	public List<TradeRestrictionType> getTradeRestrictionTypeList(TradeRestrictionTypeSearchForm searchForm);


	public TradeRestrictionType saveTradeRestrictionType(TradeRestrictionType restrictionType);


	public void deleteTradeRestrictionType(short id);

	////////////////////////////////////////////////////////////////////////////
	////////        Trade Restriction Category Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionTypeCategory getTradeRestrictionTypeCategory(short id);


	public List<TradeRestrictionTypeCategory> getTradeRestrictionTypeCategoryList(TradeRestrictionTypeCategorySearchForm searchForm);


	public TradeRestrictionTypeCategory saveTradeRestrictionTypeCategory(TradeRestrictionTypeCategory restrictionCategory);


	public void deleteTradeRestrictionTypeCategory(short id);
}
