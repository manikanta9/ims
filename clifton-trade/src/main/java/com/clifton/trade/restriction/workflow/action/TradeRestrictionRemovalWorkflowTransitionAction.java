package com.clifton.trade.restriction.workflow.action;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.search.TradeRestrictionSearchForm;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.WorkflowTransition;

import java.util.List;


/**
 * Workflow Transition Action to remove/deactivate a TradeRestriction associated with a linkedEntity
 * that is present on the WorkflowTask. The associatedEntity MUST contain an InvestmentSecurity AND/OR InvestmentAccount
 *
 * @author theodorez
 */
public class TradeRestrictionRemovalWorkflowTransitionAction extends BaseTradeRestrictionWorkflowTransitionAction {


	@Override
	public WorkflowTask processAction(WorkflowTask task, WorkflowTransition transition) {
		IdentityObject linkedEntity = getLinkedEntity(task);
		InvestmentAccount clientAccount = getClientAccount(linkedEntity);
		InvestmentSecurity investmentSecurity = getInvestmentSecurity(linkedEntity);
		ValidationUtils.assertFalse(clientAccount == null && investmentSecurity == null, "Both the Client Account and the Investment Security of the " + task.getLinkedEntityLabel() + " cannot be null");

		TradeRestrictionSearchForm restrictionSearchForm = new TradeRestrictionSearchForm();
		if (clientAccount != null) {
			restrictionSearchForm.setClientAccountId(clientAccount.getId());
		}
		if (investmentSecurity != null) {
			restrictionSearchForm.setSecurityId(investmentSecurity.getId());
		}
		TradeRestrictionType restrictionType = getTradeRestrictionType();
		if (restrictionType != null) {
			restrictionSearchForm.setRestrictionTypeName(restrictionType.getName());
			if (restrictionType.getCauseSystemTable() != null && DaoUtils.isDtoClassTableMatch(restrictionType.getCauseSystemTable().getName(), task.getClass())) {
				restrictionSearchForm.setCauseFkFieldId(task.getId());
			}
		}
		restrictionSearchForm.setNoteEquals(getRestrictionLockNote(task, restrictionType));
		restrictionSearchForm.setActive(true);
		restrictionSearchForm.setSystemManaged(true);

		List<TradeRestriction> restrictionList = getTradeRestrictionService().getTradeRestrictionList(restrictionSearchForm);
		for (TradeRestriction restriction : CollectionUtils.getIterable(restrictionList)) {
			getTradeRestrictionService().deactivateTradeRestrictionSystemManaged(restriction, getRestrictionUnlockNote(task, restrictionType));
		}
		return task;
	}
}
