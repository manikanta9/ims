package com.clifton.trade.restriction.type.evaluator;

import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class TradeRestrictionTypeSTOMinimumStrikeLimitEvaluator extends BaseTradeRestrictionTypeLimitEvaluator {

	/**
	 * Returns {@code true} if the given trade satisfies the given restriction limit with the provided limit value.
	 */
	@Override
	protected boolean isTradeWithinRestrictionLimit(Trade trade, BigDecimal restrictionValue) {
		// Handle application-enforced constraint issues
		return MathUtils.isGreaterThanOrEqual(trade.getInvestmentSecurity().getOptionStrikePrice(), restrictionValue);
	}
}
