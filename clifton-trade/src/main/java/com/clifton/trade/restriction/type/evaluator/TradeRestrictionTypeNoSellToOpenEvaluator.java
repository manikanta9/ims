package com.clifton.trade.restriction.type.evaluator;

import com.clifton.trade.Trade;
import com.clifton.trade.restriction.TradeRestriction;


/**
 * @author StevenF
 */
public class TradeRestrictionTypeNoSellToOpenEvaluator extends BaseTradeRestrictionTypeEvaluator {

	@Override
	public boolean isValidTrade(Trade trade, TradeRestriction restriction) {
		return !isSellToOpen(trade);
	}
}
