package com.clifton.trade.restriction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyProviderResult;
import com.clifton.trade.restriction.search.TradeRestrictionBrokerSearchForm;
import org.springframework.stereotype.Component;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Provides a list of allowed (included) and / or disallowed (excluded) brokers for a given account
 *
 * @author davidi
 */
@Component
public class TradeRestrictionBrokerCompanyProvider {

	private TradeRestrictionBrokerService tradeRestrictionBrokerService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeExecutingBrokerCompanyProviderResult getAllowedBrokerCompanyList(InvestmentAccount clientAccount) {

		ValidationUtils.assertNotNull(clientAccount, "Client Account cannot be null.");

		TradeExecutingBrokerCompanyProviderResult providerResult = new TradeExecutingBrokerCompanyProviderResult();

		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountId(clientAccount.getId());
		List<TradeRestrictionBroker> tradeRestrictionBrokerList = getTradeRestrictionBrokerService().getTradeRestrictionBrokerList(searchForm);

		Map<Boolean, Set<BusinessCompany>> restrictionSetByInclusionMap = CollectionUtils.getStream(tradeRestrictionBrokerList)
				.collect(Collectors.groupingBy(
						TradeRestrictionBroker::isInclude,
						Collectors.mapping(TradeRestrictionBroker::getBrokerCompany, Collectors.toCollection(LinkedHashSet::new))
				));

		providerResult.setIncludeExecutingBrokerCompanyList(restrictionSetByInclusionMap.get(true));
		providerResult.setExcludedExecutingBrokerCompanyList(restrictionSetByInclusionMap.get(false));

		return providerResult;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionBrokerService getTradeRestrictionBrokerService() {
		return this.tradeRestrictionBrokerService;
	}


	public void setTradeRestrictionBrokerService(TradeRestrictionBrokerService tradeRestrictionBrokerService) {
		this.tradeRestrictionBrokerService = tradeRestrictionBrokerService;
	}
}
