package com.clifton.trade.restriction.type.evaluator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;

import java.math.BigDecimal;
import java.util.Date;


/**
 * This class is the base bean for building comparison restrictions for option trades, for example, setting a tradeProperty of investmentSecurity.lastDeliveryDate
 * and a tradeDifferenceProperty of tradeDate and a comparisonType of Greater Than or Equal and a Restriction value of 28 will create a trade maturity restriction for 28 days or more.
 *
 * @author JasonS
 */
public class TradeRestrictionTypeTradeValueComparisonEvaluator extends BaseTradeRestrictionTypeLimitEvaluator implements ValidationAware {

	private String tradeProperty;
	private String tradeDifferenceProperty;
	private TradeComparisonTypes comparisonType;


	@Override
	public boolean isTradeWithinRestrictionLimit(Trade trade, BigDecimal restrictionValue) {
		validate();

		Object value = BeanUtils.getPropertyValue(trade, getTradeProperty());
		if (StringUtils.isEmpty(getTradeDifferenceProperty())) {
			return getComparisonType().compare((Number)value , restrictionValue);
		}
		return getComparisonType().compareDifference(value , BeanUtils.getPropertyValue(trade, getTradeDifferenceProperty()), restrictionValue);
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getTradeProperty(), "The Trade Property is a required field");
		ValidationUtils.assertNotNull(getComparisonType(), "The Comparison Type is a required field");
		Class<?> propertyTypeClazz = BeanUtils.getPropertyType(Trade.class, getTradeProperty());

		if (!Number.class.isAssignableFrom(propertyTypeClazz)) {
			ValidationUtils.assertNotEmpty(getTradeDifferenceProperty(), "Trade Difference Property is required when comparing values that are not numeric.");
		}

		if (getComparisonType() != TradeComparisonTypes.EQUAL) {
			ValidationUtils.assertTrue((Number.class.isAssignableFrom(propertyTypeClazz) || Date.class.isAssignableFrom(propertyTypeClazz)), "Non-numeric or Date fields can only be compared using the Equals comparison type.");
		}
		if (!StringUtils.isEmpty(getTradeDifferenceProperty())) {
			Class<?> differencePropertyTypeClazz = BeanUtils.getPropertyType(Trade.class, getTradeDifferenceProperty());
			ValidationUtils.assertEquals(propertyTypeClazz, differencePropertyTypeClazz, String.format("Trade Property {%s} and Trade Difference Property {%s} are not of the same type.", getTradeProperty(), getTradeDifferenceProperty()));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                Private Helper Class                 //////////////
	////////////////////////////////////////////////////////////////////////////////


	protected enum TradeComparisonTypes {
		LESS_THAN {
			@Override
			public boolean compare(Number value, BigDecimal restrictionValue) {
				return MathUtils.isLessThan(value, restrictionValue);
			}
			@Override
			public boolean compareDifference(Object valueOne, Object valueTwo, BigDecimal restrictionValue) {
				Number compareValue = Integer.MAX_VALUE;
				if (valueOne instanceof Number) {
					compareValue = getNumericDifference(MathUtils.getNumberAsBigDecimal((Number) valueOne), MathUtils.getNumberAsBigDecimal((Number) valueOne));
				}
				else if (valueOne instanceof Date) {
					compareValue = getDayDifference((Date) valueOne, (Date) valueTwo);
				}
				return MathUtils.isLessThan(compareValue, restrictionValue);
			}
		}, LESS_THAN_OR_EQUAL {
			@Override
			public boolean compare(Number value, BigDecimal restrictionValue) {
				return MathUtils.isLessThanOrEqual(value, restrictionValue);
			}
			@Override
			public boolean compareDifference(Object valueOne, Object valueTwo, BigDecimal restrictionValue) {
				Number compareValue = Integer.MAX_VALUE;
				if (valueOne instanceof Number) {
					compareValue = getNumericDifference(MathUtils.getNumberAsBigDecimal((Number) valueOne), MathUtils.getNumberAsBigDecimal((Number) valueOne));
				}
				else if (valueOne instanceof Date) {
					compareValue = getDayDifference((Date) valueOne, (Date) valueTwo);
				}
				return MathUtils.isLessThanOrEqual(compareValue, restrictionValue);
			}
		}, EQUAL {
			@Override
			public boolean compare(Number value, BigDecimal restrictionValue) {
				return MathUtils.isEqual(value, restrictionValue);
			}
			@Override
			public boolean compareDifference(Object valueOne, Object valueTwo, BigDecimal restrictionValue) {
				Number compareValue = null;
				if (valueOne instanceof Number) {
					compareValue = getNumericDifference(MathUtils.getNumberAsBigDecimal((Number) valueOne), MathUtils.getNumberAsBigDecimal((Number) valueOne));
				}
				else if (valueOne instanceof Date) {
					compareValue = getDayDifference((Date) valueOne, (Date) valueTwo);
				}
				else if (valueOne instanceof String) {
					return StringUtils.isEqual((String) valueOne, (String) valueTwo);
				}
				return MathUtils.isEqual(compareValue, restrictionValue);
			}
		}, GREATER_THAN_OR_EQUAL {
			@Override
			public boolean compare(Number value, BigDecimal restrictionValue) {
				return MathUtils.isGreaterThanOrEqual(value, restrictionValue);
			}
			@Override
			public boolean compareDifference(Object valueOne, Object valueTwo, BigDecimal restrictionValue) {
				Number compareValue = null;
				if (valueOne instanceof Number) {
					compareValue = getNumericDifference(MathUtils.getNumberAsBigDecimal((Number) valueOne), MathUtils.getNumberAsBigDecimal((Number) valueOne));
				}
				else if (valueOne instanceof Date) {
					compareValue = getDayDifference((Date) valueOne, (Date) valueTwo);
				}
				return MathUtils.isGreaterThanOrEqual(compareValue, restrictionValue);
			}
		}, GREATER_THAN {
			@Override
			public boolean compare(Number value, BigDecimal restrictionValue) {
				return MathUtils.isGreaterThan(value, restrictionValue);
			}
			@Override
			public boolean compareDifference(Object valueOne, Object valueTwo, BigDecimal restrictionValue) {
				Number compareValue = null;
				if (valueOne instanceof Number) {
					compareValue = getNumericDifference(MathUtils.getNumberAsBigDecimal((Number) valueOne), MathUtils.getNumberAsBigDecimal((Number) valueOne));
				}
				else if (valueOne instanceof Date) {
					compareValue = getDayDifference((Date) valueOne, (Date) valueTwo);
				}
				return MathUtils.isGreaterThan(compareValue, restrictionValue);
			}
		};


		public abstract boolean compare(Number value, BigDecimal restrictionValue);
		public abstract boolean compareDifference(Object valueOne, Object valueTwo, BigDecimal restrictionValue);

		public BigDecimal getNumericDifference(BigDecimal valueOne, BigDecimal valueTwo) {
			return MathUtils.subtract(valueOne, valueTwo);
		}

		public BigDecimal getDayDifference(Date valueOne, Date valueTwo) {
			return MathUtils.abs(BigDecimal.valueOf(DateUtils.getDaysDifference(valueOne, valueTwo)));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getTradeProperty() {
		return this.tradeProperty;
	}


	public void setTradeProperty(String tradeProperty) {
		this.tradeProperty = tradeProperty;
	}


	public String getTradeDifferenceProperty() {
		return this.tradeDifferenceProperty;
	}


	public void setTradeDifferenceProperty(String tradeDifferenceProperty) {
		this.tradeDifferenceProperty = tradeDifferenceProperty;
	}


	public TradeComparisonTypes getComparisonType() {
		return this.comparisonType;
	}


	public void setComparisonType(TradeComparisonTypes comparisonType) {
		this.comparisonType = comparisonType;
	}
}
