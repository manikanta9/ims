package com.clifton.trade.restriction.type;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.trade.restriction.TradeRestriction;


/**
 * The category of restrictions that may be imposed by {@link TradeRestriction} entities.
 * Groups {@link TradeRestrictionType} entities.
 *
 * @author StevenF
 */
public class TradeRestrictionTypeCategory extends NamedEntityWithoutLabel<Short> {

}
