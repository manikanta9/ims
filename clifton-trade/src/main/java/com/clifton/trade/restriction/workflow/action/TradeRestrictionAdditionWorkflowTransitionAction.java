package com.clifton.trade.restriction.workflow.action;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.WorkflowTransition;

import java.util.Date;


/**
 * Workflow Transition Action to create a Trade Restriction for a given WorkflowTask
 * that has a linked object containing either an InvestmentAccount and/or InvestmentSecurity
 *
 * @author theodorez
 */
public class TradeRestrictionAdditionWorkflowTransitionAction extends BaseTradeRestrictionWorkflowTransitionAction {


	@Override
	public WorkflowTask processAction(WorkflowTask task, WorkflowTransition transition) {
		IdentityObject linkedEntity = getLinkedEntity(task);
		TradeRestriction restriction = new TradeRestriction();
		restriction.setClientInvestmentAccount(getClientAccount(linkedEntity));
		restriction.setInvestmentSecurity(getInvestmentSecurity(linkedEntity));
		restriction.setStartDate(new Date());
		TradeRestrictionType restrictionType = getTradeRestrictionType();
		restriction.setRestrictionType(restrictionType);
		restriction.setSystemManaged(true);
		if (restrictionType != null && restrictionType.getCauseSystemTable() != null && DaoUtils.isDtoClassTableMatch(restrictionType.getCauseSystemTable().getName(), task.getClass())) {
			restriction.setCauseFkFieldId(task.getId());
		}
		restriction.setNote(getRestrictionLockNote(task, restrictionType));
		getTradeRestrictionService().saveTradeRestrictionSystemManaged(restriction);
		return task;
	}
}
