package com.clifton.trade.restriction.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeEvaluator;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistorySearchForm;
import com.clifton.workflow.history.WorkflowHistoryService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The {@code TradeRestrictionEvaluator} is a {@link RuleEvaluator} for validating applicable {@link TradeRestriction} entities against a {@link Trade}. This validates trades
 * against active restrictions for trade directions and trade limits, such as restrictions which freeze an account to prevent any trades, or restrictions which freeze a position to
 * prevent any sells on that position.
 * <p>
 * This evaluator finds all {@link TradeRestriction} entities which are applicable to a given {@link Trade}. Logic corresponding to these restrictions is then executed against the
 * trade to check for violations.
 * <p>
 * Trades which have previously reached their {@link TradeService#TRADE_EXECUTED_STATE_NAME executed} state will not be validated. These trades will be assumed to have been
 * validated previously. The reason for this is that {@link TradeRestriction} rules are intended to be applied as rails to prevent certain actions, rather than as rigid hard-stops
 * which will verifiably disallow certain actions. Additionally, {@link TradeRestrictionType trade direction} restrictions (e.g., {@link TradeRestrictionType NO_SELL} and
 * {@link TradeRestrictionType NO_BUY}) cannot be changed after a trade has been executed, rendering re-validation useless in these cases. Finally, limit
 * restriction types such as 'Minimum Strike' and 'Minimum Underlying' require live market data. Neither our current market data retrievers nor the metadata attached to {@link Trade} entities
 * would allow us to obtain this information reliably.
 *
 * @author MikeH
 */
public class TradeRestrictionEvaluator extends BaseRuleEvaluator<Trade, TradeRuleEvaluatorContext> {

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemSchemaService systemSchemaService;
	private TradeRestrictionService tradeRestrictionService;
	private WorkflowHistoryService workflowHistoryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(Trade trade, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		// Guard-clause: Skip evaluation if the Trade is excluded
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig.isExcluded()) {
			return Collections.emptyList();
		}

		// Skip evaluation if the trade has already been executed; restrictions are hard-stops on initial validation only
		if (isExecuted(trade)) {
			LogUtils.debug(LogCommand.ofMessageSupplier(TradeRestrictionEvaluator.class, () -> String.format("Skipping Trade Restriction evaluation for trade [%d]. The trade has already been executed. Restrictions will not be re-evaluated.", trade.getId())));
			return Collections.emptyList();
		}

		// Evaluate Trade Restrictions against the given Trade
		SystemTable restrictionTable = getSystemSchemaService().getSystemTableByName("TradeRestriction");
		List<TradeRestriction> restrictionList = getTradeRestrictionService().getTradeRestrictionListForTrade(trade);
		return restrictionList.stream()
				.filter(restriction -> isRestrictionViolated(trade, restriction))
				.map(restriction -> getRuleViolationService().createRuleViolationWithCause(entityConfig, trade.getSourceEntityId(), restriction.getId(), restrictionTable))
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns {@code true} if the given trade has already reached its {@link TradeService#TRADE_EXECUTED_STATE_NAME executed} state.
	 */
	private boolean isExecuted(Trade trade) {
		WorkflowHistorySearchForm searchForm = new WorkflowHistorySearchForm();
		searchForm.setLimit(1);
		searchForm.setEntityId(BeanUtils.getIdentityAsLong(trade));
		searchForm.setTableName("Trade");
		searchForm.setEndWorkflowStateNameEquals(TradeService.TRADE_EXECUTED_STATE_NAME);
		List<WorkflowHistory> workflowHistoryList = getWorkflowHistoryService().getWorkflowHistoryList(searchForm);
		return !CollectionUtils.isEmpty(workflowHistoryList);
	}


	/**
	 * Returns {@code true} if the given trade violates the provided restriction.
	 */
	private boolean isRestrictionViolated(Trade trade, TradeRestriction restriction) {
		// Guard-clause: Skip evaluation if evaluate condition returns false
		if (restriction.getRestrictionType().getEvaluateCondition() != null
				&& !getSystemConditionEvaluationHandler().isConditionTrue(restriction.getRestrictionType().getEvaluateCondition(), trade)) {
			LogUtils.debug(LogCommand.ofMessageSupplier(TradeRestrictionEvaluator.class, () -> String.format("Skipping Trade Restriction evaluation for trade [%d] and restriction ID [%d] of type [%s]. The evaluate condition has returned false.", trade.getId(), restriction.getId(), restriction.getRestrictionType().getName())));
			return false;
		}
		TradeRestrictionTypeEvaluator restrictionEvaluator = (TradeRestrictionTypeEvaluator) getSystemBeanService().getBeanInstance(restriction.getRestrictionType().getEvaluatorBean());
		return !restrictionEvaluator.isValidTrade(trade, restriction);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public TradeRestrictionService getTradeRestrictionService() {
		return this.tradeRestrictionService;
	}


	public void setTradeRestrictionService(TradeRestrictionService tradeRestrictionService) {
		this.tradeRestrictionService = tradeRestrictionService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}
}
