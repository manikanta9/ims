package com.clifton.trade.restriction.live;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.websocket.WebSocketHandler;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;


/**
 * The task scheduler for updating {@link TradeRestriction} states on the client in real-time.
 * <p>
 * This task scheduler automatically sends updates to clients for newly activated or expired restrictions.
 *
 * @author MikeH
 */
@Component
public class TradeRestrictionSubscriptionScheduler {

	private TaskScheduler taskScheduler;
	private WebSocketHandler webSocketHandler;


	/**
	 * The lock object for synchronization around publishing {@link TradeRestriction} updates to clients.
	 * <p>
	 * Operations which involve sending {@link TradeRestriction} updates to clients should, in most cases, synchronize on this lock object. This is especially important for
	 * scheduled updates, as these run on separate threads and are <i>incremental</i> updates, thus having the potential to perform an out-of-order incrementation operation due to
	 * race conditions when not properly synchronized.
	 */
	private final Object restrictionEventLock = new Object();
	/**
	 * The restriction task which will send all pending restriction events.
	 */
	private ScheduledFuture<?> restrictionTask;
	/**
	 * The ordered queue of restriction events to send out.
	 */
	private final Queue<TradeRestrictionEvent> restrictionEventQueue = new PriorityQueue<>(this::compareTradeRestrictionEventOrder);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Queues a new restriction event to occur now.
	 */
	public void queueRestrictionEvent(TradeRestriction restriction, boolean newRestrictionState) {
		getRestrictionEventQueue().add(new TradeRestrictionEvent(restriction, newRestrictionState, new Date()));
	}


	/**
	 * Schedules update tasks for the given restriction. These tasks will be used to send messages to clients indicating that the restriction has become active or has expired.
	 */
	public void scheduleRestrictionUpdates(TradeRestriction restriction) {
		Date currentDate = new Date();
		if (restriction.getStartDate() != null && DateUtils.compare(restriction.getStartDate(), currentDate, true) >= 0) {
			getRestrictionEventQueue().add(new TradeRestrictionEvent(restriction, true, restriction.getStartDate()));
		}
		if (restriction.getEndDate() != null && DateUtils.compare(restriction.getEndDate(), currentDate, true) >= 0) {
			getRestrictionEventQueue().add(new TradeRestrictionEvent(restriction, false, restriction.getEndDate()));
		}
	}


	/**
	 * Cancels all pending tasks for the given restriction.
	 */
	public void cancelRestrictionEvents(TradeRestriction restriction) {
		getRestrictionEventQueue().removeIf(event -> CompareUtils.isEqual(restriction, event.restriction));
	}


	/**
	 * Updates the scheduled task to be executed at the next event occurrence. This should be executed after all current queue manipulations have been completed.
	 */
	public void updateNextEventTime(boolean cancelRunningTask) {
		// Cancel existing task if in process
		if (cancelRunningTask && getRestrictionTask() != null) {
			getRestrictionTask().cancel(true);
		}

		// Schedule next task
		TradeRestrictionEvent firstEvent = getRestrictionEventQueue().peek();
		if (firstEvent != null) {
			setRestrictionTask(getTaskScheduler().schedule(this::processQueuedEvents, firstEvent.date));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void processQueuedEvents() {
		synchronized (getRestrictionEventLock()) {
			// Cancel execution if interrupted, allowing for task refresh
			if (Thread.interrupted()) {
				return;
			}

			// Find all pending events
			List<TradeRestrictionEvent> pendingEventList = new ArrayList<>();
			Date currentDate = new Date();
			TradeRestrictionEvent event;
			while ((event = getRestrictionEventQueue().peek()) != null && DateUtils.compare(event.date, currentDate, true) <= 0) {
				AssertUtils.assertTrue(event == getRestrictionEventQueue().poll(), "An unexpected concurrent mutation has occurred.");
				pendingEventList.add(event);
			}

			// Aggregate and send pending events
			List<TradeRestriction> addRestrictionList = pendingEventList.stream()
					.filter(evt -> evt.newRestrictionState)
					.map(evt -> evt.restriction)
					.collect(Collectors.toList());
			List<TradeRestriction> removeRestrictionList = pendingEventList.stream()
					.filter(evt -> !evt.newRestrictionState)
					.map(evt -> evt.restriction)
					.collect(Collectors.toList());
			TradeRestrictionSubscriptionMessage message = new TradeRestrictionSubscriptionMessage(false, addRestrictionList, removeRestrictionList);
			getWebSocketHandler().sendMessage(TradeRestrictionSubscriptionService.CHANNEL_TOPIC_TRADE_RESTRICTION, message);

			// Schedule next task
			updateNextEventTime(false);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class TradeRestrictionEvent {

		private final TradeRestriction restriction;
		private final boolean newRestrictionState;
		private final Date date;


		public TradeRestrictionEvent(TradeRestriction restriction, boolean newRestrictionState, Date date) {
			this.restriction = restriction;
			this.newRestrictionState = newRestrictionState;
			this.date = date;
		}
	}


	private int compareTradeRestrictionEventOrder(TradeRestrictionEvent event1, TradeRestrictionEvent event2) {
		int dateCompare = CompareUtils.compare(event1.date, event2.date);
		int stateCompare = -CompareUtils.compare(event1.newRestrictionState, event2.newRestrictionState);
		return dateCompare != 0
				? dateCompare
				: stateCompare;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ScheduledFuture<?> getRestrictionTask() {
		return this.restrictionTask;
	}


	private void setRestrictionTask(ScheduledFuture<?> restrictionTask) {
		this.restrictionTask = restrictionTask;
	}


	private Queue<TradeRestrictionEvent> getRestrictionEventQueue() {
		return this.restrictionEventQueue;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TaskScheduler getTaskScheduler() {
		return this.taskScheduler;
	}


	public void setTaskScheduler(TaskScheduler taskScheduler) {
		this.taskScheduler = taskScheduler;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}


	public Object getRestrictionEventLock() {
		return this.restrictionEventLock;
	}
}
