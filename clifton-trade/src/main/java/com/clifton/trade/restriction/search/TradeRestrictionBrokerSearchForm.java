package com.clifton.trade.restriction.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.account.InvestmentAccount;


/**
 * SearchForm for querying TradeRestrictionBroker entities based on their properties.
 *
 * @author davidi
 */
public class TradeRestrictionBrokerSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "clientAccount.name,clientAccount.shortName,clientAccount.number,brokerCompany.name")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] ids;

	@SearchField(searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "clientAccount.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] clientAccountIds;

	@SearchField(searchField = "clientAccount.number")
	private String clientAccountNumber;

	@SearchField(searchField = "clientAccount.number,clientAccount.name")
	private String clientAccountLabel;

	@SearchField(searchField = "clientAccount.number", comparisonConditions = ComparisonConditions.EQUALS)
	private String clientAccountNumberEquals;

	@SearchField(searchField = "brokerCompany.id")
	private Integer brokerCompanyId;

	@SearchField(searchField = "brokerCompany.name")
	private String brokerCompanyName;

	@SearchField(searchField = "brokerCompany.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String brokerCompanyNameEquals;

	@SearchField
	private Boolean include;


	////////////////////////////////////////////////////////////////////////////
	///////////              Getter and Setter Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeRestrictionBrokerSearchForm ofClientAccount(InvestmentAccount clientAccount, boolean include) {
		TradeRestrictionBrokerSearchForm tradeRestrictionBrokerSearchForm = new TradeRestrictionBrokerSearchForm();
		tradeRestrictionBrokerSearchForm.setClientAccountId(clientAccount.getId());
		tradeRestrictionBrokerSearchForm.setInclude(include);
		return tradeRestrictionBrokerSearchForm;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}


	public String getClientAccountNumber() {
		return this.clientAccountNumber;
	}


	public void setClientAccountNumber(String clientAccountNumber) {
		this.clientAccountNumber = clientAccountNumber;
	}


	public String getClientAccountNumberEquals() {
		return this.clientAccountNumberEquals;
	}


	public void setClientAccountNumberEquals(String clientAccountNumberEquals) {
		this.clientAccountNumberEquals = clientAccountNumberEquals;
	}


	public String getClientAccountLabel() {
		return this.clientAccountLabel;
	}


	public void setClientAccountLabel(String clientAccountLabel) {
		this.clientAccountLabel = clientAccountLabel;
	}


	public Integer getBrokerCompanyId() {
		return this.brokerCompanyId;
	}


	public void setBrokerCompanyId(Integer brokerCompanyId) {
		this.brokerCompanyId = brokerCompanyId;
	}


	public String getBrokerCompanyName() {
		return this.brokerCompanyName;
	}


	public void setBrokerCompanyName(String brokerCompanyName) {
		this.brokerCompanyName = brokerCompanyName;
	}


	public String getBrokerCompanyNameEquals() {
		return this.brokerCompanyNameEquals;
	}


	public void setBrokerCompanyNameEquals(String brokerCompanyNameEquals) {
		this.brokerCompanyNameEquals = brokerCompanyNameEquals;
	}


	public Boolean getInclude() {
		return this.include;
	}


	public void setInclude(Boolean include) {
		this.include = include;
	}
}
