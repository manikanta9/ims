package com.clifton.trade.restriction.workflow.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import com.clifton.trade.restriction.type.TradeRestrictionTypeService;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * Base Transition Action for Objects that need to create/remove TradeRestrictions.
 * <p>
 * Using this with Objects requires that they contain at least an Investment Account AND/OR InvestmentSecurity.
 *
 * @author theodorez
 */
public abstract class BaseTradeRestrictionWorkflowTransitionAction implements WorkflowTransitionActionHandler<WorkflowTask>, ValidationAware {

	private DaoLocator daoLocator;

	private TradeRestrictionService tradeRestrictionService;
	private TradeRestrictionTypeService tradeRestrictionTypeService;

	private InvestmentAccountService investmentAccountService;

	private Integer linkedEntityTableId;
	private String clientAccountBeanPropertyPath;
	private String investmentSecurityBeanPropertyPath;
	private Short tradeRestrictionTypeId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getLinkedEntityTableId(), "Linked Entity Table must be specified");
		ValidationUtils.assertNotNull(getTradeRestrictionTypeId(), "Restriction Type must be specified");
		ValidationUtils.assertFalse(StringUtils.isEmpty(getClientAccountBeanPropertyPath()) && StringUtils.isEmpty(getInvestmentSecurityBeanPropertyPath()), "Both Client Account Bean Path and Investment Security cannot be null.");
	}


	/**
	 * Gets a standard format String to define the task and linked entity.
	 * <p>
	 * If the provided restriction type has a cause table and default lock note, the default  note will be used and null will be returned.
	 * The restriction type's default lock note is handled in the saving of the restriction.
	 */
	protected String getRestrictionLockNote(WorkflowTask task, TradeRestrictionType restrictionType) {
		return (restrictionType != null && restrictionType.getCauseSystemTable() != null && restrictionType.getDefaultLockNote() != null)
				? null : "Workflow Task [" + task.getId() + "] for modified Entity: " + task.getLinkedEntityLabel();
	}


	/**
	 * Gets a standard format String to define the task and linked entity during deactivating a restriction.
	 * <p>
	 * If the provided restriction type has a cause table and default unlock note, the default note will be used and null will be returned.
	 * The restriction type's default unlock note is handled in the saving of the restriction.
	 */
	protected String getRestrictionUnlockNote(WorkflowTask task, TradeRestrictionType restrictionType) {
		return (restrictionType != null && restrictionType.getCauseSystemTable() != null && restrictionType.getDefaultUnlockNote() != null)
				? null : "Deactivated by Workflow Task: " + task.getId() + " in state: " + task.getWorkflowState().getName() + " for modified " + task.getLinkedEntityLabel() + " : " + task.getLinkedEntityFkFieldId();
	}


	/**
	 * Gets the InvestmentSecurity located at the investmentSecurityBeanPropertyPath
	 */
	protected InvestmentSecurity getInvestmentSecurity(IdentityObject linkedEntity) {
		Object investmentSecurityObject = BeanUtils.getPropertyValue(linkedEntity, getInvestmentSecurityBeanPropertyPath());
		if (investmentSecurityObject instanceof InvestmentSecurity) {
			return (InvestmentSecurity) investmentSecurityObject;
		}
		return null;
	}


	/**
	 * Gets the InvestmentAccount located at the clientAccountBeanPropertyPath
	 */
	protected InvestmentAccount getClientAccount(IdentityObject linkedEntity) {
		Object clientAccountObject = BeanUtils.getPropertyValue(linkedEntity, getClientAccountBeanPropertyPath());
		if (clientAccountObject instanceof InvestmentAccount) {
			return (InvestmentAccount) clientAccountObject;
		}
		return null;
	}


	/**
	 * Gets an actual IdentityObject from the Linked Entity's DAO
	 */
	protected IdentityObject getLinkedEntity(WorkflowTask task) {
		SystemTable linkedEntityTable = task.getDefinition().getLinkedEntityTable();
		ValidationUtils.assertNotNull(linkedEntityTable, "Linked Entity Table must be configured on the Workflow Task Definition to use this Transition Action");
		ValidationUtils.assertTrue(MathUtils.isEqual(linkedEntityTable.getId(), getLinkedEntityTableId()), "The Linked Entity Table on the Workflow Task Definition must match the Target Table on the Workflow Transition Action");
		ReadOnlyDAO<IdentityObject> linkedEntityDao = getDaoLocator().locate(linkedEntityTable.getName());
		return linkedEntityDao.findByPrimaryKey(task.getLinkedEntityFkFieldId());
	}


	/**
	 * Gets an actual TradeRestrictionType from the id
	 */
	protected TradeRestrictionType getTradeRestrictionType() {
		return getTradeRestrictionTypeService().getTradeRestrictionType(getTradeRestrictionTypeId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public TradeRestrictionService getTradeRestrictionService() {
		return this.tradeRestrictionService;
	}


	public void setTradeRestrictionService(TradeRestrictionService tradeRestrictionService) {
		this.tradeRestrictionService = tradeRestrictionService;
	}


	public TradeRestrictionTypeService getTradeRestrictionTypeService() {
		return this.tradeRestrictionTypeService;
	}


	public void setTradeRestrictionTypeService(TradeRestrictionTypeService tradeRestrictionTypeService) {
		this.tradeRestrictionTypeService = tradeRestrictionTypeService;
	}


	public Integer getLinkedEntityTableId() {
		return this.linkedEntityTableId;
	}


	public void setLinkedEntityTableId(Integer linkedEntityTableId) {
		this.linkedEntityTableId = linkedEntityTableId;
	}


	public String getClientAccountBeanPropertyPath() {
		return this.clientAccountBeanPropertyPath;
	}


	public void setClientAccountBeanPropertyPath(String clientAccountBeanPropertyPath) {
		this.clientAccountBeanPropertyPath = clientAccountBeanPropertyPath;
	}


	public String getInvestmentSecurityBeanPropertyPath() {
		return this.investmentSecurityBeanPropertyPath;
	}


	public void setInvestmentSecurityBeanPropertyPath(String investmentSecurityBeanPropertyPath) {
		this.investmentSecurityBeanPropertyPath = investmentSecurityBeanPropertyPath;
	}


	public Short getTradeRestrictionTypeId() {
		return this.tradeRestrictionTypeId;
	}


	public void setTradeRestrictionTypeId(Short tradeRestrictionTypeId) {
		this.tradeRestrictionTypeId = tradeRestrictionTypeId;
	}
}
