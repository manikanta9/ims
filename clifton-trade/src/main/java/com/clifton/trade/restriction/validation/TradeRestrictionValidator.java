package com.clifton.trade.restriction.validation;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.validation.CoreValidationUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The validator for {@link TradeRestriction} DAO operations.
 *
 * @author MikeH
 */
@Component
public class TradeRestrictionValidator extends SelfRegisteringDaoValidator<TradeRestriction> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(TradeRestriction restriction, DaoEventTypes config) throws ValidationException {
		// NOTHING HERE - USES METHOD WITH DAO PARAM
	}


	@Override
	public void validate(TradeRestriction restriction, DaoEventTypes config, ReadOnlyDAO<TradeRestriction> restrictionDAO) throws ValidationException {
		if (config.isInsert()) {
			validateInsert(restriction);
		}
		else if (config.isUpdate()) {
			validateUpdate(restriction);
		}
		if (restriction.getRestrictionType().isDoNotAllowOverlapInDates() && (config.isInsert() || config.isUpdate())) {
			validateNoOverlap(restriction, restrictionDAO);
		}
		validateFields(restriction);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateInsert(TradeRestriction restriction) {
		// Validate start date not in past; be lenient up to the beginning of the current day
		Date currentDate = new Date();
		ValidationUtils.assertTrue(restriction.getStartDate() == null || DateUtils.isDateBeforeOrEqual(currentDate, restriction.getStartDate(), false), "The start date may not take place in the past.", "startDate");
		ValidationUtils.assertTrue(restriction.getEndDate() == null || DateUtils.isDateBeforeOrEqual(currentDate, restriction.getEndDate(), false), "The end date may not take place in the past.", "endDate");
	}


	private void validateUpdate(TradeRestriction restriction) {
		TradeRestriction originalBean = getOriginalBean(restriction);
		Date currentDate = new Date();

		// Restrict mutable fields for active restrictions
		if (DateUtils.isDateAfter(currentDate, originalBean.getStartDate())) {
			CoreValidationUtils.assertAllowedModifiedFields(restriction, originalBean, "endDate", "note", "noteWithDefaults");
		}

		// Restrict end-date modification for expired restrictions
		if (originalBean.getEndDate() != null && DateUtils.isDateAfter(currentDate, originalBean.getEndDate())) {
			ValidationUtils.assertEquals(restriction.getEndDate(), originalBean.getEndDate(), "The end date cannot be changed for expired Trade Restrictions.", "endDate");
		}

		// Validate modified start and end date not in past; be lenient up to the beginning of the current day
		if (CompareUtils.compare(originalBean.getStartDate(), restriction.getStartDate()) != 0) {
			ValidationUtils.assertTrue(restriction.getStartDate() == null || DateUtils.isDateBeforeOrEqual(currentDate, restriction.getStartDate(), false), "The start date may not take place in the past.", "startDate");
		}
		if (CompareUtils.compare(originalBean.getEndDate(), restriction.getEndDate()) != 0) {
			ValidationUtils.assertTrue(restriction.getEndDate() == null || DateUtils.isDateBeforeOrEqual(currentDate, restriction.getEndDate(), false), "The end date may not take place in the past.", "endDate");
		}
	}


	//TODO - I've used this same type of validation in at least 3 places now - provide default implementation somewhere?
	private void validateNoOverlap(TradeRestriction restriction, ReadOnlyDAO<TradeRestriction> tradeRestrictionDAO) {
		TradeRestrictionType restrictionType = restriction.getRestrictionType();
		InvestmentAccount investmentAccount = restriction.getClientInvestmentAccount();
		InvestmentSecurity investmentSecurity = restriction.getInvestmentSecurity();
		List<TradeRestriction> tradeRestrictionList =
				tradeRestrictionDAO.findByFields(
						new String[]{"restrictionType.id", "clientInvestmentAccount.id", "investmentSecurity.id"},
						new Object[]{
								restrictionType != null ? restrictionType.getId() : null,
								investmentAccount != null ? investmentAccount.getId() : null,
								investmentSecurity != null ? investmentSecurity.getId() : null
						});
		//Validate there are no exclusion assignment conflicts
		for (TradeRestriction existingRestriction : CollectionUtils.getIterable(tradeRestrictionList)) {
			if (!existingRestriction.equals(restriction) && DateUtils.isOverlapInDates(existingRestriction.getStartDate(), existingRestriction.getEndDate(), restriction.getStartDate(), restriction.getEndDate())) {
				throw new ValidationException("A trade restriction already exists with an overlapping date range.  Please either edit the existing restriction, or end it and start a new one.");
			}
		}
	}


	private void validateFields(TradeRestriction restriction) {
		// Perform general field validation
		if (restriction.getRestrictionType().isRestrictionValueRequired()) {
			ValidationUtils.assertTrue(restriction.getRestrictionValue() != null, () -> String.format("[%s] restrictions must have a configured value.", restriction.getRestrictionType().getLabel()), "restrictionType");
		}
		ValidationUtils.assertTrue(restriction.getClientInvestmentAccount() == null || restriction.getClientInvestmentAccount().getType().isOurAccount(), "Trade Restrictions may only be applied to client accounts.", "investmentAccount");
		ValidationUtils.assertTrue(restriction.getClientInvestmentAccount() != null || restriction.getInvestmentSecurity() != null, "Global restrictions are not supported. One or both of a client account or a security must be specified.");
		ValidationUtils.assertTrue(DateUtils.isDateAfter(restriction.getEndDate(), restriction.getStartDate()), "The end date must be after the start date.", "endDate");
	}
}
