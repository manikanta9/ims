package com.clifton.trade.restriction.type.evaluator;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * An evaluator for "No Open" trade restrictions.
 *
 * @author davidi
 */
public class TradeRestrictionTypeNoOpenEvaluator extends BaseTradeRestrictionTypeEvaluator {

	private AccountingPositionService accountingPositionService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isValidTrade(Trade trade, TradeRestriction restriction) {

		return !isOpeningTrade(trade);
	}


	private boolean isOpeningTrade(Trade trade) {

		TradeOpenCloseType tradeOpenCloseType = trade.getOpenCloseType();

		if (tradeOpenCloseType.isOpen() && tradeOpenCloseType.isExplicitOpenClose()) {
			return true;
		}

		// if closing trades are not possible, its an opening trade.
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		if (investmentSecurity.getInstrument().getHierarchy().isCloseOnMaturityOnly() && !DateUtils.isEqualWithoutTime(trade.getTradeDate(), investmentSecurity.getLastDeliveryOrEndDate())) {
			return true;
		}

		BigDecimal totalRemainingQuantity = getPositionTotalRemainingQuantity(trade);

		// if the Trade's direction matches the security's quantity (e.g. sell with a 0 or negative position quantity,  or buy with a 0 or positive quantity) its an open.
		if ((MathUtils.isLessThanOrEqual(totalRemainingQuantity, BigDecimal.ZERO) && !trade.isBuy()) || (MathUtils.isGreaterThanOrEqual(totalRemainingQuantity, BigDecimal.ZERO) && trade.isBuy())) {
			return true;
		}

		// its an open if the trade quantity exceeds the position's total remaining quantity.  The trade is a closing trade unless it exceeds the total remaining quantity (overruns the 0 point).
		if (trade.getTradeType().isCrossingZeroAllowed() && MathUtils.isGreaterThan(trade.getQuantityIntended(), MathUtils.abs(totalRemainingQuantity))) {
			return true;
		}

		return false;
	}


	private BigDecimal getPositionTotalRemainingQuantity(Trade trade) {
		List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionList(trade.getClientInvestmentAccount().getId(), trade.getHoldingInvestmentAccount().getId(), trade.getInvestmentSecurity().getId(), trade.getTradeDate());
		return CoreMathUtils.sumProperty(positionList, AccountingPosition::getRemainingQuantity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}
}
