package com.clifton.trade.restriction.type.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The search form for querying {@link com.clifton.trade.restriction.type.TradeRestrictionType} entities.
 *
 * @author StevenF
 */
public class TradeRestrictionTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "category.id")
	private Short categoryId;

	@SearchField(searchFieldPath = "category", searchField = "name")
	private String categoryName;

	@SearchField
	private Boolean systemManaged;

	@SearchField
	private Boolean doNotAllowOverlapInDates;

	@SearchField
	private Boolean investmentAccountRequired;

	@SearchField
	private Boolean investmentSecurityRequired;

	@SearchField
	private Boolean restrictionNoteRequired;

	@SearchField(searchField = "evaluateCondition.id")
	private Integer evaluateConditionId;

	@SearchField(searchField = "evaluateCondition.name")
	private String evaluateConditionName;

	@SearchField(searchField = "violationIgnoreCondition.id")
	private Integer violationIgnoreConditionId;

	@SearchField(searchField = "violationIgnoreCondition.name")
	private String violationIgnoreConditionName;

	@SearchField(searchField = "entityModifyCondition.id")
	private Integer entityModifyConditionId;

	@SearchField(searchField = "entityModifyCondition.name")
	private String entityModifyConditionName;

	@SearchField(searchField = "causeSystemTable.id")
	private Short causeSystemTableId;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] restrictionTypeNamesEqual;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Short[] restrictionTypeIds;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Boolean getSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(Boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public Boolean getDoNotAllowOverlapInDates() {
		return this.doNotAllowOverlapInDates;
	}


	public void setDoNotAllowOverlapInDates(Boolean doNotAllowOverlapInDates) {
		this.doNotAllowOverlapInDates = doNotAllowOverlapInDates;
	}


	public Boolean getInvestmentAccountRequired() {
		return this.investmentAccountRequired;
	}


	public void setInvestmentAccountRequired(Boolean investmentAccountRequired) {
		this.investmentAccountRequired = investmentAccountRequired;
	}


	public Boolean getInvestmentSecurityRequired() {
		return this.investmentSecurityRequired;
	}


	public void setInvestmentSecurityRequired(Boolean investmentSecurityRequired) {
		this.investmentSecurityRequired = investmentSecurityRequired;
	}


	public Boolean getRestrictionNoteRequired() {
		return this.restrictionNoteRequired;
	}


	public void setRestrictionNoteRequired(Boolean restrictionNoteRequired) {
		this.restrictionNoteRequired = restrictionNoteRequired;
	}


	public Integer getEvaluateConditionId() {
		return this.evaluateConditionId;
	}


	public void setEvaluateConditionId(Integer evaluateConditionId) {
		this.evaluateConditionId = evaluateConditionId;
	}


	public String getEvaluateConditionName() {
		return this.evaluateConditionName;
	}


	public void setEvaluateConditionName(String evaluateConditionName) {
		this.evaluateConditionName = evaluateConditionName;
	}


	public Integer getViolationIgnoreConditionId() {
		return this.violationIgnoreConditionId;
	}


	public void setViolationIgnoreConditionId(Integer violationIgnoreConditionId) {
		this.violationIgnoreConditionId = violationIgnoreConditionId;
	}


	public String getViolationIgnoreConditionName() {
		return this.violationIgnoreConditionName;
	}


	public void setViolationIgnoreConditionName(String violationIgnoreConditionName) {
		this.violationIgnoreConditionName = violationIgnoreConditionName;
	}


	public Integer getEntityModifyConditionId() {
		return this.entityModifyConditionId;
	}


	public void setEntityModifyConditionId(Integer entityModifyConditionId) {
		this.entityModifyConditionId = entityModifyConditionId;
	}


	public String getEntityModifyConditionName() {
		return this.entityModifyConditionName;
	}


	public void setEntityModifyConditionName(String entityModifyConditionName) {
		this.entityModifyConditionName = entityModifyConditionName;
	}


	public Short getCauseSystemTableId() {
		return this.causeSystemTableId;
	}


	public void setCauseSystemTableId(Short causeSystemTableId) {
		this.causeSystemTableId = causeSystemTableId;
	}


	public String[] getRestrictionTypeNamesEqual() {
		return this.restrictionTypeNamesEqual;
	}


	public void setRestrictionTypeNamesEqual(String[] restrictionTypeNamesEqual) {
		this.restrictionTypeNamesEqual = restrictionTypeNamesEqual;
	}


	public Short[] getRestrictionTypeIds() {
		return this.restrictionTypeIds;
	}


	public void setRestrictionTypeIds(Short[] restrictionTypeIds) {
		this.restrictionTypeIds = restrictionTypeIds;
	}
}
