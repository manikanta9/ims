package com.clifton.trade.restriction;

import com.clifton.trade.restriction.search.TradeRestrictionBrokerSearchForm;

import java.util.List;


/**
 * The <code>TradeRestrictionBrokerService</code> is a service used to manage the TradeBrokerRestriction
 * table allowing the addition of new entries, deletion of existing entries, and query support.
 *
 * @author davidi
 */
public interface TradeRestrictionBrokerService {

	////////////////////////////////////////////////////////////////////////////
	//////               Trade Restriction Broker Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	TradeRestrictionBroker getTradeRestrictionBroker(int id);


	List<TradeRestrictionBroker> getTradeRestrictionBrokerList(TradeRestrictionBrokerSearchForm searchForm);


	TradeRestrictionBroker saveTradeRestrictionBroker(TradeRestrictionBroker bean);


	void deleteTradeRestrictionBroker(TradeRestrictionBroker bean);
}
