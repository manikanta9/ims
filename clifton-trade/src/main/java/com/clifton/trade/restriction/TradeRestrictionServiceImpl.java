package com.clifton.trade.restriction;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.restriction.search.TradeRestrictionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.StringJoiner;


/**
 * The implementation of {@link TradeRestrictionService} for {@link TradeRestriction} DAO actions.
 *
 * @author MikeH
 */
@Service
public class TradeRestrictionServiceImpl implements TradeRestrictionService {

	private AdvancedUpdatableDAO<TradeRestriction, Criteria> tradeRestrictionDAO;


	////////////////////////////////////////////////////////////////////////////
	////////            Trade Restriction Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRestriction getTradeRestriction(int id) {
		return getTradeRestrictionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeRestriction> getTradeRestrictionList(TradeRestrictionSearchForm searchForm) {
		return getTradeRestrictionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<TradeRestriction> getTradeRestrictionListForTrade(Trade trade) {
		Date currentDate = new Date();
		boolean isTradeToday = DateUtils.isEqualWithoutTime(currentDate, trade.getTradeDate());
		Date startDate = isTradeToday ? currentDate : DateUtils.clearTime(trade.getTradeDate());
		Date endDate = isTradeToday ? currentDate : DateUtils.getEndOfDay(startDate);
		return getTradeRestrictionListForTradeInDateRange(trade, startDate, endDate);
	}


	/**
	 * Retrieves all {@link TradeRestriction} entities which may apply to the given {@link Trade} at any point between the given {@code startDate} and {@code endDate}.
	 */
	private List<TradeRestriction> getTradeRestrictionListForTradeInDateRange(Trade trade, Date startDate, Date endDate) {
		return getTradeRestrictionDAO().findBySearchCriteria((HibernateSearchConfigurer) criteria -> {
			// Match client account if present on Trade Restriction
			Criterion restrictionAccountIsNull = Restrictions.isNull("clientInvestmentAccount");
			Criterion restrictionAccountMatches = Restrictions.eq("clientInvestmentAccount", trade.getClientInvestmentAccount());
			criteria.add(Restrictions.or(restrictionAccountIsNull, restrictionAccountMatches));

			// Match security or underlying security if present on Trade Restriction
			Criterion restrictionSecurityIsNull = Restrictions.isNull("investmentSecurity");
			Criterion restrictionSecurityMatches = Restrictions.eq("investmentSecurity", trade.getInvestmentSecurity());
			Criterion restrictionSecurityMatchesUnderlying = Restrictions.eq("investmentSecurity", trade.getInvestmentSecurity().getUnderlyingSecurity());
			criteria.add(Restrictions.or(restrictionSecurityIsNull, restrictionSecurityMatches, restrictionSecurityMatchesUnderlying));

			// Filter to all restrictions applicable during date range
			if (endDate != null) {
				criteria.add(Restrictions.le("startDate", endDate));
			}
			criteria.add(Restrictions.or(Restrictions.isNull("endDate"), Restrictions.ge("endDate", startDate)));
		});
	}


	@Override
	public TradeRestriction saveTradeRestriction(TradeRestriction tradeRestriction) {
		ValidationUtils.assertFalse(tradeRestriction.isSystemManaged(), "Trade Restrictions that are System Managed cannot be Created or Modified by users.");
		if (tradeRestriction.getRestrictionType() != null && tradeRestriction.getNote() == null) {
			tradeRestriction.setNote(tradeRestriction.getRestrictionType().getDefaultRestrictionNote());
		}
		return getTradeRestrictionDAO().save(tradeRestriction);
	}


	@Override
	public TradeRestriction saveTradeRestrictionSystemManaged(TradeRestriction tradeRestriction) {
		if (tradeRestriction.getRestrictionType() != null) {
			tradeRestriction.setSystemManaged(BooleanUtils.anyTrue(tradeRestriction.isSystemManaged(), tradeRestriction.getRestrictionType().isSystemManaged()));
		}
		return getTradeRestrictionDAO().save(tradeRestriction);
	}


	@Override
	public void deactivateTradeRestriction(int id, String disableNote) {
		TradeRestriction restriction = getTradeRestriction(id);
		ValidationUtils.assertFalse(restriction.isSystemManaged(), "Trade Restrictions that are System Managed cannot be deleted by users.");
		deleteOrDeactivateTradeRestriction(restriction, disableNote);
	}


	@Override
	public void deactivateTradeRestrictionSystemManaged(TradeRestriction tradeRestriction, String disableNote) {
		deleteOrDeactivateTradeRestriction(tradeRestriction, disableNote);
	}


	/**
	 * Deactivates the trade restriction if it has ever been active (or is currently active).
	 * <p>
	 * If the restriction has never been active then it will be deleted.
	 */
	private void deleteOrDeactivateTradeRestriction(TradeRestriction restriction, String disableNote) {
		Date currentDate = new Date();
		if (DateUtils.isDateAfter(currentDate, restriction.getStartDate())) {
			// Disable restrictions past their start date
			ValidationUtils.assertTrue(restriction.getEndDate() == null || DateUtils.isDateAfter(restriction.getEndDate(), currentDate), "The Trade Restriction has already expired. Expired Trade Restrictions cannot be modified.");
			restriction.setEndDate(currentDate);
			// Append disable note
			if (!StringUtils.isEmpty(disableNote)) {
				StringJoiner noteJoiner = new StringJoiner(StringUtils.NEW_LINE + StringUtils.NEW_LINE);
				if (!StringUtils.isEmpty(restriction.getNote())) {
					noteJoiner.add(restriction.getNote());
				}
				noteJoiner.add(String.format("Disabled: %s", disableNote));
				restriction.setNote(noteJoiner.toString());
			}
			if (restriction.isSystemManaged()) {
				saveTradeRestrictionSystemManaged(restriction);
			}
			else {
				saveTradeRestriction(restriction);
			}
		}
		else {
			// Delete restrictions which have not yet been put into effect
			getTradeRestrictionDAO().delete(restriction.getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeRestriction, Criteria> getTradeRestrictionDAO() {
		return this.tradeRestrictionDAO;
	}


	public void setTradeRestrictionDAO(AdvancedUpdatableDAO<TradeRestriction, Criteria> tradeRestrictionDAO) {
		this.tradeRestrictionDAO = tradeRestrictionDAO;
	}
}
