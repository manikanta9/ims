package com.clifton.trade.restriction.type.evaluator;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class TradeRestrictionTypeSTOMinimumUnderlyingLimitEvaluator extends BaseTradeRestrictionTypeLimitEvaluator {

	/**
	 * If this property is set to true, the evaluator will change its evaluation calculations to determine if the trade's option InvestmentSecurity
	 * is such that the option OTM (out of the money) percentage is greater than or equal to specified minimum percentage value. Takes into consideration
	 * whether the option is a call or put option, as the OTM regions relative to the underlying price are the opposite of each other:
	 * The strike price for a call option must be greater than the underlying price by a specified percentage.
	 * The strike price for a put option must be less than the underlying price by a specified percentage.
	 */
	public Boolean usePercentOutOfTheMoney;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns {@code true} if the given trade satisfies the given restriction limit with the provided limit value.
	 */
	@Override
	protected boolean isTradeWithinRestrictionLimit(Trade trade, BigDecimal restrictionValue) {
		// FX rates are not taken into account here; we assume that the price is provided in the currency of the underlying
		MarketDataLive livePrice = getMarketDataRetriever().getLivePrice(trade.getInvestmentSecurity().getUnderlyingSecurity());
		AssertUtils.assertNotNull(livePrice, "No live price could be located for the underlying security [%s] of the derivative security [%s].", trade.getInvestmentSecurity().getUnderlyingSecurity().getSymbol(), trade.getInvestmentSecurity().getSymbol());

		if (getUsePercentOutOfTheMoney() != null && getUsePercentOutOfTheMoney()) {
			BigDecimal strikePrice = trade.getInvestmentSecurity().getOptionStrikePrice();
			BigDecimal underlyingPrice = livePrice.asNumericValue();

			BigDecimal outOfTheMoneyValue;
			if (trade.getInvestmentSecurity().isCallOption()) {
				outOfTheMoneyValue = MathUtils.subtract(strikePrice, underlyingPrice);
			}
			else {
				// OTM Value for a put option
				outOfTheMoneyValue = MathUtils.subtract(underlyingPrice, strikePrice);
			}

			BigDecimal outOfTheMoneyPercent = CoreMathUtils.getPercentValue(outOfTheMoneyValue, underlyingPrice, true);
			return MathUtils.isGreaterThanOrEqual(outOfTheMoneyPercent, restrictionValue);
		}

		return MathUtils.isGreaterThanOrEqual(livePrice.asNumericValue(), restrictionValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getUsePercentOutOfTheMoney() {
		return this.usePercentOutOfTheMoney;
	}


	public void setUsePercentOutOfTheMoney(Boolean usePercentOutOfTheMoney) {
		this.usePercentOutOfTheMoney = usePercentOutOfTheMoney;
	}
}
