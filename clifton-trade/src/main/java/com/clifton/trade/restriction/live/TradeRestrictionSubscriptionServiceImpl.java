package com.clifton.trade.restriction.live;

import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.search.TradeRestrictionSearchForm;
import org.springframework.stereotype.Controller;

import java.util.List;


/**
 * The default implementation of {@link TradeRestrictionSubscriptionService}.
 *
 * @author MikeH
 */
@Controller
public class TradeRestrictionSubscriptionServiceImpl implements TradeRestrictionSubscriptionService {

	private TradeRestrictionService tradeRestrictionService;
	private TradeRestrictionSubscriptionScheduler tradeRestrictionSubscriptionScheduler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRestrictionSubscriptionMessage getTradeRestrictionSubscriptionMessage() {
		synchronized (getTradeRestrictionSubscriptionScheduler().getRestrictionEventLock()) {
			TradeRestrictionSearchForm searchForm = new TradeRestrictionSearchForm();
			searchForm.setActive(true);
			List<TradeRestriction> restrictionList = this.getTradeRestrictionService().getTradeRestrictionList(searchForm);
			return TradeRestrictionSubscriptionMessage.reset(restrictionList);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionService getTradeRestrictionService() {
		return this.tradeRestrictionService;
	}


	public void setTradeRestrictionService(TradeRestrictionService tradeRestrictionService) {
		this.tradeRestrictionService = tradeRestrictionService;
	}


	public TradeRestrictionSubscriptionScheduler getTradeRestrictionSubscriptionScheduler() {
		return this.tradeRestrictionSubscriptionScheduler;
	}


	public void setTradeRestrictionSubscriptionScheduler(TradeRestrictionSubscriptionScheduler tradeRestrictionSubscriptionScheduler) {
		this.tradeRestrictionSubscriptionScheduler = tradeRestrictionSubscriptionScheduler;
	}
}
