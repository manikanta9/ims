package com.clifton.trade.restriction.live;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.restriction.TradeRestriction;
import org.springframework.messaging.simp.annotation.SubscribeMapping;


/**
 * The service interface for subscriptions to {@link TradeRestriction}s. This interface provides methods for subscribing to {@link TradeRestriction} changes.
 * <p>
 * Updates for the provided channels are sent to users through the {@link TradeRestrictionSubscriptionObserver} DAO observer.
 *
 * @author MikeH
 */
public interface TradeRestrictionSubscriptionService {

	// TODO: Move these static fields to messages rather than interfaces? (For all instances)
	/**
	 * The WebSocket channel for {@link TradeRestriction} entities.
	 */
	public static final String CHANNEL_TOPIC_TRADE_RESTRICTION = "/topic/trade/restriction";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the current list of active Trade Restrictions. This method handles the STOMP <tt>SUBSCRIBE</tt> frames for the {@link #CHANNEL_TOPIC_TRADE_RESTRICTION} channel.
	 */
	@SecureMethod(dtoClass = TradeRestriction.class)
	@SubscribeMapping(CHANNEL_TOPIC_TRADE_RESTRICTION)
	@DoNotAddRequestMapping
	public TradeRestrictionSubscriptionMessage getTradeRestrictionSubscriptionMessage();
}
