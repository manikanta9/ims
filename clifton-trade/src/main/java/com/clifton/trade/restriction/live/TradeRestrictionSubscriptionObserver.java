package com.clifton.trade.restriction.live;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.date.DateUtils;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.websocket.WebSocketHandler;
import org.springframework.stereotype.Component;


/**
 * The DAO observer for {@link TradeRestriction} subscription operations.
 * <p>
 * This observer uses {@link WebSocketHandler WebSockets} to send messages to subscribed clients. Clients may subscribe through the channels listed in {@link
 * TradeRestrictionSubscriptionService}.
 *
 * @author MikeH
 */
@Component
public class TradeRestrictionSubscriptionObserver extends SelfRegisteringDaoObserver<TradeRestriction> {

	private TradeRestrictionSubscriptionScheduler tradeRestrictionSubscriptionScheduler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<TradeRestriction> dao, DaoEventTypes event, TradeRestriction restriction, Throwable e) {
		// Do not run observer during exceptions
		if (e != null) {
			return;
		}

		processTradeRestrictionUpdates(event, restriction);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void processTradeRestrictionUpdates(DaoEventTypes event, TradeRestriction restriction) {
		synchronized (getTradeRestrictionSubscriptionScheduler().getRestrictionEventLock()) {
			getTradeRestrictionSubscriptionScheduler().cancelRestrictionEvents(restriction);
			queueImmediateTradeRestrictionUpdates(event, restriction);
			scheduleFutureTradeRestrictionUpdates(event, restriction);
			getTradeRestrictionSubscriptionScheduler().updateNextEventTime(true);
		}
	}


	private void scheduleFutureTradeRestrictionUpdates(DaoEventTypes event, TradeRestriction restriction) {
		// Schedule future update events on restriction start/expiration
		if (event.isInsert() || event.isUpdate()) {
			getTradeRestrictionSubscriptionScheduler().scheduleRestrictionUpdates(restriction);
		}
	}


	private void queueImmediateTradeRestrictionUpdates(DaoEventTypes event, TradeRestriction restriction) {
		// Send update message to subscribers
		EventOperations operation = getSubscriptionMessageForEvent(event, restriction);
		switch (operation) {
			case ADD:
				getTradeRestrictionSubscriptionScheduler().queueRestrictionEvent(restriction, true);
				break;
			case REMOVE:
				getTradeRestrictionSubscriptionScheduler().queueRestrictionEvent(restriction, false);
				break;
			case NONE:
			default:
				// Do nothing
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates and returns the subscription message to send to clients for the given event, or {@code null} if no message should be sent to clients.
	 */
	private EventOperations getSubscriptionMessageForEvent(DaoEventTypes event, TradeRestriction restriction) {
		final EventOperations operation;
		if (event.isDelete()) {
			operation = EventOperations.REMOVE;
		}
		else if (event.isInsert()) {
			operation = DateUtils.isCurrentDateBetween(restriction.getStartDate(), restriction.getEndDate(), true)
					? EventOperations.ADD
					: EventOperations.NONE;
		}
		else if (event.isUpdate()) {
			operation = DateUtils.isCurrentDateBetween(restriction.getStartDate(), restriction.getEndDate(), true)
					? EventOperations.ADD
					: EventOperations.REMOVE;
		}
		else {
			throw new IllegalArgumentException(String.format("Unexpected DaoEventType [%s]. Only modification types are supported.", event));
		}
		return operation;
	}


	/**
	 * The enumerated event operations which may occur for subscription events.
	 */
	private enum EventOperations {
		ADD,
		REMOVE,
		NONE
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionSubscriptionScheduler getTradeRestrictionSubscriptionScheduler() {
		return this.tradeRestrictionSubscriptionScheduler;
	}


	public void setTradeRestrictionSubscriptionScheduler(TradeRestrictionSubscriptionScheduler tradeRestrictionSubscriptionScheduler) {
		this.tradeRestrictionSubscriptionScheduler = tradeRestrictionSubscriptionScheduler;
	}
}
