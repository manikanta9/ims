package com.clifton.trade.restriction.type;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.rule.TradeRestrictionEvaluator;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeEvaluator;


/**
 * The types of restrictions that may be imposed by {@link TradeRestriction} entities. Behavior corresponding to each of these restrictions is implemented by {@link
 * TradeRestrictionEvaluator}.
 *
 * @author StevenF
 */
public class TradeRestrictionType extends NamedEntityWithoutLabel<Short> implements SystemEntityModifyConditionAware {

	private TradeRestrictionTypeCategory category;

	/**
	 * Implements restriction type specific evaluation logic: {@link TradeRestrictionTypeEvaluator}.
	 * Returns true or false (pass or fail) for a specific restriction and trade.
	 */
	private SystemBean evaluatorBean;

	/**
	 * If present, this condition will be used to determine whether the restriction will be evaluated for the given trade.
	 * Can optionally be used to reduce the scope of a particular restriction based on trad attributes.
	 */
	private SystemCondition evaluateCondition;

	/**
	 * If present, this condition may be used by the rules engine to determine whether violations raised by this restriction type may be ignored after they are raised.
	 * <p>
	 * This differs from {@link #evaluateCondition} in that it is used to determine whether violations that are <i>already raised</i> may be ignored, while the {@link
	 * #evaluateCondition} may be used to determine whether restrictions of this type will be evaluated in the first place.
	 * <p>
	 * This is a configuration-only property and is typically not used by the system programmatically.
	 */
	private SystemCondition violationIgnoreCondition;

	/**
	 * Optional condition - can be used to allow overriding ability to modify restrictions of this type
	 */
	private SystemCondition entityModifyCondition;


	private SystemTable causeSystemTable;

	/**
	 * Defines if {@link TradeRestriction} entities are created as systemManaged.
	 * SystemManaged item are by default unmodifiable by users unless overridden by the entityModifyCondition
	 */
	private boolean systemManaged;

	/**
	 * Defines if {@link TradeRestriction} entities of this type are allowed to have overlapping dates.
	 * Most restrictions allow overlap; however, NOT_RECONCILED does not.
	 */
	private boolean doNotAllowOverlapInDates;

	/**
	 * Defines if an account is required when defining a trade restriction of this type
	 */
	private boolean investmentAccountRequired;

	/**
	 * Defines if a security is required when defining a trade restriction of this type
	 */
	private boolean investmentSecurityRequired;

	/**
	 * Defines if a restriction note is required when defining a trade restriction of this type
	 */
	private boolean restrictionNoteRequired;

	/**
	 * Defines if a restriction note is required when defining a trade restriction of this type (e.g. for limit based restrictions)
	 */
	private boolean restrictionValueRequired;


	/**
	 * The default restriction note to be used if no note is set when saving restriction instances.
	 */
	private String defaultRestrictionNote;

	private String defaultLockNote;

	private String defaultUnlockNote;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBean getEvaluatorBean() {
		return this.evaluatorBean;
	}


	public void setEvaluatorBean(SystemBean evaluatorBean) {
		this.evaluatorBean = evaluatorBean;
	}


	public SystemTable getCauseSystemTable() {
		return this.causeSystemTable;
	}


	public void setCauseSystemTable(SystemTable causeSystemTable) {
		this.causeSystemTable = causeSystemTable;
	}


	public TradeRestrictionTypeCategory getCategory() {
		return this.category;
	}


	public void setCategory(TradeRestrictionTypeCategory category) {
		this.category = category;
	}


	public String getDefaultLockNote() {
		return this.defaultLockNote;
	}


	public void setDefaultLockNote(String defaultLockNote) {
		this.defaultLockNote = defaultLockNote;
	}


	public String getDefaultUnlockNote() {
		return this.defaultUnlockNote;
	}


	public void setDefaultUnlockNote(String defaultUnlockNote) {
		this.defaultUnlockNote = defaultUnlockNote;
	}


	public boolean isSystemManaged() {
		return this.systemManaged;
	}


	public void setSystemManaged(boolean systemManaged) {
		this.systemManaged = systemManaged;
	}


	public boolean isDoNotAllowOverlapInDates() {
		return this.doNotAllowOverlapInDates;
	}


	public void setDoNotAllowOverlapInDates(boolean doNotAllowOverlapInDates) {
		this.doNotAllowOverlapInDates = doNotAllowOverlapInDates;
	}


	public boolean isInvestmentAccountRequired() {
		return this.investmentAccountRequired;
	}


	public void setInvestmentAccountRequired(boolean investmentAccountRequired) {
		this.investmentAccountRequired = investmentAccountRequired;
	}


	public boolean isInvestmentSecurityRequired() {
		return this.investmentSecurityRequired;
	}


	public void setInvestmentSecurityRequired(boolean investmentSecurityRequired) {
		this.investmentSecurityRequired = investmentSecurityRequired;
	}


	public boolean isRestrictionNoteRequired() {
		return this.restrictionNoteRequired;
	}


	public void setRestrictionNoteRequired(boolean restrictionNoteRequired) {
		this.restrictionNoteRequired = restrictionNoteRequired;
	}


	public boolean isRestrictionValueRequired() {
		return this.restrictionValueRequired;
	}


	public void setRestrictionValueRequired(boolean restrictionValueRequired) {
		this.restrictionValueRequired = restrictionValueRequired;
	}


	public SystemCondition getEvaluateCondition() {
		return this.evaluateCondition;
	}


	public void setEvaluateCondition(SystemCondition evaluateCondition) {
		this.evaluateCondition = evaluateCondition;
	}


	public SystemCondition getViolationIgnoreCondition() {
		return this.violationIgnoreCondition;
	}


	public void setViolationIgnoreCondition(SystemCondition violationIgnoreCondition) {
		this.violationIgnoreCondition = violationIgnoreCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public String getDefaultRestrictionNote() {
		return this.defaultRestrictionNote;
	}


	public void setDefaultRestrictionNote(String defaultRestrictionNote) {
		this.defaultRestrictionNote = defaultRestrictionNote;
	}
}
