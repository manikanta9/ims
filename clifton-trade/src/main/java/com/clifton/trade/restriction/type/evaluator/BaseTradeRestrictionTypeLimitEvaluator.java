package com.clifton.trade.restriction.type.evaluator;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.restriction.TradeRestriction;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public abstract class BaseTradeRestrictionTypeLimitEvaluator extends BaseTradeRestrictionTypeEvaluator {

	@Override
	public boolean isValidTrade(Trade trade, TradeRestriction restriction) {
		AssertUtils.assertNotNull(restriction.getRestrictionValue(), "The limit value must not be null.");
		if (!CompareUtils.isEqual(TradeType.OPTIONS, trade.getTradeType().getName()) || !isSellToOpen(trade)) {
			return true;
		}
		return isTradeWithinRestrictionLimit(trade, restriction.getRestrictionValue());
	}


	protected abstract boolean isTradeWithinRestrictionLimit(Trade trade, BigDecimal restrictionLimit);
}
