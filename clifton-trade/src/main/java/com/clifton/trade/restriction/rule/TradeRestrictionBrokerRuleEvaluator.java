package com.clifton.trade.restriction.rule;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import com.clifton.trade.restriction.TradeRestrictionBroker;
import com.clifton.trade.restriction.TradeRestrictionBrokerService;
import com.clifton.trade.restriction.search.TradeRestrictionBrokerSearchForm;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The TradeRestrictionBrokerRuleEvaluator class checks if a Trade contains a broker (BusinessCompany) that is configured to be excluded for the
 * client account associated with the trade.  If the trade is configured with an excluded broker, it generates a {@link RuleViolation}.
 *
 * @author davidi
 */
public class TradeRestrictionBrokerRuleEvaluator extends BaseRuleEvaluator<IdentityObject, TradeRuleEvaluatorContext> {

	private TradeRestrictionBrokerService tradeRestrictionBrokerService;


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject entity, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			if (entity != null && entity.getClass() == Trade.class) {
				Integer linkedFkFieldId = (Integer) entity.getIdentity();
				Trade trade = (Trade) entity;
				BusinessCompany broker = trade.getExecutingBrokerCompany();
				if (broker != null) {
					TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
					searchForm.setClientAccountId(trade.getClientInvestmentAccount().getId());
					List<TradeRestrictionBroker> tradeRestrictionBrokerList = getTradeRestrictionBrokerService().getTradeRestrictionBrokerList(searchForm);

					Map<Boolean, Set<BusinessCompany>> restrictionSetByInclusionMap = CollectionUtils.getStream(tradeRestrictionBrokerList)
							.collect(Collectors.groupingBy(
									TradeRestrictionBroker::isInclude,
									Collectors.mapping(TradeRestrictionBroker::getBrokerCompany, Collectors.toCollection(LinkedHashSet::new))
							));

					Set<BusinessCompany> includeSet = restrictionSetByInclusionMap.get(true);
					Set<BusinessCompany> excludeSet = restrictionSetByInclusionMap.get(false);

					if (includeSet != null && !includeSet.isEmpty() && !includeSet.contains(broker) ||
							excludeSet != null && !excludeSet.isEmpty() && excludeSet.contains(broker)) {
						Map<String, Object> templateValues = new HashMap<>();
						templateValues.put("brokerName", broker.getName());
						templateValues.put("accountLabel", trade.getClientInvestmentAccount().getLabel());
						ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, linkedFkFieldId, templateValues));
					}
				}
			}
		}

		return ruleViolationList;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionBrokerService getTradeRestrictionBrokerService() {
		return this.tradeRestrictionBrokerService;
	}


	public void setTradeRestrictionBrokerService(TradeRestrictionBrokerService tradeRestrictionBrokerService) {
		this.tradeRestrictionBrokerService = tradeRestrictionBrokerService;
	}
}
