package com.clifton.trade.restriction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.account.InvestmentAccount;


/**
 * A DTO class used to map one or more brokers (business companies) to a given investment account.
 * The purpose is to allow users to restrict an account to a set of brokers.
 *
 * @author davidi
 */
public class TradeRestrictionBroker extends BaseEntity<Integer> {

	private InvestmentAccount clientAccount;

	private BusinessCompany brokerCompany;

	/**
	 * A flag that, when enabled (true), that the restriction is an inclusion. When inclusion restrictions exist for an account,
	 * then only the inclusion brokers will be available for trading. When disabled (false), this indicates that the restriction
	 * is an exclusion and should be removed from the list of available brokers for the account.
	 */
	private boolean include;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public BusinessCompany getBrokerCompany() {
		return this.brokerCompany;
	}


	public void setBrokerCompany(BusinessCompany brokerCompany) {
		this.brokerCompany = brokerCompany;
	}


	public boolean isInclude() {
		return this.include;
	}


	public void setInclude(boolean include) {
		this.include = include;
	}
}
