package com.clifton.trade.restriction;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.trade.restriction.search.TradeRestrictionBrokerSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>TradeRestrictionBrokerService</code> is a service used to manage the TradeBrokerRestriction
 * table allowing the addition of new entries, deletion of existing entries, and query support.
 *
 * @author davidi
 */
@Service
public class TradeRestrictionBrokerServiceImpl implements TradeRestrictionBrokerService {

	AdvancedUpdatableDAO<TradeRestrictionBroker, Criteria> tradeRestrictionBrokerDAO;


	////////////////////////////////////////////////////////////////////////////
	//////               Trade Restriction Broker Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRestrictionBroker getTradeRestrictionBroker(int id) {
		return getTradeRestrictionBrokerDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeRestrictionBroker> getTradeRestrictionBrokerList(TradeRestrictionBrokerSearchForm searchForm) {
		return getTradeRestrictionBrokerDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeRestrictionBroker saveTradeRestrictionBroker(TradeRestrictionBroker bean) {
		return getTradeRestrictionBrokerDAO().save(bean);
	}


	@Override
	public void deleteTradeRestrictionBroker(TradeRestrictionBroker bean) {
		getTradeRestrictionBrokerDAO().delete(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeRestrictionBroker, Criteria> getTradeRestrictionBrokerDAO() {
		return this.tradeRestrictionBrokerDAO;
	}


	public void setTradeRestrictionBrokerDAO(AdvancedUpdatableDAO<TradeRestrictionBroker, Criteria> tradeRestrictionBrokerDAO) {
		this.tradeRestrictionBrokerDAO = tradeRestrictionBrokerDAO;
	}
}
