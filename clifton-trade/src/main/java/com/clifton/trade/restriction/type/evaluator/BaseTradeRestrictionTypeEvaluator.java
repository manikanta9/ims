package com.clifton.trade.restriction.type.evaluator;

import com.clifton.core.util.compare.CompareUtils;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;


/**
 * @author StevenF
 */
public abstract class BaseTradeRestrictionTypeEvaluator implements TradeRestrictionTypeEvaluator {

	private MarketDataRetriever marketDataRetriever;
	private SystemColumnValueService systemColumnValueService;
	private TradeService tradeService;


	/**
	 * Returns {@code true} if the given trade is a "Sell-to-Open" trade.
	 */
	protected boolean isSellToOpen(Trade trade) {
		TradeOpenCloseType sellToOpenType = getTradeService().getTradeOpenCloseTypeByName(TradeOpenCloseTypes.SELL_OPEN.getName());
		return CompareUtils.isEqual(trade.getOpenCloseType(), sellToOpenType);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
