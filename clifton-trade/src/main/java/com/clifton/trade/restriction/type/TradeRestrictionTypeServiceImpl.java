package com.clifton.trade.restriction.type;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.type.search.TradeRestrictionTypeCategorySearchForm;
import com.clifton.trade.restriction.type.search.TradeRestrictionTypeSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The implementation of {@link TradeRestrictionTypeService} for {@link TradeRestriction} DAO actions.
 *
 * @author StevenF
 */
@Service
public class TradeRestrictionTypeServiceImpl implements TradeRestrictionTypeService {

	private AdvancedUpdatableDAO<TradeRestrictionType, Criteria> tradeRestrictionTypeDAO;
	private AdvancedUpdatableDAO<TradeRestrictionTypeCategory, Criteria> tradeRestrictionTypeCategoryDAO;

	////////////////////////////////////////////////////////////////////////////
	////////          Trade Restriction Type Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRestrictionType getTradeRestrictionType(short id) {
		return getTradeRestrictionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeRestrictionType getTradeRestrictionTypeByName(String restrictionTypeName) {
		return getTradeRestrictionTypeDAO().findOneByField("name", restrictionTypeName);
	}


	@Override
	public List<TradeRestrictionType> getTradeRestrictionTypeList(TradeRestrictionTypeSearchForm searchForm) {
		return getTradeRestrictionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeRestrictionType saveTradeRestrictionType(TradeRestrictionType restrictionType) {
		return getTradeRestrictionTypeDAO().save(restrictionType);
	}


	@Override
	public void deleteTradeRestrictionType(short id) {
		getTradeRestrictionTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////        Trade Restriction Category Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeRestrictionTypeCategory getTradeRestrictionTypeCategory(short id) {
		return getTradeRestrictionTypeCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<TradeRestrictionTypeCategory> getTradeRestrictionTypeCategoryList(TradeRestrictionTypeCategorySearchForm searchForm) {
		return getTradeRestrictionTypeCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public TradeRestrictionTypeCategory saveTradeRestrictionTypeCategory(TradeRestrictionTypeCategory restrictionCategory) {
		return getTradeRestrictionTypeCategoryDAO().save(restrictionCategory);
	}


	@Override
	public void deleteTradeRestrictionTypeCategory(short id) {
		getTradeRestrictionTypeCategoryDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeRestrictionType, Criteria> getTradeRestrictionTypeDAO() {
		return this.tradeRestrictionTypeDAO;
	}


	public void setTradeRestrictionTypeDAO(AdvancedUpdatableDAO<TradeRestrictionType, Criteria> tradeRestrictionTypeDAO) {
		this.tradeRestrictionTypeDAO = tradeRestrictionTypeDAO;
	}


	public AdvancedUpdatableDAO<TradeRestrictionTypeCategory, Criteria> getTradeRestrictionTypeCategoryDAO() {
		return this.tradeRestrictionTypeCategoryDAO;
	}


	public void setTradeRestrictionTypeCategoryDAO(AdvancedUpdatableDAO<TradeRestrictionTypeCategory, Criteria> tradeRestrictionTypeCategoryDAO) {
		this.tradeRestrictionTypeCategoryDAO = tradeRestrictionTypeCategoryDAO;
	}
}
