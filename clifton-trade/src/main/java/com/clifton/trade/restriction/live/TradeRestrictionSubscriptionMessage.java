package com.clifton.trade.restriction.live;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.trade.restriction.TradeRestriction;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.monitorjbl.json.JsonView;
import com.monitorjbl.json.Match;

import java.util.Collections;
import java.util.List;


/**
 * The message DTO for sending subscription information regarding active {@link TradeRestriction} changes.
 *
 * @author MikeH
 * @see TradeRestrictionSubscriptionService
 */
public class TradeRestrictionSubscriptionMessage {

	/**
	 * If {@code true}, this message indicates that the client-side data should be reset before processing the remainder of this message.
	 */
	private final boolean reset;
	/**
	 * The list of added restrictions.
	 */
	private final List<TradeRestriction> added;
	/**
	 * The list of removed restrictions.
	 */
	private final List<TradeRestriction> removed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeRestrictionSubscriptionMessage(boolean reset, List<TradeRestriction> added, List<TradeRestriction> removed) {
		this.reset = reset;
		this.added = ObjectUtils.coalesce(added, Collections.emptyList());
		this.removed = ObjectUtils.coalesce(removed, Collections.emptyList());
	}


	public static TradeRestrictionSubscriptionMessage reset(List<TradeRestriction> added) {
		return new TradeRestrictionSubscriptionMessage(true, added, null);
	}


	public static TradeRestrictionSubscriptionMessage add(TradeRestriction added) {
		return new TradeRestrictionSubscriptionMessage(false, Collections.singletonList(added), null);
	}


	public static TradeRestrictionSubscriptionMessage remove(TradeRestriction removed) {
		return new TradeRestrictionSubscriptionMessage(false, null, Collections.singletonList(removed));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@JsonIgnore
	public List<TradeRestriction> getAdded() {
		return this.added;
	}


	@JsonGetter("added")
	public JsonView<List<TradeRestriction>> getAddedView() {
		return JsonView.with(this.added).onClass(TradeRestriction.class, Match.match()
				.exclude("*")
				.include(
						"id",
						"clientInvestmentAccount.id",
						"investmentSecurity.id",
						"restrictionType.name",
						"restrictionValue",
						"startDate",
						"endDate",
						"label",
						"note"
				));
	}


	@JsonIgnore
	public List<TradeRestriction> getRemoved() {
		return this.removed;
	}


	@JsonGetter("removed")
	public List<Integer> getRemovedView() {
		return CollectionUtils.getConverted(this.removed, BaseSimpleEntity::getId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isReset() {
		return this.reset;
	}
}
