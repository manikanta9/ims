package com.clifton.trade.restriction.type.evaluator;

import com.clifton.trade.Trade;
import com.clifton.trade.restriction.TradeRestriction;


/**
 * @author StevenF
 */
public interface TradeRestrictionTypeEvaluator {

	public boolean isValidTrade(Trade trade, TradeRestriction restriction);
}
