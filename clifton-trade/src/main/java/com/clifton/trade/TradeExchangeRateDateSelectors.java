package com.clifton.trade;

import java.util.Date;


/**
 * Some securities may require Exchange Rate from a very specific date for foreign transactions (Trade Date for Cleared CDS).
 *
 * @author vgomelsky
 */
public enum TradeExchangeRateDateSelectors {

	TRADE_DATE {
		@Override
		public Date getExchangeRateDate(Trade trade) {
			return trade.getTradeDate();
		}
	};


	/**
	 * Returns the Date that must be used for Exchange Rate lookup for foreign transactions for the specified trade.
	 */
	public abstract Date getExchangeRateDate(Trade trade);
}
