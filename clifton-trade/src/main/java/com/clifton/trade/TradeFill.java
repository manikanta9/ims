package com.clifton.trade;


import com.clifton.accounting.AccountingBean;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The <code>TradeFill</code> class represents a single fill for a trade.  Most trades are filled with a single fill.
 * If average pricing is used, then there'll be only one fill for the trade too.
 * <p/>
 * However, a trade may have more than one fill if it gets filled by different lots at different prices.
 * <p/>
 * NOTE: Fills (not trades) get booked to the General Ledger. It maybe necessary to book multiple fills from multiple trades
 * for the same account and security on the same day in a custom order in order to match the broker( lowest price first, etc.).
 *
 * @author vgomelsky
 */
public class TradeFill extends BaseEntity<Integer> implements BookableEntity, AccountingBean {

	public static final String TABLE_NAME = "TradeFill";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private Trade trade;

	private BigDecimal quantity;

	// for securities that have payment on open, these fields will hold total Payment amount as well as Payment Unit Price (0 or null otherwise)
	// for Currency trades, paymentTotalPrice holds base currency amount
	private BigDecimal paymentUnitPrice;
	private BigDecimal paymentTotalPrice;

	// trade fill price and notional
	private BigDecimal notionalUnitPrice;
	private BigDecimal notionalTotalPrice;

	/**
	 * Used to close a specific lot opened on a this date.
	 */
	private Date openingDate;

	/**
	 * Used to find the lots that were opened at this price on the opening date.
	 */
	private BigDecimal openingPrice;

	/**
	 * NOTE: Fills (not trades) get booked to the General Ledger. It maybe necessary to book multiple fills from multiple trades
	 * for the same account and security on the same day in a custom order in order to match the broker (lowest price first, etc.).
	 */
	private Date bookingDate;


	/**
	 * Indicates whether the transaction associated with this fill opens a new position or closes an existing position.
	 * The sell/buy indicator must match the trade, however, the open/close indicator is based positions at booking time.
	 */
	private TradeOpenCloseType openCloseType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(64);
		if (this.trade != null) {
			if (this.trade.getClientInvestmentAccount() != null) {
				result.append(this.trade.getClientInvestmentAccount().getNumber());
				result.append("-");
			}
			if (this.trade.getHoldingInvestmentAccount() != null) {
				result.append(this.trade.getHoldingInvestmentAccount().getNumber());
				result.append(": ");
			}
			if (this.trade.isBuy()) {
				result.append("BUY ");
			}
			else {
				result.append("SELL ");
			}
			result.append(CoreMathUtils.formatNumberDecimal(getQuantity()));
			if (this.trade.getInvestmentSecurity() != null) {
				result.append(" of ");
				result.append(this.trade.getInvestmentSecurity().getSymbol());
			}
			if (this.trade.getTradeDate() != null) {
				result.append(" on ");
				result.append(DateUtils.fromDateShort(this.trade.getTradeDate()));
			}
		}
		return result.toString();
	}


	@Override
	public AccountingClosingMethods getAccountingClosingMethod() {
		if (getTrade() != null) {
			InvestmentAccount clientAccount = getTrade().getClientInvestmentAccount();
			if (clientAccount != null) {
				return clientAccount.getAccountingClosingMethod();
			}
		}
		return AccountingClosingMethods.FIFO;
	}


	public boolean isLotSpecificClosing() {
		return getOpeningDate() != null && getOpeningPrice() != null;
	}


	@Override
	public Integer getSourceEntityId() {
		return getId();
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.trade.getInvestmentSecurity();
	}


	@Override
	public InvestmentSecurity getSettlementCurrency() {
		return this.trade.getSettlementCurrency();
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.trade.getClientInvestmentAccount();
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.trade.getHoldingInvestmentAccount();
	}


	@Override
	public BigDecimal getQuantityNormalized() {
		return getQuantity();
	}


	@Override
	public BigDecimal getAccountingNotional() {
		return getNotionalTotalPrice();
	}


	@Override
	public BigDecimal getExchangeRateToBase() {
		return this.trade.getExchangeRateToBase();
	}


	@Override
	public boolean isBuy() {
		return this.trade.isBuy();
	}


	@Override
	public boolean isCollateral() {
		return getTrade().isCollateralTrade();
	}


	@Override
	public Date getTransactionDate() {
		return this.trade.getTradeDate();
	}


	@Override
	public Date getSettlementDate() {
		return this.trade.getSettlementDate();
	}


	@Override
	public Set<IdentityObject> getEntityLockSet() {
		return CollectionUtils.createHashSet(getClientInvestmentAccount());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	@Override
	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPaymentUnitPrice() {
		return this.paymentUnitPrice;
	}


	public void setPaymentUnitPrice(BigDecimal paymentUnitPrice) {
		this.paymentUnitPrice = paymentUnitPrice;
	}


	public BigDecimal getPaymentTotalPrice() {
		return this.paymentTotalPrice;
	}


	public void setPaymentTotalPrice(BigDecimal paymentTotalPrice) {
		this.paymentTotalPrice = paymentTotalPrice;
	}


	public BigDecimal getNotionalUnitPrice() {
		return this.notionalUnitPrice;
	}


	public void setNotionalUnitPrice(BigDecimal notionalUnitPrice) {
		this.notionalUnitPrice = notionalUnitPrice;
	}


	public BigDecimal getNotionalTotalPrice() {
		return this.notionalTotalPrice;
	}


	public void setNotionalTotalPrice(BigDecimal notionalTotalPrice) {
		this.notionalTotalPrice = notionalTotalPrice;
	}


	@Override
	public Date getBookingDate() {
		return this.bookingDate;
	}


	@Override
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}


	public Date getOpeningDate() {
		return this.openingDate;
	}


	public void setOpeningDate(Date openingDate) {
		this.openingDate = openingDate;
	}


	public BigDecimal getOpeningPrice() {
		return this.openingPrice;
	}


	public void setOpeningPrice(BigDecimal openingPrice) {
		this.openingPrice = openingPrice;
	}


	public TradeOpenCloseType getOpenCloseType() {
		return this.openCloseType;
	}


	public void setOpenCloseType(TradeOpenCloseType openCloseType) {
		this.openCloseType = openCloseType;
	}
}
