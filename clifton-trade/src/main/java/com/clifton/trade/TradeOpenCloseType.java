package com.clifton.trade;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * Represents the Buy/Sell and Open/Close for a trade.  There are only 6 entries in the table 'Buy', 'Buy To Open', 'Buy To Close', 'Sell', 'Sell To Open' and 'Sell To Close'.
 *
 * @author mwacker
 */
@CacheByName
public class TradeOpenCloseType extends NamedEntity<Short> {

	/**
	 * Indicates if the trade is a buy or sell.  True is buy, False is sell.
	 */
	private boolean buy;

	/**
	 * Indicates if the trade is an opening trade.
	 */
	private boolean open;

	/**
	 * Indicates if the trade is a closing trade.
	 */
	private boolean close;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isExplicitOpenClose() {
		return isOpen() != isClose();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public boolean isOpen() {
		return this.open;
	}


	public void setOpen(boolean open) {
		this.open = open;
	}


	public boolean isClose() {
		return this.close;
	}


	public void setClose(boolean close) {
		this.close = close;
	}
}
