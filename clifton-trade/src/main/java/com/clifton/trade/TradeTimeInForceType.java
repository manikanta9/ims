package com.clifton.trade;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * Represents the type of Time In Force (TIF) types used in FIX Messages
 *
 * @author davidi
 */
@CacheByName
public class TradeTimeInForceType extends NamedEntity<Short> {

	public static final String DAY = "DAY";
	public static final String GOOD_TILL_CANCELED = "GTC";
	public static final String GOOD_TILL_DATE = "GTD";


	/**
	 * If true, this type also requires an expire date which will be set in FIX Tag 432. The expire date is the last
	 * date the broker may execute a trade to fill the order.
	 */
	private boolean expirationDateRequired;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isExpirationDateRequired() {
		return this.expirationDateRequired;
	}


	public void setExpirationDateRequired(boolean expirationDateRequired) {
		this.expirationDateRequired = expirationDateRequired;
	}
}
