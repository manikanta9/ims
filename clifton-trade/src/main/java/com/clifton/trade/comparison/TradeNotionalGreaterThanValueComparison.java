package com.clifton.trade.comparison;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.comparison.field.number.BeanFieldGreaterThanValueComparison;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>TradeNotionalGreaterThanValueComparison</code> converts trade notional into specified currency and checks to see
 * if it is greater than a specified value
 *
 * @author apopp
 */
public class TradeNotionalGreaterThanValueComparison extends BeanFieldGreaterThanValueComparison {

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	/**
	 * The to currency investment security
	 */
	private Integer securityToCurrencyId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("ConstantConditions")
	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		AssertUtils.assertNotNull(getSecurityToCurrencyId(), "Must specify to currency security ID");
		AssertUtils.assertTrue(bean instanceof Trade, "Bean must be of type trade for trade notional comparison.");

		InvestmentSecurity toCurrency = getInvestmentInstrumentService().getInvestmentSecurity(getSecurityToCurrencyId());
		AssertUtils.assertNotNull(toCurrency, "Must specify to currency security");
		AssertUtils.assertTrue(toCurrency.isCurrency(), "To currency security must be a valid currency security");
		Trade trade = (Trade) bean;

		String actual = getActualValue(bean);
		BigDecimal actualValue = new BigDecimal(actual);
		boolean result = evaluateCondition(getTradeNotionalForToCurrency(trade, toCurrency.getSymbol()), actualValue);

		if (context != null) {
			if (result) {
				context.recordTrueMessage("Trade Notional [" + CoreMathUtils.formatNumberMoney(trade.getAccountingNotional()) + " " + toCurrency.getSymbol() + "] is greater than [" + CoreMathUtils.formatNumberMoney(actualValue) + "]");
			}
			else {
				context.recordFalseMessage("Trade Notional [" + CoreMathUtils.formatNumberMoney(trade.getAccountingNotional()) + " " + toCurrency.getSymbol() + "] is less than or equal [" + actualValue + "]");
			}
		}

		return result;
	}


	private BigDecimal getTradeNotionalForToCurrency(Trade trade, String toCurrency) {
		BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forFxSource(trade.fxSourceCompany(), !trade.fxSourceCompanyOverridden(), InvestmentUtils.getSecurityCurrencyDenominationSymbol(trade), toCurrency, trade.getTradeDate()).flexibleLookup());
		return MathUtils.multiply(trade.getAccountingNotional(), fxRate);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public Integer getSecurityToCurrencyId() {
		return this.securityToCurrencyId;
	}


	public void setSecurityToCurrencyId(Integer securityToCurrencyId) {
		this.securityToCurrencyId = securityToCurrencyId;
	}
}
