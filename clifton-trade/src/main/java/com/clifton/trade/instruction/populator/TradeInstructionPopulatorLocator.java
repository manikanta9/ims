package com.clifton.trade.instruction.populator;

import com.clifton.trade.TradeFill;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class TradeInstructionPopulatorLocator implements InitializingBean, ApplicationContextAware {

	private Map<String, TradeInstructionPopulator> handlerMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeInstructionPopulator locate(TradeFill tradeFill) {
		return getHandlerMap().get(TradeInstructionPopulator.buildSupportedTypeKey(tradeFill));
	}


	@Override
	public void afterPropertiesSet() {
		Map<String, TradeInstructionPopulator> beanMap = getApplicationContext().getBeansOfType(TradeInstructionPopulator.class);

		for (Map.Entry<String, TradeInstructionPopulator> stringTradeInstructionPopulatorEntry : beanMap.entrySet()) {
			TradeInstructionPopulator populator = stringTradeInstructionPopulatorEntry.getValue();
			List<String> keyList = populator.getSupportedTypeKeyList();
			for (String key : keyList) {
				if (getHandlerMap().containsKey(key)) {
					throw new RuntimeException("Cannot register '" + stringTradeInstructionPopulatorEntry.getKey() + "' as a populator for key '" + key
							+ "' because this key already has a registered populator.");
				}
				getHandlerMap().put(key, populator);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, TradeInstructionPopulator> getHandlerMap() {
		return this.handlerMap;
	}


	public void setHandlerMap(Map<String, TradeInstructionPopulator> handlerMap) {
		this.handlerMap = handlerMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
