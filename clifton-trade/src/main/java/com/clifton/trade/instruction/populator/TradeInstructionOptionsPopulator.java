package com.clifton.trade.instruction.populator;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.StringUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@Component
public class TradeInstructionOptionsPopulator<I extends Instruction> extends TradeInstructionFuturesEquitiesPopulator<I> {

	@Override
	public List<String> getSupportedTypeKeyList() {
		return Collections.singletonList(InvestmentType.OPTIONS);
	}


	@Override
	public void populate(AbstractTradeMessage tradeMessage, Instruction instruction, TradeFill tradeFill) {
		super.populate(tradeMessage, instruction, tradeFill);

		tradeMessage.setPlaceOfSettlement("XX");

		Trade trade = tradeFill.getTrade();

		tradeMessage.setDealAmount(tradeFill.getAccountingNotional());

		tradeMessage.setClearingBrokerIdentifier(
				getPartyIdentifier(Optional.of(trade)
								.flatMap(t -> Optional.ofNullable(t.getHoldingInvestmentAccount()))
								.flatMap(ia -> Optional.ofNullable(ia.getIssuingCompany()))
								.map(BusinessCompany::getBusinessIdentifierCode)
								.orElse(null)
						, MessagePartyIdentifierTypes.BIC
				)
		);
		tradeMessage.setExecutingBrokerIdentifier(
				getPartyIdentifier(Optional.of(trade)
								.flatMap(t -> Optional.ofNullable(t.getExecutingBrokerCompany()))
								.map(BusinessCompany::getBusinessIdentifierCode)
								.orElse(null)
						, MessagePartyIdentifierTypes.BIC
				)
		);

		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentExchange investmentExchange = InvestmentUtils.getSecurityExchange(investmentSecurity);
		if (investmentExchange != null) {
			tradeMessage.setSecurityExchangeCode(trimToNull(investmentExchange.getMarketIdentifierCode()));
		}
		tradeMessage.setSecurityExpirationDate(investmentSecurity.getEndDate());
		tradeMessage.setPriceMultiplier(investmentSecurity.getPriceMultiplier());

		// Mellon:  use OCC symbol with 'TS' qualifier.
		if (!StringUtils.isEmpty(investmentSecurity.getOccSymbol())) {
			tradeMessage.setSecuritySymbol(trimToNull(investmentSecurity.getOccSymbol()));
		}
		else {
			tradeMessage.setSecuritySymbol(trimToNull(investmentSecurity.getSymbol()));
		}
		tradeMessage.setSecurityDescription(trimToNull(StringUtils.removeAll(investmentSecurity.getDescription(), "\\$")));

		tradeMessage.setOptionStyle(investmentSecurity.getOptionStyle());
		tradeMessage.setOptionType(investmentSecurity.getOptionType().toString());
		tradeMessage.setStrikePrice(investmentSecurity.getOptionStrikePrice());

		InvestmentAccount custodianAccount = getTradeInstructionUtilHandler().getCustodianAccount(trade);
		if (custodianAccount != null) {
			tradeMessage.setCustodyAccountNumber(trimToNull(custodianAccount.getNumber()));
		}

		tradeMessage.setQuantity(MathUtils.abs(tradeFill.getQuantity()));
		tradeMessage.setOpen(trade.getOpenCloseType().isOpen());
		tradeMessage.setCommissionAmount(getTradeInstructionUtilHandler().getCommissionAndFeeAmount(tradeFill));
		tradeMessage.setNetSettlementAmount(getTradeInstructionUtilHandler().getNetNotionalAmount(tradeFill, tradeMessage.getCommissionAmount()));

		// Securities that belong to this hierarchy do not require a payment when opening a position
		if (investmentSecurity.getInstrument().getHierarchy().isNoPaymentOnOpen()) {
			tradeMessage.setDealAmount(BigDecimal.ZERO);
			tradeMessage.setCommissionAmount(getTradeInstructionUtilHandler().getBrokerCommissionAmount(tradeFill));
			tradeMessage.setNetSettlementAmount(tradeMessage.getCommissionAmount());
		}
	}
}
