package com.clifton.trade.instruction.populator;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Component
public class TradeInstructionFixedIncomePopulator<I extends Instruction> extends TradeInstructionBasePopulator<I> implements TradeInstructionPopulator {


	public static final String FEDERAL_RESERVE_BANK = "FRNYUS33";

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getSupportedTypeKeyList() {
		return Collections.singletonList(InvestmentType.BONDS);
	}


	@Override
	public void populate(AbstractTradeMessage tradeMessage, Instruction instruction, TradeFill tradeFill) {
		super.populate(tradeMessage, instruction, tradeFill);

		ValidationUtils.assertNotNull(tradeMessage.getTransactionReferenceNumber(), "Transaction Reference Number is required.");
		ValidationUtils.assertNotNull(tradeMessage.getMessageFunctions(), "Message Function is required.");
		ValidationUtils.assertNotNull(tradeMessage.getSettlementDate(), "Settlement Date is required.");

		tradeMessage.setPriceType(TradeMessagePriceTypes.PERCENTAGE);
		tradeMessage.setPrice(tradeFill.getNotionalUnitPrice());
		tradeMessage.setDealAmount(tradeFill.getAccountingNotional());

		Trade trade = tradeFill.getTrade();
		AssertUtils.assertTrue(trade.getTradeType().isSingleFillTrade(), TradeInstructionFixedIncomePopulator.class.getSimpleName() + " only supports single fill trades.");

		tradeMessage.setOriginalFaceAmount(trade.getOriginalFace());

		String placeOfSettlement = (Objects.nonNull(trade.getSettlementCompany()) && !StringUtils.isEmpty(trade.getSettlementCompany().getBusinessIdentifierCode()))
				? trade.getSettlementCompany().getBusinessIdentifierCode() : FEDERAL_RESERVE_BANK;
		tradeMessage.setPlaceOfSettlement(placeOfSettlement);

		tradeMessage.setAccruedInterest(trade.getAccrualAmount());

		InvestmentAccount custodianAccount = getTradeInstructionUtilHandler().getCustodianAccount(trade);
		tradeMessage.setCustodyAccountNumber(custodianAccount.getNumber());
		ValidationUtils.assertNotNull(tradeMessage.getCustodyAccountNumber(), "Custody Account Number is required.");

		InvestmentInstructionDeliveryType deliveryType = getInvestmentInstructionDeliveryUtilHandler().getExecutingBrokerDeliveryType();
		AssertUtils.assertNotNull(deliveryType, "Could not get the [Executing Broker] delivery type.");

		Optional<InvestmentInstructionDelivery> deliveryInstruction = Optional.of(trade)
				.map(c -> getInvestmentInstructionDeliveryUtilHandler().getInvestmentInstructionDelivery(deliveryType,
						trade.getExecutingBrokerCompany(), trade.getSettlementCurrency()));

		deliveryInstruction
				.map(InvestmentInstructionDelivery::getDeliveryABA)
				.map(String::trim)
				.filter(s -> !s.isEmpty())
				.ifPresent(v -> tradeMessage.setClearingBrokerIdentifier(getPartyIdentifier(v, MessagePartyIdentifierTypes.USFW)));

		Optional.of(trade.getExecutingBrokerCompany()).map(BusinessCompany::getFedMnemonic).ifPresent(tradeMessage::setClearingSafekeepingAccount);

		tradeMessage.setExecutingBrokerIdentifier(
				getPartyIdentifier(Optional.of(trade)
								.map(Trade::getExecutingBrokerCompany)
								.map(BusinessCompany::getDtcNumber)
								.map(String::trim)
								.filter(s -> !s.isEmpty())
								.orElse(null)
						, MessagePartyIdentifierTypes.DTC
				)
		);

		tradeMessage.setNetSettlementAmount(MathUtils.add(trade.getAccountingNotional(), trade.getAccrualAmount()));
		tradeMessage.setIssueDate(trade.getInvestmentSecurity().getStartDate());

		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		tradeMessage.setInterestRate(getInvestmentSecurityUtilHandler().getInterestRate(investmentSecurity));
		tradeMessage.setMaturityDate(investmentSecurity.getEndDate());
		tradeMessage.setSecurityCurrency(trimToNull(investmentSecurity.getInstrument().getTradingCurrency().getSymbol()));
		tradeMessage.setIsin(trimToNull(investmentSecurity.getIsin()));
		ValidationUtils.assertNotNull(tradeMessage.getIsin(), "ISIN is required.");

		InvestmentInstrumentHierarchy hierarchy = trade.getInvestmentSecurity().getInstrument().getHierarchy();
		if (hierarchy.isIndexRatioAdjusted()) {
			tradeMessage.setCurrentFactor(trade.getNotionalMultiplier());
			tradeMessage.setAmortizedFaceAmount(getTradeInstructionUtilHandler().getAmortizedFaceAmount(trade));
		}
		else if (Objects.nonNull(hierarchy.getFactorChangeEventType())) {
			tradeMessage.setCurrentFactor(trade.getCurrentFactor());
			tradeMessage.setAmortizedFaceAmount(trade.getQuantityIntended());
		}
	}
}
