package com.clifton.trade.instruction;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.trade.Trade;


/**
 * The <code>TradeCounterpartyInstructionSelector</code> defines the bean type
 * used to select corresponding {@link InvestmentInstructionDefinition} for each {@link Trade} record
 * that defines the instructions for the "counterparty" i.e. Holding Account
 * that is sent to the custodian "Recipient"
 *
 * @author manderson
 */
public class TradeCounterpartyInstructionSelector extends BaseTradeInstructionSelector {

	@Override
	public BusinessCompany getRecipient(Trade trade) {
		// Prevent Null Pointer (Shouldn't happen because filters now require Holding Account Populated)
		ValidationUtils.assertNotNull(trade.getHoldingInvestmentAccount(), "Trade #: " + trade.getId() + " is missing a holding account selection. Unable to determine instruction recipient.");
		InvestmentAccount custodianAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(trade.getClientInvestmentAccount().getId(),
				trade.getHoldingInvestmentAccount().getId(), trade.getInvestmentSecurity().getId(), InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME, trade.getTradeDate());
		// Strict Lookup will thrown an exception of none or more than one is found
		return custodianAccount.getIssuingCompany();
	}
}
