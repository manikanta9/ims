package com.clifton.trade.instruction.populator;

import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;


public class TradeInstructionFuturesEquitiesPopulator<I extends Instruction> extends TradeInstructionBasePopulator<I> {

	@Override
	public void populate(AbstractTradeMessage tradeMessage, Instruction instruction, TradeFill tradeFill) {
		super.populate(tradeMessage, instruction, tradeFill);

		tradeMessage.setPriceType(TradeMessagePriceTypes.ACTUAL);
		tradeMessage.setPrice(tradeFill.getNotionalUnitPrice());

		Trade trade = tradeFill.getTrade();

		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		tradeMessage.setSecurityCurrency(trimToNull(investmentSecurity.getInstrument().getTradingCurrency().getSymbol()));
		tradeMessage.setSecurityExpirationDate(investmentSecurity.getEndDate());
		tradeMessage.setPriceMultiplier(investmentSecurity.getPriceMultiplier());

		InvestmentAccount custodianAccount = getTradeInstructionUtilHandler().getCustodianAccount(trade);
		tradeMessage.setCustodyAccountNumber(trimToNull(custodianAccount.getNumber()));
	}
}
