package com.clifton.trade.instruction.populator;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.transfers.SettlementTransactionTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.instruction.TradeInstructionUtilHandler;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


public class TradeInstructionBasePopulator<I extends Instruction> implements TradeInstructionPopulator {

	private TradeInstructionUtilHandler tradeInstructionUtilHandler;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	private InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler;
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void populate(AbstractTradeMessage tradeMessage, Instruction instruction, TradeFill tradeFill) {
		tradeMessage.setMessageFunctions(MessageFunctions.NEW_MESSAGE);
		tradeMessage.setSenderBIC(trimToNull(getTradeInstructionUtilHandler().getParametricBusinessIdentifierCode()));

		Trade trade = tradeFill.getTrade();

		tradeMessage.setTransactionReferenceNumber(getInstructionUtilHandler().getTransactionReferenceNumber(Arrays.asList(instruction, trade, tradeFill)));
		InvestmentAccount custodianAccount = getTradeInstructionUtilHandler().getCustodianAccount(trade);
		ValidationUtils.assertNotNull(custodianAccount, "Custodian Account cannot be null.");
		ValidationUtils.assertNotNull(custodianAccount.getIssuingCompany(), String.format("Custodian Account [%s] Issuing Company cannot be null", custodianAccount.getLabel()));
		String custodianBIC = trimToNull(custodianAccount.getIssuingCompany().getBusinessIdentifierCode());
		ValidationUtils.assertNotEmpty(custodianBIC, () -> String.format("The account [%s] with issuing company [%s] does not have a Business Identifier Code (BIC)",
				custodianAccount.getLabel(), custodianAccount.getIssuingCompany().getNameWithType()));
		tradeMessage.setReceiverBIC(custodianBIC);
		tradeMessage.setCustodyBIC(custodianBIC);
		tradeMessage.setTradeDate(trade.getTradeDate());
		tradeMessage.setSettlementDate(trade.getSettlementDate());

		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentInstrument investmentInstrument = investmentSecurity.getInstrument();
		ISITCCodes isitcCode = getInvestmentSecurityUtilHandler().getISITCCodeStrict(investmentInstrument);
		tradeMessage.setIsitcCode(isitcCode == ISITCCodes.NONE ? null : isitcCode);
		tradeMessage.setSettlementTransactionType(SettlementTransactionTypes.TRADE);
	}


	@Override
	public List<String> getSupportedTypeKeyList() {
		return Collections.emptyList();
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	protected String trimToNull(String value) {
		String trimmed = StringUtils.trim(value);
		return StringUtils.isEmpty(trimmed) ? null : trimmed;
	}


	protected PartyIdentifier getPartyIdentifier(String identifier, MessagePartyIdentifierTypes type) {
		String trimmed = trimToNull(identifier);
		return Objects.isNull(trimmed) ? null : new PartyIdentifier(trimmed, type);
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public TradeInstructionUtilHandler getTradeInstructionUtilHandler() {
		return this.tradeInstructionUtilHandler;
	}


	public void setTradeInstructionUtilHandler(TradeInstructionUtilHandler tradeInstructionUtilHandler) {
		this.tradeInstructionUtilHandler = tradeInstructionUtilHandler;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public InstructionUtilHandler<I, InstructionDeliveryFieldCommand> getInstructionUtilHandler() {
		return this.instructionUtilHandler;
	}


	public void setInstructionUtilHandler(InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler) {
		this.instructionUtilHandler = instructionUtilHandler;
	}


	public InvestmentInstructionDeliveryUtilHandler getInvestmentInstructionDeliveryUtilHandler() {
		return this.investmentInstructionDeliveryUtilHandler;
	}


	public void setInvestmentInstructionDeliveryUtilHandler(InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler) {
		this.investmentInstructionDeliveryUtilHandler = investmentInstructionDeliveryUtilHandler;
	}
}
