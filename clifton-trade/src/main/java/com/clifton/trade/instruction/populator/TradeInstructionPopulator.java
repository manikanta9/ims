package com.clifton.trade.instruction.populator;

import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.trade.TradeFill;

import java.util.List;


/**
 * <code>TradeInstructionPopulator</code> Populates the {@link AbstractTradeMessage} with data only applicable to
 * the supported security types determined by {@link #getSupportedTypeKeyList()}
 */
public interface TradeInstructionPopulator {

	/**
	 * Used by the locator to build the popular identifier key based on the provided Trade Fill.
	 */
	public static String buildSupportedTypeKey(TradeFill tradeFill) {
		InvestmentInstrumentHierarchy hierarchy = tradeFill.getInvestmentSecurity().getInstrument().getHierarchy();
		return hierarchy.getInvestmentType().getName();
	}


	/**
	 * @see TradeInstructionPopulator#buildSupportedTypeKey(TradeFill), must build the key in the same manner.
	 */
	public List<String> getSupportedTypeKeyList();


	/**
	 * Populates the provided {@link AbstractTradeMessage} with only data specific to the security type and destination
	 * SWIFT message format.
	 */
	public void populate(AbstractTradeMessage tradeMessage, Instruction instruction, TradeFill tradeFill);
}
