package com.clifton.trade.instruction.populator;

import com.clifton.core.util.StringUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


@Component
public class TradeInstructionEquitiesPopulator<I extends Instruction> extends TradeInstructionFuturesEquitiesPopulator<I> {

	@Override
	public List<String> getSupportedTypeKeyList() {
		return Arrays.asList(
				InvestmentType.FUNDS,
				InvestmentType.NOTES,
				InvestmentType.STOCKS
		);
	}


	@Override
	public void populate(AbstractTradeMessage tradeMessage, Instruction instruction, TradeFill tradeFill) {
		super.populate(tradeMessage, instruction, tradeFill);

		Trade trade = tradeFill.getTrade();

		tradeMessage.setClearingBrokerIdentifier(getPartyIdentifier(trade.getExecutingBrokerCompany().getDtcNumber(), MessagePartyIdentifierTypes.DTC));
		tradeMessage.setExecutingBrokerIdentifier(getPartyIdentifier(trade.getExecutingBrokerCompany().getDtcNumber(), MessagePartyIdentifierTypes.DTC));

		tradeMessage.setPlaceOfSettlement(trimToNull("DTCYUS33"));
		tradeMessage.setDealAmount(trade.getAccountingNotional());
		tradeMessage.setNetSettlementAmount(getTradeInstructionUtilHandler().getNetNotionalAmount(tradeFill));

		tradeMessage.setQuantity(MathUtils.abs(tradeFill.getQuantity()));
		tradeMessage.setRegulatoryAmount(getTradeInstructionUtilHandler().getRegulatoryAmount(tradeFill));
		tradeMessage.setCommissionAmount(getTradeInstructionUtilHandler().getBrokerCommissionAmount(tradeFill));

		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		tradeMessage.setIsin(trimToNull(investmentSecurity.getIsin()));

		if (StringUtils.isEmpty(investmentSecurity.getIsin())) {
			tradeMessage.setSecuritySymbol(trimToNull(investmentSecurity.getSymbol()));
			tradeMessage.setSecurityDescription(trimToNull(StringUtils.removeAll(investmentSecurity.getDescription(), "\\$")));
		}
	}
}
