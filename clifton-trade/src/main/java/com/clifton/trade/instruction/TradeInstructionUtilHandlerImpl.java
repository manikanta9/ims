package com.clifton.trade.instruction;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.account.search.AccountingAccountSearchForm;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


/**
 * <code>TradeInstructionUtilHandlerImpl</code> Utilities used when generating fixed income instruction messages.
 */
@Component
public class TradeInstructionUtilHandlerImpl implements TradeInstructionUtilHandler {

	private AccountingTransactionService accountingTransactionService;
	private AccountingAccountService accountingAccountService;
	private BusinessCompanyUtilHandler businessCompanyUtilHandler;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentCalculatorService investmentCalculatorService;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public String getParametricBusinessIdentifierCode() {
		return getBusinessCompanyUtilHandler().getParametricBusinessIdentifierCode();
	}


	@Override
	public List<AccountingTransaction> getAccountingTransactionPositionList(TradeFill fill) {
		if (fill != null) {
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setFkFieldId(fill.getId());
			searchForm.setTableName(TradeFill.TABLE_NAME);
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
			return getAccountingTransactionService().getAccountingTransactionList(searchForm);
		}
		return Collections.emptyList();
	}


	@Override
	public InvestmentAccount getCustodianAccount(Trade trade) {
		if (trade != null) {
			// Prevent Null Pointer (Shouldn't happen because filters now require Holding Account Populated)
			ValidationUtils.assertNotNull(trade.getHoldingInvestmentAccount(), "Trade #: " + trade.getId() + " is missing a holding account selection. Unable to determine instruction recipient.");
			return getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(trade.getClientInvestmentAccount().getId(),
					trade.getHoldingInvestmentAccount().getId(), trade.getInvestmentSecurity().getId(), InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME, trade.getTradeDate());
		}
		return null;
	}


	@Override
	public BigDecimal getCommissionAndFeeAmount(TradeFill tradeFill) {
		List<AccountingTransaction> transactionList = getAccountingTransactionPositionList(tradeFill);
		return transactionList.stream()
				.map(this::getCommissionAndFeeAmount)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}


	@Override
	public BigDecimal getCommissionAndFeeAmount(AccountingTransaction transaction) {
		List<AccountingTransaction> transactionList = Collections.emptyList();
		if (transaction != null) {
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setParentId(transaction.getId());
			// commission OR Fee if the commission checkbox is selected.
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.COMMISSION_ACCOUNTS);
			transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		}
		return CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit);
	}


	@Override
	public BigDecimal getGainLoss(TradeFill tradeFill) {
		List<AccountingTransaction> transactionList = getAccountingTransactionPositionList(tradeFill);
		return transactionList.stream()
				.filter(t -> !t.isOpening())
				.map(this::getGainLoss)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}


	@Override
	public BigDecimal getGainLoss(AccountingTransaction transaction) {
		List<AccountingTransaction> transactionList = Collections.emptyList();
		if (transaction != null) {
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setParentId(transaction.getId());
			searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.GAIN_LOSS_ACCOUNTS);
			transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		}
		return CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit);
	}


	@Override
	public BigDecimal getPositionCostBasis(TradeFill tradeFill) {
		List<AccountingTransaction> transactionList = getAccountingTransactionPositionList(tradeFill);
		return transactionList.stream()
				.map(AccountingTransaction::getPositionCostBasis)
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}


	@Override
	public BigDecimal getRegulatoryAmount(TradeFill tradeFill) {
		if (tradeFill != null) {
			// look for only the 'SEC Fees' account only.
			Short[] accountIds = getCommissionAndFeeAccounts().stream()
					.filter(AccountingAccount::isCommission)
					.filter(AccountingAccount::isFee)
					.filter(a -> Objects.equals(AccountingAccount.EXPENSE_SEC_FEES, a.getName()))
					.map(AccountingAccount::getId)
					.toArray(Short[]::new);
			AssertUtils.assertTrue(accountIds.length == 1, () -> String.format("Should be one 'SEC Fees' Account but but found %s", Arrays.toString(accountIds)));
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setFkFieldId(tradeFill.getId());
			searchForm.setTableName(TradeFill.TABLE_NAME);
			searchForm.setAccountingAccountIds(accountIds);
			List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);

			return CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit);
		}
		return BigDecimal.ZERO;
	}


	@Override
	public BigDecimal getBrokerCommissionAmount(TradeFill tradeFill) {
		if (tradeFill != null) {
			// commission and not fee should be the commission account only.
			Short[] accountIds = getCommissionAndFeeAccounts().stream()
					.filter(AccountingAccount::isCommission)
					.filter(a -> !a.isFee())
					.map(AccountingAccount::getId)
					.toArray(Short[]::new);
			AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
			searchForm.setFkFieldId(tradeFill.getId());
			searchForm.setTableName(TradeFill.TABLE_NAME);
			searchForm.setAccountingAccountIds(accountIds);
			List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);

			return CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit);
		}
		return BigDecimal.ZERO;
	}


	@Override
	public BigDecimal getAmortizedFaceAmount(Trade trade) {
		if (trade != null) {
			return getInvestmentCalculatorService().getInvestmentSecurityNotional(
					trade.getInvestmentSecurity().getId(),
					new BigDecimal("100"),
					trade.getOriginalFace(),
					trade.getNotionalMultiplier());
		}
		return BigDecimal.ZERO;
	}


	@Override
	public BigDecimal getNetNotionalAmount(TradeFill tradeFill) {
		if (tradeFill != null) {
			BigDecimal results = tradeFill.getAccountingNotional();
			Trade trade = tradeFill.getTrade();
			if (!InvestmentUtils.isNoPaymentOnOpen(trade.getInvestmentSecurity())) {
				BigDecimal fees = getCommissionAndFeeAmount(tradeFill);
				if (trade.isBuy()) {
					results = MathUtils.add(results, fees);
				}
				else {
					results = MathUtils.subtract(results, fees);
				}
			}
			return results;
		}
		return BigDecimal.ZERO;
	}


	@Override
	public BigDecimal getNetNotionalAmount(TradeFill tradeFill, BigDecimal commissionAndFees) {
		if (tradeFill != null) {
			BigDecimal results = tradeFill.getAccountingNotional();
			Trade trade = tradeFill.getTrade();
			if (!InvestmentUtils.isNoPaymentOnOpen(trade.getInvestmentSecurity())) {
				if (trade.isBuy()) {
					results = MathUtils.add(results, commissionAndFees);
				}
				else {
					results = MathUtils.subtract(results, commissionAndFees);
				}
			}
			return results;
		}
		return BigDecimal.ZERO;
	}


	@Override
	public List<AccountingAccount> getCommissionAndFeeAccounts() {
		AccountingAccountSearchForm accountingAccountSearchForm = new AccountingAccountSearchForm();
		// commission OR Fee if the commission checkbox is selected.
		accountingAccountSearchForm.setCommission(true);
		return getAccountingAccountService().getAccountingAccountList(accountingAccountSearchForm);
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public BusinessCompanyUtilHandler getBusinessCompanyUtilHandler() {
		return this.businessCompanyUtilHandler;
	}


	public void setBusinessCompanyUtilHandler(BusinessCompanyUtilHandler businessCompanyUtilHandler) {
		this.businessCompanyUtilHandler = businessCompanyUtilHandler;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}
}
