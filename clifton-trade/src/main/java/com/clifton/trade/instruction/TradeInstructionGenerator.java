package com.clifton.trade.instruction;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionCancellationHandler;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.generator.InstructionGenerator;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.instruction.populator.TradeInstructionPopulator;
import com.clifton.trade.instruction.populator.TradeInstructionPopulatorLocator;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * @author mwacker
 */
@Component
public class TradeInstructionGenerator implements InstructionGenerator<InstructionMessage, Trade, InstructionDeliveryFieldCommand> {

	private TradeService tradeService;
	private TradeInstructionPopulatorLocator tradeInstructionPopulatorLocator;
	private InstructionCancellationHandler instructionCancellationHandler;
	private DaoLocator daoLocator;

	private TradeInstructionUtilHandler tradeInstructionUtilHandler;
	private InstructionUtilHandler<? extends Instruction, InstructionDeliveryFieldCommand> instructionUtilHandler;

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public Class<Trade> getSupportedMessageClass() {
		return Trade.class;
	}


	@Override
	public boolean isCancellationSupported() {
		return true;
	}


	@Override
	public List<InstructionMessage> generateInstructionCancelMessageList(Instruction instruction) {
		return getInstructionCancellationHandler().generateCancellationMessageList(instruction);
	}


	@Override
	public InstructionDeliveryFieldCommand getDeliveryInstructionProvider(Instruction instruction, Trade trade) {
		return TradeInstructionUtils.buildDeliveryFieldCommand(instruction, trade);
	}


	@Override
	public List<InstructionMessage> generateInstructionMessageList(Instruction instruction) {
		ReadOnlyDAO<Trade> dao = getDaoLocator().locate(instruction.getSystemTable().getName());
		Trade trade = dao.findByPrimaryKey(instruction.getFkFieldId());
		final List<TradeFill> tradeFillList = getTradeService().getTradeFillListByTrade(trade.getId());
		return CollectionUtils.getStream(tradeFillList)
				.flatMap(tf -> CollectionUtils.getStream(doGenerateInstructionMessageList(instruction, tf)))
				.collect(Collectors.toList());
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	private List<InstructionMessage> doGenerateInstructionMessageList(Instruction instruction, TradeFill tradeFill) {
		List<InstructionMessage> result = new ArrayList<>();
		TradeInstructionPopulator populator = getTradeInstructionPopulatorLocator().locate(tradeFill);
		ValidationUtils.assertTrue(Objects.nonNull(populator), () -> String.format("No SWIFT conversion found for %s",
				buildHierarchyString(tradeFill.getInvestmentSecurity().getInstrument().getHierarchy())));
		if (populator != null) {
			if (!generatedFromAccountingTransactions(instruction, tradeFill, populator, result)) {
				Trade trade = tradeFill.getTrade();
				AbstractTradeMessage message = getTradeMessage(trade);
				populator.populate(message, instruction, tradeFill);
				result.add(message);
			}
		}
		return result;
	}


	// @Deprecated - this should be unnecessary in subsequent releases
	private boolean generatedFromAccountingTransactions(Instruction instruction, TradeFill tradeFill, TradeInstructionPopulator populator, List<InstructionMessage> result) {
		// OpenCloseType is not set or open close is not determinant and uses a futures populator - require open / close.
		if (!tradeFill.getOpenCloseType().isExplicitOpenClose() && populator.getSupportedTypeKeyList().stream().anyMatch(InvestmentType.FUTURES::equals)) {
			List<AccountingTransaction> transactionList = getTradeInstructionUtilHandler().getAccountingTransactionPositionList(tradeFill);
			final Map<Boolean, Long> openCloseCounts = transactionList.stream()
					.collect(Collectors.groupingBy(AccountingTransaction::isOpening, Collectors.counting()));
			if (openCloseCounts.size() == 2 && openCloseCounts.get(Boolean.TRUE) > 0 && openCloseCounts.get(Boolean.FALSE) > 0) {
				Trade trade = tradeFill.getTrade();
				for (AccountingTransaction accountingTransaction : transactionList) {
					AbstractTradeMessage message = getTradeMessage(trade);
					populator.populate(message, instruction, tradeFill);
					// Accounting Transaction overrides
					message.setTransactionReferenceNumber(getInstructionUtilHandler().getTransactionReferenceNumber(Arrays.asList(instruction, trade, tradeFill, accountingTransaction)));
					message.setQuantity(MathUtils.abs(accountingTransaction.getQuantity()));
					message.setOpen(accountingTransaction.isOpening());
					message.setCommissionAmount(getTradeInstructionUtilHandler().getCommissionAndFeeAmount(accountingTransaction));
					message.setNetSettlementAmount(getTradeInstructionUtilHandler().getCommissionAndFeeAmount(accountingTransaction));
					BigDecimal gainLossAdjustment = BigDecimal.ZERO;
					if (!accountingTransaction.isOpening()) {
						gainLossAdjustment = getTradeInstructionUtilHandler().getGainLoss(accountingTransaction);
					}
					message.setAccountingNotional(MathUtils.add(MathUtils.abs(accountingTransaction.getPositionCostBasis()), MathUtils.abs(gainLossAdjustment)));
					// done with overrides, add to list
					result.add(message);
				}
				return true;
			}
		}
		return false;
	}


	private AbstractTradeMessage getTradeMessage(Trade trade) {
		if (trade.isBuy()) {
			return new DeliverAgainstPayment();
		}
		else {
			return new ReceiveAgainstPayment();
		}
	}


	private String buildHierarchyString(InvestmentInstrumentHierarchy hierarchy) {
		return String.join(", ",
				Optional.ofNullable(hierarchy).map(InvestmentInstrumentHierarchy::getInvestmentType).map(InvestmentType::toString).orElse(""),
				Optional.ofNullable(hierarchy).map(InvestmentInstrumentHierarchy::getInvestmentTypeSubType).map(InvestmentTypeSubType::toString).orElse(""),
				Optional.ofNullable(hierarchy).map(InvestmentInstrumentHierarchy::getInvestmentTypeSubType2).map(InvestmentTypeSubType2::toString).orElse("")
		);
	}

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeInstructionPopulatorLocator getTradeInstructionPopulatorLocator() {
		return this.tradeInstructionPopulatorLocator;
	}


	public void setTradeInstructionPopulatorLocator(TradeInstructionPopulatorLocator tradeInstructionPopulatorLocator) {
		this.tradeInstructionPopulatorLocator = tradeInstructionPopulatorLocator;
	}


	public InstructionCancellationHandler getInstructionCancellationHandler() {
		return this.instructionCancellationHandler;
	}


	public void setInstructionCancellationHandler(InstructionCancellationHandler instructionCancellationHandler) {
		this.instructionCancellationHandler = instructionCancellationHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public TradeInstructionUtilHandler getTradeInstructionUtilHandler() {
		return this.tradeInstructionUtilHandler;
	}


	public void setTradeInstructionUtilHandler(TradeInstructionUtilHandler tradeInstructionUtilHandler) {
		this.tradeInstructionUtilHandler = tradeInstructionUtilHandler;
	}


	public InstructionUtilHandler<? extends Instruction, InstructionDeliveryFieldCommand> getInstructionUtilHandler() {
		return this.instructionUtilHandler;
	}


	public void setInstructionUtilHandler(InstructionUtilHandler<? extends Instruction, InstructionDeliveryFieldCommand> instructionUtilHandler) {
		this.instructionUtilHandler = instructionUtilHandler;
	}
}
