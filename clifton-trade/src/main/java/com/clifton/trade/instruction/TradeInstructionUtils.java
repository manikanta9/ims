package com.clifton.trade.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.instruction.Instruction;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;

import java.util.Optional;


/**
 * <code>TradeInstructionUtils</code> convenience methods used when generating trade-sources instruction methods.
 */
public class TradeInstructionUtils {

	private TradeInstructionUtils() {
		// utility class only.
	}


	public static InstructionDeliveryFieldCommand buildDeliveryFieldCommand(final Instruction instruction, final Trade trade) {
		return new InstructionDeliveryFieldCommand() {
			@Override
			public Instruction getInstruction() {
				return instruction;
			}


			@Override
			public BusinessCompany getDeliveryCompany() {
				return Optional.of(trade).map(Trade::getHoldingInvestmentAccount).map(InvestmentAccount::getIssuingCompany).orElse(null);
			}


			@Override
			public InvestmentAccount getDeliveryAccount() {
				return trade.getHoldingInvestmentAccount();
			}


			@Override
			public InvestmentSecurity getDeliveryCurrency() {
				return trade.getSettlementCurrency();
			}


			@Override
			public InvestmentAccount getDeliveryClientAccount() {
				return trade.getClientInvestmentAccount();
			}
		};
	}
}
