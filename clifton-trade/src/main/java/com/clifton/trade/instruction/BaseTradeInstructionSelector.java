package com.clifton.trade.instruction;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.selector.BaseInvestmentInstructionSelector;
import com.clifton.investment.instruction.selector.InvestmentInstructionSelection;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>BaseTradeInstructionDefinitionSelector</code> ...
 *
 * @author manderson
 */
public abstract class BaseTradeInstructionSelector extends BaseInvestmentInstructionSelector<Trade> {

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private TradeService tradeService;

	///////////////////////////////////////////////////

	private Integer holdingAccountId;

	private Integer holdingAccountIssuerId;

	private Short holdingAccountTypeId;

	private List<Integer> includeHoldingAccountGroupIds;

	private List<Integer> excludeHoldingAccountGroupIds;

	private Integer clientAccountId;

	private Integer clientId;

	private Integer investmentInstrumentId;

	private Short investmentTypeId;

	private Short tradeTypeId;

	private Short investmentGroupId;

	private Integer executingBrokerId;

	private Integer payingSecurityId;

	private boolean collateralTrade = false;


	////////////////////////////////////////////////////


	@Override
	public String getInstructionItemLabel(Trade entity) {
		return entity.getWorkflowState() != null ? entity.getWorkflowState().getName() + " " + entity.getLabel() : entity.getLabel();
	}


	@Override
	public boolean isInstructionItemBooked(InvestmentInstructionCategory category, Trade entity) {
		return entity.getWorkflowState() != null && TradeService.TRADE_BOOKED_STATE_NAME.equals(entity.getWorkflowState().getName());
	}


	@Override
	public void populateInstructionSelectionDetails(InvestmentInstructionSelection selection, Trade entity) {
		selection.setClientAccount(entity.getClientInvestmentAccount());
		selection.setHoldingAccount(entity.getHoldingInvestmentAccount());
		selection.setSecurity(entity.getInvestmentSecurity());
		selection.setBooked(isInstructionItemBooked(selection.getCategory(), entity));

		StringBuilder sb = new StringBuilder(64);
		if (entity.isBuy()) {
			sb.append("BUY ");
		}
		else {
			sb.append("SELL ");
		}
		sb.append(CoreMathUtils.formatNumberDecimal(entity.getQuantityIntended()));
		selection.setDescription(sb.toString());
	}


	////////////////////////////////////////////////////


	@Override
	public List<Trade> getEntityList(InvestmentInstructionCategory category, InvestmentInstructionDefinition definition, Date date) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setTradeDate(date);
		searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
		if (definition != null) {
			// Some Trades (CCY Forwards as an example) can have blank Holding Account until after execution
			// Need this field to be populated in order to generate instructions (in order to determine the Custodian/Recipient), so these trades are excluded from instructions until execution is completed.
			// Only check if definition is populated, otherwise we are checking for missing instructions and we want to see the trade in that list
			searchForm.setHoldingInvestmentAccountPopulated(true);
		}
		searchForm.setClientInvestmentAccountId(getClientAccountId());
		searchForm.setAssignmentOrExecutingBrokerCompanyId(getExecutingBrokerId());
		searchForm.setBusinessClientId(getClientId());
		searchForm.setTradeTypeId(getTradeTypeId());
		searchForm.setPayingSecurityId(getPayingSecurityId());
		// If definition is null, then we are looking for all for the category, so no filter here
		if (definition != null) {
			searchForm.setCollateralTrade(isCollateralTrade());
		}
		searchForm.setHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getIncludeHoldingAccountGroupIds(), Integer.class));
		searchForm.setExcludeHoldingInvestmentAccountGroupIds(CollectionUtils.toArrayOrNull(getExcludeHoldingAccountGroupIds(), Integer.class));

		// Specific Instrument
		if (getInvestmentInstrumentId() != null) {
			searchForm.setInvestmentInstrumentId(getInvestmentInstrumentId());
		}
		// Or Find by Investment Type and/or Investment Group
		else {
			searchForm.setInvestmentTypeId(getInvestmentTypeId());
			searchForm.setInvestmentGroupId(getInvestmentGroupId());
		}

		// Specific Holding Account
		if (getHoldingAccountId() != null) {
			searchForm.setHoldingInvestmentAccountId(getHoldingAccountId());
		}
		// Or Issuer and/or Type
		else {
			searchForm.setHoldingAccountIssuingCompanyId(getHoldingAccountIssuerId());
			searchForm.setHoldingAccountTypeId(getHoldingAccountTypeId());
		}
		return getTradeService().getTradeList(searchForm);
	}


	////////////////////////////////////////////////////
	////           Getter & Setter Methods          ////
	////////////////////////////////////////////////////


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public Short getHoldingAccountTypeId() {
		return this.holdingAccountTypeId;
	}


	public void setHoldingAccountTypeId(Short holdingAccountTypeId) {
		this.holdingAccountTypeId = holdingAccountTypeId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public Integer getExecutingBrokerId() {
		return this.executingBrokerId;
	}


	public void setExecutingBrokerId(Integer executingBrokerId) {
		this.executingBrokerId = executingBrokerId;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public Integer getPayingSecurityId() {
		return this.payingSecurityId;
	}


	public void setPayingSecurityId(Integer payingSecurityId) {
		this.payingSecurityId = payingSecurityId;
	}


	public boolean isCollateralTrade() {
		return this.collateralTrade;
	}


	public void setCollateralTrade(boolean collateralTrade) {
		this.collateralTrade = collateralTrade;
	}


	public List<Integer> getIncludeHoldingAccountGroupIds() {
		return this.includeHoldingAccountGroupIds;
	}


	public void setIncludeHoldingAccountGroupIds(List<Integer> includeHoldingAccountGroupIds) {
		this.includeHoldingAccountGroupIds = includeHoldingAccountGroupIds;
	}


	public List<Integer> getExcludeHoldingAccountGroupIds() {
		return this.excludeHoldingAccountGroupIds;
	}


	public void setExcludeHoldingAccountGroupIds(List<Integer> excludeHoldingAccountGroupIds) {
		this.excludeHoldingAccountGroupIds = excludeHoldingAccountGroupIds;
	}
}
