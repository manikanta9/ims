package com.clifton.trade.instruction;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;

import java.math.BigDecimal;
import java.util.List;


/**
 * <code>TradeInstructionUtilHandler</code> Provide utility methods for the trade instruction populators
 */
public interface TradeInstructionUtilHandler {

	/**
	 * Get Parametric BIC (business Identifier Code).
	 */
	public String getParametricBusinessIdentifierCode();


	/**
	 * Get the Accounting Transactions Position List from the provided Trade Fill.
	 */
	public List<AccountingTransaction> getAccountingTransactionPositionList(TradeFill fill);


	/**
	 * Get the Custodian Investment Account from the provide trade.
	 */
	public InvestmentAccount getCustodianAccount(Trade trade);


	/**
	 * Get the commission and fee amount for the provided the trade fill.
	 */
	public BigDecimal getCommissionAndFeeAmount(TradeFill tradeFill);


	/**
	 * Get the commission and fee amount for the provided parent accounting transaction.
	 */
	public BigDecimal getCommissionAndFeeAmount(AccountingTransaction transaction);


	/**
	 * Get the gain/loss amount for the provided Trade Fill.
	 */
	public BigDecimal getGainLoss(TradeFill tradeFill);


	/**
	 * Get the gain/loss amount for the provided parent accounting transaction.
	 */
	public BigDecimal getGainLoss(AccountingTransaction transaction);


	/**
	 * The Position Cost Basis for all positions transactions for the provided trade fill.
	 */
	public BigDecimal getPositionCostBasis(TradeFill tradeFill);


	/**
	 * Get 'SEC Fees'.
	 */
	public BigDecimal getRegulatoryAmount(TradeFill tradeFill);


	/**
	 * Get the commission amounts only, does not include fees.
	 */
	public BigDecimal getBrokerCommissionAmount(TradeFill tradeFill);


	/**
	 * Inflation adjusted face value amount.
	 */
	public BigDecimal getAmortizedFaceAmount(Trade trade);


	/**
	 * Accounting Notional - feeAmount - commissionAmount
	 */
	public BigDecimal getNetNotionalAmount(TradeFill tradeFill);


	/**
	 * Accounting Notional - feeAmount - commissionAmount; use when the commission and fees has
	 * already been calculated.
	 */
	public BigDecimal getNetNotionalAmount(TradeFill tradeFill, BigDecimal commissionAndFees);


	/**
	 * Get the commission accounts including fees.
	 */
	public List<AccountingAccount> getCommissionAndFeeAccounts();
}
