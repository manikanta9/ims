package com.clifton.trade.instruction.populator;

import com.clifton.core.util.StringUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;


@Component
public class TradeInstructionFuturesPopulator<I extends Instruction> extends TradeInstructionFuturesEquitiesPopulator<I> {

	@Override
	public List<String> getSupportedTypeKeyList() {
		return Collections.singletonList(InvestmentType.FUTURES);
	}


	@Override
	public void populate(AbstractTradeMessage tradeMessage, Instruction instruction, TradeFill tradeFill) {
		super.populate(tradeMessage, instruction, tradeFill);

		tradeMessage.setPlaceOfSettlement("XX");
		tradeMessage.setDealAmount(BigDecimal.ZERO);

		Trade trade = tradeFill.getTrade();

		tradeMessage.setClearingBrokerIdentifier(getPartyIdentifier(trade.getHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode(), MessagePartyIdentifierTypes.BIC));
		tradeMessage.setExecutingBrokerIdentifier(getPartyIdentifier(trade.getExecutingBrokerCompany().getBusinessIdentifierCode(), MessagePartyIdentifierTypes.BIC));

		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentExchange investmentExchange = InvestmentUtils.getSecurityExchange(investmentSecurity);
		if (investmentExchange != null) {
			tradeMessage.setSecurityExchangeCode(trimToNull(investmentExchange.getMarketIdentifierCode()));
		}
		tradeMessage.setSecurityExpirationDate(investmentSecurity.getEndDate());
		tradeMessage.setPriceMultiplier(investmentSecurity.getPriceMultiplier());
		tradeMessage.setSecuritySymbol(trimToNull(investmentSecurity.getSymbol()));
		tradeMessage.setSecurityDescription(trimToNull(StringUtils.removeAll(investmentSecurity.getDescription(), "\\$")));


		InvestmentAccount custodianAccount = getTradeInstructionUtilHandler().getCustodianAccount(trade);
		if (custodianAccount != null) {
			tradeMessage.setCustodyAccountNumber(trimToNull(custodianAccount.getNumber()));
		}

		tradeMessage.setQuantity(MathUtils.abs(tradeFill.getQuantity()));
		if (tradeFill.getOpenCloseType() != null && tradeFill.getOpenCloseType().isExplicitOpenClose()) {
			tradeMessage.setOpen(tradeFill.getOpenCloseType().isOpen());
		}
		tradeMessage.setCommissionAmount(getTradeInstructionUtilHandler().getCommissionAndFeeAmount(tradeFill));
		tradeMessage.setNetSettlementAmount(getTradeInstructionUtilHandler().getCommissionAndFeeAmount(tradeFill));
		BigDecimal positionCostBasis = MathUtils.abs(getTradeInstructionUtilHandler().getPositionCostBasis(tradeFill));
		BigDecimal gainLoss = MathUtils.abs(getTradeInstructionUtilHandler().getGainLoss(tradeFill));
		tradeMessage.setAccountingNotional(MathUtils.add(positionCostBasis, gainLoss));
	}
}
