package com.clifton.trade;

/**
 * An enumeration of TradeListActionTypes for use with the {@link com.clifton.trade.TradeListActionCommand}.
 * <p>
 * FILL - fills trade.
 * FILL_EXECUTE - fills trade and completes execution of trade transitioning the trade state: (awaiting fills -> executed valid or executed invalid).
 * REASSIGN - reassigns one or more trades to another trader.
 *
 * @author davidi
 */
public enum TradeListActionTypes {
	FILL,
	FILL_EXECUTE,
	REASSIGN
}
