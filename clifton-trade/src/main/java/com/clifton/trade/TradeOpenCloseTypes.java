package com.clifton.trade;

import java.util.Arrays;
import java.util.Optional;


/**
 * Enum representation of TradeOpenCloseType.  This enum is used to resolve the name of the TradeOpenCloseType
 * when saving a trade.
 *
 * @author mwacker
 */
public enum TradeOpenCloseTypes {

	BUY("Buy", true, true, true),
	BUY_OPEN("Buy to Open", true, true, false),
	BUY_CLOSE("Buy to Close", true, false, true),

	SELL("Sell", false, true, true),
	SELL_OPEN("Sell to Open", false, true, false),
	SELL_CLOSE("Sell to Close", false, false, true);

	private final String name;

	private final boolean buy;
	private final boolean open;
	private final boolean close;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	TradeOpenCloseTypes(String name, boolean buy, boolean open, boolean close) {
		this.name = name;
		this.buy = buy;
		this.open = open;
		this.close = close;
	}


	public static TradeOpenCloseTypes ofBuy(boolean buy) {
		return doOf(buy, true, true);
	}


	public static TradeOpenCloseTypes of(boolean buy, boolean open, boolean close) {
		if (!open && open == close) {
			open = true;
			close = true;
		}
		return doOf(buy, open, close);
	}


	private static TradeOpenCloseTypes doOf(boolean buy, boolean open, boolean close) {
		Optional<?> result = Arrays.stream(TradeOpenCloseTypes.values()).filter(oct -> oct.match(buy, open, close)).findFirst();
		return (TradeOpenCloseTypes) result.orElse(null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean match(boolean buy, boolean open, boolean close) {
		return isBuy() == buy && isOpen() == open && isClose() == close;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public boolean isBuy() {
		return this.buy;
	}


	public boolean isOpen() {
		return this.open;
	}


	public boolean isClose() {
		return this.close;
	}
}
