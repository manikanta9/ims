package com.clifton.trade;


import com.clifton.accounting.AccountingBean;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.accounting.commission.TradeCommissionOverride;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.group.TradeGroup;
import com.clifton.workflow.BaseWorkflowAwareEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>Trade</code> class represents an order to buy or sell a predefined quantity of investment security.
 *
 * @author vgomelsky
 */
public class Trade extends BaseWorkflowAwareEntity<Integer> implements LabeledObject, AccountingBean {

	public static final String TABLE_NAME = "Trade";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeType tradeType;
	/**
	 * Optional source this trade was created from.
	 * For example, the DeltaShift strategy has some trade windows that have client-side rails and ample compliance/trade rules to be able to bypass dual approval.
	 */
	private TradeSource tradeSource;

	// Optional group this Trade was created with (Rolls, Roll Tails, PIOS, Multi-client)
	private TradeGroup tradeGroup;
	/**
	 * The order (usually an external order like FIX) created for the trade.
	 * Reference the ID value (TradeOrderID) - not the actual order object
	 */
	private Integer orderIdentifier;

	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	/**
	 * The business company used to determine the place of settlement business identifier code (BIC) when generating settlement instructions.
	 * This is populated on newly created trades by an observer and is based on investment specificity service calls.  This field will show
	 * as read-only on the trade window and is editable on the settlement tab.  For USD and most security instruction types, this is either
	 * 'Deposit Trust & Clearing Corporation' or 'Federal Reserve Bank of New York'.
	 */
	private BusinessCompany settlementCompany;
	private InvestmentSecurity investmentSecurity;
	/**
	 * Settlement Currency for the trade. Usually it's the same as currency denomination of security.
	 * In rare instances a different currency can be used (client's base currency to settle foreign swaps)
	 */
	private InvestmentSecurity payingSecurity;

	private BusinessCompany executingBrokerCompany;
	private TradeDestination tradeDestination; // specifies the destination of the trade i.e. Goldman Sachs via FIX or Internal Clifton processing
	private SecurityUser traderUser;
	/**
	 * The company that sponsors our access to the SEF for trading.  Used for Cleared IRS/CDS.
	 */
	private BusinessCompany executingSponsorCompany;

	/**
	 * An optional value that identifies how the trade should be executed (e.g. limit order, or MOC).
	 */
	private TradeExecutionType tradeExecutionType;

	private boolean buy;
	private TradeOpenCloseType openCloseType;
	private boolean collateralTrade; // specifies whether position from this trade is used for collateral

	/**
	 * A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market.
	 * Block Trades will be charged a different commission rate than non-block trades.
	 * <p>
	 * This flag may need to be updated to a different type if more execution related types/styles are identified.
	 */
	private boolean blockTrade;

	private BigDecimal quantityIntended;
	/**
	 * Usually the same as quantityIntended but for bonds that had factor changes prior to this trade (current factor <> 1),
	 * stores Original Face value while quantityIntended will store Purchase Face.
	 */
	private BigDecimal originalFace;
	/**
	 * Trading notional can be different because it's adjusted based on client's benchmark duration
	 */
	private BigDecimal accountingNotional;
	/**
	 * Some securities may adjust accounting notional by this multiplier that is tied to something.
	 * TIPS use it to adjust for changes in inflation: Index Ratio.
	 */
	private BigDecimal notionalMultiplier;
	/**
	 * Purchase Factor that applies to bonds. Must be set to 1 for all other securities meaning no factor change.
	 */
	private BigDecimal currentFactor;

	/**
	 * The value to exchange one unit of the security's currency denomination for one unit of the client account's base currency.
	 * If settlement currency of the trade does not match the security's or client's currency, then the system will calculate the
	 * crossing rate value via the exchange rate on the trade multiplied by default data source exchange rate from settlement currency
	 * on the trade to client account's base currency.
	 * <p>
	 * For currency trades, the exchange rate will be between the traded (investmentSecurity) and settlement currency (payingSecurity) in the dominant currency rate.
	 * For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency.
	 */
	private BigDecimal exchangeRateToBase;

	/**
	 * Expected unit price for the trade: used to obtain accounting notional for validation, etc.
	 */
	private BigDecimal expectedUnitPrice;
	/**
	 * True average price based on prices of fills. Set after fills are known.
	 */
	private BigDecimal averageUnitPrice;
	/**
	 * The limit price at which the trade should be executed.
	 */
	private BigDecimal limitPrice;

	/**
	 * For securities that support accruals (bonds, swaps, etc.), specifies amount of Accrued Interest on the date of the trade.
	 */
	private BigDecimal accrualAmount1;
	/**
	 * For securities with two accrual values (Fixed and Floating Leg for IRS), the second accrual is used.
	 */
	private BigDecimal accrualAmount2;

	/**
	 * Field to indicate if trade is novation - if this field is true, then a counterparty can be specified via the assignmentCompany field.  While the trade can be created and booked without
	 * a counterparty assigned, there is a post-process rule to verify that a counterparty is assigned for novation trades by the time they get to that stage.
	 */
	private boolean novation;

	/**
	 * If trade is novation, a counterparty can be specified.  This field cannot be specified if novation is false.
	 */
	private BusinessCompany assignmentCompany;

	/**
	 * Non-persisted field that indicates that the commission/fee should be treated as an override
	 * and thus trigger a change to the associated {@link TradeCommissionOverride} (if it exists, otherwise
	 * a new one is created). The logic for this can be found in {@link TradeObserver}.
	 */
	@NonPersistentField
	private boolean enableCommissionOverride;

	private BigDecimal commissionPerUnit; // commissionAmount/quantityThatCommissionWasChargedFor
	private BigDecimal commissionAmount; // total commission amount charged for this trade
	private BigDecimal feeAmount; // total fee for the trade

	private Date tradeDate;
	private Date settlementDate;

	/**
	 * Actual settlement date, may differ from (contractual) settlement date.
	 */
	private Date actualSettlementDate;

	private String description;

	/**
	 * Used in case like loading trades from other systems such as APX or APL.
	 */
	private String externalTradeIdentifier;

	/**
	 * An optional field that specifies the TimeInForce (TIF) type to use when the order is sent to a broker via FIX. This field is
	 * optional, and the order will be of type "DAY" if not specified in this field.
	 */
	private TradeTimeInForceType tradeTimeInForceType;

	/**
	 * A Date with time (in UTC) indicating when a GTD order expires.  This should remain null unless the tradeTimeInForceType is of a type that requires the expiration date (GTD).
	 */
	private Date timeInForceExpirationDate;

	@OneToManyEntity(serviceBeanName = "tradeService", serviceMethodName = "getTradeFillListByTrade")
	private List<TradeFill> tradeFillList;

	// Used for creating fills from the Trade Group screen - not persisted in the Database
	@NonPersistentField
	private BigDecimal fillQuantity;
	@NonPersistentField
	private BigDecimal fillPrice;

	/**
	 * Transient field used during uploads to flag a trade as a roll trade. The field is unused for persisted trades and instead looks at the trade groups type's roll flag.
	 */
	@NonPersistentField
	private boolean roll;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isFixTrade() {
		return getTradeDestination() != null && getTradeDestination().getType() != null && getTradeDestination().getType().isFixTradeOnly();
	}


	public boolean isCreateOrder() {
		return getTradeDestination() != null && getTradeDestination().getType() != null && getTradeDestination().getType().isCreateOrder();
	}


	public boolean isCancelled() {
		return !isNewBean() && getWorkflowState() != null && TradeService.TRADES_CANCELLED_STATE_NAME.equals(getWorkflowState().getName());
	}


	/**
	 * Returns the natural key fields as a label for this Trade
	 */
	public String getUniqueLabel() {
		StringBuilder sb = new StringBuilder(64);
		if (getClientInvestmentAccount() != null) {
			sb.append(getClientInvestmentAccount().getNumber());
			sb.append("-");
		}
		if (getHoldingInvestmentAccount() != null) {
			sb.append(getHoldingInvestmentAccount().getNumber());
			sb.append(": ");
		}
		sb.append(getLabelShort());
		return sb.toString();
	}


	public String getLabelShort() {
		StringBuilder sb = new StringBuilder(64);
		sb.append(isBuy() ? "BUY " : "SELL ");
		sb.append(CoreMathUtils.formatNumberDecimal(getQuantityIntended()));
		if (getInvestmentSecurity() != null) {
			sb.append(" of ");
			sb.append(getInvestmentSecurity().getSymbol());
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}


	public String getLabelLong() {
		if (getClientInvestmentAccount() == null) {
			return getLabelShort();
		}
		return getLabelShort() + " for " + getClientInvestmentAccount().getLabel();
	}


	/**
	 * Returns the average unit price if it's not null, otherwise it returns the expectedUnitPrice.
	 */
	public BigDecimal getAverageOrExpectedUnitPrice() {
		return (getAverageUnitPrice() == null) ? getExpectedUnitPrice() : getAverageUnitPrice();
	}


	/**
	 * Calculates and returns local "Net Payment" amount for this trade. Calculation takes into account cost, commissions, fees, accrual.
	 * NOTE: Returns null for securities that do not have a payment on open. This calculation requires Realized Gain/Loss from GL which is not easily accessible here.
	 */
	public BigDecimal getNetPayment() {
		if (getTradeType() == null || getInvestmentSecurity() == null || InvestmentUtils.isNoPaymentOnOpen(getInvestmentSecurity())) {
			return null;
		}
		BigDecimal result = getAccountingNotional();
		if (isBuy() && !getTradeType().isAmountsSignAccountsForBuy()) {
			result = MathUtils.negate(result);
		}
		if (getTradeType().isAccrualSignAccountsForBuy() || !isBuy()) {
			result = MathUtils.add(result, getAccrualAmount());
		}
		else {
			result = MathUtils.subtract(result, getAccrualAmount());
		}
		result = MathUtils.add(result, getFeeAmount());
		result = MathUtils.add(result, getCommissionAmount());
		return result;
	}


	/**
	 * Return Net Payment converted to Base Currency of trade's Client Account
	 */
	public BigDecimal getNetPaymentBase() {
		BigDecimal result = getNetPayment();
		if (result != null && !MathUtils.isNullOrZero(getExchangeRateToBase())) {
			if (getClientInvestmentAccount() != null) {
				result = InvestmentCalculatorUtils.calculateBaseAmount(result, getExchangeRateToBase(), getClientInvestmentAccount());
			}
			else {
				result = MathUtils.multiply(result, getExchangeRateToBase(), 2);
			}
		}
		return result;
	}


	/**
	 * Returns true if the trade is a member of a Trade Group configured as a Roll Trade Group Type
	 */
	public boolean isRoll() {
		return BooleanUtils.isTrue(this.roll) || (getTradeGroup() != null && getTradeGroup().getTradeGroupType().isRoll());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getSourceEntityId() {
		return getId();
	}


	@Override
	public String getLabel() {
		return getUniqueLabel();
	}


	@Override
	public InvestmentSecurity getSettlementCurrency() {
		return getPayingSecurity();
	}


	@Override
	public BigDecimal getQuantity() {
		return getQuantityIntended();
	}


	@Override
	public BigDecimal getQuantityNormalized() {
		if (this.quantityIntended == null && getInvestmentSecurity() != null && getInvestmentSecurity().isCurrency()) {
			return getAccountingNotional();
		}
		return getQuantityIntended();
	}


	@Override
	public Date getTransactionDate() {
		return getTradeDate();
	}


	public SecurityUser getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public InvestmentSecurity getPayingSecurity() {
		return this.payingSecurity;
	}


	public void setPayingSecurity(InvestmentSecurity payingSecurity) {
		this.payingSecurity = payingSecurity;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public TradeTimeInForceType getTradeTimeInForceType() {
		return this.tradeTimeInForceType;
	}


	public void setTradeTimeInForceType(TradeTimeInForceType tradeTimeInForceType) {
		this.tradeTimeInForceType = tradeTimeInForceType;
	}


	public Date getTimeInForceExpirationDate() {
		return this.timeInForceExpirationDate;
	}


	public void setTimeInForceExpirationDate(Date timeInForceExpirationDate) {
		this.timeInForceExpirationDate = timeInForceExpirationDate;
	}


	@Override
	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getActualSettlementDate() {
		return this.actualSettlementDate;
	}


	public void setActualSettlementDate(Date actualSettlementDate) {
		this.actualSettlementDate = actualSettlementDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public void addFill(TradeFill fill) {
		if (getTradeFillList() == null) {
			setTradeFillList(new ArrayList<>());
		}
		getTradeFillList().add(fill);
	}


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public void setTradeType(TradeType tradeType) {
		this.tradeType = tradeType;
	}


	public TradeSource getTradeSource() {
		return this.tradeSource;
	}


	public void setTradeSource(TradeSource tradeSource) {
		this.tradeSource = tradeSource;
	}


	@Override
	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public BigDecimal getQuantityIntended() {
		return this.quantityIntended;
	}


	public void setQuantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
	}


	public BigDecimal getExpectedUnitPrice() {
		return this.expectedUnitPrice;
	}


	public void setExpectedUnitPrice(BigDecimal expectedUnitPrice) {
		this.expectedUnitPrice = expectedUnitPrice;
	}


	@Override
	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	@Override
	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	@Override
	public BigDecimal getExchangeRateToBase() {
		return this.exchangeRateToBase;
	}


	public void setExchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
	}


	public BigDecimal getAverageUnitPrice() {
		return this.averageUnitPrice;
	}


	public void setAverageUnitPrice(BigDecimal averageUnitPrice) {
		this.averageUnitPrice = averageUnitPrice;
	}


	public boolean isEnableCommissionOverride() {
		return this.enableCommissionOverride;
	}


	public void setEnableCommissionOverride(boolean enableCommissionOverride) {
		this.enableCommissionOverride = enableCommissionOverride;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public BigDecimal getFeeAmount() {
		return this.feeAmount;
	}


	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}


	public List<TradeFill> getTradeFillList() {
		return this.tradeFillList;
	}


	public void setTradeFillList(List<TradeFill> tradeFillList) {
		this.tradeFillList = tradeFillList;
	}


	@Override
	public BigDecimal getAccountingNotional() {
		return this.accountingNotional;
	}


	public void setAccountingNotional(BigDecimal accountingNotional) {
		this.accountingNotional = accountingNotional;
	}


	public BigDecimal getOriginalFace() {
		return this.originalFace;
	}


	public void setOriginalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
	}


	public BigDecimal getCurrentFactor() {
		return this.currentFactor;
	}


	public void setCurrentFactor(BigDecimal currentFactor) {
		this.currentFactor = currentFactor;
	}


	public BigDecimal getNotionalMultiplier() {
		return this.notionalMultiplier;
	}


	public void setNotionalMultiplier(BigDecimal notionalMultiplier) {
		this.notionalMultiplier = notionalMultiplier;
	}


	public boolean isCollateralTrade() {
		return this.collateralTrade;
	}


	public void setCollateralTrade(boolean collateralTrade) {
		this.collateralTrade = collateralTrade;
	}


	public boolean isBlockTrade() {
		return this.blockTrade;
	}


	public void setBlockTrade(boolean blockTrade) {
		this.blockTrade = blockTrade;
	}


	public BigDecimal getCommissionAmount() {
		return this.commissionAmount;
	}


	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


	public TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}


	public void setTradeDestination(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}


	public TradeGroup getTradeGroup() {
		return this.tradeGroup;
	}


	public void setTradeGroup(TradeGroup tradeGroup) {
		this.tradeGroup = tradeGroup;
	}


	public BigDecimal getFillQuantity() {
		return this.fillQuantity;
	}


	public void setFillQuantity(BigDecimal fillQuantity) {
		this.fillQuantity = fillQuantity;
	}


	public BigDecimal getFillPrice() {
		return this.fillPrice;
	}


	public void setFillPrice(BigDecimal fillPrice) {
		this.fillPrice = fillPrice;
	}


	public BigDecimal getAccrualAmount1() {
		return this.accrualAmount1;
	}


	public void setAccrualAmount1(BigDecimal accrualAmount1) {
		this.accrualAmount1 = accrualAmount1;
	}


	public BigDecimal getAccrualAmount2() {
		return this.accrualAmount2;
	}


	public void setAccrualAmount2(BigDecimal accrualAmount2) {
		this.accrualAmount2 = accrualAmount2;
	}


	public boolean isNovation() {
		return this.novation;
	}


	public void setNovation(boolean novation) {
		this.novation = novation;
	}


	public BusinessCompany getAssignmentCompany() {
		return this.assignmentCompany;
	}


	public void setAssignmentCompany(BusinessCompany assignmentCompany) {
		this.assignmentCompany = assignmentCompany;
	}


	public BigDecimal getAccrualAmount() {
		return MathUtils.add(getAccrualAmount1(), getAccrualAmount2());
	}


	public Integer getOrderIdentifier() {
		return this.orderIdentifier;
	}


	public void setOrderIdentifier(Integer orderIdentifier) {
		this.orderIdentifier = orderIdentifier;
	}


	@Override
	public boolean isCollateral() {
		return isCollateralTrade();
	}


	public BusinessCompany getExecutingSponsorCompany() {
		return this.executingSponsorCompany;
	}


	public void setExecutingSponsorCompany(BusinessCompany executingSponsorCompany) {
		this.executingSponsorCompany = executingSponsorCompany;
	}


	public TradeOpenCloseType getOpenCloseType() {
		return this.openCloseType;
	}


	public void setOpenCloseType(TradeOpenCloseType openCloseType) {
		this.openCloseType = openCloseType;
	}


	public BusinessCompany getSettlementCompany() {
		return this.settlementCompany;
	}


	public void setSettlementCompany(BusinessCompany settlementCompany) {
		this.settlementCompany = settlementCompany;
	}


	public TradeExecutionType getTradeExecutionType() {
		return this.tradeExecutionType;
	}


	public void setTradeExecutionType(TradeExecutionType tradeExecutionType) {
		this.tradeExecutionType = tradeExecutionType;
	}


	public BigDecimal getLimitPrice() {
		return this.limitPrice;
	}


	public void setLimitPrice(BigDecimal limitPrice) {
		this.limitPrice = limitPrice;
	}


	public void setRoll(boolean roll) {
		this.roll = roll;
	}


	public String getExternalTradeIdentifier() {
		return this.externalTradeIdentifier;
	}


	public void setExternalTradeIdentifier(String externalTradeIdentifier) {
		this.externalTradeIdentifier = externalTradeIdentifier;
	}
}
