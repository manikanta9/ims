package com.clifton.trade;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.trade.search.TradeFillSearchForm;
import com.clifton.trade.search.TradeOpenCloseTypeSearchForm;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.trade.search.TradeSourceSearchForm;
import com.clifton.trade.search.TradeTimeInForceTypeSearchForm;
import com.clifton.trade.search.TradeTypeSearchForm;


import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>TradeService</code> interface defines methods for working with trades.
 *
 * @author vgomelsky
 */
public interface TradeService {

	public static final String TRADE_WORKFLOW_NAME = "Trade Workflow";

	public static final String TRADES_DRAFT_STATUS_NAME = "Draft";
	public static final String TRADES_CLOSED_STATUS_NAME = "Closed";

	public static final String TRADES_CANCELLED_STATE_NAME = "Cancelled";
	public static final String TRADE_BOOKED_STATE_NAME = "Booked";
	public static final String TRADE_UNBOOKED_STATE_NAME = "Unbooked";
	public static final String TRADE_EXECUTED_STATE_NAME = "Executed";
	public static final String TRADE_EXECUTED_VALID_STATE_NAME = "Executed Valid";
	public static final String TRADE_EXECUTED_INVALID_STATE_NAME = "Executed Invalid";
	public static final String TRADE_CANNOT_FILL_STATE_NAME = "Cannot Fill";
	public static final String TRADE_AWAITING_FILLS_STATE_NAME = "Awaiting Fills";
	public static final String TRADE_EXECUTION_STATE_NAME = "Execution";
	public static final String TRADE_APPROVED_STATE_NAME = "Approved";

	public static final String FIX_CANCELED_STATE_NAME = "Cancelled";

	public static final String TRADING_PURPOSE_NAME_PREFIX = InvestmentAccountRelationshipPurpose.TRADING_PURPOSE_NAME_PREFIX;
	public static final String EXECUTING_PURPOSE_NAME_PREFIX = "Executing: ";

	////////////////////////////////////////////////////////////////////////////
	//////                   Trade Type Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeType getTradeType(short id);


	public TradeType getTradeTypeByName(String name);


	/**
	 * Checks by Investment Type and Investment Group (if populated and more than one for investment type)
	 */
	public TradeType getTradeTypeForSecurity(int investmentSecurityId);


	public List<TradeType> getTradeTypeList(TradeTypeSearchForm searchForm);


	@DoNotAddRequestMapping
	public void saveTradeType(TradeType tradeType);

	////////////////////////////////////////////////////////////////////////////
	//////               Trade Open Close Type Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeOpenCloseType getTradeOpenCloseType(short id);


	public TradeOpenCloseType getTradeOpenCloseTypeByName(String name);


	public List<TradeOpenCloseType> getTradeOpenCloseTypeList(TradeOpenCloseTypeSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	//////                     Trade Source Methods                      ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeSource getTradeSource(short id);


	public TradeSource getTradeSourceByName(String name);


	public TradeSource saveTradeSource(TradeSource tradeSource);


	public List<TradeSource> getTradeSourceList(TradeSourceSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	//////               Trade Time In Force Type Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	public TradeTimeInForceType getTradeTimeInForceType(short id);


	public TradeTimeInForceType getTradeTimeInForceTypeByName(String name);


	public TradeTimeInForceType saveTradeTimeInForceType(TradeTimeInForceType tradeTimeInForceType);


	public List<TradeTimeInForceType> getTradeTimeInForceTypeList(TradeTimeInForceTypeSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	//////                        Trade Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a fully populated Trade, including TradeFill(s), for the specified id.
	 */
	public Trade getTrade(int id);


	/**
	 * Returns the opening {@link Trade} entity corresponding to the provided position.
	 * <p>
	 * This method will throw an exception if the source of the provided position is not a trade.
	 */
	@DoNotAddRequestMapping
	public Trade getOpeningTradeForAccountingPosition(AccountingPosition position);


	public List<Trade> getTradeList(TradeSearchForm searchForm);


	/**
	 * Returns a list of pending trades (i.e. Workflow Status Name != Closed) filtered by a
	 * Client Account
	 * and/or Holding Account
	 * and/or SecurityID
	 */
	public List<Trade> getTradePendingList(Integer clientInvestmentAccountId, Integer holdingInvestmentAccountId, Integer securityId);


	/**
	 * Saves the given {@link Trade} and returns the persisted entity.
	 */
	public Trade saveTrade(Trade trade);


	/**
	 * Update the trade with the given settlement company.
	 */
	public Trade saveTradeSettlementCompany(int id, int settlementCompanyId);


	/**
	 * Iterates through all of the trade's fills and checks whether the booking date for each fill is null.
	 *
	 * @return whether the trade has been fully booked
	 */
	@DoNotAddRequestMapping
	public boolean isTradeFullyBooked(int tradeId);


	/**
	 * Splits the provided Trade into two trades. The newly created Trade will have have Quantity Intended set to the
	 * <code>quantityToSplit</code> and the provided Trade will have Quantity Intended to
	 * <code>tradeToSplit.getQuantityIntended() - quantityToSplit</code>. Both Trades will be saved and returned in
	 * an Array with the original Trade at location 0.
	 */
	@DoNotAddRequestMapping
	public Trade[] splitTrade(Trade tradeToSplit, BigDecimal quantityToSplit);


	/**
	 * Takes a trade and a BigDecimal value and looks up the trade's FX Rate based on the Trade's Investment Security's denomination currency vs. the Trade's settlement currency.
	 * Returns the a new value that is the incoming value multiplied by the FX Rate derived from the aforementioned currencies.
	 */
	@SecureMethod(dtoClass = Trade.class)
	public BigDecimal getTradeAmountForTradeSettlementCurrency(Trade trade, BigDecimal value);

	////////////////////////////////////////////////////////////////////////////
	////////////////             Trade Fill Methods       //////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeFill getTradeFill(int id);


	@DoNotAddRequestMapping
	public TradeFill getTradeFillForAccountingPosition(AccountingPosition accountingPosition);


	public List<TradeFill> getTradeFillListByTrade(int tradeId);


	public List<TradeFill> getTradeFillList(TradeFillSearchForm searchForm);


	/**
	 * Adds the specified fill to its trade if it passes validation.
	 * This method is for non-FIX trades only and will throw ValidationException
	 * if it's called for a FIX trade.
	 */
	public TradeFill saveTradeFill(TradeFill fill);


	public void saveTradeFillList(List<TradeFill> tradeFillList);


	public void deleteTradeFill(int id);


	public void deleteTradeFillList(List<TradeFill> tradeFillList);
}
