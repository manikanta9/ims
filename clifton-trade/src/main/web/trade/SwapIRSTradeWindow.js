TCG.use('Clifton.trade.SwapTradeFormBase');

Clifton.trade.SwapIRSTradeForm = Ext.extend(Clifton.trade.SwapTradeFormBase, {
	investmentTypeSubType: 'Interest Rate Swaps',

	updateFromTradeDateChange: function() {
		this.updateCalculatedValues('quantityIntended');
		Clifton.trade.updateExistingPositions(this);
	},

	updateFromSettlementDateChange: function() {
		this.updateCalculatedValues('quantityIntended');
	},

	updateFromSettlementCurrencySelection: function(currencySecurityId) {
		this.updateCalculatedValues('payingSecurity');
	},
	updateFromSecuritySelection: function(securityId) {
		this.updateCalculatedValues('investmentSecurity');
	},

	updateCalculatedValues: async function(updatedField, newValue, oldValue) {
		const fp = this;
		const f = fp.getForm();
		const securityId = f.findField('investmentSecurity.id').getValue();

		Promise.resolve()
			.then(function() {
				if ((TCG.isNull(updatedField) && TCG.isBlank(f.findField('accrualAmount1').getValue()) && TCG.isBlank(f.findField('accrualAmount2').getValue())) || updatedField === 'quantityIntended' || updatedField === 'payingSecurity' || updatedField === 'accrualMethod' || updatedField === 'investmentSecurity') {
					return fp.updateAccruedInterest();
				}
			})
			.then(function() {
				if (updatedField === 'averageUnitPrice' || updatedField === 'quantityIntended' || updatedField === 'payingSecurity' || updatedField === 'investmentSecurity'|| updatedField === 'forceReCalcPrincipal') {
					const price = f.findField('averageUnitPrice').getNumericValue();
					const qty = f.findField('quantityIntended').getNumericValue();
					if (Ext.isNumber(price) && Ext.isNumber(qty)) {
						// accounts for rounding: JPY, etc.
						return TCG.data.getDataValuePromise('investmentSecurityNotional.json?securityId=' + securityId + '&price=' + price + '&quantity=' + qty, fp)
							.then(function(notional) {
								fp.setFormValue('accountingNotional', notional);
							});
					}
				}
			})
			.then(function() {
				fp.setFormValue('accrualAmount', (f.findField('accrualAmount1').getNumericValue() || 0) + (f.findField('accrualAmount2').getNumericValue() || 0), true);

				const principal = f.findField('accountingNotional').getNumericValue();
				const interest = f.findField('accrualAmount').getNumericValue();
				const commission = f.findField('commissionAmount').getNumericValue() || 0;
				const fee = f.findField('feeAmount').getNumericValue() || 0;
				const totalPayment = principal + interest + commission + fee;
				if (TCG.isNotBlank(totalPayment)) {
					fp.setFormValue('totalPayment', totalPayment, true);
					TCG.data.getDataValuePromise(`tradeAmountForTradeSettlementCurrency.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&value=${totalPayment}&` + Ext.lib.Ajax.serializeForm(f.el.dom, true), fp)
							.then(totalPaymentCcy => fp.setFormValue('totalPaymentCcy', totalPaymentCcy, true));
				}
			});
	},

	updateAccruedInterest: function() {
		const fp = this;
		const f = fp.getForm();
		const securityId = f.findField('investmentSecurity.id').getValue();
		const purchaseFace = f.findField('quantityIntended').getNumericValue();
		const tradeDate = f.findField('settlementDate').getValue();
		if (TCG.isNull(fp.getFormValue('buy')) || TCG.isNull(fp.getFormValue('clientInvestmentAccount.label'))) {
			// Unable to calculate accrual with these missing. Wait for them to be populated and avoid showing error that will be present each time this is attempted.
			return;
		}
		return Promise.resolve()
			.then(function() {
				if (tradeDate && TCG.isNotNull(securityId) && Ext.isNumber(purchaseFace) && f.findField('accrualAmount1').isVisible()) {
					return Promise.all([
							TCG.data.getDataValuePromise('tradeAccrualAmount1.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(f.el.dom, true), fp),
							TCG.data.getDataValuePromise('tradeAccrualAmount2.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(f.el.dom, true), fp)
						]
					).then(function(ais) {
						fp.setFormValue('accrualAmount1', ais[0] || 0, true);
						fp.setFormValue('accrualAmount2', ais[1] || 0, true);
					});
				}
			});
	},

	securityEntryItems: [{
		xtype: 'panel',
		layout: 'column',
		labelWidth: 120,
		items: [
			{
				columnWidth: .42,
				layout: 'form',
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{
						fieldLabel: 'Structure', name: 'accrualMethod', hiddenName: 'buy', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
						store: new Ext.data.ArrayStore({
							fields: ['name', 'value', 'description'],
							data: [
								['Pay Floating and Receive Fixed Leg', true, 'Client pays Floating Leg and receives Fixed Leg'],
								['Pay Fixed and Receive Floating Leg', false, 'Client pays Fixed Leg and receives Floating Leg']
							]
						}),
						listeners: {
							change: function(radioGroup, newValue, oldValue) {
								const formPanel = TCG.getParentFormPanel(radioGroup);
								const params = {
									tradeTypeId: formPanel.getFormValue('tradeType.id'),
									buy: newValue
								};
								TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params})
									.then(function(openCloseType) {
										if (openCloseType && openCloseType.length === 1) {
											formPanel.getForm().findField('openCloseType.id').setValue(openCloseType[0].id);
										}
										formPanel.updateCalculatedValues('accrualMethod');
									});
							}
						}
					},
					{xtype: 'hidden', name: 'openCloseType.id'},
					{
						fieldLabel: 'Notional', name: 'quantityIntended', xtype: 'currencyfield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'quantityIntended', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'pricefield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.getForm().findField('averageUnitPrice').setValue(field.getNumericValue());
								fp.updateCalculatedValues.call(fp, 'averageUnitPrice', newValue, oldValue);
							}
						}
					},
					{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', qtip: 'The value to exchange one unit of the security\'s currency denomination for one unit of the client account\'s base currency. If settlement currency of the trade does not match the security\'s or client\'s currency, then the system will calculate the crossing rate value via the exchange rate on the trade multiplied by default data source exchange rate from settlement currency on the trade to client account\'s base currency.'}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				defaults: {anchor: '-20'},
				items: [
					{
						fieldLabel: 'Fixed Leg', name: 'accrualAmount1', xtype: 'currencyfield', allowBlank: false, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive).',
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'accrualAmount1', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Floating Leg', name: 'accrualAmount2', xtype: 'currencyfield', allowBlank: false, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive).',
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'accrualAmount2', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Commission Amount', name: 'commissionAmount', xtype: 'currencyfield', qtip: 'The commission for this trade. This quantity should be entered in units of the Swap Security\'s trading currency.  When applied to the Settlement Total, the quantity is automatically converted to the Settlement CCY.',
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'commissionAmount', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield', qtip: 'Additional fees associated wit this trade. This quantity should be entered in units of the Swap Security\'s trading currency.  When applied to the Settlement Total, the quantity is automatically converted to the Settlement CCY.',
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'feeAmount', newValue, oldValue);
							}
						}
					}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{fieldLabel: 'Current Principal', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true, qtip: 'The calculated principal value for the trade.'},
					{fieldLabel: 'Accrued Interest', name: 'accrualAmount', xtype: 'currencyfield', readOnly: true, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
					{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Settlement Total = Current Principal + Accrued Interest + Commission Amount + Fee.'},
					{fieldLabel: 'Settlement CCY Total', name: 'totalPaymentCcy', xtype: 'currencyfield', readOnly: true, submitValue: false,  qtip: 'Settlement Total in the denomination of the Settlement CCY. Settlement CCY Total = Settlement Total * FX Rate.'}
				]
			}]
	}],

	securityEntryItems_readOnly: [{
		xtype: 'panel',
		layout: 'column',
		labelWidth: 120,
		items: [
			{
				columnWidth: .42,
				layout: 'form',
				defaults: {xtype: 'textfield'},
				items: [
					{
						fieldLabel: 'Structure', name: 'buy', xtype: 'displayfield',
						setRawValue: function(v) {
							TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'Pay Floating and Receive Fixed Leg' : 'Pay Fixed and Receive Floating Leg');
						}
					},
					{fieldLabel: 'Notional', name: 'quantityIntended', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'pricefield', readOnly: true},
					{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', readOnly: true, qtip: 'The value to exchange one unit of the security\'s currency denomination for one unit of the client account\'s base currency. If settlement currency of the trade does not match the security\'s or client\'s currency, then the system will calculate the crossing rate value via the exchange rate on the trade multiplied by default data source exchange rate from settlement currency on the trade to client account\'s base currency.'}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				defaults: {xtype: 'floatfield', width: 110},
				items: [
					{fieldLabel: 'Fixed Leg', name: 'accrualAmount1', xtype: 'currencyfield', readOnly: true, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
					{fieldLabel: 'Floating Leg', name: 'accrualAmount2', xtype: 'currencyfield', readOnly: true, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
					{fieldLabel: 'Commission Amount', name: 'commissionAmount', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield', readOnly: true}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{fieldLabel: 'Current Principal', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Accrued Interest', name: 'accrualAmount', xtype: 'currencyfield', readOnly: true, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
					{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Settlement Total = Current Principal + Accrued Interest + Commission Amount + Fee'},
					{fieldLabel: 'Settlement CCY Total', name: 'totalPaymentCcy', xtype: 'currencyfield', readOnly: true, submitValue: false,  qtip: 'Settlement Total in the denomination of the Settlement CCY. Settlement CCY Total = Settlement Total * FX Rate.'}
				]
			}]
	}],

	populateClosingPositionBuySell: function(positionRecord, force) {
		const form = this.getForm();
		const quantity = this.getClosingPositionQuantity.call(this, positionRecord);
		const buy = quantity < 0;
		const existingBuySelection = form.findField('accrualMethod').getValue();
		if (TCG.isBlank(existingBuySelection) || TCG.isTrue(force)) {
			form.findField('accrualMethod').setValue(buy);
		}
		else if (existingBuySelection.inputValue !== buy) {
			TCG.showError('Unable to add closing position because it would does not satisfy the current Structure selection.', 'Unsupported Position');
			return false;
		}
		return true;
	}
});
Ext.reg('trade-swap-irs-form', Clifton.trade.SwapIRSTradeForm);


Clifton.trade.SwapIRSTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 1000,
	height: 600,
	iconCls: 'swap',


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,

		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.trade.DefaultTradeWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.trade.DefaultTradeWindowAdditionalTabs[i]);
				}
			}
		},

		items: [
			{
				title: 'Info',
				tbar: {xtype: 'trade-workflow-toolbar'},
				items: [{
					xtype: 'trade-swap-irs-form'
				}]
			},


			{
				title: 'Compliance',
				items: [{
					xtype: 'trade-violations-grid'
				}]
			},


			{
				title: 'Trade Fills',
				items: [{xtype: 'trade-fillsgrid'}]
			},


			{
				title: 'Commissions and Fees',
				items: [{
					xtype: 'trade-commissions'
				}]
			},


			{
				title: 'Payment Legs',
				items: [{
					name: 'investmentSecurityEventListFind',
					xtype: 'gridpanel',
					instructions: 'Fixed and Floating leg payments.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Event Type', width: 70, dataIndex: 'type.name'},
						{header: 'Coupon %', width: 50, dataIndex: 'afterEventValue', type: 'float'},
						{header: 'Accrual Start', width: 50, dataIndex: 'declareDate'},
						{header: 'Accrual End', width: 50, dataIndex: 'recordDate'},
						{header: 'Ex Date', width: 50, dataIndex: 'exDate'},
						{
							header: 'Payment Date', width: 50, dataIndex: 'eventDate', defaultSortColumn: true, defaultSortDirection: 'DESC',
							renderer: function(v, metaData, r) {
								const value = TCG.renderDate(v);
								if (r.json.createDate !== r.json.updateDate) {
									return '<div class="amountAdjusted" qtip="Coupon Data was updated. See Audit Trail for details.">' + value + '</div>';
								}
								return value;
							}
						},
						{header: 'Description', width: 100, dataIndex: 'eventDescription'}
					],
					getLoadParams: function() {
						return {
							securityId: this.getWindow().getMainFormPanel().getFormValue('investmentSecurity.id'),
							typeNames: ['Fixed Leg Payment', 'Floating Leg Payment']
						};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Quotes',
				items: [{
					xtype: 'trade-quotes'
				}]
			}
		]
	}]
});
