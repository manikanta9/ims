Clifton.trade.quote.TradeQuoteListWindow = Ext.extend(TCG.app.Window, {
	id: 'tradeQuoteListWindow',
	title: 'Trade Quotes',
	iconCls: 'call',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [{
			title: 'Quotes',
			items: [{
				name: 'tradeQuoteListFind',
				xtype: 'gridpanel',
				instructions: 'A list of all competitive trade quotes in the system. Multiple trade quotes are usually obtained via a single Quote Request from multiple executing parties.',
				columns: [
					{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
					{header: 'Client Account', width: 150, dataIndex: 'quoteRequest.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
					{header: 'Trade Type', width: 60, dataIndex: 'quoteRequest.trade.tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}, hidden: true},
					{header: 'Security', width: 60, dataIndex: 'quoteRequest.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
					{header: 'Underlying', width: 60, dataIndex: 'quoteRequest.investmentSecurity.instrument.underlyingInstrument.identifierPrefix', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
					{header: 'Executing Broker', width: 100, dataIndex: 'executingBroker.label', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {type: 'combo', searchFieldName: 'executingBrokerId', displayField: 'label', url: 'businessCompanyListFind.json?issuer=true&abbreviationRequired=true'}},
					{header: 'Bid Price', width: 60, dataIndex: 'bidPrice', useNull: true, type: 'float'},
					{header: 'Ask Price', width: 60, dataIndex: 'askPrice', useNull: true, type: 'float'},
					{header: 'Spread', width: 60, dataIndex: 'financingSpread', useNull: true, type: 'float', tooltip: 'Financing Spread'},
					{header: 'Indep Amt', width: 60, dataIndex: 'independentAmount', useNull: true, type: 'currency', tooltip: 'Independent Amount'},
					{header: 'Commission', width: 60, dataIndex: 'commissionAmount', useNull: true, type: 'currency'},
					{header: 'Fee', width: 60, dataIndex: 'feeAmount', useNull: true, type: 'currency'},
					{header: 'Note', width: 200, dataIndex: 'note', hidden: true},
					{header: 'Trade Date', width: 50, dataIndex: 'quoteRequest.tradeDate', filter: {searchFieldName: 'tradeDate'}},
					{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 12, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'}
				],
				getTopToolbarFilters: function(toolbar) {
					return [
						{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'}
					];
				},
				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						this.setFilterValue('quoteRequest.tradeDate', {'after': new Date().add(Date.DAY, -30)});
					}
					const params = {};
					const t = this.getTopToolbar();
					const grp = TCG.getChildByName(t, 'investmentGroupId');
					if (grp.getValue() !== '') {
						params.investmentGroupId = grp.getValue();
					}
					return params;
				},
				editor: {
					detailPageClass: 'Clifton.trade.quote.TradeQuoteWindow',
					drillDownOnly: true
				}
			}]
		},


			{
				title: 'Quote Requests',
				items: [{
					name: 'tradeQuoteRequestListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all competitive trade quote requests in the system. Each Quote Request is usually sent to multiple executing brokers and returned quotes are grouped under corresponding request.',
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Trade ID', width: 40, dataIndex: 'trade.id', type: 'int', filter: {searchFieldName: 'tradeId'}},
						{header: 'Trade Type', width: 60, dataIndex: 'trade.tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}, hidden: true},
						{header: 'Client Account', width: 150, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Security', width: 60, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Note', width: 200, dataIndex: 'note'},
						{header: 'Trade Date', width: 40, dataIndex: 'tradeDate'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -30)});
						}
					},
					editor: {
						detailPageClass: 'Clifton.trade.quote.TradeQuoteRequestWindow',
						drillDownOnly: true
					}
				}]
			}]
	}]
});
