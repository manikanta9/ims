Clifton.trade.quote.TradeQuoteWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Quote',
	iconCls: 'call',
	width: 700,
	height: 450,

	items: [{
		xtype: 'formpanel',
		url: 'tradeQuote.json?requestedMaxDepth=5',
		labelWidth: 110,
		labelFieldName: 'id',
		instructions: 'A competitive quote for a trade from the specified Executing Broker.',
		items: [
			{
				fieldLabel: 'Quote Request ID', xtype: 'container', layout: 'hbox',
				items: [
					{name: 'quoteRequest.id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.quote.TradeQuoteRequestWindow', detailIdField: 'quoteRequest.id', flex: 1},
					{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Trade ID:', xtype: 'displayfield', width: 150},
					{name: 'quoteRequest.trade.id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'quoteRequest.trade.id', flex: 1}
				]
			},
			{fieldLabel: 'Executing Broker', name: 'executingBroker.label', hiddenName: 'executingBroker.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&abbreviationRequired=true'},
			{
				fieldLabel: 'Bid Price', xtype: 'container', layout: 'hbox',
				items: [
					{name: 'bidPrice', xtype: 'floatfield', flex: 1},
					{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ask Price:', xtype: 'displayfield', width: 150},
					{name: 'askPrice', xtype: 'floatfield', flex: 1}
				]
			},
			{
				fieldLabel: 'Spread (bps)', xtype: 'container', layout: 'hbox', hidden: true, name: 'indepFields',
				items: [
					{name: 'financingSpread', xtype: 'currencyfield', flex: 1},
					{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Independent Amount:', xtype: 'displayfield', width: 150},
					{name: 'independentAmount', xtype: 'currencyfield', flex: 1}
				]
			},
			{
				fieldLabel: 'Commission Amount', xtype: 'container', layout: 'hbox', hidden: true, name: 'commFields',
				items: [
					{name: 'commissionAmount', xtype: 'currencyfield', flex: 1},
					{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fee:', xtype: 'displayfield', width: 150},
					{name: 'feeAmount', xtype: 'currencyfield', flex: 1}
				]
			},
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 70},
			{xtype: 'label', html: '&nbsp;'},
			{
				title: 'File Attachments',
				xtype: 'documentFileGrid-simple',
				definitionName: 'TradeQuoteAttachments',
				collapsed: false, // need this to fix layout bug that skips tbar
				hideDateFields: true,
				isPagingEnabled: function() {
					return false;
				}
			}
		],
		listeners: {
			beforerender: function() {
				const type = TCG.getValue('quoteRequest.trade.tradeType.name', this.getWindow().defaultData);
				this.initFields(type);
			},
			afterload: function() {
				const type = this.getFormValue('quoteRequest.trade.tradeType.name');
				this.initFields(type);
			}
		},
		initFields: function(type) {
			if (TCG.isNotNull(type) && type.indexOf('Swap') !== -1) {
				TCG.getChildByName(this, 'commFields').setVisible(true);
				TCG.getChildByName(this, 'indepFields').setVisible(true);
				this.doLayout();
			}
		}
	}]
});
