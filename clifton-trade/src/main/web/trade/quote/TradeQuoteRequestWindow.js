Clifton.trade.quote.TradeQuoteRequestWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Quote Request',
	iconCls: 'call',
	width: 700,
	height: 470,

	items: [{
		xtype: 'formpanel',
		url: 'tradeQuoteRequest.json?requestedMaxDepth=4',
		labelWidth: 120,
		labelFieldName: 'id',
		instructions: 'Each Quote Request is usually sent to multiple executing brokers and returned quotes are grouped here under corresponding request.',
		items: [
			{fieldLabel: 'Trade ID', name: 'trade.id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'trade.id'},
			{fieldLabel: 'Trade Type', name: 'trade.tradeType.name', xtype: 'displayfield'},
			{fieldLabel: 'Security', name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true'},
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true'},
			{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield'},
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 70},
			{
				xtype: 'formgrid',
				title: 'Executing Broker Quotes',
				storeRoot: 'quoteList',
				dtoClass: 'com.clifton.trade.quote.TradeQuote',
				detailPageClass: 'Clifton.trade.quote.TradeQuoteWindow',
				readOnly: true,
				columnsConfig: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Executing Broker', width: 180, dataIndex: 'executingBroker.label', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {type: 'combo', searchFieldName: 'executingBrokerId', displayField: 'label', url: 'businessCompanyListFind.json?issuer=true&abbreviationRequired=true'}},
					{header: 'Bid Price', width: 100, dataIndex: 'bidPrice', useNull: true, type: 'float'},
					{header: 'Ask Price', width: 100, dataIndex: 'askPrice', useNull: true, type: 'float'},
					{header: 'Spread', width: 60, dataIndex: 'financingSpread', useNull: true, type: 'float', hidden: true},
					{header: 'Independent Amount', width: 100, dataIndex: 'independentAmount', useNull: true, type: 'float', hidden: true},
					{header: 'Commission', width: 100, dataIndex: 'commissionAmount', useNull: true, type: 'float', hidden: true},
					{header: 'Fee', width: 100, dataIndex: 'feeAmount', useNull: true, type: 'float', hidden: true},
					{header: 'Note', width: 200, dataIndex: 'note'},
					{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', width: 20, exportHeader: 'File Attachment(s)', tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'}
				]
			}
		]
	}]
});
