Clifton.trade.TradeSourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Source',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'tradeSource.json',
		readOnly: true,
		instructions: 'A trade source defines where the trade originated. The source adds additional context to a trade that can change workflow and/or processing behavior applied to the trade.',

		items: [
			{fieldLabel: 'Source Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

			{xtype: 'label', html: '<hr/>'},

			{boxLabel: 'Allow bypassing dual approval for trades that originated from this source', name: 'bypassDualApprovalAllowed', xtype: 'checkbox'}
		]
	}]
});
