Clifton.trade.restriction.TradeRestrictionBrokerWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Restriction for Broker',
	iconCls: 'shopping-cart',
	width: 600,
	height: 300,
	items: [{
		xtype: 'formpanel',
		labelFieldName: 'clientAccount.label',
		instructions: 'This dialog allows for the adding or updating of a trade broker restriction.  Select a client account and broker company, then check the "Include" check box ' +
			'to make the broker inclusive in a set of allowed brokers, or leave it unchecked to exclude the broker from being used in trades for this account.',
		url: 'tradeRestrictionBroker.json',
		labelWidth: 150,
		items: [
			{fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', requestedProps: 'label'},
			{fieldLabel: 'Broker Company', name: 'brokerCompany.name', hiddenName: 'brokerCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?tradingAllowed=true', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{boxLabel: 'Include', name: 'include', xtype: 'checkbox', checked: true, qtip: 'Checking this box indicates that this account can only trade with this and other brokers selected for inclusion. Leaving this choice blank indicates the broker is excluded and the account is not allowed to trade through this broker.'}
		]
	}]
});
