Clifton.trade.restriction.type.TradeRestrictionTypeCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Restriction Category',
	iconCls: 'rule',
	height: 250,

	items: [{
		xtype: 'formpanel',
		instructions: 'Categories are used to group related Trade Restriction Types.',
		url: 'tradeRestrictionTypeCategory.json',
		items: [
			{fieldLabel: 'Category Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		]
	}]
});
