Clifton.trade.restriction.type.TradeRestrictionTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Restriction Type',
	iconCls: 'rule',
	height: 700,
	width: 800,

	items: [{
		xtype: 'formpanel',
		url: 'tradeRestrictionType.json',
		labelWidth: 150,
		items: [
			{fieldLabel: 'Restriction Category', name: 'category.name', hiddenName: 'category.id', xtype: 'combo', url: 'tradeRestrictionTypeCategoryListFind.json', detailPageClass: 'Clifton.trade.restriction.type.TradeRestrictionTypeCategoryWindow'},
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Evaluation Bean', name: 'evaluatorBean.name', hiddenName: 'evaluatorBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Trade Restriction Type Evaluator', detailPageClass: 'Clifton.system.bean.BeanWindow', allowBlank: false, qtip: 'The bean used to evaluate restrictions of this type.'},
			{fieldLabel: 'Evaluate Condition', name: 'evaluateCondition.name', hiddenName: 'evaluateCondition.id', xtype: 'system-condition-combo', qtip: 'If present, this condition will be used to determine whether restrictions of this type should be evaluated for a given trade. Otherwise, all restrictions of this type will be evaluated.'},
			{fieldLabel: 'Violation Ignore Condition', name: 'violationIgnoreCondition.name', hiddenName: 'violationIgnoreCondition.id', xtype: 'system-condition-combo', qtip: 'If present, this condition may be used to determine whether or not violations for restrictions of this type may be ignored. This is configured on the rule evaluator for trade restrictions. The rule evaluator may also include other constraints around violation ignore conditions.'},
			{fieldLabel: 'Entity Modify Condition', name: 'entityModifyCondition.name', hiddenName: 'entityModifyCondition.id', xtype: 'system-condition-combo'},
			{fieldLabel: 'Cause System Table', name: 'causeSystemTable.name', hiddenName: 'causeSystemTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'The table for the entities which are the sources for creation of restrictions of this type. This is used along with a soft foreign key reference on the restriction to point to the cause entity for the restriction. For example, the <code>WorkflowTask</code> table may be used if restrictions of this type are created through some workflow event.'},
			{
				fieldLabel: 'Configuration Flags', xtype: 'checkboxgroup', columns: 3, qtip: 'The configuration flags determine how this definition field is used; each configuration options has a more detailed description of its specific use.',
				items: [
					{boxLabel: 'Account Required', name: 'investmentAccountRequired', xtype: 'checkbox', qtip: 'Specifies if an account is required for Trade Restrictions of this type.'},
					{boxLabel: 'Security Required', name: 'investmentSecurityRequired', xtype: 'checkbox', qtip: 'Specifies if a security is required for Trade Restrictions of this type.'},
					{boxLabel: 'Value Required', name: 'restrictionValueRequired', xtype: 'checkbox', qtip: 'Specifies if a restriction value is required when creating a Trade Restriction of this type.'},
					{boxLabel: 'Note Required', name: 'restrictionNoteRequired', xtype: 'checkbox', qtip: 'Specifies if a note is required when creating a Trade Restriction of this type.'},
					{boxLabel: 'System Managed', name: 'systemManaged', xtype: 'checkbox', qtip: 'Specifies if Trade Restrictions of this type can only be created by the system.'},
					{boxLabel: 'Do Not Allow Overlap', name: 'doNotAllowOverlapInDates', xtype: 'checkbox', qtip: 'Specifies if Trade Restrictions of this type are allowed to overlap in start and end dates.'}
				]
			},
			{fieldLabel: 'Default Restriction Note', name: 'defaultRestrictionNote', xtype: 'textarea', qtip: 'The default restriction note. This note will be used if no note is provided when creating restrictions of this type.'},
			{fieldLabel: 'Default Lock Note', name: 'defaultLockNote', xtype: 'textarea'},
			{fieldLabel: 'Default Unlock Note', name: 'defaultUnlockNote', xtype: 'textarea'}
		]
	}]
});
