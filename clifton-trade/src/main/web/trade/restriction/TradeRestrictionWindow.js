/**
 * The window for viewing and modifying TradeRestriction entities.
 */
Clifton.trade.restriction.TradeRestrictionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Restriction',
	iconCls: 'rule',
	width: 800,
	height: 550,

	items: [{
		xtype: 'formpanel',
		instructions: 'Trade Restriction is used to restrict trading for the specified scope (client account and/or security). Restrictions will be evaluated during trade validation for all trades using the selected account and the selected security or a derivative thereof. Restriction scope and property fields cannot be changed for restrictions past their start date.',
		url: 'tradeRestriction.json',

		initComponent: function() {
			TCG.callParent(this, 'initComponent', arguments);
			// Automatically mark new windows as dirty
			const originalIsDirty = this.getForm().isDirty;
			this.getForm().isDirty = () => !this.getIdFieldValue() || originalIsDirty.call(this.getForm());
		},

		// Panel configuration methods
		getFormLabel: function() {
			return this.getFormValue('clientInvestmentAccount.label') || this.getFormValue('investmentSecurity.label');
		},

		// Field state methods
		applyFieldModificationRestrictions: function() {
			const systemManaged = this.form.findField('systemManaged').getValue();
			if (systemManaged) {
				this.setReadOnly(true);
			}
			else {
				// Restrict all modifiable fields for entities past their start date
				const startDate = this.form.findField('startDate').getValue();
				if (startDate && startDate < new Date()) {
					// Change fields into link fields
					this.convertComboToLinkField('clientInvestmentAccount.label');
					this.convertComboToLinkField('investmentSecurity.label');
					this.convertComboToLinkField('restrictionType.name', {listeners: null});

					// Disable other un-modifiable fields
					this.disableField('restrictionValue');
					this.disableField('startDate');

					// Restrict the end date once it has been passed
					const endDate = this.form.findField('endDate').getValue();
					if (endDate && endDate < new Date()) {
						this.disableField('endDate');
					}
				}
			}
		},
		updateRestrictionFieldState: function(restrictionType) {
			const restrictionValueField = this.getForm().findField('restrictionValue');
			const restrictionValueRequired = restrictionType.restrictionValueRequired;
			restrictionValueField.setDisabled(!restrictionValueRequired);
			if (!restrictionValueRequired) {
				restrictionValueField.setValue(null);
			}

			const noteField = this.getForm().findField('noteWithDefaults');
			if (!noteField.getValue()) {
				noteField.setValue(restrictionType.defaultRestrictionNote);
			}
		},

		focusOnFirstField: function() {
			// Focus on the note field; use reduced focus delay
			this.getForm().findField('noteWithDefaults').focus(false, 200);
		},

		listeners: {
			afterload: panel => {
				panel.updateRestrictionFieldState(panel.getFormValue('restrictionType'));
				panel.applyFieldModificationRestrictions();
			}
		},

		items: [
			// Localization warning
			{
				xtype: 'container', cls: 'warning-msg', html: 'Note: All dates are displayed and should be entered in Central Standard Time (CST).'
			},

			// Scope fields
			{
				xtype: 'sectionheaderfield', header: 'Restriction Scope'
			},
			{
				fieldLabel: 'Client Account', xtype: 'combo',
				name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label',
				url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow',
				qtip: 'The client account for which the restriction will be applied. If no account is selected then the restriction will apply globally to all trades using the selected security.'
			},
			{
				fieldLabel: 'Security', xtype: 'combo',
				name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', displayField: 'label',
				url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				qtip: 'The security for which the restriction will be applied. This restriction will also apply to derivatives of this security. If no security is selected then the restriction will apply to all trades for the selected client account.'
			},

			// Property fields
			{
				xtype: 'sectionheaderfield', header: 'Properties'
			},
			{
				fieldLabel: 'Restriction Type', xtype: 'combo',
				name: 'restrictionType.name', hiddenName: 'restrictionType.id', displayField: 'name',
				url: 'tradeRestrictionTypeListFind.json', detailPageClass: 'Clifton.trade.restriction.type.TradeRestrictionTypeWindow',
				listeners: {
					select: (field, record, index) => {
						const formPanel = TCG.getParentFormPanel(field);
						//Clear the note field on combo selection change.
						formPanel.getForm().findField('noteWithDefaults').setValue(null);
						formPanel.updateRestrictionFieldState(record.json);
					},
					afterrender: combo => {
						// Set default value
						combo.store.on('load', () => {
							if (!combo.getValue()) {
								const defaultRestriction = combo.findRecord(combo.displayField, 'No Trade');
								if (defaultRestriction) {
									combo.setValue(defaultRestriction.id);
								}
								combo.getParentForm().updateRestrictionFieldState(defaultRestriction.json);
							}
						}, null, {single: true});
						combo.doQuery(combo.allQuery, true);
					}
				},
				qtip: 'The type of restriction to apply.', requestedProps: 'restrictionValueRequired|defaultRestrictionNote'
			},
			{
				fieldLabel: 'Restriction Value', xtype: 'floatfield',
				name: 'restrictionValue',
				disabled: true, allowBlank: false,
				qtip: 'The restriction value to use when evaluating the selected type.'
			}, {
				fieldLabel: 'Caused By', xtype: 'linkfield',
				name: 'causeFkFieldId',
				detailIdField: 'causeFkFieldId',
				qtip: 'Link to the caused by detail page.',
				getDetailPageClass: function(fp) {
					return fp.getFormValue('restrictionType').causeSystemTable.detailScreenClass;
				}
			},
			{
				fieldLabel: 'System Managed', xtype: 'checkbox',
				name: 'systemManaged',
				disabled: true,
				qtip: 'If checked, the restriction is managed exclusively by the system and users may not edit it.'
			},

			// Date fields
			{
				xtype: 'sectionheaderfield', header: 'Applicable Dates'
			},
			{
				fieldLabel: 'Start Date', xtype: 'datefield',
				format: 'm/d/Y H:i:s',
				name: 'startDate',
				emptyText: 'Now', skipValidation: true,
				qtip: 'The start date on which the restriction should first be applied. If no value is provided then the current date and time will be used. This value cannot be changed once it is passed.'
			},
			{
				fieldLabel: 'End Date', xtype: 'datefield',
				format: 'm/d/Y H:i:s',
				name: 'endDate',
				qtip: 'The end date on which the restriction should be expired. This value cannot be changed once it is passed.'
			},

			// Notes
			{
				xtype: 'sectionheaderfield', header: 'Notes'
			},
			{
				fieldLabel: 'Restriction Note', xtype: 'textarea',
				name: 'noteWithDefaults',
				qtip: 'The note applied to the restriction. This note can be used for supplemental information, such as the reason for the restriction or a termination condition for the restriction.'
			}
		]
	}
	]
})
;
