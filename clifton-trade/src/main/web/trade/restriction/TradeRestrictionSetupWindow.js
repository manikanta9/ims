Clifton.trade.restriction.TradeRestrictionSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'tradeRestrictionSetupWindow',
	title: 'Trade Restrictions',
	iconCls: 'rule',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Trade Restrictions',
				items: [
					{xtype: 'trade-restriction-grid'}
				]
			},


			{
				title: 'Broker Restrictions',
				items: [{
					xtype: 'trade-restriction-broker-grid'
				}]
			},


			{
				title: 'Trade Restriction Types',
				items: [{
					xtype: 'trade-restriction-type-grid'
				}]
			},


			{
				title: 'Trade Restriction Categories',
				items: [{
					xtype: 'trade-restriction-type-category-grid'
				}]
			}
		]
	}]
});
