/**
 * The dialog window for disabling Trade Restrictions with a disable note.
 */
Clifton.trade.restriction.TradeRestrictionDisableWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Disable Trade Restriction',
	iconCls: 'rule',
	height: 510,
	okButtonText: 'Disable',

	tradeRestriction: null,
	clientInvestmentAccount: null,
	investmentSecurity: null,

	items: [{
		xtype: 'formpanel',
		instructions: 'Disable the displayed Trade Restriction. This will attach the provided Disable Note to the restriction and mark the Trade Restriction as expired.',
		labelWidth: 130,
		loadValidation: false,
		getSaveURL: () => 'tradeRestrictionDeactivate.json',
		focusOnFirstField: function() {
			// Focus on the note field; use reduced focus delay
			this.getForm().findField('disableNote').focus(false, 200);
		},
		listeners: {
			afterrender: formPanel => {
				const data = formPanel.getWindow().defaultData;
				const titleParams = [];
				titleParams.push(data.clientInvestmentAccount && data.clientInvestmentAccount.name);
				titleParams.push(data.investmentSecurity && data.investmentSecurity.symbol);
				formPanel.getWindow().setTitle(titleParams.filter(param => !!param).join(' - '));
				formPanel.setFormValue('id', data.tradeRestriction.id);
			}
		},
		items: [
			// Existing restriction details
			{
				xtype: 'sectionheaderfield', header: 'Restriction Details'
			},
			{
				fieldLabel: 'Trade Restriction', xtype: 'linkfield',
				name: 'tradeRestriction.label', detailIdField: 'tradeRestriction.id',
				submitDetailField: false,
				detailPageClass: 'Clifton.trade.restriction.TradeRestrictionWindow',
				qtip: 'The existing restriction. This field serves as a link to the original restriction entity.'
			},
			{
				fieldLabel: 'Client Account', xtype: 'linkfield',
				name: 'clientInvestmentAccount.label',
				submitDetailField: false,
				detailIdField: 'clientInvestmentAccount.id',
				detailPageClass: 'Clifton.investment.account.AccountWindow',
				qtip: 'The client account for which the restriction is applied. If no account is selected then the restriction applies globally to all trades using the selected security.'
			},
			{
				fieldLabel: 'Security', xtype: 'linkfield',
				name: 'investmentSecurity.label',
				submitDetailField: false,
				detailIdField: 'investmentSecurity.id',
				detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				qtip: 'The security for which the restriction is applied. This restriction will also apply to derivatives of this security. If no security is selected then the restriction applies to all trades for the selected client account.'
			},
			{
				fieldLabel: 'Restriction Type', xtype: 'combo',
				name: 'tradeRestriction.restrictionType.name', hiddenName: 'restrictionType.id', displayField: 'name',
				url: 'tradeRestrictionTypeListFind.json', detailPageClass: 'Clifton.trade.restriction.type.TradeRestrictionTypeWindow',
				qtip: 'The type of restriction to apply.'
			},
			{
				fieldLabel: 'Restriction Value', xtype: 'floatfield',
				name: 'restrictionValue',
				submitValue: false, disabled: true,
				qtip: 'The restriction value to use when evaluating the selected type.'
			},
			{
				fieldLabel: 'Restriction Note', xtype: 'textarea',
				name: 'tradeRestriction.note',
				submitValue: false, disabled: true,
				qtip: 'The note applied to the restriction. This note can be used for supplemental information, such as the reason for the restriction or a termination condition for the restriction.'
			},

			// Submission fields
			{
				xtype: 'sectionheaderfield', header: 'Reason for Disable'
			},
			{
				xtype: 'hidden',
				name: 'id'
			},
			{
				fieldLabel: 'Disable Note', xtype: 'textarea',
				name: 'disableNote',
				qtip: 'The disable note to be appended to the restriction. If provided, this note will be appended to the original restriction note along with the disabled date.'
			}
		]
	}]
});
