// not the actual window but a window selector based on 'tradeGroupType'

Clifton.trade.group.TradeGroupWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'tradeGroup.json?enableOpenSessionInView=true&requestedMaxDepth=5',

	// Copied from Default Trade Group Window
	requestedProperties: 'id|workflowState.name|clientInvestmentAccount.label|holdingInvestmentAccount.label|tradeAllocationDescription|buy|openCloseType.label|quantityIntended|investmentSecurity.symbol|investmentSecurity.name|exchangeRate|averageUnitPrice|commissionPerUnit|feeAmount|accountingNotional|clientInvestmentAccount.teamSecurityGroup.name|traderUser.label|fixTrade|tradeDate|settlementDate|blockTrade|executingBrokerCompany.id|executingBrokerCompany.label|tradeDestination.id|tradeDestination.name|tradeType.name',
	requestedPropertiesRoot: 'data.tradeList',


	getClassName: function(config, entity) {
		let screenClass;
		if (entity && entity.tradeGroupType) {
			screenClass = entity.tradeGroupType.detailScreenClass;
		}
		if (!screenClass) {
			screenClass = 'Clifton.trade.group.DefaultTradeGroupWindow';
		}
		return screenClass;
	},


	// OVERRIDE Still need the server call for Trade Group Type = Roll because it does some special processing for retrieval
	doOpenEntityWindow: function(config, entity, className) {
		if (className !== false) {
			if (typeof className === 'object') {
				if (className.getEntity) {
					entity = className.getEntity(config, entity);
				}
				if (className.className) {
					className = className.className;
				}
			}
			if (entity) {
				// the entity was already retrieved: pass it to the window and instruct not to get it again
				// don't do this for cases where there is a detail screen class override (i.e. Trade Rolls or EFP - need to get the entity from service)
				if (TCG.isBlank(entity.tradeGroupType.detailScreenClass)) {
					config = Ext.apply(config, {
						defaultDataIsReal: true,
						defaultData: entity
					});
				}
			}
			TCG.createComponent(className, config);
		}
	}
});
