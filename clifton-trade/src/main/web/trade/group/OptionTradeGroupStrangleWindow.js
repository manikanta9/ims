TCG.use('Clifton.trade.group.TradeGroupNoteDragAndDropPanel');

Clifton.trade.group.OptionTradeGroupStrangleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Option Trade Group Strangle',
	iconCls: 'options',
	width: 1200,
	height: 630,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	saveTimeout: 200,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		url: 'tradeGroupStrangle.json?enableOpenSessionInView=true&requestedMaxDepth=5',
		getSaveURL: function() {
			return 'tradeGroupActionExecute.json?enableOpenSessionInView=true';
		},

		listRequestedProperties: 'id|workflowState.name|clientInvestmentAccount.label|holdingInvestmentAccount.label|tradeAllocationDescription|buy|openCloseType.label|quantityIntended|investmentSecurity.symbol|investmentSecurity.name|exchangeRate|averageUnitPrice|commissionPerUnit|feeAmount|accountingNotional|clientInvestmentAccount.teamSecurityGroup.name|traderUser.label|fixTrade|tradeDate|settlementDate|executingBrokerCompany.id|executingBrokerCompany.label|tradeDestination.id|tradeDestination.name|tradeType.name',
		listRequestedPropertiesRoot: 'data.tradeList',
		getLoadParams: function(win) {
			const params = win.params;
			params.requestedProperties = this.listRequestedProperties;
			params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
			return params;
		},

		getSubmitParams: function() {
			const form = this.getForm();
			const params = {};
			const groupTypeName = form.findField('tradeGroupType.name');
			if (groupTypeName) {
				/*
				 * Need to submit TradeGroupType name since the binding does not occur for this virtual TradeGroupStrangle.
				 * The name is necessary to determine which TradeGroupType GroupTypes enum value to use.
				 */
				params['tradeGroupType.name'] = groupTypeName.getValue();
			}
			return params;
		},

		defaults: {anchor: '0'},
		items: [
			Clifton.trade.group.TradeGroupNoteDragAndDropPanel,
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .40,
						layout: 'form',
						defaults: {xtype: 'textfield'},
						items: [
							{fieldLabel: 'Group Type', name: 'tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroupType.id', detailPageClass: 'Clifton.trade.group.TradeGroupTypeWindow'},
							{fieldLabel: 'Put Security', name: 'investmentSecurity.label', xtype: 'linkfield', detailIdField: 'investmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
							{fieldLabel: 'Call Security', name: 'secondaryInvestmentSecurity.label', xtype: 'linkfield', detailIdField: 'secondaryInvestmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
							{fieldLabel: 'Action', name: 'tradeGroupAction', xtype: 'hidden'}
						]
					},
					{
						columnWidth: .30,
						layout: 'form',
						defaults: {xtype: 'textfield'},
						items: [
							{fieldLabel: 'Group ID', name: 'id', xtype: 'displayfield'},
							{fieldLabel: 'Put Outright', name: 'putTailGroup.id', xtype: 'linkfield', detailIdField: 'putTailGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
							{fieldLabel: 'Call Outright', name: 'callTailGroup.id', xtype: 'linkfield', detailIdField: 'callTailGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
							{fieldLabel: 'Parent Group', name: 'parent.id', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
						]
					},
					{
						columnWidth: .25,
						layout: 'form',
						defaults: {xtype: 'textfield', anchor: '-20'},
						items: [
							{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
							{fieldLabel: 'Workflow Status', name: 'workflowStatus.name', xtype: 'displayfield'},
							{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'displayfield', type: 'date'},
							{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'displayfield', detailIdField: 'traderUser.id'}
						]
					}]
			},
			{
				fieldLabel: 'Note', name: 'note', xtype: 'linkfield', detailIdField: 'sourceFkFieldId', anchor: '-20',
				getDetailPageClass: function(fp) {
					let screen = TCG.getValue('tradeGroupType.sourceDetailScreenClass', fp.getForm().formValues);
					if (TCG.isBlank(screen)) {
						screen = TCG.getValue('tradeGroupType.sourceSystemTable.detailScreenClass', fp.getForm().formValues);
					}
					const fkFieldId = TCG.getValue('sourceFkFieldId', fp.getForm().formValues);
					if (TCG.isBlank(screen) || TCG.isBlank(fkFieldId)) {
						TCG.showError('Drill down unsupported for this trade group.');
						return false;
					}
					return screen;
				}
			},
			{
				xtype: 'panel',
				layout: 'fit',
				items: [
					{
						xtype: 'trade-group-formgrid-scroll',
						height: 400,
						autoHeight: false,

						applyPriceToSelectedTrades: function(toolBar) {
							const fp = TCG.getParentFormPanel(toolBar);
							const putSecurityLabel = fp.getFormValue('investmentSecurity.label');
							const callSecurityLabel = fp.getFormValue('secondaryInvestmentSecurity.label');
							const putPrice = TCG.getChildByName(toolBar, 'putFillPrice').getValue();
							const callPrice = TCG.getChildByName(toolBar, 'callFillPrice').getValue();

							const store = this.store;
							if ((TCG.isNotBlank(putPrice) || TCG.isNotBlank(callPrice)) && store.modified) {
								store.each(function(record) {
									if (record.modified) {
										const recordSecuritySymbol = record.get('investmentSecurity.symbol');
										let priceModified = false;
										if (putSecurityLabel.startsWith(recordSecuritySymbol) && TCG.isNotBlank(putPrice)) {
											record.set('fillPrice', putPrice);
											priceModified = true;
										}
										else if (callSecurityLabel.startsWith(recordSecuritySymbol) && TCG.isNotBlank(callPrice)) {
											record.set('fillPrice', callPrice);
											priceModified = true;
										}
										if (priceModified === true) {
											const specifiedFillQuantity = record.get('fillQuantity');
											if (TCG.isBlank(specifiedFillQuantity)) {
												const intendedQuantity = record.get('quantityIntended');
												if (TCG.isNotBlank(intendedQuantity)) {
													// for convenience, set price on selected rows without having to specify fill quantities
													record.set('fillQuantity', intendedQuantity);
												}
											}
										}
									}
								}, this);
							}
							this.markModified();
						},

						addToolbarFillPriceField: function(toolBar) {
							toolBar.add({xtype: 'label', html: '&nbsp;Put Price:&nbsp;', qtip: 'Fill price to apply to selected Put trades when \'Fill Trades\' or \'Fill & Execute Trades\' is clicked.'});
							toolBar.add({xtype: 'pricefield', name: 'putFillPrice', width: 70});
							toolBar.add({xtype: 'label', html: '&nbsp;Call Price:&nbsp;', qtip: 'Fill price to apply to selected Put trades when \'Fill Trades\' or \'Fill & Execute Trades\' is clicked.'});
							toolBar.add({xtype: 'pricefield', name: 'callFillPrice', width: 70});
						}
					}
				],
				listeners: {
					afterlayout: TCG.grid.registerDynamicHeightResizing
				}
			}
		]
	}]
});
