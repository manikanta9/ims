Clifton.trade.group.TradeGroupTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Group Type',
	iconCls: 'shopping-cart',
	width: 700,
	height: 600,
	hideOKButton: false,
	hideApplyButton: true,
	hideCancelButton: true,

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			url: 'tradeGroupType.json?includeEnumProperties=true',
			labelWidth: 150,

			items: [
				{fieldLabel: 'Group Type Name', xtype: 'displayfield', name: 'name'},
				{fieldLabel: 'Description', xtype: 'textarea', name: 'description', readOnly: true},
				{
					fieldLabel: 'Detail Screen', xtype: 'displayfield', name: 'detailScreenClass',
					qtip: 'An optional window used for viewing and managing a trade group. If undefined, the default trade group window is used.'
				},
				{
					fieldLabel: 'Source Table', xtype: 'linkfield', name: 'sourceSystemTable.label', detailIdField: 'sourceSystemTable.id', detailPageClass: 'Clifton.system.schema.TableWindow',
					qtip: 'An optional table a trade group was created for. Trade groups with this defined, will contain the source ID of the entity the group was created for.'
				},
				{
					fieldLabel: 'Source Detail Screen', xtype: 'displayfield', name: 'sourceDetailScreenClass',
					qtip: 'An optional window, similar to the \'Detail Window\', but uses the \'Source Table\' and trade group\'s source entity ID to view and manage a trade group.'
				},
				{
					fieldLabel: 'Dynamic Calculator Bean', xtype: 'linkfield', name: 'dynamicCalculatorBean.name', detailIdField: 'dynamicCalculatorBean.id', detailPageClass: 'Clifton.system.bean.BeanWindow',
					qtip: 'An optional system bean used by a dynamic trade group to assist in reviewing client account positions and creating trades from those positions to include in a trade group.'
				},
				{xtype: 'label', html: '<hr/>'},
				{
					boxLabel: 'Security Required', xtype: 'checkbox', name: 'securityRequired', disabled: true,
					qtip: 'If checked, the trades of this group will all use the same security unless secondary security is also required, thus, all trades of a group will use either the defined primary or secondary investment security.'
				},
				{
					boxLabel: 'Secondary Security Required', xtype: 'checkbox', name: 'secondarySecurityRequired', disabled: true,
					qtip: 'If checked, the trades of this group will consist of a trade group\'s primary or secondary investment security. An example would be a group of trades rolling from one security to a new one of the same instrument and later maturity/expiration date.'
				},
				{
					boxLabel: 'One Trade Allowed', xtype: 'checkbox', name: 'oneTradeAllowed', disabled: true,
					qtip: 'If checked, a group can be created with a single trade. If unchecked, a trade group saved with only one trade will result in a single saved trade without the group.'
				},
				{
					boxLabel: 'Parent Group Required', xtype: 'checkbox', name: 'parentGroupRequired', disabled: true,
					qtip: 'If checked, the trade group will contain trades that are related to the defined parent trade group\'s trades. An example is the tail trades when rolling future trades (where the parent group contains closing trades and trades of equal quantity for the rolled to security; the tail group contains trades for the rolled to security with additional quantity).'
				},
				{
					boxLabel: 'Workflow Required', xtype: 'checkbox', name: 'workflowRequired', disabled: true,
					qtip: 'If checked, the trade group will have a workflow state reflecting the member trade with the least progressed workflow state. If unchecked, no workflow state will be observed.'
				},
				{
					boxLabel: 'Trade Import', xtype: 'checkbox', name: 'tradeImport', disabled: true,
					qtip: 'If checked, the trades of this group were generated from imported files.'
				},
				{
					boxLabel: 'Trade Roll', xtype: 'checkbox', name: 'roll', disabled: true,
					qtip: 'If checked, the trades of this group are associated with roll trades (rolling an existing position to a new position for a different security).'
				},
				{
					boxLabel: 'Broker Selection at Execution Allowed', xtype: 'checkbox', name: 'brokerSelectionAtExecutionAllowed', disabled: true,
					qtip: 'If checked, the trades of a trade group can avoid having a broker defined until execution. If unchecked, the broker must be defined on the trades according the rules enforced by the trade\'s type and destination.'
				},
				{
					boxLabel: 'Empty Trade List Allowed', xtype: 'checkbox', name: 'emptyTradeListAllowed', disabled: true,
					qtip: 'If checked, it is possible the trade group may contain no trades. This occurs when a parent trade group has its trades relocated to a child trade group.'
				},
				{
					boxLabel: 'Empty Trade List Allowed On Initial Save', xtype: 'checkbox', name: 'emptyTradeListOnInitialSaveAllowed', disabled: true,
					qtip: 'If checked, it is possible the trade group is created containing no trades. This occurs when a hierarchical trade group is created with a child trade group containing trades.'
				},
				{
					boxLabel: 'Trade Asset Class Allocation Allowed', xtype: 'checkbox', name: 'tradeAssetClassAllocationAllowed', disabled: true,
					qtip: 'If checked, a trade group of this type is capable of creating trade asset class position allocations for contract splitting in addition to trades.'
				}
			]
		}
	]
});
