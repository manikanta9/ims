TCG.use('Clifton.trade.group.TradeGroupNoteDragAndDropPanel');

Clifton.trade.group.OptionTradeToCloseGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Option Trade To Close Group',
	iconCls: 'options',
	width: 950,
	height: 600,
	hideApplyButton: true,
	hideOKButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true, // use to avoid "Closing Window without Save" message. The header panel and grid are not editable in this window.
	shouldConsolidateDataRows: true,

	items: [
		{
			xtype: 'formpanel',
			url: 'tradeGroupDynamic.json',
			loadValidation: false,
			dtoClassForBinding: 'com.clifton.trade.group.dynamic.options.TradeGroupDynamicOptionTradeToClose',

			listRequestedProperties: 'clientAccount.id|clientAccount.label|clientAccount.name|holdingAccount.id|holdingAccount.number|holdingAccount.issuingCompany.name|' + //
				'putTrade.id|putTrade.quantityIntended|putTrade.workflowState.name|putTrade.commissionPerUnit|putTrade.averageUnitPrice|' + //
				'callTrade.id|callTrade.quantityIntended|callTrade.workflowState.name|callTrade.commissionPerUnit|callTrade.averageUnitPrice|' + //
				'putTailTrade.id|putTailTrade.quantityIntended|putTailTrade.workflowState.name|putTailTrade.commissionPerUnit|putTailTrade.averageUnitPrice|' + //
				'callTailTrade.id|callTailTrade.quantityIntended|callTailTrade.workflowState.name|callTailTrade.commissionPerUnit|callTailTrade.averageUnitPrice|',
			listRequestedPropertiesRoot: 'data.detailList',

			appendBindingParametersToUrl: function(url) {
				url = url + ((url.indexOf('?') > 0) ? '&' : '?');
				url = url + 'requestedMaxDepth=5&enableValidatingBinding=true&dtoClassForBinding=' + this.dtoClassForBinding;
				return url;
			},

			getLoadURL: function() {
				const panel = this;
				const strangleMethod = panel.getFormValue('strangleCalculatorMethod');
				if (TCG.isNotBlank(strangleMethod)) {
					return this.appendBindingParametersToUrl('tradeGroupDynamicEntry.json');
				}
				const w = this.getWindow();
				if (this.url && w && w.params) {
					return this.appendBindingParametersToUrl(this.url);
				}
				return false;
			},

			getLoadParams: function(win) {
				const panel = this;
				const form = panel.getForm();
				const params = Ext.apply(win.params ? win.params : {}, form.getValues());
				params.requestedProperties = this.listRequestedProperties;
				params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
				return params;
			},

			getSaveURL: function() {
				const form = this.getForm();
				// allow strangle to have precedence over executing actions
				const strangleMethod = form.findField('strangleCalculatorMethod').getValue();
				if (TCG.isNotBlank(strangleMethod)) {
					return this.appendBindingParametersToUrl('tradeGroupDynamicEntrySave.json');
				}
				const executeAction = form.findField('tradeGroupAction').getValue();
				if (TCG.isNotBlank(executeAction)) {
					return this.appendBindingParametersToUrl('tradeGroupDynamicActionExecute.json?enableOpenSessionInView=true');
				}
				return false;
			},

			getSubmitParams: function() {
				return {
					requestedProperties: this.listRequestedProperties,
					requestedPropertiesRoot: this.listRequestedPropertiesRoot
				};
			},

			defaults: {anchor: '0'},
			items: [
				Clifton.trade.group.TradeGroupNoteDragAndDropPanel,
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 95,
					defaults: {
						layout: 'form'
					},
					items: [
						{
							columnWidth: .50,
							items: [
								{name: 'strangleCalculatorMethod', xtype: 'hidden'},
								// fields for applying fill price
								{name: 'putFillPrice', xtype: 'hidden'},
								{name: 'callFillPrice', xtype: 'hidden'},
								{name: 'putTailFillPrice', xtype: 'hidden'},
								{name: 'callTailFillPrice', xtype: 'hidden'},
								// fields for applying commission overrides
								{name: 'putCommissionPerUnit', xtype: 'hidden'},
								{name: 'callCommissionPerUnit', xtype: 'hidden'},
								{name: 'putTailCommissionPerUnit', xtype: 'hidden'},
								{name: 'callTailCommissionPerUnit', xtype: 'hidden'},
								{name: 'enableCommissionRemoval', xtype: 'hidden'},
								// fields for updating executing broker
								{name: 'tradeDestination.id', xtype: 'hidden'},
								{name: 'executingBrokerCompany.label', xtype: 'hidden'},
								{name: 'executingBrokerCompany.id', xtype: 'hidden'},
								{fieldLabel: 'Group ID', name: 'id', xtype: 'displayfield', hidden: true},
								{fieldLabel: 'Group Type', name: 'tradeGroupType.name', xtype: 'linkfield', hidden: true, detailIdField: 'tradeGroupType.id', detailPageClass: 'Clifton.trade.group.TradeGroupTypeWindow'},
								{fieldLabel: 'Action', name: 'tradeGroupAction', xtype: 'hidden'},
								{name: 'putSecurity.lastDeliveryDate', xtype: 'datefield', submitValue: false, readOnly: true, hidden: true},
								{name: 'callSecurity.lastDeliveryDate', xtype: 'datefield', submitValue: false, readOnly: true, hidden: true},
								{fieldLabel: 'Instrument', name: 'investmentSecurity.label', xtype: 'linkfield', detailIdField: 'investmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
								{fieldLabel: 'Put Security', name: 'putSecurity.label', xtype: 'linkfield', detailIdField: 'putSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
								{fieldLabel: 'Call Security', name: 'callSecurity.label', xtype: 'linkfield', detailIdField: 'callSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
							]
						},
						{columnWidth: .02, items: [{html: '&nbsp;'}]},
						{
							columnWidth: .20,
							labelWidth: 115,
							items: [
								{fieldLabel: 'Strangle Group', name: 'strangleGroup.id', xtype: 'linkfield', detailIdField: 'strangleGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', submitDetailField: false},
								{fieldLabel: 'Put Outright Group', name: 'strangleGroup.putTailGroup.id', xtype: 'linkfield', detailIdField: 'strangleGroup.putTailGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', submitDetailField: false},
								{fieldLabel: 'Call Outright Group', name: 'strangleGroup.callTailGroup.id', xtype: 'linkfield', detailIdField: 'strangleGroup.callTailGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', submitDetailField: false},
								{fieldLabel: 'Parent Group', name: 'parent.id', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', hidden: true}
							]
						},
						{columnWidth: .02, items: [{html: '&nbsp;'}]},
						{
							columnWidth: .26,
							items: [
								{name: 'workflowState.id', xtype: 'hidden'},
								{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
								{fieldLabel: 'Workflow Status', name: 'workflowStatus.name', xtype: 'displayfield', hidden: true},
								{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
								{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'}
							]
						}]
				},
				{
					xtype: 'panel',
					layout: 'column',
					labelWidth: 95,
					defaults: {
						layout: 'form'
					},
					items: [
						{
							columnWidth: .72,
							items: [
								{
									fieldLabel: 'Note', name: 'note', xtype: 'linkfield', detailIdField: 'sourceFkFieldId', anchor: '-20',
									getDetailPageClass: function(fp) {
										let screen = TCG.getValue('tradeGroupType.sourceDetailScreenClass', fp.getForm().formValues);
										if (TCG.isBlank(screen)) {
											screen = TCG.getValue('tradeGroupType.sourceSystemTable.detailScreenClass', fp.getForm().formValues);
										}
										const fkFieldId = TCG.getValue('sourceFkFieldId', fp.getForm().formValues);
										if (TCG.isBlank(screen) || TCG.isBlank(fkFieldId)) {
											TCG.showError('Drill down unsupported for this trade group.');
											return false;
										}
										return screen;
									}
								}
							]
						},
						{columnWidth: .02, items: [{html: '&nbsp;'}]},
						{
							columnWidth: .26,
							items: [
								{
									xtype: 'radiogroup', disabled: true,
									fieldLabel: 'Trade Direction',
									qtip: 'Trades in this group are offsetting current positions. A long trade direction (buy) is to offset a short position and a short direction (sell) is to offset a long position.',
									columns: [60, 60],
									allowBlank: false,
									items: [
										{boxLabel: 'Long', name: 'longPositions', inputValue: true},
										{boxLabel: 'Short', name: 'longPositions', inputValue: false}
									]
								}
							]
						}
					]
				},
				{
					xtype: 'panel',
					layout: 'fit',
					items: [
						{
							xtype: 'option-trade-to-close-group-formgrid',
							height: 385,
							autoHeight: false
						}
					],
					listeners: {
						afterlayout: TCG.grid.registerDynamicHeightResizing
					}
				}
			]
		}
	]
});

Clifton.trade.group.OptionTradeToCloseGroupDetailFormGrid = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleDetailFormGrid, {
	name: 'option-trade-to-close-grid',
	dtoClass: 'com.clifton.trade.group.dynamic.TradeGroupDynamicOptionStrangleDetail',
	instructions: 'Option Trade To Close screen is used to close Option positions for Client Accounts. Positions will be closed with offsetting trades that can be strangled to minimize costs.',

	listeners: {
		'celldblclick': function(grid, rowIndex, cellIndex, evt) {
			const row = grid.store.data.items[rowIndex];
			const columnName = grid.getColumnModel().getColumnById(cellIndex).dataIndex;
			let tid = '';
			if (columnName.indexOf('Tail') > -1) {
				if (columnName && columnName.startsWith('put')) {
					tid = TCG.getValue('putTailTrade.id', row.json);
				}
				else if (columnName && columnName.startsWith('call')) {
					tid = TCG.getValue('callTailTrade.id', row.json);
				}
			}
			else {
				if (columnName && columnName.startsWith('put')) {
					tid = TCG.getValue('putTrade.id', row.json);
				}
				else if (columnName && columnName.startsWith('call')) {
					tid = TCG.getValue('callTrade.id', row.json);
				}
			}
			if (TCG.isNotBlank(tid)) {
				TCG.createComponent('Clifton.trade.TradeWindow', {
					id: TCG.getComponentId('Clifton.trade.TradeWindow', tid),
					params: {id: tid},
					openerCt: grid
				});
			}
		},
		afterload: function(grid) {
			grid.reloadToolbarMenus();
			grid.markModified(true);
			const panel = TCG.getParentFormPanel(grid);
			const form = panel.getForm();
			const actionField = form.findField('tradeGroupAction');
			if (actionField) {
				// since we load the response data, make sure to clear action
				actionField.setValue('');
			}
			const strangleMethodField = form.findField('strangleCalculatorMethod');
			if (strangleMethodField) {
				// since we load the response data, make sure to clear strangle method
				strangleMethodField.setValue('');
			}
			if (grid.chainedRequestCallback !== undefined) {
				if (grid.applyChainedCallback === true) {
					grid.chainedRequestCallback.call(grid);
				}
				else {
					// clear the callback as the previous request failed and the callback was not executed
					grid.chainedRequestCallback = undefined;
				}
			}
		}
	},

	generateQuoteEmail: function() {
		Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleDetailFormGrid.prototype.generateQuoteEmail.call(this, true);
	},

	commonColumns: [
		{header: 'Client Account', dataIndex: 'clientAccount.label', hidden: false, submitValue: false, width: 350},
		{header: 'Holding Account', dataIndex: 'holdingAccount.number', hidden: false, submitValue: false, width: 120}
	],

	customColumns: [
		{
			header: 'Put Qty', width: 85, tradeEntry: false, dataIndex: 'putTrade.quantityIntended', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity to trade for selected Put above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.'
		},
		{
			header: 'Put Tail Qty', width: 85, tradeEntry: false, dataIndex: 'putTailTrade.quantityIntended', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Tail quantity to trade for selected Put above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.'
		},
		{
			header: 'Call Qty', width: 85, tradeEntry: false, dataIndex: 'callTrade.quantityIntended', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity to trade for selected Call above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.'
		},
		{
			header: 'Call Tail Qty', width: 85, tradeEntry: false, dataIndex: 'callTailTrade.quantityIntended', type: 'int', useNull: true, negativeInRed: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Tail quantity to trade for selected Call above for this account. Buy/Sell is based on Long/Short selection at the top of the screen.'
		}
	],

	plugins: {
		ptype: 'gridsummary'
	}
});
Ext.reg('option-trade-to-close-group-formgrid', Clifton.trade.group.OptionTradeToCloseGroupDetailFormGrid);
