TCG.use('Clifton.trade.group.TradeGroupNoteDragAndDropPanel');

Clifton.trade.group.DefaultTradeGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Group',
	iconCls: 'shopping-cart',
	width: 1200,
	height: 630,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	saveTimeout: 200,

	windowOnShow: function(window) {
		const tabs = window.items.get(0);
		if (TCG.isTrue(window.defaultDataIsReal)) {
			if (TCG.isTrue(window.defaultData.tradeGroupType.tradeAssetClassAllocationAllowed)) {
				// Add Trade Asset Class Allocation Tab
				tabs.add(this.tradeAssetClassAllocationTab);
			}
		}
	},

	items: [
		{
			xtype: 'tabpanel',
			items: [
				{
					title: 'Info',
					items: [
						{
							xtype: 'formpanel',
							loadValidation: false,
							url: 'tradeGroup.json?enableOpenSessionInView=true',
							getSaveURL: function() {
								return 'tradeGroupActionExecute.json?enableOpenSessionInView=true';
							},

							listRequestedProperties: 'id|workflowState.name|clientInvestmentAccount.label|holdingInvestmentAccount.label|tradeAllocationDescription|buy|openCloseType.label|quantityIntended|investmentSecurity.symbol|investmentSecurity.name|exchangeRate|averageUnitPrice|commissionPerUnit|feeAmount|accountingNotional|clientInvestmentAccount.teamSecurityGroup.name|traderUser.label|fixTrade|blockTrade|tradeDate|settlementDate|executingBrokerCompany.id|executingBrokerCompany.label|tradeDestination.id|tradeDestination.name|tradeType.name',
							listRequestedPropertiesRoot: 'data.tradeList',
							getLoadParams: function(win) {
								const params = win.params;
								params.requestedProperties = this.listRequestedProperties;
								params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
								return params;
							},

							defaults: {anchor: '0'},
							items: [
								Clifton.trade.group.TradeGroupNoteDragAndDropPanel,
								{
									xtype: 'panel',
									layout: 'column',
									items: [
										{
											columnWidth: .40,
											layout: 'form',
											defaults: {xtype: 'textfield'},
											items: [
												{fieldLabel: 'Group Type', name: 'tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroupType.id', detailPageClass: 'Clifton.trade.group.TradeGroupTypeWindow'},
												{fieldLabel: 'Security', name: 'investmentSecurity.label', xtype: 'linkfield', detailIdField: 'investmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
												{fieldLabel: 'Secondary Security', name: 'secondaryInvestmentSecurity.label', xtype: 'linkfield', detailIdField: 'secondaryInvestmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
												{fieldLabel: 'Action', name: 'tradeGroupAction', xtype: 'hidden'}
											]
										},
										{
											columnWidth: .30,
											layout: 'form',
											defaults: {xtype: 'textfield'},
											items: [
												{fieldLabel: 'Group ID', name: 'id', xtype: 'displayfield'},
												{fieldLabel: 'Parent Group', name: 'parent.id', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
												{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'displayfield', detailIdField: 'traderUser.id'}
											]
										},
										{
											columnWidth: .25,
											layout: 'form',
											defaults: {xtype: 'textfield', anchor: '-20'},
											items: [
												{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
												{fieldLabel: 'Workflow Status', name: 'workflowStatus.name', xtype: 'displayfield'},
												{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'displayfield', type: 'date'}
											]
										}]
								},
								{
									fieldLabel: 'Note', name: 'note', xtype: 'linkfield', detailIdField: 'sourceFkFieldId', anchor: '-20',
									getDetailPageClass: function(fp) {
										let screen = TCG.getValue('tradeGroupType.sourceDetailScreenClass', fp.getForm().formValues);
										if (TCG.isBlank(screen)) {
											screen = TCG.getValue('tradeGroupType.sourceSystemTable.detailScreenClass', fp.getForm().formValues);
										}
										const fkFieldId = TCG.getValue('sourceFkFieldId', fp.getForm().formValues);
										if (TCG.isBlank(screen) || TCG.isBlank(fkFieldId)) {
											TCG.showError('Drill down unsupported for this trade group.');
											return false;
										}
										return screen;
									}
								},
								{
									xtype: 'panel',
									layout: 'fit',
									items: [
										{
											xtype: 'trade-group-formgrid-scroll',
											height: 400,
											autoHeight: false
										}
									],
									listeners: {
										afterlayout: TCG.grid.registerDynamicHeightResizing
									}
								}
							]
						}
					]
				},


				{
					title: 'Trade Group Life Cycle',
					items: [{
						xtype: 'system-lifecycle-grid',
						tableName: 'TradeGroup'
					}]
				}
			]
		}
	],

	tradeAssetClassAllocationTab: {
		title: 'Trade Asset Class Allocations',
		reloadOnTabChange: true,
		items: [
			{
				xtype: 'trade-assetclass-position-allocation-grid',

				getTradeDateFilterValue: function() {
					const form = this.getWindow().getMainForm();
					return TCG.parseDate(TCG.getValue('tradeDate', form.formValues));
				}
			}
		]
	}
});
