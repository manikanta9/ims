Clifton.trade.group.FungeTradeGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Funge Trades',
	iconCls: 'futures',
	width: 1200,
	height: 675,
	hideApplyButton: true,
	hideOKButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true, // use to avoid "Closing Window without Save" message. The header panel and grid are not editable in this window.

	items: [
		{xtype: 'funge-trade-group-form-panel'}
	]
});

Clifton.trade.group.FungeTradeGroupFormPanel = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicFormPanel, {
	dtoClassForBinding: 'com.clifton.trade.group.dynamic.TradeGroupDynamicFunge',
	requestedMaxDepth: 6,

	additionalListRequestedProperties: 'holdingAccount.issuingCompany.id|holdingAccount.issuingCompany.name|bigQuantity|miniQuantity|' + //
		'bigTrade.id|bigTrade.buy|bigTrade.investmentSecurity.id|bigTrade.investmentSecurity.symbol|bigTrade.investmentSecurity.priceMultiplier|bigTrade.quantityIntended|bigTrade.executingBrokerCompany.id|bigTrade.executingBrokerCompany.name|bigTrade.tradeDestination.id|bigTrade.tradeDestination.name|bigTrade.averageUnitPrice|bigTrade.commissionPerUnit|' + //
		'miniTrade.id|miniTrade.buy|miniTrade.investmentSecurity.id|miniTrade.investmentSecurity.symbol|miniTrade.investmentSecurity.priceMultiplier|miniTrade.quantityIntended|miniTrade.executingBrokerCompany.id|miniTrade.executingBrokerCompany.name|miniTrade.tradeDestination.id|miniTrade.tradeDestination.name|miniTrade.averageUnitPrice|miniTrade.commissionPerUnit',

	applyCustomDefaultData: function(defaultData) {
		const formPanel = this;
		defaultData = Ext.apply({
			traderUser: TCG.getCurrentUser(),
			tradeDate: new Date().format('Y-m-d 00:00:00'),
			balanceDate: Clifton.calendar.getBusinessDayFromDate(new Date(), -1).format('Y-m-d 00:00:00'),
			tradeGroupType: TCG.data.getData('tradeGroupTypeByName.json?name=Funge', this, 'trade.group.type.Funge'),
			tradeDestination: TCG.data.getData('tradeDestinationByName.json?name=Manual', this, 'trade.tradeDestination.Manual'),
			tradeType: TCG.data.getData('tradeTypeByName.json?name=Futures', this, 'trade.type.Futures')
		}, defaultData);
		TCG.data.getDataPromise('investmentGroupListFind.json', this, {params: {nameExact: 'S&P Big Futures'}})
			.then(function(investmentGroups) {
				if (investmentGroups.length === 1) {
					const investmentGroupCombo = formPanel.getForm().findField('investmentGroup.label');
					investmentGroupCombo.setValue({text: investmentGroups[0].label, value: investmentGroups[0].id});
					investmentGroupCombo.fireEvent('change', investmentGroupCombo, investmentGroups[0].id);
				}
			});
		return defaultData;
	},

	reloadChildGrid: function(formpanel) {
		const gp = TCG.getChildByName(formpanel, 'funge-trade-grid');
		gp.reloadGrid.call(gp);
	},

	listeners: {
		beforerender: function() {
			this.add(this.getGroupItems());
		},
		afterload: function() {
			const actionField = this.getForm().findField('tradeGroupAction');
			if (actionField) {
				// since we load the response data, make sure to clear action
				actionField.setValue('');
			}
		},
		aftercreate: function() {
			const gp = TCG.getChildByName(this, 'funge-trade-grid');
			const clearTradeHandler = function() {
				// Clear submitted trades and keep track of non-submitted trade quantity modifications for future submits.
				const gridStore = gp.getStore();
				const modifiedRecords = gridStore.getModifiedRecords();
				gridStore.each(function(record) {
					const recordClientAccount = TCG.getValue('clientAccount.id', record.json),
						recordHoldingAccount = TCG.getValue('holdingAccount.id', record.json);
					record.beginEdit();
					for (let i = 0; i < modifiedRecords.length; i++) {
						const modifiedRecord = modifiedRecords[i];
						const modifiedClientAccount = TCG.getValue('clientAccount.id', modifiedRecord.json),
							modifiedHoldingAccount = TCG.getValue('holdingAccount.id', modifiedRecord.json);
						if ((recordClientAccount === modifiedClientAccount) && (recordHoldingAccount === modifiedHoldingAccount)) {
							if (TCG.isNumber(modifiedRecord.id)) {
								// submitted modified record - clear quantity
								record.data['bigTrade.quantityIntended'] = 0;
								record.data['miniTrade.quantityIntended'] = 0;
							}
							else {
								// non-submitted modified record - update quantity
								if (modifiedRecord.modified['bigTrade.quantityIntended']) {
									record.data['bigTrade.quantityIntended'] = modifiedRecord.modified['bigTrade.quantityIntended'];
									record.data['miniTrade.quantityIntended'] = gp.calculateMiniTradeQuantity(record);
								}
							}
						}
					}
					record.endEdit();
				}, gp);
				gp.getView().refresh();
				// Remove this listener
				gp.un('afterReloadGrid', clearTradeHandler, gp);
			};
			gp.on('afterReloadGrid', clearTradeHandler, gp);
			this.reloadChildGrid(this);
		}
	},

	getGroupItems: function() {
		const items = [];
		for (let i = 0, len = this.readOnlyCommonItems.length; i < len; i++) {
			items.push(this.readOnlyCommonItems[i]);
		}
		for (let i = 0, len = this.readOnlyItems.length; i < len; i++) {
			items.push(this.readOnlyItems[i]);
		}
		for (let i = 0, len = this.readOnlyGridItem.length; i < len; i++) {
			items.push(this.readOnlyGridItem[i]);
		}
		return items;
	},

	tradeEntryItems: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .28,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Client Info'},
						{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', hiddenName: 'clientAccountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json', allowBlank: true, detailPageClass: 'Clifton.investment.account.AccountGroupWindow', anchor: '-20'},
						{
							fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: true, detailPageClass: 'Clifton.investment.account.AccountWindow', anchor: '-20',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.store.setBaseParam('relatedPurpose', fp.getFormValue('tradeType.holdingAccountPurpose.name'));
									combo.store.setBaseParam('relatedPurposeActiveOnDate', fp.getForm().findField('tradeDate').value);
								}
							}
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .43,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Instrument Info'},
						{
							fieldLabel: 'Instrument Group', name: 'investmentGroup.label', hiddenName: 'investmentGroup.id', xtype: 'combo', submitValue: false, displayField: 'label', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', minListWidth: 550, anchor: '-20',
							listeners: {
								change: function(combo, newValue, oldValue) {
									const formPanel = combo.getParentForm();
									const form = formPanel.getForm();
									const bigInstrumentCombo = form.findField('bigInstrument.label');
									bigInstrumentCombo.store.removeAll();
									bigInstrumentCombo.lastQuery = null;
									bigInstrumentCombo.store.baseParams = {
										investmentTypeId: TCG.getValue('tradeType.investmentType.id', form.formValues),
										investmentGroupId: newValue
									};
									bigInstrumentCombo.store.on('load', function(store, records) {
										if (records.length === 1) {
											bigInstrumentCombo.setValue(records[0].id);

											// default mini instrument
											const miniInstrumentCombo = form.findField('miniInstrument.label');
											miniInstrumentCombo.store.removeAll();
											miniInstrumentCombo.lastQuery = null;
											miniInstrumentCombo.store.baseParams = {
												bigInstrumentId: records[0].id
											};
											miniInstrumentCombo.store.on('load', function(miniStore, miniRecords) {
												if (miniRecords.length === 1) {
													miniInstrumentCombo.setValue(miniRecords[0].id);
												}
											});
											miniInstrumentCombo.doQuery('');
										}
									});
									bigInstrumentCombo.doQuery('');
								}
							}

						},
						{
							fieldLabel: 'Big Instrument', name: 'bigInstrument.label', hiddenName: 'bigInstrument.id', xtype: 'combo', displayField: 'label', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', minListWidth: 550, anchor: '-20',
							listeners: {
								beforeQuery: function(qEvent) {
									const form = qEvent.combo.getParentForm().getForm();
									const investmentTypeId = TCG.getValue('tradeType.investmentType.id', form.formValues);
									qEvent.combo.store.setBaseParam('investmentTypeId', investmentTypeId);
									qEvent.combo.store.setBaseParam('investmentGroupId', form.findField('investmentGroup.label').getValue());
								}
							}
						},
						{
							fieldLabel: 'Mini Instrument', name: 'miniInstrument.label', hiddenName: 'miniInstrument.id', xtype: 'combo', displayField: 'label', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', minListWidth: 550, anchor: '-20',
							listeners: {
								beforeQuery: function(qEvent) {
									const form = qEvent.combo.getParentForm().getForm();
									const investmentTypeId = TCG.getValue('tradeType.investmentType.id', form.formValues);
									qEvent.combo.store.setBaseParam('investmentTypeId', investmentTypeId);
									const bigInstrumentId = form.findField('bigInstrument.label').getValue();
									if (TCG.isNotBlank(bigInstrumentId)) {
										qEvent.combo.store.setBaseParam('bigInstrumentId', bigInstrumentId);
									}
									else {
										qEvent.combo.store.setBaseParam('bigInstrumentPopulated', true);
									}
								},
								select: function(combo, record, index) {
									if (record) {
										const bigInstrumentCombo = TCG.getParentFormPanel(combo).getForm().findField('bigInstrument.label');
										bigInstrumentCombo.store.removeAll();
										bigInstrumentCombo.lastQuery = null;
										bigInstrumentCombo.store.baseParams = {
											id: record.json.bigInstrument.id
										};
										bigInstrumentCombo.store.on('load', function(store, records) {
											if (records.length === 1) {
												bigInstrumentCombo.setValue(records[0].id);
											}
										});
										bigInstrumentCombo.doQuery('');
									}
								}
							}
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false, anchor: '-20',
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									qEvent.combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: f.findField('tradeType.name').getValue()
									};
								}
							}
						},
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, anchor: '-20'},
						{
							fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = TCG.getParentFormPanel(field);
									const form = fp.getForm();
									form.findField('settlementDate').setValue(TCG.isBlank(newValue) ? '' : newValue);
								}
							}
						},
						{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'}
					]
				}
			]
		}
	],

	tradeEntryGridItem: [
		{
			xtype: 'panel',
			layout: 'fit',
			items: [
				{
					xtype: 'funge-detail-form-grid',
					tradeEntrySupported: true,
					autoHeight: false,
					height: 530
				}
			],
			listeners: {
				afterlayout: TCG.grid.registerDynamicHeightResizing
			}
		}
	],

	readOnlyItems: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .28,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Client Info'},
						{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', detailIdField: 'clientAccountGroup.id', xtype: 'linkfield', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
						{fieldLabel: 'Client Account', name: 'clientAccount.name', detailIdField: 'clientAccount.id', xtype: 'linkfield', url: 'investmentAccountListFind.json', detailPageClass: 'Clifton.investment.account.AccountWindow'}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .43,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Instrument Info'},
						{fieldLabel: 'Big Instrument', name: 'bigInstrument.label', detailIdField: 'bigInstrument.id', xtype: 'linkfield', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'},
						{fieldLabel: 'Mini Instrument', name: 'miniInstrument.label', detailIdField: 'miniInstrument.id', xtype: 'linkfield', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{name: 'workflowState.id', xtype: 'hidden'},
						{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
						{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'displayfield'},
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
					]
				}
			]
		}
	],

	readOnlyGridItem: [
		{
			xtype: 'panel',
			layout: 'fit',
			items: [
				{
					xtype: 'funge-detail-form-grid',
					tradeEntrySupported: false,
					autoHeight: false,
					viewConfig: {markDirty: false},
					height: 530
				}
			],
			listeners: {
				afterlayout: TCG.grid.registerDynamicHeightResizing
			}
		}
	]
});
Ext.reg('funge-trade-group-form-panel', Clifton.trade.group.FungeTradeGroupFormPanel);

Clifton.trade.group.FungeTradeGroupEntryFormPanel = Ext.extend(Clifton.trade.group.FungeTradeGroupFormPanel, {

	getLoadURL: function() {
		return false;
	},

	getLoadParams: function(win) {
		const panel = this;
		const form = panel.getForm();
		const params = Ext.apply(win.params ? win.params : {}, form.getValues());
		params.requestedProperties = this.listRequestedProperties + this.additionalListRequestedProperties;
		params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
		return params;
	},

	getSaveURL: function() {
		return this.appendBindingParametersToUrl('tradeGroupDynamicEntrySave.json');
	},

	updateTitle: function() {
		// Do nothing to prevent Option Blotter title change
	},

	getGroupItems: function() {
		const items = [];
		for (let i = 0, len = this.tradeEntryCommonItems.length; i < len; i++) {
			items.push(this.tradeEntryCommonItems[i]);
		}
		for (let i = 0, len = this.tradeEntryItems.length; i < len; i++) {
			items.push(this.tradeEntryItems[i]);
		}
		for (let i = 0, len = this.tradeEntryGridItem.length; i < len; i++) {
			items.push(this.tradeEntryGridItem[i]);
		}
		return items;
	}
});
Ext.reg('funge-trade-group-entry-form-panel', Clifton.trade.group.FungeTradeGroupEntryFormPanel);

Clifton.trade.group.FungeDetailFormGrid = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicDetailGrid, {
	name: 'funge-trade-grid',
	dtoClass: 'com.clifton.trade.group.dynamic.TradeGroupDynamicFungeDetail',
	groupField: 'clientAccount.label',
	instructions: 'When Options are subject to delivery, long/short Futures are delivered and offset trades are made. Funge trades offset these positions, often on T+1. This window helps show positions with offsetting big/mini Future positions that can be closed.',

	alwaysSubmitFields: ['holdingAccount.id', 'clientAccount.id', 'bigTrade.id', 'miniTrade.id'],
	doNotSubmitFields: ['includeTrade'],

	addEntryToolbarButtons: function(toolBar, gridpanel) {
		const gp = gridpanel;
		toolBar.add({
			text: 'Load Positions',
			tooltip: 'Load positions for the information provided above.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.submitField.setValue('[]');
				gp.reloadGrid();
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Submit Trades',
			tooltip: 'Create Funge Trades based on quantities entered below.',
			iconCls: 'shopping-cart',
			scope: this,
			handler: function() {
				if (this.submitField.getValue() === '[]') {
					TCG.showError('No rows selected.  Please select at least one row from the list.');
					return;
				}
				gp.getWindow().saveWindow(false);
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Clear Trades',
			tooltip: 'Clear all trade values in the grid below',
			iconCls: 'clear',
			scope: this,
			handler: function() {
				gp.clearTrades();
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Info',
			tooltip: 'Toggle instructions for this page',
			iconCls: 'info',
			enableToggle: true,
			handler: function() {
				if (this.pressed) {
					gp.insert(0, {
						xtype: 'panel',
						frame: true,
						layout: 'fit',
						bodyStyle: 'padding: 3px 3px 3px 3px',
						html: gp.instructions
					});
				}
				else {
					gp.remove(0);
				}
				gp.doLayout();
			}
		}, '-');
	},

	clearTrades: function() {
		const gp = this;
		gp.getStore().each(function(record) {
			record.beginEdit();
			record.data['bigTrade.quantityIntended'] = 0;
			record.data['miniTrade.quantityIntended'] = 0;
			record.endEdit();
		}, this);
		gp.getView().refresh();
		gp.markModified();
	},

	addAdditionalReviewToolbarButtonsRightSide: function(gridPanel, toolbar) {
		this.addToolbarCommissionApplyButton(gridPanel, toolbar);
		this.addToolbarFillSplitButton(gridPanel, toolbar);

		toolbar.add('-', {
			text: 'View Allocation',
			tooltip: 'Generates an email with an allocation table and attached CSV file',
			iconCls: 'export',
			scope: this,
			handler: function() {
				let csvContent = 'Broker Company,Holding Account,Security,Trade,Security,Trade';
				let allocationTable = '<table><thead><tr><th>Broker Company</th><th>Holding Account</th><th>Security</th><th>Trade</th><th>Security</th><th>Trade</th></tr></thead>';
				gridPanel.getStore().each(function(record) {
					const clearingBroker = TCG.getValue('holdingAccount.issuingCompany.name', record.json),
						holdingNumber = TCG.getValue('holdingAccount.number', record.json),
						bigSecurity = TCG.getValue('bigTrade.investmentSecurity.symbol', record.json),
						bigTrade = (TCG.isTrue(TCG.getValue('bigTrade.buy', record.json)) ? 'BUY' : 'SELL') + ': ' + TCG.getValue('bigTrade.quantityIntended', record.json),
						miniSecurity = TCG.getValue('miniTrade.investmentSecurity.symbol', record.json),
						miniTrade = (TCG.isTrue(TCG.getValue('miniTrade.buy', record.json)) ? 'BUY' : 'SELL') + ': ' + TCG.getValue('miniTrade.quantityIntended', record.json);

					csvContent += '\n\"' + clearingBroker + '\",\"' + holdingNumber + '\",\"' + bigSecurity + '\",\"' + bigTrade + '\",\"' + miniSecurity + '\",\"' + miniTrade + '\"';

					allocationTable += '<tr><td>' + clearingBroker + '</td><td>' + holdingNumber + '</td><td>' + bigSecurity + '</td><td>' + bigTrade + '</td><td>' + miniSecurity + '</td><td>' + miniTrade + '</td></tr>';
				});

				const htmlContent = '<html><head><style>table,td,p{font-family:arial,sans-serif;font-size:14px;}td{border:1px solid black;border-collapse:collapse;padding:5px}</style></head><body>' + allocationTable + '</body></html>';

				//each part needs an empty line between itself and the previous
				const emailBody = '--part\nContent-Type:text/csv\nContent-Disposition:attachment;filename=\"ParametricFungeAllocation.csv\"\n\n' + csvContent
					+ '\n\n--part\nContent-Type:text/html\n\n' + htmlContent;
				TCG.generateEmailFile('Parametric Funge Trade Allocation - ' + TCG.getParentFormPanel(gridPanel).getForm().findField('tradeDate').value, 'multipart/mixed;boundary=part', emailBody, 'ParametricFungeTradeAllocation.eml');
			}
		});
	},

	addToolbarCommissionApplyButton: function(gridPanel, toolBar) {
		const gp = gridPanel;
		toolBar.add({xtype: 'label', html: 'Commission:&nbsp;', qtip: 'Commission Per Unit override to apply to selected trades when \'Apply Commission\' is clicked.'});
		toolBar.add({xtype: 'pricefield', name: 'commissionPerUnitOverride', width: 70});
		toolBar.add({
			text: 'Apply Commission',
			tooltip: 'Apply the specified Commission Per Unit Override to the Selected Trades. Leaving the value blank will remove the Commission Per Unit Override for the Selected Trades',
			iconCls: 'add',
			scope: this,
			handler: function() {
				const commissionPerUnitOverride = TCG.getChildByName(toolBar, 'commissionPerUnitOverride').getValue();
				gp.applyCommissionOverrideToSelectedTrades(commissionPerUnitOverride);
				gp.executeAction('SAVE_TRADE');
			}
		});
	},

	addToolbarFillPriceField: function(toolBar) {
		toolBar.add({xtype: 'label', html: '&nbsp;Fill Price:&nbsp;', qtip: 'Fill price to apply to selected trades when \'Fill Trades\' or \'Fill & Execute Trades\' is clicked.'});
		toolBar.add({xtype: 'pricefield', name: 'fillPrice', width: 70});
	},

	addToolbarFillSplitButton: function(gridPanel, toolBar) {
		const gp = gridPanel;
		toolBar.add('-');
		gp.addToolbarFillPriceField.call(gp, toolBar);
		toolBar.add({
			text: 'Fill Trades',
			xtype: 'splitbutton',
			tooltip: 'Create Fills for Selected Trades with their Fill Price & Quantity. May choose \'Fill Trades\' or \'Fill & Execute Trades\'',
			iconCls: 'add',
			scope: this,
			handler: function() {
				gp.applyPriceToSelectedTrades(toolBar);
				gp.executeAction('FILL');
			},
			menu: new Ext.menu.Menu({
				items: [
					{
						text: 'Fill Trades',
						tooltip: 'Create Fills for Selected Trades and their Fill Price & Quantity.',
						iconCls: 'add',
						scope: this,
						handler: function() {
							gp.applyPriceToSelectedTrades(toolBar);
							gp.executeAction('FILL');
						}
					},
					{
						text: 'Fill & Execute Trades',
						tooltip: 'Create Fills for Selected Trades and their Fill Price & Quantity. Execute Trades after fills are created.',
						iconCls: 'add',
						scope: this,
						handler: function() {
							gp.applyPriceToSelectedTrades(toolBar);
							gp.executeAction('FILL_EXECUTE');
						}
					}
				]
			})
		});
	},

	applyPriceToSelectedTrades: function(toolBar) {
		const price = TCG.getChildByName(toolBar, 'fillPrice').getValue();
		const store = this.store;
		if (TCG.isNotBlank(price) && store.modified) {
			store.each(function(record) {
				if (record.modified) {
					record.set('fillPrice', price);
				}
			}, this);
			this.markModified();
		}
	},

	applyCommissionOverrideToSelectedTrades: function(commissionPerUnitOverride) {
		const store = this.store;
		if (TCG.isNotNull(commissionPerUnitOverride) && store.modified) {
			store.each(function(record) {
				if (record.modified) {
					record.set('commissionPerUnit', commissionPerUnitOverride);
				}
			}, this);
			this.markModified();
		}
	},

	handleCellDoubleClick: function(grid, rowIndex, cellIndex, event) {
		const row = grid.store.data.items[rowIndex];
		const columnName = grid.getColumnModel().getColumnById(cellIndex).dataIndex;
		if (columnName && (columnName === 'bigTrade.investmentSecurity.symbol' || columnName === 'miniTrade.investmentSecurity.symbol')) {
			const securityId = TCG.getValue(columnName.startsWith('big') ? 'bigTrade.investmentSecurity.id' : 'miniTrade.investmentSecurity.id', row.json);
			if (TCG.isNotBlank(securityId)) {
				TCG.createComponent('Clifton.investment.instrument.SecurityWindow', {
					id: TCG.getComponentId('Clifton.investment.instrument.SecurityWindow', securityId),
					params: {id: securityId},
					openerCt: grid
				});
			}
		}
		else if (columnName === 'bigQuantity' || columnName === 'miniQuantity') {
			const filters = [];
			filters.push({
				field: 'clientInvestmentAccountId',
				comparison: 'EQUALS',
				value: TCG.getValue('clientAccount.id', row.json)
			});
			filters.push({
				field: 'holdingInvestmentAccountId',
				comparison: 'EQUALS',
				value: TCG.getValue('holdingAccount.id', row.json)
			});
			filters.push({
				field: 'investmentSecurityId',
				comparison: 'EQUALS',
				value: TCG.getValue(columnName === 'bigQuantity' ? 'bigTrade.investmentSecurity.id' : 'miniTrade.investmentSecurity.id', row.json)
			});
			const nextDay = new Date(TCG.getParentFormPanel(grid).getForm().findField('tradeDate').getValue()).add(Date.DAY, 1).format('m/d/Y');
			filters.push({
				field: 'transactionDate',
				comparison: 'LESS_THAN',
				value: nextDay
			});
			TCG.createComponent('Clifton.accounting.gl.TransactionListWindow', {
				defaultData: filters,
				openerCt: grid
			});
		}
		else if (this.tradeEntrySupported === false) {
			let tid = '';
			if (columnName && columnName.startsWith('bigTrade')) {
				tid = TCG.getValue('bigTrade.id', row.json);
			}
			else if (columnName && columnName.startsWith('miniTrade')) {
				tid = TCG.getValue('miniTrade.id', row.json);
			}
			if (TCG.isNotBlank(tid)) {
				TCG.createComponent('Clifton.trade.TradeWindow', {
					id: TCG.getComponentId('Clifton.trade.TradeWindow', tid),
					params: {id: tid},
					openerCt: grid
				});
			}
		}
	},

	handleAfterEditEventFromExt: function(event) {
		if (event.originalValue !== event.value && !TCG.isBlank(event.value)) {
			event.record.beginEdit();
			event.record.data['includeTrade'] = true;

			if (event.field === 'bigTrade.quantityIntended') {
				event.record.data['miniTrade.quantityIntended'] = this.calculateMiniTradeQuantity(event.record);
			}
			event.record.endEdit();
		}
	},

	calculateMiniTradeQuantity: function(record) {
		return record.data['bigTrade.quantityIntended'] * (TCG.getValue('bigTrade.investmentSecurity.priceMultiplier', record.json) / TCG.getValue('miniTrade.investmentSecurity.priceMultiplier', record.json));
	},

	// Columns that have tradeEntry = true/false will be included based on if this is tradeEntry or tradeReview - anything without that property will always be included
	customColumns: [
		// Maturing
		{header: 'Big Security', dataIndex: 'bigTrade.investmentSecurity.symbol', idDataIndex: 'bigTrade.investmentSecurity.id', width: 100, submitValue: false},
		{header: 'Mini Security', dataIndex: 'miniTrade.investmentSecurity.symbol', idDataIndex: 'miniTrade.investmentSecurity.id', width: 100, submitValue: false},
		{header: 'Big Qty', dataIndex: 'bigQuantity', width: 70, type: 'int', submitValue: false, tooltip: 'Quantity of the Big Security currently held'},
		{header: 'Mini Qty', dataIndex: 'miniQuantity', width: 70, type: 'int', submitValue: false, tooltip: 'Quantity of the Mini Security currently held'},
		{header: 'Big Buy', dataIndex: 'bigTrade.buy', width: 50, type: 'boolean', hidden: true},
		{
			header: 'Big Qty Offset', width: 100, dataIndex: 'bigTrade.quantityIntended', tradeEntry: true, type: 'float', useNull: false, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity of the currently held position on the Big Security to trade/offset',
			editor: {xtype: 'floatfield', allowBlank: false, minValue: 0},
			renderer: function(value, metaData, record) {
				const buy = record.data['bigTrade.buy'],
					absQuantityToOffset = Math.abs(record.data['bigQuantity']);
				metaData.attr = 'style="BACKGROUND-COLOR: ' + (TCG.isTrue(buy) ? '#f0f0ff' : '#fff0f0') + ';"';
				if (value > absQuantityToOffset) {
					metaData.css = 'ruleViolation';
					metaData.attr = 'qtip="Value entered exceeds quantity held.  Please enter a value no greater than ' + Ext.util.Format.number(absQuantityToOffset, '0,000') + ' contracts."';
				}
				return TCG.renderAdjustedAmount(value, (absQuantityToOffset !== value), absQuantityToOffset, '0,000');
			},
			summaryRenderer: function(value) {
				return TCG.renderAmount(value, false, '0,000');
			}
		},
		{
			header: 'Big Qty Offset', width: 100, dataIndex: 'bigTrade.quantityIntended', tradeEntry: false, type: 'float', useNull: false, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity of the currently held position on the Big Security to trade/offset',
			renderer: function(value, metaData, record) {
				const buy = record.data['bigTrade.buy'],
					absQuantityToOffset = Math.abs(record.data['bigQuantity']);
				metaData.attr = 'style="BACKGROUND-COLOR: ' + (TCG.isTrue(buy) ? '#f0f0ff' : '#fff0f0') + ';"';
				return TCG.renderAdjustedAmount(value, (absQuantityToOffset !== value), absQuantityToOffset, '0,000');
			},
			summaryRenderer: function(value) {
				return TCG.renderAmount(value, false, '0,000');
			}
		},
		{header: 'Mini Buy', dataIndex: 'miniTrade.buy', width: 50, type: 'boolean', hidden: true},
		{
			header: 'Mini Qty Offset', width: 100, dataIndex: 'miniTrade.quantityIntended', tradeEntry: true, type: 'float', useNull: false, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity of the currently held position on the Mini Security to trade/offset. This value is calculated from the Big Qty Offset using the price multipliers of the big and mini securities (e.g. Big Qty Offset * (Big Security Price Multiplier / Mini Security Price Multiplier)).',
			renderer: function(value, metaData, record) {
				const buy = record.data['miniTrade.buy'],
					absQuantityToOffset = Math.abs(record.data['miniQuantity']);
				metaData.attr = 'style="BACKGROUND-COLOR: ' + (TCG.isTrue(buy) ? '#f0f0ff' : '#fff0f0') + ';"';

				if (value > absQuantityToOffset) {
					metaData.css = 'ruleViolation';
					metaData.attr = 'qtip="Value entered exceeds quantity held.  Please enter a value no greater than ' + Ext.util.Format.number(absQuantityToOffset, '0,000') + ' contracts."';
				}
				return TCG.renderAdjustedAmount(value, (absQuantityToOffset !== value), absQuantityToOffset, '0,000');
			},
			summaryRenderer: function(value) {
				return TCG.renderAmount(value, false, '0,000');
			}
		},
		{
			header: 'Mini Qty Offset', width: 100, dataIndex: 'miniTrade.quantityIntended', tradeEntry: false, type: 'float', useNull: false, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Quantity of the currently held position on the Mini Security to trade/offset. This value is calculated from the Big Qty Offset using the price multipliers of the big and mini securities (e.g. Big Qty Offset * (Big Security Price Multiplier / Mini Security Price Multiplier)).',
			renderer: function(value, metaData, record) {
				const buy = record.data['miniTrade.buy'],
					absQuantityToOffset = Math.abs(record.data['miniQuantity']);
				metaData.attr = 'style="BACKGROUND-COLOR: ' + (TCG.isTrue(buy) ? '#f0f0ff' : '#fff0f0') + ';"';
				return TCG.renderAdjustedAmount(value, (absQuantityToOffset !== value), absQuantityToOffset, '0,000');
			},
			summaryRenderer: function(value) {
				return TCG.renderAmount(value, false, '0,000');
			}
		},
		{
			header: 'Price', width: 60, dataIndex: 'bigTrade.averageUnitPrice', type: 'float', useNull: false, tradeEntry: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Average unit price used for both the Big and Mini offset trades. The price is the previous close price for the Big Security.',
			editor: {
				xtype: 'pricefield', allowBlank: true, minValue: 0,
				afterRender: function() {
					TCG.form.PriceField.superclass.afterRender.apply(this, arguments);

					const gridEditor = this.gridEditor;
					if (gridEditor) {
						if (!this.drillDownMenu) {
							this.drillDownMenu = new Ext.menu.Menu({
								items: [
									{
										text: 'Copy To All Rows With Same Security', iconCls: 'copy', scope: this,
										handler: function() {
											const grid = gridEditor.containerGrid,
												valueFieldName = grid.getColumnModel().getColumnById(gridEditor.col).dataIndex,
												modifiedRecord = gridEditor.record,
												securitySymbol = modifiedRecord.data['bigTrade.investmentSecurity.symbol'],
												price = this.parseValue(this.getValue());
											grid.store.each(function(record) {
												if (record.data['bigTrade.investmentSecurity.symbol'] === securitySymbol
													&& record.data[valueFieldName] !== price) {
													record.set(valueFieldName, price);
												}
											});
											// Refresh Grid So Updates Are Visible
											grid.getView().refresh(false);
											grid.markModified();
										}
									}
								]
							});
						}
					}
					if (this.drillDownMenu) {
						const el = this.getEl();
						el.dom.setAttribute('ext:qtip', 'Right click to display the context menu with additional features.');
						el.on('contextmenu', function(e, target) {
							e.preventDefault();
							// disable detail menu if no selection was made
							const valueExists = TCG.isNotBlank(this.getValue());

							for (let i = 0; i < this.drillDownMenu.items.length; i++) {
								const item = this.drillDownMenu.items.get(i);
								if (item.text.includes('Copy To All Rows')) {
									if (TCG.isTrue(valueExists)) {
										item.enable();
									}
									else {
										item.disable();
									}
								}
							}
							this.drillDownMenu.showAt(e.getXY());
						}, this);
					}
				}
			}
		},
		{
			header: 'Price', width: 70, dataIndex: 'bigTrade.averageUnitPrice', type: 'float', useNull: false, tradeEntry: false, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Average unit price used for both the Big and Mini offset trades. The price defaults to the previous close price for the Big Security and can be overridden.'
		},
		{
			header: 'Commission', width: 90, dataIndex: 'bigTrade.commissionPerUnit', type: 'float', useNull: true, tradeEntry: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Commission per unit override used for both the Big and Mini offset trades',
			editor: {
				xtype: 'pricefield', allowBlank: true, minValue: 0,
				afterRender: function() {
					TCG.form.PriceField.superclass.afterRender.apply(this, arguments);

					const gridEditor = this.gridEditor;
					if (gridEditor) {
						if (!this.drillDownMenu) {
							this.drillDownMenu = new Ext.menu.Menu({
								items: [
									{
										text: 'Copy To All Rows With Same Security', iconCls: 'copy', scope: this,
										handler: function() {
											const grid = gridEditor.containerGrid,
												valueFieldName = grid.getColumnModel().getColumnById(gridEditor.col).dataIndex,
												modifiedRecord = gridEditor.record,
												securitySymbol = modifiedRecord.data['bigTrade.investmentSecurity.symbol'],
												price = this.parseValue(this.getValue());
											grid.store.each(function(record) {
												if (record.data['bigTrade.investmentSecurity.symbol'] === securitySymbol
													&& record.data[valueFieldName] !== price) {
													record.set(valueFieldName, price);
												}
											});
											// Refresh Grid So Updates Are Visible
											grid.getView().refresh(false);
											grid.markModified();
										}
									}
								]
							});
						}
					}
					if (this.drillDownMenu) {
						const el = this.getEl();
						el.dom.setAttribute('ext:qtip', 'Right click to display the context menu with additional features.');
						el.on('contextmenu', function(e, target) {
							e.preventDefault();
							this.drillDownMenu.showAt(e.getXY());
						}, this);
					}
				}
			}
		},
		{
			header: 'Trade Destination', width: 100, dataIndex: 'bigTrade.tradeDestination.name', idDataIndex: 'bigTrade.tradeDestination.id', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tradeEntry: true, useNull: false, tooltip: 'Trade Destination used for both the Big and Mini offset trades',
			editor: {
				xtype: 'combo', displayField: 'name', tooltipField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false,
				beforequery: function(queryEvent) {
					const form = TCG.getParentFormPanel(queryEvent.combo.gridEditor.containerGrid).getForm();
					queryEvent.combo.store.baseParams = {
						activeOnDate: form.findField('tradeDate').value,
						tradeTypeId: form.findField('tradeType.id').getValue()

					};
				}
			}
		},
		{
			header: 'Trade Destination', tradeEntry: false, width: 100, dataIndex: 'bigTrade.tradeDestination.name', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Trade Destination used for both the Big and Mini offset trades'
		},
		{
			header: 'Executing Broker', width: 130, dataIndex: 'bigTrade.executingBrokerCompany.name', idDataIndex: 'bigTrade.executingBrokerCompany.id', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tradeEntry: true, useNull: false, tooltip: 'Trade Executing Broker used for both the Big and Mini offset trades. The default value is looked up by Trade Destination, active on Trade Date, and Holding Account Issuing Company.',
			editor: {
				xtype: 'combo', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json', tooltipField: 'label', detailPageClass: 'Clifton.business.company.CompanyWindow',
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					this.resetStore();
					const editor = queryEvent.combo.gridEditor;
					const record = editor.record;
					const grid = editor.containerGrid;
					const form = TCG.getParentFormPanel(grid).getForm();
					queryEvent.combo.store.baseParams = {
						tradeDestinationId: TCG.getValue('bigTrade.tradeDestination.id', record.json),
						tradeTypeId: form.findField('tradeType.id').getValue(),
						activeOnDate: form.findField('tradeDate').value,
						executingBrokerCompanyId: TCG.getValue('holdingAccount.issuingCompany.id', record.json)
					};
				}
			}
		},
		{
			header: 'Executing Broker', tradeEntry: false, width: 130, dataIndex: 'bigTrade.executingBrokerCompany.name', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Trade Executing Broker used for both the Big and Mini offset trades'
		},
		// columns for commissions and fills
		{
			header: 'Commission', width: 60, dataIndex: 'commissionPerUnit', hidden: true, type: 'float', useNull: true, tradeEntry: false, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Commission per unit to be applied to both the Big and Mini trades for this detail row.'
		},
		{
			header: 'Fill Price', width: 60, dataIndex: 'fillPrice', hidden: true, type: 'float', useNull: true, tradeEntry: false, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Price to be used for a single fill for each the Big and Mini trades for this detail row.'
		}
	],

	plugins: {
		ptype: 'gridsummary'
	}
});
Ext.reg('funge-detail-form-grid', Clifton.trade.group.FungeDetailFormGrid);
