Clifton.trade.group.TradeGroupNoteDragAndDropPanel = {
	xtype: 'panel',
	layout: 'column',
	items: [
		{
			columnWidth: .30,
			layout: 'form',

			items: [
				{
					xtype: 'drag-drop-container',
					message: '<b>Client Trade Direction</b>',
					tooltipMessage: 'Drag and drop file here to create <b>Client Trade Direction</b> note for the following trades and attach this file to it.',
					allowMultiple: false,
					bindToFormLoad: true,
					saveUrl: 'tradeNoteForTradeGroupUpload.json',
					getParams: function(formPanel) {
						const id = TCG.getValue('id', formPanel.getForm().formValues);
						return {
							tradeGroupId: id,
							noteTypeName: 'Client Trade Direction'
						};
					}
				}
			]
		},
		{columnWidth: .03, items: [{xtype: 'label', html: '&nbsp;'}]},
		{
			columnWidth: .30,
			layout: 'form',
			items: [
				{
					xtype: 'drag-drop-container',
					message: '<b>Internal Trade Approval</b>',
					tooltipMessage: 'Drag and drop file here to create <b>Internal Trade Approval</b> note for the following trades and attach this file to it.',
					allowMultiple: false,
					saveUrl: 'tradeNoteForTradeGroupUpload.json',
					bindToFormLoad: true,
					getParams: function(formPanel) {
						const id = TCG.getValue('id', formPanel.getForm().formValues);
						return {
							tradeGroupId: id,
							noteTypeName: 'Internal Trade Approval'
						};
					}
				}
			]
		}
		,
		{columnWidth: .03, items: [{xtype: 'label', html: '&nbsp;'}]},
		{
			columnWidth: .30,
			layout: 'form',
			items: [
				{
					xtype: 'drag-drop-container',
					message: '<b>Other</b>',
					tooltipMessage: 'Drag and drop file here to create note for the following trades and attach this file to it. Note type selection will be required after the file is dropped.',
					allowMultiple: false,
					popupComponentName: 'Clifton.trade.group.TradeGroupNoteUploadWindow',
					bindToFormLoad: true,
					getParams: function(formPanel) {
						const id = TCG.getValue('id', formPanel.getForm().formValues);
						return {
							tradeGroupId: id
						};
					}
				}
			]
		}
	]
};
