Clifton.trade.group.TradeGroupNoteUploadWindow = Ext.extend(TCG.file.DragAndDropPopupWindow, {
	title: 'Trade Group Note with Attachment Window',
	iconCls: 'attach',
	height: 200,
	width: 400,

	okButtonTooltip: 'Create Note with Upload File',

	saveUrl: 'tradeNoteForTradeGroupUpload.json',
	formItems: [
		{name: 'tradeGroupId', hidden: true},
		{fieldLabel: 'Note Type', name: 'noteTypeName', xtype: 'combo', valueField: 'name', url: 'systemNoteTypeListFind.json?excludeAttachmentSupportedType=NONE&tableName=Trade&blankNoteAllowed=true&linkToMultipleAllowed=true'}
	]
});
