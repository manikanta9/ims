TCG.use('Clifton.trade.SwapCDSTradeWindow');

Clifton.trade.SwapCDSTradeClearedWindow = Ext.extend(Clifton.trade.SwapCDSTradeWindow, {
	centralClearing: true,
	executingBrokerNotRequired: true
});
