// common section used by single and multi-client entry screens
Clifton.trade.DefaultTradeForm = Ext.extend(Clifton.trade.BaseTradeForm, {
	labelWidth: 120,
	submitEmptyText: true,

	getDefaultData: function(win) {
		const f = this.getForm();
		const fp = this;
		const dd = Ext.apply(win.defaultData || {}, {});
		if (win.defaultDataIsReal) {
			return dd;
		}
		const tradeTypeName = win.tradeType;
		const tradeDestination = TCG.isNotNull(dd.tradeDestination) ? dd.tradeDestination
			: TCG.data.getDataPromiseUsingCaching('tradeDestinationByName.json?name=Manual', fp, 'trade.tradeDestination.Manual');

		return Promise.resolve(tradeDestination)
			.then(function(tradeDestination) {
				dd.tradeDestination = tradeDestination;
				return TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + tradeTypeName, fp, 'trade.type.' + tradeTypeName);
			})
			.then(function(tradeType) {
				dd.tradeType = tradeType;
				if (TCG.isTrue(tradeType.collateralSupported)) {
					f.findField('collateralTrade').show();
				}

				if (TCG.isTrue(tradeType.tradeExecutionTypeSupported)) {
					fp.showField('tradeExecutionType.name');
					fp.showField('limitPrice');
				}

				return TCG.data.getDataPromiseUsingCaching('tradeDestinationMappingByCommand.json?defaultDestinationMapping=true&tradeTypeId=' + tradeType.id + '&activeOnDate=' + new Date().format('m/d/Y'), fp, 'trade.type.destinationMappingDefault.' + tradeTypeName + '.' + new Date().format('m/d/Y'));
			})
			.then(function(destinationMapping) {
				f.findField('tradeDestination.id').show();

				if (destinationMapping) {
					if (destinationMapping.tradeDestination) {
						dd.tradeDestination = destinationMapping.tradeDestination;
					}
					if (destinationMapping.executingBrokerCompany && TCG.isBlank(dd.executingBrokerCompany)) {
						dd.executingBrokerCompany = destinationMapping.executingBrokerCompany;
					}
				}
				dd.traderUser = TCG.getCurrentUser();
				if (TCG.isNull(dd.tradeDate)) {
					dd.tradeDate = new Date().format('Y-m-d 00:00:00');
				}
				return dd;
			});
	},

	prepareDefaultData: function(dd) {
		const type = TCG.getValue('tradeType.investmentType.name', dd);
		if (type === 'Options' || type === 'Swaptions') {
			const notionalField = this.getForm().findField('accountingNotional');
			if (notionalField) {
				notionalField.setFieldLabel('Premium:');
			}
		}
		this.processNovationFieldSetVisibility(type, TCG.getValue('investmentSecurity.instrument.hierarchy.otc'));

		return dd;
	},

	updateExecutingBrokerCompanyToIssuingCompany: function(holdingAccountIssuingCompanyId) {
		const fp = this;
		const f = fp.getForm();
		// update the executing broker default
		const tradeDate = f.findField('tradeDate').getValue().format('m/d/Y');
		const params = {
			tradeTypeId: f.findField('tradeType.id').getValue(),
			securityId: f.findField('investmentSecurity.id').getValue(),
			tradeDestinationId: f.findField('tradeDestination.id').getValue(),
			tradeExecutionTypeId: f.findField('tradeExecutionType.id').getValue(),
			executingBrokerCompanyId: holdingAccountIssuingCompanyId,
			clientInvestmentAccountId: f.findField('clientInvestmentAccount.id').getValue(),
			activeOnDate: tradeDate
		};
		const executingBrokerCompanyField = f.findField('executingBrokerCompany.label');
		TCG.data.getDataPromise('tradeExecutingBrokerCompanyListFind.json', this, {params: params})
			.then(function(data) {
				// try to match the issuing broker to the list of allowed brokers
				if (data && data.rows && data.rows.length > 0) {
					for (const brokerCompany of data.rows) {
						if (brokerCompany.id === holdingAccountIssuingCompanyId && !TCG.isTrue(fp.executingBrokerSelected)) {
							executingBrokerCompanyField.setValue({value: brokerCompany.id, text: brokerCompany.label});
							return;
						}
					}
				}
			});

	},

	// Checks the allowed broker list and the previously set broker.  If a broker was originally set, and is still an allowed broker, it is
	// kept in the ExecutingBroker combo.  If that broker is no longer allowed, the combo field is cleared.
	updateExecutingBrokerCompanyAfterClientAccountChanged: function(tradeDestinationId) {
		const fp = this;
		const f = fp.getForm();
		const executingBrokerCompanyField = f.findField('executingBrokerCompany.label');
		if (executingBrokerCompanyField.isVisible() && TCG.isNotBlank(f.findField('executingBrokerCompany.id').getValue())) {
			const localTradeDestinationId = tradeDestinationId || f.findField('tradeDestination.name').getValue();
			if (localTradeDestinationId) {
				const tradeDate = f.findField('tradeDate').getValue().format('m/d/Y');
				const executingBrokerCompanyId = executingBrokerCompanyField.getValue();
				const params = {
					tradeTypeId: f.findField('tradeType.id').getValue(),
					securityId: f.findField('investmentSecurity.id').getValue(),
					tradeDestinationId: localTradeDestinationId,
					tradeExecutionTypeId: f.findField('tradeExecutionType.id').getValue(),
					clientInvestmentAccountId: f.findField('clientInvestmentAccount.id').getValue(),
					activeOnDate: tradeDate
				};
				const loader = new TCG.data.JsonLoader({
					waitTarget: fp,
					params: params,
					onLoad: function(data, conf) {
						// try to match the issuing broker to the list of allowed brokers
						for (const brokerCompany of data.rows) {
							if (brokerCompany.id === executingBrokerCompanyId) {
								return;
							}
						}

						// match for previously set broker was not found -- clear the field
						executingBrokerCompanyField.clearValue();
						fp.executingBrokerSelected = false;
					}
				});
				loader.load('tradeExecutingBrokerCompanyListFind.json');
			}
		}
	},

	updateExecutingBrokerCompanyForTradeDestination: async function(tradeDestinationId) {
		const fp = this;
		const f = fp.getForm();
		const executingBrokerCompanyField = f.findField('executingBrokerCompany.label');
		if (executingBrokerCompanyField.isVisible()) {
			const localTradeDestinationId = tradeDestinationId || f.findField('tradeDestination.name').getValue();
			if (localTradeDestinationId) {
				const tradeDate = f.findField('tradeDate').getValue().format('m/d/Y');
				const executingBrokerCompanyId = executingBrokerCompanyField.getValue();
				let accountId = '';

				const clientInvestmentAccountField = f.findField('clientInvestmentAccount.id');
				if (TCG.isNotBlank(clientInvestmentAccountField)) {
					accountId = f.findField('clientInvestmentAccount.id').getValue();
				}

				const params = {
					clientInvestmentAccountId: accountId,
					tradeTypeId: f.findField('tradeType.id').getValue(),
					securityId: f.findField('investmentSecurity.id').getValue(),
					tradeDestinationId: localTradeDestinationId,
					tradeExecutionTypeId: f.findField('tradeExecutionType.id').getValue(),
					activeOnDate: tradeDate
				};
				const executingBrokerList = await TCG.data.getDataPromise('tradeExecutingBrokerCompanyListFind.json', this, {params: params});

				if (executingBrokerList.length < 1) {
					// no brokers, clear value and return
					executingBrokerCompanyField.clearValue();
					fp.executingBrokerSelected = false;
					return;
				}

				let execBrokerId = executingBrokerCompanyId;
				const securityWithOtcDetails = await fp.getSecurityWithOtcDetails(params.securityId);
				if (TCG.isTrue(securityWithOtcDetails.instrument.hierarchy.otc)) {
					const holdingAccount = fp.getFormValue('holdingInvestmentAccount');
					if (TCG.isNotBlank(holdingAccount) && TCG.isNotBlank(holdingAccount.issuingCompany)) {
						execBrokerId = holdingAccount.issuingCompany.id;
					}
					else if (TCG.isNotBlank(securityWithOtcDetails.businessCompany.id)) {
						execBrokerId = securityWithOtcDetails.businessCompany.id;
					}
				}

				// try to match the issuing broker to the list of allowed brokers
				for (const brokerCompany of executingBrokerList.rows) {
					if (brokerCompany.id === execBrokerId) {
						executingBrokerCompanyField.setValue({value: brokerCompany.id, text: brokerCompany.label});
						return;
					}
				}

				// default to first broker on list
				executingBrokerCompanyField.setValue({value: executingBrokerList.rows[0].id, text: executingBrokerList.rows[0].label});
			}
		}
	},

	updateTradeDestination: function(securityId) {
		const fp = this;
		const f = fp.getForm();
		const dst = f.findField('tradeDestination.name');
		const executingBrokerCompanyField = f.findField('executingBrokerCompany.label');
		// Only default if trade destination or executing broker are not selected
		if (dst.isVisible() && (!TCG.isTrue(fp.destinationSelected) || !TCG.isTrue(fp.executingBrokerSelected))) {
			const localSecurityId = securityId || f.findField('investmentSecurity.id').getValue();
			if (localSecurityId) {
				const tradeDate = f.findField('tradeDate').getValue().format('m/d/Y');
				const params = {
					securityId: localSecurityId,
					activeOnDate: tradeDate
				};
				// add criterion of selected destination or executing broker; if both were selected we shouldn't be here to default
				if (TCG.isTrue(fp.destinationSelected)) {
					params['tradeDestinationId'] = dst.getValue();
				}
				else if (TCG.isTrue(fp.executingBrokerSelected)) {
					params['executingBrokerCompanyId'] = executingBrokerCompanyField.getValue();
				}
				const loader = new TCG.data.JsonLoader({
					waitTarget: fp,
					params: params,
					onLoad: function(rec, conf) {
						// Update the destination only if it was not previously selected by the user
						if (!TCG.isTrue(fp.destinationSelected) && rec && rec.tradeDestination) {
							dst.setValue({value: rec.tradeDestination.id, text: rec.tradeDestination.name});
							f.formValues.tradeDestination = rec.tradeDestination; // needs this in order to find the record when tabbing out when the store isn't initialized
						}
						fp.updateExecutingBrokerList();
						fp.updateExecutingBrokerCompanyForTradeDestination(rec.tradeDestination.id);
						fp.updateTimeInForcePanelVisibility(TCG.isTrue(rec.tradeDestination.enableTimeInForceSelection));
					}
				});
				loader.load('tradeDestinationMappingByCommand.json');
			}
		}
	},

	updateExecutingBrokerList: function() {
		const fp = this;
		const f = fp.getForm();
		const executingBrokerCompanyField = f.findField('executingBrokerCompany.label');
		executingBrokerCompanyField.resetStore();
	},

	updateTimeInForcePanelVisibility: function(isVisible) {
		const fp = this;
		const timeInForcePanel = TCG.getChildByName(fp, 'timeInForcePanel');
		if (TCG.isTrue(isVisible)) {
			timeInForcePanel.show();
		}
		else {
			timeInForcePanel.hide();
			const tradeTimeInForceTypeComponent = TCG.getChildByName(fp, 'tradeTimeInForceType.label');
			if (tradeTimeInForceTypeComponent && tradeTimeInForceTypeComponent.clearValue && TCG.isNotBlank(tradeTimeInForceTypeComponent)) {
				tradeTimeInForceTypeComponent.clearValue();
			}
			fp.updateExpireDateComponentState(false);
		}
	},

	processNovationFieldSetVisibility: function(tradeTypeName, otc) {
		if (TCG.isNotBlank(tradeTypeName)) {
			let visibility = false;
			if (tradeTypeName.startsWith('Swaptions')) {
				visibility = true;
			}
			else if (tradeTypeName === 'Options') {
				let localOtc = otc;
				if (TCG.isBlank(localOtc)) {
					localOtc = this.getFormValue('investmentSecurity.instrument.hierarchy.otc');
				}
				visibility = TCG.isTrue(localOtc);
			}
			const novationTradeFields = TCG.getChildByName(this, 'novationFieldset');
			if (novationTradeFields) {
				novationTradeFields.setVisible(visibility);
			}
		}
	},

	updateExpireDateComponentState: function(expirationDateRequired) {
		const fp = this;
		const expireDateControl = TCG.getChildByName(fp, 'timeInForceExpirationDate');
		if (TCG.isTrue(expirationDateRequired)) {
			expireDateControl.setDisabled(false);
			expireDateControl.allowBlank = false;
		}
		else {
			expireDateControl.setValue(null);
			expireDateControl.allowBlank = true;
			expireDateControl.setDisabled(true);
		}
	},

	commonEntryItems: [{
		xtype: 'panel',
		layout: 'column',
		labelWidth: 120,
		items: [
			{
				columnWidth: .69,
				layout: 'form',
				defaults: {xtype: 'textfield'},
				items: [
					{
						fieldLabel: 'Buy/Sell', name: 'openCloseType.label', hiddenName: 'openCloseType.id', displayField: 'label', xtype: 'combo', url: 'tradeOpenCloseTypeListFind.json', allowBlank: false,
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const fp = combo.getParentForm();
								combo.store.baseParams = {
									tradeTypeId: fp.getFormValue('tradeType.id')
								};
							}
						}
					},
					{fieldLabel: 'Quantity', name: 'quantityIntended', xtype: 'spinnerfield', minValue: 0.00000001, maxValue: 10000000000, allowBlank: false, allowDecimals: true, decimalPrecision: 10},
					{fieldLabel: 'OTC', name: 'investmentSecurity.instrument.hierarchy.otc', xtype: 'hidden', submitValue: false}, // Used for Holding Account Lookup
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {layout: 'form'},
						items: [
							{
								columnWidth: .62,
								items: [
									{
										fieldLabel: 'Security', name: 'investmentSecurity.label', width: 250, allowBlank: false, displayField: 'label', hiddenName: 'investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?tradingDisallowed=false', queryParam: 'searchPatternUsingBeginsWith',
										requestedProps: 'instrument.tradingCurrency.id|instrument.tradingCurrency.symbol|instrument.hierarchy.otc',
										listeners: {
											beforequery: function(queryEvent) {
												const combo = queryEvent.combo;
												const fp = combo.getParentForm();
												const f = fp.getForm();
												combo.store.setBaseParam('investmentTypeId', fp.getFormValue('tradeType.investmentType.id'));
												combo.store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
												const groupId = fp.getFormValue('tradeType.investmentGroup.id');
												if (groupId) {
													combo.store.setBaseParam('investmentGroupId', groupId);
												}
											},
											select: function(combo, record, index) {
												const curr = TCG.getValue('settlementCurrency', record.json) || TCG.getValue('instrument.tradingCurrency', record.json);
												const fp = combo.getParentForm();
												const form = fp.getForm();
												form.findField('payingSecurity.symbol').setValue(curr.symbol);
												form.findField('payingSecurity.id').setValue(curr.id);

												const otc = record.json.instrument.hierarchy.otc;
												form.findField('investmentSecurity.instrument.hierarchy.otc').setValue(otc);
												//add or remove novation fields as needed
												fp.processNovationFieldSetVisibility(fp.getFormValue('tradeType.name'));

												const tradeDate = form.findField('tradeDate');
												const today = new Date().format('m/d/Y');
												const td = tradeDate.value;
												Promise.resolve()
													.then(function() {
														if (td === today) { // change default based on exchange hours, etc.
															return Clifton.investment.instrument.getTradeDatePromise(record.id, null, combo)
																.then(function(td) {
																	tradeDate.setValue(td);
																	return td;
																});
														}
														else {
															return td;
														}
													})
													.then(function(td) {
														return Clifton.investment.instrument.getSettlementDatePromise(record.id, td, null, null, combo);
													})
													.then(function(settlementDate) {
														form.findField('settlementDate').setValue(settlementDate);

														fp.updateTradeDestination(record.id);
														Clifton.trade.updateExistingPositions(fp, record.id);
													});
											}
										},

										getDetailPageClass: function(newInstance) {
											if (newInstance) {
												return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
											}
											return this.detailPageClass;
										},

										getDefaultData: function(f) { // defaults client/counterparty/isda
											let fp = TCG.getParentFormPanel(this);
											fp = fp.getForm();
											const ha = TCG.getValue('holdingInvestmentAccount', fp.formValues);
											const td = fp.findField('tradeDate').getValue().format('Y-m-d 00:00:00');
											const investmentGroupId = TCG.getValue('tradeType.investmentGroup.id', fp.formValues);
											const investmentTypeId = TCG.getValue('tradeType.investmentType.id', fp.formValues);

											return {
												businessCompany: (ha ? ha.issuingCompany : ''),
												businessContract: (ha ? ha.businessContract : ''),
												startDate: td,
												investmentGroupId: investmentGroupId,
												investmentTypeId: investmentTypeId,
												investmentTypeName: TCG.getValue('tradeType.investmentType.name', fp.formValues)
											};
										}
									}
								]
							},
							{
								columnWidth: .38,
								items: [
									{
										xtype: 'security-drag-and-drop-container', anchor: '-60',
										tooltipMessage: 'Drag and drop a security ticker from Bloomberg here, or click here to assist in creating a new security',
										prepareSecurityDefaultData: async function(defaultData) {
											this.resetSecurityCombos();

											const form = TCG.getParentFormPanel(this).getForm();
											const tradeType = TCG.getValue('tradeType', form.formValues);
											if (tradeType) {
												const investmentType = TCG.getValue('investmentType', tradeType);
												if (investmentType) {
													defaultData.investmentType = investmentType;
												}
												else if (tradeType.name) {
													const populatedTradeType = await TCG.data.getDataPromiseUsingCaching(`tradeTypeByName.json?name=${tradeType.name}`, this, `trade.type.${tradeType.name}`);
													defaultData.investmentType = TCG.getValue('investmentType', populatedTradeType);
												}
											}
											return defaultData;
										},
										resetSecurityCombos: function() {
											const form = TCG.getParentFormPanel(this).getForm();
											form.findField('investmentSecurity.label').resetStore();
										},
										listeners: {
											beforerender: function(container) {
												container.on('afterrender', function() {
													// reset drag-drop-msg class padding to resize element
													container.el.setStyle('padding', '1px');
													container.el.setStyle('margin-bottom', '0px');
												});
											}
										}
									}
								]
							}
						]
					},
					{fieldLabel: 'Settlement CCY', name: 'payingSecurity.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'payingSecurity.id'},
					{
						fieldLabel: 'Trade Destination', name: 'tradeDestination.name', width: 280, hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true, hidden: true,
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								const executingBrokerCompanyId = f.findField('executingBrokerCompany.label').getValue();
								combo.store.baseParams = {
									executingBrokerCompanyId: executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : null,
									tradeTypeId: f.findField('tradeType.id').getValue(),
									activeOnDate: f.findField('tradeDate').value
								};
							},
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								// Set a flag on the form panel to avoid future defaults on the destination
								fp.destinationSelected = true;
								fp.updateExecutingBrokerList();
								// default the executing broker default to avoid an invalid executing broker to destination mapping
								fp.updateExecutingBrokerCompanyForTradeDestination(record.id);
								if (TCG.isNotBlank(record) && TCG.isNotBlank(record.json.enableTimeInForceSelection)) {
									fp.updateTimeInForcePanelVisibility(record.json.enableTimeInForceSelection);
								}
							},
							change: function(combo, newValue, oldValue) {
								const fp = combo.getParentForm();
								if (TCG.isBlank(newValue)) {
									fp.destinationSelected = false;
								}
								else if (!combo.store.data.containsKey(newValue)) {
									if (combo.store.data.length > 0) {
										combo.setValue(combo.store.data.items[0].id);
										fp.destinationSelected = true;
									}
									else {
										combo.clearValue();
										fp.destinationSelected = false;
									}
								}

								// clear existing executing broker store for refresh
								fp.updateExecutingBrokerList();
							}
						}
					},
					{
						fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', width: 280, hiddenName: 'executingBrokerCompany.id', xtype: 'combo', requiredFields: ['tradeDestination.name'], doNotClearIfRequiredSet: true,
						displayField: 'label',
						queryParam: 'executingBrokerCompanyName',
						detailPageClass: 'Clifton.business.company.CompanyWindow',
						url: 'tradeExecutingBrokerCompanyListFind.json',
						allowBlank: true,
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								let accountId = '';

								const clientInvestmentAccountField = f.findField('clientInvestmentAccount.id');
								if (TCG.isNotBlank(clientInvestmentAccountField)) {
									accountId = f.findField('clientInvestmentAccount.id').getValue();
								}

								combo.store.baseParams = {
									clientInvestmentAccountId: accountId,
									securityId: f.findField('investmentSecurity.id').getValue(),
									tradeDestinationId: f.findField('tradeDestination.name').getValue(),
									tradeExecutionTypeId: f.findField('tradeExecutionType.id').getValue(),
									tradeTypeId: f.findField('tradeType.id').getValue(),
									activeOnDate: f.findField('tradeDate').value
								};
							},
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								// Set a flag on the form panel to avoid future defaults on the executing broker
								fp.executingBrokerSelected = true;
								// clear existing destinations store for refresh
								const dst = fp.getForm().findField('tradeDestination.name');
								dst.resetStore();
							},
							change: function(combo, newValue, oldValue) {
								const fp = combo.getParentForm();
								if (TCG.isBlank(newValue)) {
									fp.executingBrokerSelected = false;
								}
								else if (!combo.store.data.containsKey(newValue)) {
									if (combo.store.data.length > 0) {
										combo.setValue(combo.store.data.items[0].id);
										fp.executingBrokerSelected = true;
									}
									else {
										combo.clearValue();
										fp.executingBrokerSelected = false;
									}
								}

								// clear existing destinations store for refresh
								const dst = fp.getForm().findField('tradeDestination.name');
								dst.resetStore();
							}
						}
					},

					{xtype: 'trade-destination-link', hidden: true},
					{
						xtype: 'panel',
						width: 660,
						layout: 'column',
						items: [
							{
								columnWidth: .41,
								layout: 'form',
								labelWidth: 120,
								items: [
									{
										fieldLabel: 'Execution Type', name: 'tradeExecutionType.name', displayField: 'name', hiddenName: 'tradeExecutionType.id', xtype: 'combo', width: 125,
										url: 'tradeExecutionTypeListFind.json', autoLoad: true, hidden: true, requestedProps: 'bticTrade|clsTrade|requiredHoldingInvestmentAccountType|excludeHoldingInvestmentAccountType',
										selectDefaultValue: false,
										detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow',
										listeners: {
											afterrender: function(combo) {
												combo.store.on('load', function(store, records) {
													if (TCG.isNotBlank(combo.defaultValue) && TCG.isBlank(combo.getValue()) && TCG.isTrue(combo.selectDefaultValue)) {
														combo.setValue(combo.defaultValue.id);
														combo.selectDefaultValue = false;
													}
												});

												const fp = combo.getParentForm();
												const win = fp.getWindow();
												// A  new window (without an existing trade) will have win.tradeType as the trade type name.
												if (TCG.isNotBlank(win.tradeType)) {
													// Set and cache the default trade execution type if it exists
													TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + win.tradeType, combo, 'trade.type.' + win.tradeType)
														.then(function(tradeType) {
															combo.selectDefaultValue = true;
															combo.tradeType = tradeType;
															combo.clearAndReset();
															combo.updateDefaultTradeExecutionTypeValue(combo);
														});
												}
												else {
													// A  window opened containing previous trade data will have its the TradeType data in its defaultData.
													combo.selectDefaultValue = false;
													combo.tradeType = win.defaultData.tradeType;
													combo.updateDefaultTradeExecutionTypeValue(combo);
												}
											},
											beforequery: function(queryEvent) {
												const combo = queryEvent.combo;

												combo.store.baseParams = {
													tradeTypeId: combo.tradeType.id
												};
											},
											select: function(combo, record, index) {
												const fp = TCG.getParentFormPanel(this);
												const f = fp.getForm();
												if (TCG.isTrue(record.json.bticTrade)) {
													const blockTradeField = f.findField('blockTrade');
													blockTradeField.setValue(true);
												}

												const ha = f.findField('holdingInvestmentAccount.label');
												ha.store.removeAll();
												ha.lastQuery = null;
												ha.doQuery('');
											}
										},
										updateDefaultTradeExecutionTypeValue: function(combo) {
											return TCG.data.getDataPromiseUsingCaching('tradeTypeTradeExecutionTypeListFind.json?tradeTypeId=' + combo.tradeType.id + '&defaultExecutionType=true', combo, 'trade.defaultTradeExecutionTypeForwards' + combo.tradeType.id)
												.then(function(tradeTypeTradeExecutionTypeList) {
													if (TCG.isNotBlank(tradeTypeTradeExecutionTypeList) && tradeTypeTradeExecutionTypeList.length > 0) {
														combo.defaultValue = tradeTypeTradeExecutionTypeList[0].referenceTwo;
														combo.doQuery('');
													}
												});
										}
									}
								]
							},

							{
								columnWidth: .59,
								layout: 'form',
								labelWidth: 60,
								items: [
									{fieldLabel: 'Limit Price', name: 'limitPrice', xtype: 'pricefield', hidden: true, minValue: 0.01, width: 70, qtip: 'If a limit price is specified, the trade will become limit order, otherwise it will become a market order.'}
								]
							}

						]
					},

					{
						xtype: 'panel',
						width: 712,
						name: 'timeInForcePanel',
						layout: 'column',
						items: [
							{
								columnWidth: .38,
								layout: 'form',
								labelWidth: 120,
								items: [
									{
										fieldLabel: 'Time In Force', name: 'tradeTimeInForceType.label', displayField: 'label', hiddenName: 'tradeTimeInForceType.id', xtype: 'combo', width: 125,
										url: 'tradeTimeInForceTypeListFind.json', autoLoad: true, requestedProps: 'label|expirationDateRequired', qtip: 'Select the Time In Force (TIF) to determine how long the order should stay open waiting for a fill. If not selected, the default value is Day.',
										selectDefaultValue: false,
										detailPageClass: 'Clifton.trade..execution.TradeTimeInForceTypeWindow',
										listeners: {
											afterrender: async function(component) {
												const fp = TCG.getParentFormPanel(this);
												let tradeDestination = fp.getFormValue('tradeDestination');
												if (TCG.isBlank(tradeDestination)) {
													const dd = await fp.getDefaultData(this.getWindow());
													tradeDestination = dd.tradeDestination;
												}
												if (TCG.isNotBlank(tradeDestination)) {
													fp.updateTimeInForcePanelVisibility(tradeDestination.enableTimeInForceSelection);
													const timeInForceType = component.getValueObject();
													if (TCG.isNotBlank(timeInForceType)) {
														fp.updateExpireDateComponentState(timeInForceType.expirationDateRequired);
													}
												}
												else {
													fp.updateTimeInForcePanelVisibility(false);
												}
											},
											select: function(combo, record, index) {
												const fp = TCG.getParentFormPanel(this);
												if (TCG.isNotBlank(record)) {
													fp.updateExpireDateComponentState(TCG.isTrue(record.json.expirationDateRequired));
												}
												else {
													fp.updateExpireDateComponentState(false);
												}
											}
										}

									}
								]
							},

							{
								columnWidth: .62,
								layout: 'form',
								labelWidth: 60,
								items: [
									{fieldLabel: 'Expires', name: 'timeInForceExpirationDate', xtype: 'datefield', width: 100, qtip: 'A future date which represents the last day the order will remain open while waiting for a fill.'}
								]
							}
						]
					},

					{boxLabel: 'Collateral trade', name: 'collateralTrade', xtype: 'checkbox', hidden: true},
					{boxLabel: 'Block trade', name: 'blockTrade', xtype: 'checkbox', qtip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'}
				]
			},
			{
				columnWidth: .31,
				layout: 'form',
				labelWidth: 130,
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{fieldLabel: 'Trade Type', name: 'tradeType.name', xtype: 'linkfield', detailIdField: 'tradeType.id', detailPageClass: 'Clifton.trade.TypeWindow'},
					{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', submitDetailField: false, detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
					{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', detailPageClass: 'Clifton.security.user.UserWindow', allowBlank: false},
					{
						fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
						getParentFormPanel: function() {
							const parent = TCG.getParentFormPanel(this);
							return parent || this.ownerCt;
						},
						listeners: {
							change: function(field, newValue, oldValue) {
								const form = field.getParentFormPanel().getForm();
								const settlementDateField = form.findField('settlementDate');

								if (TCG.isBlank(newValue)) {
									settlementDateField.setValue('');
								}
								else {
									Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, newValue, null, null, field)
										.then(function(settlementDate) {
											form.findField('settlementDate').setValue(settlementDate);
										});
								}
							},
							select: function(field, date) {
								const form = field.getParentFormPanel().getForm();

								Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, date, null, null, field)
									.then(function(settlementDate) {
										form.findField('settlementDate').setValue(settlementDate);
									});
							}
						}
					},
					{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false, requiredFields: ['investmentSecurity.label']},
					{xtype: 'trade-bookingdate'}
				]
			},
			{xtype: 'novation-fieldset', hidden: true}
		]
	}]
});
Ext.reg('trade-default-form', Clifton.trade.DefaultTradeForm);

Clifton.trade.DefaultTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 1000,
	height: 600,
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,

		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.trade.DefaultTradeWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.trade.DefaultTradeWindowAdditionalTabs[i]);
				}
			}
		},

		items: [
			{
				title: 'Info',
				tbar: {
					xtype: 'trade-workflow-toolbar',
					reloadAfterTransition: function(record) {
						const fp = this.getFormPanel();

						/*
						 * If the record does not contain commission/fee amounts, then clear the associated fields, because it means
						 * the value changed. This ensures correct commission/fee override logic. This solution is likely temporary
						 * and a broader alternative solution may be more ideal.
						 */
						if (TCG.isNull(record.commissionAmount)) {
							fp.getForm().findField('commissionAmount').setValue('');
						}

						if (TCG.isNull(record.commissionPerUnit)) {
							fp.getForm().findField('commissionPerUnit').setValue('');
						}

						if (TCG.isNull(record.feeAmount)) {
							fp.getForm().findField('feeAmount').setValue('');
						}

						Clifton.workflow.Toolbar.prototype.reloadAfterTransition.apply(this, arguments);
					}
				},

				items: [{
					xtype: 'trade-default-form',

					getSubmitParams: function() {
						return {requestedPropertiesToExclude: 'order', enableCommissionOverride: true};
					},

					listeners: {
						beforerender: function() {
							const defaultData = this.getWindow().defaultData;
							const status = TCG.getValue('workflowStatus.name', defaultData);
							this.readOnlyMode = (TCG.isNotNull(status) && status !== 'Draft');
							if (this.readOnlyMode) {
								this.readOnlyMode = (this.getLoadURL() !== false);
								this.add(this.readOnlyItems);
							}
							else {
								this.add(this.commonEntryItems);
								this.add(this.additionalEntryItems);
							}
						},
						afterload: function() {
							const fp = this;
							const f = fp.getForm();

							const ro = ('Draft' !== fp.getFormValue('workflowStatus.name'));
							fp.setReadOnlyMode(ro);

							if (TCG.isNull(fp.getFormValue('settlementCompany.label'))) {
								fp.hideField('settlementCompany.label');
							}

							// quantity intended is editable for partially filled trades
							f.findField('quantityIntended').setReadOnly(ro && ('Cannot Fill' !== fp.getFormValue('workflowState.name')));

							// Always allow editing description f.findField('description').setReadOnly(ro && ('Pending' != this.getFormValue('workflowState.name')));
							// commission/fees and settlement date are editable for Active trades
							const readOnlyFlag = ro && ('Active' !== fp.getFormValue('workflowStatus.name'));

							f.findField('commissionPerUnit').setReadOnly(readOnlyFlag);
							f.findField('feeAmount').setReadOnly(readOnlyFlag);
							f.findField('settlementDate').setReadOnly(readOnlyFlag);

							// Verify the commissions and fee amounts are accurate with current trade data
							fp.setFormValue('commissionPerUnit', fp.getFormValue('commissionPerUnit'), true);
							fp.setFormValue('commissionAmount', fp.getFormValue('commissionAmount'), true);
							fp.setFormValue('feeAmount', fp.getFormValue('feeAmount'), true);

							const v = f.findField('accountingNotional').getNumericValue();
							const fx = f.findField('exchangeRateToBase').getNumericValue();
							const o = f.findField('baseNotional');

							let label = 'Notional';
							let value = v;
							if (this.getFormValue('investmentSecurity.instrument.hierarchy.noPaymentOnOpen') === false) {
								const buy = (fp.getFormValue('buy') === true);
								value = buy ? -v : v;
								value = value + (f.findField('feeAmount').getNumericValue() || 0) + (f.findField('commissionAmount').getNumericValue() || 0);

								label = (value < 0) ? 'Payment' : 'Proceeds';
								f.findField('accountingNotional').setFieldLabel(label + ':');
								const nn = f.findField('netNotional');
								nn.setFieldLabel('Net ' + label + ':');
								if (value < 0) {
									value = -value;
								}
								fp.setFormValue('netNotional', value, true);
								nn.show();
							}

							fp.setFormValue('baseNotional', value * fx, true);
							o.setFieldLabel('Base ' + label + ' (' + TCG.getValue('clientInvestmentAccount.baseCurrency.symbol', f.formValues) + '):');
							const type = this.getFormValue('tradeType.investmentType.name');
							if (type === 'Options' || type === 'Swaptions') {
								f.findField('accountingNotional').setFieldLabel('Premium:');
							}
							this.processNovationFieldSetVisibility(type);
						}
					},

					readOnlyMode: true,
					setReadOnlyMode: function(ro) {
						if (this.readOnlyMode === ro) {
							this.processFieldVisibility();
							return;
						}
						this.readOnlyMode = ro;
						this.removeAll(true);
						if (this.readOnlyMode) {
							this.add(this.readOnlyItems);
						}
						else {
							this.add(this.commonEntryItems);
							this.add(this.additionalEntryItems);
						}
						try {
							this.doLayout();
						}
						catch (e) {
							// strange error due to removal of elements with allowBlank = false
						}

						const f = this.getForm();
						f.setValues(f.formValues);
						this.loadValidationMetaData(true);
						f.isValid();
						// process field visibility after fields have been reapplied to the form
						this.processFieldVisibility();
					},

					processFieldVisibility: function() {
						const f = this.getForm();
						const fp = this;
						const createOrder = this.getFormValue('tradeDestination.type.createOrder');
						const tradeOrderField = f.findField('orderIdentifier');
						if (TCG.isTrue(f.formValues.tradeType.fixSupported) || TCG.isTrue(createOrder)) {
							f.findField('tradeDestination.name').show();
							if (tradeOrderField && !createOrder) {
								tradeOrderField.hide();
							}
							else if (tradeOrderField) {
								tradeOrderField.show();
							}
						}
						else {
							f.findField('tradeDestination.name').hide();
							if (tradeOrderField) {
								tradeOrderField.hide();
							}
						}

						if (TCG.isTrue(f.formValues.tradeType.collateralSupported)) {
							f.findField('collateralTrade').show();
						}
						else {
							f.findField('collateralTrade').hide();
						}
						if (!TCG.isTrue(this.readOnlyMode) || TCG.isTrue(f.formValues.blockTrade)) {
							f.findField('blockTrade').show();
						}
						else {
							f.findField('blockTrade').hide();
						}
						if (TCG.isTrue(f.formValues.tradeType.tradeExecutionTypeSupported)) {
							fp.showField('tradeExecutionType.name');
							fp.showField('limitPrice');
							if (TCG.isBlank(f.formValues.tradeExecutionType) && TCG.isBlank(f.formValues.limitPrice) && this.readOnlyMode === true) {
								fp.hideField('tradeExecutionType.name');
								fp.hideField('limitPrice');
							}
						}
						else {
							fp.hideField('tradeExecutionType.name');
							fp.hideField('limitPrice');
						}
						if (TCG.isNotBlank(f.formValues.tradeDestination)) {
							let showTimeInForce = f.formValues.tradeDestination.enableTimeInForceSelection;
							if (showTimeInForce && this.readOnlyMode === true && TCG.isBlank(f.formValues.tradeTimeInForceType) && TCG.isBlank(f.formValues.timeInForceExpirationDate)) {
								showTimeInForce = false;
							}
							fp.updateTimeInForcePanelVisibility(showTimeInForce);
						}
					},

					items: [],

					additionalEntryItems: [
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: false, anchor: '-20',
							detailPageClass: 'Clifton.investment.account.AccountWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.store.setBaseParam('relatedPurpose', fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'));
									combo.store.setBaseParam('relatedPurposeActiveOnDate', fp.getForm().findField('tradeDate').value);
								},
								select: function(combo, record) {
									const fp = combo.getParentForm();
									const f = fp.getForm();
									fp.updateExecutingBrokerList();
									fp.updateExecutingBrokerCompanyAfterClientAccountChanged(f.findField('tradeDestination.name').getValue());

									// default holding account
									const ha = f.findField('holdingInvestmentAccount.label');
									ha.store.removeAll();
									ha.lastQuery = null;
									ha.store.baseParams = {
										mainAccountId: record.id,
										ourAccount: false,
										workflowStatusNameEquals: 'Active',
										contractRequired: f.findField('investmentSecurity.instrument.hierarchy.otc').getValue()
									};
									const purpose = fp.getFormValue('tradeType.holdingAccountPurpose.name');
									if (TCG.isNotNull(purpose)) {
										ha.store.baseParams.mainPurpose = purpose;
										ha.store.baseParams.mainPurposeActiveOnDate = f.findField('tradeDate').value;
									}
									ha.doQuery('');
									Clifton.trade.updateExistingPositions(fp);
								}
							}
						},
						{
							fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json', allowBlank: false, anchor: '-20', requiredFields: ['clientInvestmentAccount.label'], detailPageClass: 'Clifton.investment.account.AccountWindow',
							requestedProps: 'type|issuingCompany.id',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const baseParams = combo.store.baseParams;
									const f = combo.getParentForm().getForm();
									const tradeExecutionTypeField = f.findField('tradeExecutionType.name');
									if (TCG.isNotBlank(tradeExecutionTypeField.getValueObject())) {
										const tradeExecutionType = tradeExecutionTypeField.getValueObject();
										baseParams.accountType = TCG.isNotBlank(tradeExecutionType.requiredHoldingInvestmentAccountType) ? tradeExecutionType.requiredHoldingInvestmentAccountType.name : null;
										baseParams.excludeAccountType = TCG.isNotBlank(tradeExecutionType.excludeHoldingInvestmentAccountType) ? tradeExecutionType.excludeHoldingInvestmentAccountType.name : null;
									}
									else {
										baseParams.accountType = null;
										baseParams.excludeAccountType = null;
									}
								},
								select: function(combo, record) {
									const fp = combo.getParentForm();
									if (TCG.isNotBlank(record.json.issuingCompany)) {
										fp.updateExecutingBrokerCompanyToIssuingCompany(record.json.issuingCompany.id);
									}
								},
								afterrender: function(field) {
									field.store.on('load', (store, records) => {
										if (records.length === 1) {
											const record = records[0];
											field.setValue(record.id);
											const formPanel = TCG.getParentFormPanel(field);
											if (TCG.isNotBlank(record.json.issuingCompany)) {
												formPanel.updateExecutingBrokerCompanyToIssuingCompany(record.json.issuingCompany.id);
											}
										}
									});
								}
							}
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							labelWidth: 120,
							items: [
								{
									columnWidth: .35,
									layout: 'form',
									defaults: {xtype: 'textfield'},
									items: [
										{fieldLabel: 'Average Price', name: 'averageUnitPrice', xtype: 'pricefield'},
										{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield'}
									]
								},
								{
									columnWidth: .34,
									layout: 'form',
									labelWidth: 120,
									defaults: {xtype: 'textfield'},
									items: [
										{fieldLabel: 'Commission Per Unit', name: 'commissionPerUnit', xtype: 'floatfield', qtip: 'Clear the field to use system defined Commission Schedule during booking. Or populate with Commission Per Unit override amount to bypass default Commission Schedule (will use Commission GL Account defined for this Trade Type).'},
										{fieldLabel: 'Commission Amount', name: 'commissionAmount', xtype: 'currencyfield', readOnly: true, submitValue: false, qtip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.'},
										{fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield', qtip: 'Clear the field to use system defined Fee Schedule during booking. Or populate with Fee override amount to bypass default Fee Schedule (will use Fee GL Account defined for this Trade Type).'}
									]
								},
								{
									columnWidth: .31,
									layout: 'form',
									labelWidth: 130,
									defaults: {xtype: 'textfield', anchor: '-20'},
									items: [
										{fieldLabel: 'Accounting Notional', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
										{fieldLabel: 'Net Notional', name: 'netNotional', xtype: 'currencyfield', readOnly: true, hidden: true},
										{fieldLabel: 'Base Net Notional', name: 'baseNotional', xtype: 'currencyfield', readOnly: true}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop'},
						{xtype: 'trade-existingPositionsGrid'}
					],

					readOnlyItems: [
						{
							xtype: 'panel',
							layout: 'column',
							labelWidth: 120,
							items: [
								{
									columnWidth: .69,
									layout: 'form',
									defaults: {xtype: 'textfield'},
									items: [
										{
											fieldLabel: 'Buy/Sell', name: 'openCloseType.label', xtype: 'displayfield', width: 125,
											setRawValue: function(v) {
												this.el.addClass(v.includes('BUY') ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, v);
											}
										},
										{fieldLabel: 'Quantity', name: 'quantityIntended', xtype: 'spinnerfield', minValue: 0.00000001, maxValue: 10000000000, width: 129, readOnly: true, allowBlank: false},
										{fieldLabel: 'Security', name: 'investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id'},
										{fieldLabel: 'Settlement CCY', name: 'payingSecurity.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'payingSecurity.id'},
										{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'},
										{name: 'tradeDestination.id', xtype: 'hidden'},
										{xtype: 'trade-destination-link', hidden: true},
										{
											xtype: 'panel',
											layout: 'column',
											items: [
												{
													columnWidth: .40,
													layout: 'form',
													labelWidth: 120,
													items: [
														{fieldLabel: 'Execution Type', name: 'tradeExecutionType.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow', detailIdField: 'tradeExecutionType.id', width: 125, hidden: true}
													]
												},

												{
													columnWidth: .60,
													layout: 'form',
													labelWidth: 60,
													items: [
														{fieldLabel: 'Limit Price', name: 'limitPrice', xtype: 'pricefield', minValue: 0.01, width: 70, readOnly: true}
													]
												}
											]
										},
										{
											xtype: 'panel',
											width: 712,
											hidden: true,
											name: 'timeInForcePanel',
											layout: 'column',
											items: [
												{
													columnWidth: .38,
													layout: 'form',
													labelWidth: 120,
													items: [
														{
															fieldLabel: 'Time In Force', name: 'tradeTimeInForceType.label', detailIdField: 'tradeTimeInForceType.id', xtype: 'linkfield', width: 125, detailPageClass: 'Clifton.trade.execution.TradeTimeInForceTypeWindow',
															qtip: 'Select the Time In Force (TIF) to determine how long the order should stay open waiting for a fill. If not selected, the default value is Day.'
														}
													]
												},
												{
													columnWidth: .62,
													layout: 'form',
													labelWidth: 60,
													items: [
														{fieldLabel: 'Expires', name: 'timeInForceExpirationDate', xtype: 'datefield', readOnly: true, width: 100, qtip: 'A future date which represents the last day the order will remain open while waiting for a fill.'}
													]
												}
											]
										},

										{boxLabel: 'Collateral trade', name: 'collateralTrade', xtype: 'checkbox', hidden: true, disabled: true},
										{boxLabel: 'Block trade', name: 'blockTrade', xtype: 'checkbox', hidden: true, disabled: true, qtip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'}
									]
								},
								{
									columnWidth: .31,
									layout: 'form',
									labelWidth: 130,
									defaults: {xtype: 'textfield', anchor: '-20'},
									items: [
										{fieldLabel: 'Trade Type', name: 'tradeType.name', xtype: 'linkfield', detailIdField: 'tradeType.id', detailPageClass: 'Clifton.trade.TypeWindow'},
										{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true},
										{xtype: 'trade-bookingdate'}
									]
								}
							]
						},
						{xtype: 'novation-fieldset', disabled: true, hidden: true},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
						{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'},
						{fieldLabel: 'Settlement Company', name: 'settlementCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'settlementCompany.id', qtip: 'Business company used to determine the place of settlement business identifier code (BIC) when generating settlement instructions.'},

						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							labelWidth: 120,
							items: [
								{
									columnWidth: .35,
									layout: 'form',
									defaults: {xtype: 'textfield'},
									items: [
										{fieldLabel: 'Average Price', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', readOnly: true}
									]
								},
								{
									columnWidth: .34,
									layout: 'form',
									labelWidth: 120,
									defaults: {xtype: 'textfield'},
									items: [
										{fieldLabel: 'Commission Per Unit', name: 'commissionPerUnit', xtype: 'floatfield', readOnly: true, qtip: 'Clear the field to use system defined Commission Schedule during booking. Or populate with Commission Per Unit override amount to bypass default Commission Schedule (will use Commission GL Account defined for this Trade Type).'},
										{fieldLabel: 'Commission Amount', name: 'commissionAmount', xtype: 'currencyfield', readOnly: true, submitValue: false, qtip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.'},
										{fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield', readOnly: true, qtip: 'Clear the field to use system defined Fee Schedule during booking. Or populate with Fee override amount to bypass default Fee Schedule (will use Fee GL Account defined for this Trade Type).'}
									]
								},
								{
									columnWidth: .31,
									layout: 'form',
									labelWidth: 130,
									defaults: {xtype: 'textfield', anchor: '-20'},
									items: [
										{fieldLabel: 'Accounting Notional', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
										{fieldLabel: 'Net Notional', name: 'netNotional', xtype: 'currencyfield', readOnly: true, hidden: true},
										{fieldLabel: 'Base Net Notional', name: 'baseNotional', xtype: 'currencyfield', readOnly: true}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop'}
					]
				}]
			},

			{
				title: 'Compliance',
				items: [{
					xtype: 'trade-violations-grid'
				}]
			},

			{
				title: 'Trade Fills',
				items: [{xtype: 'trade-fillsgrid'}]
			},

			{
				title: 'Commissions and Fees',
				items: [{
					xtype: 'trade-commissions'
				}]
			},

			{
				title: 'Quotes',
				items: [{
					xtype: 'trade-quotes',
					hideAdditionalAmounts: true
				}]
			}
		]
	}]
});
