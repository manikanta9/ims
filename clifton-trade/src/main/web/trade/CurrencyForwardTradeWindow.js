Clifton.trade.CurrencyForwardTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 1000,
	height: 500,
	iconCls: 'currency-forward',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,

		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.trade.DefaultTradeWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.trade.DefaultTradeWindowAdditionalTabs[i]);
				}
			}
		},

		items: [
			{
				title: 'Info',
				tbar: {xtype: 'trade-workflow-toolbar'},

				items: [{
					xtype: 'trade-form-base',
					labelWidth: 95,

					listeners: {
						beforerender: function() {
							const status = TCG.getValue('workflowStatus.name', this.getWindow().defaultData);
							this.readOnlyMode = (status !== 'Draft');
							if (this.readOnlyMode) {
								this.readOnlyMode = (this.getLoadURL() !== false);
							}
							this.add(this.readOnlyMode ? this.readOnlyItems : this.updatableItems);
						},
						afterload: function() {
							const ro = ('Draft' !== this.getFormValue('workflowStatus.name'));
							this.setReadOnlyMode(ro);

							this.updateCalculatedFields(true);
							// Forward Rate is editable for Active trades
							const flag = ro && ('Active' !== this.getFormValue('workflowStatus.name'));
							const f = this.getForm();
							f.findField('averageUnitPrice').setReadOnly(flag);
							f.findField('settlementDateForDisplay').setReadOnly(flag);
							const o = f.findField('tradeGroup.tradeGroupType.name');
							if (o.value !== undefined) {
								o.show();
							}
						}
					},

					getWarningMessage: function(form) {
						let msg = undefined;
						const values = form.formValues;
						if (values) {
							if (values.workflowState.name === 'Invalid' || form.formValues.workflowState.name === 'Executed Invalid') {
								msg = 'This trade failed compliance check(s). See <b>Compliance</b> tab for more information.';
							}
							else if (values.tradeDate !== values.settlementDate) {
								msg = 'This trade has <b>Early Settlement</b>: ' + TCG.renderDate(values.settlementDate) + ' instead of ' + TCG.renderDate(this.getDeliveryDate());
							}
						}
						return msg;
					},

					getDefaultData: function(win) {
						let dd = Ext.apply(win.defaultData || {}, {});
						if (win.defaultDataIsReal) {
							return dd;
						}
						const tradeTypeName = win.tradeType;
						const formPanel = this;
						return TCG.data.getDataPromiseUsingCaching('tradeDestinationByName.json?name=FX Connect Ticket', formPanel, 'trade.tradeDestination.FX Connect Ticket')
							.then(function(tradeDestination) {
								dd.tradeDestination = tradeDestination;
								return TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + tradeTypeName, formPanel, 'trade.type.' + tradeTypeName);
							})
							.then(function(tradeType) {
								const now = new Date().format('Y-m-d 00:00:00');
								dd = Ext.apply({
									tradeType: tradeType,
									traderUser: TCG.getCurrentUser(),
									tradeDate: now,
									settlementDate: now
								}, dd);
								return dd;
							});
					},

					afterRenderApplyAdditionalDefaultData: function(formPanel, formValues) {
						if (TCG.isNotNull(formValues.givenCurrencyForDisplay)) {
							formPanel.updateCalculatedFields(true);
						}
						if (TCG.isNotNull(formValues.holdingInvestmentAccount)) {
							formPanel.updateTradeExecutionType(formValues.holdingInvestmentAccount);
							formPanel.updateExecutingBrokerCompanyList(formValues.holdingInvestmentAccount);
						}
					},

					getSubmitParams: function() { // settlementDate == tradeDate unless it's an early settlement
						if (!this.readOnlyMode) {
							const f = this.getForm();
							const date = f.findField('settlementDateForDisplay').value;
							const securityDate = TCG.renderDate(this.getDeliveryDate());

							let buy = TCG.isTrue(f.findField('buyForDisplay').getValue());
							const sameSettle = this.isSettleInDenominationCCY();
							buy = sameSettle ? buy : !buy;

							const params = {
								buy: buy,
								quantityIntended: f.findField(sameSettle ? 'givenAmountForDisplay' : 'settlementAmountForDisplay').getRawValue(),
								accountingNotional: f.findField(!sameSettle ? 'givenAmountForDisplay' : 'settlementAmountForDisplay').getRawValue(),
								settlementDate: (date === securityDate) ? f.findField('tradeDate').value : date
							};
							TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', this, {
								params: {
									tradeTypeId: this.getFormValue('tradeType.id'),
									buy: buy
								}
							})
								.then(function(openCloseType) {
									if (openCloseType && openCloseType.length === 1) {
										params['openCloseType.id'] = openCloseType[0].id;
									}
								});
							return params;
						}
					},

					/*^
					Repopulates the executingBrokerCompany combo's data store and configures visibility and value of the control based on the the values of other fields on
					the form.
					 */
					updateExecutingBrokerCompanyList: function(holdingAccountObject) {
						const fp = this;
						const f = fp.getForm();
						const executingBrokerCombo = f.findField('executingBrokerCompany.label');
						const tradeDestination = f.findField('tradeDestination.name').getValueObject();
						const brokerCompanyList = [];
						const clientInvestmentAccountId = f.findField('clientInvestmentAccount.label').getValue();
						const holdingAccountCombo = f.findField('holdingInvestmentAccount.label');
						const tradeExecutionType = f.findField('tradeExecutionType.name').getValueObject();
						const cf = f.findField('collateralTrade');
						const holdingAccount = TCG.isNotNull(holdingAccountObject) ? holdingAccountObject : holdingAccountCombo.getValueObject();

						if (TCG.isBlank(clientInvestmentAccountId)) {
							return;
						}

						executingBrokerCombo.clearAndReset();

						// Build list of brokers based on returned list from 'tradeExecutingBrokerCompanyListFind.json', but limited to the issuing companies of the holding accounts.
						const tradeExecutionTypeName = tradeExecutionType.name || tradeExecutionType.label; // name override on combo is not always honored
						if (TCG.isEquals(tradeExecutionTypeName, 'CLS') && TCG.isEquals(tradeDestination.label, 'Manual')) {
							const params = {
								mainAccountId: fp.getFormValue('clientInvestmentAccount.id', true),
								mainPurpose: fp.getFormValue(cf && cf.getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
								mainPurposeActiveOnDate: f.findField('tradeDate').value
							};
							TCG.data.getDataPromise('investmentAccountListFind.json?&ourAccount=false&workflowStatusNameEquals=Active&accountTypes=OTC ISDA&accountTypes=OTC FX Letter&mainAccountId=' + params.mainAccountId + '&mainPurpose=' + params.mainPurpose + '&mainPurposeActiveOnDate=' + params.mainPurposeActiveOnDate, this).then(function(accountList) {
								const issuingCompanyIdSet = new Set();
								accountList.forEach(function(holdingAccount) {
									if (TCG.isNotBlank(holdingAccount.issuingCompany) && TCG.isFalse(issuingCompanyIdSet.has(holdingAccount.issuingCompany.id))) {
										issuingCompanyIdSet.add(holdingAccount.issuingCompany.id);
										brokerCompanyList.push([holdingAccount.issuingCompany.id, holdingAccount.issuingCompany.label]);
									}
								});
								executingBrokerCombo.setDisabled(false);
								executingBrokerCombo.store.loadData(brokerCompanyList);
							});
						}
						else if (TCG.isEquals(tradeExecutionTypeName, 'CLS') && TCG.isNotEquals(tradeDestination.label, 'Manual')) {
							executingBrokerCombo.setDisabled(true);
						}
						else if (TCG.isNotBlank(holdingAccount)) {
							executingBrokerCombo.setDisabled(false);

							// handles case of Non-CLS trade in with any trade destination setting
							const issuingCompany = holdingAccount.issuingCompany;
							if (TCG.isNotBlank(issuingCompany)) {
								brokerCompanyList.push([issuingCompany.id, issuingCompany.label]);
								executingBrokerCombo.store.loadData(brokerCompanyList);
								executingBrokerCombo.setValue(issuingCompany.id);
							}
						}
					},

					/**
					 * Reset and update the trade execution type based on the selected holding account type.
					 */
					updateTradeExecutionType: function(holdingAccount) {
						const fp = this;
						const f = fp.getForm();

						const tradeExecutionTypeField = f.findField('tradeExecutionType.name');
						// update is inhibited if a select event of the tradeExecutionTypeField caused another entity to call this function.
						if (TCG.isFalse(tradeExecutionTypeField.inhibitUpdate)) {
							if (TCG.isNotBlank(tradeExecutionTypeField.defaultValue)) {
								tradeExecutionTypeField.setValue({text: tradeExecutionTypeField.defaultValue.name, value: tradeExecutionTypeField.defaultValue.id});
							}

							// if selected holding account type does not match the required type of the Trade Execution Type, search for a matching Trade Execution Type.
							// In this window we will only deal with 2 Trade Execution Types:  CLS and Non-CLS
							if (TCG.isNotBlank(holdingAccount)) {
								for (let idx = 0; idx < tradeExecutionTypeField.store.data.length; ++idx) {
									const entry = tradeExecutionTypeField.store.data.items[idx].json;
									if (TCG.isNotBlank(entry.requiredHoldingInvestmentAccountType) && entry.requiredHoldingInvestmentAccountType.id === holdingAccount.type.id) {
										tradeExecutionTypeField.setValue(entry.id);
										break;
									}
								}
							}
						}
						else {
							tradeExecutionTypeField.inhibitUpdate = false;
						}
					},

					readOnlyMode: true,
					setReadOnlyMode: function(ro) {
						if (this.readOnlyMode === ro) {
							return;
						}

						const f = this.getForm();

						this.readOnlyMode = ro;
						this.removeAll(true);
						this.add(ro ? this.readOnlyItems : this.updatableItems);
						try {
							this.doLayout();
						}
						catch (e) {
							// strange error due to removal of elements with allowBlank = false
						}
						f.setValues(f.formValues);
						this.loadValidationMetaData(true);
						f.isValid();
					},

					getDeliveryDate: function() {
						const d = this.getFormValue('investmentSecurity.firstDeliveryDate');
						return (TCG.isNull(d) ? this.getFormValue('investmentSecurity.endDate') : d);
					},

					isSettleInDenominationCCY: function() {
						const denominationCCY = this.getFormValue('investmentSecurity.instrument.tradingCurrency');
						const settleCCY = this.getFormValue('payingSecurity');
						return (settleCCY.symbol === denominationCCY.symbol);
					},

					updateSettlementDate: function() {
						const f = this.getForm();
						const sd = f.findField('settlementDateForDisplay');
						if (!sd.isDirty()) { // don't touch if manually updated
							let d = this.getFormValue('settlementDate');
							if (d === this.getFormValue('tradeDate')) {
								d = this.getDeliveryDate();
							}
							this.setFormValue('settlementDateForDisplay', TCG.renderDate(d), true);
						}
					},

					updateCalculatedFields: function(afterLoad) {
						this.updateSettlementDate();

						const from = this.getFormValue('investmentSecurity.instrument.underlyingInstrument.tradingCurrency');
						const to = this.getFormValue('investmentSecurity.instrument.tradingCurrency');
						const settle = this.getFormValue('payingSecurity');
						const buy = TCG.isTrue(this.getFormValue('buy'));

						// NOTE: what do we put into quantity if Forward Rate is not defined?  How can we have notional without quantity?
						if (settle.symbol === to.symbol) {
							this.setFormValue('buyForDisplay-Group', [buy, !buy], true);
							this.setFormValue('givenCurrencyForDisplay', {value: from.id, text: from.symbol}, true);
							if (afterLoad) {
								this.setFormValue('givenAmountForDisplay', this.getFormValue('quantityIntended'), true);
								this.setFormValue('settlementAmountForDisplay', this.getFormValue('accountingNotional'), true);
							}
						}
						else {
							this.setFormValue('buyForDisplay-Group', [!buy, buy], true);
							this.setFormValue('givenCurrencyForDisplay', {value: to.id, text: to.symbol}, true);
							if (afterLoad) {
								this.setFormValue('givenAmountForDisplay', this.getFormValue('accountingNotional'), true);
								this.setFormValue('settlementAmountForDisplay', this.getFormValue('quantityIntended'), true);
							}
						}

						this.setFieldLabel('quantityIntended', (buy ? '<div class="buy-light">Buy ' : '<div class="sell-light">Sell ') + (from ? from.symbol : '') + ':</div>');
						this.setFieldLabel('accountingNotional', (buy ? '<div class="sell-light">Sell ' : '<div class="buy-light">Buy ') + (to ? to.symbol : '') + ':</div>');

						this.updateForwardRateConvention();
					},

					updateForwardRateConvention: function() {
						const from = this.getFormValue('investmentSecurity.instrument.underlyingInstrument.tradingCurrency');
						const to = this.getFormValue('investmentSecurity.instrument.tradingCurrency');
						if (from && to) {
							TCG.data.getDataValuePromiseUsingCaching('investmentCurrencyMultiplyConventionUsed.json?fromSymbol=' + from.symbol + '&toSymbol=' + to.symbol, this, 'investment.currency.convention.' + from.symbol + to.symbol)
								.then(result => this.multiplyByForwardRate = TCG.isTrue(result));
						}
					},

					amount_onchange: function(field, newValue, oldValue) {
						const fp = TCG.getParentFormPanel(field);
						const f = fp.getForm();

						const sameSettle = this.isSettleInDenominationCCY();

						const qtyField = this.readOnlyMode ? 'quantityIntended' : 'givenAmountForDisplay';
						const ntlField = this.readOnlyMode ? 'accountingNotional' : 'settlementAmountForDisplay';

						const qty = f.findField(qtyField);
						const ntl = f.findField(ntlField);
						const ap = f.findField('averageUnitPrice');
						const rate = ap.getNumericValue();

						// NOTE: first 2 if statements should never happen, refactor to use "*AmountForDisplay' fields or delete?
						if (field.name === 'accountingNotional') {
							if (TCG.isBlank(newValue)) {
								qty.setValue('');
							}
							else {
								if (rate) {
									qty.setValue((this.multiplyByForwardRate === false) ? (field.getNumericValue() * rate) : (field.getNumericValue() / rate));
								}
								else if (qty.getNumericValue()) {
									ap.setValue((this.multiplyByForwardRate === false) ? (qty.getNumericValue() / TCG.parseFloat(newValue)) : (TCG.parseFloat(newValue) / qty.getNumericValue()));
								}
							}
						}
						else if (field.name === 'quantityIntended') {
							if (TCG.isBlank(newValue)) {
								ntl.setValue('');
							}
							else {
								if (rate) {
									TCG.data.getDataValuePromise('investmentSecurityNotional.json?securityId=' + fp.getFormValue('investmentSecurity.id') + '&price=' + rate + '&quantity=' + newValue, field)
										.then(function(notional) {
											ntl.setValue(notional);
										});
								}
								else if (ntl.getNumericValue()) {
									ap.setValue((this.multiplyByForwardRate === false) ? (TCG.parseFloat(newValue) / ntl.getNumericValue()) : (ntl.getNumericValue() / TCG.parseFloat(newValue)));
								}
							}
						}
						else if (field.name === 'averageUnitPrice') {
							if (TCG.isBlank(newValue)) {
								ap.setValue('');
								ntl.setValue('');
							}
							else {
								const roundingSecurity = sameSettle ? this.getFormValue('investmentSecurity.instrument.tradingCurrency') : this.getFormValue('payingSecurity');
								let notionalField;
								// NOTE: may need to take into account this.readOnlyMode
								if (qty.getNumericValue()) {
									let multiply = (this.multiplyByForwardRate !== false);
									if (!sameSettle) {
										multiply = !multiply;
									}
									ntl.setValue(multiply ? qty.getNumericValue() * field.getNumericValue() : qty.getNumericValue() / field.getNumericValue());
									notionalField = ntl;
								}
								else if (ntl.getNumericValue()) {
									let multiply = (this.multiplyByForwardRate === false);
									if (!sameSettle) {
										multiply = !multiply;
									}
									qty.setValue(multiply ? ntl.getNumericValue() * field.getNumericValue() : ntl.getNumericValue() / field.getNumericValue());
									notionalField = qty;
								}
								else {
									TCG.showError('Unable to determine a notional amount.');
									return;
								}
								TCG.data.getDataValuePromise('investmentSecurityNotionalRoundingApply.json?roundingSecurityId=' + roundingSecurity.id + '&notionalAmount=' + notionalField.getValue(), field)
									.then(function(notional) {
										notionalField.setValue(notional);
									});
							}
						}
					},

					items: [],


					updatableItems: [
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {anchor: '-20'}},
							items: [
								{
									columnWidth: .71,
									items: [
										{
											fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: false, anchor: '-20',
											detailPageClass: 'Clifton.investment.account.AccountWindow',
											listeners: {
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;
													const fp = combo.getParentForm();
													combo.store.baseParams = {
														relatedPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
														relatedPurposeActiveOnDate: fp.getForm().findField('tradeDate').value
													};
												},
												afterrender: function(combo) {
													const fp = combo.getParentForm();
													const f = fp.getForm();
													const ha = f.findField('holdingInvestmentAccount.label');
													ha.store.on('load', function(store, records) {
														if (records.length === 1) {
															const c = records[0].json;
															ha.setValue({value: c.id, text: c.label});
															f.formValues.holdingInvestmentAccount = c;
															fp.updateTradeExecutionType(c);
															fp.updateExecutingBrokerCompanyList();
														}
														else if (records.length > 1) {
															// find the account which has a matching type of 'OTC CLS'
															ha.clearValue();
															const matchingAccounts = records.map(record => record.json)
																.filter(holdingAccount => holdingAccount.type.name === 'OTC CLS');

															// select the CLS account if we have only one, if multiple are found, let trader select
															if (matchingAccounts.length === 1) {
																const holdingAccount = matchingAccounts[0];
																ha.setValue({value: holdingAccount.id, text: holdingAccount.label});
																fp.updateTradeExecutionType(holdingAccount);
																fp.updateExecutingBrokerCompanyList();
															}
															else if (matchingAccounts.length > 1) {
																// don't select any accounts, but update the Execution Type to CLS using the first matching account as the parameter.
																fp.updateTradeExecutionType(matchingAccounts[0]);
																fp.updateExecutingBrokerCompanyList();
															}
														}
													});
												},
												beforeselect: function(combo, record) {
													const fp = combo.getParentForm();
													const f = fp.getForm();
													const ha = f.findField('holdingInvestmentAccount.label');
													const tradeExecutionTypeField = f.findField('tradeExecutionType.name');
													if (TCG.isNotBlank(tradeExecutionTypeField.defaultValue)) {
														tradeExecutionTypeField.setValue(tradeExecutionTypeField.defaultValue.id);
													}
													ha.clearAndReset();
													ha.lastQuery = null;
													ha.updateBaseParams(record.id);
													ha.doQuery('');
												},
												select: function(combo, record) {
													const fp = combo.getParentForm();

													const f = fp.getForm();
													const curr = record.json.baseCurrency;
													if (curr) {
														f.findField('clientInvestmentAccount.baseCurrency.label').setValue(curr.label);
														f.findField('clientInvestmentAccount.baseCurrency.id').setValue(curr.id);
													}

													fp.updateExecutingBrokerCompanyList();
													Clifton.trade.updateExistingPositions(fp);
												}
											}
										},
										{
											fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json', anchor: '-20', requiredFields: ['clientInvestmentAccount.label'],
											detailPageClass: 'Clifton.investment.account.AccountWindow', requestedProps: 'type',
											updateBaseParams: function(clientAccountId) {
												const fp = this.getParentForm();
												const f = fp.getForm();
												const cf = f.findField('collateralTrade');
												if (TCG.isNotNull(clientAccountId) || !this.store.baseParams.mainAccountId) {
													this.store.baseParams = {
														mainAccountId: clientAccountId || fp.getFormValue('clientInvestmentAccount.id', true),
														mainPurpose: fp.getFormValue(cf && cf.getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
														mainPurposeActiveOnDate: f.findField('tradeDate').value,
														ourAccount: false,
														workflowStatusNameEquals: 'Active'
													};
												}
												const tradeExecutionTypeField = f.findField('tradeExecutionType.name');
												if (TCG.isNotBlank(tradeExecutionTypeField.getValueObject())) {
													const tradeExecutionType = tradeExecutionTypeField.getValueObject();
													this.store.baseParams.accountType = TCG.isNotBlank(tradeExecutionType.requiredHoldingInvestmentAccountType) ? tradeExecutionType.requiredHoldingInvestmentAccountType.name : null;
													this.store.baseParams.excludeAccountType = TCG.isNotBlank(tradeExecutionType.excludeHoldingInvestmentAccountType) ? tradeExecutionType.excludeHoldingInvestmentAccountType.name : null;
												}
												else {
													this.store.baseParams.accountType = null;
													this.store.baseParams.excludeAccountType = null;
												}
											},
											listeners: {
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;
													combo.updateBaseParams();
												},
												change: function(combo, newValue, oldValue) {
													if (TCG.isBlank(newValue) || TCG.isEquals('OTC CLS', combo.getValueObject().type.name)) {
														const fp = combo.getParentForm();
														// clear executing broker, since the account selection has been cleared.
														fp.updateExecutingBrokerCompanyList();
													}
												},
												select: function(combo, record, index) {
													const fp = combo.getParentForm();
													const f = fp.getForm();

													const curr = record.json.baseCurrency;
													if (curr) {
														f.findField('holdingInvestmentAccount.baseCurrency.label').setValue(curr.label);
														f.findField('holdingInvestmentAccount.baseCurrency.id').setValue(curr.id);
													}

													fp.updateTradeExecutionType(record.json);
													fp.updateExecutingBrokerCompanyList();
												}
											}
										}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 60,
									items: [
										{fieldLabel: 'Base CCY', name: 'clientInvestmentAccount.baseCurrency.label', detailIdField: 'clientInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false},
										{fieldLabel: 'Base CCY', name: 'holdingInvestmentAccount.baseCurrency.label', detailIdField: 'holdingInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {anchor: '-20'}},
							items: [
								{
									columnWidth: .31,
									items: [
										{
											xtype: 'radiogroup',
											fieldLabel: 'Buy/Sell',
											name: 'buyForDisplay-Group',
											columns: [60, 60],
											allowBlank: true,
											anchor: '-25',
											items: [
												{boxLabel: 'Buy', name: 'buyForDisplay', inputValue: 'true'},
												{boxLabel: 'Sell', name: 'buyForDisplay', inputValue: 'false'}
											],
											listeners: {
												change: function(grp, checked) {
													const fp = TCG.getParentFormPanel(checked);
													fp.getForm().formValues.buy = TCG.isTrue(checked.inputValue);
												}
											}
										},
										{
											fieldLabel: 'Given CCY', name: 'givenCurrencyForDisplay', hiddenName: 'givenCurrencyForDisplay.id', submitValue: false, displayField: 'symbol', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
											listeners: {
												change: function(combo, newValue, oldValue) {
													// Since this field is required by Forward and is transient, the required functionality does not
													// work correctly because we set this value after the form is loaded. Thus, we have use the
													// doNotClearIfRequiredSet flag on the Forward field and we have to clear the value on changes.
													TCG.getParentFormPanel(combo).getForm().findField('investmentSecurity.symbol').clearAndReset();
												}
											}
										},
										{fieldLabel: 'Given Amount', name: 'givenAmountForDisplay', xtype: 'currencyfield', allowBlank: false, submitValue: false},
										{
											fieldLabel: 'Settle CCY', name: 'payingSecurity.symbol', hiddenName: 'payingSecurity.id', displayField: 'symbol', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
											listeners: {
												select: function(combo, record, index) {
													const fp = combo.getParentForm();
													const f = fp.getForm();
													f.formValues.payingSecurity = record.json;
												},
												change: function(combo, newValue, oldValue) {
													// This field is required by the Forward field and needs to trigger a clear on a change.
													// See the comment on the 'Given CCY' field for more information.
													TCG.getParentFormPanel(combo).getForm().findField('investmentSecurity.symbol').clearAndReset();
												}
											}
										},
										{fieldLabel: 'Settle Amount', name: 'settlementAmountForDisplay', xtype: 'currencyfield', submitValue: false}
									]
								},
								{
									columnWidth: .40,
									labelWidth: 105,
									items: [
										{
											fieldLabel: 'Forward', name: 'investmentSecurity.symbol', allowBlank: false, displayField: 'symbol', hiddenName: 'investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?investmentType=Forwards', requiredFields: ['givenCurrencyForDisplay.id', 'payingSecurity.id'], doNotClearIfRequiredSet: true,
											requestedProps: 'instrument.hierarchy',
											listeners: {
												beforeQuery: function(qEvent) {
													const f = qEvent.combo.getParentForm().getForm();
													const params = {activeOnDate: f.findField('tradeDate').value};
													// use the given and settle CCYs to look up forwards where each is used as either instrument currency denomination or underlying instrument currency denomination
													// E.g given=USD, settle=CAD will find forwards in form USD/CAD as well as CAD/USD. The listing will match the forwards for given=CAD, settle=USD.
													const currencyDenominations = [f.findField('givenCurrencyForDisplay.id').getValue(), f.findField('payingSecurity.id').getValue()];
													params.underlyingCurrencyDenominationIds = currencyDenominations;
													params.currencyDenominationIds = currencyDenominations;

													qEvent.combo.store.baseParams = params;
												},
												change: function(combo, newValue, oldValue) {
													if (TCG.isTrue(combo.requiresFormValueUpdate)) {
														if (TCG.isNotBlank(newValue)) {
															TCG.data.getDataPromise('investmentSecurity.json?id=' + newValue, combo).then(investmentSecurity => {
																	if (TCG.isNotBlank(investmentSecurity)) {
																		combo.updateFormValues(combo, investmentSecurity);
																	}
																}
															);
														}
													}
												},
												select: function(combo, record, index) {
													combo.updateFormValues(combo, record.json);
												}
											},

											updateFormValues: function(combo, investmentSecurity) {
												const fp = this.getParentForm();
												const f = fp.getForm();
												f.formValues.investmentSecurity = investmentSecurity;
												fp.updateForwardRateConvention.call(fp);
												fp.updateSettlementDate.call(fp);
												const endDate = TCG.parseDate(investmentSecurity.endDate);
												f.findField('investmentSecurity.endDate').setValue(endDate.format('m/d/y'));
												Clifton.trade.updateExistingPositions(fp, investmentSecurity.id);
												this.requiresFormValueUpdate = false;
											},

											getDetailPageClass: function(newInstance) {
												if (newInstance) {
													this.requiresFormValueUpdate = true;
													return 'Clifton.investment.instrument.copy.ForwardCopyWindow';
												}
												return this.detailPageClass;
											},

											getDefaultData: async function(f) {
												const combo = this;
												let securityToCopy = combo.getValueObject();

												if (TCG.isBlank(securityToCopy)) {
													const fp = TCG.getParentFormPanel(this);
													const frm = fp.getForm();
													const givenCCYId = frm.findField('givenCurrencyForDisplay.id').getValue();
													const settleCCYId = frm.findField('payingSecurity.id').getValue();
													const investmentTypeId = fp.getFormValue('tradeType.investmentType.id');
													const queryParams = {
														investmentTypeId: investmentTypeId,
														underlyingTradingCurrencyId: givenCCYId,
														tradingCurrencyId: settleCCYId,
														inactive: false,
														start: 0,
														limit: 1
													};

													const matchingInstrumentList = await TCG.data.getDataPromise('investmentInstrumentListFind.json?', this, {params: queryParams});
													const investmentInstrument = matchingInstrumentList && matchingInstrumentList.length > 0 ? matchingInstrumentList[0] : null;
													if (TCG.isNotBlank(investmentInstrument)) {
														return {
															instrument: investmentInstrument,
															hierarchyId: investmentInstrument.hierarchy.id
														};
													}
													return {};
												}

												// Newly-added forward securities will not have an instrument. If there is no instrument, refresh security so it has instrument populated.
												if (TCG.isBlank(securityToCopy.instrument)) {
													securityToCopy = await TCG.data.getDataPromise('investmentSecurity.json?id=' + securityToCopy.id, this);
												}

												return {
													securityToCopy: securityToCopy,
													instrument: securityToCopy.instrument,
													hierarchyId: securityToCopy.instrument.hierarchy.id
												};
											}
										},
										{
											fieldLabel: 'Forward Rate', name: 'averageUnitPrice', xtype: 'floatfield', requiredFields: ['investmentSecurity.id'],
											listeners: {
												change: function(field, newValue, oldValue) {
													const fp = TCG.getParentFormPanel(field);
													fp.amount_onchange(field, newValue, oldValue);
												}
											}
										},
										{
											fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow',
											mode: 'local', displayField: 'label', valueField: 'id',
											store: new Ext.data.ArrayStore({
												fields: ['id', 'label'],
												data: []
											})
										},
										{
											fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
											listeners: {
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;
													const f = combo.getParentForm().getForm();
													combo.store.baseParams = {
														tradeTypeId: f.findField('tradeType.id').getValue(),
														activeOnDate: f.findField('tradeDate').value
													};
												},
												select: function(combo, record, index) {
													const fp = combo.getParentForm();
													fp.updateExecutingBrokerCompanyList();
												}
											}
										},
										{
											fieldLabel: 'Execution Type', name: 'tradeExecutionType.name', displayField: 'name', hiddenName: 'tradeExecutionType.id', xtype: 'combo', width: 120,
											url: 'tradeExecutionTypeListFind.json', requestedProps: 'bticTrade|clsTrade|requiredHoldingInvestmentAccountType|excludeHoldingInvestmentAccountType',
											detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow', selectDefaultValue: false, inhibitUpdate: false,

											listeners: {
												afterrender: function(combo) {
													combo.store.on('load', function(store, records) {
														if (TCG.isNotBlank(combo.defaultValue) && TCG.isBlank(combo.getValue()) && TCG.isTrue(combo.selectDefaultValue)) {
															combo.setValue(combo.defaultValue.id);
															combo.selectDefaultValue = false;
														}
													});

													const fp = combo.getParentForm();
													const win = fp.getWindow();
													// A  new window (without an existing trade) will have win.tradeType as the trade type name.
													if (TCG.isNotBlank(win.tradeType)) {
														// Set and cache the default trade execution type if it exists
														TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + win.tradeType, combo, 'trade.type.' + win.tradeType)
															.then(function(tradeType) {
																combo.selectDefaultValue = true;
																combo.tradeType = tradeType;
																combo.clearAndReset();
																combo.updateDefaultTradeExecutionTypeValue(combo);
															});
													}
													else {
														// A  window opened containing previous trade data will have its the TradeType data in its defaultData.
														combo.selectDefaultValue = false;
														combo.tradeType = win.defaultData.tradeType;
														combo.updateDefaultTradeExecutionTypeValue(combo);
														fp.updateExecutingBrokerCompanyList();
													}
												},
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;

													combo.store.baseParams = {
														tradeTypeId: combo.tradeType.id
													};
												},
												change: function(combo, newValue, oldValue) {
													if (TCG.isBlank(newValue)) {
														combo.updateLimitPriceFieldVisibility(false);
													}
												},
												select: function(combo, record, index) {
													// when selecting a value from here, inhibit the update of the value from within updateTradeExecutionType()
													const fp = combo.getParentForm();
													const f = fp.getForm();
													const holdingAccountField = f.findField('holdingInvestmentAccount.label');
													combo.inhibitUpdate = true;
													holdingAccountField.clearAndReset();
													holdingAccountField.doQuery();
													fp.updateExecutingBrokerCompanyList();
												}
											},

											updateDefaultTradeExecutionTypeValue: function(combo) {
												return TCG.data.getDataPromiseUsingCaching('tradeTypeTradeExecutionTypeListFind.json?tradeTypeId=' + combo.tradeType.id + '&defaultExecutionType=true', combo, 'trade.defaultTradeExecution' + combo.tradeType.id)
													.then(function(tradeTypeTradeExecutionTypeList) {
														if (TCG.isNotBlank(tradeTypeTradeExecutionTypeList) && tradeTypeTradeExecutionTypeList.length > 0) {
															combo.defaultValue = tradeTypeTradeExecutionTypeList[0].referenceTwo;
															combo.doQuery('');
														}
													});
											}
										}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 100,
									items: [
										{name: 'tradeType.id', xtype: 'hidden'},
										{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', hidden: true},
										{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield'},
										{fieldLabel: 'Maturity Date', name: 'investmentSecurity.endDate', xtype: 'datefield', readOnly: true, tabIndex: 1000, submitValue: false},
										{fieldLabel: 'Settlement Date', name: 'settlementDateForDisplay', xtype: 'datefield', submitValue: false, qtip: 'Cash Settlement Date for Maturity (Unless this is an early Termination, Position Settlement Date is the same as Trade Date'},
										{xtype: 'trade-bookingdate'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop', labelWidth: 80},
						{xtype: 'trade-existingPositionsGrid'}
					],


					readOnlyItems: [
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {anchor: '-20'}},
							items: [
								{
									columnWidth: .71,
									items: [
										{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
										{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 60,
									items: [
										{fieldLabel: 'Base CCY', name: 'clientInvestmentAccount.baseCurrency.label', detailIdField: 'clientInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false},
										{fieldLabel: 'Base CCY', name: 'holdingInvestmentAccount.baseCurrency.label', detailIdField: 'holdingInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {anchor: '-20'}},
							items: [
								{
									columnWidth: .31,
									items: [
										{fieldLabel: 'Forward', name: 'investmentSecurity.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id'},
										{fieldLabel: 'Settlement CCY', name: 'payingSecurity.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'payingSecurity.id'},
										{fieldLabel: 'Sell', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
										{fieldLabel: 'Buy', name: 'quantityIntended', xtype: 'currencyfield', readOnly: true},
										{
											fieldLabel: 'Forward Rate', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true,
											listeners: {
												change: function(field, newValue, oldValue) {
													const fp = TCG.getParentFormPanel(field);
													fp.amount_onchange(field, newValue, oldValue);
												}
											}
										}
									]
								},
								{
									columnWidth: .40,
									labelWidth: 105,
									items: [
										{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'},
										{fieldLabel: 'Trade Destination', xtype: 'trade-destination-link'},
										{fieldLabel: 'Execution Type', name: 'tradeExecutionType.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow', detailIdField: 'tradeExecutionType.id'},
										{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 100,
									items: [
										{name: 'tradeType.id', xtype: 'hidden'},
										// Trade group should be here for consistency but is in the middle column to avoid a mismatch in the number of fields in each column
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
										{fieldLabel: 'Maturity Date', name: 'investmentSecurity.endDate', xtype: 'datefield', readOnly: true, submitValue: false},
										{fieldLabel: 'Settlement Date', name: 'settlementDateForDisplay', xtype: 'datefield', readOnly: true, submitValue: false, qtip: 'Cash Settlement Date for Maturity (Unless this is an early Termination, Position Settlement Date is the same as Trade Date'},
										{xtype: 'trade-bookingdate'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop', labelWidth: 80}
					],

					populateClosingPositionBuySell: function(positionRecord, force) {
						const form = this.getForm();
						const quantity = positionRecord.json.quantity;
						const buy = quantity < 0;
						const existingBuySelection = form.items.find(i => i.fieldLabel === 'Buy/Sell').getValue();
						if (TCG.isBlank(existingBuySelection) || TCG.isTrue(force)) {
							form.items.find(i => i.fieldLabel === 'Buy/Sell').items.get((buy) ? 0 : 1).setValue(true);
						}
						else if (existingBuySelection.inputValue !== buy) {
							TCG.showError('Unable to add closing position because it would does not satisfy the current buy/sell selection.', 'Unsupported Position');
							return false;
						}
						return true;
					},

					populateClosingPositionQuantity: function(positionRecord) {
						const formPanel = this;
						const from = formPanel.getFormValue('investmentSecurity.instrument.underlyingInstrument.tradingCurrency');
						const to = formPanel.getFormValue('investmentSecurity.instrument.tradingCurrency');
						formPanel.setFormValue.call(formPanel, 'givenCurrencyForDisplay', {text: from.symbol, value: from.id});
						formPanel.setFormValue.call(formPanel, 'payingSecurity.symbol', {text: to.symbol, value: to.id});
						formPanel.setFormValue.call(formPanel, 'givenAmountForDisplay', Math.abs(positionRecord.json.quantity));
					}
				}]
			},

			{
				title: 'Compliance',
				items: [{
					xtype: 'trade-violations-grid'
				}]
			},

			{
				title: 'Commissions and Fees',
				items: [{
					xtype: 'trade-commissions'
				}]
			},

			{
				title: 'Quotes',
				items: [{
					xtype: 'trade-quotes',
					hideAdditionalAmounts: true
				}]
			}]
	}]
});
