Clifton.trade.TradeAllocationWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Trade Allocation',
	width: 1040,
	height: 600,
	hideCancelButton: true,
	allowOpenFromModal: true,

	saveWindow: function() {
		this.close();
	},

	items: [
		{
			xtype: 'formpanel',

			isGroupAllocation: false,
			shouldConsolidateDataRows: false,
			shouldInsertTotalRows: false,

			getDefaultData: async function(win) {
				const dd = win.defaultData;
				let tradeList = [];
				if (dd.tradeGroupId) {
					// Default data for TradeGroup Allocation
					this.isGroupAllocation = true;
					// get non-cancelled trades for this trade group
					tradeList = await TCG.data.getDataPromise('tradeListFind.json', this, {
						params: {
							tradeGroupId: dd.tradeGroupId,
							excludeWorkflowStateName: 'Cancelled',
							orderBy: 'investmentSecurity.id#holdingInvestmentAccount.id',
							requestedPropertiesRoot: 'data',
							requestedProperties: 'clientInvestmentAccount.name|holdingInvestmentAccount.number|holdingInvestmentAccount.issuingCompany.name|'
								+ 'executingBrokerCompany.name|executingBrokerCompany.label|executingBrokerCompany.id|investmentSecurity.symbol|quantityIntended|openCloseType.label|'
								+ 'averageUnitPrice|commissionPerUnit|workflowState.name|tradeGroup.tradeGroupType.name'
						}
					});
				}
				else if (TCG.isNotBlank(dd.tradeIds)) {
					// Manual Trade Allocation:  get trades in Awaiting Fills state
					tradeList = await TCG.data.getDataPromise('tradeListFind.json', this, {
						params: {
							ids: dd.tradeIds,
							workflowStateName: 'Awaiting Fills',
							requestedPropertiesRoot: 'data',
							requestedProperties: 'id|clientInvestmentAccount.name|holdingInvestmentAccount.number|holdingInvestmentAccount.issuingCompany.name|'
								+ 'executingBrokerCompany.name|executingBrokerCompany.label|executingBrokerCompany.id|investmentSecurity.symbol|quantityIntended|openCloseType.label|'
								+ 'workflowState.name|tradeGroup|tradeDestination.name|uniqueLabel'
						}
					});
				}

				if (tradeList.length > 0) {
					const fp = this;

					fp.shouldConsolidateDataRows = TCG.isNotBlank(dd.shouldConsolidateDataRows) ? dd.shouldConsolidateDataRows : false;
					fp.shouldInsertTotalRows = TCG.isNotBlank(dd.shouldInsertTotalRows) ? dd.shouldInsertTotalRows : false;

					// Set the trade list on the form panel for the load event handling where formValues is not yet set.
					this.tradeList = tradeList;
					if (TCG.isNotNull(tradeList) && Array.isArray(tradeList)) {
						const brokerMap = tradeList
							.map(trade => trade.executingBrokerCompany)
							.reduce((brokersObject, broker) => {
								brokersObject[broker.id] = [broker.name, broker.label, broker.id];
								return brokersObject;
							}, {});
						const brokerList = Object.values(brokerMap);
						this.getForm().findField('brokerLabel').getStore().loadData(brokerList, false);
					}
					// extra validation when in manual trade allocation mode
					if (!this.isGroupAllocation && !fp.validateTradesForManualAllocation()) {
						win.close();
					}
				}
				else {
					if (this.isGroupAllocation) {
						TCG.showInfo('No applicable trades were found for this trade group. All trades may be cancelled.', 'Trade Group Trade Allocation');
					}
					else {
						TCG.showInfo('No manually processed trades were found with a workflow state of "Awaiting Fills"', 'Manual Trade Allocation');
					}
					win.close();
					return;
				}

				return {
					id: dd.id,
					tradeList: tradeList || []
				};
			},

			validateTradesForManualAllocation: function() {
				for (let index = 0; index < this.tradeList.length; ++index) {
					const trade = this.tradeList[index];
					if (TCG.isNotBlank(trade.tradeGroup)) {
						TCG.showError('Trade: [' + trade.uniqueLabel + '] belongs to a trade group of type: ' + trade.tradeGroup.tradeGroupType.label + '.  Manual allocation is not allowed.');
						return false;
					}

					if (TCG.isNotEquals(trade.tradeDestination.name, 'Manual')) {
						TCG.showError('Trade: [' + trade.uniqueLabel + '] does not have a trade destination of "Manual", therefore manual allocation is not allowed.');
						return false;
					}
				}
				return true;
			},
			defaults: {
				anchor: '0'
			},

			loadTradeSummaryAllocationGrid: function(tradeList) {
				if (tradeList && tradeList.length > 0) {
					const childGrid = TCG.getChildByName(this, 'allocationSummaryGrid');
					childGrid.getStore().loadData({tradeList: tradeList});
				}
			},

			items: [
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						layout: 'form',
						columnWidth: .20,
						defaults: {anchor: '-20'}
					},
					items: [
						{
							items: [{html: '&nbsp;'}]
						},
						{
							columnWidth: .60,
							items: [
								{
									fieldLabel: 'Executing Broker', name: 'brokerLabel', hiddenName: 'brokerId', displayField: 'label', valueField: 'id', xtype: 'combo', mode: 'local',
									qtip: 'This field shows the executing brokers used for executing trades of the trade group. If multiple brokers were used for trades within this trade group, the below trade list will be filtered to those applicable to the selected broker.',
									store: {
										xtype: 'arraystore',
										fields: ['name', 'label', 'id'],
										data: [],
										listeners: {
											load: function(store, records, options) {
												const brokerField = store.parent;
												if (store.getTotalCount() > 0) {
													// default to the first executing broker when the arraystore's data is populated
													const record = store.getAt(0);
													brokerField.setValue(record.get(brokerField.valueField));
													brokerField.fireEvent('select', brokerField, record, 0);
												}
											}
										}
									},
									listeners: {
										afterrender: function() {
											this.getStore().parent = this;
										},
										select: function(field, record, index) {
											field.loadChildGrid(record.data['id']);
										}
									},
									loadChildGrid: function(brokerId) {
										const formPanel = TCG.getParentFormPanel(this);
										const form = formPanel.getForm();
										let tradeList = [...TCG.getValue('tradeList', form.formValues || formPanel)];
										tradeList = tradeList.filter(trade => trade.executingBrokerCompany.id === brokerId);
										formPanel.loadTradeSummaryAllocationGrid(tradeList);
									}
								}
							]
						},
						{items: [{html: '&nbsp;'}]}
					]
				},
				{
					xtype: 'panel',
					layout: 'fit',
					items: [
						{
							name: 'allocationSummaryGrid',
							xtype: 'formgrid',
							storeRoot: 'tradeList',
							readOnly: true,
							autoHeight: false,
							height: 475,
							viewConfig: {markDirty: false, forceFit: true},

							columnsConfig: [
								{header: 'Client Account', width: 100, dataIndex: 'clientInvestmentAccount.name'},
								{header: 'Holding Account #', width: 100, dataIndex: 'holdingInvestmentAccount.number'},
								{header: 'Clearing Broker', width: 100, dataIndex: 'holdingInvestmentAccount.issuingCompany.name'},
								{header: 'Executing Broker', width: 100, dataIndex: 'executingBrokerCompany.name'},
								{header: 'Security', width: 100, dataIndex: 'investmentSecurity.symbol'},
								{
									header: 'Open/Close', width: 60, dataIndex: 'openCloseType.label',
									renderer: function(v, metaData) {
										if (TCG.isNotBlank(v)) {
											metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
										}
										return v;
									}
								},
								{header: 'Quantity', width: 60, dataIndex: 'quantityIntended', type: 'float', summaryType: 'sum'},
								{header: 'Commission', width: 60, dataIndex: 'commissionPerUnit', type: 'float', tooltip: 'The commission amount charged for each unit being traded.'}
							],

							listeners: {
								afterrender: function(grid) {
									const fp = TCG.getParentFormPanel(grid);
									if (TCG.isFalse(fp.isGroupAllocation)) {
										const columnModel = grid.getColumnModel();
										columnModel.setHidden(columnModel.findColumnIndex('commissionPerUnit'), true);
									}
								}
							},

							plugins: {
								ptype: 'gridsummary'
							},

							addToolbarButtons: function(toolBar) {
								const gp = this;
								toolBar.add('->');
								const emailWithAttachmentHandler = function() {
									if (gp.getStore().getTotalCount() < 1) {
										TCG.showError('Please select an Executing Broker to generate an allocation report for.', 'Select Executing Broker');
										return;
									}
									const form = TCG.getParentFormPanel(this).getForm();
									//each part needs an empty line between itself and the previous
									const emailBody = '--part\nContent-Type:text/csv\nContent-Disposition:attachment;filename=\"ParametricAllocation.csv\"\n\n'
										+ gp.generateCsvData.call(gp) + '\n\n--part\nContent-Type:text/html\n\n' + gp.generateHtmlData.call(gp);
									TCG.generateEmailFile(form.findField('brokerLabel').lastSelectionText + ' Post-Trade Allocation', 'multipart/mixed;boundary=part', emailBody, 'TradeAllocation.eml');
								};
								toolBar.add({
									text: 'Open Email with Attachment',
									xtype: 'splitbutton',
									iconCls: 'email',
									tooltip: 'Generates a Trade Allocation Email with the table data included in the body of the email and in an attached CSV file.',
									handler: emailWithAttachmentHandler,
									menu: {
										items: [{
											text: 'Open Email',
											iconCls: 'email',
											tooltip: 'Generates a Trade Allocation Email with the table data included in the body of the email.',
											handler: function() {
												if (gp.getStore().getTotalCount() < 1) {
													TCG.showError('Please select an Executing Broker to generate an allocation report for.', 'Select Executing Broker');
													return;
												}
												const form = TCG.getParentFormPanel(this).getForm();
												TCG.generateEmailFile(form.findField('brokerLabel').lastSelectionText + ' Post-Trade Allocation', 'text/html', gp.generateHtmlData.call(gp), 'TradeAllocation.eml');
											}
										}, {
											text: 'Open Email with Attachment',
											iconCls: 'email',
											tooltip: 'Generates a Trade Allocation Email with the table data included in the body of the email and in an attached CSV file.',
											handler: emailWithAttachmentHandler
										}
										]
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Export to CSV',
									iconCls: 'export',
									tooltip: 'Export the table to CSV file',
									handler: function() {
										if (gp.getStore().getTotalCount() < 1) {
											TCG.showError('Please select an Executing Broker to generate an allocation report for.', 'Select Executing Broker');
											return;
										}
										TCG.downloadDataAsFile('text/csv', gp.generateCsvData.call(gp), 'ParametricAllocation.csv');
									}
								});
							},

							generateCsvData: function() {
								const gp = this;
								const fp = TCG.getParentFormPanel(gp);

								let records = gp.getStore().data.items;
								const consolidateRows = fp.isGroupAllocation && fp.shouldConsolidateDataRows;

								let csvContent = '';
								if (fp.isGroupAllocation) {
									csvContent = consolidateRows ? 'Account,Quantity,Commission Per Unit' : 'Account,Quantity,Security,Commission Per Unit';
								}
								else {
									csvContent = 'Account,Quantity,Security,Buy/Sell ';
								}

								if (consolidateRows) {
									records = this.consolidateDataRows(records);
								}

								records.forEach(function(record) {
									const quantity = gp.getFieldValueFromObject(record.data, 'quantityIntended');
									const commissionPerUnit = gp.getNonNullNumberValue(gp.getFieldValueFromObject(record.data, 'commissionPerUnit'));

									// If trades of a spread are being consolidated, include only account, quantity, and commission per unit
									if (consolidateRows) {
										csvContent += '\n\"' + gp.getFieldValueFromObject(record.data, 'holdingInvestmentAccount.number') + '\",\"'
											+ quantity + '\",\"' + commissionPerUnit + '\"';
									}
									else {
										csvContent += '\n\"' + gp.getFieldValueFromObject(record.data, 'holdingInvestmentAccount.number') + '\",\"'
											+ quantity + '\",\"'
											+ gp.getFieldValueFromObject(record.data, 'investmentSecurity.symbol') + '\"';
										if (TCG.isTrue(fp.isGroupAllocation)) {
											csvContent += ',\"' + commissionPerUnit + '\"';
										}
										else {
											csvContent += ',\"' + gp.getFieldValueFromObject(record.data, 'openCloseType.label') + '\"';
										}
									}
								}, gp);
								return csvContent;
							},

							generateHtmlData: function() {
								const gp = this;
								const fp = TCG.getParentFormPanel(gp);
								let totalQuantityForSecurity = 0;

								let allocationTable = '<table><thead><tr><th>Broker</th><th>Account</th><th>Security</th><th>Buy/Sell</th><th>Quantity</th></tr></thead><tbody>';
								if (fp.isGroupAllocation) {
									allocationTable = '<table><thead><tr><th>Broker</th><th>Account</th><th>Security</th><th>Buy/Sell</th><th>Quantity</th><th>Commission Per Unit</th></tr></thead><tbody>';
								}

								const store = gp.getStore();
								let previousSymbol = '';
								let recordIndex = -1;

								store.each(function(record) {
									const quantity = gp.getFieldValueFromObject(record.data, 'quantityIntended');
									const commissionPerUnit = gp.getNonNullNumberValue(gp.getFieldValueFromObject(record.data, 'commissionPerUnit'));

									// check for symbol change to insert a "Total" summary row.  This only occurs once.
									const symbol = gp.getFieldValueFromObject(record.data, 'investmentSecurity.symbol');
									++recordIndex;
									if (fp.shouldInsertTotalRows && TCG.isNotBlank(previousSymbol) && previousSymbol !== symbol) {
										allocationTable = this.createTotalRow(store, totalQuantityForSecurity, fp, allocationTable);
										totalQuantityForSecurity = 0;
									}
									totalQuantityForSecurity += quantity;
									previousSymbol = symbol;

									allocationTable += '<tr><td style="padding:5px">'
										+ gp.getFieldValueFromObject(record.data, 'holdingInvestmentAccount.issuingCompany.name') + '</td><td style="padding:5px">'
										+ gp.getFieldValueFromObject(record.data, 'holdingInvestmentAccount.number') + '</td><td style="padding:5px">'
										+ gp.getFieldValueFromObject(record.data, 'investmentSecurity.symbol') + '</td><td style="padding:5px">'
										+ gp.getFieldValueFromObject(record.data, 'openCloseType.label') + '</td><td style="padding:5px">' + quantity + '</td>';
									if (fp.isGroupAllocation) {
										allocationTable += '<td style="padding:5px">' + commissionPerUnit + '</td>';
									}
									allocationTable += '</tr>';
									// if the condition below exists, this allocation is a single-leg allocation, so we emit the total column as the last column.
									if (fp.shouldInsertTotalRows && recordIndex === store.getTotalCount() - 1) {
										allocationTable = this.createTotalRow(store, totalQuantityForSecurity, fp, allocationTable);
									}
								}, gp);


								return '<!DOCTYPE html><html><head><style>'
									+ 'p{font-family:arial,sans-serif;font-size:14px;}table{border-collapse:collapse;font-family:arial,sans-serif;font-size:14px;}th,td{border:1px solid black;}th{padding:5px;}table#tableInner{border:0;width:100%;}table#tableInner td{border:0;padding:5px;}table#tableInner tr{border-bottom:1px solid black;}table#tableInner tr:last-child{border-bottom:none;}'
									+ '</style></head><body>'
									// allocation table
									+ '<p>Allocation below.</p>' + allocationTable + '</body></html>';
							},

							createTotalRow: function(store, quantityTotal, fp, allocationTable) {
								let allocTable = allocationTable;
								allocTable += '<tr><td></td><td></td><td></td><td style="padding:5px">Total</td><td style="padding:5px">' + quantityTotal + '</td>';
								if (fp.isGroupAllocation) {
									allocTable += '<td></td></tr>';
								}
								return allocTable;
							},


							/**
							 * Takes a list of records and combines spreads with equal leg quantities into a single data row, so that each account shows a single row for a spread.
							 * Note:  Before calling this routine, call shouldConsolidateDataRows(recordList) to determine if the records should be consolidated.
							 *
							 */
							consolidateDataRows: function(recordArray) {
								const gp = this;
								const holdingAccountToRecord = {};

								if (TCG.isBlank(recordArray) || recordArray.length === 0) {
									return Object.values(holdingAccountToRecord);
								}

								recordArray.forEach(record => {
									const currentHoldingAccountNumber = gp.getFieldValueFromObject(record.data, 'holdingInvestmentAccount.number');
									if (TCG.isNull(holdingAccountToRecord[currentHoldingAccountNumber])) {
										holdingAccountToRecord[currentHoldingAccountNumber] = record;
									}
								});

								return Object.values(holdingAccountToRecord);
							},

							getValueFromRecordConditionally: function(currentValue, property, record) {
								return (TCG.isBlank(currentValue) || currentValue === 0) ? TCG.getValue(property, record.json) : currentValue;
							},

							getFieldValueFromObject: function(object, field) {
								if (field) {
									const value = object[field];
									if (!TCG.isBlank(value)) {
										return value;
									}
								}
								return '';
							},

							getNonNullNumberValue: function(value) {
								return TCG.isBlank(value) ? 0 : value;
							},

							getValueForDisplayConditionally: function(condition, value) {
								return condition ? value : '---';
							}
						}
					],
					listeners: {
						afterlayout: TCG.grid.registerDynamicHeightResizing
					}
				}
			]
		}
	]
});

