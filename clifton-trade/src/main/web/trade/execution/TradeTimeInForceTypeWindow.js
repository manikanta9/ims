Clifton.trade.execution.TradeTimeInForceTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Time In Force Type',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'tradeTimeInForceType.json',
		readOnly: true,
		labelWidth: 170,
		instructions: 'The time in force type specifies how long a trade order should stay open while it waits for a fill.',

		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Type Label', name: 'label'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},

			{xtype: 'label', html: '<hr/>'},

			{boxLabel: 'Requires Expiration Date', name: 'expirationDateRequired', xtype: 'checkbox', qtip: 'If checked, the TIF selection requires the Expiration Date to be specified in the order.'}
		]
	}]
});
