Clifton.trade.execution.TradeTypeTradeExecutionTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Execution Type Assignments',
	iconCls: 'shopping-cart',
	height: 240,
	width: 800,

	items: [{
		xtype: 'formpanel',
		url: 'tradeTypeTradeExecutionType.json',
		instructions: 'This screen allows for the addition of new Trade Execution Types associated with this trade type.',
		readOnly: false,
		labelWidth: 170,
		items: [
			{fieldLabel: 'Trade Type', name: 'referenceOne.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TypeWindow', detailIdField: 'referenceOne.id'},
			{fieldLabel: 'Trade Execution Type', name: 'referenceTwo.name', hiddenName: 'referenceTwo.id', xtype: 'combo', url: 'tradeExecutionTypeListFind.json'},
			{boxLabel: 'This trade execution type is the default execution type for this trade type.', name: 'defaultExecutionType', xtype: 'checkbox'}
		]
	}]
});
