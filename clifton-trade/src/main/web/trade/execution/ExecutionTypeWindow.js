Clifton.trade.execution.ExecutionTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Execution Type',
	iconCls: 'shopping-cart',
	height: 500,
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'tradeExecutionType.json',
					instructions: 'The trade execution type represents an execution mode which can optionally be applied to a trade to designate it as a particular trade such as' +
						'Continuous Linked Settlement (CLS) trade, Market On Close (MOC) trade, BTIC or Limit trade.',
					labelWidth: 180,
					items: [
						{fieldLabel: 'Execution Type', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Required Holding Account Type', name: 'requiredHoldingInvestmentAccountType.name', hiddenName: 'requiredHoldingInvestmentAccountType.id',
							qtip: 'This trade execution type requires the trade to use a holding account of this type.', displayField: 'label',
							xtype: 'combo', url: 'investmentAccountTypeListFind.json', detailPageClass: 'Clifton.investment.account.AccountTypeWindow'
						},
						{
							fieldLabel: 'Exclude Holding Account Type', name: 'excludeHoldingInvestmentAccountType.name', hiddenName: 'excludeHoldingInvestmentAccountType.id',
							qtip: 'This trade execution type requires that the trade does not use a holding account of this type.', displayField: 'label',
							xtype: 'combo', url: 'investmentAccountTypeListFind.json', detailPageClass: 'Clifton.investment.account.AccountTypeWindow'
						},
						{
							fieldLabel: 'Executing Broker Filter', name: 'executingBrokerFilter.name', hiddenName: 'executingBrokerFilter.id',
							qtip: 'Defines a system condition that will include a specific holding account if the result is true otherwise it will exclude it.',
							xtype: 'combo', url: 'systemConditionListFind.json?systemTableName=BusinessCompany', detailPageClass: 'Clifton.system.condition.ConditionWindow'
						},

						{xtype: 'label', html: '<hr/>'},
						{boxLabel: 'This trade execution type represents a Market On Close (MOC) trade.', name: 'marketOnCloseTrade', xtype: 'checkbox'},
						{boxLabel: 'This trade execution type represents a Basis Trade at Index Close (BTIC) trade.', name: 'bticTrade', xtype: 'checkbox'},
						{boxLabel: 'This trade execution type represents a Continuous Linked Settlement (CLS) trade.', name: 'clsTrade', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Trade Types',
				items: [{
					name: 'tradeTypeTradeExecutionTypeListFind',
					xtype: 'gridpanel',
					readOnly: false,
					instructions: 'Below are the trade execution types that are allowed for use with this trade type. Trades using this trade type will be limited to the trade execution types listed below.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Type', width: 100, dataIndex: 'referenceOne.name'},
						{header: 'Default Execution Type', width: 40, dataIndex: 'defaultExecutionType', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {tradeExecutionTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.trade.execution.TradeExecutionTypeTradeTypeWindow',
						getDeleteURL: function() {
							return 'tradeTypeTradeExecutionTypeDelete.json';
						},
						getDefaultData: function(gridPanel) {
							return {referenceTwo: gridPanel.getWindow().getMainForm().formValues};
						}
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'TradeExecutionType'
				}]
			}
		]
	}]
});
