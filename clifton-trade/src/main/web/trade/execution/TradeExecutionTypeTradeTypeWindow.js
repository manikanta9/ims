Clifton.trade.execution.TradeExecutionTypeTradeTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Type Assignments',
	iconCls: 'shopping-cart',
	height: 250,
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'tradeTypeTradeExecutionType.json',
		instructions: 'This screen allows new Trade Types to be associated with this trade execution type.',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Trade Execution Type', name: 'referenceTwo.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow', detailIdField: 'referenceTwo.id'},
			{fieldLabel: 'Trade Type', name: 'referenceOne.name', hiddenName: 'referenceOne.id', xtype: 'combo', url: 'tradeTypeListFind.json', detailPageClass: 'Clifton.trade.TypeWindow'},
			{boxLabel: 'Default Execution Type for the selected Trade Type (only one default per trade type).', name: 'defaultExecutionType', xtype: 'checkbox'}
		]
	}]
});
