Clifton.trade.accounting.commission.TradeCommissionOverrideWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Trade Commission Override',
	iconCls: 'fee',
	height: 320,
	width: 500,

	items: [{
		xtype: 'formpanel',
		url: 'tradeCommissionOverride.json',
		instructions: 'Commission/Fee overrides are used to override default Commission Schedules. They should be used rarely for unusual circumstances.',
		labelWidth: 140,
		getDefaultData: function(win) {
			return win.defaultData;
		},
		listeners: {
			afterrender: function(fp, isClosing) {
				//
			},
			afterload: function(fp, isClosing) {
				//
			}
		},
		items: [
			{name: 'trade.id', xtype: 'hidden'},
			{
				fieldLabel: 'GL Account', name: 'accountingAccount.name', hiddenName: 'accountingAccount.id', displayField: 'name', xtype: 'combo', disableAddNewItem: true,
				url: 'accountingAccountListFind.json?commission=true', detailPageClass: 'Clifton.accounting.account.AccountWindow',
				qtip: 'Selected GL Account (the list is restricted to "Commission" GL Accounts) will be used for booking to the General Ledger.'
			},
			{
				fieldLabel: 'Commission Currency', name: 'commissionCurrency.name', hiddenName: 'commissionCurrency.id', displayField: 'name', xtype: 'combo', disableAddNewItem: true,
				url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				qtip: 'Indicates the currency type of the commission. This should usually be left blank, which indicates that the currency denomination of the security is used.'
			},
			{
				fieldLabel: 'Amount Per Unit', name: 'amountPerUnit', xtype: 'floatfield',
				qtip: 'An amount applied on a per-unit basis. <b>This amount should be entered as a positive value</b>. Note: For round-turn commissions, this amount is applied to both the opening and closing trades.'
			},
			{
				fieldLabel: 'Flat Amount', name: 'totalAmount', xtype: 'floatfield',
				qtip: 'A flat commission/fee applied to the trade. <b>The amount should be entered as a negative value.</b>'
			},
			{fieldLabel: 'Note', name: 'overrideNote', xtype: 'textarea'}
		]
	}]
});
