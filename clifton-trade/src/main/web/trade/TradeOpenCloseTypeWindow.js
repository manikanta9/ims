Clifton.trade.TradeOpenCloseTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Open/Close Type',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'tradeOpenCloseType.json',
		readOnly: true,
		labelWidth: 170,
		instructions: 'When possible, more specific open/close type should be used. This will result in additional validation during trade booking. Individual Trade Types can be configured to limit the choice to a sub-set of these types.',

		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Type Label', name: 'label'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},

			{xtype: 'label', html: '<hr/>'},

			{boxLabel: 'Buy', name: 'buy', xtype: 'checkbox'},
			{boxLabel: 'Open', name: 'open', xtype: 'checkbox'},
			{boxLabel: 'Close', name: 'close', xtype: 'checkbox'}
		]
	}]
});
