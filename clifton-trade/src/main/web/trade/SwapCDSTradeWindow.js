TCG.use('Clifton.trade.SwapTradeFormBase');

Clifton.trade.SwapCDSTradeForm = Ext.extend(Clifton.trade.SwapTradeFormBase, {

	investmentTypeSubType: 'Credit Default Swaps',

	afterRenderApplyAdditionalDefaultData: function(formPanel, formValues) {
		if (formValues.originalFace) {
			formPanel.updateFromTradeDateChange();
		}

		formPanel.afterRenderApplyAdditionalDefaultCounterparty(formPanel, formValues);
	},

	updateFromTradeDateChange: function() {
		const fp = this;
		fp.updateAccruedInterest()
			.then(function() {
				return fp.updateCurrentCoupon();
			})
			.then(function() {
				return fp.updateCurrentFactor();
			})
			.then(function() {
				fp.updateCalculatedValues('originalFace');
			});
	},

	updateFromSecuritySelection: function(securityId) {
		this.updateCurrentCoupon(securityId);
		this.updateCurrentFactor(securityId);
	},

	updateCalculatedValues: function(updatedField, newValue, oldValue) {
		const fp = this;
		const f = fp.getForm();

		const securityId = f.findField('investmentSecurity.id').value;
		const orig = f.findField('originalFace').getNumericValue();
		const purchase = f.findField('quantityIntended').getNumericValue();
		const factor = f.findField('currentFactor').getNumericValue();

		if (!Ext.isNumber(factor) && Ext.isNumber(purchase) && Ext.isNumber(orig)) {
			fp.setFormValue('currentFactor', purchase / orig, true);
		}
		Promise.resolve()
			.then(function() {
				if (Ext.isNumber(factor) && Ext.isNumber(orig) && (!Ext.isNumber(purchase) || (updatedField === 'originalFace' || updatedField === 'currentFactor'))) {
					return TCG.data.getDataValuePromise('investmentMultiplyAndRound.json?value1=' + orig + '&value2=' + factor + '&scale=2', fp)
						.then(function(amount) {
							fp.setFormValue('quantityIntended', amount, true); // need to round up, not truncate
						});
				}
			})
			.then(function() {
				if ((TCG.isNull(updatedField) && !f.findField('accrualAmount1').value) || updatedField === 'quantityIntended' || updatedField === 'currentFactor' || updatedField === 'originalFace' || updatedField === 'buy') {
					return fp.updateAccruedInterest();
				}
			})
			.then(function() {
				if (updatedField === 'averageUnitPrice' || updatedField === 'quantityIntended' || updatedField === 'currentFactor' || updatedField === 'originalFace' || updatedField === 'buy') {
					const price = f.findField('averageUnitPrice').getNumericValue();
					const qty = f.findField('quantityIntended').getNumericValue();
					if (Ext.isNumber(price) && Ext.isNumber(qty)) {
						// accounts for rounding: JPY, etc.
						return TCG.data.getDataValuePromise('investmentSecurityNotional.json?securityId=' + securityId + '&price=' + price + '&quantity=' + qty, fp)
							.then(function(notional) {
								fp.setFormValue('accountingNotional', (f.findField('buy').getValue() ? -1 : 1) * notional);
							});
					}
				}
			})
			.then(function() {
				const principal = f.findField('accountingNotional').getNumericValue();
				const interest = f.findField('accrualAmount1').getNumericValue();
				const commission = f.findField('commissionAmount').getNumericValue() || 0;
				const fee = f.findField('feeAmount').getNumericValue() || 0;
				fp.setFormValue('upfrontPayment', principal + interest, true);
				fp.setFormValue('totalPayment', principal + interest + commission + fee, true);

				fp.updateCurrentCoupon();
				Clifton.trade.updateExistingPositions(fp);
			});
	},

	updateAccruedInterest: function() { // NOTE: CDS accrues up to and including Trade Date vs bonds that are up to and excluding Settlement Date
		const fp = this;
		const f = fp.getForm();
		const securityId = f.findField('investmentSecurity.id').value;
		const purchaseFace = f.findField('quantityIntended').getNumericValue();
		const tradeDate = f.findField('tradeDate').value;
		return Promise.resolve()
			.then(function() {
				if (tradeDate && TCG.isNotNull(securityId) && Ext.isNumber(purchaseFace) && f.findField('accrualAmount1').isVisible()) {
					return TCG.data.getDataValuePromise('tradeAccrualAmount.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(f.el.dom, true), fp)
						.then(function(ai) {
							if (ai !== undefined) { // no errors
								fp.setFormValue('accrualAmount1', ai, true);
							}
						});
				}
			});
	},

	updateCurrentCoupon: function(securityId) {
		const fp = this;
		const f = fp.getForm();
		return Promise.resolve()
			.then(function() {
				if (f.findField('currentCoupon').isVisible()) {
					return TCG.data.getDataPromise('investmentSecurityEventForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue', fp, {
						params: {
							securityId: securityId || f.findField('investmentSecurity.id').value,
							typeName: 'Premium Leg Payment',
							accrualEndDate: f.findField('tradeDate').value
						}
					})
						.then(function(event) {
							fp.setFormValue('currentCoupon', event ? event.afterEventValue : 0, true);
						});
				}
			});
	},

	updateCurrentFactor: function(securityId) {
		const fp = this;
		const f = fp.getForm();
		const factorField = f.findField('currentFactor');
		return Promise.resolve()
			.then(function() {
				if (factorField.isVisible()) {
					return TCG.data.getDataPromise('investmentSecurityEventWithLatestExDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue', fp, {
						params: {
							securityId: securityId || f.findField('investmentSecurity.id').value,
							typeName: 'Credit Event',
							valuationDate: f.findField('tradeDate').value
						}
					})
						.then(function(event) {
							factorField.setValue(event ? event.afterEventValue : 1);
						});
				}
			});
	},

	setSecurityCustomFields: function(values) {
		const fp = this;
		let v = TCG.getArrayElement(values, 'column.name', 'Day Count');
		if (v) {
			fp.setFormValue('daycountConvention', v.text, true);
		}
		v = TCG.getArrayElement(values, 'column.name', 'Coupon Frequency');
		if (v) {
			fp.setFormValue('couponFrequency', v.text, true);
		}
	},

	securityEntryItems: [{
		xtype: 'panel',
		layout: 'column',
		labelWidth: 120,
		items: [
			{
				columnWidth: .42,
				layout: 'form',
				defaults: {xtype: 'textfield'},
				items: [
					{
						xtype: 'radiogroup',
						fieldLabel: 'Buy/Sell Protection',
						columns: [60, 60],
						allowBlank: false,
						anchor: '-114',
						items: [
							{boxLabel: 'Buy', name: 'buy', inputValue: true},
							{boxLabel: 'Sell', name: 'buy', inputValue: false}
						],
						listeners: {
							change: function(radioGroup, checkedRadio) {
								const formPanel = TCG.getParentFormPanel(radioGroup);
								formPanel.updateCalculatedValues.call(formPanel, 'buy');

								const params = {
									tradeTypeId: formPanel.getFormValue('tradeType.id'),
									buy: checkedRadio.getRawValue()
								};
								TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params})
									.then(function(openCloseType) {
										if (openCloseType && openCloseType.length === 1) {
											formPanel.getForm().findField('openCloseType.id').setValue(openCloseType[0].id);
										}
									});
							}
						}
					},
					{xtype: 'hidden', name: 'openCloseType.id'},

					{
						fieldLabel: 'Original Notional', name: 'originalFace', xtype: 'currencyfield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'originalFace', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Factor', name: 'currentFactor', xtype: 'floatfield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'currentFactor', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Adjusted Notional', name: 'quantityIntended', xtype: 'currencyfield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'quantityIntended', newValue, oldValue);
							}
						}
					},
					{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', qtip: 'The value to exchange one unit of the security\'s currency denomination for one unit of the client account\'s base currency. If settlement currency of the trade does not match the security\'s or client\'s currency, then the system will calculate the crossing rate value via the exchange rate on the trade multiplied by default data source exchange rate from settlement currency on the trade to client account\'s base currency.'}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				labelWidth: 100,
				defaults: {anchor: '-20'},
				items: [
					{
						fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'pricefield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.getForm().findField('averageUnitPrice').setValue(field.getNumericValue());
								fp.updateCalculatedValues.call(fp, 'averageUnitPrice', newValue, oldValue);
							}
						}
					},
					{fieldLabel: 'Coupon', name: 'currentCoupon', xtype: 'floatfield', readOnly: true},
					{fieldLabel: 'Frequency', name: 'couponFrequency', xtype: 'displayfield', readOnly: true},
					{fieldLabel: 'Day Count', name: 'daycountConvention', xtype: 'displayfield', readOnly: true},
					{
						fieldLabel: 'Accrued Interest', name: 'accrualAmount1', xtype: 'currencyfield', allowBlank: false, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)',
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'accrualAmount1', newValue, oldValue);
							}
						}
					}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{fieldLabel: 'Current Principal', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Upfront Payment', name: 'upfrontPayment', xtype: 'currencyfield', readOnly: true, qtip: 'Upfront Payment = Principal + Accrued Interest<br /><br />Note: initial reconciliation does not include commissions and fees'},
					{
						fieldLabel: 'Commission Amount', name: 'commissionAmount', xtype: 'currencyfield',
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'commissionAmount', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield',
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'feeAmount', newValue, oldValue);
							}
						}
					},
					{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Settlement Total = Current Principal + Accrued Interest + Commission Amount + Fee'}
				]
			}
		]
	}],

	securityEntryItems_readOnly: [{
		xtype: 'panel',
		layout: 'column',
		labelWidth: 120,
		items: [
			{
				columnWidth: .42,
				layout: 'form',
				defaults: {xtype: 'textfield'},
				items: [
					{
						fieldLabel: 'Buy/Sell Protection', name: 'buy', xtype: 'displayfield', width: 139,
						setRawValue: function(v) {
							this.el.addClass(v ? 'buy' : 'sell');
							TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
						}
					},
					{fieldLabel: 'Original Notional', name: 'originalFace', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Factor', name: 'currentFactor', xtype: 'floatfield', readOnly: true},
					{fieldLabel: 'Adjusted Notional', name: 'quantityIntended', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', readOnly: true, qtip: 'The value to exchange one unit of the security\'s currency denomination for one unit of the client account\'s base currency. If settlement currency of the trade does not match the security\'s or client\'s currency, then the system will calculate the crossing rate value via the exchange rate on the trade multiplied by default data source exchange rate from settlement currency on the trade to client account\'s base currency.'}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				labelWidth: 100,
				defaults: {xtype: 'floatfield', width: 110},
				items: [
					{fieldLabel: 'Price', name: 'averageUnitPrice', readOnly: true},
					{fieldLabel: 'Coupon', name: 'currentCoupon', readOnly: true},
					{fieldLabel: 'Frequency', name: 'couponFrequency', xtype: 'displayfield', readOnly: true},
					{fieldLabel: 'Day Count', name: 'daycountConvention', xtype: 'displayfield', readOnly: true},
					{fieldLabel: 'Accrued Interest', name: 'accrualAmount1', xtype: 'currencyfield', readOnly: true, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{fieldLabel: 'Current Principal', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Upfront Payment', name: 'upfrontPayment', xtype: 'currencyfield', readOnly: true, qtip: 'Upfront Payment = Principal + Accrued Interest<br /><br />Note: initial reconciliation does not include commissions and fees'},
					{fieldLabel: 'Commission Amount', name: 'commissionAmount', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Fee', name: 'feeAmount', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Settlement Total = Current Principal + Accrued Interest + Commission Amount + Fee'}
				]
			}
		]
	}],

	populateClosingPositionBuySell: function(positionRecord, force) {
		const form = this.getForm();
		const quantity = this.getClosingPositionQuantity.call(this, positionRecord);
		const buy = quantity < 0;
		const existingBuySelection = form.items.find(i => i.fieldLabel === 'Buy/Sell Protection').getValue();
		if (TCG.isBlank(existingBuySelection) || TCG.isTrue(force)) {
			form.items.find(i => i.fieldLabel === 'Buy/Sell Protection').items.get((buy) ? 0 : 1).setValue(true);
		}
		else if (existingBuySelection.inputValue !== buy) {
			TCG.showError('Unable to add closing position because it would does not satisfy the current buy/sell selection.', 'Unsupported Position');
			return false;
		}
		return true;
	},

	populateClosingPositionQuantity: function(positionRecord) {
		this.setFormValue.call(this, 'originalFace', Math.abs(this.getClosingPositionQuantity.call(this, positionRecord)));
	}
});
Ext.reg('trade-swap-cds-form', Clifton.trade.SwapCDSTradeForm);


Clifton.trade.SwapCDSTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 1050,
	height: 580,
	iconCls: 'swap',


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,

		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.trade.DefaultTradeWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.trade.DefaultTradeWindowAdditionalTabs[i]);
				}
			}
		},

		items: [
			{
				title: 'Info',
				tbar: {xtype: 'trade-workflow-toolbar'},

				items: [{
					xtype: 'trade-swap-cds-form'
				}]
			},


			{
				title: 'Compliance',
				items: [{
					xtype: 'trade-violations-grid'
				}]
			},


			{
				title: 'Trade Fills',
				items: [{xtype: 'trade-fillsgrid'}]
			},


			{
				title: 'Commissions and Fees',
				items: [{
					xtype: 'trade-commissions'
				}]
			},


			{
				title: 'Coupons',
				items: [{
					name: 'investmentSecurityEventListFind',
					xtype: 'gridpanel',
					instructions: 'Coupon payments are made by protection buyer. Payments are represented as cash percentage of Adjusted Notional owned.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Coupon %', width: 50, dataIndex: 'afterEventValue', type: 'float'},
						{header: 'Accrual Start', width: 50, dataIndex: 'declareDate'},
						{header: 'Accrual End', width: 50, dataIndex: 'recordDate'},
						{header: 'Ex Date', width: 50, dataIndex: 'exDate'},
						{
							header: 'Payment Date', width: 50, dataIndex: 'eventDate', defaultSortColumn: true, defaultSortDirection: 'DESC',
							renderer: function(v, metaData, r) {
								const value = TCG.renderDate(v);
								if (r.json.createDate !== r.json.updateDate) {
									return '<div class="amountAdjusted" qtip="Coupon Data was updated. See Audit Trail for details.">' + value + '</div>';
								}
								return value;
							}
						},
						{header: 'Description', width: 100, dataIndex: 'eventDescription'}
					],
					getLoadParams: function() {
						return {
							securityId: this.getWindow().getMainFormPanel().getFormValue('investmentSecurity.id'),
							typeName: 'Premium Leg Payment'
						};
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
						getDefaultData: function(gridPanel) {
							return TCG.data.getDataPromise('investmentSecurityEventTypeByName.json?name=Premium Leg Payment', gridPanel, 'investment.security.event.interestLegPayment')
								.then(function(eventType) {
									return {
										security: gridPanel.getWindow().getMainForm().formValues.investmentSecurity,
										type: eventType
									};
								});
						}
					}
				}]
			},


			{
				title: 'Credit Events',
				items: [{
					name: 'investmentSecurityEventListFind',
					xtype: 'gridpanel',
					instructions: 'For credit default swaps, credit event represents the reduction of outstanding notional usually due to bankruptcy. Value of 1 represents full notional and value of 0 represents no notional left.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Previous Factor', width: 60, dataIndex: 'beforeEventValue', type: 'float'},
						{header: 'New Factor', width: 50, dataIndex: 'afterEventValue', type: 'float'},
						{header: 'Recovery %', width: 45, dataIndex: 'additionalEventValue', type: 'float', useNull: true},
						{header: 'Default Date', width: 50, dataIndex: 'additionalDate'},
						{header: 'Accrual Start', width: 48, dataIndex: 'declareDate', hidden: true},
						{header: 'Accrual End', width: 48, dataIndex: 'recordDate', hidden: true},
						{header: 'Ex Date', width: 50, dataIndex: 'exDate', hidden: true},
						{
							header: 'Payment Date', width: 50, dataIndex: 'eventDate', defaultSortColumn: true, defaultSortDirection: 'DESC',
							renderer: function(v, metaData, r) {
								const value = TCG.renderDate(v);
								if (r.json.createDate !== r.json.updateDate) {
									return '<div class="amountAdjusted" qtip="Factor Data was updated. See Audit Trail for details.">' + value + '</div>';
								}
								return value;
							}
						},
						{header: 'Description', width: 100, dataIndex: 'eventDescription'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.event.SecurityEventWindow',
						getDefaultData: function(gridPanel) {
							return TCG.data.getDataPromise('investmentSecurityEventTypeByName.json?name=Credit Event', gridPanel, 'investment.security.event.creditEvent')
								.then(function(eventType) {
									return {
										security: gridPanel.getWindow().getMainForm().formValues.investmentSecurity,
										type: eventType
									};
								});
						}
					},
					getLoadParams: function() {
						return {
							securityId: this.getWindow().getMainFormPanel().getFormValue('investmentSecurity.id'),
							typeName: 'Credit Event'
						};
					}
				}]
			},


			{
				title: 'Quotes',
				items: [{
					xtype: 'trade-quotes'
				}]
			}
		]
	}]
});
