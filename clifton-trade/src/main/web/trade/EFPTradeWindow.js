TCG.use('Clifton.trade.group.TradeGroupNoteDragAndDropPanel');

Clifton.trade.EFPTradeForm = Ext.extend(TCG.form.FormPanel, {
	url: 'tradeGroup.json?enableOpenSessionInView=true',
	labelFieldName: 'id',
	loadValidation: false,
	submitEmptyText: true,
	labelWidth: 120,
	defaults: {anchor: '0'},

	listRequestedProperties: 'id|workflowStatus.name|workflowState.name|clientInvestmentAccount.label|clientInvestmentAccount.id|holdingInvestmentAccount.label|holdingInvestmentAccount.id|tradeAllocationDescription|buy|quantityIntended|investmentSecurity.id|investmentSecurity.label|orderIdentifier|averageUnitPrice|commissionPerUnit|tradeDate|settlementDate|tradeType.id|tradeType.name|tradeDestination.name|tradeDestination.id|tradeDestination.type.createOrder|executingBrokerCompany.id|executingBrokerCompany.label|payingSecurity.id|traderUser.id|description|accountingNotional|exchangeRateToBase|tradeFillList',
	listRequestedPropertiesRoot: 'data.tradeList',

	getSubmitParams: function() {
		const f = this.getForm();
		const groupId = f.findField('id').getValue();
		const params = {};
		if (TCG.isBlank(groupId)) {
			const exchangeTradeBuy = f.findField('tradeList[1].buy').getRawValue();
			const descriptionField = f.findField('tradeList[0].description');
			if (descriptionField) {
				const description = descriptionField.getValue();
				if (TCG.isNotBlank(description)) {
					f.findField('tradeList[1].description').setValue(description);
				}
			}

			params['tradeList[1].buy'] = exchangeTradeBuy === 'BUY';
			params['tradeList[0].enableCommissionOverride'] = true;
			params['tradeList[1].enableCommissionOverride'] = true;

			// add populated fill fields to the requested save
			const fillFields = [
				'tradeList[0].tradeFillList[0].notionalUnitPrice',
				'tradeList[0].tradeFillList[0].quantity',
				'tradeList[0].tradeFillList[1].notionalUnitPrice',
				'tradeList[0].tradeFillList[1].quantity'
			];
			for (let i = 0; i < fillFields.length; i++) {
				const fillField = f.findField(fillFields[i]);
				if (TCG.isNotBlank(fillField.getValue())) {
					params[fillField.name] = fillField.getValue();
				}
			}
		}
		else if (TCG.isFalse(f.findField('tradeList[0].id').submitValue)) {
			params['tradeList[0].id'] = f.findField('tradeList[0].id').getValue();
			params['tradeList[1].id'] = f.findField('tradeList[1].id').getValue();
		}

		return params;
	},

	getSaveURL: function() {
		if (TCG.isNotBlank(this.getFormValue('id'))) {
			if (TCG.isBlank(this.getFormValue('tradeGroupAction'))) {
				this.setFormValue('tradeGroupAction', 'SAVE_TRADE');
			}
			return 'tradeGroupActionExecute.json?enableOpenSessionInView=true&enableValidatingBinding=true';
		}
		else {
			return 'tradeGroupSave.json';
		}
	},


	getDefaultData: function(win) {
		let dd = win.defaultData || {};
		if (win.defaultDataIsReal) {
			return dd;
		}

		const traderUser = TCG.getCurrentUser();
		const now = new Date().format('Y-m-d 00:00:00');
		const defaultTradeDestination = TCG.data.getData('tradeDestinationByName.json?name=Manual', this, 'trade.tradeDestination.Manual');
		const futuresTrade = this.defaultTradeData('Futures', '', traderUser, now, defaultTradeDestination);
		const exchangeTrade = this.defaultTradeData('Funds', 'Exchange Traded Funds', traderUser, now, defaultTradeDestination);
		const destinationMappingDefault = TCG.data.getData('tradeDestinationMappingByCommand.json?defaultDestinationMapping=true&tradeTypeId='
			+ futuresTrade.tradeType.id + '&activeOnDate=' + new Date().format('m/d/Y'), this, 'trade.type.destinationMappingDefault.' + futuresTrade.tradeType.name + '.' + new Date().format('m/d/Y'));
		const executingBrokerCompany = destinationMappingDefault && destinationMappingDefault.executingBrokerCompany
			? {id: destinationMappingDefault.executingBrokerCompany.id, label: destinationMappingDefault.executingBrokerCompany.label} : {};
		futuresTrade['executingBrokerCompany'] = executingBrokerCompany;
		exchangeTrade['executingBrokerCompany'] = executingBrokerCompany;

		const tradeList = [futuresTrade, exchangeTrade];
		const tradeGroupType = TCG.data.getData('tradeGroupTypeByName.json?name=Exchange For Physical', this);

		dd = Ext.apply({
			tradeList: tradeList,
			traderUser: traderUser,
			tradeDate: now,
			tradeGroupType: tradeGroupType
		}, dd);
		return dd;
	},

	defaultTradeData: function(tradeType, tradeSubType, tradeUser, tradeDate, defaultTradeDestination) {
		const tt = TCG.data.getData('tradeTypeByName.json?name=' + tradeType, this, 'trade.type.' + tradeType);

		return {
			class: 'com.clifton.trade.Trade',
			tradeType: {
				id: tt.id,
				name: tt.name,
				investmentGroup: tt.investmentGroup ? {id: tt.investmentGroup.id} : {},
				investmentType: {id: tt.investmentType.id},
				holdingAccountPurpose: {name: tt.holdingAccountPurpose.name}
			},
			tradeTypeSubType: tradeSubType,
			tradeDestination: {
				id: defaultTradeDestination.id,
				name: defaultTradeDestination.name
			},
			tradeDate: tradeDate,
			traderUser: {
				id: tradeUser.id
			}
		};
	},

	executeAction: function(groupAction) {
		const form = this.getForm();
		form.findField('tradeGroupAction').setValue(groupAction);
		this.getWindow().saveWindow(false);
	},

	getLoadParams: function(win) {
		let params = win.params;
		if (!params) {
			params = {id: this.getFormValue('id')};
		}
		params.requestedProperties = this.listRequestedProperties;
		params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
		return params;
	},

	getLoadURL: function() {
		const w = this.getWindow();
		if (this.url && ((w && w.params) || (TCG.isNotBlank(this.getIdFieldValue())))) {
			return this.url;
		}
		return false;
	},

	reload: function() {
		const fp = this;
		const url = fp.getLoadURL();
		if (TCG.isFalse(url)) {
			return;
		}
		const win = fp.getWindow();
		fp.load(Ext.applyIf({
			url: encodeURI(url),
			params: fp.getLoadParams(win),
			waitMsg: 'Loading...',
			success: function(form, action) {
				fp.loadJsonResult(action.result, true);
				fp.updateReadOnlyMode.call(fp);
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	addToolbarButtons: function(toolBar) {
		const fp = this;

		toolBar.add({
			text: 'Approve Trades',
			tooltip: 'Approve Trades.',
			iconCls: 'verify',
			scope: fp,
			handler: function() {
				fp.executeAction('APPROVE');
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Reload',
			tooltip: 'Reload trades for this trade group.',
			iconCls: 'table-refresh',
			scope: fp,
			handler: function() {
				fp.reload();
			}
		});
		toolBar.add('->');
		toolBar.add({
			text: 'Execute Trades',
			tooltip: 'Execute Trades.',
			iconCls: 'run',
			scope: fp,
			handler: function() {
				fp.executeAction('EXECUTE');
			}
		});
		toolBar.add('-');

		const moreActionsMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'Validate Trades',
					iconCls: 'row_reconciled',
					tooltip: 'Validate Trades',
					scope: fp,
					handler: function() {
						fp.executeAction('VALIDATE');
					}
				},
				{
					text: 'Import Fills',
					iconCls: 'import',
					tooltip: 'Import Trade Fills for Existing Trades',
					handler: function() {
						TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
					}
				}, '-',
				{
					text: 'Book & Post',
					tooltip: 'Book & Post Trades.',
					iconCls: 'book-red',
					scope: fp,
					handler: function() {
						fp.executeAction('BOOK');
					}
				},
				{
					text: 'Unbook',
					tooltip: 'Unbook Trades.',
					iconCls: 'undo',
					scope: fp,
					handler: function() {
						fp.executeAction('UNBOOK');
					}
				}, '-',
				{
					text: 'Reject Trades',
					tooltip: 'Reject Trades For Edit.',
					iconCls: 'run',
					scope: fp,
					handler: function() {
						fp.executeAction('REJECT');
					}
				},
				{
					text: 'Delete Fills',
					tooltip: 'Delete all Fills for Trades.',
					iconCls: 'remove',
					scope: fp,
					handler: function() {
						fp.executeAction('DELETE_FILLS');
					}
				},
				{
					text: 'Cancel Trades',
					tooltip: 'Cancel Trades',
					iconCls: 'cancel',
					scope: fp,
					handler: function() {
						fp.executeAction('CANCEL');
					}
				}
			]
		});

		toolBar.add({
			text: 'More Actions',
			iconCls: 'workflow',
			menu: moreActionsMenu
		});
	},

	updateFromTradeDateChange: function(date) {
		const form = this.getForm();
		if (TCG.isBlank(date)) {
			return;
		}
		const settlementDateFieldValueSetter = function(settlementDate) {
			if (this) {
				this.setValue(settlementDate);
			}
		};
		for (let i = 0; i < form.formValues.tradeList.length; i++) {
			form.findField('tradeList[' + i + '].tradeDate').setValue(date);
			const securityId = form.findField('tradeList[' + i + '].investmentSecurity.id').getValue();
			const settlementDateField = form.findField('tradeList[' + i + '].settlementDate');
			Clifton.investment.instrument.getSettlementDatePromise(securityId, date, null, null, this)
				.then(settlementDateFieldValueSetter.bind(settlementDateField));
		}
	},

	getSecurityQueryParams: function(tradeListIndex) {
		const f = this.getForm();
		const params = {
			investmentTypeId: this.getFormValue('tradeList[' + tradeListIndex + '].tradeType.investmentType.id'),
			activeOnDate: f.findField('tradeDate').value
		};
		const tradeTypeSubType = this.getFormValue('tradeList[' + tradeListIndex + '].tradeTypeSubType');
		if (TCG.isNotBlank(tradeTypeSubType)) {
			params.investmentTypeSubType = tradeTypeSubType;
		}
		const groupId = this.getFormValue('tradeList[' + tradeListIndex + '].tradeType.investmentGroup.id');
		if (groupId) {
			params.investmentGroupId = groupId;
		}
		return params;
	},

	getSecurityDefaultData: function(tradeListIndex) {
		const f = this.getForm();
		const ha = TCG.getValue('tradeList[' + tradeListIndex + '].holdingInvestmentAccount', f.formValues);
		const td = f.findField('tradeDate').getValue().format('Y-m-d 00:00:00');
		const investmentGroupId = TCG.getValue('tradeList[' + tradeListIndex + '].tradeType.investmentGroup.id', f.formValues);
		const investmentTypeId = TCG.getValue('tradeList[' + tradeListIndex + '].tradeType.investmentType.id', f.formValues);

		return {
			businessCompany: (ha ? ha.issuingCompany : ''),
			businessContract: (ha ? ha.businessContract : ''),
			startDate: td,
			investmentGroupId: investmentGroupId,
			investmentTypeId: investmentTypeId
		};
	},

	updatePayingSecurityOnSecuritySelection: function(securityRecord, tradeListIndex) {
		const fp = this;
		const form = fp.getForm();
		const curr = TCG.getValue('settlementCurrency', securityRecord.json) || TCG.getValue('instrument.tradingCurrency', securityRecord.json);
		form.findField('tradeList[' + tradeListIndex + '].payingSecurity.id').setValue(curr.id);

		const tradeDate = form.findField('tradeDate');
		const td = tradeDate.value;
		Clifton.investment.instrument.getSettlementDatePromise(securityRecord.id, td, null, null, this)
			.then(function(settlementDate) {
				form.findField('tradeList[' + tradeListIndex + '].settlementDate').setValue(settlementDate);
				Clifton.trade.updateExistingPositions(fp, securityRecord.id);
			});
	},

	listeners: {
		afterrender: function() {
			this.setReadOnlyMode(TCG.isNotBlank(this.getIdFieldValue()));
		},
		aftercreate: function() {
			this.updateReadOnlyMode();
		},
		afterload: function(formPanel, closeOnSuccess) {
			const window = formPanel.getWindow();
			if ((TCG.isBlank(closeOnSuccess) || TCG.isFalse(closeOnSuccess)) && window.savedSinceOpen) {
				formPanel.reload();
			}
			else {
				this.updateReadOnlyMode();
			}

			const form = formPanel.getForm();
			const trade1Buy = TCG.getValue('tradeList[0].buy', form.formValues);
			if (TCG.isNotBlank(trade1Buy)) {
				formPanel.setFormValue('buy', trade1Buy);
			}
		}
	},

	tradeInDraftMode: function() {
		const trade1Status = this.getFormValue('tradeList[0].workflowStatus.name', true),
			trade2Status = this.getFormValue('tradeList[1].workflowStatus.name', true);
		return (TCG.isBlank(trade1Status) || 'Draft' === trade1Status) || (TCG.isBlank(trade2Status) || 'Draft' === trade2Status);
	},

	updateReadOnlyMode: function() {
		const ro = TCG.isFalse(this.tradeInDraftMode());
		this.setReadOnlyMode(ro);
	},

	readOnlyMode: undefined,
	setReadOnlyMode: function(ro) {
		const f = this.getForm();
		if (this.readOnlyMode === ro) {
			return;
		}
		this.readOnlyMode = ro;
		this.removeAll(true);
		this.addCommonEntryItems(ro);
		try {
			this.doLayout();
		}
		catch (e) {
			// strange error due to removal of elements with allowBlank = false
		}
		f.setValues(f.formValues);
		this.loadValidationMetaData(true);
		f.isValid();
	},

	addCommonEntryItems: function(readOnly) {
		this.add(this.items_TradeGroup);
		this.add({xtype: 'label', html: '<hr/>'});
		if (readOnly) {
			this.add(this.readOnlyItems_Accounts);
			this.add({xtype: 'label', html: '<hr/>'});
			this.add(this.readOnlyItems_Trades);
			this.add({xtype: 'label', html: '<hr/>'});
			this.add({fieldLabel: 'Trade Note', name: 'tradeList[0].description', xtype: 'textarea', height: 40, anchor: '-20', disabled: true});
		}
		else {
			this.add(this.entryItems_Accounts);
			this.add({xtype: 'label', html: '<hr/>'});
			this.add(this.entryItems_Trades);
			this.add({xtype: 'label', html: '<hr/>'});
			this.add({fieldLabel: 'Trade Note', name: 'tradeList[0].description', xtype: 'textarea', height: 40, anchor: '-20'});
			this.add({xtype: 'trade-existingPositionsGrid'});
		}
		this.add({name: 'tradeList[1].description', xtype: 'textarea', hidden: true});
	},

	items_TradeGroup: [
		{
			xtype: 'panel', layout: 'form',
			items: [
				Clifton.trade.group.TradeGroupNoteDragAndDropPanel,
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {layout: 'form', labelWidth: 120, defaults: {xtype: 'textfield'}},
					items: [
						{
							columnWidth: .45,
							items: [
								{fieldLabel: 'Group Type', name: 'tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroupType.id', detailPageClass: 'Clifton.trade.group.TradeGroupTypeWindow'},
								{name: 'investmentSecurity.id', xtype: 'hidden'},
								{name: 'secondaryInvestmentSecurity.id', xtype: 'hidden'},
								{fieldLabel: 'Action', name: 'tradeGroupAction', xtype: 'hidden'}
							]
						},
						{
							columnWidth: .30,
							items: [
								{fieldLabel: 'Group ID', name: 'id', xtype: 'displayfield'},
								{fieldLabel: 'Parent Group', name: 'parent.id', xtype: 'hidden', detailIdField: 'parent.id', detailPageClass: 'Clifton.trade.TradeGroupWindow', submitValue: false}
							]
						},
						{
							columnWidth: .25,
							items: [
								{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'hidden', submitValue: false},
								{fieldLabel: 'Workflow Status', name: 'workflowStatus.name', xtype: 'hidden', submitValue: false}
							]
						}]
				},
				{
					fieldLabel: 'Note', name: 'note', xtype: 'linkfield', detailIdField: 'sourceFkFieldId', anchor: '-20',
					getDetailPageClass: function(fp) {
						let screen = TCG.getValue('tradeGroupType.sourceDetailScreenClass', fp.getForm().formValues);
						if (TCG.isBlank(screen)) {
							screen = TCG.getValue('tradeGroupType.sourceSystemTable.detailScreenClass', fp.getForm().formValues);
						}
						const fkFieldId = TCG.getValue('sourceFkFieldId', fp.getForm().formValues);
						if (TCG.isBlank(fkFieldId)) {
							TCG.showError('Drill down unsupported for this trade group.');
							return false;
						}
						return screen;
					}
				}
			]
		}
	],

	entryItems_Accounts: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .58,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-30'},
					items: [
						{
							fieldLabel: 'Client Account', name: 'tradeList[0].clientInvestmentAccount.label', hiddenName: 'tradeList[0].clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: false, anchor: '-20',
							detailPageClass: 'Clifton.investment.account.AccountWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const formPanel = TCG.getParentFormPanel(combo);
									const purposeFirstTrade = formPanel.getFormValue('tradeList[0].tradeType.holdingAccountPurpose.name');
									const purposeSecondTrade = formPanel.getFormValue('tradeList[1].tradeType.holdingAccountPurpose.name');
									const purposes = [];
									if (TCG.isNotBlank(purposeFirstTrade)) {
										purposes.push(purposeFirstTrade);
									}
									if (TCG.isNotBlank(purposeSecondTrade)) {
										purposes.push(purposeSecondTrade);
									}
									if (purposes.length > 0) {
										const store = combo.store;
										if (purposes.length === 1) {
											store.baseParams.relatedPurpose = purposes[0];
										}
										else {
											store.baseParams.relatedPurposesAllApplicable = purposes;
										}
										store.baseParams.relatedPurposeActiveOnDate = formPanel.getForm().findField('tradeDate').value;
									}
								},
								beforeselect: function(combo, record) {
									const fp = combo.getParentForm();
									const accountId = record.json.id;

									for (let i = 0; i < 2; i++) {
										combo.beforeSelectHoldingAccountUpdate(fp, accountId, i);
									}
								},
								select: function(combo, record, index) {
									const fp = TCG.getParentFormPanel(this);
									const r = record.json;
									fp.setFormValue('tradeList[1].clientInvestmentAccount.id', r.id);
									Clifton.trade.updateExistingPositions(fp, null, r.id);
								}
							},
							beforeSelectHoldingAccountUpdate: function(formPanel, accountId, tradeListIndex) {
								const f = formPanel.getForm();
								const ha = f.findField('tradeList[' + tradeListIndex + '].holdingInvestmentAccount.label');
								ha.store.removeAll();
								ha.lastQuery = null;
								ha.store.baseParams = {
									mainAccountId: accountId,
									ourAccount: false,
									workflowStatusNameEquals: 'Active'
								};
								const purpose = formPanel.getFormValue('tradeList[' + tradeListIndex + '].tradeType.holdingAccountPurpose.name');
								if (TCG.isNotNull(purpose)) {
									ha.store.baseParams.mainPurpose = purpose;
									ha.store.baseParams.mainPurposeActiveOnDate = f.findField('tradeDate').value;
								}
								ha.store.on('load', function(store, records) {
									if (records.length === 1) {
										ha.setValue(records[0].id);
									}
								});
								ha.doQuery('');
							}
						},
						{name: 'tradeList[1].clientInvestmentAccount.id', xtype: 'hidden'}
					]
				},
				{
					columnWidth: .22,
					layout: 'form',
					labelWidth: 55,
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false},
						{name: 'tradeList[0].traderUser.id', xtype: 'hidden'},
						{name: 'tradeList[1].traderUser.id', xtype: 'hidden'}
					]
				},
				{
					columnWidth: .20,
					layout: 'form',
					labelWidth: 78,
					defaults: {xtype: 'textfield'},
					items: [
						{
							fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									field.updateValuesFromTradeDateChange(newValue);
								},
								select: function(field, date) {
									field.updateValuesFromTradeDateChange(date);
								}
							},
							updateValuesFromTradeDateChange: function(date) {
								const fp = TCG.getParentFormPanel(this);
								fp.updateFromTradeDateChange(date);
							}
						},
						{name: 'tradeList[0].tradeDate', xtype: 'datefield', hidden: true},
						{name: 'tradeList[1].tradeDate', xtype: 'datefield', hidden: true}
					]
				}
			]
		}
	],

	entryItems_Trades: [
		{
			xtype: 'panel',
			layout: 'column',
			labelWidth: 120,
			defaults: {columnWidth: .49, layout: 'form', defaults: {xtype: 'textfield'}},
			items: [
				// Trade 1
				{
					items: [
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Trade ID', name: 'tradeList[0].id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'tradeList[0].id', submitValue: false, submitDetailField: false},
										{
											xtype: 'radiogroup', fieldLabel: 'Buy/Sell', allowBlank: false, submitValue: false, anchor: '-25', columns: [50, 50],
											listeners: {
												afterrender: function(radiogroup) {
													const fp = TCG.getParentFormPanel(this);
													const tradeBuy = fp.getFormValue('tradeList[0].buy');
													// Need to convert buy field value to that of trade 1 if it is available
													if (TCG.isNotNull(tradeBuy)) {
														this.items.items[tradeBuy ? 0 : 1].setValue(true);
													}
												},
												change: function(radioGroup, checked) {
													const fp = TCG.getParentFormPanel(radioGroup);
													const f = fp.getForm();
													// [ and ] cannot be used in radio names, so explicity set trade values
													fp.setFormValue('tradeList[0].buy', checked.inputValue);
													const relatedBuy = f.findField('tradeList[1].buy');
													relatedBuy.setRawValue(!checked.inputValue);
													const params = {
														tradeTypeId: fp.getFormValue('tradeList[0].tradeType.id'),
														buy: checked.inputValue
													};
													TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params})
														.then(function(openCloseType) {
															if (openCloseType && openCloseType.length === 1) {
																fp.getForm().findField('tradeList[0].openCloseType.id').setValue(openCloseType[0].id);
															}
															params['tradeTypeId'] = fp.getFormValue('tradeList[0].tradeType.id');
															params['buy'] = !checked.inputValue;
															return TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params});
														})
														.then(function(openCloseType) {
															if (openCloseType && openCloseType.length === 1) {
																fp.getForm().findField('tradeList[1].openCloseType.id').setValue(openCloseType[0].id);
															}
														});
												}
											},
											items: [
												{boxLabel: 'Buy', name: 'buy', inputValue: true},
												{boxLabel: 'Sell', name: 'buy', inputValue: false}
											]
										},
										{name: 'tradeList[0].buy', xtype: 'hidden'},
										{name: 'tradeList[0].openCloseType.id', xtype: 'hidden'}
									]
								},
								{
									items: [
										{fieldLabel: 'Trade Type', name: 'tradeList[0].tradeType.name', xtype: 'displayfield'},
										{name: 'tradeList[0].tradeTypeSubType', xtype: 'hidden'},
										{fieldLabel: 'Workflow State', name: 'tradeList[0].workflowState.name', xtype: 'displayfield', submitValue: false}
									]
								}
							]
						},
						{fieldLabel: 'Quantity', name: 'tradeList[0].quantityIntended', xtype: 'spinnerfield', minValue: 0.00000001, maxValue: 1000000000, allowBlank: false, allowDecimals: true, decimalPrecision: 10, width: 115},
						{name: 'tradeList[0].tradeType.id', xtype: 'hidden'},
						{
							fieldLabel: 'Security', name: 'tradeList[0].investmentSecurity.label', allowBlank: false, displayField: 'label', hiddenName: 'tradeList[0].investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?tradingDisallowed=false', queryParam: 'searchPatternUsingBeginsWith', anchor: '-20',

							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = TCG.getParentFormPanel(combo);
									combo.store.baseParams = fp.getSecurityQueryParams(0);
								},
								select: function(combo, record, index) {
									const fp = TCG.getParentFormPanel(combo);
									const form = fp.getForm();
									form.findField('investmentSecurity.id').setValue(record.json.id);
									fp.updatePayingSecurityOnSecuritySelection(record, 0);

								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},

							getDefaultData: function(f) { // defaults client/counterparty/isda
								const fp = TCG.getParentFormPanel(this);
								return fp.getSecurityDefaultData(0);
							}
						},
						{name: 'tradeList[0].payingSecurity.id', xtype: 'hidden'},
						{fieldLabel: 'Holding Account', name: 'tradeList[0].holdingInvestmentAccount.label', hiddenName: 'tradeList[0].holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json', allowBlank: false, requiredFields: ['tradeList[0].clientInvestmentAccount.label'], detailPageClass: 'Clifton.investment.account.AccountWindow', anchor: '-20'},
						{
							fieldLabel: 'Executing Broker', name: 'tradeList[0].executingBrokerCompany.label', hiddenName: 'tradeList[0].executingBrokerCompany.id', xtype: 'combo', anchor: '-20',
							displayField: 'label',
							queryParam: 'executingBrokerCompanyName',
							detailPageClass: 'Clifton.business.company.CompanyWindow',
							url: 'tradeExecutingBrokerCompanyListFind.json',
							allowBlank: true,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = TCG.getParentFormPanel(combo).getForm();
									combo.store.baseParams = {
										tradeDestinationId: f.findField('tradeList[0].tradeDestination.name').getValue(),
										tradeTypeId: f.findField('tradeList[0].tradeType.id').getValue(),
										activeOnDate: f.findField('tradeDate').value
									};
								},
								select: function(combo, record, index) {
									const fp = TCG.getParentFormPanel(combo);
									const broker = record.json;
									fp.setFormValue('tradeList[1].executingBrokerCompany.label', broker.label);
									fp.setFormValue('tradeList[1].executingBrokerCompany.id', broker.id);
								}
							}
						},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Trade Destination', name: 'tradeList[0].tradeDestination.name', hiddenName: 'tradeList[0].tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true}
									]
								},
								{
									items: [
										{fieldLabel: 'Settlement Date', name: 'tradeList[0].settlementDate', xtype: 'datefield', allowBlank: false, requiredFields: ['tradeDate'], doNotClearIfRequiredSet: true}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Average Price', name: 'tradeList[0].averageUnitPrice', xtype: 'pricefield', qtip: 'Average price will be calculated by specifying fills.'},
										{fieldLabel: 'Exchange Rate', name: 'tradeList[0].exchangeRateToBase', xtype: 'floatfield'}
									]
								},
								{
									items: [
										{fieldLabel: 'Commission Per Unit', name: 'tradeList[0].commissionPerUnit', xtype: 'floatfield'},
										{fieldLabel: 'Accounting Notional', name: 'tradeList[0].accountingNotional', xtype: 'currencyfield', readOnly: true}
									]
								}
							]
						},
						{
							fieldLabel: 'Trade Fill 1&nbsp;&nbsp;', labelStyle: 'text-align: right;', labelSeparator: '', xtype: 'container', layout: 'hbox', defaults: {flex: 1, layout: 'form', labelWidth: 57, defaults: {anchor: '-20', xtype: 'floatfield', submitValue: false}},
							items: [ // wrap fields in forms to ensure labels show up.
								{
									items: [
										{fieldLabel: 'Price', name: 'tradeList[0].tradeFillList[0].notionalUnitPrice'}
									]
								},
								{
									items: [
										{
											fieldLabel: 'Quantity', name: 'tradeList[0].tradeFillList[0].quantity',
											listeners: {
												change: function(field, newValue, oldValue) {
													const fp = TCG.getParentFormPanel(field);
													const form = fp.getForm();
													const tradeQuantity = form.findField('tradeList[0].quantityIntended').getValue();
													const tradeFill2Field = form.findField('tradeList[0].tradeFillList[1].quantity');
													if (tradeQuantity > 0) {
														tradeFill2Field.setValue(tradeQuantity - newValue);
													}
												}
											}
										}]
								}
							]
						},
						{
							fieldLabel: 'Trade Fill 2&nbsp;&nbsp;', labelStyle: 'text-align: right;', labelSeparator: '', xtype: 'container', layout: 'hbox', defaults: {flex: 1, layout: 'form', labelWidth: 57, defaults: {anchor: '-20', xtype: 'floatfield', submitValue: false}},
							items: [ // wrap fields in forms to ensure labels show up.
								{
									items: [
										{fieldLabel: 'Price', name: 'tradeList[0].tradeFillList[1].notionalUnitPrice'}
									]
								},
								{
									items: [
										{fieldLabel: 'Quantity', name: 'tradeList[0].tradeFillList[1].quantity'}
									]
								}
							]
						}
					]
				},
				{
					columnWidth: .02,
					items: [
						{xtype: 'label', html: '<hr style="width: 1px; height: 300px; display: inline-block; margin: 0 5px;">'}
					]
				},
				// Trade 2
				{
					items: [
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Trade ID', name: 'tradeList[1].id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'tradeList[1].id', submitValue: false, submitDetailField: false},
										{
											fieldLabel: 'Buy/Sell', name: 'tradeList[1].buy', xtype: 'displayfield', width: 110,
											setRawValue: function(v) {
												// need to remove the old to replace the new
												this.el.removeClass(v ? 'sell' : 'buy').addClass(v ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
											}
										},
										{name: 'tradeList[1].openCloseType.id', xtype: 'hidden'}
									]
								},
								{
									items: [
										{fieldLabel: 'Trade Type', name: 'tradeList[1].tradeType.name', xtype: 'displayfield'},
										{name: 'tradeList[1].tradeTypeSubType', xtype: 'hidden'},
										{fieldLabel: 'Workflow State', name: 'tradeList[1].workflowState.name', xtype: 'displayfield', submitValue: false}
									]
								}
							]
						},
						{fieldLabel: 'Quantity', name: 'tradeList[1].quantityIntended', xtype: 'spinnerfield', minValue: 0.00000001, maxValue: 1000000000, allowBlank: false, allowDecimals: true, decimalPrecision: 10, width: 115},
						{name: 'tradeList[1].tradeType.id', xtype: 'hidden'},
						{
							fieldLabel: 'Security', name: 'tradeList[1].investmentSecurity.label', allowBlank: false, displayField: 'label', hiddenName: 'tradeList[1].investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?tradingDisallowed=false', queryParam: 'searchPatternUsingBeginsWith', anchor: '-20',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = TCG.getParentFormPanel(combo);
									combo.store.baseParams = fp.getSecurityQueryParams(1);
								},
								select: function(combo, record, index) {
									const fp = TCG.getParentFormPanel(combo);
									const form = fp.getForm();
									form.findField('secondaryInvestmentSecurity.id').setValue(record.json.id);
									fp.updatePayingSecurityOnSecuritySelection(record, 1);
								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},

							getDefaultData: function(f) { // defaults client/counterparty/isda
								const fp = TCG.getParentFormPanel(this);
								return fp.getSecurityDefaultData(1);
							}
						},
						{fieldLabel: 'Holding Account', name: 'tradeList[1].holdingInvestmentAccount.label', hiddenName: 'tradeList[1].holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json', allowBlank: false, requiredFields: ['tradeList[0].clientInvestmentAccount.label'], detailPageClass: 'Clifton.investment.account.AccountWindow', anchor: '-20'},
						{name: 'tradeList[1].payingSecurity.id', xtype: 'hidden'},
						{fieldLabel: 'Executing Broker', name: 'tradeList[1].executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'tradeList[1].executingBrokerCompany.id', submitDetailField: true},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Trade Destination', name: 'tradeList[1].tradeDestination.name', hiddenName: 'tradeList[1].tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true}
									]
								},
								{
									items: [
										{fieldLabel: 'Settlement Date', name: 'tradeList[1].settlementDate', xtype: 'datefield', allowBlank: false, requiredFields: ['tradeDate'], doNotClearIfRequiredSet: true}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Average Price', name: 'tradeList[1].averageUnitPrice', xtype: 'pricefield'},
										{fieldLabel: 'Exchange Rate', name: 'tradeList[1].exchangeRateToBase', xtype: 'floatfield'}
									]
								},
								{
									items: [
										{
											fieldLabel: 'Commission Per Unit', name: 'tradeList[1].commissionPerUnit', xtype: 'floatfield',
											listeners: {
												afterrender: function() {
													this.setValue(0);
												}
											}
										},
										{fieldLabel: 'Accounting Notional', name: 'tradeList[1].accountingNotional', xtype: 'currencyfield', readOnly: true}
									]
								}
							]
						}
					]
				}
			]
		}
	],

	readOnlyItems_Accounts: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .58,
					layout: 'form',
					defaults: {xtype: 'textfield'},
					items: [
						{fieldLabel: 'Client Account', name: 'tradeList[0].clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'tradeList[0].clientInvestmentAccount.id'},
						{name: 'tradeList[1].clientInvestmentAccount.id', xtype: 'hidden'}
					]
				},
				{
					columnWidth: .22,
					layout: 'form',
					labelWidth: 55,
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'}
					]
				},
				{
					columnWidth: .20,
					layout: 'form',
					labelWidth: 78,
					defaults: {xtype: 'textfield'},
					items: [
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'displayfield', type: 'date'}
					]
				}
			]
		}
	],

	readOnlyItems_Trades: [
		{
			xtype: 'panel',
			layout: 'column',
			labelWidth: 120,
			defaults: {columnWidth: .49, layout: 'form', defaults: {xtype: 'textfield'}},
			items: [
				// Trade 1
				{
					items: [
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Trade', name: 'tradeList[0].id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'tradeList[0].id'},
										{
											fieldLabel: 'Buy/Sell', name: 'tradeList[0].buy', xtype: 'displayfield', width: 110,
											setRawValue: function(v) {
												// need to remove the old to replace the new
												this.el.removeClass(v ? 'sell' : 'buy').addClass(v ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
											}
										}
									]
								},
								{
									items: [
										{fieldLabel: 'Trade Type', name: 'tradeList[0].tradeType.name', xtype: 'displayfield'},
										{fieldLabel: 'Workflow State', name: 'tradeList[0].workflowState.name', xtype: 'displayfield'}
									]
								}
							]
						},
						{fieldLabel: 'Quantity', name: 'tradeList[0].quantityIntended', xtype: 'spinnerfield', minValue: 0.00000001, maxValue: 1000000000, allowBlank: false, allowDecimals: true, decimalPrecision: 10, readOnly: true, width: 120},
						{fieldLabel: 'Security', name: 'tradeList[0].investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'tradeList[0].investmentSecurity.id'},
						{name: 'tradeList[0].payingSecurity.id', xtype: 'hidden'},
						{fieldLabel: 'Holding Account', name: 'tradeList[0].holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'tradeList[0].holdingInvestmentAccount.id'},
						{fieldLabel: 'Executing Broker', name: 'tradeList[0].executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'tradeList[0].executingBrokerCompany.id'},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{
											fieldLabel: 'Trade Destination', name: 'tradeList[0].tradeDestination.name', xtype: 'trade-destination-link',
											getDetailIdFieldValue: function(formPanel) {
												const createOrder = formPanel.getFormValue('tradeList[0].tradeDestination.type.createOrder');
												if (TCG.isTrue(createOrder)) {
													const orderId = formPanel.getFormValue('tradeList[0].orderIdentifier');
													return TCG.isNull(orderId) ? false : orderId;
												}
												TCG.showInfo('Selected Trade Destination does not support orders.', 'Trade Order');
												return false;
											}
										}
									]
								},
								{
									items: [
										{fieldLabel: 'Settlement Date', name: 'tradeList[0].settlementDate', xtype: 'datefield', readOnly: true}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Average Price', name: 'tradeList[0].averageUnitPrice', xtype: 'pricefield', readOnly: true},
										{fieldLabel: 'Exchange Rate', name: 'tradeList[0].exchangeRateToBase', xtype: 'floatfield', readOnly: true}
									]
								},
								{
									items: [
										{fieldLabel: 'Commission Per Unit', name: 'tradeList[0].commissionPerUnit', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Accounting Notional', name: 'tradeList[0].accountingNotional', xtype: 'currencyfield', readOnly: true}
									]
								}
							]
						}
					]
				},
				{
					columnWidth: .02,
					items: [
						{xtype: 'label', html: '<hr style="width: 2px; height: 250px; display: inline-block; margin: 0 10px;">'}
					]
				},
				// Trade 2
				{
					items: [
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Trade', name: 'tradeList[1].id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'tradeList[1].id'},
										{
											fieldLabel: 'Buy/Sell', name: 'tradeList[1].buy', xtype: 'displayfield', width: 110,
											setRawValue: function(v) {
												// need to remove the old to replace the new
												this.el.removeClass(v ? 'sell' : 'buy').addClass(v ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
											}
										}
									]
								},
								{
									items: [
										{fieldLabel: 'Trade Type', name: 'tradeList[1].tradeType.name', xtype: 'displayfield'},
										{fieldLabel: 'Workflow State', name: 'tradeList[1].workflowState.name', xtype: 'displayfield'}
									]
								}
							]
						},
						{fieldLabel: 'Quantity', name: 'tradeList[1].quantityIntended', xtype: 'spinnerfield', minValue: 0.00000001, maxValue: 1000000000, allowBlank: false, allowDecimals: true, decimalPrecision: 10, readOnly: true, width: 120},
						{name: 'tradeList[1].tradeType.id', xtype: 'hidden'},
						{fieldLabel: 'Security', name: 'tradeList[1].investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'tradeList[1].investmentSecurity.id'},
						{name: 'tradeList[1].payingSecurity.id', xtype: 'hidden'},
						{fieldLabel: 'Holding Account', name: 'tradeList[1].holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'tradeList[1].holdingInvestmentAccount.id'},
						{fieldLabel: 'Executing Broker', name: 'tradeList[1].executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'tradeList[1].executingBrokerCompany.id'},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{
											fieldLabel: 'Trade Destination', name: 'tradeList[1].tradeDestination.name', xtype: 'trade-destination-link',
											getDetailIdFieldValue: function(formPanel) {
												const createOrder = formPanel.getFormValue('tradeList[1].tradeDestination.type.createOrder');
												if (TCG.isTrue(createOrder)) {
													const orderId = formPanel.getFormValue('tradeList[1].orderIdentifier');
													return TCG.isNull(orderId) ? false : orderId;
												}
												TCG.showInfo('Selected Trade Destination does not support orders.', 'Trade Order');
												return false;
											}
										}
									]
								},
								{
									items: [
										{fieldLabel: 'Settlement Date', name: 'tradeList[1].settlementDate', xtype: 'datefield', readOnly: true}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form', labelWidth: 120, columnWidth: .50, defaults: {anchor: '-20'}},
							items: [
								{
									items: [
										{fieldLabel: 'Average Price', name: 'tradeList[1].averageUnitPrice', xtype: 'pricefield', readOnly: true},
										{fieldLabel: 'Exchange Rate', name: 'tradeList[1].exchangeRateToBase', xtype: 'floatfield', readOnly: true}
									]
								},
								{
									items: [
										{fieldLabel: 'Commission Per Unit', name: 'tradeList[1].commissionPerUnit', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Accounting Notional', name: 'tradeList[1].accountingNotional', xtype: 'currencyfield', readOnly: true}
									]
								}
							]
						}
					]
				}
			]
		}
	]
});
Ext.reg('trade-efp-form', Clifton.trade.EFPTradeForm);

Clifton.trade.EFPTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'EFP Trade',
	width: 1100,
	height: 650,
	iconCls: 'shopping-cart',

	items: [
		{xtype: 'trade-efp-form'}
	]
});
