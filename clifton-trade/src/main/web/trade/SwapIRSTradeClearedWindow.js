TCG.use('Clifton.trade.SwapIRSTradeWindow');

Clifton.trade.SwapIRSTradeClearedWindow = Ext.extend(Clifton.trade.SwapIRSTradeWindow, {
	centralClearing: true,
	executingBrokerNotRequired: true
});
