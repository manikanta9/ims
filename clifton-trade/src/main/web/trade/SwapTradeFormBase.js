Clifton.trade.SwapTradeFormBase = Ext.extend(Clifton.trade.BaseTradeForm, {
	submitEmptyText: true,
	labelWidth: 120,

	getSubmitParams: function() {
		return {enableCommissionOverride: true};
	},

	//////////////////////////////////////////////
	// Swap Type Specific - Override where necessary
	investmentTypeSubType: 'OVERRIDE_ME',

	updateCalculatedValues: function() {
		//OVERRIDE ME - Currently used in CDS version only
	},

	setSecurityCustomFields: function(values) {
		// OVERRIDE_ME - Base Form loads custom field values and passes to this method
		// which looks for specific columns/values and sets on the screen
	},

	updateFromTradeDateChange: function() {
		// OVERRIDE_ME - Currently used in CDS version only
	},

	updateFromSettlementDateChange: function() {
		// OVERRIDE_ME - Currently used in CDS version only
	},

	updateFromSecuritySelection: function(securityId) {
		// OVERRIDE_ME - Currently used in CDS version only
	},

	updateFromSettlementCurrencySelection: function(currencySecurityId) {
		// OVERRIDE_ME - Currently used in IRS version only
	},

	// securityEntryItems: []
	// securityEntryItems_readOnly: [] - readOnly copy of security entry items

	//////////////////////////////////////////////

	afterRenderApplyAdditionalDefaultData: function(formPanel, formValues) {
		if (formValues.quantityIntended) {
			formPanel.updateCalculatedValues('quantityIntended');
		}
		formPanel.afterRenderApplyAdditionalDefaultCounterparty(formPanel, formValues);
	},

	afterRenderApplyAdditionalDefaultCounterparty: function(formPanel, formValues) {
		const counterpartyField = formPanel.getForm().findField('holdingInvestmentAccount.issuingCompany.name');
		const counterpartyId = TCG.getValue('holdingInvestmentAccount.issuingCompany.id', formValues);
		if (counterpartyField && TCG.isNotBlank(counterpartyId)) {
			formPanel.setFormValue('executingBrokerCompany.id', counterpartyId, true);
		}
	},

	getDefaultData: function(win) {
		const dd = Ext.apply(win.defaultData || {}, {});
		if (win.defaultDataIsReal) {
			return dd;
		}
		const tradeTypeName = win.tradeType;
		const formPanel = this;
		return TCG.data.getDataPromiseUsingCaching('tradeDestinationByName.json?name=Manual', formPanel, 'trade.tradeDestination.Manual')
				.then(function(tradeDestination) {
					dd.tradeDestination = tradeDestination;
					return TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + tradeTypeName, formPanel, 'trade.type.' + tradeTypeName);
				})
				.then(function(tradeType) {
					dd.tradeType = tradeType;
					return TCG.data.getDataPromiseUsingCaching('tradeDestinationMappingByCommand.json?defaultDestinationMapping=true&tradeTypeId=' + tradeType.id + '&activeOnDate=' + new Date().format('m/d/Y'), formPanel, 'trade.type.destinationMappingDefault.' + tradeTypeName + '.' + new Date().format('m/d/Y'));
				})
				.then(function(destinationMapping) {
					if (destinationMapping) {
						if (destinationMapping.tradeDestination) {
							dd.tradeDestination = destinationMapping.tradeDestination;
						}
						if (destinationMapping.executingBrokerCompany) {
							dd.executingBrokerCompany = destinationMapping.executingBrokerCompany;
						}
					}
					dd.traderUser = TCG.getCurrentUser();
					dd.tradeDate = new Date().format('Y-m-d 00:00:00');
					return dd;
				});
	},

	loadSecurityCustomFields: function() {
		const fp = this;
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			params: {
				columnGroupName: 'Security Custom Fields',
				entityId: fp.getFormValue('investmentSecurity.id'),
				linkedValue: fp.getFormValue('investmentSecurity.instrument.hierarchy.id')
			},
			scope: this,
			onLoad: function(record, conf) {
				const v = TCG.getValue('columnValueList', record);
				fp.setSecurityCustomFields(v);
			}
		});
		loader.load('systemColumnCustomConfig.json');
	},

	updateReadOnlyFields: function() {
		const ro = ('Draft' !== this.getFormValue('workflowStatus.name'));
		this.setReadOnlyMode(ro);

		// quantity intended is editable for partially filled trades
		const fp = this;
		fp.setReadOnlyField('quantityIntended', ro && ('Cannot Fill' !== this.getFormValue('workflowState.name')));
		// Always allow trade notes to be editable f.findField('description').setReadOnly(ro && ('Pending' != this.getFormValue('workflowState.name')));
		// commission/fees and settlement date are editable for Active trades
		const flag = ro && ('Active' !== this.getFormValue('workflowStatus.name'));
		fp.setReadOnlyField('exchangeRateToBase', flag);
		fp.setReadOnlyField('accrualAmount1', flag);
		fp.setReadOnlyField('accrualAmount2', flag);
		fp.setReadOnlyField('settlementDate', flag);
		fp.setReadOnlyField('actualSettlementDate', flag);
		fp.setReadOnlyField('commissionAmount', flag);
		fp.setReadOnlyField('feeAmount', flag);

		const booked = ('Booked' === this.getFormValue('workflowState.name'));
		if (booked) {
			fp.setReadOnlyField('actualSettlementDate', false);
		}
	},

	listeners: {
		beforerender: function() {
			const status = TCG.getValue('workflowStatus.name', this.getWindow().defaultData);
			this.readOnlyMode = (TCG.isNotNull(status) && status !== 'Draft');
			if (this.readOnlyMode) {
				this.readOnlyMode = (this.getLoadURL() !== false);
			}
			this.addCommonEntryItems(this.readOnlyMode);
			this.add((this.readOnlyMode) ? this.securityEntryItems_readOnly : this.securityEntryItems);
			this.add((this.readOnlyMode) ? this.additionalEntryItems_readOnly : this.additionalEntryItems);
		},
		afterload: function() {
			const fp = this;
			fp.updateReadOnlyFields();
			fp.loadSecurityCustomFields();
			fp.updateCalculatedValues();
		}
	},

	readOnlyMode: true,
	setReadOnlyMode: function(ro) {
		const f = this.getForm();
		if (this.readOnlyMode === ro) {
			return;
		}
		this.readOnlyMode = ro;
		this.removeAll(true);
		this.addCommonEntryItems(ro);
		this.add((ro) ? this.securityEntryItems_readOnly : this.securityEntryItems);
		this.add((ro) ? this.additionalEntryItems_readOnly : this.additionalEntryItems);
		try {
			this.doLayout();
		}
		catch (e) {
			// strange error due to removal of elements with allowBlank = false
		}
		f.setValues(f.formValues);
		this.loadValidationMetaData(true);
		f.isValid();
	},

	addCommonEntryItems: function(readOnly) {
		const centralClearing = (this.getWindow().centralClearing === true);
		const executingBrokerNotRequired = (this.getWindow().executingBrokerNotRequired === true);
		if (readOnly) {
			if (centralClearing) {
				this.add(this.commonEntryItems_readOnly_Dates);
				this.add(this.commonEntryItems_readOnly_Securities);
				this.add({xtype: 'label', html: '<hr/>'});
				this.add(this.commonEntryItems_readOnly_Accounts_CLEARED);
				this.add({xtype: 'label', html: '<hr/>'});
			}
			else {
				this.add(this.commonEntryItems_readOnly_Dates);
				this.add({xtype: 'label', html: '<hr/>'});
				this.add(this.commonEntryItems_readOnly_Accounts);
				this.add({xtype: 'label', html: '<hr/>'});
				this.add(this.commonEntryItems_readOnly_Securities);
			}
		}
		else {
			if (centralClearing) {
				this.add(this.commonEntryItems_Dates);
				this.add(this.commonEntryItems_Securities);
				this.add({xtype: 'label', html: '<hr/>'});
				this.add(this.commonEntryItems_Accounts_CLEARED);
				this.add({xtype: 'label', html: '<hr/>'});

				const f = this.getForm();
				if (executingBrokerNotRequired) {
					f.findField('executingBrokerCompany.label').allowBlank = true;
				}
			}
			else {
				this.add(this.commonEntryItems_Dates);
				this.add({xtype: 'label', html: '<hr/>'});
				this.add(this.commonEntryItems_Accounts);
				this.add({xtype: 'label', html: '<hr/>'});
				this.add(this.commonEntryItems_Securities);
				this.add({name: 'executingBrokerCompany.id', xtype: 'hidden'});
			}
		}
	},

	updateExecutingBrokerCompany: function(tradeDestinationId) {
		const f = this.getForm();
		const executingBroker = f.findField('executingBrokerCompany.label');
		if (executingBroker.isVisible()) {
			tradeDestinationId = tradeDestinationId || f.findField('tradeDestination.name').getValue();
			if (tradeDestinationId) {
				const tradeDate = f.findField('tradeDate').getValue().format('m/d/Y');
				const loader = new TCG.data.JsonLoader({
					waitTarget: this,
					params: {
						tradeTypeId: f.findField('tradeType.id').getValue(),
						tradeDestinationId: tradeDestinationId,
						activeOnDate: tradeDate
					},
					root: 'data',
					onLoad: function(rec, conf) {
						if (rec && rec.length === 1 && rec[0].executingBrokerCompany) {
							executingBroker.setValue({value: rec[0].executingBrokerCompany.id, text: rec[0].executingBrokerCompany.label});
							f.formValues.executingBrokerCompany = rec[0].executingBrokerCompany; // needs this in order to find the record when tabbing out when the store isn't initialized
						}
						else {
							executingBroker.reset();
							executingBroker.clearValue();
						}
					}
				});
				loader.load('tradeDestinationMappingListByCommandFind.json');
			}
		}
	},

	validateActualSettlementDate: function() {
		const f = this.getForm();
		const actualSettlementDateField = f.findField('actualSettlementDate');
		if (!actualSettlementDateField.el.dom.readOnly) {
			const settlementDateField = f.findField('settlementDate');
			const actualSettlementDateValue = actualSettlementDateField.getValue();
			const settlementDateValue = settlementDateField.getValue();
			if (actualSettlementDateValue && settlementDateValue && actualSettlementDateValue < settlementDateValue) {
				actualSettlementDateField.markInvalid('Actual Settlement Date cannot be before the Settlement Date.');
				return false;
			}
		}
		return true;
	},

	//////////////////////////////////////////////

	commonEntryItems_Dates: [
		{
			xtype: 'panel',
			layout: 'column',
			labelWidth: 75,
			items: [
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{
							fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									field.updateValuesFromTradeDateChange(newValue);
								},
								select: function(field, date) {
									field.updateValuesFromTradeDateChange(date);
								}
							},
							updateValuesFromTradeDateChange: function(date) {
								const fp = TCG.getParentFormPanel(this);
								const form = fp.getForm();
								const securityId = form.findField('investmentSecurity.id').value;
								if (TCG.isBlank(date) || TCG.isBlank(securityId)) {
									return;
								}

								Clifton.investment.instrument.getSettlementDatePromise(securityId, date, null, null, this)
										.then(function(settlementDate) {
											form.findField('settlementDate').setValue(settlementDate);
											fp.updateFromTradeDateChange();
										});
							}
						}
					]
				},
				{
					columnWidth: .25,
					layout: 'form',
					labelWidth: 100,
					items: [
						{
							fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false, requiredFields: ['tradeDate', 'investmentSecurity.id'], doNotClearIfRequiredSet: true, qtip: 'The contractual settlement date.',
							getParentFormPanel: function() {
								const parent = TCG.getParentFormPanel(this);
								return parent || this.ownerCt;
							},
							listeners: {
								change: function(field, newValue, oldValue) {
									field.updateValuesFromSettlementDateChange(newValue);
								},
								select: function(field, date) {
									field.updateValuesFromSettlementDateChange(date);
								}
							},
							updateValuesFromSettlementDateChange: function(date) {
								const fp = TCG.getParentFormPanel(this);
								fp.updateFromSettlementDateChange();
								fp.validateActualSettlementDate();
							}
						}
					]
				},
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{
							fieldLabel: 'Actual Settle', name: 'actualSettlementDate', xtype: 'datefield', allowBlank: true, requiredFields: ['settlementDate'],
							qtip: 'The actual settlement (wire) date.',
							validateValue: function(value) {
								const fp = TCG.getParentFormPanel(this);
								return fp.validateActualSettlementDate();
							}
						}
					]
				},
				{
					columnWidth: .25,
					layout: 'form',
					labelWidth: 100,

					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Maturity Date', name: 'investmentSecurity.endDate', xtype: 'datefield', readOnly: true}
					]
				}
			]
		}
	],
	commonEntryItems_Accounts: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .71,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{
							fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: false, anchor: '-20', detailPageClass: 'Clifton.investment.account.AccountWindow',
							requiredFields: ['tradeDate'],
							doNotClearIfRequiredChanges: true,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.store.setBaseParam('relatedPurpose', fp.getFormValue('tradeType.holdingAccountPurpose.name'));
									combo.store.setBaseParam('relatedPurposeActiveOnDate', fp.getForm().findField('tradeDate').value);
								},
								select: function(combo, record, index) {
									// On Select - Attempt to Auto Load the correct Counterparty
									const fp = combo.getParentForm();
									// Client Account
									const clientAcctId = fp.getFormValue('clientInvestmentAccount.id', true);
									const params = {contractInvestmentAccountId: clientAcctId};
									// ISDA Contract Category: ISDA Amendments need to be included in the look up.
									params.contractCategoryName = 'ISDA';
									// Counterparty Role
									params.contractPartyRoleName = 'Counterparty';

									const loader = new TCG.data.JsonLoader({
										waitTarget: fp,
										params: params,
										onLoad: function(record, conf) {
											const l = record.length || 0;
											if (l === 0) {
												TCG.showError('Cannot find any Counterparties available for selected Client Account', 'Invalid Setup');
											}
											else if (l === 1) {
												const cp = record[0];
												const form = fp.getForm();
												form.formValues = form.formValues || {};
												form.formValues.holdingInvestmentAccount = {issuingCompany: cp};
												const val = {value: cp.id, text: cp.name};
												const fld = fp.getForm().findField('holdingInvestmentAccount.issuingCompany.id');
												fld.setValue(val);
												// Fire Event so the Holding Account is also attempted to autoload
												fld.fireEvent('select', fld, cp);
											}
										}
									});
									loader.load('businessCompanyListFind.json?issuer=true&ourCompany=false');
									Clifton.trade.updateExistingPositions(fp);
								}
							}
						},
						{
							fieldLabel: 'Counterparty', name: 'holdingInvestmentAccount.issuingCompany.name', submitValue: false, hiddenName: 'holdingInvestmentAccount.issuingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', anchor: '-20', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
							requiredFields: ['clientInvestmentAccount.id'],
							beforequery: function(queryEvent) {
								const fp = queryEvent.combo.getParentForm();
								// Client Account
								const clientAcctId = fp.getFormValue('clientInvestmentAccount.id', true);
								const params = {contractInvestmentAccountId: clientAcctId};
								// ISDA Contract Category: ISDA Amendments need to be included in the look up.
								params.contractCategoryName = 'ISDA';
								// Counterparty Role
								params.contractPartyRoleName = 'Counterparty';
								queryEvent.combo.store.baseParams = params;
							},
							listeners: {
								select: function(combo, record, index) {
									// On Select - Attempt to Auto Load the correct Holding Account
									const fp = combo.getParentForm();
									fp.setFormValue('executingBrokerCompany.id', record.id, true);
									const params = {
										mainAccountId: fp.getFormValue('clientInvestmentAccount.id', true),
										issuingCompanyId: record.id,
										mainPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
										mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value,
										businessContractPopulated: true
									};
									const accountType = fp.getFormValue('investmentSecurity.instrument.hierarchy.holdingAccountType.name');
									if (accountType) {
										params.accountType = accountType;
									}
									const loader = new TCG.data.JsonLoader({
										waitTarget: fp,
										params: params,
										onLoad: function(record, conf) {
											const l = record.length || 0;
											if (l === 0) {
												TCG.showError('Cannot find any active Holding Accounts that match selected Client Account, Counterparty selections for ' + fp.getFormValue('tradeType.holdingAccountPurpose.name') + ' purpose.', 'Invalid Setup');
											}
											else if (l === 1) {
												const ha = record[0];
												const form = fp.getForm();
												form.formValues = form.formValues || {};
												form.formValues.holdingInvestmentAccount = ha;
												fp.getForm().findField('holdingInvestmentAccount.id').setValue({value: ha.id, text: ha.label});
												fp.setFormValue('holdingInvestmentAccount.businessContract.label', ha.businessContract.name, true);
												fp.setFormValue('holdingInvestmentAccount.businessContract.id', ha.businessContract.id, true);
											}
										}
									});
									loader.load('investmentAccountListFind.json?ourAccount=false');
								}
							}
						},
						{
							fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', anchor: '-20', detailPageClass: 'Clifton.investment.account.AccountWindow', url: 'investmentAccountListFind.json?ourAccount=false',
							requiredFields: 'holdingInvestmentAccount.issuingCompany.id',
							beforequery: function(queryEvent) {
								const fp = queryEvent.combo.getParentForm();
								// Client Account
								const params = {
									mainAccountId: fp.getFormValue('clientInvestmentAccount.id', true),
									issuingCompanyId: fp.getFormValue('holdingInvestmentAccount.issuingCompany.id', true),
									mainPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
									mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value,
									businessContractPopulated: true
								};
								const accountType = fp.getFormValue('investmentSecurity.instrument.hierarchy.holdingAccountType.name');
								if (accountType) {
									params.accountType = accountType;
								}
								queryEvent.combo.store.baseParams = params;
							},
							listeners: {
								select: function(combo, record, index) {
									// On Select - Attempt to Auto Load the correct Holding Account
									const fp = combo.getParentForm();
									const form = fp.getForm();
									form.formValues = form.formValues || {};
									form.formValues.holdingInvestmentAccount = record.json;
									fp.setFormValue('holdingInvestmentAccount.businessContract.label', record.json.businessContract.name, true);
									fp.setFormValue('holdingInvestmentAccount.businessContract.id', record.json.businessContract.id, true);
								}
							}
						},
						{fieldLabel: 'ISDA', name: 'holdingInvestmentAccount.businessContract.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'holdingInvestmentAccount.businessContract.id', submitDetailField: false}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{xtype: 'trade-bookingdate'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
							//requiredFields: 'holdingInvestmentAccount.issuingCompany.name',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {
										tradeTypeId: f.findField('tradeType.id').getValue(),
										activeOnDate: f.findField('tradeDate').value,
										executingBrokerCompanyId: f.findField('holdingInvestmentAccount.issuingCompany.name').getValue()
									};
								},
								select: function(combo, record, index) {
									const fp = combo.getParentForm();

									if (TCG.isTrue(record.json.optionalQuantityOrNotionalEntry)) {
										const qtyField = fp.getForm().findField('quantityIntended');
										const notionalField = fp.getForm().findField('accountingNotional');
										const priceField = fp.getForm().findField('averageUnitPrice');

										qtyField.allowBlank = true;
										priceField.allowBlank = true;
										notionalField.allowBlank = true;
										notionalField.setReadOnly(false);
									}
								}
							}
						},
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false},
						{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
					]
				}
			]
		},
		{xtype: 'novation-fieldset'}
	],
	commonEntryItems_Accounts_CLEARED: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .71,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{
							fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: false, anchor: '-20', detailPageClass: 'Clifton.investment.account.AccountWindow',
							requiredFields: ['tradeDate'],
							doNotClearIfRequiredChanges: true,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.store.setBaseParam('relatedPurpose', fp.getFormValue('tradeType.holdingAccountPurpose.name'));
									combo.store.setBaseParam('relatedPurposeActiveOnDate', fp.getForm().findField('tradeDate').value);
								},
								select: function(combo, record, index) {
									// On Select - Attempt to Auto Load the correct Holding Account
									const fp = combo.getParentForm();
									const params = {
										mainAccountId: fp.getFormValue('clientInvestmentAccount.id', true),
										mainPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
										mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value,
										businessContractPopulated: true
									};
									const accountType = fp.getFormValue('investmentSecurity.instrument.hierarchy.holdingAccountType.name');
									if (accountType) {
										params.accountType = accountType;
									}
									const loader = new TCG.data.JsonLoader({
										waitTarget: fp,
										params: params,
										onLoad: function(record, conf) {
											const l = record.length || 0;
											if (l === 0) {
												TCG.showError('Cannot find any active Holding Accounts that match selected Client Account for ' + fp.getFormValue('tradeType.holdingAccountPurpose.name') + ' purpose.', 'Invalid Setup');
											}
											else if (l === 1) {
												const ha = record[0];
												const form = fp.getForm();
												form.formValues = form.formValues || {};
												form.formValues.holdingInvestmentAccount = ha;
												fp.getForm().findField('holdingInvestmentAccount.id').setValue({value: ha.id, text: ha.label});
												fp.setFormValue('holdingInvestmentAccount.businessContract.label', ha.businessContract.name, true);
												fp.setFormValue('holdingInvestmentAccount.businessContract.id', ha.businessContract.id, true);
											}
										}
									});
									loader.load('investmentAccountListFind.json?ourAccount=false');
									Clifton.trade.updateExistingPositions(fp);
								}
							}
						},
						{
							fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', anchor: '-20', detailPageClass: 'Clifton.investment.account.AccountWindow', url: 'investmentAccountListFind.json?ourAccount=false',
							requiredFields: 'clientInvestmentAccount.id',
							beforequery: function(queryEvent) {
								const fp = queryEvent.combo.getParentForm();
								// Client Account
								const params = {
									mainAccountId: fp.getFormValue('clientInvestmentAccount.id', true),
									mainPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
									mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value,
									businessContractPopulated: true
								};
								const accountType = fp.getFormValue('investmentSecurity.instrument.hierarchy.holdingAccountType.name');
								if (accountType) {
									params.accountType = accountType;
								}
								queryEvent.combo.store.baseParams = params;
							},
							listeners: {
								select: function(combo, record, index) {
									// On Select - Attempt to Auto Load the correct Holding Account
									const fp = combo.getParentForm();
									const form = fp.getForm();
									form.formValues = form.formValues || {};
									form.formValues.holdingInvestmentAccount = record.json;
									fp.setFormValue('holdingInvestmentAccount.businessContract.label', record.json.businessContract.name, true);
									fp.setFormValue('holdingInvestmentAccount.businessContract.id', record.json.businessContract.id, true);
								}
							}
						},
						{
							fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', width: 250, hiddenName: 'executingBrokerCompany.id', xtype: 'combo',
							displayField: 'label',
							queryParam: 'executingBrokerCompanyName',
							detailPageClass: 'Clifton.business.company.CompanyWindow',
							url: 'tradeExecutingBrokerCompanyListFind.json',
							allowBlank: false,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {
										tradeDestinationId: f.findField('tradeDestination.name').getValue(),
										tradeTypeId: f.findField('tradeType.id').getValue(),
										activeOnDate: f.findField('tradeDate').value
									};
								}
							}
						},
						{
							fieldLabel: 'SEF Sponsor', name: 'executingSponsorCompany.label', hiddenName: 'executingSponsorCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow',
							url: 'businessCompanyListFind.json',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const tradeTypeName = combo.getParentForm().getFormValue('tradeType.name');
									combo.store.baseParams = {
										categoryHierarchyName: 'SEF Sponsor (' + tradeTypeName + ')',
										categoryName: 'Business Company Tags'
									};
								}
							}
						},
						{fieldLabel: 'CDA', name: 'holdingInvestmentAccount.businessContract.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'holdingInvestmentAccount.businessContract.id', qtip: 'Cleared Derivatives Addendum', submitDetailField: false}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{xtype: 'trade-bookingdate'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams = {
										tradeTypeId: f.findField('tradeType.id').getValue(),
										activeOnDate: f.findField('tradeDate').value
									};
								},
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									// clear existing destinations
									const dst = fp.getForm().findField('executingBrokerCompany.label');
									dst.store.removeAll();
									dst.lastQuery = null;
									fp.updateExecutingBrokerCompany(record.id);
								}
							}
						},
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false},
						{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
					]
				}]
		},
		{xtype: 'novation-fieldset'}
	],
	commonEntryItems_Securities: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .42,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{
							fieldLabel: 'Swap', name: 'investmentSecurity.label', width: 210, allowBlank: false, displayField: 'label', hiddenName: 'investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', limitRequestedProperties: false,
							TODO_requiredFields: ['holdingInvestmentAccount.id'],
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									const f = fp.getForm();
									const otc = (fp.getWindow().centralClearing !== true);
									const securityCreationAtExecutionAllowed = (fp.getFormValue('tradeType.securityCreationAtExecutionAllowed') === true);
									const params = {
										otc: otc,
										activeOnDate: f.findField('tradeDate').value,
										instrumentIsInactive: false
									};
									if (!securityCreationAtExecutionAllowed) {
										params.investmentType = fp.getFormValue('tradeType.investmentType.name');
										params.investmentTypeSubType = fp.investmentTypeSubType;
										params.tradingDisallowed = false;
									}
									if (otc) {
										params.businessCompanyId = fp.getFormValue('holdingInvestmentAccount.issuingCompany.id', true);
										params.businessContractId = fp.getFormValue('holdingInvestmentAccount.businessContract.id', true);
									}
									const groupId = fp.getFormValue('tradeType.investmentGroup.id');
									if (groupId) {
										params.investmentGroupId = groupId;
									}
									combo.store.baseParams = params;
								},
								select: function(combo, record, index) {
									combo.selectSecurity(record.json);
								}
							},

							selectSecurity: function(record) {
								const combo = this;
								const curr = record.settlementCurrency || record.instrument.tradingCurrency;
								const fp = combo.getParentForm();
								const form = fp.getForm();

								form.formValues = form.formValues || {};
								form.formValues.payingSecurity = curr;
								fp.getForm().findField('payingSecurity.id').setValue({value: curr.id, text: curr.label});

								fp.setFormValue('investmentSecurity.endDate', TCG.parseDate(record.endDate), true);
								if (record.instrument.underlyingInstrument) {
									fp.setFormValue('investmentSecurity.instrument.underlyingInstrument.id', record.instrument.underlyingInstrument.id, true);
									fp.setFormValue('investmentSecurity.instrument.underlyingInstrument.identifierPrefix', record.instrument.underlyingInstrument.identifierPrefix, true);
								}
								else {
									fp.getForm().findField('investmentSecurity.instrument.underlyingInstrument.identifierPrefix').setVisible(false);
								}

								form.formValues.investmentSecurity = record;
								fp.loadSecurityCustomFields.call(fp);

								TCG.getChildByName(fp, 'novationFieldset').setDisabled(!record.instrument.hierarchy.otc);

								const td = form.findField('tradeDate').getValue();
								Promise.resolve()
										.then(function() {
											return TCG.isNotNull(td) ? td : Clifton.investment.instrument.getTradeDatePromise(record.id, null, combo)
													.then(function(td) {
														tradeDate.setValue(td);
														return td;
													});
										})
										.then(function(td) {
											return Clifton.investment.instrument.getSettlementDatePromise(record.id, td, null, null, combo);
										})
										.then(function(settlementDate) {
											form.findField('settlementDate').setValue(settlementDate);

											Clifton.trade.updateExistingPositions(fp, record.id);
											fp.updateFromSecuritySelection(record.id);
										});
							},

							// Additional method to query the security again to set dependent fields
							reload: function(win) {
								this.clearValue();
								this.store.removeAll();
								this.lastQuery = null;
								// populate value/text from the window passed (not in store)
								const v = win.getMainFormId();
								const text = win.getMainFormPanel().getFormLabel();
								// code block from setValue
								this.lastSelectionText = text;
								if (this.hiddenField) {
									this.hiddenField.value = v;
								}
								Ext.form.ComboBox.superclass.setValue.call(this, text);
								this.value = v;

								TCG.data.getDataPromise('investmentSecurity.json?id=' + v, this)
										.then(function(sec) {
											if (sec) {
												this.selectSecurity(sec);
											}
										});
							},

							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},

							getDefaultData: function(f) { // defaults client/counterparty/isda
								let fp = TCG.getParentFormPanel(this);
								const subType = fp.investmentTypeSubType;
								fp = fp.getForm();
								const ha = TCG.getValue('holdingInvestmentAccount', fp.formValues);
								const td = fp.findField('tradeDate').getValue().format('Y-m-d 00:00:00');
								const investmentGroupId = TCG.getValue('tradeType.investmentGroup.id', fp.formValues);


								return {
									businessCompany: (ha ? ha.issuingCompany : ''),
									businessContract: (ha ? ha.businessContract : ''),
									startDate: td,
									investmentGroupId: investmentGroupId,
									instrument: {hierarchy: {investmentTypeSubType: {name: subType}}}
								};
							}
						}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					labelWidth: 100,
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Underlying', name: 'investmentSecurity.instrument.underlyingInstrument.identifierPrefix', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'investmentSecurity.instrument.underlyingInstrument.id', submitDetailField: false}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{
							fieldLabel: 'Settlement CCY', name: 'payingSecurity.label', xtype: 'combo', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', hiddenName: 'payingSecurity.id', allowBlank: false, url: 'investmentSecurityListFind.json?currency=true',
							qtip: 'Typically the currency denomination of the security, but can be overwritten to settle the payment from the trade in a different currency; such as the client account\'s base currency.',
							requiredFields: ['investmentSecurity.id'],
							listeners: {
								select: function(combo, record, index) {
									const fp = TCG.getParentFormPanel(this);
									fp.updateFromSettlementCurrencySelection(record.id);
								}
							}
						}
					]
				}
			]
		},
		{name: 'tradeType.id', xtype: 'hidden'}
	],


	commonEntryItems_readOnly_Dates: [
		{
			xtype: 'panel',
			layout: 'column',
			labelWidth: 75,
			items: [
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
					]
				},
				{
					columnWidth: .25,
					layout: 'form',
					labelWidth: 100,
					items: [
						{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true}
					]
				},
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{
							fieldLabel: 'Actual Settle', name: 'actualSettlementDate', xtype: 'datefield', readOnly: true,
							validateValue: function(value) {
								const fp = TCG.getParentFormPanel(this);
								return fp.validateActualSettlementDate();
							}
						}
					]
				},
				{
					columnWidth: .25,
					layout: 'form',
					labelWidth: 100,
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Maturity Date', name: 'investmentSecurity.endDate', xtype: 'datefield', readOnly: true}
					]
				}
			]
		}
	],
	commonEntryItems_readOnly_Accounts: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .71,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
						{fieldLabel: 'Counterparty', name: 'holdingInvestmentAccount.issuingCompany.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'holdingInvestmentAccount.issuingCompany.id'},
						{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'},
						{fieldLabel: 'ISDA', name: 'holdingInvestmentAccount.businessContract.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'holdingInvestmentAccount.businessContract.id', submitDetailField: false},
						{name: 'executingBrokerCompany.id', xtype: 'hidden'}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					items: [
						{xtype: 'trade-bookingdate'},
						{xtype: 'trade-destination-link'},
						{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
						{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
					]
				}
			]
		},
		{xtype: 'novation-fieldset', disabled: true}
	],
	commonEntryItems_readOnly_Accounts_CLEARED: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .71,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
						{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'},
						{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'},
						{fieldLabel: 'SEF Sponsor', name: 'executingSponsorCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingSponsorCompany.id'},
						{fieldLabel: 'CDA', name: 'holdingInvestmentAccount.businessContract.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'holdingInvestmentAccount.businessContract.id', qtip: 'Cleared Derivatives Addendum', submitDetailField: false}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					items: [
						{xtype: 'trade-bookingdate'},
						{xtype: 'trade-destination-link'},
						{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
						{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
					]
				}
			]
		},
		{xtype: 'novation-fieldset', disabled: true}
	],
	commonEntryItems_readOnly_Securities: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .42,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Swap', name: 'investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id'}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					labelWidth: 100,
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Underlying', name: 'investmentSecurity.instrument.underlyingInstrument.identifierPrefix', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', detailIdField: 'investmentSecurity.instrument.underlyingInstrument.id', submitDetailField: false}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Settlement CCY', name: 'payingSecurity.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'payingSecurity.id', qtip: 'Typically the currency denomination of the security, but can be overwritten to settle the payment from the trade in a different currency; such as the client account\'s base currency.'}
					]
				}
			]
		},
		{name: 'tradeDestination.id', xtype: 'hidden'},
		{name: 'tradeType.id', xtype: 'hidden'}
	],


	additionalEntryItems: [
		{xtype: 'label', html: '<hr/>'},
		{xtype: 'trade-descriptionWithNoteDragDrop'},
		{xtype: 'trade-existingPositionsGrid', quantityColumnLabel: 'Notional', usePositionBalance: true}
	],

	additionalEntryItems_readOnly: [
		{xtype: 'label', html: '<hr/>'},
		{xtype: 'trade-descriptionWithNoteDragDrop'}
	],

	getClosingPositionQuantity: function(positionRecord) {
		return positionRecord.json.unadjustedQuantity;
	},

	populateClosingPositionAdditional: async function(positionRecord) {
		const form = this.getForm();
		const holdingAccountField = form.findField('holdingInvestmentAccount.label');
		const counterpartyField = form.findField('holdingInvestmentAccount.issuingCompany.name');
		if (holdingAccountField && counterpartyField) {
			const holdingAccountId = holdingAccountField.getValue();
			if (holdingAccountId && TCG.isNumber(holdingAccountId)) {
				const holdingAccount = await TCG.data.getDataPromise(`investmentAccount.json?id=${holdingAccountId}&requestedPropertiesRoot=data&requestedProperties=issuingCompany.id|issuingCompany.name|label|id`, this);
				if (holdingAccount) {
					counterpartyField.setValue({text: holdingAccount.issuingCompany.name, value: holdingAccount.issuingCompany.id});
					holdingAccountField.setValue({text: holdingAccount.label, value: holdingAccount.id});
					this.setFormValue('executingBrokerCompany.id', holdingAccount.issuingCompany.id, true);
				}
			}
		}
	}
});
