Clifton.trade.TradeBlotterWindow = Ext.extend(TCG.app.Window, {
	id: 'tradeBlotterWindow',
	title: 'Trading Blotter',
	iconCls: 'blotter',
	width: 1700,
	height: 700,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.trade.TradeBlotterAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [

			{
				title: 'All Trades',
				items: [{xtype: 'trade-blotter-all-trades-gridpanel'}]
			},


			{
				title: 'Draft Trades',
				items: [{xtype: 'trade-blotter-draft-trades-gridpanel'}]
			},


			{
				title: 'Pending Trades',
				items: [{xtype: 'trade-blotter-pending-trades-gridpanel'}]
			},


			{
				title: 'Approved Trades',
				items: [{xtype: 'trade-blotter-approved-trades-gridpanel'}]
			},


			{
				title: 'Active Orders',
				items: [{xtype: 'trade-blotter-active-orders-panel'}]
			},


			{
				title: 'Booked Trades',
				items: [{
					name: 'tradeListFind',
					xtype: 'trade-blotter-base-gridpanel',
					instructions: 'Booked trades have been successfully executed and posted to the general ledger.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client Account', width: 45, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true},
						{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
						{
							header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{
							header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
							renderer: function(v, metaData) {
								metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
								return v;
							}
						},
						{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
						{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', width: 22, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
						{header: 'Avg Price', width: 13, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
						{header: 'Expected Price', width: 13, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
						{header: 'Accounting Notional', width: 15, dataIndex: 'accountingNotional', type: 'currency'},
						{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
						{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
						{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
						{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
						{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}, defaultSortColumn: true},
						{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Destination', width: 12, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
						{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
						{header: 'Execution Type', width: 14, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
						{header: 'Limit Price', width: 12, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
						{header: 'Traded On', width: 11, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
						{header: 'Settled On', width: 11, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}
					],
					getLoadParams: async function(firstLoad) {
						if (firstLoad) {
							// try to get trader's team: only if one
							const team = await Clifton.security.user.getUserTeam(this);
							if (TCG.isNotNull(team)) {
								this.setFilterValue('clientInvestmentAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
							}
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
						}
						return this.applyTopToolbarFilterLoadParams({
							readUncommittedRequested: true,
							workflowStateName: 'Booked',
							excludeTradeGroupTypeName: 'Roll Trade'
						});
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Cancelled Trades',
				items: [{
					name: 'tradeListFind',
					xtype: 'trade-blotter-base-gridpanel',
					instructions: 'Cancelled trades were cancelled prior to execution. No part of a cancelled trade was filled.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client Account', width: 45, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true},
						{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
						{
							header: 'Buy/Sell', width: 8, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{
							header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
							renderer: function(v, metaData) {
								metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
								return v;
							}
						},
						{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
						{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', width: 22, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
						{header: 'Avg Price', width: 12, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
						{header: 'Expected Price', width: 12, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
						{header: 'Accounting Notional', width: 15, dataIndex: 'accountingNotional', type: 'currency'},
						{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
						{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
						{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
						{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
						{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{header: 'Trader', width: 13, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}},
						{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Destination', width: 15, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
						{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
						{header: 'Execution Type', width: 14, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
						{header: 'Limit Price', width: 12, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
						{header: 'Traded On', width: 11, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
						{header: 'Settled On', width: 11, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const trader = TCG.getCurrentUser();
							this.setFilterValue('traderUser.label', {value: trader.id, text: trader.label});
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
						}
						return this.applyTopToolbarFilterLoadParams({
							readUncommittedRequested: true,
							workflowStateName: 'Cancelled',
							excludeTradeGroupTypeName: 'Roll Trade'
						});
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Roll Trades',
				items: [{
					name: 'tradeListFind',
					xtype: 'trade-blotter-base-gridpanel',
					instructions: 'Lists all roll trade details for all trades. Defaults trades to today\'s and future trades for current user.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Workflow Status', width: 15, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}},
						{header: 'Workflow State', width: 15, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, defaultSortColumn: true},
						{header: 'Client Account', width: 45, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true},
						{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
						{
							header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{
							header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
							renderer: function(v, metaData) {
								metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
								return v;
							}
						},
						{header: 'Quantity', width: 12, dataIndex: 'quantityIntended', type: 'float', useNull: true},
						{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', width: 22, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
						{header: 'Avg Price', width: 15, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
						{header: 'Expected Price', width: 15, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
						{header: 'Accounting Notional', width: 20, dataIndex: 'accountingNotional', type: 'currency'},
						{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
						{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
						{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
						{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
						{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{header: 'Trader', width: 15, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}},
						{header: 'FIX', width: 7, dataIndex: 'fixTrade', type: 'boolean', hidden: true},
						{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Destination', width: 15, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
						{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
						{header: 'Execution Type', width: 14, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
						{header: 'Limit Price', width: 12, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
						{header: 'Traded On', width: 12, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
						{header: 'Settled On', width: 12, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const trader = TCG.getCurrentUser();
							this.setFilterValue('traderUser.label', {value: trader.id, text: trader.label});
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
						}
						return this.applyTopToolbarFilterLoadParams({
							readUncommittedRequested: true,
							includeTradeGroupTypeName: 'Roll Trade'
						});
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true
					}
				}]
			}]
	}]
});
