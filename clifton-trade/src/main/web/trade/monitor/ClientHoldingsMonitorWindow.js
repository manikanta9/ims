Clifton.trade.monitor.ClientHoldingsMonitorWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client Account',
	iconCls: 'account',
	width: 1200,
	height: 650,
	items: [
		{
			xtype: 'tabpanel',
			requiredFormIndex: 0,
			items: [
				{
					title: 'Position Monitor',
					items: [
						{
							xtype: 'formpanel',
							url: 'investmentAccount.json',
							controlWindowModified: false,
							readOnly: true,
							layout: 'vbox',
							layoutConfig: {align: 'stretch'},

							items: [
								{
									xtype: 'panel',
									layout: 'column',
									defaults: {layout: 'form'},
									items: [
										{
											columnWidth: .40,
											labelWidth: 100,
											defaults: {anchor: '-20'},
											items: [
												{fieldLabel: 'Client', name: 'businessClient.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'businessClient.id'},
												{fieldLabel: 'Account Name', name: 'name', xtype: 'textfield', readOnly: true},
												{fieldLabel: 'Account Number', name: 'number', xtype: 'textfield', readOnly: true},
												{fieldLabel: 'Account Label', name: 'label', xtype: 'linkfield', detailIdField: 'id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
												{fieldLabel: 'Inception Date', name: 'inceptionDate', xtype: 'datefield'},
												{
													fieldLabel: 'Financial Advisors', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailPageWhenNoId: true, loadDefaultDataAfterRender: true, value: 'show',
													getDefaultData(formPanel) {
														return {defaultActiveTabName: 'Contacts', additionalComponentConfig: {contactType: {label: 'Financial Advisor'}}};
													}
												}
											]
										},
										{
											columnWidth: .60,
											items: [
												{
													xtype: 'fieldset',
													collapsible: false,
													title: 'Position Summary',
													defaults: {
														xtype: 'panel', layout: 'column',
														defaults: {
															columnWidth: .23, layout: 'form', labelWidth: 0, layoutConfig: {extraCls: 'x-form-item x-form-item-label'},
															defaults: {xtype: 'currencyfield', decimalPrecision: 2, readOnly: true, submitValue: false, hideLabel: true, anchor: '-5'}
														}
													},
													items: [
														{
															items: [
																{
																	columnWidth: .08,
																	items: [
																		{value: '', xtype: 'textfield', style: 'border-color:transparent;'},
																		{value: 'Long:', xtype: 'textfield', style: 'border-color:transparent;'},
																		{value: 'Short:', xtype: 'textfield', style: 'border-color:transparent;'}
																	]
																},
																{
																	items: [
																		{value: 'Equity', xtype: 'textfield', style: 'text-align: center;border-color:transparent;'},
																		{name: 'longEquityTotal'},
																		{name: 'shortEquityTotal'}
																	]
																},
																{
																	items: [
																		{value: 'Options', xtype: 'textfield', style: 'text-align: center;border-color:transparent;'},
																		{name: 'longOptionTotal'},
																		{name: 'shortOptionTotal'}
																	]
																},
																{
																	items: [
																		{value: 'Cash', xtype: 'textfield', style: 'text-align: center;border-color:transparent;'},
																		{name: 'longCashTotal'},
																		{name: 'shortCashTotal'}
																	]
																},
																{
																	items: [
																		{value: 'Total', xtype: 'textfield', style: 'text-align: center;border-color:transparent;'},
																		{name: 'longTotal'},
																		{name: 'shortTotal'}
																	]
																}
															]
														},
														{html: '<hr/>', xtype: 'label', colspan: 5, width: '99%'},
														{
															items: [
																{
																	columnWidth: .08,
																	items: [
																		{value: 'Net:', xtype: 'textfield', style: 'border-color:transparent;'}
																	]
																},
																{
																	items: [
																		{name: 'equityTotal'}
																	]
																},
																{
																	items: [
																		{name: 'optionTotal'}
																	]
																},
																{
																	items: [
																		{name: 'cashTotal'}
																	]
																},
																{
																	items: [
																		{name: 'netTotal'}
																	]
																}
															]
														}
													]
												}
											]
										}
									]
								},
								{
									xtype: 'panel',
									flex: 1,
									layout: 'fit',
									layoutConfig: {align: 'stretch'},
									items: [
										{
											xtype: 'trade-position-monitor-grid',
											defaultViewName: 'Market Value',
											includeTradingActions: true,
											includeOperationsActions: true,

											getTopToolbarFilters: function(toolbar) {
												return [
													{fieldLabel: 'Investment Type', name: 'investmentTypeName', width: 90, hiddenName: 'investmentTypeId', xtype: 'toolbar-combo', loadAll: true, url: 'investmentTypeList.json', linkedFilter: 'investmentSecurity.instrument.hierarchy.investmentType.name'},
													{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'toolbar-datefield', hidden: true}
												];
											},

											getLoadParams: function(firstLoad) {
												const gridPanel = this;
												const defaultParams = {
													requestedMaxDepth: 6,
													requestedPropertiesToExclude: 'accountingBalanceSearchForm'
												};
												if (firstLoad) {
													// inherit passed in auto-subscription flag
													const windowDefaultData = TCG.getParentFormPanel(this).getWindow().defaultData;
													if (windowDefaultData && TCG.isTrue(windowDefaultData.disableAutoSubscription)) {
														const actionsMenu = TCG.getChildByName(this.getTopToolbar(), 'actionsMenu');
														if (actionsMenu) {
															const disableSubscriptionsCheckbox = TCG.getChildByName(actionsMenu.menu, 'disableSubscriptionCheckbox');
															if (disableSubscriptionsCheckbox) {
																disableSubscriptionsCheckbox.setChecked(windowDefaultData.disableAutoSubscription);
															}
														}
													}

													TCG.getChildByName(this.getTopToolbar(), 'balanceDate').setValue(new Date());
													return TCG.data.getDataPromise('investmentAccount.json?id=' + gridPanel.getWindow().getMainFormId(), this)
														.then(clientAccount => {
															gridPanel.setFilterValue('clientInvestmentAccount.label', {value: clientAccount.id, text: clientAccount.label});
															defaultParams['transactionDate'] = this.getSelectedBalanceDate().format('m/d/Y');
															return gridPanel.applyTopToolbarFilterLoadParams(defaultParams);
														});
												}
												else {
													defaultParams['transactionDate'] = this.getSelectedBalanceDate().format('m/d/Y');
													return gridPanel.applyTopToolbarFilterLoadParams(defaultParams);
												}
											},

											columnOverrides: [
												{dataIndex: 'totalValue', summaryType: 'sum'}
											],

											recordsUpdated: function() {
												const store = this.grid.store;
												const form = TCG.getParentFormPanel(this).getForm();

												let longEquityTotal = 0;
												let shortEquityTotal = 0;
												let longOptionTotal = 0;
												let shortOptionTotal = 0;
												let cash = void 0;

												store.each(record => {
													if (TCG.isNull(cash)) {
														// each record has the cash balance
														cash = TCG.getValue('cashBalance', record.json) || 0;
													}

													const investmentType = TCG.getValue('investmentSecurity.instrument.hierarchy.investmentType.name', record.json);
													const long = record.get('quantity') > 0;
													const value = record.get('totalValue');
													if (investmentType === 'Options') {
														if (long) {
															longOptionTotal += value;
														}
														else {
															shortOptionTotal += value;
														}
													}
													else if (investmentType === 'Stocks' || investmentType === 'Benchmarks') {
														if (long) {
															longEquityTotal += value;
														}
														else {
															shortEquityTotal += value;
														}
													}
												});

												const equityTotal = longEquityTotal + shortEquityTotal;
												const optionTotal = longOptionTotal + shortOptionTotal;

												form.findField('longEquityTotal').setValue(longEquityTotal);
												form.findField('shortEquityTotal').setValue(shortEquityTotal);
												form.findField('longOptionTotal').setValue(longOptionTotal);
												form.findField('shortOptionTotal').setValue(shortOptionTotal);
												form.findField('equityTotal').setValue(equityTotal);
												form.findField('optionTotal').setValue(optionTotal);

												if (cash > 0) {
													form.findField('longCashTotal').setValue(cash);
													form.findField('shortCashTotal').setValue(0);
													form.findField('longTotal').setValue(longEquityTotal + longOptionTotal + cash);
													form.findField('shortTotal').setValue(shortEquityTotal + shortOptionTotal);
												}
												else {
													form.findField('longCashTotal').setValue(0);
													form.findField('shortCashTotal').setValue(cash);
													form.findField('longTotal').setValue(longEquityTotal + longOptionTotal);
													form.findField('shortTotal').setValue(shortEquityTotal + shortOptionTotal + cash);
												}
												form.findField('cashTotal').setValue(cash);
												form.findField('netTotal').setValue(equityTotal + optionTotal + cash);
											},

											plugins: [
												{ptype: 'gridsummary'},
												{ptype: 'trade-restriction-aware'}
											]
										}
									]
								}
							]
						}
					]
				},
				{
					title: 'Security Targets',
					items: [{
						xtype: 'security-target-gridpanel',
						columnOverrides: [
							{dataIndex: 'clientInvestmentAccount.label', hidden: true},
							{dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true},
							{dataIndex: 'clientInvestmentAccount.businessService.name', hidden: true}
						]
					}]
				},
				{
					title: 'Utilization',
					items: [
						{
							xtype: 'trade-utilization-grid',
							includeTradingActions: true,
							includeOperationsActions: true,

							getTopToolbarFilters: function(toolbar) {
								return [
									{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'toolbar-datefield'}
								];
							},

							getLoadParams: function(firstLoad) {
								const gridPanel = this;
								const defaultParams = {
									'holdingAccountPurpose.name': 'Trading: Options',
									requestedMaxDepth: 6
								};
								if (firstLoad) {
									TCG.getChildByName(gridPanel.getTopToolbar(), 'balanceDate').setValue(new Date());
									if (gridPanel.defaultViewName) {
										gridPanel.setDefaultView(gridPanel.defaultViewName);
									}

									return TCG.data.getDataPromise('investmentAccount.json?id=' + gridPanel.getWindow().getMainFormId(), this)
										.then(clientAccount => {
											gridPanel.setFilterValue('securityTarget.clientInvestmentAccount.label', {value: clientAccount.id, text: clientAccount.label});
											return gridPanel.applyTopToolbarFilterLoadParams(defaultParams);
										});
								}
								else {
									return gridPanel.applyTopToolbarFilterLoadParams(defaultParams);
								}
							},

							columnOverrides: [
								{dataIndex: 'securityTarget.clientInvestmentAccount.label', hidden: true},
								{dataIndex: 'securityTarget.clientInvestmentAccount.number', hidden: false},
								{dataIndex: 'securityTarget.clientInvestmentAccount.name', hidden: false}
							]
						}
					]
				}
			]
		}
	]
});
