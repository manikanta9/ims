TCG.use('Clifton.trade.monitor.BaseTradeModalWindow');

Clifton.trade.monitor.PositionCloseTradeWindow = Ext.extend(Clifton.trade.monitor.BaseTradeModalWindow, {
	title: 'Position Close',

	items: [
		{
			xtype: 'base-modal-trade-formpanel',
			instructions: 'Specify the destination for the trades',

			afterRenderPromise: function(formPanel) {
				// If default data was passed in with tradeDetailList data, the grid needs to see that data
				formPanel.afterRenderRefreshTradeEntryGrid();
				formPanel.updateTitle();

				const tradeDetailListGrid = formPanel.getChildTradeEntryGrid();
				if (TCG.isTrue(formPanel.getForm().findField('optionAssignment').getValue())) {
					const columnModel = tradeDetailListGrid.getColumnModel();
					columnModel.getCellEditor(columnModel.findColumnIndex('investmentSecurity.underlyingPrice')).setDisabled(true);
				}

				const securityId = formPanel.getForm().findField('investmentSecurity.id').getValue();
				if (securityId) {
					// if security is provided in the header hide it from the grid
					const columnModel = tradeDetailListGrid.getColumnModel();
					columnModel.setHidden(columnModel.findColumnIndex('investmentSecurity.symbol'), true);
					TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, formPanel)
						.then(security => formPanel.processSecuritySelection.call(formPanel, security));
				}
			},

			getFormLabel: function() {
				let suffix = this.getForm().formValues.tradeType.name;
				if (this.getForm().formValues.tradeGroupType) {
					suffix = this.getForm().formValues.tradeGroupType.name;
				}
				return `${this.getWindow().title} - ${suffix}`;
			},

			formPanelTopItems: [
				{name: 'underlyingInvestmentSecurity.id', xtype: 'hidden', submitValue: false},
				{name: 'optionAssignment', xtype: 'checkbox', boxLabel: 'Process trades as an Option Assignment', hidden: true}
			],

			formPanelSecurityColumnItems: [
				{name: 'investmentType.id', xtype: 'hidden', submitValue: false},
				{fieldLabel: 'Trade Type', name: 'tradeType.name', hiddenName: 'tradeType.id', xtype: 'combo', url: 'tradeTypeListFind.json', loadAll: true, allowBlank: false, readOnly: true},
				{name: 'tradeType.holdingAccountPurpose.name', xtype: 'hidden', submitValue: false},
				{name: 'payingSecurity.id', xtype: 'hidden'},
				{name: 'investmentSecurity.instrument.hierarchy.otc', xtype: 'hidden', submitValue: false},
				{fieldLabel: 'Security', name: 'investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id'}
			],

			formPanelBottomItems: [
				{
					xtype: 'base-modal-trade-group-entry-grid',

					addToolbarButtons: function(toolbar) {
						toolbar.add({
							text: 'Apply Changes',
							tooltip: 'Apply available changes to the grid details based on selected form values. Changes should be automatically applied when changing applicable values.',
							iconCls: 'preview',
							handler: () => this.reload()
						});
						toolbar.add('-', {
							text: 'Debit Balance Report',
							tooltip: 'Generate a debit balance report email showing cash needed for each client account trade in the below list of trades.',
							iconCls: 'email',
							handler: async () => {
								const gridPanel = this;
								const records = this.getStore().getRange(0, this.getStore().getTotalCount());
								const cashDetails = [];
								for (const record of records) {
									const row = await generateCashDebitDetailHtmlTableRow(record);
									if (TCG.isNotBlank(row)) {
										cashDetails.push(row);
									}
								}

								if (cashDetails.length > 0) {
									const subject = 'Option Debit Balance Report Closing Positions';
									const fileName = 'CloseDebitBalanceReport.eml';
									const emailContent = '<!DOCTYPE html><html lang="en"><head><style>'
										+ 'p{font-family:arial,sans-serif;font-size:14px;}table{border-collapse:collapse;font-family:arial,sans-serif;font-size:14px;}th,td{border:1px solid black;}th{padding:5px;}'
										+ '</style><title></title></head><body>'
										+ '<p>Debit Balance Report Closing:</p>'
										+ '<table><thead><tr><th>Client</th><th>Account</th><th>Close Security Symbol</th><th>Close Quantity</th><th>Close Fill Price</th><th>Underlying Price</th><th>Actual Cash</th><th>Close Cost</th><th>Cash Needed</th></tr></thead><tbody>'
										+ cashDetails.join('')
										+ '</body></html>';
									TCG.generateEmailFile(subject, 'text/html', emailContent, fileName, void 0, 'PRATrading@paraport.com');
								}
								else {
									TCG.showInfo('There are no rows with a negative cash balance', 'Debit Balance Report');
								}

								async function generateCashDebitDetailHtmlTableRow(record) {
									const cashBalance = record.data['cashBalance'];
									const closeCost = record.data['netTradeCashImpact'];
									const cashMarginAccount = await gridPanel.isClientInvestmentAccountNegativeCashBalanceAllowed(record);

									if (closeCost > 0 || (cashBalance + closeCost) >= 0 || cashMarginAccount) {
										return void 0;
									}


									const [clientAccount, holdingAccount] = await Promise.all([
										TCG.data.getDataPromise('investmentAccount.json', gridPanel, {params: {id: record.data['clientInvestmentAccount.id'], requestedProperties: 'name', requestedPropertiesRoot: 'data'}}),
										TCG.data.getDataPromise('investmentAccount.json', gridPanel, {params: {id: record.data['holdingInvestmentAccount.id'], requestedProperties: 'number', requestedPropertiesRoot: 'data'}})
									]);

									return '<tr>' +
										'<td style="padding:5px;">' + clientAccount.name + '</td>' +
										'<td style="padding:5px;">' + holdingAccount.number + '</td>' +
										'<td style="padding:5px;">' + record.data['investmentSecurity.symbol'] + '</td>' +
										'<td style="padding:5px;text-align:right;">' + record.data['calculatedCloseQuantity'] + '</td>' +
										'<td style="padding:5px;text-align:right;">' + record.data['investmentSecurity.price'] + '</td>' +
										'<td style="padding:5px;text-align:right;">' + TCG.getValue('investmentSecurity.underlyingPrice', record.json) + '</td>' +
										'<td style="padding:5px;text-align:right;">' + formatCash(cashBalance) + '</td>' +
										'<td style="padding:5px;text-align:right;">' + formatCash(closeCost) + '</td>' +
										'<td style="padding:5px;text-align:right;">' + formatCash(Math.abs(cashBalance < 0 ? closeCost : cashBalance + closeCost)) + '</td>' +
										'</tr>';
								}

								function formatCash(cashValue) {
									return TCG.numberFormat(cashValue, cashValue < 0 ? '($0,000.00)' : '$0,0000.00');
								}
							}
						});

						this.addTradeUnderlyingSecurityToToolBar(toolbar);
					},

					updateQuantity: async function(gridRecord, newQuantity) {
						let quantity = newQuantity || gridRecord.get('quantityIntended');
						const originalInsufficientQuantity = gridRecord.get('insufficientQuantity');
						const originalInsufficientCash = gridRecord.get('insufficientCash');
						const originalQuantity = gridRecord.get('originalQuantity');
						if (quantity === 0) {
							quantity = originalQuantity;
						}

						let updatedQuantity = quantity;
						if (updatedQuantity > originalQuantity) {
							// cannot close more than we own
							gridRecord.set('insufficientQuantity');
							updatedQuantity = originalQuantity;
						}
						const originalQuantityIntended = this.getColumnModel().getCellEditor(this.getColumnModel().findColumnIndex('quantityIntended')).field.startValue || 0;
						const originalCalculatedCloseQuantity = gridRecord.get('calculatedCloseQuantity') || 0;

						try {
							// If trading underlying validate it before validating option close trade
							const tradeUnderlying = TCG.isTrue(gridRecord.get('tradeUnderlying'));

							if (TCG.isTrue(tradeUnderlying)) {
								try {
									const validatedUnderlyingQuantity = await this.validateUnderlyingQuantityAgainstAvailable(gridRecord, quantity);
									gridRecord.set('underlyingQuantity', validatedUnderlyingQuantity);
								}
								catch (e) {
									console.warn(e);
									gridRecord.set('underlyingQuantity', 0);
								}
							}

							const validatedQuantity = await this.validateQuantityAgainstCash(updatedQuantity, gridRecord);
							gridRecord.set('quantityIntended', validatedQuantity);
						}
						catch (e) {
							console.warn(e);
							// roll back values
							gridRecord.set('quantityIntended', originalQuantityIntended);
							gridRecord.set('calculatedCloseQuantity', originalCalculatedCloseQuantity);
							gridRecord.set('insufficientQuantity', originalInsufficientQuantity);
							gridRecord.set('insufficientCash', originalInsufficientCash);
						}
					},

					validateUnderlyingQuantityAgainstAvailable: async function(gridRecord, optionCloseQuantity) {
						const underlyingQuantity = gridRecord.get('underlyingQuantity');
						let availableUnderlyingQuantity = 0;
						const underlyingBuy = TCG.isTrue(gridRecord.get('underlyingBuy'));

						// validate underlying sell trade quantity
						if (TCG.isFalse(underlyingBuy)) {
							const optionMultiplier = gridRecord.get('investmentSecurity.priceMultiplier');
							availableUnderlyingQuantity = TCG.isNotBlank(optionMultiplier) && TCG.isNotBlank(optionCloseQuantity) ? optionCloseQuantity * optionMultiplier : 0;
						}
						else if (TCG.isTrue(gridRecord.get('ignoreCash'))) {
							// a buy with no cash limitations
							return underlyingQuantity;
						}
						else {
							// validate underlying buy trade: cashBalance must be sufficient for purchase. Note: for closing trades, this is not a likely scenario
							const cashBalance = gridRecord.data['cashBalance'];
							if (TCG.isBlank(cashBalance) || cashBalance < 0.00) {
								return 0;
							}

							const underlyingPriceMultiplier = gridRecord.json.investmentSecurity.underlyingSecurity.priceMultiplier;
							const form = TCG.getParentFormPanel(this).getForm();
							const optionAssignment = form.findField('optionAssignment').getValue();
							let underlyingPrice;
							// cannot use or reset underlying price if this is an option assignment, as it has been previously set to the strike price for the assignment's sale of underlying.
							if (TCG.isFalse(optionAssignment) && TCG.isNotBlank(gridRecord.json.investmentSecurity.underlyingPrice)) {
								underlyingPrice = gridRecord.json.investmentSecurity.underlyingPrice;
							}
							else {
								const underlyingSecurityId = await this.getUnderlyingSecurityId(gridRecord, gridRecord.get('investmentSecurity.id'));
								underlyingPrice = await this.getUnderlyingPrice(gridRecord, underlyingSecurityId, underlyingBuy);
							}

							const pricePerUnderlyingShare = underlyingPriceMultiplier * (TCG.isNotBlank(underlyingPrice) ? underlyingPrice : 0.00);
							if (pricePerUnderlyingShare > 0.00) {
								availableUnderlyingQuantity = Math.trunc(cashBalance / pricePerUnderlyingShare);
							}
						}

						return underlyingQuantity <= availableUnderlyingQuantity ? underlyingQuantity : availableUnderlyingQuantity;
					},

					validateQuantityAgainstCash: async function(quantity, gridRecord) {
						let returnQuantity = quantity;
						gridRecord.set('calculatedCloseQuantity', quantity);
						const form = TCG.getParentFormPanel(this).getForm();
						const buy = gridRecord.get('buy');
						const securityId = gridRecord.get('investmentSecurity.id');

						const optionAssignment = form.findField('optionAssignment').getValue();
						const price = await this.getOptionPrice(gridRecord, securityId, buy, optionAssignment);
						const cashBalance = gridRecord.get('cashBalance');
						const securityMultiplier = TCG.getValue('investmentSecurity.priceMultiplier', gridRecord.json);
						const closeCashImpact = quantity * securityMultiplier * price * (buy ? -1 : 1);
						let newCashBalance = cashBalance + closeCashImpact;

						// calculate underlying assignment if applicable
						let underlyingCashImpact = 0;
						if (optionAssignment) {
							// price is strike of the option
							const underlyingPrice = TCG.getValue('investmentSecurity.underlyingPrice', gridRecord.json) || gridRecord.get('investmentSecurity.underlyingPrice');
							underlyingCashImpact = quantity * securityMultiplier * underlyingPrice * (buy ? 1 : -1);
						}
						underlyingCashImpact = await this.getUnderlyingCashImpact(gridRecord, securityId, newCashBalance, underlyingCashImpact, optionAssignment);

						const netTradeCashImpact = closeCashImpact + underlyingCashImpact;
						// note: newCashBalance at this point already has closeCashImpact added in from previous calculation.
						newCashBalance += underlyingCashImpact;
						if (netTradeCashImpact < 0 && newCashBalance < 0) {
							gridRecord.set('insufficientCash', true);
							if (!gridRecord.data['ignoreCash']) {
								const cashCanGoNegative = await this.isClientInvestmentAccountNegativeCashBalanceAllowed(gridRecord);
								if (TCG.isFalse(cashCanGoNegative)) {
									// adjust the quantity to 0 if insufficient cash to avoid partial closes
									returnQuantity = 0;
								}
							}
						}
						else {
							gridRecord.set('insufficientCash', false);
						}

						gridRecord.set('netTradeCashImpact', netTradeCashImpact);
						return returnQuantity;
					},

					getOptionPrice: async function(gridRecord, securityId, buy, optionAssignment) {
						let price = gridRecord.get('investmentSecurity.price');
						if (optionAssignment) {
							price = 0;
						}
						else if (!price) {
							price = await this.getSecurityLivePrice(securityId, buy);
							if (TCG.isBlank(price)) {
								TCG.showError('Unable to obtain price for closing security. Please manually enter a price.', 'Price Failure');
								throw new Error('Unable to obtain price for closing security.');
							}
							gridRecord.set('investmentSecurity.price', price);
						}
						return price;
					},

					getUnderlyingCashImpact: async function(gridRecord, optionSecurityId, cashBalance, startingUnderlyingCashImpact, optionAssignment) {
						let underlyingCashImpact = startingUnderlyingCashImpact;
						const underlyingEnabledCheckbox = TCG.getChildByName(this.getTopToolbar(), 'tradeUnderlyingEnabled');
						const underlyingEnabled = underlyingEnabledCheckbox ? TCG.isTrue(underlyingEnabledCheckbox.getValue()) : false;
						if (underlyingEnabled && TCG.isTrue(gridRecord.get('tradeUnderlying'))) {
							const underlyingSecurityId = await this.getUnderlyingSecurityId(gridRecord, optionSecurityId);
							const underlyingBuy = TCG.isTrue(gridRecord.get('underlyingBuy'));
							const underlyingPrice = await this.getUnderlyingPrice(gridRecord, underlyingSecurityId, underlyingBuy);

							if (underlyingSecurityId) {
								// if this is an option assignment, we need to get the live price, since the underlying price on the record is set to the option's strike price
								const underlyingLivePrice = !optionAssignment ? underlyingPrice : await this.getSecurityLivePrice(underlyingSecurityId, underlyingBuy);
								const underlyingPriceMultiplier = gridRecord.json.investmentSecurity.underlyingSecurity.priceMultiplier;
								const originalUnderlyingQuantity = gridRecord.get('underlyingQuantity') || 0;
								let underlyingQuantity = originalUnderlyingQuantity;

								if (underlyingQuantity === 0 && cashBalance < 0) {
									underlyingQuantity = Math.ceil(Math.abs(cashBalance) / underlyingLivePrice);
								}
								const underlyingTotal = Math.abs(gridRecord.data['utilizationUnderlyingTarget']);
								if (underlyingQuantity > underlyingTotal) {
									underlyingQuantity = underlyingTotal;
								}
								if (originalUnderlyingQuantity !== underlyingQuantity) {
									gridRecord.set('underlyingQuantity', underlyingQuantity);
								}
								underlyingCashImpact += underlyingQuantity * underlyingPriceMultiplier * underlyingLivePrice * (underlyingBuy ? -1 : 1);
								gridRecord.set('underlyingCashImpact', underlyingCashImpact);
							}
							else {
								console.warn('Trade Underlying is checked, but there is no underlying security applicable to traded securities.');
							}
						}
						return underlyingCashImpact;
					},

					getUnderlyingSecurityId: async function(gridRecord, securityId) {
						let underlyingSecurityId = TCG.getValue('investmentSecurity.underlyingSecurity.id', gridRecord.json);
						if (!underlyingSecurityId) {
							underlyingSecurityId = await TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, this)
								.then(security => security.underlyingSecurity.id);
						}
						return underlyingSecurityId;
					},

					getUnderlyingPrice: async function(gridRecord, underlyingId, buy) {
						const underlyingSecurityId = underlyingId;
						let underlyingPrice = TCG.getValue('investmentSecurity.underlyingPrice', gridRecord.json) || gridRecord.get('investmentSecurity.underlyingPrice');
						if (!underlyingPrice) {
							underlyingPrice = await this.getSecurityLivePrice(underlyingSecurityId, buy);
							if (TCG.isBlank(underlyingPrice)) {
								TCG.showError('Unable to obtain price for underlying security. Please manually enter a price.', 'Price Failure');
								throw new Error('Unable to obtain price for underlying security.');
							}
							gridRecord.set('investmentSecurity.underlyingPrice', underlyingPrice);
						}
						return underlyingPrice;
					},

					onAfterEdit: function(e, field) {
						const column = this.getColumnModel().getColumnAt(e.column);
						if (column.dataIndex === 'underlyingQuantity') {
							e.grid.updateQuantityEditAware(e.record);
						}
					},

					// Fields not applicable to TradeEntryDetail
					doNotSubmitFields: ['investmentSecurity.id', 'investmentSecurity.price', 'investmentSecurity.underlyingPrice', 'investmentSecurity.priceMultiplier', 'buy', 'originalQuantity', 'utilizationUnderlyingTarget', 'utilizationTotalUsage', 'utilizationCovered', 'utilizationPending', 'utilizationFrozen', 'utilizationAvailable', 'coveredPercent', 'cashBalance', 'insufficientCash', 'insufficientQuantity', 'netTradeCashImpact', 'ignoreCash', 'calculatedCloseQuantity', 'validateCashMargin', 'cashMargin', 'clientAccountNotes', 'underlyingBuy', 'tradeUnderlying', 'underlyingQuantity'],

					columnOverrides: [
						{
							dataIndex: 'clientInvestmentAccount.label', width: 200,
							editor: {
								xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', displayField: 'label', allowBlank: false, readOnly: true, addGridCopyContextMenu: false
							},
							renderer: function(value, metaData, record) {
								const cashBalance = TCG.getValue('cashBalance', record.json) || 0;
								const tradeCashImpact = record.get('netTradeCashImpact') || 0;
								const cashBalanceQtip = TCG.trimWhitespace(`
									<br/><br/>
									<table style="background-color: #eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td nowrap="true" style="width: 100px;">Cash Balance:</td>
											<td style="text-align:right;">${TCG.renderAmount(cashBalance, true, '$0,000')}</td>
										</tr>
										<tr>
											<td nowrap="true" style="width: 100px;">Trade Cash Impact:</td>
											<td style="text-align:right;">${TCG.renderAmount(tradeCashImpact, true, '$0,000')}</td>
										</tr>
										<tr><td colspan="5"><hr/></td></tr>
										<tr>
											<td nowrap="true" style="width: 100px;">Net Total:</td>
											<td style="text-align:right;">${TCG.renderAmount((cashBalance + tradeCashImpact), true, '$0,000')}</td>
										</tr>
									</table>
								`);
								this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(value, metaData, record, false, cashBalanceQtip);
								// no security column in this grid, so highlight client account column if there is a security restriction
								this.scope.gridPanel.applyTradeRestrictionGridSecurityColumnFreeze(metaData, record, record.underlyingSecurity);
								return value;
							},
							eventListeners: {
								contextmenu: function(column, grid, rowIndex, event) {
									const restrictionItems = grid.getRestrictionActionMenuItems(rowIndex, column);
									if (restrictionItems.length > 0) {
										const menu = new Ext.menu.Menu({
											items: [
												...restrictionItems.length > 0 ? ['-', ...restrictionItems] : []
											]
										});

										event.preventDefault();
										menu.showAt(event.getXY());
									}
								}
							}
						},
						{
							dataIndex: 'holdingInvestmentAccount.label', width: 200, renderer: TCG.renderValueWithTooltip,
							editor: {
								xtype: 'combo', url: 'investmentAccountListFind.json', displayField: 'label', allowBlank: false, addGridCopyContextMenu: false,
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const grid = combo.gridEditor.containerGrid;
									const formPanel = TCG.getParentFormPanel(grid);
									const form = formPanel.getForm();
									combo.store.baseParams = {
										ourAccount: false,
										workflowStatusNameEquals: 'Active',
										mainAccountId: TCG.getValue('clientInvestmentAccount.id', combo.gridEditor.record.json),
										mainPurpose: formPanel.getFormValue('tradeType.holdingAccountPurpose.name'),
										mainPurposeActiveOnDate: form.findField('tradeDate').value
									};
								},
								listeners: {
									change: function(combo, newValue, oldValue) {
										const gridPanel = combo.gridEditor.containerGrid;
										const record = combo.gridEditor.record;
										record.beginEdit();
										record.set('executingBrokerCompany.label', '');
										record.set('executingBrokerCompany.id', '');
										gridPanel.applyDefaultExecutingBroker(record, newValue)
											.then(() => record.endEdit());
									}
								}
							}
						},
						{
							dataIndex: 'quantityIntended',
							editor: {
								xtype: 'numberfield', allowBlank: false, minValue: 0,
								listeners: {
									change: function(field, newValue, oldValue) {
										const gridPanel = field.gridEditor.containerGrid;
										const record = field.gridEditor.record;
										gridPanel.updateQuantityEditAware(record, newValue);
									}
								}
							},
							renderer: function(value, metaData, record) {
								let qtip = TCG.trimWhitespace(`
									<table style="background-color: #eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td>Original:</td>
											<td style="text-align:right;">${record.data['originalQuantity']}</td>
										</tr>
										<tr>
											<td>Intended:</td>
											<td style="text-align:right;">${record.data['calculatedCloseQuantity']}</td>
										</tr>
									</table>
								`);
								// Attach style for restrictions involving the account
								if (record && (record.data['insufficientCash'] || record.data['insufficientQuantity'])) {
									qtip += '<br/><br/>Insufficient ' + (record.data['insufficientQuantity'] ? 'quantity available to trade more than ' + value : 'cash.');
									if (record.get('insufficientCash')) {
										if (record.get('validateCashMargin')) {
											qtip += TCG.isTrue(record.get('cashMargin')) ? ' Client Investment Account is a Cash Margin account and the quantity was not adjusted.'
												: ' Client Investment Account is not a Cash Margin account and the quantity was adjusted.';
										}
										else {
											qtip += ' Client Investment Account\'s service processing type does not validate cash margin accounts and the quantity was not adjusted.';
										}
									}
									metaData.css += 'x-grid3-row-highlight-light-red';
								}

								metaData.attr = TCG.renderQtip(qtip);
								return TCG.numberFormat(value, '0,000', true);
							}
						},
						{header: 'Security', width: 150, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', renderer: TCG.renderValueWithTooltip},
						{
							header: 'Price', width: 50, dataIndex: 'investmentSecurity.price', type: 'float',
							editor: {
								xtype: 'pricefield', allowBlank: false, minValue: 0,
								listeners: {
									change: function(field, newValue, oldValue) {
										const gridPanel = field.gridEditor.containerGrid;
										const record = field.gridEditor.record;
										gridPanel.updateQuantityEditAware(record);
									}
								}
							}
						},
						{header: 'Security Multiplier', width: 40, dataIndex: 'investmentSecurity.priceMultiplier', type: 'float', hidden: true},
						{header: 'Buy', width: 40, dataIndex: 'buy', type: 'boolean', hidden: true},
						{header: 'Original Qty', dataIndex: 'originalQuantity', type: 'float', hidden: true},
						{dataIndex: 'expectedUnitPrice', hidden: true},
						{dataIndex: 'commissionPerUnit', hidden: true},
						{dataIndex: 'feeAmount', hidden: true},
						{
							header: 'Executing Broker', width: 150, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', useNull: false, renderer: TCG.renderValueWithTooltip,
							tooltip: 'Trade Executing Broker used for this client\'s trade. The default value is looked up by Trade Destination, active on Trade Date, and Holding Account Issuing Company. If the Holding Account defined is configured to be a Directed Broker, this value is populated to override the Executing Broker above.',

							editor: {
								xtype: 'combo', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json', tooltipField: 'label', detailPageClass: 'Clifton.business.company.CompanyWindow',

								beforequery: function(queryEvent) {
									// Reset Combo so re-queries each time (since each row has different results)
									this.resetStore();
									const editor = queryEvent.combo.gridEditor;
									const grid = editor.containerGrid;
									const form = TCG.getParentFormPanel(grid).getForm();
									queryEvent.combo.store.baseParams = {
										clientInvestmentAccountId: TCG.getValue('clientInvestmentAccount.id', editor.record.json),
										tradeDestinationId: form.findField('tradeDestination.id').getValue(),
										tradeTypeId: form.findField('tradeType.id').getValue(),
										activeOnDate: form.findField('tradeDate').value
									};
								}
							}
						},
						{
							header: 'Trade Destination', dataIndex: 'tradeDestination.name', width: 150, idDataIndex: 'tradeDestination.id', allowBlank: true,
							editor: {
								xtype: 'combo', url: 'tradeDestinationListFind.json',
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const grid = combo.gridEditor.containerGrid;
									const fp = TCG.getParentFormPanel(grid);

									// Clear Query Store so Re-Queries each time
									this.store.removeAll();
									this.lastQuery = null;

									const record = queryEvent.combo.gridEditor.record;
									const executingBrokerCompanyId = record.get('executingBrokerCompany.id');
									const tradeDate = fp.getForm().findField('tradeDate').value;
									const activeOnDate = TCG.isNotBlank(tradeDate) ? tradeDate : new Date().format('Y-m-d 00:00:00');
									queryEvent.combo.store.baseParams = {
										tradeTypeId: fp.getFormValue('tradeType.id'),
										executingBrokerCompanyId: executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : null,
										activeOnDate: activeOnDate
									};
								}
							}
						},
						{header: 'Underlying Target', dataIndex: 'utilizationUnderlyingTarget', type: 'float', hidden: true},
						{header: 'Usage Total', dataIndex: 'utilizationTotalUsage', type: 'float', hidden: true},
						{header: 'Usage Covered', dataIndex: 'utilizationCovered', type: 'float', hidden: true},
						{
							header: 'Covered %', dataIndex: 'coveredPercent', type: 'float', width: 80, tooltip: 'Coverage % for selected security and quantity (does not include positions beyond the quantity being traded).',
							renderer: function(value, metadata, record) {
								let returnValue = 0.00;
								const securityPriceMultiplier = record.data['investmentSecurity.priceMultiplier'];
								const quantityIntended = record.data['quantityIntended'];

								if (TCG.isNotBlank(quantityIntended) && TCG.isNotBlank(securityPriceMultiplier)) {
									const shares = quantityIntended * securityPriceMultiplier;
									const utilizationUnderlyingTarget = TCG.getValue('utilizationUnderlyingTarget', record.json);
									if (TCG.isNotBlank(utilizationUnderlyingTarget)) {
										returnValue = (utilizationUnderlyingTarget === 0.00) ? 0.00 : Math.abs(shares / utilizationUnderlyingTarget * 100.0);
									}
								}
								record.data['coveredPercent'] = returnValue;
								return TCG.renderAmount(returnValue, false, '0,000.00 %');
							}
						},
						{header: 'Usage Pending', dataIndex: 'utilizationPending', type: 'float', hidden: true},
						{header: 'Usage Frozen', dataIndex: 'utilizationFrozen', type: 'float', hidden: true},
						{
							header: 'Usage Available', dataIndex: 'utilizationAvailable', type: 'float', hidden: true,
							renderer: function(value, metaData, record) {
								return TCG.numberFormat(record.data['utilizationUnderlyingTarget'] - record.data['utilizationTotalUsage'], '0,000', true);
							}
						},
						{header: 'Cash', dataIndex: 'cashBalance', type: 'float', hidden: true},
						{
							header: 'Ignore Cash', dataIndex: 'ignoreCash', xtype: 'checkcolumn', width: 70,
							tooltip: 'If checked, the validation for cash will not automatically adjust the trade quantity allowing trades to be created',
							onClick: (grid, record) => {
								record.beginEdit();
								record.set('quantityIntended', record.get('originalQuantity'));
								grid.updateQuantityEditAware(record);
							}
						},
						{
							header: 'Trade Und.', dataIndex: 'tradeUnderlying', xtype: 'checkcolumn', width: 60, hidden: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
							tooltip: 'If checked, the underlying quantity will be used to trade the underlying security in addition to the securities included in the roll',
							onClick: (grid, record) => grid.updateQuantityEditAware(record)
						},
						{
							header: 'Und. Quantity', dataIndex: 'underlyingQuantity', width: 80, type: 'float', summaryType: 'sum', hidden: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
							tooltip: 'The quantity of the underlying security to trade. The value is only used if the Trade Underlying cell value is checked for the row.',
							editor: {
								xtype: 'spinnerfield', allowBlank: false, minValue: 0,
								listeners: {
									change: function(field, newValue, oldValue) {
										const gridPanel = field.gridEditor.containerGrid;
										const record = field.gridEditor.record;
										gridPanel.updateQuantityEditAware(record);
									}
								}
							},

							renderer: function(value, metaData, record) {
								if (value && value > 0) {
									metaData.attr = TCG.renderQtip(TCG.trimWhitespace(`
									<table style="background-color: #eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td>Underlying Price:</td>
											<td style="text-align:right;">${TCG.renderAmount(record.underlyingPrice, false, '$0,000')}</td>
										</tr>
										<tr><td colspan="2"><hr/></td></tr>
										<tr>
											<td>Cash Impact:</td>
											<td style="text-align:right;">${TCG.renderAmount(record.data['underlyingCashImpact'], true, '$0,000')}</td>
										</tr>
									</table>
								`));
								}
								return TCG.numberFormat(value, '0,000', true);
							}
						},
						{
							header: 'Und. Buy', dataIndex: 'underlyingBuy', xtype: 'checkcolumn', width: 60, hidden: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
							tooltip: 'If checked, the underlying trade will be a buy of the specified underlying quantity. If unchecked, the underlying trade will be a sell of the specified underlying quantity. This flag is only applicable if Trade Underlying is checked for this row.',
							onClick: (grid, record) => grid.updateQuantityEditAware(record)
						},
						{
							header: 'Und. Price', dataIndex: 'investmentSecurity.underlyingPrice', type: 'float', width: 60,
							editor: {
								xtype: 'pricefield', allowBlank: false, minValue: 0,
								listeners: {
									change: function(field, newValue, oldValue) {
										const gridPanel = field.gridEditor.containerGrid;
										const record = field.gridEditor.record;
										gridPanel.updateQuantityEditAware(record);
									}
								}
							}
						},
						{header: 'Account Notes', width: 200, dataIndex: 'clientAccountNotes', renderer: TCG.renderTextNowrapWithTooltip}
					]
				}]
		}
	]
});
