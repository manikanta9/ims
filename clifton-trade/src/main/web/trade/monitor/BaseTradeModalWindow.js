Clifton.trade.monitor.BaseTradeModalWindow = Ext.extend(TCG.app.OKCancelWindow, {
	iconCls: 'shopping-cart',
	height: 600,
	width: 1500,
	modal: true,
	allowOpenFromModal: true,
	okButtonText: 'Create Trades',
	okButtonTooltip: 'Create trades from the details entered on this window',

	submitTradesCallback: function(formPanel) {
		// Override to perform trade creation action
		Ext.Msg.alert('Failed to Trade', 'Callback to submit trades was not implemented.');
	},

	saveWindow: function() {
		const panel = this.getFormPanels(!this.forceModified)[0];

		if (TCG.getChildByName(panel, 'tradeDetailListGrid').store.modified.filter(record => record.editing).length > 0) {
			TCG.showError('Please finish editing trade details before creating trades.', 'Finish Editing');
			return;
		}

		const form = panel.getForm();
		const invalidField = panel.getFirstInValidField();

		// Guard-clauses
		if (!TCG.isNull(invalidField)) {
			TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
			return;
		}
		if (!form.isDirty()) {
			this.close();
			return;
		}
		if (!this.submitTradesCallback) {
			TCG.showError('Opener component must define a callback to submit trade data.', 'Callback Missing');
			return;
		}

		// Submit trades
		this.submitTradesCallback(panel);
	}
});

Clifton.trade.monitor.BaseTradeModalFormPanel = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false,
	labelWidth: 100,
	includeSecurityDragAndDrop: false,
	securityDragAndDropInvestmentTypeName: 'Options',
	defaultTradeDestinationName: 'Bloomberg EMSX DeltaShift',

	defaults: {
		anchor: '-20'
	},

	initComponent: function() {
		const items = this.items || [];
		if (this.includeSecurityDragAndDrop) {
			items.splice(0, 0, {
				xtype: 'security-drag-and-drop-container',
				prepareSecurityDefaultData: function(defaultData) {
					const formPanel = TCG.getParentFormPanel(this);
					const form = formPanel.getForm();

					// reset security combo store
					form.findField('investmentSecurity.label').resetStore();

					const securityField = form.findField('underlyingInvestmentSecurity.id');
					const underlyingSecurityId = securityField ? securityField.getValue() : void 0;
					return TCG.data.getDataPromise('investmentTypeByName.json?name=' + formPanel.securityDragAndDropInvestmentTypeName, formPanel)
						.then(investmentType => {
							defaultData.investmentType = investmentType;

							if (underlyingSecurityId) {
								return TCG.data.getDataPromise('investmentSecurity.json?id=' + underlyingSecurityId, formPanel)
									.then(underlyingSecurity => {
										defaultData.underlyingSecurity = underlyingSecurity;
										return defaultData;
									});
							}
							return defaultData;
						});
				}
			});
		}
		if (this.formPanelTopItems) {
			items.push(this.formPanelTopItems);
		}
		this.items = [
			items,
			{
				xtype: 'panel',
				layout: 'column',
				defaults: {layout: 'form', defaults: {anchor: '-20'}},
				items: [
					{
						columnWidth: .30,
						labelWidth: 80,
						items: this.formPanelSecurityColumnItems
					},
					{
						columnWidth: .33,
						items: this.formPanelExecutionColumnItems
					},
					{
						columnWidth: .22,
						labelWidth: 90,
						items: this.formPanelTradeDateColumnItems
					},
					{
						columnWidth: .15,
						labelWidth: 100,
						defaults: {anchor: '0'},
						items: this.formPanelAdditionalColumnItems
					}
				]
			},
			this.formPanelBottomItems
		];

		TCG.callSuper(this, 'initComponent', arguments);
	},

	getDefaultData: function(window) {
		const defaultData = window.defaultData || {};
		defaultData.tradeDate = new Date().format('Y-m-d 00:00:00');
		defaultData.traderUser = TCG.getCurrentUser();
		return defaultData;
	},

	afterRenderPromise: function(formPanel) {
		// If default data was passed in with tradeDetailList data, the grid needs to see that data
		formPanel.afterRenderRefreshTradeEntryGrid();
	},

	getChildTradeEntryGrid: function() {
		return TCG.getChildByName(this, 'tradeDetailListGrid');
	},

	reloadChildTradeEntryGrid: async function() {
		const grid = this.getChildTradeEntryGrid();
		if (grid) {
			await grid.reload();
		}
	},

	afterRenderRefreshTradeEntryGrid: function() {
		const formPanel = this;
		formPanel.setDefaultTradeDestination()
			.then(() => {
				const tradeDetailListGrid = formPanel.getChildTradeEntryGrid();
				if (tradeDetailListGrid) {
					const tradeDetailRows = formPanel.getFormValue('tradeDetailList');
					if (Array.isArray(tradeDetailRows) && tradeDetailRows.length > 0 && tradeDetailListGrid.getStore().getTotalCount() !== tradeDetailRows.length) {
						tradeDetailListGrid.getStore().loadData(formPanel.getForm().formValues);
					}
					if (tradeDetailListGrid.reload) {
						tradeDetailListGrid.reload();
					}
				}

				formPanel.processFieldVisibility();
			});
	},

	processFieldVisibility: function() {
		const form = this.getForm();
		if (form.formValues.tradeType && TCG.isTrue(form.formValues.tradeType.tradeExecutionTypeSupported)) {
			this.showField('tradeExecutionType.name');
			this.showField('limitPrice');
			if (TCG.isBlank(form.formValues.tradeExecutionType) && this.readOnlyMode === true) {
				this.hideField('tradeExecutionType.name');
				this.hideField('limitPrice');
			}
		}
		else {
			this.hideField('tradeExecutionType.name');
			this.hideField('limitPrice');
		}
	},

	processSecuritySelection: async function(security) {
		if (security) {
			const formPanel = this;
			const form = formPanel.getForm();

			const payingSecurityField = form.findField('payingSecurity.id');
			if (payingSecurityField) {
				const settleCcy = TCG.getValue('settlementCurrency', security) || TCG.getValue('instrument.tradingCurrency', security);
				if (settleCcy) {
					payingSecurityField.setValue(settleCcy.id);
				}
			}
			const otcField = form.findField('investmentSecurity.instrument.hierarchy.otc');
			if (otcField) {
				otcField.setValue(security.instrument.hierarchy.otc);
			}
			const priceMultiplierField = form.findField('investmentSecurity.priceMultiplier');
			if (priceMultiplierField) {
				priceMultiplierField.setValue(security.priceMultiplier);
			}
			const securityPriceField = form.findField('investmentSecurity.price');
			if (securityPriceField) {
				securityPriceField.setValue(void 0);
				// load the price for the security here instead of relying on the child rows when validating quantities and cash impact
				const openCloseTypeField = form.findField('openCloseType.id');
				const openCloseTypeId = openCloseTypeField ? openCloseTypeField.getValue() : void 0;
				const openTradeIsBuy = !TCG.isTrue(formPanel.getFormValue('openTradeIsSell') || true);
				await Promise.resolve((openCloseTypeId)
					? TCG.data.getDataPromiseUsingCaching('tradeOpenCloseType.json?id=' + openCloseTypeId, this, 'tradeOpenCloseType.' + openCloseTypeId).then(openCloseType => openCloseType.buy)
					: openTradeIsBuy)
					.then(buy => Clifton.marketdata.field.getSecurityLivePriceForBuySell(security.id, buy, {maxAgeInSeconds: 5, providerOverride: 'BPIPE'}))
					.then(result => {
						const price = result ? result.value : void 0;
						securityPriceField.setValue(price);
					});
			}

			await formPanel.reloadChildTradeEntryGrid();

			Clifton.investment.instrument.getTradeDatePromise(security.id, null, formPanel)
				.then(tradeDate => {
					form.findField('tradeDate').setValue(new Date(tradeDate).format('m/d/Y'));
					return Clifton.investment.instrument.getSettlementDatePromise(security.id, tradeDate, null, null, this);
				})
				.then(settlementDate => {
					const settlementDateField = form.findField('settlementDate');
					if (settlementDateField) {
						settlementDateField.setValue(new Date(settlementDate).format('m/d/Y'));
					}
				});

			// update default destination
			formPanel.setDefaultTradeDestination();
		}
	},

	/**
	 * Loads the TradeDestination combo's selection list and sets the default TradeDestination value.  If the
	 * default destination value is not available for the current trade, an alternate TradeDestination is chosen
	 * based on the returned destinations from the 'tradeDestinationMappingListByCommand.json' query.
	 * @returns {*}
	 */
	setDefaultTradeDestination: function() {
		const fp = this;
		const form = fp.getForm();
		const tradeDestinationField = form.findField('tradeDestination.name');
		const currentlySelectedTradeDestinationId = tradeDestinationField.value;

		return TCG.data.getDataPromise('tradeDestinationMappingListByCommandFind.json', this, {
			params: {
				securityId: form.findField('investmentSecurity.label').value,
				activeOnDate: form.findField('tradeDate').value,
				tradeTypeId: form.ownerCt.defaultData.tradeType.id
			}
		}).then(destinationMappingList => {
			let defaultOrAlternateTradeDestinationMapping = null;

			// try to match currently selected trade destination
			if (TCG.isNotBlank(destinationMappingList) && destinationMappingList.length > 0) {
				for (const tradeDestinationMapping of destinationMappingList) {
					if (tradeDestinationMapping.tradeDestination.id === currentlySelectedTradeDestinationId
						|| tradeDestinationMapping.tradeDestination.name === fp.defaultTradeDestinationName) {
						defaultOrAlternateTradeDestinationMapping = tradeDestinationMapping;
						break;
					}
				}

				// if the default trade destination could not be used, use first mapping in the list.
				if (defaultOrAlternateTradeDestinationMapping === null) {
					defaultOrAlternateTradeDestinationMapping = destinationMappingList[0];
				}

				tradeDestinationField.setValue({value: defaultOrAlternateTradeDestinationMapping.tradeDestination.id, text: defaultOrAlternateTradeDestinationMapping.tradeDestination.name});

				if (TCG.isNotBlank(defaultOrAlternateTradeDestinationMapping.executingBrokerCompany)) {
					const executingBrokerField = form.findField('executingBrokerCompany.label');
					if (TCG.isNotBlank(executingBrokerField)) {
						executingBrokerField.setValue({value: defaultOrAlternateTradeDestinationMapping.executingBrokerCompany.id, text: defaultOrAlternateTradeDestinationMapping.executingBrokerCompany.label});
					}
				}
			}
		});
	},

	formPanelTopItems: [],

	formPanelSecurityColumnItems: [
		{name: 'investmentType.id', xtype: 'hidden', submitValue: false},
		{name: 'tradeType.holdingAccountPurpose.name', xtype: 'hidden', submitValue: false},
		{name: 'payingSecurity.id', xtype: 'hidden'},
		{name: 'investmentSecurity.instrument.hierarchy.otc', xtype: 'hidden', submitValue: false},
		{name: 'investmentSecurity.priceMultiplier', xtype: 'hidden', submitValue: false},
		{
			fieldLabel: 'Trade Type', name: 'tradeType.name', hiddenName: 'tradeType.id', xtype: 'combo', url: 'tradeTypeListFind.json', loadAll: true, allowBlank: false,
			listeners: {
				select: async function(combo, record) {
					if (!record) {
						return;
					}
					const tradeType = await TCG.data.getDataPromise('tradeType.json?id=' + record.json.id, combo);
					const formPanel = TCG.getParentFormPanel(combo);
					const form = formPanel.getForm();
					form.formValues.tradeType = tradeType;
					const investmentTypeField = form.findField('investmentType.id');
					if (investmentTypeField) {
						investmentTypeField.setValue(tradeType.investmentType.id);
					}
					const holdingAccountPurposeNameField = form.findField('tradeType.holdingAccountPurpose.name');
					if (holdingAccountPurposeNameField) {
						holdingAccountPurposeNameField.setValue(tradeType.holdingAccountPurpose.name);
					}
					await formPanel.reloadChildTradeEntryGrid();
					formPanel.processFieldVisibility();
				}
			}
		},
		{
			fieldLabel: 'Buy/Sell', name: 'openCloseType.label', hiddenName: 'openCloseType.id', displayField: 'label', xtype: 'combo', url: 'tradeOpenCloseTypeListFind.json', allowBlank: false,
			listeners: {
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					combo.store.baseParams = {
						tradeTypeId: combo.getParentForm().getFormValue('tradeType.id'),
						open: true
					};
				},
				select: async function(combo, record, index) {
					combo.getParentForm().reloadChildTradeEntryGrid();
				}
			}
		},
		{
			fieldLabel: 'New Security', name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', xtype: 'combo', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?active=true', allowBlank: false,
			investmentSecurity: null,
			listeners: {
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const formPanel = TCG.getParentFormPanel(combo);
					combo.store.baseParams = {
						investmentTypeId: formPanel.getFormValue('investmentType.id'),
						underlyingSecurityId: formPanel.getFormValue('underlyingInvestmentSecurity.id')
					};
					if (formPanel.getFormValue('tradeType.name') === 'Options') {
						const expirationDate = new Date().format('m/d/Y');
						combo.store.setBaseParam('restrictionList', Ext.util.JSON.encode([{comparison: 'GREATER_THAN', value: expirationDate, field: 'lastDeliveryDate'}]));
					}
				},
				select: function(combo, record, index) {
					const formPanel = combo.getParentForm();
					this.investmentSecurity = record.json;
					formPanel.processSecuritySelection(record.json);
				}
			},
			getDetailPageClass: function(newInstance) {
				return newInstance ? 'Clifton.investment.instrument.copy.SecurityCopyWindow' : this.detailPageClass;
			},
			getDefaultData: function(form, newInstance) {
				if (!newInstance) {
					return {id: form.findField('investmentSecurity.id').getValue()};
				}
				const securityId = form.findField('investmentSecurity.id').getValue();
				return TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, this)
					.then(security => ({
						instrument: security.instrument,
						securityToCopy: security,
						optionType: 'PUT'
					}));
			}
		}
	],

	formPanelExecutionColumnItems: [
		{
			fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false,
			listeners: {
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					combo.store.baseParams = {
						tradeTypeId: TCG.getParentFormPanel(combo).getFormValue('tradeType.id'),
						activeOnDate: combo.getParentForm().getForm().findField('tradeDate').value
					};
				},
				change: function(combo, newValue, oldValue) {
					const executingBrokerField = TCG.getParentFormPanel(combo).getForm().findField('executingBrokerCompany.label');
					if (executingBrokerField) {
						executingBrokerField.clearAndReset();
					}
				}
			}
		},
		{
			fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', requiredFields: ['tradeDestination.name'], doNotClearIfRequiredSet: true,
			displayField: 'label', queryParam: 'executingBrokerCompanyName', detailPageClass: 'Clifton.business.company.CompanyWindow', url: 'tradeExecutingBrokerCompanyListFind.json', allowBlank: true,
			listeners: {
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const form = combo.getParentForm().getForm();
					combo.store.baseParams = {
						tradeDestinationId: form.findField('tradeDestination.name').getValue(),
						tradeTypeId: TCG.getParentFormPanel(combo).getFormValue('tradeType.id'),
						activeOnDate: form.findField('tradeDate').value
					};
				}
			}
		},
		{
			xtype: 'panel',
			layout: 'column',
			defaults: {layout: 'form', defaults: {anchor: '0'}},
			items: [
				{
					columnWidth: .63,
					items: [
						{
							fieldLabel: 'Execution Type', name: 'tradeExecutionType.name', displayField: 'name', hiddenName: 'tradeExecutionType.id', xtype: 'combo',
							url: 'tradeExecutionTypeListFind.json', hidden: true, width: 120,
							detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow'
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .35,
					labelWidth: 60,
					items: [
						{fieldLabel: 'Limit Price', name: 'limitPrice', xtype: 'pricefield', hidden: true, minValue: 0.01, width: 70}
					]
				}

			]
		}
	],

	formPanelTradeDateColumnItems: [
		{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, detailPageClass: 'Clifton.security.user.UserWindow'},
		{
			fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
			getParentFormPanel: function() {
				const parent = TCG.getParentFormPanel(this);
				return parent || this.ownerCt;
			},
			listeners: {
				change: function(field, newValue, oldValue) {
					const form = field.getParentFormPanel().getForm();
					const settlementDateField = form.findField('settlementDate');

					if (TCG.isBlank(newValue)) {
						settlementDateField.setValue('');
					}
					else {
						Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, newValue, null, null, field)
							.then(settlementDate => form.findField('settlementDate').setValue(settlementDate));
					}
				},
				select: function(field, date) {
					const fp = field.getParentFormPanel();
					const form = fp.getForm();

					Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, date, null, null, field)
						.then(settlementDate => form.findField('settlementDate').setValue(settlementDate));

					// since trade destination selection is dependent upon the selected trade date, we should refresh the list.
					fp.setDefaultTradeDestination();

				}
			}
		},
		{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false, requiredFields: ['investmentSecurity.label']}
	],

	formPanelAdditionalColumnItems: [],

	formPanelBottomItems: []
});
Ext.reg('base-modal-trade-formpanel', Clifton.trade.monitor.BaseTradeModalFormPanel);

Clifton.trade.monitor.BaseTradeModalTradeEntryGrid = Ext.extend(Clifton.trade.group.TradeGroupEntryGridPanel, {

	plugins: [
		{
			ptype: 'gridsummary'
		},
		{
			ptype: 'trade-restriction-aware',
			getRecordQuantityName: () => 'quantityIntended',
			isApplyNotesToRecordColumn: () => true
		}
	],


	reload: async function() {
		if (TCG.isTrue(this.reloadActive)) {
			return;
		}

		this.reloadActive = true;
		try {
			const recordUpdatePromiseList = this.getStore().data.items.map(record => {
				record.beginEdit();
				return Promise.resolve(this.applyDefaultHoldingAccount(record))
					.then(() => this.updateQuantityEditAware(record));
			});
			await Promise.all(recordUpdatePromiseList);
			this.getView().refresh();
		}
		finally {
			this.reloadActive = false;
		}
	},

	applyDefaultHoldingAccount: async function(gridRecord) {
		if (!TCG.getValue('holdingInvestmentAccount.id', gridRecord.json)) {
			const formPanel = TCG.getParentFormPanel(this);
			const form = formPanel.getForm();
			const columnModel = this.getColumnModel();

			const holdingAccountCombo = columnModel.getColumnById(columnModel.findColumnIndex('holdingInvestmentAccount.label')).getCellEditor().field;
			holdingAccountCombo.store.removeAll();
			holdingAccountCombo.lastQuery = null;
			const params = {
				mainAccountId: TCG.getValue('clientInvestmentAccount.id', gridRecord.json),
				ourAccount: false,
				workflowStatusNameEquals: 'Active'
			};
			const otcField = form.findField('investmentSecurity.instrument.hierarchy.otc');
			if (otcField) {
				const otc = otcField.getValue();
				if (otc) {
					params['contractRequired'] = otc;
				}
			}
			const purpose = formPanel.getFormValue('tradeType.holdingAccountPurpose.name');
			if (TCG.isNotNull(purpose)) {
				params['mainPurpose'] = purpose;
				params['mainPurposeActiveOnDate'] = form.findField('tradeDate').value;
			}
			const holdingAccountList = await TCG.data.getDataPromise('investmentAccountListFind.json', this, {params: params});
			if (holdingAccountList.length === 1) {
				gridRecord.set('holdingInvestmentAccount.label', holdingAccountList[0].label);
				gridRecord.set('holdingInvestmentAccount.id', holdingAccountList[0].id);
			}
		}

		return this.applyDefaultExecutingBroker(gridRecord);
	},

	applyDefaultExecutingBroker: async function(gridRecord, newHoldingAccountId) {
		const holdingAccountId = newHoldingAccountId || gridRecord.data['holdingInvestmentAccount.id'];
		const executingBrokerId = gridRecord.data['executingBrokerCompany.id'];

		// Guard-clause: Ignore records with a pre-existing broker or with no holding account
		if (executingBrokerId || !holdingAccountId) {
			this.applyTradeDestination(gridRecord);
			return;
		}

		const directedExecutingBroker = await Clifton.trade.getDirectedExecutingBrokerCompany({id: gridRecord.data['clientInvestmentAccount.id']}, {id: holdingAccountId});
		if (directedExecutingBroker) {
			gridRecord.set('executingBrokerCompany.label', directedExecutingBroker.label);
			gridRecord.set('executingBrokerCompany.id', directedExecutingBroker.id);

			// Disable editing the executing broker column for a directed brokerage account
			const columnModel = this.getColumnModel();
			const cellEditor = columnModel.getCellEditor(columnModel.findColumnIndex('executingBrokerCompany.label'), this.getStore().indexOf(gridRecord));
			if (cellEditor && cellEditor.field) {
				cellEditor.field.setReadOnly(true);
			}
		}
		this.applyTradeDestination(gridRecord);
	},

	/**
	 * Looks up the trade destinations for this account.  Sets the trade destination on the record if a single trade destination is found,
	 * and it is the "Manual" trade destination.  This trade destination will override the trade destination specified for the trade group.
	 */
	applyTradeDestination: async function(gridRecord) {
		const fp = TCG.getParentFormPanel(this);
		let executingBrokerCompanyId = gridRecord.get('executingBrokerCompany.id');
		executingBrokerCompanyId = executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : null;
		const executingBrokerCompanyForUrl = executingBrokerCompanyId !== null ? '&executingBrokerCompanyId=' + executingBrokerCompanyId : '';

		const tradeTypeId = fp.getFormValue('tradeType.id');
		const tradeDate = fp.getForm().findField('tradeDate').value;
		const activeOnDate = TCG.isNotBlank(tradeDate) ? tradeDate : new Date().format('Y-m-d 00:00:00');
		const key = tradeDate + '_applyTradeDestinationKey_' + tradeTypeId + '_' + executingBrokerCompanyId + '_' + gridRecord.json.clientInvestmentAccount.id;
		const url = 'tradeDestinationMappingListByCommandFind.json?' + 'usePagingArray=false' + '&activeOnDate=' + activeOnDate + '&tradeTypeId=' + tradeTypeId + '&clientInvestmentAccountId=' + gridRecord.json.clientInvestmentAccount.id + executingBrokerCompanyForUrl;

		const tradeDestinationMappingList = await TCG.data.getDataPromiseUsingCaching(url, this, key);

		// Automatically set the tradeDestinationMapping if there is only one entry and its TradeDestination is 'Manual'.
		if (TCG.isNotBlank(tradeDestinationMappingList) && tradeDestinationMappingList.length === 1 && tradeDestinationMappingList[0].tradeDestination.name === 'Manual') {
			gridRecord.set('tradeDestination.name', tradeDestinationMappingList[0].tradeDestination.name);
			gridRecord.set('tradeDestination.id', tradeDestinationMappingList[0].tradeDestination.id);
		}
	},


	updateQuantityEditAware: async function(gridRecord, newValue) {
		gridRecord.beginEdit();

		await this.updateQuantity(gridRecord, newValue)
			.then(() => gridRecord.endEdit());
	},

	updateQuantity: async function(gridRecord, newValue) {
		// OVERRIDE
	},

	isClientInvestmentAccountNegativeCashBalanceAllowed: async function(gridRecord) {
		let [validateCashMargin, cashMargin] = [gridRecord.get('validateCashMargin'), gridRecord.get('cashMargin')];
		if (TCG.isNull(validateCashMargin) || TCG.isNull(cashMargin)) {
			const [validateCashMarginResult, cashMarginResult] = await TCG.data.getDataPromise('investmentAccount.json', this, {
				params: {
					id: gridRecord.data['clientInvestmentAccount.id'],
					requestedPropertiesRoot: 'data',
					requestedProperties: 'id|serviceProcessingType.id'
				}
			}).then(clientAccount => Promise.all([
				Clifton.system.GetCustomColumnValuePromise(clientAccount.serviceProcessingType.id, 'BusinessServiceProcessingType', 'Business Service Processing Type Custom Fields', 'Validate Client Cash Margin', true),
				Clifton.system.GetCustomColumnValuePromise(clientAccount.id, 'InvestmentAccount', 'Portfolio Setup', 'Cash Margin', true)
			]));
			gridRecord.set('validateCashMargin', validateCashMarginResult);
			gridRecord.set('cashMargin', cashMarginResult);
			validateCashMargin = validateCashMarginResult;
			cashMargin = cashMarginResult;
		}
		if (TCG.isTrue(validateCashMargin)) {
			// if the cash margin is to be validated, return whether the client allows negative cash balances as a cash margin account
			return TCG.isTrue(cashMargin);
		}
		// if the cash margin is not to be validated, return true allowing negative cash balances
		return true;
	},
	addTradeUnderlyingSecurityToToolBar: function(toolbar) {
		toolbar.add('->', {
			fieldLabel: '', boxLabel: 'Trade Underlying Security', name: 'tradeUnderlyingEnabled', xtype: 'toolbar-checkbox',
			qtip: 'If checked, the ability to trade the underlying security for the Option securities included in the roll will be presented.',
			listeners: {
				check: function(field) {
					const gridPanel = TCG.getParentByClass(field, Ext.Panel);
					const columnModel = gridPanel.getColumnModel();
					['tradeUnderlying', 'underlyingBuy', 'underlyingQuantity'].forEach(columnDataIndex => {
						const columnId = columnModel.findColumnIndex(columnDataIndex);
						columnModel.setHidden(columnId, !field.getValue());
					});
					gridPanel.reload();
				}
			}
		});
	},
	getSecurityLivePrice: async function(securityId, buy) {
		return await Clifton.marketdata.field.getSecurityLivePriceForBuySell(securityId, buy, {maxAgeInSeconds: 5, providerOverride: 'BPIPE'})
			.then(result => result ? result.value : void 0);
	}
});
Ext.reg('base-modal-trade-group-entry-grid', Clifton.trade.monitor.BaseTradeModalTradeEntryGrid);
