TCG.use('Clifton.trade.monitor.BaseTradeModalWindow');

Clifton.trade.monitor.PositionOpenTradeWindow = Ext.extend(Clifton.trade.monitor.BaseTradeModalWindow, {
	title: 'Open Position',
	saveTimeout: 180000,

	items: [
		{
			xtype: 'base-modal-trade-formpanel',
			instructions: 'This window asks for additional details for opening new positions across client accounts for a single security.',
			includeSecurityDragAndDrop: true,

			afterRenderPromise: function(formPanel) {
				// If default data was passed in with tradeDetailList data, the grid needs to see that data
				formPanel.afterRenderRefreshTradeEntryGrid();

				const tradeDetailListGrid = formPanel.getChildTradeEntryGrid();
				if (!TCG.isTrue(formPanel.getForm().findField('applySecurityTargetUtilization').getValue())) {
					const columnModel = tradeDetailListGrid.getColumnModel();
					columnModel.setHidden(columnModel.findColumnIndex('openPercent'), true);
					columnModel.setHidden(columnModel.findColumnIndex('useFrozen'), true);
				}

				const securityId = formPanel.getForm().findField('investmentSecurity.id').getValue();
				if (securityId) {
					TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, formPanel)
						.then(security => formPanel.processSecuritySelection.call(formPanel, security));
				}
			},

			formPanelTopItems: [
				{name: 'applySecurityTargetUtilization', xtype: 'checkbox', hidden: true, submitValue: false},
				{name: 'underlyingInvestmentSecurity.id', xtype: 'hidden', submitValue: false}
			],

			formPanelAdditionalColumnItems: [
				{
					fieldLabel: 'Security Price', name: 'investmentSecurity.price', xtype: 'floatfield',
					listeners: {
						change: function(field, newValue, oldValue) {
							TCG.getParentFormPanel(field).reloadChildTradeEntryGrid();
						}
					}
				}
			],

			formPanelBottomItems: [
				{
					xtype: 'base-modal-trade-group-entry-grid',

					listeners: {
						afterrender: function() {
							this.createCopyDrillDownMenuForColumn('openPercent');
						}
					},

					createCopyDrillDownMenuForColumn: function(columnDataIndex) {
						const grid = this;
						const columnModel = grid.getColumnModel();
						const column = columnModel.getColumnAt(columnModel.findColumnIndex(columnDataIndex));

						// set context menu on column once rather than on every right-click
						if (!column.drillDown) {
							column.drillDown = new Ext.menu.Menu({
								items: [
									{
										text: 'Copy to all rows',
										iconCls: 'copy',
										handler: () => {
											// get current row selection for copying
											const currentRecord = grid.getStore().getAt(grid.contextRowIndex);
											const newValue = currentRecord.get(columnDataIndex);
											grid.getStore().each(record => {
												if (record !== currentRecord && record.get(columnDataIndex) !== newValue) {
													record.set(columnDataIndex, newValue);
												}
											});
										}
									}
								]
							});
						}

						// add menu to column
						column.on('contextmenu', function(column, grid, row, event) {
							event.preventDefault();
							// set grid row placeholder for the context menu to use
							grid.contextRowIndex = row;
							column.drillDown.showAt(event.getXY());
						}, column);

						// add menu to column editor
						column.editor.on('afterrender', function(editor) {
							const column = this;
							const el = column.editor.getEl();
							el.dom.setAttribute('ext:qtip', 'Right click to display the context menu with additional features.');
							el.on('contextmenu', function(event, target) {
								event.preventDefault();
								// set grid row placeholder for the context menu to use
								grid.contextRowIndex = column.editor.gridEditor.row;
								column.drillDown.showAt(event.getXY());
							});
						}, column);
					},

					addToolbarButtons: function(toolbar) {
						toolbar.add({
							text: 'Apply Changes',
							tooltip: 'Apply available changes to the grid details based on selected form values. Changes should be automatically applied when changing applicable values.',
							iconCls: 'preview',
							handler: () => this.reload()
						});
					},

					updateQuantity: async function(gridRecord, newQuantity, newOpenPercent) {
						const formPanel = TCG.getParentFormPanel(this);
						const form = formPanel.getForm();

						const securityMultiplier = form.findField('investmentSecurity.priceMultiplier').getValue();
						if (!securityMultiplier) {
							return;
						}

						const originalQuantityIntended = this.getColumnModel().getCellEditor(this.getColumnModel().findColumnIndex('quantityIntended')).field.startValue || 0;
						const originalInsufficientQuantity = gridRecord.get('insufficientQuantity');
						const originalInsufficientCash = gridRecord.get('insufficientCash');
						try {
							let quantity = newQuantity || gridRecord.get('quantityIntended');
							const target = gridRecord.data['utilizationUnderlyingTarget'];
							if (TCG.isTrue(form.findField('applySecurityTargetUtilization').getValue())) {
								const userModifiedQuantity = TCG.isBlank(newQuantity);
								const openPercent = newOpenPercent || gridRecord.data['openPercent'];
								quantity = this.validateQuantityAgainstAvailableQuantity(form, gridRecord, quantity, target, openPercent, userModifiedQuantity);
							}
							const validatedQuantity = await this.validateQuantityAgainstCash(quantity, gridRecord);
							gridRecord.set('quantityIntended', validatedQuantity);
							let calculatedPercentFromQuantity = Math.abs(validatedQuantity / target * securityMultiplier * 100);
							// Round up at three decimal digits
							calculatedPercentFromQuantity = Math.ceil(calculatedPercentFromQuantity * 1000) / 1000;
							gridRecord.set('openPercent', calculatedPercentFromQuantity);
						}
						catch (e) {
							console.warn(e);
							// roll back values
							gridRecord.set('quantityIntended', originalQuantityIntended);
							gridRecord.set('insufficientQuantity', originalInsufficientQuantity);
							gridRecord.set('insufficientCash', originalInsufficientCash);
						}
					},

					validateQuantityAgainstAvailableQuantity: function(form, gridRecord, quantity, target, openPercent, userModifiedQuantity) {
						let returnQuantity = quantity;
						const useFrozen = gridRecord.data['useFrozen'];
						const usage = (useFrozen) ? gridRecord.data['utilizationTotalUsage'] - gridRecord.data['utilizationFrozen'] : gridRecord.data['utilizationTotalUsage'];

						if (TCG.isNotBlank(openPercent) && TCG.isNotBlank(target) && TCG.isNotBlank(usage)) {
							const securityMultiplier = form.findField('investmentSecurity.priceMultiplier').getValue();
							let availableUnderlyingQuantity = target - usage;
							if (availableUnderlyingQuantity !== 0 && (target > 0 !== availableUnderlyingQuantity > 0)) {
								// if the available quantity is of different sign of the target, we are over utilized; adjust quantity to 0
								availableUnderlyingQuantity = 0;
							}
							availableUnderlyingQuantity = Math.abs(availableUnderlyingQuantity);
							if (TCG.isTrue(userModifiedQuantity)) {
								// if the client's target is small and it results in a contract < 1, we will default the sellToOpen to 1 adjusted to the underlying target
								let sellToOpenPercentUnderlyingQuantity = Math.max(1 * securityMultiplier, Math.abs(target * openPercent / 100));
								if (sellToOpenPercentUnderlyingQuantity > availableUnderlyingQuantity) {
									sellToOpenPercentUnderlyingQuantity = availableUnderlyingQuantity;
									gridRecord.set('insufficientQuantity', true);
								}
								else {
									gridRecord.set('insufficientQuantity', false);
								}
								const allowedQuantity = Math.floor(sellToOpenPercentUnderlyingQuantity / securityMultiplier);
								if (returnQuantity !== allowedQuantity) {
									// adjust quantity to maximum allowed for defined trade percent
									returnQuantity = allowedQuantity;
								}
							}
							else {
								gridRecord.set('insufficientQuantity', (quantity > Math.floor(availableUnderlyingQuantity / securityMultiplier)));
								// adjust quantity to the maximum available in case the user entered a large quantity
								returnQuantity = Math.min(quantity, Math.floor(availableUnderlyingQuantity / securityMultiplier));
							}
						}
						return returnQuantity;
					},

					validateQuantityAgainstCash: async function(quantity, gridRecord) {
						let returnQuantity = quantity;
						const buy = await this.isOpenBuy();
						const form = TCG.getParentFormPanel(this).getForm();

						const price = await this.getSecurityPrice(form, buy);
						const cashBalance = gridRecord.get('cashBalance');
						const securityMultiplier = form.findField('investmentSecurity.priceMultiplier').getValue();
						const cashImpact = quantity * securityMultiplier * price * (buy ? -1 : 1);
						const newCashBalance = cashBalance + cashImpact;
						if (cashImpact < 0 && newCashBalance < 0) {
							if (!gridRecord.data['ignoreCash']) {
								const cashCanGoNegative = await this.isClientInvestmentAccountNegativeCashBalanceAllowed(gridRecord);
								if (TCG.isFalse(cashCanGoNegative)) {
									// adjust the quantity to 0 if insufficient cash
									returnQuantity = 0;
								}
							}
							gridRecord.set('insufficientCash', true);
						}
						else {
							gridRecord.set('insufficientCash', false);
						}
						gridRecord.set('netTradeCashImpact', returnQuantity * securityMultiplier * price * (buy ? -1 : 1));
						return returnQuantity;
					},

					isOpenBuy: async function() {
						const form = TCG.getParentFormPanel(this).getForm();
						const openCloseTypeId = form.findField('openCloseType.id').getValue();
						return await ((openCloseTypeId)
							? TCG.data.getDataPromiseUsingCaching('tradeOpenCloseType.json?id=' + openCloseTypeId, this, 'tradeOpenCloseType.' + openCloseTypeId).then(openCloseType => openCloseType.buy)
							: false);
					},

					getSecurityPrice: async function(form, buy) {
						// get price for the trade; ASK for buying, BID for selling to get more accurate cost
						const priceField = form.findField('investmentSecurity.price');
						let price = priceField.getNumericValue();
						if (!price) {
							const securityId = form.findField('investmentSecurity.id').getValue();
							price = await this.getSecurityLivePrice(securityId, buy);
							if (TCG.isBlank(price)) {
								TCG.showError('Unable to obtain price for opening security. Please manually enter a price.', 'Price Failure');
								throw new Error('Unable to obtain price for opening security.');
							}
							priceField.setValue(price);
						}
						return price;
					},

					// Fields not applicable to TradeEntryDetail
					doNotSubmitFields: ['openPercent', 'utilizationUnderlyingTarget', 'utilizationTotalUsage', 'utilizationCovered', 'utilizationPending', 'utilizationFrozen', 'utilizationAvailable', 'useFrozen', 'cashBalance', 'insufficientQuantity', 'insufficientCash', 'netTradeCashImpact', 'ignoreCash', 'validateCashMargin', 'cashMargin', 'ignoreOpenPercent', 'clientAccountNotes'],

					columnOverrides: [
						{
							dataIndex: 'clientInvestmentAccount.label', width: 200,
							editor: {
								xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', displayField: 'label', allowBlank: false, readOnly: true, addGridCopyContextMenu: false
							},
							renderer: function(value, metaData, record) {
								const cashBalance = TCG.getValue('cashBalance', record.json) || 0;
								const tradeCashImpact = record.get('netTradeCashImpact') || 0;
								const cashBalanceQtip = TCG.trimWhitespace(`
									<br/><br/>
									<table style="background-color:#eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td nowrap="true" style="width: 100px;">Cash Balance:</td>
											<td style="text-align:right;">${TCG.renderAmount(cashBalance, true, '$0,000')}</td>
										</tr>
										<tr>
											<td nowrap="true" style="width: 100px;">Trade Cash Impact:</td>
											<td style="text-align:right;">${TCG.renderAmount(tradeCashImpact, true, '$0,000')}</td>
										</tr>
										<tr><td colspan="5"><hr/></td></tr>
										<tr>
											<td nowrap="true" style="width: 100px;">Net Total:</td>
											<td style="text-align:right;">${TCG.renderAmount((cashBalance + tradeCashImpact), true, '$0,000')}</td>
										</tr>
									</table>
								`);
								this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(value, metaData, record, false, cashBalanceQtip);
								// no security column in this grid, so highlight client account column if there is a security restriction
								this.scope.gridPanel.applyTradeRestrictionGridSecurityColumnFreeze(metaData, record, record.underlyingSecurity);
								return value;
							},
							eventListeners: {
								contextmenu: function(column, grid, rowIndex, event) {
									const restrictionItems = grid.getRestrictionActionMenuItems(rowIndex, column);
									if (restrictionItems.length > 0) {
										const menu = new Ext.menu.Menu({
											items: [
												...restrictionItems.length > 0 ? ['-', ...restrictionItems] : []
											]
										});

										event.preventDefault();
										menu.showAt(event.getXY());
									}
								}
							}
						},
						{
							dataIndex: 'holdingInvestmentAccount.label', width: 200, renderer: TCG.renderValueWithTooltip,
							editor: {
								xtype: 'combo', url: 'investmentAccountListFind.json', displayField: 'label', allowBlank: false, addGridCopyContextMenu: false,
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const grid = combo.gridEditor.containerGrid;
									const formPanel = TCG.getParentFormPanel(grid);
									const form = formPanel.getForm();
									combo.store.baseParams = {
										ourAccount: false,
										workflowStatusNameEquals: 'Active',
										mainAccountId: TCG.getValue('clientInvestmentAccount.id', combo.gridEditor.record.json),
										mainPurpose: formPanel.getFormValue('tradeType.holdingAccountPurpose.name'),
										mainPurposeActiveOnDate: form.findField('tradeDate').value,
										contractRequired: formPanel.getFormValue('investmentSecurity.instrument.hierarchy.otc')
									};
								},
								listeners: {
									change: function(combo, newValue, oldValue) {
										const gridPanel = combo.gridEditor.containerGrid;
										const record = combo.gridEditor.record;
										record.beginEdit();
										record.set('executingBrokerCompany.label', '');
										record.set('executingBrokerCompany.id', '');
										gridPanel.applyDefaultExecutingBroker(record, newValue)
											.then(() => record.endEdit());
									}
								}
							}
						},
						{
							dataIndex: 'quantityIntended', width: 75,
							editor: {
								xtype: 'spinnerfield', allowBlank: false, minValue: 0, readOnly: true,
								listeners: {
									change: function(field, newValue, oldValue) {
										const gridPanel = field.gridEditor.containerGrid;
										const record = field.gridEditor.record;
										gridPanel.updateQuantityEditAware(record, newValue);
									},
									specialkey: function(field, event) {
										if (event.getKey() === event.ENTER && this.spinner.mimicing) {
											this.spinner.triggerBlur();
										}
									}
								}
							},
							renderer: function(value, metaData, record) {
								// Attach style for restrictions involving the account
								if (record && (record.data['insufficientCash'] || record.data['insufficientQuantity'])) {
									let qtip = 'Insufficient ' + (record.get('insufficientQuantity') ? 'quantity' : 'cash') + ' available to trade more than ' + value + '.';
									if (record.get('insufficientCash')) {
										if (record.get('validateCashMargin')) {
											qtip += TCG.isTrue(record.get('cashMargin')) ? ' Client Investment Account is a Cash Margin account and the quantity was not adjusted.'
												: ' Client Investment Account is not a Cash Margin account and the quantity was adjusted.';
										}
										else {
											qtip += ' Client Investment Account\'s service processing type does not validate cash margin accounts and teh quantity was not adjusted.';
										}
									}
									metaData.attr = TCG.renderQtip(qtip);
									metaData.css += 'x-grid3-row-highlight-light-red';
								}
								return TCG.numberFormat(value, '0,000', true);
							}
						},
						{dataIndex: 'expectedUnitPrice', hidden: true},
						{dataIndex: 'commissionPerUnit', hidden: true},
						{dataIndex: 'feeAmount', hidden: true},
						{
							header: 'Trade %', dataIndex: 'openPercent', type: 'float', width: 75, renderer: TCG.renderPercent, tooltip: 'The client configured percentage of defined target to use when opening a new position',
							editor: {
								xtype: 'numberfield', allowBlank: false,
								listeners: {
									change: async (field, newValue, oldValue) => {
										const record = field.gridEditor.record;
										record.beginEdit();

										await field.gridEditor.containerGrid.updateQuantity(record, void (0), newValue)
											.then(() => record.endEdit());
									}
								}
							}
						},
						{
							header: 'Executing Broker', width: 200, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', useNull: false, renderer: TCG.renderValueWithTooltip,
							tooltip: 'Trade Executing Broker used for this client\'s trade. The default value is looked up by Trade Destination, active on Trade Date, and Holding Account Issuing Company.If the Holding Account defined is configured to be a Directed Broker, this value is populated to override the Executing Broker above.',
							editor: {
								xtype: 'combo', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json', tooltipField: 'label', detailPageClass: 'Clifton.business.company.CompanyWindow',
								beforequery: function(queryEvent) {
									// Reset Combo so re-queries each time (since each row has different results)
									this.resetStore();
									const editor = queryEvent.combo.gridEditor;
									const grid = editor.containerGrid;
									const form = TCG.getParentFormPanel(grid).getForm();
									queryEvent.combo.store.baseParams = {
										clientInvestmentAccountId: TCG.getValue('clientInvestmentAccount.id', editor.record.json),
										tradeDestinationId: form.findField('tradeDestination.id').getValue(),
										tradeTypeId: form.findField('tradeType.id').getValue(),
										activeOnDate: form.findField('tradeDate').value
									};
								}
							}
						},
						{
							header: 'Trade Destination', dataIndex: 'tradeDestination.name', width: 200, idDataIndex: 'tradeDestination.id', allowBlank: true,
							editor: {
								xtype: 'combo', url: 'tradeDestinationListFind.json',
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const grid = combo.gridEditor.containerGrid;
									const fp = TCG.getParentFormPanel(grid);

									// Clear Query Store so Re-Queries each time
									this.store.removeAll();
									this.lastQuery = null;

									const record = queryEvent.combo.gridEditor.record;
									const executingBrokerCompanyId = record.get('executingBrokerCompany.id');
									const tradeDate = fp.getForm().findField('tradeDate').value;
									const activeOnDate = TCG.isNotBlank(tradeDate) ? tradeDate : new Date().format('Y-m-d 00:00:00');
									queryEvent.combo.store.baseParams = {
										tradeTypeId: fp.getFormValue('tradeType.id'),
										executingBrokerCompanyId: executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : null,
										activeOnDate: activeOnDate
									};
								}
							}
						},
						{header: 'Underlying Target', dataIndex: 'utilizationUnderlyingTarget', type: 'float', hidden: true},
						{header: 'Usage Total', dataIndex: 'utilizationTotalUsage', type: 'float', hidden: true},
						{header: 'Usage Covered', dataIndex: 'utilizationCovered', type: 'float', hidden: true},
						{header: 'Usage Pending', dataIndex: 'utilizationPending', type: 'float', hidden: true},
						{header: 'Usage Frozen', dataIndex: 'utilizationFrozen', type: 'float', hidden: true},
						{
							header: 'Usage Available', dataIndex: 'utilizationAvailable', type: 'float', hidden: true,
							renderer: function(value, metaData, record) {
								return TCG.numberFormat(record.data['utilizationUnderlyingTarget'] - record.data['utilizationTotalUsage'], '0,000', true);
							}
						},
						{
							header: 'Use Frozen', dataIndex: 'useFrozen', xtype: 'checkcolumn', width: 70,
							onClick: (grid, record) => grid.updateQuantityEditAware(record)
						},
						{header: 'Cash', dataIndex: 'cashBalance', type: 'float', hidden: true},
						{
							header: 'Ignore Cash', dataIndex: 'ignoreCash', xtype: 'checkcolumn', width: 70,
							tooltip: 'If checked, the validation for cash will not automatically adjust the trade quantity allowing trades to be created',
							onClick: (grid, record) => grid.updateQuantityEditAware(record)
						},
						{
							header: 'Ignore Trade %', dataIndex: 'ignoreOpenPercent', xtype: 'checkcolumn', width: 85,
							tooltip: 'If checked, the validation for trade percent will not automatically adjust the trade quantity allowing trades to be created',
							onClick: (grid, record, rowIndex) => {
								const columnModel = grid.getColumnModel();
								const quantityIntendedField = columnModel.getCellEditor(columnModel.findColumnIndex('quantityIntended'), rowIndex).field;
								quantityIntendedField.setReadOnly(!record.data['ignoreOpenPercent']);
							}
						},
						{header: 'Account Notes', width: 200, dataIndex: 'clientAccountNotes', renderer: TCG.renderTextNowrapWithTooltip}
					]
				}
			]
		}
	],

	submitTradesCallback: async function(formPanel) {
		const restrictionCheckResult = this.checkTradeToOpenTradeRestrictions(formPanel);
		const tradePositions = await formPanel.ownerCt.openerCt.getTradeRestrictionActionForPositions(restrictionCheckResult);
		if (!tradePositions) {
			return;
		}
		const grid = formPanel.getChildTradeEntryGrid();
		const records = grid.getStore().data.items;
		const totalQuantity = tradePositions
			.map(record => record.get('quantityIntended'))
			.reduce((totalQuantity, recordQuantity) => totalQuantity + (recordQuantity || 0), 0);
		grid.getStore().data.items = tradePositions;
		grid.markModified();
		grid.getStore().data.items = records;
		const tradeSource = await TCG.data.getDataPromiseUsingCaching('tradeSourceByName.json?name=DeltaShift Blotter', this, 'tradeSource.DeltaShift Blotter');
		formPanel.getForm().submit({
			...TCG.form.submitDefaults,
			timeout: formPanel.getWindow().saveTimeout,
			url: encodeURI('tradeEntrySave.json'),
			params: {
				quantityIntended: totalQuantity,
				'tradeSource.id': tradeSource ? tradeSource.id : void 0
			},
			waitMsg: 'Saving...',
			submitEmptyText: formPanel.submitEmptyText,
			success: (form, action) => {
				const data = formPanel.getDataAfterSave(action.result.data);
				if (data !== false) {
					formPanel.getWindow().closeWindow();
				}
			}
		});
	},

	/**
	 * Checks the accounts proposed for this trade against trade restrictions to determine if the trade should be blocked.
	 *
	 * @returns {*} object containing an {@link Array} of restricted trades and an {@link Array} of valid trades.
	 */
	checkTradeToOpenTradeRestrictions: function(fp) {
		const form = fp.getForm();
		const tradeOpenCloseTypeLabel = form.findField('openCloseType.label').lastSelectionText;
		const investmentSecurity = form.findField('investmentSecurity.id').investmentSecurity;
		const underlyingSecurityId = fp.getFormValue('underlyingInvestmentSecurity.id');
		const isSell = tradeOpenCloseTypeLabel.startsWith('SELL');
		const tradeGrid = fp.getChildTradeEntryGrid();
		const tradeDetailList = tradeGrid.store.data.items;
		const [validPositions, restrictedPositions] = tradeDetailList.partition(tradeDetail => fp.getChildTradeEntryGrid().checkTradeRestrictions(tradeDetail.json.clientInvestmentAccount.id, investmentSecurity, underlyingSecurityId, tradeDetail.json.underlyingPrice, isSell, true, null));
		return {validPositions, restrictedPositions};
	}
});
