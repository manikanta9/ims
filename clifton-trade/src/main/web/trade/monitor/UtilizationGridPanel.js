TCG.useAsynchronous('Clifton.investment.account.securitytarget.AccountSecurityTargetWindow');

Clifton.trade.monitor.UtilizationGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeSecurityTargetUtilizationListFind',
	instructions: 'A list of client account portfolio utilization',
	standardColumns: [], // avoid create and update columns
	rowSelectionModel: 'checkbox',
	requestIdDataIndex: true,
	includeMarketData: true,
	includeTradingActions: false,
	includeOperationsActions: false,

	gridFiltersConfig: {
		local: false // force remote load for initial request; needed since this panel does not perform initial load
	},

	plugins: [
		{
			ptype: 'trade-restriction-aware',
			getRecordClientInvestmentAccount: record => record.json.clientInvestmentAccount || record.json.securityTarget.clientInvestmentAccount,
			getRecordInvestmentSecurity: record => record.json.investmentSecurity || record.json.securityTarget.targetUnderlyingSecurity
		}
	],

	isEditEnabled: () => false,
	isPagingEnabled: () => false,

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Client Acct Group', name: 'clientAccountGroupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Client Acct', name: 'clientAccountName', xtype: 'toolbar-combo', url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'securityTarget.clientInvestmentAccount.label'},
			{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'toolbar-datefield'}
		];
	},

	applyTopToolbarFilterLoadParams: function(params) {
		const returnParams = params || {};
		const t = this.getTopToolbar();
		let filter = TCG.getChildByName(t, 'clientAccountGroupName');
		if (filter && TCG.isNotBlank(filter.getValue())) {
			returnParams.investmentAccountGroupId = filter.getValue();
		}
		filter = TCG.getChildByName(t, 'balanceDate');
		if (filter && TCG.isNotBlank(filter.getValue())) {
			returnParams.balanceDate = filter.getValue().format('m/d/Y');
		}

		// View flags
		returnParams.includeMarketData = this.includeMarketData;

		return returnParams;
	},

	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			TCG.getChildByName(this.getTopToolbar(), 'balanceDate').setValue(new Date());
			if (this.defaultViewName) {
				this.setDefaultView(this.defaultViewName);
			}
			return false; // avoid loading without client account or client account group filtering
		}

		const defaultParams = {
			'holdingAccountPurpose.name': 'Trading: Options',
			usePriorPriceDate: true,
			requestedMaxDepth: 6
		};
		return this.applyTopToolbarFilterLoadParams(defaultParams);
	},

	gridConfig: {
		listeners: {
			'celldblclick': function(grid, rowIndex, cellIndex, evt) {
				const column = grid.getColumnModel().getColumnAt(cellIndex);
				const row = grid.getStore().getAt(rowIndex);
				if (column && column.header.includes('Client') && grid.ownerGridPanel.clientInvestmentAccountEditorWindow) {
					const clientId = TCG.getValue('securityTarget.clientInvestmentAccount.id', row.json);
					grid.ownerGridPanel.launchEditorWindow(grid.ownerGridPanel.clientInvestmentAccountEditorWindow, clientId);
				}
				else {
					const securityTargetId = TCG.getValue('securityTarget.id', row.json);
					const balanceDate = grid.ownerGridPanel.getBalanceDate(grid.ownerGridPanel, rowIndex);
					if (TCG.isNotBlank(securityTargetId)) {
						TCG.createComponent('Clifton.trade.monitor.TradeCoveredCallUtilizationDetailWindow', {
							id: TCG.getComponentId('Clifton.trade.monitor.TradeCoveredCallUtilizationDetailWindow', securityTargetId),
							params: {id: securityTargetId},
							defaultData: {
								securityTargetId: securityTargetId,
								balanceDate: balanceDate
							},
							openerCt: grid
						});
					}
				}
			}
		}
	},

	listeners: {
		afterrender: function() {
			this.grid.store.on('load', () => this.loadHandler());
		}
	},

	loadHandler: function() {
		const columnModel = this.getColumnModel();
		const calculatedColumns = columnModel.getColumnsBy(column => column.calculate && typeof column.calculate === 'function');
		this.grid.store.each(record => {
			calculatedColumns.forEach(column => column.calculate(record));
		});
	},

	configureViews: function(menu, gridPanel) {
		menu.add('-',
			{
				name: 'includeMarketData',
				text: 'Include Market Data',
				tooltip: 'Include market data value retrieval when calculating the usage details. Values include one day and YTD price change percents and earnings date',
				xtype: 'menucheckitem',
				checked: true,
				listeners: {
					checkchange: (item, checked) => {
						gridPanel.includeMarketData = checked;
						gridPanel.reload();
					}
				}
			});
	},

	addFirstToolbarButtons: function(toolbar, gridPanel) {
		toolbar.add({
			name: 'actionsMenu',
			text: 'Actions',
			iconCls: 'run',
			menu: new Ext.menu.Menu({
				items: gridPanel.getActionMenuItems(gridPanel)
			})
		}, '-');
	},

	getActionMenuItems: function() {
		const actions = [];
		if (TCG.isTrue(this.includeTradingActions)) {
			actions.push({
					text: 'Trade to Open',
					tooltip: 'Trade to open a new position. Buy for long position, sell for short position.',
					iconCls: 'run',
					handler: () => this.tradeToOpen()
				},
				'-',
				{
					text: 'Sell Underlying',
					tooltip: 'Trade to sell the underlying Security of the selected position(s)',
					iconCls: 'run',
					handler: () => this.tradeSellUnderlying()
				},
				{
					text: 'Buy Underlying',
					tooltip: 'Trade to buy the underlying Security of the selected position(s)',
					iconCls: 'run',
					handler: () => this.tradeBuyUnderlying()
				}
			);
		}
		if (TCG.isTrue(this.includeOperationsActions)) {
			if (TCG.isTrue(this.includeTradingActions)) {
				actions.push('-');
			}

			const transferHandler = async contribution => {
				const transferTypeName = contribution ? 'Security Target Contribution' : 'Security Target Distribution';
				const transferAccountPrefix = contribution ? 'to' : 'from';
				const records = this.grid.getSelectionModel().getSelections();
				if (records.length === 0) {
					TCG.showInfo('Please select a client position to create a transfer for.', 'No Position(s) Selected');
					return;
				}
				if (records.length !== 1) {
					TCG.showInfo('Please select only one client position to create a transfer for.', 'Multiple Positions Selected');
					return;
				}
				const record = TCG.grid.getUpdatedRecordAsJson(records[0]);
				const clientAccount = record.securityTarget.clientInvestmentAccount;
				const today = new Date();
				const transferType = await TCG.data.getDataPromiseUsingCaching('accountingPositionTransferTypeByName.json?name=' + transferTypeName, this, 'accounting.positiontransfer.type.' + transferTypeName);

				const defaultData = {
					type: transferType,
					settlementDate: today.format('Y-m-d 00:00:00'),
					transactionDate: today.format('Y-m-d 00:00:00'),
					sourceSystemTable: transferType.sourceSystemTable,
					sourceFkFieldId: record.id
				};
				defaultData[transferAccountPrefix + 'ClientInvestmentAccount'] = clientAccount;
				defaultData[transferAccountPrefix + 'HoldingInvestmentAccount'] = record.holdingInvestmentAccount;
				TCG.createComponent(contribution ? 'Clifton.accounting.positiontransfer.TransferToWindow' : 'Clifton.accounting.positiontransfer.TransferFromWindow', {defaultData: defaultData});
			};
			actions.push(
				{
					text: 'Security Target Contribution', iconCls: 'arrow-left-green', tooltip: 'Create a contribution to our account by defining the positions being transfer into the system',
					handler: () => transferHandler(true)
				},
				{
					text: 'Security Target Distribution', iconCls: 'arrow-right-red', tooltip: 'Create a distribution from our account by selecting lot positions',
					handler: () => transferHandler(false)
				}
			);
		}
		return actions;
	},

	getDailyCashActivityAction: function(grid, rowIndex) {
		return {
			text: 'Cash Activity', iconCls: 'book-red', tooltip: 'Display cash activity for this account',
			handler: () => {
				const componentName = 'Clifton.accounting.gl.CashActivityWindow';
				const row = grid.getStore().getAt(rowIndex);
				TCG.createComponent(componentName, {
					defaultData: {
						clientInvestmentAccount: TCG.getValue('securityTarget.clientInvestmentAccount', row.json),
						holdingInvestmentAccount: TCG.getValue('holdingInvestmentAccount', row.json)
					},
					openerCt: this
				});
			}
		};
	},

	launchEditorWindow: function(componentName, id) {
		if (TCG.isNotBlank(componentName) && TCG.isNotBlank(id)) {
			TCG.createComponent(componentName, {
				id: TCG.getComponentId(componentName, id),
				params: {id: id},
				openerCt: this
			});
		}
	},

	getBalanceDate: function(gridPanel, rowIndex) {
		return TCG.getChildByName(gridPanel.getTopToolbar(), 'balanceDate').getValue().format('m/d/Y');
	},

	additionalPropertiesToRequest: 'underlyingSecurityTargetQuantityAdjusted|underlyingSecurityTargetNotionalAdjusted|securityTarget.targetUnderlyingSecurity.label|holdingInvestmentAccount.label',

	viewNames: ['Quantity', 'Notional'],
	defaultViewName: 'Quantity',
	columns: [
		{header: 'Security Target ID', width: 10, dataIndex: 'securityTarget.id', type: 'int', hidden: true},
		{
			header: 'Client Account', width: 40, dataIndex: 'securityTarget.clientInvestmentAccount.label', idDataIndex: 'securityTarget.clientInvestmentAccount.id', hidden: true, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'},
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const tradingActionItems = grid.ownerGridPanel.getActionMenuItems();
					const restrictionItems = grid.ownerGridPanel.getRestrictionActionMenuItems(rowIndex, column);
					const filterItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, column);

					const menu = new Ext.menu.Menu({
						items: [
							...tradingActionItems,
							...restrictionItems.length > 0 ? ['-', ...restrictionItems] : [],
							'-', grid.ownerGridPanel.getDailyCashActivityAction(grid, rowIndex),
							...filterItems.length > 0 ? ['-', ...filterItems] : []
						]
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				}
			},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(value, metaData, record, true);
				return value;
			}
		},
		{
			header: 'Client Number', width: 20, dataIndex: 'securityTarget.clientInvestmentAccount.number', idDataIndex: 'securityTarget.clientInvestmentAccount.id', hidden: true, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'},
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const tradingActionItems = grid.ownerGridPanel.getActionMenuItems();
					const restrictionItems = grid.ownerGridPanel.getRestrictionActionMenuItems(rowIndex, column);
					const filterItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, column);

					const menu = new Ext.menu.Menu({
						items: [
							...tradingActionItems,
							...restrictionItems.length > 0 ? ['-', ...restrictionItems] : [],
							'-', grid.ownerGridPanel.getDailyCashActivityAction(grid, rowIndex),
							...filterItems.length > 0 ? ['-', ...filterItems] : []
						]
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				}
			},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(record.get('securityTarget.clientInvestmentAccount.label'), metaData, record, true);
				return value;
			}
		},
		{
			header: 'Client Name', width: 25, dataIndex: 'securityTarget.clientInvestmentAccount.name', idDataIndex: 'securityTarget.clientInvestmentAccount.id', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'},
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const tradingActionItems = grid.ownerGridPanel.getActionMenuItems();
					const restrictionItems = grid.ownerGridPanel.getRestrictionActionMenuItems(rowIndex, column);
					const filterItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, column);

					const menu = new Ext.menu.Menu({
						items: [
							...tradingActionItems,
							...restrictionItems.length > 0 ? ['-', ...restrictionItems] : [],
							'-', grid.ownerGridPanel.getDailyCashActivityAction(grid, rowIndex),
							...filterItems.length > 0 ? ['-', ...filterItems] : []
						]
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				}
			},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(record.get('securityTarget.clientInvestmentAccount.label'), metaData, record, true);
				return value;
			}
		},
		{header: 'Holding Account', width: 15, dataIndex: 'holdingInvestmentAccount.number', idDataIndex: 'holdingInvestmentAccount.id', nonPersistentField: true, renderer: TCG.renderValueWithTooltip},
		{header: 'Holding Company', width: 13, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', idDataIndex: 'holdingInvestmentAccount.issuingCompany.id', nonPersistentField: true, renderer: TCG.renderValueWithTooltip, hidden: true},
		{header: 'Inception Date', width: 13, dataIndex: 'securityTarget.clientInvestmentAccount.inceptionDate', hidden: true, nonPersistentField: true},
		{header: 'Cash', width: 13, dataIndex: 'cashBalance', type: 'currency', nonPersistentField: true, hidden: true},
		{
			header: 'Underlying', width: 10, dataIndex: 'securityTarget.targetUnderlyingSecurity.symbol', idDataIndex: 'securityTarget.targetUnderlyingSecurity.id', filter: {type: 'combo', searchFieldName: 'targetUnderlyingSecurityId', displayField: 'symbol', comboDisplayField: 'label', url: 'investmentSecurityListFind.json'},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridSecurityColumnFreeze(metaData, record, false);
				return value;
			}
		},
		{header: 'Underlying Price', width: 10, dataIndex: 'underlyingSecurityPrice', type: 'currency', useNull: true, nonPersistentField: true, hidden: true},
		{header: 'Target Multiplier', width: 10, dataIndex: 'securityTarget.targetUnderlyingMultiplier', type: 'float', nonPersistentField: true, hidden: true},
		{
			header: 'Quantity', width: 13, dataIndex: 'underlyingSecurityTargetQuantity', type: 'float', useNull: true, numberFormat: '0,000.00', nonPersistentField: true, viewNames: ['Quantity'],
			tooltip: 'Underlying Security quantity to target for the client account',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordSecurityTargetValue(value, record, true);
			}
		}, {
			header: 'Notional', width: 13, dataIndex: 'underlyingSecurityTargetNotional', type: 'float', useNull: true, numberFormat: '0,000.00', nonPersistentField: true, viewNames: ['Notional'],
			tooltip: 'Underlying Security notional to target for the client account',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordSecurityTargetValue(value, record, false);
			}
		},
		{
			header: 'Covered', width: 13, dataIndex: 'coveredQuantity', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			tooltip: 'Utilization of the target by held positions pertaining to the target underlying security for the client account',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordNumericValue(value, record, true, metadata);
			}
		},
		{
			header: 'Cov. %', width: 9, dataIndex: 'coveredQuantityPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('covered', record, true),
			renderer: function(value, metadata, record) {
				const gp = this.scope.gridPanel;
				const returnValue = Clifton.trade.monitor.renderRecordValueAsPercentage('covered', record, true);
				gp.utilizationPercentRendererHelper(metadata, record.data.coveredQuantityPercent);
				return returnValue;
			}
		},
		{
			header: 'Covered', width: 13, dataIndex: 'coveredNotional', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			tooltip: 'Utilization of the target by held positions pertaining to the target underlying security for the client account',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata);
			}
		},
		{
			header: 'Cov. %', width: 9, dataIndex: 'coveredNotionalPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('covered', record, false),
			renderer: function(value, metadata, record) {
				const gp = this.scope.gridPanel;
				const returnValue = Clifton.trade.monitor.renderRecordValueAsPercentage('covered', record, false);
				gp.utilizationPercentRendererHelper(metadata, record.data.coveredNotionalPercent);
				return returnValue;
			}
		},
		{
			header: 'Frozen', width: 13, dataIndex: 'frozenQuantity', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			tooltip: 'Utilization of the target by frozen positions pertaining to the target underlying security for the client account',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata);
			}
		},
		{
			header: 'Fro. %', width: 9, dataIndex: 'frozenQuantityPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('frozen', record, true),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('frozen', record, true)
		},
		{
			header: 'Frozen', width: 13, dataIndex: 'frozenNotional', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			tooltip: 'Utilization of the target by frozen positions pertaining to the target underlying security for the client account',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata);
			}
		},
		{
			header: 'Fro. %', width: 9, dataIndex: 'frozenNotionalPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('frozen', record, false),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('frozen', record, false)
		},
		{
			header: 'Pending', width: 13, dataIndex: 'pendingQuantity', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			tooltip: 'Utilization of the target by pending activity pertaining to the target underlying security for the client account. Light orange highlighting indicates pending activity.',
			renderer: function(value, metadata, record) {
				const returnValue = Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata);
				if (TCG.isNotBlank(value)) {
					metadata.css += ' x-grid3-row-highlight-light-orange';
				}
				return returnValue;
			}
		},
		{
			header: 'Pnd. %', width: 9, dataIndex: 'pendingQuantityPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('pending', record, true),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('pending', record, true)
		},
		{
			header: 'Pending', width: 13, dataIndex: 'pendingNotional', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			tooltip: 'Utilization of the target by pending activity pertaining to the target underlying security for the client account. Light orange highlighting indicates pending activity.',
			renderer: function(value, metadata, record) {
				const returnValue = Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata);
				if (TCG.isNotBlank(value)) {
					metadata.css += ' x-grid3-row-highlight-light-orange';
				}
				return returnValue;
			}
		},
		{
			header: 'Pnd. %', width: 9, dataIndex: 'pendingNotionalPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('pending', record, false),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('pending', record, false)
		},
		{
			header: 'Total Usage', width: 13, dataIndex: 'totalUsageQuantity', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			tooltip: 'Total current utilization of the target underlying security including covered, frozen, and pending balances',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata);
			}
		},
		{
			header: 'Total %', width: 9, dataIndex: 'totalUsageQuantityPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('totalUsage', record, true),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('totalUsage', record, true)
		},
		{
			header: 'Total Usage', width: 13, dataIndex: 'totalUsageNotional', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			tooltip: 'Total current utilization of the target underlying security including covered, frozen, and pending balances',
			renderer: function(value, metadata, record) {
				return Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata);
			}
		},
		{
			header: 'Total %', width: 9, dataIndex: 'totalUsageNotionalPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('totalUsage', record, false),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('totalUsage', record, false)
		},
		{
			header: 'Unwritten', width: 13, dataIndex: 'unwrittenQuantity', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			tooltip: 'Amount of the adjusted target underlying security that can be traded to fully utilize the target',
			calculate: record => Clifton.trade.monitor.calculateAvailableForRecord(record, false, true),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordNumericValue(Clifton.trade.monitor.calculateAvailableForRecord(record, false, true), record, metadata)
		},
		{
			header: 'Unw. %', width: 9, dataIndex: 'unwrittenQuantityPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('unwritten', record, true),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('unwritten', record, true)
		},
		{
			header: 'Unwritten', width: 13, dataIndex: 'unwrittenNotional', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			tooltip: 'Amount of the adjusted target underlying security that can be traded to fully utilize the target',
			calculate: record => Clifton.trade.monitor.calculateAvailableForRecord(record, false, false),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordNumericValue(Clifton.trade.monitor.calculateAvailableForRecord(record, false, false), record, metadata)
		},
		{
			header: 'Unw. %', width: 9, dataIndex: 'unwrittenNotionalPercent', type: 'percent', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			calculate: record => Clifton.trade.monitor.calculateRecordValuePercentage('unwritten', record, false),
			renderer: (value, metadata, record) => Clifton.trade.monitor.renderRecordValueAsPercentage('unwritten', record, false)
		},
		{
			header: 'Available', width: 13, dataIndex: 'availableQuantity', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Quantity'],
			tooltip: 'Amount of the defined target underlying security available to be traded',
			calculate: record => Clifton.trade.monitor.calculateAvailableForRecord(record, true, true),
			renderer: (value, metadata, record) => TCG.renderAmount(Clifton.trade.monitor.calculateAvailableForRecord(record, true, true), false, '0,000.00')
		},
		{
			header: 'Available', width: 13, dataIndex: 'availableNotional', type: 'float', useNull: true, nonPersistentField: true, viewNames: ['Notional'],
			tooltip: 'Amount of the defined target underlying security available to be traded',
			calculate: record => Clifton.trade.monitor.calculateAvailableForRecord(record, true, false),
			renderer: (value, metadata, record) => TCG.renderAmount(Clifton.trade.monitor.calculateAvailableForRecord(record, true, false), false, '0,000.00')
		},
		{
			header: 'Px Chg 1D', width: 9, dataIndex: 'priceChangePercentDaily', type: 'percent', useNull: true, nonPersistentField: true,
			tooltip: 'Percent change in underlying security price over the last day ((Last Trade - Closing Price One Day Ago) / Closing Price One Day Ago) * 100'
		},
		{
			header: 'Px Chg YTD', width: 9, dataIndex: 'priceChangePercentYTD', type: 'percent', useNull: true, nonPersistentField: true,
			tooltip: 'Percent change in underlying security price for the calendar year to date; calculated from last close of prior year to last price'
		},
		{
			header: 'Earnings Date', width: 12, dataIndex: 'earningsDate', type: 'date', useNull: true, nonPersistentField: true,
			tooltip: 'Date on which the next earnings report should occur'
		},
		{
			header: 'Last Buy', width: 12, dataIndex: 'securityTarget.lastBuyTradeDate', type: 'date', useNull: true, nonPersistentField: true,
			tooltip: 'Date for the last buy trade applicable to this Security Target',
			renderer: function(value, metadata, record) {
				if (TCG.isNotBlank(value)) {
					const gp = this.scope.gridPanel;
					if (Clifton.trade.monitor.getUnderlyingSecurityTargetAdjustedForRecord(record, true) > 0.00) {
						gp.lastOpenDateRendererHelper(metadata, new Date(value));
					}
					return TCG.renderDate(value);
				}
				return null;
			}
		},
		{
			header: 'Last Sell', width: 12, dataIndex: 'securityTarget.lastSellTradeDate', type: 'date', useNull: true, nonPersistentField: true,
			tooltip: 'Date for the last buy trade applicable to this Security Target',
			renderer: function(value, metadata, record) {
				if (TCG.isNotBlank(value)) {
					const gp = this.scope.gridPanel;
					if (Clifton.trade.monitor.getUnderlyingSecurityTargetAdjustedForRecord(record, true) < 0.00) {
						gp.lastOpenDateRendererHelper(metadata, new Date(value));
					}
					return TCG.renderDate(value);
				}
				return null;
			}
		}
	],

	///////////////////////////////////////////////////////////////////////////
	// Custom Rendering Functions
	///////////////////////////////////////////////////////////////////////////

	lastOpenDateRendererHelper: function(metadata, lastOpenDate) {
		const dateSelector = TCG.getChildByName(this.toolbars[0], 'balanceDate');
		if (TCG.isNotBlank(dateSelector) && TCG.isNotBlank(dateSelector.value)) {
			const balanceDate = new Date(dateSelector.value);
			const thresholdDate = Clifton.calendar.getBusinessDayFromDate(balanceDate, -10.0, false);

			// if our calculated thresholdDate is after the lastOpenDate, we have a warning condition.
			if (TCG.isDateAfter(thresholdDate, lastOpenDate, true)) {
				metadata.css += ' x-grid3-row-highlight-light-red';
			}
		}
	},

	utilizationPercentRendererHelper: function(metadata, coveredPercent) {
		const lowPercentThreshold = 65;
		if (TCG.isNotBlank(coveredPercent) && coveredPercent < lowPercentThreshold) {
			metadata.css += ' x-grid3-row-highlight-light-orange';
		}
	},

	///////////////////////////////////////////////////////////////////////////
	// Trading Functions
	///////////////////////////////////////////////////////////////////////////

	tradeToOpen: async function() {
		const records = this.grid.getSelectionModel().getSelections();
		if (records.length === 0) {
			TCG.showInfo('Please select a record to trade.', 'No Records Selected');
			return;
		}
		const investmentTypeName = 'Options'; // default to Options for now

		if (TCG.isFalse(this.validatePositionDirectionsAreSame(records))) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because not all the selected positions have the same direction (short / long).');
			return;
		}

		const underlyingSecurityMap = records
			.map(record => TCG.getValue('securityTarget.targetUnderlyingSecurity', record.json))
			.filter(underlyingSecurity => !!underlyingSecurity.id)
			.reduce((securityMap, underlyingSecurity) => {
				securityMap[underlyingSecurity.id] = underlyingSecurity;
				return securityMap;
			}, {});

		const underlyingSecurityIds = Object.keys(underlyingSecurityMap);
		if (underlyingSecurityIds.length > 1) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because more than one underlying security was submitted for trading.');
			return;
		}

		const underlyingSecurityId = (underlyingSecurityIds.length === 1) ? parseInt(underlyingSecurityIds[0]) : void 0;

		// check for SELL & SELL TO OPEN, NO TRADE restrictions for given underlying.
		const restrictionCheckResult = this.checkTradeRestrictionsUnderlyingTrade(records, underlyingSecurityId, true, true);
		const tradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
		if (!tradePositions) {
			return;
		}

		// Validate selected positions
		const underlyingPriceMarketDataValue = await Clifton.marketdata.field.getSecurityLiveMarketDataValue(underlyingSecurityId, void 0, {maxAgeInSeconds: 5, providerOverride: 'BPIPE'});
		const underlyingPrice = underlyingPriceMarketDataValue && underlyingPriceMarketDataValue.value;

		// map/object to track processed client accounts to prevent defaulting trade data for the same account more than once
		// populate trade details for each selected row for the trade window
		const distinctClientList = tradePositions
			.map(row => row.json)
			.distinct(utilization => utilization.securityTarget.clientInvestmentAccount.id);
		const tradeDetailPromiseList = distinctClientList.map(async utilization => {
			const tradeDetail = {
				clientInvestmentAccount: utilization.securityTarget.clientInvestmentAccount,
				cashBalance: utilization.cashBalance,
				underlyingPrice: underlyingPrice,
				investmentSecurity: utilization.securityTarget.targetUnderlyingSecurity
			};
			// if there is an underlying security, look up client security target for defaulting quantity
			if (underlyingSecurityId) {
				const columnValue = await Clifton.system.GetCustomColumnValuePromise(utilization.securityTarget.clientInvestmentAccount.id, 'InvestmentAccount', 'Portfolio Setup', 'Position Open Percent', true);
				if (columnValue != null) {
					tradeDetail['openPercent'] = columnValue;
				}
				tradeDetail['utilizationUnderlyingTarget'] = utilization.underlyingSecurityTargetQuantityAdjusted;
				tradeDetail['utilizationTotalUsage'] = utilization.totalUsageQuantity;
				tradeDetail['utilizationCovered'] = utilization.coveredQuantity;
				tradeDetail['utilizationPending'] = utilization.pendingQuantity;
				tradeDetail['utilizationFrozen'] = utilization.frozenQuantity;
			}
			return tradeDetail;
		});

		// Retrieve trade data
		const [tradeType, openCloseType, tradeDetailList] = await Promise.all([
			TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json', this, 'trade.type.' + investmentTypeName, {params: {name: investmentTypeName}}),
			TCG.data.getDataPromise('tradeOpenCloseTypeByName.json', this, {params: {name: 'Sell To Open'}}),
			Promise.all(tradeDetailPromiseList)
		]);

		TCG.createComponent('Clifton.trade.monitor.PositionOpenTradeWindow', {
			defaultData: {
				tradeType: tradeType,
				openCloseType: openCloseType,
				applySecurityTargetUtilization: true, // flag to apply usage details
				tradeDetailList: tradeDetailList,
				...underlyingSecurityId ? {underlyingInvestmentSecurity: {id: underlyingSecurityId}} : {}
			},
			openerCt: this
		});
	},

	///////////////////////////////////////////////////////////////////////////
	// Trading Underlying functions
	///////////////////////////////////////////////////////////////////////////

	tradeSellUnderlying: async function() {
		this.tradeUnderlying(true);
	},

	tradeBuyUnderlying: async function() {
		this.tradeUnderlying(false);
	},

	tradeUnderlying: async function(sell) {
		const records = this.grid.getSelectionModel().getSelections();
		if (records.length === 0) {
			TCG.showInfo('Please select a record to trade.', 'No Records Selected');
			return;
		}
		const underlyingSecurityMap = records
			.map(record => TCG.getValue('securityTarget.targetUnderlyingSecurity', record.json))
			.filter(underlyingSecurity => !!underlyingSecurity.id)
			.reduce((securityMap, underlyingSecurity) => {
				securityMap[underlyingSecurity.id] = underlyingSecurity;
				return securityMap;
			}, {});

		const underlyingSecurityIds = Object.keys(underlyingSecurityMap);
		if (underlyingSecurityIds.length > 1) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because more than one underlying security was submitted for trading.');
			return;
		}

		const underlyingSecurityId = (underlyingSecurityIds.length === 1) ? parseInt(underlyingSecurityIds[0]) : void 0;
		if (!underlyingSecurityId) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because an underlying security to trade could not be determined.');
			return;
		}

		const underlyingSecurity = await TCG.data.getDataPromise('investmentSecurity.json?id=' + underlyingSecurityId, this);
		const investmentTypeName = underlyingSecurity.instrument.hierarchy.investmentType.name;
		if (investmentTypeName === 'Benchmarks') {
			Ext.Msg.alert('Unable to Trade', 'Unable to trade Underlying Benchmarks.');
			return;
		}

		// Check for restrictions on underlying.  Note: for underlying trades, if sell is true, open is false, similarly if sell is false, open is true.
		const restrictionCheckResult = this.checkTradeRestrictionsUnderlyingTrade(records, underlyingSecurityId, sell, !sell);
		const tradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
		if (!tradePositions) {
			return;
		}

		// map/object to track processed client accounts to prevent defaulting trade data for the same account more than once
		// populate trade details for each selected row for the trade window
		const distinctClientUtilizationList = tradePositions
			.map(row => row.json)
			.distinct(utilization => utilization.securityTarget.clientInvestmentAccount.id);

		const directionFunction = sell ? utilization => utilization.underlyingSecurityTargetQuantity < 0 : utilization => utilization.underlyingSecurityTargetQuantity > 0;
		// validate all positions same direction
		const underlyingTargetDirections = distinctClientUtilizationList.map(directionFunction).distinct();
		if (underlyingTargetDirections.length !== 1) {
			Ext.Msg.alert('Unable to Trade', 'Can only trade an Underlying Security in the same direction (long/short). Security Targets selected result in opposing directions.');
			return;
		}

		const tradeDetailPromiseList = distinctClientUtilizationList.map(async utilization => {
			const tradeDetail = {
				clientInvestmentAccount: utilization.securityTarget.clientInvestmentAccount,
				cashBalance: utilization.cashBalance,
				investmentSecurity: utilization.securityTarget.targetUnderlyingSecurity,
				underlyingPrice: utilization.underlyingSecurityPrice
			};
			tradeDetail[sell ? 'originalQuantity' : 'utilizationUnderlyingTarget'] = utilization.underlyingSecurityTargetQuantity;
			return tradeDetail;
		});
		const openCloseTypeParams = {
			buy: !sell,
			close: true,
			open: true
		};
		const [tradeType, openCloseType, tradeDetailList] = await Promise.all([
			TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json', this, 'trade.type.' + investmentTypeName, {params: {name: investmentTypeName}}),
			TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', this, {params: openCloseTypeParams}).then(types => types[0]),
			Promise.all(tradeDetailPromiseList)
		]);
		const componentWindow = sell ? 'Clifton.trade.monitor.PositionCloseTradeWindow' : 'Clifton.trade.monitor.PositionOpenTradeWindow';
		const launchParams = {
			defaultData: {
				tradeType: tradeType,
				openCloseType: openCloseType,
				investmentSecurity: underlyingSecurity,
				allowQuantityEdits: true, // flag to apply usage details to trades
				tradeDetailList: tradeDetailList
			},
			openerCt: this
		};
		if (sell) {
			launchParams.submitTradesCallback = async formPanel => {
				const grid = TCG.getChildByName(formPanel, 'tradeDetailListGrid');
				const records = grid.getStore().data.items;

				const restrictionCheckResult = formPanel.ownerCt.openerCt.checkTradeRestrictionsUnderlyingTrade(records, formPanel.ownerCt.defaultData.investmentSecurity.id, true, false);
				const sellTradePositions = await formPanel.ownerCt.openerCt.getTradeRestrictionActionForPositions(restrictionCheckResult);
				if (!sellTradePositions) {
					return;
				}
				const totalQuantity = sellTradePositions
					.map(record => record.get('quantityIntended'))
					.reduce((totalQuantity, recordQuantity) => totalQuantity + (recordQuantity || 0), 0);
				grid.getStore().data.items = sellTradePositions;
				grid.markModified();
				grid.getStore().data.items = records;
				formPanel.getForm().submit({
					...TCG.form.submitDefaults,
					timeout: formPanel.getWindow().saveTimeout,
					url: encodeURI('tradeEntrySave.json'),
					params: {quantityIntended: totalQuantity},
					waitMsg: 'Saving...',
					submitEmptyText: formPanel.submitEmptyText,
					success: (form, action) => {
						const data = formPanel.getDataAfterSave(action.result.data);
						if (data !== false) {
							formPanel.getWindow().closeWindow();
						}
					}
				});
			};
		}
		TCG.createComponent(componentWindow, launchParams);
	},
	/**
	 * A function that checks for TradeRestrictions for trades of the underlying security
	 *
	 * @param selectedPositionList selected positions to check restrictions for
	 * @param underlyingSecurityId id of underlying security to be traded
	 * @param isSell a flag that indicates whether or not this is a selling trade
	 * @param isOpen a flag that indicates whether or not this is an opening trade
	 * @returns {*} an object containing an {@link Array} of positions with trade restrictions and an {@link Array} of positions that are valid for trading
	 */
	checkTradeRestrictionsUnderlyingTrade: function(selectedPositionList, underlyingSecurityId, isSell, isOpen) {
		const [validPositions, restrictedPositions] = selectedPositionList.partition(selectedPosition => {
			const clientInvestmentAccountId = selectedPosition.data['clientInvestmentAccount.id'] || selectedPosition.json.securityTarget.clientInvestmentAccount.id;
			const underlyingPrice = selectedPosition.json.underlyingSecurityPrice;
			return this.checkTradeRestrictions(clientInvestmentAccountId, null, underlyingSecurityId, underlyingPrice, isSell, isOpen, this.lastTradeRestrictionList);
		});
		return {validPositions, restrictedPositions};
	},
	/**
	 * This function checks a list of positions and validates that all positions have the same direction(long or short).
	 *
	 * Returns true if all positions have the same direction, otherwise returns false.
	 *
	 * @param positionList list of selected positions from grid.
	 */
	validatePositionDirectionsAreSame: function(positionList) {
		if (TCG.isBlank(positionList) || positionList.length <= 1) {
			return true;
		}

		const directionIsNegative = positionList[0].json.underlyingSecurityTargetQuantityAdjusted < 0.00;

		for (const position of positionList) {
			if ((directionIsNegative && position.json.underlyingSecurityTargetQuantityAdjusted > 0.00) || (!directionIsNegative && position.json.underlyingSecurityTargetQuantityAdjusted < 0.00)) {
				return false;
			}
		}
		return true;
	}
});
Ext.reg('trade-utilization-grid', Clifton.trade.monitor.UtilizationGridPanel);

Clifton.trade.monitor.getSecurityTargetTotalUsageForRecord = function(record, useQuantity) {
	return TCG.getValue('totalUsage' + (TCG.isTrue(useQuantity) ? 'Quantity' : 'Notional'), record.json);
};

Clifton.trade.monitor.getUnderlyingSecurityTargetOriginalForRecord = function(record, useQuantity) {
	return TCG.getValue('underlyingSecurityTarget' + (TCG.isTrue(useQuantity) ? 'Quantity' : 'Notional'), record.json);
};

Clifton.trade.monitor.getUnderlyingSecurityTargetAdjustedForRecord = function(record, useQuantity) {
	return TCG.getValue('underlyingSecurityTarget' + (TCG.isTrue(useQuantity) ? 'Quantity' : 'Notional') + 'Adjusted', record.json);
};

Clifton.trade.monitor.calculateAvailableForRecord = function(record, useAdjusted, useQuantity) {
	const suffix = (TCG.isTrue(useQuantity) ? 'Quantity' : 'Notional');
	const fieldName = (TCG.isTrue(useAdjusted) ? 'available' : 'unwritten') + suffix;
	let value = record.data[fieldName];
	if (TCG.isBlank(value)) {
		let usage = Clifton.trade.monitor.getSecurityTargetTotalUsageForRecord(record, useQuantity);
		let targetValue = Clifton.trade.monitor.getUnderlyingSecurityTargetAdjustedForRecord(record, useQuantity);
		if (TCG.isTrue(useAdjusted)) {
			targetValue = Math.abs(targetValue);
			// convert according to multiplier sign to correctly adjust the value.
			usage *= (TCG.getValue('securityTarget.targetUnderlyingMultiplier', record.json) < 0) ? -1 : 1;
		}
		value = targetValue - usage;
		record.data[fieldName] = value;
	}
	return value;
};

Clifton.trade.monitor.renderRecordSecurityTargetValue = function(value, record, useQuantity) {
	const format = '0,000.00';
	const target = value;
	const targetAdjusted = Clifton.trade.monitor.getUnderlyingSecurityTargetAdjustedForRecord(record, useQuantity);

	if (target === targetAdjusted) {
		return TCG.renderAmount(target, false, format);
	}
	const targetFormatted = TCG.numberFormat(target, format);
	const targetAdjustedFormatted = TCG.numberFormat(targetAdjusted, format);
	return '<div class="amountAdjusted" qtip=\'<table><tr><td>Underlying Target: </td><td align="right">' + targetFormatted + '</td></tr><tr><td nowrap>' + 'Multiplier Adjusted Target' + ': </td><td align="right">' + targetAdjustedFormatted + '</td></tr></table>\'>' + TCG.numberFormat(Math.abs(targetAdjusted), format) + '</div>';

};

Clifton.trade.monitor.renderRecordNumericValue = function(value, record, metadata, percentage) {
	const renderValue = (value || 0);
	const returnValue = TCG.numberFormat(renderValue, (TCG.isTrue(percentage) ? '0,000.00 %' : '0,000.00'));
	const returnValueFormatted = TCG.isTrue(renderValue < 0) ? '<div class="amountNegative">' + returnValue + '</div>' : returnValue;
	return TCG.isNotNull(metadata) ? TCG.renderValueWithTooltip(returnValueFormatted, metadata, record) : returnValueFormatted;
};

Clifton.trade.monitor.calculateRecordValuePercentage = function(fieldPrefix, record, useQuantity) {
	const field = fieldPrefix + (useQuantity ? 'Quantity' : 'Notional');
	const percentField = field + 'Percent';
	let percentageValue = record.data[percentField];
	if (TCG.isBlank(percentageValue)) {
		const value = (record.get(field) || 0);
		let target = Clifton.trade.monitor.getUnderlyingSecurityTargetAdjustedForRecord(record, useQuantity);
		if (target === 0) {
			target = 1; // prevent divide by 0
		}
		percentageValue = value / target * 100;
		record.data[percentField] = percentageValue;
	}
	return percentageValue;
};

Clifton.trade.monitor.renderRecordValueAsPercentage = function(fieldPrefix, record, useQuantity, metadata) {
	const value = Clifton.trade.monitor.calculateRecordValuePercentage(fieldPrefix, record, useQuantity);
	return Clifton.trade.monitor.renderRecordNumericValue(value, record, metadata, true);
};

Clifton.trade.monitor.TradeCoveredCallUtilizationDetailWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Target Utilization',
	iconCls: 'account',
	width: 900,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: ' Info',
				items: [
					{
						xtype: 'formpanel',
						url: 'tradeSecurityTargetUtilization.json',
						labelFieldName: 'securityTarget.label',
						loadValidation: false,
						readOnly: true,
						controlWindowModified: false,
						labelWidth: 140,
						layout: 'vbox',
						layoutConfig: {
							align: 'stretch'
						},

						getLoadParams: function(window) {
							return {
								investmentAccountSecurityTargetId: window.defaultData.securityTargetId,
								balanceDate: window.defaultData.balanceDate,
								usePriorPriceDate: true
							};
						},

						listeners: {
							afterload: function() {
								if (TCG.isBlank(this.getFormValue('securityTarget.securityTargetNote'))) {
									this.setFormValue('securityTarget.securityTargetNote', 'Underlying security target for ' + this.getFormValue('securityTarget.targetUnderlyingSecurity.label'));
								}
								if (TCG.isBlank(this.getFormValue('securityTarget.targetUnderlyingQuantity'))) {
									this.getForm().findField('securityTarget.targetUnderlyingQuantity').setVisible(false);
									if (TCG.isBlank(this.getFormValue('securityTarget.targetUnderlyingNotional'))) {
										this.getForm().findField('securityTarget.targetUnderlyingManagerAccount.label').setVisible(true);
									}
									else {
										this.getForm().findField('securityTarget.targetUnderlyingNotional').setVisible(true);
									}
								}
								if (TCG.isBlank(this.getFormValue('holdingInvestmentAccount.id'))) {
									TCG.data.getDataPromise('investmentAccountListFind.json?ourAccount=false', this, {
										params: {
											mainAccountId: this.getFormValue('securityTarget.clientInvestmentAccount.id'),
											mainPurpose: 'Trading: Options',
											mainPurposeActiveOnDate: new Date().format('m/d/Y'),
											requestedPropertiesRoot: 'data',
											requestedProperties: 'name|label|id'
										}
									}).then(clientHoldingAccounts => {
										if (clientHoldingAccounts && clientHoldingAccounts.length === 1) {
											this.getForm().findField('holdingInvestmentAccount.label').setValue(clientHoldingAccounts[0].label);
											this.getForm().findField('holdingInvestmentAccount.id').setValue(clientHoldingAccounts[0].id);
										}
									});
								}
							}
						},

						items: [
							{
								xtype: 'panel',
								layout: 'form',
								defaults: {anchor: '-20'},
								items: [
									{
										xtype: 'panel',
										layout: 'column',
										anchor: '0',
										defaults: {layout: 'form'},
										items: [
											{
												columnWidth: .65,
												defaults: {xtype: 'linkfield', flex: 1, anchor: '-20'},
												items: [
													{fieldLabel: 'Client Account', name: 'securityTarget.clientInvestmentAccount.label', detailIdField: 'securityTarget.clientInvestmentAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
													{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', detailIdField: 'holdingInvestmentAccount.id', submitValue: false, submitDetailField: false, detailPageClass: 'Clifton.investment.account.AccountWindow'},
													{fieldLabel: 'Target Note', name: 'securityTarget.securityTargetNote', detailIdField: 'securityTarget.id', detailPageClass: 'Clifton.investment.account.securitytarget.AccountSecurityTargetWindow'}
												]
											},
											{
												columnWidth: .35,
												defaults: {xtype: 'datefield', anchor: '-20'},
												items: [
													{fieldLabel: 'Start Date', name: 'securityTarget.startDate'},
													{fieldLabel: 'End Date', name: 'securityTarget.endDate'}
												]
											}
										]
									},
									{xtype: 'sectionheaderfield', header: 'Underlying Target'},
									{
										xtype: 'panel',
										layout: 'column',
										anchor: '0',
										defaults: {layout: 'form', defaults: {anchor: '-20'}},
										items: [
											{
												columnWidth: .65,
												items: [
													{fieldLabel: 'Target Security', name: 'securityTarget.targetUnderlyingSecurity.label', xtype: 'linkfield', detailIdField: 'securityTarget.targetUnderlyingSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', flex: 1},
													{fieldLabel: 'Target Quantity', name: 'securityTarget.targetUnderlyingQuantity', xtype: 'currencyfield', hidden: false},
													{fieldLabel: 'Target Notional', name: 'securityTarget.targetUnderlyingNotional', xtype: 'currencyfield', hidden: true},
													{fieldLabel: 'Target Manager Account', name: 'securityTarget.targetUnderlyingManagerAccount.label', detailIdField: 'securityTarget.targetUnderlyingManagerAccount.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.manager.AccountWindow', renderDataTip: true, hidden: true, flex: 1}
												]
											},
											{
												columnWidth: .35,
												items: [
													{fieldLabel: 'Target Multiplier', name: 'securityTarget.targetUnderlyingMultiplier', xtype: 'floatfield'},
													{fieldLabel: 'Tranche Count', name: 'securityTarget.trancheCount', xtype: 'integerfield'}
												]
											}
										]
									}
								]
							},
							{
								xtype: 'formgrid',
								title: 'Utilization',
								collapsible: false,
								storeRoot: 'detailList',
								dtoClass: 'com.clifton.trad.options.securitytarget.TradeSecurityTargetUtilizationDetail',
								readOnly: true,
								viewConfig: {forceFit: true},
								autoHeight: false,
								flex: 1,
								anchor: '-20',
								addToolbarButtons: function(toolbar) {
									const gridPanel = this;
									toolbar.add({
										text: 'Reload',
										xtype: 'button',
										iconCls: 'table-refresh',
										handler: function() {
											const formPanel = TCG.getParentFormPanel(gridPanel);
											formPanel.load(Ext.applyIf({
												url: encodeURI(formPanel.getLoadURL()),
												params: formPanel.getLoadParams(formPanel.getWindow()),
												waitMsg: 'Loading...',
												success: function(form, action) {
													formPanel.loadJsonResult(action.result, true);
													const data = formPanel.getForm().formValues;
													gridPanel.store.loadData(data);
												}
											}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
										}
									});
								},
								columnsConfig: [
									{header: 'Security', width: 50, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', filter: false},
									{header: 'Covered', width: 20, dataIndex: 'coveredQuantity', type: 'float', useNull: true, nonPersistentField: true, numberFormat: '0,000.00', summaryType: 'sum', tooltip: 'Utilization of the target by held positions of the security'},
									{
										header: 'Cov. %', width: 15, dataIndex: 'coveredPercent', type: 'float', useNull: true, nonPersistentField: true, summaryType: 'sum', tooltip: 'Utilization percent of the target by held positions of the security',
										renderer: function(value, metadata, record) {
											let returnValue = value;
											if (TCG.isBlank(returnValue)) {
												const quantity = TCG.getValue('coveredQuantity', record.json);
												if (TCG.isNotBlank(quantity)) {
													const target = Clifton.trade.monitor.getFormGridUnderlyingSecurityTargetAdjusted(this.gridPanel, record);
													returnValue = (target === 0) ? 0 : quantity / target * 100;
													record.data['coveredPercent'] = returnValue;
												}
											}
											return TCG.isBlank(returnValue) ? returnValue : TCG.renderAmount(returnValue, false, '0,000.00 %');
										}
									},
									{header: 'Frozen', width: 20, dataIndex: 'frozenQuantity', type: 'float', useNull: true, nonPersistentField: true, numberFormat: '0,000.00', summaryType: 'sum', tooltip: 'Utilization of the target by frozen positions of the security'},
									{
										header: 'Froz. %', width: 15, dataIndex: 'frozenPercent', type: 'float', useNull: true, nonPersistentField: true, summaryType: 'sum', tooltip: 'Utilization percent of the target by held positions of the security',
										renderer: function(value, metadata, record) {
											let returnValue = value;
											if (TCG.isBlank(returnValue)) {
												const quantity = TCG.getValue('frozenQuantity', record.json);
												if (TCG.isNotBlank(quantity)) {
													const target = Clifton.trade.monitor.getFormGridUnderlyingSecurityTargetAdjusted(this.gridPanel, record);
													returnValue = (target === 0) ? 0 : quantity / target * 100;
													record.data['frozenPercent'] = returnValue;
												}
											}
											return TCG.isBlank(returnValue) ? returnValue : TCG.renderAmount(returnValue, false, '0,000.00 %');
										}
									},
									{header: 'Pending', width: 20, dataIndex: 'pendingQuantity', type: 'float', useNull: true, nonPersistentField: true, numberFormat: '0,000.00', summaryType: 'sum', tooltip: 'Utilization of the target by pending activity of the security'},
									{
										header: 'Pend. %', width: 15, dataIndex: 'pendingPercent', type: 'float', useNull: true, nonPersistentField: true, summaryType: 'sum', tooltip: 'Utilization percent of the target by held positions of the security',
										renderer: function(value, metadata, record) {
											let returnValue = value;
											if (TCG.isBlank(returnValue)) {
												const quantity = TCG.getValue('pendingQuantity', record.json);
												if (TCG.isNotBlank(quantity)) {
													const target = Clifton.trade.monitor.getFormGridUnderlyingSecurityTargetAdjusted(this.gridPanel, record);
													returnValue = (target === 0) ? 0 : quantity / target * 100;
													record.data['pendingPercent'] = returnValue;
												}
											}
											return TCG.isBlank(returnValue) ? returnValue : TCG.renderAmount(returnValue, false, '0,000.00 %');
										}
									},
									{
										header: 'Total', width: 20, dataIndex: 'totalQuantity', type: 'float', useNull: true, nonPersistentField: true, summaryType: 'sum',
										renderer: function(value, metadata, record) {
											let returnValue = value;
											if (TCG.isBlank(returnValue)) {
												returnValue = (TCG.getValue('coveredQuantity', record.json) || 0) + (TCG.getValue('frozenQuantity', record.json) || 0);
												record.data['totalQuantity'] = returnValue;
											}
											return TCG.renderAmount(returnValue, false, '0,000.00');
										}
									},
									{
										header: 'Total %', width: 15, dataIndex: 'totalPercent', type: 'float', useNull: true, nonPersistentField: true, summaryType: 'sum',
										renderer: function(value, metadata, record) {
											let returnValue = value;
											if (TCG.isBlank(returnValue)) {
												const quantity = record.data['totalQuantity'];
												if (TCG.isNotBlank(quantity)) {
													const target = Clifton.trade.monitor.getFormGridUnderlyingSecurityTargetAdjusted(this.gridPanel, record);
													returnValue = (target === 0) ? 0 : quantity / target * 100;
													record.data['totalPercent'] = returnValue;
												}
											}
											return TCG.isBlank(returnValue) ? returnValue : TCG.renderAmount(returnValue, false, '0,000.00 %');
										}
									}
								],
								plugins: {ptype: 'gridsummary'}
							}
						]
					}
				]
			},
			{
				title: 'Frozen Allocations',
				items: [{
					xtype: 'accounting-security-target-frozen-allocation-gridpanel',
					getClientSecurityTargetId: function(gridPanel) {
						return gridPanel.getWindow().defaultData.securityTargetId;
					},
					getTargetUnderlyingSecurityId: function(gridPanel) {
						return gridPanel.getWindow().getMainForm().formValues.securityTarget.targetUnderlyingSecurity.id;
					},
					getTargetClientAccountServiceName: function(gridPanel) {
						return gridPanel.getWindow().getMainForm().formValues.securityTarget.clientInvestmentAccount.businessService.name;
					},
					getTargetClientAccountId: function(gridPanel) {
						return gridPanel.getWindow().getMainForm().formValues.securityTarget.clientInvestmentAccount.id;
					}
				}]
			},
			{
				title: 'Utilization History',
				items: [
					{
						xtype: 'panel',
						layout: 'vbox',
						layoutConfig: {
							align: 'stretch'
						},
						defaults: {flex: 1},
						items: [
							{
								xtype: 'trade-utilization-grid',
								name: 'tradeSecurityTargetUtilizationListForDateRange',
								includeTradingActions: false,
								includeOperationsActions: false,
								columnOverrides: [
									{dataIndex: 'securityTarget.clientInvestmentAccount.name', hidden: true},
									{dataIndex: 'securityTarget.targetUnderlyingSecurity.symbol', hidden: true},
									{dataIndex: 'holdingInvestmentAccount.number', hidden: true},
									{dataIndex: 'pendingQuantity', hidden: true},
									{dataIndex: 'pendingQuantityPercent', hidden: true},
									{dataIndex: 'pendingNotional', hidden: true},
									{dataIndex: 'pendingNotionalPercent', hidden: true},
									{dataIndex: 'priceChangePercentDaily', hidden: true},
									{dataIndex: 'priceChangePercentYTD', hidden: true},
									{dataIndex: 'earningsDate', hidden: true},
									{dataIndex: 'securityTarget.lastBuyTradeDate', hidden: true},
									{dataIndex: 'securityTarget.lastSellTradeDate', hidden: true},
									{header: 'Balance Date', width: 12, dataIndex: 'balanceDate', type: 'date'}
								],
								getTopToolbarFilters: function(toolbar) {
									return [
										{fieldLabel: 'Und Security', name: 'underlyingSecuritySymbol', hiddenName: 'underlyingSecurityId', width: 70, xtype: 'toolbar-combo', url: 'investmentSecurityListFind.json?active=true', linkedFilter: 'securityTarget.targetUnderlyingSecurity.symbol', displayField: 'symbol', comboDisplayField: 'label', tooltip: 'Underlying security to filter'},
										{fieldLabel: 'Start Date', name: 'rangeStartDate', xtype: 'toolbar-datefield'},
										{fieldLabel: 'End Date', name: 'rangeEndDate', xtype: 'toolbar-datefield'}
									];
								},
								getLoadParams: function(firstLoad) {
									const toolbar = this.getTopToolbar();
									const rangeStartDateFilter = TCG.getChildByName(toolbar, 'rangeStartDate');
									const rangeEndDateFilter = TCG.getChildByName(toolbar, 'rangeEndDate');
									const targetUnderlyingSecurityFilter = TCG.getChildByName(toolbar, 'underlyingSecuritySymbol');
									if (firstLoad) {
										if (this.defaultViewName) {
											this.setDefaultView(this.defaultViewName);
										}
										const today = new Date();
										rangeStartDateFilter.setValue(today.add(Date.DAY, -30));
										rangeEndDateFilter.setValue(today);
										targetUnderlyingSecurityFilter.setValue({text: this.getWindow().getMainForm().formValues.securityTarget.targetUnderlyingSecurity.symbol, value: this.getWindow().getMainForm().formValues.securityTarget.targetUnderlyingSecurity.id});
									}

									const defaultParams = {
										rangeStartDate: rangeStartDateFilter ? rangeStartDateFilter.getValue().format('m/d/Y') : void 0,
										rangeEndDate: rangeEndDateFilter ? rangeEndDateFilter.getValue().format('m/d/Y') : void 0
									};
									const clientInvestmentAccountId = this.getWindow().getMainForm().formValues.securityTarget.clientInvestmentAccount.id;
									if (TCG.isNotBlank(clientInvestmentAccountId)) {
										defaultParams['clientInvestmentAccountId'] = clientInvestmentAccountId;
									}
									if (TCG.isNotBlank(targetUnderlyingSecurityFilter.getValue())) {
										defaultParams['underlyingSecurityId'] = targetUnderlyingSecurityFilter.getValue();
									}
									return defaultParams;
								},
								getBalanceDate: function(gridPanel, rowIndex) {
									const row = gridPanel.grid.getStore().getAt(rowIndex);
									return row.get('balanceDate');
								},
								loadHandler: function() {
									const columnModel = this.getColumnModel();
									const calculatedColumns = columnModel.getColumnsBy(column => column.calculate && typeof column.calculate === 'function');
									this.grid.store.each(record => calculatedColumns.forEach(column => column.calculate(record)));
									// refresh chart
									const usageHistoryPanel = TCG.getChildByName(TCG.getParentByClass(this, Ext.Panel), 'usageHistoryChart');
									usageHistoryPanel.updateChartData.call(usageHistoryPanel);
								}
							},
							{
								xtype: 'chartpanel',
								name: 'usageHistoryChart',
								chartType: 'line',
								chartOptions: {
									scales: {
										xAxes: [
											{
												scaleLabel: {display: false, labelString: 'Date'},
												type: 'time',
												distribution: 'series', // equally space data points; e.g. between Fri-Mon
												time: {
													displayFormats: {day: 'M/D/Y'}
												}
											}
										],
										yAxes: [
											{
												scaleLabel: {display: true, labelString: 'Shares'},
												position: 'left'
											},
											{
												scaleLabel: {display: true, labelString: 'Share Price'},
												id: 'Price', // create ID to reference via data set
												position: 'right'
											}
										]
									}
								},
								getChartData: function() {
									const dataValues = {};
									const siblingGridPanel = TCG.getChildByName(this.ownerCt, 'tradeSecurityTargetUtilizationListForDateRange');
									const store = siblingGridPanel.grid.store;
									store.each(record => {
										const balanceDate = record.get('balanceDate');
										let balanceDateData = dataValues[balanceDate];
										if (TCG.isNull(balanceDateData)) {
											balanceDateData = {
												targetData: 0,
												coveredData: 0,
												frozenData: 0,
												usageData: 0,
												stockPrice: 0
											};
											dataValues[balanceDate] = balanceDateData;
										}
										balanceDateData.targetData += parseFloat(TCG.getValue('underlyingSecurityTargetQuantityAdjusted', record.json));
										balanceDateData.coveredData += parseFloat(record.get('coveredQuantity'));
										balanceDateData.frozenData += parseFloat(record.get('frozenQuantity'));
										balanceDateData.usageData += parseFloat(record.get('totalUsageQuantity'));
										balanceDateData.stockPrice += parseFloat(record.get('underlyingSecurityPrice'));
									});

									const balanceDates = Object.keys(dataValues);
									const balanceDateDatas = Object.values(dataValues);
									const returnData = {
										labels: balanceDates.map(e => new Date(e)),
										datasets: [
											{label: 'Target', borderColor: '#5c6f76', fill: false, data: balanceDateDatas.map(e => e.targetData)},
											{label: 'Covered', borderColor: '#b50f52', fill: false, data: balanceDateDatas.map(e => e.coveredData)},
											{label: 'Frozen', borderColor: '#3997ab', fill: false, data: balanceDateDatas.map(e => e.frozenData)},
											{label: 'Total', borderColor: '#b8c228', fill: false, data: balanceDateDatas.map(e => e.usageData)}
										]
									};
									if (TCG.isNotBlank(TCG.getChildByName(siblingGridPanel.getTopToolbar(), 'underlyingSecuritySymbol').getValue())) {
										// If there is no underlying security filter, do not include price data
										returnData.datasets.push({label: 'Stock Price', borderColor: '#db5d39', data: balanceDateDatas.map(e => e.stockPrice), fill: false, yAxisID: 'Price'});
									}

									return returnData;
								}
							}
						]
					}
				]
			}
		]
	}]
});

Clifton.trade.monitor.getFormGridUnderlyingSecurityTargetAdjusted = function(gridPanel, record) {
	if (gridPanel) {
		let target = record ? record.data['targetAdjusted'] : void 0;
		if (TCG.isBlank(target)) {
			const formPanel = TCG.getParentFormPanel(gridPanel);
			target = formPanel.getFormValue('underlyingSecurityTargetQuantityAdjusted');
			if (record) {
				record.data['targetAdjusted'] = target;
			}
		}
		return target;
	}
	return void 0;
};
