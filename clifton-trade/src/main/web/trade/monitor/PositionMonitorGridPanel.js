Clifton.trade.monitor.PositionMonitorGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'accountingAnalyticsPositionDetailListFind',
	instructions: 'A list of client account positions reflecting real-time prices',
	storeRoot: 'data.rows', // results should always be PagingArrayList
	enableCellContextMenuFilterItems: true,
	investmentType: '', // placeholder for injecting investment type for filtering
	subscriptionsActive: false, // tracks whether subscriptions are active
	disableAutoSubscription: false, // boolean to disable auto subscribing to securities of viewed positions
	securityIdToRowsMap: {}, // placeholder for collection of security IDs loaded in the grid to corresponding grid rows
	americanCallOptionSecurityIds: [], // list of security IDs that are options for dividend exercise risk
	viewNames: ['Options', 'Market Value'],
	defaultViewName: 'Options',
	standardColumns: [], // avoid create and update columns
	rowSelectionModel: 'checkbox',
	requestIdDataIndex: true,
	includeOpeningCommissions: true,
	includeCollateralPositions: false,
	applyTradeRules: false, // flag to apply trade rules to grid records based on view
	clientInvestmentAccountEditorWindow: 'Clifton.trade.monitor.ClientHoldingsMonitorWindow',
	includeTradingActions: false,
	includeOperationsActions: false,

	gridFiltersConfig: {
		local: false // force remote load for initial request; needed since this panel does not perform initial load
	},

	plugins: [
		{ptype: 'trade-restriction-aware'}
	],

	isEditEnabled: () => false,
	isPagingEnabled: () => false,

	gridConfig: {
		listeners: {
			destroy: function(gridPanel) {
				// when the child grid panel is destroyed, cancel active subscriptions on its parent (this gridpanel)
				gridPanel.ownerGridPanel.unsubscribeHandler();
			}
		}
	},

	viewConfig: {
		markDirty: false, // do not mark dirty cells
		highlightUpdates: true, // custom field to toggle highlighting of updated rows
		listeners: {
			rowupdated: (view, rowIndex, record) => {
				// Get and reset modifications
				const recordChangeList = record.changeList || [];
				record.changeList = [];

				// Flash modified cells
				if (view.highlightUpdates) {
					const cellList = view.getColumnData()
						.filter(col => recordChangeList.includes(col.name))
						.map(col => col.id)
						.map(colId => view.getCell(rowIndex, colId));
					cellList.forEach(cell => cell.classList.remove('flash-element', 'background-fade'));
					requestAnimationFrame(() => {
						cellList.forEach(cell => cell.classList.add('flash-element'));
						requestAnimationFrame(() => {
							cellList.forEach(cell => cell.classList.add('background-fade'));
							requestAnimationFrame(() => cellList.forEach(cell => cell.classList.remove('flash-element')));
						});
					});
				}
			}
		}
	},

	configureViews: function(menu, gridPanel) {
		menu.add(
			'-',
			{
				text: 'Highlight Updates',
				tooltip: 'Highlight each cell when its value is updated.',
				xtype: 'menucheckitem',
				checked: true,
				listeners: {
					checkchange: (item, checked) => gridPanel.grid.view.highlightUpdates = checked
				}
			},
			{
				name: 'includeOpeningCommissions',
				text: 'Include Opening Commissions',
				tooltip: 'Include position opening commissions to calculate each position\'s net cost basis',
				xtype: 'menucheckitem',
				checked: true,
				listeners: {
					checkchange: (item, checked) => {
						gridPanel.includeOpeningCommissions = checked;
						gridPanel.reload();
					}
				}
			},
			{
				text: 'Include Collateral Positions',
				tooltip: 'Display collateral positions',
				xtype: 'menucheckitem',
				checked: false,
				listeners: {
					checkchange: (item, checked) => {
						gridPanel.includeCollateralPositions = checked;
						const cm = gridPanel.getColumnModel();
						const columnIndex = cm.findColumnIndex('accountingBalance.accountingAccount.collateral');
						cm.setHidden(columnIndex, !checked);
						gridPanel.reload();
					}
				}
			},
			{
				text: 'Show Balance Date Filter',
				tooltip: 'Display the Balance Date filter, which can be used to view positions as of a given date.',
				xtype: 'menucheckitem',
				checked: false,
				listeners: {
					checkchange: (item, checked) => {
						const balanceDateFields = gridPanel.getTopToolbar().findBy(cmp => cmp.name === 'balanceDate' || (cmp.text || '').includes('Balance Date'));
						balanceDateFields.forEach(field => field.setVisible(checked));
					}
				}
			}
		);
	},

	switchToViewBeforeReload: function(viewName) {
		switch (viewName) {
			case 'Options': {
				this.setInvestmentTypeFilter('Options');
				this.applyTradeRules = true;
				break;
			}
			case 'Market Value': {
				return TCG.data.getDataPromise('investmentAccountGroupByName.json?name=Client Accounts Managed by Westport', this)
					.then(clientAccountGroup => {
						if (clientAccountGroup) {
							const clientAccountGroupToolbarCombo = TCG.getChildByName(this.getTopToolbar(), 'clientAccountGroupName');
							if (clientAccountGroupToolbarCombo) {
								clientAccountGroupToolbarCombo.setValue({text: clientAccountGroup.name, value: clientAccountGroup.id});
							}
						}
						this.setInvestmentTypeFilter();
						this.applyTradeRules = false;
					});
			}
			default: {
				// do nothing
			}
		}
	},

	addFirstToolbarButtons: function(toolbar, gridPanel) {
		toolbar.add(
			{
				name: 'actionsMenu',
				text: 'Actions',
				iconCls: 'run',
				menu: {
					items: [
						...gridPanel.getActionMenuItems(),
						'-',
						{
							name: 'subscriptionButton', text: 'Subscribe', iconCls: 'bloomberg', handler: gridPanel.subscribeHandler, scope: gridPanel,
							tooltip: '(Un)Subscribes to real-time market data updates for the securities of the listed positions below'
						}, '-',
						{
							text: 'Force Unsubscribe', iconCls: 'bloomberg', scope: gridPanel,
							tooltip: 'Multiple windows opened by you can be using subscriptions. This force unsubscribes any known subscriptions within this browser/tab. If multiple tabs are used, each could be aware of different subscriptions and may need to be processed independently.',
							handler: async () => {
								const activeSubscriptions = Clifton.marketdata.SecuritySubscriptionHandler.getActiveSecurityIds();
								if (activeSubscriptions.length < 1) {
									if ('yes' === await TCG.showConfirm('There are no active subscriptions registered in this browser tab. Continuing will cancel and clear all active subscriptions across all browser windows and tabs for you.', 'Force Subscription Unsubscribe')) {
										this.forceUnsubscribeHandler();
									}
								}
								else if ('yes' === await TCG.showConfirm('Are you sure you want to force unsubscribe ' + activeSubscriptions.length + ' securities? This will affect all active subscriptions for these securities you have across browser windows and tabs. Active subscriptions exist for securities with ID(s):<br/>[' + activeSubscriptions.join(',') + ']', 'Force Subscription Unsubscribe')) {
									this.forceUnsubscribeHandler();
								}
							}
						}, '-',
						{
							text: 'Disable Auto-Subscribe', name: 'disableSubscriptionCheckbox',
							tooltip: 'Disable the auto-subscription of securities of viewed positions',
							xtype: 'menucheckitem',
							checked: gridPanel.disableAutoSubscription,
							listeners: {
								checkchange: (item, checked) => gridPanel.disableAutoSubscription = checked
							}
						}
					]
				}
			},
			'-'
		);
	},

	getActionMenuItems: function() {
		const actions = [];
		if (TCG.isTrue(this.includeTradingActions)) {
			actions.push(
				{
					text: 'Trade to Close',
					tooltip: 'Trade to close the selected position(s). Buy for short position, sell for long position.',
					iconCls: 'run',
					handler: () => this.tradeToClose()
				},
				{
					text: 'Trade to Roll',
					tooltip: 'Trade to roll the selected position(s) to a different security with a later expiration date. Two trades will be created for each position: a close for the option security of the position and an open for the new Option security.',
					iconCls: 'run',
					handler: () => this.tradeToRoll()
				},
				{
					text: 'Process Assignment',
					tooltip: 'Process assignment of selected position(s). Two trades will be created for each position: a close for the option security of the position and delivery of the option\'s underlying.',
					iconCls: 'run',
					handler: () => this.processAssignment()
				},
				{
					text: 'Trade to Open',
					tooltip: 'Trade to open a new position. Buy for long position, sell for short position.',
					iconCls: 'run',
					handler: () => this.tradeToOpen()
				},
				'-',
				{
					text: 'Sell Underlying',
					tooltip: 'Trade to sell the underlying Security of the selected position(s)',
					iconCls: 'run',
					handler: () => this.tradeSellUnderlying()
				},
				{
					text: 'Buy Underlying',
					tooltip: 'Trade to buy the underlying Security of the selected position(s)',
					iconCls: 'run',
					handler: () => this.tradeBuyUnderlying()
				}
			);
		}
		if (TCG.isTrue(this.includeOperationsActions)) {
			if (TCG.isTrue(this.includeTradingActions)) {
				actions.push('-');
			}

			const transferHandler = async contribution => {
				const transferTypeName = contribution ? 'Security Target Contribution' : 'Security Target Distribution';
				const transferAccountPrefix = contribution ? 'to' : 'from';
				const records = this.grid.getSelectionModel().getSelections();
				if (records.length === 0) {
					TCG.showInfo('Please select a client position to create a transfer for.', 'No Position(s) Selected');
					return;
				}
				if (records.length !== 1) {
					TCG.showInfo('Please select only one client position to create a transfer for.', 'Multiple Positions Selected');
					return;
				}
				const record = TCG.grid.getUpdatedRecordAsJson(records[0]);
				const clientAccount = record.clientInvestmentAccount;
				const balanceDate = this.getSelectedBalanceDate();
				const promises = [
					TCG.data.getDataPromiseUsingCaching('accountingPositionTransferTypeByName.json?name=' + transferTypeName, this, 'accounting.positiontransfer.type.' + transferTypeName)
				];
				const investmentType = TCG.getValue('investmentSecurity.instrument.hierarchy.investmentType.name', record);
				const underlyingSecurity = (investmentType === 'Options') ? record.investmentSecurity.underlyingSecurity : record.investmentSecurity;

				promises.push(
					TCG.data.getDataPromise('investmentAccountSecurityTargetForAccountAndUnderlyingSecurityId.json?', this, {
						params: {
							accountId: clientAccount.id,
							underlyingSecurityId: underlyingSecurity.id,
							date: balanceDate.format('m/d/Y')
						}
					})
				);
				const [transferType, securityTarget] = await Promise.all(promises);
				if (securityTarget) {
					const defaultData = {
						type: transferType,
						settlementDate: balanceDate.format('Y-m-d 00:00:00'),
						transactionDate: balanceDate.format('Y-m-d 00:00:00'),
						sourceSystemTable: transferType.sourceSystemTable,
						sourceFkFieldId: securityTarget.id
					};
					defaultData[transferAccountPrefix + 'ClientInvestmentAccount'] = clientAccount;
					defaultData[transferAccountPrefix + 'HoldingInvestmentAccount'] = record.holdingInvestmentAccount;
					TCG.createComponent(contribution ? 'Clifton.accounting.positiontransfer.TransferToWindow' : 'Clifton.accounting.positiontransfer.TransferFromWindow', {defaultData: defaultData});
				}
				else {
					TCG.showError('A security target could not be found for client account ' + clientAccount.name + ' and underlying security ' + underlyingSecurity.symbol, 'Security Target Not Found');
				}
			};
			actions.push(
				{
					text: 'Security Target Contribution', iconCls: 'arrow-left-green', tooltip: 'Create a contribution to our account by defining the positions being transfer into the system',
					handler: () => transferHandler(true)
				},
				{
					text: 'Security Target Distribution', iconCls: 'arrow-right-red', tooltip: 'Create a distribution from our account by selecting lot positions',
					handler: () => transferHandler(false)
				}
			);
		}

		return actions;
	},

	getDailyCashActivityAction: function(grid, rowIndex) {
		return {
			text: 'Cash Activity', iconCls: 'book-red', tooltip: 'Display cash activity for this account',
			handler: () => {
				const componentName = 'Clifton.accounting.gl.CashActivityWindow';
				const row = grid.getStore().getAt(rowIndex);
				TCG.createComponent(componentName, {
					defaultData: {
						clientInvestmentAccount: TCG.getValue('clientInvestmentAccount', row.json),
						holdingInvestmentAccount: TCG.getValue('holdingInvestmentAccount', row.json)
					},
					openerCt: this
				});
			}
		};
	},

	getTradeCoveredCallUtilizationDetailAction: function(grid, rowIndex) {
		return {
			text: 'Security Target Utilization', iconCls: 'account', tooltip: 'Display utilization for underlying security target.',
			handler: async () => {
				const row = grid.getStore().getAt(rowIndex);
				const underlyingSecurityId = row.json.investmentSecurity.underlyingSecurity.id;
				const balanceDate = TCG.getChildByName(grid.ownerGridPanel.topToolbar, 'balanceDate').getValue();

				const securityTarget = await TCG.data.getDataPromise('investmentAccountSecurityTargetForAccountAndUnderlyingSecurityId.json?', this, {
					params: {
						accountId: row.json.clientInvestmentAccount.id,
						underlyingSecurityId: underlyingSecurityId,
						date: balanceDate.format('m/d/Y')
					}
				});

				if (TCG.isNotBlank(securityTarget)) {
					TCG.createComponent('Clifton.trade.monitor.TradeCoveredCallUtilizationDetailWindow', {
						id: TCG.getComponentId('Clifton.trade.monitor.TradeCoveredCallUtilizationDetailWindow', securityTarget.id),
						params: {id: securityTarget.id},
						defaultData: {
							securityTargetId: securityTarget.id,
							balanceDate: balanceDate.format('m/d/Y')
						},
						openerCt: grid
					});
				}
			}
		};
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Investment Type', name: 'investmentTypeName', width: 90, hiddenName: 'investmentTypeId', xtype: 'toolbar-combo', loadAll: true, url: 'investmentTypeList.json', linkedFilter: 'investmentSecurity.instrument.hierarchy.investmentType.name'},
			{fieldLabel: 'Client Acct Group', name: 'clientAccountGroupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
			{fieldLabel: 'Client Acct', name: 'clientAccountName', xtype: 'toolbar-combo', url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.name', detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'toolbar-datefield', hidden: true}
		];
	},

	applyTopToolbarFilterLoadParams: function(params) {
		const preparedParams = params || {};
		const filter = TCG.getChildByName(this.getTopToolbar(), 'clientAccountGroupName');
		if (filter && TCG.isNotBlank(filter.getValue())) {
			preparedParams.clientInvestmentAccountGroupId = filter.getValue();
		}
		preparedParams.includeOpeningCommissions = this.includeOpeningCommissions;
		preparedParams.includeCollateralPositions = this.includeCollateralPositions;

		return preparedParams;
	},

	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			TCG.getChildByName(this.getTopToolbar(), 'balanceDate').setValue(new Date());
			return false; // avoid loading without client account or client account group filtering
		}

		return this.applyTopToolbarFilterLoadParams({
			transactionDate: this.getSelectedBalanceDate().format('m/d/Y'),
			requestedMaxDepth: 6,
			requestedPropertiesToExclude: 'accountingBalanceSearchForm'
		});
	},

	listeners: {
		afterrender: function() {
			if (this.defaultViewName) {
				this.setDefaultView(this.defaultViewName);
				// set default view does not reload, but call our method to update grid property state
				this.switchToViewBeforeReload(this.defaultViewName);
			}

			if (TCG.isNotBlank(this.investmentType)) {
				this.setInvestmentTypeFilter(this.investmentType);
			}

			// upon load, refresh local security cache
			this.grid.store.on('load', () => this.loadHandler());
			// upon record changes, signal event to recalculate
			this.grid.store.addListener('update', (...args) => this.updateRecordCalculatedValues(...args));

			// register subscription handling
			const subscriptionCallback = subscriptionData => this.refreshRecordData(subscriptionData);
			Clifton.marketdata.SecuritySubscriptionHandler.addCallback(subscriptionCallback);
			this.on('destroy', () => Clifton.marketdata.SecuritySubscriptionHandler.removeCallback(subscriptionCallback));
		}
	},

	setInvestmentTypeFilter: function(investmentTypeName) {
		const toolbar = this.getTopToolbar();
		const filterChangeFunction = (typeName, typeId) => {
			const investmentTypeFilter = TCG.getChildByName(toolbar, 'investmentTypeName');
			investmentTypeFilter.setValue({text: typeName, value: typeId});
			investmentTypeFilter.fireEvent('select', investmentTypeFilter, investmentTypeFilter.getValue());
		};
		if (TCG.isBlank(investmentTypeName)) {
			filterChangeFunction();
		}
		else {
			TCG.data.getDataPromiseUsingCaching('investmentTypeByName.json?name=' + investmentTypeName, this, 'investment.type.' + investmentTypeName)
				.then(investmentType => {
					if (investmentType) {
						filterChangeFunction(investmentType.name, investmentType.id);
					}
				});
		}
	},

	launchEditorWindow: function(componentName, id, defaultData) {
		if (TCG.isNotBlank(componentName) && TCG.isNotBlank(id)) {
			TCG.createComponent(componentName, {
				id: TCG.getComponentId(componentName, id),
				params: {id: id},
				defaultData: defaultData,
				openerCt: this
			});
		}
	},

	launchClientEditorWindow: function(grid, rowIndex) {
		const clientId = TCG.getValue('clientInvestmentAccount.id', grid.getStore().getAt(rowIndex).json);
		const defaultData = 'Clifton.trade.monitor.ClientHoldingsMonitorWindow' === grid.ownerGridPanel.clientInvestmentAccountEditorWindow
			? {disableAutoSubscription: grid.ownerGridPanel.disableAutoSubscription} : void 0;
		grid.ownerGridPanel.launchEditorWindow(grid.ownerGridPanel.clientInvestmentAccountEditorWindow, clientId, defaultData);
	},

	// Include security label for restriction creation windows
	additionalPropertiesToRequest: 'investmentSecurity.label|holdingInvestmentAccount.label|pending|investmentSecurity.underlyingSecurity.priceMultiplier',

	columns: [
		{
			header: 'Client Account', width: 60, dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true,
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const tradingActionItems = grid.ownerGridPanel.getActionMenuItems();
					const restrictionItems = grid.ownerGridPanel.getRestrictionActionMenuItems(rowIndex, column);
					const filterItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, column);

					const menu = new Ext.menu.Menu({
						items: [
							...tradingActionItems,
							...restrictionItems.length > 0 ? ['-', ...restrictionItems] : [],
							'-', grid.ownerGridPanel.getDailyCashActivityAction(grid, rowIndex),
							'-', grid.ownerGridPanel.getTradeCoveredCallUtilizationDetailAction(grid, rowIndex),
							...filterItems.length > 0 ? ['-', ...filterItems] : []
						]
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				},
				dblclick: function(column, grid, rowIndex, event) {
					grid.ownerGridPanel.launchClientEditorWindow(grid, rowIndex);
				}
			},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(value, metaData, record, true);
				return value;
			}
		},
		{
			header: 'Client Number', width: 20, dataIndex: 'clientInvestmentAccount.number', idDataIndex: 'clientInvestmentAccount.id', hidden: true, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'},
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const tradingActionItems = grid.ownerGridPanel.getActionMenuItems();
					const restrictionItems = grid.ownerGridPanel.getRestrictionActionMenuItems(rowIndex, column);
					const filterItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, column);

					const menu = new Ext.menu.Menu({
						items: [
							...tradingActionItems,
							...restrictionItems.length > 0 ? ['-', ...restrictionItems] : [],
							'-', grid.ownerGridPanel.getDailyCashActivityAction(grid, rowIndex),
							'-', grid.ownerGridPanel.getTradeCoveredCallUtilizationDetailAction(grid, rowIndex),
							...filterItems.length > 0 ? ['-', ...filterItems] : []
						]
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				},
				dblclick: function(column, grid, rowIndex, event) {
					grid.ownerGridPanel.launchClientEditorWindow(grid, rowIndex);
				}
			},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(record.get('clientInvestmentAccount.label'), metaData, record, true);
				return value;
			}
		},
		{
			header: 'Client Name', width: 40, dataIndex: 'clientInvestmentAccount.name', idDataIndex: 'clientInvestmentAccount.id', hidden: true, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', showNotEquals: true},
			viewNames: ['Options', 'Market Value'],
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const tradingActionItems = grid.ownerGridPanel.getActionMenuItems();
					const restrictionItems = grid.ownerGridPanel.getRestrictionActionMenuItems(rowIndex, column);
					const filterItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, column);

					const menu = new Ext.menu.Menu({
						items: [
							...tradingActionItems,
							...restrictionItems.length > 0 ? ['-', ...restrictionItems] : [],
							'-', grid.ownerGridPanel.getDailyCashActivityAction(grid, rowIndex),
							'-', grid.ownerGridPanel.getTradeCoveredCallUtilizationDetailAction(grid, rowIndex),
							...filterItems.length > 0 ? ['-', ...filterItems] : []
						]
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				},
				dblclick: function(column, grid, rowIndex, event) {
					grid.ownerGridPanel.launchClientEditorWindow(grid, rowIndex);
				}
			},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(record.get('clientInvestmentAccount.label'), metaData, record, true);
				return value;
			}
		},
		{
			header: 'Processing Type', width: 40, dataIndex: 'clientInvestmentAccount.serviceProcessingType.name', hidden: true, idDataIndex: 'clientInvestmentAccount.serviceProcessingType.id', filter: false,
			eventListeners: {
				dblclick: function(column, grid, rowIndex, event) {
					const id = TCG.getValue('clientInvestmentAccount.serviceProcessingType.id', grid.getStore().getAt(rowIndex).json);
					grid.ownerGridPanel.launchEditorWindow('Clifton.business.service.ProcessingTypeWindow', id);
				}
			}
		},
		{
			header: 'Holding Account', width: 25, dataIndex: 'holdingInvestmentAccount.number', idDataIndex: 'holdingInvestmentAccount.id', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', showNotEquals: true},
			viewNames: ['Options', 'Market Value'], renderer: TCG.renderValueWithTooltip,
			eventListeners: {
				dblclick: function(column, grid, rowIndex, event) {
					const id = TCG.getValue('holdingInvestmentAccount.id', grid.getStore().getAt(rowIndex).json);
					grid.ownerGridPanel.launchEditorWindow('Clifton.investment.account.AccountWindow', id);
				}
			}
		},
		{
			header: 'Collateral', width: 10, dataIndex: 'accountingBalance.accountingAccount.collateral', type: 'boolean', hidden: true,
			renderer: TCG.renderBoolean
		},
		{header: 'Inception Date', width: 15, dataIndex: 'clientInvestmentAccount.inceptionDate', hidden: true, nonPersistentField: true, viewNames: ['Market Value']},
		{header: 'Cash', width: 20, dataIndex: 'cashBalance', type: 'currency', nonPersistentField: true, hidden: true},
		{header: 'Cash Collateral', width: 20, dataIndex: 'cashCollateralBalance', type: 'currency', nonPersistentField: true, hidden: true, useNull: true},
		{header: 'Directed', width: 15, dataIndex: 'holdingInvestmentAccount.type.executionWithIssuerRequired', type: 'boolean', hidden: true, nonPersistentField: true, tooltip: 'This position is held with a directed broker holding account and is configured to execute trades with the issuing company only.'},
		{
			header: 'Investment Hierarchy', width: 60, dataIndex: 'investmentSecurity.instrument.hierarchy.labelExpanded', hidden: true, idDataIndex: 'investmentSecurity.instrument.hierarchy.id', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600},
			eventListeners: {
				dblclick: function(column, grid, rowIndex, event) {
					const id = TCG.getValue('investmentSecurity.instrument.hierarchy.id', grid.getStore().getAt(rowIndex).json);
					grid.ownerGridPanel.launchEditorWindow('Clifton.investment.setup.InstrumentHierarchyWindow', id);
				}
			}
		},
		{
			header: 'Investment Instrument', width: 60, dataIndex: 'investmentSecurity.instrument.name', hidden: true, idDataIndex: 'investmentSecurity.instrument.id', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json', listWidth: 600},
			eventListeners: {
				dblclick: function(column, grid, rowIndex, event) {
					const id = TCG.getValue('investmentSecurity.instrument.id', grid.getStore().getAt(rowIndex).json);
					grid.ownerGridPanel.launchEditorWindow('Clifton.investment.instrument.InstrumentWindow', id);
				}
			}
		},
		{header: 'Investment Type', width: 40, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentType.name', idDataIndex: 'investmentSecurity.instrument.hierarchy.investmentType.id', hidden: true, filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'label', loadAll: true, url: 'investmentTypeList.json'}},
		{header: 'CCY Denomination', width: 30, dataIndex: 'investmentSecurity.instrument.tradingCurrency.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', showNotEquals: true, displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
		{
			header: 'Underlying Security', width: 25, dataIndex: 'investmentSecurity.underlyingSecurity.symbol', idDataIndex: 'investmentSecurity.underlyingSecurity.id', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'symbol', comboDisplayField: 'label', url: 'investmentSecurityListFind.json', showNotEquals: true},
			viewNames: ['Options'],
			eventListeners: {
				dblclick: function(column, grid, rowIndex, event) {
					const id = TCG.getValue('investmentSecurity.underlyingSecurity.id', grid.getStore().getAt(rowIndex).json);
					grid.ownerGridPanel.launchEditorWindow('Clifton.investment.instrument.SecurityWindow', id);
				}
			},
			renderer: function(value, metaData, record) {
				// Attach style for restrictions involving the underlying security
				this.scope.gridPanel.applyTradeRestrictionGridSecurityColumnFreeze(metaData, record, true);
				return value;
			}
		},
		{
			header: 'Security', width: 40, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json?active=true', showNotEquals: true},
			eventListeners: {
				contextmenu: async function(column, grid, rowIndex, event) {
					const record = grid.store.getAt(rowIndex);
					const filterItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, column);
					const menu = new Ext.menu.Menu({
						items: [
							{
								text: 'Open Detail Window', iconCls: 'info',
								handler: () => column.createDetailClass(grid, record.get(column.idDataIndex), false)
							},
							{
								text: 'Add New Item', iconCls: 'add',
								handler: () => column.createDetailClass(grid, record.get(column.idDataIndex), true)
							},
							...filterItems.length > 0 ? ['-', ...filterItems] : []
						]
					});

					event.preventDefault();
					menu.showAt(event.getXY());
				},
				dblclick: function(column, grid, rowIndex, event) {
					const id = TCG.getValue('investmentSecurity.id', grid.getStore().getAt(rowIndex).json);
					grid.ownerGridPanel.launchEditorWindow('Clifton.investment.instrument.SecurityWindow', id);
				}
			},
			renderer: function(value, metaData, record) {
				this.scope.gridPanel.applyTradeRestrictionGridSecurityColumnFreeze(metaData, record, false);
				return TCG.renderValueWithTooltip(value, metaData);
			},
			createDetailClass: async function(grid, id, newInstance) {
				const className = newInstance ? 'Clifton.investment.instrument.copy.SecurityCopyWindow' : 'Clifton.investment.instrument.SecurityWindow';
				const security = await newInstance ? TCG.data.getDataPromise('investmentSecurity.json', grid, {params: {id: id}}) : null;
				const defaultData = security ? {instrument: security.instrument, securityToCopy: {id: security.id}} : null;
				const params = newInstance ? null : {id: id};

				TCG.createComponent(className, {
					modal: !!newInstance,
					id: TCG.getComponentId(className, id),
					defaultData: defaultData,
					params: params,
					openerCt: grid.ownerGridPanel,
					defaultIconCls: grid.ownerGridPanel.getWindow().iconCls
				});
			}
		},
		{header: 'Price Multiplier', width: 20, dataIndex: 'investmentSecurity.priceMultiplier', type: 'float', nonPersistentField: true, hidden: true},
		{header: 'Call', width: 15, dataIndex: 'call', type: 'boolean', hidden: true, nonPersistentField: true, tooltip: 'If the security is a Call Option, this value will be checked. If the security is not an option, or is a Put Option the value will be unchecked'},
		{header: 'Option Type', width: 15, dataIndex: 'optionType', hidden: true, nonPersistentField: true, tooltip: 'If the security is an Option, the value will define the Option\'s type (e.g. Put or Call'},
		{header: 'Option Style', width: 15, dataIndex: 'optionStyle', hidden: true, nonPersistentField: true, tooltip: 'If the security is an Option, the value will define the Option\'s style (e.g. American or European'},
		{header: 'Underlying Price', width: 20, dataIndex: 'underlyingPrice', type: 'currency', useNull: true, nonPersistentField: true, viewNames: ['Options']},
		{header: 'Strike', width: 20, dataIndex: 'strikePrice', type: 'float', useNull: true, nonPersistentField: true, hidden: true},
		{
			header: 'Quantity Actual', width: 20, dataIndex: 'actualQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true, hidden: true,
			tooltip: 'The position quantity excluding pending activity. See the Quantity Adjusted column for net quantity. Light orange highlighting indicates pending activity',
			renderer: function(value, metadata, record) {
				if (TCG.isTrue(TCG.getValue('pending', record.json))) {
					metadata.css += ' x-grid3-row-highlight-light-orange';
				}
				return TCG.renderAmount(value, true, '0,000.00');
			}
		},
		{
			header: 'Quantity Pending', width: 20, dataIndex: 'pendingQuantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true, hidden: true,
			tooltip: 'The position applicable pending quantity. See the Quantity Adjusted column for net quantity. Light orange highlighting indicates pending activity',
			renderer: function(value, metadata, record) {
				if (TCG.isTrue(TCG.getValue('pending', record.json))) {
					metadata.css += ' x-grid3-row-highlight-light-orange';
				}
				return TCG.renderAmount(value, true, '0,000.00');
			}
		},
		{
			header: 'Quantity', width: 20, dataIndex: 'quantity', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true,
			tooltip: 'Light orange highlighting indicates pending activity',
			renderer: function(value, metadata, record) {
				const pending = TCG.isTrue(TCG.getValue('pending', record.json));
				if (pending) {
					metadata.css += ' x-grid3-row-highlight-light-orange';
				}
				return TCG.renderAdjustedAmount(value, pending, record.get('actualQuantity'), '0,000.00', 'Pending Quantity', true);
			}
		},
		{
			header: 'Price', width: 20, dataIndex: 'price', type: 'currency', useNull: true, nonPersistentField: true, hidden: true,
			tooltip: 'The security price. On initial load the value is the previous day close. Once subscriptions are active, it is the last sale price.'
		},
		{
			header: 'Ask', width: 20, dataIndex: 'investmentSecurity.ASK', type: 'currency', useNull: true, nonPersistentField: true, hidden: true, viewNames: ['Options', 'Market Value'],
			tooltip: 'The real-time security ASK price; the lowest price a dealer will accept to sell a security'
		},
		{
			header: 'Bid', width: 20, dataIndex: 'investmentSecurity.BID', type: 'currency', useNull: true, nonPersistentField: true, hidden: true,
			tooltip: 'The real-time security BID price; the highest price an investor will accept to pay for a security'
		},
		{
			header: 'Total Value', width: 25, dataIndex: 'totalValue', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true,
			tooltip: 'The total market value of this position (price * quantity)'
		},
		{
			header: 'Net Cost Actual', width: 25, dataIndex: 'actualNetCostBasis', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: false, hidden: true, nonPersistentField: true,
			tooltip: 'The net premium paid to hold this position for this security and client account excluding pending activity. The value is gross if commission are not included.'
		},
		{
			header: 'Net Cost', width: 25, dataIndex: 'netCostBasis', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: false, hidden: true, nonPersistentField: true,
			tooltip: 'The net premium paid to hold this position for this security and client account excluding pending activity. The value is gross if commission are not included.',
			renderer: function(value, metadata, record) {
				const pending = TCG.isTrue(TCG.getValue('pending', record.json));
				if (pending) {
					metadata.css += ' x-grid3-row-highlight-light-orange';
				}
				return TCG.renderAdjustedAmount(value, pending, TCG.getValue('actualNetCostBasis', record.json), '0,000.00', 'Pending Cost', true);
			}
		},
		{
			header: 'Cost Basis', width: 25, dataIndex: 'localCostBasis', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: false, hidden: true, nonPersistentField: true,
			tooltip: 'The local cost basis of opening the position for this security and client account'
		},
		{
			header: 'Cost Basis Actual', width: 25, dataIndex: 'actualLocalCostBasis', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: false, hidden: true, nonPersistentField: true,
			tooltip: 'The local cost basis excluding pending activity for the position for this security and client account'
		},
		{
			header: 'Opening Commission', width: 25, dataIndex: 'openingLocalCommission', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: false, hidden: true, nonPersistentField: true,
			tooltip: 'The commission applicable to opening the position for this security and client account'
		},
		{
			header: 'Cost/Share Actual', width: 20, dataIndex: 'actualCostPerShare', type: 'currency', negativeInRed: true, positiveInGreen: false, useNull: true, nonPersistentField: true, hidden: true,
			tooltip: 'The per share cost for this position (total cost / quantity)'
		},
		{
			header: 'Cost/Share', width: 20, dataIndex: 'costPerShare', type: 'currency', negativeInRed: true, positiveInGreen: false, useNull: true, nonPersistentField: true, hidden: true,
			viewNames: ['Options'],
			tooltip: 'The per share cost for this position (total cost / quantity)',
			renderer: function(value, metadata, record) {
				const pending = TCG.isTrue(TCG.getValue('pending', record.json));
				if (pending) {
					metadata.css += ' x-grid3-row-highlight-light-orange';
				}
				return TCG.renderAdjustedAmount(value, pending, TCG.getValue('actualCostPerShare', record.json), '0,000.00', 'Pending Adjustment', true);
			}
		},
		{
			header: 'P&L/Share', width: 20, dataIndex: 'profitAndLossPerShare', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true,
			viewNames: ['Options'],
			tooltip: 'Profit and loss per share of this position ((long position) price - cost per share; (short position) cost per share - price)'
		},
		{
			header: 'P&L Total', width: 25, dataIndex: 'profitAndLoss', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true,
			viewNames: ['Options'],
			tooltip: 'Total profit and loss for this position (per share PnL * quantity)'
		},
		{
			header: '% Realized', width: 25, dataIndex: 'percentRealized', type: 'percent', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true,
			tooltip: 'Percent by which this position has realized profit for its cost (per share PnL / per share cost)'
		},
		{
			header: 'ITM', width: 15, dataIndex: 'moneyness', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true, nonPersistentField: true,
			viewNames: ['Market Value'],
			tooltip: 'The amount this position is \'In The Money\' (Intrinsic Value) ((call) strike price - underlying price; (put) underlying price - strike price); value ITM will be green, OTM will be read',
			renderer: (value, metadata, record) => (TCG.isNull(value) || value <= 0) ? void 0 : TCG.renderAmount(value, true, '0,000.00')
		},
		{
			header: '% Extrinsic', width: 25, dataIndex: 'percentExtrinsic', type: 'percent', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true,
			viewNames: ['Options'],
			tooltip: 'Percent of which portion of this position is assigned to external factors ((price - ITM) / underlying price)',
			renderer: (value, metadata, record) => {
				let renderValue = value;
				if (renderValue > 100) {
					renderValue = 100;
				}
				else if (renderValue < 0) {
					renderValue = 0;
				}
				return TCG.renderAmount(renderValue, true, '0,000.00 %');
			}
		},
		{
			header: '% OTM', width: 20, dataIndex: 'percentInTheMoney', type: 'percent', useNull: true, negativeInRed: true, positiveInGreen: true, nonPersistentField: true,
			viewNames: ['Options'],
			tooltip: 'Percent this position is \'Out of the Money\' (ITM / underlying price); values ITM will be red, OTM will be green',
			// convert % ITM to % OTM
			renderer: (value, metadata, record) => TCG.renderAmount(value ? value * -1 : 0, true, '0,000.00 %')
		},
		{
			header: 'Days', width: 18, dataIndex: 'daysToExpiration', type: 'float', useNull: true, nonPersistentField: true,
			tooltip: 'Days until expiration or maturity of this security in tenths of a day',
			renderer: (value, metadata, record) => Ext.util.Format.number(value, '0,000.0')
		},
		{
			header: 'Delta', width: 20, dataIndex: 'delta', type: 'float', useNull: true, nonPersistentField: true,
			viewNames: ['Options', 'Market Value'],
			renderer: (value, metadata, record) => value ? TCG.renderValueWithTooltip(TCG.numberFormat(value * 100, '0,000.00 %'), metadata) : value
		},
		{
			header: 'Volatility', width: 20, dataIndex: 'investmentSecurity.IVOL_MID_RT', type: 'currency', useNull: true, nonPersistentField: true, hidden: true,
			tooltip: 'The real-time security volatility'
		},
		{
			header: 'Position Open Date', width: 20, dataIndex: 'positionOpenDate', type: 'date', useNull: true, nonPersistentField: true, skipFilterOnLoad: true, hidden: true,
			tooltip: 'The original date of the open position'
		},
		{
			header: 'Div. Ex Dt.', width: 20, dataIndex: 'investmentSecurity.DVD_EX_DT', type: 'date', useNull: true, nonPersistentField: true, skipFilterOnLoad: true, hidden: true, viewNames: ['Options'],
			tooltip: 'Applicable to Option position. The first date the underlying stock trades without entitlement to the current dividend. The next Ex Date may be announced or projected. Used for the Dividend Exercise Risk rule'
		},
		{
			header: 'Dividend', width: 20, dataIndex: 'investmentSecurity.OPT_DIVIDEND', type: 'currency', useNull: true, nonPersistentField: true, skipFilterOnLoad: true, hidden: true,
			tooltip: 'Applicable to Option positions. The dividend expected for the underlying stock for an option security. This is equal to the dividend flow over the remaining life of the option. The dividend value may included projected values. Used for the Dividend Exercise Risk rule'
		},
		{header: 'Expiration', width: 25, dataIndex: 'investmentSecurity.lastDeliveryDate', type: 'date', hidden: true, tooltip: 'Date the security for this position expires'},
		// Rules
		{
			header: 'Buyback', width: 15, dataIndex: 'buyBack', nonPersistentField: true,
			viewNames: ['Options'],
			tooltip: 'Indicates one or more buyback rules have been triggered. Dividend Exercise Risk (code D, red indicator, (all option processing types)), 10/10/80 (code T, yellow indicator, processing types: (Call Writing, Call Selling)), or Linear Decay 90 (code B, orange indicator, processing types: (Structured Options, Defensive Equity, Fully Funded)). Multiple buyback rules triggered: (multiple codes, red indicator).',
			renderer: (value, metadata, record) => {
				let qtip = '';
				let ruleCounter = 0;
				let rowHighlightColorCss = '';
				const quantity = record.get('quantity');
				const bidAsk = (quantity > 0) ? record.get('investmentSecurity.BID') : record.get('investmentSecurity.ASK');
				const bidAskPriceLabel = (quantity > 0) ? 'Bid Price' : 'Ask Price';
				const buybackCodes = [];

				if (TCG.isBlank(bidAsk)) {
					metadata.css += ' x-grid3-row-highlight-light-gray';
					qtip = 'Flag is inaccurate due to missing BID/ASK price.<br/></br>';
				}

				if (TCG.isTrue(record.get('dividendExerciseRisk'))) {
					++ruleCounter;
					rowHighlightColorCss = ' x-grid3-row-highlight-light-red';
					const dividend = record.get('investmentSecurity.OPT_DIVIDEND');
					let approximatePutValue = record.get('approximatePutValue');
					// round to 5 digits
					approximatePutValue = TCG.roundToDecimalDigits(approximatePutValue, 5);
					qtip += '<b>Dividend Exercise Risk</b> - Option\'s price is at risk of becoming less than the dividend yield.<br/>';
					qtip += '<br/>Approximate Put Value: ' + approximatePutValue;
					qtip += '<br/>Quantity: ' + quantity;
					qtip += '<br/>Dividend: ' + dividend;
					qtip += '<br/><br/>Quantity < 0 AND Dividend > 0 AND Approximate Put Value < (0.75 * Dividend)';
					qtip += '<br/>' + quantity + ' < 0 AND ' + dividend + ' > 0 AND ' + approximatePutValue + ' < ' + 0.75 * dividend;
					qtip += '<br/><br/>';
					buybackCodes.push('D');
				}

				if (TCG.isTrue(record.get('tenTenEighty'))) {
					++ruleCounter;
					rowHighlightColorCss = ' x-grid3-row-highlight-light-yellow';
					let percentRealized = record.get('percentRealized');
					// round to 5 digits
					percentRealized = TCG.roundToDecimalDigits(percentRealized, 5);
					const daysToExpiry = record.get('daysToExpiration');
					const delta = record.get('delta');

					qtip += '<b>10/10/80</b> - Option position should be bought to close OR rolled into new option position to harvest gains and/or reduce risk.<br/>';
					qtip += '<br/>' + bidAskPriceLabel + ': ' + bidAsk;
					qtip += '<br/>Quantity: ' + quantity;
					qtip += '<br/>Percent Realized: ' + percentRealized;
					qtip += '<br/>Delta: ' + delta;
					qtip += '<br/><br/>Quantity < 0 AND (Percent Realized > 80 OR Percent Realized < -250 OR Days to Expiry < 10 OR Delta >= -0.1)';
					qtip += '<br/>' + quantity + ' < 0 AND (' + percentRealized + ' > 80 OR ' + percentRealized + ' < -250 OR ' + daysToExpiry + ' < 10 OR ' + delta + ' >= -0.1)';
					qtip += '<br/><br/>';
					buybackCodes.push('T');
				}

				if (TCG.isTrue(record.get('linearDecay90'))) {
					++ruleCounter;
					rowHighlightColorCss = ' x-grid3-row-highlight-light-orange';
					const ld90BuyBackPrice = record.get('ld90BuyBackPrice');
					const ld90BidAskPrice = record.get('ld90BidAskPrice');
					qtip += '<b>Linear Decay (LD90)</b> - Option\'s value has depreciated by over 90%, close this position.<br/>';
					qtip += '<br/>' + bidAskPriceLabel + ': ' + ld90BidAskPrice +
						'<br/>LD90 Buy Back Price: ' + ld90BuyBackPrice +
						'<br/><br/>' + bidAskPriceLabel + ' <= 2 * LD90 Buy Back Price' +
						'<br/>' + ld90BidAskPrice + ' <= 2 * ' + ld90BuyBackPrice +
						'<br/>' + ld90BidAskPrice + ' <= ' + 2 * ld90BuyBackPrice;
					buybackCodes.push('B');
				}

				if (TCG.isNotBlank(bidAsk)) {
					if (ruleCounter === 1) {
						metadata.css += rowHighlightColorCss;
					}
					else if (ruleCounter > 1) {
						// red for multiple buyback codes
						metadata.css += ' x-grid3-row-highlight-light-red';
					}
				}

				value = '<div align="center"><b>' + buybackCodes.join(', ') + '<b/></div>';
				metadata.attr = 'qtip="' + Ext.util.Format.htmlEncode(qtip) + '"';
				return value;
			}
		},
		{header: 'Executing Broker', width: 50, dataIndex: 'holdingInvestmentAccount.issuingCompany.label', idDataIndex: 'holdingInvestmentAccount.issuingCompany.id', hidden: true, filter: {type: 'combo', searchFieldName: 'holdingInvestmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}}
	],

	loadHandler: function(skipMarketDataLookup) {
		const active = this.subscriptionsActive;

		// update security mapping
		const securityIdToRowsMap = {};
		const americanCallOptionSecurityIds = {}; // use object to avoid duplicates
		const dvdExDateUnderlyingSecurityIds = {}; // use object to avoid duplicates
		const visibleRows = this.grid.view.getVisibleRows(); // buffered view visible rows; used to commit/render only visible rows

		this.grid.store.each(record => {
			const security = TCG.getValue('investmentSecurity', record.json);
			const recordId = record.id;
			const recordIndex = this.grid.store.indexOf(record);
			const recordVisible = TCG.isNumber(recordIndex) && recordIndex >= visibleRows.first && recordIndex <= visibleRows.last;

			if (TCG.isNull(securityIdToRowsMap[security.id])) {
				securityIdToRowsMap[security.id] = [];
			}
			securityIdToRowsMap[security.id].push(recordId);
			if (security.underlyingSecurity) {
				if (TCG.isNull(securityIdToRowsMap[security.underlyingSecurity.id])) {
					securityIdToRowsMap[security.underlyingSecurity.id] = [];
				}
				securityIdToRowsMap[security.underlyingSecurity.id].push(recordId);
			}
			// update calculated values
			this.updateRecordCalculatedValues(this.grid.store, record, 'load', TCG.isFalse(recordVisible));
			// update option security array
			if (this.isAmericanCallOptionSecurityPosition(record)) {
				americanCallOptionSecurityIds[security.id] = void 0;
				dvdExDateUnderlyingSecurityIds[security.underlyingSecurity.id] = void 0;
			}
			else if (this.isEuropeanFlexOptionSecurityPosition(record)) {
				dvdExDateUnderlyingSecurityIds[security.underlyingSecurity.id] = void 0;
			}
		});

		this.americanCallOptionSecurityIds = Object.keys(americanCallOptionSecurityIds).map(Number); // create int array from string keys
		this.dvdExDateUnderlyingSecurityIds = Object.keys(dvdExDateUnderlyingSecurityIds).map(Number); // create int array from string keys

		let subscriptionSyncFunction;
		if (active) {
			// if subscriptions are active, unsubscribe to no longer viewed securities and subscribe to newly viewed securities before updating the local cache
			const subscribedToSecurityIds = Object.keys(this.securityIdToRowsMap);
			const newlyViewedSecurityIds = Object.keys(securityIdToRowsMap);
			// Wait a few seconds to resubscribe so the previous unsubscribe is fully processed by Bloomberg.
			// Subscription requests occurring before complete on BPipe can cause requests to be ignored or jam up the connection.
			subscriptionSyncFunction = async () => {
				// Unsubscribe to previously viewed position securities and subscribe to those now viewed.
				// The same security may be in both listings, but needs to be resubscribed to get accurate values.
				// Keeping the same subscription open results in updates only when a value changes and the data may not update frequently.
				await this.unsubscribeToSecurityIdList(subscribedToSecurityIds);
				if (newlyViewedSecurityIds.length === 0 && newlyViewedSecurityIds.length > 0) {
					// the unsubscribe disabled and no new subscriptions enabled so enable subscriptions
					this.enableSubscriptions();
				}
				this.subscribeToSecurityIdList(newlyViewedSecurityIds)
					.then(() => {
						if (!TCG.isTrue(skipMarketDataLookup)) {
							// look up and populate dividend values for positions/rows applicable to the Dividend Exercise Risk rule
							this.fetchDividendMarketData();
						}
					});
			};
		}
		else {
			// automatically subscribe to viewed securities on load if not active
			subscriptionSyncFunction = this.disableAutoSubscription ? Ext.emptyFn
				: () => this.subscribeHandler()
					.then(() => {
						if (!TCG.isTrue(skipMarketDataLookup)) {
							// look up and populate dividend values for positions/rows applicable to the Dividend Exercise Risk rule
							this.fetchDividendMarketData();
						}
					});
		}

		// update local security cache
		this.securityIdToRowsMap = securityIdToRowsMap;
		subscriptionSyncFunction();
		// notify records updated for optional additional processing
		this.recordsUpdated();
	},

	subscribeHandler: async function() {
		const securityIdList = Object.keys(this.securityIdToRowsMap);
		this.subscribeToSecurityIdList(securityIdList);
	},

	subscribeToSecurityIdList: async function(securityIdList) {
		if (securityIdList.length > 0) {
			// Initiate subscriptions
			this.enableSubscriptions();
			try {
				await Clifton.marketdata.SecuritySubscriptionHandler.subscribe(securityIdList, this);
			}
			catch (err) {
				// Subscription command failed
				this.disableSubscriptions();
				throw new Error('An error occurred while enabling subscriptions: ' + err.message);
			}
		}
	},

	fetchDividendMarketData: function() {
		const formPanel = this;
		if (formPanel.subscriptionsActive && formPanel.applyTradeRules) {
			// the two dividend (dividend and Ex Date values are looked up separately because they pertain to different sets of data
			// ex date can apply to all stock options, dividends pertains to only american call options
			Promise.all([
				formPanel.fetchDividendMarketDataFieldValues(this.americanCallOptionSecurityIds, 'Dividend Projection', 'investmentSecurity.OPT_DIVIDEND'),
				formPanel.fetchDividendMarketDataFieldValues(this.dvdExDateUnderlyingSecurityIds, 'Dividend Ex Date Projection', 'investmentSecurity.DVD_EX_DT')
			]).then(function(updatedRecordsArrays) {
				// object to track unique updated records without duplicates
				const updatedRecords = {};
				const appendUpdatedRecords = records => {
					const uRecords = records || [];
					for (const record of uRecords) {
						updatedRecords[record.id] = record;
					}
				};
				for (const updatedRecordArray of updatedRecordsArrays) {
					appendUpdatedRecords(updatedRecordArray);
				}

				// end edit for all modified grid rows so calculations can be performed
				const updatedRecordArray = Object.values(updatedRecords);
				if (updatedRecordArray.length > 0) {
					formPanel.applyEditedRecords(updatedRecordArray);

					// apply the filtering for dividend related columns after the data has been applied to the grid
					const gridPlugins = formPanel.grid.plugins;
					const gridFilterPlugin = gridPlugins && Array.isArray(gridPlugins) ? formPanel.grid.plugins.filter(item => item instanceof Ext.ux.grid.GridFilters) : void 0;
					if (gridFilterPlugin && gridFilterPlugin.length === 1) {
						const dataUpdate = gridFilterPlugin[0].applyNonPersistentFieldFilters(formPanel.grid.store, false);
						if (TCG.isTrue(dataUpdate)) {
							formPanel.loadHandler(true);
						}
					}
				}
			});
		}
	},

	/**
	 * Fetches the provided dividend field values and applies them to applicable viewed rows, quietly updates necessary record values, and returns an array of records that should be committed.
	 * @returns {Promise<Array>}
	 */
	fetchDividendMarketDataFieldValues: async function(securityIds, fieldName, columnName) {
		const securityIdToRowsMap = this.securityIdToRowsMap;
		const grid = this.grid;
		const cacheDurationSeconds = (8 * 60 * 60); // cache for 8 hours
		// array to track updated records
		const updatedRecords = [];

		try {
			if (securityIds && securityIds.length > 0) {
				const dvdValues = await Clifton.marketdata.field.getSecurityLiveMarketDataValue(securityIds, fieldName, {maxAgeInSeconds: cacheDurationSeconds, providerOverride: 'BPIPE', ignoreErrors: true});
				for (const securityId of securityIds) {
					const rowIds = securityIdToRowsMap[securityId];
					let dvdValue = null;
					for (const dvdFieldValue of dvdValues) {
						if (dvdFieldValue.securityId === securityId) {
							dvdValue = dvdFieldValue.value;
							break;
						}
					}
					if (TCG.isNotBlank(dvdValue)) {
						// set the dividend yield value for each applicable row
						for (const optionRowId of rowIds) {
							const record = grid.store.getById(optionRowId);
							record.beginEdit();
							record.set(columnName, fieldName.includes('Date') ? new Date(dvdValue) : dvdValue);
							updatedRecords.push(record);
						}
					}
				}
			}
		}
		catch (e) {
			console.warn(e ? e : 'Failed to retrieve dividend data.');
		}
		return updatedRecords;
	},

	unsubscribeHandler: function(securityIds) {
		const securityIdList = Object.keys(this.securityIdToRowsMap);
		this.unsubscribeToSecurityIdList(securityIdList);
	},

	unsubscribeToSecurityIdList: async function(securityIdList) {
		if (securityIdList.length === 0) {
			return;
		}

		// Remove all subscriptions
		this.disableSubscriptions();
		await Clifton.marketdata.SecuritySubscriptionHandler.unsubscribe(securityIdList, this);
	},

	enableSubscriptions: function() {
		// Update UI elements
		this.subscriptionsActive = true;
		const actionsMenu = TCG.getChildByName(this.getTopToolbar(), 'actionsMenu');
		if (actionsMenu) {
			const subscriptionButton = TCG.getChildByName(actionsMenu.menu, 'subscriptionButton');
			if (subscriptionButton) {
				subscriptionButton.setText('Unsubscribe');
				subscriptionButton.setHandler(this.unsubscribeHandler, this);
			}
		}
	},

	disableSubscriptions: function() {
		// Update UI elements
		this.subscriptionsActive = false;
		const actionsMenu = TCG.getChildByName(this.getTopToolbar(), 'actionsMenu');
		if (actionsMenu) {
			const subscriptionButton = TCG.getChildByName(actionsMenu.menu, 'subscriptionButton');
			if (subscriptionButton) {
				subscriptionButton.setText('Subscribe');
				subscriptionButton.setHandler(this.subscribeHandler, this);
			}
		}
	},

	forceUnsubscribeHandler: async function() {
		try {
			await Clifton.marketdata.SecuritySubscriptionHandler.forceUnsubscribeAll(this);
		}
		catch (err) {
			// Subscription command failed
			throw new Error('An error occurred while disabling subscriptions: ' + err.message);
		}
		finally {
			this.unsubscribeHandler();
		}
	},

	refreshRecordData: function(securityDataMap) {
		if (!securityDataMap) {
			return;
		}

		/*
		 * Security data map comes back as:
		 * {
		 *   securityDataValueMap: {
		 *     security1 ID: [{name: field1, value: field1Value}, {name: field2, value: field2Value} ...],
		 *     security1 ID: [{name: field1, value: field1Value}, {name: field2, value: field2Value}]
		 *   }
		 * }
		 */
		const securityDataIds = Object.keys(securityDataMap).map(idStr => parseInt(idStr));
		const store = this.grid.store;

		// Iterate subscription data updates from server by security ID and apply changes to grid rows
		const updatedRecords = [];
		for (const securityId of securityDataIds) {
			const fieldDataList = securityDataMap[securityId];
			const recordIds = this.securityIdToRowsMap[securityId];
			// apply security data updates to grid rows
			for (const recordId of (recordIds || [])) {
				const record = store.getById(recordId);
				if (record) {
					const security = TCG.getValue('investmentSecurity', record.json);
					let securityPrefix = null;
					if (security.id === securityId) {
						securityPrefix = 'investmentSecurity.';
					}
					else if (security.underlyingSecurity && security.underlyingSecurity.id === securityId) {
						securityPrefix = 'investmentSecurity.underlyingSecurity.';
					}
					if (securityPrefix) {
						record.beginEdit();
						for (const fieldData of fieldDataList) {
							record.set(securityPrefix + fieldData['fieldName'], fieldData['value']);
						}
						updatedRecords.push(record);
					}
				}
			}
		}

		// end edit for all modified grid rows so calculations can be performed
		this.applyEditedRecords(updatedRecords);
		// notify records updated for optional additional processing
		this.recordsUpdated();
	},

	/**
	 * Applies the updates to the provided records while updating local calculated values. Each record updated will be conditionally committed based on
	 * modified fields and current visibility.
	 * @param updatedRecords an array of records that were edited
	 */
	applyEditedRecords: function(updatedRecords) {
		if (updatedRecords && updatedRecords.length > 0) {
			const store = this.grid.store;
			const visibleRows = this.grid.view.getVisibleRows();
			updatedRecords.forEach(record => {
				const recordIndex = store.indexOf(record);
				const recordIsVisible = recordIndex >= visibleRows.first && recordIndex <= visibleRows.last;
				this.updateRecordCalculatedValues(store, record, 'edit', !recordIsVisible);
			});
		}
	},

	recordsUpdated: function() {
		// optional hook that can be used to update totals upon record changes
	},

	/**
	 * Recalculates a record's values upon store update events or custom requests. The option argument value will be ('commit', 'edit')
	 * for the store's update event, or 'load' for loading of new data. Updates will be applied only if the option argument is 'edit' or 'load'
	 */
	updateRecordCalculatedValues: function(store, record, option, commitSilently) {
		if (option !== 'edit' && option !== 'load') {
			return;
		}

		const gridPanel = this;
		// Get shared data for processing
		const quantity = record.get('quantity');
		const priceUpdated = record.isModified('investmentSecurity.LAST_PRICE') || record.isModified('investmentSecurity.BID') || record.isModified('investmentSecurity.ASK');
		const priceMultiplier = record.get('investmentSecurity.priceMultiplier');
		// get Price: use last price if updated, otherwise use BID if long position, ASK if short position, and existing price if no BID/ASK
		const price = record.isModified('investmentSecurity.LAST_PRICE') ? record.get('investmentSecurity.LAST_PRICE') : record.get('price');
		const bidAskPrice = ((quantity > 0) ? record.get('investmentSecurity.BID') : record.get('investmentSecurity.ASK')) || price;
		const underlyingPriceUpdated = record.isModified('investmentSecurity.underlyingSecurity.LAST_PRICE');
		const underlyingPrice = underlyingPriceUpdated ? record.get('investmentSecurity.underlyingSecurity.LAST_PRICE') : record.get('underlyingPrice');
		const call = record.get('call');
		const itm = call ? underlyingPrice - record.get('strikePrice') : record.get('strikePrice') - underlyingPrice;
		const itmOrZero = Math.max(0, itm);

		// Perform record updates
		record.beginEdit();
		if (priceUpdated) {
			processPriceChange();
		}
		if (underlyingPriceUpdated) {
			processUnderlyingPriceChange();
		}
		if (this.isOptionSecurityPosition(record)) {
			processOptionSecurityPosition();
		}

		// Commit updates
		if (record.dirty) {
			record.changeList = Object.keys(record.modified);
			record.commit(TCG.isTrue(commitSilently));
		}
		else {
			record.cancelEdit();
		}

		// Record modification functions
		function processPriceChange() {
			record.set('price', price);
			record.set('totalValue', (bidAskPrice * priceMultiplier) * quantity);
			// Update pending cost basis if applicable. The pending cost basis is calculated using previous day close price if the security is not new.
			// If the security is new, there is no price in the system and the pending cost basis is likely not populated.
			if (TCG.isTrue(TCG.getValue('pending', record.json))) {
				const pendingQuantity = TCG.getValue('pendingQuantity', record.json);
				if (TCG.isBlank(pendingQuantity) || pendingQuantity === 0) {
					// New position that is pending
					const pendingPrice = (pendingQuantity < 0 ? record.get('investmentSecurity.BID') : record.get('investmentSecurity.ASK')) || bidAskPrice;
					record.set('netCostBasis', quantity * pendingPrice * priceMultiplier);
					record.set('costPerShare', pendingPrice);
				}
				else {
					// Recalculate cost basis adjusting for pending cost basis only when pending is further opening the position; quantity in same direction.
					// If the pending activity is closing, the pending cost basis is to reflect the price of the existing position.
					if (record.get('actualQuantity') > 0 === pendingQuantity > 0) {
						// Use BID for Selling, ASK for Buying
						const pendingPrice = (pendingQuantity < 0 ? record.get('investmentSecurity.BID') : record.get('investmentSecurity.ASK')) || bidAskPrice;
						record.set('netCostBasis', TCG.getValue('actualNetCostBasis', record.json) + (pendingQuantity * pendingPrice * priceMultiplier));
						record.set('costPerShare', record.get('netCostBasis') / quantity / priceMultiplier);
					}
				}
			}
			// if position has pending trades fully closing it, so use the original cost/share and quantity for PnL and realized
			const costPerShare = record.get('costPerShare') === 0 ? record.get('actualCostPerShare') : record.get('costPerShare');
			const pnLQuantity = quantity === 0 ? record.get('actualQuantity') : quantity;
			const pnLPerShare = (pnLQuantity > 0 ? bidAskPrice - costPerShare : costPerShare - bidAskPrice);
			record.set('profitAndLossPerShare', pnLPerShare);
			record.set('profitAndLoss', pnLPerShare * Math.abs(pnLQuantity) * priceMultiplier);
			record.set('percentRealized', (costPerShare && costPerShare > 0) ? pnLPerShare / costPerShare * 100 : 0);
			processExtrinsic();
		}

		function processUnderlyingPriceChange() {
			record.set('underlyingPrice', underlyingPrice);
			record.set('moneyness', itmOrZero);
			record.set('percentInTheMoney', itm / underlyingPrice * 100);
			processExtrinsic();
		}

		function processOptionSecurityPosition() {
			// delta is positive if a long call or short put; negative if a short call or long put
			const multiplier = record.get('call') ? ((quantity > 0) ? 1 : -1) : ((quantity < 0) ? 1 : -1);
			const delta = record.get('delta');
			if (delta > 0 && multiplier < 0) {
				// update delta on initial load
				record.set('delta', delta * multiplier);
			}
			else if (record.isModified('investmentSecurity.DELTA_MID_RT')) {
				record.set('delta', record.get('investmentSecurity.DELTA_MID_RT') * multiplier);
			}

			if (gridPanel.applyTradeRules) {
				// ANY CHANGES TO THE BELOW RULES MUST BE VERIFIED BY WESTPORT
				/* Dividend Exercise Rule
				 * Dividends are obtained from IMS as an aggregate of projected dividend per share amounts for an underlying security from current day to option expiration.
				 * The record's ex date is the first announced or projected ex date either equal to or after today.
				 * Since the dividends are captured according to the option's expiration server side, we do not need to consider the ex date in the rule calculation.
				 */
				const processingType = record.get('clientInvestmentAccount.serviceProcessingType.name');
				const isApplyTenTenEighty = ['Call Writing', 'Call Selling'].includes(processingType, 0);
				const isApplyLD90 = ['Structured Options', 'Defensive Equity', 'Fully Funded'].includes(processingType, 0);
				const daysToExpiration = record.get('daysToExpiration');

				// Dividend Ex Risk
				const approximatePutValue = (bidAskPrice) - itm; // C - (S - K) = Call option value - (spot (underlying price) - strike) = approximate put value
				// only American Call Options will have a value for dividend via local grid panel property americanCallOptionSecurityIds
				const dividend = record.get('investmentSecurity.OPT_DIVIDEND');
				// (2 * approximatePutValue < 1.5 * dividend) nets to (approximatePutValue < 0.75 * dividend) - simplified to avoid extra calculations
				const dividendExerciseRisk = quantity < 0 && (TCG.isNotNull(dividend) && dividend > 0 && (approximatePutValue < (0.75 * dividend)));
				record.set('dividendExerciseRisk', dividendExerciseRisk);
				record.set('approximatePutValue', approximatePutValue);

				// 10/10/80
				if (TCG.isTrue(isApplyTenTenEighty)) {
					const percentRealized = record.get('percentRealized');
					const tenTenEighty = quantity < 0 && (percentRealized > 80 || percentRealized < -250 || daysToExpiration < 10 || record.get('delta') >= -.1);
					record.set('tenTenEighty', tenTenEighty);
				}

				/* LD90 Rule
				 * "Linear Decay"
				 * A 90% depreciation trigger would be applied on a Premium Per Day-based reference. We define the following:
				 * Cost Basis Premium Per Day (CBP/d) = Premium Collected / Calendar Days till Expiration When Sold
				 * Market Premium Per Day (MP/d) = Market Price / Live Calendar Days Remaining till Expiration Including the Expiration Day
				 * And, the trigger to repurchase would be applied as such: MP/d <= .10 * CBP/d
				 * Or Buyback price = 10% * Sale Price * ( # of calendar days remaining till expiration / # of calendar days of the option tenor when sold)
				 * Example:
				 * Put Option Sold for $20.00 with 28 days till expiry.
				 * CBP/d = $20 / 28 = $0.71
				 * .10 * CBP/d = .10 * $0.71 = $0.071
				 * The rule would apply a linearly-depreciating buyback price of $0.071 premium per day remaining in options held short. As such, the buyback price would be revised
				 * lower daily.
				 *
				 * When considering the formula: Or Buyback price = 10% * Sale Price * ( # of calendar days remaining till expiration / # of calendar days of the option tenor when sold),
				 * we have to consider the current day as well, per language in the rule as seen above "Market Premium Per Day (MP/d) = Market Price / Live Calendar Days Remaining till Expiration Including the Expiration Day"
				 * For example: an option sold 27 days until expiration, with 20 days left to expiration = the below. But we need to consider today, so the buyback price increases by .08, and is really $1.66.
				 * Buyback price = 10%$21.35 ( 20/27)
				 * = $2.135 * .74074
				 * = $1.5814
				 */
				if (TCG.isTrue(isApplyLD90)) {
					let linearDecay90 = false;
					if (daysToExpiration > 0 && quantity < 0) {
						const tenPercentOfSalePrice = 0.1 * Math.abs(record.get('costPerShare'));
						// costPerShare * days between position open and expiration
						const daysTillExpirationOnOpen = Math.ceil(((record.get('investmentSecurity.lastDeliveryDate').getTime() - record.get('positionOpenDate').getTime()) / (1000 * 3600 * 24)));
						const buybackPrice = Math.ceil(tenPercentOfSalePrice * ((daysToExpiration + 1) / daysTillExpirationOnOpen) * 100) / 100;
						// is current price < twice buyback price
						linearDecay90 = bidAskPrice <= buybackPrice * 2;
						record.set('ld90BidAskPrice', bidAskPrice);
						record.set('ld90BuyBackPrice', buybackPrice);
					}
					record.set('linearDecay90', linearDecay90);
				}
			}
		}

		function processExtrinsic() {
			record.set('percentExtrinsic', (bidAskPrice - itmOrZero) / bidAskPrice * 100);
		}
	},

	isOptionSecurityPosition: function(record) {
		return TCG.getValue('investmentSecurity.instrument.hierarchy.investmentType.name', record.json) === 'Options';
	},

	isAmericanCallOptionSecurityPosition: function(record) {
		const optionStyle = record.get('optionStyle');
		return this.isOptionSecurityPosition(record)
			&& record.get('call')
			&& (optionStyle && optionStyle.toUpperCase() === 'AMERICAN');
	},

	isEuropeanFlexOptionSecurityPosition: function(record) {
		const optionStyle = record.get('optionStyle');
		return this.isOptionSecurityPosition(record)
			&& (optionStyle && optionStyle.toUpperCase() === 'EUROPEAN')
			&& record.get('investmentSecurity.instrument.hierarchy.labelExpanded').includes('FLEX');
	},

	///////////////////////////////////////////////////////////////////////////
	// Trading Functions
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the investment type name for the selected positions. Shows an alert if no positions are selected or the selected positions
	 * results in more than one investment type, and returns undefined.
	 */
	getInvestmentTypeNameForSelectedPositions: function() {
		const records = this.grid.getSelectionModel().getSelections();
		const investmentTypes = [...new Set(records.map(record => TCG.getValue('investmentSecurity.instrument.hierarchy.investmentType.name', record.json)))];
		if (investmentTypes.length === 0) {
			TCG.showInfo('Please select a position to trade.', 'No Position(s) Selected');
			return;
		}
		if (investmentTypes.length !== 1) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because more than one investment type was submitted to trade. Found ' + investmentTypes + '.');
			return;
		}

		return investmentTypes[0];
	},

	tradeToClose: function() {
		this.tradeOptionsPositions('Option Trade To Close');
	},

	processAssignment: function() {
		this.tradeOptionsPositions('Option Assignment');
	},

	tradeToOpen: async function() {
		const investmentTypeName = this.getInvestmentTypeNameForSelectedPositions();
		if (!investmentTypeName) {
			return; // message to select a position is presented
		}

		const records = this.grid.getSelectionModel().getSelections();
		const underlyingSecurityIdToPriceMultiplierMap = records
			.map(record => [TCG.getValue('investmentSecurity.underlyingSecurity.id', record.json), TCG.getValue('investmentSecurity.priceMultiplier', record.json)])
			.filter(([underlyingSecurityId, priceMultiplier]) => !!underlyingSecurityId)
			.reduce((securityIdMap, [underlyingSecurityId, priceMultiplier]) => {
				securityIdMap[underlyingSecurityId] = priceMultiplier;
				return securityIdMap;
			}, {});

		const underlyingSecurityIds = Object.keys(underlyingSecurityIdToPriceMultiplierMap);
		if (underlyingSecurityIds.length > 1) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because more than one underlying security was submitted for trading.');
			return;
		}

		const underlyingSecurityId = (underlyingSecurityIds.length === 1) ? parseInt(underlyingSecurityIds[0]) : void 0;
		const securityMultiplier = TCG.isNotNull(underlyingSecurityId) ? underlyingSecurityIdToPriceMultiplierMap[underlyingSecurityIds[0]] : 0;

		if (TCG.isFalse(this.validatePositionDirectionsAreSame(records))) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because not all the selected positions have the same direction (short / long).');
			return;
		}

		const restrictionCheckResult = this.checkTradeRestrictionsOptionTrade(records, true);
		const tradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
		if (!tradePositions) {
			return;
		}

		// map/object to track processed client accounts to prevent defaulting trade data for the same account more than once
		// populate trade details for each selected row for the trade window
		const distinctPositionList = tradePositions
			.map(row => TCG.grid.getUpdatedRecordAsJson(row))
			.distinct(position => position.clientInvestmentAccount.id);

		const clientInvestmentAccountIds = distinctPositionList.map(position => position.clientInvestmentAccount.id);
		let targetUtilizationMap = {};
		// if there is an underlying security, look up client security targets for defaulting quantity
		if (underlyingSecurityId) {
			const targetUtilizationList = await TCG.data.getDataPromise('tradeSecurityTargetUtilizationListFind.json', this, {
				params: {
					balanceDate: this.getSelectedBalanceDate().format('m/d/Y'),
					usePriorPriceDate: true,
					clientInvestmentAccountIds: clientInvestmentAccountIds,
					targetUnderlyingSecurityId: underlyingSecurityId,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'securityTarget.clientInvestmentAccount.id|underlyingSecurityTargetQuantityAdjusted|totalUsageQuantity|coveredQuantity|pendingQuantity|frozenQuantity'
				}
			});
			targetUtilizationMap = targetUtilizationList.reduce((utilizationMap, clientUtilization) => {
				utilizationMap[clientUtilization.securityTarget.clientInvestmentAccount.id] = clientUtilization;
				return utilizationMap;
			}, targetUtilizationMap);
		}

		const tradeDetailPromiseList = distinctPositionList.map(async position => {
			const tradeDetail = {
				clientInvestmentAccount: position.clientInvestmentAccount,
				holdingInvestmentAccount: position.holdingInvestmentAccount,
				cashBalance: position.cashBalance,
				underlyingPrice: position.underlyingPrice,
				investmentSecurity: position.investmentSecurity
			};
			const utilization = targetUtilizationMap[position.clientInvestmentAccount.id];
			if (utilization) {
				tradeDetail['utilizationUnderlyingTarget'] = utilization.underlyingSecurityTargetQuantityAdjusted;
				tradeDetail['utilizationTotalUsage'] = utilization.totalUsageQuantity;
				tradeDetail['utilizationCovered'] = utilization.coveredQuantity;
				tradeDetail['utilizationPending'] = utilization.pendingQuantity;
				tradeDetail['utilizationFrozen'] = utilization.frozenQuantity;
			}
			const columnValue = await Clifton.system.GetCustomColumnValuePromise(position.clientInvestmentAccount.id, 'InvestmentAccount', 'Portfolio Setup', 'Position Open Percent', true);
			if (columnValue != null) {
				tradeDetail['openPercent'] = columnValue;
			}
			return tradeDetail;
		});

		// Retrieve trade data
		const [tradeType, openCloseType, tradeDetailList] = await Promise.all([
			TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json', this, 'trade.type.' + investmentTypeName, {params: {name: investmentTypeName}}),
			TCG.data.getDataPromise('tradeOpenCloseTypeByName.json', this, {params: {name: 'Sell To Open'}}), // default open type for sell for now
			Promise.all(tradeDetailPromiseList)
		]);

		TCG.createComponent('Clifton.trade.monitor.PositionOpenTradeWindow', {
			defaultData: {
				tradeType: tradeType,
				openCloseType: openCloseType,
				'investmentSecurity.priceMultiplier': securityMultiplier, // default the price multiplier
				applySecurityTargetUtilization: true, // flag to apply usage details to trades
				tradeDetailList: tradeDetailList,
				...underlyingSecurityId ? {underlyingInvestmentSecurity: {id: underlyingSecurityId}} : {}
			},
			openerCt: this
		});
	},

	tradeOptionsPositions: async function(tradeGroupTypeName) {
		const investmentTypeName = this.getInvestmentTypeNameForSelectedPositions();
		if (!investmentTypeName) {
			return;  // message to select a position is presented
		}
		else if (investmentTypeName !== 'Options') {
			Ext.Msg.alert('Unable to Trade', 'Only Options positions can be traded at this time.');
			return;
		}

		const records = this.grid.getSelectionModel().getSelections();
		if (TCG.isFalse(this.validatePositionDirectionsAreSame(records))) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because not all the selected positions have the same direction (short / long).');
			return;
		}

		const restrictionCheckResult = this.checkTradeRestrictionsOptionTrade(records, false);
		const tradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
		if (!tradePositions) {
			return;
		}

		const assignment = tradeGroupTypeName === 'Option Assignment';
		const jsonPositionList = tradePositions
			.map(row => TCG.grid.getUpdatedRecordAsJson(row))
			.distinct(position => position.clientInvestmentAccount.id);
		const distinctClientAccountIds = jsonPositionList.map(position => position.clientInvestmentAccount.id).distinct();
		const distinctUnderlyingSecurityIds = jsonPositionList.map(position => position.investmentSecurity.underlyingSecurity.id).distinct();
		let targetUtilizationMap = {};
		if (distinctClientAccountIds.length > 0 && distinctUnderlyingSecurityIds.length > 0) {
			const targetUtilizationList = await TCG.data.getDataPromise('tradeSecurityTargetUtilizationListFind.json', this, {
				params: {
					balanceDate: this.getSelectedBalanceDate().format('m/d/Y'),
					usePriorPriceDate: true,
					clientInvestmentAccountIds: distinctClientAccountIds,
					targetUnderlyingSecurityIds: distinctUnderlyingSecurityIds,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'securityTarget.clientInvestmentAccount.id|securityTarget.targetUnderlyingSecurity.id|underlyingSecurityTargetQuantityAdjusted|totalUsageQuantity|coveredQuantity|pendingQuantity|frozenQuantity'
				}
			});
			targetUtilizationMap = targetUtilizationList.reduce((utilizationMap, clientUtilization) => {
				let clientUtilizationMap = utilizationMap[clientUtilization.securityTarget.clientInvestmentAccount.id];
				if (TCG.isNull(clientUtilizationMap)) {
					clientUtilizationMap = {};
					utilizationMap[clientUtilization.securityTarget.clientInvestmentAccount.id] = clientUtilizationMap;
				}
				clientUtilizationMap[clientUtilization.securityTarget.targetUnderlyingSecurity.id] = clientUtilization;
				return utilizationMap;
			}, targetUtilizationMap);
		}
		const tradeDetailPromiseList = tradePositions
			.map(row => TCG.grid.getUpdatedRecordAsJson(row))
			.map(async position => {
				// ASK for short, BID for long to get more accurate cost to close
				const securityPrice = (position.quantity > 0 ? position.investmentSecurity.BID : position.investmentSecurity.ASK);
				const underlyingPrice = assignment ? position.strikePrice : position.underlyingPrice;
				const tradeDetail = {
					clientInvestmentAccount: position.clientInvestmentAccount,
					holdingInvestmentAccount: position.holdingInvestmentAccount,
					investmentSecurity: Ext.apply(TCG.clone(position.investmentSecurity), {price: securityPrice, underlyingPrice: underlyingPrice}),
					originalQuantity: Math.abs(position.quantity),
					quantityIntended: Math.abs(position.quantity),
					buy: position.quantity < 0,
					cashBalance: position.cashBalance
				};
				const clientUtilizationMap = targetUtilizationMap[position.clientInvestmentAccount.id];
				if (clientUtilizationMap) {
					const utilization = clientUtilizationMap[position.investmentSecurity.underlyingSecurity.id];
					if (utilization) {
						tradeDetail['utilizationUnderlyingTarget'] = utilization.underlyingSecurityTargetQuantityAdjusted;
						tradeDetail['utilizationTotalUsage'] = utilization.totalUsageQuantity;
						tradeDetail['utilizationCovered'] = utilization.coveredQuantity;
						tradeDetail['utilizationPending'] = utilization.pendingQuantity;
						tradeDetail['utilizationFrozen'] = utilization.frozenQuantity;
					}
				}
				return tradeDetail;
			});
		const [tradeType, tradeGroupType, tradeDetailList] = await Promise.all([
			TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json', this, 'trade.type.' + investmentTypeName, {params: {name: investmentTypeName}}),
			TCG.data.getDataPromise('tradeGroupTypeByName.json', this, {params: {name: tradeGroupTypeName}}),
			Promise.all(tradeDetailPromiseList)
		]);
		TCG.createComponent('Clifton.trade.monitor.PositionCloseTradeWindow', {
			defaultData: {
				tradeType: tradeType,
				tradeGroupType: tradeGroupType,
				optionAssignment: tradeGroupTypeName.toLowerCase().includes('assignment'),
				tradeDetailList: tradeDetailList,
				url: 'tradeGroupListForTradeOptionsRequestGenerate.json'
			},
			openerCt: this,
			submitTradesCallback: async formPanel => {
				const tradeGrid = TCG.getChildByName(formPanel, 'tradeDetailListGrid');
				const records = tradeGrid.store.data.items;
				const restrictionCheckResult = this.checkTradeRestrictionsOptionTrade(records, false);
				const submitTradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
				if (!submitTradePositions) {
					return;
				}
				const form = formPanel.getForm();
				const formData = Ext.apply({
					'tradeDestination.id': form.findField('tradeDestination.id').getValue(),
					'executingBrokerCompany.id': form.findField('executingBrokerCompany.id').getValue(),
					'tradeExecutionType.id': form.findField('tradeExecutionType.id').getValue(),
					'limitPrice': form.findField('limitPrice').getValue()
				}, formPanel.getWindow().defaultData);
				formData.tradeDetailList = submitTradePositions.map(record => TCG.grid.getUpdatedRecordAsJson(record));
				this.tradeOptionServiceCallback(formData, () => formPanel.getWindow().close());
			}
		});
	},

	tradeToRoll: async function() {
		const investmentTypeName = this.getInvestmentTypeNameForSelectedPositions();
		if (!investmentTypeName) {
			return;  // message to select a position is presented
		}
		else if (investmentTypeName !== 'Options') {
			Ext.Msg.alert('Unable to Trade', 'Only Options positions can be rolled at this time.');
			return;
		}
		const selections = this.grid.getSelectionModel().getSelections();
		const symbolList = [...new Set(selections.map(row => row.json.investmentSecurity.symbol))];
		if (symbolList.length > 1) {
			Ext.Msg.alert('Unable to Trade', `Can only trade one security at a time. Found the following securities:<br/>${symbolList.map(symbol => `<br/>${symbol}`)}`);
			return;
		}

		if (TCG.isFalse(this.validatePositionDirectionsAreSame(selections))) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because not all the selected positions have the same direction (short / long).');
			return;
		}

		// Check both closing and opening trades for trade restriction violations
		const openRestrictionCheckResult = this.checkTradeRestrictionsOptionTrade(selections, true);
		const closeRestrictionCheckResult = this.checkTradeRestrictionsOptionTrade(selections, false);
		const restrictionCheckResult = {};
		restrictionCheckResult.restrictedPositions = [...openRestrictionCheckResult.restrictedPositions, ...closeRestrictionCheckResult.restrictedPositions].distinct();
		restrictionCheckResult.validPositions = [...openRestrictionCheckResult.validPositions, ...closeRestrictionCheckResult.validPositions]
			.distinct()
			.filter(position => !restrictionCheckResult.restrictedPositions.includes(position));

		const tradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
		if (!tradePositions) {
			return;
		}

		const distinctPositionList = tradePositions
			.map(row => TCG.grid.getUpdatedRecordAsJson(row))
			.distinct(position => position.clientInvestmentAccount.id);
		const firstSelectedRow = distinctPositionList[0];
		const closeIsBuy = firstSelectedRow.quantity < 0;
		const closeSecurityPrice = (closeIsBuy ? firstSelectedRow.investmentSecurity.ASK : firstSelectedRow.investmentSecurity.BID);
		const clientInvestmentAccountIds = distinctPositionList.map(position => position.clientInvestmentAccount.id);
		const targetUtilizationList = await TCG.data.getDataPromise('tradeSecurityTargetUtilizationListFind.json', this, {
			params: {
				balanceDate: this.getSelectedBalanceDate().format('m/d/Y'),
				usePriorPriceDate: true,
				clientInvestmentAccountIds: clientInvestmentAccountIds,
				targetUnderlyingSecurityId: firstSelectedRow.investmentSecurity.underlyingSecurity.id,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'securityTarget.clientInvestmentAccount.id|underlyingSecurityTargetQuantityAdjusted|totalUsageQuantity|coveredQuantity|pendingQuantity|frozenQuantity'
			}
		});
		const targetUtilizationMap = targetUtilizationList.reduce((utilizationMap, clientUtilization) => {
			utilizationMap[clientUtilization.securityTarget.clientInvestmentAccount.id] = clientUtilization;
			return utilizationMap;
		}, {});
		const tradeDetailPromiseList = distinctPositionList.map(position => {
			// Convert to promise resolving to trade details with target utilization
			const rowData = {
				clientInvestmentAccount: position.clientInvestmentAccount,
				holdingInvestmentAccount: position.holdingInvestmentAccount,
				investmentSecurity: position.investmentSecurity,
				originalQuantity: Math.abs(position.quantity),
				quantityIntended: Math.abs(position.quantity),
				originalRollQuantity: Math.abs(position.quantity),
				rollQuantity: Math.abs(position.quantity),
				cashBalance: position.cashBalance,
				costPerShare: position.costPerShare,
				daysToExpiration: position.daysToExpiration
			};
			const utilization = targetUtilizationMap[position.clientInvestmentAccount.id];
			if (utilization) {
				rowData['utilizationUnderlyingTarget'] = utilization.underlyingSecurityTargetQuantityAdjusted;
				rowData['utilizationTotalUsage'] = utilization.totalUsageQuantity;
				rowData['utilizationCovered'] = utilization.coveredQuantity;
				rowData['utilizationPending'] = utilization.pendingQuantity;
				rowData['utilizationFrozen'] = utilization.frozenQuantity;
			}
			return rowData;
		});
		const [tradeType, tradeGroupType, tradeDetailList] = await Promise.all([
			TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json', this, 'trade.type.' + investmentTypeName, {params: {name: investmentTypeName}}),
			TCG.data.getDataPromise('tradeGroupTypeByName.json', this, {params: {name: 'Option Roll Forward'}}),
			Promise.all(tradeDetailPromiseList)
		]);
		TCG.createComponent('Clifton.trade.monitor.OptionRollTradeWindow', {
			defaultData: {
				tradeType: tradeType,
				openTradeIsSell: closeIsBuy,
				underlyingPrice: firstSelectedRow.underlyingPrice,
				tradeGroupType: tradeGroupType,
				underlyingInvestmentSecurity: firstSelectedRow.investmentSecurity.underlyingSecurity,
				putCall: TCG.getValue('call', firstSelectedRow) ? 'CALL' : 'PUT',
				closeInvestmentSecurity: {price: closeSecurityPrice},
				tradeDetailList: tradeDetailList,
				url: 'tradeGroupListForTradeOptionsRequestGenerate.json'
			},
			openerCt: this,
			submitTradesCallback: async formPanel => {
				const restrictionCheckResult = formPanel.checkTradeToRollTradeRestrictions();
				const submitTradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
				if (!submitTradePositions) {
					return;
				}
				const form = formPanel.getForm();
				const formData = Ext.apply({
					'tradeDestination.id': form.findField('tradeDestination.id').getValue(),
					'executingBrokerCompany.id': form.findField('executingBrokerCompany.id').getValue(),
					'rollSecurity.id': form.findField('investmentSecurity.id').getValue(),
					rollRatio: form.findField('rollRatio').getValue(),
					'tradeExecutionType.id': form.findField('tradeExecutionType.id').getValue(),
					'limitPrice': form.findField('limitPrice').getValue()
				}, formPanel.getWindow().defaultData);
				formData.tradeDetailList = submitTradePositions
					.map(record => TCG.grid.getUpdatedRecordAsJson(record));
				this.tradeOptionServiceCallback(formData, () => formPanel.getWindow().close());
			}
		});
	},

	tradeOptionServiceCallback: async function(formData, successCallback) {
		if (TCG.isNull(formData.tradeGroupType) || TCG.isBlank(formData.tradeGroupType.id)) {
			Ext.Msg.alert('Unable to Trade', 'No Trade Group Type was defined to for processing the Option trades.');
			return;
		}

		const tradeSource = await TCG.data.getDataPromiseUsingCaching('tradeSourceByName.json?name=DeltaShift Blotter', this, 'tradeSource.DeltaShift Blotter');

		const tradeAllocations = (formData.tradeDetailList || [])
			.map(detail => {
				const tradeDetail = {
					'class': 'com.clifton.trade.options.TradeOptionsRequestDetail',
					'clientInvestmentAccount.id': TCG.getValue('clientInvestmentAccount.id', detail),
					'holdingInvestmentAccount.id': TCG.getValue('holdingInvestmentAccount.id', detail),
					'investmentSecurity.id': TCG.getValue('investmentSecurity.id', detail),
					tradeQuantity: Math.abs(TCG.getValue('quantityIntended', detail)),
					'btic': TCG.isTrue(formData.btic),
					'tradeExecutionType.id': formData['tradeExecutionType.id'],
					'limitPrice': formData['limitPrice']
				};
				tradeDetail['executingBroker.id'] = TCG.getValue('executingBrokerCompany.id', detail);
				tradeDetail['tradeDestination.id'] = TCG.getValue('tradeDestination.id', detail);

				if (TCG.isNotNull(formData['rollSecurity.id'])) {
					tradeDetail['rollSecurity.id'] = formData['rollSecurity.id'];
					tradeDetail['rollRatio'] = formData.rollRatio;
					tradeDetail['rollUsingFrozen'] = TCG.isTrue(detail.useFrozen);
					tradeDetail['rollQuantity'] = detail.rollQuantity;
				}
				if (TCG.isTrue(detail['tradeUnderlying'])) {
					tradeDetail['tradeUnderlying'] = true;
					tradeDetail['underlyingTradeQuantity'] = detail.underlyingQuantity;
					tradeDetail['underlyingTradeBuy'] = TCG.isTrue(detail.underlyingBuy);
				}
				return tradeDetail;
			});
		const loader = new TCG.data.JsonLoader({
			waitTarget: this,
			waitMsg: 'Processing...',
			timeout: 180000,
			params: {
				'tradeSource.id': tradeSource ? tradeSource.id : void 0,
				'traderUser.id': formData.traderUser.id,
				'tradeGroupType.id': formData.tradeGroupType.id,
				'tradeDestination.id': formData['tradeDestination.id'],
				'executingBroker.id': formData['executingBrokerCompany.id'],
				// response will include tradeGroupList and optionRequest, exclude the following from the response
				requestedPropertiesToExcludeGlobally: 'tradeList|details',
				details: Ext.util.JSON.encode(tradeAllocations)
			}
		});
		if (successCallback) {
			loader.onLoad = successCallback;
		}
		loader.load(formData.url);
	},

	///////////////////////////////////////////////////////////////////////////
	// Trading Underlying functions
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the underlying security for the for the selected positions. Shows an alert if no positions are selected or the selected positions
	 * results in more than one underlying security, and returns undefined.
	 */
	getUnderlyingSecurityForSelectedPositions: function() {
		const records = this.grid.getSelectionModel().getSelections();
		if (records.length === 0) {
			TCG.showInfo('Please select a position to trade.', 'No Position(s) Selected');
			return;
		}
		const underlyingSecurityList = records.map(record => TCG.getValue('investmentSecurity.underlyingSecurity', record.json))
			.filter(underlyingSecurity => TCG.isNotNull(underlyingSecurity));
		const underlyingSecurityIdList = [...new Set(underlyingSecurityList.map(security => security.id))];
		if (underlyingSecurityIdList.length !== 1) {
			Ext.Msg.alert('Unable to Trade', 'Cancelling because ' + (underlyingSecurityIdList.length > 1 ? 'more than one' : 'no') + ' Underlying Security was determined from the selected positions. Found [' + underlyingSecurityList.map(security => security.symbol) + '].');
			return;
		}

		return underlyingSecurityList[0];
	},

	tradeSellUnderlying: async function() {
		this.tradeUnderlying(true);
	},

	tradeBuyUnderlying: async function() {
		this.tradeUnderlying(false);
	},

	tradeUnderlying: async function(sell) {
		let underlyingSecurity = this.getUnderlyingSecurityForSelectedPositions();
		if (underlyingSecurity) {
			underlyingSecurity = await TCG.data.getDataPromise('investmentSecurity.json?id=' + underlyingSecurity.id, this);
			const investmentTypeName = underlyingSecurity.instrument.hierarchy.investmentType.name;
			if (investmentTypeName === 'Benchmarks') {
				Ext.Msg.alert('Unable to Trade', 'Unable to trade Underlying Benchmarks.');
				return;
			}

			const positions = this.grid.getSelectionModel().getSelections();
			const restrictionCheckResult = this.checkTradeRestrictionsUnderlyingTrade(positions, underlyingSecurity.id, !sell);
			const tradePositions = await this.getTradeRestrictionActionForPositions(restrictionCheckResult);
			if (!tradePositions) {
				return;
			}

			const directionFunction = sell ? position => position.quantity > 0 : position => position.quantity < 0;
			const positionsToTrade = tradePositions
				.map(position => TCG.grid.getUpdatedRecordAsJson(position))
				.filter(position => position.investmentSecurity.underlyingSecurity);
			const positionDirections = positionsToTrade.map(directionFunction).distinct();
			if (positionDirections.length !== 1) {
				Ext.Msg.alert('Unable to Trade', 'Can only trade an Underlying Security for positions in the same direction (long/short).');
				return;
			}
			const buy = positionDirections[0];
			const distinctPositionList = positionsToTrade.distinct(position => position.clientInvestmentAccount.id);
			const underlyingPrice = (buy ? distinctPositionList[0].investmentSecurity.underlyingSecurity.ASK
				: distinctPositionList[0].investmentSecurity.underlyingSecurity.BID) || distinctPositionList[0].underlyingPrice;
			const clientInvestmentAccountIds = distinctPositionList.map(position => position.clientInvestmentAccount.id);
			const targetUtilizationList = await TCG.data.getDataPromise('tradeSecurityTargetUtilizationListFind.json', this, {
				params: {
					balanceDate: this.getSelectedBalanceDate().format('m/d/Y'),
					usePriorPriceDate: true,
					clientInvestmentAccountIds: clientInvestmentAccountIds,
					targetUnderlyingSecurityId: underlyingSecurity.id,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'securityTarget.clientInvestmentAccount.id|underlyingSecurityTargetQuantity|underlyingSecurityTargetQuantityAdjusted|totalUsageQuantity|coveredQuantity|pendingQuantity|frozenQuantity'
				}
			});
			const targetUtilizationMap = targetUtilizationList.reduce((utilizationMap, clientUtilization) => {
				utilizationMap[clientUtilization.securityTarget.clientInvestmentAccount.id] = clientUtilization;
				return utilizationMap;
			}, {});
			const tradeDetailPromiseList = distinctPositionList.map(async position => {
				const tradeDetail = {
					clientInvestmentAccount: position.clientInvestmentAccount,
					holdingInvestmentAccount: position.holdingInvestmentAccount,
					investmentSecurity: Ext.applyIf(underlyingSecurity, {price: underlyingPrice}),
					buy: buy,
					cashBalance: position.cashBalance,
					underlyingPrice: underlyingPrice
				};
				const utilization = targetUtilizationMap[position.clientInvestmentAccount.id];
				if (utilization) {
					tradeDetail[sell ? 'originalQuantity' : 'utilizationUnderlyingTarget'] = utilization.underlyingSecurityTargetQuantity;
				}
				return tradeDetail;
			});
			const openCloseTypeParams = {
				buy: !sell,
				close: true,
				open: true
			};
			const [tradeType, openCloseType, tradeDetailList] = await Promise.all([
				TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json', this, 'trade.type.' + investmentTypeName, {params: {name: investmentTypeName}}),
				TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', this, {params: openCloseTypeParams}).then(types => types[0]),
				Promise.all(tradeDetailPromiseList)
			]);
			const componentWindow = sell ? 'Clifton.trade.monitor.PositionCloseTradeWindow' : 'Clifton.trade.monitor.PositionOpenTradeWindow';
			const launchParams = {
				defaultData: {
					tradeType: tradeType,
					openCloseType: openCloseType,
					investmentSecurity: underlyingSecurity,
					tradeDetailList: tradeDetailList
				},
				openerCt: this
			};
			if (sell) {
				launchParams.submitTradesCallback = async function(formPanel) {
					const grid = TCG.getChildByName(formPanel, 'tradeDetailListGrid');
					const records = grid.getStore().data.items;
					const restrictionCheckResult = formPanel.ownerCt.openerCt.checkTradeRestrictionsUnderlyingTrade(records, formPanel.ownerCt.defaultData.investmentSecurity.id, false);
					const submitTradePositions = await formPanel.ownerCt.openerCt.getTradeRestrictionActionForPositions(restrictionCheckResult);
					if (!submitTradePositions) {
						return;
					}
					const totalQuantity = submitTradePositions
						.map(record => record.get('quantityIntended'))
						.reduce((totalQuantity, recordQuantity) => totalQuantity + (recordQuantity || 0), 0);
					grid.getStore().data.items = submitTradePositions;
					grid.markModified();
					grid.getStore().data.items = records;
					formPanel.getForm().submit({
						...TCG.form.submitDefaults,
						timeout: formPanel.getWindow().saveTimeout,
						url: encodeURI('tradeEntrySave.json'),
						params: {quantityIntended: totalQuantity},
						waitMsg: 'Saving...',
						submitEmptyText: formPanel.submitEmptyText,
						success: (form, action) => {
							const data = formPanel.getDataAfterSave(action.result.data);
							if (data !== false) {
								formPanel.getWindow().closeWindow();
							}
						}
					});
				};
			}
			TCG.createComponent(componentWindow, launchParams);
		}
	},
	/**
	 * A function that checks for TradeRestrictions for option trades.
	 *
	 * @param selectedPositionList selected positions to check restrictions for
	 * @param isOpen a flag, that indicates whether or not this is an opening trade
	 * @returns {*}  an object containing an {@link Array} of positions with trade restrictions and an {@link Array} of valid positions.
	 */
	checkTradeRestrictionsOptionTrade: function(selectedPositionList, isOpen) {
		const restrictedPositions = [], validPositions = [];
		for (const selectedPosition of selectedPositionList) {
			let investmentSecurity = null;
			let isSell = null;
			const underlyingSecurityId = selectedPosition.json.investmentSecurity.underlyingSecurity.id;
			const clientInvestmentAccountId = selectedPosition.json.clientInvestmentAccount.id;
			const underlyingPrice = selectedPosition.json.underlyingPrice;

			if (isOpen) {
				//note: investmentSecurity not set here as it is unknown until selected in the trade window.
				isSell = selectedPosition.json.quantity < 0.00;
			}
			else {
				investmentSecurity = selectedPosition.json.investmentSecurity;
				isSell = selectedPosition.json.quantity > 0.00;
			}

			if (TCG.isTrue(this.checkTradeRestrictions(clientInvestmentAccountId, investmentSecurity, underlyingSecurityId, underlyingPrice, isSell, isOpen, this.lastTradeRestrictionList))) {
				validPositions.push(selectedPosition);
			}
			else {
				restrictedPositions.push(selectedPosition);
			}
		}
		return {restrictedPositions: restrictedPositions, validPositions: validPositions};
	},
	/**
	 * A function that checks for TradeRestrictions for trades of the underlying security.
	 *
	 * @param selectedPositionList selected positions to check restrictions for
	 * @param underlyingSecurityId the underlying security of the option position list
	 * @param isOpen a flag, that indicates whether or not this is an opening trade
	 * @returns {*} an object containing an {@link Array} of positions with trade restrictions and an {@link Array} of valid positions to trade.
	 */
	checkTradeRestrictionsUnderlyingTrade: function(selectedPositionList, underlyingSecurityId, isOpen) {
		const restrictedPositions = [], validPositions = [];
		const isSell = !isOpen;
		for (const selectedPosition of selectedPositionList) {
			const clientInvestmentAccountId = selectedPosition.json.clientInvestmentAccount.id;
			const underlyingPrice = selectedPosition.json.underlyingPrice;
			if (TCG.isTrue(this.checkTradeRestrictions(clientInvestmentAccountId, null, underlyingSecurityId, underlyingPrice, isSell, isOpen, this.lastTradeRestrictionList))) {
				validPositions.push(selectedPosition);
			}
			else {
				restrictedPositions.push(selectedPosition);
			}
		}
		return {restrictedPositions: restrictedPositions, validPositions: validPositions};
	},
	/**
	 * This function checks a list of positions and validates that all positions have the same direction(long or short).
	 *
	 * Returns true if all positions have the same direction, otherwise returns false.
	 *
	 * @param positionList list of selected positions from grid.
	 */
	validatePositionDirectionsAreSame: function(positionList) {
		if (TCG.isBlank(positionList) || positionList.length <= 1) {
			return true;
		}

		const directionIsNegative = positionList[0].json.quantity < 0.00;

		for (const position of positionList) {
			if ((directionIsNegative && position.json.quantity > 0.00) || (!directionIsNegative && position.json.quantity < 0.00)) {
				return false;
			}
		}
		return true;
	},

	getSelectedBalanceDate: function() {
		const balanceDateField = TCG.getChildByName(this.getTopToolbar(), 'balanceDate');
		return balanceDateField.getValue() || new Date();
	}
});
Ext.reg('trade-position-monitor-grid', Clifton.trade.monitor.PositionMonitorGridPanel);
