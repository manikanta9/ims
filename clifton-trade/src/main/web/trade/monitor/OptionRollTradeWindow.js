TCG.use('Clifton.trade.monitor.BaseTradeModalWindow');

Clifton.trade.monitor.OptionRollTradeWindow = Ext.extend(Clifton.trade.monitor.BaseTradeModalWindow, {
	title: 'Roll Option Position',

	items: [
		{
			xtype: 'base-modal-trade-formpanel',
			instructions: 'This window asks for additional details for opening new positions across client accounts for a single security.',
			includeSecurityDragAndDrop: true,

			formPanelSecurityColumnItems: [
				{name: 'underlyingInvestmentSecurity.id', xtype: 'hidden', submitValue: false},
				{name: 'investmentType.id', xtype: 'hidden', submitValue: false},
				{name: 'tradeType.holdingAccountPurpose.name', xtype: 'hidden', submitValue: false},
				{name: 'payingSecurity.id', xtype: 'hidden'},
				{name: 'investmentSecurity.priceMultiplier', xtype: 'hidden', submitValue: false},
				{fieldLabel: 'Trade Type', name: 'tradeType.name', hiddenName: 'tradeType.id', xtype: 'combo', url: 'tradeTypeListFind.json', loadAll: true, allowBlank: false, readOnly: true},
				{
					fieldLabel: 'New Security', name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', xtype: 'combo', displayField: 'label', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?active=true', allowBlank: false,
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const formPanel = TCG.getParentFormPanel(combo);
							combo.store.baseParams = {
								investmentTypeId: formPanel.getFormValue('investmentType.id'),
								underlyingSecurityId: formPanel.getFormValue('underlyingInvestmentSecurity.id')
							};
							if (formPanel.getFormValue('tradeType.name') === 'Options') {
								const expirationDate = new Date().format('m/d/Y');
								combo.store.setBaseParam('restrictionList', Ext.util.JSON.encode([{comparison: 'GREATER_THAN', value: expirationDate, field: 'lastDeliveryDate'}]));
							}
						},
						select: function(combo, record, index) {
							const formPanel = combo.getParentForm();
							this.selectedSecurity = record.json;
							formPanel.processSecuritySelection(record.json);
						}
					},
					getDetailPageClass: function(newInstance) {
						return newInstance ? 'Clifton.investment.instrument.copy.SecurityCopyWindow' : this.detailPageClass;
					},
					getDefaultData: function(form, newInstance) {
						if (!newInstance) {
							return {id: form.findField('investmentSecurity.id').getValue()};
						}
						const securityId = form.findField('investmentSecurity.id').getValue();
						return TCG.data.getDataPromise('investmentSecurity.json?id=' + securityId, this)
							.then(security => ({
								instrument: security.instrument,
								securityToCopy: security,
								optionType: 'PUT'
							}));
					}
				},
				{
					fieldLabel: 'Roll Ratio', name: 'rollRatio', xtype: 'spinnerfield', value: 1.00, incrementValue: 0.25, minValue: 0.00, allowDecimals: true, decimalPrecision: 2, allowBlank: false,
					listeners: {
						change: (field, newValue, oldValue) => field.applyUpdate(field, newValue, oldValue),
						spin: spinner => {
							const field = spinner.field; // field is wrapped
							field.applyUpdate(field, field.getValue(), field.previousValue || field.originalValue);
						}
					},
					applyUpdate: async (field, newValue, oldValue) => {
						if (newValue !== oldValue) {
							// store previous value while grid is updated so a call to isUpdated() is accurate
							field.previousValue = oldValue;
							await TCG.getParentFormPanel(field).reloadChildTradeEntryGrid();
							// after the grid quantities are updated, set previous value to the new value; necessary in the event the spinner is used to update
							field.previousValue = newValue;
						}
					},
					isUpdated: function() {
						const previousValue = this.previousValue || this.originalValue;
						return previousValue !== this.getValue();
					}
				}
			],

			formPanelAdditionalColumnItems: [
				{
					fieldLabel: 'Close Price', name: 'closeInvestmentSecurity.price', xtype: 'floatfield',
					qtip: 'Price of the security used for the closing leg of the trade; price used is ASK if buying, BID if selling',
					listeners: {
						valid: function(field) {
							const form = TCG.getParentFormPanel(field).getForm();
							const netPriceField = form.findField('netPrice');
							netPriceField.recalculate.call(netPriceField);
						}
					}
				},
				{
					fieldLabel: 'Open Price', name: 'investmentSecurity.price', xtype: 'floatfield',
					qtip: 'Price of the new security used for the roll leg of the trade; price used is ASK if buying, BID if selling',
					listeners: {
						valid: function(field) {
							const form = TCG.getParentFormPanel(field).getForm();
							const netPriceField = form.findField('netPrice');
							netPriceField.recalculate.call(netPriceField);
						}
					}
				},
				{
					fieldLabel: 'Net Price', name: 'netPrice', xtype: 'floatfield',
					qtip: 'The net price difference between the price of the Close Security and the price of the New Security for calculating cash impact of the Roll Trade',
					recalculate: function() {
						const formPanel = TCG.getParentFormPanel(this);
						const form = formPanel.getForm();
						const closePrice = form.findField('closeInvestmentSecurity.price').getNumericValue();
						const openPrice = form.findField('investmentSecurity.price').getNumericValue();
						if (TCG.isNotBlank(closePrice) && TCG.isNotBlank(openPrice)) {
							const newValue = parseFloat((openPrice - closePrice).toFixed(5));
							if (newValue !== this.getNumericValue()) {
								this.setValue(newValue);
								formPanel.reloadChildTradeEntryGrid();
							}
						}
					}
				},
				{fieldLabel: 'Underlying Price', name: 'investmentSecurity.underlying.price', xtype: 'floatfield', qtip: 'Price of the underlying security used when trading the underlying security; price used is ASK if buying, BID if selling'}
			],

			formPanelBottomItems: [
				{
					xtype: 'base-modal-trade-group-entry-grid',

					plugins: [
						{
							ptype: 'gridsummary'
						},
						{
							ptype: 'trade-restriction-aware',
							getRecordQuantityName: () => 'quantityIntended',
							/**
							 * Sets close quantity and roll quantity to 0 for records that have trade restrictions and restores these quantities if
							 * all trade restrictions have been removed.
							 * @param record to adjust quantity for
							 */
							updateQuantityAfterRestrictionChange: function(record) {
								if ((record.tradeRestrictionList || []).length > 0 && !TCG.isNumber(record.restrictedCloseQuantity)) {
									// close quantity
									record.restrictedCloseQuantity = record.get(this.getRecordQuantityName());
									record.set(this.getRecordQuantityName(), 0);
									// roll quantity
									record.restrictedRollQuantity = record.get('rollQuantity');
									record.set('rollQuantity', 0);
								}
								else if ((record.tradeRestrictionList || []).length === 0 && TCG.isNumber(record.restrictedCloseQuantity)) {
									// close quantity
									record.set(this.getRecordQuantityName(), record.restrictedCloseQuantity);
									record.restrictedCloseQuantity = '';
									//roll quantity
									record.set('rollQuantity', record.restrictedRollQuantity);
									record.restrictedRollQuantity = '';
								}
							},
							isApplyNotesToRecordColumn: () => true
						}
					],

					addToolbarButtons: function(toolbar) {
						toolbar.add({
							text: 'Apply Changes',
							tooltip: 'Apply available changes to the grid details based on selected form values. Changes should be automatically applied when changing applicable values.',
							iconCls: 'preview',
							handler: () => this.reload()
						});
						toolbar.add('-', {
							text: 'Debit Balance Report',
							tooltip: 'Generate a debit balance report email showing cash needed for each client account trade in the below list of trades.',
							iconCls: 'email',
							handler: async () => {
								const formPanel = TCG.getParentFormPanel(this);
								const form = formPanel.getForm();
								const closePrice = form.findField('closeInvestmentSecurity.price').getValue();
								const rollPrice = form.findField('investmentSecurity.price').getValue();
								const netRollPrice = (rollPrice - closePrice).toFixed(5);
								const records = this.getStore().getRange(0, this.getStore().getTotalCount());
								const closeSecuritySymbol = records.length > 0 ? records[0].data['investmentSecurity.symbol'] : void 0; // all details will be for the same security, so pull it out of the first record
								const cashDetails = [];
								for (const record of records) {
									const row = await generateCashDebitDetailHtmlTableRow(record);
									if (TCG.isNotBlank(row)) {
										cashDetails.push(row);
									}
								}

								if (cashDetails.length > 0) {
									const rollSecurity = await TCG.data.getDataPromise('investmentSecurity.json', formPanel, {params: {id: form.findField('investmentSecurity.id').getValue(), requestedProperties: 'symbol', requestedPropertiesRoot: 'data'}});
									const subject = 'Option Debit Balance Report Rolling from \'' + closeSecuritySymbol + '\' to \'' + rollSecurity.symbol + '\'';
									const fileName = 'RollDebitBalanceReport.eml';
									const emailContent = '<!DOCTYPE html><html lang="en"><head><style>'
										+ 'p{font-family:arial,sans-serif;font-size:14px;}table{border-collapse:collapse;font-family:arial,sans-serif;font-size:14px;}th,td{border:1px solid black;}th{padding:5px;}'
										+ '</style><title></title></head><body>'
										+ '<p>Debit Balance Report Rolling from <b>' + closeSecuritySymbol + '</b> to <b>' + rollSecurity.symbol + '</b>:</p>'
										+ '<table><thead><tr><th>Client</th><th>Account</th><th>Close Quantity</th><th>Close Fill Price</th><th>Roll Quantity</th><th>Roll Fill Price</th><th>Net Roll Price</th><th>Underlying Price</th><th>Actual Cash</th><th>Roll Cost</th><th>Cash Needed</th></tr></thead><tbody>'
										+ cashDetails.join('')
										+ '</body></html>';
									TCG.generateEmailFile(subject, 'text/html', emailContent, fileName, void 0, 'PRATrading@paraport.com');
								}
								else {
									TCG.showInfo('There are no rows with a negative cash balance', 'Debit Balance Report');
								}

								async function generateCashDebitDetailHtmlTableRow(record) {
									const cashBalance = record.data['cashBalance'];
									const rollCost = record.data['netTradeCashImpact'];
									const openIsSell = formPanel.getFormValue('openTradeIsSell');
									const underlyingPrice = TCG.getValue('investmentSecurity.underlyingSecurity.' + (!openIsSell ? 'ASK' : 'BID'), record.json);
									const cashMarginAccount = await formPanel.getChildTradeEntryGrid().isClientInvestmentAccountNegativeCashBalanceAllowed(record);

									if (rollCost > 0 || (cashBalance + rollCost) >= 0 || cashMarginAccount) {
										return void 0;
									}

									const [clientAccount, holdingAccount] = await Promise.all([
										TCG.data.getDataPromise('investmentAccount.json', formPanel, {params: {id: record.data['clientInvestmentAccount.id'], requestedProperties: 'name', requestedPropertiesRoot: 'data'}}),
										TCG.data.getDataPromise('investmentAccount.json', formPanel, {params: {id: record.data['holdingInvestmentAccount.id'], requestedProperties: 'number', requestedPropertiesRoot: 'data'}})
									]);

									return '<tr>' +
										'<td style="padding:5px;">' + clientAccount.name + '</td>' +
										'<td style="padding:5px;">' + holdingAccount.number + '</td>' +
										'<td style="padding:5px;text-align:right;">' + record.data['calculatedCloseQuantity'] + '</td>' +
										'<td style="padding:5px;text-align:right;">' + closePrice + '</td>' +
										'<td style="padding:5px;text-align:right;">' + record.data['calculatedRollQuantity'] + '</td>' +
										'<td style="padding:5px;text-align:right;">' + rollPrice + '</td>' +
										'<td style="padding:5px;text-align:right;">' + netRollPrice + '</td>' +
										'<td style="padding:5px;text-align:right;">' + underlyingPrice + '</td>' +
										'<td style="padding:5px;text-align:right;">' + formatCash(cashBalance) + '</td>' +
										'<td style="padding:5px;text-align:right;">' + formatCash(rollCost) + '</td>' +
										'<td style="padding:5px;text-align:right;">' + formatCash(Math.abs(cashBalance < 0 ? rollCost : cashBalance + rollCost)) + '</td>' +
										'</tr>';
								}

								function formatCash(cashValue) {
									return TCG.numberFormat(cashValue, cashValue < 0 ? '($0,000.00)' : '$0,0000.00');
								}
							}
						});
						this.addTradeUnderlyingSecurityToToolBar(toolbar);
					},

					updateQuantity: async function(gridRecord, newRollQuantity) {
						const formPanel = TCG.getParentFormPanel(this);
						const form = formPanel.getForm();

						const securityMultiplier = form.findField('investmentSecurity.priceMultiplier').getValue();

						if (securityMultiplier) {
							const originalInsufficientQuantity = gridRecord.get('insufficientQuantity');
							const originalInsufficientCash = gridRecord.get('insufficientCash');
							let closeQuantity = gridRecord.data['quantityIntended'];
							// reset quantityIntended to recalculate
							const originalQuantity = gridRecord.get('originalQuantity');
							if (closeQuantity === 0 || closeQuantity > originalQuantity) {
								closeQuantity = originalQuantity;
								gridRecord.set('quantityIntended', closeQuantity);
							}

							const originalRollQuantity = this.getColumnModel().getCellEditor(this.getColumnModel().findColumnIndex('rollQuantity')).field.startValue || 0;
							const originalCalculatedRollQuantity = gridRecord.get('calculatedRollQuantity') || 0;
							const originalCalculatedCloseQuantity = gridRecord.get('calculatedCloseQuantity') || 0;
							try {
								const validatedRollQuantity = await this.validateQuantityAgainstCash(closeQuantity, newRollQuantity, gridRecord);
								gridRecord.set('rollQuantity', validatedRollQuantity);
							}
							catch (e) {
								console.warn(e);
								// roll back values
								gridRecord.set('rollQuantity', originalRollQuantity);
								gridRecord.set('calculatedRollQuantity', originalCalculatedRollQuantity);
								gridRecord.set('calculatedCloseQuantity', originalCalculatedCloseQuantity);
								gridRecord.set('insufficientQuantity', originalInsufficientQuantity);
								gridRecord.set('insufficientCash', originalInsufficientCash);
							}
						}
					},

					validateQuantityAgainstCash: async function(closeQuantity, rollQuantity, gridRecord) {
						gridRecord.set('calculatedCloseQuantity', closeQuantity);
						const formPanel = TCG.getParentFormPanel(this);
						const form = formPanel.getForm();

						const openIsSell = formPanel.getFormValue('openTradeIsSell');
						const closeSecurityPrice = await this.getCloseSecurityPrice(form, gridRecord, openIsSell);
						const rollSecurityPrice = await this.getRollSecurityPrice(form, gridRecord, !openIsSell);

						const cashBalance = gridRecord.get('cashBalance');
						const securityMultiplier = form.findField('investmentSecurity.priceMultiplier').getValue();
						const closeCashImpact = closeQuantity * securityMultiplier * closeSecurityPrice;

						let returnRollQuantity = this.getRollQuantity(gridRecord, closeQuantity, rollQuantity);
						const rollCashImpact = returnRollQuantity * securityMultiplier * rollSecurityPrice;
						let netCashImpact = rollCashImpact - closeCashImpact;
						let newCashBalance = cashBalance + netCashImpact;

						// validate and adjust the roll and underlying quantities with changes
						let underlyingCashImpact = 0;
						let cashAdjusted = false;
						const maxAttempts = 10;
						let attempts = 0; // add protection in balancing attempts to 10, it should be possible within a few iterations
						do {
							// apply underlying trade quantity and cash balance adjustments; remove previous underlying impact before calculating underlying
							netCashImpact -= underlyingCashImpact;
							newCashBalance -= underlyingCashImpact;
							const updatedUnderlyingCashImpact = await this.getUnderlyingTradeCashImpact(form, gridRecord, newCashBalance);
							cashAdjusted = underlyingCashImpact !== updatedUnderlyingCashImpact;
							underlyingCashImpact = updatedUnderlyingCashImpact;
							netCashImpact += underlyingCashImpact;
							newCashBalance += underlyingCashImpact;

							// After getting underlying trades, validate the roll quantity and adjust cash balances with calculated roll quantity impact
							const updatedRollQuantity = this.getRollQuantity(gridRecord, closeQuantity, returnRollQuantity);
							gridRecord.set('calculatedRollQuantity', updatedRollQuantity);
							cashAdjusted = cashAdjusted || (returnRollQuantity !== updatedRollQuantity);
							const rollQuantityCashAdjustment = ((returnRollQuantity - updatedRollQuantity) * securityMultiplier * rollSecurityPrice);
							returnRollQuantity = updatedRollQuantity;
							netCashImpact -= rollQuantityCashAdjustment;
							newCashBalance -= rollQuantityCashAdjustment;
							attempts++;
						} while (returnRollQuantity > 0 && newCashBalance < 0 && cashAdjusted && (attempts < maxAttempts));

						gridRecord.set('netTradeCashImpact', netCashImpact);

						if (netCashImpact < 0 && newCashBalance < 0) {
							if (!gridRecord.data['ignoreCash']) {
								const cashCanGoNegative = await this.isClientInvestmentAccountNegativeCashBalanceAllowed(gridRecord);
								if (TCG.isFalse(cashCanGoNegative)) {
									// adjust the quantity to 0 if insufficient cash to avoid partial closes
									gridRecord.set('quantityIntended', 0);
									returnRollQuantity = 0;
								}
							}
							gridRecord.set('insufficientCash', true);
							gridRecord.set('insufficientCashValue', newCashBalance);
						}
						else {
							gridRecord.set('insufficientCash', false);
						}
						return returnRollQuantity;
					},

					getCloseSecurityPrice: async function(form, gridRecord, buy) {
						// get prices for the trades; ASK for buying, BID for selling to get more accurate cost
						const closeSecurityPriceField = form.findField('closeInvestmentSecurity.price');
						let closeSecurityPrice = closeSecurityPriceField.getNumericValue();
						if (!closeSecurityPrice) {
							const closeSecurityId = gridRecord.get('investmentSecurity.id');
							closeSecurityPrice = await this.getSecurityLivePrice(closeSecurityId, buy);
							if (TCG.isBlank(closeSecurityPrice)) {
								TCG.showError('Unable to obtain price for close security. Please manually enter a price.', 'Price Failure');
								throw new Error('Unable to obtain price for close security.');
							}
							closeSecurityPriceField.setValue(closeSecurityPrice);
						}
						return closeSecurityPrice;
					},

					getRollSecurityPrice: async function(form, gridRecord, buy) {
						const rollSecurityPriceField = form.findField('investmentSecurity.price');
						let rollSecurityPrice = rollSecurityPriceField.getNumericValue();
						if (!rollSecurityPrice) {
							const rollSecurityId = form.findField('investmentSecurity.id').getValue();
							rollSecurityPrice = await this.getSecurityLivePrice(rollSecurityId, buy);
							if (TCG.isBlank(rollSecurityPrice)) {
								TCG.showError('Unable to obtain price for roll security. Please manually enter a price.', 'Price Failure');
								throw new Error('Unable to obtain price for roll security.');
							}
							rollSecurityPriceField.setValue(rollSecurityPrice);
						}
						return rollSecurityPrice;
					},

					getRollQuantity: function(gridRecord, closeQuantity, newRollQuantity) {
						const formPanel = TCG.getParentFormPanel(this);
						const form = formPanel.getForm();
						const securityMultiplier = form.findField('investmentSecurity.priceMultiplier').getValue();
						// adjust the available underlying quantity by the quantity being closed as available
						const openIsSell = formPanel.getFormValue('openTradeIsSell');
						// adjust the close quantity for the position; if open is sell, the position being closed is negative
						const closeUnderlyingUsageImpact = closeQuantity * securityMultiplier * (openIsSell ? -1 : 1);
						let rollQuantity = newRollQuantity || gridRecord.get('rollQuantity');
						const availableUnderlyingQuantity = this.getAvailableUnderlyingQuantity(gridRecord, closeUnderlyingUsageImpact);

						const absCloseUnderlyingUsageImpact = Math.abs(closeUnderlyingUsageImpact);
						const ignoreRollRatio = TCG.isTrue(gridRecord.data['ignoreRollRatio']);
						const rollRatioField = form.findField('rollRatio');
						let rollUnderlyingQuantityIntended = (ignoreRollRatio ? rollQuantity * securityMultiplier : absCloseUnderlyingUsageImpact * rollRatioField.getValue());
						const insufficientRollQuantity = rollUnderlyingQuantityIntended > availableUnderlyingQuantity;
						gridRecord.set('insufficientQuantity', insufficientRollQuantity);
						if (insufficientRollQuantity) {
							rollUnderlyingQuantityIntended = availableUnderlyingQuantity;
						}
						const allowedRollQuantity = Math.floor(rollUnderlyingQuantityIntended / securityMultiplier);
						if (rollQuantity !== allowedRollQuantity) {
							// adjust quantity to maximum allowed for defined trade percent
							rollQuantity = allowedRollQuantity;
						}
						return rollQuantity;
					},

					getAvailableUnderlyingQuantity: function(gridRecord, closeUnderlyingUsageImpact) {
						const target = gridRecord.data['utilizationUnderlyingTarget'];
						// close amount is always removed from covered for available to trade
						// if the closed position will be frozen, we apply the adjustment later in frozen adjustments for available
						const covered = gridRecord.data['utilizationCovered'] - closeUnderlyingUsageImpact;

						if (TCG.isBlank(target) || TCG.isBlank(covered)) {
							return 0;
						}

						let availableUnderlyingQuantity = target - covered;
						if (availableUnderlyingQuantity !== 0 && (target > 0 !== availableUnderlyingQuantity > 0)) {
							// if the available quantity is of different sign of the target, we are over utilized; adjust quantity to 0
							availableUnderlyingQuantity = 0;
						}
						availableUnderlyingQuantity = Math.abs(availableUnderlyingQuantity);
						const underlyingQuantityToTrade = gridRecord.get('underlyingQuantity');
						if (gridRecord.get('tradeUnderlying') && underlyingQuantityToTrade > 0) {
							// subtract from available quantity if selling underlying, add to available quantity if buying underlying
							availableUnderlyingQuantity += underlyingQuantityToTrade * (gridRecord.get('underlyingBuy') ? 1 : -1);
						}

						// adjust available amount for frozen shares; close impact is applied to available if sell allowed, or frozen if sell not allowed
						const frozen = this.isCloseToBeFrozen(gridRecord)
							? Math.abs(gridRecord.data['utilizationFrozen']) + Math.abs(closeUnderlyingUsageImpact)
							: Math.abs(gridRecord.data['utilizationFrozen']);
						if (frozen > 0 && availableUnderlyingQuantity > 0) {
							if (!TCG.isTrue(gridRecord.data['useFrozen'])) {
								availableUnderlyingQuantity -= frozen;
								if (availableUnderlyingQuantity < 0) {
									availableUnderlyingQuantity = 0;
								}
							}
						}

						return availableUnderlyingQuantity;
					},

					isAccountDebit: function(record) {
						const closePrice = TCG.getParentFormPanel(this).getForm().findField('closeInvestmentSecurity.price').getNumericValue();
						return closePrice && closePrice > record.data['costPerShare'];
					},

					isCloseToBeFrozen: function(record) {
						// Hard coded 10 here to validate daysToExpiration because an extra request to the server would be required to
						// determine the correct value to use based upon the client account's FREEZE_EARLY_CLOSE_DAYS_COLUMN_NAME.
						return !this.isAccountDebit(record) && record.data['daysToExpiration'] >= 10;
					},

					getUnderlyingTradeCashImpact: async function(form, gridRecord, cashBalance) {
						let underlyingCashImpact = 0;
						const underlyingEnabledCheckbox = TCG.getChildByName(this.getTopToolbar(), 'tradeUnderlyingEnabled');
						const underlyingEnabled = underlyingEnabledCheckbox ? TCG.isTrue(underlyingEnabledCheckbox.getValue()) : false;
						if (underlyingEnabled && gridRecord.data['tradeUnderlying']) {
							const underlyingId = TCG.getValue('investmentSecurity.underlyingSecurity.id', gridRecord.json);
							if (underlyingId) {
								const originalUnderlyingQuantity = gridRecord.get('underlyingQuantity') || 0;
								let underlyingQuantity = originalUnderlyingQuantity;
								const underlyingBuy = TCG.isTrue(gridRecord.data['underlyingBuy']);
								const underlyingPrice = await this.getUnderlyingPrice(form, gridRecord, underlyingId, underlyingBuy);
								const underlyingQtyEditor = this.getColumnModel().getCellEditor(this.getColumnModel().findColumnIndex('underlyingQuantity')).field;
								if ((underlyingQuantity === 0 && cashBalance < 0) || !TCG.isTrue(underlyingQtyEditor.userModified)) {
									underlyingQuantity = Math.ceil(Math.abs(cashBalance) / underlyingPrice);
								}
								const underlyingTotal = Math.abs(gridRecord.data['utilizationUnderlyingTarget']);
								if (underlyingQuantity > underlyingTotal) {
									underlyingQuantity = underlyingTotal;
								}
								if (originalUnderlyingQuantity !== underlyingQuantity) {
									gridRecord.set('underlyingQuantity', underlyingQuantity);
								}
								underlyingCashImpact = underlyingQuantity * underlyingPrice * (underlyingBuy ? -1 : 1);
								gridRecord.set('underlyingCashImpact', underlyingCashImpact);
							}
							else {
								console.warn('Trade Underlying is checked, but there is no underlying security applicable to traded securities.');
							}
						}
						else {
							gridRecord.set('tradeUnderlying', false);
						}
						return underlyingCashImpact;
					},

					getUnderlyingPrice: async function(form, gridRecord, underlyingId, buy) {
						const underlyingPriceField = form.findField('investmentSecurity.underlying.price');
						let underlyingPrice = underlyingPriceField.getNumericValue() || gridRecord.underlyingPrice || TCG.getValue('investmentSecurity.underlyingSecurity.' + (buy ? 'ASK' : 'BID'), gridRecord.json);
						if (TCG.isBlank(underlyingPrice) && TCG.isNotBlank(underlyingId)) {
							underlyingPrice = await this.getSecurityLivePrice(underlyingId, buy);
							if (TCG.isBlank(underlyingPrice)) {
								TCG.showError('Unable to obtain price for underlying security. Please manually enter a price.', 'Price Failure');
								throw new Error('Unable to obtain price for underlying security.');
							}
							underlyingPriceField.setValue(underlyingPrice);
							gridRecord.underlyingPrice = underlyingPrice;
						}
						if (gridRecord.underlyingPrice !== underlyingPrice) {
							gridRecord.underlyingPrice = underlyingPrice;
						}
						if (TCG.isBlank(underlyingPriceField.getValue()) && underlyingPrice !== underlyingPriceField.getNumericValue()) {
							underlyingPriceField.setValue(underlyingPrice);
						}
						return underlyingPrice;
					},

					onAfterEdit: function(e, field) {
						const columnModel = this.getColumnModel();
						const column = columnModel.getColumnAt(e.column);
						if (column.dataIndex === 'underlyingQuantity') {
							this.triggerUnderlyingQuantityUpdate(this, e.record);
						}
					},

					triggerUnderlyingQuantityUpdate: async function(gridPanel, record) {
						const columnModel = gridPanel.getColumnModel();
						const editor = columnModel.getCellEditor(columnModel.findColumnIndex('underlyingQuantity')).field;
						editor.userModified = true;
						try {
							await gridPanel.updateQuantityEditAware(record);
						}
						finally {
							editor.userModified = false;
						}
					},

					// Fields not applicable to TradeEntryDetail
					doNotSubmitFields: ['investmentSecurity.id', 'originalQuantity', 'rollQuantity', 'originalRollQuantity', 'utilizationUnderlyingTarget', 'utilizationTotalUsage', 'utilizationCovered', 'utilizationPending', 'utilizationFrozen', 'utilizationAvailable', 'coveredPercentClose', 'coveredPercentOpen', 'useFrozen', 'ignoreRollRatio', 'cashBalance', 'insufficientCash', 'insufficientQuantity', 'insufficientCashValue', 'netTradeCashImpact', 'ignoreCash', 'calculatedCloseQuantity', 'calculatedRollQuantity', 'validateCashMargin', 'cashMargin', 'tradeUnderlying', 'underlyingCashImpact', 'clientAccountNotes', 'costPerShare', 'accountDebit'],

					columnOverrides: [
						{
							dataIndex: 'clientInvestmentAccount.label', width: 200,
							editor: {
								xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', displayField: 'label', allowBlank: false, readOnly: true, addGridCopyContextMenu: false
							},
							renderer: function(value, metaData, record) {
								const cashBalance = TCG.getValue('cashBalance', record.json) || 0;
								const tradeCashImpact = record.get('netTradeCashImpact') || 0;
								const cashBalanceQtip = TCG.trimWhitespace(`
									<br/><br/>
									<table style="background-color:#eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td nowrap="true" style="width: 100px;">Cash Balance:</td>
											<td style="text-align:right;">${TCG.renderAmount(cashBalance, true, '$0,000')}</td>
										</tr>
										<tr>
											<td nowrap="true" style="width: 100px;">Trade Cash Impact:</td>
											<td style="text-align:right;">${TCG.renderAmount(tradeCashImpact, true, '$0,000')}</td>
										</tr>
										<tr><td colspan="5"><hr/></td></tr>
										<tr>
											<td nowrap="true" style="width: 100px;">Net Total:</td>
											<td style="text-align:right;">${TCG.renderAmount((cashBalance + tradeCashImpact), true, '$0,000')}</td>
										</tr>
									</table>
								`);
								this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(value, metaData, record, false, cashBalanceQtip);
								// no security column in this grid, so highlight client account column if there is a security restriction
								this.scope.gridPanel.applyTradeRestrictionGridSecurityColumnFreeze(metaData, record, record.underlyingSecurity);
								return value;
							},
							eventListeners: {
								contextmenu: function(column, grid, rowIndex, event) {
									const restrictionItems = grid.getRestrictionActionMenuItems(rowIndex, column);
									if (restrictionItems.length > 0) {
										const menu = new Ext.menu.Menu({
											items: [
												...restrictionItems.length > 0 ? ['-', ...restrictionItems] : []
											]
										});

										event.preventDefault();
										menu.showAt(event.getXY());
									}
								}
							}
						},
						{
							dataIndex: 'holdingInvestmentAccount.label', width: 200, renderer: TCG.renderValueWithTooltip,
							editor: {
								xtype: 'combo', url: 'investmentAccountListFind.json', displayField: 'label', allowBlank: false, addGridCopyContextMenu: false,
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const grid = combo.gridEditor.containerGrid;
									const formPanel = TCG.getParentFormPanel(grid);
									const form = formPanel.getForm();
									combo.store.baseParams = {
										ourAccount: false,
										workflowStatusNameEquals: 'Active',
										mainAccountId: TCG.getValue('clientInvestmentAccount.id', combo.gridEditor.record.json),
										mainPurpose: formPanel.getFormValue('tradeType.holdingAccountPurpose.name'),
										mainPurposeActiveOnDate: form.findField('tradeDate').value
									};
								},
								listeners: {
									change: function(combo, newValue, oldValue) {
										const gridPanel = combo.gridEditor.containerGrid;
										const record = combo.gridEditor.record;
										record.beginEdit();
										record.set('executingBrokerCompany.label', '');
										record.set('executingBrokerCompany.id', '');
										gridPanel.applyDefaultExecutingBroker(record, newValue)
											.then(() => record.endEdit());
									}
								}
							}
						},
						{
							header: 'Close Quantity', dataIndex: 'quantityIntended', tooltip: 'Quantity for the closing leg trade',
							editor: {
								xtype: 'floatfield', allowBlank: false, minValue: 0,
								listeners: {
									blur: function(field) {
										const gridPanel = field.gridEditor.containerGrid;
										const record = field.gridEditor.record;
										// update quantity, but delay it so the value change can be reflected in the record beforehand
										setTimeout(() => gridPanel.updateQuantityEditAware(record), 300);
									}
								}
							},
							renderer: function(value, metaData, record) {
								metaData.attr = TCG.renderQtip(TCG.trimWhitespace(`
									<table style="background-color:#eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td>Original:</td>
											<td style="text-align:right;">${record.data['originalQuantity']}</td>
										</tr>
										<tr>
											<td>Intended:</td>
											<td style="text-align:right;">${record.data['calculatedCloseQuantity'] || 0}</td>
										</tr>
									</table>
								`));
								return TCG.numberFormat(value, '0,000', true);
							}
						},
						{header: 'Close Security', width: 150, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', renderer: TCG.renderValueWithTooltip},
						{header: 'Original Qty', dataIndex: 'originalQuantity', type: 'float', hidden: true},
						{
							header: 'Roll Quantity', dataIndex: 'rollQuantity', width: 80, type: 'float', summaryType: 'sum',
							editor: {
								xtype: 'spinnerfield', allowBlank: false, minValue: 0,
								listeners: {
									change: function(field, newValue, oldValue) {
										const gridPanel = field.gridEditor.containerGrid;
										const record = field.gridEditor.record;
										gridPanel.updateQuantityEditAware(record, newValue);
									},
									specialkey: function(field, event) {
										if (event.getKey() === event.ENTER && this.spinner.mimicing) {
											this.spinner.triggerBlur();
										}
									}
								}
							},
							renderer: function(value, metaData, record) {
								let qtip = TCG.trimWhitespace(`
									<table style="background-color:#eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td>Original:</td>
											<td style="text-align:right;">${record.data['originalRollQuantity']}</td>
										</tr>
										<tr>
											<td>Intended:</td>
											<td style="text-align:right;">${record.data['calculatedRollQuantity'] || 0}</td>
										</tr>
									</table>
								`);
								// Attach style for restrictions involving the account
								if (record && (record.data['insufficientCash'] || record.data['insufficientQuantity'])) {
									qtip += '<br/>Insufficient ' + (record.get('insufficientQuantity')
										? 'quantity available to trade more than ' + value + '. Quantity is controlled by ' + (TCG.isTrue(record.get('ignoreRollRatio')) ? '' : 'roll ratio and ') + 'available quantity.'
										: 'cash. Net cash impact results in ' + TCG.numberFormat(record.get('insufficientCashValue'), '$0,000.00', false) + '.');
									if (record.get('insufficientCash')) {
										if (record.get('validateCashMargin')) {
											qtip += TCG.isTrue(record.get('cashMargin')) ? ' Client Investment Account is a Cash Margin account and the quantity was not adjusted.'
												: ' Client Investment Account is not a Cash Margin account and the quantity was adjusted.';
										}
										else {
											qtip += ' Client Investment Account\'s service processing type does not validate cash margin accounts and teh quantity was not adjusted.';
										}
									}
									metaData.css += 'x-grid3-row-highlight-light-red';
								}

								metaData.attr = TCG.renderQtip(qtip);
								return TCG.numberFormat(value, '0,000', true);
							}
						},
						{header: 'Original Roll Qty', dataIndex: 'originalRollQuantity', type: 'float', hidden: true},
						{dataIndex: 'expectedUnitPrice', hidden: true},
						{dataIndex: 'commissionPerUnit', hidden: true},
						{dataIndex: 'feeAmount', hidden: true},
						{
							header: 'Executing Broker', width: 150, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', useNull: false, renderer: TCG.renderValueWithTooltip,
							tooltip: 'Trade Executing Broker used for this client\'s trade. The default value is looked up by Trade Destination, active on Trade Date, and Holding Account Issuing Company. If the Holding Account defined is configured to be a Directed Broker, this value is populated to override the Executing Broker above.',
							editor: {
								xtype: 'combo', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json', tooltipField: 'label', detailPageClass: 'Clifton.business.company.CompanyWindow',
								beforequery: function(queryEvent) {
									// Reset Combo so re-queries each time (since each row has different results)
									this.resetStore();
									const editor = queryEvent.combo.gridEditor;
									const grid = editor.containerGrid;
									const form = TCG.getParentFormPanel(grid).getForm();
									queryEvent.combo.store.baseParams = {
										clientInvestmentAccountId: TCG.getValue('clientInvestmentAccount.id', editor.record.json),
										tradeDestinationId: form.findField('tradeDestination.id').getValue(),
										tradeTypeId: form.findField('tradeType.id').getValue(),
										activeOnDate: form.findField('tradeDate').value
									};
								}
							}
						},
						{
							header: 'Trade Destination', dataIndex: 'tradeDestination.name', width: 150, idDataIndex: 'tradeDestination.id', allowBlank: true,
							editor: {
								xtype: 'combo', url: 'tradeDestinationListFind.json',
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const grid = combo.gridEditor.containerGrid;
									const fp = TCG.getParentFormPanel(grid);

									// Clear Query Store so Re-Queries each time
									this.store.removeAll();
									this.lastQuery = null;

									const record = queryEvent.combo.gridEditor.record;
									const executingBrokerCompanyId = record.get('executingBrokerCompany.id');
									const tradeDate = fp.getForm().findField('tradeDate').value;
									const activeOnDate = TCG.isNotBlank(tradeDate) ? tradeDate : new Date().format('Y-m-d 00:00:00');
									queryEvent.combo.store.baseParams = {
										tradeTypeId: fp.getFormValue('tradeType.id'),
										executingBrokerCompanyId: executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : null,
										activeOnDate: activeOnDate
									};
								}
							}
						},
						{header: 'Underlying Target', dataIndex: 'utilizationUnderlyingTarget', type: 'float', hidden: true},
						{header: 'Usage Total', dataIndex: 'utilizationTotalUsage', type: 'float', hidden: true},
						{header: 'Usage Covered', dataIndex: 'utilizationCovered', type: 'float', hidden: true},
						{
							header: 'Covered % (close)', dataIndex: 'coveredPercentClose', type: 'float', width: 100, tooltip: 'Coverage % (closing trade) for security and quantity (does not include positions beyond the quantity being traded).',
							renderer: function(value, metadata, record) {
								let returnValue = 0.00;
								const securityPriceMultiplier = record.json['investmentSecurity'].priceMultiplier;
								const quantityIntended = record.json['quantityIntended'];

								if (TCG.isNotBlank(quantityIntended) && TCG.isNotBlank(securityPriceMultiplier)) {
									const shares = quantityIntended * securityPriceMultiplier;
									const utilizationUnderlyingTarget = TCG.getValue('utilizationUnderlyingTarget', record.json);
									if (TCG.isNotBlank(utilizationUnderlyingTarget)) {
										returnValue = (utilizationUnderlyingTarget === 0.00) ? 0.00 : Math.abs(shares / utilizationUnderlyingTarget * 100.0);
									}
								}
								record.data['coveredPercentClose'] = returnValue;
								return TCG.renderAmount(returnValue, false, '0,000.00 %');
							}
						},
						{
							header: 'Covered % (open)', dataIndex: 'coveredPercentOpen', type: 'float', width: 100, tooltip: 'Coverage % (opening trade) for selected security and quantity (does not include positions beyond the quantity being traded).',
							renderer: function(value, metadata, record) {
								let returnValue = 0.00;
								const form = TCG.getParentFormPanel(this.gridPanel).getForm();
								const securityPriceMultiplier = form.findField('investmentSecurity.priceMultiplier').getValue();
								const rollQuantity = TCG.getValue('rollQuantity', record.data);

								if (TCG.isNotBlank(rollQuantity) && TCG.isNotBlank(securityPriceMultiplier)) {
									const shares = rollQuantity * securityPriceMultiplier;
									const utilizationUnderlyingTarget = TCG.getValue('utilizationUnderlyingTarget', record.json);
									if (TCG.isNotBlank(utilizationUnderlyingTarget)) {
										returnValue = (utilizationUnderlyingTarget === 0.00) ? 0.00 : Math.abs(shares / utilizationUnderlyingTarget * 100.0);
									}
								}
								record.data['coveredPercentOpen'] = returnValue;
								return TCG.renderAmount(returnValue, false, '0,000.00 %');
							}
						},
						{header: 'Usage Pending', dataIndex: 'utilizationPending', type: 'float', hidden: true},
						{header: 'Usage Frozen', dataIndex: 'utilizationFrozen', type: 'float', hidden: true},
						{
							header: 'Usage Available', dataIndex: 'utilizationAvailable', type: 'float', hidden: true,
							renderer: function(value, metaData, record) {
								const underlyingQuantityToTrade = record.data['underlyingQuantity'];
								if (record.data['tradeUnderlying'] && underlyingQuantityToTrade > 0) {
									// subtract from available quantity if selling underlying, add to available quantity if buying underlying
									const buyOrSellMultiplier = (record.get('underlyingBuy') ? 1 : -1);
									const available = record.data['utilizationUnderlyingTarget'] - record.data['utilizationTotalUsage'];
									const availableMultiplier = available > 0 ? 1 : -1;
									return TCG.numberFormat((Math.abs(available) + (underlyingQuantityToTrade * buyOrSellMultiplier)) * availableMultiplier, '0,000', true);
								}
								return TCG.numberFormat(record.data['utilizationUnderlyingTarget'] - record.data['utilizationTotalUsage'], '0,000', true);
							}
						},
						{
							header: 'Cost/Share', width: 20, dataIndex: 'costPerShare', type: 'currency', negativeInRed: true, positiveInGreen: false, useNull: true, nonPersistentField: true, hidden: true,
							tooltip: 'The per share cost for this position (total cost / quantity)'
						},
						{
							header: 'Days', width: 18, dataIndex: 'daysToExpiration', type: 'float', useNull: true, nonPersistentField: true, hidden: true,
							tooltip: 'Days until expiration or maturity of this security in tenths of a day',
							renderer: (value, metadata, record) => Ext.util.Format.number(value, '0,000.0')
						},
						{
							header: 'Debit', dataIndex: 'accountDebit', type: 'boolean', width: 60, tooltip: 'Indicates whether this roll is for an account debit (Cost/Share < Close Price) or an account credit (Cost/Share > Close Price).',
							renderer: function(value, metaData, record) {
								return TCG.renderBoolean(this.gridPanel.isAccountDebit(record));
							}
						},
						{
							header: 'Use Frozen', dataIndex: 'useFrozen', xtype: 'checkcolumn', width: 60, tooltip: 'The ability to use frozen shares/contracts will be only be used if this roll is for an account debit.',
							onClick: (grid, record) => grid.updateQuantityEditAware(record)
						},
						{
							header: 'Ignore Ratio', dataIndex: 'ignoreRollRatio', xtype: 'checkcolumn', width: 60,
							tooltip: 'If checked, the roll ratio will not be applied to the row when updating quantities to support manual quantity overrides',
							onClick: (grid, record) => grid.updateQuantityEditAware(record)
						},
						{header: 'Cash', dataIndex: 'cashBalance', type: 'float', hidden: true},
						{
							header: 'Ignore Cash', dataIndex: 'ignoreCash', xtype: 'checkcolumn', width: 60,
							tooltip: 'If checked, the validation for cash will not automatically adjust the trade quantity allowing trades to be created',
							onClick: (grid, record) => {
								record.beginEdit();
								record.set('quantityIntended', record.get('originalQuantity'));
								grid.updateQuantityEditAware(record);
							}
						},
						{
							header: 'Trade Und.', dataIndex: 'tradeUnderlying', xtype: 'checkcolumn', width: 60, hidden: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
							tooltip: 'If checked, the underlying quantity will be used to trade the underlying security in addition to the securities included in the roll',
							onClick: (grid, record) => grid.triggerUnderlyingQuantityUpdate(grid, record)
						},
						{
							header: 'Und. Quantity', dataIndex: 'underlyingQuantity', width: 80, type: 'float', summaryType: 'sum', hidden: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
							tooltip: 'The quantity of the underlying security to trade. The value is only used if the Trade Underlying cell value is checked for the row.',
							editor: {xtype: 'spinnerfield', allowBlank: false, minValue: 0},
							renderer: function(value, metaData, record) {
								if (value && value > 0) {
									metaData.attr = TCG.renderQtip(TCG.trimWhitespace(`
									<table style="background-color:#eee; border-collapse: separate; border-spacing: 5px;">
										<tr>
											<td>Underlying Price:</td>
											<td style="text-align:right;">${TCG.renderAmount(record.underlyingPrice, false, '$0,000')}</td>
										</tr>
										<tr><td colspan="2"><hr/></td></tr>
										<tr>
											<td>Cash Impact:</td>
											<td style="text-align:right;">${TCG.renderAmount(record.data['underlyingCashImpact'], true, '$0,000')}</td>
										</tr>
									</table>
								`));
								}
								return TCG.numberFormat(value, '0,000', true);
							}
						},
						{
							header: 'Und. Buy', dataIndex: 'underlyingBuy', xtype: 'checkcolumn', width: 60, hidden: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
							tooltip: 'If checked, the underlying trade will be a buy of the specified underlying quantity. If unchecked, the underlying trade will be a sell of the specified underlying quantity. This flag is only applicable if Trade Underlying is checked for this row.',
							onClick: (grid, record) => grid.updateQuantityEditAware(record)
						},
						{header: 'Account Notes', width: 200, dataIndex: 'clientAccountNotes', renderer: TCG.renderTextNowrapWithTooltip}
					]
				}
			],

			/**
			 * Checks the accounts proposed for this trade against trade restrictions to determine if the trade should be blocked.
			 * @returns {*} an object containing an {@link Array} of valid trades and an {@link Array} of trades that have trade restrictions.
			 *
			 * Note:  only checks restriction for the open trade.  The close trade restrictions have already been checked before this point.
			 *
			 */
			checkTradeToRollTradeRestrictions: function() {
				const fp = this;
				const tradeGrid = fp.getChildTradeEntryGrid();
				const tradeDetailList = tradeGrid.store.data.items;
				const openTradeIsSell = fp.getFormValue('openTradeIsSell');
				const underlyingPrice = fp.getFormValue('underlyingPrice');
				const investmentSecurity = fp.getForm().findField('investmentSecurity.id').selectedSecurity;
				const underlyingSecurityId = investmentSecurity.underlyingSecurity.id;
				const [validPositions, restrictedPositions] = tradeDetailList.partition(tradeDetail => tradeGrid.checkTradeRestrictions(tradeDetail.json.clientInvestmentAccount.id, investmentSecurity, underlyingSecurityId, underlyingPrice, openTradeIsSell, true));
				return {restrictedPositions, validPositions};
			}
		}
	]
});
