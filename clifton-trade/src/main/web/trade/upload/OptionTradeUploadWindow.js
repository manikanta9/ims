TCG.use('Clifton.trade.upload.TradeUploadForm');
TCG.use('Clifton.trade.upload.TradeUploadWindowBase');

Clifton.trade.upload.OptionTradeUploadWindow = Ext.extend(Clifton.trade.upload.TradeUploadWindowBase, {
	id: 'tradeUploadOptionsWindow',
	title: 'Options Trade Import',

	importTab: {
		title: 'Trade Import',
		tbar: [{
			text: 'Options File (Options Upload)',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File for Options Trade Uploads',
			handler: function() {
				TCG.openFile('trade/upload/OptionTradeUploadFileSample.xls');
			}
		}],
		items: [{
			xtype: 'trade-upload-form',
			allowBlankTraderAndTradeDate: true,
			simpleTradeTypeName: 'Options',
			simpleItems: [
				{fieldLabel: 'Settlement Date', name: 'simpleSettlementDate', xtype: 'datefield', qtip: 'If blank, and not defined for the trade in the file, each trade will calculate correct settlement date based on the Trade Date and security.'},
				{
					fieldLabel: 'Trade Destination', name: 'simpleTradeDestination.name', width: 250, hiddenName: 'simpleTradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();
							combo.store.baseParams = {
								tradeTypeId: f.findField('simpleTradeType.id').getValue(),
								activeOnDate: f.findField('tradeGroup.tradeDate').getValue().format('m/d/Y')
							};
						},
						select: function(combo, record, index) {
							const fp = combo.getParentForm();
							// clear existing destinations
							const dst = fp.getForm().findField('simpleExecutingBrokerCompany.label');
							dst.store.removeAll();
							dst.lastQuery = null;
							fp.updateExecutingBrokerCompany(record.id);
						}
					}
				},
				{
					fieldLabel: 'Executing Broker', name: 'simpleExecutingBrokerCompany.label', hiddenName: 'simpleExecutingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json',
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();
							combo.store.baseParams = {
								tradeDestinationId: f.findField('simpleTradeDestination.name').getValue(),
								tradeTypeId: f.findField('simpleTradeType.id').getValue(),
								activeOnDate: f.findField('tradeGroup.tradeDate').getValue().format('m/d/Y')
							};
						}
					}
				},
				{xtype: 'label', html: 'Trades without a specific description in the upload file will also have this note as the Trade Description.'},
				{fieldLabel: 'Notes', name: 'tradeGroup.note', xtype: 'textarea'},
				{xtype: 'hidden', name: 'simple', value: 'true'},
				{xtype: 'checkbox', name: 'createMissingSecurities', fieldLabel: '', boxLabel: 'Create Missing Option Securities', qtip: 'If checked, and option security is missing, the system will attempt to create the option first before the trade.  Note that additional fields are required if the Option security needs to be created.'}
			]
		}]
	}

});
