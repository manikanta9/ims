Clifton.trade.upload.TradeUploadWindowBase = Ext.extend(TCG.app.DetailWindow, {
	title: 'Trade Import',
	iconCls: 'import',
	height: 600,
	hideOKButton: true,
	saveTimeout: 240,


	init: function() {
		// replace the first tab with the override
		const tabs = this.items[0].items;
		tabs[0] = this.importTab;
		Clifton.trade.upload.TradeUploadWindowBase.superclass.init.apply(this, arguments);
	},

	importTab: {
		title: 'OVERRIDE ME',
		items: [{}]
	},

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{}, // first tab to be overridden
			{
				title: 'Import History',
				items: [{
					name: 'tradeGroupListFind',
					xtype: 'gridpanel',
					instructions: 'The following trade groups have been imported into the system.',
					columns: [
						{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
						{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Note', width: 100, dataIndex: 'note', filter: false},
						{header: 'Trader', width: 13, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', displayField: 'label', url: 'securityUserListFind.json'}},
						{header: 'Traded On', width: 15, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of trades
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -30)});
						}
						return {
							import: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
