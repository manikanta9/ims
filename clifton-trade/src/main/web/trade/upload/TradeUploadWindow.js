TCG.use('Clifton.trade.upload.TradeUploadForm');
TCG.use('Clifton.trade.upload.TradeUploadWindowBase');

Clifton.trade.upload.TradeUploadWindow = Ext.extend(Clifton.trade.upload.TradeUploadWindowBase, {
	id: 'tradeUploadWindow',

	importTab: {
		title: 'Trade Import',
		tbar: [
			{
				text: 'Sample File',
				iconCls: 'excel',
				tooltip: 'Download Excel Sample File',
				handler: function() {
					Clifton.system.downloadSampleUploadFile('Trade', false, this);
				}
			}, '-', {
				text: 'Sample File (All Data)',
				iconCls: 'excel',
				tooltip: 'Download Excel Sample File (Contains All (up to 500 rows) Existing Data)',
				handler: function() {
					Clifton.system.downloadSampleUploadFile('Trade', true, this);
				}
			}, '-', {
				text: 'Sample File (Simple Upload)',
				iconCls: 'excel',
				tooltip: 'Download Excel Sample File for Simple Trade Uploads',
				handler: function() {
					TCG.openFile('trade/upload/SimpleTradeUploadFileSample.xls');
				}
			}, {
				xtype: 'tbfill'
			}, {
				text: 'Trade Fill Import',
				iconCls: 'import',
				tooltip: 'Import Trade Fills for Existing Trades',
				handler: function() {
					TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
				}
			}
		],
		items: [
			{
				xtype: 'trade-upload-form',
				allowBlankTraderAndTradeDate: true,

				simpleItems: [
					{xtype: 'label', html: 'Trades without a specific description in the upload file will also have this note as the Trade Description.'},
					{fieldLabel: 'Notes', name: 'tradeGroup.note', xtype: 'textarea'},
					{
						xtype: 'fieldset-checkbox',
						title: 'Simple Trade Uploads',
						checkboxName: 'simple',
						instructions: 'Simple Trade Uploads allow setting less columns in the upload file and the system will make certain assumptions in order to correctly lookup data based on less information.' +
							'<ul class="c-list">Trade Type selection below is required and will apply to all rows in the file.  Executing Broker can optionally be selected and will be set for each trade where not explicitly set in the file.  The following Columns apply to Simple Trade Uploads:' +
							'<li>Required: ClientInvestmentAccount-Number, InvestmentSecurity-Symbol, QuantityIntended, AverageUnitPrice, Buy</li>' +
							'<li>Optional: HoldingInvestmentAccount-Number, ExecutingBrokerCompany-Name, and other Trade fields (i.e. Description, CommissionPerUnit, etc.)</li>' +
							'</ul>',

						items: [
							// Entity Modify Fields
							{fieldLabel: 'Settlement Date', name: 'simpleSettlementDate', xtype: 'datefield', qtip: 'Most times can leave blank and each trade will calculate correct settlement date based on the Trade Date and security.  For Forwards, will default to Trade Date so it is implied to not be an early settlement.'},
							{
								fieldLabel: 'Trade Type', name: 'simpleTradeType.name', hiddenName: 'simpleTradeType.id', xtype: 'combo', url: 'tradeTypeListFind.json',
								listeners: {
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										// clear existing destinations
										let dst = fp.getForm().findField('simpleTradeDestination.name');
										dst.store.removeAll();
										dst.setValue('');
										dst.lastQuery = null;

										dst = fp.getForm().findField('simpleExecutingBrokerCompany.label');
										dst.setValue('');
										dst.store.removeAll();
										dst.lastQuery = null;
									}
								}
							},
							{fieldLabel: 'Trade Import Type', name: 'tradeGroup.tradeGroupType.name', hiddenName: 'tradeGroup.tradeGroupType.id', xtype: 'combo', url: 'tradeGroupTypeListFind.json?tradeImport=true'},
							{
								fieldLabel: 'Trade Destination', name: 'simpleTradeDestination.name', width: 250, hiddenName: 'simpleTradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										combo.store.baseParams = {
											tradeTypeId: f.findField('simpleTradeType.id').getValue(),
											activeOnDate: f.findField('tradeGroup.tradeDate').getValue().format('m/d/Y')
										};
									},
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										// clear existing destinations
										const dst = fp.getForm().findField('simpleExecutingBrokerCompany.label');
										dst.store.removeAll();
										dst.lastQuery = null;
										fp.updateExecutingBrokerCompany(record.id);
									}
								}
							},
							{
								fieldLabel: 'Executing Broker', name: 'simpleExecutingBrokerCompany.label', hiddenName: 'simpleExecutingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json',
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										combo.store.baseParams = {
											tradeDestinationId: f.findField('simpleTradeDestination.name').getValue(),
											tradeTypeId: f.findField('simpleTradeType.id').getValue(),
											activeOnDate: f.findField('tradeGroup.tradeDate').getValue().format('m/d/Y')
										};
									}
								}
							},
							{fieldLabel: 'Settlement CCY', name: 'simplePayingSecurity.label', hiddenName: 'simplePayingSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true', qtip: 'Note: PayingSecurity-Symbol is the column in the upload file for settlement currency.'}
						]
					}
				]
			}
		]
	}
});
