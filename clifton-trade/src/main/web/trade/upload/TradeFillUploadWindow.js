Clifton.trade.upload.TradeFillUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'tradeFillUploadWindow',
	title: 'Trade Fill Import',
	iconCls: 'import',
	height: 600,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('TradeFill', false, this);
		}
	}, '-', {
		text: 'Sample File (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File (Contains All (up to 500 rows) Existing Data)',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('TradeFill', true, this);
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Trade Fill Uploads allow you to fill trades and optionally execute them from files directly into the system.  Use the options below to customize how to upload your data.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{boxLabel: 'Automatically execute fully filled trades. (If a trade fill in the file has a quantity that fully fills a trade, the trade will automatically be executed.)', name: 'executeFilledTrades', xtype: 'checkbox'},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit', items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}

		],

		getSaveURL: function() {
			return 'tradeFillUploadFileUpload.json';
		}
	}]
});
