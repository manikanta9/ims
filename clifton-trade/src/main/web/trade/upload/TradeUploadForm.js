Clifton.trade.upload.TradeUploadForm = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false,
	loadDefaultDataAfterRender: true,
	fileUpload: true,
	instructions: 'Trade Uploads allow you to import trades from files directly into the system.  Use the options below to customize how to upload your data.',
	simpleTradeTypeName: undefined, // Override if form is for specific trade type
	allowBlankTraderAndTradeDate: false,

	initComponent: function() {
		const currentItems = [];
		// If Specific Trade Type - Put First as Link Field
		if (this.simpleTradeTypeName) {
			currentItems.push({fieldLabel: 'Trade Type', name: 'simpleTradeType.name', hiddenName: 'simpleTradeType.id', detailIdField: 'simpleTradeType.id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TypeWindow'});
		}
		// Then Common Trade Upload Items
		const allowBlankTraderAndTradeDate = this.allowBlankTraderAndTradeDate;
		Ext.each(this.commonItems, function(f) {
			if (f.name === 'tradeGroup.traderUser.label' || f.name === 'tradeGroup.tradeDate') {
				f.allowBlank = allowBlankTraderAndTradeDate;
			}
			currentItems.push(f);
		});

		// Then Screen Specific Simple Upload Options
		Ext.each(this.simpleItems, function(f) {
			currentItems.push(f);
		});

		// Then Import Results Area
		Ext.each(this.importResultItems, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;
		Clifton.trade.upload.TradeUploadForm.superclass.initComponent.call(this);
	},


	getDefaultData: async function(win) {
		let dd = win.defaultData || {};
		if (win.defaultDataIsReal) {
			return dd;
		}
		const now = new Date().format('Y-m-d 00:00:00');
		if (this.simpleTradeTypeName) {
			const type = TCG.data.getData('tradeTypeByName.json?name=' + this.simpleTradeTypeName, this, 'trade.type.' + this.simpleTradeTypeName);
			dd = Ext.apply({
				simpleTradeType: type
			}, dd);
		}
		const tradeGroupType = await TCG.data.getDataPromiseUsingCaching('tradeGroupTypeByName.json?name=Trade Import', this, 'trade.group.type.Trade Import');
		dd = Ext.apply({
			tradeGroup: {
				traderUser: TCG.getCurrentUser(),
				tradeDate: now,
				tradeGroupType: tradeGroupType
			}
		}, dd);
		return dd;
	},
	updateExecutingBrokerCompany: function(tradeDestinationId) {
		const f = this.getForm();
		const broker = f.findField('simpleExecutingBrokerCompany.label');
		if (broker.isVisible()) {
			tradeDestinationId = tradeDestinationId || f.findField('simpleTradeDestination.name').getValue();
			const tradeDate = f.findField('tradeGroup.tradeDate').getValue().format('m/d/Y');
			if (tradeDestinationId) {
				const loader = new TCG.data.JsonLoader({
					waitTarget: this,
					params: {
						tradeTypeId: f.findField('simpleTradeType.id').getValue(),
						tradeDestinationId: tradeDestinationId,
						activeOnDate: tradeDate
					},
					onLoad: function(rec, conf) {
						if (rec && rec.executingBrokerCompany) {
							broker.setValue({value: rec.executingBrokerCompany.id, text: rec.executingBrokerCompany.label});
							f.formValues.executingBrokerCompany = rec.executingBrokerCompany; // needs this in order to find the record when tabbing out when the store isn't initialized
						}
						else {
							broker.reset();
							broker.clearValue();
						}
					}
				});
				loader.load('tradeDestinationMappingByCommand.json');
			}
		}
	},
	commonItems: [
		{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
		{
			fieldLabel: 'Trader', name: 'tradeGroup.traderUser.label', hiddenName: 'tradeGroup.traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false',
			qtip: 'If not selected, the trader user is required for each trade in the file. For the group, the first trade\'s trader will be set at the group level.'
		},
		{
			fieldLabel: 'Trade Date', name: 'tradeGroup.tradeDate', xtype: 'datefield',
			qtip: 'If not selected, the trade date is required for each trade in the file.  For the group, the first trade\'s trade date will be set at the group level.'
		}
	],
	// Override with additional options
	simpleItems: [],
	importResultItems: [
		{
			xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
			items: [
				{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
			]
		}
	],

	getSaveURL: function() {
		return 'tradeUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&requestedMaxDepth=2';
	}
});
Ext.reg('trade-upload-form', Clifton.trade.upload.TradeUploadForm);
