TCG.use('Clifton.trade.upload.TradeUploadForm');
TCG.use('Clifton.trade.upload.TradeUploadWindowBase');

Clifton.trade.upload.CurrencyTradeUploadWindow = Ext.extend(Clifton.trade.upload.TradeUploadWindowBase, {
	id: 'tradeUploadCurrencyWindow',
	title: '',

	init: function() {
		if (this.simpleTradeTypeName) {
			this.importTab.items[0].simpleTradeTypeName = this.simpleTradeTypeName;

			const tbarItems = this.importTab.tbarItems;
			const tbar = [];
			if (this.simpleTradeTypeName === 'Currency') {
				this.title = 'Currency Trade Import';
				this.id = 'tradeUploadCurrencyWindow';
				tbar.push(tbarItems[1]);
			}
			else if (this.simpleTradeTypeName === 'Forwards') {
				this.title = 'Forwards Trade Import';
				this.id = 'tradeUploadCurrencyForwardWindow';
				tbar.push(tbarItems[0]);
			}
			else {
				throw {message: '[' + this.simpleTradeTypeName + '] is not supported by the CurrencyTradeUploadWindow screen.'};
			}
			this.importTab.tbar = tbar;
		}
		Clifton.trade.upload.CurrencyTradeUploadWindow.superclass.init.apply(this, arguments);
	},

	importTab: {
		title: 'Trade Import',
		tbarItems: [{
			text: 'Sample Forwards File',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File for Forwards Trade Uploads',
			handler: function() {
				TCG.openFile('trade/upload/CurrencyForwardTradeUploadFileSample.xls');
			}
		}, {
			text: 'Sample Currency File',
			iconCls: 'excel',
			tooltip: 'Download Excel Sample File for Forwards Trade Uploads',
			handler: function() {
				TCG.openFile('trade/upload/CurrencyTradeUploadFileSample.xls');
			}
		}],
		items: [{
			xtype: 'trade-upload-form',
			simpleItems: [
				{fieldLabel: 'Settlement Date', name: 'simpleSettlementDate', xtype: 'datefield', qtip: 'Most times can leave blank and each trade will calculate correct settlement date based on the Trade Date and security.  For Forwards, will default to Trade Date so it is implied to not be an early settlement.'},
				{
					fieldLabel: 'Trade Destination', name: 'simpleTradeDestination.name', width: 250, hiddenName: 'simpleTradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();
							combo.store.baseParams = {
								tradeTypeId: f.findField('simpleTradeType.id').getValue(),
								activeOnDate: f.findField('tradeGroup.tradeDate').getValue().format('m/d/Y')
							};
						}
					}
				},
				{
					fieldLabel: 'Executing Broker', name: 'simpleExecutingBrokerCompany.label', hiddenName: 'simpleExecutingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json',
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();
							combo.store.baseParams = {
								tradeDestinationId: f.findField('simpleTradeDestination.name').getValue(),
								tradeTypeId: f.findField('simpleTradeType.id').getValue(),
								activeOnDate: f.findField('tradeGroup.tradeDate').getValue().format('m/d/Y')
							};
						}
					}
				},
				{xtype: 'label', html: 'Trades without a specific description in the upload file will also have this note as the Trade Description.'},
				{fieldLabel: 'Notes', name: 'tradeGroup.note', xtype: 'textarea'},
				{xtype: 'hidden', name: 'simple', value: 'true'},
				{xtype: 'hidden', name: 'custom', value: 'true'}
			]
		}]
	}
});
