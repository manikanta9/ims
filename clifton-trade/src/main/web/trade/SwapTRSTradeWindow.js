TCG.use('Clifton.trade.SwapTradeFormBase');
TCG.use('Clifton.investment.instrument.SwapWindow');

Clifton.trade.SwapTRSTradeForm = Ext.extend(Clifton.trade.SwapTradeFormBase, {
	investmentTypeSubType: 'Total Return Swaps',

	setSecurityCustomFields: function(values) {
		const fp = this;
		let v = TCG.getArrayElement(values, 'column.name', 'Reference Rate');
		if (v) {
			fp.setFormValue('referenceRate.id', v.value, true);
			fp.setFormValue('referenceRate.label', v.text, true);
		}
		v = TCG.getArrayElement(values, 'column.name', 'Spread');
		if (v) {
			fp.setFormValue('spread', v.text, true);
		}
		v = TCG.getArrayElement(values, 'column.name', 'Day Count');
		if (v) {
			fp.setFormValue('daycountConvention', v.text, true);
		}
		v = TCG.getArrayElement(values, 'column.name', 'Reset Frequency');
		if (v) {
			fp.setFormValue('resetFrequency', v.text, true);
		}
		v = TCG.getArrayElement(values, 'column.name', 'Commission');
		if (v) {
			fp.setFieldQtip('commissionAmount', 'Enter total Commission amount (' + v.text + ' bps of Accounting Notional)');
		}
		v = TCG.getArrayElement(values, 'column.name', 'Termination Fee');
		if (v) {
			fp.setFieldQtip('feeAmount', 'Enter total Termination Fee amount (' + v.text + ' bps of Accounting Notional) if position is closed before Maturity Date');
		}

		TCG.data.getDataPromise('investmentSecurityEventTypeAllowedForSecurity.json?eventTypeName=Dividend Leg Payment&securityId=' + fp.getFormValue('investmentSecurity.id'), this)
			.then(function(allowed) {
				if (TCG.isFalse(allowed)) {
					// hide Dividend Leg field
					fp.hideField('accrualAmount2');
				}
			});
	},

	updateFromTradeDateChange: function() {
		this.updateAccruedInterest();
	},

	updateCalculatedValues: function(updatedField, newValue, oldValue) {
		const fp = this;
		const f = fp.getForm();

		Promise.resolve()
			.then(function() {
				if (updatedField === 'averageUnitPrice' || updatedField === 'quantityIntended') {
					const price = f.findField('averageUnitPrice').getNumericValue();
					const qty = f.findField('quantityIntended').getNumericValue();
					if (Ext.isNumber(price) && Ext.isNumber(qty)) {
						// accounts for rounding: JPY, etc.
						const securityId = f.findField('investmentSecurity.id').value;
						return TCG.data.getDataValuePromise('investmentSecurityNotional.json?securityId=' + securityId + '&price=' + price + '&quantity=' + qty, fp)
							.then(function(notional) {
								fp.setFormValue('accountingNotional', notional);
							});
					}
				}
			})
			.then(function() {
				if ((TCG.isNull(updatedField) && !TCG.isTrue(this.readOnlyMode) && !f.findField('accrualAmount1').value) || updatedField === 'quantityIntended' || updatedField === 'averageUnitPrice') {
					fp.updateAccruedInterest();
				}
			});
	},

	updateAccruedInterest: function() {
		const fp = this;
		const f = fp.getForm();
		const securityId = f.findField('investmentSecurity.id').value;
		const qty = f.findField('quantityIntended').getNumericValue();
		const price = f.findField('averageUnitPrice').getNumericValue();
		// 210616214A early termination on 12/31/2014 (need transaction date here: period end [Daily High Rate Swap])
		// may be better to look at instrument accrual date calculator override here. For daily high rate we don't know future rates so can't use settlement date
		const date = f.findField('settlementDate').value;
		return Promise.resolve()
			.then(function() {
				if (date && TCG.isNotNull(securityId) && Ext.isNumber(qty) && Ext.isNumber(price)) {
					return Promise.all([
							TCG.data.getDataValuePromise('tradeAccrualAmount1.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(f.el.dom, true), fp),
							TCG.data.getDataValuePromise('tradeAccrualAmount2.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(f.el.dom, true), fp)
						]
					).then(function(ais) {
						fp.setFormValue('accrualAmount1', fp.normalizeAccrualAmount(ais[0]), true);
						fp.setFormValue('accrualAmount2', fp.normalizeAccrualAmount(ais[1]), true);
					});
				}
			});
	},

	normalizeAccrualAmount: function(v) {
		if (TCG.isBlank(v) || TCG.isEquals(v, 0)) {
			return '';
		}
		return v;
	},

	securityEntryItems: [
		{
			xtype: 'panel',
			layout: 'column',
			labelWidth: 120,
			items: [
				{
					columnWidth: .42,
					layout: 'form',
					defaults: {xtype: 'textfield'},
					items: [
						{
							xtype: 'radiogroup',
							fieldLabel: 'Buy/Sell',
							columns: [60, 60],
							allowBlank: false,
							anchor: '-114',
							items: [
								{boxLabel: 'Buy', name: 'buy', inputValue: true},
								{boxLabel: 'Sell', name: 'buy', inputValue: false}
							],
							listeners: {
								change: function(radioGroup, checkedRadio) {
									const formPanel = TCG.getParentFormPanel(radioGroup);
									const params = {
										tradeTypeId: formPanel.getFormValue('tradeType.id'),
										buy: checkedRadio.getRawValue()
									};
									TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params})
										.then(function(openCloseType) {
											if (openCloseType && openCloseType.length === 1) {
												formPanel.getForm().findField('openCloseType.id').setValue(openCloseType[0].id);
											}
										});
								}
							}
						},
						{xtype: 'hidden', name: 'openCloseType.id'},
						{
							fieldLabel: 'Units', name: 'quantityIntended', xtype: 'floatfield', minValue: 0.00000001, maxValue: 1000000000, allowBlank: false,
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = TCG.getParentFormPanel(field);
									fp.updateCalculatedValues.call(fp, 'quantityIntended', newValue, oldValue);
								}
							}
						},
						{
							fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'pricefield', allowBlank: false,
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = TCG.getParentFormPanel(field);
									fp.updateCalculatedValues.call(fp, 'averageUnitPrice', newValue, oldValue);
								}
							}
						},
						{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', qtip: 'The value to exchange one unit of the security\'s currency denomination for one unit of the client account\'s base currency. If settlement currency of the trade does not match the security\'s or client\'s currency, then the system will calculate the crossing rate value via the exchange rate on the trade multiplied by default data source exchange rate from settlement currency on the trade to client account\'s base currency.'}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					labelWidth: 100,
					items: [
						{fieldLabel: 'Reference Rate', name: 'referenceRate.label', xtype: 'linkfield', detailPageClass: 'Clifton.marketdata.rates.InterestRateIndexWindow', detailIdField: 'referenceRate.id', submitDetailField: false},
						{fieldLabel: 'Spread (bps)', name: 'spread', xtype: 'floatfield', readOnly: true, width: 110},
						{fieldLabel: 'Reset Frequency', name: 'resetFrequency', xtype: 'displayfield', readOnly: true},
						{fieldLabel: 'Day Count', name: 'daycountConvention', xtype: 'displayfield', readOnly: true}
					]
				},
				{
					columnWidth: .29,
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					items: [
						{fieldLabel: 'Current Notional', name: 'accountingNotional', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Click to see details about Trade Amounts'},
						{fieldLabel: 'Interest Leg', name: 'accrualAmount1', xtype: 'currencyfield', qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
						{fieldLabel: 'Dividend Leg', name: 'accrualAmount2', xtype: 'currencyfield', qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
						{fieldLabel: 'Commission', name: 'commissionAmount', xtype: 'currencyfield'},
						{fieldLabel: 'Termination Fee', name: 'feeAmount', xtype: 'currencyfield'}
					]
				}
			]
		}
	],

	securityEntryItems_readOnly: [{
		xtype: 'panel',
		layout: 'column',
		labelWidth: 120,
		items: [
			{
				columnWidth: .42,
				layout: 'form',
				defaults: {xtype: 'textfield'},
				items: [
					{
						fieldLabel: 'Buy/Sell', name: 'buy', xtype: 'displayfield', width: 139,
						setRawValue: function(v) {
							this.el.addClass(v ? 'buy' : 'sell');
							TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
						}
					},
					{fieldLabel: 'Units', name: 'quantityIntended', xtype: 'floatfield', readOnly: true},
					{fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true},
					{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', readOnly: true, qtip: 'The value to exchange one unit of the security\'s currency denomination for one unit of the client account\'s base currency. If settlement currency of the trade does not match the security\'s or client\'s currency, then the system will calculate the crossing rate value via the exchange rate on the trade multiplied by default data source exchange rate from settlement currency on the trade to client account\'s base currency.'}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				labelWidth: 100,
				items: [
					{fieldLabel: 'Reference Rate', name: 'referenceRate.label', xtype: 'linkfield', detailPageClass: 'Clifton.marketdata.rates.InterestRateIndexWindow', detailIdField: 'referenceRate.id', submitDetailField: false},
					{fieldLabel: 'Spread (bps)', name: 'spread', xtype: 'floatfield', readOnly: true, width: 110},
					{fieldLabel: 'Reset Frequency', name: 'resetFrequency', xtype: 'displayfield', readOnly: true},
					{fieldLabel: 'Day Count', name: 'daycountConvention', xtype: 'displayfield', readOnly: true}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{fieldLabel: 'Current Notional', name: 'accountingNotional', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Click to see details about Trade Amounts'},
					{fieldLabel: 'Interest Leg', name: 'accrualAmount1', xtype: 'currencyfield', readOnly: true, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
					{fieldLabel: 'Dividend Leg', name: 'accrualAmount2', xtype: 'currencyfield', readOnly: true, qtip: 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)'},
					{fieldLabel: 'Commission', name: 'commissionAmount', xtype: 'currencyfield', readOnly: true},
					{fieldLabel: 'Termination Fee', name: 'feeAmount', xtype: 'currencyfield', readOnly: true}
				]
			}
		]
	}],

	populateClosingPositionBuySell: function(positionRecord, force) {
		const form = this.getForm();
		const quantity = this.getClosingPositionQuantity.call(this, positionRecord);
		const buy = quantity < 0;
		const existingBuySelection = form.items.find(i => i.fieldLabel === 'Buy/Sell').getValue();
		if (TCG.isBlank(existingBuySelection) || TCG.isTrue(force)) {
			form.items.find(i => i.fieldLabel === 'Buy/Sell').items.get((buy) ? 0 : 1).setValue(true);
		}
		else if (existingBuySelection.inputValue !== buy) {
			TCG.showError('Unable to add closing position because it would does not satisfy the current buy/sell selection.', 'Unsupported Position');
			return false;
		}
		return true;
	}
});
Ext.reg('trade-swap-trs-form', Clifton.trade.SwapTRSTradeForm);


Clifton.trade.SwapTRSTradeEventTab = Ext.apply(Ext.apply({}, Clifton.investment.instrument.SwapTRSWindowResetsTab), {
	getSecurityId: function() {
		const w = this.ownerCt.getWindow();
		return w.getMainFormPanel().getFormValue('investmentSecurity.id');
	},

	gridConfig: {
		editor: {
			getDefaultData: function(gridPanel) {
				return {security: gridPanel.getWindow().getMainForm().formValues.investmentSecurity};
			}
		},
		getLoadParams: function() {
			return {'securityId': this.getWindow().getMainFormPanel().getFormValue('investmentSecurity.id')};
		}
	}
});


Clifton.trade.SwapTRSTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 1000,
	height: 600,
	iconCls: 'swap',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,

		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.trade.DefaultTradeWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.trade.DefaultTradeWindowAdditionalTabs[i]);
				}
			}
		},

		items: [
			{
				title: 'Info',
				tbar: {xtype: 'trade-workflow-toolbar'},

				items: [{
					xtype: 'trade-swap-trs-form'
				}]
			},


			{
				title: 'Compliance',
				items: [{
					xtype: 'trade-violations-grid'
				}]
			},


			{
				title: 'Trade Fills',
				items: [{xtype: 'trade-fillsgrid'}]
			},


			{
				title: 'Commissions and Fees',
				items: [{
					xtype: 'trade-commissions'
				}]
			},


			Clifton.trade.SwapTRSTradeEventTab,


			{
				title: 'Quotes',
				items: [{
					xtype: 'trade-quotes'
				}]
			}
		]
	}]
});
