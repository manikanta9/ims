TCG.use('Clifton.trade.monitor.BaseTradeModalWindow');

Clifton.trade.TradeReassignWindow = Ext.extend(Clifton.trade.monitor.BaseTradeModalWindow, {
	title: 'Reassign Trade',
	iconCls: 'shopping-cart',
	okButtonText: 'Reassign Trades',
	okButtonTooltip: 'Reassign trades based on the details entered in this window',

	submitTradesCallback: async function(formPanel) {
		const grid = formPanel.getChildTradeEntryGrid();
		const fp = grid.ownerCt;
		const newAssigneeObject = fp.getForm().findField('newAssignee.id').getValueObject();
		const tradeList = grid.getStore().data.items.map(record => ({
			'class': 'com.clifton.trade.Trade',
			'id': TCG.getValue('id', record)
		}));
		const params = {
			enableValidatingBinding: true,
			disableValidatingBindingValidation: true,
			'newAssignee.id': newAssigneeObject.id,
			'actionType': 'REASSIGN',
			beanList: JSON.stringify(tradeList)
		};
		TCG.data.getDataPromise('tradeListActionCommandExecute.json', formPanel, {
			waitMsg: 'Reassigning Trades...',
			timeout: 10000,
			params: params
		}).then(result => {
			const window = formPanel.getWindow();
			TCG.createComponent('Clifton.core.StatusWindow', {
				title: 'Trade Reassignment Status',
				defaultData: {status: result}
			});
			window.openerCt.reload();
			window.closeWindow();
		});
	},

	items: [
		{
			xtype: 'base-modal-trade-formpanel',
			instructions: 'A trade can be reassigned when in the status of: Draft, Pending, or Approved. The new assignee must have WRITE access to Trade.',
			includeSecurityDragAndDrop: false,

			setDefaultTradeDestination: () => Promise.resolve(),
			formPanelSecurityColumnItems: [
				{fieldLabel: 'New Assignee', name: 'newAssignee.label', hiddenName: 'newAssignee.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, limitRequestedProperties: false}
			],
			formPanelExecutionColumnItems: [],
			formPanelTradeDateColumnItems: [],
			formPanelBottomItems: [
				{
					xtype: 'trade-group-entry-grid',
					title: 'Trades',
					readOnly: true,
					plugins: {ptype: 'gridsummary'},
					columnsConfig: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client Account', width: 35, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true},
						{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
						{header: 'Quantity', width: 12, dataIndex: 'quantityIntended', type: 'float', useNull: true},
						{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', width: 22, dataIndex: 'investmentSecurity.name', hidden: true, filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
						{header: 'Avg Price', width: 14, dataIndex: 'averageUnitPrice', hidden: true, type: 'float', useNull: true},
						{header: 'Expected Price', width: 15, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
						{header: 'Accounting Notional', width: 18, dataIndex: 'accountingNotional', type: 'currency'},
						{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
						{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
						{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
						{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
						{header: 'Trader', width: 14, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}, defaultSortColumn: true},
						{header: 'FIX', width: 7, dataIndex: 'fixTrade', type: 'boolean', hidden: true},
						{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Destination', width: 12, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
						{header: 'Executing Broker', width: 20, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
						{header: 'Execution Type', width: 13, dataIndex: 'tradeExecutionType.name', hidden: true, filter: {searchFieldName: 'tradeExecutionTypeName'}},
						{header: 'Limit Price', width: 11, dataIndex: 'limitPrice', hidden: true, type: 'float', useNull: true},
						{header: 'Traded On', width: 11, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
						{header: 'Settled On', width: 11, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}
					]
				}
			]
		}
	]
});
