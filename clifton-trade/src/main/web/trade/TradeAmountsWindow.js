Clifton.trade.TradeAmountsWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'Trade Amounts',
	iconCls: 'shopping-cart',
	height: 570,
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'trade.json',
		instructions: 'A summary of trade amounts in both local currency (currency denomination of trade security) as well as client account\'s base currency.',
		readOnly: true,
		labelWidth: 150,
		items: [
			{fieldLabel: 'Trade ID', name: 'id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'id'},
			{fieldLabel: 'Trade Type', name: 'tradeType.name', xtype: 'linkfield', detailIdField: 'tradeType.id', detailPageClass: 'Clifton.trade.TypeWindow'},
			{fieldLabel: 'Security', name: 'investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id'},
			{fieldLabel: 'Settlement Currency', name: 'payingSecurity.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'payingSecurity.id'},
			{html: '<hr/>', xtype: 'label'},
			{
				xtype: 'formtable',
				columns: 3,
				defaults: {
					readOnly: true,
					submitValue: false,
					style: {marginLeft: '5px', textAlign: 'right'},
					width: '90%'
				},
				items: [
					{cellWidth: '150px', xtype: 'label'},
					{html: '<b>Local Currency</b>', xtype: 'label', style: 'text-align: right;'},
					{html: '<b>Base Currency</b>', xtype: 'label', style: 'text-align: right;'},

					{html: 'Currency:', xtype: 'label', style: 'text-align: left'},
					{name: 'investmentSecurity.instrument.tradingCurrency.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.instrument.tradingCurrency.id', style: 'text-align: right;'},
					{name: 'clientInvestmentAccount.baseCurrency.symbol', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'clientInvestmentAccount.baseCurrency.id', style: 'text-align: right;'},

					{html: 'Exchange Rate:', xtype: 'label', style: 'text-align: left'},
					{name: 'exchangeRateToLocal', xtype: 'floatfield', value: 1},
					{name: 'exchangeRateToBase', xtype: 'floatfield'},

					{html: 'Position Amount:', xtype: 'label', style: 'text-align: left'},
					{name: 'accountingNotional', xtype: 'currencyfield'},
					{name: 'accountingNotionalBase', xtype: 'currencyfield'},

					{html: 'Accrual Amount:', xtype: 'label', style: 'text-align: left'},
					{name: 'accrualAmount', xtype: 'currencyfield', value: 0},
					{name: 'accrualAmountBase', xtype: 'currencyfield'},

					{html: 'Accrual 1 Amount:', xtype: 'label'},
					{name: 'accrualAmount1', xtype: 'currencyfield', value: 0},
					{name: 'accrualAmount1Base', xtype: 'currencyfield'},

					{html: 'Accrual 2 Amount:', xtype: 'label'},
					{name: 'accrualAmount2', xtype: 'currencyfield', value: 0},
					{name: 'accrualAmount2Base', xtype: 'currencyfield'},

					{html: 'Commission Amount:', xtype: 'label', style: 'text-align: left'},
					{name: 'commissionAmount', xtype: 'currencyfield', value: 0},
					{name: 'commissionAmountBase', xtype: 'currencyfield'},

					{html: 'Fee Amount:', xtype: 'label', style: 'text-align: left'},
					{name: 'feeAmount', xtype: 'currencyfield', value: 0},
					{name: 'feeAmountBase', xtype: 'currencyfield'},

					{xtype: 'label', html: '<hr />', colspan: 3, width: '100%'},

					{html: 'Position + Accrual Total:', xtype: 'label', style: 'text-align: left'},
					{name: 'positionAndAccrualAmount', xtype: 'currencyfield'},
					{name: 'positionAndAccrualAmountBase', xtype: 'currencyfield'},

					{html: 'Commission + Fee Total:', xtype: 'label', style: 'text-align: left'},
					{name: 'commissionAndFeeAmount', xtype: 'currencyfield'},
					{name: 'commissionAndFeeAmountBase', xtype: 'currencyfield'},

					{html: '<b>Trade Total:</b>', xtype: 'label', style: 'text-align: left', qtip: 'Retrieved from GL for booked trades and calculated from GL booking preview for unbooked trades.'},
					{name: 'netPayment', xtype: 'currencyfield', style: 'margin-left: 5px; text-align: right; font-weight: bold'},
					{name: 'netPaymentBase', xtype: 'currencyfield', style: 'margin-left: 5px; text-align: right; font-weight: bold'}
				]
			}
		],
		listeners: {
			afterload: function() {
				const fp = this;
				const f = fp.getForm();
				const fxRate = fp.getFormValue('exchangeRateToBase');
				const positionAmount = fp.getFormValue('accountingNotional');
				const accrualAmount = fp.getFormValue('accrualAmount');
				const commissionAmount = f.findField('commissionAmount').getNumericValue();
				const feeAmount = f.findField('feeAmount').getNumericValue();

				fp.setFormValue('accountingNotionalBase', positionAmount * fxRate, true);

				fp.setFormValue('accrualAmountBase', accrualAmount * fxRate, true);
				fp.setFormValue('accrualAmount1Base', fp.getFormValue('accrualAmount1') * fxRate, true);
				fp.setFormValue('accrualAmount2Base', fp.getFormValue('accrualAmount2') * fxRate, true);

				fp.setFormValue('commissionAmountBase', commissionAmount * fxRate, true);
				fp.setFormValue('feeAmountBase', feeAmount * fxRate, true);

				fp.setFormValue('positionAndAccrualAmount', positionAmount + accrualAmount, true);
				fp.setFormValue('commissionAndFeeAmount', commissionAmount + feeAmount, true);
				fp.setFormValue('positionAndAccrualAmountBase', (positionAmount + accrualAmount) * fxRate, true);
				fp.setFormValue('commissionAndFeeAmountBase', (commissionAmount + feeAmount) * fxRate, true);

				TCG.data.getDataValuePromise('tradeNetPaymentAmount.json?tradeId=' + fp.getFormValue('id'), fp)
					.then(function(netPayment) {
						if (netPayment) {
							fp.setFormValue('netPayment', netPayment, true);
							fp.setFormValue('netPaymentBase', netPayment * fxRate, true);
						}
					});
			}
		}
	}]
});
