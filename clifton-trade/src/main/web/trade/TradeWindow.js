// not the actual window but a window selector based on 'tradeType'

Clifton.trade.TradeWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'trade.json?requestedMaxDepth=6&UI_SOURCE=TradeWindowSelector', // deep enough to get hierarchy attributes

	getClassName: function(config, entity) {
		let tradeType = this.tradeType;
		if (entity && entity.tradeType) {
			tradeType = entity.tradeType.name;
		}
		let clazz = Clifton.trade.TradeWindowOverrides[tradeType];
		if (!clazz) {
			clazz = 'Clifton.trade.DefaultTradeWindow';
		}
		return clazz;
	}
});
