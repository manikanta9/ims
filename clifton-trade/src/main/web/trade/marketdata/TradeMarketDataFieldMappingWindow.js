Clifton.trade.marketdata.TradeMarketDataFieldMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Market Data Field Mapping',
	iconCls: 'numbers',
	width: 600,
	height: 250,

	items: [{

		xtype: 'formpanel',
		labelWidth: 150,
		url: 'tradeMarketDataFieldMapping.json',
		instructions: 'Capture market data only for trades define by the following selections. Always try to limit the scope as much as possible.',

		items: [
			{fieldLabel: 'Trade Market Data Field', name: 'tradeMarketDataField.name', xtype: 'linkfield', detailIdField: 'tradeMarketDataField.id', detailPageClass: 'Clifton.trade.marketdata.TradeMarketDataFieldWindow'},
			{fieldLabel: 'Trade Type', name: 'tradeType.name', hiddenName: 'tradeType.id', xtype: 'combo', url: 'tradeTypeListFind.json', detailPageClass: 'Clifton.trade.TypeWindow'},
			{fieldLabel: 'Instrument Group', name: 'investmentGroup.name', hiddenName: 'investmentGroup.id', xtype: 'combo', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
			{fieldLabel: 'Business Service', name: 'businessService.name', hiddenName: 'businessService.id', xtype: 'combo', url: 'businessServiceListFind.json', detailPageClass: 'Clifton.business.service.ServiceWindow'}
		]
	}]
});
