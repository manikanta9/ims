Clifton.trade.marketdata.TradeMarketDataValueWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Market Data Value',
	iconCls: 'numbers',
	width: 550,
	height: 350,
	items: [{
		xtype: 'formpanel',
		url: 'tradeMarketDataValue.json?enableOpenSessionInView=true',
		instructions: 'The Trade Market Data Value object ties a specific Market Data Value to a Trade.',
		labelWidth: 150,
		listeners: {afterload: panel => panel.applyFieldModificationRestrictions()},
		items: [
			// Mapping fields
			{xtype: 'sectionheaderfield', header: 'Mapping Info'},
			{fieldLabel: 'Trade', name: 'referenceOne.labelShort', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', detailIdField: 'referenceOne.id'},
			{fieldLabel: 'Trade Market Data Field', xtype: 'combo', name: 'tradeMarketDataField.label', hiddenName: 'tradeMarketDataField.id', url: 'tradeMarketDataFieldListFind.json', detailPageClass: 'Clifton.trade.marketdata.TradeMarketDataFieldWindow'},

			// Measure fields
			{xtype: 'sectionheaderfield', header: 'Market Data Value'},
			{fieldLabel: 'Measure Date', xtype: 'datefield', name: 'referenceTwo.measureDate'},
			{fieldLabel: 'Measure Time', xtype: 'timefield', name: 'referenceTwo.measureTime'},
			{
				fieldLabel: 'Measure Value', name: 'referenceTwo.measureValue',
				setValue: function(value) {
					TCG.callParent(this, 'setValue', [this.value = value != null ? value : '<span style="color: red; font-weight: bold;">Insufficient Access</span>']);
				}
			}
		],
		getFormLabel: function() {
			const measureValue = this.getFormValue('referenceTwo.measureValue');
			return `Trade ${this.getFormValue('referenceOne.id')} - ${this.getFormValue('tradeMarketDataField.label')} on ${this.getFormValue('referenceTwo.measureDateWithTime')} was ${measureValue != null ? measureValue : '(Insufficient Access)'}`;
		},
		applyFieldModificationRestrictions: function() {
			// Guard-clause: Do not modify fields when creating new entities
			if (!this.getIdFieldValue()) {
				return;
			}
			// Convert to non-editable fields
			this.convertComboToLinkField('referenceOne.labelShort');
			const tradeMarketDataFieldCmp = this.convertComboToLinkField('tradeMarketDataField.label');
			this.insert(this.items.indexOf(tradeMarketDataFieldCmp) + 1, {fieldLabel: 'Market Data Field', xtype: 'linkfield', name: 'referenceTwo.dataField.label', detailIdField: 'referenceTwo.dataField.id', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', value: TCG.getValue('referenceTwo.dataField.label', this.getForm().formValues)});
			this.convertComboToLinkField('referenceTwo.measureDate', {name: 'referenceTwo.measureDateWithTime', detailIdField: 'referenceTwo.id', detailPageClass: 'Clifton.marketdata.field.DataValueWindow'});
			this.removeField(this.getForm().findField('referenceTwo.measureTime'));
			this.overrideField('referenceTwo.measureValue', {xtype: 'linkfield', detailIdField: 'referenceTwo.id', detailPageClass: 'Clifton.marketdata.field.DataValueWindow'});
		}
	}]
});
