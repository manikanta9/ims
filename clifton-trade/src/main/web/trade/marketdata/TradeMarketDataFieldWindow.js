Clifton.trade.marketdata.TradeMarketDataFieldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Market Data Field',
	iconCls: 'numbers',
	width: 800,
	height: 520,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'tradeMarketDataField.json',
					instructions: 'Select Market Data Field and corresponding parameters as well as use the "Scope" to limit trades that it will be captured for.  Trade Market Data Fields are mapped market data fields that will be captured at different stages in the lifecycle of each trade (workflow transition actions).',
					labelWidth: 120,

					listeners: {
						afterload: function(formPanel) {
							if (formPanel.getFormValue('systemDefined')) {
								formPanel.disableField('name');
							}
						}
					},

					items: [
						{fieldLabel: 'Trade Field Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Market Data Field', name: 'marketDataField.name', hiddenName: 'marketDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json?timeSensitive=true&categoryName=Market Data Tags&categoryHierarchyName=Trade Prices', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'},
						{fieldLabel: 'Data Source', name: 'marketDataSource.name', hiddenName: 'marketDataSource.id', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
						{fieldLabel: 'Value Read Condition', name: 'valueReadCondition.label', hiddenName: 'valueReadCondition.id', displayField: 'name', xtype: 'system-condition-combo', qtip: 'Condition to limit who can view market data applicable to a trade. For example, market data values from Bloomberg should be visible only to users with necessary entitlements.'},
						{xtype: 'system-entity-modify-condition-combo', requireConditionForNonAdmin: true},
						{fieldLabel: 'Workflow End State', name: 'tradeWorkflowEndState.name', hiddenName: 'tradeWorkflowEndState.id', xtype: 'combo', url: 'workflowStateListByWorkflowName.json?workflowName=Trade Workflow', root: 'data', detailPageClass: 'Clifton.workflow.definition.StateWindow', qtip: 'The Trade\'s workflow transition ending state that triggers the capture of Market Data Field Values.'},
						{boxLabel: 'Use Underlying Security to lookup Market Data Values', name: 'underlyingSecurity', xtype: 'checkbox'},
						{boxLabel: 'Retrieve Market Data synchronously', name: 'synchronous', xtype: 'checkbox', qtip: 'If checked, market data will be retrieved synchronously on the specified workflow transition. This will cause the system to wait until data is retrieved before moving forward with the transition. This can be useful in such cases as pre-validation steps in order to obtain data before validation is performed.'},
						{boxLabel: 'Only Retrieve Market Data Once', name: 'onlyRetrieveOnce', xtype: 'checkbox', qtip: 'If checked, indicates that the market data should be retrieved only once, and, once retrieved, may be reused.'},
						{boxLabel: 'Active (uncheck to disable lookups for this field)', name: 'active', xtype: 'checkbox'},
						{boxLabel: 'System Defined (cannot be edited except: Description, Value Read Condition, Active)', name: 'systemDefined', xtype: 'checkbox', disabled: true}
					]
				}]
			},


			{
				title: 'Scope',
				items: [{
					xtype: 'gridpanel',
					name: 'tradeMarketDataFieldMappingListFind',
					instructions: 'The scope is used to limit the number of trades that the data should be captured for. Always try to limit as much as possible because market data capture introduces additional overhead.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Type', width: 40, dataIndex: 'tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}},
						{header: 'Instrument Group', width: 50, dataIndex: 'investmentGroup.name', filter: {type: 'combo', searchFieldName: 'investmentGroupId', url: 'investmentGroupListFind.json'}},
						{header: 'Business Service', width: 50, dataIndex: 'businessService.name', filter: {type: 'combo', searchFieldName: 'businessServiceId', url: 'businessServiceListFind.json'}}
					],
					getTradeMarketDataFieldId: function() {
						const win = TCG.getParentByClass(this, Ext.Window);
						return win.getMainFormId();
					},
					getLoadParams: function(firstLoad) {
						const gridPanel = this;
						return {
							tradeMarketDataFieldId: gridPanel.getTradeMarketDataFieldId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.trade.marketdata.TradeMarketDataFieldMappingWindow',
						getDefaultData: function(gridPanel) {
							return {
								tradeMarketDataField: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'TradeMarketDataField'
				}]
			}
		]
	}]
});
