// not the actual window but a window selector based on 'tradeType': selects Trade window for the fill
TCG.use('Clifton.trade.TradeWindow');

Clifton.trade.TradeFillWindow = Ext.extend(Clifton.trade.TradeWindow, {
	url: 'tradeFill.json?enableOpenSessionInView=true',

	prepareEntity: function(entity) {
		return entity ? entity.trade : entity;
	}
});
