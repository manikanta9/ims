Clifton.trade.roll.CurrencyForwardRollTradeGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Currency Forwards Roll Trades',
	iconCls: 'currency-forward',
	width: 1500,
	height: 700,
	hideApplyButton: true,
	hideOKButton: true, // use true if only want Apply & Cancel Buttons
	hideCancelButton: true,
	doNotWarnOnCloseModified: true, // avoid warning to save when closing
	saveTimeout: 180,

	items: [
		{xtype: 'currency-forward-roll-trade-group-form-panel'}
	]
});

Clifton.trade.roll.CurrencyForwardRollTradeGroupFormPanel = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicFormPanel, {
	dtoClassForBinding: 'com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRoll',
	requestedMaxDepth: 8,
	requestedPropertiesToExclude: 'tradeList',

	additionalListRequestedProperties: 'longPosition|maturingAmount|maturingPrice|settlementAmount|maturingSecurity.id|maturingSecurity.symbol|maturingSecurity.instrument.id|maturingSecurity.instrument.underlyingInstrument.tradingCurrency.symbol|maturingSecurity.instrument.underlyingInstrument.tradingCurrency.id|maturingSecurity.instrument.tradingCurrency.symbol|maturingSecurity.instrument.tradingCurrency.id|maturingSecurity.instrument.labelShort|maturingSecurity.instrument.hierarchy.id|maturingSecurity.instrument.hierarchy.investmentTypeSubType.name|maturingSecurity.instrument.hierarchy.labelExpanded|pending|' + //
		'closeBuy|closeGivenAmount|closeGivenCurrency.symbol|closeGivenCurrency.id|closeSecurity.id|closeSecurity.symbol|' + //
		'closeTrade.id|closeTrade.buy|closeTrade.quantityIntended|closeTrade.accountingNotional|closeTrade.payingSecurity.symbol|closeTrade.payingSecurity.id|closeTrade.investmentSecurity.symbol|closeTrade.investmentSecurity.name|closeTrade.investmentSecurity.instrument.underlyingInstrument.tradingCurrency.symbol|closeTrade.investmentSecurity.instrument.underlyingInstrument.tradingCurrency.id|closeTrade.investmentSecurity.instrument.tradingCurrency.symbol|closeTrade.investmentSecurity.instrument.tradingCurrency.id|closeTrade.investmentSecurity.id|closeTrade.averageUnitPrice|closeTrade.executingBrokerCompany.label|closeTrade.executingBrokerCompany.id|closeTrade.tradeDestination.name|closeTrade.tradeDestination.id|closeTrade.workflowState.name|' + //
		'rollBuy|rollGivenAmount|rollGivenCurrency.symbol|rollGivenCurrency.id|rollSecurity.id|rollSecurity.symbol|' + //
		'rollTrade.id|rollTrade.buy|rollTrade.quantityIntended|rollTrade.accountingNotional|rollTrade.payingSecurity.symbol|rollTrade.payingSecurity.id|rollTrade.investmentSecurity.symbol|rollTrade.investmentSecurity.name|rollTrade.investmentSecurity.instrument.underlyingInstrument.tradingCurrency.symbol|rollTrade.investmentSecurity.instrument.underlyingInstrument.tradingCurrency.id|rollTrade.investmentSecurity.instrument.tradingCurrency.symbol|rollTrade.investmentSecurity.instrument.tradingCurrency.id|rollTrade.investmentSecurity.id|rollTrade.averageUnitPrice|rollTrade.executingBrokerCompany.label|rollTrade.executingBrokerCompany.id|rollTrade.tradeDestination.name|rollTrade.tradeDestination.id|holdingAccount.issuingCompany.id|rollTrade.holdingInvestmentAccount.number|rollTrade.holdingInvestmentAccount.id',

	applyCustomDefaultData: function(defaultData) {
		return Ext.apply({
			traderUser: TCG.getCurrentUser(),
			tradeDate: new Date().format('Y-m-d 00:00:00'),
			startMaturityDate: new Date().format('Y-m-d 00:00:00'),
			balanceDate: Clifton.calendar.getBusinessDayFromDate(new Date(), -1).format('Y-m-d 00:00:00'),
			tradeGroupType: TCG.data.getData('tradeGroupTypeByName.json?name=Forward Roll', this, 'trade.group.type.Forward Roll'),
			tradeDestination: TCG.data.getData('tradeDestinationByName.json?name=FX Connect Ticket', this, 'trade.tradeDestination.FX Connect Ticket'),
			tradeType: TCG.data.getData('tradeTypeByName.json?name=Forwards', this)
		}, defaultData);
	},

	reloadChildGrid: function(formpanel) {
		const gp = TCG.getChildByName(formpanel, 'forward-roll-trade-grid');
		gp.reloadGrid.call(gp);
	},


	executionTypeIsCls: function(formpanel) {
		const f = formpanel.getForm();
		const tradeExecutionTypeField = f.findField('tradeExecutionTypeForRollTrade.name');
		if (TCG.isNotBlank(tradeExecutionTypeField)) {
			const tradeExecutionType = tradeExecutionTypeField.getValueObject();
			return TCG.isTrue(tradeExecutionType.clsTrade);
		}
		return false;
	},

	tradeEntryItems: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .42,
					layout: 'form',
					labelWidth: 120,
					items: [
						{xtype: 'sectionheaderfield', header: 'Maturing Forwards Info'},
						{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', hiddenName: 'clientAccountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json', allowBlank: true, detailPageClass: 'Clifton.investment.account.AccountGroupWindow', anchor: '-20'},
						{
							fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: true, detailPageClass: 'Clifton.investment.account.AccountWindow', anchor: '-20',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.store.setBaseParam('relatedPurpose', fp.getFormValue('tradeType.holdingAccountPurpose.name'));
									combo.store.setBaseParam('relatedPurposeActiveOnDate', fp.getForm().findField('tradeDate').value);
								}
							}
						},
						{
							fieldLabel: 'Maturity Beginning', xtype: 'compositefield', anchor: '-20',
							defaults: {
								xtype: 'datefield',
								allowBlank: false,
								flex: 1
							},
							items: [
								{
									name: 'startMaturityDate',
									qtip: 'Beginning Maturity Date Range for looking up Maturing Forwards Positions.'
								},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End:', xtype: 'displayfield', width: 80},
								{
									name: 'endMaturityDate',
									qtip: 'Ending Maturity Date Range for looking up Maturing Forwards Positions.'
								}
							]
						},
						{
							fieldLabel: 'Position Direction', xtype: 'compositefield',
							qtip: 'Used to filter Maturing Forwards as Buy for Long, Sell for Short, or Both.',
							items: [
								{
									xtype: 'radiogroup', readOnly: true, allowBlank: true, width: 275,
									columns: [60, 60, 60],
									items: [
										{boxLabel: 'Long', name: 'longPositions', inputValue: true},
										{boxLabel: 'Short', name: 'longPositions', inputValue: false},
										{boxLabel: 'Both', name: 'longPositions', inputValue: null, checked: true}
									]
								},
								{
									boxLabel: 'Apply Pending Trades', name: 'includePendingTransactions', xtype: 'checkbox', checked: true,
									qtip: 'Include Pending Transactions to adjust the shown Maturing positions.',
									listeners: {
										render: function(field) {
											// Had trouble getting the qtip to display, so specifically set it
											Ext.QuickTips.register({target: field, text: field.qtip});
										}
									}
								}
							]
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .29,
					layout: 'form',
					labelWidth: 95,
					items: [
						{xtype: 'sectionheaderfield', header: 'New Forwards Info'},
						{
							fieldLabel: 'Maturity (New)', name: 'newMaturityDate', xtype: 'datefield',
							qtip: 'Maturity Date of the New Forwards that will be traded here.'
						},
						{
							fieldLabel: '', boxLabel: 'Default Far Leg Given CCY to Base CCY', name: 'defaultRollToBaseCurrency', xtype: 'checkbox', checked: true,
							listeners: {
								check: function(f, checked) {
									const p = TCG.getParentFormPanel(f);
									const gp = TCG.getChildByName(p, 'forward-roll-trade-grid');
									gp.updateFarLegDefaultCurrency(checked);
								}
							},
							qtip: 'When defaulting the Given CCY for the Far Leg (Position on New Forward), use the base currency for the Forward (often USD).'
						},
						{
							fieldLabel: '', boxLabel: 'Default Far Leg To Same Broker', name: 'defaultRollToSameBroker', xtype: 'checkbox', checked: true,
							qtip: 'When creating a Trade for the Far Leg (Position on New Forward), use the same Holding Account and Executing Broker as the Maturing position and Closing Trade.'
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{
							fieldLabel: 'Execution Type', name: 'tradeExecutionType.name', displayField: 'name', hiddenName: 'tradeExecutionType.id', xtype: 'combo', width: 120,
							qtip: 'Trade execution type for the new opening trade (roll trade).',
							url: 'tradeExecutionTypeListFind.json', autoLoad: true, requestedProps: 'bticTrade|clsTrade|requiredHoldingInvestmentAccountType|excludeHoldingInvestmentAccountType',
							detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow', submitValue: true,
							listeners: {
								afterrender: function(combo) {
									combo.store.on('load', function(store, records) {
										if (TCG.isNotBlank(combo.defaultValue)) {
											combo.setValue(combo.defaultValue.id);
										}
									});

									combo.updateDefaultTradeExecutionTypeValue(combo);
								},
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;

									combo.store.baseParams = {
										tradeTypeId: combo.tradeType.id
									};
								},
								select: function(combo, record, index) {
									// reload grid after selecting a Trade Execution Type to repopulate the grid with applicable entries.
									const fp = TCG.getParentFormPanel(combo);
									fp.reloadChildGrid(fp);
								}
							},
							updateDefaultTradeExecutionTypeValue: function(combo) {
								TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=Forwards', combo, 'CurrencyForwardsRollTrades.tradeTypeForwards')
									.then(function(tradeType) {
										combo.tradeType = tradeType;
										combo.clearAndReset();
										return TCG.data.getDataPromiseUsingCaching('tradeTypeTradeExecutionTypeListFind.json?tradeTypeId=' + combo.tradeType.id + '&defaultExecutionType=true', combo, 'trade.defaultTradeExecutionTypeForwards' + combo.tradeType.id);
									})
									.then(function(tradeTypeTradeExecutionTypeList) {
										if (TCG.isNotBlank(tradeTypeTradeExecutionTypeList) && tradeTypeTradeExecutionTypeList.length > 0) {
											combo.defaultValue = tradeTypeTradeExecutionTypeList[0].referenceTwo;
											combo.doQuery('');
										}
									});
							}
						},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false, anchor: '-20',
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									qEvent.combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: f.findField('tradeType.name').getValue()
									};
								}
							}
						},
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, anchor: '-20'},
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false}
					]
				}
			]
		}
	],

	tradeEntryGridItem: [
		{
			xtype: 'panel',
			layout: 'fit',
			items: [
				{
					xtype: 'currency-forward-roll-detail-form-grid',
					tradeEntrySupported: true,
					autoHeight: false,
					height: 510
				}
			],
			listeners: {
				afterlayout: TCG.grid.registerDynamicHeightResizing
			}
		}
	],

	readOnlyItems: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .42,
					layout: 'form',
					labelWidth: 120,
					items: [
						{xtype: 'sectionheaderfield', header: 'Maturing Forwards Info'},
						{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', detailIdField: 'clientAccountGroup.id', xtype: 'linkfield', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
						{fieldLabel: 'Client Account', name: 'clientAccount.name', detailIdField: 'clientAccount.id', xtype: 'linkfield', url: 'investmentAccountListFind.json', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{
							fieldLabel: 'Maturity Beginning', xtype: 'compositefield', anchor: '-20',
							defaults: {
								xtype: 'datefield',
								allowBlank: false,
								flex: 1
							},
							items: [
								{
									name: 'startMaturityDate', readOnly: true,
									qtip: 'Beginning Maturity Date Range for looking up Maturing Forwards Positions.'
								},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End:', xtype: 'displayfield', width: 80},
								{
									name: 'endMaturityDate', readOnly: true,
									qtip: 'Ending Maturity Date Range for looking up Maturing Forwards Positions.'
								}
							]
						},
						{
							xtype: 'radiogroup', readOnly: true, disabled: true,
							fieldLabel: 'Position Direction',
							qtip: 'Used to filter Maturing Forwards as Buy for Long, Sell for Short, or Both.',
							columns: [60, 60, 60],
							allowBlank: true,
							anchor: '-82',
							items: [
								{boxLabel: 'Long', name: 'longPositions', inputValue: true},
								{boxLabel: 'Short', name: 'longPositions', inputValue: false},
								{boxLabel: 'Both', name: 'longPositions', inputValue: null}
							]
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .29,
					layout: 'form',
					labelWidth: 95,
					items: [
						{xtype: 'sectionheaderfield', header: 'New Forwards Info'},
						{fieldLabel: 'Maturity (New)', name: 'newMaturityDate', xtype: 'datefield', submitValue: false, readOnly: true},
						{fieldLabel: '', boxLabel: 'Default Far Leg Given CCY to Base CCY', name: 'defaultRollToBaseCurrency', xtype: 'checkbox', readOnly: true, disabled: true},
						{fieldLabel: '', boxLabel: 'Default Far Leg To Same Broker', name: 'defaultRollToSameBroker', xtype: 'checkbox', readOnly: true, disabled: true}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .25,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{name: 'workflowState.id', xtype: 'hidden'},
						{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
						{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'displayfield'},
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
					]
				}
			]
		}
	],

	readOnlyGridItem: [
		{
			xtype: 'panel',
			layout: 'fit',
			items: [
				{
					xtype: 'currency-forward-roll-detail-form-grid',
					tradeEntrySupported: false,
					autoHeight: false,
					viewConfig: {markDirty: false},
					height: 460
				}
			],
			listeners: {
				afterlayout: TCG.grid.registerDynamicHeightResizing
			}
		}
	]
});
Ext.reg('currency-forward-roll-trade-group-form-panel', Clifton.trade.roll.CurrencyForwardRollTradeGroupFormPanel);

Clifton.trade.roll.CurrencyForwardRollDetailFormGrid = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicDetailGrid, {
	name: 'forward-roll-trade-grid',
	dtoClass: 'com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRollDetail',
	groupField: 'clientAccount.label',
	instructions: 'Forwards Roll screen allows for filtering Maturing Forwards positions for the selected Client Account or Client Account Group in order to close the positions and optionally open new Forwards position. Maturing Positions are populated by those maturing between the specified maturity dates.',

	alwaysSubmitFields: ['holdingAccount.id', 'clientAccount.id', 'maturingPrice', 'closeBuy', 'closeGivenAmount', 'closeSecurity.id', 'closeGivenCurrency.id', 'closeGivenAmount', 'rollBuy', 'rollSecurity.id', 'rollGivenCurrency.id', 'closeTrade.id', 'rollTrade.id'],
	doNotSubmitFields: ['includeTrade', 'closeTradeBuy', 'closeTradeCurrencySymbol', 'closeTradeAmount', 'rollTradeBuy', 'rollTradeCurrencySymbol', 'rollTradeAmount'],

	addEntryToolbarButtons: function(toolBar, gridpanel) {
		const gp = gridpanel;
		toolBar.add({
			text: 'Load Forwards',
			tooltip: 'Load Maturing forwards for the information provided above.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.submitField.setValue('[]');
				gp.reloadGrid();
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Submit Trades',
			tooltip: 'Create New Forwards Trades based on quantities entered below.',
			iconCls: 'shopping-cart',
			scope: this,
			handler: function() {
				if (this.submitField.getValue() === '[]') {
					TCG.showError('No rows selected.  Please select at least one row from the list.');
					return;
				}
				gp.confirmTradeGroupAction('Submit', () => gp.getWindow().saveWindow(false));
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Clear Trades',
			tooltip: 'Clear all Far Leg (New Position) Given Amounts entered below.',
			iconCls: 'clear',
			scope: this,
			handler: function() {
				gp.clearTrades();
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Info',
			tooltip: 'Toggle instructions for this page',
			iconCls: 'info',
			enableToggle: true,
			handler: function() {
				if (this.pressed) {
					gp.insert(0, {
						xtype: 'panel',
						frame: true,
						layout: 'fit',
						bodyStyle: 'padding: 3px 3px 3px 3px',
						html: gp.instructions
					});
				}
				else {
					gp.remove(0);
				}
				gp.doLayout();
			}
		}, '-');
	},

	confirmTradeGroupAction: function(actionLabel, handler) {
		const form = TCG.getParentFormPanel(this).getForm();
		const farLegBaseCurrencyCheckbox = form.findField('defaultRollToBaseCurrency');
		Ext.Msg.confirm(actionLabel + ' Currency Forward Trades',
			'You are performing an action for Currency Forward Trades where the far leg trades\' Given Currency was defaulted to the <b>' + (farLegBaseCurrencyCheckbox.getValue() ? 'Base' : 'Foreign') + ' Currency</b>. Is this what was intended?',
			answer => {
				if (answer === 'yes') {
					handler();
				}
			});
	},

	executeAction: function(groupAction) {
		const actionFunction = () => TCG.callSuper(this, 'executeAction', arguments);

		if (groupAction && groupAction === 'APPROVE') {
			// check for selection before prompting for confirmation
			if (this.submitField.getValue() === '[]') {
				TCG.showError('No row selected.  Please select at least one row from the list.');
				return;
			}
			// prompt for confirm and perform action
			this.confirmTradeGroupAction('Approve', actionFunction);
		}
		else {
			actionFunction();
		}
	},

	clearTrades: function() {
		const gp = this;
		gp.getStore().each(function(record) {
			record.set('rollGivenAmount', '');
		}, this);
		gp.markModified();
	},

	handleCellDoubleClick: function(grid, rowIndex, cellIndex, event) {
		const row = grid.store.data.items[rowIndex];
		const columnName = grid.getColumnModel().getColumnById(cellIndex).dataIndex;
		if (columnName && (columnName === 'maturingSecurity.symbol' || columnName === 'rollTrade.investmentSecurity.symbol')) {
			const securityId = TCG.getValue(columnName.startsWith('maturing') ? 'maturingSecurity.id' : 'rollTrade.investmentSecurity.id', row.json);
			if (TCG.isNotBlank(securityId)) {
				TCG.createComponent('Clifton.investment.instrument.SecurityWindow', {
					id: TCG.getComponentId('Clifton.investment.instrument.SecurityWindow', securityId),
					params: {id: securityId},
					openerCt: grid
				});
			}
		}
		else if (cellIndex > 0 && cellIndex < 8) {
			const filters = [];
			filters.push({
				field: 'clientInvestmentAccountId',
				comparison: 'EQUALS',
				value: TCG.getValue('clientAccount.id', row.json)
			});
			filters.push({
				field: 'holdingInvestmentAccountId',
				comparison: 'EQUALS',
				value: TCG.getValue('holdingAccount.id', row.json)
			});
			filters.push({
				field: 'investmentSecurityId',
				comparison: 'EQUALS',
				value: TCG.getValue('maturingSecurity.id', row.json)
			});
			const nextDay = new Date(TCG.getParentFormPanel(grid).getForm().findField('tradeDate').getValue()).add(Date.DAY, 1).format('m/d/Y');
			filters.push({
				field: 'transactionDate',
				comparison: 'LESS_THAN',
				value: nextDay
			});
			TCG.createComponent('Clifton.accounting.gl.TransactionListWindow', {
				defaultData: filters,
				openerCt: grid
			});
		}
		else if (this.tradeEntrySupported === false) {
			let tid = '';
			if (columnName && columnName.startsWith('closeTrade')) {
				tid = TCG.getValue('closeTrade.id', row.json);
			}
			else if (columnName && columnName.startsWith('rollTrade')) {
				tid = TCG.getValue('rollTrade.id', row.json);
			}
			if (TCG.isNotBlank(tid)) {
				TCG.createComponent('Clifton.trade.TradeWindow', {
					id: TCG.getComponentId('Clifton.trade.TradeWindow', tid),
					params: {id: tid},
					openerCt: grid
				});
			}
		}
	},

	handleAfterLoadTransientRecordValues: function(grid) {
		this.getStore().each(function(record) {
			record.beginEdit();
			if (grid.tradeEntrySupported === true) {
				const longPositions = TCG.getValue('longPosition', record.json);
				if (TCG.isBlank(TCG.getValue('closeBuy', record.json))) {
					record.set('closeBuy', longPositions !== true);
				}
				if (TCG.isBlank(TCG.getValue('rollBuy', record.json))) {
					record.set('rollBuy', longPositions === true);
				}
			}
			else {
				if (record.json.closeTrade) {
					grid.updateRecordTransientTradeValues(record, 'closeTrade');
				}
				if (record.json.rollTrade) {
					grid.updateRecordTransientTradeValues(record, 'rollTrade');
				}
				record.commit();// removed dirty flag for transient fields
			}
			record.endEdit();
		});
	},

	updateRecordTransientTradeValues: function(record, tradePropertyPrefix) {
		const settlementCurrency = TCG.getValue(tradePropertyPrefix + '.payingSecurity.symbol', record.json),
			instrumentTradingCurrency = TCG.getValue(tradePropertyPrefix + '.investmentSecurity.instrument.tradingCurrency.symbol', record.json);
		const buy = TCG.getValue(tradePropertyPrefix + '.buy', record.json);
		if (settlementCurrency === instrumentTradingCurrency) {
			record.set(tradePropertyPrefix + 'Amount', TCG.getValue(tradePropertyPrefix + '.quantityIntended', record.json));
			record.set(tradePropertyPrefix + 'Buy', buy);
			record.set(tradePropertyPrefix + 'CurrencySymbol', TCG.getValue(tradePropertyPrefix + '.investmentSecurity.instrument.underlyingInstrument.tradingCurrency.symbol', record.json));
		}
		else {
			record.set(tradePropertyPrefix + 'Amount', TCG.getValue(tradePropertyPrefix + '.accountingNotional', record.json));
			record.set(tradePropertyPrefix + 'Buy', !buy);
			record.set(tradePropertyPrefix + 'CurrencySymbol', instrumentTradingCurrency);
		}
	},

	handleAfterEditEventFromExt: function(event) {
		if (event.originalValue !== event.value && !TCG.isBlank(event.value)) {
			event.record.set('includeTrade', true);
		}
	},

	updateFarLegDefaultCurrency: function(useBaseCurrency) {
		this.getStore().each(function(record) {
			record.beginEdit();
			const givenCurrency = TCG.getValue(useBaseCurrency === true ? 'maturingSecurity.instrument.tradingCurrency' : 'maturingSecurity.instrument.underlyingInstrument.tradingCurrency', record.json);
			const long = TCG.getValue('longPosition', record.json);
			record.set('rollBuy', (useBaseCurrency === true) ? !long : long);
			record.set('rollGivenCurrency.id', givenCurrency.id);
			record.set('rollGivenCurrency.symbol', givenCurrency.symbol);
			record.endEdit();
		});
	},

	// Columns that have tradeEntry = true/false will be included based on if this is tradeEntry or tradeReview - anything without that property will always be included
	customColumns: [
		// Maturing
		{
			header: 'Maturing Forward', dataIndex: 'maturingSecurity.symbol', width: 150, submitValue: false,
			renderer: function(value, metaData, record) {
				if (TCG.isBlank(value)) {
					metaData.css += ' ruleViolation';
					metaData.attr = TCG.renderQtip('Unable to find maturing position. This can occur when the closing leg trade was cancelled or the position was double traded. Consider cancelling the new trade since it is no longer part of a roll.');
				}
				return value;
			}
		},
		{header: 'Maturing CCY', dataIndex: 'maturingSecurity.instrument.underlyingInstrument.tradingCurrency.symbol', width: 70, submitValue: false},
		{
			header: 'Maturing Amount', dataIndex: 'maturingAmount', sortable: false, type: 'float', width: 105, submitValue: false,
			tooltip: 'Amount maturing within the Maturity Range specified.',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(TCG.getValue('pending', r.json))) {
					metaData.css += ' x-grid3-row-highlight-light-orange';
					metaData.attr = TCG.renderQtip('This value reflects pending activity');
				}
				return TCG.renderAmount(v, true, '0,000.00');
			}
		},
		{header: 'Settle CCY', dataIndex: 'maturingSecurity.instrument.tradingCurrency.symbol', sortable: false, width: 70, submitValue: false},
		{
			header: 'Settle Amount', dataIndex: 'settlementAmount', sortable: false, type: 'float', width: 105, submitValue: false,
			tooltip: 'Settlement Amount calculated from Maturing Amount within the Maturity Range specified.',
			renderer: function(v, metaData, r) {
				return TCG.renderAmount(v, true, '0,000.00');
			}
		},
		// Closing Position Entry Columns
		{
			header: 'Close Direction', width: 70, dataIndex: 'closeBuy', sortable: false, filter: false, tradeEntry: true, menuDisabled: true, css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid; PADDING-LEFT: 0px !important;',
			tooltip: 'Direction of the close.',
			renderer: function(v, metadata, record) {
				metadata.css = (v === true) ? 'buy-light' : 'sell-light';
				return (v === true) ? 'BUY' : 'SELL';
			}
		},
		{header: 'Given CCY', dataIndex: 'closeGivenCurrency.symbol', idDataIndex: 'closeGivenCurrency.id', width: 70, hidden: false, tradeEntry: true, css: 'BACKGROUND-COLOR: #fff0f0;'},
		{
			header: 'Given Amount', dataIndex: 'closeGivenAmount', type: 'float', allowNegative: false, useNull: true, sortable: false, tradeEntry: true, width: 105, css: 'BACKGROUND-COLOR: #fff0f0;',
			tooltip: 'Amount to close of the Maturing Forward. Always enter a positive value and use the Buy check box to indicate direction.',
			renderer: function(v, metadata, record) {
				let value = v;
				if (!value) {
					// default to maturing amount
					const closingAmount = Math.abs(TCG.getValue('maturingAmount', record.json));
					record.data['closeGivenAmount'] = closingAmount;
					value = closingAmount;
				}
				if (record.json) {
					const tooltip = Clifton.trade.roll.getNetForwardRollTooltip(metadata, record);
					metadata.attr = TCG.renderQtip(tooltip);
				}
				return TCG.renderAmount(value, false, '0,000.00');
			}
		},
		// Closing Position Read Only Columns
		{
			header: 'Close Direction', width: 55, dataIndex: 'closeTradeBuy', tradeEntry: false, css: 'BACKGROUND-COLOR: #fff0f0; BORDER-LEFT: #c0c0c0 1px solid; PADDING-LEFT: 0px !important;',
			tooltip: 'Direction of the close.',
			renderer: function(v, metadata, record) {
				if (v !== '') {
					metadata.css = v ? 'buy-light' : 'sell-light';
					return v ? 'BUY' : 'SELL';
				}
			}
		},
		{header: 'Given CCY', dataIndex: 'closeTradeCurrencySymbol', hidden: false, width: 70, tradeEntry: false, css: 'BACKGROUND-COLOR: #fff0f0;'},
		{
			header: 'Given Amount', dataIndex: 'closeTradeAmount', type: 'float', allowNegative: false, useNull: true, sortable: false, tradeEntry: false, width: 105, css: 'BACKGROUND-COLOR: #fff0f0;',
			renderer: function(v, metadata, record) {
				const tooltip = Clifton.trade.roll.getNetForwardRollTooltip(metadata, record);
				metadata.attr = TCG.renderQtip(tooltip);
				return TCG.renderAmount(v, false, '0,000.00');
			}
		},
		// New Position entry columns for Forward with later Maturity Date
		{
			header: 'New Direction', dataIndex: 'rollBuy', width: 70, sortable: false, filter: false, tradeEntry: true, menuDisabled: true, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid; PADDING-LEFT: 0px !important;',
			tooltip: 'Direction of the new position.',
			renderer: function(v, metadata, record) {
				metadata.css = (v === true) ? 'buy-light' : 'sell-light';
				return (v === true) ? 'BUY' : 'SELL';
			}
		},
		{header: 'Given CCY', dataIndex: 'rollGivenCurrency.symbol', idDataIndex: 'rollGivenCurrency.id', width: 70, tradeEntry: true, css: 'BACKGROUND-COLOR: #f0f0ff;'},
		{
			header: 'Given Amount', dataIndex: 'rollGivenAmount', sortable: false, type: 'float', allowNegative: false, useNull: true, tradeEntry: true, css: 'BACKGROUND-COLOR: #f0f0ff;',
			editor: {xtype: 'currencyfield'},
			tooltip: 'Amount to roll to the new Forward. Always enter a positive value and use the Direction column to indicate direction.',
			renderer: function(v, metadata, record) {
				if (record.json) {
					const tooltip = Clifton.trade.roll.getNetForwardRollTooltip(metadata, record);
					metadata.attr = TCG.renderQtip(tooltip);
				}
				return TCG.renderAmount(v, false, '0,000.00');
			}
		},
		{
			header: 'New Forward', dataIndex: 'rollSecurity.symbol', idDataIndex: 'rollSecurity.id', width: 120, tradeEntry: true, css: 'BACKGROUND-COLOR: #f0f0ff;',
			tooltip: 'New Currency Forward that will be used for the new Given Amount',
			editor: {
				xtype: 'combo', tooltipField: 'label', displayField: 'symbol', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false, minListWidth: 230,
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					queryEvent.combo.resetStore();
					const editor = queryEvent.combo.gridEditor;
					const record = editor.record;
					const f = TCG.getParentFormPanel(editor.containerGrid).getForm();
					const store = queryEvent.combo.store;
					store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
					store.setBaseParam('instrumentId', TCG.getValue('maturingSecurity.instrument.id', record.json));
					store.setBaseParam('requestedProperties', 'id|name|symbol|label');
				},
				selectHandler: function(combo, selection, index) {
					const gridRecord = combo.gridEditor.record;
					const securityId = TCG.getValue('id', selection.json);
					gridRecord.json.rollSecurity = TCG.data.getData('investmentSecurity.json?id=' + securityId, combo, 'investment.security.id.' + securityId);
				},
				getDetailPageClass: function(newInstance) {
					if (newInstance) {
						this.requiresFormValueUpdate = true;
						return 'Clifton.investment.instrument.copy.ForwardCopyWindow';
					}
					return this.detailPageClass;
				},

				getDefaultData: async function() {
					const editor = this.gridEditor;
					const record = editor.record;
					const maturingSecurity = TCG.getValue('maturingSecurity', record.json);

					return {
						securityToCopy: maturingSecurity,
						instrument: maturingSecurity.instrument,
						hierarchyId: maturingSecurity.instrument.hierarchy.id
					};
				}
			}

		},
		{
			header: 'New Holding Account', width: 100, dataIndex: 'rollTrade.holdingInvestmentAccount.number', idDataIndex: 'rollTrade.holdingInvestmentAccount.id', css: 'BACKGROUND-COLOR: #f0f0ff;',
			tradeEntry: true, useNull: false,
			tooltip: 'Holding Account to use for the new Forward trade',
			editor: {
				xtype: 'combo', displayField: 'number', url: 'investmentAccountListFind.json?ourAccount=false', tooltipField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow',
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					this.resetStore();
					const editor = queryEvent.combo.gridEditor;
					const record = editor.record;
					const grid = editor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					const f = fp.getForm();
					let tradeExecutionType = '';

					// query CLS-compliant holding accounts for CLS trades
					const tradeExecutionTypeField = f.findField('tradeExecutionType.name');
					if (TCG.isNotBlank(tradeExecutionTypeField)) {
						tradeExecutionType = tradeExecutionTypeField.getValueObject();
					}

					let issuingCompanyId = null;
					let clsHoldingAccountTypeName = null;
					if (TCG.isNotBlank(tradeExecutionType) && TCG.isTrue(tradeExecutionType.clsTrade) && TCG.isNotBlank(tradeExecutionType.requiredHoldingInvestmentAccountType)) {
						clsHoldingAccountTypeName = tradeExecutionType.requiredHoldingInvestmentAccountType.name;
					}
					else {
						issuingCompanyId = TCG.getValue('holdingAccount.issuingCompany.id', record.json);
					}

					queryEvent.combo.store.baseParams = {
						mainAccountId: TCG.getValue('clientAccount.id', record.json),
						mainPurpose: 'Trading: Forwards',
						mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value,
						issuingCompanyId: issuingCompanyId,
						accountType: clsHoldingAccountTypeName
					};
				}
			}
		},
		// New Position Read Only columns for Forward with later Maturity Date
		{
			header: 'New Direction', width: 55, dataIndex: 'rollTradeBuy', sortable: false, tradeEntry: false, css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid; PADDING-LEFT: 0px !important;',
			tooltip: 'Direction of the new position.',
			renderer: function(v, metadata, record) {
				if (v !== '') {
					metadata.css = v ? 'buy-light' : 'sell-light';
					return v ? 'BUY' : 'SELL';
				}
			}
		},
		{header: 'Given CCY', dataIndex: 'rollTradeCurrencySymbol', width: 40, tradeEntry: false, css: 'BACKGROUND-COLOR: #f0f0ff;'},
		{
			header: 'Given Amount', dataIndex: 'rollTradeAmount', sortable: false, type: 'float', allowNegative: false, useNull: true, tradeEntry: false, css: 'BACKGROUND-COLOR: #f0f0ff;',
			renderer: function(v, metadata, record) {
				const tooltip = Clifton.trade.roll.getNetForwardRollTooltip(metadata, record);
				metadata.attr = TCG.renderQtip(tooltip);
				return TCG.renderAmount(v, false, '0,000.00');
			}
		},
		{
			header: 'New Forward', dataIndex: 'rollTrade.investmentSecurity.symbol', width: 120, tradeEntry: false, css: 'BACKGROUND-COLOR: #f0f0ff;',
			tooltip: 'New Currency Forward that will be used for the new Given Amount'
		},
		{header: 'New Holding Account', tradeEntry: false, width: 100, dataIndex: 'rollTrade.holdingInvestmentAccount.number', css: 'BACKGROUND-COLOR: #f0f0ff;'}
	]
});
Ext.reg('currency-forward-roll-detail-form-grid', Clifton.trade.roll.CurrencyForwardRollDetailFormGrid);

Clifton.trade.roll.getNetForwardRollTooltip = function(metadata, record) {
	const maturingSecuritySymbol = record.data['maturingSecurity.symbol'];
	const baseCurrencySymbol = record.data['maturingSecurity.instrument.tradingCurrency.symbol'];

	let maturingAmount = record.json.maturingAmount;
	const closeTrade = record.json.closeTrade;
	let closingAmount;
	let closingCurrency;
	let closingBuy;
	if (closeTrade) {
		closingCurrency = record.data['closeTradeCurrencySymbol'];
		closingBuy = record.data['closeTradeBuy'];
		closingAmount = record.data['closeTradeAmount'];
		if (TCG.getValue('closeTrade.workflowState.name', record.json) === 'Booked') {
			closingAmount = 0;
		}
	}
	else {
		closingCurrency = record.data['closeGivenCurrency.symbol'];
		closingBuy = record.data['closeBuy'];
		closingAmount = record.data['closeGivenAmount'];
	}
	if (closingCurrency === baseCurrencySymbol) {
		maturingAmount = record.json.settlementAmount;
		closingBuy = !closingBuy;
	}
	closingAmount = closingAmount * (closingBuy === true ? 1 : -1);
	const netAmount = maturingAmount + closingAmount;

	let rollSecurity;
	let rollAmount;
	let rollBuy;
	const rollTrade = record.json.rollTrade;
	let rollCurrency;
	if (rollTrade) {
		rollSecurity = rollTrade.investmentSecurity;
		rollCurrency = record.data['rollTradeCurrencySymbol'];
		rollAmount = record.data['rollTradeAmount'];
		rollBuy = record.data['rollTradeBuy'];
	}
	else {
		rollSecurity = record.json.rollSecurity ? record.json.rollSecurity : null;
		rollCurrency = record.data['rollGivenCurrency.symbol'];
		rollAmount = rollSecurity ? record.data['rollGivenAmount'] : 0;
		rollBuy = record.data['rollBuy'];
	}
	if (rollAmount > 0) {
		if (rollCurrency === baseCurrencySymbol) {
			rollBuy = !rollBuy;
		}
		rollAmount = rollAmount * (rollBuy === true ? 1 : -1);
	}

	let qtip = '<table cellspacing="5">';
	qtip += '<tr><td colspan="5"><b>Summary:</b></td>';
	qtip += '<tr><td>Forward</td><td>Currency</td><td align="right">Maturing Amount</td><td align="right">Trade Impact</td><td align="right">Total</td></tr>';
	qtip += '<tr><td>' + maturingSecuritySymbol + '</td><td>' + closingCurrency + '</td><td align="right">' + TCG.renderAmount(maturingAmount, true, '0,000.00') + '</td><td align="right">' + TCG.renderAmount(closingAmount, true, '0,000.00') + '</td><td align="right">' + TCG.renderAmount(netAmount, true, '0,000.00') + '</td></tr>';
	if (rollSecurity) {
		qtip += '<tr><td>' + rollSecurity.symbol + '</td><td>' + rollCurrency + '</td><td>&nbsp;</td><td align="right">' + TCG.renderAmount(rollAmount, true, '0,000.00') + '</td>' + '<td align="right">' + TCG.renderAmount(rollAmount, true, '0,000.00') + '</td></tr>';
	}
	qtip += '<tr><td colspan="4"><hr></td></tr>';
	qtip += '<tr><td><b>Net Trade:</b></td><td colspan="3">&nbsp;</td><td align="right">' + TCG.renderAmount(netAmount + rollAmount, true, '0,000.00') + '</td></tr>';
	qtip += '</table>';
	return qtip;
};
