Clifton.trade.roll.RollGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Roll',
	iconCls: 'arrow-right-green',
	width: 1170,
	height: 620,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	saveTimeout: 200,
	doNotWarnOnCloseModified: true,

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Roll',
				items: [{
					xtype: 'formpanel',
					loadValidation: false,
					url: 'tradeRollGroup.json?enableOpenSessionInView=true',
					labelFieldName: 'rollTradeGroup.label',
					getSaveURL: function() {
						return 'tradeRollGroupSave.json?enableOpenSessionInView=true';
					},

					listRequestedProperties: 'id|holdingAccount.issuingCompany.name|clientAccount.id|clientAccount.label|holdingAccount.id|holdingAccount.label|actualQuantity|tailQuantity|rollQuantity|tailTrade.id|tailTrade.workflowState.name|tailTrade.blockTrade|rollFromTrade.id|rollFromTrade.workflowState.name|rollFromTrade.blockTrade|rollToTrade.id|rollToTrade.workflowState.name|rollToTrade.blockTrade|rollFromTrade.executingBrokerCompany.id|rollToTrade.executingBrokerCompany.id|tailTrade.executingBrokerCompany.id|rollFromTrade.executingBrokerCompany.label|' +
						'rollToTrade.executingBrokerCompany.label|tailTrade.executingBrokerCompany.label|rollFromTrade.tradeDestination.id|rollToTrade.tradeDestination.id|tailTrade.tradeDestination.id|rollFromTrade.tradeDestination.name|rollToTrade.tradeDestination.name|tailTrade.tradeDestination.name|rollFromTrade.settlementDate|rollToTrade.settlementDate|tailTrade.settlementDate',
					listRequestedPropertiesRoot: 'data.positionList',

					getLoadParams: function(win) {
						const params = win.params;
						params.requestedProperties = this.listRequestedProperties;
						params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
						// remove serialization of unused data
						params.requestedPropertiesToExclude = 'data.rollTradeGroup.tradeList';
						return params;
					},

					getSubmitParams: function() {
						return {
							requestedProperties: this.listRequestedProperties,
							requestedPropertiesRoot: this.listRequestedPropertiesRoot
						};
					},

					listeners: {
						// populate broker, destination, settlement date and block trade if values are consistent for all trades in the Roll
						afterload: function(formPanel) {
							if (formPanel.getFormValue('tradeGroupAction') === 'FILL_EXECUTE' && TCG.isNotBlank(formPanel.getFormValue('fillQuantity'))) {
								// Partially Filled - present message to create new Trade Roll for remaining quantity to fill
								TCG.showInfo('You have partially filled the trade ticket. The remaining unfilled quantity can no longer be filled with this ticket. Please create a new Security Roll for the remaining trade quantities for this trade.', 'Security Roll Partial Fill');
							}

							const positionList = formPanel.getFormValue('positionList');
							let blockTrade = positionList[0].rollToTrade.blockTrade;
							let executingBrokerCompanyId = positionList[0].rollToTrade.executingBrokerCompany.id;
							let tradeDestinationId = positionList[0].rollToTrade.tradeDestination.id;
							let settlementDate = positionList[0].rollToTrade.settlementDate;
							positionList.forEach(position => {
								// determine which types of trades need to be checked
								const tradesToCheck = [position.rollToTrade, position.rollFromTrade];
								if (TCG.isNotNull(position.tailTrade)) {
									tradesToCheck.push(position.tailTrade);
								}
								tradesToCheck.forEach(trade => {
									if (TCG.isNotNull(blockTrade) && trade.blockTrade !== blockTrade) {
										blockTrade = null;
									}
									if (TCG.isNotNull(executingBrokerCompanyId) && trade.executingBrokerCompany && TCG.isNotEquals(trade.executingBrokerCompany.id, executingBrokerCompanyId)) {
										executingBrokerCompanyId = null;
									}
									if (TCG.isNotNull(tradeDestinationId) && trade.tradeDestination && TCG.isNotEquals(trade.tradeDestination.id, tradeDestinationId)) {
										tradeDestinationId = null;
									}
									if (TCG.isNotNull(settlementDate) && TCG.isNotEquals(trade.settlementDate, settlementDate)) {
										settlementDate = null;
									}
								});
							});

							// update values in formpanel
							if (TCG.isNotNull(blockTrade)) {
								formPanel.setFormValue('blockTrade', blockTrade);
								TCG.getChildByName(formPanel, 'blockTrade').show();
							}
							if (TCG.isNotNull(executingBrokerCompanyId)) {
								formPanel.setFormValue('executingBrokerCompanyId', executingBrokerCompanyId);
								formPanel.setFormValue('executingBrokerCompany', positionList[0].rollToTrade.executingBrokerCompany.label);
							}
							if (TCG.isNotNull(tradeDestinationId)) {
								formPanel.setFormValue('tradeDestinationId', tradeDestinationId);
								formPanel.setFormValue('tradeDestination', positionList[0].rollToTrade.tradeDestination.name);
							}
							if (TCG.isNotNull(settlementDate)) {
								formPanel.setFormValue('settlementDate', settlementDate);
							}
						}
					},

					defaults: {anchor: '0'},
					items: [
						{
							xtype: 'drag-drop-container',
							bindToFormLoad: true,
							message: 'Drag and drop file here to create <b>Trade Verification</b> note for the following trades and attach this file to it.',
							allowMultiple: false,
							saveUrl: 'tradeNoteForTradeGroupUpload.json',
							getParams: function() {
								const panel = TCG.getParentFormPanel(this);
								const id = TCG.getValue('rollTradeGroup.id', panel.getForm().formValues);
								return {
									tradeGroupId: id,
									noteTypeName: 'Trade Verification'
								};
							}
						},

						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .4,
									layout: 'form',
									items: [
										{fieldLabel: 'Security', name: 'rollTradeGroup.investmentSecurity.label', xtype: 'linkfield', detailIdField: 'rollTradeGroup.investmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{fieldLabel: 'New Security', name: 'rollTradeGroup.secondaryInvestmentSecurity.label', xtype: 'linkfield', detailIdField: 'rollTradeGroup.secondaryInvestmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{fieldLabel: 'Action', name: 'tradeGroupAction', xtype: 'hidden'},
										{name: 'createBlockTrades', xtype: 'hidden'},
										{xtype: 'integerfield', name: 'fillMaxQty', hidden: true},
										{xtype: 'integerfield', name: 'fillQuantity', hidden: true},
										{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingBrokerCompanyId', qtip: 'Only populated when all trades have the same Executing Broker'},
										{fieldLabel: 'Trade Destination', name: 'tradeDestination', xtype: 'linkfield', detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', detailIdField: 'tradeDestinationId', qtip: 'Only populated when all trades have the same Trade Destination'}
									]
								},

								{columnWidth: .03, items: [{html: '&nbsp;'}]},

								{
									columnWidth: .32,
									layout: 'form',
									items: [
										{fieldLabel: 'Trader', name: 'rollTradeGroup.traderUser.label', xtype: 'linkfield', detailIdField: 'rollTradeGroup.traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Trade Date', name: 'rollTradeGroup.tradeDate', xtype: 'displayfield', type: 'date'},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'displayfield', type: 'date', qtip: 'Only populated when all trades have the same Settlement Date'}

									]
								},

								{columnWidth: .03, items: [{html: '&nbsp;'}]},

								{
									columnWidth: .22,
									layout: 'form',
									items: [
										{fieldLabel: 'Group ID', name: 'rollTradeGroup.id', xtype: 'displayfield'},
										{fieldLabel: 'Roll Group', name: 'rollTradeGroup.id', xtype: 'hidden'},
										{fieldLabel: 'Tail Group', name: 'tailTradeGroup.id', xtype: 'linkfield', detailIdField: 'tailTradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
										{fieldLabel: 'Block Trade', name: 'blockTrade', xtype: 'checkbox', disabled: true, hidden: true, qtip: 'Only visible when all or none of the trades are block trades'}
									]
								}
							]
						},
						{
							xtype: 'formgrid-scroll',
							height: 400,
							heightResized: true,
							storeRoot: 'positionList',
							dtoClass: 'com.clifton.trade.roll.TradeRollPosition',
							readOnly: true,
							collapsible: false,
							groupField: 'holdingAccount.issuingCompany.name',
							anchor: '-20',

							executeAction: function(groupAction) {
								if (this.submitField.getValue() === '[]') {
									TCG.showError('No rolls selected.  Please select at least one roll from the list.');
									return;
								}
								const panel = TCG.getParentFormPanel(this);
								const form = panel.getForm();
								form.findField('tradeGroupAction').setValue(groupAction);
								this.getWindow().saveWindow(false);
							},

							convertTradesBlockTrades: function(toBlockTrades) {
								if (this.submitField.getValue() === '[]') {
									TCG.showError('No rolls selected.  Please select at least one roll from the list.');
									return;
								}
								const panel = TCG.getParentFormPanel(this);
								const form = panel.getForm();
								form.findField('createBlockTrades').setValue(TCG.isTrue(toBlockTrades));
								form.findField('tradeGroupAction').setValue('SAVE_TRADE');
								this.getWindow().saveWindow(false);
							},

							addToolbarButtons: function(toolBar) {
								const gp = this;

								toolBar.add({
									text: 'Approve',
									tooltip: 'Approve Roll/Tail Trades for Selected Roll(s) below.',
									iconCls: 'verify',
									scope: this,
									handler: function() {
										gp.executeAction('APPROVE');
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Reload',
									tooltip: 'Reload trades for this trade group.',
									iconCls: 'table-refresh',
									scope: this,
									handler: function() {
										gp.reload();
									}
								});
								toolBar.add('-');
								const exportMenu = new Ext.menu.Menu();
								exportMenu.add({
									text: 'Export to Excel',
									iconCls: 'excel',
									menu: {
										items: [
											{
												text: 'From Trades (Excel 97-2003)',
												iconCls: 'excel',
												handler: function() {
													gp.exportGrid('xls', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.investmentSecurity.id'));
												}
											}, {
												text: 'To Trades (Excel 97-2003)',
												iconCls: 'excel',
												handler: function() {
													gp.exportGrid('xls', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.secondaryInvestmentSecurity.id'));
												}
											}, {
												text: 'All Trades (Excel 97-2003)',
												iconCls: 'excel',
												handler: function() {
													gp.exportGrid('xls');
												}
											}, '-', {
												text: 'From Trades (*.xlsx)',
												iconCls: 'excel_open_xml',
												handler: function() {
													gp.exportGrid('xlsx', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.investmentSecurity.id'));
												}
											}, {
												text: 'To Trades (*.xlsx)',
												iconCls: 'excel_open_xml',
												handler: function() {
													gp.exportGrid('xlsx', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.secondaryInvestmentSecurity.id'));
												}
											}, {
												text: 'All Trades (*.xlsx)',
												iconCls: 'excel_open_xml',
												handler: function() {
													gp.exportGrid('xlsx');
												}
											}

										]
									}
								});
								exportMenu.add({
									text: 'Export to CSV',
									iconCls: 'csv',
									menu: {
										items: [
											{
												text: 'From Trades',
												iconCls: 'csv',
												handler: function() {
													gp.exportGrid('csv', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.investmentSecurity.id'));
												}
											}, {
												text: 'To Trades',
												iconCls: 'csv',
												handler: function() {
													gp.exportGrid('csv', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.secondaryInvestmentSecurity.id'));
												}
											}, {
												text: 'All Trades',
												iconCls: 'csv',
												handler: function() {
													gp.exportGrid('csv');
												}
											}
										]
									}
								});
								exportMenu.add({
									text: 'Export to PDF',
									iconCls: 'pdf',
									menu: {
										items: [
											{
												text: 'From Trades',
												iconCls: 'pdf',
												handler: function() {
													gp.exportGrid('pdf', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.investmentSecurity.id'));
												}
											}, {
												text: 'To Trades',
												iconCls: 'pdf',
												handler: function() {
													gp.exportGrid('pdf', TCG.getParentFormPanel(gp).getFormValue('rollTradeGroup.secondaryInvestmentSecurity.id'));
												}
											}, {
												text: 'All Trades',
												iconCls: 'pdf',
												handler: function() {
													gp.exportGrid('pdf');
												}
											}
										]
									}
								});
								exportMenu.add({
									text: 'Trade Allocation',
									tooltip: 'View trades of this trade group by executing broker for viewing and sending trade allocations',
									iconCls: 'export',
									handler: function() {
										const form = TCG.getParentFormPanel(gp).getForm();
										TCG.createComponent('Clifton.trade.TradeAllocationWindow', {defaultData: {tradeGroupId: TCG.getValue('rollTradeGroup.id', form.formValues)}});
									}
								});
								toolBar.add({
									text: 'Tools',
									iconCls: 'config',
									menu: exportMenu
								});

								toolBar.addFill();
								toolBar.add({xtype: 'label', html: 'From Price:&nbsp;'});
								toolBar.add({xtype: 'pricefield', name: 'fromSecurityPrice', width: 70});
								toolBar.add({xtype: 'label', html: '&nbsp;To Price:&nbsp;'});
								toolBar.add({xtype: 'pricefield', name: 'toSecurityPrice', width: 70});

								toolBar.add({
									text: 'Fill & Execute',
									iconCls: 'add',
									tooltip: 'Fill Rolls only with given prices. Selected rolls will be fully filled. Tails can be filled via the Trading Blotter.  Trades will also be executed after fills are created.',
									scope: toolBar,
									handler: function() {
										if (gp.submitField.getValue() === '[]') {
											TCG.showError('No rolls selected.  Please select at least one roll to fill.');
											return;
										}
										const fPrice = TCG.getChildByName(toolBar, 'fromSecurityPrice').getValue();
										const tPrice = TCG.getChildByName(toolBar, 'toSecurityPrice').getValue();
										if (TCG.isBlank(fPrice) || TCG.isBlank(tPrice)) {
											TCG.showError('From and To Security Unit Price must be populated.', 'Validation');
											return;
										}
										const panel = TCG.getParentFormPanel(this);
										const form = panel.getForm();
										form.findField('fillQuantity').setValue('');
										form.findField('tradeGroupAction').setValue('FILL_EXECUTE');
										gp.getWindow().saveWindow(false);
									}
								});
								toolBar.add('-');
								toolBar.add({xtype: 'label', html: '&nbsp;Fill Qty:&nbsp;'});
								toolBar.add({xtype: 'integerfield', name: 'partialQty', width: 70});
								toolBar.add({
									text: 'Partially Fill & Execute',
									iconCls: 'add',
									tooltip: 'Fill Rolls only with given prices and a total quantity.  Fills will be allocated proportionally as a percentage of the total, i.e. if filling 80% of the total available to fill, each trade will be filled 80%. Tails can be filled via the Trading Blotter.  Trades will also be executed after fills are created.',
									scope: toolBar,
									handler: function() {
										const panel = TCG.getParentFormPanel(this);
										const form = panel.getForm();
										if (gp.submitField.getValue() === '[]') {
											TCG.showError('No rolls selected.  Please select at least one roll to fill.');
											return;
										}
										const fillQty = TCG.getChildByName(toolBar, 'partialQty').getNumericValue();
										const maxFillQty = form.findField('fillMaxQty').getNumericValue();
										if (TCG.isBlank(fillQty) || fillQty > maxFillQty) {
											TCG.showError('Fill Quantity is required, and cannot exceed ' + TCG.numberFormat(maxFillQty, '0,000') + ' (the total roll quantity for selected rolls) for partial allocation.');
											return;
										}

										const fPrice = TCG.getChildByName(toolBar, 'fromSecurityPrice').getValue();
										const tPrice = TCG.getChildByName(toolBar, 'toSecurityPrice').getValue();
										if (TCG.isBlank(fPrice) || TCG.isBlank(tPrice)) {
											TCG.showError('From and To Security Unit Price must be populated.', 'Validation');
											return;
										}
										form.findField('fillQuantity').setValue(fillQty);
										form.findField('tradeGroupAction').setValue('FILL_EXECUTE');
										gp.getWindow().saveWindow(false);
									}
								});

								toolBar.add('-');

								const moreActionsMenu = new Ext.menu.Menu({
									items: [
										{
											text: 'Validate Trades',
											iconCls: 'row_reconciled',
											tooltip: 'Validate Selected Trades',
											scope: this,
											handler: function() {
												gp.executeAction('VALIDATE');
											}

										},

										{
											text: 'Import Fills',
											iconCls: 'import',
											tooltip: 'Import Trade Fills for Existing Trades',
											handler: function() {
												TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
											}
										},
										{
											text: 'Execute Rolls',
											tooltip: 'Execute selected filled Rolls(s).',
											iconCls: 'run',
											scope: this,
											handler: function() {
												gp.executeAction('EXECUTE');
											}
										}, '-',
										{
											text: 'Book & Post',
											tooltip: 'Book & Post selected Rolls(s).',
											iconCls: 'book-red',
											scope: this,
											handler: function() {
												gp.executeAction('BOOK');
											}
										},
										{
											text: 'Unbook',
											tooltip: 'Unbook Rolls for Selected Booked Rolls(s) below.',
											iconCls: 'undo',
											scope: this,
											handler: function() {
												gp.executeAction('UNBOOK');
											}
										}, '-',
										{
											text: 'Convert To Block',
											tooltip: 'Convert the trades of the selected roll(s) to block trades. The trades of the selected roll(s) will need to be rejected to allow editing.',
											iconCls: 'run',
											scope: this,
											handler: function() {
												gp.convertTradesBlockTrades(true);
											}
										},
										{
											text: 'Convert From Block',
											tooltip: 'Convert the trades of the selected roll(s) from block trades to normal trades. The trades of the selected roll(s) will need to be rejected to allow editing.',
											iconCls: 'undo',
											scope: this,
											handler: function() {
												gp.convertTradesBlockTrades(false);
											}
										}, '-',
										{
											text: 'Reject Trades',
											tooltip: 'Reject the trades of the selected rolls(s) so they can be edited.',
											iconCls: 'run',
											scope: this,
											handler: function() {
												gp.executeAction('REJECT');
											}
										},
										{
											text: 'Delete Fills',
											tooltip: 'Delete all Fills for Selected Rolls(s).',
											iconCls: 'remove',
											scope: this,
											handler: function() {
												gp.executeAction('DELETE_FILLS');
											}
										},
										{
											text: 'Cancel',
											tooltip: 'Cancel Roll/Tail Trades for Selected Roll(s) below.',
											iconCls: 'cancel',
											scope: this,
											handler: function() {
												gp.executeAction('CANCEL');
											}
										}
									]
								});

								toolBar.add({
									text: 'More Actions',
									iconCls: 'workflow',
									menu: moreActionsMenu
								});


							},
							columnsConfig: [
								{
									width: 30, dataIndex: 'includeInRoll', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false,
									menuDisabled: true,
									header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', ''),
									renderer: function(v, p, record) {
										p.css += ' x-grid3-check-col-td';
										if (TCG.isBlank(v)) {
											record.data['includeInRoll'] = false;
											v = false;
										}
										return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
									}
								},
								{header: 'Holding Company', width: 100, dataIndex: 'holdingAccount.issuingCompany.name', hidden: true},
								{header: 'Client Account', width: 260, dataIndex: 'clientAccount.label'},
								{header: 'Holding Account', width: 250, dataIndex: 'holdingAccount.label'},
								{header: 'Tail Trade ID', width: 20, hidden: true, dataIndex: 'tailTrade.id', type: 'int', useNull: true},
								{header: 'Tail Executing Broker', width: 100, dataIndex: 'tailTrade.executingBrokerCompany.label', idDataIndex: 'tailTrade.executingBrokerCompany.id', hidden: true},
								{header: 'Tail Trade Destination', width: 70, dataIndex: 'tailTrade.tradeDestination.name', idDataIndex: 'tailTrade.tradeDestination.id', hidden: true},
								{header: 'Tail Trade Settlement Date', width: 50, dataIndex: 'tailTrade.settlementDate', hidden: true},
								{header: 'Tail Trade Block', width: 20, hidden: true, dataIndex: 'tailTrade.blockTrade', type: 'boolean', tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
								{
									header: 'Tail State', width: 85, dataIndex: 'tailTrade.workflowState.name',
									renderer: function(value, metaData, r) {
										if (value === 'Invalid') {
											metaData.css = 'ruleViolation';
										}
										return value;
									}
								},
								{header: 'Roll From Trade ID', width: 20, hidden: true, dataIndex: 'rollFromTrade.id', type: 'int', useNull: true},
								{header: 'Roll From Executing Broker', width: 100, dataIndex: 'rollFromTrade.executingBrokerCompany.label', idDataIndex: 'rollFromTrade.executingBrokerCompany.id', hidden: true},
								{header: 'Roll From Trade Destination', width: 70, dataIndex: 'rollFromTrade.tradeDestination.name', idDataIndex: 'rollFromTrade.tradeDestination.id', hidden: true},
								{header: 'Roll From Trade Settlement Date', width: 50, dataIndex: 'rollFromTrade.settlementDate', hidden: true},
								{header: 'Roll From Trade Block', width: 20, hidden: true, dataIndex: 'rollFromTrade.blockTrade', type: 'boolean', tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
								{
									header: 'Roll From State', width: 85, dataIndex: 'rollFromTrade.workflowState.name',
									renderer: function(value, metaData, r) {
										if (value === 'Invalid') {
											metaData.css = 'ruleViolation';
										}
										return value;
									}
								},
								{header: 'Roll To Trade ID', width: 20, hidden: true, dataIndex: 'rollToTrade.id', type: 'int', useNull: true},
								{header: 'Roll To Executing Broker', width: 100, dataIndex: 'rollToTrade.executingBrokerCompany.label', idDataIndex: 'rollToTrade.executingBrokerCompany.id', hidden: true},
								{header: 'Roll To Trade Destination', width: 70, dataIndex: 'rollToTrade.tradeDestination.name', idDataIndex: 'rollToTrade.tradeDestination.id', hidden: true},
								{header: 'Roll To Trade Settlement Date', width: 50, dataIndex: 'rollToTrade.settlementDate', hidden: true},
								{header: 'Roll To Trade Block', width: 20, hidden: true, dataIndex: 'rollToTrade.blockTrade', type: 'boolean', tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
								{
									header: 'Roll To State', width: 85, dataIndex: 'rollToTrade.workflowState.name',
									renderer: function(value, metaData, r) {
										if (value === 'Invalid') {
											metaData.css = 'ruleViolation';
										}
										return value;
									}
								},
								{header: 'Original Quantity', width: 70, dataIndex: 'actualQuantity', type: 'int', useNull: true, summaryType: 'sum', tooltip: 'The position quantity of the rolled from security.'},
								{header: 'Tail Quantity', width: 70, dataIndex: 'tailQuantity', type: 'int', useNull: true, summaryType: 'sum', tooltip: 'The quantity of the original quantity to close and not roll.'},
								{header: 'Roll Quantity', width: 70, dataIndex: 'rollQuantity', type: 'int', summaryType: 'sum', tooltip: 'The quantity of the original quantity to roll to the new security.'},
								{
									header: 'Fill Max Quantity', width: 70, type: 'int', summaryType: 'sum',
									renderer: function(value, metaData, r) {
										let fillMaxQty = 0;
										if (TCG.isTrue(r.data.includeInRoll)) {
											fillMaxQty = r.data.rollQuantity;
										}
										return fillMaxQty;
									},
									summaryCalculation: function(v, r, field, data, col) {
										if (TCG.isTrue(r.data.includeInRoll)) {
											return v + r.data.rollQuantity;
										}
										return v;
									},
									summaryRenderer: function(v) {
										return TCG.renderAmount(v, false, '0,000');
									}
								}
							],
							plugins: {ptype: 'gridsummary'},
							allRows: false,
							toggleAll: function() {
								this.allRows = !TCG.isTrue(this.allRows);
								const grid = this;
								grid.getColumnModel().setColumnHeader(0, String.format('<div class="x-grid3-check-col{0}">&#160;</div>', this.allRows ? '-on' : ''));
								let i = 0;
								let row = grid.store.data.items[i];
								while (row) {
									row.set('includeInRoll', this.allRows);
									row = grid.store.data.items[i++];
								}
								this.markModified();
							},
							doNotSubmitFields: ['includeInRoll', 'Fill Max Quantity'],
							markModified: function(initOnly) {
								const result = [];
								let newId = -10;
								this.store.each(function(record) {
									if (record.modified && TCG.isTrue(record.modified['includeInRoll'])) {
										record.id = newId--;
										record.markDirty(); // new records should be fully submitted

										const r = {
											id: record.id,
											'class': this.dtoClass
										};
										for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
											const f = this.alwaysSubmitFields[i];
											const val = TCG.getValue(f, record.json);
											if (TCG.isNotNull(val)) {
												let v = TCG.getValue(f, record.json);
												if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
													v = TCG.renderDate(v);
												}
												r[f] = v;
											}
										}
										// copy all modified fields
										for (const m of Object.keys(record.modified)) {
											const field = record.fields.get(m);
											// skip 'text' field submission when there's 'value'
											if ((!field || !field.valueFieldName) && this.doNotSubmitFields.indexOf(m) === -1) {
												let v = record.modified[m];
												let skip = false;
												if (typeof v === 'object') {
													if (TCG.isNull(v)) {
														skip = true;
													}
													else if (Ext.isDate(v)) {
														v = v.dateFormat('m/d/Y');
													}
												}
												if (!skip) {
													r[m] = v;
												}
											}
										}
										result.push(r);
									}
								}, this);
								const value = Ext.util.JSON.encode(result);
								this.submitField.setValue(value); // changed value
								if (initOnly) {
									this.submitField.originalValue = value; // initial value
								}
								else {
									this.getWindow().setModified(true);
								}
							},

							reload: function() {
								const gp = this;
								const fp = TCG.getParentFormPanel(this);
								const url = fp.getLoadURL();
								const win = fp.getWindow();
								fp.load({
									url: encodeURI(url),
									params: fp.getLoadParams(win),
									waitMsg: 'Loading...',
									success: function(form, action) {
										fp.loadJsonResult(action.result, true);
										const data = fp.getForm().formValues;
										gp.store.loadData(data);
										gp.markModified(true);
									}
								});
							},

							listeners: {
								afterRender: function() {
									const grid = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
											grid.reload();
										}
									}, this);
								},
								// drill into specific trade journal
								'celldblclick': function(grid, rowIndex, cellIndex, evt) {
									const row = grid.store.data.items[rowIndex];
									const column = grid.getColumnModel().getColumnAt(cellIndex);
									let tid = '';
									if (column.dataIndex) {
										if (column.dataIndex.includes('tail')) {
											tid = row.data['tailTrade.id'];
										}
										else if (column.dataIndex.includes('rollFromTrade')) {
											tid = row.data['rollFromTrade.id'];
										}
										else if (column.dataIndex.includes('rollToTrade')) {
											tid = row.data['rollToTrade.id'];
										}
									}

									if (TCG.isNotBlank(tid)) {
										TCG.createComponent('Clifton.trade.TradeWindow', {
											id: TCG.getComponentId('Clifton.trade.TradeWindow', tid),
											params: {id: tid},
											openerCt: grid
										});
									}
								},


								grandtotalupdated: function(v, cf) {
									// NOTE: USE FORM PANEL setFormValue METHOD SO WINDOW IS NOT MARKED AS MODIFIED!
									if (cf.dataIndex === 'Fill Max Quantity') {
										this.setFormCalculatedValue('fillMaxQty', v);
									}
								},
								'headerclick': function(grid, col, e) {
									if (col === 0) {
										grid.toggleAll();
									}
								}
							},

							setFormCalculatedValue: function(fieldName, v) {
								const f = TCG.getParentFormPanel(this);
								f.setFormValue(fieldName, v, true);
							},

							exportGrid: function(outputFormat, securityId) {
								const formPanel = TCG.getParentFormPanel(this),
									params = {
										tradeGroupId: formPanel.getFormValue('rollTradeGroup.id'),
										excludeWorkflowStateName: 'Cancelled'
									};
								if (TCG.isNotBlank(securityId)) {
									params.securityId = securityId;
								}
								params.columns = this.getColumnsMetaData();
								params.outputFormat = outputFormat;
								// filename will be of format: RollTrade_Trades_03082018.xsls
								params.fileName = formPanel.getFormValue('rollTradeGroup.tradeGroupType.name').replace(/\s/g, '')
									+ '_Trades_' + new Date(formPanel.getFormValue('rollTradeGroup.tradeDate')).format('mdY');
								TCG.downloadFile('tradeListFind.json', params, this);
							},

							getColumnsMetaData: function() {
								return 'Client Account:clientInvestmentAccount.label|'
									+ 'Holding Account:holdingInvestmentAccount.label|'
									+ 'Holding Company:holdingInvestmentAccount.issuingCompany.name|'
									+ 'Executing Broker:executingBrokerCompany.name|'
									+ 'Security:investmentSecurity.symbol|'
									+ 'Buy/Sell:buy|'
									+ 'Quantity:quantityIntended:align=right';
							}
						}
					]
				}]
			},

			{
				title: 'Trade Asset Class Allocations',
				items: [{
					xtype: 'trade-assetclass-position-allocation-grid',

					getLoadParams: function(firstLoad) {
						const f = this.getWindow().getMainForm();
						const rollGroupId = TCG.getValue('rollTradeGroup.id', f.formValues);
						const tailGroupId = TCG.getValue('tailTradeGroup.id', f.formValues);

						const gIds = [];
						if (TCG.isNotBlank(rollGroupId)) {
							gIds.push(rollGroupId);
						}
						if (TCG.isNotBlank(tailGroupId)) {
							gIds.push(tailGroupId);
						}
						const params = {'tradeGroupIds': gIds};
						return this.appendToolbarFilterParams(params);
					}
				}]
			}]
	}]
});



