Clifton.trade.roll.RollEntryWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Trading: Security Roll',
	iconCls: 'arrow-right-green',
	width: 1300,
	height: 700,
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	saveTimeout: 200,
	doNotWarnOnCloseModified: true,

	defaultRollTypeName: 'Default Futures Roll Dates',

	getDefaultRollType: function() {
		return this.getRollTypeByName(this.defaultRollTypeName);
	},

	getRollTypeByName: function(name) {
		if (name) {
			return TCG.data.getData('tradeRollTypeByName.json?name=' + name, this, 'trade.roll.type.' + name);
		}
		return undefined;
	},

	setDoNotWarnOnCloseModified: function(newVal) {
		this.doNotWarnOnCloseModified = newVal;
	},

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [

			{
				title: 'Roll Entry',
				items: [{
					xtype: 'formpanel',
					loadValidation: false,
					loadDefaultDataAfterRender: true,
					getSaveURL: function() {
						return 'tradeRollEntrySave.json';
					},
					instructions: 'Use the following screen to roll Futures.  The first column of fields is used to filter the positions to filter.  Select at least a Security & Long or Short Positions before trying to load current positions.  All fields marked as required are required for submitting trades.',

					getDefaultData: function(win) {
						const now = new Date().format('Y-m-d 00:00:00');
						let dd = win.defaultData || {};
						if (win.defaultDataIsReal) {
							return dd;
						}
						dd = Ext.apply({
							traderUser: TCG.getCurrentUser(),
							tradeDate: now,
							rollType: win.getDefaultRollType()
						}, dd);
						return dd;
					},


					getLoadParams: function(win) {
						const params = win.params;
						params.requestedProperties = this.listRequestedProperties;
						params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
						return params;
					},

					listRequestedProperties: 'id|holdingAccount.issuingCompany.name|executingBrokerCompany.name|clientAccount.id|clientAccount.label|holdingAccount.id|holdingAccount.label|longPosition|quantity|previousQuantity|actualQuantity|pendingQuantity|tailQuantity|rollQuantity',
					listRequestedPropertiesRoot: 'data.positionList',

					confirmBeforeSaveMsg: 'Are you sure you want to submit trades for all tails/rolls entered?',
					confirmBeforeSaveMsgTitle: 'Submit Trades',

					defaults: {anchor: '0'},
					items: [
						//Need to add id as a hidden field and submitValue = false so that id is not double submitted
						{name: 'id', xtype: 'hidden', submitValue: false},
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .34,
									layout: 'form',
									items: [
										{fieldLabel: 'Roll Type', name: 'rollType.name', hiddenName: 'rollType.id', xtype: 'combo', url: 'tradeRollTypeList.json', loadAll: 'true', allowBlank: false},
										{
											fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false,
											requiredFields: ['rollType.name'],
											listeners: {
												beforeQuery: function(qEvent) {
													const f = qEvent.combo.getParentForm().getForm();
													const it = TCG.getParentFormPanel(qEvent.combo).getWindow().getRollTypeByName(f.findField('rollType.name').getRawValue()).investmentType;
													if (it) {
														qEvent.combo.store.baseParams = {investmentType: it.name, activeOnDate: f.findField('tradeDate').value};
													}
													else {
														qEvent.combo.store.baseParams = {oneSecurityPerInstrument: false, activeOnDate: f.findField('tradeDate').value};
													}
												},
												select: function(combo, record, index) {
													const f = combo.getParentForm().getForm();
													Clifton.investment.instrument.getTradeDatePromise(record.id, null, combo)
														.then(function(td) {
															f.findField('tradeDate').setValue(td);
															TCG.data.getDataPromise('tradeRollDefaultNewSecurityForSecurity.json?rollTypeId=' + f.findField('rollType.id').getValue() + '&securityId=' + record.id + '&date=' + td.format('m/d/Y'), combo)
																.then(function(defNS) {
																	if (defNS) {
																		f.findField('newSecurity.label').setValue({value: defNS.id, text: defNS.label});
																		f.formValues.newSecurity = defNS; // needs this in order to find the record when tabbing out when the store isn't initialized
																	}
																});

															return Clifton.investment.instrument.getSettlementDatePromise(record.id, td, null, null, combo);
														})
														.then(function(settlementDate) {
															f.findField('settlementDate').setValue(settlementDate);
														});
												}
											}
										},
										{fieldLabel: 'Holding Company', name: 'holdingCompany.label', hiddenName: 'holdingCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', displayField: 'label', url: 'businessCompanyListFind.json?issuer=true'},
										{
											xtype: 'radiogroup',
											fieldLabel: 'Positions',
											columns: [70, 70],
											allowBlank: false,
											anchor: '-82',
											items: [
												{boxLabel: 'Long', name: 'longPositions', inputValue: true},
												{boxLabel: 'Short', name: 'longPositions', inputValue: false}
											]
										}
									]
								},

								{columnWidth: .03, items: [{html: '&nbsp;'}]},

								{
									columnWidth: .34,
									layout: 'form',
									items: [
										{
											fieldLabel: 'New Security', name: 'newSecurity.label', hiddenName: 'newSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false,
											requiredFields: ['security.id'],
											listeners: {
												beforeQuery: function(qEvent) {
													const f = qEvent.combo.getParentForm().getForm();
													const secId = f.findField('security.id').getValue();
													const sec = TCG.data.getData('investmentSecurity.json?id=' + secId, qEvent.combo, 'investment.security.id.' + secId);
													qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value, instrumentId: sec.instrument.id};
												}
											}
										},
										{
											fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow',
											displayField: 'label',
											url: 'tradeExecutingBrokerCompanyListFind.json',
											allowBlank: false,
											listeners: {
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;
													const f = combo.getParentForm().getForm();

													combo.store.baseParams = {
														activeOnDate: f.findField('tradeDate').value,
														securityIdForTradeType: f.findField('security.id').getValue()
													};
												},

												select: function(combo, record, index) {
													// Clear trade destination when executing broker is selected
													const f = combo.getParentForm().getForm();
													const td = f.findField('tradeDestination.name');
													td.clearValue();
													td.reset();
													td.store.removeAll();
													td.lastQuery = null;
												}
											}
										},

										{
											fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false,
											listeners: {
												beforeQuery: function(qEvent) {
													const f = qEvent.combo.getParentForm().getForm();
													const eb = f.findField('executingBrokerCompany.id').getValue();
													if (TCG.isBlank(eb)) {
														TCG.showError('Please select an executing broker before attempting to select a trade destination.');
														return false;
													}
													qEvent.combo.store.baseParams = {
														activeOnDate: f.findField('tradeDate').value,
														executingBrokerCompanyId: eb,
														securityIdForTradeType: f.findField('security.id').getValue()
													};
												}
											}
										},

										{boxLabel: 'Create block trades', name: 'createBlockTrades', xtype: 'checkbox', qtip: 'If checked, the trades will be created as block trades. A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'}
									]
								},

								{columnWidth: .03, items: [{html: '&nbsp;'}]},

								{
									columnWidth: .26,
									layout: 'form',
									items: [

										{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, width: 100},
										{
											fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
											listeners: {
												change: function(field, newValue, oldValue) {
													const form = TCG.getParentFormPanel(field).getForm();
													const settlementDateField = form.findField('settlementDate');

													if (TCG.isBlank(newValue)) {
														settlementDateField.setValue('');
													}
													else {
														Clifton.investment.instrument.getSettlementDatePromise(form.findField('newSecurity.id').value, newValue, null, null, field)
															.then(function(settlementDate) {
																form.findField('settlementDate').setValue(settlementDate);
															});
													}
												},
												select: function(field, date) {
													const form = TCG.getParentFormPanel(field).getForm();
													Clifton.investment.instrument.getSettlementDatePromise(form.findField('newSecurity.id').value, date, null, null, field)
														.then(function(settlementDate) {
															form.findField('settlementDate').setValue(settlementDate);
														});
												}
											}
										},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false}

									]
								}
							]
						},


						{
							xtype: 'formgrid-scroll',
							height: 480,
							heightResized: true,
							storeRoot: 'positionList',
							dtoClass: 'com.clifton.trade.roll.TradeRollPosition',
							readOnly: true,
							collapsible: false,
							groupField: 'holdingAccount.issuingCompany.name',
							anchor: '-20',
							addToolbarButtons: function(toolBar) {
								const gp = this;
								toolBar.add({
									text: 'Load Positions',
									tooltip: 'Load Existing Security Positions for the selected security above',
									iconCls: 'table-refresh',
									scope: this,
									handler: function() {
										gp.reloadGrid();
										const win = this.getWindow();
										win.setDoNotWarnOnCloseModified(false);
									}
								});

								toolBar.add('-');

								toolBar.add({
									text: 'Submit Trades',
									tooltip: 'Create Tail and Roll Trades based on quantities entered below.',
									iconCls: 'shopping-cart',
									scope: this,
									handler: function() {
										gp.getWindow().saveWindow(false);
									}
								});
								toolBar.add('-');
								toolBar.addFill();
								toolBar.add({xtype: 'label', html: 'Roll Quantity:&nbsp;'});
								toolBar.add({xtype: 'floatfield', minValue: 0, name: 'rollQuantity', width: 70, submitValue: false});
								toolBar.add({
									text: 'Apply Quantity',
									iconCls: 'arrow-down-green',
									tooltip: 'Calculate a roll quantity for each position - round down the result of ([Position Quantity] * [Roll Quantity] / [Total Quantity]).<br/>NOTE: Trader will have to manually adjust some roll quantities to account for the rounding error.',
									scope: toolBar,
									handler: function() {
										let total = 0;
										gp.getStore().each(function(record) {
											if (TCG.isTrue(record.get('includeInRoll'))) {
												const quantity = Math.abs(record.get('quantity'));
												total = total + quantity;
											}
										}, this);
										const rollQuantity = TCG.getChildByName(toolBar, 'rollQuantity').getNumericValue();
										gp.getStore().each(function(record) {
											if (TCG.isTrue(record.get('includeInRoll'))) {
												const quantity = Math.abs(record.get('quantity'));
												const newRollQuantity = Math.floor(quantity * rollQuantity / total);
												record.set('rollQuantity', newRollQuantity);
											}
										}, this);

										gp.markModified();
									}
								});
								toolBar.add('-');
								toolBar.add({xtype: 'label', html: 'Tail Percentage&nbsp;(%):&nbsp;'});
								toolBar.add({xtype: 'floatfield', minValue: 0, maxValue: 100, name: 'tailPercentage', width: 70, submitValue: false});
								toolBar.add({
									text: 'Apply Tail',
									iconCls: 'arrow-down-green',
									tooltip: 'Apply the tail percentage - uses conventional rounding.',
									scope: toolBar,
									handler: function() {
										gp.getStore().each(function(record) {
											if (TCG.isTrue(record.get('includeInRoll'))) {
												const quantity = Math.abs(record.get('quantity'));
												const tailPercentage = TCG.getChildByName(toolBar, 'tailPercentage').getValue();
												const newTailQuantity = Math.round(quantity * (tailPercentage / 100));
												const newRollQuantity = quantity - newTailQuantity;
												record.set('tailQuantity', newTailQuantity);
												record.set('rollQuantity', newRollQuantity);
											}
										});

										gp.markModified();
									}
								});
							},

							// Defaults to Checked
							allRows: true,
							toggleAll: function() {
								this.allRows = !TCG.isTrue(this.allRows);
								const grid = this;
								const index = grid.getColumnModel().findColumnIndex('includeInRoll');
								grid.getColumnModel().setColumnHeader(index, String.format('<div class="x-grid3-check-col{0}">&#160;</div>', this.allRows ? '-on' : ''));
								let i = 0;
								let row = grid.store.data.items[i];
								while (row) {
									row.set('includeInRoll', this.allRows);
									grid.fireEvent('afteredit', grid, i, index);
									i = i + 1;
									row = grid.store.data.items[i];
								}
							},

							reloadGrid: function() {
								const grid = this;
								// get the submit parameters
								const panel = TCG.getParentFormPanel(this);
								const form = panel.getForm();

								const securityId = form.findField('security.id').getValue();
								const longPositions = form.findField('longPositions').getGroupValue();


								if (TCG.isBlank(securityId) || TCG.isBlank(longPositions)) {
									TCG.showError('A security and long/short positions must be selected in order to load existing positions.');
									return;
								}

								const params = Ext.apply({}, form.getValues());
								params['positionList'] = ''; // don't pass it in because we have to reload it anyway
								params['requestedProperties'] = panel.listRequestedProperties;
								params['requestedPropertiesRoot'] = panel.listRequestedPropertiesRoot;

								const loader = new TCG.data.JsonLoader({
									waitTarget: TCG.getParentFormPanel(grid).getEl(),
									params: params,
									scope: grid,
									onLoad: function(record, conf) {
										grid.store.loadData(record);
										grid.markModified(true);
									}
								});
								loader.load('tradeRollEntry.json');
							},
							alwaysSubmitFields: ['holdingAccount.id', 'clientAccount.id', 'previousQuantity', 'actualQuantity', 'pendingQuantity', 'tailQuantity', 'rollQuantity'],
							doNotSubmitFields: ['longPosition', 'quantity', 'includeInRoll'],

							onAfterUpdateFieldValue: function(editor, field) {
								const f = editor.field;
								if (f === 'tailQuantity') {
									const v = editor.value;
									const long = TCG.isTrue(editor.record.get('longPosition'));
									let total = editor.record.get('quantity');
									if (!TCG.isTrue(long)) {
										total = total * -1;
									}
									if (total - v < 0) {
										editor.record.set('rollQuantity', 0);
									}
									else {
										editor.record.set('rollQuantity', total - v);
									}
								}
							},
							listeners: {
								afterRender: function() {
									const grid = this;
									const fp = TCG.getParentFormPanel(this);
									fp.on('afterload', function() {
										if (TCG.isTrue(grid.getWindow().savedSinceOpen)) {
											this.reloadGrid();
										}
									}, this);
								},
								afteredit: function(grid, row, col) {
									// on checkbox clear, set roll to 0 and on check, set it to full roll
									if (grid.colModel) {
										const includeInRollIndex = grid.colModel.findColumnIndex('includeInRoll');
										if (col === includeInRollIndex) {
											const rollQuantityIndex = grid.colModel.findColumnIndex('rollQuantity');
											grid.stopEditing();
											grid.startEditing(row, rollQuantityIndex);
											const rollEditor = grid.colModel.getCellEditor(rollQuantityIndex, row);
											rollEditor.setValue(rollEditor.record.get('includeInRoll') ? (rollEditor.record.get('quantity') - rollEditor.record.get('tailQuantity')) : 0);
											grid.stopEditing();
										}
									}
								},
								'headerclick': function(grid, col, e) {
									if (col === grid.getColumnModel().findColumnIndex('includeInRoll')) {
										grid.toggleAll();
									}
								}
							},
							columnsConfig: [
								{header: 'Holding Company', width: 200, dataIndex: 'holdingAccount.issuingCompany.name', hidden: true},
								{header: 'Client Account', width: 400, dataIndex: 'clientAccount.label'},
								{header: 'Holding Account', width: 400, dataIndex: 'holdingAccount.label'},
								{header: 'Executing Broker', width: 300, dataIndex: 'executingBrokerCompany.name', hidden: true},
								{
									header: 'REDI Alias', width: 100, dataIndex: 'holdingAccount.id', hidden: true,
									renderer: function(value, metaData, r) {
										return Clifton.system.GetCustomColumnValue(value, 'InvestmentAccount', 'Holding Account Fields', 'Goldman Sachs REDI Alias', false);
									}
								},
								{header: 'Long Position', width: 100, dataIndex: 'longPosition', type: 'boolean', hidden: true},
								{header: 'Quantity (Previous Day)', width: 100, dataIndex: 'previousQuantity', type: 'float', negativeInRed: true, summaryType: 'sum', hidden: true},
								{header: 'Quantity (Original)', width: 100, dataIndex: 'actualQuantity', type: 'float', negativeInRed: true, summaryType: 'sum', hidden: true},
								{header: 'Quantity (Pending)', width: 100, dataIndex: 'pendingQuantity', type: 'float', negativeInRed: true, summaryType: 'sum', hidden: true},

								{
									header: 'Quantity', width: 100, dataIndex: 'quantity', type: 'float', negativeInRed: true, summaryType: 'sum',
									tooltip: 'The position quantity of the rolled from security.',
									renderer: function(value, metaData, r) {
										return TCG.renderAdjustedAmount(value, (r.data['actualQuantity'] !== value), r.data['actualQuantity'], '0,000', 'Pending');
									}
								},
								// Trading Fields
								{
									header: 'Tail Quantity', width: 95, dataIndex: 'tailQuantity', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 0}, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid;',
									tooltip: 'The quantity of the original quantity to close and not roll.',
									renderer: function(value, metaData, r) {

										if (TCG.isTrue(r.data['longPosition'])) {
											metaData.attr = 'style="BACKGROUND-COLOR: #fff0f0;"';
										}
										else {
											metaData.attr = 'style="BACKGROUND-COLOR: #f0f0ff;"';
										}
										let qty = r.data['quantity'];
										if (qty < 0) {
											qty = qty * -1;
										}

										if (TCG.isNotBlank(value)) {
											if (qty < value) {
												metaData.css = 'ruleViolation';
												metaData.attr = 'qtip="Value entered exceeds quantity held.  Please enter a value no greater than ' + Ext.util.Format.number(r.data['quantity'], '0,000') + ' contracts."';
											}
											else if (TCG.isTrue(r.data['longPosition'])) {
												metaData.css = 'sell-dark';
												metaData.attr = '';
											}
											else {
												metaData.css = 'buy-dark';
												metaData.attr = '';
											}
										}
										return TCG.numberFormat(value, '0,000');
									},
									summaryRenderer: function(value) {
										return TCG.numberFormat(value, '0,000');
									}
								},
								{
									header: 'Roll Quantity', width: 95, dataIndex: 'rollQuantity', type: 'int', useNull: true, editor: {xtype: 'spinnerfield', allowBlank: true, minValue: 0}, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
									tooltip: 'The quantity of the original quantity to roll to the new security.',
									renderer: function(value, metaData, r) {
										let qty = r.data['quantity'];
										if (qty < 0) {
											qty = qty * -1;
										}
										const tail = r.data['tailQuantity'];
										if (TCG.isNumber(tail)) {
											qty = qty - tail;
										}

										if (qty < value) {
											metaData.css = 'ruleViolation';
											metaData.attr = 'qtip="Value entered exceeds quantity held (less tail).  Please enter a value no greater than ' + Ext.util.Format.number(qty, '0,000') + ' contracts."';
										}
										return TCG.renderAdjustedAmount(value, (qty !== value), qty, '0,000');
									}

								},
								{
									width: 30, dataIndex: 'includeInRoll', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false,
									menuDisabled: true,
									header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', '-on'),
									renderer: function(v, p, record) {
										p.css += ' x-grid3-check-col-td';
										if (TCG.isBlank(v)) {
											record.data['includeInRoll'] = true;
											v = true;
										}
										return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
									}
								}
							],
							plugins: {ptype: 'gridsummary'}
						}
					]
				}]
			},


			{
				title: 'All Rolls',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Rolls',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Roll From', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Roll To', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 14, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							// Go Directly to Roll Group Window to prevent 2 DB Retrievals (One for Group, and then again for Roll Specific Retrieval)
							detailPageClass: 'Clifton.trade.roll.RollGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Trade',
								readUncommittedRequested: true
							};
						}
					},

					{
						title: 'Tails',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Tail Trade',
								readUncommittedRequested: true
							};
						}
					}]
			},


			{
				title: 'Draft Rolls',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Rolls',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Roll From', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Roll To', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 14, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							// Go Directly to Roll Group Window to prevent 2 DB Retrievals (One for Group, and then again for Roll Specific Retrieval)
							detailPageClass: 'Clifton.trade.roll.RollGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Trade',
								workflowStatusNameEquals: 'Draft',
								readUncommittedRequested: true
							};
						}
					},

					{
						title: 'Tails',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Tail Trade',
								workflowStatusNameEquals: 'Draft',
								readUncommittedRequested: true
							};
						}
					}]
			},


			{
				title: 'Pending Rolls',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Rolls',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Roll From', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Roll To', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 14, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							// Go Directly to Roll Group Window to prevent 2 DB Retrievals (One for Group, and then again for Roll Specific Retrieval)
							detailPageClass: 'Clifton.trade.roll.RollGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Trade',
								workflowStatusNameEquals: 'Pending',
								readUncommittedRequested: true
							};
						}
					},

					{
						title: 'Tails',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Tail Trade',
								workflowStatusNameEquals: 'Pending',
								readUncommittedRequested: true
							};
						}
					}]
			},


			{
				title: 'Approved Rolls',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Rolls',
						name: 'tradeGroupListFind',
						rowSelectionModel: 'checkbox',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Roll From', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Roll To', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 14, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							// Go Directly to Roll Group Window to prevent 2 DB Retrievals (One for Group, and then again for Roll Specific Retrieval)
							detailPageClass: 'Clifton.trade.roll.RollGroupWindow',
							drillDownOnly: true,
							addEditButtons: function(t, gridPanel) {
								t.add({
									text: 'Group and Execute',
									tooltip: 'Group trades from selected roll into as few orders as possible (same security, direction) and submit them for execution.',
									iconCls: 'run',
									handler: function() {
										const sm = gridPanel.grid.getSelectionModel();
										gridPanel.executeTradeGroups(gridPanel, sm, 'DEFAULT');
									}
								}, '-');
								t.add({
									text: 'Execute Separately',
									tooltip: 'Create a separate order for each trade in selected roll(s) and submit them for execution.',
									iconCls: 'run',
									handler: function() {
										const sm = gridPanel.grid.getSelectionModel();
										gridPanel.executeTradeGroups(gridPanel, sm, 'EXECUTE_SEPARATELY');
									}
								}, '-');
							}
						},
						executeTradeGroups: function(gp, sm, groupingType) {
							if (sm.getCount() === 0) {
								TCG.showError('Please select at least one roll group to be executed.', 'No Roll(s) Selected');
							}
							else {
								const tradeGroups = sm.getSelections();
								const tradeGroupIds = [];
								for (let i = 0; i < tradeGroups.length; i++) {
									tradeGroupIds[i] = tradeGroups[i].id;
								}
								const loader = new TCG.data.JsonLoader({
									waitMsg: 'Executing Trades...',
									waitTarget: gp,
									params: {
										'tradeGroupIds': tradeGroupIds,
										'groupingType': groupingType
									},
									onFailure: function() {
										// If it failed, reload to current status
										gp.reload();
									},
									onLoad: function(record, conf) {
										gp.reload();
									}
								});
								loader.load('tradeOrderTradeGroupListExecute.json');
							}
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Trade',
								workflowStatusNameEquals: 'Approved',
								readUncommittedRequested: true
							};
						}
					},

					{
						title: 'Tails',
						name: 'tradeGroupListFind',
						rowSelectionModel: 'checkbox',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true,
							addEditButtons: function(t, gridPanel) {
								t.add({
									text: 'Group and Execute',
									tooltip: 'Group trades from selected tail(s) into as few orders as possible (same security, direction) and submit them for execution.',
									iconCls: 'run',
									handler: function() {
										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select at least one roll group to be executed.', 'No Roll(s) Selected');
										}
										else {
											const tradeGroups = sm.getSelections();
											const tradeGroupIdList = {};
											for (let i = 0; i < tradeGroups.length; i++) {
												tradeGroupIdList['tradeGroupIdList[' + i + ']'] = tradeGroups[i].id;
											}
											const loader = new TCG.data.JsonLoader({
												waitMsg: 'Executing Transition',
												waitTarget: gridPanel,
												params: tradeGroupIdList,
												onLoad: function(record, conf) {
													gridPanel.reload();
												}
											});
											loader.load('tradeOrderTradeGroupListExecute.json?groupingType=DEFAULT');
										}
									}
								}, '-');
								t.add({
									text: 'Execute Separately',
									tooltip: 'Create a separate order for each trade in selected tail(s) and submit them for execution.',
									iconCls: 'run',
									handler: function() {
										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select at least one tail group to be executed.', 'No Tail(s) Selected');
										}
										else {
											const tradeGroups = sm.getSelections();
											const tradeGroupIdList = {};
											for (let i = 0; i < tradeGroups.length; i++) {
												tradeGroupIdList['tradeGroupIdList[' + i + ']'] = tradeGroups[i].id;
											}
											const loader = new TCG.data.JsonLoader({
												waitMsg: 'Executing Transition',
												waitTarget: gridPanel,
												params: tradeGroupIdList,
												onLoad: function(record, conf) {
													gridPanel.reload();
												}
											});
											loader.load('tradeOrderTradeGroupListExecute.json?groupingType=EXECUTE_SEPARATELY');
										}
									}
								}, '-');
							}
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Tail Trade',
								workflowStatusNameEquals: 'Approved',
								readUncommittedRequested: true
							};
						}
					}]
			},


			{
				title: 'Active Rolls',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Rolls',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Roll From', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Roll To', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 14, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							// Go Directly to Roll Group Window to prevent 2 DB Retrievals (One for Group, and then again for Roll Specific Retrieval)
							detailPageClass: 'Clifton.trade.roll.RollGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Trade',
								workflowStatusNameEquals: 'Active',
								readUncommittedRequested: true
							};
						}
					},

					{
						title: 'Tails',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Tail Trade',
								workflowStatusNameEquals: 'Active',
								readUncommittedRequested: true
							};
						}
					}]
			},


			{
				title: 'Booked Rolls',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Rolls',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Roll From', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Roll To', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 14, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							// Go Directly to Roll Group Window to prevent 2 DB Retrievals (One for Group, and then again for Roll Specific Retrieval)
							detailPageClass: 'Clifton.trade.roll.RollGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Trade',
								workflowStateName: 'Booked',
								readUncommittedRequested: true
							};
						}
					},

					{
						title: 'Tails',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Tail Trade',
								workflowStateName: 'Booked',
								readUncommittedRequested: true
							};
						}
					}]
			},


			{
				title: 'Cancelled Rolls',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				items: [
					{
						title: 'Rolls',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Roll From', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Roll To', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 14, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							// Go Directly to Roll Group Window to prevent 2 DB Retrievals (One for Group, and then again for Roll Specific Retrieval)
							detailPageClass: 'Clifton.trade.roll.RollGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Trade',
								workflowStateName: 'Cancelled',
								readUncommittedRequested: true
							};
						}
					},

					{
						title: 'Tails',
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						border: 1,
						flex: 1,
						columns: [
							{header: 'Workflow Status', width: 14, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 13, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
							{header: 'Note', width: 100, dataIndex: 'note', filter: false}
						],
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
							}
							return {
								tradeGroupTypeName: 'Roll Tail Trade',
								workflowStateName: 'Cancelled'
							};
						}
					}]
			},


			{
				title: 'Roll Types',
				items: [{
					instructions: 'A roll type defines grouping for roll dates and which security group contains the current securities for this type. Each type can define a different roll schedules for similar securities and are generally manually maintained based on user input dates.',
					xtype: 'gridpanel',
					name: 'tradeRollTypeList',

					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Name', width: 50, dataIndex: 'name'},
						{header: 'Current Securities Group', width: 50, dataIndex: 'currentSecuritiesGroup.name'},
						{header: 'Investment Type', width: 50, dataIndex: 'investmentType.name'},
						{header: 'Description', width: 100, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.trade.roll.setup.RollTypeWindow',
						drillDownOnly: true // Could potentially open this up, but for now drill down only/read only
					}
				}]
			},


			{
				title: 'Roll Dates',
				items: [{
					instructions: 'Rolls Dates define time period in the system when a security will be rolled.  The start/end dates for the security roll are required, and a default new security is optional.  When populated, if the roll from security is in the type\'s selected current securities group, it will be automatically updated to the new security on the start date.',
					xtype: 'gridpanel',
					name: 'tradeRollDateListFind',
					importTableName: 'TradeRollDate',
					importComponentName: 'Clifton.trade.roll.setup.RollDateUploadWindow',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Roll Type', width: 50, dataIndex: 'rollType.name', filter: {searchFieldName: 'rollTypeName'}},
						{header: 'Instrument', width: 50, dataIndex: 'rollFromSecurity.instrument.labelShort', filter: {searchFieldName: 'instrumentLabel'}},
						{header: 'Roll From Security', width: 50, dataIndex: 'rollFromSecurity.label', filter: {searchFieldName: 'rollFromSecurityLabel'}},
						{header: 'Default New Security', width: 50, dataIndex: 'defaultNewSecurity.label', filter: {searchFieldName: 'defaultNewSecurityLabel'}},
						{header: 'Start Date', width: 30, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'End Date', width: 30, dataIndex: 'endDate'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Active On Date', name: 'activeOnDate', xtype: 'toolbar-datefield'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -30)});
						}

						const params = {};
						const t = this.getTopToolbar();
						const activeOnDate = TCG.getChildByName(t, 'activeOnDate');

						if (TCG.isNotBlank(activeOnDate.getValue())) {
							const dateValue = (activeOnDate.getValue()).format('m/d/Y');
							return {
								activeOnDate: dateValue
							};
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.trade.roll.setup.RollDateWindow',
						getDefaultData: function(gridPanel) { // defaults roll type for the detail page
							return {
								rollType: gridPanel.getWindow().getDefaultRollType()
							};
						}
					}
				}]
			}]
	}]
});


