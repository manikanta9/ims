Clifton.trade.roll.setup.RollTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Roll Type',
	iconCls: 'arrow-right-green',
	width: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Roll Type',
				items: [{
					xtype: 'formpanel',
					instructions: 'A roll type defines grouping for roll dates and which security group contains the current securities for this type. Each type can define a different roll schedules for similar securities and are generally manually maintained based on user input dates.',
					url: 'tradeRollType.json',
					readOnly: true, // Could potentially open this up, but for now drill down only/read only
					labelWidth: 150,
					items: [

						{fieldLabel: 'Name', name: 'name'},
						{
							fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', xtype: 'combo', loadAll: true, url: 'investmentTypeList.json',
							tooltip: 'Optional investment type to limit security selection when entering roll dates for this type.'
						},
						{
							fieldLabel: 'Current Security Group', name: 'currentSecuritiesGroup.name', hiddenName: 'currentSecuritiesGroup.id', xtype: 'combo', url: 'investmentSecurityGroupListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityGroupWindow',
							tooltip: 'Optional security group that holds the current securities for this roll type for "today".  When populated, as roll dates are entered the security group will automatically replace the roll from to the default new security where it exists.'
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Roll Dates',
				items: [{
					instructions: 'Rolls Dates define time period in the system when a security will be rolled.  The start/end dates for the security roll are required, and a default new security is optional.  When populated, if the roll from security is in the type\'s defined current security group, it will be automatically updated to the new security on the start date.',
					xtype: 'gridpanel',
					name: 'tradeRollDateListFind',
					importTableName: 'TradeRollDate',
					importComponentName: 'Clifton.trade.roll.setup.RollDateUploadWindow',
					getImportComponentDefaultData: function() {
						return {
							rollType: this.getWindow().getMainForm().formValues
						};
					},
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Instrument', width: 50, dataIndex: 'rollFromSecurity.instrument.labelShort', filter: {searchFieldName: 'instrumentLabel'}},
						{header: 'Roll From Security', width: 50, dataIndex: 'rollFromSecurity.label', filter: {searchFieldName: 'rollFromSecurityLabel'}},
						{header: 'Default New Security', width: 50, dataIndex: 'defaultNewSecurity.label', filter: {searchFieldName: 'defaultNewSecurityLabel'}},
						{header: 'Start Date', width: 30, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'End Date', width: 30, dataIndex: 'endDate'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Active On Date', name: 'activeOnDate', xtype: 'toolbar-datefield'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -30)});
						}

						const params = {};
						params.rollTypeId = this.getWindow().getMainFormId();
						const t = this.getTopToolbar();
						const activeOnDate = TCG.getChildByName(t, 'activeOnDate');

						if (activeOnDate.getValue() !== '') {
							params.activeOnDate = (activeOnDate.getValue()).format('m/d/Y');
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.trade.roll.setup.RollDateWindow',
						getDefaultData: function(gridPanel) { // defaults roll type for the detail page
							return {
								rollType: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Current Securities',
				instructions: 'Roll Dates can be used to generate the list of current securities for a given date.  For today, the current security group (selected on the type) holds the current securities, however you can also view the current security for the type for any date based on the roll dates populated for that type.',
				xtype: 'gridpanel',
				name: 'tradeRollTypeCurrentSecurityList',
				columns: [
					{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
					{header: 'Instrument', width: 50, dataIndex: 'instrument.labelShort'},
					{header: 'Security', width: 50, dataIndex: 'label'}
				],
				getTopToolbarFilters: function(toolbar) {
					return [
						{
							fieldLabel: 'Date', name: 'activeOnDate', xtype: 'toolbar-datefield', value: new Date().format('m/d/Y')
						}
					];
				},
				getLoadParams: function(firstLoad) {
					const params = {};
					params.rollTypeId = this.getWindow().getMainFormId();
					const t = this.getTopToolbar();
					const activeOnDate = TCG.getChildByName(t, 'activeOnDate');

					if (activeOnDate.getValue() !== '') {
						const dateValue = (activeOnDate.getValue()).format('m/d/Y');
						params.activeOnDate = dateValue;
					}
					else {
						TCG.showError('Please select a date in the toolbar to find current securities.');
						return false;
					}
					return params;
				},
				editor: {
					detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
					drillDownOnly: true
				}
			}
		]
	}]
});
