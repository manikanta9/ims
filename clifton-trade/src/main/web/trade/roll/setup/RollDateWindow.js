Clifton.trade.roll.setup.RollDateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Roll Dates',
	iconCls: 'arrow-right-green',
	items: [{
		xtype: 'formpanel',
		instructions: 'Rolls Dates define time period in the system when a security will be rolled.  The start/end dates for the security roll are required, and a default new security is optional.',
		url: 'tradeRollDate.json',
		labelWidth: 125,
		items: [
			{fieldLabel: 'Roll Type', name: 'rollType.name', hiddenName: 'rollType.id', xtype: 'combo', url: 'tradeRollTypeList.json', loadAll: true},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},

			{
				fieldLabel: 'Roll From Security', name: 'rollFromSecurity.label', hiddenName: 'rollFromSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				requiredFields: ['rollType.name', 'startDate'],
				doNotClearIfRequiredChanges: true,
				listeners: {
					beforeQuery: function(qEvent) {
						const f = qEvent.combo.getParentForm().getForm();
						qEvent.combo.store.baseParams = {investmentType: 'Futures', activeOnDate: f.findField('startDate').value};
					}
				}
			},
			{
				fieldLabel: 'Default New Security', name: 'defaultNewSecurity.label', hiddenName: 'defaultNewSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
				qtip: 'When populated, if the roll from security is in the "Current Securities" group, it will be automatically updated to the new security on the start date.',
				requiredFields: ['rollFromSecurity.id'],
				listeners: {
					beforeQuery: function(qEvent) {
						const f = qEvent.combo.getParentForm().getForm();
						const secId = f.findField('rollFromSecurity.id').getValue();
						const sec = TCG.data.getData('investmentSecurity.json?id=' + secId, qEvent.combo, 'investment.security.id.' + secId);
						qEvent.combo.store.baseParams = {investmentType: 'Futures', activeOnDate: f.findField('startDate').value, instrumentId: sec.instrument.id};
					}
				}
			}
		]
	}]
});
