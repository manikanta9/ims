Clifton.trade.roll.setup.RollDateUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'tradeRollDateUploadWindow',
	title: 'Trade Roll Date Import',
	iconCls: 'import',
	height: 600,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('TradeRollDate', false, this);
		}
	}, '-', {
		text: 'Sample File (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File (Contains All (up to 500 rows) Existing Data)',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('TradeRollDate', true, this);
		}
	}, '-', {
		text: 'Sample File (Simple Upload)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File for Simple Trade Roll Date Uploads',
		handler: function() {
			TCG.openFile('trade/roll/setup/SimpleTradeRollDateUploadFileSample.xls');
		}
	}, {
		xtype: 'tbfill'
	}, {
		text: 'Generic Import',
		iconCls: 'import',
		tooltip: 'Use generic upload window',
		handler: function() {
			TCG.createComponent('Clifton.system.upload.UploadWindow', {
				defaultData: {tableName: 'TradeRollDate'}
			});
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Trade Roll Date Uploads allow you to import roll dates from files directly into the system.  Use the options below to customize how to upload your data.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},

			{
				xtype: 'fieldset-checkbox',
				title: 'Simple Trade Roll Date Uploads',
				checkboxName: 'simple',
				instructions: 'Simple Trade Roll Date Uploads allow setting less columns in the upload file and the system will make certain assumptions in order to correctly lookup data based on less information. Selections below are optional and can be supplied instead of setting each row in the file.',
				items: [
					{fieldLabel: 'Roll Type', name: 'rollType.name', hiddenName: 'rollType.id', xtype: 'combo', url: 'tradeRollTypeList.json', loadAll: true},
					{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
				]
			},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'tradeRollDateUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		}

	}]
});
