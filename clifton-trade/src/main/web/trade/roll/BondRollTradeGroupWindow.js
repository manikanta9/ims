Clifton.trade.roll.BondRollTradeGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Bond Roll Trades',
	iconCls: 'bonds',
	width: 1300,
	height: 700,
	hideApplyButton: true,
	hideOKButton: true, // use true if only want Apply & Cancel Buttons
	hideCancelButton: true,
	saveTimeout: 180,

	items: [
		{xtype: 'bond-roll-trade-group-form-panel'}
	]
});

Clifton.trade.roll.BondRollTradeGroupFormPanel = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicFormPanel, {
	dtoClassForBinding: 'com.clifton.trade.group.dynamic.roll.TradeGroupDynamicBondRoll',
	requestedMaxDepth: 6,

	additionalListRequestedProperties: 'longPosition|maturingAmount|maturingSecurity.id|maturingSecurity.label|maturingSecurity.name|maturingSecurity.instrument.hierarchy.id|maturingCollateral|collateralOnly|' + //
		'rollBuy|rollAmount|rollAmountAdjustment|rollAmountTotal|rollCollateral|rollHoldingAccount.id|rollHoldingAccount.number|rollTradeDestination.id|rollTradeDestination.name|rollExecutingBroker.label|rollExecutingBroker.id|' + //
		'rollTradeList',

	applyCustomDefaultData: function(defaultData) {
		const tradeType = TCG.data.getData('tradeTypeByName.json?name=Bonds', this);
		let investmentSubType = TCG.data.getData('investmentTypeSubTypeListFind.json?nameEquals=Central Government Bonds&investmentTypeId=' + tradeType.investmentType.id, this);
		if (Array.isArray(investmentSubType) && investmentSubType.length > 0) {
			investmentSubType = investmentSubType[0];
		}
		const currentDate = new Date().format('Y-m-d 00:00:00');
		return Ext.apply({
			traderUser: TCG.getCurrentUser(),
			tradeDate: currentDate,
			startMaturityDate: currentDate,
			endMaturityDate: currentDate,
			tradeGroupType: TCG.data.getData('tradeGroupTypeByName.json?name=Bond Roll', this, 'trade.group.type.Bond Roll'),
			tradeDestination: TCG.data.getData('tradeDestinationByName.json?name=Bloomberg Ticket', this, 'trade.tradeDestination.Bloomberg Ticket'),
			tradeType: tradeType,
			investmentTypeSubType: investmentSubType
		}, defaultData);
	},

	getAllocationGridPanel: function() {
		return TCG.getChildByName(this, 'bond-roll-allocation-grid');
	},

	reloadChildGrid: function() {
		const tradeGrid = TCG.getChildByName(this, 'bond-roll-trade-grid');
		tradeGrid.reloadGrid.call(tradeGrid);
		this.doLayout();
		this.getAllocationGridPanel().store.loadData(this.getForm().formValues);
	},

	getLoadParams: function(win) {
		const allocationGrid = this.getAllocationGridPanel();
		if (allocationGrid) {
			allocationGrid.markModified();
		}
		return Clifton.trade.roll.BondRollTradeGroupFormPanel.superclass.getLoadParams.call(this, win);
	},

	getSubmitParams: function() {
		this.getAllocationGridPanel().markModified();
		return Clifton.trade.roll.BondRollTradeGroupFormPanel.superclass.getSubmitParams.call(this);
	},

	getConfirmBeforeSaveMsg: function() {
		let totalAllocationWeight = 0,
			securityWithNoWeight = void 0,
			reason = void 0;
		this.getAllocationGridPanel().store.each(function(record) {
			const allocationWeight = record.get('allocationPercent');
			if (TCG.isNotBlank(allocationWeight)) {
				totalAllocationWeight += allocationWeight;
			}
			else {
				securityWithNoWeight = record.get('security.label');
			}
		});
		if (TCG.isNotBlank(securityWithNoWeight)) {
			reason = 'The security [' + securityWithNoWeight + '] has no weight specified.';
		}
		else if (totalAllocationWeight < 0) {
			reason = 'The security allocations weight total is less than 0.';
		}
		else if (totalAllocationWeight > 100) {
			reason = 'The security allocations weight total is more than 100.';
		}
		else if (totalAllocationWeight < 100) {
			reason = 'The security allocations weight total is less than 100.';
		}

		return TCG.isBlank(reason) ? false : reason + ' Do you want to continue?';
	},

	tradeEntryItems: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .50,
					layout: 'form',
					labelWidth: 110,
					items: [
						{xtype: 'sectionheaderfield', header: 'Maturing Bond Info'},
						{
							fieldLabel: 'Bond Sub Type', xtype: 'panel', layout: 'hbox', anchor: '-20',
							defaults: {
								xtype: 'combo', loadAll: true
							},
							items: [
								{
									name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', disabled: true, url: 'investmentTypeSubTypeListByType.json', flex: 1,
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const f = combo.getParentForm().getForm();
											combo.store.baseParams = {investmentTypeId: TCG.getValue('tradeType.investmentType.id', f.formValues)};
										}
									}
								},
								{html: '&nbsp;&nbsp;Sub Type 2:', xtype: 'label', width: 80},
								{
									name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', url: 'investmentTypeSubType2ListByType.json', flex: 1,
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const f = TCG.getParentFormPanel(combo).getForm();
											combo.store.baseParams = {investmentTypeId: TCG.getValue('tradeType.investmentType.id', f.formValues)};
										}
									}
								}
							]
						},
						{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', hiddenName: 'clientAccountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json', allowBlank: true, detailPageClass: 'Clifton.investment.account.AccountGroupWindow', anchor: '-20'},
						{
							fieldLabel: 'Client Account', xtype: 'panel', layout: 'hbox', anchor: '-20',
							items: [
								{
									name: 'clientAccount.label', hiddenName: 'clientAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: true, detailPageClass: 'Clifton.investment.account.AccountWindow', flex: 1,
									listeners: {
										beforequery: function(queryEvent) {
											const combo = queryEvent.combo;
											const fp = combo.getParentForm();
											const collateralOnly = fp.getForm().findField('collateralOnly').getValue(),
												tradingBondPurpose = fp.getFormValue(collateralOnly === true ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name', true),
												relatedPurposes = [tradingBondPurpose],
												clientTradeTypePurpose = fp.getFormValue('clientAccountTradingPurposeName', true);
											if (TCG.isNotBlank(clientTradeTypePurpose)) {
												relatedPurposes.push(clientTradeTypePurpose);
											}
											combo.store.setBaseParam('relatedPurposesAllApplicable', relatedPurposes);
											combo.store.setBaseParam('relatedPurposeActiveOnDate', fp.getForm().findField('tradeDate').value);
										}
									}
								},
								{html: 'Trade Type:&nbsp;&nbsp;', style: 'text-align: right;', xtype: 'label', width: 80},
								{
									name: 'clientTradeType.label', hiddenName: 'clientTradeType.id', submitValue: false, displayField: 'label', requestedProps: 'holdingAccountPurpose.name', xtype: 'combo', loadAll: true, url: 'tradeTypeListFind.json', width: 130,
									qtip: 'Optional filter used to filter client accounts to those that support the specified trade type.',
									listeners: {
										change: function(combo, newValue, oldValue) {
											const form = TCG.getParentFormPanel(combo).getForm();
											if (TCG.isBlank(newValue)) {
												form.findField('clientAccountTradingPurposeName').setValue('');
											}
											form.findField('clientAccount.label').clearAndReset();
										},
										select: function(combo, record, index) {
											const form = TCG.getParentFormPanel(combo).getForm();
											form.findField('clientAccountTradingPurposeName').setValue(TCG.getValue('holdingAccountPurpose.name', record.json));
										}
									}
								},
								{name: 'clientAccountTradingPurposeName', xtype: 'hidden'}
							]
						},
						{
							fieldLabel: 'Maturity Beginning', xtype: 'panel', layout: 'hbox', anchor: '-20',
							defaults: {
								xtype: 'datefield', allowBlank: false, flex: 1
							},
							items: [
								{
									name: 'startMaturityDate', qtip: 'Beginning Maturity Date Range for looking up Maturing Bond Positions.',
									listeners: {
										select: function(datefield, date) {
											const form = TCG.getParentByClass(datefield, Clifton.trade.roll.BondRollTradeGroupFormPanel).getForm();
											form.findField('endMaturityDate').setValue(date);
										}
									}
								},
								{value: 'End:&nbsp;&nbsp;', style: 'text-align: right;', xtype: 'displayfield', width: 40},
								{name: 'endMaturityDate', qtip: 'Ending Maturity Date Range for looking up Maturing Bond Positions.'},
								{
									value: 'Collateral:&nbsp;&nbsp;', style: 'text-align: right;', xtype: 'displayfield', width: 80,
									qtip: 'Only load Maturing Collateral Positions if Yes, non-Collateral positions if No, and both if Both'
								},
								{
									xtype: 'radiogroup', readOnly: true, allowBlank: true, columns: [40, 40, 50], width: 130,
									items: [
										{boxLabel: 'Yes', name: 'collateralOnly', inputValue: true},
										{boxLabel: 'No', name: 'collateralOnly', inputValue: false},
										{boxLabel: 'Both', name: 'collateralOnly', inputValue: null, checked: true}
									],
									listeners: {
										change: function(radiogroup, checked) {
											const form = TCG.getParentFormPanel(radiogroup).getForm();
											form.findField('clientAccount.label').clearAndReset();
										}
									}
								}
							]
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .23,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false, anchor: '-20',
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									qEvent.combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: f.findField('tradeType.name').getValue()
									};
								}
							}
						},
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, anchor: '-20'},
						{
							fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
							listeners: {
								change: function(field, newValue, oldValue) {
									const fp = TCG.getParentFormPanel(field);
									const form = fp.getForm();
									const settlementDateField = form.findField('settlementDate');
									settlementDateField.setValue(newValue);
								}
							}
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .23,
					layout: 'form',
					labelWidth: 95,
					items: [
						{
							xtype: 'bond-roll-security-allocation-grid',
							height: 100,
							listeners: {
								afterlayout: TCG.grid.registerDynamicHeightResizing
							}

						}
					]
				}
			]
		}
	],

	tradeEntryGridItem: [
		{
			xtype: 'panel',
			layout: 'fit',
			items: [
				{
					xtype: 'bond-roll-detail-form-grid',
					tradeEntrySupported: true,
					autoHeight: false,
					height: 505
				}
			],
			listeners: {
				afterlayout: TCG.grid.registerDynamicHeightResizing
			}
		}
	],

	readOnlyItems: [
		{
			xtype: 'panel',
			layout: 'column',
			items: [
				{
					columnWidth: .50,
					layout: 'form',
					labelWidth: 110,
					items: [
						{xtype: 'sectionheaderfield', header: 'Maturing Bond Info'},
						{
							fieldLabel: 'Bond Sub Type', xtype: 'panel', layout: 'hbox', anchor: '-20',
							defaults: {
								xtype: 'displayfield', loadAll: true
							},
							items: [
								{name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', flex: 1},
								{html: '&nbsp;&nbsp;Sub Type 2:', xtype: 'label', width: 80},
								{name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', flex: 1}
							]
						},
						{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', detailIdField: 'clientAccountGroup.id', xtype: 'linkfield', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
						{fieldLabel: 'Client Account', name: 'clientAccount.name', detailIdField: 'clientAccount.id', xtype: 'linkfield', url: 'investmentAccountListFind.json', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{
							fieldLabel: 'Maturity Beginning', xtype: 'panel', layout: 'hbox',
							defaults: {
								xtype: 'datefield', allowBlank: false
							},
							items: [
								{name: 'startMaturityDate', readOnly: true, width: 100},
								{value: '&nbsp;&nbsp;End:', xtype: 'displayfield', width: 40},
								{name: 'endMaturityDate', readOnly: true, width: 100},
								{value: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Collateral:', xtype: 'displayfield', width: 85},
								{
									xtype: 'radiogroup', readOnly: true, disabled: true, allowBlank: true, columns: [40, 40, 50],
									items: [
										{boxLabel: 'Yes', name: 'collateralOnly', inputValue: true},
										{boxLabel: 'No', name: 'collateralOnly', inputValue: false},
										{boxLabel: 'Both', name: 'collateralOnly', inputValue: null}
									]
								}
							]
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .23,
					layout: 'form',
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{name: 'workflowState.id', xtype: 'hidden'},
						{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
						{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'displayfield'},
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
						{fieldLabel: 'Parent Bond Roll', name: 'parent.id', detailIdField: 'parent.id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.group.TradeGroupWindow', submitDetailField: false}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .23,
					layout: 'form',
					labelWidth: 95,
					items: [
						{
							xtype: 'bond-roll-security-allocation-grid',
							height: 100,
							readOnly: true,
							listeners: {
								afterlayout: TCG.grid.registerDynamicHeightResizing
							}

						}
					]
				}
			]
		}
	],

	readOnlyGridItem: [
		{
			xtype: 'panel',
			layout: 'fit',
			items: [
				{
					xtype: 'bond-roll-detail-form-grid',
					tradeEntrySupported: false,
					autoHeight: false,
					viewConfig: {markDirty: false},
					height: 460
				}
			],
			listeners: {
				afterlayout: TCG.grid.registerDynamicHeightResizing
			}
		}
	]
});
Ext.reg('bond-roll-trade-group-form-panel', Clifton.trade.roll.BondRollTradeGroupFormPanel);

Clifton.trade.roll.BondRollAllocationGrid = Ext.extend(TCG.grid.FormGridPanel, {
	name: 'bond-roll-allocation-grid',
	storeRoot: 'allocationList',
	dtoClass: 'com.clifton.trade.group.dynamic.roll.TradeGroupDynamicBondRollAllocation',
	collapsible: false,
	viewConfig: {markDirty: false},

	initComponent: function() {
		const cols = [];
		const editable = !TCG.isTrue(this.readOnly);
		Ext.each(this.columnsConfig, function(f) {
			f['editable'] = editable;
			cols.push(f);
		});
		this.columnsConfig = cols;
		TCG.grid.FormGridPanel.prototype.initComponent.call(this, arguments);
	},

	addToolbarButtons: function(toolbar, gridPanel) {
		// add toolbar description instead of title to conserve horizontal real estate
		toolbar.insert(0, '<b>New Bond Allocations</b>');
		if (!TCG.isTrue(this.readOnly)) {
			toolbar.insert(1, '-');
		}
	},

	columnsConfig: [
		{
			header: 'Bond', dataIndex: 'security.label', idDataIndex: 'security.id', width: 170, useNull: false,
			tooltip: 'New Bond that will be rolled to with a specific allocation.',
			editor: {
				xtype: 'combo', tooltipField: 'label', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: false, minListWidth: 230,
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					queryEvent.combo.resetStore();
					const formPanel = TCG.getParentFormPanel(queryEvent.combo.gridEditor.containerGrid);
					const form = formPanel.getForm();
					const store = queryEvent.combo.store;
					store.setBaseParam('activeOnDate', form.findField('tradeDate').value);
					store.setBaseParam('investmentType', 'Bonds');
					store.setBaseParam('investmentTypeSubTypeId', formPanel.getFormValue('investmentTypeSubType.id', true));
					store.setBaseParam('investmentTypeSubType2Id', formPanel.getFormValue('investmentTypeSubType2.id', true));
					store.setBaseParam('requestedProperties', 'id|name|symbol|label');
				}
			}
		},
		{
			header: 'Weight', dataIndex: 'allocationPercent', type: 'int', summaryType: 'sum', width: 70, useNull: false,
			tooltip: 'The percentage of total face amount to allocate to this security.',
			editor: {
				xtype: 'integerfield'
			},
			renderer: function(v, metaData, r) {
				return TCG.renderAmount(v, true, '0,000%');
			}
		}
	],

	plugins: {
		ptype: 'gridsummary'
	},

	alwaysSubmitFields: ['security.id', 'allocationPercent'],

	markModified: function(initOnly) {
		const result = [];
		this.store.each(function(record) {
			if (this.isRecordApplicableForDirtyChecking(record)) {
				const r = {
					'class': this.dtoClass
				};
				for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
					const f = this.alwaysSubmitFields[i];
					let val = TCG.getValue(f, record.json);
					if (TCG.isNotBlank(val)) {
						if (typeof val === 'number' && isNaN(val)) {
							val = '';
						}
						else if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
							val = TCG.renderDate(val);
						}
						r[f] = val;
					}
				}
				if (record.dirty) {
					// copy all modified fields
					for (const m in record.modified) {
						if (record.modified.hasOwnProperty(m)) {
							const field = record.fields.get(m);
							// skip 'text' field submission when there's 'value'
							if ((!field || !field.valueFieldName) && this.doNotSubmitFields.indexOf(m) === -1) {
								let v = record.modified[m];
								let skip = false;
								if (typeof v === 'object') {
									if (TCG.isNull(v)) {
										skip = true;
									}
									else if (Ext.isDate(v)) {
										v = v.dateFormat('m/d/Y');
									}
								}
								else if (typeof v === 'number' && isNaN(v)) {
									v = '';
								}
								if (!skip) {
									r[m] = v;
								}
							}
						}
					}
				}

				result.push(r);
			}
		}, this);
		const value = Ext.util.JSON.encode(result);
		if (this.submitField) {
			this.submitField.setValue(value); // changed value
		}
		if (initOnly) {
			this.submitField.originalValue = value; // initial value
		}
		else {
			const win = this.getWindow();
			if (typeof (win.setModified) === 'function' && TCG.isTrue(this.controlWindowModified)) {
				win.setModified(true);
			}
		}
	}
});
Ext.reg('bond-roll-security-allocation-grid', Clifton.trade.roll.BondRollAllocationGrid);

Clifton.trade.roll.BondRollDetailFormGrid = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicDetailGrid, {
	name: 'bond-roll-trade-grid',
	storeRoot: 'detailList',
	dtoClass: 'com.clifton.trade.group.dynamic.roll.TradeGroupDynamicBondRollDetail',
	groupField: 'clientAccount.label',
	instructions: 'Bond Roll screen allows for filtering Maturing Bond positions for the selected Client Account or Client Account Group in order to roll to new Bonds. Maturing Positions are populated by those maturing between the specified maturity dates.',
	processDetailChildList: true,
	detailChildListClassDictionary: {rollTradeList: 'com.clifton.trade.Trade'},

	alwaysSubmitFields: ['holdingAccount.id', 'clientAccount.id', 'rollAmountAdjustment', 'rollAmountTotal'],
	doNotSubmitFields: ['includeTrade', 'maturingSecurity.label'],

	addEntryToolbarButtons: function(toolBar, gridpanel) {
		const gp = gridpanel;
		toolBar.add({
			text: 'Load Positions',
			tooltip: 'Load Maturing Bond Positions for the information provided above.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.reloadGrid();
			}
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Submit Trades',
			tooltip: 'Create New Bond Trades based on quantities entered below.',
			iconCls: 'shopping-cart',
			scope: this,
			handler: function() {
				if (this.submitField.getValue() === '[]') {
					TCG.showError('No rows selected.  Please select at least one row from the list.');
					return;
				}
				gp.getWindow().saveWindow(false);
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Clear Trades',
			tooltip: 'Clear all Given Amounts entered below.',
			iconCls: 'clear',
			scope: this,
			handler: function() {
				gp.clearTrades();
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Info',
			tooltip: 'Toggle instructions for this page',
			iconCls: 'info',
			enableToggle: true,
			handler: function() {
				if (this.pressed) {
					gp.insert(0, {
						xtype: 'panel',
						frame: true,
						layout: 'fit',
						bodyStyle: 'padding: 3px 3px 3px 3px',
						html: gp.instructions
					});
				}
				else {
					gp.remove(0);
				}
				gp.doLayout();
			}
		}, '-');
	},

	handleAfterLoadTransientRecordValues: function(grid) {
		const tradeGrid = this;

		// select any rows that have modified roll face amounts
		grid.store.each(function(record) {
			if (TCG.isNotBlank(record.get('rollAmountAdjustment'))) {
				record.set('includeTrade', true);
			}
		});

		const formPanel = TCG.getParentFormPanel(tradeGrid);
		const allocationList = formPanel.getForm().formValues.allocationList;
		if (allocationList) {
			// Add transient security allocation columns to the trade grid.
			// - remove current transient columns if any exist
			// - add grid column for face amount per allocation
			// - reload the store value extractors
			if (TCG.isNotNull(tradeGrid.transientColumns) && Array.isArray(tradeGrid.transientColumns)) {
				let removeIndex = 0;
				const length = tradeGrid.transientColumns.length;
				for (; removeIndex < length; removeIndex++) {
					tradeGrid.removeColumn(tradeGrid.transientColumns[removeIndex]['dataIndex']);
					const submitFieldIndex = tradeGrid.alwaysSubmitFields.indexOf(tradeGrid.transientColumns[removeIndex]['idDataIndex']);
					if (submitFieldIndex > -1) {
						tradeGrid.alwaysSubmitFields.splice(submitFieldIndex, 1);
					}
				}
			}
			tradeGrid['transientColumns'] = [];
			const rendererFunction = function(v, metadata, record) {
				if (TCG.isBlank(v)) {
					v = TCG.getValue(this.dataIndex, record.json);
					record.data[this.dataIndex] = v;
					record.data[this.idDataIndex] = TCG.getValue(this.idDataIndex, record.json);
				}
				return TCG.renderAmount(v, false, '0,000.00');
			};
			for (let i = 0; i < allocationList.length; i++) {
				const allocation = allocationList[i];
				if (Object.keys(allocation).length > 0) {
					const dataIndex = 'rollTradeList[' + i + '].originalFace',
						idDataIndex = 'rollTradeList[' + i + '].id';
					const column = {
						header: allocation.security.symbol + ' Face', name: dataIndex, dataIndex: dataIndex, idDataIndex: idDataIndex,
						tradeIndex: i, submitDetailField: false, align: 'right',
						sortable: false, type: 'float', summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
						tooltip: 'Roll ' + TCG.renderAmount(allocation.allocationPercent, false, '0,000.00%') + ' new face to ' + allocation.security.label,
						renderer: rendererFunction
					};
					tradeGrid.addColumn(column);
					tradeGrid.transientColumns[i] = {dataIndex: dataIndex, idDataIndex: idDataIndex};
					tradeGrid.alwaysSubmitFields.push(idDataIndex);
				}
			}
			delete tradeGrid.store.reader.ef;
			tradeGrid.store.reader.buildExtractors();
		}
	},

	clearTrades: function() {
		const gp = this;
		gp.getStore().each(function(record) {
			record.data['rollAmountAdjustment'] = '';
			record.data['rollAmountTotal'] = 0;
		}, this);
		gp.markModified();
		gp.getView().refresh();
	},

	handleCellDoubleClick: function(grid, rowIndex, cellIndex, event) {
		const row = grid.store.data.items[rowIndex];
		const column = grid.getColumnModel().getColumnById(cellIndex);
		const columnName = column.dataIndex;
		if (columnName && (columnName === 'maturingSecurity.label')) {
			const securityId = TCG.getValue('maturingSecurity.id', row.json);
			if (TCG.isNotBlank(securityId)) {
				TCG.createComponent('Clifton.investment.instrument.SecurityWindow', {
					id: TCG.getComponentId('Clifton.investment.instrument.SecurityWindow', securityId),
					params: {id: securityId},
					openerCt: grid
				});
			}
		}
		else if (cellIndex > 0 && cellIndex < 5) {
			const filters = [];
			filters.push({
				field: 'clientInvestmentAccountId',
				comparison: 'EQUALS',
				value: TCG.getValue('clientAccount.id', row.json)
			});
			filters.push({
				field: 'holdingInvestmentAccountId',
				comparison: 'EQUALS',
				value: TCG.getValue('holdingAccount.id', row.json)
			});
			filters.push({
				field: 'investmentSecurityId',
				comparison: 'EQUALS',
				value: TCG.getValue('maturingSecurity.id', row.json)
			});
			const nextDay = new Date(TCG.getParentFormPanel(grid).getForm().findField('tradeDate').getValue()).add(Date.DAY, 1).format('m/d/Y');
			filters.push({
				field: 'transactionDate',
				comparison: 'LESS_THAN',
				value: nextDay
			});
			TCG.createComponent('Clifton.accounting.gl.TransactionListWindow', {
				defaultData: filters,
				openerCt: grid
			});
		}
		else if (this.tradeEntrySupported === false) {
			let tid = '';
			if (columnName && columnName.includes('rollTrade')) {
				tid = TCG.getValue('rollTradeList[' + column.tradeIndex + '].id', row.json);
			}
			if (TCG.isNotBlank(tid)) {
				TCG.createComponent('Clifton.trade.TradeWindow', {
					id: TCG.getComponentId('Clifton.trade.TradeWindow', tid),
					params: {id: tid},
					openerCt: grid
				});
			}
		}
	},

	handleAfterEditEventFromExt: function(event) {
		if (event.originalValue !== event.value) {
			event.record.set('includeTrade', true);
			event.grid.updateCalculatedValues(event.record, event.field, event.originalValue, event.value);
		}
	},

	// Columns that have tradeEntry = true/false will be included based on if this is tradeEntry or tradeReview - anything without that property will always be included
	customColumns: [
		// Maturing
		{header: 'Maturing Security', dataIndex: 'maturingSecurity.label', idDataIndex: 'maturingSecurity.id', width: 170, submitValue: false},
		{
			header: 'Maturing Face', dataIndex: 'maturingAmount', sortable: false, type: 'float', width: 110, submitValue: false, summaryType: 'sum',
			tooltip: 'Amount maturing within the Maturity Range specified.',
			renderer: function(v, metaData, r) {
				return TCG.renderAmount(v, true, '0,000.00');
			}
		},
		{header: 'Maturing Collateral', dataIndex: 'maturingCollateral', width: 110, align: 'center', type: 'boolean', sortable: false, filter: false, menuDisabled: true},
		// entry columns for Bond with later Maturity Date
		{
			header: 'Suggested Face', dataIndex: 'rollAmount', sortable: false, type: 'float', width: 110, allowNegative: false, useNull: true, tradeEntry: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Suggested amount to roll to the new Bond.',
			renderer: function(v, metadata, record) {
				return TCG.renderAmount(v, false, '0,000.00');
			}
		},
		{
			header: 'Face Adjustment', dataIndex: 'rollAmountAdjustment', sortable: false, type: 'float', width: 110, allowNegative: true, useNull: true, tradeEntry: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			editor: {
				xtype: 'currencyfield'
			},
			tooltip: 'Amount to adjust the suggested new face to roll to the new Bond.',
			renderer: function(v, metadata, record) {
				return TCG.renderAmount(v, false, '0,000.00');
			}
		},
		{
			header: 'Total New Face', dataIndex: 'rollAmountTotal', sortable: false, type: 'float', width: 110, allowNegative: false, useNull: true, tradeEntry: true, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			editor: {
				xtype: 'currencyfield'
			},
			tooltip: 'Amount to roll to the new Bond. Always enter a positive value.',
			renderer: function(v, metadata, record) {
				const rollAmount = record.data['rollAmount'];
				if (TCG.isBlank(v)) {
					v = rollAmount;
					record.data['rollAmountTotal'] = v;
				}
				return TCG.renderAdjustedAmount(v, v !== rollAmount, rollAmount, '0,000.00');
			}
		},
		{
			header: 'New Holding Account', width: 125, dataIndex: 'rollHoldingAccount.number', idDataIndex: 'rollHoldingAccount.id', css: 'BACKGROUND-COLOR: #f0f0ff;',
			tradeEntry: true, tooltip: 'Holding Account to trade new Bond under.', hidden: true,
			editor: {
				xtype: 'combo', displayField: 'number', url: 'investmentAccountListFind.json?ourAccount=false', tooltipField: 'label',
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					this.resetStore();
					const editor = queryEvent.combo.gridEditor;
					const record = editor.record;
					const grid = editor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					queryEvent.combo.store.baseParams = {
						mainAccountId: TCG.getValue('clientAccount.id', record.json),
						mainPurpose: fp.getFormValue(record.get('rollCollateral') ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
						mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value
					};
				}
			}
		},
		{
			header: 'Trade Destination', dataIndex: 'rollTradeDestination.name', idDataIndex: 'rollTradeDestination.id', width: 120, tradeEntry: true, css: 'BACKGROUND-COLOR: #f0f0ff;',
			tooltip: 'Trade Destination to use for trading new Bond.', hidden: true,
			editor: {
				xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false,
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					queryEvent.combo.resetStore();
					const grid = queryEvent.combo.gridEditor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					queryEvent.combo.store.baseParams = {
						activeOnDate: fp.getForm().findField('tradeDate').value,
						tradeTypeNameEquals: fp.getFormValue('tradeType.name')
					};
				}
			}
		},
		{
			header: 'Executing Broker', dataIndex: 'rollExecutingBroker.label', idDataIndex: 'rollExecutingBroker.id', width: 120, tradeEntry: true, css: 'BACKGROUND-COLOR: #f0f0ff;',
			tooltip: 'Executing Broker to use for trading new Bond.', hidden: true,
			editor: {
				xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.business.company.CompanyWindow', url: 'tradeExecutingBrokerCompanyListFind.json', allowBlank: false,
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					queryEvent.combo.resetStore();
					const grid = queryEvent.combo.gridEditor.containerGrid;
					const record = queryEvent.combo.gridEditor.record;
					const fp = TCG.getParentFormPanel(grid);
					queryEvent.combo.store.baseParams = {
						tradeDestinationId: record.get('rollTradeDestination.id'),
						tradeTypeId: fp.getFormValue('tradeType.id'),
						activeOnDate: fp.getForm().findField('tradeDate').value
					};
				}
			}
		},
		{
			header: 'Collateral', dataIndex: 'rollCollateral', align: 'center', xtype: 'checkcolumn', width: 70, tradeEntry: true, sortable: false, filter: false, menuDisabled: true, css: 'BACKGROUND-COLOR: #f0f0ff;',
			renderer: function(v, p, record) {
				p.css += ' x-grid3-check-col-td';
				if (TCG.isBlank(v)) {
					v = false;
				}
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
			}
		},
		// review columns for Bond with later Maturity Date
		{
			header: 'Total New Face', dataIndex: 'rollAmountTotal', sortable: false, type: 'float', allowNegative: false, useNull: true, tradeEntry: false, summaryType: 'sum', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Amount to roll to the new Bond(s).',
			renderer: function(v, metadata, record) {
				return TCG.renderAmount(v, false, '0,000.00');
			}
		},
		{header: 'New Holding Account', tradeEntry: false, hidden: true, width: 100, dataIndex: 'rollHoldingAccount.number', css: 'BACKGROUND-COLOR: #f0f0ff;'},
		{header: 'Trade Destination', tradeEntry: false, hidden: true, width: 100, dataIndex: 'rollTradeDestination.name', css: 'BACKGROUND-COLOR: #f0f0ff;'},
		{header: 'Executing Broker', tradeEntry: false, hidden: true, width: 100, dataIndex: 'rollExecutingBroker.label', css: 'BACKGROUND-COLOR: #f0f0ff;'},
		{header: 'Collateral', dataIndex: 'rollCollateral', align: 'center', type: 'boolean', tradeEntry: false, width: 60, sortable: false, filter: false, menuDisabled: true, css: 'BACKGROUND-COLOR: #f0f0ff;'}
	],

	updateCalculatedValues: function(record, updatedField, oldValue, newValue) {
		if (updatedField === 'rollAmountAdjustment') {
			const newTotal = record.get('rollAmount') + (TCG.isBlank(newValue) ? 0 : newValue);
			record.set('rollAmountTotal', newTotal);

			const allocationList = TCG.getParentFormPanel(this).getForm().formValues.allocationList;
			if (allocationList) {
				for (let i = 0, length = allocationList.length; i < length; i++) {
					record.set('rollTradeList[' + i + '].originalFace', newTotal * allocationList[i].allocationPercent / 100);
				}
			}
		}
		else if (updatedField === 'rollAmountTotal') {
			record.set('rollAmountAdjustment', (TCG.isBlank(newValue) ? 0 : newValue) - record.get('rollAmount'));

			const allocationList = TCG.getParentFormPanel(this).getForm().formValues.allocationList;
			if (allocationList) {
				for (let i = 0, length = allocationList.length; i < length; i++) {
					record.set('rollTradeList[' + i + '].originalFace', newValue * allocationList[i].allocationPercent / 100);
				}
			}
		}
	},

	plugins: {
		ptype: 'gridsummary'
	}
});
Ext.reg('bond-roll-detail-form-grid', Clifton.trade.roll.BondRollDetailFormGrid);

Clifton.trade.roll.BondRollDetailTreePanel = Ext.extend(TCG.tree.AdvancedTreePanel, {});
Ext.reg('bond-roll-detail-tree-panel', Clifton.trade.roll.BondRollDetailTreePanel);
