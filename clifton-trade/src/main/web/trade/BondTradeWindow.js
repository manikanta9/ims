// common section used by single and multi-client entry screens
Clifton.trade.BondTradeForm = Ext.extend(Clifton.trade.BaseTradeForm, {
	labelWidth: 115,
	submitEmptyText: true,

	getDefaultData: function(win) {
		let dd = Ext.apply(win.defaultData || {}, {});
		if (win.defaultDataIsReal) {
			return dd;
		}
		const tradeTypeName = win.tradeType;
		const formPanel = this;
		return TCG.data.getDataPromiseUsingCaching('tradeDestinationByName.json?name=Manual', formPanel, 'trade.tradeDestination.Manual')
			.then(function(tradeDestination) {
				dd.tradeDestination = tradeDestination;
				return TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + tradeTypeName, formPanel, 'trade.type.' + tradeTypeName);
			})
			.then(function(tradeType) {
				const now = new Date().format('Y-m-d 00:00:00');
				dd = Ext.apply({
					tradeType: tradeType,
					traderUser: TCG.getCurrentUser(),
					tradeDate: now,
					settlementDate: now
				}, dd);
				return dd;
			});
	},

	afterRenderApplyAdditionalDefaultData: async function(formPanel, formValues) {
		const hierarchy = TCG.getValue('investmentSecurity.instrument.hierarchy', formValues);
		if (hierarchy) {
			// set display field per security selection from default data
			await formPanel.activateBondType.call(formPanel, hierarchy);
		}
	},

	activateBondType: function(hierarchy) {
		const fp = this;
		const f = fp.getForm();
		let factor = false;
		let coupon = false;
		let cpi = false;

		return Promise.resolve()
			.then(function() {
				if (hierarchy.indexRatioAdjusted) {
					coupon = true;
					cpi = true;
				}
				else {
					return TCG.data.getDataPromiseUsingCaching('investmentSecurityEventTypeListByHierarchy.json?hierarchyId=' + hierarchy.id, fp, 'investment.hierarchy.eventTypeList.' + hierarchy.id)
						.then(function(events) {
							TCG.each(events, function(e) {
								if (e.name === 'Cash Coupon Payment') {
									coupon = true;
								}
								else if (e.name === 'Factor Change') {
									factor = true;
								}
							});
						});
				}
			})
			.then(function() {
				fp.activateField(f, 'currentFactor', factor);
				fp.activateField(f, 'quantityIntended', factor);
				fp.activateField(f, 'inflationAdjustedFace', cpi);
				fp.activateField(f, 'notionalMultiplier', cpi);
				fp.activateField(f, 'currentCoupon', coupon);
				fp.activateField(f, 'accrualDate', coupon);
				fp.activateField(f, 'accrualDays', coupon);
				fp.activateField(f, 'accrualAmount1', coupon);
				fp.activateField(f, 'totalPayment', coupon);
				fp.setFieldQtip('accrualAmount1', TCG.isTrue(fp.getFormValue('tradeType.amountsSignAccountsForBuy')) ? 'Amount is expressed in positive or negative terms based on payment direction: Negative (pay) or Positive (receive)' : 'Amount is expressed in absolute terms and payment direction is determined based on trade direction: BUY (pay) or SELL (receive)');
				try {
					fp.doLayout();
				}
				catch (e) {
					// strange error due to removal of elements with allowBlank = false
				}
			});
	},

	activateField: function(form, field, show) {
		const f = form.findField(field);
		f.setVisible(show);
		if (f.readOnly === false) {
			f.allowBlank = !show;
		}
		if (!show) {
			f.setValue('');
			f.originalValue = f.getValue();
		}
	},

	updateCalculatedValues: async function(updatedField, newValue, oldValue) {
		const fp = this;
		const f = fp.getForm();

		const securityId = f.findField('investmentSecurity.id').value;
		let orig = f.findField('originalFace').getNumericValue();
		if (Number.isNaN(orig)) {
			orig = 0;
		}

		const notionalMultiplierField = f.findField('notionalMultiplier');
		if (Ext.isNumber(orig)) {
			if (f.findField('currentFactor').isVisible()) {
				await fp.updateQuantityIntended(orig, updatedField);
			}
			else if (notionalMultiplierField.isVisible()) {
				// accounts for rounding: JPY, etc.
				await fp.updateInflationAdjusted(securityId, orig);
			}
		}

		const paymentFieldUpdated = updatedField === 'quantityIntended' || updatedField === 'currentFactor' || updatedField === 'originalFace' || updatedField === 'notionalMultiplier';
		if ((TCG.isNull(updatedField) && !f.findField('accrualAmount1').value) || paymentFieldUpdated) {
			await fp.updateAccruedInterest();
		}

		if (updatedField === 'averageUnitPrice' || paymentFieldUpdated) {
			const price = f.findField('averageUnitPrice').getNumericValue();
			const pf = fp.getPurchaseFace();
			if (Ext.isNumber(price) && Ext.isNumber(pf)) {
				// accounts for rounding: JPY, etc.
				let qty = pf;
				let multiplier = 1;
				if (notionalMultiplierField.isVisible()) {
					qty = orig;
					multiplier = notionalMultiplierField.getNumericValue();
				}
				const value = await TCG.data.getDataValuePromise(`investmentSecurityNotional.json?securityId=${securityId}&price=${price}&quantity=${qty}&notionalMultiplier=${multiplier}`, this);
				fp.setFormValue('accountingNotional', value);
				fp.updateTotalPayment();
			}
		}
		else {
			fp.updateTotalPayment();
		}

		if (!f.findField('currentCoupon').value) {
			await fp.updateCurrentCoupon();
			await fp.updateNumberOfAccrualDays();
		}
	},

	updateQuantityIntended: async function(originalFace, updatedField) {
		const fp = this;
		const f = fp.getForm();
		let factor = f.findField('currentFactor').getNumericValue();
		const purchase = f.findField('quantityIntended').getNumericValue();

		if (!Ext.isNumber(factor) && Ext.isNumber(purchase)) {
			factor = purchase / originalFace;
			fp.setFormValue('currentFactor', factor, true);
		}

		if (Ext.isNumber(factor) && (!Ext.isNumber(purchase) || (updatedField === 'originalFace' || updatedField === 'currentFactor'))) {
			const amount = await TCG.data.getDataValuePromise(`investmentMultiplyAndRound.json?value1=${originalFace}&value2=${factor}&scale=2`, this);
			fp.setFormValue('quantityIntended', amount, true); // need to round up, not truncate
		}
	},

	updateInflationAdjusted: async function(securityId, originalFace) {
		const fp = this;
		const f = fp.getForm();
		const face = await TCG.data.getDataValuePromise(`investmentSecurityNotional.json?securityId=${securityId}&price=100&quantity=${originalFace}&notionalMultiplier=${f.findField('notionalMultiplier').getNumericValue()}`, this);
		fp.setFormValue('inflationAdjustedFace', face, true);
	},

	updateTotalPayment: function() {
		const fp = this;
		const f = fp.getForm();
		const principal = f.findField('accountingNotional').getNumericValue();
		const interest = f.findField('accrualAmount1').getNumericValue();
		fp.setFormValue('totalPayment', principal + interest, true);
	},

	updateCurrentCoupon: async function(securityId) {
		const fp = this;
		const f = fp.getForm();
		if (f.findField('currentCoupon').isVisible()) {
			const params = {
				securityId: securityId || f.findField('investmentSecurity.id').value,
				typeName: 'Cash Coupon Payment',
				accrualEndDate: f.findField('settlementDate').value
			};
			let coupon = 0;
			let date = '';
			let event = await TCG.data.getDataPromise('investmentSecurityEventForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue|declareDate', this, {params: params});
			if (event) {
				coupon = event.afterEventValue;
				date = event.declareDate;
			}
			else {
				// lookup previous (assumed) coupon if current one is not available
				event = await TCG.data.getDataPromise('investmentSecurityEventPreviousForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue|declareDate', this, {params: params});
				if (event) {
					coupon = event.afterEventValue;
					date = event.declareDate;
				}
			}
			fp.setFormValue('currentCoupon', coupon, true);
			fp.setFormValue('accrualDate', date, true);
		}
		else {
			fp.setFormValue('notionalMultiplier', 1, true);
		}
	},

	updateCurrentFactor: async function(securityId) {
		const f = this.getForm();
		const factorField = f.findField('currentFactor');
		if (factorField.isVisible()) {
			const params = {
				securityId: securityId,
				typeName: 'Factor Change',
				accrualEndDate: f.findField('settlementDate').value
			};
			// use previous because current factor may not be available
			let event = await TCG.data.getDataPromise('investmentSecurityEventPreviousForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=afterEventValue', this, {params: params});
			if (event) {
				factorField.setValue(event.afterEventValue);
			}
			else {
				// no previous factor found: try current
				event = await TCG.data.getDataPromise('investmentSecurityEventForAccrualEndDate.json?requestedPropertiesRoot=data&requestedProperties=beforeEventValue', this, {params: params});
				factorField.setValue(event ? event.beforeEventValue : 1);
			}
		}
	},

	getPurchaseFace: function(adjustForIndexRatio) {
		// quantityIntended if it's enabled or inflationAdjustedFace if it's enabled
		const f = this.getForm();
		let fieldName = 'originalFace';
		if (f.findField('quantityIntended').isVisible()) {
			fieldName = 'quantityIntended';
		}
		else if (f.findField('inflationAdjustedFace').isVisible() && (adjustForIndexRatio || this.getFormValue('investmentSecurity.instrument.hierarchy.notionalMultiplierForPriceNotUsed') !== true)) {
			fieldName = 'inflationAdjustedFace';
		}
		return f.findField(fieldName).getNumericValue();
	},

	updateNumberOfAccrualDays: async function() {
		const fp = this;
		const toDate = await TCG.data.getDataValuePromise('tradeAccrualUpToDate.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(fp.getForm().el.dom, true), this);
		const parsedToDate = TCG.parseDate(toDate);
		if (Ext.isDate(parsedToDate)) {
			const fromDate = TCG.parseDate(fp.getFormValue('accrualDate', true), 'm/d/Y');
			const days = Ext.isDate(fromDate) ? Math.round((parsedToDate.getTime() - fromDate.getTime()) / (24 * 60 * 60 * 1000)) : undefined;
			fp.setFormValue('accrualDays', days, true);
		}
	},

	updateAccruedInterest: async function() {
		const fp = this;
		const f = fp.getForm();
		const securityId = f.findField('investmentSecurity.id').value;
		let purchaseFace = fp.getPurchaseFace(true);
		if (f.findField('notionalMultiplier').isVisible()) {
			purchaseFace = f.findField('originalFace').getNumericValue();
		}
		const settlementDate = f.findField('settlementDate').value;
		if (settlementDate && TCG.isNotNull(securityId) && Ext.isNumber(purchaseFace) && f.findField('accrualAmount1').isVisible()) {
			const ai = await TCG.data.getDataValuePromise('tradeAccrualAmount.json?enableValidatingBinding=true&disableValidatingBindingValidation=true&' + Ext.lib.Ajax.serializeForm(f.el.dom, true), this);
			if (TCG.isNotNull(ai)) { // no errors
				fp.setFormValue('accrualAmount1', ai, true);
			}
		}
	},

	updateIndexRatio: async function(securityId, force) {
		const fp = this;
		const f = fp.getForm();
		if (securityId && (force || f.findField('notionalMultiplier').isVisible())) {
			const indexRatio = await TCG.data.getDataValuePromise(`marketDataIndexRatioForSecurity.json?securityId=${securityId}&date=${f.findField('settlementDate').getValue().format('m/d/Y')}`, this);
			fp.setFormValue('notionalMultiplier', indexRatio, true);
			// accounts for rounding: JPY, etc.
			const orig = f.findField('originalFace').getNumericValue();
			if (Ext.isNumber(orig)) {
				const value = await TCG.data.getDataValuePromise(`investmentSecurityNotional.json?securityId=${securityId}&price=100&quantity=${orig}&notionalMultiplier=${indexRatio}`, this);
				fp.setFormValue('inflationAdjustedFace', value, true);
			}
		}
		else {
			fp.setFormValue('notionalMultiplier', 1, true);
		}
	},

	updateExecutingBrokerCompany: async function(providedTradeDestinationId) {
		const f = this.getForm();
		const executingBroker = f.findField('executingBrokerCompany.label');
		if (executingBroker.isVisible()) {
			const tradeDestinationId = providedTradeDestinationId || f.findField('tradeDestination.name').getValue();
			const tradeDate = f.findField('tradeDate').getValue().format('m/d/Y');
			if (tradeDestinationId) {
				const rec = await TCG.data.getDataPromise('tradeDestinationMappingByCommand.json', this, {
					params: {
						tradeTypeId: f.findField('tradeType.id').getValue(),
						tradeDestinationId: tradeDestinationId,
						activeOnDate: tradeDate
					}
				});

				if (rec && rec.executingBrokerCompany) {
					executingBroker.setValue({value: rec.executingBrokerCompany.id, text: rec.executingBrokerCompany.label});
					f.formValues.executingBrokerCompany = rec.executingBrokerCompany; // needs this in order to find the record when tabbing out when the store isn't initialized
				}
				else {
					executingBroker.reset();
					executingBroker.clearValue();
				}
			}
		}
	},

	getExistingPositionsGrid: function() {
		const fgs = this.findByType('formgrid');
		for (let i = 0; i < fgs.length; i++) {
			if (fgs[i].storeRoot === 'positionList') {
				return fgs[i];
			}
		}
		return undefined;
	},


	commonEntryItems: [{
		xtype: 'panel',
		layout: 'column',
		items: [
			{
				columnWidth: .38,
				layout: 'form',
				labelWidth: 115,
				defaults: {xtype: 'textfield', width: 200},
				items: [
					{
						xtype: 'radiogroup',
						fieldLabel: 'Buy/Sell',
						columns: [70, 70],
						allowBlank: false,
						anchor: '-35',
						items: [
							{boxLabel: 'Buy', name: 'buy', inputValue: true},
							{boxLabel: 'Sell', name: 'buy', inputValue: false}
						],
						listeners: {
							change: function(radioGroup, checkedRadio) {
								const formPanel = TCG.getParentFormPanel(radioGroup);
								const params = {
									tradeTypeId: formPanel.getFormValue('tradeType.id'),
									buy: checkedRadio.getRawValue()
								};
								TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params})
									.then(function(openCloseType) {
										if (openCloseType && openCloseType.length === 1) {
											formPanel.getForm().findField('openCloseType.id').setValue(openCloseType[0].id);
										}
									});
							}
						}
					},
					{xtype: 'hidden', name: 'openCloseType.id'},
					{fieldLabel: 'OTC', name: 'investmentSecurity.instrument.hierarchy.otc', xtype: 'hidden', submitValue: false}, // Used for Holding Account Lookup
					{
						fieldLabel: 'Bond', name: 'investmentSecurity.label', allowBlank: false, displayField: 'label', hiddenName: 'investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?tradingDisallowed=false&investmentType=Bonds',
						getDetailPageClass: function(newInstance) {
							if (newInstance) {
								if (!this.getParentForm().getForm().findField('investmentSecurity.instrument.hierarchy.id').value) {
									TCG.showError('You must first select Bond Type for the new bond.', 'New Bond');
									return false;
								}
								return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
							}
							return this.detailPageClass;
						},
						getDefaultData: function(f) { // defaults client/counterparty/isda
							let fp = TCG.getParentFormPanel(this);
							fp = fp.getForm();
							const ha = TCG.getValue('holdingInvestmentAccount', fp.formValues);
							const td = fp.findField('tradeDate').getValue().format('Y-m-d 00:00:00');
							const investmentHierarchyId = fp.findField('investmentSecurity.instrument.hierarchy.id').value;
							const investmentGroupId = TCG.getValue('tradeType.investmentGroup.id', fp.formValues);
							const investmentTypeId = TCG.getValue('tradeType.investmentType.id', fp.formValues);
							return {
								businessCompany: (ha ? ha.issuingCompany : ''),
								businessContract: (ha ? ha.businessContract : ''),
								startDate: td,
								hierarchyId: investmentHierarchyId,
								investmentGroupId: investmentGroupId,
								investmentTypeId: investmentTypeId
							};
						},
						listeners: {
							beforeQuery: function(qEvent) {
								const f = qEvent.combo.getParentForm().getForm();
								const params = {activeOnDate: f.findField('settlementDate').value};
								const hierarchyId = f.findField('investmentSecurity.instrument.hierarchy.id').getValue();
								if (hierarchyId) {
									params.hierarchyId = hierarchyId;
								}
								const tradeDate = f.findField('tradeDate').value;
								const today = new Date().format('m/d/Y');
								if (tradeDate === today) {
									params.instrumentIsInactive = false;
								}
								qEvent.combo.store.baseParams = params;
							},
							select: function(combo, record, index) {
								const curr = TCG.getValue('settlementCurrency', record.json) || TCG.getValue('instrument.tradingCurrency', record.json);
								const fp = combo.getParentForm();
								const f = fp.getForm();
								f.formValues.investmentSecurity = record.json; // makes bond type selection stay
								f.findField('payingSecurity.id').setValue(curr.id);

								const tradeDate = f.findField('tradeDate');
								const today = new Date().format('m/d/Y');
								const td = tradeDate.value;
								Promise.resolve()
									.then(function() {
										if (td === today) { // change default based on exchange hours, etc.
											return Clifton.investment.instrument.getTradeDatePromise(record.id, null, combo)
												.then(function(td) {
													tradeDate.setValue(td);
													return td;
												});
										}
										else {
											return td;
										}
									})
									.then(function(td) {
										return Clifton.investment.instrument.getSettlementDatePromise(record.id, td, null, null, combo);
									})
									.then(function(settlementDate) {
										f.findField('settlementDate').setValue(settlementDate);

										const otc = record.json.instrument.hierarchy.otc;
										f.findField('investmentSecurity.instrument.hierarchy.otc').setValue(otc);

										const type = f.findField('investmentSecurity.instrument.hierarchy.name');
										if (!type.getValue()) {
											const h = record.json.instrument.hierarchy;
											type.setValue({text: h.name, value: h.id});
											return fp.activateBondType.call(fp, h);
										}
									})
									.then(function(settlementDate) {
										Clifton.trade.updateExistingPositions(fp, record.id);
										return Promise.all([
											fp.updateCurrentCoupon.call(fp, record.id).then(function() {
												fp.updateNumberOfAccrualDays.call(fp);
											}),
											fp.updateCurrentFactor.call(fp, record.id),
											fp.updateIndexRatio.call(fp, record.id)
										]);
									})
									.then(function() {
										fp.updateCalculatedValues.call(fp, 'originalFace');
									});
							}
						}
					},
					{
						fieldLabel: 'Bond Type', name: 'investmentSecurity.instrument.hierarchy.name', allowBlank: false, hiddenName: 'investmentSecurity.instrument.hierarchy.id', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json?investmentTypeName=Bonds&assignmentAllowed=true',
						detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', disableAddNewItem: true,
						limitRequestedProperties: false,
						listeners: {
							beforeselect: function(combo, record) {
								const fp = combo.getParentForm();
								const f = fp.getForm();
								const s = f.findField('investmentSecurity.label');
								s.store.removeAll();
								s.lastQuery = null;
								s.store.baseParams = {hierarchyId: record.id};
								s.clearValue();
								fp.activateBondType(record.json);
							}
						}
					},

					{name: 'payingSecurity.id', xtype: 'hidden'},
					{
						fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo',
						displayField: 'label',
						queryParam: 'executingBrokerCompanyName',
						detailPageClass: 'Clifton.business.company.CompanyWindow',
						url: 'tradeExecutingBrokerCompanyListFind.json',
						allowBlank: true,
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								combo.store.baseParams = {
									tradeDestinationId: f.findField('tradeDestination.id').getValue(),
									tradeTypeId: f.findField('tradeType.id').getValue(),
									activeOnDate: f.findField('tradeDate').value
								};
							}
						}
					},
					{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', detailPageClass: 'Clifton.security.user.UserWindow', allowBlank: false},
					{name: 'tradeType.id', xtype: 'hidden'},
					{
						fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
						listeners: {
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								combo.store.baseParams = {
									tradeTypeId: f.findField('tradeType.id').getValue(),
									activeOnDate: f.findField('tradeDate').value
								};
							},
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								// clear existing destinations
								const dst = fp.getForm().findField('executingBrokerCompany.label');
								dst.store.removeAll();
								dst.lastQuery = null;
								fp.updateExecutingBrokerCompany(record.id);
							}
						}
					},
					{boxLabel: 'Collateral trade', name: 'collateralTrade', xtype: 'checkbox'}
				]
			},
			{
				columnWidth: .33,
				layout: 'form',
				labelWidth: 100,
				defaults: {xtype: 'textfield', width: 110},
				items: [
					{
						fieldLabel: 'Original Face', name: 'originalFace', xtype: 'currencyfield', allowBlank: false,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'originalFace', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'pricefield', allowBlank: true,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'averageUnitPrice', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Factor', name: 'currentFactor', xtype: 'floatfield', allowBlank: false, hidden: true,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'currentFactor', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Purchase Face', name: 'quantityIntended', xtype: 'currencyfield', allowBlank: false, hidden: true,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'quantityIntended', newValue, oldValue);
							}
						}
					},
					{
						fieldLabel: 'Index Ratio', name: 'notionalMultiplier', xtype: 'floatfield', readOnly: true, hidden: true,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'notionalMultiplier', newValue, oldValue);
							}
						}
					},
					{fieldLabel: 'Inflation Adj Face', name: 'inflationAdjustedFace', xtype: 'currencyfield', readOnly: true, hidden: true},
					{fieldLabel: 'Current Coupon', name: 'currentCoupon', xtype: 'floatfield', readOnly: true, hidden: true},
					{fieldLabel: 'Accrual Start', name: 'accrualDate', xtype: 'displayfield', type: 'date', readOnly: true, hidden: true, qtip: 'Accrual starts the day after this date: End Of Day industry convention'},
					{fieldLabel: 'Accrual Days', name: 'accrualDays', xtype: 'displayfield', readOnly: true, hidden: true, qtip: 'Calendar days between Accrual Start and End Dates (using accrual date calculator). Does not take into account security Day Count convention.'}
				]
			},
			{
				columnWidth: .29,
				layout: 'form',
				labelWidth: 100,
				defaults: {xtype: 'textfield', anchor: '-20'},
				items: [
					{fieldLabel: 'Principal', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
					{
						fieldLabel: 'Accrued Interest', name: 'accrualAmount1', xtype: 'currencyfield', allowBlank: false, hidden: true,
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = TCG.getParentFormPanel(field);
								fp.updateCalculatedValues.call(fp, 'accrualAmount1', newValue, oldValue);
							}
						}
					},
					{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Settlement Total = Principal + Accrued Interest', hidden: true},
					{
						fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
						getParentFormPanel: function() {
							const parent = TCG.getParentFormPanel(this);
							return parent || this.ownerCt;
						},
						listeners: {
							change: function(field, newValue, oldValue) {
								field.updateFormValuesUponChange(field, newValue);
							},
							select: function(field, date) {
								field.updateFormValuesUponChange(field, date);
							}
						},
						updateFormValuesUponChange: function(field, tradeDate) {
							const fp = field.getParentFormPanel();
							const form = fp.getForm();

							const securityId = form.findField('investmentSecurity.id').value;
							if (TCG.isNotNull(securityId)) {
								Promise.resolve(tradeDate)
									.then(function(date) {
										if (TCG.isBlank(date)) {
											return '';
										}
										return Clifton.investment.instrument.getSettlementDatePromise(securityId, date, null, null, field);
									})
									.then(function(settlementDate) {
										form.findField('settlementDate').setValue(settlementDate);

										// Return promise for resolving all promises
										return Promise.all([
											fp.updateCurrentCoupon.call(fp, securityId),
											fp.updateAccruedInterest.call(fp),
											fp.updateIndexRatio.call(fp, securityId),
											fp.updateCurrentFactor.call(fp, securityId)
										]);
									})
									.then(function() {
										fp.updateNumberOfAccrualDays.call(fp);
										fp.updateCalculatedValues.call(fp, 'originalFace');
									});
							}
						}
					},
					{
						fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false, requiredFields: ['investmentSecurity.label'],
						getParentFormPanel: function() {
							const parent = TCG.getParentFormPanel(this);
							return parent || this.ownerCt;
						},
						listeners: {
							change: function(field, newValue, oldValue) {
								const fp = field.getParentFormPanel();
								const form = fp.getForm();

								fp.updateAccruedInterest.call(fp);

								const securityId = form.findField('investmentSecurity.id').value;
								if (TCG.isNotNull(securityId)) {
									Promise.all([
										fp.updateCurrentCoupon.call(fp, securityId),
										fp.updateIndexRatio.call(fp, securityId),
										fp.updateCurrentFactor.call(fp, securityId)
									]).then(function() {
										fp.updateNumberOfAccrualDays.call(fp);
										fp.updateCalculatedValues.call(fp, 'originalFace');
									});
								}
							},
							select: function(field, date) {
								const fp = field.getParentFormPanel();
								const form = fp.getForm();

								fp.updateAccruedInterest.call(fp);

								const securityId = form.findField('investmentSecurity.id').value;
								if (TCG.isNotNull(securityId)) {
									Promise.all([
										fp.updateCurrentCoupon.call(fp, securityId),
										fp.updateIndexRatio.call(fp, securityId),
										fp.updateCurrentFactor.call(fp, securityId)
									]).then(function() {
										fp.updateNumberOfAccrualDays.call(fp);
										fp.updateCalculatedValues.call(fp, 'originalFace');
									});
								}
							}
						}
					},
					{xtype: 'trade-bookingdate'}
				]
			}
		]
	}],

	populateClosingPosition: async function(positionRecord) {
		const formPanel = this;

		const getRecordFieldValue = function(recordKey) {
			return {
				text: TCG.getValue(recordKey, positionRecord.json),
				value: TCG.getValue(recordKey.substring(0, recordKey.lastIndexOf('.')) + '.id', positionRecord.json)
			};
		};

		formPanel.populateClosingPositionBuySell(positionRecord, true);
		formPanel.setFormValue.call(formPanel, 'clientInvestmentAccount.label', getRecordFieldValue('clientInvestmentAccount.label'));
		formPanel.setFormValue.call(formPanel, 'holdingInvestmentAccount.label', getRecordFieldValue('holdingInvestmentAccount.label'));

		const originalFace = formPanel.getClosingPositionOriginalFace(positionRecord);
		await formPanel.populateClosingPositionOriginalFace(originalFace);
	},

	populateClosingPositionBuySell: function(positionRecord, force) {
		const form = this.getForm();
		const quantity = this.getClosingPositionOriginalFace.call(this, positionRecord);
		const buy = quantity < 0;
		const existingBuySelection = form.items.find(i => i.fieldLabel === 'Buy/Sell').getValue();
		if (TCG.isBlank(existingBuySelection) || TCG.isTrue(force)) {
			form.items.find(i => i.fieldLabel === 'Buy/Sell').items.get((buy) ? 0 : 1).setValue(true);
		}
		else if (existingBuySelection.inputValue !== buy) {
			TCG.showError('Unable to add closing position because it would does not satisfy the current buy/sell selection.', 'Unsupported Position');
			return false;
		}
		return true;
	},

	getClosingPositionOriginalFace: function(positionRecord) {
		return positionRecord.json.unadjustedQuantity;
	},

	populateClosingPositionOriginalFace: async function(originalFace) {
		const formPanel = this;
		formPanel.setFormValue.call(formPanel, 'originalFace', originalFace);
		await formPanel.updateCalculatedValues.call(formPanel, 'originalFace');
	}
});
Ext.reg('trade-bond-form', Clifton.trade.BondTradeForm);


Clifton.trade.BondTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 950,
	height: 530,
	iconCls: 'bonds',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,

		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.trade.DefaultTradeWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.trade.DefaultTradeWindowAdditionalTabs[i]);
				}
			}
		},

		items: [
			{
				title: 'Info',
				tbar: {xtype: 'trade-workflow-toolbar'},

				items: [{
					xtype: 'trade-bond-form',

					listeners: {
						beforerender: function() {
							const status = TCG.getValue('workflowStatus.name', this.getWindow().defaultData);
							this.readOnlyMode = (TCG.isNotNull(status) && status !== 'Draft');
							if (this.readOnlyMode) {
								this.readOnlyMode = (this.getLoadURL() !== false);
							}
							if (this.readOnlyMode) {
								this.add(this.readOnlyItems);
							}
							else {
								this.add(this.commonEntryItems);
								this.add(this.additionalEntryItems);
							}
						},
						afterload: function() {
							const ro = ('Draft' !== this.getFormValue('workflowStatus.name'));
							this.setReadOnlyMode(ro);

							// Accrued Interest and settlement date are editable for Active trades
							const fp = this;
							const f = fp.getForm();
							const flag = ro && ('Active' !== fp.getFormValue('workflowStatus.name'));
							f.findField('notionalMultiplier').setReadOnly(flag);
							f.findField('accrualAmount1').setReadOnly(flag);
							f.findField('settlementDate').setReadOnly(flag);
							if (flag && fp.getFormValue('collateralTrade') === false) {
								fp.hideField('collateralTrade');
							}
							if (TCG.isNull(fp.getFormValue('tradeGroup.tradeGroupType.name'))) {
								fp.hideField('tradeGroup.tradeGroupType.name');
							}
							if (TCG.isNull(fp.getFormValue('settlementCompany.label'))) {
								fp.hideField('settlementCompany.label');
							}

							fp.activateBondType(fp.getFormValue('investmentSecurity.instrument.hierarchy'))
								.then(function() {
									fp.updateCalculatedValues();
								});
						}
					},

					readOnlyMode: true,
					setReadOnlyMode: function(ro) {
						if (this.readOnlyMode === ro) {
							this.processFieldVisibility();
							return;
						}
						this.readOnlyMode = ro;
						this.removeAll(true);
						if (this.readOnlyMode) {
							this.add(this.readOnlyItems);
						}
						else {
							this.add(this.commonEntryItems);
							this.add(this.additionalEntryItems);
						}
						try {
							this.doLayout();
						}
						catch (e) {
							// strange error due to removal of elements with allowBlank = false
						}

						const f = this.getForm();
						f.setValues(f.formValues);
						this.loadValidationMetaData(true);
						f.isValid();
						// process field visibility after fields have been reapplied to the form
						this.processFieldVisibility();
					},

					processFieldVisibility: function() {
						const f = this.getForm();
						const createOrder = this.getFormValue('tradeDestination.type.createOrder');
						const tradeOrderField = f.findField('orderIdentifier');
						if (TCG.isTrue(f.formValues.tradeType.fixSupported) || TCG.isTrue(createOrder)) {
							f.findField('tradeDestination.name').show();
							if (tradeOrderField && !createOrder) {
								tradeOrderField.hide();
							}
							else if (tradeOrderField) {
								tradeOrderField.show();
							}
						}
						else {
							f.findField('tradeDestination.name').hide();
							if (tradeOrderField) {
								tradeOrderField.hide();
							}
						}
					},


					items: [],

					additionalEntryItems: [
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: false, anchor: '-20',
							detailPageClass: 'Clifton.investment.account.AccountWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const fp = combo.getParentForm();
									combo.store.baseParams = {
										relatedPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
										relatedPurposeActiveOnDate: fp.getForm().findField('tradeDate').value
									};
								},
								beforeselect: function(combo, record) {
									const fp = combo.getParentForm();
									const f = fp.getForm();
									const ha = f.findField('holdingInvestmentAccount.label');
									ha.store.removeAll();
									ha.lastQuery = null;
									ha.store.baseParams = {
										mainAccountId: record.id,
										ourAccount: false,
										workflowStatusNameEquals: 'Active',
										mainPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
										mainPurposeActiveOnDate: f.findField('tradeDate').value
									};
									ha.store.on('load', function(store, records) {
										if (records.length === 1) {
											ha.setValue(records[0].id);
										}
									});
									ha.doQuery('');
								},
								select: function(combo, record) {
									const fp = combo.getParentForm();
									Clifton.trade.updateExistingPositions(fp);
								}
							}
						},
						{
							fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json', allowBlank: false, anchor: '-20', requiredFields: ['clientInvestmentAccount.label'], detailPageClass: 'Clifton.investment.account.AccountWindow',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.baseParams.contractRequired = f.findField('investmentSecurity.instrument.hierarchy.otc').getValue();
								}
							}

						},

						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop'},
						{xtype: 'trade-existingPositionsGrid', quantityColumnLabel: 'Current Face', usePositionBalance: true}
					],


					readOnlyItems: [
						{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .38,
									layout: 'form',
									labelWidth: 115,
									defaults: {xtype: 'textfield', width: 200},
									items: [
										{
											fieldLabel: 'Buy/Sell', name: 'openCloseType.label', xtype: 'displayfield', width: 125,
											setRawValue: function(v) {
												this.el.addClass(v.includes('BUY') ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, v);
											}
										},
										{fieldLabel: 'Bond', name: 'investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id'},
										{fieldLabel: 'Bond Type', name: 'investmentSecurity.instrument.hierarchy.name', detailIdField: 'investmentSecurity.instrument.hierarchy.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'},
										{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{name: 'tradeDestination.id', xtype: 'hidden'},
										{name: 'order.externalIdentifier', xtype: 'hidden', submitValue: false},
										{
											fieldLabel: 'Trade Destination', xtype: 'trade-destination-link',
											getDefaultData: function(fp) {
												return {
													externalIdentifier: fp.getFormValue('order.externalIdentifier'),
													tradeOrderStatus: fp.getFormValue('order.status'),
													sessionParameters: fp.getFormValue('order.destinationMapping.tradeDestination.connection')
												};
											}
										},
										{boxLabel: 'Collateral trade', name: 'collateralTrade', xtype: 'checkbox', disabled: true},
										{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
									]
								},
								{
									columnWidth: .33,
									layout: 'form',
									labelWidth: 100,
									defaults: {xtype: 'textfield', width: 115},
									items: [
										{fieldLabel: 'Original Face', name: 'originalFace', xtype: 'currencyfield', readOnly: true},
										{fieldLabel: 'Price', name: 'averageUnitPrice', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Factor', name: 'currentFactor', xtype: 'floatfield', readOnly: true, hidden: true},
										{fieldLabel: 'Purchase Face', name: 'quantityIntended', xtype: 'currencyfield', readOnly: true, hidden: true},
										{
											fieldLabel: 'Index Ratio', name: 'notionalMultiplier', xtype: 'floatfield', readOnly: true, hidden: true,
											listeners: {
												change: function(field, newValue, oldValue) {
													const fp = TCG.getParentFormPanel(field);
													fp.updateCalculatedValues.call(fp, 'notionalMultiplier', newValue, oldValue);
												}
											}
										},
										{fieldLabel: 'Inflation Adj Face', name: 'inflationAdjustedFace', xtype: 'currencyfield', readOnly: true, hidden: true},
										{fieldLabel: 'Current Coupon', name: 'currentCoupon', xtype: 'floatfield', readOnly: true, hidden: true},
										{fieldLabel: 'Accrual Start', name: 'accrualDate', xtype: 'displayfield', type: 'date', readOnly: true, hidden: true, qtip: 'Accrual starts the day after this date: End Of Day industry convention'},
										{fieldLabel: 'Accrual Days', name: 'accrualDays', xtype: 'displayfield', readOnly: true, hidden: true, qtip: 'Calendar days between Accrual Start and End Dates (using accrual date calculator). Does not take into account security Day Count convention.'}
									]
								},
								{
									columnWidth: .29,
									layout: 'form',
									labelWidth: 100,
									defaults: {xtype: 'textfield', anchor: '-20'},
									items: [
										{fieldLabel: 'Principal', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
										{fieldLabel: 'Accrued Interest', name: 'accrualAmount1', xtype: 'currencyfield', readOnly: true, hidden: true},
										{fieldLabel: 'Settlement Total', name: 'totalPayment', xtype: 'linkfield', type: 'currency', detailPageClass: 'Clifton.trade.TradeAmountsWindow', detailIdField: 'id', submitDetailField: false, qtip: 'Settlement Total = Principal + Accrued Interest', hidden: true},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true},
										{xtype: 'trade-bookingdate'}
									]
								}]
						},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
						{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'},
						{fieldLabel: 'Settlement Company', name: 'settlementCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'settlementCompany.id', qtip: 'Business company used to determine the place of settlement business identifier code (BIC) when generating settlement instructions.'},

						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop'}
					]
				}]
			},


			{
				title: 'Compliance',
				items: [{
					xtype: 'trade-violations-grid'
				}]
			},


			{
				title: 'Trade Fills',
				items: [{xtype: 'trade-fillsgrid'}]
			},


			{
				title: 'Commissions and Fees',
				items: [{
					xtype: 'trade-commissions'
				}]
			},


			{
				title: 'Quotes',
				items: [{
					xtype: 'trade-quotes',
					hideAdditionalAmounts: true
				}]
			}
		]
	}]
});
