Ext.ns(
	'Clifton.trade',
	'Clifton.trade.accounting.commission',
	'Clifton.trade.assetclass',
	'Clifton.trade.blotter',
	'Clifton.trade.destination',
	'Clifton.trade.entry',
	'Clifton.trade.execution',
	'Clifton.trade.group',
	'Clifton.trade.marketdata',
	'Clifton.trade.monitor',
	'Clifton.trade.option',
	'Clifton.trade.options.securityTarget',
	'Clifton.trade.quote',
	'Clifton.trade.restriction',
	'Clifton.trade.restriction.type',
	'Clifton.trade.roll',
	'Clifton.trade.roll.setup',
	'Clifton.trade.upload'
);

/*
 * Constants
 */
Clifton.trade.option.OPTION_LEG_TYPES = [
	['LONG_CALL', 'Long Call'],
	['SHORT_CALL', 'Short Call'],
	['LONG_PUT', 'Long Put'],
	['SHORT_PUT', 'Short Put']
];

Clifton.trade.option.OPTION_COMBINATION_TYPES = [
	['LONG_CALL', 'Long Call', 'An options strategy that utilizes long call positions only.'],
	['SHORT_CALL', 'Short Call', 'An options strategy that utilizes short call positions only.'],
	['LONG_PUT', 'Long Put', 'An options strategy that utilizes long put positions only.'],
	['SHORT_PUT', 'Short Put', 'An options strategy that utilizes short put positions only.'],
	['BEAR_CALL_SPREAD', 'Bear Call Spread', 'An options strategy that utilizes a short call at a lower strike price and a long call at a higher strike price.'],
	['BULL_CALL_SPREAD', 'Bull Call Spread', 'An options strategy that utilizes a long call at a lower strike price and a short call at a higher strike price.'],
	['BEAR_PUT_SPREAD', 'Bear Put Spread', 'An options strategy that utilizes a short put at a lower strike price and a long put at a higher strike price.'],
	['BULL_PUT_SPREAD', 'Bull Put Spread', 'An options strategy that utilizes a long put at a lower strike price and a short put at a higher strike price.'],
	['IRON_CONDOR', 'Iron Condor', 'An options strategy that utilizes a long put, a short put, a short call, and a long call in increasing order of strike price.'],
	['COLLAR', 'Collar', 'An options strategy that utilizes a long put at a lower strike price and a short call at a higher strike price, typically with a long position in the underlying.']
];

// Simple Upload Windows
Clifton.system.schema.SimpleUploadWindows['Trade'] = 'Clifton.trade.upload.TradeUploadWindow';
Clifton.system.schema.SimpleUploadWindows['TradeFill'] = 'Clifton.trade.upload.TradeFillUploadWindow';
Clifton.system.schema.SimpleUploadWindows['TradeRollDate'] = 'Clifton.trade.roll.setup.RollDateUploadWindow';

// additional tabs to be appended to trading blotter window can be added to this array, i.e. Portfolio Trade Creation
Clifton.trade.TradeBlotterAdditionalTabs = [];

// additional tabs to be appended to option blotter window can be added to this array, i.e. Portfolio DE Options Rolls
Clifton.trade.option.OptionBlotterAdditionalTabs = [];

// add restriction plugin to security target grid
Clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetGridPlugins.push({
	ptype: 'trade-restriction-aware',
	getRecordInvestmentSecurity: record => ({underlyingSecurity: record.json.targetUnderlyingSecurity}),
	clientInvestmentAccountColumnDataIndex: 'clientInvestmentAccount.label',
	underlyingSecurityColumnDataIndex: 'targetUnderlyingSecurity.label'
});

Clifton.trade.TradeWorkflowToolbar = Ext.extend(Clifton.workflow.Toolbar, {
	tableName: 'Trade',
	finalState: 'Canceled',
	enableOpenSessionInView: true,
	additionalSubmitParameters: {
		requestedPropertiesToExclude: ['order|exception.failedDTO.order|exception.cause.failedDTO.order'],
		requestedMaxDepth: 6
	},
	validateTransition: function(entity, stateName, userScope) {
		return Clifton.trade.validateWorkflowTransition([entity], stateName, userScope);
	}
});
Ext.reg('trade-workflow-toolbar', Clifton.trade.TradeWorkflowToolbar);

Clifton.trade.validateWorkflowTransition = async function(items, stateName, userScope) {
	if (stateName === 'Approved' || stateName === 'Cancelled') {
		const trades = (items[0] && items[0].json) ? items.map(record => record.json) : items;
		const userTeams = await Clifton.security.user.getUserTeamList(userScope)
			.then(teamList => teamList ? teamList : []);
		const userTeamNames = userTeams.map(team => team.name).distinct();
		const nonTeamTrades = trades.filter(trade => TCG.getValue('clientInvestmentAccount.teamSecurityGroup.name', trade) && !userTeamNames.includes(trade.clientInvestmentAccount.teamSecurityGroup.name));
		if (nonTeamTrades.length > 0) {
			const tradesString = nonTeamTrades.map(trade => `(id=${trade.id}, team=<b>${trade.clientInvestmentAccount.teamSecurityGroup.name}</b>}) ${trade.clientInvestmentAccount.label}: ${trade.buy ? 'BUY' : 'SELL'} ${trade.quantityIntended} of ${trade.investmentSecurity.symbol}`)
				.map(tradeString => `<li>${tradeString}</li>`).join('');
			return `Found selected trade(s) where you are not a member of the supporting team. Trades include:<br/><ul style="list-style: square; padding-inline-start: 20px;">${tradesString}</ul>`;
		}
	}
	return '';
};


// UI Overrides for other Projects
Clifton.trade.TradeWindowOverrides = {
	'Bonds': 'Clifton.trade.BondTradeWindow',
	'Currency': 'Clifton.trade.CurrencyTradeWindow',
	'Forwards': 'Clifton.trade.CurrencyForwardTradeWindow',
	'Swaps: CDS': 'Clifton.trade.SwapCDSTradeWindow',
	'Swaps: IRS': 'Clifton.trade.SwapIRSTradeWindow',
	'Swaps: TRS': 'Clifton.trade.SwapTRSTradeWindow',
	'Cleared: CDS': 'Clifton.trade.SwapCDSTradeClearedWindow',
	'Cleared: INFS': 'Clifton.trade.SwapINFSTradeClearedWindow',
	'Cleared: IRS': 'Clifton.trade.SwapIRSTradeClearedWindow',
	'Cleared: ZCS': 'Clifton.trade.SwapZCSTradeClearedWindow'
};

Clifton.trade.TradeNovationFieldSetCheckbox = Ext.extend(TCG.form.FieldSetCheckbox, {
	// xtype: 'fieldset-checkbox',
	title: 'Novation Trade',
	checkboxName: 'novation',
	name: 'novationFieldset',
	labelWidth: 140,
	anchor: '-20',

	listeners: {
		afterrender: function(fieldset) {
			// listener needed to update the value when a trade's state is updated from readOnly to editable, or the other way around
			fieldset.synchCheckboxValue();
		}
	},

	initItems: function() {
		// Overridden to reset the items due to some funny behavior in ExtJS when initializing.
		if (!(this.items instanceof Ext.util.MixedCollection)) {
			this.items = void 0;
		}

		if (!this.items) {
			this.items = new Ext.util.MixedCollection(false, this.getComponentId);
			this.getLayout();
		}
	},

	synchCheckboxValue: function() {
		// Overridden to check for rendered due to form field changes from readOnly to editable, or the other way around
		if (this.rendered) {
			this.checkbox.dom.checked = TCG.getValue(this.checkboxName, this.getParentForm().form.formValues);
			this.syncChildFormField();
			this.onCheckClick();
		}
	},

	onCheckClick: function() {
		if (this.rendered) {
			// overridden to handle form modification of hidden checkbox on fieldset
			const checked = this.checkbox.dom.checked;
			this.syncChildFormField();
			const formPanel = this.getParentForm();
			const originalValue = TCG.isTrue(formPanel.getFormValue('novation'));
			formPanel.getWindow().setModified(checked !== originalValue);
			// keep expanded if current or original value was true
			this[(checked || originalValue) ? 'expand' : 'collapse']();
		}
	},

	syncChildFormField: function() {
		const novationFormField = TCG.getChildByName(this, 'novation');
		if (novationFormField) {
			novationFormField.setValue(this.checkbox.dom.checked);
		}
	},

	items: [
		{xtype: 'hidden', name: 'novation', submitValue: false}, // form field to manage dirty since fieldset checkbox is not on form
		{
			fieldLabel: 'Assign To Counterparty', name: 'assignmentCompany.label', hiddenName: 'assignmentCompany.id', displayField: 'label', xtype: 'combo',
			url: 'tradeExecutingBrokerCompanyListFind.json', allowBlank: true, labelWidth: 150, detailPageClass: 'Clifton.business.company.CompanyWindow',
			queryParam: 'executingBrokerCompanyName',
			beforequery: function(queryEvent) {
				const combo = queryEvent.combo;
				const f = combo.getParentForm().getForm();

				combo.store.baseParams = {
					tradeDestinationId: f.findField('tradeDestination.name').getValue(),
					tradeTypeId: f.findField('tradeType.id').getValue(),
					activeOnDate: f.findField('tradeDate').value
				};
			}
		}
	]
});
Ext.reg('novation-fieldset', Clifton.trade.TradeNovationFieldSetCheckbox);

//optional additional tabs that can be added from other projects
Clifton.trade.DefaultTradeWindowAdditionalTabs = [];

Clifton.trade.downloadTradeReport = function(tradeId, componentScope) {
	const url = 'tradeReportDownload.json?tradeId=' + tradeId;
	TCG.downloadFile(url, null, componentScope);
};

Clifton.trade.TradeDestinationLink = Ext.extend(TCG.form.LinkField, {
	fieldLabel: 'Trade Destination',
	name: 'tradeDestination.name'
	// Overridden By trade order to link to the order correctly
});
Ext.reg('trade-destination-link', Clifton.trade.TradeDestinationLink);

Clifton.trade.BookingDateField = Ext.extend(TCG.form.LinkField, {
	fieldLabel: 'Booking Date',
	name: 'createDate', // need real field that will be set on load so that we can override with booking date
	detailIdField: 'id',
	detailPageClass: 'Clifton.accounting.journal.JournalWindow',
	submitDetailField: false,
	type: 'date',

	// render bookingDate of first booked fill
	setValue: function(v) {
		let bookingDate = '';
		const fp = TCG.getParentFormPanel(this);
		let fills = fp.getFormValue('tradeFillList');
		if (!fills) { // workflow transitions don't return fully populated trade
			const id = fp.getFormValue('id');
			if (id) {
				fills = TCG.data.getData('tradeFillListByTrade.json?requestedMaxDepth=2&tradeId=' + id, this);
				fp.getForm().formValues.tradeFillList = fills;
			}
		}
		if (fills) {
			for (let i = 0; i < fills.length; i++) {
				if (fills[i].bookingDate) {
					bookingDate = TCG.renderDate(fills[i].bookingDate);
					break;
				}
			}
		}
		Clifton.trade.BookingDateField.superclass.setValue.apply(this, [bookingDate]);
	},

	// drill into first booked fill journal
	getDetailIdFieldValue: function(formPanel) {
		const fills = formPanel.getFormValue('tradeFillList');
		let sourceId = -1;
		if (fills) {
			for (let i = 0; i < fills.length; i++) {
				if (fills[i].bookingDate) {
					if (sourceId !== -1) {
						TCG.showError('The trade has multiple fills that have been booked. Use Trade Fills tab to drill into the journal corresponding to each fill.', 'Multiple Fills');
						return false;
					}
					sourceId = fills[i].id;
				}
			}
		}
		const journal = TCG.data.getData('accountingJournalBySourceAndSequence.json?requestedMaxDepth=2&sourceTable=TradeFill&sourceId=' + sourceId + '&journalSequence=1', formPanel);
		return journal ? journal.id : null;
	}
});
Ext.reg('trade-bookingdate', Clifton.trade.BookingDateField);

Clifton.trade.DescriptionWithNoteDragDrop = Ext.extend(Ext.Panel, {
	xtype: 'panel',
	name: 'tradeNoteWithDragAndDrop',
	layout: 'column',
	labelWidth: 100,
	initComponent: function() {
		this.items = this.currentItems;
		Clifton.trade.DescriptionWithNoteDragDrop.superclass.initComponent.call(this);
	},
	currentItems: [
		{
			columnWidth: .02,
			layout: 'form',
			items: [{
				xtype: 'drag-drop-container',
				message: '&nbsp;',
				cls: undefined,
				layout: 'fit',
				popupComponentName: 'Clifton.document.system.note.SingleNoteWithDocumentCreateWindow',
				parentDDComponentName: 'tradeNoteWithDragAndDrop',
				bindToFormLoad: true,
				tooltipMessage: 'Drag and drop a file to create a new note associated with this trade and attach the file to it. You will need to select note type that is tagged with <i>Trade Info Tab</i> tag. <br><b>Note:</b> New Trades must be saved first before drag and drop is enabled.',
				getParams: function(fp) {
					const id = TCG.getValue('id', fp.getForm().formValues);
					const tbl = TCG.data.getData('systemTableByName.json?tableName=Trade', fp, 'system.table.Trade');
					const hierarchy = TCG.data.getData('systemHierarchyByCategoryAndNameExpanded.json?categoryName=System Note Type Tags&name=Trade Info Tab', fp, 'system.note.tag.Trade Info Tab');
					return {
						fkFieldId: id,
						categoryHierarchyId: hierarchy.id,
						noteType: {table: tbl}
					};
				}
			}]
		},
		{
			columnWidth: .98,
			layout: 'form',
			items: [
				{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', height: 50, anchor: '-20'}
			]
		}
	]

});
Ext.reg('trade-descriptionWithNoteDragDrop', Clifton.trade.DescriptionWithNoteDragDrop);

Clifton.trade.BaseTradeForm = Ext.extend(TCG.form.FormPanel, {
	url: 'trade.json',
	labelFieldName: 'labelLong',
	maxWindowTitleLength: 130,
	loadValidation: false,
	loadDefaultDataAfterRender: true,
	defaults: {anchor: '0'},

	reload: function() {
		const fp = this;
		TCG.data.getDataPromise('trade.json?requestedMaxDepth=6&UI_SOURCE=BaseTradeForm_reload&id=' + fp.getWindow().getMainFormId(), fp)
			.then(function(trade) {
				fp.getForm().setValues(trade, true);
				fp.fireEvent('afterload', fp);
			});
	},

	afterRenderPromise: async function(formPanel) {
		const form = formPanel.getForm();
		if (form.formValues && TCG.isBlank(form.formValues.id)) {
			if (TCG.isNotNull(form.formValues.investmentSecurity)) {
				// If the security was passed in with default data, set the record on the combo for form functions to use
				const security = form.formValues.investmentSecurity;
				const securityField = form.findField('investmentSecurity.label') || form.findField('investmentSecurity.symbol');
				if (securityField && securityField.xtype === 'combo') {
					const recordConstructor = securityField.getStore().recordType;
					const securityRecord = new recordConstructor(security);
					if (TCG.isNull(securityRecord.json)) {
						securityRecord.json = security;
						securityRecord.id = security.id;
					}
					await securityField.fireEvent('select', securityField, securityRecord);
				}
			}
			if (formPanel.afterRenderApplyAdditionalDefaultData) {
				await formPanel.afterRenderApplyAdditionalDefaultData.call(formPanel, formPanel, form.formValues);
			}

			if (TCG.isNotNull(form.formValues.buy)) {
				// check if form is valid to remove warning that buy/sell is not selected
				form.isValid();
			}
		}
	},

	getWarningMessage: function(form) {
		let msg = undefined;
		if (form.formValues && form.formValues.workflowState) {
			if (form.formValues.workflowState.name === 'Invalid' || form.formValues.workflowState.name === 'Executed Invalid') {
				msg = 'This trade failed compliance check(s). See <b>Compliance</b> tab for more information.';
			}
		}
		return msg;
	},

	getSecurityWithOtcDetails: async function(securityId) {
		const keyPrefix = 'OTC_DETAILS_SECURITY_LOOKUP';
		let security = null;
		if (TCG.isNotBlank(securityId)) {
			security = await TCG.data.getDataPromiseUsingCaching(`investmentSecurity.json?id=${securityId}&requestedPropertiesRoot=data&requestedProperties=instrument.hierarchy.otc|businessCompany.id|businessCompany.label`, this, `${keyPrefix}${securityId}`);
		}
		return security;
	},

	populateClosingPosition: async function(positionRecord) {
		const formPanel = this;

		const getRecordFieldValue = function(recordKey) {
			return {
				text: TCG.getValue(recordKey, positionRecord.json),
				value: TCG.getValue(recordKey.substring(0, recordKey.lastIndexOf('.')) + '.id', positionRecord.json)
			};
		};

		await formPanel.populateClosingPositionBuySell.call(formPanel, positionRecord, true);
		formPanel.setFormValue.call(formPanel, 'clientInvestmentAccount.label', getRecordFieldValue('clientInvestmentAccount.label'));
		formPanel.setFormValue.call(formPanel, 'holdingInvestmentAccount.label', getRecordFieldValue('holdingInvestmentAccount.label'));

		const securityWithOtcDetails = await formPanel.getSecurityWithOtcDetails(positionRecord.json.investmentSecurity.id);
		if (TCG.isTrue(securityWithOtcDetails.instrument.hierarchy.otc)) {
			const securityIssuingCompany = securityWithOtcDetails.businessCompany;
			const holdingAccountIssuingCompany = TCG.getValue('holdingInvestmentAccount.issuingCompany', positionRecord.json);
			const issuingCompany = TCG.coalesce(holdingAccountIssuingCompany, securityIssuingCompany);
			if (TCG.isNotBlank(issuingCompany)) {
				// populate control if available, as the above statement does not refresh 'Executing Broker' form componenet
				const executingBrokerComponent = TCG.getChildByName(formPanel, 'executingBrokerCompany.label');
				if (TCG.isNotBlank(executingBrokerComponent)) {
					executingBrokerComponent.setValue({value: issuingCompany.id, text: issuingCompany.label});
				}
			}
		}
		formPanel.populateClosingPositionQuantity.call(formPanel, positionRecord);
		await formPanel.populateClosingPositionAdditional.call(formPanel, positionRecord);
	},

	getClosingPositionQuantity: function(positionRecord) {
		return positionRecord.json.quantity;
	},

	populateClosingPositionBuySell: async function(positionRecord, force) {
		const form = this.getForm();
		const quantity = this.getClosingPositionQuantity.call(this, positionRecord);
		const buy = quantity < 0;
		const openCloseField = form.findField('openCloseType.label');
		let openCloseTypeName = buy ? 'Buy' : 'Sell';
		if (form.findField('tradeType.name').getValue() === 'Options') {
			openCloseTypeName = buy ? 'Buy to Close' : 'Sell to Close';
		}
		const openCloseType = await TCG.data.getDataPromiseUsingCaching(`tradeOpenCloseTypeByName.json?name=${openCloseTypeName}`, this, `openCloseType.${openCloseTypeName}`);
		if (TCG.isBlank(openCloseField.getValue()) || force) {
			openCloseField.setValue({text: openCloseType.label, value: openCloseType.id});
		}
		else if (openCloseField.getValue() !== openCloseType.id) {
			TCG.showError('Unable to add closing position because it would does not satisfy the current buy/sell selection.', 'Unsupported Position');
			return false;
		}
		return true;
	},

	populateClosingPositionQuantity: function(positionRecord) {
		this.setFormValue.call(this, 'quantityIntended', Math.abs(this.getClosingPositionQuantity.call(this, positionRecord)));
	},

	populateClosingPositionAdditional: function(positionRecord) {
		// Override for populating additional data for closing position.
	}
});
Ext.reg('trade-form-base', Clifton.trade.BaseTradeForm);

Clifton.trade.FillsGrid = Ext.extend(TCG.grid.EditorGridPanel, {
	name: 'tradeFillListByTrade',
	xtype: 'gridpanel',
	instructions: 'The following trade fills have been applied to this trade. If average pricing was not used, a trade may get multiple fills at different prices which affect realized gain/loss and cash flows on closing.',
	additionalPropertiesToRequest: 'trade.id',
	columns: [
		{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
		{header: 'Quantity', width: 40, dataIndex: 'quantity', type: 'float', negativeInRed: true, summaryType: 'sum', editor: {xtype: 'integerfield', allowBlank: false}},
		{header: 'Unit Price', width: 40, dataIndex: 'notionalUnitPrice', type: 'float', editor: {xtype: 'floatfield', allowBlank: false}},
		{header: 'Opening Date', width: 40, dataIndex: 'openingDate', type: 'date', editor: {xtype: 'datefield', allowBlank: true}, hidden: true},
		{header: 'Opening Price', width: 40, dataIndex: 'openingPrice', type: 'float', editor: {xtype: 'floatfield', allowBlank: true}, hidden: true},
		{header: 'Notional', width: 40, dataIndex: 'notionalTotalPrice', type: 'currency', summaryType: 'sum'},
		{header: 'Payment Unit Price', width: 40, dataIndex: 'paymentUnitPrice', type: 'float'},
		{header: 'Payment', width: 40, dataIndex: 'paymentTotalPrice', type: 'currency', summaryType: 'sum'},
		{header: 'Booking Date', width: 40, dataIndex: 'bookingDate'},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json', loadAll: true}, viewNames: ['Export Friendly', 'Westport Trading', 'Westport Operations'],
			renderer: function(v, metaData) {
				metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		}
	],
	onRender: function() {
		Clifton.trade.FillsGrid.superclass.onRender.apply(this, arguments);

		const win = this.getWindow();
		const gridPanel = this;
		const grid = gridPanel.grid;

		win.isModified = function() {
			let modified = false;
			grid.getStore().each(function(record) {
				if (record.modified) {
					modified = true;
				}
			});
			return modified || (win.getFormPanels(!win.forceModified).length > 0);
		};

		win.on('afterload', function(form, isClosing) {
			this.saveFills();
		}, this);
	},
	updateRecord: function(store, record, index) {
		//
	},
	saveFills: function() {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const fillList = [];
		let row = -1;

		// only save rows that were edited
		grid.getStore().each(function(record) {
			row = row + 1;
			if (record.modified) {
				const rowData = grid.getStore().getAt(row).data;
				const fill = {
					id: rowData['id'],
					quantity: rowData['quantity'],
					notionalUnitPrice: rowData['notionalUnitPrice'],
					notionalTotalPrice: null,
					openingDate: rowData['openingDate'].format('m/d/Y'),
					openingPrice: rowData['openingPrice'],
					'trade.id': gridPanel.getWindow().getMainFormId()
				};
				fillList.push(fill);
			}
		});
		this.doSaveFills(fillList, 0, gridPanel);
	},
	doSaveFills: function(fillList, count, gridPanel) {
		if (fillList.length > 0) {
			const fill = fillList[count];
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Saving...',
				waitTarget: this,
				params: fill,
				timeout: 120000,
				onLoad: function(record, conf) {
					count++;
					if (count === fillList.length) { // refresh after all trades were transitioned
						gridPanel.reload();
						gridPanel.grid.getSelectionModel().clearSelections(true);
					}
					else {
						gridPanel.doSaveFills(fillList, count, gridPanel);
					}
				},
				onFailure: function() {
					// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
					gridPanel.reload();
				}
			});
			loader.load('tradeFillSave.json?requestedPropertiesRoot=data&requestedProperties=id');
		}
	},
	plugins: {ptype: 'gridsummary'},
	getLoadParams: function() {
		return {tradeId: this.getWindow().getMainFormId()};
	},
	configureToolsMenu: function(menu) {
		const gp = this;
		menu.add('-');
		menu.add({
			text: 'Book and Post',
			tooltip: 'Book selected trade fill and immediately post it to the General Ledger',
			iconCls: 'book-red',
			scope: this,
			handler: function() {
				const sm = gp.grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a fill to be posted.', 'No Row(s) Selected');
				}
				else {
					Ext.Msg.confirm('Book & Post', 'Would you like to book and post selected trade fill?', function(a) {
						if (a === 'yes') {
							const loader = new TCG.data.JsonLoader({
								waitTarget: gp,
								waitMsg: 'Booking...',
								params: {
									journalTypeName: 'Trade Journal',
									sourceEntityId: sm.getSelected().id,
									postJournal: true,
									autoAdjust: false
								},
								timeout: 180000,
								onLoad: function(record, conf) {
									gp.reload();
								}
							});
							loader.load('accountingJournalBook.json?enableOpenSessionInView=true');
						}
					});
				}
			}
		});
		menu.add({
			text: 'Unpost',
			tooltip: 'Remove selected trade fill from GL, delete corresponding journal, and mark corresponding source entity as un-booked.',
			iconCls: 'undo',
			scope: this,
			handler: function() {
				const sm = gp.grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a fill to be unbooked.', 'No Row(s) Selected');
				}
				else {
					Ext.Msg.confirm('Unpost', 'Would you like to remove selected trade fill from GL, delete corresponding journal, and mark corresponding source entity as un-booked?', function(a) {
						if (a === 'yes') {
							const journal = TCG.data.getData('accountingJournalBySourceAndSequence.json?requestedMaxDepth=2&sourceTable=TradeFill&sourceId=' + sm.getSelected().id + '&journalSequence=1', gp);
							if (journal) {
								const loader = new TCG.data.JsonLoader({
									waitTarget: gp,
									params: {
										journalId: journal.id
									},
									onLoad: function(record, conf) {
										gp.reload();
									}
								});
								loader.load('accountingJournalUnpost.json');
							}
							else {
								TCG.showError('Cannot find a journal for selected fill.', 'No Journal Found');
							}
						}
					});
				}
			}
		});
	},
	editor: {
		detailPageClass: 'Clifton.accounting.journal.JournalWindow',
		getDetailPageId: function(gridPanel, row) {
			const journal = TCG.data.getData('accountingJournalBySourceAndSequence.json?requestedMaxDepth=2&sourceTable=TradeFill&sourceId=' + row.id + '&journalSequence=1', gridPanel);
			if (journal) {
				return journal.id;
			}
			TCG.showError('Selected Trade Fill must be booked before corresponding Journal can be open.', 'Drill Into Journal');
			return undefined;
		},
		getDeleteURL: function() {
			return 'tradeFillDelete.json?enableOpenSessionInView=true';
		},
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Save',
				tooltip: '',
				iconCls: 'disk',
				scope: this,
				handler: function() {
					gridPanel.saveFills();
				}
			});
			t.add('-');

			TCG.grid.GridEditor.prototype.addEditButtons.call(this, t);
		},
		addToolbarAddButton: function(t) {
			const gridPanel = this.getGridPanel();
			const trade = gridPanel.getWindow().getMainForm().formValues;
			t.add({html: 'Quantity:&nbsp;', xtype: 'label'});
			t.add({xtype: 'floatfield', name: 'fillQty', width: 50, value: trade.quantityIntended});
			t.add({html: '&nbsp;Unit Price:&nbsp;', xtype: 'label'});
			t.add({xtype: 'pricefield', name: 'fillPrice', width: 70});
			t.add({
				text: 'Add',
				iconCls: 'add',
				tooltip: 'Add trade fill for the specified quantity and unit price',
				scope: t,
				handler: function() {
					const qty = TCG.getChildByName(t, 'fillQty').getValue();
					const price = TCG.getChildByName(t, 'fillPrice').getValue();
					if (qty === '' || price === '') {
						TCG.showError('Required fields Quantity and Unit Price must be populated.', 'Validation');
						return;
					}
					const loader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						params: {
							'trade.id': trade.id,
							quantity: qty,
							notionalUnitPrice: price
						},
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					loader.load('tradeFillSave.json?requestedPropertiesRoot=data&requestedProperties=id'); // order is disconnected
				}
			}, '-');
		}
	}
});
Ext.reg('trade-fillsgrid', Clifton.trade.FillsGrid);

Clifton.trade.TradeViolationGridPanel = Ext.extend(Clifton.rule.violation.ViolationGridPanel, {
	previewAllowed: false, //Custom preview button defined below
	tableName: 'Trade',
	// OPTIONALLY ADD TRADE GROUP IGNORE VIOLATIONS BUTTON
	addCustomIgnoreButtons: function(toolBar) {
		const gridPanel = this;
		const tradeGroupId = gridPanel.getWindow().getMainFormPanel().getFormValue('tradeGroup.id');
		if (!TCG.isBlank(tradeGroupId)) {
			return [{
				text: 'Ignore Selected (For All Trades In Group)',
				tooltip: 'Mark selected violation as ignored (applies only if description is not required) and will ignore all violations of that type for all Trades in the Trade Group',
				iconCls: 'cancel',
				scope: gridPanel,
				handler: function() {
					const grid = this.grid;
					const sm = grid.getSelectionModel();

					if (sm.getCount() === 0) {
						TCG.showError('Please select a violation to mark as ignored.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection ignores are not supported.  Please select one row or click the Ignore All button to mark all as ignored.', 'NOT SUPPORTED');
					}
					else {
						gridPanel.ignoreViolations(null, 'tradeGroup.id');
					}
				}
			}];
		}
		return [];
	},
	addCustomEditButtons: function(toolBar) {
		const gridPanel = this;
		toolBar.add({
			text: 'Preview',
			xtype: 'splitbutton',
			tooltip: 'Preview rule options',
			iconCls: 'preview',
			scope: gridPanel,
			isClickOnArrow: function(e) {
				return true;
			},
			menu: new Ext.menu.Menu({
				items: [{
					text: 'All Rules',
					tooltip: 'Preview all violations including those that passed.',
					iconCls: 'rule',
					scope: gridPanel,
					handler: function() {
						gridPanel.previewHandler();
					}
				}, {
					text: 'Compliance Rules',
					tooltip: 'Preview fine grain details for Compliance department specific rules.',
					iconCls: 'verify',
					scope: gridPanel,
					handler: function() {
						const t = this.getWindow().getMainForm().formValues;
						const processDate = TCG.getChildByName(this.getWindow(), 'tradeDate').getValue().format('m/d/Y');
						let clientAccountId = TCG.getValue('clientInvestmentAccount.id', t);
						let clientInvestmentLabel = TCG.getValue('clientInvestmentAccount.label', t);
						const categoryTableEntityId = TCG.getValue('id', t);

						const grid = this.grid;
						const sm = grid.getSelectionModel();

						if (sm.getCount() !== 1 && sm.getCount() !== 0) {
							TCG.showError('Multi-selection preview is not supported.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							let complianceRuleId = undefined;
							let ruleName = undefined;
							if (sm.getCount() !== 0) {
								const warnNote = sm.getSelected().json.violationNote;
								if (warnNote && warnNote.indexOf('Parent [') !== -1) {
									const accountNumber = warnNote.substring(warnNote.indexOf('[') + 1, warnNote.indexOf(']'));
									const parentAccount = (TCG.data.getData('investmentAccountListFind.json?numberEquals=' + accountNumber, gridPanel)[0]);
									clientAccountId = parentAccount.id;
									clientInvestmentLabel = parentAccount.label;
								}
								complianceRuleId = sm.getSelected().json.causeFkFieldId;

								if (TCG.isNull(complianceRuleId)) {
									TCG.showError('Preview not supported for selected violation. Preview all by deselecting all violations with ctrl and mouse click.', 'NOT SUPPORTED');
									return;
								}

								const url = 'complianceRule.json?id=' + complianceRuleId;
								ruleName = TCG.data.getData(url, gridPanel).name;
							}

							const defaultData = {
								categoryTableEntityId: categoryTableEntityId,
								clientAccountId: clientAccountId,
								clientInvestmentLabel: clientInvestmentLabel,
								complianceRuleId: complianceRuleId,
								excludeParentSecuritySpecificRules: true,
								processDate: processDate,
								realTime: true,
								ruleName: ruleName
							};

							const className = 'Clifton.compliance.rule.ComplianceRuleRunPreviewWindow';
							TCG.createComponent(className, {
								defaultData: defaultData,
								openerCt: this
							});
						}
					}
				}]
			})
		});
		toolBar.add('-');
	},
	// Trades after ignored will be automatically validated
	afterIgnoreSuccessAction: function(includeIgnoreRelatedByField) {
		// Specified Warning Id means that we are only ignoring one warning, otherwise, marking all as ignored
		const gridPanel = this;
		const params = {};
		const url = 'tradeValidateActionExecute.json';
		if (includeIgnoreRelatedByField) {
			params['tradeGroupId'] = gridPanel.getWindow().getMainFormPanel().getFormValue('tradeGroup.id');
		}
		// Always pass this trade id so we can refresh main tab with updated values
		params['tradeId'] = gridPanel.getEntityId();

		const loader = new TCG.data.JsonLoader({
			waitTarget: gridPanel,
			waitMsg: 'Validating...',
			params: params,
			conf: params,
			onLoad: function(record, conf) {
				gridPanel.refreshAfterValidate();
			},
			onFailure: function() {
				gridPanel.refreshAfterValidate();
			}
		});
		loader.load(url);
	},
	refreshAfterValidate: function() {
		// Changed Trade Windows to contain a Reload method
		// Reloading from here forces some items to be re-drawn which is causing javascript errors because that tab is not currently visible
		const grid = this;
		grid.reload();

		/**
		 var w = grid.getWindow();
		 var fp = w.getMainFormPanel();


		 // If reload method is there then the tab will automatically reload when switching tabs
		 if (!fp.reload) {
			var f = fp.getForm();
			f.setValues(TCG.data.getData('trade.json?id=' + w.getMainFormId()), true);
			grid.getWindow().savedSinceOpen = true;
			fp.updateTitle();
			fp.fireEvent('afterload', fp, false);
		}
		 **/
	}
});
Ext.reg('trade-violations-grid', Clifton.trade.TradeViolationGridPanel);

Clifton.trade.TradeRestrictionBrokerGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeRestrictionBrokerListFind',
	instructions: 'A list of all trade broker restrictions and the accounts to which these restrictions apply.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 40, dataIndex: 'clientAccount.label', filter: {searchFieldName: 'clientAccountLabel'}},
		{header: 'Client Account Number', width: 18, dataIndex: 'clientAccount.number', filter: {searchFieldName: 'clientAccountNumber'}, hidden: true},
		{header: 'Broker Company', width: 40, dataIndex: 'brokerCompany.name', filter: {type: 'combo', searchFieldName: 'brokerCompanyId', url: 'businessCompanyListFind.json?tradingAllowed=true'}},
		{header: 'Include', width: 10, dataIndex: 'include', type: 'boolean', qtip: 'A check mark indicates that this account can only trade with this and other brokers selected for inclusion. An unchecked box indicates the broker' + ' is excluded and the account is not allowed to trade through this broker.'}
	],
	editor: {
		detailPageClass: 'Clifton.trade.restriction.TradeRestrictionBrokerWindow'
	}

});
Ext.reg('trade-restriction-broker-grid', Clifton.trade.TradeRestrictionBrokerGridPanel);

Clifton.trade.TradeRestrictionTypeGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeRestrictionTypeListFind',
	instructions: 'A list of all available trade restriction types.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
		{header: 'Type Name', width: 50, dataIndex: 'name'},
		{header: 'Category', width: 30, dataIndex: 'category.name', filter: {type: 'combo', searchFieldName: 'categoryName', url: 'tradeRestrictionTypeCategoryListFind.json'}},
		{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
		{header: 'Evaluate Condition', width: 50, dataIndex: 'evaluateCondition.name', filter: {type: 'combo', searchFieldName: 'evaluateConditionId', url: 'systemConditionListFind.json'}},
		{header: 'Violation Ignore Condition', width: 50, dataIndex: 'violationIgnoreCondition.name', filter: {type: 'combo', searchFieldName: 'violationIgnoreConditionId', url: 'systemConditionListFind.json'}},
		{header: 'Modify Condition', width: 50, dataIndex: 'entityModifyCondition.name', filter: {type: 'combo', searchFieldName: 'entityModifyConditionId', url: 'systemConditionListFind.json'}},
		{header: 'Cause System Table', width: 50, dataIndex: 'causeSystemTable.label', idDataIndex: 'causeSystemTable.id', filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'causeSystemTableId', displayField: 'label'}},
		{header: 'System Managed', width: 25, dataIndex: 'systemManaged', type: 'boolean', qtip: 'A check mark indicates that restrictions of this type can only be created by the system.'},
		{header: 'Do Not Allow Overlap', width: 27, dataIndex: 'doNotAllowOverlapInDates', type: 'boolean', qtip: 'A check mark indicates that restrictions of this type are allowed to overlap in start and end dates.'},
		{header: 'Account Required', width: 25, dataIndex: 'investmentAccountRequired', type: 'boolean', qtip: 'A check mark indicates that restrictions of this type must define an investment account.'},
		{header: 'Security Required', width: 25, dataIndex: 'investmentSecurityRequired', type: 'boolean', qtip: 'A check mark indicates that restrictions of this type must define an investment security.'},
		{header: 'Note Required', width: 25, dataIndex: 'restrictionNoteRequired', type: 'boolean', qtip: 'A check mark indicates that restrictions of this type must specify a note on creation.'}
	],
	editor: {
		detailPageClass: 'Clifton.trade.restriction.type.TradeRestrictionTypeWindow'
	}

});
Ext.reg('trade-restriction-type-grid', Clifton.trade.TradeRestrictionTypeGridPanel);

Clifton.trade.TradeRestrictionTypeCategoryGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeRestrictionTypeCategoryListFind',
	instructions: 'A list of all trade restriction categories.  Categories are used to group related Trade Restriction Types.',
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Category Name', width: 100, dataIndex: 'name'},
		{header: 'Description', width: 250, dataIndex: 'description'}
	],
	editor: {
		detailPageClass: 'Clifton.trade.restriction.type.TradeRestrictionTypeCategoryWindow'
	}

});
Ext.reg('trade-restriction-type-category-grid', Clifton.trade.TradeRestrictionTypeCategoryGridPanel);

Clifton.trade.TradeCommissionsGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeCommissionListByTrade.json',
	instructions: 'Previewed and overridden commissions and fees are listed here. Click <b>Override</b> to override a commission/fee. To configure commission/fee definitions, go to <b>Operations > Commissions and Fees</b>.',
	viewNames: [{text: 'Condensed', url: 'tradeCommissionListByTrade.json'}, {text: 'Expanded', url: 'tradeCommissionListExpandedByTrade.json'}],
	columns: [
		{header: 'FK Field ID', width: 30, dataIndex: 'fkFieldId', useNull: true, hidden: true},
		{header: 'Trade Fill ID', width: 35, dataIndex: 'tradeFill.id', useNull: true, hidden: true, viewNames: ['Expanded']},
		{
			header: 'Source', width: 40, dataIndex: 'source',
			tooltip: 'The source of the commission/fee.<br/><br/>' +
				'<b>Preview:</b> Indicates that the commission/fee is a preview of what the amount will be once the trade is booked. Previewed amounts are calculated from Commission Definitions.<br/><br/>' +
				'<b>Definition:</b> Indicates that the commission/fee was calculated from a Commission Definition.<br/><br/>' +
				'<b>Override:</b> Indicates that the commission/fee is a manual override. Overrides are set before a trade is booked.<br/><br/>' +
				'<b>Adjustment:</b> Indicates that the commission/fee is an adjustment to an amount that was already applied. Adjustments are set after a trade is booked.',
			renderer: function(value, metaData, r) {
				if (value === 'PREVIEW') {
					return 'Preview';
				}
				else if (value === 'OVERRIDE') {
					return 'Override';
				}
				else if (value === 'ACTUAL') {
					return 'Definition';
				}
				else if (value === 'ADJUSTMENT') {
					return 'Adjustment';
				}
			}
		},
		{header: 'GL Account', width: 110, dataIndex: 'accountingAccount.label', filter: {type: 'combo', searchFieldName: 'accountingAccountId', displayField: 'label', url: 'accountingAccountListFind.json?commission=true'}},
		{header: 'Note', width: 120, dataIndex: 'note'},
		{header: 'CCY', width: 50, dataIndex: 'commissionCurrency.symbol'},
		{header: 'Amount Per Unit', width: 50, dataIndex: 'amountPerUnit', type: 'float', useNull: true},
		{header: 'Total Amount', width: 50, dataIndex: 'totalAmount', type: 'currency', summaryType: 'sum', useNull: true, negativeInRed: true, positiveInGreen: true}
	],
	getLoadParams: function() {
		return {tradeId: this.getWindow().getMainFormId()};
	},
	plugins: {ptype: 'gridsummary', negativeInRed: true, positiveInGreen: true},
	listeners: {
		afterRender: function() {
			const win = this.getWindow();
			this.setDefaultView((win.defaultData && win.defaultData.viewName === 'Expanded') ? 'Expanded' : 'Condensed');
		}
	},
	editor: {
		detailPageClass: 'Clifton.trade.accounting.commission.TradeCommissionOverrideWindow',

		addEditButtons: function(t, gridPanel) {
			const win = this.getWindow();
			t.add({
				text: 'Override',
				tooltip: 'Override commission or fee',
				iconCls: 'fee',
				scope: this,
				handler: function() {
					TCG.createComponent('Clifton.trade.accounting.commission.TradeCommissionOverrideWindow', {
						openerCt: gridPanel,
						defaultData: {
							trade: {
								id: win.getMainFormId()
							}
						}
					});
				}
			});
			t.add('-');
			/*
			 * Have to use custom delete logic here (can only delete trade commissions of type 'OVERRIDE').
			 * In the future, it may make sense to enable deletion of multiple selections.
			 */
			t.add({
				text: 'Remove',
				tooltip: 'Remove selected item(s)',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const editor = this;
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
					}
					else if (sm.getCount() !== 1) {
						TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
					}
					else {
						const selected = sm.getSelected().json;
						if (selected.source !== 'OVERRIDE') {
							TCG.showError('Only commission overrides can be deleted in this view.');
						}
						else {
							Ext.Msg.confirm('Delete Selected Row', 'Would you like to delete selected row?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid.ownerCt,
										waitMsg: 'Deleting...',
										params: editor.getDeleteParams(sm),
										conf: {rowId: sm.getSelected().id},
										onLoad: function(record, conf) {
											grid.getStore().remove(sm.getSelected());
											grid.ownerCt.updateCount();
										}
									});
									loader.load(editor.getDeleteURL());
								}
							});
						}
					}
				}
			});
			t.add('-');
		},

		getDeleteParams: function(selectionModel) {
			const selected = selectionModel.getSelected().json;
			if (selected.source === 'OVERRIDE') {
				return {id: selected.fkFieldId};
			}
		},

		getDeleteURL: function() {
			return 'tradeCommissionOverrideDelete.json';
		},

		openDetailPage: function(className, gridPanel, id, row, itemText) {
			const win = gridPanel.getWindow();

			const source = row.data.source;
			const fkFieldId = row.data.fkFieldId;

			if (TCG.isBlank(fkFieldId) && (source !== 'PREVIEW')) {
				TCG.showError('Change the view to <b>Expanded</b> prior to opening the details for a trade commission.');
				return;
			}

			if (source === 'OVERRIDE') {

				const defaultData = fkFieldId ? this.getDefaultDataForExisting(gridPanel, row, className, itemText) : this.getDefaultData(gridPanel, row, className, itemText);
				if (defaultData === false) {
					return;
				}

				const cmpId = TCG.getComponentId(className, fkFieldId);
				const params = fkFieldId ? this.getDetailPageParams(fkFieldId) : undefined;

				TCG.createComponent(className, {
					id: cmpId,
					defaultData: defaultData,
					params: params,
					openerCt: gridPanel,
					defaultIconCls: win.iconCls
				});
			}
			else if (source === 'ACTUAL' || source === 'ADJUSTMENT') {
				const journal = TCG.data.getData('accountingJournal.json?id=' + fkFieldId, gridPanel);

				TCG.createComponent('Clifton.accounting.journal.JournalWindow', {
					defaultData: journal,
					defaultDataIsReal: true //We have to set this to true, otherwise the journal id won't get populated
				});
			}
		},

		getDefaultData: function(gridPanel) {
			return this.defaultData;
		}
	}
});
Ext.reg('trade-commissions', Clifton.trade.TradeCommissionsGrid);

Clifton.trade.DefaultTradeExistingPositionsFormGrid = Ext.extend(TCG.grid.FormGridPanel, {
	name: 'existingPositionsGrid',
	storeRoot: 'positionList',
	title: 'No existing positions found for selected security',
	bindToFormLoad: false,
	readOnly: true,
	collapsible: true,
	collapsed: true,
	requireClientAccountForPositionUpdates: false,
	anchor: '-20',
	quantityColumnLabel: 'Quantity',
	populateClosingPositionFunctionName: 'populateClosingPosition',
	viewConfig: {forceFit: true},
	usePositionBalance: false, // set to true to use accounting position lookup instead of accounting balance to include unadjusted quantity of positions (e.g. security with factor change: MBS, ABS, CMO, CDS (bonds and swaps)).

	initComponent: function() {
		this.columnsConfig = [...this.commonPrefixColumns];
		const usePositionBalance = TCG.isTrue(this.usePositionBalance);
		if (usePositionBalance) {
			this.urlOverride = 'accountingPositionBalanceListFind.json';
			this.additionalRequestedProperties = this.positionBalanceColumnsConfig.map(column => column.dataIndex).join('|');
			this.columnsConfig.push(...this.positionBalanceColumnsConfig);
		}
		else {
			this.columnsConfig.push(...this.accountingBalanceColumnsConfig);
		}
		this.columnsConfig.push(...this.commonSuffixColumns);

		Clifton.trade.DefaultTradeExistingPositionsFormGrid.superclass.initComponent.call(this);

		const columnModel = this.getColumnModel();
		if (usePositionBalance) {
			let qtyIndex = columnModel.findColumnIndex('unadjustedQuantity');
			columnModel.setColumnHeader(qtyIndex, this.quantityColumnLabel);
			qtyIndex = columnModel.findColumnIndex('remainingQuantity');
			columnModel.setColumnHeader(qtyIndex, 'Remaining ' + this.quantityColumnLabel);
		}
		else {
			const qtyIndex = columnModel.findColumnIndex('quantity');
			columnModel.setColumnHeader(qtyIndex, this.quantityColumnLabel);
		}
	},

	listeners: {
		afterrender: function(grid) {
			const columnModel = grid.getColumnModel();
			const closeColumnIndex = columnModel.findColumnIndex('fullyClose');
			const formPanel = TCG.getParentFormPanel(grid);
			const show = grid.isComponentCloseEnabled(formPanel);
			columnModel.setHidden(closeColumnIndex, !show);
			TCG.getChildByName(grid.getTopToolbar(), 'closeAllButton').setVisible(show && TCG.isNotNull(TCG.getChildByName(formPanel, 'tradeDetailListGrid')));
			grid.getTopToolbar().setVisible(true);
		}
	},

	isComponentCloseEnabled: function(component) {
		return TCG.isNotNull(component) && TCG.isNotNull(component[this.populateClosingPositionFunctionName]) && typeof component[this.populateClosingPositionFunctionName] === 'function';
	},

	addToolbarButtons: function(toolbar, grid) {
		toolbar.add({
			name: 'reloadButton',
			text: 'Reload',
			xtype: 'button',
			iconCls: 'table-refresh',
			handler: button => {
				const fp = TCG.getParentFormPanel(button);
				Clifton.trade.updateExistingPositions(fp);
			}
		});
		toolbar.add({
			name: 'closeAllButton',
			iconCls: 'shopping-cart',
			text: 'Close All',
			tooltip: 'Populate trade window fields for fully closing all positions within this grid',
			handler: function() {
				const formPanel = TCG.getParentFormPanel(grid);
				if (!grid.isComponentCloseEnabled(formPanel)) {
					TCG.showError('Unable to apply fully close details because the parent form is missing an implementation function to apply the position (' + grid.populateClosingPositionFunctionName + ').', 'Unsupported Operation');
					return;
				}

				grid.getStore().each(row => formPanel[grid.populateClosingPositionFunctionName].call(formPanel, row));
			}
		});
		toolbar.add('->');
		toolbar.add({
			name: 'requireClientAccount',
			boxLabel: 'Require Client Account',
			qtip: 'If checked, requires a client account selection before updating existing positions grid.',
			xtype: 'checkbox',
			checked: grid.requireClientAccountForPositionUpdates,
			listeners: {
				check: function(field) {
					const fp = TCG.getParentFormPanel(grid);
					grid.requireClientAccountForPositionUpdates = this.checked;
					Clifton.trade.updateExistingPositions(fp);
				},
				destroy: function(field) {
					field.disabled = true;
				}
			}
		});
		// render element members may be not shown collapsed such as fields since this is on the form
		toolbar.doLayout();
	},

	commonPrefixColumns: [
		{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 270, filter: {type: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
		{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', width: 270, filter: {type: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
		{header: 'GL Account', dataIndex: 'accountingAccount.name', width: 100, filter: {type: 'combo', displayField: 'label', url: 'accountingAccountListFind.json'}},
		{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 100, filter: {type: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true}
	],

	commonSuffixColumns: [
		{header: 'Holding Company', dataIndex: 'holdingInvestmentAccount.issuingCompany.label', width: 150, filter: {type: 'combo', displayField: 'name', url: 'businessCompanyListFind.json'}, hidden: true},
		{
			dataIndex: 'fullyClose', width: 60, sortable: false, filter: false,
			renderer: function(value, metadata, record) {
				return TCG.renderActionColumn('shopping-cart', 'Close', 'Populate trade window fields for full position close for this row', 'PopulateClose');
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const formPanel = TCG.getParentFormPanel(grid);
					if (!grid.isComponentCloseEnabled(formPanel)) {
						TCG.showError('Unable to apply fully close details because the parent form is missing an implementation function to apply the position (' + grid.populateClosingPositionFunctionName + ').', 'Unsupported Operation');
						return;
					}

					const row = grid.getStore().getAt(rowIndex);
					formPanel[grid.populateClosingPositionFunctionName].call(formPanel, row);
				}
			}
		}
	],

	accountingBalanceColumnsConfig: [
		{header: 'Quantity', dataIndex: 'quantity', width: 100, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, filter: false, sortable: false, summaryType: 'sum'},
		{header: 'Local Amount', dataIndex: 'localAmount', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: 'Base Amount', dataIndex: 'baseAmount', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, summaryType: 'sum', hidden: true},
		{header: 'Local Cost Basis', dataIndex: 'localCostBasis', type: 'currency', width: 120, sortable: false, negativeInRed: true, positiveInGreen: true}
	],

	positionBalanceColumnsConfig: [
		{header: 'Unadjusted Quantity', dataIndex: 'unadjustedQuantity', width: 100, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, filter: false, sortable: false, summaryType: 'sum', tooltip: 'Opening Quantity (Original Notional or Face) for securities with Factor Changes and Remaining Quantity for those without.'},
		{header: 'Remaining Quantity', dataIndex: 'remainingQuantity', width: 100, type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, filter: false, sortable: false, summaryType: 'sum', hidden: true},
		{header: 'Base Amount', dataIndex: 'remainingBaseDebitCredit', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, summaryType: 'sum', hidden: true},
		{header: 'Local Cost Basis', dataIndex: 'remainingCostBasis', type: 'currency', width: 120, sortable: false, negativeInRed: true, positiveInGreen: true}
	],

	plugins: {ptype: 'gridsummary'},
	detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
	getDefaultData: function(gridPanel, row) {
		// use selected row parameters for drill down filtering
		const filters = [];
		const data = row.json;

		filters.push({
			field: 'clientInvestmentAccountId',
			comparison: 'EQUALS',
			value: data.clientInvestmentAccount.id
		});
		filters.push({
			field: 'holdingInvestmentAccountId',
			comparison: 'EQUALS',
			value: data.holdingInvestmentAccount.id
		});
		filters.push({
			field: 'accountingAccountId',
			comparison: 'EQUALS',
			value: data.accountingAccount.id
		});
		filters.push({
			field: 'investmentSecurityId',
			comparison: 'EQUALS',
			value: data.investmentSecurity.id
		});
		const nextDay = new Date(gridPanel.getWindow().getMainFormPanel().getForm().findField('tradeDate').value).add(Date.DAY, 1).format('m/d/Y');

		filters.push({
			field: 'transactionDate',
			comparison: 'LESS_THAN',
			value: nextDay
		});
		return filters;
	}
});
Ext.reg('trade-existingPositionsGrid', Clifton.trade.DefaultTradeExistingPositionsFormGrid);

/**
 * Updates the positions in the Existing Positions Grid
 *
 * @param fp - form panel containing grid
 * @param securityId - security ID (required)
 * @param clientAccountId - client account ID (optional - used to filter down the returned list to positions for specified client account)
 */
Clifton.trade.updateExistingPositions = function(fp, securityId, clientAccountId) {
	const f = fp.getForm();
	const fg = TCG.getChildByName(fp, 'existingPositionsGrid');
	const noPositionsFoundTitle = 'No existing positions found for selected security';

	const secId = securityId ? securityId : f.findField('investmentSecurity.id').getValue();

	// optional field, that, if passed in, will help limit the existing position list to a specific account
	let clientInvestmentAccountId = clientAccountId;
	if (TCG.isBlank(clientInvestmentAccountId)) {
		const accountField = f.findField('clientInvestmentAccount.id');
		if (TCG.isNotBlank(accountField)) {
			clientInvestmentAccountId = accountField.getValue();
		}
	}

	if (fg && TCG.isTrue(fg.requireClientAccountForPositionUpdates) && TCG.isBlank(clientInvestmentAccountId)) {
		fg.store.removeAll();
		fg.setTitle(noPositionsFoundTitle);
		return;
	}

	if (fg && secId) {
		const urlService = fg.urlOverride ? fg.urlOverride : 'accountingAssetListFind.json';
		const additionalRequestedProperties = fg.additionalRequestedProperties ? fg.additionalRequestedProperties : '';
		const requestedProperties = `clientInvestmentAccount.label|clientInvestmentAccount.id|holdingInvestmentAccount.label|holdingInvestmentAccount.id|accountingAccount.name|accountingAccount.id|investmentSecurity.symbol|investmentSecurity.id|quantity|localAmount|baseAmount|localCostBasis|holdingInvestmentAccount.issuingCompany.id|holdingInvestmentAccount.issuingCompany.label|${additionalRequestedProperties}`;
		let url = `${urlService}?investmentSecurityId=${secId}&transactionDate=${f.findField('tradeDate').value}&positionAccountingAccount=true&requestedMaxDepth=4&requestedPropertiesRoot=data&requestedProperties=${requestedProperties}`;
		if (TCG.isNotBlank(clientInvestmentAccountId)) {
			url += '&clientInvestmentAccountId=' + clientInvestmentAccountId;
		}

		TCG.data.getDataPromise(url, fp)
			.then(function(data) {
				fg.store.loadData({positionList: data});
				if (data && data.length > 0) {
					fg.setTitle('Existing Positions: ' + data.length);
					fg.expand(false);
				}
				else {
					fg.setTitle(noPositionsFoundTitle);
					fg.collapse(false);
				}
				fp.doLayout();
			});
	}
};

Clifton.trade.UpdateTradeSettlementCompanyWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Update Trade Settlement Company',
	iconCls: 'shopping-cart',
	width: 600,
	height: 200,
	defaultDataIsReal: true,
	modal: true,
	items: [{
		xtype: 'formpanel',
		instructions: 'Update the Settlement Business Company on the Trade used to determine the place of settlement business identifier code (BIC) when generating settlement instructions.',
		items: [
			{
				fieldLabel: 'Settlement Company',
				xtype: 'combo',
				name: 'settlementCompanyName',
				displayField: 'label',
				hiddenName: 'settlementCompanyId',
				loadAll: true,
				url: 'businessCompanyListFind.json?companyType=Clearing House'
			}
		],
		getSaveURL: function() {
			return 'tradeSettlementCompanySave.json?enableOpenSessionInView=true';
		}
	}]
});

Clifton.trade.quote.TradeQuotesGrid = Ext.extend(TCG.grid.EditorGridPanel, {
	name: 'tradeQuoteListOrTemplateForTrade',
	instructions: 'The following competitive quotes were obtained from executing parties for this trade.',
	rowSelectionModel: 'checkbox',
	additionalPropertiesToRequest: 'executingBroker.id',
	appendStandardColumns: false,
	columns: [
		{header: 'Trade ID', width: 15, dataIndex: 'quoteRequest.trade.id', hidden: true},
		{header: 'Executing Broker', width: 140, dataIndex: 'executingBroker.label', idDataIndex: 'executingBroker.id', defaultSortColumn: true, filter: {type: 'combo', searchFieldName: 'executingBrokerId', displayField: 'label', url: 'businessCompanyListFind.json?issuer=true&abbreviationRequired=true'}, editor: {xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&abbreviationRequired=true'}},
		{header: 'Bid Price', width: 60, dataIndex: 'bidPrice', useNull: true, type: 'float', editor: {xtype: 'floatfield'}},
		{header: 'Ask Price', width: 60, dataIndex: 'askPrice', useNull: true, type: 'float', editor: {xtype: 'floatfield'}},
		{header: 'Spread', width: 60, dataIndex: 'financingSpread', useNull: true, type: 'float', tooltip: 'Financing Spread', editor: {xtype: 'floatfield'}},
		{header: 'Indep Amt', width: 60, dataIndex: 'independentAmount', useNull: true, type: 'currency', tooltip: 'Independent Amount', editor: {xtype: 'currencyfield'}},
		{header: 'Commission', width: 60, dataIndex: 'commissionAmount', useNull: true, type: 'currency', editor: {xtype: 'currencyfield'}},
		{header: 'Fee', width: 60, dataIndex: 'feeAmount', useNull: true, type: 'currency', editor: {xtype: 'currencyfield'}},
		{header: 'Note', width: 140, dataIndex: 'note', editor: {xtype: 'textfield'}},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', width: 16, exportHeader: 'File Attachment(s)', tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center'},
		{
			width: 25, fixed: true, dataIndex: 'id', filter: false, sortable: false,
			renderer: function(v, args, r) {
				return TCG.renderActionColumn('pencil', '', 'Advanced Quote Edit (allows file attachments)', 'EditQuote');
			}
		}
	],
	initComponent: function() {
		Clifton.trade.quote.TradeQuotesGrid.superclass.initComponent.call(this, arguments);

		const cm = this.grid.getColumnModel();
		if (this.hideAdditionalAmounts) {
			cm.setHidden(cm.findColumnIndex('financingSpread'), true);
			cm.setHidden(cm.findColumnIndex('independentAmount'), true);
			cm.setHidden(cm.findColumnIndex('commissionAmount'), true);
			cm.setHidden(cm.findColumnIndex('feeAmount'), true);
		}
	},
	getLoadParams: function(firstLoad) {
		return {tradeId: this.getWindow().getMainFormId()};
	},
	getNewRowDefaults: function() {
		return {'quoteRequest.trade.id': this.getWindow().getMainFormId()};
	},
	updateRecord: function(store, record, index) {
		//
	},
	getEditableElement: function() { // used by window to check if it was modified
		return this;
	},
	editor: {
		detailPageClass: 'Clifton.trade.quote.TradeQuoteWindow',
		getDetailPageClass: function(grid, row) {
			return row ? false : this.detailPageClass; // allow Add New but NO drill down
		},
		getDefaultData: function(gridPanel, row) {
			return {quoteRequest: {trade: gridPanel.getWindow().getMainForm().formValues}};
		},
		getDeleteURL: function() {
			return 'tradeQuoteDelete.json';
		},

		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Save',
				tooltip: '',
				iconCls: 'disk',
				scope: gridPanel,
				handler: function() {
					this.saveQuotes();
				}
			}, '-', {
				text: 'Quick Add',
				tooltip: 'Add a new item to the grid',
				iconCls: 'add',
				scope: gridPanel,
				handler: function() {
					const store = this.grid.getStore();
					const RowClass = store.recordType;
					this.grid.stopEditing();
					store.insert(0, new RowClass(this.getNewRowDefaults()));
					this.grid.startEditing(0, 0);
				}
			}, '-', {
				text: 'Add',
				tooltip: 'Add a new item using new window',
				iconCls: 'add',
				scope: this,
				handler: function() {
					this.openDetailPage(this.getDetailPageClass(), this.getGridPanel());
				}
			}, '-', {
				text: 'Remove',
				tooltip: 'Remove selected item(s)',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					const gridPanel = this.getGridPanel();
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
					}
					else {
						Ext.Msg.confirm('Delete Selected Rows', 'Would you like to delete selected rows?', function(a) {
							if (a === 'yes') {
								const ut = sm.getSelections();
								const recordList = [];
								for (let i = 0; i < ut.length; i++) {
									recordList.push(ut[i].json);
								}
								gridPanel.removeQuotes(recordList, 0, gridPanel);
							}
						});
					}
				}
			}, '-');
		}
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];

					// Default to the Main Account
					const id = row.json.id;
					if (eventName === 'EditQuote' && id && id > 0) {
						const gridPanel = grid.ownerGridPanel;
						gridPanel.editor.openDetailPage('Clifton.trade.quote.TradeQuoteWindow', gridPanel, id, row);
					}
				}
			}
		}
	},
	saveQuotes: function() {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const recordList = [];
		let row = -1;

		// only save rows that were edited
		grid.getStore().each(function(record) {
			row = row + 1;
			if (record.modified) {
				const rowData = grid.getStore().getAt(row).data;
				recordList.push(rowData);
			}
		});
		this.doSaveQuotes(recordList, 0, gridPanel);
	},
	doSaveQuotes: function(recordList, count, gridPanel) {
		if (recordList.length > 0) {
			const r = recordList[count];
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Saving...',
				waitTarget: gridPanel,
				params: r,
				onLoad: function(record, conf) {
					count++;
					if (count === recordList.length) { // refresh in the end
						gridPanel.reload();
						gridPanel.grid.getSelectionModel().clearSelections(true);
					}
					else {
						gridPanel.doSaveQuotes(recordList, count, gridPanel);
					}
				}
			});
			loader.load('tradeQuoteSave.json?requestedMaxDepth=3');
		}
	},
	removeQuotes: function(recordList, count, gridPanel) {
		const r = recordList[count];
		const loader = new TCG.data.JsonLoader({
			waitTarget: gridPanel,
			waitMsg: 'Deleting...',
			params: {id: r.id},
			conf: {rowId: r.id},
			onLoad: function(record, conf) {
				count++;
				if (count === recordList.length) { // refresh after all records were deleted
					gridPanel.reload();
				}
				else {
					gridPanel.removeQuotes(recordList, count, gridPanel);
				}
			}
		});
		loader.load(gridPanel.editor.getDeleteURL());
	}
});
Ext.reg('trade-quotes', Clifton.trade.quote.TradeQuotesGrid);

Clifton.trade.assetclass.TradeAssetClassPositionAllocationGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeAssetClassPositionAllocationListFind',
	xtype: 'gridpanel',
	instructions: 'The following asset class allocations have been created from Portfolio Trade Creation, Roll Entry, and/or Trade Import for contract splitting. If viewing this panel from a trade group window, the allocations listed may not have be created from the current trade group.',
	wikiPage: 'IT/Trading: Asset Class Allocations',
	showAccountColumn: true,
	viewNames: ['Default', 'Group By Asset Class'],
	defaultViewName: 'Group By Asset Class',

	switchToViewBeforeReload: function(viewName) {
		if (this.grid && this.grid.getStore) {
			const store = this.grid.getStore();
			if (store) {
				let columnDataIndex = void 0;
				switch (viewName) {
					case 'Group By Asset Class': {
						columnDataIndex = 'accountAssetClass.label';
						break;
					}
					default: {
						columnDataIndex = 'tradeLabel';
					}
				}
				this.groupField = columnDataIndex;
				store.groupBy(columnDataIndex, false);
				store.setDefaultSort('tradeDate', 'asc');
			}
		}
	},

	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({
			fieldLabel: 'Instrument Grp', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json',
			tooltip: 'Filter trade asset class allocations for a security belonging to an instrument group'
		});
		if (this.showAccountColumn === true) {
			filters.push({fieldLabel: 'Client Acct Grp', xtype: 'toolbar-combo', name: 'clientGroupName', hiddenName: 'clientInvestmentAccountGroupId', url: 'investmentAccountGroupListFind.json'});
			filters.push({fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'accountId', url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'accountAssetClass.account.label', displayField: 'label'});
		}
		filters.push({fieldLabel: '&nbsp;', labelSeparator: '', boxLabel: 'Include Canceled Trades', xtype: 'toolbar-checkbox', name: 'includeCanceledTrades'});
		return filters;
	},

	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			if (this.getTradeDateFilterValue) {
				// filter for provided date
				this.setFilterValue('tradeDate', {'on': this.getTradeDateFilterValue.call(this)});
			}
			else {
				// default balance date to previous business day
				const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
				this.setFilterValue('tradeDate', {'after': prevBD});
			}
		}
		return this.appendToolbarFilterParams();
	},

	appendToolbarFilterParams: function(params) {
		const includeCanceled = TCG.getChildByName(this.getTopToolbar(), 'includeCanceledTrades');
		if (includeCanceled.getValue() !== true) {
			if (!params) {
				params = {};
			}
			params.excludeTradeWorkflowStateName = 'Cancelled';
		}

		const t = this.getTopToolbar();
		let grp = TCG.getChildByName(t, 'clientGroupName');
		if (grp && TCG.isNotBlank(grp.getValue())) {
			params.clientInvestmentAccountGroupId = grp.getValue();
		}
		grp = TCG.getChildByName(t, 'investmentGroupId');
		if (grp && TCG.isNotBlank(grp.getValue())) {
			params.investmentGroupId = grp.getValue();
		}

		return params;
	},

	hiddenColumns: undefined, // Override to immediately hide columns
	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		if (this.hiddenColumns) {
			const cm = this.getColumnModel();
			for (let i = 0; i < this.hiddenColumns.length; i++) {
				const name = this.hiddenColumns[i];
				const colIndex = cm.findColumnIndex(name);
				cm.setHidden(colIndex, true);
			}
		}
	},

	listeners: {
		afterrender: function(gridPanel) {
			if (gridPanel.defaultViewName) {
				gridPanel.setDefaultView(gridPanel.defaultViewName);
			}
		}
	},

	groupField: 'tradeLabel',
	groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Allocations" : "Allocation"]}',

	columns: [
		{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
		{header: 'Trade Label', width: 50, dataIndex: 'tradeLabel', hidden: true, filter: false},

		{header: 'Trade Date', width: 50, dataIndex: 'tradeDate'},
		{header: 'Client Account', width: 175, dataIndex: 'accountAssetClass.account.label', filter: {searchFieldName: 'accountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}},
		{header: 'Security', width: 65, dataIndex: 'security.symbol', filter: {searchFieldName: 'securityLabel'}},

		{header: 'Asset Class', width: 100, dataIndex: 'accountAssetClass.label', filter: {searchFieldName: 'assetClassName'}},
		{header: 'Replication', width: 100, dataIndex: 'replication.name', filter: {searchFieldName: 'replicationName'}},

		{header: 'Trade ID', width: 30, dataIndex: 'trade.id', hidden: true, filter: {searchFieldName: 'tradeId'}},
		{header: 'Trade Note', width: 100, dataIndex: 'trade.description', hidden: true, filter: false, sortable: false},
		{
			header: 'Trade State', width: 65, dataIndex: 'trade.workflowState.name',
			renderer: function(value, metaData, r) {
				if (TCG.isNotBlank(value)) {
					return TCG.renderActionColumn('shopping-cart', value, 'View Trade', 'ViewTrade');
				}
				metaData.css = 'amountAdjusted';
				return 'Virtual Trade';
			}
		},
		{header: 'Quantity', width: 50, dataIndex: 'allocationQuantity', type: 'float', negativeInRed: true, summaryType: 'sum'},
		{
			header: 'Buy/Sell', width: 30, dataIndex: 'trade.buy', type: 'boolean', hidden: true, viewNames: ['Group By Asset Class'],
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Exchange Rate', width: 30, dataIndex: 'trade.exchangeRateToBase', type: 'float', useNull: true, hidden: true, viewNames: ['Group By Asset Class']},
		{header: 'Average Price', width: 30, dataIndex: 'trade.averageUnitPrice', type: 'float', useNull: true, hidden: true, viewNames: ['Group By Asset Class']},
		{header: 'Commission Per Unit', width: 30, dataIndex: 'trade.commissionPerUnit', type: 'float', hidden: true, viewNames: ['Group By Asset Class']},
		{header: 'Fee', width: 30, dataIndex: 'trade.feeAmount', type: 'float', hidden: true},
		{header: 'Accounting Notional', width: 30, dataIndex: 'trade.accountingNotional', type: 'currency', useNull: true, hidden: true},
		{header: 'Processed', width: 30, dataIndex: 'processed', type: 'boolean'},
		{
			header: 'Pending', width: 50, dataIndex: 'pendingQuantity', type: 'float', negativeInRed: true, summaryType: 'sum', useNull: true,
			tooltip: 'Contracts are still considered Pending and would show up as such on the Trade Creation View'
		},
		{
			header: 'Booked', width: 50, dataIndex: 'actualQuantity', type: 'float', negativeInRed: true, summaryType: 'sum', useNull: true,
			tooltip: 'Related trade has been booked, and or Virtual Trade has been processed full for contract splitting and the affected quantity applies now to Actual Contracts.'
		}
	],
	plugins: {ptype: 'gridsummary', showGrandTotal: false},
	editor: {
		detailPageClass: 'Clifton.trade.assetclass.PositionAllocationWindow',
		drillDownOnly: true
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					const row = grid.store.data.items[rowIndex];
					if (eventName === 'ViewTrade') {
						const tradeId = row.json.trade.id;
						if (tradeId !== '') {
							const gridPanel = grid.ownerGridPanel;
							TCG.createComponent('Clifton.trade.TradeWindow', {
								id: TCG.getComponentId('Clifton.trade.TradeWindow', tradeId),
								params: {id: tradeId},
								openerCt: gridPanel
							});
						}
					}
				}
			}
		}
	}
});
Ext.reg('trade-assetclass-position-allocation-grid', Clifton.trade.assetclass.TradeAssetClassPositionAllocationGrid);

Clifton.investment.account.assetclass.position.PositionAllocationListWindowAdditionalTabs[Clifton.investment.account.assetclass.position.PositionAllocationListWindowAdditionalTabs.length] = {
	title: 'Trade Allocation',
	items: [{
		xtype: 'trade-assetclass-position-allocation-grid'
	}]
};

Clifton.trade.group.TradeGroupEntryGridPanel = Ext.extend(TCG.grid.FormGridPanel, {
	title: 'Trade Allocations',
	storeRoot: 'tradeDetailList',
	name: 'tradeDetailListGrid',
	dtoClass: 'com.clifton.trade.group.TradeEntryDetail',
	anchor: '-20',
	viewConfig: {forceFit: true},

	columnsConfig: [
		{
			header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', width: 250,
			editor: {
				xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', displayField: 'label', allowBlank: false,
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const fp = TCG.getParentFormPanel(combo.gridEditor.containerGrid);
					const f = fp.getForm();
					combo.store.baseParams = {
						relatedPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
						relatedPurposeActiveOnDate: f.findField('tradeDate').value
					};
				},
				selectHandler: function(combo, record, index) {
					const grid = combo.gridEditor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					const f = fp.getForm();
					const row = combo.gridEditor.row;
					grid.startEditing(row, 1);

					// clear holding account and auto-select new if one
					const hCombo = grid.getColumnModel().getCellEditor(1, row).field;
					hCombo.clearValue();
					hCombo.store.removeAll();
					hCombo.lastQuery = null;
					hCombo.store.baseParams = {
						mainAccountId: combo.value,
						ourAccount: false,
						workflowStatusNameEquals: 'Active',
						mainPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
						mainPurposeActiveOnDate: f.findField('tradeDate').value,
						contractRequired: fp.getFormValue('investmentSecurity.instrument.hierarchy.otc')
					};
					hCombo.store.load({
						callback: function(records, opts, success) {
							if (success && records.length === 1) {
								const r = records[0];
								hCombo.setValue({value: r.get('id'), text: r.get('label')});
								grid.startEditing(row, 2);
							}
						}
					});
				}
			}
		},
		{
			header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', width: 250,
			editor: {
				xtype: 'combo', loadAll: true, url: 'investmentAccountListFind.json', displayField: 'label', allowBlank: false,
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const grid = combo.gridEditor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					const f = fp.getForm();
					const row = combo.gridEditor.row;
					combo.store.baseParams = {
						ourAccount: false,
						workflowStatusNameEquals: 'Active',
						mainAccountId: grid.getColumnModel().getCellEditor(0, row).getValue(),
						mainPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
						mainPurposeActiveOnDate: f.findField('tradeDate').value,
						contractRequired: fp.getFormValue('investmentSecurity.instrument.hierarchy.otc')
					};
				}
			}
		},
		{header: 'Quantity', dataIndex: 'quantityIntended', width: 80, type: 'float', editor: {xtype: 'numberfield', allowBlank: false}, summaryType: 'sum'},
		{header: 'Unit Price', dataIndex: 'expectedUnitPrice', width: 80, type: 'float', useNull: true, editor: {xtype: 'floatfield'}},
		{header: 'Commission Per Unit', dataIndex: 'commissionPerUnit', width: 120, type: 'float', useNull: true, editor: {xtype: 'floatfield', allowBlank: true}},
		{header: 'Fee', dataIndex: 'feeAmount', width: 80, type: 'currency', useNull: true, editor: {xtype: 'currencyfield', allowBlank: true}, summaryType: 'sum'}
	],
	plugins: {ptype: 'gridsummary'},

	/**
	 * Looks for an existing allocation row of this grid with a matching clientInvestmentAccount and holdingInvestmentAccount. If a match is found, the record is returned.
	 * If a match is not found, a new record will be added to the end of the grid with the clientInvestmentAccount and holdingInvestmentAccount populated from the provided positionRecord.
	 * If there is no positionRecord provided, a new record will be added as the last item to the current grid.
	 * @returns {Ext.data.Record|Ext.data.Store.constructor.reader.recordType|Ext.data.Store.onMetaChange.reader.recordType}
	 */
	getAllocationRecord: function(positionRecord) {
		const allocationGrid = this;
		const store = allocationGrid.getStore();

		let existingAllocationRecordId = -1;
		if (positionRecord) {
			existingAllocationRecordId = store.findBy((record, id) => record.data['clientInvestmentAccount.id'] === TCG.getValue('clientInvestmentAccount.id', positionRecord.json)
				&& record.data['holdingInvestmentAccount.id'] === TCG.getValue('holdingInvestmentAccount.id', positionRecord.json));
		}

		if (existingAllocationRecordId < 0) {
			const rowClass = store.recordType;
			const defaultData = positionRecord ? {
				'clientInvestmentAccount.label': positionRecord.json.clientInvestmentAccount.label,
				'clientInvestmentAccount.id': positionRecord.json.clientInvestmentAccount.id,
				'holdingInvestmentAccount.label': positionRecord.json.holdingInvestmentAccount.label,
				'holdingInvestmentAccount.id': positionRecord.json.holdingInvestmentAccount.id
			} : {};
			const allocationRecord = new rowClass(defaultData);
			allocationGrid.stopEditing();
			store.insert(store.getCount(), allocationRecord);
			return allocationRecord;
		}

		return store.getAt(existingAllocationRecordId);
	}
});
Ext.reg('trade-group-entry-grid', Clifton.trade.group.TradeGroupEntryGridPanel);


Clifton.trade.group.DefaultTradeGroupGridPanel = Ext.extend(TCG.grid.FormGridPanel, {
	title: 'Trades',
	name: 'default-trade-group-grid',
	storeRoot: 'tradeList',
	dtoClass: 'com.clifton.trade.Trade',
	readOnly: true,
	heightResized: true,
	collapsible: false,
	detailPageClass: 'Clifton.trade.TradeWindow',
	anchor: '-20',
	useFilters: true,
	// callback used control changed trade group actions and grid refreshes
	actionCallback: undefined,
	// placeholder to be set to the selectField's value for callbacks to access
	actionCallbackSelectionValue: undefined,
	// placeholder for tracking action callback results for display after the final one executes
	actionCallbackResultAppender: undefined,

	executeAction: function(groupAction) {
		if (this.submitField.getValue() === '[]') {
			TCG.showError('No trades selected.  Please select at least one trade from the list.');
			return;
		}
		const panel = TCG.getParentFormPanel(this);
		const form = panel.getForm();
		form.findField('tradeGroupAction').setValue(groupAction);
		this.getWindow().saveWindow(false);
	},

	openActionWindow: function(windowClass, toolBar, gridPanel) {
		if (this.submitField.getValue() === '[]') {
			TCG.showError('No trades selected.  Please select at least one trade from the list.');
			return;
		}
		const panel = TCG.getParentFormPanel(this);
		const form = panel.getForm();
		const dd = form.formValues;
		// Get Destination and Executing Broker from the first selected row to default the combos in the modal window.
		this.store.each(function(record) {
			if (record.modified) {
				const executingBrokerId = record.get('executingBrokerCompany.id');
				const tradeDestinationId = record.get('tradeDestination.id');
				if (executingBrokerId && tradeDestinationId) {
					dd.executingBrokerCompany = {id: executingBrokerId, label: record.get('executingBrokerCompany.label')};
					dd.tradeDestination = {id: tradeDestinationId, name: record.get('tradeDestination.name')};
					dd.tradeType = {name: TCG.getValue('tradeType.name', record.json)};
					return false; // break iteration
				}
			}
		});
		TCG.createComponent(windowClass, {
			defaultData: dd,
			openerCt: gridPanel
		});
	},

	applyModalTradeUpdates: function(tradeValues) {
		const grid = this;
		const executingBrokerCompanyId = tradeValues['executingBrokerCompany.id'];
		const tradeDestinationId = tradeValues['tradeDestination.id'];
		const removeCommission = tradeValues['removeCommission'];
		const commissionPerUnitOverride = removeCommission === 'on' ? '' : tradeValues['commissionPerUnit'];
		let changes = false;
		grid.store.each(function(record) {
			if (record.modified) {
				if (executingBrokerCompanyId && record.get('executingBrokerCompany.id') !== executingBrokerCompanyId) {
					record.set('executingBrokerCompany.id', executingBrokerCompanyId);
					record.set('executingBrokerCompany.label', tradeValues['executingBrokerCompany.label']);
					changes = true;
				}
				if (tradeDestinationId && record.get('tradeDestination.id') !== tradeDestinationId) {
					record.set('tradeDestination.id', tradeDestinationId);
					record.set('tradeDestination.name', tradeValues['tradeDestination.name']);
					changes = true;
				}
				if ((commissionPerUnitOverride && commissionPerUnitOverride !== '') || removeCommission === 'on') {
					record.set('commissionPerUnit', commissionPerUnitOverride);
					// add this to signal to TradeObserver that the UI is trying to override commission
					record.set('enableCommissionOverride', true);
					changes = true;
				}
			}
		}, grid);
		grid.markModified();
		if (changes) {
			grid.actionCallbackSelectionValue = grid.submitField.getValue();
			if (tradeValues['rejectTradesForEdit'] === 'on') {
				// Execute save with pre-reject and optional post-validate callback(s).
				grid.actionCallback = function() {
					if (tradeValues['validateTradesAfterEdit'] === 'on') {
						grid.actionCallback = function() {
							grid.submitField.setValue(grid.actionCallbackSelectionValue);
							grid.executeAction.call(grid, 'VALIDATE');
						};
						grid.submitField.setValue(grid.actionCallbackSelectionValue);
						grid.executeAction.call(grid, 'SAVE_TRADE');
					}
					else {
						grid.submitField.setValue(grid.actionCallbackSelectionValue);
						grid.executeAction.call(grid, 'SAVE_TRADE');
					}
				};
				grid.executeAction('REJECT');
			}
			else {
				// Execute save with no callback
				grid.executeAction('SAVE_TRADE');
			}
		}
		else {
			TCG.showInfo('No changes were made to the selected row(s).');

		}
	},

	applyPriceToSelectedTrades: function(toolBar) {
		const price = TCG.getChildByName(toolBar, 'fillPrice').getValue();
		const store = this.store;
		if (price !== '' && store.modified) {
			store.each(function(record) {
				if (record.modified) {
					record.set('fillPrice', price);
					const specifiedFillQuantity = record.get('fillQuantity');
					if (!specifiedFillQuantity || '' === specifiedFillQuantity) {
						const intendedQuantity = record.get('quantityIntended');
						if (intendedQuantity && '' !== intendedQuantity) {
							// for convenience, set price on selected rows without having to specify fill quantities
							record.set('fillQuantity', intendedQuantity);
						}
					}
				}
			}, this);
			this.markModified();
		}
	},

	applyCommissionOverrideToSelectedTrades: function(commissionPerUnitOverride) {
		const store = this.store;
		if (TCG.isNotNull(commissionPerUnitOverride) && store.modified) {
			store.each(function(record) {
				if (record.modified) {
					record.set('commissionPerUnit', commissionPerUnitOverride);
					// add this to signal to TradeObserver that the UI is trying to override commission
					record.set('enableCommissionOverride', true);
				}
			}, this);
			this.markModified();
		}
	},

	applyTradeBlockToSelectedTrades: function(toBlockTrades) {
		const store = this.store;
		if (store.modified) {
			toBlockTrades = TCG.isTrue(toBlockTrades);
			store.each(function(record) {
				record.set('blockTrade', toBlockTrades);
			});
			this.markModified();
		}
	},

	addToolbarButtons: function(toolBar) {
		const gp = this;
		// Use methods to add Toolbar items to allow extensiblity
		gp.addToolbarApproveTradesButton.call(gp, toolBar);
		gp.addToolbarUpdateTradesButton.call(gp, toolBar);
		gp.addToolbarReloadButton.call(gp, toolBar);
		gp.addToolbarToolsMenu.call(gp, toolBar);
		toolBar.add('->');
		gp.addToolbarCommissionApplyButton.call(gp, toolBar);
		gp.addToolbarFillSplitButton.call(gp, toolBar);
		gp.addToolbarMoreActionsButton.call(gp, toolBar);
	},

	addToolbarApproveTradesButton: function(toolBar) {
		const gp = this;
		toolBar.add({
			text: 'Approve Trades',
			tooltip: 'Approve Selected Trades.',
			iconCls: 'verify',
			scope: this,
			handler: function() {
				gp.executeAction('APPROVE');
			}
		});
	},

	addToolbarUpdateTradesButton: function(toolBar) {
		const gp = this;
		toolBar.add('-');
		toolBar.add({
			text: 'Update Trades...',
			tooltip: 'Update the Executing Broker and/or Apply Commission Overrides to the selected Trades.',
			iconCls: 'add',
			scope: this,
			handler: function() {
				gp.openActionWindow('Clifton.trade.group.TradeGroupTradeUpdateWindow', toolBar, gp);
			}
		});
	},

	addToolbarReloadButton: function(toolBar) {
		const gp = this;
		toolBar.add('-');
		toolBar.add({
			text: 'Reload',
			tooltip: 'Reload trades for this trade group.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.reload();
			}
		});
	},

	addToolbarToolsMenu: function(toolBar) {
		const gp = this;
		toolBar.add('-');
		const exportMenu = new Ext.menu.Menu();
		exportMenu.add({
			text: 'Export to Excel',
			iconCls: 'excel',
			menu: {
				items: [
					{
						text: 'Trade Columns (Excel 97-2003)',
						iconCls: 'excel',
						handler: function() {
							gp.exportGrid('xls');
						}
					}, '-', {
						text: 'Trade Columns (*.xlsx)',
						iconCls: 'excel_open_xml',
						handler: function() {
							gp.exportGrid('xlsx');
						}
					}

				]
			}
		});
		exportMenu.add({
			text: 'Export to CSV',
			iconCls: 'csv',
			handler: function() {
				gp.exportGrid('csv');
			}
		});
		exportMenu.add({
			text: 'Export to PDF',
			iconCls: 'pdf',
			handler: function() {
				gp.exportGrid('pdf');
			}
		});
		exportMenu.add({
			text: 'Trade Allocation',
			tooltip: 'View trades of this trade group by executing broker for viewing and sending trade allocations',
			iconCls: 'export',
			handler: function() {
				const fp = TCG.getParentFormPanel(gp);
				const form = fp.getForm();
				TCG.createComponent('Clifton.trade.TradeAllocationWindow', {defaultData: {tradeGroupId: TCG.getValue('id', form.formValues),
					shouldConsolidateDataRows: fp.shouldConsolidateDataRows, shouldInsertTotalRows: fp.shouldInsertTotalRows}});
			}
		});
		toolBar.add({
			text: 'Tools',
			iconCls: 'config',
			menu: exportMenu
		});
	},

	addToolbarCommissionApplyButton: function(toolBar) {
		const gp = this;
		toolBar.add({xtype: 'label', html: 'Commission:&nbsp;', qtip: 'Commission Per Unit override to apply to selected trades when \'Apply Commission\' is clicked.'});
		toolBar.add({xtype: 'pricefield', name: 'commissionPerUnitOverride', width: 70});
		toolBar.add({
			text: 'Apply Commission',
			tooltip: 'Apply the specified Commission Per Unit Override to the Selected Trades. Leaving the value blank will remove the Commission Per Unit Override for the Selected Trades',
			iconCls: 'add',
			scope: this,
			handler: function() {
				const commissionPerUnitOverride = TCG.getChildByName(toolBar, 'commissionPerUnitOverride').getValue();
				gp.applyCommissionOverrideToSelectedTrades(commissionPerUnitOverride);
				gp.executeAction('SAVE_TRADE');
			}
		});
	},

	addToolbarFillPriceField: function(toolBar) {
		toolBar.add({xtype: 'label', html: '&nbsp;Fill Price:&nbsp;', qtip: 'Fill price to apply to selected trades when \'Fill Trades\' or \'Fill & Execute Trades\' is clicked.'});
		toolBar.add({xtype: 'pricefield', name: 'fillPrice', width: 70});
	},

	addToolbarFillSplitButton: function(toolBar) {
		const gp = this;
		toolBar.add('-');
		gp.addToolbarFillPriceField.call(gp, toolBar);
		toolBar.add({
			text: 'Fill Trades',
			xtype: 'splitbutton',
			tooltip: 'Create Fills for Selected Trades with their Fill Price & Quantity. May choose \'Fill Trades\' or \'Fill & Execute Trades\'',
			iconCls: 'add',
			scope: this,
			handler: function() {
				gp.applyPriceToSelectedTrades(toolBar);
				gp.executeAction('FILL');
			},
			menu: new Ext.menu.Menu({
				items: [
					{
						text: 'Fill Trades',
						tooltip: 'Create Fills for Selected Trades and their Fill Price & Quantity.',
						iconCls: 'add',
						scope: this,
						handler: function() {
							gp.applyPriceToSelectedTrades(toolBar);
							gp.executeAction('FILL');
						}
					},
					{
						text: 'Fill & Execute Trades',
						tooltip: 'Create Fills for Selected Trades and their Fill Price & Quantity. Execute Trades after fills are created.',
						iconCls: 'add',
						scope: this,
						handler: function() {
							gp.applyPriceToSelectedTrades(toolBar);
							gp.executeAction('FILL_EXECUTE');
						}
					}
				]
			})
		});
	},

	addToolbarMoreActionsButton: function(toolBar) {
		const gp = this;
		toolBar.add('-');
		const moreActionsMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'Validate Trades',
					iconCls: 'row_reconciled',
					tooltip: 'Validate Selected Trades',
					scope: this,
					handler: function() {
						gp.executeAction('VALIDATE');
					}

				},
				{
					text: 'Import Fills',
					iconCls: 'import',
					tooltip: 'Import Trade Fills for Existing Trades',
					handler: function() {
						TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
					}
				},
				{
					text: 'Execute Trades',
					tooltip: 'Execute selected filled Trades.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gp.executeAction('EXECUTE');
					}
				}, '-',
				{
					text: 'Book & Post',
					tooltip: 'Book & Post all selected Trades.',
					iconCls: 'book-red',
					scope: this,
					handler: function() {
						gp.executeAction('BOOK');
					}
				},
				{
					text: 'Unbook',
					tooltip: 'Unbook Trades for Selected Booked Trade(s) below.',
					iconCls: 'undo',
					scope: this,
					handler: function() {
						gp.executeAction('UNBOOK');
					}
				}, '-',
				{
					text: 'Convert To Block',
					tooltip: 'Convert the trades of the selected roll(s) to block trades. The trades of the selected roll(s) will need to be rejected to allow editing.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gp.applyTradeBlockToSelectedTrades(true);
						gp.executeAction('SAVE_TRADE');
					}
				},
				{
					text: 'Convert From Block',
					tooltip: 'Convert the trades of the selected roll(s) from block trades to normal trades. The trades of the selected roll(s) will need to be rejected to allow editing.',
					iconCls: 'undo',
					scope: this,
					handler: function() {
						gp.applyTradeBlockToSelectedTrades(false);
						gp.executeAction('SAVE_TRADE');
					}
				}, '-',
				{
					text: 'Reject Trades',
					tooltip: 'Reject Selected Trade(s) To Edit.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gp.executeAction('REJECT');
					}
				},
				{
					text: 'Remove Commissions',
					tooltip: 'Remove the Commission Override for Selected Trade(s).',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						gp.applyCommissionOverrideToSelectedTrades('');
						gp.executeAction('SAVE_TRADE');
					}
				},
				{
					text: 'Delete Fills',
					tooltip: 'Delete all Fills for Selected Trade(s).',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						gp.executeAction('DELETE_FILLS');
					}
				},
				{
					text: 'Cancel Trades',
					tooltip: 'Cancel Selected Trades',
					iconCls: 'cancel',
					scope: this,
					handler: function() {
						gp.executeAction('CANCEL');
					}
				}
			]
		});

		toolBar.add({
			text: 'More Actions',
			iconCls: 'workflow',
			menu: moreActionsMenu
		});
	},

	columnsConfig: [
		{
			width: 30, dataIndex: 'includeTrade', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false,
			menuDisabled: true,
			header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', ''),
			renderer: function(v, p, record) {
				p.css += ' x-grid3-check-col-td';
				if (v === '') { // CHANGE: added to default to checked
					record.data['includeTrade'] = false;
					v = false;
				}
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
			}
		},
		{
			header: 'Workflow State', width: 100, dataIndex: 'workflowState.name',
			renderer: function(value, metaData, r) {
				if (value === 'Invalid') {
					metaData.css = 'ruleViolation';
				}
				return value;
			}
		},
		{header: 'Client Account', width: 220, dataIndex: 'clientInvestmentAccount.label'},
		{header: 'Holding Account', width: 230, dataIndex: 'holdingInvestmentAccount.label'},
		{header: 'Description', width: 100, dataIndex: 'tradeAllocationDescription', hidden: true},
		{
			header: 'Buy/Sell', width: 60, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json', loadAll: true}, viewNames: ['Export Friendly'],
			renderer: function(v, metaData) {
				if (TCG.isNotBlank(v)) {
					metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				}
				return v;
			}
		},
		{header: 'Quantity', width: 75, dataIndex: 'quantityIntended', type: 'float', useNull: true, summaryType: 'sum'},
		{header: 'Security', width: 150, dataIndex: 'investmentSecurity.symbol'},
		{header: 'Security Name', width: 150, dataIndex: 'investmentSecurity.name', hidden: true},
		{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
		{header: 'Avg Price', width: 85, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Fill Price', width: 80, dataIndex: 'fillPrice', type: 'float', useNull: true, editor: {xtype: 'pricefield', allowBlank: true, minValue: 0}, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;'},
		{
			header: 'Fill Qty', width: 75, dataIndex: 'fillQuantity', type: 'float', useNull: true, editor: {xtype: 'floatfield', allowBlank: true, minValue: 0}, summaryType: 'sum', css: 'BORDER-LEFT: #c0c0c0 1px solid; BORDER-RIGHT: #c0c0c0 1px solid;',
			renderer: function(value, metaData, r) {
				const qty = r.data['quantityIntended'];
				if (qty < value) {
					metaData.css = 'ruleViolation';
					metaData.attr = 'qtip="Fill Quantity entered exceeds quantity on the trade.  Please enter a value no greater than ' + Ext.util.Format.number(qty, '0,000') + ' contracts."';
				}
				return TCG.renderAdjustedAmount(value, (qty !== value), qty, '0,000');
			}
		},
		{header: 'Commission Per Unit', width: 50, dataIndex: 'commissionPerUnit', type: 'float', useNull: true, hidden: true},
		{header: 'Fee', width: 50, dataIndex: 'feeAmount', type: 'float', hidden: true},
		{header: 'Accounting Notional', width: 120, dataIndex: 'accountingNotional', type: 'currency', hidden: true},
		{header: 'Team', width: 70, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true},
		{header: 'Trader', width: 75, dataIndex: 'traderUser.label', hidden: true},
		{header: 'FIX', width: 35, dataIndex: 'fixTrade', type: 'boolean', hidden: true},
		{header: 'Block', width: 50, dataIndex: 'blockTrade', type: 'boolean', hidden: true},
		{header: 'Traded On', width: 70, dataIndex: 'tradeDate', hidden: true},
		{header: 'Settled On', width: 70, dataIndex: 'settlementDate', hidden: true},
		{header: 'Executing Broker', width: 120, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', hidden: true},
		{header: 'Trade Destination', width: 70, dataIndex: 'tradeDestination.name', idDataIndex: 'tradeDestination.id', hidden: true}
	],
	allRows: false,
	toggleAll: function() {
		this.allRows = (this.allRows !== true);
		const grid = this;
		grid.getColumnModel().setColumnHeader(0, String.format('<div class="x-grid3-check-col{0}">&#160;</div>', this.allRows ? '-on' : ''));
		let i = 0;
		let row = grid.store.data.items[i];
		while (row) {
			row.set('includeTrade', this.allRows);
			row = grid.store.data.items[i++];
		}
		this.markModified();
	},
	doNotSubmitFields: ['includeTrade', 'workflowState.name', 'clientInvestmentAccount.label', 'holdingInvestmentAccount.label', 'tradeAllocationDescription', 'buy', 'quantityIntended', 'investmentSecurity.symbol', 'investmentSecurity.name', 'exchangeRateToBase', 'averageUnitPrice', 'feeAmount', 'accountingNotional', 'clientInvestmentAccount.teamSecurityGroup.name', 'traderUser.label', 'fixTrade', 'tradeDate', 'settlementDate', 'order'],
	markModified: function(initOnly) {
		const result = [];
		this.store.each(function(record) {
			if (record.modified && record.modified['includeTrade'] === true) {
				record.markDirty(); // new records should be fully submitted

				const r = {
					id: record.id,
					'class': this.dtoClass
				};
				for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
					const f = this.alwaysSubmitFields[i];
					const val = TCG.getValue(f, record.json);
					if (TCG.isNotNull(val)) {
						let v = TCG.getValue(f, record.json);
						if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
							v = TCG.renderDate(v);
						}
						r[f] = v;
					}
				}
				// copy all modified fields
				for (const m in record.modified) {
					if (record.modified.hasOwnProperty(m)) {
						const field = record.fields.get(m);
						// skip 'text' field submission when there's 'value'
						if ((!field || !field.valueFieldName) && this.doNotSubmitFields.indexOf(m) === -1) {
							let v = record.modified[m];
							let skip = false;
							if (typeof v == 'object') {
								if (TCG.isNull(v)) {
									skip = true;
								}
								else if (Ext.isDate(v)) {
									v = v.dateFormat('m/d/Y');
								}
							}
							if (!skip) {
								r[m] = v;
							}
						}
					}
				}
				result.push(r);
			}
		}, this);
		const value = Ext.util.JSON.encode(result);
		this.submitField.setValue(value); // changed value
		if (initOnly) {
			this.submitField.originalValue = value; // initial value
		}
		else {
			this.getWindow().setModified(true);
		}
	},

	reload: function() {
		const gp = this;
		const fp = TCG.getParentFormPanel(this);
		const url = fp.getLoadURL();
		const win = fp.getWindow();
		fp.load({
			url: encodeURI(url),
			params: fp.getLoadParams(win),
			waitMsg: 'Loading...',
			success: function(form, action) {
				fp.loadJsonResult(action.result, true);
				const data = fp.getForm().formValues;
				gp.store.loadData(data);
				gp.markModified(!gp.actionCallback);
			}
		});
	},

	resetCallback: function() {
		const grid = this;
		grid.actionCallback = undefined;
		grid.actionCallbackSelectionValue = undefined;
		grid.actionCallbackResultAppender = undefined;
	},

	exportGrid: function(outputFormat) {
		const formPanel = TCG.getParentFormPanel(this);
		const params = {
			tradeGroupId: this.getWindow().getMainFormId()
		};
		params.columns = this.getColumnsMetaData();
		params.outputFormat = outputFormat;
		// filename will be of format: (PortfolioRun|MultiClientTrade)_Trades_03082018.xsls
		params.fileName = formPanel.getFormValue('tradeGroupType.name').replace(/\s|-/g, '')
			+ '_Trades_' + new Date(formPanel.getFormValue('tradeDate')).format('mdY');
		TCG.downloadFile('tradeListFind.json', params, this);
	},

	getColumnsMetaData: function() {
		return 'Client Account:clientInvestmentAccount.label|'
			+ 'Holding Account:holdingInvestmentAccount.label|'
			+ 'Holding Company:holdingInvestmentAccount.issuingCompany.name|'
			+ 'Executing Broker:executingBrokerCompany.name|'
			+ 'Security:investmentSecurity.symbol|'
			+ 'Buy/Sell:buy|'
			+ 'Quantity:quantityIntended:align=right';
	},

	listeners: {
		afterRender: function() {
			const grid = this;
			const fp = TCG.getParentFormPanel(this);
			fp.on('afterload', function() {
				const actionExecuted = fp.getForm().formValues['tradeGroupAction'];
				const reloadFunction = function() {
					// reset callback items - all callbacks have been executed
					grid.resetCallback();
					if (grid.getWindow().savedSinceOpen === true) {
						grid.reload();
					}
				};
				if (actionExecuted && actionExecuted !== '') {
					let result = grid.actionCallbackResultAppender;
					let actionResult = fp.getForm().formValues['tradeGroupActionResult'];
					if (actionResult) {
						actionResult = actionExecuted + ':\n' + actionResult;
						result = result ? result + '\n\n' + actionResult : actionResult;
					}
					if (grid.actionCallback) {
						grid.actionCallbackResultAppender = result;
						const callback = grid.actionCallback;
						grid.actionCallback = undefined;
						callback.call(grid);
					}
					else {
						if (result) {
							const resultWindow = new TCG.app.Window({
								title: 'Trade Group Action Results',
								iconCls: 'run',
								width: 500,
								height: 300,
								modal: true,
								minimizable: false,
								maximizable: false,
								resizable: false,
								border: true,
								bodyStyle: 'padding: 20px;',
								openerCt: this,

								items: [
									{xtype: 'textarea', value: result, readOnly: true}
								],
								buttons: [
									{
										text: 'OK',
										tooltip: 'Closes this window and reloads the grid to get the latest state of the current Trade Group.',
										width: 120,
										handler: function() {
											resultWindow.win.closeWindow();
											reloadFunction();
										}
									}
								],
								defaultButton: 0,
								windowOnShow: function() {
									this.focus(); // allow keyboard navigation selection: unfortunately visual highlight only happens after the user hits tab once (anywhere/anytime after our app is loaded)
								}
							});
						}
						else {
							/*
							 * Reload if the tradeGroupActionResult is not present. It was successful, but the server probably had no work to do for the provided request.
							 * Examples: DELETE_FILLS for Trade(s) with no fills.
							 */
							reloadFunction();
						}
					}
				}
				else {
					reloadFunction();
				}
			}, this);

			// add function to clear callback upon form submission failure
			fp.getForm().on('actionfailed', function() {
				// reset callback items on failed request
				this.resetCallback();
			}, grid);
		},
		'headerclick': function(grid, col, e) {
			if (col === 0) {
				grid.toggleAll();
			}
		}
	}
});
Ext.reg('trade-group-formgrid-scroll', Clifton.trade.group.DefaultTradeGroupGridPanel);

Clifton.trade.group.TradeGroupTradeUpdateWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Trade Group Trade Update',
	iconCls: 'add',
	height: 375,
	width: 450,
	modal: true,
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify values for updating the selected Trades of this Trade Group by changing the Executing Broker and/or setting Commission Per Unit amount. Use the Workflow actions to assist in transitioning the selected Trade(s) to allow modification.',
			items: [
				{
					xtype: 'panel',
					layout: 'form',
					labelWidth: 150,
					items: [
						{name: 'tradeDate', xtype: 'datefield', hidden: true},
						{fieldLabel: 'Trade Type', name: 'tradeType.name', xtype: 'displayfield'},
						{xtype: 'sectionheaderfield', header: 'Trade Execution'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false, width: 225,
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									qEvent.combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: f.findField('tradeType.name').getValue()
									};
								},
								select: function(combo, record, index) {
									// Clear trade destination when executing broker is selected
									const f = combo.getParentForm().getForm();
									const eb = f.findField('executingBrokerCompany.label');
									eb.clearAndReset();
								}
							}
						},
						{
							fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', width: 225,
							displayField: 'label',
							requiredFields: ['tradeDestination.id'],
							url: 'tradeExecutingBrokerCompanyListFind.json',
							queryParam: 'executingBrokerCompanyName',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();

									combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeDestinationId: f.findField('tradeDestination.id').getValue(),
										tradeTypeNameEquals: f.findField('tradeType.name').getValue()
									};
								}
							}
						},
						{xtype: 'sectionheaderfield', header: 'Trade Commission'},
						{fieldLabel: 'Commission Per Unit', name: 'commissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit Override amount for all selected Trades. Leave ths value blank to avoid modifying the existing commission(s).'},
						{
							name: 'removeCommission', xtype: 'checkbox', checked: false, boxLabel: 'Remove Commission Override',
							qtip: 'Removes the Commission Per Unit Override from the selected Trade(s).'
						},
						{xtype: 'sectionheaderfield', header: 'Trade Workflow Actions'},
						{
							name: 'rejectTradesForEdit', xtype: 'checkbox', checked: false, boxLabel: 'Reject Trades Before Edits',
							qtip: 'Perform the REJECT action on the selected Trade(s) to enable edits before trying to set the new values.',
							listeners: {
								check: function(checkbox, checked) {
									const fp = TCG.getParentFormPanel(checkbox);
									fp.getForm().findField('validateTradesAfterEdit').setValue(checked);
								}
							}
						},
						{
							name: 'validateTradesAfterEdit', xtype: 'checkbox', checked: false, boxLabel: 'Validate Trades After Edits',
							qtip: 'Perform the VALIDATE action on the selected Trade(s) after the new values have been saved.'
						}
					]
				}
			]
		}
	],

	saveWindow: function() {
		const panel = this.items.get(0);
		const form = panel.getForm();
		if (this.openerCt && this.openerCt.applyModalTradeUpdates) {
			this.openerCt.applyModalTradeUpdates.call(this.openerCt, form.getValues());
			this.close();
		}
		else {
			TCG.showError('Opener component must be defined and must have applyModalTradeUpdates(params) method.', 'Callback Missing');
		}
	}
});

Clifton.trade.addTradeNote = function(rows, editor, gridPanel, tradeIdFieldName, tradeTypeFieldName, verifyTradeType, maxTradeCount) {
	if (maxTradeCount) {
		if (rows.length > maxTradeCount) {
			TCG.showError('You have selected ' + rows.length + ' trades, which is greater than the maximum allowed of ' + maxTradeCount + '.  Please limit your selection.  If the trades all belong to a trade group, please open the trade group window and add your note there.');
			return;
		}
	}
	let className = 'Clifton.system.note.SingleNoteWindow';
	const tbl = TCG.data.getData('systemTableByName.json?tableName=Trade', gridPanel, 'system.table.Trade');
	let dd = {
		noteType: {table: tbl}
	};

	const tradeIds = [];

	// Per Eric - to prevent someone from attaching notes incorrectly across all
	// or attaching to wrong trades add check that they all belong to the same trade type.
	let tradeType = undefined;
	for (let i = 0; i < rows.length; i++) {
		const rec = rows[i].json;
		let tradeId = TCG.getValue(tradeIdFieldName, rec);
		if (i === 0) {
			tradeType = TCG.getValue(tradeTypeFieldName, rec);
		}
		else {
			for (let j = 0; j < tradeIds.length; j++) {
				if (tradeIds[j] === tradeId) {
					// Already In the List, Don't add it again
					tradeId = undefined;
					break;
				}
			}
			if (verifyTradeType === true && tradeId && tradeType !== TCG.getValue(tradeTypeFieldName, rec)) {
				TCG.showError('When creating linked notes across trades, they all must be for the same trade type.  Found at least 2 different [' + tradeType + ', ' + TCG.getValue(tradeTypeFieldName, rec) + '].');
				return;
			}
		}
		if (tradeId) {
			tradeIds.push(tradeId);
		}
	}
	if (tradeIds.length === 1) {
		dd = Ext.apply(dd, {linkedToMultiple: false, fkFieldId: tradeIds[0]});
	}
	else {
		className = 'Clifton.system.note.LinkedNoteWindow';
		dd = Ext.apply(dd, {linkedToMultiple: true, fkFieldIds: tradeIds});
	}
	const cmpId = TCG.getComponentId(className);
	TCG.createComponent(className, {
		id: cmpId,
		defaultData: dd,
		openerCt: gridPanel
	});
};

/*
 * Base form panel for TradeGroupDynamic Vitual DTOs. The form layout is designed to have
 * a header for TradeGroup properties followed by a grid panel for the detail items. There
 * are hooks to differ views for entry and review.
 */
Clifton.trade.group.BaseTradeGroupDynamicFormPanel = Ext.extend(TCG.form.FormPanel, {
	url: 'tradeGroupDynamic.json',
	dtoClassForBinding: 'TRADE-GROUP-DYNAMIC-CLASS',
	loadValidation: false,
	readOnly: false,

	listRequestedProperties: 'clientAccount.id|clientAccount.label|holdingAccount.id|holdingAccount.number|',
	additionalListRequestedProperties: '',
	listRequestedPropertiesRoot: 'data.detailList',
	requestedMaxDepth: 5,
	enableOpenSessionInView: true,
	requestedPropertiesToExclude: '',

	appendBindingParametersToUrl: function(url) {
		let resultUrl = url + ((url.indexOf('?') > 0) ? '&' : '?') + 'enableOpenSessionInView=' + this.enableOpenSessionInView + '&requestedMaxDepth=' + this.requestedMaxDepth + '&enableValidatingBinding=true&dtoClassForBinding=' + this.dtoClassForBinding;
		if (TCG.isNotBlank(this.requestedPropertiesToExclude)) {
			resultUrl += '&requestedPropertiesToExcludeGlobally=' + this.requestedPropertiesToExclude;
		}
		return resultUrl;
	},

	getLoadURL: function() {
		const w = this.getWindow();
		if (this.url && ((w && w.params) || (this.getIdFieldValue()))) {
			return this.appendBindingParametersToUrl(this.url);
		}
		return false;
	},

	getLoadParams: function(win) {
		const panel = this;
		const form = panel.getForm();
		const params = Ext.apply(win.params ? win.params : {}, form.getValues());
		if (!params['id'] && panel.getIdFieldValue()) {
			params['id'] = panel.getIdFieldValue();
		}
		params.requestedProperties = this.listRequestedProperties + this.additionalListRequestedProperties;
		params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
		return params;
	},

	getSaveURL: function() {
		const tradeGroupAction = this.getFormValue('tradeGroupAction');
		if (TCG.isBlank(tradeGroupAction) === false) {
			return this.appendBindingParametersToUrl('tradeGroupDynamicActionExecute.json');
		}
		if (this.getWindow().isModified()) {
			return this.appendBindingParametersToUrl('tradeGroupDynamicEntrySave.json');
		}
		return false;
	},

	getSubmitParams: function() {
		return {
			requestedProperties: this.listRequestedProperties + this.additionalListRequestedProperties,
			requestedPropertiesRoot: this.listRequestedPropertiesRoot
		};
	},

	getDefaultData: function(win) {
		const dd = win.defaultData || {};
		if (win.defaultDataIsReal) {
			return dd;
		}
		return this.applyCustomDefaultData(dd);
	},

	applyCustomDefaultData: function(defaultData) {
		return defaultData;
	},

	defaults: {
		anchor: '0'
	},

	readOnlyMode: undefined,
	setReadOnlyMode: function(ro) {
		if (this.readOnlyMode === ro) {
			return;
		}
		this.readOnlyMode = ro;
		this.removeAll(true);
		if (ro === true) {
			this.add(this.readOnlyCommonItems);
			this.add(this.readOnlyItems);
			this.add(this.readOnlyGridItem);
		}
		else {
			this.add(this.tradeEntryCommonItems);
			this.add(this.tradeEntryItems);
			this.add(this.tradeEntryGridItem);
		}
		try {
			this.doLayout();
		}
		catch (e) {
			// strange error due to removal of elements with allowBlank = false
		}
		const f = this.getForm();
		f.setValues(f.formValues);
		this.loadValidationMetaData(true);
		f.isValid();
	},

	listeners: {
		afterrender: function() {
			const id = this.getIdFieldValue();
			let readOnly = TCG.isNotNull(id);
			if (readOnly) {
				readOnly = (this.getLoadURL() !== false);
			}
			this.setReadOnlyMode(readOnly);
		},
		afterload: function() {
			const actionField = this.getForm().findField('tradeGroupAction');
			if (actionField) {
				// since we load the response data, make sure to clear action
				actionField.setValue('');
			}
		},
		aftercreate: function() {
			const id = this.getIdFieldValue();
			this.setReadOnlyMode(TCG.isNotNull(id));
			// reload the grid after create
			this.reloadChildGrid(this);
		}
	},

	// Hook for child implementions to reload a Grid on the panel
	reloadChildGrid: function(formpanel) {
		// empty
	},

	tradeEntryCommonItems: [
		{name: 'tradeGroupType.id', xtype: 'hidden'},
		{name: 'tradeType.id', xtype: 'hidden'},
		{name: 'tradeType.name', submitValue: false, xtype: 'hidden'}
	],

	// Hook for child implementations to add header fields to the panel for entry
	tradeEntryItems: [
		// empty
	],

	// Hook for child implementations to add a Grid for entry to the panel
	// See Clifton.trade.group.BaseTradeGroupDynamicDetailFormGrid
	tradeEntryGridItem: [
		// empty
	],

	readOnlyCommonItems: [
		{name: 'tradeGroupType.id', xtype: 'hidden'},
		//Need to add id as a hidden field and submitValue = false so that id is not double submitted
		{name: 'id', xtype: 'hidden', submitValue: false},
		{name: 'tradeGroupAction', xtype: 'hidden'}
	],

	// Hook for child implementations to add header fields to the panel for review
	readOnlyItems: [
		// empty
	],

	// Hook for child implementations to add a Grid for review to the panel
	// See Clifton.trade.group.BaseTradeGroupDynamicDetailFormGrid
	readOnlyGridItem: [
		// empty
	]
});
Ext.reg('trade-group-dynamic-form-panel', Clifton.trade.group.BaseTradeGroupDynamicFormPanel);

/*
 * Base grid form panel for TradeGroupDynamic Vitual DTOs' detail items. There
 * are hooks to modify toolbars, columns, and actions for entry and review.
 */
Clifton.trade.group.BaseTradeGroupDynamicDetailGrid = Ext.extend(TCG.grid.FormGridPanel, {

	// Used to switch the detail grid from allowing trade entry vs. review of trades
	tradeEntrySupported: false,

	name: 'TRADE-GROUP-DYNAMIC-DETAIL-FORM-GRID-NAME',
	dtoClass: 'TRADE-GROUP-DYNAMIC-DETAIL-DTO',
	instructions: 'DESCRIPTION',
	storeRoot: 'detailList',
	readOnly: true,
	// when populating submit values for the detail rows (markModified), some uses have child collections that need to be grouped
	// as an array to correctly map objects on the server side data objects
	// Example: rollTradeList[0].id: X from the column data index will become rollTradeList: [{class: 'detailChildListClass['rollTradeList']', id: X}]
	processDetailChildList: false,
	detailChildListClassDictionary: {},
	preventReloadOnInvalidForm: false, // if true, will prevent reloading the grid if the form has an invalid field

	initComponent: function() {
		const cols = [];
		const tradeEntry = this.tradeEntrySupported;
		const groupField = this.groupField;
		Ext.each(this.commonColumns, function(f) {
			if (TCG.isNull(f.tradeEntry) || f.tradeEntry === tradeEntry) {
				if (groupField && (TCG.isBlank(groupField) === false)) {
					f.hidden = f.dataIndex === groupField;
				}
				cols.push(f);
			}
		});
		Ext.each(this.customColumns, function(f) {
			if (TCG.isNull(f.tradeEntry) || f.tradeEntry === tradeEntry) {
				cols.push(f);
			}
		});
		this.columnsConfig = cols;

		this.viewConfig = this.viewConfig || {};
		this.viewConfig.emptyText = TCG.isTrue(this.tradeEntrySupported) ? 'No positions found for the specified criteria' : 'All trades have been removed or cancelled';

		TCG.grid.FormGridPanel.prototype.initComponent.call(this, arguments);
	},

	commonColumns: [
		{
			width: 30, dataIndex: 'includeTrade', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false, menuDisabled: true,
			header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', ''),
			renderer: function(v, p, record) {
				p.css += ' x-grid3-check-col-td';
				if (v === '') {
					record.data['includeTrade'] = false;
					v = false;
				}
				return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
			}
		},
		{header: 'Client Account', dataIndex: 'clientAccount.label', hidden: false, submitValue: false, width: 190},
		{header: 'Holding Account', dataIndex: 'holdingAccount.number', hidden: false, submitValue: false, width: 95}
	],

	// Columns that have tradeEntry = true/false will be included based on if this is tradeEntry or tradeReview - anything without that property will always be included
	customColumns: [
		//empty
	],

	addToolbarButtons: function(toolBar) {
		const gp = this;
		if (this.tradeEntrySupported === true) {
			gp.addEntryToolbarButtons(toolBar, gp);
		}
		else {
			gp.addReviewToolbarButtons(toolBar, gp);
		}
		toolBar.doLayout();
	},

	addEntryToolbarButtons: function(toolBar, gridpanel) {
		// empty
	},

	addReviewToolbarButtons: function(toolBar, gridpanel) {
		const gp = gridpanel;
		toolBar.add({
			text: 'Approve Trades',
			tooltip: 'Approve Trades.',
			iconCls: 'verify',
			scope: this,
			handler: function() {
				gp.executeAction('APPROVE');
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Reload',
			tooltip: 'Reload trades for this trade group.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.reloadGrid();
			}
		});
		toolBar.add('-');
		this.addAdditionalReviewToolbarButtonsLeftSide(gp, toolBar);

		const exportMenu = new Ext.menu.Menu();
		exportMenu.add({
			text: 'Trade Allocation',
			tooltip: 'View trades of this trade group by executing broker for viewing and sending trade allocations',
			iconCls: 'export',
			handler: function() {
				const fp = TCG.getParentFormPanel(gp);
				const form = fp.getForm();
				TCG.createComponent('Clifton.trade.TradeAllocationWindow', {defaultData: {tradeGroupId: TCG.getValue('id', form.formValues),
						shouldConsolidateDataRows: fp.shouldConsolidateDataRows, shouldInsertTotalRows: fp.shouldInsertTotalRows}});
			}
		});
		toolBar.add({
			text: 'Tools',
			iconCls: 'config',
			menu: exportMenu
		}, '-');

		toolBar.add('->');
		this.addAdditionalReviewToolbarButtonsRightSide(gp, toolBar);
		toolBar.add('-');
		const moreActionsMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'Validate Trades',
					iconCls: 'row_reconciled',
					tooltip: 'Validate Selected Trades',
					scope: this,
					handler: function() {
						gp.executeAction('VALIDATE');
					}
				},
				{
					text: 'Import Fills',
					iconCls: 'import',
					tooltip: 'Import Trade Fills for Existing Trades',
					handler: function() {
						TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
					}
				},
				{
					text: 'Execute Trades',
					tooltip: 'Execute selected filled Trades.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gp.executeAction('EXECUTE');
					}
				}, '-',
				{
					text: 'Book & Post',
					tooltip: 'Book & Post all selected Trades.',
					iconCls: 'book-red',
					scope: this,
					handler: function() {
						gp.executeAction('BOOK');
					}
				},
				{
					text: 'Unbook',
					tooltip: 'Unbook Trades for Selected Booked Trade(s) below.',
					iconCls: 'undo',
					scope: this,
					handler: function() {
						gp.executeAction('UNBOOK');
					}
				}, '-',
				{
					text: 'Reject Trades',
					tooltip: 'Reject Selected Trade(s) To Edit.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gp.executeAction('REJECT');
					}
				},
				{
					text: 'Delete Fills',
					tooltip: 'Delete all Fills for Selected Trade(s).',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						gp.executeAction('DELETE_FILLS');
					}
				},
				{
					text: 'Cancel Trades',
					tooltip: 'Cancel Selected Trades',
					iconCls: 'cancel',
					scope: this,
					handler: function() {
						gp.executeAction('CANCEL');
					}
				}
			]
		});
		toolBar.add({
			text: 'More Actions',
			iconCls: 'workflow',
			menu: moreActionsMenu
		});
	},

	addAdditionalReviewToolbarButtonsLeftSide: function(gridPanel, toolbar) {
		// nothing - can be overrideen
	},

	addAdditionalReviewToolbarButtonsRightSide: function(gridPanel, toolbar) {
		// nothing - can be overrideen
	},

	listeners: {
		// drill into specific trade or run
		'celldblclick': function(grid, rowIndex, cellIndex, evt) {
			this.handleCellDoubleClick(grid, rowIndex, cellIndex, evt);
		},
		'headerclick': function(grid, col, e) {
			if (col === grid.getColumnModel().findColumnIndex('includeTrade')) {
				grid.toggleAll();
			}
		},
		afterload: function() {
			this.handleAfterLoadTransientRecordValues(this);
		},
		afteredit: function(gridOrEvent, rowIndex, columnIndex) {
			/*
			 * If this event is fired by us, there are three arguments: grid, rowIndex, and columnIndex.
			 * If this event is fired by Ext JS, there is one event argument that contains: column, field, grid, originalValue, record, row, and value.
			 */
			if (arguments.length === 1) {
				this.handleAfterEditEventFromExt(gridOrEvent);
			}
			else {
				this.handleAfterEditEventFromTCG(gridOrEvent, rowIndex, columnIndex);
			}
		}
	},

	// Hook to handle a Cell double-click event
	handleCellDoubleClick: function(grid, rowIndex, cellIndex, event) {
		// empty
	},

	// Hook to handle a record modifications after the grid is loaded.
	handleAfterLoadTransientRecordValues: function(grid) {
		// empty
	},

	// Hook to handle a Cell modification triggered by IMS UX Ext Extensions
	handleAfterEditEventFromTCG: function(grid, rowIndex, columnIndex) {
		// empty
	},

	// Hook to handle a Cell modification triggered by Ext framework
	// Event argument that contains: column, field, grid, originalValue, record, row, and value
	handleAfterEditEventFromExt: function(event) {
		// empty
	},

	reloadToolbarMenus: function() {
		const grid = this;
		const toolbar = grid.getTopToolbar();
		toolbar.removeAll();
		grid.addToolbarButtons(toolbar);
	},

	hasBeenApproved: function() {
		const panel = TCG.getParentFormPanel(this);
		const status = panel.getFormValue('workflowStatus.name');
		return (!TCG.isBlank(status) && (status === 'Approved' || status === 'Active'));
	},

	reloadGrid: function() {
		const grid = this;
		const panel = TCG.getParentFormPanel(this);

		if (this.preventReloadOnInvalidForm === true) {
			const invalidField = panel.getFirstInValidField();
			// Guard-clauses
			if (!TCG.isNull(invalidField)) {
				TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
				return;
			}
		}

		let url = panel.getLoadURL();
		if (url === false) {
			url = panel.appendBindingParametersToUrl('tradeGroupDynamicEntry.json');
		}
		const win = panel.getWindow();
		panel.load(Ext.applyIf({
			url: encodeURI(url),
			params: panel.getLoadParams(win),
			waitMsg: 'Loading...',
			success: function(form, action) {
				panel.loadJsonResult(action.result, true);
				const data = panel.getForm().formValues;
				grid.store.loadData(data);
				grid.handleAfterLoadTransientRecordValues(grid);
				grid.markModified(true);
				grid.reloadToolbarMenus();
				// Special Event that aftercreate listener adds a listener to to clear trades
				grid.fireEvent('afterReloadGrid', grid);
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	executeAction: function(groupAction) {
		if (this.submitField.getValue() === '[]') {
			TCG.showError('No row selected.  Please select at least one row from the list.');
			return;
		}
		const panel = TCG.getParentFormPanel(this);
		if (groupAction) {
			panel.setFormValue('tradeGroupAction', groupAction);
		}
		if (this.getWindow().isModified()) {
			this.getWindow().saveWindow(false);
		}
	},

	// Defaults to unchecked
	allRows: false,
	toggleAll: function() {
		this.allRows = (this.allRows !== true);
		const grid = this;
		const index = grid.getColumnModel().findColumnIndex('includeTrade');
		grid.getColumnModel().setColumnHeader(index, String.format('<div class="x-grid3-check-col{0}">&#160;</div>', this.allRows ? '-on' : ''));
		let i = 0;
		let row = grid.store.data.items[i];
		while (row) {
			row.set('includeTrade', this.allRows);
			grid.fireEvent('afteredit', grid, i, index);
			row = grid.store.data.items[i++];
		}
		this.markModified();
	},

	markModified: function(initOnly) {
		const result = [];
		let newId = -10;
		this.store.each(function(record) {
			if (record.modified && record.modified['includeTrade'] === true) {
				record.markDirty(); // new records should be fully submitted

				if (!Ext.isNumber(record.id)) {
					record.id = newId--;
				}
				const r = {
					id: record.id,
					'class': this.dtoClass
				};
				for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
					const f = this.alwaysSubmitFields[i];
					let val = TCG.getValue(f, record.json);
					if (TCG.isNotNull(val)) {
						if (typeof val == 'number' && isNaN(val)) {
							val = '';
						}
						else if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
							val = TCG.renderDate(val);
						}
						r[f] = val;
					}
				}
				if (record.dirty) {
					// copy all modified fields
					for (const m in record.modified) {
						if (record.modified.hasOwnProperty(m)) {
							const field = record.fields.get(m);
							// skip 'text' field submission when there's 'value'
							if ((!field || !field.valueFieldName) && this.doNotSubmitFields.indexOf(m) === -1) {
								let v = record.modified[m];
								let skip = false;
								if (typeof v == 'object') {
									if (TCG.isNull(v)) {
										skip = true;
									}
									else if (Ext.isDate(v)) {
										v = v.dateFormat('m/d/Y');
									}
								}
								else if (typeof v == 'number' && isNaN(v)) {
									v = '';
								}
								if (!skip) {
									r[m] = v;
								}
							}
						}
					}
				}

				if (TCG.isTrue(this.processDetailChildList)) {
					/* Convert any array column fields to a JSON array for submission
					 *
					 * First create a map of property name -> map of object index -> object
					 *   e.g. tradeList -> {0 -> {id: X, quantityIntended: XXX}, 1 -> {id: X+1}}
					 * Then add the property to the record with the array of objects
					 *   e.g. tradeList -> [{id: X, quantityIntended: XXX},{id: X+1}]
					 */
					// Create property object mapping
					const childCollectionMap = {};
					for (const property in r) {
						if (r.hasOwnProperty(property)) {
							const bracketStartIndex = property.indexOf('[');
							if (bracketStartIndex > -1) {
								// get value and remove property from record
								const value = r[property];
								delete r[property];
								// create or update array object
								const bracketEndIndex = property.indexOf(']'),
									collectionName = property.substr(0, bracketStartIndex),
									objectIndex = property.substr(bracketStartIndex + 1, bracketEndIndex - (bracketStartIndex + 1)),
									objectProperty = property.substr(bracketEndIndex + 2);
								let childCollectionObjectMap = childCollectionMap[collectionName];
								if (TCG.isNull(childCollectionObjectMap)) {
									childCollectionObjectMap = {};
									childCollectionMap[collectionName] = childCollectionObjectMap;
								}
								let childObject = childCollectionObjectMap[objectIndex];
								if (TCG.isNull(childObject)) {
									childObject = {class: (this.detailChildListClassDictionary ? this.detailChildListClassDictionary[collectionName] : '')};
									childCollectionObjectMap[objectIndex] = childObject;
								}
								childObject[objectProperty] = value;
							}
						}
					}
					// Put child object list back into record
					for (const property in childCollectionMap) {
						if (childCollectionMap.hasOwnProperty(property)) {
							const childObjectMap = childCollectionMap[property],
								childList = [];
							for (const childPropertyIndex in childObjectMap) {
								if (childObjectMap.hasOwnProperty(childPropertyIndex)) {
									const childObject = childObjectMap[childPropertyIndex];
									if (Object.keys(childObject).length > 0) {
										childList.push(childObject);
									}
								}
							}
							if (childList.length > 0) {
								r[property] = childList;
							}
						}
					}
				}

				result.push(r);
			}
			else if (record.modified && record.modified['includeTrade'] === false) {
				// record will not be included in trade group, id should be empty so aftercreate method knows record wasn't submitted
				record.id = '';
			}
		}, this);
		const value = Ext.util.JSON.encode(result);
		this.submitField.setValue(value); // changed value
		if (initOnly) {
			this.submitField.originalValue = value; // initial value
		}
		else {
			this.getWindow().setModified(true);
		}
	}
});
Ext.reg('trade-group-dynamic-detail-grid-panel', Clifton.trade.group.BaseTradeGroupDynamicDetailGrid);

/**
 * Base formgrid that contains common functionality for working with Option TradeGroupDynamic groups
 * where their Trades can be strangled to minimize execution costs.
 */
Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleDetailFormGrid = Ext.extend(TCG.grid.FormGridPanel, {
	name: 'TRADE-GROUP-DYNAMIC-DETAIL-FORM-GRID-NAME',
	storeRoot: 'detailList',
	readOnly: true,
	// Used to switch the detail grid from allowing trade entry vs. review of trades
	tradeEntrySupported: false,
	// boolean flag to allow the form to clear the callback in the event one of the previous requests fails.
	applyChainedCallback: false,
	// Callback used to chain Trade Group actions (e.g. strangle -> SAVE_TRADES -> FILL. Do nothing by default.
	chainedRequestCallback: undefined,
	includeSelectionColumn: false,
	useQuantityOnTrade: true, // Override to use putTradeQuantity and callTradeQuantity such as with Option Roll
	emailFileNamePrefix: 'OptionGroup', // Override to define a different email file name prefix such as with Option Roll

	alwaysSubmitFields: ['holdingAccount.id', 'clientAccount.id', 'callTrade.id', 'callTradeQuantity', 'putTrade.id', 'putTradeQuantity', 'callTailTrade.id', 'putTailTrade.id'],

	initComponent: function() {
		const cols = [];
		const tradeEntry = this.tradeEntrySupported;
		const groupField = this.groupField;
		if (this.includeSelectionColumn === true) {
			cols.push(this.selectionColumn);
		}
		Ext.each(this.commonColumns, function(f) {
			if (TCG.isNull(f.tradeEntry) || f.tradeEntry === tradeEntry) {
				if (groupField && (TCG.isBlank(groupField) === false)) {
					f.hidden = f.dataIndex === groupField;
				}
				cols.push(f);
			}
		});
		Ext.each(this.customColumns, function(f) {
			if (TCG.isNull(f.tradeEntry) || f.tradeEntry === tradeEntry) {
				cols.push(f);
			}
		});
		this.columnsConfig = cols;
		TCG.grid.FormGridPanel.prototype.initComponent.call(this, arguments);
	},

	selectionColumn: {
		width: 30, dataIndex: 'includeTrade', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false, menuDisabled: true,
		header: String.format('<div class="x-grid3-check-col{0}">&#160;</div>', ''),
		renderer: function(v, p, record) {
			p.css += ' x-grid3-check-col-td';
			if (v === '') {
				record.data['includeTrade'] = false;
				v = false;
			}
			return String.format('<div class="x-grid3-check-col{0}">&#160;</div>', v ? '-on' : '');
		}
	},

	commonColumns: [
		{header: 'Client Account', dataIndex: 'clientAccount.label', hidden: false, submitValue: false, width: 190},
		{header: 'Holding Account', dataIndex: 'holdingAccount.number', hidden: false, submitValue: false, width: 95}
	],

	// Columns that have tradeEntry = true/false will be included based on if this is tradeEntry or tradeReview - anything without that property will always be included
	customColumns: [
		//empty
	],

	addToolbarButtons: function(toolBar) {
		const gp = this;
		if (this.tradeEntrySupported === true) {
			gp.addEntryToolbarButtons(toolBar, gp);
		}
		else {
			gp.addReviewToolbarButtons(toolBar, gp);
		}
		toolBar.doLayout();
	},

	addEntryToolbarButtons: function(toolBar, gridpanel) {
		// empty
	},

	addReviewToolbarButtons: function(toolBar, gridpanel) {
		gridpanel.addStandardUpdateAndReloadToolbarItems(gridpanel, toolBar);
		toolBar.add('-');
		gridpanel.addPreStrangleToolbarItems(gridpanel, toolBar);
		toolBar.add('->');
		gridpanel.addPostApproveToolbarItems(gridpanel, toolBar);
	},

	addStandardUpdateAndReloadToolbarItems: function(gridPanel, toolBar) {
		toolBar.add({
			text: 'Approve Trades',
			tooltip: 'Approve Trades.',
			iconCls: 'verify',
			scope: this,
			handler: function() {
				gridPanel.executeAction('APPROVE');
			}
		});
		toolBar.add('-');
		if (!TCG.isTrue(gridPanel.hasBeenClosed())) {
			toolBar.add({
				text: 'Update Trades...',
				tooltip: 'Update the Executing Broker, Option Securities, and/or Apply Commission Overrides to the Trades of this Option Group.',
				iconCls: 'add',
				scope: this,
				handler: function() {
					gridPanel.openActionWindow('Clifton.trade.group.TradeGroupDynamicOptionStrangleApplyTradeUpdatesWindow', toolBar, gridPanel);
				}
			});
			toolBar.add('-');
		}
		toolBar.add({
			text: 'Reload',
			tooltip: 'Reload trades for this trade group.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gridPanel.reloadGrid();
			}
		});
	},

	addPreStrangleToolbarItems: function(gridPanel, toolBar) {
		if (!TCG.isTrue(gridPanel.hasBeenClosed()) && !TCG.isTrue(gridPanel.hasBeenStrangled())) {
			const viewStrangleMenu = new Ext.menu.Menu({
				items: [
					{
						xtype: 'menucheckitem',
						group: 'strangleView',
						checked: true,
						text: 'By Account',
						tooltip: 'Strangle Trades By Account.',
						scope: this,
						handler: function() {
							gridPanel.reloadGrid('OPTIONS_BY_ACCOUNT');
						}

					},
					{
						xtype: 'menucheckitem',
						group: 'strangleView',
						text: 'Across Accounts',
						tooltip: 'Strangle Trades Across Accounts.',
						handler: function() {
							gridPanel.reloadGrid('OPTIONS_ACROSS_ACCOUNTS');
						}
					},
					{
						xtype: 'menucheckitem',
						group: 'strangleView',
						text: 'No Tail',
						tooltip: 'Group the Trades as they are.',
						scope: this,
						handler: function() {
							gridPanel.reloadGrid('OPTIONS_NO_TAIL');
						}
					}
				]
			});
			toolBar.add({
				text: 'Preview Strangle',
				iconCls: 'views',
				menu: viewStrangleMenu
			});
			toolBar.add('-');
			toolBar.add({
				text: 'Email Quote',
				tooltip: 'Generates a Trade Quote Email for the current view. The quote will look different depending on which strangle preview is active.',
				iconCls: 'email',
				scope: this,
				handler: function() {
					gridPanel.generateQuoteEmail();
				}
			});
		}

		const exportMenu = new Ext.menu.Menu();
		exportMenu.add({
			text: 'Trade Allocation',
			tooltip: 'View trades of this trade group by executing broker for viewing and sending trade allocations',
			iconCls: 'export',
			handler: function() {
				const fp = TCG.getParentFormPanel(gridPanel);
				const form = fp.getForm();
				TCG.createComponent('Clifton.trade.TradeAllocationWindow', {defaultData: {tradeGroupId: TCG.getValue('id', form.formValues),
						shouldConsolidateDataRows: fp.shouldConsolidateDataRows, shouldInsertTotalRows: fp.shouldInsertTotalRows}});
			}
		});
		toolBar.add({
			text: 'Tools',
			iconCls: 'config',
			menu: exportMenu
		}, '-');
	},

	addPostApproveToolbarItems: function(gridPanel, toolBar) {
		if (TCG.isTrue(gridPanel.hasBeenApproved()) || TCG.isTrue(gridPanel.hasBeenClosed())) {
			if (!TCG.isTrue(gridPanel.hasBeenStrangled()) && !TCG.isTrue(gridPanel.hasBeenClosed())) {
				toolBar.add({
					text: 'Strangle Trades...',
					tooltip: 'Strangle the selected Trades, set the Executing Broker, Apply Commission Overrides, and/or Apply Fills to the Trades of this Option Group.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gridPanel.openActionWindow('Clifton.trade.group.TradeGroupDynamicOptionStranglePerformStrangleTradeWindow', toolBar, gridPanel);
					}
				});
			}
			else {
				toolBar.add({
					text: 'View Allocation',
					tooltip: 'View the Strangled Trade Allocation.',
					iconCls: 'export',
					scope: this,
					handler: function() {
						gridPanel.openActionWindow('Clifton.trade.group.TradeGroupDynamicOptionStrangleTradeAllocationWindow', toolBar, gridPanel);
					}
				});
			}
			toolBar.add('-');
			if (!TCG.isTrue(gridPanel.hasBeenClosed())) {
				toolBar.add({
					text: 'Fill Trades...',
					tooltip: 'Apply Fills to the Trades of this Option Group.',
					iconCls: 'add',
					scope: this,
					handler: function() {
						gridPanel.openActionWindow('Clifton.trade.group.TradeGroupDynamicOptionStrangleApplyTradePricesWindow', toolBar, gridPanel);
					}
				});
				toolBar.add('-');
			}
		}
		const moreActionsMenu = new Ext.menu.Menu({
			items: [
				{
					text: 'Validate Trades',
					iconCls: 'row_reconciled',
					tooltip: 'Validate Selected Trades',
					scope: this,
					handler: function() {
						gridPanel.executeAction('VALIDATE');
					}

				},
				{
					text: 'Import Fills',
					iconCls: 'import',
					tooltip: 'Import Trade Fills for Existing Trades',
					handler: function() {
						TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
					}
				},
				{
					text: 'Execute Trades',
					tooltip: 'Execute selected filled Trades.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gridPanel.executeAction('EXECUTE');
					}
				}, '-',
				{
					text: 'Book & Post',
					tooltip: 'Book & Post all selected Trades.',
					iconCls: 'book-red',
					scope: this,
					handler: function() {
						gridPanel.executeAction('BOOK');
					}
				},
				{
					text: 'Unbook',
					tooltip: 'Unbook Trades for Selected Booked Trade(s) below.',
					iconCls: 'undo',
					scope: this,
					handler: function() {
						gridPanel.executeAction('UNBOOK');
					}
				}, '-',
				{
					text: 'Reject Trades',
					tooltip: 'Reject Selected Trade(s) To Edit.',
					iconCls: 'run',
					scope: this,
					handler: function() {
						gridPanel.executeAction('REJECT');
					}
				},
				{
					text: 'Remove Commissions',
					tooltip: 'Remove the Commission Override for Selected Trade(s).',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						const form = TCG.getParentFormPanel(gridPanel).getForm();
						form.findField('enableCommissionRemoval').setValue(true);
						gridPanel.executeAction('SAVE_TRADE');
					}
				},
				{
					text: 'Delete Fills',
					tooltip: 'Delete all Fills for Selected Trade(s).',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						gridPanel.executeAction('DELETE_FILLS');
					}
				},
				{
					text: 'Cancel Trades',
					tooltip: 'Cancel Selected Trades',
					iconCls: 'cancel',
					scope: this,
					handler: function() {
						gridPanel.executeAction('CANCEL');
					}
				}
			]
		});
		toolBar.add({
			text: 'More Actions',
			iconCls: 'workflow',
			menu: moreActionsMenu
		});
	},

	listeners: {
		'celldblclick': function(grid, rowIndex, cellIndex, evt) {
			const row = grid.store.data.items[rowIndex];
			const columnName = grid.getColumnModel().getColumnById(cellIndex).dataIndex;
			let tid = '';
			if (columnName.indexOf('Tail') > -1) {
				if (columnName && columnName.startsWith('put')) {
					tid = TCG.getValue('putTailTrade.id', row.json);
				}
				else if (columnName && columnName.startsWith('call')) {
					tid = TCG.getValue('callTailTrade.id', row.json);
				}
			}
			else {
				if (columnName && columnName.startsWith('put')) {
					tid = TCG.getValue('putTrade.id', row.json);
				}
				else if (columnName && columnName.startsWith('call')) {
					tid = TCG.getValue('callTrade.id', row.json);
				}
			}
			if (TCG.isNotBlank(tid)) {
				TCG.createComponent('Clifton.trade.TradeWindow', {
					id: TCG.getComponentId('Clifton.trade.TradeWindow', tid),
					params: {id: tid},
					openerCt: grid
				});
			}
		},
		afterload: function(grid) {
			grid.reloadToolbarMenus();
			grid.markModified(true);
			const panel = TCG.getParentFormPanel(grid);
			const form = panel.getForm();
			const actionField = form.findField('tradeGroupAction');
			if (actionField) {
				// since we load the response data, make sure to clear action
				actionField.setValue('');
			}
			const strangleMethodField = form.findField('strangleCalculatorMethod');
			if (strangleMethodField) {
				// since we load the response data, make sure to clear strangle method
				strangleMethodField.setValue('');
			}
			if (grid.chainedRequestCallback !== undefined) {
				if (grid.applyChainedCallback === true) {
					grid.chainedRequestCallback.call(grid);
				}
				else {
					// clear the callback as the previous request failed and the callback was not executed
					grid.chainedRequestCallback = undefined;
				}
			}
		}
	},

	reloadToolbarMenus: function() {
		const grid = this;
		const toolbar = grid.getTopToolbar();
		toolbar.removeAll();
		grid.addToolbarButtons(toolbar);
	},

	hasBeenStrangled: function() {
		const panel = TCG.getParentFormPanel(this);
		const form = panel.getForm();
		const strangleField = form.findField('strangleGroup.id');
		if (!strangleField) {
			return false;
		}
		if (!TCG.isBlank(strangleField.getValue())) {
			return true;
		}
		return this.hasBeenApproved() && (panel.getFormValue('workflowState.name') !== 'Awaiting Fills');
	},

	hasBeenApproved: function() {
		const panel = TCG.getParentFormPanel(this);
		const stateName = panel.getFormValue('workflowState.name');
		const approvedStateNames = ['Approved', 'Awaiting Fills', 'Execution', 'Executed', 'Executed Valid', 'Cannot Fill', 'Unbooked'];
		return (!TCG.isBlank(stateName) && (approvedStateNames.indexOf(stateName) > -1));
	},

	hasBeenClosed: function() {
		const panel = TCG.getParentFormPanel(this);
		const stateName = panel.getFormValue('workflowState.name');
		const closedStateNames = ['Booked', 'Cancelled'];
		return (!TCG.isBlank(stateName) && (closedStateNames.indexOf(stateName) > -1));
	},

	reloadGrid: function(strangleMethod) {
		const grid = this;
		const panel = TCG.getParentFormPanel(this);
		panel.setFormValue('strangleCalculatorMethod', strangleMethod);

		let url = panel.getLoadURL();
		if (url === false) {
			url = panel.appendBindingParametersToUrl('tradeGroupDynamicEntry.json');
		}
		const win = panel.getWindow();
		panel.load(Ext.applyIf({
			url: encodeURI(url),
			params: panel.getLoadParams(win),
			waitMsg: 'Loading...',
			success: function(form, action) {
				panel.loadJsonResult(action.result, true);
				const data = panel.getForm().formValues;
				grid.store.loadData(data);
				grid.markModified(true);
				grid.reloadToolbarMenus();
				const strangleMethodField = panel.getForm().findField('strangleCalculatorMethod');
				if (strangleMethodField) {
					// since we load the response data, make sure to clear strangle method value
					strangleMethodField.setValue('');
				}
				// Special Event that aftercreate listener adds a listener to to clear trades
				grid.fireEvent('afterReloadGrid', grid);
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	executeStrangle: function(strangleMethod) {
		const panel = TCG.getParentFormPanel(this);
		if (strangleMethod) {
			panel.setFormValue('strangleCalculatorMethod', strangleMethod);
		}
		this.applyChainedCallback = false;
		if (this.getWindow().isModified()) {
			this.getWindow().saveWindow(false);
		}
	},

	executeAction: function(groupAction) {
		const panel = TCG.getParentFormPanel(this);
		if (groupAction) {
			panel.setFormValue('tradeGroupAction', groupAction);
		}
		this.applyChainedCallback = false;
		if (this.getWindow().isModified()) {
			this.getWindow().saveWindow(false);
		}
	},

	openActionWindow: function(windowClass, toolBar, gridPanel) {
		const panel = TCG.getParentFormPanel(this);
		const form = panel.getForm();
		TCG.createComponent(windowClass, {
			defaultData: form.formValues,
			openerCt: gridPanel,
			useQuantityOnTrade: gridPanel.useQuantityOnTrade,
			emailFileNamePrefix: gridPanel.emailFileNamePrefix
		});
	},

	applyTradeUpdates: function(tradeValues, callback) {
		const grid = this;
		const panel = TCG.getParentFormPanel(grid);
		const form = panel.getForm();
		// apply Trade changes to details
		Object.keys(tradeValues).forEach(function(key, index) {
			const value = tradeValues[key];
			if (value && value !== '') {
				const f = form.findField(key);
				if (f) {
					f.setValue(value);
				}
			}
		});
		grid.applyChainedCallback = callback !== undefined;
		grid.chainedRequestCallback = callback; // will clear the callback if not present
		if (this.getWindow().isModified()) {
			this.getWindow().saveWindow(false);
		}
	},

	generateQuoteEmail: function(investmentSecurityIsInstrument) {
		const rollTotals = Clifton.trade.group.getTradeGroupDynamicOptionStrangleTradeTotals(this.getStore(), true);
		const form = TCG.getParentFormPanel(this).getForm();
		const formValues = form.formValues;
		let instrumentName = TCG.getValue((investmentSecurityIsInstrument === true) ? 'investmentSecurity.name' : 'investmentSecurity.instrument.name', formValues);
		if (!TCG.isTrue(investmentSecurityIsInstrument) && TCG.isBlank(instrumentName)) {
			instrumentName = TCG.getValue('secondaryInvestmentSecurity.instrument.name', formValues);
		}
		let expirationDate = TCG.getValue('callSecurity.lastDeliveryDate', formValues);
		if (!expirationDate) {
			expirationDate = TCG.getValue('putSecurity.lastDeliveryDate', formValues);
		}
		const expirationDateString = new Date(expirationDate).format('m/d/Y'),
			longPositions = form.findField('longPositions').getValue(),
			buySellString = TCG.isTrue(longPositions) ? 'Buy ' : 'Sell ',
			putSecuritySymbol = TCG.getValue('putSecurity.symbol', formValues),
			callSecuritySymbol = TCG.getValue('callSecurity.symbol', formValues),
			putSymbolPresent = TCG.isNotBlank(putSecuritySymbol),
			callSymbolPresent = TCG.isNotBlank(callSecuritySymbol),
			putTotal = rollTotals.getPutOutrightTotal(),
			callTotal = rollTotals.getCallOutrightTotal(),
			strangleTotal = rollTotals.getStrangleTotal();
		const emailBody = '<!DOCTYPE html><html><head><style>'
			+ 'p{font-family:arial,sans-serif;font-size:14px;}table{border-collapse:collapse;font-family:arial,sans-serif;font-size:14px;}th,td{border:1px solid black;}th{padding:5px;}table#tableInner{border:0;width:100%;}table#tableInner td{border:0;padding:5px;}table#tableInner tr{border-bottom:1px solid black;}table#tableInner tr:last-child{border-bottom:none;}'
			+ '</style></head><body><p>Please provide a quote for the positions below.</p>'
			+ '<p><b><u>Listed ' + instrumentName + '</u></b> contracts expiring <b><u>' + expirationDateString + '</u></b></p>'
			+ '<table><thead><tr><th>Ticker</th><th>Outright</th><th>Strangle</th><th>Total</th></tr></thead>'
			+ '<tbody><tr>'
			// Need to use nested tables because Outlook does not fully support the rowspan attribute for td
			// security column
			+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent, putSecuritySymbol) + '</td></tr>'
			+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent, callSecuritySymbol) + '</td></tr></table></td>'
			// outright/tail column
			+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent && putTotal > 0, buySellString + putTotal) + '</td></tr>'
			+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent && callTotal > 0, buySellString + callTotal) + '</td></tr></table></td>'
			// strangle column
			+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(strangleTotal > 0, buySellString + strangleTotal) + '</td></tr></table></td>'
			// total column
			+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent, rollTotals.getPutGrandTotalWithSign(longPositions)) + '</td></tr>'
			+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent, rollTotals.getCallGrandTotalWithSign(longPositions)) + '</td></tr></table></td>'
			+ '</tr></tbody></table></body></html>';
		TCG.generateEmailFile('Parametric - Quote Request (' + instrumentName + ' - ' + expirationDateString + ' expiry)', 'text/html', emailBody, this.emailFileNamePrefix + 'TradeQuote.eml');
	},

	getValueForDisplayConditionally: function(condition, value) {
		return condition ? value : '---';
	}
});
Ext.reg('base-trade-group-dynamic-option-strangle-detail-formgrid', Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleDetailFormGrid);

/**
 * Base modal window for performaing TradeGroup actions on Option TradeGroupDynamic groups where trades can be strangled.
 */
Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	iconCls: 'add',
	height: 200,
	width: 625,
	modal: true,
	useQuantityOnTrade: false,
	emailFileNamePrefix: 'OptionGroup',

	getForm: function() {
		return this.items.get(0).getForm();
	},

	hasData: function(fieldNameArray) {
		if (fieldNameArray && fieldNameArray.length > 0) {
			const form = this.getForm();
			for (let i = 0; i < fieldNameArray.length; i++) {
				const field = form.findField(fieldNameArray[i]);
				if (field) {
					const value = field.getValue();
					if (value && value !== '') {
						return true;
					}
				}
			}
		}
		return false;
	},

	saveWindow: function() {
		const form = this.getForm();
		const params = form.getValues();
		if (form.isDirty()) {
			if (this.openerCt && this.openerCt.applyTradeUpdates) {
				this.invokeOpenerApplyTradeUpdates.call(this, params);
				this.close();
			}
			else {
				TCG.showError('Opener component must be defined and must have applyTradeUpdates(params) method.', 'Callback Missing');
			}
		}
		else {
			this.close();
		}
	},

	invokeOpenerApplyTradeUpdates: function(params) {
		// override in implementation
	}
});

Clifton.trade.group.TradeGroupDynamicOptionStrangleApplyTradePricesWindow = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow, {
	title: 'Option Trade Group Trade Prices',
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify the Fill Prices to apply to the Put and Call trades within this Option Group.',
			labelWidth: 150,
			items: [
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						layout: 'form',
						columnWidth: .50,
						defaults: {anchor: '-20'}
					},
					items: [
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Strangle Trade Info'},
								{fieldLabel: 'Put Average Price', name: 'putFillPrice', xtype: 'pricefield', qtip: 'Price for all Put Trades. If no outright Put Price is provided, this value will be applied to the outright trades.'},
								{fieldLabel: 'Call Average Price', name: 'callFillPrice', xtype: 'pricefield', qtip: 'Price for all Call Trades. If no outright Call Price is provided, this value will be applied to the outright trades.'}
							]
						},
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Put Outright Trade Info'},
								{fieldLabel: 'Put Average Price', name: 'putTailFillPrice', xtype: 'pricefield', qtip: 'Price for Outright Put Trades only. If left blank, it will inherit the Strangle Put Average Price'},
								{xtype: 'sectionheaderfield', header: 'Call Outright Trade Info'},
								{fieldLabel: 'Call Average Price', name: 'callTailFillPrice', xtype: 'pricefield', qtip: 'Price for Outright Call Trades only. If left blank, it will inherit the Strangle Call Average Price'}
							]
						}
					]
				}
			]
		}
	],

	invokeOpenerApplyTradeUpdates: function(params) {
		params['tradeGroupAction'] = 'FILL';
		this.openerCt.applyTradeUpdates.call(this.openerCt, params);
	}
});

Clifton.trade.group.TradeGroupDynamicOptionStrangleApplyTradeUpdatesWindow = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow, {
	title: 'Option Trade Group Trade Updates',
	height: 440,
	modal: false, // not modal so new Options can be created from template
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify values for updating the Put and Call Trades of this Option Group by changing the Executing Broker, Option securities, and/or Commission Per Unit amounts. If you do not wish to update the Commission amounts, leave the fields blank. NOTE: Updating the securities requires the Trades to first be Rejected, which will require a new Approval of the Trades.',
			labelWidth: 150,

			defaults: {
				anchor: '-20'
			},

			getDefaultData: function(win) {
				const dd = win.defaultData;
				// set callSecurity.lastDeliveryDate if no call
				if (TCG.isBlank(TCG.getValue('callSecurity.lastDeliveryDate', dd))) {
					dd.callSecurity = {
						lastDeliveryDate: dd.putSecurity.lastDeliveryDate
					};
				}
				return dd;
			},

			items: [
				{name: 'tradeDate', xtype: 'datefield', hidden: true},
				{
					fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false,
					listeners: {
						beforeQuery: function(qEvent) {
							const f = qEvent.combo.getParentForm().getForm();
							qEvent.combo.store.baseParams = {
								activeOnDate: f.findField('tradeDate').value,
								tradeTypeNameEquals: 'Options'
							};
						},
						select: function(combo, record, index) {
							// Clear trade destination when executing broker is selected
							const f = combo.getParentForm().getForm();
							const eb = f.findField('executingBrokerCompany.label');
							eb.clearValue();
							eb.reset();
							eb.store.removeAll();
							eb.lastQuery = null;
						}
					}
				},
				{
					fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow',
					displayField: 'label',
					requiredFields: ['tradeDestination.id'],
					url: 'tradeExecutingBrokerCompanyListFind.json',
					queryParam: 'executingBrokerCompanyName',
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();

							combo.store.baseParams = {
								activeOnDate: f.findField('tradeDate').value,
								tradeTypeNameEquals: 'Options',
								tradeDestinationId: f.findField('tradeDestination.id').getValue()
							};
						}
					}
				},
				{xtype: 'sectionheaderfield', header: 'Securities'},
				{xtype: 'checkbox', name: 'putSecurityChanged', hidden: true},
				{xtype: 'checkbox', name: 'callSecurityChanged', hidden: true},
				{fieldLabel: 'Expiration (New)', name: 'callSecurity.lastDeliveryDate', xtype: 'datefield', hidden: true},
				{
					fieldLabel: 'Instrument', xtype: 'panel', layout: 'hbox',
					items: [
						{name: 'optionInstrument.name', detailIdField: 'optionInstrument.id', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow', flex: 1},
						{html: '&nbsp;'},
						{
							iconCls: 'bloomberg', text: 'Live Prices', xtype: 'button', width: 144, tooltip: 'Load the live data values for the specified Put and Call options. The form fields are dynamically loaded as data is retrieved, so there may be a slight delay before the values are visible.',
							listeners: {
								click: function(button, e) {
									const fp = TCG.getParentFormPanel(button);
									const form = fp.getForm();
									const putId = form.findField('putSecurity.id').getValue(),
										callId = form.findField('callSecurity.id').getValue();
									if (!TCG.isBlank(putId) && !TCG.isBlank(callId)) {
										Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, [putId, callId]);
									}
									else if (!TCG.isBlank(putId)) {
										Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, putId, true);
									}
									else if (!TCG.isBlank(callId)) {
										Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues(fp, callId, false);
									}
								}
							}
						}
					]
				},
				{
					fieldLabel: 'Put', xtype: 'panel', layout: 'hbox',
					items: [
						{
							name: 'putSecurity.label', hiddenName: 'putSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true, flex: 1,
							qtip: 'Put Security that will be traded based on quantities entered below',
							requiredFields: ['optionInstrument.id', 'callSecurity.lastDeliveryDate'],
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const instId = f.findField('optionInstrument.id').getValue();
									const store = qEvent.combo.store;
									store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
									store.setBaseParam('instrumentId', instId);
									store.setBaseParam('lastDeliveryDate', f.findField('callSecurity.lastDeliveryDate').value);
									store.setBaseParam('optionType', 'PUT');
								},
								change: function(combo, newValue, oldValue) {
									const form = combo.getParentForm().getForm();
									form.findField('putSecurityChanged').setValue(true);
									form.findField('rejectTradesForEdit').setValue(true);
									if (TCG.isBlank(newValue)) {
										form.findField('putDelta').setValue('');
										form.findField('putPrice').setValue('');
									}
								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									if (!this.getParentForm().getForm().findField('optionInstrument.id').value) {
										TCG.showError('You must first select Instrument for the option.', 'New Option');
										return false;
									}
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},
							getDefaultData: function(f) { // defaults client/counterparty/isda
								const fp = TCG.getParentFormPanel(this);
								const form = fp.getForm();
								const investmentInstrumentId = form.findField('optionInstrument.id').value;
								const secId = form.findField('putSecurity.id').value;
								return {
									instrument: TCG.data.getData('investmentInstrument.json?id=' + investmentInstrumentId, this),
									securityToCopy: {id: secId},
									optionType: 'PUT'
								};
							}
						},
						{html: '&nbsp;'},
						{name: 'putDelta', xtype: 'floatfield', readOnly: true, width: 80, submitValue: false, qtip: 'The live Bloomberg delta for the selected Put Option.'},
						{html: '&nbsp;'},
						{name: 'putPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Put Option.'}
					]
				},
				{
					fieldLabel: 'Call', xtype: 'panel', layout: 'hbox',
					items: [
						{
							name: 'callSecurity.label', hiddenName: 'callSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', allowBlank: true, flex: 1,
							qtip: 'Call Security that will be traded based on quantities entered below',
							requiredFields: ['optionInstrument.id', 'callSecurity.lastDeliveryDate'],
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const instId = f.findField('optionInstrument.id').getValue();
									qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value, instrumentId: instId, lastDeliveryDate: f.findField('callSecurity.lastDeliveryDate').value, optionType: 'CALL'};
								},
								change: function(combo, newValue, oldValue) {
									const form = combo.getParentForm().getForm();
									form.findField('callSecurityChanged').setValue(true);
									form.findField('rejectTradesForEdit').setValue(true);
									if (TCG.isBlank(newValue)) {
										form.findField('callDelta').setValue('');
										form.findField('callPrice').setValue('');
									}
								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									if (!this.getParentForm().getForm().findField('optionInstrument.id').value) {
										TCG.showError('You must first select Instrument for the option.', 'New Option');
										return false;
									}
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},
							getDefaultData: function(f) { // defaults client/counterparty/isda
								const fp = TCG.getParentFormPanel(this);
								const form = fp.getForm();
								const investmentInstrumentId = form.findField('optionInstrument.id').value;
								const secId = form.findField('callSecurity.id').value;
								return {
									instrument: TCG.data.getData('investmentInstrument.json?id=' + investmentInstrumentId, this),
									securityToCopy: {id: secId},
									optionType: 'CALL'
								};
							}
						},
						{html: '&nbsp;'},
						{name: 'callDelta', xtype: 'floatfield', readOnly: true, width: 80, submitValue: false, qtip: 'The live Bloomberg delta for the selected Call Option.'},
						{html: '&nbsp;'},
						{name: 'callPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Call Option.'}
					]
				},
				{
					name: 'rejectTradesForEdit', xtype: 'checkbox', checked: false, boxLabel: 'Reject Trades Before Edits',
					qtip: 'Perform the REJECT action on the Trades to enable edits before trying to set the new values.',
					listeners: {
						check: function(checkbox, checked) {
							const fp = TCG.getParentFormPanel(checkbox);
							fp.getForm().findField('validateTradesAfterEdit').setValue(checked);
						}
					}
				},
				{
					name: 'validateTradesAfterEdit', xtype: 'checkbox', checked: false, boxLabel: 'Validate Trades After Edits',
					qtip: 'Perform the VALIDATE action on the Trades after the new values have been saved.'
				},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						layout: 'form',
						columnWidth: .50,
						defaults: {anchor: '-20'}
					},
					items: [
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Strangle Trade Commissions'},
								{fieldLabel: 'Put Commission Per Unit', name: 'putCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for all Put Trades. If no outright Put Commission is provided, this value will be applied to the outright trades.'},
								{fieldLabel: 'Call Commission Per Unit', name: 'callCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for all Call Trades. If no outright Call Commission is provided, this value will be applied to the outright trades'}
							]
						},
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Put Outright Trade Commissions'},
								{fieldLabel: 'Put Commission Per Unit', name: 'putTailCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for Outright Put Trades only. If left blank, it will inherit the Strangle Commission Per Unit amount.'},
								{xtype: 'sectionheaderfield', header: 'Call Outright Trade Commissions'},
								{fieldLabel: 'Call Commission Per Unit', name: 'callTailCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for Outright Call Trades only. If left blank, it will inherit the Strangle Commission Per Unit amount.'}
							]
						}
					]
				}
			]
		}
	],

	invokeOpenerApplyTradeUpdates: function(params) {
		const openerWindow = this.openerCt;
		const form = this.getForm();
		const rejectTrades = form.findField('rejectTradesForEdit').getValue();
		if (rejectTrades === true) {
			// need to reject, save, and validate the trades.
			const validateTradesCallback = form.findField('validateTradesAfterEdit').getValue() === false ? undefined : function() {
				params['tradeGroupAction'] = 'VALIDATE';
				openerWindow.applyTradeUpdates.call(openerWindow, params);
			};
			const saveTradesCallback = function() {
				params['tradeGroupAction'] = 'SAVE_TRADE';
				openerWindow.applyTradeUpdates.call(openerWindow, params, validateTradesCallback);
			};
			params['tradeGroupAction'] = 'REJECT';
			openerWindow.applyTradeUpdates.call(openerWindow, params, saveTradesCallback);
		}
		else {
			// just save the changes.
			params['tradeGroupAction'] = 'SAVE_TRADE';
			openerWindow.applyTradeUpdates.call(this.openerCt, params);
		}
	}
});

Clifton.trade.group.TradeGroupDynamicOptionStranglePerformStrangleTradeWindow = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow, {
	title: 'Option Strangle Trades',
	height: 365,
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Strangle the Trades in this Option Trade Group with the defined strangle method. May optionally specify values for updating the Put and Call Trades of this Option Trade Group by setting the Executing Broker, Commission Per Unit amounts, and Fill Prices. If you do not wish to update the Commission amounts, leave the fields blank.',
			labelWidth: 150,
			items: [
				{
					fieldLabel: 'Strangle Method', name: 'strangleCalculatorMethod', hiddenName: 'strangleCalculatorMethod', displayField: 'name', valueField: 'strangleCalculatorMethod', xtype: 'combo', mode: 'local', width: 225,
					store: {
						xtype: 'arraystore',
						fields: ['name', 'strangleCalculatorMethod', 'description'],
						data: [
							['By Account', 'OPTIONS_BY_ACCOUNT', 'Strangle Trades By Account.'],
							['Across Accounts', 'OPTIONS_ACROSS_ACCOUNTS', 'Strangle Trades Across Accounts.'],
							['No Tail', 'OPTIONS_NO_TAIL', 'Group the Trades as they are with no Tail/Outright.']
						]
					}
				},
				{xtype: 'label', html: '<hr/>'},
				{name: 'tradeDate', xtype: 'datefield', hidden: true},
				{
					fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false, width: 225,
					listeners: {
						beforeQuery: function(qEvent) {
							const f = qEvent.combo.getParentForm().getForm();
							qEvent.combo.store.baseParams = {
								activeOnDate: f.findField('tradeDate').value,
								tradeTypeNameEquals: 'Options'
							};
						},
						select: function(combo, record, index) {
							// Clear trade destination when executing broker is selected
							const f = combo.getParentForm().getForm();
							const eb = f.findField('executingBrokerCompany.label');
							eb.clearValue();
							eb.reset();
							eb.store.removeAll();
							eb.lastQuery = null;
						}
					}
				},
				{
					fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', width: 225,
					displayField: 'label',
					requiredFields: ['tradeDestination.id'],
					url: 'tradeExecutingBrokerCompanyListFind.json',
					queryParam: 'executingBrokerCompanyName',
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();

							combo.store.baseParams = {
								activeOnDate: f.findField('tradeDate').value,
								tradeTypeNameEquals: 'Options',
								tradeDestinationId: f.findField('tradeDestination.id').getValue()
							};
						}
					}
				},
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {
						layout: 'form',
						columnWidth: .50,
						defaults: {anchor: '-20'}
					},
					items: [
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Strangle Trade'},
								{fieldLabel: 'Put Average Price', name: 'putFillPrice', xtype: 'pricefield', qtip: 'Price for all Put Trades. If no outright Put Price is provided, this value will be applied to the outright trades.'},
								{fieldLabel: 'Put Commission Per Unit', name: 'putCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for all Put Trades. If no outright Put Commission is provided, this value will be applied to the outright trades.'},
								{fieldLabel: 'Call Average Price', name: 'callFillPrice', xtype: 'pricefield', qtip: 'Price for all Call Trades. If no outright Call Price is provided, this value will be applied to the outright trades.'},
								{fieldLabel: 'Call Commission Per Unit', name: 'callCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for all Call Trades. If no outright Call Commission is provided, this value will be applied to the outright trades'}
							]
						},
						{
							items: [
								{xtype: 'sectionheaderfield', header: 'Put Outright Trade'},
								{fieldLabel: 'Put Average Price', name: 'putTailFillPrice', xtype: 'pricefield', qtip: 'Price for Outright Put Trades only. If left blank, it will inherit the Strangle Put Average Price'},
								{fieldLabel: 'Put Commission Per Unit', name: 'putTailCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for Outright Put Trades only. If left blank, it will inherit the Strangle Commission Per Unit amount.'},
								{xtype: 'sectionheaderfield', header: 'Call Outright Trade'},
								{fieldLabel: 'Call Average Price', name: 'callTailFillPrice', xtype: 'pricefield', qtip: 'Price for Outright Call Trades only. If left blank, it will inherit the Strangle Call Average Price'},
								{fieldLabel: 'Call Commission Per Unit', name: 'callTailCommissionPerUnit', xtype: 'floatfield', qtip: 'Commission Per Unit amount for Outright Call Trades only. If left blank, it will inherit the Strangle Commission Per Unit amount.'}
							]
						}
					]
				}
			]
		}
	],

	hasFillFieldsPopulated: function() {
		return this.hasData(['putFillPrice', 'callFillPrice', 'putTailFillPrice', 'callTailFillPrice']);
	},

	hasSaveFieldsPopulated: function() {
		return this.hasData(['executingBrokerCompany.id', 'putCommissionPerUnit', 'callCommissionPerUnit', 'putTailCommissionPerUnit', 'callTailCommissionPerUnit']);
	},

	invokeOpenerApplyTradeUpdates: function(params) {
		const win = this;
		const openerWindow = this.openerCt;
		const fillTradesCallback = win.hasFillFieldsPopulated() === false ? undefined : function() {
			params['tradeGroupAction'] = 'FILL';
			openerWindow.applyTradeUpdates.call(openerWindow, params);
		};
		const saveTradesCallback = win.hasSaveFieldsPopulated() === false ? undefined : function() {
			// then call to update the Trades.
			params['strangleCalculatorMethod'] = '';
			params['tradeGroupAction'] = 'SAVE_TRADE';
			openerWindow.applyTradeUpdates.call(openerWindow, params, fillTradesCallback);
		};
		openerWindow.applyTradeUpdates.call(openerWindow, params, saveTradesCallback);
	}
});

Clifton.trade.group.TradeGroupDynamicOptionStrangleTradeAllocationWindow = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow, {
	title: 'Option Strangle Trade Allocation',
	width: 800,
	height: 600,
	defaultDataIsReal: true,
	hideCancelButton: true,
	allowOpenFromModal: true,

	items: [
		{
			xtype: 'formpanel',

			getDefaultData: function(win) {
				const dd = win.defaultData;
				let expirationDate = TCG.getValue('callSecurity.lastDeliveryDate', dd);
				if (!expirationDate) {
					expirationDate = TCG.getValue('putSecurity.lastDeliveryDate', dd);
				}
				let instrumentName = TCG.getValue('investmentSecurity.instrument.name', dd);
				if (TCG.isBlank(instrumentName)) {
					instrumentName = TCG.getValue('secondaryInvestmentSecurity.instrument.name', dd);
				}
				const allocationLabel = instrumentName + ' contracts expiring ' + new Date(expirationDate).format('Y-m-d') + ' post-trade allocation ';
				return {
					instrumentName: instrumentName,
					putSecuritySymbol: TCG.getValue('putSecurity.symbol', dd),
					callSecuritySymbol: TCG.getValue('callSecurity.symbol', dd),
					expirationDateString: new Date(expirationDate).format('m/d/Y'),
					longPositions: dd.longPositions,
					detailList: dd.detailList,
					allocationLabel: allocationLabel
				};
			},

			defaults: {
				anchor: '0'
			},

			items: [
				{name: 'instrumentName', xtype: 'hidden'},
				{name: 'putSecuritySymbol', xtype: 'hidden'},
				{name: 'callSecuritySymbol', xtype: 'hidden'},
				{name: 'expirationDateString', xtype: 'hidden'},
				{name: 'longPositions', xtype: 'hidden'},
				{hideLabel: true, name: 'allocationLabel', xtype: 'textfield', readOnly: true},
				{
					xtype: 'panel',
					layout: 'fit',
					items: [
						{
							xtype: 'formgrid',
							storeRoot: 'detailList',
							dtoClass: 'com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunTradeGroupDynamicOptionAccountRoll',
							readOnly: true,
							autoHeight: false,
							height: 475,
							viewConfig: {markDirty: false},

							listeners: {
								afterload: function(grid) {
									// calculate the total put and call to determine if the group was strangled with outright groups
									let putTotal = 0, callTotal = 0;
									const useQuantityOnTrade = this.getWindow().useQuantityOnTrade;
									grid.getStore().each(function(record) {
										// calculate totals
										putTotal += grid.getNonNullNumberValue(TCG.getValue(TCG.isTrue(useQuantityOnTrade) ? 'putTrade.quantityIntended' : 'putTradeQuantity', record.json));
										callTotal += grid.getNonNullNumberValue(TCG.getValue(TCG.isTrue(useQuantityOnTrade) ? 'callTrade.quantityIntended' : 'callTradeQuantity', record.json));
									});
									const strangled = putTotal === callTotal;
									// set the grid column value according to the strangle status
									grid.getStore().each(function(record) {
										record.beginEdit();
										if (strangled) {
											record.set('strangle', TCG.getValue(TCG.isTrue(useQuantityOnTrade) ? 'putTrade.quantityIntended' : 'putTradeQuantity', record.json));
											record.set('putOutright', TCG.getValue('putTailTrade.quantityIntended', record.json));
											record.set('callOutright', TCG.getValue('callTailTrade.quantityIntended', record.json));
										}
										else {
											record.set('putOutright', TCG.getValue(TCG.isTrue(useQuantityOnTrade) ? 'putTrade.quantityIntended' : 'putTradeQuantity', record.json));
											record.set('callOutright', TCG.getValue(TCG.isTrue(useQuantityOnTrade) ? 'callTrade.quantityIntended' : 'callTradeQuantity', record.json));
										}
										record.endEdit();
									}, grid);
									grid.getView().refresh();
								}
							},

							columnsConfig: [
								{header: 'Client Account', width: 280, dataIndex: 'clientAccount.name'},
								{header: 'Clearing Broker', width: 150, dataIndex: 'holdingAccount.issuingCompany.name'},
								{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.number'},
								{header: 'Strangle', width: 65, dataIndex: 'strangle', type: 'int', useNull: true, summaryType: 'sum'},
								{header: 'Put', width: 60, dataIndex: 'putOutright', type: 'int', useNull: true, summaryType: 'sum'},
								{header: 'Call', width: 60, dataIndex: 'callOutright', type: 'int', useNull: true, summaryType: 'sum'}
							],

							plugins: {
								ptype: 'gridsummary'
							},

							addToolbarButtons: function(toolBar) {
								const gp = this;
								toolBar.add('->');
								const emailWithAttachmentHandler = function() {
									const form = TCG.getParentFormPanel(this).getForm();
									//each part needs an empty line between itself and the previous
									const emailBody = '--part\nContent-Type:text/csv\nContent-Disposition:attachment;filename=\"ParametricOptionAllocation.csv\"\n\n'
										+ gp.generateCsvData.call(gp) + '\n\n--part\nContent-Type:text/html\n\n' + gp.generateHtmlData.call(gp);
									TCG.generateEmailFile(form.findField('allocationLabel').getValue(), 'multipart/mixed;boundary=part', emailBody, gp.getWindow().emailFileNamePrefix + 'TradeAllocation.eml');
								};
								toolBar.add({
									text: 'Open Email with Attachment',
									xtype: 'splitbutton',
									iconCls: 'email',
									tooltip: 'Generates a Trade Allocation Email with the table data included in the body of the email and in an attached CSV file.',
									handler: emailWithAttachmentHandler,
									menu: {
										items: [{
											text: 'Open Email',
											iconCls: 'email',
											tooltip: 'Generates a Trade Allocation Email with the table data included in the body of the email.',
											handler: function() {
												const form = TCG.getParentFormPanel(this).getForm();
												TCG.generateEmailFile(form.findField('allocationLabel').getValue(), 'text/html', gp.generateHtmlData.call(gp), gp.getWindow().emailFileNamePrefix + 'TradeAllocation.eml');
											}
										}, {
											text: 'Open Email with Attachment',
											iconCls: 'email',
											tooltip: 'Generates a Trade Allocation Email with the table data included in the body of the email and in an attached CSV file.',
											handler: emailWithAttachmentHandler
										}
										]
									}
								});
								toolBar.add('-');
								toolBar.add({
									text: 'Export to CSV',
									iconCls: 'export',
									tooltip: 'Export the table to CSV file',
									handler: function() {
										TCG.downloadDataAsFile('text/csv', gp.generateCsvData.call(gp), 'ParametricOptionAllocation.csv');
									}
								});
							},

							generateCsvData: function() {
								const gp = this;
								let csvContent = 'Broker,Account,Strangle,Put (Outright),Call (Outright)';
								let strangleTotal = 0, putTotal = 0, callTotal = 0;
								gp.getStore().each(function(record) {
									const strangleValue = gp.getFieldValueFromObject(record.data, 'strangle'),
										putValue = gp.getFieldValueFromObject(record.data, 'putOutright'),
										callValue = gp.getFieldValueFromObject(record.data, 'callOutright');
									// calculate totals
									strangleTotal += gp.getNonNullNumberValue(strangleValue);
									putTotal += gp.getNonNullNumberValue(putValue);
									callTotal += gp.getNonNullNumberValue(callValue);

									csvContent += '\n\"' + gp.getFieldValueFromObject(record.data, 'holdingAccount.issuingCompany.name') + '\",\"'
										+ gp.getFieldValueFromObject(record.data, 'holdingAccount.number') + '\",\"'
										+ strangleValue + '\",\"' + putValue + '\",\"' + callValue + '\"';
								}, gp);
								csvContent += '\n,,' + strangleTotal + ',' + putTotal + ',' + callTotal;
								return csvContent;
							},

							generateHtmlData: function() {
								const gp = this;
								const form = TCG.getParentFormPanel(gp).form;
								const longPositions = form.findField('longPositions').getValue();
								const buySellString = TCG.isTrue(longPositions) ? 'Bought ' : 'Sold ';
								const totalSign = TCG.isTrue(longPositions) ? '' : '-';
								let strangleTotal = 0, putTotal = 0, callTotal = 0, putPrice = 0, callPrice = 0,
									putCommission = 0, callCommission = 0, putOutrightCommission = 0, callOutrightCommission = 0;
								let allocationTable = '<table><thead><tr><th>Broker</th><th>Account</th><th>Strangle</th><th>Put<br>(Outright)</th><th>Call<br>(Outright)</th></tr></thead><tbody>';
								gp.getStore().each(function(record) {
									const strangleValue = gp.getFieldValueFromObject(record.data, 'strangle'),
										putValue = gp.getFieldValueFromObject(record.data, 'putOutright'),
										callValue = gp.getFieldValueFromObject(record.data, 'callOutright');

									// calculate totals
									strangleTotal += gp.getNonNullNumberValue(strangleValue);
									putTotal += gp.getNonNullNumberValue(putValue);
									callTotal += gp.getNonNullNumberValue(callValue);

									// Update value from record. All records should have the same value for the attribute.
									putPrice = gp.getValueFromRecordConditionally(putPrice, 'putTrade.averageUnitPrice', record);
									if (TCG.isBlank(putPrice)) {
										putPrice = gp.getValueFromRecordConditionally(putPrice, 'putTailTrade.averageUnitPrice', record);
									}
									callPrice = gp.getValueFromRecordConditionally(callPrice, 'callTrade.averageUnitPrice', record);
									if (TCG.isBlank(callPrice)) {
										callPrice = gp.getValueFromRecordConditionally(callPrice, 'callTailTrade.averageUnitPrice', record);
									}
									putCommission = gp.getValueFromRecordConditionally(putCommission, 'putTrade.commissionPerUnit', record);
									callCommission = gp.getValueFromRecordConditionally(callCommission, 'callTrade.commissionPerUnit', record);
									putOutrightCommission = gp.getValueFromRecordConditionally(putOutrightCommission, 'putTailTrade.commissionPerUnit', record);
									callOutrightCommission = gp.getValueFromRecordConditionally(callOutrightCommission, 'callTailTrade.commissionPerUnit', record);

									allocationTable += '<tr><td style="padding:5px">'
										+ gp.getFieldValueFromObject(record.data, 'holdingAccount.issuingCompany.name') + '</td><td style="padding:5px">'
										+ gp.getFieldValueFromObject(record.data, 'holdingAccount.number') + '</td><td style="padding:5px">'
										+ strangleValue + '</td><td style="padding:5px">' + putValue + '</td><td style="padding:5px">' + callValue + '</td></tr>';
								}, gp);
								if (strangleTotal === 0) {
									// if there is not a strangle amount, correct the commission accordingly
									putOutrightCommission = putCommission;
									callOutrightCommission = callCommission;
									putCommission = 0;
									callCommission = 0;
								}
								allocationTable += '<tr><td colspan="2"></td><td style="padding:5px">' + strangleTotal + '</td><td style="padding:5px">' + putTotal + '</td><td style="padding:5px">' + callTotal + '</td></tr></tbody></table>';

								const putSymbol = form.findField('putSecuritySymbol').getValue(),
									callSymbol = form.findField('callSecuritySymbol').getValue(),
									putSymbolPresent = TCG.isNotBlank(putSymbol),
									callSymbolPresent = TCG.isNotBlank(callSymbol);
								return '<!DOCTYPE html><html><head><style>'
									+ 'p{font-family:arial,sans-serif;font-size:14px;}table{border-collapse:collapse;font-family:arial,sans-serif;font-size:14px;}th,td{border:1px solid black;}th{padding:5px;}table#tableInner{border:0;width:100%;}table#tableInner td{border:0;padding:5px;}table#tableInner tr{border-bottom:1px solid black;}table#tableInner tr:last-child{border-bottom:none;}'
									+ '</style></head><body>'
									+ '<p><b><u>Listed ' + form.findField('instrumentName').getValue() + '</u></b> contracts expiring <b><u>' + form.findField('expirationDateString').getValue() + '</u></b></p>'
									// summary table
									+ '<table><thead><tr><th>Ticker</th><th>Outright</th><th>Strangle</th><th>Total</th><th>Price</th><th>Commission<br>(Strangle)</th><th>Commission<br>(Outright)</th></tr></thead>'
									+ '<tbody><tr>'
									// Need to use nested tables because Outlook does not fully support the rowspan attribute for td
									// security column
									+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent, putSymbol) + '</td></tr>'
									+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent, callSymbol) + '</td></tr></table></td>'
									// outright/tail column
									+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent && putTotal > 0, buySellString + this.getNonNullNumberValue(putTotal)) + '</td></tr>'
									+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent && callTotal > 0, buySellString + this.getNonNullNumberValue(callTotal)) + '</td></tr></table></td>'
									// strangle column
									+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(strangleTotal > 0, buySellString + this.getNonNullNumberValue(strangleTotal)) + '</td></tr></table></td>'
									// total column
									+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent, totalSign + this.getNonNullNumberValue(putTotal + strangleTotal)) + '</td></tr>'
									+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent, totalSign + this.getNonNullNumberValue(callTotal + strangleTotal)) + '</td></tr></table></td>'
									// price column
									+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent, this.getNonNullNumberValue(putPrice)) + '</td></tr>'
									+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent, this.getNonNullNumberValue(callPrice)) + '</td></tr></table></td>'
									// commission strangle column
									+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent, this.getNonNullNumberValue(putCommission)) + '</td></tr>'
									+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent, this.getNonNullNumberValue(callCommission)) + '</td></tr></table></td>'
									// commission outright column
									+ '<td><table id="tableInner"><tr><td>' + this.getValueForDisplayConditionally(putSymbolPresent, this.getNonNullNumberValue(putOutrightCommission)) + '</td></tr>'
									+ '<tr><td>' + this.getValueForDisplayConditionally(callSymbolPresent, this.getNonNullNumberValue(callOutrightCommission)) + '</td></tr></table></td>'
									+ '</tr></tbody></table>'
									// allocation table
									+ '<p>Allocation below.</p>' + allocationTable + '</body></html>';
							},

							getValueFromRecordConditionally: function(currentValue, property, record) {
								return (TCG.isBlank(currentValue) || currentValue === 0) ? TCG.getValue(property, record.json) : currentValue;
							},

							getFieldValueFromObject: function(object, field) {
								if (field) {
									const value = object[field];
									if (!TCG.isBlank(value)) {
										return value;
									}
								}
								return '';
							},

							getNonNullNumberValue: function(value) {
								return TCG.isBlank(value) ? 0 : value;
							},

							getValueForDisplayConditionally: function(condition, value) {
								return condition ? value : '---';
							}
						}
					],
					listeners: {
						afterlayout: TCG.grid.registerDynamicHeightResizing
					}
				}
			]
		}
	]
});

Clifton.trade.group.getTradeGroupDynamicOptionStrangleTradeTotals = function(records, recordsIsStore) {
	const rollTotals = {
		putOutrightTotal: 0,
		callOutrightTotal: 0,
		stranglePutTotal: 0,
		strangleCallTotal: 0,

		isStrangled: function() {
			return this.strangleCallTotal === this.stranglePutTotal;
		},

		getPutOutrightTotal: function() {
			return this.isStrangled() ? this.putOutrightTotal : this.stranglePutTotal;
		},

		getCallOutrightTotal: function() {
			return this.isStrangled() ? this.callOutrightTotal : this.strangleCallTotal;
		},

		getStrangleTotal: function() {
			return this.isStrangled() ? this.stranglePutTotal : 0;
		},

		getPutGrandTotalWithSign: function(longPositions) {
			return (this.getPutOutrightTotal() + this.getStrangleTotal()) * (TCG.isTrue(longPositions) ? 1 : -1);
		},

		getCallGrandTotalWithSign: function(longPositions) {
			return (this.getCallOutrightTotal() + this.getStrangleTotal()) * (TCG.isTrue(longPositions) ? 1 : -1);
		},

		incrementTotal: function(fieldName, value) {
			if (value && TCG.isNumber(value)) {
				switch (fieldName) {
					case 'putTailTrade.quantityIntended': {
						this.putOutrightTotal += value;
						break;
					}
					case 'callTailTrade.quantityIntended': {
						this.callOutrightTotal += value;
						break;
					}
					case 'putTrade.quantityIntended':
					case 'putTradeQuantity': {
						this.stranglePutTotal += value;
						break;
					}
					case 'callTrade.quantityIntended':
					case 'callTradeQuantity': {
						this.strangleCallTotal += value;
						break;
					}
					default: {
						// do nothing - this case should never occur
						break;
					}
				}
			}
		}
	};
	const fieldNames = ['putTailTrade.quantityIntended', 'callTailTrade.quantityIntended', 'putTrade.quantityIntended', 'putTradeQuantity', 'callTrade.quantityIntended', 'callTradeQuantity'];
	if (recordsIsStore === true) {
		records.each(function(record) {
			for (let j = 0; j < fieldNames.length; j++) {
				rollTotals.incrementTotal(fieldNames[j], TCG.getValue(fieldNames[j], record.json));
			}
		});
	}
	else {
		for (let i = 0; i < records.length; i++) {
			for (let j = 0; j < fieldNames.length; j++) {
				rollTotals.incrementTotal(fieldNames[j], TCG.getValue(fieldNames[j], records[i]));
			}
		}
	}
	return rollTotals;
};

/* Asynchronously looks up the live Price and Delta for the specified option security ID and updates the
 * necessary form fields when the values are obtained. Supports an array of security IDs to look up both the
 * PUT and CALL securities together, with the necessary form updates.
 */
Clifton.trade.group.updateTradeGroupDynamicOptionFormMarketDataValues = function(formPanel, securityId, put) {
	if (formPanel) {
		TCG.data.getDataPromise('investmentSecurityListFind.json', formPanel, {
			// Lookup if there are mini options for the provided security ID(s) to get live pricing.
			params: {
				bigSecurityIdList: securityId
			}
		}).then(function(securities) {
			if (securities) {
				// If mini options are returned, update the appropriate big option so a live price can be obtained.
				// The big options only have daily closing price. The mini prices can get live pricing.
				let i = 0;
				const len = securities.length;
				for (; i < len; i++) {
					if (Ext.isArray(securityId)) {
						let j = 0;
						const innerLen = securityId.length;
						for (; j < innerLen; j++) {
							if (securities[i].bigSecurity.id === securityId[j]) {
								securityId[j] = securities[i].id;
								break;
							}
						}
					}
					else {
						if (securities[i].bigSecurity.id === securityId) {
							securityId = securities[i].id;
						}
					}
				}
			}
		}).then(function() {
			const maxAgeSeconds = 2 * 60; // 2 minutes
			const undefinedForPrice = void 0; // passing no field will look up live price
			// Fetch the live data values and update the appropriate form panel fields.
			if (Ext.isArray(securityId)) {
				Clifton.marketdata.field.updateSecurityLiveMarketDataValues(formPanel, securityId, ['putPrice', 'callPrice'], undefinedForPrice, {maxAgeInSeconds: maxAgeSeconds, providerOverride: 'BPIPE'});
				Clifton.marketdata.field.updateSecurityLiveMarketDataValues(formPanel, securityId, ['putDelta', 'callDelta'], 'Delta', {maxAgeInSeconds: maxAgeSeconds, providerOverride: 'BPIPE'});
			}
			else {
				if (put === true) {
					Clifton.marketdata.field.updateSecurityLiveMarketDataValues(formPanel, securityId, 'putPrice', undefinedForPrice, {maxAgeInSeconds: maxAgeSeconds, providerOverride: 'BPIPE'});
					Clifton.marketdata.field.updateSecurityLiveMarketDataValues(formPanel, securityId, 'putDelta', 'Delta', {maxAgeInSeconds: maxAgeSeconds, providerOverride: 'BPIPE'});
				}
				else {
					Clifton.marketdata.field.updateSecurityLiveMarketDataValues(formPanel, securityId, 'callPrice', undefinedForPrice, {maxAgeInSeconds: maxAgeSeconds, providerOverride: 'BPIPE'});
					Clifton.marketdata.field.updateSecurityLiveMarketDataValues(formPanel, securityId, 'callDelta', 'Delta', {maxAgeInSeconds: maxAgeSeconds, providerOverride: 'BPIPE'});
				}
			}
		});
	}
};

Clifton.trade.restriction.TradeRestrictionGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeRestrictionListFind',
	// language=HTML
	instructions: `
		<div>
			Trade Restriction is used to restrict trading for the specified scope (client account and/or security). Restrictions are evaluated during trade validation for all
			trades using the selected account and the selected security or a derivative thereof.
		</div>
		<div class="warning-msg" style="margin-top: 5px">
			Note: All dates are displayed in Central Standard Time (CST).
		</div>
	`,
	wikiPage: 'IT/Trade Restrictions',
	topToolbarSearchParameter: 'searchPattern',

	getDefaultClientAccount: () => null,
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Category', name: 'restrictionTypeCategoryId', width: 200, displayfield: 'name', xtype: 'toolbar-combo', url: 'tradeRestrictionTypeCategoryListFind.json', loadAll: true, linkedFilter: 'restrictionType.category.name'});
		if (!this.getDefaultClientAccount()) {
			filters.push(...[{fieldLabel: 'Client Account', name: 'clientInvestmentAccountId', width: 200, displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountListFind.json?ourAccount=true', linkedFilter: 'clientInvestmentAccount.label'}]);
		}
		filters.push(...[
			{
				fieldLabel: 'Display', xtype: 'combo', name: 'displayFilter',
				width: 140, forceSelection: true,
				mode: 'local', value: 'ACTIVE_AND_UPCOMING',
				store: {
					xtype: 'arraystore',
					data: [
						['ALL', 'All', 'Display all Trade Restrictions.'],
						['ACTIVE', 'Active', 'Display all Trade Restrictions that are currently active.'],
						['ACTIVE_AND_UPCOMING', 'Active and Upcoming', 'Display all Trade Restrictions that are currently active or have not yet become active.'],
						['INACTIVE', 'Inactive', 'Display all Trade Restrictions that are currently inactive.'],
						['UPCOMING', 'Upcoming', 'Display all Trade Restrictions that have not yet become active.'],
						['EXPIRED', 'Expired', 'Display all Trade Restrictions that have already expired.']
					]
				},
				listeners: {select: field => this.reload()}
			},
			...TCG.callSuper(this, 'getTopToolbarFilters', arguments) || []
		]);
		return filters;
	},
	getLoadParams: function() {
		const params = TCG.callSuper(this, 'getLoadParams', arguments);
		if (this.getDefaultClientAccount()) {
			params.clientAccountId = this.getDefaultClientAccount().id;
		}
		return params;
	},
	onBeforeReturnFilterData: function(filters) {
		// Apply filters
		const currentDate = new Date().format('Y-m-d H:i:s');
		switch (TCG.getChildByName(this.gridPanel.getTopToolbar(), 'displayFilter').getValue()) {
			case 'ACTIVE':
				filters.push({field: 'active', data: {comparison: 'EQUALS', value: true}});
				break;
			case 'ACTIVE_AND_UPCOMING':
				filters.push({field: 'endDate', data: {comparison: 'GREATER_THAN_OR_EQUALS_OR_IS_NULL', value: currentDate}});
				break;
			case 'INACTIVE':
				filters.push({field: 'active', data: {comparison: 'EQUALS', value: false}});
				break;
			case 'UPCOMING':
				filters.push({field: 'startDate', data: {comparison: 'GREATER_THAN', value: currentDate}});
				break;
			case 'EXPIRED':
				filters.push({field: 'endDate', data: {comparison: 'LESS_THAN', value: currentDate}});
				break;
			default:
				// Do nothing
				break;
		}
	},

	editor: {
		detailPageClass: 'Clifton.trade.restriction.TradeRestrictionWindow',
		deleteURL: 'tradeRestrictionDeactivate.json',
		allowToDeleteMultiple: true,
		reloadGridAfterDelete: true,
		getDefaultData: gridPanel => ({
			clientInvestmentAccount: gridPanel.getDefaultClientAccount() || TCG.getChildByName(gridPanel.getTopToolbar(), 'clientInvestmentAccountId').getValueObject()
		})
	},

	listeners: {
		afterrender: panel => {
			// Hide account if rendered in the context of an account
			if (panel.getDefaultClientAccount()) {
				const columnModel = panel.grid.getColumnModel();
				const accountColumn = columnModel.getColumnsBy(col => col.dataIndex === 'clientInvestmentAccount.label')[0];
				columnModel.setHidden(columnModel.getIndexById(accountColumn.id), true);
			}
		}
	},

	columns: [
		{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
		{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 150, filter: {searchFieldName: 'clientAccountId', type: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'}},
		{header: 'Security', dataIndex: 'investmentSecurity.symbol', width: 100, filter: {searchFieldName: 'securityId', type: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', dataIndex: 'investmentSecurity.name', width: 150, hidden: true, filter: {searchFieldName: 'securityId', type: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Restriction Type Category', dataIndex: 'restrictionType.category.name', width: 80, hidden: true, filter: {searchFieldName: 'restrictionTypeCategoryId', type: 'combo', displayField: 'label', url: 'tradeRestrictionTypeCategoryListFind.json'}},
		{header: 'Restriction Type', dataIndex: 'restrictionType.name', width: 80, filter: {searchFieldName: 'restrictionTypeId', type: 'combo', displayField: 'label', url: 'tradeRestrictionTypeListFind.json'}},
		{header: 'Restriction Value', dataIndex: 'restrictionValue', width: 70, type: 'float', useNull: true},
		{header: 'Start Date', dataIndex: 'startDate', width: 100, alwaysIncludeTime: true, defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'End Date', dataIndex: 'endDate', width: 100, alwaysIncludeTime: true},
		{header: 'Cause Table', width: 50, dataIndex: 'restrictionType.causeSystemTable.label', idDataIndex: 'restrictionType.causeSystemTable.id', filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'restrictionTypeCausedSystemTableId', displayField: 'label'}},
		{header: 'Cause FK ID', width: 50, dataIndex: 'causeFkFieldId', hidden: true, filter: false},
		{
			header: 'Cause', width: 40, dataIndex: 'restrictionType.causeSystemTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(value, metadata, record) {
				if (TCG.isNotBlank(value) && TCG.isNotBlank(record.data.causeFkFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('restrictionType.causeSystemTable.label', record.json) + ' that is linked to this freeze entry', 'ShowCause');
				}
				return 'N/A';
			},
			eventListeners: {
				'click': function(column, grid, rowIndex, event) {
					const row = grid.store.data.items[rowIndex];
					const clz = row.json.restrictionType.causeSystemTable.detailScreenClass;
					const id = row.json.causeFkFieldId;
					const gridPanel = grid.ownerGridPanel;

					gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
				}
			}
		},
		{header: 'Restriction Note', dataIndex: 'noteWithDefaults', width: 200},
		{header: 'System Managed', dataIndex: 'systemManaged', type: 'boolean', hidden: true}
	]
});
Ext.reg('trade-restriction-grid', Clifton.trade.restriction.TradeRestrictionGrid);

/**
 * This is a trade group window that shows a set of common group related fields in the header and a filterable grid panel of the group's trades.
 * Some trade group windows display a summary view for the client accounts' trades, where multiple trades exist for each client account. This window
 * helps navigating to the unique trades responsible for the summary numbers possible.
 *
 * For example, the Option Spread trade group contains multiple trades for different option securities with the same quantity for each client account traded.
 * Each client will have a pair/spread of Put and/or Call options. The Option Spread group window only shows a summary with the quantity traded for the options involved.
 * Using this window with the params.id equal to the Option Spread trade group ID and the defaultData.clientInvestmentAccount equal to the row from the Option Spread group row
 * displays only the applicable client trade details for further review.
 */
Clifton.trade.TradeGroupTradeListReviewWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Group Filtered Trades',
	iconCls: 'shopping-cart',
	width: 1200,
	height: 400,
	hideApplyButton: true,
	hideCancelButton: true,

	windowOnToFront: function(window, config) {
		let reload = false;
		if (!TCG.isEquals(window.params, config.params)) {
			window.params = config.params;
			reload = true;
		}
		if (!TCG.isEquals(window.defaultData, config.defaultData)) {
			window.defaultData = config.defaultData;
			reload = true;
		}
		if (reload) {
			const formPanel = window.getMainFormPanel();
			formPanel.doLoad(formPanel.getLoadURL());
		}
	},

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			url: 'tradeGroup.json?requestedMaxDepth=3&requestedPropertiesToExcludeGlobally=tradeList',

			layout: 'vbox',
			layoutConfig: {
				align: 'stretch'
			},

			getFormLabel: function() {
				const form = this.getForm();
				return form.findField('tradeGroupType.name').getValue() + '-' + form.findField('id').getValue();
			},

			listeners: {
				afterload: function() {
					const tradeGrid = TCG.getChildByName(this, 'tradeGroupTradeSubListGrid');

					const defaultData = this.getWindow().defaultData || {};
					if (TCG.getValue('clientInvestmentAccount.id', defaultData)) {
						tradeGrid.setFilterValue('clientInvestmentAccount.label', {
							text: TCG.getValue('clientInvestmentAccount.label', defaultData),
							value: TCG.getValue('clientInvestmentAccount.id', defaultData)
						});
					}

					const formValues = this.getForm().formValues || {};
					if (TCG.getValue('id', formValues)) {
						tradeGrid.setFilterValue('tradeGroup.id', {eq: TCG.getValue('id', formValues)});
					}
					else if (TCG.getValue('id', defaultData)) {
						tradeGrid.setFilterValue('tradeGroup.id', {eq: TCG.getValue('id', defaultData)});
					}

					tradeGrid.reload();
				}
			},

			items: [
				{
					xtype: 'panel',
					layout: 'column',
					defaults: {columnWidth: .33, layout: 'form'},
					items: [
						{
							items: [
								{fieldLabel: 'Group Type', name: 'tradeGroupType.name', xtype: 'linkfield', detailIdField: 'id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'}
							]
						},
						{
							items: [
								{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'displayfield', detailIdField: 'traderUser.id'},
								{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'displayfield', type: 'date'}
							]
						},
						{
							items: [
								{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
								{fieldLabel: 'Workflow Status', name: 'workflowStatus.name', xtype: 'displayfield'}
							]
						}]
				},
				{
					xtype: 'gridpanel',
					name: 'tradeGroupTradeSubListGrid',
					readOnly: true,
					requestIdDataIndex: true,
					hideStandardButtons: true,
					reloadOnRender: false,
					autoHeight: false,
					flex: 1,

					getLoadURL: function() {
						return 'tradeListFind.json';
					},

					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true, type: 'int'},
						{header: 'Trade Group ID', dataIndex: 'tradeGroup.id', hidden: true, type: 'int', filter: {searchFieldName: 'tradeGroupId'}},
						{header: 'Workflow Status', width: 75, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade', showNotEquals: true}, hidden: true},
						{header: 'Workflow State', width: 75, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Client Account', width: 150, dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Security', width: 120, dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json?active=true'}},
						{
							header: 'Buy/Sell', width: 50, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{
							header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json', loadAll: true}, viewNames: ['Export Friendly'],
							renderer: function(v, metaData) {
								if (TCG.isNotBlank(v)) {
									metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
								}
								return v;
							}
						},
						{header: 'Quantity', width: 75, dataIndex: 'quantityIntended', type: 'float', useNull: true, summaryType: 'sum'},
						{header: 'Exchange Rate', width: 75, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
						{header: 'Avg Price', width: 75, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
						{header: 'Commission Per Unit', width: 50, dataIndex: 'commissionPerUnit', type: 'float', useNull: true, hidden: true},
						{header: 'Fee', width: 50, dataIndex: 'feeAmount', type: 'float', hidden: true},
						{header: 'Accounting Notional', width: 120, dataIndex: 'accountingNotional', type: 'currency', hidden: true},
						{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
						{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Team', width: 70, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', hidden: true},
						{header: 'Trader', width: 75, dataIndex: 'traderUser.label', hidden: true},
						{header: 'Executing Broker', width: 120, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Trade Destination', width: 100, dataIndex: 'tradeDestination.name', idDataIndex: 'tradeDestination.id', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}}
					],
					editor: {detailPageClass: 'Clifton.trade.TradeWindow', drillDownOnly: true}
				}
			]
		}
	]
});

/**
 * Looks up if the provided client and holding account combination is defined to use a directed broker for executing trades.
 * Returns the single executing broker company if it is defined. Returns undefined if client and holding account combination
 * is not set up to use directed brokers.
 *
 * Looks the directed broker up by:
 * - the holding account's account type executionWithIssuerRequired flag, if true return the holding account's issuing company
 * - if a single include broker restriction is defined for the client account
 *
 * @param clientAccount is a client account object with the id property defined at a minimum
 * @param holdingAccount is a holding account object with the id property defined at a minimum
 * @returns {Promise<*>} If a directed broker is applicable, an executing business company object with a minimum of name, label, and id properties defined; undefined otherwise.
 */
Clifton.trade.getDirectedExecutingBrokerCompany = async (clientAccount, holdingAccount) => {
	let holdingAccountPromise = void 0;
	let executingBrokerCompanyPromise = void 0;
	let tradeBrokerRestrictionPromise = void 0;
	if (holdingAccount) {
		if (holdingAccount.type && holdingAccount.type.executionWithIssuerRequired) {
			if (holdingAccount.issuingCompany && holdingAccount.issuingCompany.label && holdingAccount.issuingCompany.id) {
				return holdingAccount.issuingCompany;
			}
			else if (holdingAccount.issuingCompany && holdingAccount.issuingCompany.id) {
				executingBrokerCompanyPromise = TCG.data.getDataPromise('businessCompany.json', null, {
					id: holdingAccount.issuingCompany.id,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'name|label|id'
				});
			}
		}
		if (holdingAccount.id && TCG.isNull(executingBrokerCompanyPromise)) {
			holdingAccountPromise = TCG.data.getDataPromise('investmentAccount.json', null, {
				params: {
					id: holdingAccount.id,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'type.executionWithIssuerRequired|issuingCompany.name|issuingCompany.label|issuingCompany.id'
				}
			});
		}
	}
	if (TCG.isNull(executingBrokerCompanyPromise) && clientAccount && clientAccount.id) {
		tradeBrokerRestrictionPromise = TCG.data.getDataPromise('tradeRestrictionBrokerListFind.json', null, {
			params: {
				clientAccountId: clientAccount.id,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'brokerCompany.name|brokerCompany.label|brokerCompany.id|include',
				requestedPropertiesToExcludeGlobally: 'tradeRestrictionBrokerSearchForm'
			}
		});
	}
	const [fetchedHoldingAccount, fetchedExecutingBrokerCompany, restrictionList] = await Promise.all([holdingAccountPromise, executingBrokerCompanyPromise, tradeBrokerRestrictionPromise]);

	if (fetchedHoldingAccount && fetchedHoldingAccount.type.executionWithIssuerRequired) {
		return fetchedHoldingAccount.issuingCompany;
	}
	else if (fetchedExecutingBrokerCompany) {
		return fetchedExecutingBrokerCompany;
	}
	else if (restrictionList && restrictionList.length === 1) {
		const singleBrokerRestriction = restrictionList[0];
		if (singleBrokerRestriction.include) {
			return {
				name: singleBrokerRestriction.brokerCompany.name,
				label: singleBrokerRestriction.brokerCompany.label,
				id: singleBrokerRestriction.brokerCompany.id
			};
		}
	}
	return void 0;
};

Clifton.trade.TradeMarketDataValueGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeMarketDataValueListFind',
	xtype: 'gridpanel',
	instructions: 'The listed Trade Market Data Values have been captured for existing trades. These are market data values that have been captured for the traded security at various points in the trade lifecycle, such as on trade creation, validation, or execution.',
	isTradeForm: false,
	editor: {
		detailPageClass: 'Clifton.trade.marketdata.TradeMarketDataValueWindow',
		getDefaultData: (gridPanel, row) => ({referenceOne: gridPanel.getTrade()})
	},
	listeners: {
		afterrender: panel => panel.isTradeForm && panel.getColumnModel().setHidden(panel.getColumnModel().findColumnIndex('referenceOne.labelShort'), true)
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Trade', width: 75, dataIndex: 'referenceOne.labelShort', filter: {type: 'combo', searchFieldName: 'tradeId', url: 'tradeListFind.json?orderBy=tradeDate:DESC', detailPageClass: 'Clifton.trade.TradeWindow', disableAddNewItem: true}},
		{header: 'Trade Field', width: 50, dataIndex: 'tradeMarketDataField.name', filter: {type: 'combo', searchFieldName: 'tradeMarketDataFieldId', url: 'tradeMarketDataFieldListFind.json'}},
		{header: 'Data Source', width: 50, dataIndex: 'tradeMarketDataField.marketDataSource.name', hidden: true, filter: {type: 'combo', searchFieldName: 'marketDataSourceId', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'}},
		{header: 'Workflow State', width: 40, dataIndex: 'tradeMarketDataField.tradeWorkflowEndState.label', filter: {type: 'combo', searchFieldName: 'tradeWorkflowEndStateId', url: 'workflowStateListByWorkflowName.json?workflowName=Trade Workflow', root: 'data'}},
		{header: 'Measure Date', width: 40, dataIndex: 'referenceTwo.measureDateWithTime', type: 'date', filter: {searchFieldName: 'measureDate'}},
		{
			header: 'Measure Value', width: 40, dataIndex: 'referenceTwo.measureValue', type: 'float',
			renderer: function(v, metaData, r) {
				let processedValue = r.json.referenceTwo.measureValue != null
					? TCG.numberFormat(v, '0,000', true)
					: '<span style="color: red; font-weight: bold;">Insufficient Access</span>';
				if (r.json.createDate !== r.json.updateDate) {
					processedValue = `<div class="amountAdjusted" qtip="Market Data was updated. See Audit Trail for details.">${processedValue}</div>`;
				}
				return processedValue;
			}
		}
	],
	getTrade: function() {
		if (!this.isTradeForm) {
			return null;
		}
		const win = TCG.getParentByClass(this, Ext.Window);
		return win.getMainForm().formValues;
	},
	getLoadParams: function(firstLoad) {
		const params = {
			tradeId: this.isTradeForm ? this.getTrade().id : null,
			enableOpenSessionInView: true
		};
		// Apply default convenience sorting
		if (firstLoad) {
			this.ds.sortInfo = null;
			if (!this.isTradeForm) {
				this.setFilterValue('referenceTwo.measureDateWithTime', {'after': new Date().add(Date.DAY, -7)});
			}
		}
		if (!this.ds.sortInfo) {
			params.orderBy = 'measureDate:DESC#measureTime:DESC#tradeMarketDataFieldId:ASC';
		}
		return params;
	}
});
Ext.reg('trade-market-data-value-grid', Clifton.trade.TradeMarketDataValueGridPanel);

Clifton.trade.DefaultTradeWindowAdditionalTabs[Clifton.trade.DefaultTradeWindowAdditionalTabs.length] = {
	title: 'Market Data',
	items: [{
		xtype: 'trade-market-data-value-grid',
		instructions: 'The following Market Data Values were captured during specified trade workflow transitions.',
		isTradeForm: true
	}]
};

Clifton.trade.DefaultTradeWindowAdditionalTabs[Clifton.trade.DefaultTradeWindowAdditionalTabs.length] = {
	title: 'Trade Life Cycle',
	items: [{
		xtype: 'system-lifecycle-grid',
		tableName: 'Trade'
	}]
};

/**
 * Class to manage active trade restrictions real-time. Listeners can register a callback to be invoked when restrictions are updated.
 */
Clifton.trade.restriction.TradeRestrictionHandler = new class extends TCG.websocket.WebSocketSubscriptionHandler {

	constructor() {
		super('trade-restrictions', '/topic/trade/restriction');
	}


	/**
	 * Returns a list of active trade restrictions
	 * @returns {any[]}
	 */
	getTradeRestrictions() {
		return this.getEntityList();
	}
};

/**
 * Plugin that enables common functionality for applying trade restrictions to client account and security columns.
 * Compatible with both {@link TCG.grid.GridPanel} and {@link TCG.grid.FormGridPanel}.
 */
Clifton.trade.restriction.TradeRestrictionAwarePlugin = Ext.extend(Ext.util.Observable, {
	grid: null,
	accountTradingNotesMap: null,

	constructor: function(config) {
		Object.assign(this, config);
		TCG.callSuper(this, 'constructor', arguments);
	},

	init: function(gridOrPanel) {
		// Unwrap grid panel if necessary
		this.grid = (gridOrPanel instanceof TCG.grid.GridPanel) ? gridOrPanel.grid : gridOrPanel;
		this.lastTradeRestrictionList = [];
		this.accountTradingNotesMap = new Map();
		gridOrPanel.getRestrictionActionMenuItems = (...args) => this.getRestrictionActionMenuItems(...args);
		gridOrPanel.applyTradeRestrictionGridClientAccountColumnFreeze = (...args) => this.applyTradeRestrictionGridClientAccountColumnFreeze(...args);
		gridOrPanel.applyTradeRestrictionGridSecurityColumnFreeze = (...args) => this.applyTradeRestrictionGridSecurityColumnFreeze(...args);
		gridOrPanel.checkTradeRestrictions = (...args) => this.checkTradeRestrictions(...args);
		gridOrPanel.getTradeRestrictionActionForPositions = (...args) => this.getTradeRestrictionActionForPositions(...args);
		gridOrPanel.applyAccountTradingNotes = (...args) => this.applyAccountTradingNotes(...args);

		const columnModel = this.grid.getColumnModel();
		const applyFreezeRenderer = (dataIndex, freezeFunction) => {
			if (TCG.isNotBlank(dataIndex)) {
				const columnIndex = columnModel.findColumnIndex(dataIndex);
				if (columnIndex > -1) {
					const column = columnModel.getColumnById(columnIndex);
					if (column) {
						const previousRenderer = column.renderer;
						column.renderer = (...args) => {
							freezeFunction(...args);
							return previousRenderer ? previousRenderer(...args) : args[0];
						};
					}
				}
			}
		};
		applyFreezeRenderer(this.clientInvestmentAccountColumnDataIndex, gridOrPanel.applyTradeRestrictionGridClientAccountColumnFreeze);
		applyFreezeRenderer(this.securityColumnDataIndex, (...args) => gridOrPanel.applyTradeRestrictionGridSecurityColumnFreeze(args[1], args[2], false));
		applyFreezeRenderer(this.underlyingSecurityColumnDataIndex, (...args) => gridOrPanel.applyTradeRestrictionGridSecurityColumnFreeze(args[1], args[2], true));

		this.addRestrictionEventListeners();
	},

	addRestrictionEventListeners: function() {
		const grid = this.grid;
		const restrictionCallback = tradeRestrictionList => this.applyTradeRestrictionList(tradeRestrictionList);
		grid.on('afterrender', () => {
			grid.store.on('beforeload', () => this.accountTradingNotesMap.clear());
			grid.store.on('load', () => this.applyTradeRestrictionList(Clifton.trade.restriction.TradeRestrictionHandler.getTradeRestrictions()));
			Clifton.trade.restriction.TradeRestrictionHandler.addCallback(restrictionCallback);
		});
		grid.on('destroy', () => Clifton.trade.restriction.TradeRestrictionHandler.removeCallback(restrictionCallback));
	},

	/* Store record property paths */
	getRecordClientInvestmentAccount: record => record.clientInvestmentAccount || record.json.clientInvestmentAccount,
	getRecordInvestmentSecurity: record => record.investmentSecurity || record.json.investmentSecurity,
	/**
	 * Returns the the name of the Quantity field. Used by {@link updateQuantityAfterRestrictionChange} and defaults to null so that
	 * grids must override this function to adjust quantity to 0 if trade restrictions are present.
	 */
	getRecordQuantityName: () => null,

	/* Position object property paths */
	getTradePositionClientInvestmentAccount: function(record) {
		return this.getRecordClientInvestmentAccount(record);
	},
	getTradePositionInvestmentSecurity: function(record) {
		return this.getRecordInvestmentSecurity(record);
	},

	/**
	 * Sets quantity to 0 for records that have trade restrictions and restores quantity if all trade restrictions have
	 * been removed.
	 */
	updateQuantityAfterRestrictionChange: function(record) {
		const quantityName = this.getRecordQuantityName();
		if (TCG.isNotBlank(quantityName)) {
			if ((record.tradeRestrictionList || []).length > 0 && !TCG.isNumber(record.restrictedQuantity)) {
				record.restrictedQuantity = record.get(quantityName);
				record.set(quantityName, 0);
			}
			else if ((record.tradeRestrictionList || []).length === 0 && TCG.isNumber(record.restrictedQuantity)) {
				record.set(quantityName, record.restrictedQuantity);
				record.restrictedQuantity = '';
			}
		}
	},

	/**
	 * Can be overridden to define whether there is a column in the grid for displaying client trade restriction and client notes.
	 * Returns false by default.
	 *
	 * @returns {boolean} true if there is a grid column to display trade restriction and client notes
	 */
	isApplyNotesToRecordColumn: () => false,
	/**
	 * Returns the grid column name for setting the trade restriction and client notes for a record (default value is 'clientAccountNotes').
	 *
	 * @returns {string} client notes column name
	 */
	getNotesRecordColumnName: () => 'clientAccountNotes',
	/**
	 * Applies the trade restriction and client notes applicable to the provided record to the grid column defined by {@link getNotesRecordColumnName}
	 * only if {@link isApplyNotesToRecordColumn} returns true. The result is true only if notes were obtained and set on the record.
	 *
	 * The notes are obtained using {@link getRecordRestrictionNotes}.
	 * Since the result of this is decorated with HTML markup, it is recommended to use the column renderer: {@link TCG.renderTextNowrap}.
	 *
	 * @param record the record to get trade restriction and client notes for and apply the note to
	 * @returns {boolean} true only if notes were obtained and applied to the record
	 */
	updateNotesRecordColumn: function(record) {
		if (TCG.isTrue(this.isApplyNotesToRecordColumn())) {
			const notesColumnName = this.getNotesRecordColumnName();
			if (notesColumnName) {
				const notes = this.getRecordRestrictionNotes(record);
				if (notes && notes.length > 0) {
					record.set(notesColumnName, notes);
					return true;
				}
			}
		}
		return false;
	},

	/**
	 * Updates the applicability and display for all existing trade restrictions to all existing records.
	 *
	 * Note: This operation can be expensive. Record committing requires rewriting the DOM. Rapid committing may cause the UI-thread to hang. This method should be wrapped in a rate-limiting {@link TCG.DebounceBuffer} when redundant execution is likely.
	 */
	applyTradeRestrictionList: function(tradeRestrictions) {
		const tradeRestrictionValues = tradeRestrictions || Clifton.trade.restriction.TradeRestrictionHandler.getTradeRestrictions();
		this.lastTradeRestrictionList = tradeRestrictionValues;

		// function to retrieve the list of all applicable trade restriction values for a given record
		const getTradeRestrictionListForRecord = record => {
			const clientAccount = this.getRecordClientInvestmentAccount(record);
			const clientInvestmentAccountId = clientAccount ? clientAccount.id : void 0;
			const investmentSecurity = this.getRecordInvestmentSecurity(record);
			const investmentSecurityId = investmentSecurity ? investmentSecurity.id : void 0;
			const underlyingSecurityId = (investmentSecurity && investmentSecurity.underlyingSecurity) ? investmentSecurity.underlyingSecurity.id : void 0;
			return tradeRestrictionValues.filter(restriction => (!restriction.clientInvestmentAccount || restriction.clientInvestmentAccount.id === clientInvestmentAccountId)
				&& (!restriction.investmentSecurity || (restriction.investmentSecurity.id === investmentSecurityId || restriction.investmentSecurity.id === underlyingSecurityId)));
		};

		const visibleRows = (this.grid.view instanceof Ext.ux.grid.BufferView) ? this.grid.view.getVisibleRows() : null;
		this.grid.store.each((record, index) => {
			const clientAccount = this.getRecordClientInvestmentAccount(record);
			const investmentSecurity = this.getRecordInvestmentSecurity(record);
			if (clientAccount || investmentSecurity) {
				// Update state of applicable trade restrictions for the record
				const originalRestrictionIdSet = new Set((record.tradeRestrictionList || []).map(restriction => restriction.id));
				record.tradeRestrictionList = getTradeRestrictionListForRecord(record);
				const newRestrictionIdList = record.tradeRestrictionList.map(restriction => restriction.id);
				const restrictionListIsDirty = !(newRestrictionIdList.length === originalRestrictionIdSet.size && newRestrictionIdList.every(id => originalRestrictionIdSet.has(id)));

				// enable record for bulk edits so commit can be batched
				record.beginEdit();
				// apply notes to column if enabled
				let commitRecord = this.updateNotesRecordColumn(record);

				if (restrictionListIsDirty) {
					// Update flags for applied restriction cells
					if (clientAccount) {
						const clientInvestmentAccountId = clientAccount.id;
						record.set('freeze.clientInvestmentAccount', !!record.tradeRestrictionList.find(restriction => restriction.clientInvestmentAccount && restriction.clientInvestmentAccount.id === clientInvestmentAccountId));
					}
					if (investmentSecurity) {
						const investmentSecurityId = investmentSecurity.id;
						record.set('freeze.investmentSecurity', !!record.tradeRestrictionList.find(restriction => restriction.investmentSecurity && restriction.investmentSecurity.id === investmentSecurityId));
						if (investmentSecurity.underlyingSecurity) {
							const underlyingSecurityId = investmentSecurity.underlyingSecurity.id;
							record.set('freeze.underlyingSecurity', !!record.tradeRestrictionList.find(restriction => restriction.investmentSecurity && restriction.investmentSecurity.id === underlyingSecurityId));
						}
					}

					// TODO disabling this for now. We should only be zeroing out the quantity if the restriction is applicable to the row
					// e.g. Restriction for No Open, should allow closing a position. Thus the close quantities should not be zeroed out.
					// This should be safe for now because we already filter out rows with restrictions on submission to the server on DS trade windows.
					// this.updateQuantityAfterRestrictionChange(record);

					commitRecord = true;
				}

				if (commitRecord) {
					// Commit record
					const rowHidden = visibleRows != null && !(index >= visibleRows.first && index <= visibleRows.last);
					record.commit(rowHidden);
				}
				else {
					record.cancelEdit();
				}
			}
		});
	},

	/**
	 * Adds a tooltip to the column's metaData with the clientAccountLabel, the cash balance, and any trade restriction labels applicable to the record.
	 * Adds freeze highlighting if the provided record has a valid trade restriction for the client account of the record.
	 *
	 * This function assumes trade restrictions were applied to the record via {@link Clifton.trade.restriction.TradeRestrictionAwarePlugin.applyTradeRestrictionList}.
	 *
	 * @param clientAccountLabel the full client account label to include in the tooltip
	 * @param metaData the column metadata to add css and a tooltip to
	 * @param record the record to render
	 * @param includeRecordCashBalance true to render the cash balance from the record ('cashBalance') in the tooltip
	 * @param optionalQtipToAppend an optional string value to include in the tooltip before the restriction list
	 */
	applyTradeRestrictionGridClientAccountColumnFreeze: function(clientAccountLabel, metaData, record, includeRecordCashBalance, optionalQtipToAppend) {
		if (metaData && record) {
			const cashBalanceRow = TCG.isTrue(includeRecordCashBalance) && TCG.getValue('cashBalance', record.json)
				? `
					<tr>
						<td nowrap="true" style="width: 100px;">Cash Balance:</td>
						<td>${TCG.renderAmount((TCG.getValue('cashBalance', record.json) || 0), true, '$0,000')}</td>
					</tr>
				`
				: '';

			const restrictionNotes = this.getRecordRestrictionNotes(record);
			const restrictionNotesTable = TCG.trimWhitespace(`<table cellspacing="5" bgcolor="#eee">
					${cashBalanceRow}
					${restrictionNotes}
				</table>`);

			metaData.attr = TCG.renderQtip(TCG.trimWhitespace(`
				${clientAccountLabel}
				${optionalQtipToAppend ? optionalQtipToAppend : ''}
				${restrictionNotesTable}
			`));

			// Attach style for restrictions involving the account
			if (record.get('freeze.clientInvestmentAccount')) {
				metaData.css += ' frozen';
			}
		}
	},

	/**
	 * Returns a string value of HTML decorated trade restriction and client account notes.
	 * If there are no restrictions or notes for the record, an empty string is returned.
	 *
	 * @param record the record to generate notes for
	 * @returns {string} the notes string for the provided record
	 * @private
	 */
	getRecordRestrictionNotes: function(record) {
		if (record) {
			this.applyAccountTradingNotes(record);

			// Attach tooltip
			const tradeRestrictionLabelList = (record.tradeRestrictionList || [])
				.slice(0, 5)
				.map(restriction => restriction.label)
				.map(restrictionLabel => `${restrictionLabel.replace(/[\r\n](\r|\n|.)+/g, '...')}`)
				.map(restrictionLabel => `<li style="padding-bottom:10px;">${restrictionLabel}</li>`)
				.sort();
			if ((record.tradeRestrictionList || []).length > 5) {
				tradeRestrictionLabelList.push('<li>...</li>');
			}
			const restrictionRow = tradeRestrictionLabelList.length > 0
				? `
					<tr>
						<td colspan="2">
							<div>Restrictions:</div>
							<ul>${tradeRestrictionLabelList.join('')}</ul>
						</td>
					</tr>
				`
				: '';

			const notesList = [];
			if (TCG.isNotBlank(record.tradeDirectionNoteList)) {
				for (const entry of record.tradeDirectionNoteList) {
					notesList.push('<li>' + entry.text + '</li>');
				}
			}
			const notesRow = notesList.length > 0
				? `
					<tr>
						<td colspan="2">
							<div>Trading Direction Notes:</div>
							<ul>${notesList.join('')}</ul>
						</td>
					</tr>
				`
				: '';

			return TCG.trimWhitespace(`
					${restrictionRow}
					${notesRow}
					`);
		}
	},

	/**
	 * Adds freeze highlighting if the provided record has a valid trade restriction for the security of the record.
	 *
	 * This function assumes trade restrictions were applied to the record via {@link Clifton.trade.restriction.TradeRestrictionAwarePlugin.applyTradeRestrictionList}.
	 *
	 * @param metaData the column metadata to add css and a tooltip to
	 * @param record the record being rendered
	 * @param underlyingSecurity boolean value of whether the security to check for is an underlying security
	 */
	applyTradeRestrictionGridSecurityColumnFreeze: (metaData, record, underlyingSecurity) => {
		if (metaData && record) {
			// Attach style for restrictions involving the security
			if (record.get('freeze.' + (underlyingSecurity ? 'underlyingSecurity' : 'investmentSecurity'))) {
				metaData.css += ' frozen';
			}
		}
	},

	/**
	 * A facility to retrieve, cache, and assign an client investment account's active 'Trading Direction Notes' to a data store record.
	 * @param record - record (containing the client investment account) to update with trading direction notes
	 *
	 * Note:  this function is defined here to allow reuse with other grids owned by this window.
	 */
	applyAccountTradingNotes: function(record) {
		if (TCG.isBlank(record)) {
			return;
		}

		const clientAccount = this.getRecordClientInvestmentAccount(record);

		if (TCG.isNotBlank(this.accountTradingNotesMap) && this.accountTradingNotesMap.has(clientAccount.id)) {
			record.tradeDirectionNoteList = this.accountTradingNotesMap.get(clientAccount.id);
			return;
		}

		const url = 'systemNoteListForEntityFind.json?' + 'UI_SOURCE=TradeRestrictionAwarePlugin&entityTableName=InvestmentAccount&active=true&noteTypeName=Trading Direction';
		record.tradeDirectionNoteList = TCG.data.getData(url, this.grid, null, {
			params: {
				fkFieldId: clientAccount.id,
				requestedPropertiesRoot: 'data',
				requestedProperties: 'text',
				requestedPropertiesToExcludeGlobally: 'systemNoteSearchForm'
			}
		});
		this.accountTradingNotesMap.set(clientAccount.id, record.tradeDirectionNoteList);
	},

	/**
	 * A function for validation of trade restrictions when the target investmentSecurity and trade open/close type are known.  This is
	 * useful to facilitate trade restriction when opening a new position.
	 *
	 * @param clientAccountId - client account ID for the trade.
	 * @param investmentSecurity - investment security to be traded, if blank, only the underlying security will be evaluated.
	 * @param underlyingSecurityId - id of underlying security of investmentSecurity.
	 * @param underlyingPrice - last price of underlying security.
	 * @param isSell - boolean flag that if true, indicates this is a sell trade, otherwise it is a buy trade.
	 * @param isOpen - boolean flag that if true indicates this is an opening trade, otherwise it is a closing trade.
	 * @returns {boolean} true if there are no active trade restrictions for any of the positions, false if there are.
	 */
	checkTradeRestrictions: function(clientAccountId, investmentSecurity, underlyingSecurityId, underlyingPrice, isSell, isOpen) {
		const investmentSecurityId = TCG.isNotBlank(investmentSecurity) ? investmentSecurity.id : null;
		const allTradeRestrictions = this.lastTradeRestrictionList;
		const filteredTradeRestrictions = allTradeRestrictions.filter(restriction => !restriction.investmentSecurity || restriction.investmentSecurity.id === investmentSecurityId || restriction.investmentSecurity.id === underlyingSecurityId);

		let optionType = null;
		let strikePrice = null;
		let investmentTypeName = null;
		if (TCG.isNotBlank(investmentSecurity)) {
			investmentTypeName = investmentSecurity.instrument.hierarchy.investmentType.name;
			optionType = investmentTypeName === 'Options' ? investmentSecurity.optionType : null;
			strikePrice = investmentTypeName === 'Options' ? investmentSecurity.optionStrikePrice : null;
		}

		const tradeRestrictionList = filteredTradeRestrictions.filter(restriction => (!restriction.clientInvestmentAccount || restriction.clientInvestmentAccount.id === clientAccountId));
		if (tradeRestrictionList.length === 0) {
			return true;
		}

		// process each trade restriction, return false if any restrictions pertain to the TradeOpenCloseType
		for (const tradeRestriction of tradeRestrictionList) {
			const tradeRestrictionType = tradeRestriction.restrictionType.name;
			switch (tradeRestrictionType) {
				case 'No Trade':
				case 'Not Reconciled':
					return false;
				case 'No Buy':
					if (!isSell) {
						return false;
					}
					break;
				case 'No Sell':
					if (isSell) {
						return false;
					}
					break;
				case 'No Sell-to-Open':
					if (isSell && isOpen) {
						return false;
					}
					break;
				case 'No Open':
					if (isOpen) {
						return false;
					}
					break;
				case 'S/O Minimum Strike Price':
					// special handling for limit trade restrictions -- limits apply to call option trading
					if (isSell && isOpen && investmentTypeName === 'Options' && optionType === 'CALL') {
						if (strikePrice < tradeRestriction.restrictionValue) {
							return false;
						}
					}
					break;
				case 'S/O Minimum Underlying Price':
					if (isSell && isOpen && investmentTypeName === 'Options' && optionType === 'CALL') {
						if (TCG.isBlank(underlyingPrice)) {
							const errorMessage = `Unable to evaluate restrictions due to undefined underlying price for security [${investmentSecurity && investmentSecurity.label}].`;
							TCG.showError(errorMessage);
							throw new Error(errorMessage);
						}
						if (underlyingPrice < tradeRestriction.restrictionValue) {
							return false;
						}
					}
					break;
				default:
				// Ignore unrecognized restriction types
			}
		}
		return true;
	},
	getRestrictionActionMenuItems: function(rowIndex, column) {
		const position = this.grid.store.getAt(rowIndex);
		const tradeRestrictionList = position.tradeRestrictionList || [];
		const accountRestrictionList = tradeRestrictionList.filter(restriction => restriction.clientInvestmentAccount && !restriction.investmentSecurity);
		const positionRestrictionList = tradeRestrictionList.filter(restriction => restriction.clientInvestmentAccount && restriction.investmentSecurity);
		const securityRestrictionList = tradeRestrictionList.filter(restriction => !restriction.clientInvestmentAccount && restriction.investmentSecurity);

		// Addition menu items
		const menuAdditionItemList = [
			{
				text: 'Add Account Restriction',
				tooltip: 'Add a Trade Restriction for the client account',
				iconCls: 'rule',
				handler: () => TCG.createComponent('Clifton.trade.restriction.TradeRestrictionWindow', {
					defaultData: {
						clientInvestmentAccount: this.getRecordClientInvestmentAccount(position)
					}
				})
			},
			{
				text: 'Add Position Restriction',
				tooltip: 'Add a Trade Restriction for the position (client account and security)',
				iconCls: 'rule',
				handler: () => TCG.createComponent('Clifton.trade.restriction.TradeRestrictionWindow', {
					defaultData: {
						clientInvestmentAccount: this.getRecordClientInvestmentAccount(position),
						investmentSecurity: this.getRecordInvestmentSecurity(position)
					}
				})
			},
			{
				text: 'Add Security Restriction',
				tooltip: 'Add a Trade Restriction for the security',
				iconCls: 'rule',
				handler: () => TCG.createComponent('Clifton.trade.restriction.TradeRestrictionWindow', {
					defaultData: {
						investmentSecurity: this.getRecordInvestmentSecurity(position)
					}
				})
			}
		];

		// View and Removal menu items
		const menuViewItemList = [];
		const menuRemovalItemList = [];
		const plugin = this;
		if (accountRestrictionList.length > 0) {
			menuViewItemList.push(createViewMenuItem(accountRestrictionList, {
				text: 'View Account Restriction',
				tooltip: 'View an account Trade Restriction',
				includeAccount: true
			}));
			menuRemovalItemList.push(createDisableMenuItem(accountRestrictionList, {
				text: 'Remove Account Restriction',
				tooltip: 'Remove an account Trade Restriction',
				includeAccount: true
			}));
		}
		if (positionRestrictionList.length > 0) {
			menuViewItemList.push(createViewMenuItem(positionRestrictionList, {
				text: 'View Position Restriction',
				tooltip: 'View a position Trade Restriction',
				includeAccount: true,
				includeSecurity: true
			}));
			menuRemovalItemList.push(createDisableMenuItem(positionRestrictionList, {
				text: 'Remove Position Restriction',
				tooltip: 'Remove a position Trade Restriction',
				includeAccount: true,
				includeSecurity: true
			}));
		}
		if (securityRestrictionList.length > 0) {
			menuViewItemList.push(createViewMenuItem(securityRestrictionList, {
				text: 'View Security Restriction',
				tooltip: 'View a security Trade Restriction',
				includeSecurity: true
			}));
			menuRemovalItemList.push(createDisableMenuItem(securityRestrictionList, {
				text: 'Remove Security Restriction',
				tooltip: 'Remove a security Trade Restriction',
				includeSecurity: true
			}));
		}

		const menuClientTradingDirectionItemList = ['-',
			{
				text: 'Add Client Trading Direction Note',
				tooltip: 'Add a Client Account note for Trading Direction',
				iconCls: 'pencil',
				handler: async () => {
					const clientId = this.getRecordClientInvestmentAccount(position).id;
					const table = await TCG.data.getDataPromiseUsingCaching('systemTableByName.json?tableName=InvestmentAccount', this.grid, 'system.table.InvestmentAccount');
					const noteType = await TCG.data.getDataPromiseUsingCaching('systemNoteTypeByTableAndName.json?noteTypeName=Trading Direction&tableId=' + table.id, this.grid, 'system.noteType.investmentAccount.Trading Direction');
					TCG.createComponent('Clifton.system.note.SingleNoteWindow', {
						defaultData: {
							noteType: noteType ? noteType : {table: table},
							linkedToMultiple: false,
							fkFieldId: clientId
						},
						openerCt: this
					});
				}
			},
			{
				text: 'Manage Client Notes',
				tooltip: 'Open the client\'s notes to add or remove notes',
				iconCls: 'pencil',
				handler: () => {
					const componentName = 'Clifton.investment.account.CliftonAccountWindow';
					const clientId = this.getRecordClientInvestmentAccount(position).id;
					TCG.createComponent(componentName, {
						id: TCG.getComponentId(componentName, clientId),
						params: {id: clientId},
						defaultActiveTabName: 'Notes',
						openerCt: this
					});
				}
			}
		];

		return [
			...menuAdditionItemList,
			...menuViewItemList.length > 0 ? ['-', ...menuViewItemList] : [],
			...menuRemovalItemList.length > 0 ? ['-', ...menuRemovalItemList] : [],
			...menuClientTradingDirectionItemList
		];

		function createViewMenuItem(restrictionList, {text, tooltip, includeAccount, includeSecurity}) {
			return {
				text: text,
				tooltip: tooltip,
				iconCls: 'rule',
				menu: {
					items: restrictionList
						.slice(0, 5)
						.map(restriction => ({
							text: Ext.util.Format.ellipsis(restriction.label, 90),
							iconCls: 'rule',
							handler: () => TCG.createComponent('Clifton.trade.restriction.TradeRestrictionWindow', {params: {id: restriction.id}})
						}))
				}
			};
		}

		function createDisableMenuItem(restrictionList, {text, tooltip, includeAccount, includeSecurity}) {
			return {
				text: text,
				tooltip: tooltip,
				iconCls: 'rule',
				menu: {
					items: restrictionList
						.slice(0, 5)
						.map(restriction => ({
							text: Ext.util.Format.ellipsis(restriction.label, 90),
							iconCls: 'rule',
							handler: () => TCG.createComponent('Clifton.trade.restriction.TradeRestrictionDisableWindow', {
								defaultData: {
									tradeRestriction: restriction,
									...includeAccount ? {clientInvestmentAccount: plugin.getRecordClientInvestmentAccount(position)} : {},
									...includeSecurity ? {investmentSecurity: plugin.getRecordInvestmentSecurity(position)} : {}
								}
							})
						}))
				}
			};
		}
	},

	/**
	 * Function to confirm whether or not the user would like to proceed with a trade even though some of the positions have
	 * trade restrictions and must be excluded from the trade.
	 *
	 * @template T
	 * @param {{restrictedPositions: T, validPositions: T}[]} positionEvaluations that should be excluded from the trade
	 * @returns {Promise<T[] | boolean>} positions to include, or <code>false</code> if the action should be cancelled
	 */
	getTradeRestrictionActionForPositions: async function(positionEvaluations) {
		// Guard-clause: No restrictions apply
		if (positionEvaluations.restrictedPositions.length === 0) {
			return [...positionEvaluations.validPositions];
		}
		const restrictedPositionList = positionEvaluations.restrictedPositions
			.map(position => `<li>${this.getTradePositionClientInvestmentAccount(position).label} : ${this.getTradePositionInvestmentSecurity(position).label}</li>`)
			.join('');
		const includeRestrictedPositions = await TCG.showConfirm(`The following accounts and securities have trade restrictions that prohibit this action:<br/><br/><ul style="list-style: square; padding-inline-start: 40px;">${restrictedPositionList}</ul><br/>Would you like to include these items, exclude these items, or cancel the action?`, 'Trade Restriction Violations', {
			buttons: {yes: 'Include', no: 'Exclude', cancel: true},
			fn: (response, resolve) => resolve(response),
			minWidth: 600
		});
		let resultPositions;
		switch (includeRestrictedPositions) {
			case 'yes':
				resultPositions = [...positionEvaluations.restrictedPositions, ...positionEvaluations.validPositions];
				break;
			case 'no':
				resultPositions = [...positionEvaluations.validPositions];
				break;
			default:
				resultPositions = false;
		}

		// Handle empty accepted position list
		if (resultPositions && resultPositions.length === 0) {
			TCG.showError('All selected positions have active trade restrictions that prohibit this action.', 'Unable to Trade', {minWidth: 500});
			resultPositions = false;
		}
		return resultPositions;
	}
});
Ext.preg('trade-restriction-aware', Clifton.trade.restriction.TradeRestrictionAwarePlugin);

Clifton.trade.blotter.BaseGridPanel = Ext.extend(TCG.grid.GridPanel, {
	getTopToolbarFilters: function(toolbar) {
		return Clifton.trade.blotter.getCommonToolbarFilters();
	},

	applyTopToolbarFilterLoadParams: function(params) {
		return Clifton.trade.blotter.applyTopToolbarFilterLoadParams(this, params);
	}
});
Ext.reg('trade-blotter-base-gridpanel', Clifton.trade.blotter.BaseGridPanel);

Clifton.trade.blotter.getCommonToolbarFilters = function() {
	return [
		{
			fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow',
			tooltip: 'Filter trades for a security belonging to an Instrument Group.'
		},
		{
			fieldLabel: 'Client Acct Group', name: 'clientGroupName', hiddenName: 'clientInvestmentAccountGroupId', width: 150, xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow',
			tooltip: 'Filter trades of a client account belonging to a Client Account Group.'
		},
		{fieldLabel: 'Team', width: 100, minListWidth: 150, xtype: 'combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientInvestmentAccount.teamSecurityGroup.name'}
	];
};

Clifton.trade.blotter.mergeToCommonToolbarFilters = function(filtersToAppend) {
	const commonFilters = Clifton.trade.blotter.getCommonToolbarFilters.call();
	// Include the common items first and append the parent's to the end. Replace any
	// common item matches with that from the parent to update the field reference.
	const mergedFilters = [].concat(commonFilters);
	for (let i = 0; i < filtersToAppend.length; i++) {
		let found = false;
		for (let j = 0; j < commonFilters.length; j++) {
			if (filtersToAppend[i].fieldLabel === commonFilters[j].fieldLabel) {
				found = true;
				mergedFilters[j] = filtersToAppend[i];
				break;
			}
		}
		if (!found) {
			mergedFilters.push(filtersToAppend[i]);
		}
	}
	return mergedFilters;
};

Clifton.trade.blotter.applyTopToolbarFilterLoadParams = function(gridPanel, params) {
	const t = gridPanel.getTopToolbar();
	let grp = TCG.getChildByName(t, 'clientGroupName');
	if (grp && !TCG.isBlank(grp.getValue())) {
		params.clientInvestmentAccountGroupId = grp.getValue();
	}
	grp = TCG.getChildByName(t, 'investmentGroupId');
	if (grp && !TCG.isBlank(grp.getValue())) {
		params.investmentGroupId = grp.getValue();
	}
	return params;
};

Clifton.trade.blotter.addTradeMenu = function(toolbar, grid) {
	toolbar.add({
			text: 'New Trade',
			iconCls: 'add',
			menu: new Ext.menu.Menu({
				items: [
					{
						text: 'Single Client',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Trade Bonds',
								iconCls: 'bonds',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Bonds', openerCt: grid});
								}
							}, {
								text: 'Trade Currency',
								iconCls: 'currency',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Currency', openerCt: grid});
								}
							}, {
								text: 'Trade Currency Forwards',
								iconCls: 'currency-forward',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Forwards', openerCt: grid});
								}
							}, {
								text: 'Trade Funds',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Funds', openerCt: grid});
								}
							}, {
								text: 'Trade Futures',
								iconCls: 'futures',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Futures', openerCt: grid});
								}
							}, {
								text: 'Trade Notes',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Notes', openerCt: grid});
								}
							}, {
								text: 'Trade Options',
								iconCls: 'options',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Options', openerCt: grid});
								}
							}, {
								text: 'Trade Stocks',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Stocks', openerCt: grid});
								}
							}, {
								text: 'Trade Swaps',
								iconCls: 'swap',
								menu: {
									items: [{
										text: 'Credit Default Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps: CDS', openerCt: grid});
										}
									}, {
										text: 'Interest Rate Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps: IRS', openerCt: grid});
										}
									}, {
										text: 'Total Return Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps: TRS', openerCt: grid});
										}
									}, {
										text: 'Other Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps', openerCt: grid});
										}
									}]
								}
							}, {
								text: 'Trade Swaps - Cleared',
								iconCls: 'swap',
								menu: {
									items: [{
										text: 'Credit Default Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: CDS', openerCt: grid});
										}
									}, {
										text: 'Interest Rate Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: IRS', openerCt: grid});
										}
									}, {
										text: 'Inflation Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: INFS', openerCt: grid});
										}
									}, {
										text: 'Zero Coupon Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: ZCS', openerCt: grid});
										}
									}]
								}
							}, {
								text: 'Trade Swaptions',
								iconCls: 'options',
								menu: {
									items: [{
										text: 'Credit Default Swaptions',
										iconCls: 'options',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaptions: CDS', openerCt: grid});
										}
									}, {
										text: 'Interest Rate Swaptions',
										iconCls: 'options',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaptions: IRS', openerCt: grid});
										}
									}]
								}
							}, {
								text: 'Trade EFP',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.EFPTradeWindow');
								}
							}]
						})
					},
					{
						text: 'Multiple Clients',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Trade Bonds',
								iconCls: 'bonds',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.BondTradeEntryWindow', {tradeType: 'Bonds', openerCt: grid});
								}
							}, {
								text: 'Trade Funds',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Funds', openerCt: grid});
								}
							}, {
								text: 'Trade Futures',
								iconCls: 'futures',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Futures', openerCt: grid});
								}
							}, {
								text: 'Trade Notes',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Notes', openerCt: grid});
								}
							}, {
								text: 'Trade Options',
								iconCls: 'options',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Options'});
								}
							}, {
								text: 'Trade Stocks',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Stocks', openerCt: grid});
								}
							}]
						})
					},
					'-',
					{
						text: 'Generic Trade Import',
						iconCls: 'import',
						handler: function() {
							TCG.createComponent('Clifton.trade.upload.TradeUploadWindow');
						}
					}, {
						text: 'Currency Trade Import',
						iconCls: 'import',
						handler: function() {
							TCG.createComponent('Clifton.trade.upload.CurrencyTradeUploadWindow', {simpleTradeTypeName: 'Currency'});
						}
					}, {
						text: 'Forwards Trade Import',
						iconCls: 'import',
						handler: function() {
							TCG.createComponent('Clifton.trade.upload.CurrencyTradeUploadWindow', {simpleTradeTypeName: 'Forwards'});
						}
					}, {
						text: 'Options Trade Import',
						iconCls: 'import',
						handler: function() {
							TCG.createComponent('Clifton.trade.upload.OptionTradeUploadWindow', {simpleTradeTypeName: 'Forwards'});
						}
					}, '-',
					{
						text: 'Trade Fill Import',
						iconCls: 'import',
						handler: function() {
							TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
						}
					}]
			})
		}, '-',
		{
			text: 'Trade Futures',
			iconCls: 'futures',
			handler: function() {
				TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Futures', openerCt: grid});
			}
		}, '-');
};

Clifton.trade.blotter.AllTradesGridPanel = Ext.extend(Clifton.trade.blotter.BaseGridPanel, {
	name: 'tradeListFind',
	instructions: 'Lists trades details for all trades (excludes Roll Trades). Defaults trades to today\'s and future trades for current user\' team.',
	viewNames: ['Default View', 'Export Friendly'],

	standardColumnViewNames: {'createDate': ['Export Friendly'], 'updateDate': ['Export Friendly']},
	rowSelectionModel: 'multiple',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true, viewNames: ['Export Friendly']},
		{header: 'Order ID', width: 15, dataIndex: 'orderIdentifier', hidden: true, type: 'int', doNotFormat: true, useNull: true},
		{header: 'Workflow Status', width: 15, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Workflow State', width: 15, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, defaultSortColumn: true, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Trade Type', width: 11, hidden: true, dataIndex: 'tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}, viewNames: ['Export Friendly']},
		{header: 'Trade Group Type', width: 11, hidden: true, dataIndex: 'tradeGroup.tradeGroupType.name', filter: {type: 'combo', searchFieldName: 'tradeGroupTypeId', url: 'tradeGroupTypeListFind.json'}, viewNames: ['Export Friendly']},
		{header: 'Client Account', width: 40, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Executing Broker', width: 30, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Executing Sponsor', width: 30, dataIndex: 'executingSponsorCompany.name', filter: {type: 'combo', searchFieldName: 'executingSponsorCompanyId', url: 'businessCompanyListFind.json?companyType==Broker'}, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Trade Note', width: 30, dataIndex: 'description', hidden: true, renderer: TCG.renderTextNowrapWithTooltip, viewNames: ['Export Friendly']},
		{
			header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean', viewNames: ['Default View', 'Export Friendly'],
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
			renderer: function(v, metaData) {
				metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Quantity', width: 12, dataIndex: 'quantityIntended', type: 'float', useNull: true, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Security Name', width: 22, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['Export Friendly']},
		{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true, viewNames: ['Export Friendly']},
		{header: 'Avg Price', width: 14, dataIndex: 'averageUnitPrice', type: 'float', useNull: true, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Expected Price', width: 15, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
		{header: 'Accounting Notional', width: 19, dataIndex: 'accountingNotional', type: 'currency', viewNames: ['Default View', 'Export Friendly']},
		{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
		{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true, viewNames: ['Export Friendly']},
		{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
		{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
		{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
		{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}, defaultSortColumn: true, viewNames: ['Default View', 'Export Friendly']},
		{header: 'FIX', width: 7, dataIndex: 'fixTrade', type: 'boolean', hidden: true, viewNames: ['Export Friendly']},
		{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
		{header: 'Destination', width: 13, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}, viewNames: ['Default View', 'Export Friendly']},
		{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
		{header: 'Execution Type', width: 14, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
		{header: 'Limit Price', width: 13, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
		{header: 'Time In Force', width: 14, dataIndex: 'tradeTimeInForceType.label', filter: {type: 'combo', searchFieldName: 'tradeTimeInForceTypeId', displayField: 'label', url: 'tradeTimeInForceTypeListFind.json'}, hidden: true, tooltip: 'A future date which represents the last day the order will remain open while waiting for a fill.'},
		{header: 'Expires', width: 13, dataIndex: 'timeInForceExpirationDate', hidden: true, tooltip: 'A future date which represents the last day the order will remain open while waiting for a fill.'},
		{header: 'External Identifier', width: 13, dataIndex: 'externalTradeIdentifier', searchFieldName: 'externalTradeIdentifier', hidden: true},
		{header: 'Traded On', width: 13, dataIndex: 'tradeDate', searchFieldName: 'tradeDate', viewNames: ['Default View', 'Export Friendly']},
		{header: 'Settled On', width: 13, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true, viewNames: ['Export Friendly']}
	],
	getLoadParams: async function(firstLoad) {
		if (firstLoad) {
			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(this);
			if (TCG.isNotNull(team)) {
				this.setFilterValue('clientInvestmentAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
			}
			this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
		}
		return this.applyTopToolbarFilterLoadParams({
			readUncommittedRequested: true,
			excludeTradeGroupTypeName: 'Roll Trade'
		});
	},
	editor: {
		detailPageClass: 'Clifton.trade.TradeWindow',
		drillDownOnly: true,
		addEditButtons: function(toolBar, gridPanel) {
			Clifton.trade.blotter.addTradeMenu(toolBar, gridPanel);

			const gridEditor = this;

			toolBar.add({
				text: 'Add Note',
				tooltip: 'Add a note linked to selected trades (NOTE: Maximum of 50 trades can be selected)',
				iconCls: 'pencil',
				scope: this,
				handler: function() {
					const grid = gridPanel.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one trade to add note to.', 'No Row(s) Selected');
					}
					else {
						const ut = sm.getSelections();
						gridEditor.addTradeNote(ut, gridEditor, gridPanel);
					}
				}
			});
			toolBar.add('-');
		},
		addTradeNote: function(rows, editor, gridPanel) {
			Clifton.trade.blotter.addTradeNote(rows, editor, gridPanel, 'id', 'tradeType.name', false, 50);
		}
	},
	configureToolsMenu: function(menu) {
		menu.add({
			text: 'Manual Trade Allocation',
			tooltip: 'View selected trades by executing broker for viewing and sending manual trade allocations',
			iconCls: 'export',
			handler: function() {
				const tradeIdList = [];
				const gp = this.parentMenu.ownerCt.ownerCt.ownerCt;
				const selectionModel = gp.grid.getSelectionModel();
				selectionModel.selections.items.forEach(row => {
					tradeIdList.push(row.get('id'));
				});
				if (tradeIdList.length === 0) {
					TCG.showError('No trades selected. Please select one or more trades from the grid.', 'Data Submission Error');
				}
				else {
					TCG.createComponent('Clifton.trade.TradeAllocationWindow', {defaultData: {tradeIds: tradeIdList}});
				}
			}
		});
	}
});
Ext.reg('trade-blotter-all-trades-gridpanel', Clifton.trade.blotter.AllTradesGridPanel);

Clifton.trade.blotter.DraftTradesGridPanel = Ext.extend(Clifton.trade.blotter.BaseGridPanel, {
	name: 'tradeListFind',
	instructions: 'Draft trades include trades that have not been submitted for approval as well as trades that failed validation.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Order ID', width: 15, dataIndex: 'orderIdentifier', hidden: true, type: 'int', doNotFormat: true, useNull: true},
		{header: 'Workflow State', width: 15, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, defaultSortColumn: true},
		{header: 'Client Account', width: 35, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true},
		{
			header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
			renderer: function(v, metaData) {
				metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
		{header: 'Security', width: 20, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', width: 20, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
		{header: 'Avg Price', width: 14, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Expected Price', width: 15, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
		{header: 'Accounting Notional', width: 18, dataIndex: 'accountingNotional', type: 'currency'},
		{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
		{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
		{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
		{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
		{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
		{header: 'Trader', width: 15, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}},
		{header: 'FIX', width: 7, dataIndex: 'fixTrade', type: 'boolean', hidden: true},
		{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
		{header: 'Destination', width: 12, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
		{header: 'Executing Broker', width: 20, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
		{header: 'Execution Type', width: 13, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
		{header: 'Trade Note', width: 20, dataIndex: 'description', renderer: TCG.renderTextNowrapWithTooltip},
		{header: 'Limit Price', width: 15, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
		{header: 'Traded On', width: 12, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
		{header: 'Settled On', width: 12, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}
	],
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			const trader = TCG.getCurrentUser();
			this.setFilterValue('traderUser.label', {value: trader.id, text: trader.label});
		}
		return this.applyTopToolbarFilterLoadParams({
			readUncommittedRequested: true,
			workflowStatusNameEquals: 'Draft',
			excludeTradeGroupTypeName: 'Roll Trade'
		});
	},
	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add(Clifton.trade.ReassignmentToolbarButton(gridPanel));
		toolBar.add('-');
	},
	editor: {
		detailPageClass: 'Clifton.trade.TradeWindow',
		drillDownOnly: true,
		addEditButtons: function(t, gridPanel) {
			Clifton.trade.blotter.addTradeMenu(t, gridPanel);
		}
	}
});
Ext.reg('trade-blotter-draft-trades-gridpanel', Clifton.trade.blotter.DraftTradesGridPanel);

Clifton.trade.blotter.PendingTradesGridPanel = Ext.extend(Clifton.trade.blotter.BaseGridPanel, {
	name: 'tradeListFind',
	instructions: 'Pending trades are trades that are waiting to be approved. Select the trades that you want to approve and click the approve button.',
	rowSelectionModel: 'checkbox',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Order ID', width: 15, dataIndex: 'orderIdentifier', hidden: true, type: 'int', doNotFormat: true, useNull: true},
		{header: 'Client Account', width: 35, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true},
		{
			header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
			renderer: function(v, metaData) {
				metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Quantity', width: 12, dataIndex: 'quantityIntended', type: 'float', useNull: true},
		{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', width: 22, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
		{header: 'Avg Price', width: 14, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Expected Price', width: 15, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
		{header: 'Accounting Notional', width: 18, dataIndex: 'accountingNotional', type: 'currency'},
		{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
		{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
		{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
		{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
		{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}},
		{header: 'Trader', width: 14, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}, defaultSortColumn: true},
		{header: 'FIX', width: 7, dataIndex: 'fixTrade', type: 'boolean', hidden: true},
		{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
		{header: 'Destination', width: 12, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
		{header: 'Executing Broker', width: 20, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
		{header: 'Execution Type', width: 13, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
		{header: 'Trade Note', width: 25, dataIndex: 'description', renderer: TCG.renderTextNowrapWithTooltip},
		{header: 'Limit Price', width: 11, dataIndex: 'limitPrice', type: 'float', useNull: true},
		{header: 'Traded On', width: 11, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
		{header: 'Settled On', width: 11, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}

	],
	getLoadParams: async function(firstLoad) {
		if (firstLoad) {
			// try to get trader's team: only if one
			const team = await Clifton.security.user.getUserTeam(this);
			if (TCG.isNotNull(team)) {

				this.setFilterValue('clientInvestmentAccount.teamSecurityGroup.name', {value: team.id, text: team.name});
			}
		}
		return this.applyTopToolbarFilterLoadParams({
			readUncommittedRequested: true,
			workflowStatusNameEquals: 'Pending',
			excludeTradeGroupTypeName: 'Roll Trade'
		});
	},
	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add(Clifton.trade.ReassignmentToolbarButton(gridPanel));
		toolBar.add('-');
	},
	editor: {
		detailPageClass: 'Clifton.trade.TradeWindow',
		drillDownOnly: true,
		ptype: 'workflowAwareEntity-grideditor',
		executeMultipleInBatch: true,
		// ignore all response data, which is ignored to speed up each row's transition request
		requestedPropertiesToExclude: 'workflowTransitionCommand',
		tableName: 'Trade',
		transitionWorkflowStateList: [
			{stateName: 'Approved', iconCls: 'run', buttonText: 'Approve Selected', buttonTooltip: 'Approved Selected Trades'},
			{stateName: 'Cancelled', iconCls: 'run', buttonText: 'Cancel Selected', buttonTooltip: 'Cancel Selected Trades', requireConfirm: true}
		],
		validateRowsToBeTransitioned: Clifton.trade.validateWorkflowTransition,
		sortList: function(rows, stateName, gridEditor, gridPanel) {
			// sort trades by tradeDate and then averageUnitPrice: proper booking order
			const trades = [];
			trades[0] = rows[0];
			for (let i = 1; i < rows.length; i++) {
				const n = rows[i].data;
				for (let j = 0; j < trades.length; j++) {
					const o = trades[j].data;
					if (n.tradeDate >= o.tradeDate && n.averageUnitPrice > o.averageUnitPrice) {
						continue;
					}
					for (let k = trades.length; k > j; k--) {
						trades[k] = trades[k - 1];
					}
					trades[j] = rows[i];
					break;
				}
				if (trades.length === i) {
					trades[i] = rows[i];
				}
			}
			return trades;
		}
	}
});
Ext.reg('trade-blotter-pending-trades-gridpanel', Clifton.trade.blotter.PendingTradesGridPanel);

Clifton.trade.blotter.ApprovedTradesGridPanel = Ext.extend(Clifton.trade.blotter.BaseGridPanel, {
	name: 'tradeListFind',
	rowSelectionModel: 'checkbox',
	instructions: 'Approved trades have been validated, approved and are waiting for execution.',
	pageSize: 500,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Order ID', width: 15, dataIndex: 'orderIdentifier', hidden: true, type: 'int', doNotFormat: true, useNull: true},
		{header: 'Client Account', width: 35, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true},
		{
			header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{
			header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeList.json', loadAll: true}, viewNames: ['Export Friendly'],
			renderer: function(v, metaData) {
				metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
		{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, defaultSortColumn: true},
		{header: 'Security Name', width: 22, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
		{header: 'Avg Price', width: 14, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Expected Price', width: 15, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
		{header: 'Accounting Notional', width: 15, dataIndex: 'accountingNotional', type: 'currency'},
		{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
		{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
		{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
		{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
		{header: 'Team', width: 15, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
		{header: 'Trader', width: 14, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}},
		{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
		{header: 'Destination', width: 12, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
		{header: 'Executing Broker', width: 20, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Source', width: 16, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
		{header: 'Execution Type', width: 16, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}},
		{header: 'Trade Note', width: 20, dataIndex: 'description', renderer: TCG.renderTextNowrapWithTooltip},
		{header: 'Limit Price', width: 15, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
		{header: 'Trade Group', width: 12, dataIndex: 'tradeGroup.id', type: 'int', doNotFormat: true, hidden: true, filter: {searchFieldName: 'tradeGroupId'}},
		{header: 'Trade Group Type', width: 13, dataIndex: 'tradeGroup.tradeGroupType.name', hidden: true, filter: {comparison: 'EQUALS', searchFieldName: 'includeTradeGroupTypeName'}},
		{header: 'Traded On', width: 11, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
		{header: 'Settled On', width: 11, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}
	],
	addToolbarButtons: function(toolBar, gridPanel) {
		toolBar.add(Clifton.trade.ReassignmentToolbarButton(gridPanel));
		toolBar.add('-');
	},
	getTopToolbarFilters: function(toolbar) {
		const parentFilters = Clifton.trade.blotter.BaseGridPanel.prototype.getTopToolbarFilters.call(this, toolbar);
		// Real estate is an issue with this tab, so modify the filters to fit
		for (let i = 0; i < parentFilters.length; i++) {
			if (parentFilters[i].fieldLabel === 'Instrument Group') {
				parentFilters[i].fieldLabel = 'Instr Grp';
				parentFilters[i].width = 125;
			}
			else if (parentFilters[i].fieldLabel === 'Client Acct Group') {
				parentFilters[i].fieldLabel = 'Clnt Acct Grp';
				parentFilters[i].width = 125;
			}
		}
		return parentFilters;
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			const trader = TCG.getCurrentUser();
			this.setFilterValue('traderUser.label', {value: trader.id, text: trader.label});
		}
		return this.applyTopToolbarFilterLoadParams({
			readUncommittedRequested: true,
			workflowStatusNameEquals: 'Approved',
			excludeTradeGroupTypeName: 'Roll Trade'
		});
	},

	// Group / Execute for Order Commands overridden in trade-order-shared.js


	editor: {
		detailPageClass: 'Clifton.trade.TradeWindow',
		drillDownOnly: true,
		ptype: 'workflowAwareEntity-grideditor',
		executeMultipleInBatch: true,
		// ignore all response data, which is ignored to speed up each row's transition request
		requestedPropertiesToExclude: 'workflowTransitionCommand',
		tableName: 'Trade',
		transitionWorkflowStateList: [
			{stateName: 'Cancelled', iconCls: 'run', buttonText: 'Cancel Selected', buttonTooltip: 'Cancel Selected Trades', requireConfirm: true}
		],
		validateRowsToBeTransitioned: Clifton.trade.validateWorkflowTransition
	}
});
Ext.reg('trade-blotter-approved-trades-gridpanel', Clifton.trade.blotter.ApprovedTradesGridPanel);

Clifton.trade.blotter.ActiveOrdersPanel = Ext.extend(Ext.Panel, {
	// Overridden in trade-order-fix-shared.js
});
Ext.reg('trade-blotter-active-orders-panel', Clifton.trade.blotter.ActiveOrdersPanel);


Clifton.trade.ReassignmentToolbarButton = function(gridPanel) {
	return {
		text: 'Reassign Selected',
		tooltip: 'Reassign the selected trades',
		iconCls: 'shopping-cart',
		scope: this,
		handler: function() {
			const grid = gridPanel.grid;
			const sm = grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select a row to reassign.', 'No Row(s) Selected');
			}
			else {
				const tradeDetailList = sm.getSelections().map(row => row.json);
				TCG.createComponent('Clifton.trade.TradeReassignWindow', {
					defaultData: {
						tradeDetailList: tradeDetailList
					},
					openerCt: gridPanel
				});
			}
		}
	};
};

Clifton.trade.TradeContextMenuPlugin = Ext.extend(Ext.util.Observable, {
	grid: null,
	/**
	 * Set to false to not include the contextmenu item to launch a trade window populated with details to close a position.
	 */
	tradeToClose: true,
	/**
	 * Set to false to not include the contextmenu item to launch a trade window with only the security populated.
	 */
	tradeSecurity: true,
	/**
	 * Set to false to not include the contextmenu item to populate account details (client and holding) along with the security.
	 */
	tradeSecurityWithAccounts: true,
	/**
	 * Path to security ID to launch the correct trade window. Always required.
	 */
	recordSecurityIdPath: 'investmentSecurity.id',
	/**
	 * Account properties are optional. Used only when populating a trade window with account details (e.g. closing).
	 */
	recordClientAccountIdPath: 'clientInvestmentAccount.id',
	recordHoldingAccountIdPath: 'holdingInvestmentAccount.id',
	/**
	 * Used by most security types to get the quantity to trade when closing a position.
	 * Only applicable for closing a position.
	 */
	recordQuantityPath: 'quantity',
	/**
	 * Used by some Bonds that have factor change events that adjust quantity.
	 * Only applicable for closing a position.
	 */
	recordOriginalFacePath: 'unadjustedQuantity',
	/**
	 * Used for Currency positions to get teh quantity. Currency trades accounting notional instead of quantity.
	 * Only applicable for closing a position.
	 */
	recordLocalNotionalPath: 'localNotional',
	/**
	 * For position based grids, this path defines the accounting account name to look for Positions and Currency rows.
	 */
	filterTradable: false,
	recordAccountingAccountNamePath: 'accountingAccount.name',

	constructor: function(config) {
		Object.assign(this, config);
		TCG.callSuper(this, 'constructor', arguments);
	},

	init: function(gridOrPanel) {
		// Unwrap grid panel if necessary
		this.grid = (gridOrPanel instanceof TCG.grid.GridPanel) ? gridOrPanel.grid : gridOrPanel;

		const tradePlugin = this;
		const existingRowContextMenuItemsFunction = gridOrPanel.getGridRowContextMenuItems
			? gridOrPanel.getGridRowContextMenuItems : () => [];
		gridOrPanel.getGridRowContextMenuItems = async (...args) => {
			const menuItems = await existingRowContextMenuItemsFunction(...args) || [];
			const tradePluginItems = await tradePlugin.getTradeRowContextMenuItems(...args);
			if (tradePluginItems && tradePluginItems.length > 0) {
				if (menuItems.length > 0) {
					menuItems.push('-');
				}
				menuItems.push(...tradePluginItems);
			}
			return menuItems;
		};
	},

	getTradeRowContextMenuItems: function(grid, rowId, record) {
		const tradePlugin = this;
		const menuItems = [];

		if (!TCG.isTrue(tradePlugin.isRecordTradable(record))) {
			return menuItems;
		}

		if (TCG.isTrue(tradePlugin.tradeToClose)) {
			menuItems.push({
				text: 'Trade to Close',
				tooltip: 'Launch a trade window for the security of this row populating the available quantity, direction, and client and holding account applicable to this row',
				iconCls: 'shopping-cart',
				handler: function() {
					tradePlugin.navigateToTradeWindow(record, true, true);
				}
			});
		}
		if (TCG.isTrue(tradePlugin.tradeSecurity)) {
			menuItems.push({
				text: 'Trade Security',
				tooltip: 'Launch a trade window for the security of this row',
				iconCls: 'shopping-cart',
				handler: function() {
					tradePlugin.navigateToTradeWindow(record, false, false);
				}
			});
		}
		if (TCG.isTrue(tradePlugin.tradeSecurityWithAccounts)) {
			menuItems.push({
				text: 'Trade Security (With Accounts)',
				tooltip: 'Launch a trade window for the security of this row populating the available client and holding account applicable to this row',
				iconCls: 'shopping-cart',
				handler: function() {
					tradePlugin.navigateToTradeWindow(record, true, false);
				}
			});
		}

		return [
			{
				text: 'Trade',
				iconCls: 'shopping-cart',
				menu: {
					xtype: 'menu',
					items: menuItems
				}
			}
		];
	},

	isRecordTradable: function(record) {
		if (TCG.isFalse(this.filterTradable)) {
			return true;
		}
		const accountingAccountName = TCG.getValue(this.recordAccountingAccountNamePath, record.json);
		return accountingAccountName && (accountingAccountName === 'Position' || accountingAccountName === 'Currency');
	},

	navigateToTradeWindow: async function(record, includeAccounts, includeQuantity) {
		const tradePlugin = this;
		const security = await tradePlugin.getRecordSecurity(record);

		if (TCG.isNull(security)) {
			return;
		}

		const tradeDate = await Clifton.investment.instrument.getTradeDatePromise(security.id, null, this.grid);
		const settlementDate = await Clifton.investment.instrument.getSettlementDatePromise(security.id, tradeDate, null, null, this.grid);

		const defaultData = {
			investmentSecurity: security,
			payingSecurity: tradePlugin.getSettlementCurrency(security),
			tradeDate: tradeDate,
			settlementDate: settlementDate
		};

		if (TCG.isTrue(includeAccounts)) {
			defaultData['clientInvestmentAccount'] = await tradePlugin.getRecordInvestmentAccount(record, tradePlugin.recordClientAccountIdPath);
			defaultData['holdingInvestmentAccount'] = await tradePlugin.getRecordInvestmentAccount(record, tradePlugin.recordHoldingAccountIdPath);
		}
		if (TCG.isTrue(includeQuantity)) {
			const quantityParameters = await tradePlugin.getClosingPositionQuantityParameters(record, security);
			if (quantityParameters && typeof quantityParameters === 'object') {
				const quantityIntended = quantityParameters['quantityIntended'];
				const originalFace = quantityParameters['originalFace'];
				const buy = quantityParameters['buy'];
				const openCloseType = quantityParameters['openCloseType'];
				if (TCG.isNotBlank(quantityIntended)) {
					defaultData['quantityIntended'] = quantityIntended;
				}
				if (TCG.isNotBlank(originalFace)) {
					defaultData['originalFace'] = originalFace;
				}
				if (TCG.isNotBlank(buy)) {
					defaultData['buy'] = buy;
				}
				if (TCG.isNotBlank(openCloseType)) {
					defaultData['openCloseType'] = openCloseType;
				}
			}
		}

		const tradeType = await tradePlugin.getTradeType(security);
		if (TCG.isNull(tradeType)) {
			TCG.showError('Failed to determine Trade Type from security: ' + TCG.getValue('label', security), 'Trade Navigation');
			return;
		}
		await tradePlugin.applyTradeTypeOverrides(tradeType, defaultData, record, includeQuantity);

		TCG.createComponent('Clifton.trade.TradeWindow', {
			tradeType: tradeType.name,
			defaultData: defaultData
		});
	},

	getRecordSecurity: async function(record) {
		const securityId = TCG.getValue(this.recordSecurityIdPath, record.json);
		if (TCG.isBlank(securityId)) {
			TCG.showError('Failed to find Security ID for row using property: ' + this.recordSecurityIdPath, 'Trade Navigation');
			return;
		}
		return await TCG.data.getDataPromise(`investmentSecurity.json?id=${securityId}`, this.grid);
	},

	getRecordInvestmentAccount: async function(record, accountPath) {
		const accountId = TCG.getValue(accountPath, record.json);
		if (TCG.isBlank(accountId)) {
			return;
		}
		return await TCG.data.getDataPromise(`investmentAccount.json?id=${accountId}`, this.grid);
	},

	getTradeType: async function(security) {
		const securityId = TCG.getValue('id', security);
		return await TCG.data.getDataPromise(`tradeTypeForSecurity.json?investmentSecurityId=${securityId}`, this.grid);
	},

	getSettlementCurrency: function(security) {
		return TCG.getValue('instrument.tradingCurrency', security);
	},

	getClosingPositionQuantityParameters: async function(record, security) {
		const tradePlugin = this;
		const investmentTypeName = TCG.getValue('instrument.hierarchy.investmentType.name', security);

		const quantityIntended = TCG.getValue((investmentTypeName === 'Currency') ? tradePlugin.recordLocalNotionalPath : tradePlugin.recordQuantityPath, record.json);
		const buy = quantityIntended < 0;
		let openCloseTypeName = buy ? 'Buy' : 'Sell';
		if (investmentTypeName === 'Options') {
			openCloseTypeName = buy ? 'Buy to Close' : 'Sell to Close';
		}
		const openCloseType = await TCG.data.getDataPromiseUsingCaching(`tradeOpenCloseTypeByName.json?name=${openCloseTypeName}`, this.grid, `openCloseType.${openCloseTypeName}`);

		return {
			quantityIntended: Math.abs(quantityIntended),
			buy: buy,
			openCloseType: openCloseType
		};
	},

	applyTradeTypeOverrides: async function(tradeType, defaultData, record, includeQuantity) {
		const tradePlugin = this;
		const tradeTypeName = tradeType.name;
		if (tradeTypeName === 'Forwards') {
			const from = TCG.getValue('investmentSecurity.instrument.underlyingInstrument.tradingCurrency', defaultData);
			const to = TCG.getValue('investmentSecurity.instrument.tradingCurrency', defaultData);
			defaultData['givenCurrencyForDisplay'] = from;
			defaultData['payingSecurity'] = to;
		}
		else if (tradeTypeName === 'Currency') {
			if (record && TCG.isTrue(includeQuantity)) {
				defaultData['accountingNotional'] = defaultData.quantityIntended;
				delete defaultData.quantityIntended;
			}
			if (defaultData.clientInvestmentAccount) {
				defaultData['payingSecurity'] = TCG.getValue('clientInvestmentAccount.baseCurrency', defaultData);
			}
		}
		else if ((tradeTypeName.includes('CDS') || tradeTypeName === 'Bonds') && !tradeTypeName.includes('Swaptions')) {
			if (TCG.isTrue(includeQuantity)) {
				defaultData['originalFace'] = defaultData['quantityIntended'];
				delete defaultData.quantityIntended;
			}
		}

		if (TCG.isNotBlank(defaultData.investmentSecurity) && TCG.isTrue(defaultData.investmentSecurity.instrument.hierarchy.otc) && TCG.isNotBlank(defaultData.holdingInvestmentAccount)) {
			defaultData.executingBrokerCompany = defaultData.holdingInvestmentAccount.issuingCompany;
		}

		if (TCG.isTrue(includeQuantity)) {
			await tradePlugin.validateClosingPositionQuantity(tradeType, defaultData, record);
		}
	},

	validateClosingPositionQuantity: async function(tradeType, defaultData, record) {
		// Validate the position still exists; should result in one position balance for the client account, holding account, and security now
		const params = {
			investmentSecurityId: TCG.getValue('investmentSecurity.id', defaultData),
			clientInvestmentAccountId: TCG.getValue('clientInvestmentAccount.id', defaultData),
			holdingInvestmentAccountId: TCG.getValue('holdingInvestmentAccount.id', defaultData),
			transactionDate: new Date().format('m/d/Y'),
			requestedPropertiesRoot: 'data',
			requestedProperties: 'investmentSecurity.id|clientInvestmentAccount.id|holdingInvestmentAccount.id|unadjustedQuantity'
		};
		let url = 'accountingPositionBalanceListFind.json';
		if (tradeType.name === 'Currency') {
			url = 'accountingBalanceListFind.json';
			params['requestedProperties'] = params['requestedProperties'] + '|localAmount';
		}
		const positionList = await TCG.data.getDataPromise(url, this.grid, {params: params});
		if (TCG.isNull(positionList) || positionList.length === 0) {
			TCG.showError('The position you are closing is no longer open. Verify that you are filtering with the current date for the account or security.', 'Missing Position');
		}
		else if (positionList.length === 1) {
			if (TCG.isNotNull(TCG.getValue('investmentSecurity.instrument.hierarchy.factorChangeEventType', defaultData))) {
				// update original face for factor adjusted securities
				defaultData.originalFace = Math.abs(positionList[0]['unadjustedQuantity']);
			}
		}
	}
});
Ext.preg('trade-contextmenu-plugin', Clifton.trade.TradeContextMenuPlugin);
