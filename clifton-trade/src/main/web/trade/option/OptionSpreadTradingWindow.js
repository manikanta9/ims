Clifton.trade.option.OptionSpreadTradingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Option Spread Trades',
	iconCls: 'options',
	width: 1300,
	height: 715,
	hideApplyButton: true,
	hideOKButton: true, // use true if only want Apply & Cancel Buttons
	hideCancelButton: true, // use true to hide Cancel button
	doNotWarnOnCloseModified: true, // use to avoid "Closing Window without Save" message. The header panel and grid are not editable in this window.
	saveTimeout: 200,

	items: [
		{
			xtype: 'option-spread-form-panel'
		}
	]
});

Clifton.trade.option.OptionSpreadTradingForm = Ext.extend(TCG.form.FormPanel, {
	url: 'tradeGroupDynamic.json',
	loadValidation: false,
	dtoClassForBinding: 'com.clifton.trade.options.securitytarget.group.TradeGroupDynamicSecurityTargetOptionSpread',
	// field used to pass to the server that live prices should be obtained when calculating trade cash impact
	calculateCashTradeImpact: false,
	tradeEntry: false,
	shouldConsolidateDataRows: true,
	shouldInsertTotalRows: true,

	// Use vbox layout to maximize the Grid to the remaining space of the window after the header column
	// panel is rendered. The header panel needs to have a height defined and adjusted as fields are added.
	layout: 'vbox',
	layoutConfig: {
		align: 'stretch'
	},

	updateTitle: function() {
		if (TCG.isFalse(this.tradeEntry)) {
			TCG.callSuper(this, 'updateTitle');
		}
		// Do nothing, in the blotter
	},

	appendBindingParametersToUrl: function(url) {
		let returnUrl = url + ((url.indexOf('?') > 0) ? '&' : '?');
		returnUrl += 'requestedMaxDepth=5&enableValidatingBinding=true&dtoClassForBinding=' + this.dtoClassForBinding;
		returnUrl += '&requestedPropertiesToExcludeGlobally=tradeList|dynamicCalculatorBean';
		return returnUrl;
	},

	listRequestedPropertiesRoot: 'data.detailList',
	listRequestedProperties: 'clientAccount.label|clientAccount.id|clientAccount.serviceProcessingType.id|clientAccount.serviceProcessingType.name|holdingAccount.number|holdingAccount.id|executingBrokerCompany.label|executingBrokerCompany.id|' +
		'valueAtRiskPercent|securityTargetNotional|averageTrancheNotional|averageTrancheValueAtRisk|targetUnderlyingQuantity|unexpiredCoveredUnderlyingQuantity|availableUnderlyingQuantity|availableQuantity|expiringQuantity|tradeQuantity|putSpreadExposure|callSpreadExposure|' +
		'securityTarget.trancheCount|securityTarget.targetUnderlyingSecurity.id|securityTarget.targetUnderlyingSecurity.symbol|securityTarget.targetUnderlyingSecurity.label|' +
		'longCallTrade.id|shortCallTrade.id|longPutTrade.id|shortPutTrade.id|validationNoteList|tradeDestination.name|tradeDestination.id',

	getDefaultData: function(win) {
		let dd = win.defaultData || {};
		if (win.defaultDataIsReal) {
			return dd;
		}
		dd = Ext.apply({
			traderUser: TCG.getCurrentUser(),
			tradeDate: new Date().format('Y-m-d 00:00:00'),
			balanceDate: new Date().format('Y-m-d 00:00:00'),
			tradeGroupType: TCG.data.getData('tradeGroupTypeByName.json?name=Option Spread', this, 'trade.group.type.Option Spread'),
			tradeDestination: TCG.data.getData('tradeDestinationByName.json?name=Manual', this, 'trade.tradeDestination.Manual')
		}, dd);
		return dd;
	},

	getLoadURL: function() {
		const panel = this;
		const window = panel.getWindow();
		if (panel.url && window && window.params) {
			return panel.appendBindingParametersToUrl(panel.url);
		}
		return false;
	},

	getLoadParams: function(win) {
		const form = this.getForm();
		const params = Ext.apply(win.params ? win.params : {}, form.getValues());
		params.requestedPropertiesRoot = this.listRequestedPropertiesRoot;
		params.requestedProperties = this.listRequestedProperties;
		params.calculateCashBalance = true;
		params.calculateCashTradeImpact = this.calculateCashTradeImpact;
		return params;
	},

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			submitEmptyText: panel.submitEmptyText,
			success: function(form, action) {
				win.windowSaveInProgress = false;
				panel.fireEvent('afterload', panel, win.closeOnSuccess);
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	getSaveURL: function() {
		const form = this.getForm();
		if (TCG.isTrue(this.tradeEntry)) {
			return this.appendBindingParametersToUrl('tradeGroupDynamicEntrySave.json');
		}
		const executeAction = form.findField('tradeGroupAction').getValue();
		if (TCG.isNotBlank(executeAction)) {
			return this.appendBindingParametersToUrl('tradeGroupDynamicActionExecute.json?enableOpenSessionInView=true');
		}
		return false;
	},

	getSubmitParams: function() {
		return {
			requestedPropertiesRoot: this.listRequestedPropertiesRoot,
			requestedProperties: this.listRequestedProperties
		};
	},

	defaults: {
		anchor: '0'
	},

	listeners: {
		beforerender: function() {
			this.add(TCG.isFalse(this.tradeEntry) ? this.readOnlyItems : this.entryItems);
		},
		afterload: function() {
			// update the new expiration field from the available securities on the form
			const newExpirationDateField = this.getForm().findField('optionSecurity.lastDeliveryDate');
			if (newExpirationDateField && TCG.isBlank(newExpirationDateField.getValue())) {
				const newExpirationDate = ['longPutSecurity', 'shortPutSecurity', 'shortCallSecurity', 'longCallSecurity']
					.map(security => TCG.getValue(security + '.lastDeliveryDate', this.getForm().formValues))
					.find(date => TCG.isNotBlank(date));
				if (newExpirationDate) {
					newExpirationDateField.setValue(new Date(newExpirationDate));
				}
			}
		},
		// After Submitting Trades the group is created, but the grid is being reloaded - so we add a listener to after the reload finishes to clear the trades so it doesn't double count what was submitted and new quantities
		aftercreate: function() {
			const gp = TCG.getChildByName(this, 'option-spread-trade-grid');
			const clearTradeHandler = function() {
				// Clear submitted trades and keep track of non-submitted trade quantity modifications for future submits.
				const gridStore = gp.getStore();
				const modifiedRecords = gridStore.getModifiedRecords();
				gridStore.each(function(record) {
					const recordClientAccount = TCG.getValue('clientAccount.id', record.json),
						recordHoldingAccount = TCG.getValue('holdingAccount.id', record.json);
					record.beginEdit();
					for (let i = 0; i < modifiedRecords.length; i++) {
						const modifiedRecord = modifiedRecords[i];
						const modifiedClientAccount = TCG.getValue('clientAccount.id', modifiedRecord.json),
							modifiedHoldingAccount = TCG.getValue('holdingAccount.id', modifiedRecord.json);
						if ((recordClientAccount === modifiedClientAccount) && (recordHoldingAccount === modifiedHoldingAccount)) {
							if (TCG.isNumber(modifiedRecord.id)) {
								// submitted modified record - clear quantity
								gp.clearRecordTradeQuantities(record);
							}
							else {
								if (modifiedRecord.modified['tradeQuantity']) {
									record.data['tradeQuantity'] = modifiedRecord.modified['tradeQuantity'];
								}
								if (modifiedRecord.modified['holdingAccount.id']) {
									record.data['holdingAccount.id'] = modifiedRecord.modified['holdingAccount.id'];
									record.data['holdingAccount.number'] = modifiedRecord.modified['holdingAccount.number'];
								}
							}
						}
					}
					record.endEdit();
				}, gp);
				gp.getView().refresh();
				// Remove this listener
				gp.un('afterReloadGrid', clearTradeHandler, gp);
			};
			gp.on('afterReloadGrid', clearTradeHandler, gp);
			gp.reloadGrid();
		}
	},

	readOnlyItems: [
		{name: 'tradeGroupType.id', xtype: 'hidden'},
		//Need to add id as a hidden field and submitValue = false so that id is not double submitted
		{name: 'id', xtype: 'hidden', submitValue: false},
		{name: 'tradeGroupAction', xtype: 'hidden'},
		// fields for applying fill price
		{name: 'longPutFillPrice', xtype: 'hidden'},
		{name: 'shortPutFillPrice', xtype: 'hidden'},
		{name: 'shortCallFillPrice', xtype: 'hidden'},
		{name: 'longCallFillPrice', xtype: 'hidden'},
		// fields for applying commission overrides
		{name: 'longPutCommissionPerUnit', xtype: 'hidden'},
		{name: 'shortPutCommissionPerUnit', xtype: 'hidden'},
		{name: 'shortCallCommissionPerUnit', xtype: 'hidden'},
		{name: 'longCallCommissionPerUnit', xtype: 'hidden'},
		{name: 'enableCommissionRemoval', xtype: 'hidden'},
		{
			xtype: 'panel',
			layout: 'column',
			// height in the parent vbox formpanel - resize as fields are added/removed
			height: 175,
			defaults: {layout: 'form'},
			items: [
				{
					columnWidth: .73,
					items: [
						{xtype: 'sectionheaderfield', header: 'Client Info'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {columnWidth: .49, layout: 'form'},
							items: [
								{
									items: [
										{fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', detailIdField: 'clientAccountGroup.id', xtype: 'linkfield', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									columnWidth: .27,
									items: [
										{fieldLabel: 'Balance Date', name: 'balanceDate', xtype: 'datefield', readOnly: true}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									columnWidth: .20,
									labelWidth: 1,
									items: []
								}
							]
						},
						{xtype: 'sectionheaderfield', header: 'New Options Info'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {columnWidth: .49, layout: 'form'},
							items: [
								{
									items: [
										{
											fieldLabel: 'Underlying Security', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{flex: 1, name: 'optionUnderlyingSecurity.label', detailIdField: 'optionUnderlyingSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
												{html: '&nbsp;'},
												{name: 'underlyingPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Underlying Security'}
											]
										}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									items: [
										{
											fieldLabel: 'Expiration (New)', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{
													name: 'optionSecurity.lastDeliveryDate', xtype: 'datefield', submitValue: false, readOnly: true,
													qtip: 'Expiration Date of the New Options that will be traded here.'
												},
												{html: '&nbsp;', flex: 1},
												{
													iconCls: 'bloomberg', text: 'Live Prices', xtype: 'button', tooltip: 'Load the live data value(s) for the specified Put and/or Call options. The form fields are dynamically loaded as data is retrieved, so there may be a slight delay before the values are visible.',
													width: 134,
													listeners: {
														click: function(button, e) {
															const fp = TCG.getParentFormPanel(button);
															const form = fp.getForm();
															const optionFieldPrefixes = ['longPut', 'shortPut', 'shortCall', 'longCall'];
															const securityIds = [];
															const securityPriceUpdateFields = [];
															const securityDeltaUpdateFields = [];
															const optionFieldFunction = function(optionFieldPrefix) {
																if (TCG.isNotBlank(optionFieldPrefix)) {
																	const fieldValue = form.findField(optionFieldPrefix + 'Security.id').getValue();
																	if (TCG.isNotBlank(fieldValue)) {
																		securityIds.push(fieldValue);
																		securityPriceUpdateFields.push(optionFieldPrefix + 'Price');
																		securityDeltaUpdateFields.push(optionFieldPrefix + 'Delta');
																	}
																}
															};
															const underlyingSecurityId = form.findField('optionUnderlyingSecurity.id').getValue();
															if (TCG.isNotBlank(underlyingSecurityId)) {
																securityIds.push(underlyingSecurityId);
																securityPriceUpdateFields.push('underlyingPrice');
															}
															for (const fieldPrefix of optionFieldPrefixes) {
																optionFieldFunction(fieldPrefix);
															}
															if (securityIds.length > 0) {
																Clifton.marketdata.field.updateSecurityLiveMarketDataValues(fp, securityIds, securityPriceUpdateFields, void 0, {maxAgeInSeconds: 60, providerOverride: 'BPIPE'});
																// retrieve delta values excluding underlying
																const deltaSecurityIds = TCG.isNotBlank(underlyingSecurityId) ? securityIds.slice(1) : securityIds;
																if (deltaSecurityIds.length > 0) {
																	Clifton.marketdata.field.updateSecurityLiveMarketDataValues(fp, deltaSecurityIds, securityDeltaUpdateFields, 'Delta', {maxAgeInSeconds: 60, providerOverride: 'BPIPE'});
																}
															}
														}
													}
												}
											]
										}
									]
								}
							]
						},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {columnWidth: .49, layout: 'form'},
							items: [
								{
									items: [
										{xtype: 'sectionheaderfield', header: 'Put Spread'},
										{
											fieldLabel: 'Long Put', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{flex: 1, name: 'longPutSecurity.label', detailIdField: 'longPutSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
												{html: '&nbsp;'},
												{name: 'longPutDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Long Put Option.'},
												{html: '&nbsp;'},
												{name: 'longPutPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Long Put Option.'}
											]
										},
										{
											fieldLabel: 'Short Put', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{flex: 1, name: 'shortPutSecurity.label', detailIdField: 'shortPutSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
												{html: '&nbsp;'},
												{name: 'shortPutDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Short Put Option.'},
												{html: '&nbsp;'},
												{name: 'shortPutPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Short Put Option.'}
											]
										}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									items: [
										{xtype: 'sectionheaderfield', header: 'Call Spread'},
										{
											fieldLabel: 'Short Call', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{flex: 1, name: 'shortCallSecurity.label', detailIdField: 'shortCallSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
												{html: '&nbsp;'},
												{name: 'shortCallDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Long Call Option.'},
												{html: '&nbsp;'},
												{name: 'shortCallPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Long Call Option.'}
											]
										},
										{
											fieldLabel: 'Long Call', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{flex: 1, name: 'longCallSecurity.label', detailIdField: 'longCallSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
												{html: '&nbsp;'},
												{name: 'longCallDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Long Call Option.'},
												{html: '&nbsp;'},
												{name: 'longCallPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Long Call Option.'}
											]
										}
									]
								}
							]
						}

					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .25,
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{name: 'workflowState.id', xtype: 'hidden'},
						{fieldLabel: 'Workflow State', name: 'workflowState.name', xtype: 'displayfield'},
						{name: 'tradeDestination.id', xtype: 'hidden'},
						{name: 'executingBrokerCompany.label', xtype: 'hidden'},
						{name: 'executingBrokerCompany.id', xtype: 'hidden'},
						{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
						{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
					]
				}
			]
		},
		{
			xtype: 'option-spread-form-grid',
			tradeEntrySupported: false,
			autoHeight: false,
			flex: 1 // consume remaining space of the parent vbox formpanel
		}
	],

	entryItems: [
		{
			xtype: 'security-drag-and-drop-container', height: 30,
			prepareSecurityDefaultData: function(defaultData) {
				this.resetSecurityCombos();

				const form = TCG.getParentFormPanel(this).getForm();
				const underlyingSecurityId = form.findField('optionUnderlyingSecurity.id').getValue();
				return TCG.data.getDataPromise('investmentTypeByName.json?name=Options', this)
					.then(investmentType => {
						defaultData.investmentType = investmentType;

						if (underlyingSecurityId) {
							return TCG.data.getDataPromise('investmentSecurity.json?id=' + underlyingSecurityId, this)
								.then(underlyingSecurity => {
									defaultData.underlyingSecurity = underlyingSecurity;
									return defaultData;
								});
						}
						return defaultData;
					});
			},
			resetSecurityCombos: function() {
				const form = TCG.getParentFormPanel(this).getForm();
				form.findField('longPutSecurity.label').resetStore();
				form.findField('shortPutSecurity.label').resetStore();
				form.findField('shortCallSecurity.label').resetStore();
				form.findField('longCallSecurity.label').resetStore();
			}
		},
		{name: 'tradeGroupType.id', xtype: 'hidden'},
		{
			xtype: 'panel',
			layout: 'column',
			// height in the parent vbox formpanel - resize as fields are added/removed
			height: 175,
			defaults: {layout: 'form'},
			items: [
				{
					columnWidth: .73,
					items: [
						{xtype: 'sectionheaderfield', header: 'Client Info'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {columnWidth: .49, layout: 'form'},
							items: [
								{
									items: [
										{
											fieldLabel: 'Client Acct Group', name: 'clientAccountGroup.name', hiddenName: 'clientAccountGroup.id', xtype: 'combo',
											url: 'investmentAccountGroupListFind.json', allowBlank: false, detailPageClass: 'Clifton.investment.account.AccountGroupWindow', anchor: '-20',
											qtip: 'Account group to be traded with a particular option trading strategy. All accounts in a group must utilize the same strategy.',
											businessServiceProcessingTypeId: null,
											listeners: {
												select: function(combo, record, index) {
													const fp = TCG.getParentFormPanel(this);
													fp.updateProcessingTypeIdAndOptionSecurityComboStates(record.json.id);
												}
											}
										}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									items: [
										{
											fieldLabel: 'Balance Date', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{name: 'balanceDate', xtype: 'datefield', allowBlank: false, qtip: 'Balance Date to use to look up existing portfolio information for calculating suggested trades.'},
												{html: '&nbsp;', width: 30},
												{
													boxLabel: 'Validate Expiring', qtip: 'If checked will validate every account in the account group with a Security Target for the Underlying Security for expiring positions on the Balance Date. Notes will be included on each client account where expiring positions do not match what is expected.',
													xtype: 'checkbox', name: 'validateExpiringPositions', checked: true
												}
											]
										}
									]
								}
							]
						},
						{xtype: 'sectionheaderfield', header: 'New Options Info'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {columnWidth: .49, layout: 'form'},
							items: [
								{
									items: [
										{
											fieldLabel: 'Underlying Security', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{
													name: 'optionUnderlyingSecurity.label', hiddenName: 'optionUnderlyingSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json?active=true', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
													allowBlank: false, flex: 1, qtip: 'Underlying Security to create trades for',
													underlyingPrice: null,
													listeners: {
														select: function(combo, record, index) {
															Clifton.marketdata.field.getSecurityLiveMarketDataValue(record.json.id, void 0, {maxAgeInSeconds: 60, providerOverride: 'BPIPE'})
																.then(result => {
																	if (TCG.isNotBlank(result)) {
																		this.underlyingPrice = result.value;
																	}
																	else {
																		this.underlyingPrice = null;
																	}
																});

															const fp = combo.getParentForm();
															fp.updateOptionSelectionComboState();
														}
													}
												},
												{html: '&nbsp;'},
												{name: 'underlyingPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Underlying Security'}
											]
										}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									items: [
										{
											fieldLabel: 'Expiration (New)', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{
													name: 'optionSecurity.lastDeliveryDate', xtype: 'datefield', submitValue: false,
													qtip: 'Expiration Date of the New Options that will be traded here.',
													listeners: {
														select: function(dateField, dateValue) {
															const fp = TCG.getParentFormPanel(this);
															fp.updateOptionSelectionComboState();
														}
													}
												},
												{html: '&nbsp;', width: 30},
												{
													boxLabel: 'Use Live Prices', qtip: 'If checked, will use the live price for the Underlying Security for validation and calculating trades and exposures. Will use the previous day closing price if unchecked.',
													xtype: 'checkbox', name: 'livePrices', checked: true, flex: 1
												},
												{html: '&nbsp;'},
												{
													iconCls: 'bloomberg', text: 'Prices', xtype: 'button', tooltip: 'Load the live data value(s) for the specified Put and/or Call options. The form fields are dynamically loaded as data is retrieved, so there may be a slight delay before the values are visible.',
													width: 60,
													listeners: {
														click: function(button, e) {
															const fp = TCG.getParentFormPanel(button);
															const form = fp.getForm();
															const optionFieldPrefixes = ['longPut', 'shortPut', 'shortCall', 'longCall'];
															const securityIds = [];
															const securityPriceUpdateFields = [];
															const securityDeltaUpdateFields = [];
															const optionFieldFunction = function(optionFieldPrefix) {
																if (TCG.isNotBlank(optionFieldPrefix)) {
																	const fieldValue = form.findField(optionFieldPrefix + 'Security.id').getValue();
																	if (TCG.isNotBlank(fieldValue)) {
																		securityIds.push(fieldValue);
																		securityPriceUpdateFields.push(optionFieldPrefix + 'Price');
																		securityDeltaUpdateFields.push(optionFieldPrefix + 'Delta');
																	}
																}
															};
															const underlyingSecurityId = form.findField('optionUnderlyingSecurity.id').getValue();
															if (TCG.isNotBlank(underlyingSecurityId)) {
																securityIds.push(underlyingSecurityId);
																securityPriceUpdateFields.push('underlyingPrice');
															}
															for (const fieldPrefix of optionFieldPrefixes) {
																optionFieldFunction(fieldPrefix);
															}
															if (securityIds.length > 0) {
																Clifton.marketdata.field.updateSecurityLiveMarketDataValues(fp, securityIds, securityPriceUpdateFields, void 0, {maxAgeInSeconds: 60, providerOverride: 'BPIPE'});
																// retrieve delta values excluding underlying
																const deltaSecurityIds = TCG.isNotBlank(underlyingSecurityId) ? securityIds.slice(1) : securityIds;
																if (deltaSecurityIds.length > 0) {
																	Clifton.marketdata.field.updateSecurityLiveMarketDataValues(fp, deltaSecurityIds, securityDeltaUpdateFields, 'Delta', {maxAgeInSeconds: 60, providerOverride: 'BPIPE'});
																}
															}
														}
													}
												}
											]
										}
									]
								}
							]
						},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {columnWidth: .49, layout: 'form'},
							items: [
								{
									items: [
										{xtype: 'sectionheaderfield', header: 'Put Spread'},
										{
											fieldLabel: 'Long Put', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{
													name: 'longPutSecurity.label', hiddenName: 'longPutSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
													flex: 1, qtip: 'Long Put Security that will be traded based on quantities entered below',
													requestedProps: 'instrument',
													investmentSecurity: null,
													listeners: {
														beforeQuery: function(qEvent) {
															const f = qEvent.combo.getParentForm().getForm();
															const store = qEvent.combo.store;
															store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
															store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
															store.setBaseParam('lastDeliveryDate', f.findField('optionSecurity.lastDeliveryDate').value);
															store.setBaseParam('optionType', 'PUT');
														},
														select: function(combo, record, index) {
															const fp = combo.getParentForm();
															const form = fp.getForm();

															this.investmentSecurity = record.json;
															Clifton.investment.instrument.getSettlementDatePromise(record.id, form.findField('tradeDate').value, null, null, combo)
																.then(function(settlementDate) {
																	form.findField('settlementDate').setValue(settlementDate);
																});
														},
														change: function(combo, newValue, oldValue) {
															if (TCG.isBlank(newValue)) {
																const form = combo.getParentForm().getForm();
																form.findField('longPutDelta').setValue('');
																form.findField('longPutPrice').setValue('');
															}
														}
													},
													getDetailPageClass: function(newInstance) {
														if (newInstance) {
															if (!this.getParentForm().getForm().findField('longPutSecurity.id').value) {
																TCG.showError('You must first select an option to copy', 'New Option');
																return false;
															}
															return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
														}
														return this.detailPageClass;
													},
													getDefaultData: function(f) { // defaults client/counterparty/isda
														let fp = TCG.getParentFormPanel(this);
														fp = fp.getForm();
														const secId = fp.findField('longPutSecurity.id').value;
														return Promise.resolve(secId)
															.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
															.then(securityToCopy => ({
																instrument: securityToCopy.instrument,
																securityToCopy: securityToCopy,
																optionType: 'PUT'
															}));
													}
												},
												{html: '&nbsp;'},
												{name: 'longPutDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Long Put Option.'},
												{html: '&nbsp;'},
												{name: 'longPutPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Long Put Option.'}
											]
										},
										{
											fieldLabel: 'Short Put', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{
													name: 'shortPutSecurity.label', hiddenName: 'shortPutSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
													allowBlank: false, flex: 1, qtip: 'Short Put Security that will be traded based on quantities entered below',
													requestedProps: 'instrument',
													investmentSecurity: null,
													listeners: {
														beforeQuery: function(qEvent) {
															const f = qEvent.combo.getParentForm().getForm();
															const store = qEvent.combo.store;
															store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
															store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
															store.setBaseParam('lastDeliveryDate', f.findField('optionSecurity.lastDeliveryDate').value);
															store.setBaseParam('optionType', 'PUT');
														},
														select: function(combo, record, index) {
															const fp = combo.getParentForm();
															const form = fp.getForm();

															this.investmentSecurity = record.json;
															Clifton.investment.instrument.getSettlementDatePromise(record.id, form.findField('tradeDate').value, null, null, combo)
																.then(function(settlementDate) {
																	form.findField('settlementDate').setValue(settlementDate);
																});
														},
														change: function(combo, newValue, oldValue) {
															if (TCG.isBlank(newValue)) {
																const form = combo.getParentForm().getForm();
																form.findField('shortPutDelta').setValue('');
																form.findField('shortPutPrice').setValue('');
															}
														}
													},
													getDetailPageClass: function(newInstance) {
														if (newInstance) {
															if (!this.getParentForm().getForm().findField('shortPutSecurity.id').value) {
																TCG.showError('You must first select an option to copy', 'New Option');
																return false;
															}
															return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
														}
														return this.detailPageClass;
													},
													getDefaultData: function(f) { // defaults client/counterparty/isda
														let fp = TCG.getParentFormPanel(this);
														fp = fp.getForm();
														const secId = fp.findField('shortPutSecurity.id').value;
														return Promise.resolve(secId)
															.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
															.then(securityToCopy => ({
																instrument: securityToCopy.instrument,
																securityToCopy: securityToCopy,
																optionType: 'PUT'
															}));
													}
												},
												{html: '&nbsp;'},
												{name: 'shortPutDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Short Put Option.'},
												{html: '&nbsp;'},
												{name: 'shortPutPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Short Put Option.'}
											]
										}
									]
								},
								{columnWidth: .02, items: [{html: '&nbsp;'}]},
								{
									items: [
										{xtype: 'sectionheaderfield', header: 'Call Spread'},
										{
											fieldLabel: 'Short Call', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{
													name: 'shortCallSecurity.label', hiddenName: 'shortCallSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
													flex: 1, qtip: 'Short Call Security that will be traded based on quantities entered below',
													requestedProps: 'instrument',
													investmentSecurity: null,
													listeners: {
														beforeQuery: function(qEvent) {
															const f = qEvent.combo.getParentForm().getForm();
															const store = qEvent.combo.store;
															store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
															store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
															store.setBaseParam('lastDeliveryDate', f.findField('optionSecurity.lastDeliveryDate').value);
															store.setBaseParam('optionType', 'CALL');
														},
														select: function(combo, record, index) {
															const fp = combo.getParentForm();
															const form = fp.getForm();

															this.investmentSecurity = record.json;
															Clifton.investment.instrument.getSettlementDatePromise(record.id, form.findField('tradeDate').value, null, null, combo)
																.then(function(settlementDate) {
																	form.findField('settlementDate').setValue(settlementDate);
																});
														},
														change: function(combo, newValue, oldValue) {
															if (TCG.isBlank(newValue)) {
																const form = combo.getParentForm().getForm();
																form.findField('shortCallDelta').setValue('');
																form.findField('shortCallPrice').setValue('');
															}
														}
													},
													getDetailPageClass: function(newInstance) {
														if (newInstance) {
															if (!this.getParentForm().getForm().findField('shortCallSecurity.id').value) {
																TCG.showError('You must first select an option to copy.', 'New Option');
																return false;
															}
															return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
														}
														return this.detailPageClass;
													},
													getDefaultData: function(f) { // defaults client/counterparty/isda
														let fp = TCG.getParentFormPanel(this);
														fp = fp.getForm();
														const secId = fp.findField('shortCallSecurity.id').value;
														return Promise.resolve(secId)
															.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
															.then(securityToCopy => ({
																instrument: securityToCopy.instrument,
																securityToCopy: securityToCopy,
																optionType: 'CALL'
															}));
													}
												},
												{html: '&nbsp;'},
												{name: 'shortCallDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Short Call Option.'},
												{html: '&nbsp;'},
												{name: 'shortCallPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Short Call Option.'}
											]
										},
										{
											fieldLabel: 'Long Call', xtype: 'panel', layout: 'hbox', anchor: '0',
											items: [
												{
													name: 'longCallSecurity.label', hiddenName: 'longCallSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
													allowBlank: false, flex: 1, qtip: 'Long Call Security that will be traded based on quantities entered below',
													requestedProps: 'instrument',
													investmentSecurity: null,
													listeners: {
														beforeQuery: function(qEvent) {
															const f = qEvent.combo.getParentForm().getForm();
															const store = qEvent.combo.store;
															store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
															store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
															store.setBaseParam('lastDeliveryDate', f.findField('optionSecurity.lastDeliveryDate').value);
															store.setBaseParam('optionType', 'CALL');
														},
														select: function(combo, record, index) {
															const fp = combo.getParentForm();
															const form = fp.getForm();

															this.investmentSecurity = record.json;
															Clifton.investment.instrument.getSettlementDatePromise(record.id, form.findField('tradeDate').value, null, null, combo)
																.then(function(settlementDate) {
																	form.findField('settlementDate').setValue(settlementDate);
																});
														},
														change: function(combo, newValue, oldValue) {
															if (TCG.isBlank(newValue)) {
																const form = combo.getParentForm().getForm();
																form.findField('longCallDelta').setValue('');
																form.findField('longCallPrice').setValue('');
															}
														}
													},
													getDetailPageClass: function(newInstance) {
														if (newInstance) {
															if (!this.getParentForm().getForm().findField('longCallSecurity.id').value) {
																TCG.showError('You must first select an option to copy.', 'New Option');
																return false;
															}
															return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
														}
														return this.detailPageClass;
													},
													getDefaultData: function(f) { // defaults client/counterparty/isda
														let fp = TCG.getParentFormPanel(this);
														fp = fp.getForm();
														const secId = fp.findField('longCallSecurity.id').value;
														return Promise.resolve(secId)
															.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
															.then(securityToCopy => ({
																instrument: securityToCopy.instrument,
																securityToCopy: securityToCopy,
																optionType: 'CALL'
															}));
													}
												},
												{html: '&nbsp;'},
												{name: 'longCallDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Long Call Option.'},
												{html: '&nbsp;'},
												{name: 'longCallPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Long Call Option.'}
											]
										}
									]
								}
							]
						}
					]
				},
				{columnWidth: .02, items: [{html: '&nbsp;'}]},
				{
					columnWidth: .25,
					items: [
						{xtype: 'sectionheaderfield', header: 'Trade Info'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: false, anchor: '-20',
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									qEvent.combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: 'Options'
									};
								},
								select: function(combo, record, index) {
									// Clear trade destination when executing broker is selected
									const f = combo.getParentForm().getForm();
									const eb = f.findField('executingBrokerCompany.label');
									eb.clearValue();
									eb.reset();
									eb.store.removeAll();
									eb.lastQuery = null;
								}
							}
						},
						{
							fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', anchor: '-20',
							displayField: 'label',
							requiredFields: ['tradeDestination.id'],
							url: 'tradeExecutingBrokerCompanyListFind.json',
							queryParam: 'executingBrokerCompanyName',
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();

									combo.store.baseParams = {
										activeOnDate: f.findField('tradeDate').value,
										tradeTypeNameEquals: 'Options',
										tradeDestinationId: f.findField('tradeDestination.id').getValue()
									};
								}
							}
						},
						{fieldLabel: 'Trader', name: 'traderUser.label', hiddenName: 'traderUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', allowBlank: false, anchor: '-20'},
						{
							fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
							listeners: {
								change: function(field, newValue, oldValue) {
									const form = field.getParentFormPanel().getForm();
									const settlementDateField = form.findField('settlementDate');

									if (TCG.isBlank(newValue)) {
										settlementDateField.setValue('');
									}
									else {
										Clifton.investment.instrument.getSettlementDatePromise(form.findField('putSecurity.id').value, newValue, null, null, field)
											.then(function(settlementDate) {
												form.findField('settlementDate').setValue(settlementDate);
											});
									}
								},
								select: function(field, date) {
									const form = TCG.getParentFormPanel(field).getForm();

									Clifton.investment.instrument.getSettlementDatePromise(form.findField('putSecurity.id').value, date, null, null, field)
										.then(function(settlementDate) {
											form.findField('settlementDate').setValue(settlementDate);
										});
								}
							}
						},
						{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield'}
					]
				}
			]
		},
		{
			xtype: 'option-spread-form-grid',
			tradeEntrySupported: true,
			autoHeight: false,
			flex: 1 // consume remaining space in the parent vbox formpanel
		}
	],

	/**
	 * Checks the accounts proposed for this trade against trade restrictions to determine which trades should be blocked.
	 *
	 * @returns {*} an object containing an {@link Array} of valid positions and an {@link Array} of positions that have trade restrictions.
	 */
	checkOptionSpreadTradeRestrictions: function() {
		const fp = this;
		const f = fp.getForm();

		const underlyingSecurityId = f.findField('optionUnderlyingSecurity.id').getValue();
		const underlyingPrice = f.findField('optionUnderlyingSecurity.id').underlyingPrice;
		const investmentSecurityList = [];
		const longPutSecurityField = f.findField('longPutSecurity.id');
		const longPutSecurity = longPutSecurityField.investmentSecurity;
		if (TCG.isNotBlank(longPutSecurity)) {
			longPutSecurity.isSell = false;
			investmentSecurityList.push(longPutSecurity);
		}

		const shortPutSecurityField = f.findField('shortPutSecurity.id');
		const shortPutSecurity = shortPutSecurityField.investmentSecurity;
		if (TCG.isNotBlank(shortPutSecurity)) {
			shortPutSecurity.isSell = true;
			investmentSecurityList.push(shortPutSecurity);
		}

		const longCallSecurityField = f.findField('longCallSecurity.id');
		const longCallSecurity = longCallSecurityField.investmentSecurity;
		if (TCG.isNotBlank(longCallSecurity)) {
			longCallSecurity.isSell = false;
			investmentSecurityList.push(longCallSecurity);
		}

		const shortCallSecurityField = f.findField('shortCallSecurity.id');
		const shortCallSecurity = shortCallSecurityField.investmentSecurity;
		if (TCG.isNotBlank(shortCallSecurity)) {
			shortCallSecurity.isSell = true;
			investmentSecurityList.push(shortCallSecurity);
		}

		const positionJsonData = TCG.getChildByName(fp, 'detailList').value;
		const positionList = JSON.parse(positionJsonData);
		const restrictedPositions = [], validPositions = [];
		for (const position of positionList) {
			let isRestricted = false;
			for (const investmentSecurity of investmentSecurityList) {
				const status = TCG.getChildByName(fp, 'option-spread-trade-grid').checkTradeRestrictions(position['clientAccount.id'], investmentSecurity, underlyingSecurityId, underlyingPrice, investmentSecurity.isSell);
				if (TCG.isFalse(status)) {
					isRestricted = true;
				}
			}
			isRestricted ? restrictedPositions.push(position) : validPositions.push(position);
		}
		return {restrictedPositions: restrictedPositions, validPositions: validPositions};
	},

	/**
	 * A function to enable or disable the option selection fields based on the BusinessServiceProcessingTypeId's optionStructureType.
	 */
	updateOptionSelectionComboState: function() {
		const frm = this.getForm();
		const dateSelector = frm.findField('optionSecurity.lastDeliveryDate');
		const underlyingSecurityCombo = frm.findField('optionUnderlyingSecurity.label');
		const clientAccountGroupCombo = frm.findField('clientAccountGroup.name');
		const businessServiceProcessingTypeId = clientAccountGroupCombo.businessServiceProcessingTypeId;

		// Check for prerequisite field configuration before getting optionStructureType.
		const isPrerequisiteFieldsConfigured = TCG.isNotBlank(dateSelector) && TCG.isNotBlank(dateSelector.getValue()) && TCG.isNotBlank(underlyingSecurityCombo) && TCG.isNotBlank(underlyingSecurityCombo.getValue());
		const optionCombinationType = TCG.isNotNull(businessServiceProcessingTypeId) && isPrerequisiteFieldsConfigured ? Clifton.business.service.getOptionCombinationTypeFromBusinessServiceProcessingType(businessServiceProcessingTypeId) : null;

		const longPutSecurityCombo = frm.findField('longPutSecurity.label');
		const shortPutSecurityCombo = frm.findField('shortPutSecurity.label');
		const shortCallSecurityCombo = frm.findField('shortCallSecurity.label');
		const longCallSecurityCombo = frm.findField('longCallSecurity.label');

		switch (optionCombinationType) {
			case 'LONG_PUT':
				longPutSecurityCombo.enable();
				longPutSecurityCombo.allowBlank = false;
				shortPutSecurityCombo.disable();
				shortCallSecurityCombo.disable();
				longCallSecurityCombo.disable();
				break;
			case 'SHORT_PUT':
				longPutSecurityCombo.disable();
				shortPutSecurityCombo.enable();
				shortPutSecurityCombo.allowBlank = false;
				shortCallSecurityCombo.disable();
				longCallSecurityCombo.disable();
				break;
			case 'SHORT_CALL':
				longPutSecurityCombo.disable();
				shortPutSecurityCombo.disable();
				shortCallSecurityCombo.enable();
				shortCallSecurityCombo.allowBlank = false;
				longCallSecurityCombo.disable();
				break;
			case 'LONG_CALL':
				longPutSecurityCombo.disable();
				shortPutSecurityCombo.disable();
				shortCallSecurityCombo.disable();
				longCallSecurityCombo.enable();
				longCallSecurityCombo.allowBlank = false;
				break;
			case 'BEAR_CALL_SPREAD':
			case 'BULL_CALL_SPREAD':
				longPutSecurityCombo.disable();
				shortPutSecurityCombo.disable();
				shortCallSecurityCombo.enable();
				shortCallSecurityCombo.allowBlank = false;
				longCallSecurityCombo.enable();
				longCallSecurityCombo.allowBlank = false;
				break;
			case 'BEAR_PUT_SPREAD':
			case 'BULL_PUT_SPREAD':
				longPutSecurityCombo.enable();
				longPutSecurityCombo.allowBlank = false;
				shortPutSecurityCombo.enable();
				shortPutSecurityCombo.allowBlank = false;
				shortCallSecurityCombo.disable();
				longCallSecurityCombo.disable();
				break;
			case 'COLLAR':
				longPutSecurityCombo.enable();
				longPutSecurityCombo.allowBlank = false;
				shortPutSecurityCombo.disable();
				shortCallSecurityCombo.enable();
				shortCallSecurityCombo.allowBlank = false;
				longCallSecurityCombo.disable();
				break;
			case 'IRON_CONDOR':
				longPutSecurityCombo.enable();
				longPutSecurityCombo.allowBlank = false;
				shortPutSecurityCombo.enable();
				shortPutSecurityCombo.allowBlank = false;
				shortCallSecurityCombo.enable();
				shortCallSecurityCombo.allowBlank = false;
				longCallSecurityCombo.enable();
				longCallSecurityCombo.allowBlank = false;
				break;
			default:
				// we hit this if we did not resolve an TradeOptionCombinationType or if the prerequisite fields have not been set.
				longPutSecurityCombo.disable();
				shortPutSecurityCombo.disable();
				shortCallSecurityCombo.disable();
				longCallSecurityCombo.disable();
				break;
		}

		this.clearAndResetOptionSelectionCombos();
		this.clearOptionPriceAndDeltaValues();
	},

	updateProcessingTypeIdAndOptionSecurityComboStates: function(clientAccountGroupId) {
		const frm = this.getForm();
		const clientAccountGroupCombo = frm.findField('clientAccountGroup.name');
		const fp = this;
		const grid = TCG.getChildByName(fp, 'option-spread-trade-grid');
		grid.store.removeAll();

		if (TCG.isBlank(clientAccountGroupId)) {
			TCG.showError('Client account group ID value cannot be null.');
			return null;
		}

		TCG.data.getDataPromise('investmentAccountGroupAccountListFind.json?groupId=' + clientAccountGroupId, this)
			.then(function(resultArray) {
				const serviceProcessingTypeIdSet = new Set();
				const accountsWithNullProcessingType = [];
				clientAccountGroupCombo.businessServiceProcessingTypeId = null;

				if (TCG.isNotBlank(resultArray) && resultArray.length > 0) {
					for (const resultEntry of resultArray) {
						const businessServiceProcessingType = resultEntry.referenceTwo.serviceProcessingType;
						if (TCG.isNotBlank(businessServiceProcessingType)) {
							if (!serviceProcessingTypeIdSet.has(businessServiceProcessingType.id)) {
								serviceProcessingTypeIdSet.add(businessServiceProcessingType.id);
							}
						}
						else {
							accountsWithNullProcessingType.push(resultEntry.referenceTwo.label);
						}
					}

					// Validate and apply results
					const serviceProcessingTypeIdArray = Array.from(serviceProcessingTypeIdSet);
					if (serviceProcessingTypeIdSet.size === 0) {
						TCG.showError('No accounts in the selected account group are configured with a business service processing type.', 'Account Configuration Error');
					}
					else if (accountsWithNullProcessingType.length > 0) {
						const accounts = accountsWithNullProcessingType.join(', ');
						TCG.showError('Accounts within the selected group are missing business service processing types: ' + accounts + '.', 'Account Configuration Error');
					}
					else if (serviceProcessingTypeIdSet.size === 1) {
						clientAccountGroupCombo.businessServiceProcessingTypeId = serviceProcessingTypeIdArray[0];
					}
					else {
						TCG.showError('Accounts with differing business service processing types found in the selected account group.', 'Account Group Configuration Error');
					}
				}
				else {
					TCG.showError('No accounts found for selected account group.', 'Account Group Configuration Error');
				}

				fp.updateOptionSelectionComboState();
			});
	},

	clearAndResetOptionSelectionCombos: function() {
		const frm = this.getForm();
		const comboFields = ['longPutSecurity.label', 'shortPutSecurity.label', 'longCallSecurity.label', 'shortCallSecurity.label'];
		for (const comboName of comboFields) {
			const combo = frm.findField(comboName);
			combo.clearAndReset();
		}
	},

	clearOptionPriceAndDeltaValues: function() {
		const frm = this.getForm();
		const priceAndDeltaFields = ['longPutDelta', 'longPutPrice', 'shortPutDelta', 'shortPutPrice', 'shortCallDelta', 'shortCallPrice', 'longCallDelta', 'longCallPrice'];
		for (const fieldName of priceAndDeltaFields) {
			const field = frm.findField(fieldName);
			field.setValue('');
		}
	}

});
Ext.reg('option-spread-form-panel', Clifton.trade.option.OptionSpreadTradingForm);

Clifton.trade.option.OptionSpreadTradingFormGrid = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicDetailGrid, {
	name: 'option-spread-trade-grid',
	dtoClass: 'com.clifton.trade.options.securitytarget.group.TradeGroupDynamicSecurityTargetOptionSpreadDetail',
	instructions: 'Option Spread screen used to create Option Put and Call Spreads for a tranche based on Client Account Security Targets',
	preventReloadOnInvalidForm: true,

	alwaysSubmitFields: ['holdingAccount.id', 'clientAccount.id', 'longPutTrade.id', 'shortPutTrade.id', 'shortCallTrade.id', 'longCallTrade.id'],
	doNotSubmitFields: ['includeTrade'],
	viewConfig: {
		markDirty: false, // all records are dirty for entry, and none are for review
		forceFit: true
	},

	plugins: [
		{
			ptype: 'gridsummary'
		},
		{
			ptype: 'trade-restriction-aware',
			getRecordClientInvestmentAccount: record => record.json.clientAccount,
			getRecordInvestmentSecurity: record => record.json.securityTarget.targetUnderlyingSecurity,
			getRecordQuantityName: () => 'tradeQuantity',
			getTradePositionClientInvestmentAccount: position => ({id: position['clientAccount.id'], label: position['clientAccount.label']}),
			getTradePositionInvestmentSecurity: function(position) {
				return this.grid.ownerCt.form.formValues.optionUnderlyingSecurity;
			}
		}
	],

	addEntryToolbarButtons: function(toolBar, gridpanel) {
		const gp = gridpanel;
		toolBar.add({
			text: 'Load Positions',
			tooltip: 'Load tradable accounts for the selected account group.',
			iconCls: 'table-refresh',
			scope: this,
			handler: function() {
				gp.submitField.setValue('[]');
				gp.reloadGrid();
			}
		});
		toolBar.add('-');

		toolBar.add({
			text: 'Submit Trades',
			tooltip: 'Create New Option Trades based on the calculated contract quantities shown below.',
			iconCls: 'shopping-cart',
			scope: this,
			handler: async function() {
				const submitValue = this.submitField.getValue();
				if (submitValue === '[]') {
					TCG.showError('No rows selected.  Please select at least one row from the list.');
					return;
				}
				const restrictionCheckResult = gp.ownerCt.checkOptionSpreadTradeRestrictions();
				const tradePositions = await gp.getTradeRestrictionActionForPositions(restrictionCheckResult);
				if (!tradePositions) {
					return;
				}
				this.submitField.setValue(JSON.stringify(tradePositions));
				gp.getWindow().saveWindow(false);
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Clear Trades',
			tooltip: 'Clear all Given Amounts entered below.',
			iconCls: 'clear',
			scope: this,
			handler: function() {
				gp.clearTrades();
			}
		});
		toolBar.add('-');
		toolBar.add({
			text: 'Info',
			tooltip: 'Toggle instructions for this page',
			iconCls: 'info',
			enableToggle: true,
			handler: function() {
				if (this.pressed) {
					gp.insert(0, {
						xtype: 'panel',
						frame: true,
						layout: 'fit',
						bodyStyle: 'padding: 3px 3px 3px 3px',
						html: gp.instructions
					});
				}
				else {
					gp.remove(0);
				}
				gp.doLayout();
			}
		}, '-');
	},

	addAdditionalReviewToolbarButtonsLeftSide: function(gridpanel, toolBar) {
		toolBar.add({
			text: 'Update Commissions...',
			tooltip: 'Update Commission prices for trades in the Option Spread',
			iconCls: 'add',
			scope: this,
			handler: function() {
				gridpanel.openActionWindow('Clifton.trade.group.TradeGroupDynamicOptionSpreadApplyTradeCommissionPricesWindow', toolBar, gridpanel);
			}
		});

		toolBar.add({
			text: 'Update Fill Prices...',
			tooltip: 'Update Fill prices for trades in the Option Spread',
			iconCls: 'add',
			scope: this,
			handler: function() {
				gridpanel.openActionWindow('Clifton.trade.group.TradeGroupDynamicOptionSpreadApplyTradeFillPricesWindow', toolBar, gridpanel);
			}
		});

		toolBar.add({
			text: 'Update Trades...',
			tooltip: 'Update the Trade Destination and/or Option Securities for Trades of this Option Group.',
			iconCls: 'add',
			scope: this,
			handler: function() {
				const fp = TCG.getParentFormPanel(gridpanel);
				const form = fp.form;

				const tradeDate = TCG.isNotBlank(form.findField('tradeDate')) ? form.findField('tradeDate').getValue() : null;
				const lastDeliveryDate = TCG.isNotBlank(form.findField('optionSecurity.lastDeliveryDate')) ? form.findField('optionSecurity.lastDeliveryDate').getValue() : null;

				const defaultData = {
					openerWindow: this,
					lastDeliveryDate: lastDeliveryDate,
					tradeDate: tradeDate,
					prevLongPutSecurity: form.formValues.longPutSecurity,
					prevShortPutSecurity: form.formValues.shortPutSecurity,
					prevShortCallSecurity: form.formValues.shortCallSecurity,
					prevLongCallSecurity: form.formValues.longCallSecurity,
					optionUnderlyingSecurity: form.formValues.optionUnderlyingSecurity
				};

				TCG.createComponent('Clifton.trade.option.OptionSpreadTradeUpdateWindow', {defaultData: defaultData});
			}
		});
	},

	clearTrades: function() {
		const gp = this;
		gp.getStore().each(function(record) {
			gp.clearRecordTradeQuantities(record);
		}, this);
		gp.markModified();
		gp.getView().refresh();
	},

	launchEditorWindow: function(componentName, id, defaultData) {
		if (TCG.isNotBlank(componentName) && TCG.isNotBlank(id)) {
			const config = {openerCt: this};
			if (TCG.isNotBlank(id)) {
				config.id = TCG.getComponentId(componentName, id);
				config.params = {id: id};
			}
			if (TCG.isNotNull(defaultData)) {
				config.defaultData = defaultData;
			}
			TCG.createComponent(componentName, config);
		}
	},

	commonColumns: [
		{
			width: 30, dataIndex: 'includeTrade', align: 'center', xtype: 'checkcolumn', sortable: false, filter: false, menuDisabled: true,
			header: '<div class="x-grid3-check-col">&#160;</div>',
			renderer: function(value, metadata, record) {
				let v = value;
				metadata.css += ' x-grid3-check-col-td';
				if (TCG.isBlank(v)) {
					record.data['includeTrade'] = false;
					v = false;
				}
				return `<div class="x-grid3-check-col${v ? '-on' : ''}">&#160;</div>`;
			}
		},
		{
			header: 'Client Account', width: 150, dataIndex: 'clientAccount.label',
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const accountId = TCG.getValue('clientAccount.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.investment.account.CliftonAccountWindow', accountId);
				},
				contextmenu: function(column, grid, rowIndex, event) {
					const restrictionItems = grid.getRestrictionActionMenuItems(rowIndex, column);
					if (restrictionItems.length > 0) {
						const menu = new Ext.menu.Menu({items: restrictionItems});
						event.preventDefault();
						menu.showAt(event.getXY());
					}
				}
			},
			renderer: function(value, metaData, record) {
				const notes = (TCG.getValue('validationNoteList', record.json) || []).map(note => `<li  style="padding-bottom:10px;">${note}</li>`);
				let notesRow = '';
				if (notes.length > 0) {
					notesRow = `<div>Validation Concerns:</div><ul>${notes.join('')}</ul>`;
					metaData.css += 'x-grid3-row-highlight-light-red';
				}
				this.scope.gridPanel.applyTradeRestrictionGridClientAccountColumnFreeze(value, metaData, record, true, notesRow);
				// no security column in this grid, so highlight client account column if there is a security restriction
				this.scope.gridPanel.applyTradeRestrictionGridSecurityColumnFreeze(metaData, record, record.underlyingSecurity);
				return value;
			}
		},
		// Holding Account - Editable for Trade Entry, Read Only for Trade Review
		{
			header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.number', idDataIndex: 'holdingAccount.id', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tradeEntry: true,
			tooltip: 'Holding Account to trade new Options under.',
			editor: {
				xtype: 'combo', displayField: 'number', url: 'investmentAccountListFind.json?ourAccount=false', tooltipField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', addGridCopyContextMenu: false,
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					this.clearAndReset();
					const editor = queryEvent.combo.gridEditor;
					const record = editor.record;
					const grid = editor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					queryEvent.combo.store.baseParams = {
						mainAccountId: TCG.getValue('clientAccount.id', record.json),
						mainPurpose: 'Trading: Options',
						mainPurposeActiveOnDate: fp.getForm().findField('tradeDate').value
					};
				}
			}
		},
		{
			header: 'Holding Account', tradeEntry: false, width: 100, dataIndex: 'holdingAccount.number', idDataIndex: 'holdingAccount.id',
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const accountId = TCG.getValue('holdingAccount.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.investment.account.AccountWindow', accountId);
				}
			}
		},
		{
			header: 'Executing Broker', width: 100, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id',
			tradeEntry: true,
			tooltip: 'Executing Broker Company to trade new Options under. This column will be blank by default and will be set to the value of the defined Executing Broker from the Trade Info section of this window. If a row has an automatically populated value, the client account is set up with a directed broker',
			editor: {
				xtype: 'combo', displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', addGridCopyContextMenu: false,
				beforequery: function(queryEvent) {
					// Reset Combo so re-queries each time (since each row has different results)
					this.clearAndReset();
					const editor = queryEvent.combo.gridEditor;
					const record = editor.record;
					const grid = editor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);
					queryEvent.combo.store.baseParams = {
						clientInvestmentAccountId: TCG.getValue('clientAccount.id', record.json),
						tradeTypeNameEquals: 'Options',
						tradeDestinationId: fp.getForm().findField('tradeDestination.id').getValue(),
						activeOnDate: fp.getForm().findField('tradeDate').value
					};
				},
				listeners: {
					select: function(combo) {
						// clear the TradeDestination value because it depends on the Executing Broker field value
						combo.gridEditor.record.set('tradeDestination.name', null);
					}
				}
			}
		},
		{
			header: 'Executing Broker', tradeEntry: false, width: 100, dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id',
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const brokerId = TCG.getValue('executingBrokerCompany.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.business.company.CompanyWindow', brokerId);
				}
			}
		},
		{
			header: 'Trade Destination', dataIndex: 'tradeDestination.name', width: 100, idDataIndex: 'tradeDestination.id', allowBlank: true,
			tradeEntry: true,
			editor: {
				xtype: 'combo', url: 'tradeDestinationListFind.json',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const grid = combo.gridEditor.containerGrid;
					const fp = TCG.getParentFormPanel(grid);

					// Clear Query Store so Re-Queries each time
					this.store.removeAll();
					this.lastQuery = null;

					const record = queryEvent.combo.gridEditor.record;
					const executingBrokerCompanyId = record.get('executingBrokerCompany.id');
					const tradeDate = fp.getForm().findField('tradeDate').value;
					const activeOnDate = TCG.isNotBlank(tradeDate) ? tradeDate : new Date().format('Y-m-d 00:00:00');
					queryEvent.combo.store.baseParams = {
						tradeTypeNameEquals: 'Options',
						executingBrokerCompanyId: executingBrokerCompanyId && executingBrokerCompanyId > 0 ? executingBrokerCompanyId : null,
						activeOnDate: activeOnDate
					};
				}
			}
		},
		{
			header: 'Trade Destination', dataIndex: 'tradeDestination.name', width: 100, idDataIndex: 'tradeDestination.id', allowBlank: true,
			tradeEntry: false,
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const tradeDestinationId = TCG.getValue('tradeDestination.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.trade.destination.TradeDestinationWindow', tradeDestinationId);
				}
			}
		}
	],

	customColumns: [
		{
			header: 'VAR', dataIndex: 'valueAtRiskPercent', type: 'float', width: 45, submitValue: false, numberFormat: '0,000.00',
			tooltip: 'Configured Value At Risk for this Client Account'
		},
		{
			header: 'Tranches', dataIndex: 'securityTarget.trancheCount', type: 'int', width: 65, submitValue: false, numberFormat: '0,000.00',
			tooltip: 'Security Target configured tranche count for Underlying Security and this Client Account',
			renderer: function(value, metaData, record, rowIndex, colIndex, store) {
				if (TCG.isBlank(value) || value === 0) {
					value = Clifton.business.service.getTrancheCountFromBusinessServiceProcessingType(record.json.clientAccount.serviceProcessingType.id);
				}
				return value;
			},
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const securityTargetId = TCG.getValue('securityTarget.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.investment.account.securitytarget.AccountSecurityTargetWindow', securityTargetId);
				}
			}
		},
		{
			header: 'Notional', dataIndex: 'securityTargetNotional', type: 'currency', width: 100, submitValue: false,
			tooltip: 'Notional target of the underlying security for this client account',
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const securityTargetId = TCG.getValue('securityTarget.id', gridPanel.getStore().getAt(rowIndex).json);
					gridPanel.launchEditorWindow.call(gridPanel, 'Clifton.investment.account.securitytarget.AccountSecurityTargetWindow', securityTargetId);
				}
			}
		},
		{
			header: 'Avg Tranche Notional', dataIndex: 'averageTrancheNotional', type: 'currency', width: 100, submitValue: false, hidden: true,
			tooltip: 'Average Tranche Notional calculated using the notional target of the underlying security tranches for this client account'
		},
		{
			header: 'Avg Tranche VAR', dataIndex: 'averageTrancheValueAtRisk', type: 'currency', width: 100, submitValue: false,
			tooltip: 'Average Tranche Value At Risk calculated using the notional target of the underlying security, tranches, and VAR for this client account'
		},
		{
			header: 'Target Qty', dataIndex: 'targetUnderlyingQuantity', type: 'int', width: 80, submitValue: false,
			tooltip: 'Target quantity of the underlying security for this client account. If the security target is configured with a notional value, this value is calculated from the underlying price. Reviewing the Option Spread after created will use the last system saved price (not live price). Value is always positive.'
		},
		{
			header: 'Covered Qty', dataIndex: 'unexpiredCoveredUnderlyingQuantity', type: 'int', width: 80, submitValue: false,
			tooltip: 'Underlying quantity of aggregated unexpired Option positions quantities adjusted to the target\'s underlying security quantity. Value is always positive.'
		},
		{
			header: 'Free Qty', dataIndex: 'availableUnderlyingQuantity', type: 'int', width: 80, submitValue: false, negativeInRed: true,
			tooltip: 'Underlying quantity available to trade to fully utilize the target. This value is the difference between Target Quantity and Covered Quantity. Negative if the target is over utilized.'
		},
		{
			header: 'Available Qty', dataIndex: 'availableQuantity', type: 'int', width: 80, submitValue: false, negativeInRed: true,
			tooltip: 'Option quantity available to trade to fully utilize the target. This value is the Free Und. Qty. adjusted from the target\'s underlying quantity.'
		},
		{
			header: 'Expiring Qty', dataIndex: 'expiringQuantity', type: 'float', width: 80, submitValue: false, summaryType: 'sum', numberFormat: '0,000.00',
			tooltip: 'Option spread quantity expiring on the specified balance date'
		},
		{
			header: 'Quantity', tradeEntry: true, dataIndex: 'tradeQuantity', type: 'float', width: 80, submitValue: true, summaryType: 'sum', numberFormat: '0,000.00', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Calculated spread quantity to trade for each of the specified Options',
			editor: {xtype: 'spinnerfield', minValue: 0, allowBlank: true}
		},
		{
			header: 'Quantity', tradeEntry: false, dataIndex: 'tradeQuantity', type: 'float', width: 80, submitValue: true, summaryType: 'sum', numberFormat: '0,000.00', css: 'BACKGROUND-COLOR: #f0f0ff; BORDER-LEFT: #c0c0c0 1px solid;',
			tooltip: 'Calculated spread quantity to trade for each of the specified Options',
			eventListeners: {
				dblclick: function(column, gridPanel, rowIndex, event) {
					const record = gridPanel.getStore().getAt(rowIndex);
					const id = TCG.getParentFormPanel(gridPanel).getForm().idFieldValue;
					gridPanel.launchEditorWindow('Clifton.trade.TradeGroupTradeListReviewWindow', id, {
						clientInvestmentAccount: {
							id: record.json.clientAccount.id,
							label: record.json.clientAccount.label
						}
					});
				}
			}
		},
		{
			header: 'PS Exposure', dataIndex: 'putSpreadExposure', type: 'currency', width: 100, submitValue: false,
			tooltip: 'Max Put Spread Exposure calculated with the Trade Quantity'
		},
		{
			header: 'CS Exposure', dataIndex: 'callSpreadExposure', type: 'currency', width: 100, submitValue: false,
			tooltip: 'Max Call Spread Exposure calculated with the Trade Quantity'
		}
	],

	handleAfterLoadTransientRecordValues: function(grid) {
		grid.reloadToolbarMenus();
		grid.markModified(true);
		const panel = TCG.getParentFormPanel(grid);
		const form = panel.getForm();
		const actionField = form.findField('tradeGroupAction');
		if (TCG.isNotBlank(actionField)) {
			// since we load the response data, make sure to clear action
			actionField.setValue('');
		}

		if (TCG.isNotNull(grid.chainedRequestCallback)) {
			if (grid.applyChainedCallback === true) {
				grid.chainedRequestCallback.call(grid);
			}
			else {
				// clear the callback as the previous request failed and the callback was not executed
				grid.chainedRequestCallback = undefined;
			}
		}
	},

	clearRecordTradeQuantities: function(record) {
		record.beginEdit();
		record.data['tradeQuantity'] = '0';
		record.endEdit();
	},

	openActionWindow: function(windowClass, toolBar, gridPanel) {
		const panel = TCG.getParentFormPanel(this);
		const form = panel.getForm();
		TCG.createComponent(windowClass, {
			defaultData: form.formValues,
			openerCt: gridPanel,
			useQuantityOnTrade: gridPanel.useQuantityOnTrade,
			emailFileNamePrefix: gridPanel.emailFileNamePrefix
		});
	},

	applyTradeUpdates: function(tradeValues, callback) {
		const grid = this;
		const panel = TCG.getParentFormPanel(grid);
		const form = panel.getForm();
		Object.keys(tradeValues).forEach(function(key, index) {
			const value = tradeValues[key];
			if (TCG.isNotBlank(value)) {
				const f = form.findField(key);
				if (f) {
					f.setValue(value);
				}
			}
		});

		const tradeDetailListField = form.findField('detailList');
		if (tradeDetailListField) {
			const detailListString = tradeDetailListField.getValue();
			if (TCG.isBlank(detailListString) || '[]' === detailListString) {
				// clear group action field and callback
				form.findField('tradeGroupAction').setValue(null);
				TCG.showError('Please select at least one row to modify trade(s) for.', 'No Row(s) Selected');
				return;
			}
			if (callback && TCG.isBlank(tradeValues.detailList)) {
				tradeValues['detailList'] = detailListString;
			}
		}

		grid.applyChainedCallback = callback !== undefined;
		grid.chainedRequestCallback = callback; // will clear the callback if not present
		if (this.getWindow().isModified()) {
			this.getWindow().saveWindow(false);
		}
	}
});
Ext.reg('option-spread-form-grid', Clifton.trade.option.OptionSpreadTradingFormGrid);

Clifton.trade.group.TradeGroupDynamicOptionSpreadApplyTradeCommissionPricesWindow = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow, {
	title: 'Option Spread Trade Group Commission Prices',
	height: 250,
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify the Commission Prices to apply to the Put and Call trades within this Option Group.',
			labelWidth: 150,
			items: [
				{xtype: 'sectionheaderfield', header: 'Option Spread Trade Commission Prices'},
				{fieldLabel: 'Long Put Commission', name: 'longPutCommissionPerUnit', xtype: 'pricefield', qtip: 'Commission override for all Long Put Trades.'},
				{fieldLabel: 'Short Put Commission', name: 'shortPutCommissionPerUnit', xtype: 'pricefield', qtip: 'Commission override  for all Short Put Trades.'},
				{fieldLabel: 'Short Call Commission', name: 'shortCallCommissionPerUnit', xtype: 'pricefield', qtip: 'Commission override  for all Short Call Trades.'},
				{fieldLabel: 'Long Call Commission', name: 'longCallCommissionPerUnit', xtype: 'pricefield', qtip: 'Commission override for all Long Call Trades.'}
			],

			listeners: {
				afterrender: function() {
					// hide non-applicable fields
					const defaultData = this.ownerCt.defaultData;
					const form = this.getForm();
					if (TCG.isNotBlank(defaultData.detailList) && defaultData.detailList.length > 0) {
						const firstEntry = defaultData.detailList[0];

						if (TCG.isBlank(firstEntry.longPutTrade)) {
							form.findField('longPutCommissionPerUnit').hide();
						}

						if (TCG.isBlank(firstEntry.shortPutTrade)) {
							form.findField('shortPutCommissionPerUnit').hide();
						}

						if (TCG.isBlank(firstEntry.shortCallTrade)) {
							form.findField('shortCallCommissionPerUnit').hide();
						}

						if (TCG.isBlank(firstEntry.longCallTrade)) {
							form.findField('longCallCommissionPerUnit').hide();
						}
					}
				}
			}
		}
	],

	invokeOpenerApplyTradeUpdates: function(params) {
		const form = this.getForm();
		const hasCommissionPrices = TCG.isNotBlank(form.findField('longPutCommissionPerUnit').getValue()) || TCG.isNotBlank(form.findField('shortPutCommissionPerUnit').getValue()) || TCG.isNotBlank(form.findField('shortCallCommissionPerUnit').getValue()) || TCG.isNotBlank(form.findField('longCallCommissionPerUnit').getValue());
		const openerCt = this.openerCt;

		if (hasCommissionPrices) {
			params['tradeGroupAction'] = 'SAVE_TRADE';
			openerCt.applyTradeUpdates.call(openerCt, params);
		}

	}
});
Ext.reg('trade-group-dynamic-option-spread-apply-trade-commission-prices', Clifton.trade.group.TradeGroupDynamicOptionSpreadApplyTradeCommissionPricesWindow);

Clifton.trade.group.TradeGroupDynamicOptionSpreadApplyTradeFillPricesWindow = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow, {
		title: 'Option Spread Trade Group Trade Fill Prices',
		height: 250,
		items: [
			{
				xtype: 'formpanel',
				loadValidation: false,
				instructions: 'Specify the Fill Prices to apply to the Put and Call trades within this Option Group.',
				labelWidth: 150,
				items: [
					{xtype: 'sectionheaderfield', header: 'Option Spread Trade Fill Prices'},
					{fieldLabel: 'Long Put Average Price', name: 'longPutFillPrice', xtype: 'pricefield', qtip: 'Average fill price all Long Put Trades.'},
					{fieldLabel: 'Short Put Average Price', name: 'shortPutFillPrice', xtype: 'pricefield', qtip: 'Average fill price all Short Put Trades.'},
					{fieldLabel: 'Short Call Average Price', name: 'shortCallFillPrice', xtype: 'pricefield', qtip: 'Average fill price for all Short Call Trades.'},
					{fieldLabel: 'Long Call Average Price', name: 'longCallFillPrice', xtype: 'pricefield', qtip: 'Average fill price all Long Call Trades.'}
				],

				listeners: {
					afterrender: function() {
						// hide non-applicable fields
						const defaultData = this.ownerCt.defaultData;
						const form = this.getForm();
						if (TCG.isNotBlank(defaultData.detailList) && defaultData.detailList.length > 0) {
							const firstEntry = defaultData.detailList[0];

							if (TCG.isBlank(firstEntry.longPutTrade)) {
								form.findField('longPutFillPrice').hide();
							}

							if (TCG.isBlank(firstEntry.shortPutTrade)) {
								form.findField('shortPutFillPrice').hide();
							}

							if (TCG.isBlank(firstEntry.shortCallTrade)) {
								form.findField('shortCallFillPrice').hide();
							}

							if (TCG.isBlank(firstEntry.longCallTrade)) {
								form.findField('longCallFillPrice').hide();
							}
						}
					}
				}
			}
		],

		invokeOpenerApplyTradeUpdates: function(params) {
			const form = this.getForm();
			const hasFillPrices = TCG.isNotBlank(form.findField('longPutFillPrice').getValue()) || TCG.isNotBlank(form.findField('shortPutFillPrice').getValue()) || TCG.isNotBlank(form.findField('shortCallFillPrice').getValue()) || TCG.isNotBlank(form.findField('longCallFillPrice').getValue());
			const openerCt = this.openerCt;

			if (hasFillPrices) {
				params['tradeGroupAction'] = 'UPDATE_FILLS';
				openerCt.applyTradeUpdates.call(openerCt, params);
			}
		}
	}
);
Ext.reg('trade-group-dynamic-option-spread-apply-trade-fill-prices', Clifton.trade.group.TradeGroupDynamicOptionSpreadApplyTradeFillPricesWindow);
