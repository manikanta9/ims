TCG.use('Clifton.trade.group.FungeTradeGroupWindow');

Clifton.trade.option.OptionsBlotterWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'optionsBlotterWindow',
	title: 'Options Blotter',
	iconCls: 'options',
	width: 1500,
	height: 750,

	// Additional Tabs that are Added need "save" feature from the Detail Window, even though the window won't look like a detail window
	hideApplyButton: true,
	hideOKButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,


	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.trade.option.OptionBlotterAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
		// Add Trade Groups at the End of the Other Tabs
		tabs.add(this.tradeGroupsTab);
	},

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Expiration',
				items: [{xtype: 'optionsExpirationGrid'}]
			},
			{
				title: 'Delivery',
				items: [{xtype: 'optionsDeliveryGrid'}]
			},
			{
				title: 'Funge',
				items: [{xtype: 'funge-trade-group-entry-form-panel'}]
			}
		]
	}],

	tradeGroupsTab: {
		title: 'Trade Groups',
		reloadOnTabChange: true,
		items: [{
			name: 'tradeGroupListFind',
			xtype: 'gridpanel',
			instructions: 'The following list contains all Option-specific Trade Groups. Trade Groups are collections of trades executed for a purpose, i.e. Option Delivery, Option Expiration, Option Rolls',
			columns: [
				{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
				{header: 'Workflow Status', width: 15, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
				{header: 'Workflow State', width: 15, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
				{header: 'Group Type', width: 20, dataIndex: 'tradeGroupType.name', filter: {searchFieldName: 'tradeGroupTypeName'}},
				{header: 'Group Note', width: 80, dataIndex: 'note', filter: false, hidden: true},
				{header: 'Security', width: 20, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
				{header: '2nd Security', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
				{header: 'Trader', width: 13, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', displayField: 'label', url: 'securityUserListFind.json'}},
				{header: 'Traded On', width: 15, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'}
			],
			getTopToolbarFilters: function(toolbar) {
				return [
					{
						fieldLabel: '', boxLabel: 'Exclude Strangle Groups', name: 'excludeStrangleGroupTypes', xtype: 'toolbar-checkbox', checked: true,
						qtip: 'If checked, the Option Strangle and Option Tail trade groups will be excluded so only the highest level trade groups are displayed.'
					}
				];
			},
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
				}
				const params = {
					includeBeginsWithTradeGroupTypeNames: ['Option', 'Funge'],
					readUncommittedRequested: true
				};

				const gridPanel = this;
				const toolbar = gridPanel.getTopToolbar();
				const excludeStrangleGroupTypesCheckbox = TCG.getChildByName(toolbar, 'excludeStrangleGroupTypes');
				let groupExclusionPromise;
				if (TCG.isTrue(excludeStrangleGroupTypesCheckbox.getValue())) {
					const excludedTradeGroupIds = [];
					groupExclusionPromise = TCG.data.getDataPromiseUsingCaching('tradeGroupTypeByName.json?name=Option Strangle', gridPanel, 'trade.group.type.Option Strangle')
						.then(function(strangleGroup) {
							if (strangleGroup) {
								excludedTradeGroupIds.push(strangleGroup.id);
							}
							return TCG.data.getDataPromiseUsingCaching('tradeGroupTypeByName.json?name=Option Tail', gridPanel, 'trade.group.type.Option Tail');
						})
						.then(function(tailGroup) {
							if (tailGroup) {
								excludedTradeGroupIds.push(tailGroup.id);
							}
						})
						.then(function() {
							if (excludedTradeGroupIds.length > 0) {
								params['excludeTradeGroupTypeIds'] = excludedTradeGroupIds;
							}
						});
				}
				return Promise.resolve(groupExclusionPromise)
					.then(function() {
						return params;
					});
			},
			editor: {
				detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
				drillDownOnly: true
			}
		}]
	}
});

Clifton.trade.option.BaseOptionPositionsGrid = Ext.extend(TCG.grid.EditorGridPanel, {
	name: 'tradeOptionPositionBalanceListFind',
	rowSelectionModel: 'checkbox',
	groupField: 'positionBalance.investmentSecurity.symbol',
	groupTextTpl: '<div ext:qtip="Right click to display the grouping context menu with additional features">{values.group} - Underlying {[values.rs[0].data["positionBalance.investmentSecurity.underlyingSecurity.symbol"]]} ({[values.rs.length]} {[values.rs.length > 1 ? "Accounts" : "Account"]})</div>',
	additionalPropertiesToRequest: 'positionBalance.clientInvestmentAccount.id|positionBalance.holdingInvestmentAccount.id|positionBalance.investmentSecurity.id|executingBroker.id|executingBroker.id|positionBalance.investmentSecurity.underlyingSecurity.id|positionBalance.investmentSecurity.instrument.hierarchy.otc|positionBalance.investmentSecurity.instrument.hierarchy.investmentTypeSubType.name',
	appendStandardColumns: false,
	highlightInstructions: 'Row highlighting is used based on \'In the Money\' (ITM) calculations based on the row\'s strike price in relation to the security\'s underlying instrument price. Rows calculated as \'In the Money\' will be highlighted in red and rows calculated as \'Out of the Money\' with a value less than 1% will be highlighted in yellow.',
	defaultClientAccountGroup: 'VRP - Minneapolis: Service Defined Group',

	plugins: {ptype: 'groupsummary', showGrandTotal: true},

	viewConfig: {
		getRowClass: function(record) {
			const percentItm = record.get('percentInTheMoney');
			if (percentItm) {
				if (percentItm < 0) {
					// in the money
					return 'x-grid3-row-highlight-light-red';
				}
				else if (percentItm < 1) {
					return 'x-grid3-row-highlight-light-yellow';
				}
			}
			return ''; // default
		}
	},

	gridConfig: {
		listeners: {
			'celldblclick': function(grid, rowIndex, cellIndex, evt) {
				const row = grid.store.data.items[rowIndex],
					column = grid.getColumnModel().getColumnById(cellIndex);
				let componentName = '',
					id = '';

				if (column.header.includes('Client')) {
					componentName = 'Clifton.investment.account.AccountWindow';
					id = TCG.getValue('positionBalance.clientInvestmentAccount.id', row.json);
				}
				else if (column.header.includes('Holding')) {
					componentName = 'Clifton.investment.account.AccountWindow';
					id = TCG.getValue('positionBalance.holdingInvestmentAccount.id', row.json);
				}
				else if (column.header.includes('Security')) {
					componentName = 'Clifton.investment.instrument.SecurityWindow';
					id = TCG.getValue('positionBalance.investmentSecurity.id', row.json);
				}
				else if (column.header.includes('Underlying')) {
					componentName = 'Clifton.investment.instrument.SecurityWindow';
					id = TCG.getValue('positionBalance.investmentSecurity.underlyingSecurity.id', row.json);
				}

				if (TCG.isNotBlank(componentName) && TCG.isNotBlank(id)) {
					TCG.createComponent(componentName, {
						id: TCG.getComponentId(componentName, id),
						params: {id: id},
						openerCt: grid
					});
				}
			}
		}
	},

	addGridGroupSelectionContextMenu: function(gridPanel) {
		// register reset of group selection state on load
		gridPanel.grid.store.on('load', () => gridPanel.groupSelectionState = {});
		// register group context menu
		gridPanel.grid.on('groupcontextmenu', function(grid, groupField, groupValue, event) {
			event.preventDefault();
			const gridPanel = this;
			gridPanel.groupValue = groupValue;
			if (!grid.drillDown) {
				// set context menu on grid once rather than on every right-click
				grid.drillDown = new Ext.menu.Menu({
					items: [
						{
							text: '(De)Select all rows in this group.',
							iconCls: 'copy',
							handler: function() {
								const gp = this;
								if (!gp.groupSelectionState) {
									gp.groupSelectionState = {};
								}
								gp.groupSelectionState[gp.groupValue] = gp.groupSelectionState[gp.groupValue] ? !gp.groupSelectionState[gp.groupValue] : true;
								const grid = gp.grid;
								grid.getStore().each(function(record, index) {
									if (TCG.isEquals(record.get(gp.groupField), gp.groupValue)) {
										const selectionModel = grid.getSelectionModel();
										if (gp.groupSelectionState[gp.groupValue]) {
											selectionModel.selectRow(index, true);
										}
										else {
											selectionModel.deselectRow(index);
										}
									}
								});
							},
							scope: gridPanel
						},
						{
							text: 'Expand or Collapse all groups.',
							iconCls: 'copy',
							handler: function() {
								this.expandCollapseGroups.call(this);
							},
							scope: gridPanel
						}
					]
				});
			}
			grid.drillDown.showAt(event.getXY());
		}, gridPanel);
	},

	expandCollapseGroups: function() {
		this.grid.collapsed = !this.grid.collapsed;
		this.grid.view.toggleAllGroups(!this.grid.collapsed);
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			'-',
			{fieldLabel: 'Team', name: 'teamSecurityGroup', hiddenName: 'teamSecurityGroupId', width: 180, xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true},
			{fieldLabel: 'Client Acct Group', name: 'clientGroup', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json?accountTypeName=Client'}
		];
	},

	updateRecord: function(store, record, index) {
		//Overridden to avoid saving upon changes to cells
	},

	// Returns a promise that resolves the load parameters object
	getLoadParams: function(firstLoad) {
		const gridPanel = this,
			toolbar = gridPanel.getTopToolbar();

		// return a promise that conditionally populates initial load defaults, followed by a promise populating params
		return new Promise(function(resolveCallback) {
			const parameters = {};
			if (firstLoad) {
				gridPanel.setFilterValue('positionBalance.investmentSecurity.lastDeliveryDate', {'on': new Date()});
				// Populate defaults for account group defaults
				const clientAccountGroupDefaultParams = {
					name: gridPanel.defaultClientAccountGroup,
					requestedPropertiesToExclude: 'investmentAccountGroupSearchForm'
				};
				return TCG.data.getDataPromiseUsingCaching('investmentAccountGroupListFind.json', gridPanel, 'investment.accountGroup.' + clientAccountGroupDefaultParams.name, {params: clientAccountGroupDefaultParams})
					.then(function(clientAccountGroup) {
						if (TCG.isNotNull(clientAccountGroup) && clientAccountGroup.length === 1) {
							TCG.getChildByName(toolbar, 'clientGroup').setValue({value: clientAccountGroup[0].id, text: clientAccountGroup[0].name});
						}
					}).then(function() {
						// call the chained promise callback after defaults are populated
						return resolveCallback(parameters);
					});
			}
			else {
				// call the chained promise callback immediately
				return resolveCallback(parameters);
			}
		}).then(function(params) {
			let toolbarField = TCG.getChildByName(toolbar, 'clientGroup');
			if (TCG.isNotBlank(toolbarField.getValue())) {
				params.clientInvestmentAccountGroupId = toolbarField.getValue();
			}
			toolbarField = TCG.getChildByName(toolbar, 'teamSecurityGroup');
			if (TCG.isNotBlank(toolbarField.getValue())) {
				params.teamSecurityGroupId = toolbarField.getValue();
			}
			return params;
		});
	},

	isPagingEnabled: function() {
		return false;
	},

	/**
	 * Processes a trading request for the provided url and details provided. The selected rows in the grid are used to
	 * populate OptionRequestDetails, which the server will use to generate trades and trade group(s).
	 *
	 * @param tradeGroupTypeName the name of the trade group type to use for the request
	 * @param gridPanel the current grid panel for populiting details for selected rows
	 * @param confirmationTitle title of the confirmation window
	 * @param confirmationText text of the confirmation window
	 * @param tradeDestinationId trade destination for trades to be created
	 * @param tradeQuantityField quantity field to use for selected grid rows
	 * @param btic true if trades should be generated with the BTIC/Block Trade flag set for determining commissions
	 */
	processRequest: function(tradeGroupTypeName, gridPanel, confirmationTitle, confirmationText, tradeDestinationId, tradeQuantityField, btic) {
		const grid = this.grid;
		const sm = grid.getSelectionModel();

		if (TCG.isEquals(sm.getCount(), 0)) {
			TCG.showError('Please select at least one row to process.', 'No Row(s) Selected');
		}
		else {
			TCG.data.getDataPromise('tradeGroupTypeByName.json', this, {params: {name: tradeGroupTypeName}})
				.then(function(tradeGroupType) {
					Ext.Msg.confirm(confirmationTitle, confirmationText, async function(a) {
						if (TCG.isEquals(a, 'yes')) {

							//Get the list of valid executing brokers for the trade destination and trade type of Options
							const tradeDestinationListUrl = 'tradeExecutingBrokerCompanyListFind.json?tradeDestinationId=' + tradeDestinationId +
								'&tradeTypeId=' + TCG.data.getData('tradeTypeByName.json?name=Options', gridPanel, 'trade.type.Options').id;
							const validBrokers = TCG.data.getData(tradeDestinationListUrl, gridPanel);

							//If there is only one executing broker, set a broker "override" so that this broker will be used, regardless of what was set
							//in the dropdown. This is a semi-temporary solution that helps improve usability (e.g. with REDI, Goldman Sachs is the only executing broker)
							let brokerOverride = null;
							if (validBrokers && TCG.isEqualsStrict(validBrokers.length, 1)) {
								brokerOverride = validBrokers[0];
							}

							const positions = [];
							const selections = sm.getSelections();

							for (let i = 0; i < selections.length; i++) {
								const position = selections[i].json;
								const rowInfo = selections[i].data;


								const optionInfo = {
									'clientInvestmentAccount.id': position.positionBalance.clientInvestmentAccount.id,
									'holdingInvestmentAccount.id': position.positionBalance.holdingInvestmentAccount.id,
									'investmentSecurity.id': position.positionBalance.investmentSecurity.id,
									'executingBroker.id': brokerOverride ? brokerOverride.id : rowInfo['executingBroker.id'],
									'tradeQuantity': rowInfo[tradeQuantityField],
									'btic': TCG.isTrue(btic)
								};

								// Set the price of of the submitted record if it is defined and not Trade To Close
								// Price quotes will be obtained for Trade To Close groups.
								if (TCG.isNotBlank(rowInfo.averageUnitPrice) && TCG.isNotEqualsStrict(confirmationTitle, 'Trade To Close')) {
									optionInfo.averageUnitPrice = rowInfo.averageUnitPrice;
								}

								if (TCG.isNotBlank(rowInfo.commissionPerUnit)) {
									optionInfo.commissionPerUnit = rowInfo.commissionPerUnit;
								}

								optionInfo['class'] = 'com.clifton.trade.options.TradeOptionsRequestDetail';

								positions.push(optionInfo);

							}

							const detailsStr = Ext.util.JSON.encode(positions);

							const loader = new TCG.data.JsonLoader({
								waitTarget: grid.ownerCt,
								waitMsg: 'Processing...',

								params: {
									'traderUser.id': TCG.getCurrentUser().id,
									'tradeDestination.id': tradeDestinationId,
									'tradeGroupType.id': tradeGroupType.id,
									// response will include tradeGroupList and optionRequest, exclude the following from the response
									requestedPropertiesToExcludeGlobally: 'tradeList|details',
									details: detailsStr
								},
								onLoad: function(record, conf) {
									gridPanel.reload();

								}
							});

							if (TCG.isNotBlank(gridPanel.tradeSourceName)) {
								const tradeSource = await TCG.data.getDataPromiseUsingCaching('tradeSourceByName.json?name=' + gridPanel.tradeSourceName, gridPanel, 'tradeSource.' + gridPanel.tradeSourceName);
								if (TCG.isNotNull(tradeSource)) {
									loader.params['tradeSource.id'] = tradeSource.id;
								}
							}
							loader.load('tradeGroupListForTradeOptionsRequestGenerate.json');
						}
					});
				});
		}
	},

	addExpandCollapseToToolbar: function(toolbar, gridPanel) {
		toolbar.add({
			iconCls: 'expand-all',
			tooltip: 'Expand or Collapse all groups',
			scope: this,
			handler: function() {
				gridPanel.expandCollapseGroups.call(gridPanel);
			}
		});
		toolbar.add('-');
	},

	addApplyPricesToToolbar: function(toolbar, gridPanel, applyCommissions) {
		toolbar.add({
			text: 'Apply Prices...',
			tooltip: 'Opens a modal window to look up or specify prices for the underlying securities for option position rows in this grid.',
			iconCls: 'bloomberg',
			scope: gridPanel,
			handler: function() {
				const params = {underlyingSecurities: [], applyCommissions: applyCommissions};
				const selectionModel = gridPanel.grid.getSelectionModel();
				const selections = selectionModel.getSelections();
				gridPanel.populateOptionPriceWindowParameters.call(gridPanel, params, (selections.length > 0) ? selections : gridPanel.grid.getStore().data.items);
				if (params.underlyingSecurities.length < 1) {
					TCG.showInfo('The blotter has no options listed to apply prices. Please reload the grid to apply prices.', 'No Security Instruments');
				}
				else {
					TCG.createComponent('Clifton.trade.option.OptionsUnderlyingPriceWindow', {
						defaultData: params,
						openerCt: gridPanel
					});
				}
			}
		});
		toolbar.add('-');
	},

	populateOptionPriceWindowParameters: function(params, records) {
		for (let i = 0; i < records.length; i++) {
			const record = records[i];
			const newInstrument = {
				symbol: record.get('positionBalance.investmentSecurity.underlyingSecurity.symbol'),
				id: TCG.getValue('positionBalance.investmentSecurity.underlyingSecurity.id', record.json),
				price: undefined
			};
			let found = false;
			for (let j = 0; j < params.underlyingSecurities.length; j++) {
				const instrument = params.underlyingSecurities[j];
				if (TCG.isEqualsStrict(instrument.symbol, newInstrument.symbol)) {
					found = true;
					break;
				}
			}
			if (!found) {
				params.underlyingSecurities.push(newInstrument);
			}
		}
	},

	/*
	 * Used by the OptionsUnderlyingPriceWindow to apply prices for the underlying securities
	 * of the positions in the grid. The prices come back as parameters such as:
	 * 1.symbol: 'SPX', 1.price: 2145, 2.symbol: 'EFA', 2.price: 58.5 ...
	 *
	 * The number of instruments is dynamic based on the number of underlying instruments
	 * derived from the options expiration grid rows (security).
	 */
	applyPrices: function(instrumentPrices) {
		const fp = this;
		for (let i = 0; i < Object.keys(instrumentPrices).length; i++) {
			const symbol = instrumentPrices[i + '.symbol'];
			const price = instrumentPrices[i + '.price'];
			if (TCG.isBlank(symbol)) {
				break;
			}
			else if (!TCG.isBlank(price)) {
				fp.applyPriceToRowsWithUnderlyingSecurity.call(fp, TCG.parseFloat(price), symbol);
			}
		}
		if (TCG.isEquals(instrumentPrices['applyCommissions'], 'on')) {
			fp.calculateCommissionForRows.call(fp);
		}
	},

	applyPriceToRowsWithUnderlyingSecurity: function(price, underlyingSymbol) {
		if (!TCG.isBlank(price) && !TCG.isBlank(underlyingSymbol)) {
			const gridPanel = this;
			const grid = gridPanel.grid;
			grid.getStore().each(function(record) {
				// set price if securities match
				if (TCG.isEqualsStrict(record.get('positionBalance.investmentSecurity.underlyingSecurity.symbol'), underlyingSymbol)) {
					let recordPrice;
					const strikePrice = record.get('optionStrikePrice');
					const subTypeName = TCG.getValue('positionBalance.investmentSecurity.instrument.hierarchy.investmentTypeSubType.name', record.json);
					if (TCG.isNotEqualsStrict(subTypeName, 'Options On Indices')) {
						// All options that are not on indices expire with a price of 0.
						recordPrice = 0;
					}
					else if (gridPanel.isOptionTypePut(record.get('optionType'))) {
						// PUT: 0 if strike < underlying or (strike - underlying) if strike >= underlying
						recordPrice = strikePrice < price ? 0 : strikePrice - price;
					}
					else {
						// Call: 0 if strike > underlying or (underlying - strike) if strike <= underlying
						recordPrice = strikePrice > price ? 0 : price - strikePrice;
					}

					if (recordPrice) {
						if (TCG.isNotEqualsStrict(recordPrice, 0)) {
							const stringPrice = price.toString();
							const decimalIndex = stringPrice.indexOf('.');
							if (decimalIndex > -1) {
								// Sometimes prices come back with more than two decimal digits. Found to two decimal digits for price
								recordPrice = recordPrice.toFixed(2);
							}
						}
						record.set('averageUnitPrice', recordPrice);
					}
					gridPanel.populateRecordInTheMoneyForPrice.call(this, record, price);
				}
			}, gridPanel);
			grid.getView().refresh();
		}
	},

	populateRecordInTheMoneyForPrice: function(record, price) {
		let percentItm;
		const strikePrice = record.get('optionStrikePrice');
		/*
		 * ITM calculations are opposite of expected. ITM results will be negative and OTM
		 * will be positive because the traders want to see ITM results as a liability.
		 */
		if (this.isOptionTypePut(record.get('optionType'))) {
			percentItm = (price - strikePrice) / price * 100;
		}
		else {
			percentItm = (strikePrice - price) / price * 100;
		}
		if (percentItm) {
			record.set('percentInTheMoney', percentItm);
		}
	},

	isOptionTypePut: function(optionType) {
		return TCG.isNotBlank(optionType) && TCG.isEqualsStrict(optionType.toUpperCase(), 'PUT');
	},

	calculateCommissionForRows: function() {
		const grid = this.grid;
		grid.getStore().each(function(record) {
			let commission = undefined;
			const underlyingSymbol = record.get('positionBalance.investmentSecurity.underlyingSecurity.symbol');
			if (record.get('positionBalance.investmentSecurity.instrument.hierarchy.otc')) {
				commission = 0;
			}
			else if (TCG.isEqualsStrict(underlyingSymbol, 'SPX')) {
				commission = record.get('averageUnitPrice') > 0 ? 1 : 0;
			}
			else {
				// let default to commission schedule
			}
			if (commission) {
				record.set('commissionPerUnit', commission);
			}
		});
	}
});

Clifton.trade.option.OptionsBlotterGrid = Ext.extend(Clifton.trade.option.BaseOptionPositionsGrid, {
	instructions: 'This section lists positions for both cash-settled and physically-settled options that are expiring on the specified date (the expire date column defaults to today).',
	columns: [
		{header: 'Client Account', width: 120, dataIndex: 'positionBalance.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 50, dataIndex: 'positionBalance.holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
		{
			header: 'Trade Quantity', dataIndex: 'tradeQuantityForExpire', width: 40, type: 'float', filter: false, sortable: false, summaryType: 'sum', editor: {xtype: 'floatfield', allowNegative: false, allowBlank: true},
			tooltip: 'The quantity to trade. Must be positive. Buy/Sell is automatically determined.'
		},
		{header: 'Security', width: 50, dataIndex: 'positionBalance.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
		{header: 'Local Amount', dataIndex: 'positionBalance.localAmount', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: 'Base Amount', dataIndex: 'positionBalance.baseAmount', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, summaryType: 'sum', hidden: true},
		{header: 'Local Cost Basis', dataIndex: 'positionBalance.localCostBasis', type: 'currency', width: 60, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: 'Underlying', width: 45, dataIndex: 'positionBalance.investmentSecurity.underlyingSecurity.symbol', filter: false, sortable: false, hidden: true},
		{
			header: 'Position Quantity', dataIndex: 'positionQuantity', width: 40, type: 'float', negativeInRed: true, positiveInGreen: true, filter: false, sortable: false,
			tooltip: 'The number of option contracts left that have not yet been processed for expiration of the option position. A value of 0 indicates that associated trade(s) with the appropriate quantity to close the option position are in Pending state. ' +
				'The sign of the quantity indicates whether the position is long or short.'
		},
		{
			header: '% ITM', width: 30, dataIndex: 'percentInTheMoney', type: 'currency', useNull: true, negativeInRed: true, summaryType: 'min',
			tooltip: 'Percent this row\'s security strike price is \'In The Money\'. ITM is red to indicate it is not expiring worthless; OTM is green.',
			renderer: function(v, metaData, r) {
				return TCG.renderAmount(v, true, '0,000.00%');
			}
		},
		{
			header: 'Executing Broker', dataIndex: 'executingBroker.label', idDataIndex: 'executingBroker.id', width: 100, editor: {xtype: 'combo', allowBlank: false, displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json?tradeTypeId=' + TCG.data.getData('tradeTypeByName.json?name=Options', null, 'trade.type.Options').id}
		},
		{
			header: 'Destination', dataIndex: 'tradeDestination.name', width: 50, skipExport: true, editor: {xtype: 'combo', loadAll: true, displayField: 'label', url: 'tradeDestinationListFind.json?tradeTypeId=' + TCG.data.getData('tradeTypeByName.json?name=Options', null, 'trade.type.Options').id},
			renderer: function(value, metaData, r) {
				const destinationName = r.data['tradeDestination.name'];
				if (TCG.isBlank(destinationName)) {
					return 'Manual';
				}

				return destinationName;
			}
		},
		{header: 'Avg Price', width: 45, dataIndex: 'averageUnitPrice', type: 'float', skipExport: true, summaryType: 'max', editor: {xtype: 'floatfield', value: 0, allowBlank: true}},
		{header: 'Commission Per Unit', width: 45, dataIndex: 'commissionPerUnit', type: 'float', skipExport: true, useNull: true, summaryType: 'max', editor: {xtype: 'floatfield', allowBlank: true}},
		{header: 'Expire Date', width: 45, dataIndex: 'positionBalance.investmentSecurity.lastDeliveryDate', filter: {searchFieldName: 'lastDeliveryDate'}},
		{header: 'Option Type', width: 45, dataIndex: 'optionType', hidden: true},
		{header: 'Strike Price', width: 45, dataIndex: 'optionStrikePrice', type: 'float', hidden: true}
	],

	editor: {
		addEditButtons: function(t, gridPanel) {
			gridPanel.addExpandCollapseToToolbar(t, gridPanel);
			t.add({
				text: 'Actions',
				iconCls: 'run',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Process Option Expiration',
							tooltip: 'Generates offsetting trade(s) to close the option position(s). Trade(s) will be grouped by the trade group type <b>Option Expiration</b>.',
							iconCls: 'run',
							scope: this.getGridPanel(),
							handler: function() {
								gridPanel.processRequest(
									'Option Expiration',
									gridPanel,
									'Process Option Expiration',
									'Would you like to generate trades to process the option expiration for the selected option position(s)?',
									TCG.data.getData('tradeDestinationByName.json?name=Manual', gridPanel, 'trade.tradeDestination.Manual').id,
									'tradeQuantityForExpire'
								);
							}
						},
						{
							text: 'Trade To Close',
							tooltip: 'Generates offsetting trade(s) to close the option position(s). Trade(s) will be grouped by the trade group type <b>Option Close</b>.',
							iconCls: 'run',
							scope: this.getGridPanel(),
							handler: function() {
								gridPanel.processRequest(
									'Option Trade To Close',
									gridPanel,
									'Trade To Close',
									'Would you like to generate trades to process the trade to close for the selected option position(s)?',
									TCG.data.getData('tradeDestinationByName.json?name=Manual', gridPanel, 'trade.tradeDestination.Manual').id,
									'tradeQuantityForExpire'
								);
							}
						}
					]
				})
			});
			t.add('-');
			gridPanel.addApplyPricesToToolbar(t, gridPanel, false);
		}
	},

	listeners: {
		afterrender: function(gridPanel) {
			this.instructions += ' ' + this.highlightInstructions;

			gridPanel.createCopyDrillDownMenuForColumn('averageUnitPrice');
			gridPanel.createCopyDrillDownMenuForColumn('commissionPerUnit');

			gridPanel.addGridGroupSelectionContextMenu(gridPanel);

			gridPanel.grid.on('afteredit', function(event) {
				const record = event.record;
				const field = event.field;
				const value = event.value;
				if (TCG.isEqualsStrict(field, 'averageUnitPrice')) {
					this.populateRecordInTheMoneyForPriceDelta.call(this, record, value);
				}
			}, gridPanel);
		}
	},

	createCopyDrillDownMenuForColumn: function(columnName, copyFunction) {
		const gridPanel = this;
		const columnModel = gridPanel.grid.getColumnModel();
		const column = columnModel.getColumnAt(columnModel.findColumnIndex(columnName));

		// set context menu on column once rather than on every right-click
		if (!column.drillDown) {
			column.drillDown = new Ext.menu.Menu({
				items: [
					{
						text: 'Copy to all rows with same underlying security.',
						iconCls: 'copy',
						handler: function() {
							const gp = this;
							// get current row selection for copying
							const currentRecord = gp.grid.getStore().getAt(gp.grid.contextRowIndex);
							const underlyingSymbolFieldName = 'positionBalance.investmentSecurity.underlyingSecurity.symbol';
							gp.copyValueToRowsWithSecuritySymbol.call(gp, columnName, currentRecord.get(columnName), underlyingSymbolFieldName, currentRecord.get(underlyingSymbolFieldName));
						},
						scope: gridPanel
					},
					{
						text: 'Copy to all rows with same security.',
						iconCls: 'copy',
						handler: function() {
							const gp = this;
							// get current row selection for copying
							const currentRecord = gp.grid.getStore().getAt(gp.grid.contextRowIndex);
							const securitySymbolFieldName = 'positionBalance.investmentSecurity.symbol';
							gp.copyValueToRowsWithSecuritySymbol.call(gp, columnName, currentRecord.get(columnName), securitySymbolFieldName, currentRecord.get(securitySymbolFieldName));
						},
						scope: gridPanel
					}
				]
			});
		}

		// add menu to column
		column.on('contextmenu', function(column, grid, row, event) {
			event.preventDefault();
			// set grid row placeholder for the context menu to use
			grid.contextRowIndex = row;
			column.drillDown.showAt(event.getXY());
		}, column);

		// add menu to column editor
		column.editor.on('afterrender', function(editor) {
			const column = this;
			const el = column.editor.getEl();
			el.dom.setAttribute('ext:qtip', 'Right click to display the context menu with additional features.');
			el.on('contextmenu', function(event, target) {
				event.preventDefault();
				// set grid row placeholder for the context menu to use
				this.grid.contextRowIndex = column.editor.gridEditor.row;
				column.drillDown.showAt(event.getXY());
			}, gridPanel);
		}, column);
	},

	copyValueToRowsWithSecuritySymbol: function(columnName, columnValue, securityField, securitySymbol) {
		if (!TCG.isBlank(columnName) && !TCG.isBlank(columnValue) && !TCG.isBlank(securityField) && !TCG.isBlank(securitySymbol)) {
			const grid = this.grid;
			grid.getStore().each(function(record) {
				if (TCG.isEqualsStrict(record.get(securityField), securitySymbol)) {
					if (TCG.isNotEqualsStrict(record.get(columnName), columnValue)) {
						record.set(columnName, columnValue);

						if (TCG.isEqualsStrict(columnName, 'averageUnitPrice')) {
							this.populateRecordInTheMoneyForPriceDelta.call(this, record, columnValue);
						}
					}
				}
			}, this);
		}
	},

	populateRecordInTheMoneyForPriceDelta: function(record, priceDelta) {
		const strikePrice = record.get('optionStrikePrice');
		const price = this.isOptionTypePut(record.get('optionType')) ? strikePrice - priceDelta : strikePrice + priceDelta;
		if (price) {
			this.populateRecordInTheMoneyForPrice.call(this, record, price);
		}
	}
});
Ext.reg('optionsExpirationGrid', Clifton.trade.option.OptionsBlotterGrid);

Clifton.trade.option.OptionsDeliveryGrid = Ext.extend(Clifton.trade.option.BaseOptionPositionsGrid, {
	instructions: 'This section lists positions for physically-settled options that are expiring on the specified date (the expire date column defaults to today).',
	columns: [
		{header: 'Client Account', dataIndex: 'positionBalance.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Holding Account', width: 45, dataIndex: 'positionBalance.holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
		{
			header: 'Trade Quantity', dataIndex: 'tradeQuantityForDeliveryOrOffset', width: 45, type: 'float', filter: false, sortable: false, summaryType: 'sum', editor: {xtype: 'floatfield', allowNegative: false, allowBlank: true},
			tooltip: 'The quantity to trade. Must be positive. Buy/Sell is automatically determined.'
		},
		{header: 'Security', width: 50, dataIndex: 'positionBalance.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
		{header: 'Local Amount', dataIndex: 'positionBalance.localAmount', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: 'Base Amount', dataIndex: 'positionBalance.baseAmount', type: 'currency', width: 55, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, summaryType: 'sum', hidden: true},
		{header: 'Local Cost Basis', dataIndex: 'positionBalance.localCostBasis', type: 'currency', width: 60, filter: false, sortable: false, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: 'Underlying', width: 45, dataIndex: 'positionBalance.investmentSecurity.underlyingSecurity.symbol', filter: false, sortable: false, hidden: true},
		{
			header: 'Remaining Delivery', dataIndex: 'numRemainingContractsToDeliver', width: 50, type: 'float', filter: false, sortable: false,
			tooltip: 'The number of option contracts left that have not yet been processed for physical delivery of the option position. A value of 0 indicates that associated trade(s) with the appropriate quantity to delivery the underlying are in Pending state.'
		},
		{
			header: 'Remaining Offset', dataIndex: 'numRemainingContractsToOffsetDelivery', width: 45, type: 'float', filter: false, sortable: false,
			tooltip: 'The number of option contracts left that have not yet been processed for the offsetting of the delivery of the option position. A value of 0 indicates that associated trade(s) with the appropriate quantity to offset the delivery of the option are in Pending state.'
		},
		/*
		 * For the 'Delivery' tab, Executing Broker is filtered down to those which trade futures. This is because for not the only deliverable Option contracts that we trade are options on futures. Note that
		 * valid Executing Brokers are usually also determined by the Trade Destination, but in this case the destination could be REDI or Manual (we don't know until the user selects an action).
		 *
		 * TODO Consider changing this so that the available Executing Brokers in each row is dependent on the option contract's underlying security in that row.
		 */
		{
			header: 'Executing Broker', dataIndex: 'executingBroker.label', idDataIndex: 'executingBroker.id', width: 100, editor: {xtype: 'combo', allowBlank: false, displayField: 'label', url: 'tradeExecutingBrokerCompanyListFind.json?tradeTypeId=' + TCG.data.getData('tradeTypeByName.json?name=Futures', null, 'trade.type.Futures').id}
		},
		{
			header: '% ITM', width: 30, dataIndex: 'percentInTheMoney', type: 'currency', useNull: true, negativeInRed: true, summaryType: 'min',
			tooltip: 'Percent this row\'s security strike price is \'In The Money\'. ITM is red to indicate it is not expiring worthless; OTM is green.',
			renderer: function(v, metaData, r) {
				return TCG.renderAmount(v, true, '0,000.00%');
			}
		},
		{header: 'Expire Date', width: 50, dataIndex: 'positionBalance.investmentSecurity.lastDeliveryDate', filter: {searchFieldName: 'lastDeliveryDate'}},
		{header: 'Option Type', width: 45, dataIndex: 'optionType', hidden: true},
		{header: 'Strike Price', width: 45, dataIndex: 'optionStrikePrice', type: 'float', hidden: true}
	],

	getLoadParams: function(firstLoad) {
		return TCG.callSuper(this, 'getLoadParams', [firstLoad])
			.then(function(params) {
				params['deliverable'] = true;
				return params;
			});
	},

	editor: {
		addEditButtons: function(t, gridPanel) {
			gridPanel.addExpandCollapseToToolbar(t, gridPanel);
			t.add({
				text: 'Actions',
				iconCls: 'run',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Process Option Physical Delivery',
							tooltip: 'Generates trade(s) to process the delivery of the underlying security. Trade(s) will be grouped by the trade group type <b>Option Physical Delivery</b>. This option should only be used for options that are in-the-money.',
							iconCls: 'run',
							scope: this.getGridPanel(),
							handler: function() {
								gridPanel.processRequest(
									'Option Physical Delivery',
									gridPanel,
									'Process Underlying Delivery',
									'Would you like to generate trades to process the delivery of the underlying for the selected option position(s)?',
									TCG.data.getData('tradeDestinationByName.json?name=Manual', gridPanel, 'trade.tradeDestination.Manual').id,
									'tradeQuantityForDeliveryOrOffset'
								);
							}
						},
						{
							text: 'Process Option Delivery Offset (REDI)',
							tooltip: 'Generates <b>REDI</b> trade ticket(s) to offset trade(s) generated by <b>Process Option Physical Delivery</b>. Trade(s) will be grouped by the trade group type <b>Option Delivery Offset</b>. ' +
								'This should only be used for options that are in-the-money. <br/><br/>Note: If the underlying is a "big" security, then the associated "mini" security is traded instead with the appropriate adjusted quantity.',
							iconCls: 'run',
							scope: this.getGridPanel(),
							handler: function() {
								gridPanel.processRequest(
									'Option Delivery Offset',
									gridPanel,
									'Generate Offsetting Trade (REDI)',
									'Would you like to generate offsetting REDI trades for the delivery of the underlying of the selected option position(s)?',
									TCG.data.getData('tradeDestinationByName.json?name=REDI Ticket', gridPanel, 'trade.tradeDestination.REDI').id,
									'tradeQuantityForDeliveryOrOffset'
								);
							}
						},
						{
							text: 'Process Option Delivery Offset (BTIC)',
							tooltip: 'Generates <b>Manual</b> trade ticket(s) to offset trade(s) generated by <b>Process Option Physical Delivery</b>. Trade price(s) reflect the index closing price. Trade(s) will be grouped by the trade group type <b>Option Delivery Offset</b>. ' +
								'This should only be used for options that are in-the-money. <br/><br/>Note: If the underlying is a "big" security, then the associated "mini" security is traded instead with the appropriate adjusted quantity.',
							iconCls: 'run',
							scope: this.getGridPanel(),
							handler: function() {
								gridPanel.processRequest(
									'Option Delivery Offset',
									gridPanel,
									'Generate Offsetting Trade (BTIC)',
									'Would you like to generate offsetting Manual trades for the delivery of the underlying of the selected option position(s)?',
									TCG.data.getData('tradeDestinationByName.json?name=Manual', gridPanel, 'trade.tradeDestination.Manual').id,
									'tradeQuantityForDeliveryOrOffset',
									true // BTIC trade
								);
							}
						}]
				})
			});
			t.add('-');
			gridPanel.addApplyPricesToToolbar(t, gridPanel, false);
		}
	},


	listeners: {
		afterrender: function(gridPanel) {
			this.instructions += ' ' + this.highlightInstructions;

			gridPanel.addGridGroupSelectionContextMenu(gridPanel);
		}
	}
});
Ext.reg('optionsDeliveryGrid', Clifton.trade.option.OptionsDeliveryGrid);

Clifton.trade.option.OptionsUnderlyingPriceWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Option Underlying Prices',
	iconCls: 'add',
	height: 400,
	width: 300,
	modal: true,

	okButtonText: 'Apply',
	okButtonTooltip: 'Apply the prices to the rows of Option Expiration Blotter.',

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify prices of the underlying securities to calculate the average prices in the parent grid. Only prices specified will be applied.',
			labelWidth: 95,
			defaults: {
				anchor: '-20'
			},

			getDefaultData: function(win) {
				const fp = this;
				const instruments = win.defaultData.underlyingSecurities;
				if (!instruments) {
					TCG.showError('The blotter has no expiring options listed to apply prices. Please reload the grid to apply prices.', 'No Security Instruments');
				}
				fp.add({
					xtype: 'compositefield',
					items: [
						{
							fieldLabel: 'Cache Threshold', xtype: 'integerfield', name: 'cacheThreshold', submitValue: false, minValue: 0, value: 5, width: 25,
							qtip: 'The number of minutes to allow prices to be cached before a new price is retrieved. The default value is five minutes. To ensure a live price is looked up, enter a value of zero before looking up prices.'
						},
						{
							xtype: 'button', text: 'Lookup Prices', iconCls: 'bloomberg', flex: 1, tooltip: 'Click to load prices from Bloomberg for these Ticker Symbols',
							scope: fp,
							handler: function(btn) {
								fp.lookupAndApplyPrices.call(fp);
							}
						}
					]
				});
				fp.add({xtype: 'label', html: '<hr/>'});
				const priceLookupFunction = function(btn) {
					fp.lookupAndApplyPriceForUnderlyingIndex.call(fp, btn.underlyingSecurityIndex);
				};
				for (let i = 0; i < instruments.length; i++) {
					const instrument = instruments[i];
					fp.add({xtype: 'hidden', name: i + '.symbol'});
					fp.setFormValue(i + '.symbol', instrument['symbol']);
					fp.add({xtype: 'hidden', name: i + '.id'});
					fp.setFormValue(i + '.id', instrument['id']);
					fp.add({
						xtype: 'compositefield', id: instrument['id'],
						items: [
							{xtype: 'floatfield', fieldLabel: instrument['symbol'], name: i + '.price', flex: 1},
							{
								xtype: 'button', iconCls: 'bloomberg', width: 25, tooltip: 'Click to load price from Bloomberg for this Ticker Symbol',
								scope: fp, underlyingSecurityIndex: i, tabIndex: -1, // disable tab focus
								handler: priceLookupFunction
							}
						]
					});
				}
				fp.add({xtype: 'checkbox', boxLabel: 'Apply Commissions', name: 'applyCommissions', checked: win.defaultData.applyCommissions});
			},

			items: [],

			lookupAndApplyPriceForUnderlyingIndex: function(underlyingSecurityIndex) {
				const fp = this;
				const params = {
					securityId: fp.getFormValue(underlyingSecurityIndex + '.id'),
					maxAgeInSeconds: (fp.getCacheThreshold() * 60),
					requestedPropertiesToExcludeGlobally: 'marketDataLiveCommand'
				};
				const loader = new TCG.data.JsonLoader({
					params: params,
					waitTarget: fp,
					waitMsg: 'Loading price for ' + fp.getFormValue(underlyingSecurityIndex + '.symbol') + '...',
					onLoad: function(record, conf) {
						// set price for this security in the composite field with id
						fp.form.findField(record.securityId).items.get(0).setValue(record.value);
					}
				});
				loader.load('marketDataLivePrice.json');
			},

			lookupAndApplyPrices: function() {
				const fp = this;
				const formValues = fp.form.getValues();
				const params = {
					securityIds: [],
					maxAgeInSeconds: (fp.getCacheThreshold() * 60),
					requestedPropertiesToExcludeGlobally: 'marketDataLiveCommand'
				};
				for (let i = 0; i < Object.keys(formValues).length; i++) {
					const id = formValues[i + '.id'];
					if (TCG.isBlank(id)) {
						break;
					}
					params.securityIds.push(id);
				}
				const loader = new TCG.data.JsonLoader({
					params: params,
					waitTarget: fp,
					waitMsg: 'Loading prices...',
					onLoad: function(records, conf) {
						if (records.length > 0) {
							for (let i = 0; i < records.length; i++) {
								const record = records[i];
								// set price for this security in the composite field with id
								fp.form.findField(record.securityId).items.get(0).setValue(record.value);
							}
						}
					}
				});
				loader.load('marketDataLivePricesWithCommand.json');
			},

			getCacheThreshold: function() {
				let cacheThreshold = this.form.findField('cacheThreshold').getNumericValue();
				if (TCG.isBlank(cacheThreshold) || !TCG.isNumber(cacheThreshold)) {
					cacheThreshold = 5;
				}
				return cacheThreshold;
			}
		}
	],

	getForm: function() {
		return this.items.get(0).getForm();
	},

	saveWindow: function() {
		const win = this;
		const form = win.getForm();
		const openerWindow = this.openerCt;
		if (form.isDirty()) {
			// first call to strangle the Trades
			if (openerWindow && openerWindow.applyPrices) {
				openerWindow.applyPrices.call(openerWindow, form.getValues());
				this.close();
			}
			else {
				TCG.showError('Opener component must be defined and must have applyPrices(params) method.', 'Callback Missing');
			}
		}
		else {
			this.close();
		}
	}
});
