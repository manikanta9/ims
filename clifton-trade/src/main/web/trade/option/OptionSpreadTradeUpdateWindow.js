Clifton.trade.option.OptionSpreadTradeUpdateWindow = Ext.extend(Clifton.trade.group.BaseTradeGroupDynamicOptionStrangleApplyWindow, {
	title: 'Option Spread Trade Updates',
	height: 520,
	modal: false, // not modal so new Options can be created from template
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Specify values for updating the Put and Call Trades of this Option Group by changing the Trade Destination or Option securities. NOTE: Updating the securities requires the Trades to first be Rejected, which will require a new Approval of the Trades.',
			labelWidth: 150,

			defaults: {
				anchor: '-20'
			},

			getDefaultData: function(win) {
				return win.defaultData;
			},

			items: [
				{
					fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json',
					listeners: {
						beforeQuery: function(qEvent) {
							const f = qEvent.combo.getParentForm().getForm();
							qEvent.combo.store.baseParams = {
								activeOnDate: f.findField('tradeDate').value,
								tradeTypeNameEquals: 'Options'
							};
						}
					}
				},
				{xtype: 'sectionheaderfield', header: 'Securities'},
				{
					fieldLabel: 'Underlying Security', xtype: 'panel', layout: 'hbox', anchor: '0',
					items: [
						{name: 'optionUnderlyingSecurity.label', hiddenName: 'optionUnderlyingSecurity.id', xtype: 'linkfield', detailIdField: 'optionUnderlyingSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', flex: 1},
						{html: '&nbsp;'},
						{name: 'underlyingPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Underlying Security'}
					]
				},
				{fieldLabel: 'TradeDate', name: 'tradeDate', xtype: 'datefield', submitValue: false, disabled: true, hidden: true},
				{
					fieldLabel: 'Expiration (New)', name: 'lastDeliveryDate', xtype: 'datefield', submitValue: false, disabled: true,
					qtip: 'Expiration Date of the New Options that will be traded here.'
				},
				{
					fieldLabel: 'Pricing', xtype: 'panel', layout: 'hbox', anchor: 0,
					items: [
						{
							boxLabel: 'Use Live Prices', qtip: 'If checked, will use the live price for the Underlying Security for validation and calculating trades and exposures. Will use the previous day closing price if unchecked.',
							xtype: 'checkbox', name: 'useLivePrices', checked: true, flex: 1
						},
						{
							iconCls: 'bloomberg', text: 'Live Prices', xtype: 'button', width: 144, tooltip: 'Load the live data values for the specified Put and Call options. The form fields are dynamically loaded as data is retrieved, so there may be a slight delay before the values are visible.',
							listeners: {
								click: function(button, e) {
									const fp = TCG.getParentFormPanel(button);
									const form = fp.getForm();
									const isUseLivePricing = form.findField('useLivePrices').getValue();

									if (TCG.isTrue(isUseLivePricing)) {
										const optionFieldPrefixes = ['longPut', 'shortPut', 'shortCall', 'longCall'];
										const securityIds = [];
										const securityPriceUpdateFields = [];
										const securityDeltaUpdateFields = [];
										const optionFieldFunction = function(optionFieldPrefix) {
											if (TCG.isNotBlank(optionFieldPrefix)) {
												const fieldValue = form.findField(optionFieldPrefix + 'Security.id').getValue();
												if (TCG.isNotBlank(fieldValue)) {
													securityIds.push(fieldValue);
													securityPriceUpdateFields.push(optionFieldPrefix + 'Price');
													securityDeltaUpdateFields.push(optionFieldPrefix + 'Delta');
												}
											}
										};
										const underlyingSecurityId = form.findField('optionUnderlyingSecurity.id').getValue();
										if (TCG.isNotBlank(underlyingSecurityId)) {
											securityIds.push(underlyingSecurityId);
											securityPriceUpdateFields.push('underlyingPrice');
										}
										for (const fieldPrefix of optionFieldPrefixes) {
											optionFieldFunction(fieldPrefix);
										}
										if (securityIds.length > 0) {
											Clifton.marketdata.field.updateSecurityLiveMarketDataValues(fp, securityIds, securityPriceUpdateFields, void 0, {maxAgeInSeconds: 60, providerOverride: 'BPIPE'});
											// retrieve delta values excluding underlying
											const deltaSecurityIds = TCG.isNotBlank(underlyingSecurityId) ? securityIds.slice(1) : securityIds;
											if (deltaSecurityIds.length > 0) {
												Clifton.marketdata.field.updateSecurityLiveMarketDataValues(fp, deltaSecurityIds, securityDeltaUpdateFields, 'Delta', {maxAgeInSeconds: 60, providerOverride: 'BPIPE'});
											}
										}
									}
								}
							}
						}
					]
				},
				{
					fieldLabel: 'Long Put', xtype: 'panel', layout: 'hbox', anchor: '0',
					items: [
						{
							name: 'longPutSecurity.label', hiddenName: 'longPutSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							allowBlank: true, flex: 1, qtip: 'Long Put Security that will be traded based on quantities entered below',
							requestedProps: 'instrument',
							investmentSecurity: null,
							listeners: {
								afterrender: function(combo) {
									const fp = TCG.getParentFormPanel(combo);
									const dd = fp.getDefaultData(fp.ownerCt);
									if (TCG.isBlank(dd.prevLongPutSecurity)) {
										combo.disable();
									}
								},
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const store = qEvent.combo.store;
									store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
									store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
									store.setBaseParam('lastDeliveryDate', f.findField('lastDeliveryDate').value);
									store.setBaseParam('optionType', 'PUT');
								},
								change: function(combo, newValue, oldValue) {
									if (TCG.isBlank(newValue)) {
										const form = combo.getParentForm().getForm();
										form.findField('longPutDelta').setValue('');
										form.findField('longPutPrice').setValue('');
									}
								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									if (!this.getParentForm().getForm().findField('longPutSecurity.id').value) {
										TCG.showError('You must first select an option to copy', 'New Option');
										return false;
									}
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},
							getDefaultData: function(f) { // defaults client/counterparty/isda
								let fp = TCG.getParentFormPanel(this);
								fp = fp.getForm();
								const secId = fp.findField('longPutSecurity.id').value;
								return Promise.resolve(secId)
									.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
									.then(securityToCopy => ({
										instrument: securityToCopy.instrument,
										securityToCopy: securityToCopy,
										optionType: 'PUT'
									}));
							}
						},
						{html: '&nbsp;'},
						{name: 'longPutDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Long Put Option.'},
						{html: '&nbsp;'},
						{name: 'longPutPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Long Put Option.'}
					]
				},
				{
					fieldLabel: 'Short Put', xtype: 'panel', layout: 'hbox', anchor: '0',
					items: [
						{
							name: 'shortPutSecurity.label', hiddenName: 'shortPutSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							allowBlank: true, flex: 1, qtip: 'Short Put Security that will be traded based on quantities entered below',
							requestedProps: 'instrument',
							investmentSecurity: null,
							listeners: {
								afterrender: function(combo) {
									const fp = TCG.getParentFormPanel(combo);
									const dd = fp.getDefaultData(fp.ownerCt);
									if (TCG.isBlank(dd.prevShortPutSecurity)) {
										combo.disable();
									}
								},
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const store = qEvent.combo.store;
									store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
									store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
									store.setBaseParam('lastDeliveryDate', f.findField('lastDeliveryDate').value);
									store.setBaseParam('optionType', 'PUT');
								},
								change: function(combo, newValue, oldValue) {
									if (TCG.isBlank(newValue)) {
										const form = combo.getParentForm().getForm();
										form.findField('shortPutDelta').setValue('');
										form.findField('shortPutPrice').setValue('');
									}
								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									if (!this.getParentForm().getForm().findField('shortPutSecurity.id').value) {
										TCG.showError('You must first select an option to copy', 'New Option');
										return false;
									}
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},
							getDefaultData: function(f) { // defaults client/counterparty/isda
								let fp = TCG.getParentFormPanel(this);
								fp = fp.getForm();
								const secId = fp.findField('shortPutSecurity.id').value;
								return Promise.resolve(secId)
									.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
									.then(securityToCopy => ({
										instrument: securityToCopy.instrument,
										securityToCopy: securityToCopy,
										optionType: 'PUT'
									}));
							}
						},
						{html: '&nbsp;'},
						{name: 'shortPutDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Short Put Option.'},
						{html: '&nbsp;'},
						{name: 'shortPutPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Short Put Option.'}
					]
				},
				{
					fieldLabel: 'Short Call', xtype: 'panel', layout: 'hbox', anchor: '0',
					items: [
						{
							name: 'shortCallSecurity.label', hiddenName: 'shortCallSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							allowBlank: true, flex: 1, qtip: 'Short Call Security that will be traded based on quantities entered below',
							requestedProps: 'instrument',
							investmentSecurity: null,
							listeners: {
								afterrender: function(combo) {
									const fp = TCG.getParentFormPanel(combo);
									const dd = fp.getDefaultData(fp.ownerCt);
									if (TCG.isBlank(dd.prevShortCallSecurity)) {
										combo.disable();
									}
								},
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const store = qEvent.combo.store;
									store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
									store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
									store.setBaseParam('lastDeliveryDate', f.findField('lastDeliveryDate').value);
									store.setBaseParam('optionType', 'CALL');
								},
								change: function(combo, newValue, oldValue) {
									if (TCG.isBlank(newValue)) {
										const form = combo.getParentForm().getForm();
										form.findField('shortCallDelta').setValue('');
										form.findField('shortCallPrice').setValue('');
									}
								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									if (!this.getParentForm().getForm().findField('shortCallSecurity.id').value) {
										TCG.showError('You must first select an option to copy.', 'New Option');
										return false;
									}
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},
							getDefaultData: function(f) { // defaults client/counterparty/isda
								let fp = TCG.getParentFormPanel(this);
								fp = fp.getForm();
								const secId = fp.findField('shortCallSecurity.id').value;
								return Promise.resolve(secId)
									.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
									.then(securityToCopy => ({
										instrument: securityToCopy.instrument,
										securityToCopy: securityToCopy,
										optionType: 'CALL'
									}));
							}
						},
						{html: '&nbsp;'},
						{name: 'shortCallDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Short Call Option.'},
						{html: '&nbsp;'},
						{name: 'shortCallPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Short Call Option.'}
					]
				},
				{
					fieldLabel: 'Long Call', xtype: 'panel', layout: 'hbox', anchor: '0',
					items: [
						{
							name: 'longCallSecurity.label', hiddenName: 'longCallSecurity.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', queryParam: 'searchPatternUsingBeginsWith', detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
							allowBlank: true, flex: 1, qtip: 'Long Call Security that will be traded based on quantities entered below',
							requestedProps: 'instrument',
							investmentSecurity: null,
							listeners: {
								afterrender: function(combo) {
									const fp = TCG.getParentFormPanel(combo);
									const dd = fp.getDefaultData(fp.ownerCt);
									if (TCG.isBlank(dd.prevLongCallSecurity)) {
										combo.disable();
									}
								},
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const store = qEvent.combo.store;
									store.setBaseParam('activeOnDate', f.findField('tradeDate').value);
									store.setBaseParam('underlyingSecurityId', f.findField('optionUnderlyingSecurity.id').getValue());
									store.setBaseParam('lastDeliveryDate', f.findField('lastDeliveryDate').value);
									store.setBaseParam('optionType', 'CALL');
								},
								change: function(combo, newValue, oldValue) {
									if (TCG.isBlank(newValue)) {
										const form = combo.getParentForm().getForm();
										form.findField('longCallDelta').setValue('');
										form.findField('longCallPrice').setValue('');
									}
								}
							},
							getDetailPageClass: function(newInstance) {
								if (newInstance) {
									if (!this.getParentForm().getForm().findField('longCallSecurity.id').value) {
										TCG.showError('You must first select an option to copy.', 'New Option');
										return false;
									}
									return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
								}
								return this.detailPageClass;
							},
							getDefaultData: function(f) { // defaults client/counterparty/isda
								let fp = TCG.getParentFormPanel(this);
								fp = fp.getForm();
								const secId = fp.findField('longCallSecurity.id').value;
								return Promise.resolve(secId)
									.then(id => TCG.data.getDataPromise('investmentSecurity.json', this, {params: {id: id}}))
									.then(securityToCopy => ({
										instrument: securityToCopy.instrument,
										securityToCopy: securityToCopy,
										optionType: 'CALL'
									}));
							}
						},
						{html: '&nbsp;'},
						{name: 'longCallDelta', xtype: 'floatfield', readOnly: true, width: 70, submitValue: false, qtip: 'The live Bloomberg delta for the selected Long Call Option.'},
						{html: '&nbsp;'},
						{name: 'longCallPrice', xtype: 'currencyfield', readOnly: true, width: 60, submitValue: false, qtip: 'The live Bloomberg price for the selected Long Call Option.'}
					]
				},
				{
					name: 'rejectTradesForEdit', xtype: 'checkbox', checked: false, boxLabel: 'Reject Trades Before Edits',
					qtip: 'Perform the REJECT action on the Trades to enable edits before trying to set the new values.',
					listeners: {
						check: function(checkbox, checked) {
							const fp = TCG.getParentFormPanel(checkbox);
							fp.getForm().findField('validateTradesAfterEdit').setValue(checked);
						}
					}
				},
				{
					name: 'validateTradesAfterEdit', xtype: 'checkbox', checked: false, boxLabel: 'Validate Trades After Edits',
					qtip: 'Perform the VALIDATE action on the Trades after the new values have been saved.'
				},
				{xtype: 'sectionheaderfield', header: 'Previous Trade Securities'},
				{fieldLabel: 'Long Put Option', name: 'prevLongPutSecurity.label', hiddenName: 'prevLongPutSecurity.id', xtype: 'linkfield', detailIdField: 'prevLongPutSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false},
				{fieldLabel: 'Short Put Option', name: 'prevShortPutSecurity.label', hiddenName: 'prevShortPutSecurity.id', xtype: 'linkfield', detailIdField: 'prevShortPutSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false},
				{fieldLabel: 'Short Call Option', name: 'prevShortCallSecurity.label', hiddenName: 'prevShortCallSecurity.id', xtype: 'linkfield', detailIdField: 'prevShortCallSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false},
				{fieldLabel: 'Long Call Option', name: 'prevLongCallSecurity.label', hiddenName: 'prevLongCallSecurity.id', xtype: 'linkfield', detailIdField: 'prevLongCallSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false}
			]
		}
	],

	/**
	 * Overridden method to send parameters to applyTradeUpdates() function in the openerWindow.
	 */
	saveWindow: function() {
		const openerWindow = this.defaultData.openerWindow;
		const form = this.getForm();
		const rejectTrades = form.findField('rejectTradesForEdit').getValue();
		const params = {
			'tradeDestination.id': form.findField('tradeDestination.id').getValue()
		};

		const longPutSecurityId = form.findField('longPutSecurity.id').getValue();
		const shortPutSecurityId = form.findField('shortPutSecurity.id').getValue();
		const shortCallSecurityId = form.findField('shortCallSecurity.id').getValue();
		const longCallSecurityId = form.findField('longCallSecurity.id').getValue();

		if (TCG.isNotBlank(longPutSecurityId)) {
			params['longPutSecurity.id'] = longPutSecurityId;
		}
		if (TCG.isNotBlank(shortPutSecurityId)) {
			params['shortPutSecurity.id'] = shortPutSecurityId;
		}
		if (TCG.isNotBlank(shortCallSecurityId)) {
			params['shortCallSecurity.id'] = shortCallSecurityId;
		}
		if (TCG.isNotBlank(longCallSecurityId)) {
			params['longCallSecurity.id'] = longCallSecurityId;
		}

		if (rejectTrades === true) {
			// need to reject, save, and validate the trades.
			const validateTradesCallback = form.findField('validateTradesAfterEdit').getValue() === false ? undefined : function() {
				params['tradeGroupAction'] = 'VALIDATE';
				openerWindow.applyTradeUpdates.call(openerWindow, params);
			};
			const saveTradesCallback = function() {
				params['tradeGroupAction'] = 'SAVE_TRADE';
				openerWindow.applyTradeUpdates.call(openerWindow, params, validateTradesCallback);
			};
			params['tradeGroupAction'] = 'REJECT';
			openerWindow.applyTradeUpdates.call(openerWindow, params, saveTradesCallback);
		}
		else {
			// Save the changes.
			params['tradeGroupAction'] = 'SAVE_TRADE';
			openerWindow.applyTradeUpdates.call(openerWindow, params);
		}

		this.close();
	}
});
