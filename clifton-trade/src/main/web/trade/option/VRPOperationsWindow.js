TCG.use('Clifton.trade.monitor.PositionMonitorGridPanel');
TCG.use('Clifton.trade.monitor.UtilizationGridPanel');
TCG.use('Clifton.trade.option.OptionSpreadTradingWindow');

Clifton.trade.option.VRPOperationsWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'vrpOperationsWindow',
	title: 'Volatility Risk Premium Operations',
	iconCls: 'options',
	width: 1250,
	height: 750,
	maximized: true,

	// Additional Tabs that are Added need "save" feature from the Detail Window, even though the window won't look like a detail window
	hideApplyButton: true,
	hideOKButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Position Monitor',
				items: [{
					xtype: 'trade-position-monitor-grid',
					defaultViewName: 'Market Value',
					includeTradingActions: false,
					includeOperationsActions: true
				}]
			},
			{
				title: 'Utilization',
				reloadOnTabChange: true,
				items: [{
					xtype: 'trade-utilization-grid',
					clientInvestmentAccountEditorWindow: 'Clifton.trade.monitor.ClientHoldingsMonitorWindow',
					includeTradingActions: false,
					includeOperationsActions: true
				}]
			}
		]
	}]
});
