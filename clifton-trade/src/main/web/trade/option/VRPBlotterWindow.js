TCG.use('Clifton.trade.monitor.PositionMonitorGridPanel');
TCG.use('Clifton.trade.monitor.UtilizationGridPanel');
TCG.use('Clifton.trade.option.OptionSpreadTradingWindow');
TCG.use('Clifton.trade.option.OptionsBlotterWindow');

Clifton.trade.option.VRPBlotterWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'vrpBlotterWindow',
	title: 'Volatility Risk Premium Blotter',
	iconCls: 'options',
	width: 1250,
	height: 750,
	maximized: true,

	// Additional Tabs that are Added need "save" feature from the Detail Window, even though the window won't look like a detail window
	hideApplyButton: true,
	hideOKButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Position Monitor',
				items: [{
					xtype: 'trade-position-monitor-grid',
					investmentType: 'Options',
					includeTradingActions: true,
					includeOperationsActions: false
				}]
			},
			{
				title: 'Utilization',
				reloadOnTabChange: true,
				items: [{
					xtype: 'trade-utilization-grid',
					includeTradingActions: true,
					includeOperationsActions: false
				}]
			},
			{
				title: 'Option Spread',
				items: [
					{
						xtype: 'option-spread-form-panel',
						tradeEntry: true
					}
				]
			},
			{
				title: 'Expiration',
				reloadOnTabChange: true,
				items: [{xtype: 'optionsExpirationGrid', defaultClientAccountGroup: 'Client Accounts Managed by Westport', tradeSourceName: 'DeltaShift Blotter'}]
			},
			{
				title: 'All Trades',
				reloadOnTabChange: true,
				items: [{xtype: 'trade-blotter-all-trades-gridpanel'}]
			},
			{
				title: 'Draft Trades',
				reloadOnTabChange: true,
				items: [{xtype: 'trade-blotter-draft-trades-gridpanel'}]
			},
			{
				title: 'Pending Trades',
				reloadOnTabChange: true,
				items: [{xtype: 'trade-blotter-pending-trades-gridpanel'}]
			},
			{
				title: 'Approved Trades',
				reloadOnTabChange: true,
				items: [{xtype: 'trade-blotter-approved-trades-gridpanel'}]
			},
			{
				title: 'Active Orders',
				reloadOnTabChange: true,
				items: [{xtype: 'trade-blotter-active-orders-panel'}]
			},
			{
				title: 'Trade Groups',
				reloadOnTabChange: true,
				items: [
					{
						name: 'tradeGroupListFind',
						xtype: 'gridpanel',
						instructions: 'The follow lists any Options specific Trade Groups. Trade Groups are collections of trades executed for a purpose, i.e. Option Delivery, Option Expiration, Option Rolls',
						columns: [
							{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
							{header: 'Workflow Status', width: 15, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
							{header: 'Workflow State', width: 15, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Group Type', width: 20, dataIndex: 'tradeGroupType.name', filter: {searchFieldName: 'tradeGroupTypeName'}},
							{header: 'Group Note', width: 80, dataIndex: 'note', filter: false, hidden: true},
							{header: 'Security', width: 20, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: '2nd Security', width: 15, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 13, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 15, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'}
						],
						getTopToolbarFilters: function(toolbar) {
							return [
								{
									fieldLabel: 'Team', width: 100, minListWidth: 150, name: 'teamFilter', hiddenName: 'securityUserGroupId', xtype: 'toolbar-combo', url: 'securityGroupTeamList.json', loadAll: true, required: false
								},
								{
									fieldLabel: '&nbsp', labelSeparator: '', boxLabel: 'Exclude Strangle Groups', name: 'excludeStrangleGroupTypes', xtype: 'toolbar-checkbox', checked: true,
									qtip: 'If checked, the Option Strangle and Option Tail trade groups will be excluded so only the highest level trade groups are displayed.'
								}
							];
						},
						applyTopToolbarFilterLoadParams: function(params) {
							const preparedParams = params || {};
							const filter = TCG.getChildByName(this.getTopToolbar(), 'teamFilter');
							if (filter && TCG.isNotBlank(filter.getValue())) {
								preparedParams.securityUserGroupId = filter.getValue();
							}
							return preparedParams;
						},
						getLoadParams: async function(firstLoad) {
							const params = {
								includeBeginsWithTradeGroupTypeNames: ['Option', 'Multi-Client'],
								readUncommittedRequested: true
							};

							const toolbar = this.getTopToolbar();
							const excludeStrangleGroupTypesCheckbox = TCG.getChildByName(toolbar, 'excludeStrangleGroupTypes');
							let groupExclusionPromise = void 0;
							if (TCG.isTrue(excludeStrangleGroupTypesCheckbox.getValue())) {
								groupExclusionPromise = Promise.all([
									TCG.data.getDataPromiseUsingCaching('tradeGroupTypeByName.json?name=Option Strangle', this, 'trade.group.type.Option Strangle'),
									TCG.data.getDataPromiseUsingCaching('tradeGroupTypeByName.json?name=Option Tail', this, 'trade.group.type.Option Tail')
								]).then(([strangleGroup, tailGroup]) => {
									const excludedTradeGroupIds = [];
									if (strangleGroup) {
										excludedTradeGroupIds.push(strangleGroup.id);
									}
									if (tailGroup) {
										excludedTradeGroupIds.push(tailGroup.id);
									}
									if (excludedTradeGroupIds.length > 0) {
										params['excludeTradeGroupTypeIds'] = excludedTradeGroupIds;
									}
								});
							}
							return Promise.resolve(groupExclusionPromise)
								.then(() => {
									if (firstLoad) {
										TCG.data.getDataPromiseUsingCaching('securityGroupByName.json?name=Team Strategy', this, 'securityGroup.name.Team Strategy')
											.then(team => {
												if (TCG.isNotNull(team)) {
													TCG.getChildByName(this.getTopToolbar(), 'teamFilter').setValue({value: team.id, text: team.name});
												}
											});
										this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
									}
									return this.applyTopToolbarFilterLoadParams(params);
								});
						},
						editor: {
							detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
							drillDownOnly: true
						}
					}
				]
			}
		]
	}]
});
