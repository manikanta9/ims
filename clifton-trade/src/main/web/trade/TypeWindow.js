Clifton.trade.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Type',
	iconCls: 'shopping-cart',
	height: 700,
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'tradeType.json',
					instructions: 'The following trade type applies to securities for the selected investment type.  If instrument group is selected, then it applies to a sub-set of that investment type for securities that belong to that group.  Each trade type defines trade specifics, including accounting booking rules.',
					readOnly: true,
					labelWidth: 170,
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},

						{
							fieldLabel: 'Investment Type', name: 'investmentType.name', xtype: 'displayfield',
							qtip: 'Limits securities managed by this Trade Type to those of the specified Investment Type.'
						},
						{
							fieldLabel: 'Instrument Group', name: 'investmentGroup.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.setup.GroupWindow', detailIdField: 'investmentGroup.id',
							qtip: 'Optional Instrument Group that further limits securities for selected Investment Type.'
						},
						{
							fieldLabel: 'Holding Account Purpose', name: 'holdingAccountPurpose.name', xtype: 'displayfield',
							qtip: 'Restricts client/holding account selection to active relationships with this purpose.'
						},
						{
							fieldLabel: 'Trade Report', name: 'tradeReport.name', xtype: 'linkfield', detailPageClass: 'Clifton.report.ReportWindow', detailIdField: 'tradeReport.id',
							qtip: 'Optional trade report defined for this trade type.  When defined, users can pull the report directly from the Trade Match screen.'
						},
						{
							fieldLabel: 'Investment Specificity', name: 'executionInvestmentSpecificityDefinition.name', xtype: 'linkfield', detailPageClass: 'Clifton.investment.specificity.SpecificityDefinitionWindow', detailIdField: 'executionInvestmentSpecificityDefinition.id',
							qtip: 'The specificity definition that will be used to retrieve the Freemark FpML of order execution via FIX and the InvestmentInstrumentHierarchy that will be used to create a new security if needed.'
						},

						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Commission GL Account', name: 'commissionAccountingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.account.AccountWindow', detailIdField: 'commissionAccountingAccount.id',
							qtip: 'When Booking trades of this type use this GL Account for Commission Amount field'
						},
						{
							fieldLabel: 'Fee GL Account', name: 'feeAccountingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.account.AccountWindow', detailIdField: 'feeAccountingAccount.id',
							qtip: 'When Booking trades of this type use this GL Account for Fee Amount field'
						},
						{
							fieldLabel: 'Accrual GL Account (BUY)', name: 'buyAccrualAccountingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.account.AccountWindow', detailIdField: 'buyAccrualAccountingAccount.id',
							qtip: 'Different GL Accounts maybe used for accrualAmount on buy vs sell trades. For example, Interest Income vs Interest Expense for bond trades. These fields are set only for trade types that support accruals.'
						},
						{
							fieldLabel: 'Accrual GL Account (SELL)', name: 'sellAccrualAccountingAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.accounting.account.AccountWindow', detailIdField: 'sellAccrualAccountingAccount.id',
							qtip: 'Different GL Accounts maybe used for accrualAmount on buy vs sell trades. For example, Interest Income vs Interest Expense for bond trades. These fields are set only for trade types that support accruals.'
						},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'FX Date Selector', name: 'exchangeRateDateSelector',
							qtip: 'Some securities may require Exchange Rate from a very specific date for foreign transactions (Trade Date for Cleared CDS).'
						},
						{
							boxLabel: 'Follows industry standard currency conventions', width: 25, name: 'currencyConventionFollowed', xtype: 'checkbox',
							qtip: 'Specifies whether trades of this type follow industry standard exchange rate currency convention. If true, then conversion from local currency to base currency will take into account the dominant currency before deciding whether local amount should be multiplied or divided by exchange rate. If false, then local currency amount is always multiplied by the exchange rate to get to base currency amount.'
						},
						{boxLabel: 'Trades of this type can only have one fill: no need to show Fills tab on Trade detail window.', name: 'singleFillTrade', xtype: 'checkbox'},
						{boxLabel: 'Specifies whether trades of this type support execution type field: Limit, MOC, etc.', name: 'tradeExecutionTypeSupported', xtype: 'checkbox'},
						{boxLabel: 'Specifies whether trades of this type support execution using FIX protocol.', name: 'fixSupported', xtype: 'checkbox'},
						{boxLabel: 'Specifies whether trades of this type can also be used as Collateral (bonds and stocks).', name: 'collateralSupported', xtype: 'checkbox'},
						{boxLabel: 'Allow trades to be saved with no executing broker, and then updated after execution is complete.', name: 'brokerSelectionAtExecutionAllowed', xtype: 'checkbox'},
						{boxLabel: 'Allow trades to be saved with no holding account, and then updated after execution is complete.', name: 'holdingAccountSelectionAtExecutionAllowed', xtype: 'checkbox'},
						{boxLabel: 'Allow trades to be saved with an invalid security, and then a new security is created after execution and the trades are updated.', name: 'securityCreationAtExecutionAllowed', xtype: 'checkbox'},

						{boxLabel: 'Signed Amounts: Buy/Sell does not determine direction of notional or cash flow.', name: 'amountsSignAccountsForBuy', xtype: 'checkbox', qtip: 'Notional amount is usually positive and its sign is determined by buy/sell field. If this option is checked, then buy/sell is ignored and positive amount indicates cash inflow while negative cash outflow.'},
						{boxLabel: 'Signed Accrual: use accrual sign to determine accrual direction instead of Buy/Sell indicator', name: 'accrualSignAccountsForBuy', xtype: 'checkbox', qtip: 'Accrual amount is usually positive and its sign is determined by buy/sell field. If this option is checked, then buy/sell is ignored and positive amount indicates cash inflow while negative cash outflow.'},

						{boxLabel: 'Trades of this type are allowed to take a position from short to long or vice versa.', name: 'crossingZeroAllowed', xtype: 'checkbox'},
						{boxLabel: 'Explicit Open/Close selection is allowed for trades of this type.', name: 'explicitOpenCloseSelectionAllowed', xtype: 'checkbox'},
						{boxLabel: 'Explicit Open/Close selection is required for trades of this type.', name: 'explicitOpenCloseSelectionRequired', xtype: 'checkbox'},
						{boxLabel: 'Trades of this type require a trade execution type.', name: 'executionTypeRequired', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Execution Types',
				items: [{
					name: 'tradeTypeTradeExecutionTypeListFind',
					xtype: 'gridpanel',
					readOnly: false,
					instructions: 'Below are the trade execution types that are allowed for use with this trade type. Trades using this trade type will be limited to the trade execution types listed below.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Type', width: 50, dataIndex: 'referenceOne.name', hidden: true},
						{header: 'Trade Execution Type', width: 50, dataIndex: 'referenceTwo.name'},
						{header: 'Default Execution Type', width: 40, dataIndex: 'defaultExecutionType', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {tradeTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.trade.execution.TradeTypeTradeExecutionTypeWindow',
						getDeleteURL: function() {
							return 'tradeTypeTradeExecutionTypeDelete.json';
						},
						getDefaultData: function(gridPanel) {
							return {referenceOne: gridPanel.getWindow().getMainForm().formValues};
						}
					}
				}]
			},


			{
				title: 'Destination Mappings',
				items: [{
					name: 'tradeDestinationMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Defines allowed trade destinations and executing brokers for this Trade Type. If Instrument Group is selected, then only securities in that group will be mapped.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Destination', width: 45, dataIndex: 'tradeDestination.name', filter: {searchFieldName: 'tradeDestinationName'}, defaultSortColumn: true},
						{header: 'Executing Broker', width: 75, dataIndex: 'executingBrokerCompany.name', filter: {searchFieldName: 'executingBrokerCompanyName'}},
						{header: 'Override Broker Code', width: 45, dataIndex: 'overrideBrokerCode', filter: {searchFieldName: 'overrideBrokerCode'}},
						{header: 'Instrument Group', width: 75, dataIndex: 'defaultInvestmentGroup.name', filter: {searchFieldName: 'defaultInvestmentGroupId', type: 'combo', url: 'investmentGroupListFind.json'}},
						{header: 'Default', width: 23, dataIndex: 'defaultDestinationMapping', type: 'boolean'},
						{header: 'Active', width: 23, dataIndex: 'active', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {tradeTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.trade.destination.TradeDestinationMappingWindow',
						getDefaultData: function(gridPanel) {
							return {tradeType: gridPanel.getWindow().getMainForm().formValues};
						}
					}
				}]
			}
		]
	}]
});
