TCG.use('Clifton.trade.CurrencyTradeWindow');

Clifton.trade.entry.CurrencyTradeEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Currency Trade Entry',
	title: 'NEW',
	width: 1200,
	height: 450,
	iconCls: 'currency',
	saveTimeout: 180,

	items: [{
		xtype: 'trade-form-base',
		url: 'tradeEntry.json',

		listeners: {},

		getDefaultData: function(win) {
			const fp = this;
			const f = fp.getForm();
			let dd = Ext.apply(win.defaultData || {}, {});
			if (win.defaultDataIsReal) {
				return dd;
			}
			const tradeTypeName = win.tradeType;

			return TCG.data.getDataPromiseUsingCaching('tradeDestinationByName.json?name=FX Connect Ticket', fp, 'trade.tradeDestination.FX Connect Ticket')
				.then(function(tradeDestination) {
					dd.tradeDestination = tradeDestination;
					return TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + tradeTypeName, fp, 'trade.type.' + tradeTypeName);
				})
				.then(function(tradeType) {
					dd = Ext.apply({
						tradeType: tradeType,
						executingBrokerCompany: tradeType.defaultExecutingBrokerCompany,
						fixTrade: tradeType.fixSupported,
						traderUser: TCG.getCurrentUser(),
						tradeDate: new Date().format('Y-m-d 00:00:00') // Default to Today
					}, dd);

					if (dd.clientInvestmentAccount && !dd.holdingInvestmentAccount) {
						// Need to trigger client account as selected so holding account selection is updated (and usually pre-selected when only one account applies)
						// The default data investmentSecurity is passed in to indicate that if the holding account can be auto selected, the current balance should be loaded
						f.addListener('afterRender', fp.updateHoldingAccountSelection.defer(300, fp, [dd.clientInvestmentAccount]));
					}

					if (dd.investmentSecurity) {
						return Clifton.investment.instrument.getTradeDatePromise(dd.investmentSecurity.id, null, fp)
							.then(function(tradeDate) {
								dd.tradeDate = tradeDate.format('Y-m-d 00:00:00');
								if (dd.payingSecurity) {
									return Clifton.investment.instrument.getSettlementDatePromise(dd.investmentSecurity.id, tradeDate.format('m/d/Y'), false, dd.payingSecurity.id, fp)
										.then(function(settlementDate) {
											dd.settlementDate = settlementDate.format('Y-m-d 00:00:00');
											return dd;
										});
								}
								return dd;
							});
					}

					return dd;
				});
		},

		updatePayingNotional: function() {
			const fp = this;
			const f = fp.getForm();
			const v = f.findField('accountingNotional').getNumericValue();
			const fx = f.findField('exchangeRateToBase').getNumericValue();
			if (TCG.isNumber(v) && TCG.isNumber(fx)) {
				const to = f.findField('payingSecurity.label').getRawValue().substring(0, 3);
				const from = f.findField('investmentSecurity.label').getRawValue().substring(0, 3);
				TCG.data.getDataValuePromise('investmentCurrencyMultiplyConventionUsed.json?fromSymbol=' + from + '&toSymbol=' + to, this)
					.then(function(multiply) {
						fp.setFormValue('payingNotional', TCG.isTrue(multiply) ? v * fx : v / fx, true);
					});
			}
		},

		updateExchangeRate: async function() {
			const formPanel = this;
			const form = formPanel.getForm();
			const fxField = form.findField('exchangeRateToBase');
			const fromId = form.findField('investmentSecurity.id').getValue();
			const toId = form.findField('payingSecurity.id').getValue();
			if (TCG.isNumber(fromId) && TCG.isNumber(toId)) {
				const holdingAccountIssuingCompanyId = 0; //use default source, not all issuing/executing companies have values
				const settlementDate = form.findField('tradeDate').value || form.findField('settlementDate').value;
				const exchangeRate = await TCG.data.getDataPromise('marketDataExchangeRateForDateFlexible.json?companyId=' + holdingAccountIssuingCompanyId + '&fromCurrencyId=' + fromId + '&toCurrencyId=' + toId + '&rateDate=' + settlementDate, this)
					.then(rate => rate ? rate.exchangeRate : void 0);
				if (exchangeRate) {
					const fromSymbol = this.getSymbolForCurrencyFieldPrefix('investmentSecurity');
					const toSymbol = this.getSymbolForCurrencyFieldPrefix('payingSecurity');
					const isMultipleConvention = await TCG.data.getDataValuePromise(`investmentCurrencyMultiplyConventionUsed.json?fromSymbol=${fromSymbol}&toSymbol=${toSymbol}`, this);
					const rate = TCG.isTrue(isMultipleConvention) ? exchangeRate : 1 / exchangeRate;
					fxField.setValue(rate);
				}
			}
		},

		getSymbolForCurrencyFieldPrefix: function(currencyFieldPrefix) {
			const l = this.getForm().findField(currencyFieldPrefix + '.label').getRawValue();
			return (l.length > 2) ? l.substring(0, 3) : l;
		},

		items: [
			{
				xtype: 'panel',
				layout: 'column',
				defaults: {
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-20'},
					columnWidth: .33,
					labelWidth: 90
				},
				items: [
					{
						items: [
							{
								xtype: 'radiogroup',
								fieldLabel: 'Buy/Sell',
								columns: [60, 60],
								allowBlank: false,
								anchor: '-25',
								items: [
									{boxLabel: 'Buy', name: 'buy', inputValue: true},
									{boxLabel: 'Sell', name: 'buy', inputValue: false}
								],
								listeners: {
									change: function(radioGroup, checkedRadio) {
										const formPanel = TCG.getParentFormPanel(radioGroup);
										const params = {
											tradeTypeId: formPanel.getFormValue('tradeType.id'),
											buy: checkedRadio.getRawValue()
										};
										TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params})
											.then(function(openCloseType) {
												if (openCloseType && openCloseType.length === 1) {
													formPanel.getForm().findField('openCloseType.id').setValue(openCloseType[0].id);
												}
											});
									}
								}
							},
							{xtype: 'hidden', name: 'openCloseType.id'},
							{
								fieldLabel: 'Given CCY', name: 'investmentSecurity.label', allowBlank: false, displayField: 'label', hiddenName: 'investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?currency=true', doNotClearIfRequiredSet: true,
								listeners: {

									beforeQuery: function(qEvent) {
										const f = qEvent.combo.getParentForm().getForm();
										qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value};
									},

									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const form = fp.getForm();

										Clifton.investment.instrument.getTradeDatePromise(record.id, null, combo)
											.then(function(tradeDate) {
												form.findField('tradeDate').setValue(tradeDate);
												return Clifton.investment.instrument.getSettlementDatePromise(record.id, tradeDate, false, form.findField('payingSecurity.id').value, combo);
											})
											.then(function(settlementDate) {
												form.findField('settlementDate').setValue(settlementDate);
												fp.updateExchangeRate.call(fp);
												Clifton.trade.updateExistingPositions(fp, record.id);
											});
									}
								},

								getDetailPageClass: function(newInstance) {
									if (newInstance) {
										return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
									}
									return this.detailPageClass;
								},

								getDefaultData: function(f) { // defaults client/counterparty/isda
									let fp = TCG.getParentFormPanel(this);
									fp = fp.getForm();
									const investmentGroupId = TCG.getValue('tradeType.investmentGroup.id', fp.formValues);
									const investmentTypeId = TCG.getValue('tradeType.investmentType.id', fp.formValues);

									return {
										investmentGroupId: investmentGroupId,
										investmentTypeId: investmentTypeId
									};
								}
							},
							{
								fieldLabel: 'Settle CCY', name: 'payingSecurity.label', hiddenName: 'payingSecurity.id', allowBlank: false, displayField: 'label', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?currency=true', doNotClearIfRequiredSet: true,
								listeners: {
									beforeQuery: function(qEvent) {
										const f = qEvent.combo.getParentForm().getForm();
										qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value};
									},
									select: function(combo, record, index) {
										const fp = combo.getParentForm();
										const form = fp.getForm();

										Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, form.findField('tradeDate').value, false, record.id, combo)
											.then(function(settlementDate) {
												form.findField('settlementDate').setValue(settlementDate);
												fp.updateExchangeRate.call(fp);
											});
									}
								},

								getDetailPageClass: function(newInstance) {
									if (newInstance) {
										return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
									}
									return this.detailPageClass;
								},

								getDefaultData: function(f) { // defaults client/counterparty/isda
									let fp = TCG.getParentFormPanel(this);
									fp = fp.getForm();
									const investmentGroupId = TCG.getValue('tradeType.investmentGroup.id', fp.formValues);
									const investmentTypeId = TCG.getValue('tradeType.investmentType.id', fp.formValues);

									return {
										investmentGroupId: investmentGroupId,
										investmentTypeId: investmentTypeId
									};
								}
							},
							{
								fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', allowBlank: true, tooltip: 'Currency exchange rates should always be the exchange rate between the dominant currency to the other, which is the industry standard convention. For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency',
								listeners: {
									valid: function(field) {
										const fp = TCG.getParentFormPanel(field);
										fp.updatePayingNotional.call(fp);
									}
								}
							}

						]
					},
					{
						items: [
							{
								fieldLabel: 'Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										combo.store.baseParams = {
											tradeTypeId: f.findField('tradeType.id').getValue(),
											activeOnDate: f.findField('tradeDate').value
										};
									},
									TODO_select: function(combo, record, index) {
										const fp = combo.getParentForm();
										// clear existing destinations
										const dst = fp.getForm().findField('executingBrokerCompany.label');
										dst.store.removeAll();
										dst.lastQuery = null;
										fp.updateExecutingBrokerCompany(record.id);
									}
								}
							},
							{
								fieldLabel: 'Given Amount', name: 'accountingNotional', xtype: 'currencyfield', minValue: 0, readOnly: true,
								listeners: {
									change: function(field, newValue, oldValue) {
										const fp = TCG.getParentFormPanel(field);
										fp.updateExchangeRate.call(fp); // exchange rate update will update paying notional
									}
								}
							},
							{fieldLabel: 'Settle Amount', name: 'payingNotional', xtype: 'currencyfield', readOnly: true}
						]
					},
					{
						items: [
							{name: 'tradeType.id', xtype: 'hidden'},
							{
								fieldLabel: 'Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo',
								displayField: 'label',
								//valueField: 'executingBrokerCompany.id',
								//tooltipField: 'executingBrokerCompany.description',
								queryParam: 'executingBrokerCompanyName',
								detailPageClass: 'Clifton.business.company.CompanyWindow',
								url: 'tradeExecutingBrokerCompanyListFind.json',
								allowBlank: true,
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const f = combo.getParentForm().getForm();
										combo.store.baseParams = {
											tradeDestinationId: f.findField('tradeDestination.name').getValue(),
											tradeTypeId: f.findField('tradeType.id').getValue(),
											activeOnDate: f.findField('tradeDate').value
										};
									}
								}
							},
							{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
							{
								fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
								getParentFormPanel: function() {
									const parent = TCG.getParentFormPanel(this);
									return parent || this.ownerCt;
								},
								listeners: {
									change: function(field, newValue, oldValue) {
										const form = field.getParentFormPanel().getForm();
										const settlementDateField = form.findField('settlementDate');

										if (TCG.isBlank(newValue)) {
											settlementDateField.setValue('');
										}
										else {
											Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, newValue, false, form.findField('payingSecurity.id').value, field)
												.then(function(settlementDate) {
													form.findField('settlementDate').setValue(settlementDate);
												});
										}
									},
									select: function(field, date) {
										const form = field.getParentFormPanel().getForm();
										Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, date, false, form.findField('payingSecurity.id').value, field)
											.then(function(settlementDate) {
												form.findField('settlementDate').setValue(settlementDate);
											});
									}
								}
							},
							{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false}
						]
					}
				]
			},
			{xtype: 'label', html: '<hr/>'},
			{xtype: 'trade-descriptionWithNoteDragDrop'},
			{
				xtype: 'trade-group-entry-grid',
				doNotSubmitFields: ['payingNotional', 'currentBalance'],
				columnOverrides: [
					{
						dataIndex: 'clientInvestmentAccount.label',
						editor: {
							xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', displayField: 'label', allowBlank: false,
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const fp = TCG.getParentFormPanel(combo.gridEditor.containerGrid);
								const f = fp.getForm();
								combo.store.baseParams = {
									relatedPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
									relatedPurposeActiveOnDate: f.findField('tradeDate').value
								};
							},
							selectHandler: function(combo, record, index) {
								const grid = combo.gridEditor.containerGrid;
								const fp = TCG.getParentFormPanel(grid);
								const f = fp.getForm();
								const row = combo.gridEditor.row;
								grid.startEditing(row, 1);

								// clear holding account and auto-select new if one
								const hCombo = grid.getColumnModel().getCellEditor(1, row).field;
								hCombo.clearValue();
								hCombo.store.removeAll();
								hCombo.lastQuery = null;
								hCombo.store.baseParams = {
									mainAccountId: combo.value,
									ourAccount: false,
									workflowStatusNameEquals: 'Active',
									mainPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
									mainPurposeActiveOnDate: f.findField('tradeDate').value,
									contractRequired: fp.getFormValue('investmentSecurity.instrument.hierarchy.otc')
								};
								hCombo.store.load({
									callback: function(records, opts, success) {
										if (success && records.length === 1) {
											const r = records[0];
											hCombo.setValue({value: r.get('id'), text: r.get('label')});
											grid.startEditing(row, 2);
										}
									}
								});
								// Set base ccy

								const gridRecord = grid.getStore().getAt(row);
								gridRecord.beginEdit();
								gridRecord.set('clientInvestmentAccount.baseCurrency.label', TCG.getValue('baseCurrency.label', record.json));
								gridRecord.set('clientInvestmentAccount.baseCurrency.id', TCG.getValue('baseCurrency.id', record.json));
								gridRecord.endEdit();
							}
						}
					},
					{
						dataIndex: 'holdingInvestmentAccount.label',
						editor: {
							xtype: 'combo', loadAll: true, url: 'investmentAccountListFind.json', displayField: 'label', allowBlank: false,
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const grid = combo.gridEditor.containerGrid;
								const fp = TCG.getParentFormPanel(grid);
								const f = fp.getForm();
								const row = combo.gridEditor.row;
								combo.store.baseParams = {
									ourAccount: false,
									workflowStatusNameEquals: 'Active',
									mainAccountId: grid.getColumnModel().getCellEditor(0, row).getValue(),
									mainPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
									mainPurposeActiveOnDate: f.findField('tradeDate').value,
									contractRequired: fp.getFormValue('investmentSecurity.instrument.hierarchy.otc')
								};
							}
						}
					},
					{
						header: 'Given Amount', dataIndex: 'accountingNotional', type: 'currency',
						editor: {xtype: 'currencyfield', readOnly: false}
					},
					{header: 'Settle Amount', dataIndex: 'payingNotional', type: 'currency'},
					{header: 'Current Balance', dataIndex: 'currentBalance', type: 'currency', tooltip: 'Balance of the given currency for the client account.'},
					{header: 'Base CCY', dataIndex: 'clientInvestmentAccount.baseCurrency.label', idDataIndex: 'clientInvestmentAccount.baseCurrency.id', tooltip: 'Base currency of the client account'},
					{dataIndex: 'quantityIntended', hidden: true},
					{dataIndex: 'expectedUnitPrice', hidden: true},
					{dataIndex: 'commissionPerUnit', hidden: true},
					{dataIndex: 'feeAmount', hidden: true},
					{
						header: 'Executing Broker', dataIndex: 'executingBrokerCompany.label', idDataIndex: 'executingBrokerCompany.id',
						tooltip: 'Use this field to override the Executing Broker.  If this is left blank, the Executing Broker specified in the form above will be used for all trades. Use this field to override a broker for an individual trade.',
						editor: {
							xtype: 'combo', displayField: 'label', allowBlank: true,
							url: 'tradeExecutingBrokerCompanyListFind.json',
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const grid = combo.gridEditor.containerGrid;
								const fp = TCG.getParentFormPanel(grid);
								const f = fp.getForm();
								combo.store.baseParams = {
									tradeDestinationId: f.findField('tradeDestination.name').getValue(),
									tradeTypeId: f.findField('tradeType.id').getValue(),
									activeOnDate: f.findField('tradeDate').value
								};
							}
						}
					}
				],
				plugins: {ptype: 'gridsummary'},
				listeners: {
					afteredit: function(event) {
						/*
						 * event fired by Ext JS, there is one argument that contains: column, field, grid, originalValue, record, row, and value.
						 */
						if (event.originalValue !== event.value && !TCG.isBlank(event.value)) {
							const grid = event.grid;
							const fp = TCG.getParentFormPanel(grid);
							const f = fp.getForm();
							const gridRecord = grid.getStore().getAt(event.row);

							if (gridRecord) {
								// Update current account balance (make sure required fields are defined)
								const acctId = gridRecord.get('clientInvestmentAccount.id');
								const holdingAcctId = gridRecord.get('holdingInvestmentAccount.id');
								const securityId = f.findField('investmentSecurity.id').getValue();
								let date = f.findField('tradeDate').value;
								if (TCG.isBlank(date)) {
									date = new Date().format('m/d/Y');
								}


								if (acctId && holdingAcctId && securityId && date) {
									TCG.data.getDataValuePromise('accountingBalanceForSecurityLocal.json?investmentSecurityId=' + securityId + '&clientInvestmentAccountId=' + acctId + '&holdingInvestmentAccountId=' + holdingAcctId + '&transactionDate=' + date, this)
										.then(function(result) {
											if (!TCG.isBlank(result)) {
												gridRecord.beginEdit();
												gridRecord.set('currentBalance', result);
												gridRecord.endEdit();
											}
										});
								}

								// Update given and settle amounts
								const v = gridRecord.get('accountingNotional');
								const exchRate = f.findField('exchangeRateToBase');

								if (v && exchRate) {
									const fx = exchRate.getNumericValue();
									if (TCG.isNumber(v) && TCG.isNumber(fx)) {
										const to = f.findField('payingSecurity.label').getRawValue().substring(0, 3);
										const from = f.findField('investmentSecurity.label').getRawValue().substring(0, 3);
										TCG.data.getDataValuePromise('investmentCurrencyMultiplyConventionUsed.json?fromSymbol=' + from + '&toSymbol=' + to, this)
											.then(function(multiply) {
												gridRecord.beginEdit();
												gridRecord.set('payingNotional', TCG.isTrue(multiply) ? v * fx : v / fx);
												gridRecord.endEdit();
												// Update parent form amounts
												let totalGivenAmount = 0;
												let totalSettleAmount = 0;
												event.grid.getStore().each(function(r) {
													totalGivenAmount += r.get('accountingNotional');
													totalSettleAmount += r.get('payingNotional');
												});
												const currencyForm = TCG.getParentFormPanel(event.grid).getForm();
												const givenAmountField = currencyForm.findField('accountingNotional');
												givenAmountField.setValue(totalGivenAmount);
												const settleAmountField = currencyForm.findField('payingNotional');
												settleAmountField.setValue(totalSettleAmount);
												// Force user to Executing Broker column, that way settle amount column is visible post-edit to avoid confusion
												grid.startEditing(event.row, 10);
											});
									}
								}
							}
						}
					}
				},
				addToolbarRemoveButton: function(toolBar) {
					const grid = this;
					toolBar.add({
						text: 'Remove',
						tooltip: 'Remove selected item',
						iconCls: 'remove',
						scope: this,
						handler: function() {
							const index = grid.getSelectionModel().getSelectedCell();
							if (index) {
								const store = grid.getStore();
								const rec = store.getAt(index[0]);
								store.remove(rec);
								grid.markModified();
								let totalGivenAmount = 0;
								let totalSettleAmount = 0;
								grid.getStore().each(function(r) {
									totalGivenAmount += r.get('accountingNotional');
									totalSettleAmount += r.get('payingNotional');
								});
								const currencyForm = TCG.getParentFormPanel(grid).getForm();
								const givenAmountField = currencyForm.findField('accountingNotional');
								givenAmountField.setValue(totalGivenAmount);
								const settleAmountField = currencyForm.findField('payingNotional');
								settleAmountField.setValue(totalSettleAmount);
							}
						}
					});
				}
			}
		]
	}]
});
