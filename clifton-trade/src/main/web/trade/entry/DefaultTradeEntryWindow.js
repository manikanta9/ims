TCG.use('Clifton.trade.DefaultTradeWindow');

Clifton.trade.entry.DefaultTradeEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Trade Entry',
	title: 'NEW',
	width: 950,
	height: 600,
	iconCls: 'shopping-cart',
	saveTimeout: 180,

	items: [{
		xtype: 'trade-default-form',
		url: 'tradeEntry.json',

		listeners: {
			beforerender: function() {
				this.add(this.commonEntryItems);
				this.add(this.additionalEntryItems);
			}
		},

		afterRenderPromise: function(formPanel) {
			// If default data was passed in with tradeDetailList data, the grid needs to see that data
			const tradeDetailRows = this.getFormValue('tradeDetailList');
			const tradeDetailListGrid = TCG.getChildByName(formPanel, 'tradeDetailListGrid');
			if (Array.isArray(tradeDetailRows) && tradeDetailRows.length > 0 && tradeDetailListGrid.getStore().getTotalCount() !== tradeDetailRows.length) {
				tradeDetailListGrid.getStore().loadData(this.getForm().formValues);
				// apply changes for submit
				tradeDetailListGrid.markModified();
			}
		},

		items: [],

		additionalEntryItems: [
			{name: 'expectedUnitPrice', xtype: 'floatfield', hidden: true},
			{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', height: 50, anchor: '-20'},
			{xtype: 'trade-group-entry-grid'},
			{xtype: 'trade-existingPositionsGrid'}
		],

		populateClosingPosition: async function(positionRecord) {
			const formPanel = this;

			if (!(await formPanel.populateClosingPositionBuySell(positionRecord))) {
				return;
			}

			const allocationGrid = TCG.getChildByName(formPanel, 'tradeDetailListGrid');
			const allocationRecord = allocationGrid.getAllocationRecord.call(allocationGrid, positionRecord);
			const quantity = Math.abs(formPanel.getClosingPositionQuantity.call(formPanel, positionRecord));
			const formQuantityAdjustment = quantity - (allocationRecord.get('quantityIntended') || 0);
			allocationRecord.set('quantityIntended', quantity);
			allocationGrid.markModified();

			if (formQuantityAdjustment !== 0) {
				const quantityField = formPanel.getForm().findField('quantityIntended');
				const currentOriginalFace = quantityField.getValue() || 0;
				quantityField.setValue(currentOriginalFace + formQuantityAdjustment);
			}
		}
	}]
});
