TCG.use('Clifton.trade.BondTradeWindow');

Clifton.trade.entry.BondTradeEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Bond Trade Entry',
	title: 'NEW',
	width: 950,
	height: 600,
	iconCls: 'bonds',
	saveTimeout: 180,

	items: [{
		xtype: 'trade-bond-form',
		url: 'tradeEntry.json',

		listeners: {
			beforerender: function() {
				this.add(this.commonEntryItems);
				this.add(this.additionalEntryItems);
			}
		},

		items: [],

		additionalEntryItems: [
			{fieldLabel: 'Trade Note', name: 'description', xtype: 'textarea', height: 50, anchor: '-20'},
			{
				xtype: 'trade-group-entry-grid',
				doNotSubmitFields: ['totalSpent'],
				columnsConfig: [
					{
						header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', width: 280,
						editor: {
							xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', displayField: 'label', allowBlank: false,
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const fp = TCG.getParentFormPanel(combo.gridEditor.containerGrid);
								const f = fp.getForm();
								combo.store.baseParams = {
									relatedPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
									relatedPurposeActiveOnDate: f.findField('tradeDate').value
								};
							},
							selectHandler: function(combo, record, index) {
								const grid = combo.gridEditor.containerGrid;
								const fp = TCG.getParentFormPanel(grid);
								const f = fp.getForm();
								const row = combo.gridEditor.row;
								grid.startEditing(row, 1);

								// clear holding account and auto-select new if one
								const hCombo = grid.getColumnModel().getCellEditor(1, row).field;
								hCombo.clearValue();
								hCombo.store.removeAll();
								hCombo.lastQuery = null;
								hCombo.store.baseParams = {
									mainAccountId: combo.value,
									ourAccount: false,
									workflowStatusNameEquals: 'Active',
									mainPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
									mainPurposeActiveOnDate: f.findField('tradeDate').value,
									contractRequired: fp.getFormValue('investmentSecurity.instrument.hierarchy.otc')
								};
								hCombo.store.load({
									callback: function(records, opts, success) {
										if (success && records.length === 1) {
											const r = records[0];
											hCombo.setValue({value: r.get('id'), text: r.get('label')});
											grid.startEditing(row, 2);
										}
									}
								});
							}
						}
					},
					{
						header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', width: 280,
						editor: {
							xtype: 'combo', loadAll: true, url: 'investmentAccountListFind.json', displayField: 'label', allowBlank: false,
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const grid = combo.gridEditor.containerGrid;
								const fp = TCG.getParentFormPanel(grid);
								const f = fp.getForm();
								const row = combo.gridEditor.row;
								combo.store.baseParams = {
									ourAccount: false,
									workflowStatusNameEquals: 'Active',
									mainAccountId: grid.getColumnModel().getCellEditor(0, row).getValue(),
									mainPurpose: fp.getFormValue(fp.getForm().findField('collateralTrade').getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
									mainPurposeActiveOnDate: f.findField('tradeDate').value,
									contractRequired: fp.getFormValue('investmentSecurity.instrument.hierarchy.otc')
								};
							}
						}
					},
					{header: 'Original Face', dataIndex: 'originalFace', width: 145, type: 'float', editor: {xtype: 'numberfield', allowBlank: false}, summaryType: 'sum'},
					{header: 'Total Spent', dataIndex: 'totalSpent', width: 145, type: 'currency', hidden: true, editor: {xtype: 'currencyfield', readOnly: true}, summaryType: 'sum', tooltip: 'The proportion of the above total this line item represents.'}
				],
				plugins: {ptype: 'gridsummary'},
				listeners: {
					afteredit: function(event) {
						/*
						 * event fired by Ext JS, there is one argument that contains: column, field, grid, originalValue, record, row, and value.
						 */
						if (event.originalValue !== event.value && !TCG.isBlank(event.value)) {
							const bondForm = TCG.getParentFormPanel(event.grid).getForm();
							const totalField = bondForm.findField('totalPayment');
							const cm = event.grid.getColumnModel();
							const columnId = cm.findColumnIndex('totalSpent');
							if (totalField && totalField.isVisible()) {
								cm.setHidden(columnId, false);
								if (event.field === 'originalFace') {
									const totalOriginalFace = bondForm.findField('originalFace');
									event.grid.getStore().each(function(r) {
										const totalSpent = (r.get('originalFace') / totalOriginalFace.getNumericValue()) * totalField.getNumericValue();
										r.set('totalSpent', totalSpent);
									});
								}
							}
							else {
								cm.setHidden(columnId, true);
								event.grid.getStore().each(function(r) {
									r.set('totalSpent', 0);
								});
							}
						}
					}
				}
			},
			{xtype: 'trade-existingPositionsGrid', quantityColumnLabel: 'Current Face', usePositionBalance: true}
		],

		populateClosingPosition: async function(positionRecord) {
			const formPanel = this;

			if (!formPanel.populateClosingPositionBuySell(positionRecord)) {
				return;
			}

			const allocationGrid = TCG.getChildByName(formPanel, 'tradeDetailListGrid');
			const allocationRecord = allocationGrid.getAllocationRecord.call(allocationGrid, positionRecord);
			const originalFace = formPanel.getClosingPositionOriginalFace(positionRecord);
			const startingFaceValue = allocationRecord.get('originalFace');
			const formOriginalFaceAdjustment = originalFace - (startingFaceValue || 0);
			allocationRecord.set('originalFace', originalFace);
			allocationGrid.markModified();

			if (formOriginalFaceAdjustment !== 0) {
				const originalFaceField = formPanel.getForm().findField('originalFace');
				const currentOriginalFace = originalFaceField.getNumericValue() || 0;
				await formPanel.populateClosingPositionOriginalFace(currentOriginalFace + formOriginalFaceAdjustment);

				allocationGrid.fireEvent('afteredit', {
					grid: allocationGrid,
					record: allocationRecord,
					field: 'originalFace',
					value: originalFace,
					originalValue: startingFaceValue,
					row: allocationGrid.getStore().indexOf(allocationRecord),
					column: allocationGrid.getColumnModel().findColumnIndex('originalFace')
				});
			}
		}
	}]
});
