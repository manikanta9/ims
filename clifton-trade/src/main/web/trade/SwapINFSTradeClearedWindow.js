TCG.use('Clifton.trade.SwapINFSTradeWindow');

Clifton.trade.SwapINFSTradeClearedWindow = Ext.extend(Clifton.trade.SwapINFSTradeWindow, {
	centralClearing: true,
	executingBrokerNotRequired: true
});
