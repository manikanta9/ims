Clifton.trade.TradeListWindow = Ext.extend(TCG.app.Window, {
	id: 'tradeListWindow',
	title: 'Trades',
	iconCls: 'shopping-cart',
	width: 1700,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Trades',
				items: [{
					name: 'tradeListFind',
					xtype: 'trade-list-base-gridpanel',
					instructions: 'A list of all trades in the system. Use filters to limit the trades to only those that you need.',
					queryExportTagName: 'Trade',
					viewNames: ['Default View', 'Export Friendly', 'Westport Trading', 'Westport Operations'],
					standardColumnViewNames: {'createDate': ['Export Friendly'], 'updateDate': ['Export Friendly']},
					rowSelectionModel: 'multiple',
					columns: [
						{header: 'ID', width: 24, dataIndex: 'id', hidden: true, viewNames: ['Export Friendly']},
						{header: 'Order ID', width: 24, dataIndex: 'orderIdentifier', hidden: true, type: 'int', doNotFormat: true, useNull: true},
						{header: 'Workflow Status', width: 24, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade', showNotEquals: true}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Workflow State', width: 24, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default View', 'Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Trade Type', width: 22, dataIndex: 'tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}, viewNames: ['Default View', 'Export Friendly']},
						{header: 'Trade Group Type', width: 22, hidden: true, dataIndex: 'tradeGroup.tradeGroupType.name', filter: {type: 'combo', searchFieldName: 'tradeGroupTypeId', url: 'tradeGroupTypeListFind.json'}, viewNames: ['Export Friendly']},
						{header: 'Client Relationship', width: 80, dataIndex: 'clientInvestmentAccount.businessClient.clientRelationship.name', filter: {searchFieldName: 'businessClientRelationshipName'}, hidden: true},
						{header: 'Client', width: 80, dataIndex: 'clientInvestmentAccount.businessClient.name', filter: {searchFieldName: 'businessClientName'}, hidden: true},
						{header: 'Client Account', width: 80, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, viewNames: ['Default View', 'Export Friendly']},
						{header: 'Service', width: 80, dataIndex: 'clientInvestmentAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true, viewNames: ['Westport Trading']},
						{header: 'Holding Account ID', width: 40, dataIndex: 'holdingInvestmentAccount.id', type: 'int', filter: {searchFieldName: 'holdingInvestmentAccountId'}, hidden: true},
						{header: 'Holding Account', width: 80, dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true, viewNames: ['Export Friendly', 'Westport Trading']},
						{header: 'Holding Account Number', width: 30, dataIndex: 'holdingInvestmentAccount.number', idDataIndex: 'holdingInvestmentAccount.id', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}, hidden: true, viewNames: ['Westport Operations']},
						{header: 'Holding Account Name', width: 60, dataIndex: 'holdingInvestmentAccount.labelShort', idDataIndex: 'holdingInvestmentAccount.id', hidden: true, viewNames: ['Westport Operations'], filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false', dependencyFilter: {filterName: 'clientInvestmentAccount.label', parameterName: 'mainAccountId'}}},
						{header: 'Holding Company', width: 60, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Executing Broker', width: 60, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true, viewNames: ['Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Executing Sponsor', width: 30, dataIndex: 'executingSponsorCompany.name', filter: {type: 'combo', searchFieldName: 'executingSponsorCompanyId', url: 'businessCompanyListFind.json?companyType=Broker'}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Settlement Company', width: 60, dataIndex: 'settlementCompany.name', filter: {type: 'combo', searchFieldName: 'settlementCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Trade Note', width: 100, dataIndex: 'description', hidden: true, viewNames: ['Export Friendly']},
						{
							header: 'Buy/Sell', width: 15, dataIndex: 'buy', type: 'boolean', viewNames: ['Default View', 'Export Friendly'],
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{
							header: 'Open/Close', width: 20, dataIndex: 'openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json', loadAll: true}, viewNames: ['Export Friendly', 'Westport Trading', 'Westport Operations'],
							renderer: function(v, metaData) {
								metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
								return v;
							}
						},
						{header: 'Original Face', width: 20, dataIndex: 'originalFace', type: 'float', useNull: true, hidden: true},
						{header: 'Quantity', width: 20, dataIndex: 'quantityIntended', type: 'float', useNull: true, viewNames: ['Default View', 'Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Notional Multiplier', width: 20, dataIndex: 'notionalMultiplier', type: 'float', useNull: true, hidden: true},
						{header: 'Price Multiplier', width: 30, dataIndex: 'investmentSecurity.priceMultiplier', type: 'float', hidden: true, filter: {searchFieldName: 'priceMultiplier'}},
						{header: 'Security', width: 45, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['Default View', 'Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Security Name', width: 45, dataIndex: 'investmentSecurity.name', filter: {searchFieldName: 'investmentSecurityName'}, viewNames: ['Export Friendly']},
						{header: 'Underlying Security', width: 40, dataIndex: 'investmentSecurity.underlyingSecurity.symbol', filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true, viewNames: ['Westport Trading']},
						{header: 'Investment Hierarchy', width: 80, dataIndex: 'investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}, hidden: true},
						{
							header: 'CCY Denomination', width: 30, dataIndex: 'investmentSecurity.instrument.tradingCurrency.symbol', hidden: true,
							filter: {
								type: 'combo', searchFieldName: 'tradingCurrencyId', showNotEquals: true, displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'
							}
						},
						{
							header: 'Settlement CCY', width: 30, dataIndex: 'payingSecurity.symbol', hidden: true,
							filter: {
								type: 'combo', searchFieldName: 'payingSecurityId', showNotEquals: true, displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'
							}
						},
						{header: 'Exchange Rate', width: 24, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Average Price', width: 28, dataIndex: 'averageUnitPrice', type: 'float', useNull: true, viewNames: ['Default View', 'Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Expected Price', width: 28, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
						{header: 'Accounting Notional', width: 30, dataIndex: 'accountingNotional', type: 'currency', useNull: true, viewNames: ['Default View', 'Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Accrual 1', width: 20, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
						{header: 'Accrual 2', width: 20, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
						{header: 'Accrual', width: 20, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
						{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
						{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Commission Per Unit', width: 20, dataIndex: 'commissionPerUnit', type: 'float', hidden: true, useNull: true, viewNames: ['Export Friendly']},
						{header: 'Commission Amount', width: 20, dataIndex: 'commissionAmount', type: 'currency', hidden: true, useNull: true, tooltip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.', viewNames: ['Westport Trading', 'Westport Operations']},
						{header: 'Fee', width: 20, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.', viewNames: ['Westport Trading', 'Westport Operations']},
						{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
						{header: 'Team', width: 24, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Trader', width: 24, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}, viewNames: ['Default View', 'Export Friendly', 'Westport Trading']},
						{header: 'Collateral', width: 20, dataIndex: 'collateralTrade', type: 'boolean', hidden: true},
						{header: 'FIX', width: 20, dataIndex: 'fixTrade', type: 'boolean', hidden: true, viewNames: ['Export Friendly']},
						{header: 'Block', width: 20, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Roll', width: 20, dataIndex: 'tradeGroup.tradeGroupType.roll', type: 'boolean', hidden: true, tooltip: 'Trade is a roll trade (rolling an existing position to a new position of a different security). The trade is a member of a Trade Group configured with the Roll property.'},
						{header: 'Import', width: 20, dataIndex: 'tradeGroup.tradeGroupType.tradeImport', type: 'boolean', hidden: true, tooltip: 'Trade is the result of an imported file. The trade is a member of a Trade Group configured with the Trade Import property.'},
						{header: 'Destination', width: 26, dataIndex: 'tradeDestination.name', idDataIndex: 'tradeDestination.id', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}, viewNames: ['Default View', 'Export Friendly']},
						{header: 'Source', width: 26, dataIndex: 'tradeSource.name', idDataIndex: 'tradeSource.id', hidden: true, filter: {searchFieldName: 'tradeSourceId', type: 'combo', url: 'tradeSourceListFind.json'}, tooltip: 'Defines the optional source the trade originated from (e.g. client specific window, external source, etc.). If blank, the trade was created in IMS with no specific source.'},
						{header: 'Execution Type', width: 23, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}, viewNames: ['Default View', 'Export Friendly']},
						{header: 'Limit Price', width: 20, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
						{header: 'Time In Force', width: 23, dataIndex: 'tradeTimeInForceType.label', filter: {type: 'combo', searchFieldName: 'tradeTimeInForceTypeId', displayField: 'label', url: 'tradeTimeInForceTypeListFind.json'}, hidden: true, viewNames: ['Export Friendly'], tooltip: 'A future date which represents the last day the order will remain open while waiting for a fill.'},
						{header: 'Expires', width: 23, dataIndex: 'timeInForceExpirationDate', hidden: true, viewNames: ['Export Friendly'], tooltip: 'A future date which represents the last day the order will remain open while waiting for a fill.'},
						{header: 'External Identifier', width: 13, dataIndex: 'externalTradeIdentifier', hidden: true},
						{header: 'Traded On', width: 23, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc', viewNames: ['Export Friendly', 'Westport Trading', 'Westport Operations']},
						{header: 'Settled On', width: 23, dataIndex: 'settlementDate', hidden: true, viewNames: ['Export Friendly']}
					],
					switchToViewBeforeReload: function(viewName) {
						const toolbar = this.getTopToolbar();

						if (viewName === 'Westport Operations') {
							this.setFilterValue('workflowState.name', 'Booked', true, true);

						}
						else {
							this.clearFilter('workflowState.name', true);

						}
						if (viewName === 'Westport Trading' || viewName === 'Westport Operations') {

							return TCG.data.getDataPromise('investmentAccountGroupByName.json?name=Client Accounts Managed by Westport', this)
								.then(clientAccountGroup => {
									if (TCG.isNotNull(clientAccountGroup)) {
										TCG.getChildByName(toolbar, 'clientGroupName').setValue({value: clientAccountGroup.id, text: clientAccountGroup.name});
									}
								});
						}
						else {
							TCG.getChildByName(toolbar, 'clientGroupName').setValue({value: null, text: null});

						}
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 8 days of trades
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -8)});
						}

						const params = {readUncommittedRequested: true};
						return this.applyTopToolbarFilterLoadParams(params);
					},
					configureToolsMenu: function(menu) {
						const gridPanel = this;
						menu.add('-');
						menu.add({
							text: 'Preview Instruction Message',
							tooltip: 'Preview Instruction SWIFT Message',
							iconCls: 'shopping-cart',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								const fills = sm.getSelections();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single trade to preview.', 'Incorrect Selection');
								}
								else {
									const id = fills[0].id;
									Clifton.instruction.openInstructionPreview(id, 'Trade');
								}
							}
						});
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true,
						addEditButtons: function(toolBar, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

							const gridEditor = this;

							toolBar.add({
								text: 'Add Note',
								tooltip: 'Add a note linked to selected trades (NOTE: Maximum of 50 trades can be selected)',
								iconCls: 'pencil',
								scope: this,
								handler: function() {
									const grid = gridPanel.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one trade to add note to.', 'No Row(s) Selected');
									}
									else {
										const ut = sm.getSelections();
										gridEditor.addTradeNote(ut, gridEditor, gridPanel);
									}
								}
							});
							toolBar.add('-');
						},
						addTradeNote: function(rows, editor, gridPanel) {
							Clifton.trade.addTradeNote(rows, editor, gridPanel, 'id', 'tradeType.name', false, 50);
						}
					}
				}]
			},


			{
				title: 'Trade Fills',
				items: [{
					name: 'tradeFillListFind',
					xtype: 'trade-list-base-gridpanel',
					instructions: 'A list of all trade fills in the system.',
					viewNames: ['Default', 'Bonds', 'Export Friendly'],

					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Trade ID', width: 50, dataIndex: 'trade.id', type: 'int', filter: {searchFieldName: 'tradeId'}, hidden: true},
						{header: 'Workflow Status', width: 85, dataIndex: 'trade.workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade', showNotEquals: true}, hidden: true},
						{header: 'Workflow State', width: 85, dataIndex: 'trade.workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Trade Type', width: 85, dataIndex: 'trade.tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}},
						{header: 'Trade Group Type', width: 90, hidden: true, dataIndex: 'trade.tradeGroup.tradeGroupType.name', filter: {type: 'combo', searchFieldName: 'tradeGroupTypeId', url: 'tradeGroupTypeListFind.json'}},
						{header: 'Destination', width: 90, dataIndex: 'trade.tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}, hidden: true},
						{header: 'Account #', width: 70, dataIndex: 'trade.clientInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Client Account', width: 250, dataIndex: 'trade.clientInvestmentAccount.name', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 90, dataIndex: 'trade.holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
						{header: 'Holding Company', width: 150, dataIndex: 'trade.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Executing Broker', width: 150, dataIndex: 'trade.executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Executing Sponsor', width: 30, dataIndex: 'trade.executingSponsorCompany.name', filter: {type: 'combo', searchFieldName: 'executingSponsorCompanyId', url: 'businessCompanyListFind.json?companyType=Broker'}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Description', width: 200, dataIndex: 'trade.description', hidden: true},
						{
							header: 'Buy/Sell', width: 55, dataIndex: 'trade.buy', filter: {searchFieldName: 'buy'}, type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{
							header: 'Open/Close', width: 55, dataIndex: 'trade.openCloseType.label', hidden: true, filter: {type: 'combo', searchFieldName: 'tradeOpenCloseTypeId', url: 'tradeOpenCloseTypeListFind.json', loadAll: true}, viewNames: ['Export Friendly'],
							renderer: function(v, metaData) {
								metaData.css = v.includes('BUY') ? 'buy-light' : 'sell-light';
								return v;
							}
						},
						{header: 'Original Face', width: 80, dataIndex: 'trade.originalFace', type: 'float', useNull: true, hidden: true, viewNames: ['Bonds', 'Export Friendly']},
						{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float', useNull: true},
						{header: 'Notional Multiplier', width: 100, dataIndex: 'trade.notionalMultiplier', type: 'float', useNull: true, hidden: true, viewNames: ['Bonds', 'Export Friendly']},
						{header: 'Price Multiplier', width: 100, dataIndex: 'trade.investmentSecurity.priceMultiplier', type: 'float', hidden: true, filter: {searchFieldName: 'priceMultiplier'}},
						{header: 'Security', width: 130, dataIndex: 'trade.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', width: 130, dataIndex: 'trade.investmentSecurity.name'},
						{header: 'Investment Hierarchy', width: 200, dataIndex: 'trade.investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'CCY Denom', width: 100, dataIndex: 'trade.investmentSecurity.instrument.tradingCurrency.symbol', filter: {type: 'combo', searchFieldName: 'securityCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Exchange Rate', width: 90, dataIndex: 'trade.exchangeRateToBase', filter: {searchFieldName: 'exchangeRateToBase'}, type: 'float', useNull: true, hidden: true},
						{header: 'Fill Price', width: 90, dataIndex: 'notionalUnitPrice', type: 'float', useNull: true},
						{header: 'Fill Notional', width: 90, dataIndex: 'notionalTotalPrice', type: 'currency', useNull: true, hidden: true},
						{header: 'Fill Payment Price', width: 90, dataIndex: 'paymentUnitPrice', type: 'float', useNull: true, hidden: true},
						{header: 'Fill Payment', width: 90, dataIndex: 'paymentTotalPrice', type: 'currency', useNull: true, hidden: true},
						{header: 'Expected Price', width: 90, dataIndex: 'trade.expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
						{header: 'Accrual 1', width: 90, dataIndex: 'trade.accrualAmount1', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
						{header: 'Accrual 2', width: 90, dataIndex: 'trade.accrualAmount2', type: 'currency', hidden: true, useNull: true, viewNames: ['Export Friendly']},
						{header: 'Accrual', width: 90, dataIndex: 'trade.accrualAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['Bonds', 'Export Friendly']},
						{header: 'Commission Per Unit', width: 110, dataIndex: 'trade.commissionPerUnit', filter: {searchFieldName: 'tradeCommissionPerUnit'}, type: 'float', useNull: true},
						{header: 'Commission Amount', width: 110, dataIndex: 'trade.commissionAmount', filter: {searchFieldName: 'tradeCommissionAmount'}, type: 'currency', useNull: true, hidden: true, viewNames: ['Export Friendly'], tooltip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.'},
						{header: 'Fee Amount', width: 100, dataIndex: 'trade.feeAmount', filter: {searchFieldName: 'tradeFeeAmount'}, type: 'currency', useNull: true, hidden: true, viewNames: ['Export Friendly']},
						{header: 'Trader', width: 100, dataIndex: 'trade.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}, hidden: true},
						{header: 'Traded On', width: 80, dataIndex: 'trade.tradeDate', filter: {searchFieldName: 'tradeDate'}, defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Settled On', width: 80, dataIndex: 'trade.settlementDate', filter: {searchFieldName: 'settlementDate'}, useNull: true, hidden: true},
						{header: 'Opening Date', width: 90, dataIndex: 'openingDate', useNull: true, hidden: true},
						{header: 'Opening Price', width: 90, dataIndex: 'openingPrice', type: 'float', useNull: true, hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 8 days of trades
							this.setFilterValue('trade.tradeDate', {'after': new Date().add(Date.DAY, -8)});
						}

						return this.applyTopToolbarFilterLoadParams();
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.trade.id;
						}
					}
				}]
			},


			{
				title: 'Trade Groups',
				items: [{
					name: 'tradeGroupListFind',
					xtype: 'trade-list-base-gridpanel',
					instructions: 'Trade Groups are collections of trades executed for a purpose, i.e. Multi-Client, PIOS, Rolls, Tails',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Workflow Status', width: 12, dataIndex: 'workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade', showNotEquals: true}, hidden: true},
						{header: 'Workflow State', width: 12, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Group Type', width: 18, dataIndex: 'tradeGroupType.name', filter: {searchFieldName: 'tradeGroupTypeName'}},
						{header: 'Group Note', width: 80, dataIndex: 'note', filter: false},
						{header: 'Security', width: 20, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
						{header: '2nd Security', width: 20, dataIndex: 'secondaryInvestmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'secondaryInvestmentSecurityId', displayField: 'symbol', url: 'investmentSecurityListFind.json'}},
						{header: 'Trader', width: 13, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderUserId', displayField: 'label', url: 'securityUserListFind.json'}},
						{header: 'Traded On', width: 10, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Roll', width: 8, dataIndex: 'tradeGroupType.roll', type: 'boolean'},
						{header: 'Import', width: 8, dataIndex: 'tradeGroupType.tradeImport', type: 'boolean'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 8 days of trades
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -8)});
						}

						return this.applyTopToolbarFilterLoadParams({
							readUncommittedRequested: true
						});
					},
					editor: {
						detailPageClass: 'Clifton.trade.group.TradeGroupWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Trade Types',
				items: [{
					name: 'tradeTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all trade types. Different types of instruments may have different trade types and use different accounting booking rules.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 40, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{
							header: 'Investment Type', width: 35, dataIndex: 'investmentType.name',
							tooltip: 'Limits securities managed by this Trade Type to those of the specified Investment Type.'
						},
						{
							header: 'Instrument Group', width: 35, dataIndex: 'investmentGroup.name',
							tooltip: 'Optional Instrument Group that further limits securities for selected Investment Type.'
						},
						{
							header: 'Holding Account Purpose', width: 50, dataIndex: 'holdingAccountPurpose.name',
							tooltip: 'Restricts client/holding account selection to active relationships with this purpose.'
						},
						{
							header: 'Trade Report', width: 52, dataIndex: 'tradeReport.name', hidden: true,
							tooltip: 'Optional trade report defined for this trade type.  When defined, users can pull the report directly from the Trade Match screen.'
						},
						{
							header: 'Investment Specificity', width: 52, dataIndex: 'executionInvestmentSpecificityDefinition.name', hidden: true,
							tooltip: 'The specificity definition that will be used to retrieve the Freemark FpML of order execution via FIX and the InvestmentInstrumentHierarchy that will be used to create a new security if needed.'
						},

						{
							header: 'Commission GL Account', width: 50, dataIndex: 'commissionAccountingAccount.label', hidden: true,
							tooltip: 'When Booking trades of this type use this GL Account for Commission Amount field'
						},
						{
							header: 'Fee GL Account', width: 50, dataIndex: 'feeAccountingAccount.label', hidden: true,
							tooltip: 'When Booking trades of this type use this GL Account for Fee Amount field'
						},
						{
							header: 'Accrual GL Account (BUY)', width: 50, dataIndex: 'buyAccrualAccountingAccount.label', hidden: true,
							tooltip: 'Different GL Accounts maybe used for accrualAmount on buy vs sell trades. For example, Interest Income vs Interest Expense for bond trades. These fields are set only for trade types that support accruals.'
						},
						{
							header: 'Accrual GL Account (SELL)', width: 52, dataIndex: 'sellAccrualAccountingAccount.label', hidden: true,
							tooltip: 'Different GL Accounts maybe used for accrualAmount on buy vs sell trades. For example, Interest Income vs Interest Expense for bond trades. These fields are set only for trade types that support accruals.'
						},

						{
							header: 'FX Date Selector', dataIndex: 'exchangeRateDateSelector', width: 30,
							tooltip: 'Some securities may require Exchange Rate from a very specific date for foreign transactions (Trade Date for Cleared CDS).'
						},
						{
							header: 'Currency Convention', width: 25, dataIndex: 'currencyConventionFollowed', type: 'boolean',
							tooltip: 'Specifies whether trades of this type follow industry standard exchange rate currency convention. If true, then conversion from local currency to base currency will take into account the dominant currency before deciding whether local amount should be multiplied or divided by exchange rate. If false, then local currency amount is always multiplied by the exchange rate to get to base currency amount.'
						},
						{
							header: 'Single Fill', width: 25, dataIndex: 'singleFillTrade', type: 'boolean',
							tooltip: 'Trades of this type can only have one fill: no need to show Fills tab on Trade detail window.'
						},
						{
							header: 'Execution Type', width: 25, dataIndex: 'tradeExecutionTypeSupported', type: 'boolean',
							tooltip: 'Specifies whether trades of this type support execution type field: Limit, MOC, etc.'
						},
						{
							header: 'FIX', width: 25, dataIndex: 'fixSupported', type: 'boolean',
							tooltip: 'Specifies whether trades of this type support execution using FIX protocol.'
						},
						{
							header: 'Collateral Supported', width: 30, dataIndex: 'collateralSupported', type: 'boolean',
							tooltip: 'Specifies whether trades of this type can also be used as Collateral (bonds and stocks).'
						},
						{
							header: 'Broker Selection', width: 25, dataIndex: 'brokerSelectionAtExecutionAllowed', type: 'boolean',
							tooltip: 'Allow trades to be saved with no executing broker, and update it after execution is complete.'
						},
						{
							header: 'Holding Account Selection', width: 30, dataIndex: 'holdingAccountSelectionAtExecutionAllowed', type: 'boolean',
							tooltip: 'Allow trades to be saved with no holding account, and update it after execution is complete.'
						},
						{
							header: 'Security Creation', width: 30, dataIndex: 'securityCreationAtExecutionAllowed', type: 'boolean',
							tooltip: 'Allow trades to be saved with an invalid security, and then a new security is created after execution and the trades are updated.'
						},
						{
							header: 'Signed Amounts', width: 25, dataIndex: 'amountsSignAccountsForBuy', type: 'boolean',
							tooltip: 'Notional amounts is usually positive and its sign is determined by buy/sell field. If this option is checked, then buy/sell is ignored and positive amount indicates cash inflow while negative cash outflow.'
						},
						{
							header: 'Signed Accrual', width: 25, dataIndex: 'accrualSignAccountsForBuy', type: 'boolean',
							tooltip: 'Accrual amounts is usually positive and its sign is determined by buy/sell field. If this option is checked, then buy/sell is ignored and positive amount indicates cash inflow while negative cash outflow.'
						},
						{
							header: 'Crossing Zero', width: 25, dataIndex: 'crossingZeroAllowed', type: 'boolean',
							tooltip: 'Trades of this type are allow to take a position from short to long or vice versa.'
						},
						{
							header: 'Open/Close', width: 25, dataIndex: 'explicitOpenCloseSelectionAllowed', type: 'boolean',
							tooltip: 'Explicit Open/Close selection is allowed for trades of this type.'
						},
						{
							header: 'Open/Close Required', width: 25, dataIndex: 'explicitOpenCloseSelectionRequired', type: 'boolean',
							tooltip: 'Explicit Open/Close selection is required for trades of this type.'
						},
						{
							header: 'Execution Type Required', width: 25, dataIndex: 'executionTypeRequired', type: 'boolean',
							tooltip: 'A Trade Execution Type selection is required for trades of this type.'
						}

					],
					editor: {
						detailPageClass: 'Clifton.trade.TypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Trade Group Types',
				items: [{
					name: 'tradeGroupTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all trade group types.  Trades can be grouped together to allow common operations or just to identify their relationship.',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 45, dataIndex: 'name'},
						{header: 'Group Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Dynamic Calculator', width: 100, hidden: true, dataIndex: 'dynamicCalculatorBean.name'},
						{header: 'Detail Page', width: 100, dataIndex: 'detailScreenClass', tooltip: 'When set, the trade group uses a custom window'},
						{header: 'Source Table', width: 40, dataIndex: 'sourceSystemTable.name'},
						{header: 'Source Detail Page', width: 100, hidden: true, dataIndex: 'sourceDetailScreenClass'},
						{
							header: 'Dynamic Calculator Bean', width: 80, hidden: true, dataIndex: 'dynamicCalculatorBean.name',
							tooltip: 'An optional system bean used by a dynamic trade group to assist in reviewing client account positions and creating trades from those positions to include in a trade group.'
						},
						{header: 'Security Required', width: 30, dataIndex: 'securityRequired', type: 'boolean'},
						{header: 'Second Security Required', width: 35, dataIndex: 'secondarySecurityRequired', type: 'boolean'},
						{header: 'One Trade Allowed', width: 30, dataIndex: 'oneTradeAllowed', type: 'boolean'},
						{header: 'Parent Group Required', width: 30, dataIndex: 'parentGroupRequired', type: 'boolean'},
						{header: 'Workflow Required', width: 30, dataIndex: 'workflowRequired', type: 'boolean'},
						{header: 'Roll', width: 20, dataIndex: 'roll', type: 'boolean', tooltip: 'If checked, a trade group of this type contains roll trades, rolling an existing position to a new position of a different security'},
						{header: 'Import', width: 20, dataIndex: 'tradeImport', type: 'boolean', tooltip: 'If checked, a trade group of this type is the result of an imported file'},
						{
							header: 'Broker Selection at Execution Allowed', width: 40, dataIndex: 'brokerSelectionAtExecutionAllowed', hidden: true, type: 'boolean',
							tooltip: 'If checked, the trades of a trade group can avoid having a broker defined until execution. If unchecked, the broker must be defined on the trades according the rules enforced by the trade\'s type and destination.'
						},
						{
							header: 'Empty Trade List Allowed', width: 40, dataIndex: 'emptyTradeListAllowed', hidden: true, type: 'boolean',
							tooltip: 'If checked, it is possible the trade group may contain no trades. This occurs when a parent trade group has its trades relocated to a child trade group.'
						},
						{
							header: 'Empty Trade List Allowed On Initial Save', width: 40, dataIndex: 'emptyTradeListOnInitialSaveAllowed', hidden: true, type: 'boolean',
							tooltip: 'If checked, it is possible the trade group is created containing no trades. This occurs when a hierarchical trade group is created with a child trade group containing trades.'
						},
						{
							header: 'Trade Asset Class Allocation Allowed', width: 40, dataIndex: 'tradeAssetClassAllocationAllowed', hidden: true, type: 'boolean',
							tooltip: 'If checked, a trade group of this type is capable of creating trade asset class position allocations for contract splitting in addition to trades.'
						}
					],
					editor: {
						detailPageClass: 'Clifton.trade.group.TradeGroupTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Trade Sources',
				items: [{
					name: 'tradeSourceListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all source that define where the trade originated. The source adds additional context to a trade that can change workflow and/or processing behavior applied to the trade.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Source Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'No Dual Approval', width: 50, dataIndex: 'bypassDualApprovalAllowed', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.trade.TradeSourceWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Trade Open/Close Types',
				items: [{
					name: 'tradeOpenCloseTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all possible trade open/close types. When possible, more specific open/close type should be used. This will result in additional validation during trade booking. Individual Trade Types can be configured to limit the choice to a sub-set of these types.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 50, dataIndex: 'name'},
						{header: 'Type Label', width: 50, dataIndex: 'label'},
						{header: 'Description', width: 100, dataIndex: 'description'},
						{header: 'Buy', width: 30, dataIndex: 'buy', type: 'boolean'},
						{header: 'Open', width: 30, dataIndex: 'open', type: 'boolean'},
						{header: 'Close', width: 30, dataIndex: 'close', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.trade.TradeOpenCloseTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Trade Execution Types',
				items: [{
					name: 'tradeExecutionTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all trade execution types. Trade execution types are optional but can be used to identify trades with certain characteristics such as Limit Trades or MOC trades.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 40, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Required Holding Account Type', width: 40, dataIndex: 'requiredHoldingInvestmentAccountType.name'},
						{header: 'Exclude Holding Account Type', width: 40, dataIndex: 'excludeHoldingInvestmentAccountType.name'},
						{header: 'Executing Broker Filter', width: 50, dataIndex: 'executingBrokerFilter.name'},
						{header: 'MOC Trade', width: 18, dataIndex: 'marketOnCloseTrade', type: 'boolean'},
						{header: 'BTIC Trade', width: 18, dataIndex: 'bticTrade', type: 'boolean'},
						{header: 'CLS Trade', width: 18, dataIndex: 'clsTrade', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.trade.execution.ExecutionTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Trade Time In Force Types',
				items: [{
					name: 'tradeTimeInForceTypeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all trade time-in-force (TIF) types which determine how long an order may remain open when waiting for a fill. The default TIF for all trades is DAY.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 12, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 40, dataIndex: 'name'},
						{header: 'Type Label', width: 40, dataIndex: 'label'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Requires Expiration Date', width: 30, dataIndex: 'expirationDateRequired', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.trade.execution.TradeTimeInForceTypeWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Trade Instructions',
				items: [{
					xtype: 'investment-instruction-grid',
					tableName: 'Trade',
					defaultCategoryName: 'Trade Counterparty',
					defaultViewName: 'Group by Recipient'
				}]
			},


			{
				title: 'Trade Market Data Fields',
				items: [{
					xtype: 'gridpanel',
					name: 'tradeMarketDataFieldListFind',
					instructions: 'Trade Market Data Fields are mapped market data fields that will be captured at different stages in the lifecycle of each trade (workflow transition actions). These market data fields are captured using the security that is being traded. For example, a market price could be recorded on trade creation as well as on trade execution, or an option delta value could be recorded on trade validation. Each Trade Market Data Field will only apply to trades that match the scope defined for the field.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Field', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Market Data Field', width: 100, dataIndex: 'marketDataField.name', filter: {type: 'combo', searchFieldName: 'marketDataFieldId', url: 'marketDataFieldListFind.json'}},
						{header: 'Data Source', width: 50, dataIndex: 'marketDataSource.name', filter: {type: 'combo', searchFieldName: 'marketDataSourceId', url: 'marketDataSourceListFind.json'}},
						{header: 'Value Read Condition', dataIndex: 'valueReadCondition.name', width: 150, filter: {type: 'combo', searchFieldName: 'valueReadConditionId', url: 'systemConditionListFind.json'}},
						{header: 'Modify Condition', width: 110, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyCondition'}},
						{header: 'Workflow End State', width: 60, dataIndex: 'tradeWorkflowEndState.name', filter: {type: 'combo', searchFieldName: 'tradeWorkflowEndStateId', url: 'workflowStateListByWorkflowName.json?workflowName=Trade Workflow', root: 'data'}},
						{header: 'Underlying Security', width: 60, dataIndex: 'underlyingSecurity', type: 'boolean'},
						{header: 'Synchronous', width: 40, dataIndex: 'synchronous', type: 'boolean'},
						{header: 'Retrieve Once', width: 40, dataIndex: 'onlyRetrieveOnce', type: 'boolean'},
						{header: 'Active', width: 40, dataIndex: 'active', type: 'boolean'},
						{header: 'System', width: 40, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.trade.marketdata.TradeMarketDataFieldWindow'
					}
				}]
			},


			{
				title: 'Trade Market Data',
				items: [{
					xtype: 'trade-market-data-value-grid',
					editor: {
						detailPageClass: 'Clifton.trade.marketdata.TradeMarketDataValueWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});


Clifton.trade.TradeListWindow.BaseFilterGridPanel = Ext.extend(TCG.grid.GridPanel, {
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
			{fieldLabel: 'Client Acct Group', name: 'clientGroupName', hiddenName: 'clientInvestmentAccountGroupId', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
			{fieldLabel: 'Holding Acct Group', name: 'holdingGroupName', hiddenName: 'holdingInvestmentAccountGroupId', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'}
		];
	},

	applyTopToolbarFilterLoadParams: function(argParams) {
		const params = argParams || {};
		const t = this.getTopToolbar();
		let grp = TCG.getChildByName(t, 'clientGroupName');
		if (TCG.isNotBlank(grp.getValue())) {
			params.clientInvestmentAccountGroupId = grp.getValue();
		}
		grp = TCG.getChildByName(t, 'holdingGroupName');
		if (TCG.isNotBlank(grp.getValue())) {
			params.holdingInvestmentAccountGroupId = grp.getValue();
		}
		grp = TCG.getChildByName(t, 'investmentGroupId');
		if (TCG.isNotBlank(grp.getValue())) {
			params.investmentGroupId = grp.getValue();
		}
		return params;
	}
});
Ext.reg('trade-list-base-gridpanel', Clifton.trade.TradeListWindow.BaseFilterGridPanel);
