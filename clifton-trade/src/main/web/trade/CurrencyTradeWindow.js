Clifton.trade.CurrencyTradeWindow = Ext.extend(TCG.app.DetailWindow, {
	width: 1000,
	height: 500,
	iconCls: 'currency',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,

		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.trade.DefaultTradeWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.trade.DefaultTradeWindowAdditionalTabs[i]);
				}
			}
		},

		items: [
			{
				title: 'Info',
				tbar: {xtype: 'trade-workflow-toolbar'},

				items: [{
					xtype: 'trade-form-base',
					labelWidth: 95,
					instructions: 'Currency trade is exchange of base currency (buy) for non-base currency at specific exchange rate or vice versa (sell).',

					listeners: {
						beforerender: function() {
							const status = TCG.getValue('workflowStatus.name', this.getWindow().defaultData);
							this.readOnlyMode = (status !== 'Draft');
							if (this.readOnlyMode) {
								this.readOnlyMode = (this.getLoadURL() !== false);
							}
							this.add(this.readOnlyMode ? this.readOnlyItems : this.updatableItems);
						},
						afterload: function() {
							const fp = this;
							const ro = ('Draft' !== this.getFormValue('workflowStatus.name'));
							fp.setReadOnlyMode(ro);

							fp.updateCurrentBalance();
							fp.updateCurrentBalance('settleCurrencyBalance', 'payingSecurity');
							fp.updatePayingNotional();
						}
					},

					getDefaultData: function(win) {
						const fp = this;
						const f = fp.getForm();
						let dd = Ext.apply(win.defaultData || {}, {});
						if (win.defaultDataIsReal) {
							return dd;
						}
						const tradeTypeName = win.tradeType;

						return TCG.data.getDataPromiseUsingCaching('tradeDestinationByName.json?name=FX Connect Ticket', fp, 'trade.tradeDestination.FX Connect Ticket')
							.then(function(tradeDestination) {
								dd.tradeDestination = tradeDestination;
								return TCG.data.getDataPromiseUsingCaching('tradeTypeByName.json?name=' + tradeTypeName, fp, 'trade.type.' + tradeTypeName);
							})
							.then(function(tradeType) {
								dd = Ext.apply({
									tradeType: tradeType,
									executingBrokerCompany: tradeType.defaultExecutingBrokerCompany,
									fixTrade: tradeType.fixSupported,
									traderUser: TCG.getCurrentUser(),
									tradeDate: new Date().format('Y-m-d 00:00:00') // Default to Today
								}, dd);

								if (dd.clientInvestmentAccount && !dd.holdingInvestmentAccount) {
									// Need to trigger client account as selected so holding account selection is updated (and usually pre-selected when only one account applies)
									// The default data investmentSecurity is passed in to indicate that if the holding account can be auto selected, the current balance should be loaded
									f.addListener('afterRender', fp.updateHoldingAccountSelection.defer(300, fp, [dd.clientInvestmentAccount]));
								}

								if (dd.investmentSecurity) {
									return Clifton.investment.instrument.getTradeDatePromise(dd.investmentSecurity.id, null, fp)
										.then(function(tradeDate) {
											dd.tradeDate = tradeDate.format('Y-m-d 00:00:00');
											if (dd.payingSecurity) {
												return Clifton.investment.instrument.getSettlementDatePromise(dd.investmentSecurity.id, tradeDate.format('m/d/Y'), false, dd.payingSecurity.id, fp)
													.then(function(settlementDate) {
														dd.settlementDate = settlementDate.format('Y-m-d 00:00:00');
														return dd;
													});
											}
											return dd;
										});
								}

								return dd;
							});
					},

					afterRenderApplyAdditionalDefaultData: function(formPanel, formValues) {
						if (TCG.isNotNull(formValues.payingSecurity)) {
							formPanel.updateCurrentBalance('settleCurrencyBalance', 'payingSecurity');
						}
					},

					readOnlyMode: true,
					setReadOnlyMode: function(ro) {
						if (TCG.isEquals(this.readOnlyMode, ro)) {
							return;
						}
						this.readOnlyMode = ro;
						this.removeAll(true);
						this.add(ro ? this.readOnlyItems : this.updatableItems);
						try {
							this.doLayout();
						}
						catch (e) {
							// strange error due to removal of elements with allowBlank = false
						}
						const f = this.getForm();
						f.setValues(f.formValues);
						this.loadValidationMetaData(true);
						f.isValid();
					},

					updateCurrentBalance: function(balanceFieldName = 'currentBalance', securityFieldPrefix = 'investmentSecurity') {
						const formPanel = this;
						const f = formPanel.getForm();
						let date = f.findField('tradeDate').value;
						if (TCG.isBlank(date)) {
							date = new Date().format('m/d/Y');
						}
						const securityId = f.findField(securityFieldPrefix + '.id').getValue();
						if (TCG.isNotBlank(securityId)) {
							TCG.data.getDataValuePromise('accountingBalanceForSecurityLocal.json?investmentSecurityId=' + securityId + '&clientInvestmentAccountId=' + f.findField('clientInvestmentAccount.id').getValue() + '&holdingInvestmentAccountId=' + f.findField('holdingInvestmentAccount.id').getValue() + '&transactionDate=' + date, this)
								.then(function(b) {
									const o = f.findField(balanceFieldName);
									o.setValue(b);
									o.originalValue = o.getValue();
									let label = 'Balance:';
									const currencySymbol = formPanel.getSymbolForCurrencyFieldPrefix(securityFieldPrefix);
									if (TCG.isNotBlank(currencySymbol)) {
										label = currencySymbol + ' Balance:';
									}
									o.setFieldLabel(label);
								});
						}
					},

					updatePayingNotional: function() {
						const fp = this;
						const f = fp.getForm();
						const v = f.findField('accountingNotional').getNumericValue();
						const fx = f.findField('exchangeRateToBase').getNumericValue();
						if (TCG.isNumber(v) && TCG.isNumber(fx)) {
							const to = f.findField('payingSecurity.label').getRawValue().substring(0, 3);
							const from = f.findField('investmentSecurity.label').getRawValue().substring(0, 3);
							TCG.data.getDataValuePromise('investmentCurrencyMultiplyConventionUsed.json?fromSymbol=' + from + '&toSymbol=' + to, this)
								.then(function(multiply) {
									fp.setFormValue('payingNotional', TCG.isTrue(multiply) ? v * fx : v / fx, true);
								});
						}
					},

					updateExchangeRate: async function() {
						const formPanel = this;
						const form = formPanel.getForm();
						const fxField = form.findField('exchangeRateToBase');
						const fromId = form.findField('investmentSecurity.id').getValue();
						const toId = form.findField('payingSecurity.id').getValue();
						if (TCG.isNumber(fromId) && TCG.isNumber(toId)) {
							const holdingAccountIssuingCompanyId = 0; //use default source, not all issuing/executing companies have values
							const settlementDate = form.findField('tradeDate').value || form.findField('settlementDate').value;
							const exchangeRate = await TCG.data.getDataPromise('marketDataExchangeRateForDateFlexible.json?companyId=' + holdingAccountIssuingCompanyId + '&fromCurrencyId=' + fromId + '&toCurrencyId=' + toId + '&rateDate=' + settlementDate, this)
								.then(rate => rate ? rate.exchangeRate : void 0);
							if (exchangeRate) {
								const fromSymbol = this.getSymbolForCurrencyFieldPrefix('investmentSecurity');
								const toSymbol = this.getSymbolForCurrencyFieldPrefix('payingSecurity');
								const isMultipleConvention = await TCG.data.getDataValuePromise(`investmentCurrencyMultiplyConventionUsed.json?fromSymbol=${fromSymbol}&toSymbol=${toSymbol}`, this);
								const rate = TCG.isTrue(isMultipleConvention) ? exchangeRate : 1 / exchangeRate;
								fxField.setValue(rate);
							}
						}
					},

					getSymbolForCurrencyFieldPrefix: function(currencyFieldPrefix) {
						const l = this.getForm().findField(currencyFieldPrefix + '.label').getRawValue();
						return (l.length > 2) ? l.substring(0, 3) : l;
					},

					updateHoldingAccountSelection: function(clientAccountRecord) {
						const f = this.getForm();
						const ha = f.findField('holdingInvestmentAccount.label');
						const cf = f.findField('collateralTrade');
						ha.store.removeAll();
						ha.lastQuery = null;

						ha.store.baseParams = {
							mainAccountId: clientAccountRecord.id,
							mainPurpose: this.getFormValue(cf && cf.getValue() ? 'tradeType.holdingAccountPurpose.collateralPurpose.name' : 'tradeType.holdingAccountPurpose.name'),
							ourAccount: false,
							workflowStatusNameEquals: 'Active',
							mainPurposeActiveOnDate: f.findField('tradeDate').value
						};
						ha.store.on('load', function(store, records) {
							if (records.length === 1) {
								ha.setValue(records[0].id);
								ha.beforeSelect(ha, records[0]);
							}
						});
						ha.doQuery('');
					},

					items: [],

					updatableItems: [
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {anchor: '-20'}},
							items: [
								{
									columnWidth: .71,
									items: [
										{
											fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', limitRequestedProperties: false, xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&workflowStatusNameEquals=Active', allowBlank: false, anchor: '-20',
											detailPageClass: 'Clifton.investment.account.AccountWindow',
											listeners: {
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;
													const fp = combo.getParentForm();
													combo.store.baseParams = {
														relatedPurpose: fp.getFormValue('tradeType.holdingAccountPurpose.name'),
														relatedPurposeActiveOnDate: fp.getForm().findField('tradeDate').value
													};
												},
												beforeselect: function(combo, record) {
													const fp = combo.getParentForm();
													const f = fp.getForm();
													const curr = record.json.baseCurrency;
													if (curr) {
														f.findField('payingSecurity.label').setValue(curr.label);
														f.findField('payingSecurity.id').setValue(curr.id);
														f.findField('clientInvestmentAccount.baseCurrency.label').setValue(curr.label);
														f.findField('clientInvestmentAccount.baseCurrency.id').setValue(curr.id);
													}
													// clear balance fields for validity; will be populated upon data entry
													f.findField('currentBalance').setValue(void 0);
													f.findField('settleCurrencyBalance').setValue(void 0);

													fp.updateHoldingAccountSelection(record);
												}
											}
										},
										{
											fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json', allowBlank: false, anchor: '-20', requiredFields: ['clientInvestmentAccount.label'],
											detailPageClass: 'Clifton.investment.account.AccountWindow',
											beforeSelect: function(combo, record) {
												const f = combo.getParentForm().getForm();
												const executingBrokerField = f.findField('executingBrokerCompany.label');
												const tradeDestinationId = f.findField('tradeDestination.name').getValue();
												const tradeTypeId = f.findField('tradeType.id').getValue();
												const executingBrokerCompanyId = (record.json ? record.json.issuingCompany.id : record.issuingCompany.id);
												let tradeDate = f.findField('tradeDate').getValue();
												if (TCG.isNotBlank(tradeDate)) {
													tradeDate = tradeDate.format('m/d/Y');
												}
												const curr = record.json.baseCurrency;
												if (curr) {
													f.findField('payingSecurity.label').setValue(curr.label);
													f.findField('payingSecurity.id').setValue(curr.id);
													f.findField('holdingInvestmentAccount.baseCurrency.label').setValue(curr.label);
													f.findField('holdingInvestmentAccount.baseCurrency.id').setValue(curr.id);
												}
												combo.updateCurrencyBalances.defer(200, combo, [combo]);

												executingBrokerField.store.removeAll();
												executingBrokerField.lastQuery = null;

												TCG.data.getDataPromise('tradeDestinationMappingListByCommandFind.json?executingBrokerCompanyId=' + executingBrokerCompanyId + '&tradeTypeId=' + tradeTypeId + '&tradeDestinationId=' + tradeDestinationId + '&activeOnDate=' + tradeDate, combo)
													.then(function(broker) {
														if (broker.length === 1) {
															const executingBrokerCompany = broker[0].executingBrokerCompany;
															const value = {
																text: executingBrokerCompany.label,
																value: executingBrokerCompany.id
															};
															executingBrokerField.setValue(value);
															f.formValues.executingBrokerCompany = executingBrokerCompany; // needs this in order to find the record when tabbing out when the store isn't initialized
														}
													});
											},
											updateCurrencyBalances: function(combo) {
												const fp = combo.getParentForm();
												fp.updateCurrentBalance.call(fp);
												fp.updateCurrentBalance.call(fp, 'settleCurrencyBalance', 'payingSecurity');
											},
											listeners: {
												beforeselect: function(combo, record) {
													combo.beforeSelect(combo, record);
												}
											}
										}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 60,
									items: [
										{fieldLabel: 'Base CCY', name: 'clientInvestmentAccount.baseCurrency.label', detailIdField: 'clientInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitDetailField: false},
										{fieldLabel: 'Base CCY', name: 'holdingInvestmentAccount.baseCurrency.label', detailIdField: 'holdingInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitDetailField: false}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
							items: [
								{
									columnWidth: .31,
									labelWidth: 85,
									items: [
										{
											xtype: 'radiogroup',
											fieldLabel: 'Buy/Sell',
											columns: [60, 60],
											allowBlank: false,
											anchor: '-25',
											items: [
												{boxLabel: 'Buy', name: 'buy', inputValue: true},
												{boxLabel: 'Sell', name: 'buy', inputValue: false}
											],
											listeners: {
												change: function(radioGroup, checkedRadio) {
													const formPanel = TCG.getParentFormPanel(radioGroup);
													const params = {
														tradeTypeId: formPanel.getFormValue('tradeType.id'),
														buy: checkedRadio.getRawValue()
													};
													TCG.data.getDataPromise('tradeOpenCloseTypeListFind.json', radioGroup, {params: params})
														.then(function(openCloseType) {
															if (openCloseType && openCloseType.length === 1) {
																formPanel.getForm().findField('openCloseType.id').setValue(openCloseType[0].id);
															}
														});
												}
											}
										},
										{xtype: 'hidden', name: 'openCloseType.id'},
										{
											fieldLabel: 'Given CCY', name: 'investmentSecurity.label', allowBlank: false, displayField: 'label', hiddenName: 'investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?currency=true', requiredFields: ['holdingInvestmentAccount.label'], doNotClearIfRequiredSet: true,
											listeners: {

												beforeQuery: function(qEvent) {
													const f = qEvent.combo.getParentForm().getForm();
													qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value};
												},

												select: function(combo, record, index) {
													const fp = combo.getParentForm();
													const form = fp.getForm();

													Clifton.investment.instrument.getTradeDatePromise(record.id, null, combo)
														.then(function(tradeDate) {
															form.findField('tradeDate').setValue(tradeDate);
															return Clifton.investment.instrument.getSettlementDatePromise(record.id, tradeDate, false, form.findField('payingSecurity.id').value, combo);
														})
														.then(function(settlementDate) {
															form.findField('settlementDate').setValue(settlementDate);
															fp.updateCurrentBalance.call(fp);
															fp.updateExchangeRate.call(fp);
														});
												}
											},

											getDetailPageClass: function(newInstance) {
												if (newInstance) {
													return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
												}
												return this.detailPageClass;
											},

											getDefaultData: function(f) { // defaults client/counterparty/isda
												let fp = TCG.getParentFormPanel(this);
												fp = fp.getForm();
												const ha = TCG.getValue('holdingInvestmentAccount', fp.formValues);
												const investmentGroupId = TCG.getValue('tradeType.investmentGroup.id', fp.formValues);
												const investmentTypeId = TCG.getValue('tradeType.investmentType.id', fp.formValues);

												return {
													businessCompany: (ha ? ha.issuingCompany : ''),
													businessContract: (ha ? ha.businessContract : ''),
													investmentGroupId: investmentGroupId,
													investmentTypeId: investmentTypeId
												};
											}
										},
										{
											fieldLabel: 'Given Amount', name: 'accountingNotional', xtype: 'currencyfield', allowBlank: false, minValue: 0,
											listeners: {
												change: function(field, newValue, oldValue) {
													const fp = TCG.getParentFormPanel(field);
													fp.updateExchangeRate.call(fp); // exchange rate update will update paying notional
												}
											}
										},
										{
											fieldLabel: 'Settle CCY', name: 'payingSecurity.label', hiddenName: 'payingSecurity.id', allowBlank: false, displayField: 'label', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json?currency=true', requiredFields: ['holdingInvestmentAccount.label'], doNotClearIfRequiredSet: true,
											listeners: {
												beforeQuery: function(qEvent) {
													const f = qEvent.combo.getParentForm().getForm();
													qEvent.combo.store.baseParams = {activeOnDate: f.findField('tradeDate').value};
												},
												select: function(combo, record, index) {
													const fp = combo.getParentForm();
													const form = fp.getForm();

													Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, form.findField('tradeDate').value, false, record.id, combo)
														.then(function(settlementDate) {
															form.findField('settlementDate').setValue(settlementDate);
															fp.updateCurrentBalance.call(fp, 'settleCurrencyBalance', 'payingSecurity');
															fp.updateExchangeRate.call(fp);
														});
												}
											},

											getDetailPageClass: function(newInstance) {
												if (newInstance) {
													return 'Clifton.investment.instrument.copy.SecurityCopyWindow';
												}
												return this.detailPageClass;
											},

											getDefaultData: function(f) { // defaults client/counterparty/isda
												let fp = TCG.getParentFormPanel(this);
												fp = fp.getForm();
												const ha = TCG.getValue('holdingInvestmentAccount', fp.formValues);
												const investmentGroupId = TCG.getValue('tradeType.investmentGroup.id', fp.formValues);
												const investmentTypeId = TCG.getValue('tradeType.investmentType.id', fp.formValues);

												return {
													businessCompany: (ha ? ha.issuingCompany : ''),
													businessContract: (ha ? ha.businessContract : ''),
													investmentGroupId: investmentGroupId,
													investmentTypeId: investmentTypeId
												};
											}
										},
										{fieldLabel: 'Settle Amount', name: 'payingNotional', xtype: 'currencyfield', readOnly: true}
									]
								},
								{
									columnWidth: .40,
									labelWidth: 90,
									items: [
										{
											fieldLabel: 'Broker', name: 'executingBrokerCompany.label', hiddenName: 'executingBrokerCompany.id', xtype: 'combo',
											requiredFields: ['holdingInvestmentAccount.label'],
											displayField: 'label',
											//valueField: 'executingBrokerCompany.id',
											//tooltipField: 'executingBrokerCompany.description',
											queryParam: 'executingBrokerCompanyName',
											detailPageClass: 'Clifton.business.company.CompanyWindow',
											url: 'tradeExecutingBrokerCompanyListFind.json',
											allowBlank: true,
											listeners: {
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;
													const f = combo.getParentForm().getForm();
													combo.store.baseParams = {
														tradeDestinationId: f.findField('tradeDestination.name').getValue(),
														tradeTypeId: f.findField('tradeType.id').getValue(),
														activeOnDate: f.findField('tradeDate').value
													};
												}
											}
										},
										{
											fieldLabel: 'Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json', allowBlank: true,
											listeners: {
												beforequery: function(queryEvent) {
													const combo = queryEvent.combo;
													const f = combo.getParentForm().getForm();
													combo.store.baseParams = {
														tradeTypeId: f.findField('tradeType.id').getValue(),
														activeOnDate: f.findField('tradeDate').value
													};
												},
												TODO_select: function(combo, record, index) {
													const fp = combo.getParentForm();
													// clear existing destinations
													const dst = fp.getForm().findField('executingBrokerCompany.label');
													dst.store.removeAll();
													dst.lastQuery = null;
													fp.updateExecutingBrokerCompany(record.id);
												}
											}
										},
										{
											fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', allowBlank: true, tooltip: 'Currency exchange rates should always be the exchange rate between the dominant currency to the other, which is the industry standard convention. For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency',
											listeners: {
												valid: function(field) {
													const fp = TCG.getParentFormPanel(field);
													fp.updatePayingNotional.call(fp);
												}
											}
										},
										{xtype: 'trade-currency-balance-field'},
										{name: 'settleCurrencyBalance', securityFieldPrefix: 'payingSecurity', xtype: 'trade-currency-balance-field'}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 100,
									items: [
										{name: 'tradeType.id', xtype: 'hidden'},
										{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{
											fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', allowBlank: false,
											getParentFormPanel: function() {
												const parent = TCG.getParentFormPanel(this);
												return parent || this.ownerCt;
											},
											listeners: {
												change: function(field, newValue, oldValue) {
													const form = field.getParentFormPanel().getForm();
													const settlementDateField = form.findField('settlementDate');

													if (TCG.isBlank(newValue)) {
														settlementDateField.setValue('');
													}
													else {
														Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, newValue, false, form.findField('payingSecurity.id').value, field)
															.then(function(settlementDate) {
																form.findField('settlementDate').setValue(settlementDate);
															});
													}
												},
												select: function(field, date) {
													const form = field.getParentFormPanel().getForm();
													Clifton.investment.instrument.getSettlementDatePromise(form.findField('investmentSecurity.id').value, date, false, form.findField('payingSecurity.id').value, field)
														.then(function(settlementDate) {
															form.findField('settlementDate').setValue(settlementDate);
														});
												}
											}
										},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', allowBlank: false},
										{xtype: 'trade-bookingdate'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop'}
					],


					readOnlyItems: [
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {anchor: '-20'}},
							items: [
								{
									columnWidth: .71,
									items: [
										{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'clientInvestmentAccount.id'},
										{fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'holdingInvestmentAccount.id'}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 60,
									items: [
										{fieldLabel: 'Base CCY', name: 'clientInvestmentAccount.baseCurrency.label', detailIdField: 'clientInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false},
										{fieldLabel: 'Base CCY', name: 'holdingInvestmentAccount.baseCurrency.label', detailIdField: 'holdingInvestmentAccount.baseCurrency.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitValue: false}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel',
							layout: 'column',
							defaults: {layout: 'form', defaults: {xtype: 'textfield', anchor: '-20'}},
							items: [
								{
									columnWidth: .31,
									labelWidth: 85,
									items: [
										{
											fieldLabel: 'Buy/Sell', name: 'buy', xtype: 'displayfield', width: 140,
											setRawValue: function(v) {
												this.el.addClass(v ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
											}
										},
										{fieldLabel: 'Given CCY', name: 'investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'investmentSecurity.id'},
										{fieldLabel: 'Given Amount', name: 'accountingNotional', xtype: 'currencyfield', readOnly: true},
										{fieldLabel: 'Settle CCY', name: 'payingSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'payingSecurity.id'},
										{fieldLabel: 'Settle Amount', name: 'payingNotional', xtype: 'currencyfield', readOnly: true}
									]
								},
								{
									columnWidth: .40,
									labelWidth: 90,
									items: [
										{fieldLabel: 'Broker', name: 'executingBrokerCompany.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'executingBrokerCompany.id'},
										{fieldLabel: 'Destination', xtype: 'trade-destination-link'},
										{fieldLabel: 'Exchange Rate', name: 'exchangeRateToBase', xtype: 'floatfield', readOnly: true, tooltip: 'Currency exchange rates should always be the exchange rate between the dominant currency to the other, which is the industry standard convention. For example, USD -> GBP and GBP -> USD will both use the GBP -> USD exchange rate because GBP is the dominant currency'},
										{xtype: 'trade-currency-balance-field'},
										{name: 'settleCurrencyBalance', securityFieldPrefix: 'payingSecurity', xtype: 'trade-currency-balance-field'}
									]
								},
								{
									columnWidth: .29,
									labelWidth: 100,
									items: [
										{name: 'tradeType.id', xtype: 'hidden'},
										{fieldLabel: 'Trade Group', name: 'tradeGroup.tradeGroupType.name', xtype: 'linkfield', detailIdField: 'tradeGroup.id', detailPageClass: 'Clifton.trade.group.TradeGroupWindow'},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true},
										{fieldLabel: 'Settlement Date', name: 'settlementDate', xtype: 'datefield', readOnly: true},
										{xtype: 'trade-bookingdate'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{xtype: 'trade-descriptionWithNoteDragDrop'}
					]
				}]
			},


			{
				title: 'Compliance',
				items: [{
					xtype: 'trade-violations-grid'
				}]
			},


			{
				title: 'Commissions and Fees',
				items: [{
					xtype: 'trade-commissions'
				}]
			},


			{
				title: 'Quotes',
				items: [{
					xtype: 'trade-quotes',
					hideAdditionalAmounts: true
				}]
			}
		]
	}]
});


Clifton.trade.CurrencyCurrentBalanceField = Ext.extend(TCG.form.LinkField, {
	fieldLabel: 'Balance',
	name: 'currentBalance',
	xtype: 'linkfield',
	type: 'currency',
	detailPageClass: 'Clifton.accounting.gl.TransactionListWindow',
	securityFieldPrefix: 'investmentSecurity',
	getDefaultData: function(fp) {
		const f = fp.getForm();
		const filters = [];
		filters.push({
			field: 'clientInvestmentAccountId',
			comparison: 'EQUALS',
			value: f.findField('clientInvestmentAccount.id').getValue()
		});
		filters.push({
			field: 'holdingInvestmentAccountId',
			comparison: 'EQUALS',
			value: f.findField('holdingInvestmentAccount.id').getValue()
		});
		filters.push({
			field: 'investmentSecurityId',
			comparison: 'EQUALS',
			value: f.findField(this.securityFieldPrefix + '.id').getValue()
		});
		filters.push({
			field: 'transactionDate',
			comparison: 'LESS_THAN',
			value: f.findField('tradeDate').value
		});
		filters.forEach(filter => {
			if (TCG.isBlank(filter.value)) {
				return false;
			}
		});
		return filters;
	}
});
Ext.reg('trade-currency-balance-field', Clifton.trade.CurrencyCurrentBalanceField);

