Clifton.trade.destination.TradeDestinationTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Destination Type',
	iconCls: 'shopping-cart',
	height: 575,
	width: 750,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{

					xtype: 'formpanel',
					url: 'tradeDestinationType.json',
					labelWidth: 125,
					readOnly: true,
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

						{boxLabel: 'Manual Destination', name: 'manual', xtype: 'checkbox'},
						{boxLabel: 'FIX Trade Only', name: 'fixTradeOnly', xtype: 'checkbox'},
						{boxLabel: 'Create Order', name: 'createOrder', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Trade Destinations',
				items: [{
					name: 'tradeDestinationListFind',
					xtype: 'gridpanel',
					instructions: 'A list of Trade Destinations for this type.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Destination Name', width: 120, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Data Source', width: 90, dataIndex: 'marketDataSource.name', filter: {searchFieldName: 'marketDataSourceId', type: 'combo', url: 'marketDataSourceListFind.json'}},
						{header: 'Data Source Purpose', width: 100, dataIndex: 'marketDataSourcePurpose.name', filter: {searchFieldName: 'marketDataSourcePurposeId', type: 'combo', url: 'marketDataSourcePurposeListFind.json'}},
						{header: 'Connection', width: 120, dataIndex: 'connection.name', hidden: true, filter: {searchFieldName: 'connectionId', type: 'combo', url: 'tradeDestinationConnectionListFind.json'}},
						{header: 'FIX Target', width: 70, dataIndex: 'fixTargetSubId', hidden: true},
						{header: 'FIX Handling Instructions', width: 100, dataIndex: 'fixHandlingInstructions', hidden: true},
						{header: 'Modify Condition', width: 90, dataIndex: 'tradeModifySystemCondition.name', hidden: true, filter: {searchFieldName: 'tradeModifySystemConditionName'}},
						{header: 'Order', width: 50, dataIndex: 'destinationOrder', type: 'int', defaultSortColumn: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {destinationTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow'
					}
				}]
			}
		]
	}]
});
