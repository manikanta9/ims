Clifton.trade.destination.TradeDestinationMappingConfigurationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Destination Mapping Configuration',
	iconCls: 'shopping-cart',
	width: 950,
	height: 850,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Defines the settings for a Trade Destination Mapping Configuration, which is used to aggregate common setting combinations for use in Trade Destination Mappings.',
					url: 'tradeDestinationMappingConfiguration.json',
					labelWidth: 150,

					items: [
						{fieldLabel: 'Configuration Name', name: 'name', defaultSortColumn: 'true'},
						{fieldLabel: 'Configuration Description', name: 'description', xtype: 'textarea'},

						{xtype: 'sectionheaderfield', header: 'Order Execution Configuration'},
						{boxLabel: 'Allow trades to be grouped for FIX execution (Buy/Sell, Security, Executing Broker, & Trader)', name: 'fixTradeGroupingAllowed', xtype: 'checkbox', qtip: 'When creating orders the trades will be grouped by direction (buy/sell), security, executing broker, trader.'},
						{boxLabel: 'Group trades by holding accounts for FIX execution', name: 'fixTradeGroupingByHoldingAccount', xtype: 'checkbox', qtip: 'Group trades on holding account.', requiredFields: ['fixTradeGroupingAllowed'], disabled: true},
						{boxLabel: 'Send exchange MIC code on FIX ORDER messages', name: 'fixMICExchangeCodeRequiredOnOrderMessages', xtype: 'checkbox', qtip: 'Send security exchange code in tag 207 on order messages. Uses code from Exchange Mappings if it exists otherwise uses the exchangeCode on the Investment Exchange.'},
						{boxLabel: 'Send the client account number on the order (this will override any Top Account that is set on the mapping).', name: 'sendClientAccountWithOrder', xtype: 'checkbox'},
						{boxLabel: 'Restrict executing brokers using destination mappings and compliance rules when no Executing Broker is selected.', name: 'sendRestrictiveBrokerList', xtype: 'checkbox'},
						{boxLabel: 'Restrict executing brokers to issuers of related holding accounts.', name: 'brokerRestrictedToRelatedHoldingAccountList', xtype: 'checkbox'},
						{boxLabel: 'Search the company subsidiary hierarchy for the matching executing broker', name: 'searchCompanyHierarchyForBrokerMatch', xtype: 'checkbox', qtip: 'Search for the executing broker based on the issuer of the holding account being a subsidiary at any level of the mapped company.'},
						{boxLabel: 'Send symbol from the investment security in FIX Order message for equity orders.', name: 'fixUseSymbolForEquityOrders', xtype: 'checkbox'},


						{xtype: 'sectionheaderfield', header: 'Allocation Configuration'},
						{fieldLabel: 'FIX Allocation Type', qtip: 'The allocation type that will be sent on the allocation instruction.', xtype: 'combo', name: 'fixAllocationType', hiddenName: 'fixAllocationType', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.AllocationTypes}},
						{boxLabel: 'Trades are allocated via FIX after execution', name: 'fixPostTradeAllocation', xtype: 'checkbox', value: true},
						{boxLabel: 'Disable Pre-Trade Allocations', name: 'fixDisablePreTradeAllocations', xtype: 'checkbox', qtip: 'Disable pre-trade allocations from being included in the order.  Pre-trade allocations are automatically enabled if the "Trades are allocated via FIX after execution" flag is unchecked.  This flag will provide the ability to disable pre-trade allocations.'},
						{boxLabel: 'Use allocation report to create trade fills and book', name: 'fixAllocationReportExpected', xtype: 'checkbox', value: true, mutuallyExclusiveFields: ['fixAllocationCompleteWhenStatusIsReceived']},
						{boxLabel: 'When an allocation report is expected, this flag will book the trades with average execution price not the allocation report price', name: 'fixForceExecutionAveragePriceWithAllocationReport', xtype: 'checkbox'},
						{boxLabel: 'Allocation is complete when it has a status of [RECEIVED] ', name: 'fixAllocationCompleteWhenStatusIsReceived', xtype: 'checkbox', mutuallyExclusiveFields: ['fixAllocationReportExpected']},
						{boxLabel: 'Trades are allocated to an executing account', name: 'fixExecutingAccountUsed', xtype: 'checkbox', qtip: 'Indicates the trade will be allocated to an execution account.  This requires that the holding account has a related execution account.  For example, for Stocks, in order to execute the holding account needs to be related to a custodian account with purpose "Executing Stocks"'},
						{boxLabel: 'Include the order quantity for each order in the AllocationInstruction NoOrders group of FIX ALLOCATION messages', name: 'fixAllocationOrderQuantityRequiredInNoOrdersGroup', xtype: 'checkbox', qtip: 'If true, calculates and includes the order quantity for each order in the AllocationInstruction NoOrders group.'},
						{boxLabel: 'Include allocation price in allocation details section of FIX ALLOCATION messages', name: 'fixAllocationPriceRequiredInAllocationDetails', xtype: 'checkbox', qtip: 'Include the allocation price in (tag 366) in the allocation details section on allocation messages.  This price will be the equivalent of the average price of the allocation.'},
						{boxLabel: 'Send exchange currency code on FIX ALLOCATION messages', name: 'fixCurrencyCodeRequiredOnAllocationMessages', xtype: 'checkbox', qtip: 'Send currency code in tag 15 on allocation messages.  Uses security symbol code from Paying Security of the Trade.'},
						{boxLabel: 'Send exchange MIC code on FIX ALLOCATION messages', name: 'fixMICExchangeCodeRequiredOnAllocationMessages', xtype: 'checkbox', qtip: 'Send security exchange code in tag 207 on allocation messages.  Uses code from Exchange Mappings if it exists otherwise uses the exchangeCode on the Investment Exchange.'},
						{boxLabel: 'Send exchange CFI code on FIX ALLOCATION messages', name: 'fixCFICodeRequiredOnAllocationMessages', xtype: 'checkbox', qtip: 'Send security CFI code in tag 461 on allocation messages.'},
						{boxLabel: 'Send SecondaryClOrdID instead of our ClOrderID on FIX allocation messages', name: 'fixSecondaryClientOrderIdUsedForAllocations', xtype: 'checkbox', qtip: 'Indicates that we should send SecondaryClOrdID (tag 526) from the execution reports instead of our ClOrdID from tag 11.  NOTE: This is currently used for allocations with CITI via REDI.'},
						{boxLabel: 'Send the allocation top account on the allocation message.', name: 'fixSendTopAccountOnAllocationMessages', xtype: 'checkbox', qtip: 'Send the post trade allocation top account number on the allocation message and not the order.'},
						{boxLabel: 'Send Party IDs of the clearing account on fix allocation messages.', name: 'fixSendBrokerPartyIdsOnAllocationMessages', xtype: 'checkbox', qtip: 'When this is true, each allocation on the fix message will have a party id that indicates who the clearing broker account is.'},
						{boxLabel: 'Send the LEI of the clearing firm with pre and post trade allocations.', name: 'fixSendClearingFirmLEIWithAllocation', xtype: 'checkbox', qtip: 'If true, we will send a nested party id with the LEI of the issuing company of the holding account. Currently used for sending pre-trade allocations to Bloomberg for Cleared CDS/IRS traded on the SEF.'},


						{xtype: 'sectionheaderfield', header: 'Commissions Configuration'},
						{fieldLabel: 'FIX Commission Type', qtip: 'The commission type that will be sent on the allocation instruction.', xtype: 'combo', name: 'fixCommissionType', hiddenName: 'fixCommissionType', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.CommissionTypes}},
						{fieldLabel: 'FIX Commission Process', qtip: 'The commission processing code that will be sent on the allocation instruction.', xtype: 'combo', name: 'fixProcessCode', hiddenName: 'fixProcessCode', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.ProcessCodes}},
						{fieldLabel: 'FIX Default Commission', name: 'fixDefaultCommissionPerUnit', qtip: 'The default commission per unit that will be sent on the allocation.'}
					]
				}]
			},


			{
				title: 'Destination Mappings',
				items: [{
					name: 'tradeDestinationMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Defines allowed trade destinations and executing brokers for each trade type. If Instrument Group is selected, then only securities in that group will be mapped.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Type', width: 40, dataIndex: 'tradeType.name', filter: {searchFieldName: 'tradeTypeName'}, defaultSortColumn: true},
						{header: 'Trade Destination', width: 40, dataIndex: 'tradeDestination.name', filter: {searchFieldName: 'tradeDestinationName'}},
						{header: 'Executing Broker', width: 75, dataIndex: 'executingBrokerCompany.name', filter: {searchFieldName: 'executingBrokerCompanyName'}},
						{header: 'Override Broker Code', width: 45, dataIndex: 'overrideBrokerCode', filter: {searchFieldName: 'overrideBrokerCode'}},
						{header: 'Instrument Group', width: 75, dataIndex: 'defaultInvestmentGroup.name', filter: {searchFieldName: 'defaultInvestmentGroupId', type: 'combo', url: 'investmentGroupListFind.json'}},
						{header: 'Default', width: 23, dataIndex: 'defaultDestinationMapping', type: 'boolean'},
						{header: 'Active', width: 23, dataIndex: 'active', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function() {
						return {
							mappingConfigurationId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.trade.destination.TradeDestinationMappingWindow',
						getDefaultData: function(gridPanel) {
							return {
								mappingConfiguration: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'TradeDestinationMappingConfiguration'
				}]
			}
		]
	}
	]
})
;
