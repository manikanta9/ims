Clifton.trade.destination.TradeDestinationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Destination',
	iconCls: 'shopping-cart',
	width: 900,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Trade Destination defines location how/where a trade must be routed: Desk Order, Trade Ticket, Manual, etc.',
					url: 'tradeDestination.json',
					labelWidth: 140,
					items: [
						{xtype: 'checkbox', name: 'type.fixTradeOnly', hidden: true},
						{fieldLabel: 'Destination Type', name: 'type.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.destination.TradeDestinationTypeWindow', detailIdField: 'type.id'},
						{fieldLabel: 'Destination Name', name: 'name', xtype: 'displayfield'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Trade Modify Condition', name: 'tradeModifySystemCondition.name', hiddenName: 'tradeModifySystemCondition.id', xtype: 'combo', displayField: 'name', valueField: 'id', url: 'systemConditionListFind.json', detailPageClass: 'Clifton.system.condition.ConditionWindow',
							qtip: 'This field specifies a system condition which will be evaluated against Trades assigned to this trade destination.  Trades for this Destination cannot be created/updated/deleted if the condition evaluates to false.'
						},
						{fieldLabel: 'Destination Order', name: 'destinationOrder', xtype: 'spinnerfield', minValue: 1, maxValue: 1000, qtip: 'If multiple destinations apply the one with the smallest order is used'},

						{xtype: 'sectionheaderfield', header: 'FIX Trade Options'},
						{fieldLabel: 'Data Source', name: 'marketDataSource.name', hiddenName: 'marketDataSource.id', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', qtip: 'The market data sources used to map the securities, broker codes and exchanges code to an order. The data source for price multiplier is found using the purpose below and the executing broker on the order.', requiredFields: ['type.fixTradeOnly']},
						{fieldLabel: 'Data Source Purpose', name: 'marketDataSourcePurpose.name', hiddenName: 'marketDataSourcePurpose.id', xtype: 'combo', url: 'marketDataSourcePurposeListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourcePurposeWindow', qtip: 'Limits lookups for data source specific security price multiplier, broker codes, and exchange mappings to this purpose.', requiredFields: ['type.fixTradeOnly']},

						{fieldLabel: 'Destination Connection', name: 'connection.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.destination.TradeDestinationConnectionWindow', detailIdField: 'connection.id', qtip: 'Trade Destination Connection defines the properties needed to send an order to a destination'},

						{fieldLabel: 'FIX Target', name: 'fixTargetSubId', xtype: 'textfield', qtip: 'The fix target that tags messages for appropriate FIX target destination. For example, TKTS, GSDESK, etc.', readOnly: true, requiredFields: ['type.fixTradeOnly']},
						{fieldLabel: 'FIX Handling Instructions', name: 'fixHandlingInstructions', xtype: 'textfield', readOnly: true, requiredFields: ['type.fixTradeOnly']},
						{boxLabel: 'Send allocations after "Done For Day" FIX message received', name: 'allocateAfterDoneForDay', xtype: 'checkbox', qtip: 'If this flag is enabled, trade allocations will only be performed after receiving the FIX "Done For Day" message.', requiredFields: ['type.fixTradeOnly']},
						{boxLabel: 'Trades for this destination must have either Quantity OR Accounting Notional populated but not both', name: 'optionalQuantityOrNotionalEntry', xtype: 'checkbox'},
						{boxLabel: 'Trades for this destination support time-in-force selections.', name: 'enableTimeInForceSelection', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Destination Mappings',
				items: [{
					name: 'tradeDestinationMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Defines allowed trade destinations and executing brokers for this Trade Destination. If Instrument Group is selected, then only securities in that group will be mapped.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Type', width: 40, dataIndex: 'tradeType.name', filter: {searchFieldName: 'tradeTypeName'}, defaultSortColumn: true},
						{header: 'Trade Destination', width: 45, dataIndex: 'tradeDestination.name', filter: {searchFieldName: 'tradeDestinationName'}, hidden: true},
						{header: 'Executing Broker', width: 75, dataIndex: 'executingBrokerCompany.name', filter: {searchFieldName: 'executingBrokerCompanyName'}},
						{header: 'Override Broker Code', width: 45, dataIndex: 'overrideBrokerCode', filter: {searchFieldName: 'overrideBrokerCode'}, hidden: true},
						{header: 'Instrument Group', width: 75, dataIndex: 'defaultInvestmentGroup.name', filter: {searchFieldName: 'defaultInvestmentGroupId', type: 'combo', url: 'investmentGroupListFind.json'}},
						{header: 'Default', width: 23, dataIndex: 'defaultDestinationMapping', type: 'boolean'},
						{header: 'Active', width: 23, dataIndex: 'active', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {tradeDestinationId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.trade.destination.TradeDestinationMappingWindow',
						getDefaultData: function(gridPanel) {
							return {tradeDestination: gridPanel.getWindow().getMainForm().formValues};
						}
					}
				}]
			},


			{
				title: 'Price Multipliers',
				items: [{
					name: 'marketDataSourcePriceMultiplierListFind',
					xtype: 'gridpanel',
					instructions: 'Some market data sources may use non-industry-standard price multipliers. Our system will apply the following additional multipliers to their prices sent to us.  This list is already pre-filtered to the data source purpose as selected on the Destination.  If none is found for that specific purpose, and one exists that applies without a purpose that value will be used.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 150, dataIndex: 'marketDataSource.name', hidden: true, filter: {type: 'combo', searchFieldName: 'marketDataSourceId', url: 'marketDataSourceListFind.json'}},
						{header: 'Investment Instrument', width: 230, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'Additional Multiplier', width: 60, dataIndex: 'additionalPriceMultiplier', type: 'float'},
						{header: 'Purpose', width: 80, dataIndex: 'marketDataSourcePurpose.name', filter: {type: 'combo', searchFieldName: 'marketDataSourcePurposeId', displayField: 'label', url: 'marketDataSourcePurposeListFind.json'}, tooltip: 'If two results for the same instrument appear, the one with the purpose defined is the one that is used.'}
					],
					getLoadParams: function(firstLoad) {
						const dataSourceId = TCG.getValue('marketDataSource.id', this.getWindow().getMainForm().formValues);
						const dataSourcePurposeId = TCG.getValue('marketDataSourcePurpose.id', this.getWindow().getMainForm().formValues);
						if (TCG.isTrue(firstLoad) && TCG.isBlank(dataSourceId)) {
							const index = this.getColumnModel().findColumnIndex('marketDataSource.name');
							this.getColumnModel().setHidden(index, false);
						}
						return {
							marketDataSourceId: dataSourceId,
							marketDataSourcePurposeIdOrNull: dataSourcePurposeId,
							marketDataSourcePurposeIsNull: TCG.isBlank(dataSourcePurposeId) || !dataSourcePurposeId ? true : undefined
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.DataSourcePriceMultiplierWindow',
						deleteURL: 'marketDataSourcePriceMultiplierDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								marketDataSource: TCG.getValue('marketDataSource', gridPanel.getWindow().getMainForm().formValues),
								marketDataSourcePurpose: TCG.getValue('marketDataSourcePurpose', gridPanel.getWindow().getMainForm().formValues)
							};
						}
					}
				}]
			},


			{
				title: 'Company Mappings (Broker Codes)',
				items: [{
					xtype: 'marketdata-datasource-company-mapping-grid',
					getLoadParams: function() {
						const dataSourcePurposeId = TCG.getValue('marketDataSourcePurpose.id', this.getWindow().getMainForm().formValues);
						return {
							marketDataSourceId: TCG.getValue('marketDataSource.id', this.getWindow().getMainForm().formValues),
							marketDataSourcePurposeId: dataSourcePurposeId,
							marketDataSourcePurposeIsNull: TCG.isBlank(dataSourcePurposeId) || !dataSourcePurposeId ? true : undefined
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.CompanyMappingWindow',
						deleteURL: 'marketDataSourceCompanyMappingDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								marketDataSource: TCG.getValue('marketDataSource', gridPanel.getWindow().getMainForm().formValues),
								marketDataSourcePurpose: TCG.getValue('marketDataSourcePurpose', gridPanel.getWindow().getMainForm().formValues)
							};
						}
					}
				}]
			},


			{
				title: 'Exchange Mappings',
				items: [{
					xtype: 'marketdata-datasource-exchange-mapping-grid',
					getLoadParams: function() {
						const dataSourcePurposeId = TCG.getValue('marketDataSourcePurpose.id', this.getWindow().getMainForm().formValues);
						return {
							marketDataSourceId: TCG.getValue('marketDataSource.id', this.getWindow().getMainForm().formValues),
							marketDataSourcePurposeId: dataSourcePurposeId,
							marketDataSourcePurposeIsNull: TCG.isBlank(dataSourcePurposeId) || !dataSourcePurposeId ? true : undefined
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.ExchangeMappingWindow',
						deleteURL: 'marketDataSourceExchangeMappingDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								marketDataSource: TCG.getValue('marketDataSource', gridPanel.getWindow().getMainForm().formValues),
								marketDataSourcePurpose: TCG.getValue('marketDataSourcePurpose', gridPanel.getWindow().getMainForm().formValues)
							};
						}
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'TradeDestination'
				}]
			}
		]
	}]
});
