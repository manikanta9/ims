Clifton.trade.destination.TradeDestinationMappingSetupWindow = Ext.extend(TCG.app.Window, {
	title: 'Trade Destination Settings',
	iconCls: 'shopping-cart',
	id: 'tradeDestinationMappingSetupWindow',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Destination Mappings',
				items: [{
					name: 'tradeDestinationMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Defines allowed trade destinations and executing brokers for each trade type. If Instrument Group is selected, then only securities in that group will be mapped.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'tradeDestination.connection.name',
					viewNames: ['Default View', 'Order Execution Configuration', 'Allocation Configuration', 'Commissions Configuration'],
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Trade Type', width: 40, dataIndex: 'tradeType.name', filter: {searchFieldName: 'tradeTypeName'}, defaultSortColumn: true, allViews: true},
						{header: 'Trade Destination', width: 40, dataIndex: 'tradeDestination.name', filter: {searchFieldName: 'tradeDestinationName'}, allViews: true},
						{header: 'Executing Broker', width: 75, dataIndex: 'executingBrokerCompany.name', filter: {searchFieldName: 'executingBrokerCompanyName'}, allViews: true},
						{header: 'Default Instrument Group', width: 75, dataIndex: 'defaultInvestmentGroup.name', filter: {searchFieldName: 'defaultInvestmentGroupId', type: 'combo', url: 'investmentGroupListFind.json'}, viewNames: ['Default View']},
						{
							header: 'Destination Connection', width: 75, dataIndex: 'connection.name', filter: {searchFieldName: 'coalesceOverrideConnectionId', type: 'combo', url: 'tradeDestinationConnectionListFind.json', viewNames: ['Default View']},
							renderer: function(v, metaData, r) {
								if (TCG.isNotEquals(v, TCG.getValue('tradeDestination.connection.name', r.json))) {
									metaData.css = 'amountAdjusted';
									metaData.attr = 'qtip=\'Mapping Override.  Destination Connection is [' + TCG.getValue('tradeDestination.connection.name', r.json) + ']\'';
								}
								return v;
							}
						},

						{header: 'Override Broker Code', width: 40, dataIndex: 'overrideBrokerCode', filter: {searchFieldName: 'overrideBrokerCode'}, viewNames: ['Default View']},
						{header: 'Mapping Configuration', width: 75, dataIndex: 'mappingConfiguration.name', filter: {searchFieldName: 'mappingConfigurationId', type: 'combo', url: 'tradeDestinationMappingConfigurationListFind.json'}, viewNames: ['Default View']},
						{header: 'Default', width: 23, dataIndex: 'defaultDestinationMapping', type: 'boolean', viewNames: ['Default View']},


						// Order Execution Configuration
						{header: 'FIX Global Trade Note', dataIndex: 'fixGlobalTradeNote', width: 75, hidden: true, tooltip: 'Note that will be appended to the trade notes for all fix orders to this destination.', renderer: TCG.renderValueWithTooltip, viewNames: ['Order Execution Configuration']},
						{header: 'Order Account Mapping Purpose', dataIndex: 'orderAccountMappingPurpose.name', width: 65, hidden: true, filter: {searchFieldName: 'orderAccountMappingPurposeId', type: 'combo', url: 'investmentAccountMappingPurposeListFind.json'}, tooltip: 'The mapping purpose to use for account mapping in orders.', viewNames: ['Order Execution Configuration']},


						// Order Execution Configuration (CONFIG)
						{header: 'Fix Trade Grouping Allowed', type: 'boolean', width: 65, hidden: true, dataIndex: 'mappingConfiguration.fixTradeGroupingAllowed', filter: {searchFieldName: 'fixTradeGroupingAllowed'}, tooltip: 'When creating orders the trades will be grouped by direction (buy/sell), security, executing broker, trader.', viewNames: ['Order Execution Configuration']},
						{header: 'Fix Group By Holding Account', type: 'boolean', width: 65, hidden: true, dataIndex: 'mappingConfiguration.fixTradeGroupingByHoldingAccount', filter: {searchFieldName: 'fixTradeGroupingByHoldingAccount'}, viewNames: ['Order Execution Configuration']},
						{header: 'Fix MIC Exchange Code Required On Order Messages', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixMICExchangeCodeRequiredOnOrderMessages', filter: {searchFieldName: 'fixMICExchangeCodeRequiredOnOrderMessages'}, hidden: true, tooltip: 'Send security exchange code in tag 207 on order messages. Uses code from Exchange Mappings if it exists otherwise uses the exchangeCode on the Investment Exchange.', viewNames: ['Order Execution Configuration']},
						{header: 'Send Client Account With Order', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.sendClientAccountWithOrder', filter: {searchFieldName: 'sendClientAccountWithOrder'}, hidden: true, viewNames: ['Order Execution Configuration']},
						{header: 'Send Restrictive Broker List', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.sendRestrictiveBrokerList', filter: {searchFieldName: 'sendRestrictiveBrokerList'}, hidden: true, viewNames: ['Order Execution Configuration']},
						{header: 'Broker Restricted To Related Holding AccountList', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.brokerRestrictedToRelatedHoldingAccountList', filter: {searchFieldName: 'brokerRestrictedToRelatedHoldingAccountList'}, hidden: true, viewNames: ['Order Execution Configuration']},
						{header: 'Search Company Hierarchy For Broker Match', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.searchCompanyHierarchyForBrokerMatch', filter: {searchFieldName: 'searchCompanyHierarchyForBrokerMatch'}, hidden: true, tooltip: 'Search for the executing broker based on the issuer of the holding account being a subsidiary at any level of the mapped company.', viewNames: ['Order Execution Configuration']},
						{header: 'Fix Use Symbol For Equity Orders', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixUseSymbolForEquityOrders', hidden: true, filter: {searchFieldName: 'fixUseSymbolForEquityOrders'}, viewNames: ['Order Execution Configuration']},

						// Allocation Configuration
						{header: 'FIX Post Trade Allocation Top Account', dataIndex: 'fixPostTradeAllocationTopAccountNumber', width: 65, hidden: true, qtip: 'For post trade allocations, this is the account that the orders will be put in before they are allocated.', viewNames: ['Allocation Configuration']},
						{header: 'Allocation Account Mapping Purpose', dataIndex: 'allocationAccountMappingPurpose.name', width: 65, hidden: true, filter: {searchFieldName: 'allocationAccountMappingPurposeId', type: 'combo', url: 'investmentAccountMappingPurposeListFind.json'}, tooltip: 'The mapping purpose to use for account mapping in allocations.', viewNames: ['Allocation Configuration']},
						{header: 'Override Allocation Broker Code', width: 40, dataIndex: 'overrideAllocationBrokerCode', filter: {searchFieldName: 'overrideAllocationBrokerCode'}, viewNames: ['Default View', 'Allocation Configuration']},

						// Allocation Configuration (CONFIG)
						{header: 'Fix Allocation Type', width: 65, dataIndex: 'mappingConfiguration.fixAllocationType', hidden: true, filter: {type: 'combo', searchFieldName: 'fixAllocationType', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.AllocationTypes}}, viewNames: ['Allocation Configuration']},
						{header: 'Fix Post-Trade Allocation', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixPostTradeAllocation', filter: {searchFieldName: 'fixPostTradeAllocation'}, hidden: true, viewNames: ['Allocation Configuration']},
						{header: 'Disable Pre-Trade Allocations', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixDisablePreTradeAllocations', filter: {searchFieldName: 'fixDisablePreTradeAllocations'}, hidden: true, tooltip: 'Disable pre-trade allocations from being included in the order.  Pre-trade allocations are automatically enabled if the "Trades are allocated via FIX after execution" flag is unchecked.  This flag will provide the ability to disable pre-trade allocations.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Allocation Report Expected', type: 'boolean', width: 70, dataIndex: 'mappingConfiguration.fixAllocationReportExpected', filter: {searchFieldName: 'fixAllocationReportExpected'}, hidden: true, viewNames: ['Allocation Configuration']},
						{header: 'Force Booking Average Price', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixForceExecutionAveragePriceWithAllocationReport', filter: {searchFieldName: 'fixForceExecutionAveragePriceWithAllocationReport'}, hidden: true, viewNames: ['Allocation Configuration']},
						{header: 'Fix Allocation Complete When Status Is Received', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixAllocationCompleteWhenStatusIsReceived', filter: {searchFieldName: 'fixAllocationCompleteWhenStatusIsReceived'}, hidden: true, viewNames: ['Allocation Configuration']},
						{header: 'Fix Executing Account Used', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixExecutingAccountUsed', filter: {searchFieldName: 'fixExecutingAccountUsed'}, hidden: true, tooltip: 'Indicates the trade will be allocated to an execution account.  This requires that the holding account has a related execution account.  For example, for Stocks, in order to execute the holding account needs to be related to a custodian account with purpose "Executing Stocks"', viewNames: ['Allocation Configuration']},
						{header: 'Fix Allocation Order Quantity Required In NoOrders Group', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixAllocationOrderQuantityRequiredInNoOrdersGroup', filter: {searchFieldName: 'fixAllocationOrderQuantityRequiredInNoOrdersGroup'}, hidden: true, tooltip: 'If true, calculates and includes the order quantity for each order in the AllocationInstruction NoOrders group.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Allocation Price Required In Allocation Details', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixAllocationPriceRequiredInAllocationDetails', filter: {searchFieldName: 'fixAllocationPriceRequiredInAllocationDetails'}, hidden: true, tooltip: 'Include the allocation price in (tag 366) in the allocation details section on allocation messages.  This price will be the equivalent of the average price of the allocation.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Currency Code Required On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixCurrencyCodeRequiredOnAllocationMessages', filter: {searchFieldName: 'fixCurrencyCodeRequiredOnAllocationMessages'}, hidden: true, tooltip: 'Send currency code in tag 15 on allocation messages.  Uses security symbol code from Paying Security of the Trade.', viewNames: ['Allocation Configuration']},
						{header: 'Fix MIC Exchange Code Required On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixMICExchangeCodeRequiredOnAllocationMessages', filter: {searchFieldName: 'fixMICExchangeCodeRequiredOnAllocationMessages'}, hidden: true, tooltip: 'Send security exchange code in tag 207 on allocation messages.  Uses code from Exchange Mappings if it exists otherwise uses the exchangeCode on the Investment Exchange.', viewNames: ['Allocation Configuration']},
						{header: 'Fix CFI Code Required On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixCFICodeRequiredOnAllocationMessages', filter: {searchFieldName: 'fixCFICodeRequiredOnAllocationMessages'}, hidden: true, tooltip: 'Send security CFI code in tag 461 on allocation messages.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Secondary Client Order ID Used For Allocations', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixSecondaryClientOrderIdUsedForAllocations', filter: {searchFieldName: 'fixSecondaryClientOrderIdUsedForAllocations'}, hidden: true, tooltip: 'Indicates that we should send SecondaryClOrdID (tag 526) from the execution reports instead of our ClOrdID from tag 11.  NOTE: This is currently used for allocations with CITI via REDI.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Send Top Account On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixSendTopAccountOnAllocationMessages', filter: {searchFieldName: 'fixSendTopAccountOnAllocationMessages'}, hidden: true, tooltip: 'Send the post trade allocation top account number on the allocation message and not the order.', viewNames: ['Allocation Configuration']},
						{header: 'Fix SendBroker Party Ids On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixSendBrokerPartyIdsOnAllocationMessages', filter: {searchFieldName: 'fixSendBrokerPartyIdsOnAllocationMessages'}, hidden: true, tooltip: 'When this is true, each allocation on the fix message will have a party id that indicates who the clearing broker account is.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Send Clearing Firm LEI With Allocation', type: 'boolean', width: 65, dataIndex: 'mappingConfiguration.fixSendClearingFirmLEIWithAllocation', filter: {searchFieldName: 'fixSendClearingFirmLEIWithAllocation'}, hidden: true, qtip: 'If true, we will send a nested party id with the LEI of the issuing company of the holding account. Currently used for sending pre-trade allocations to Bloomberg for Cleared CDS/IRS traded on the SEF.', viewNames: ['Allocation Configuration']},

						// Commissions Configuration (CONFIG)
						{header: 'Fix Commission Type', width: 55, dataIndex: 'mappingConfiguration.fixCommissionType', hidden: true, filter: {type: 'combo', searchFieldName: 'fixCommissionType', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.CommissionTypes}}, viewNames: ['Commissions Configuration']},
						{header: 'Fix Commission Process Code', width: 65, dataIndex: 'mappingConfiguration.fixProcessCode', hidden: true, filter: {type: 'combo', searchFieldName: 'fixProcessCode', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.ProcessCodes}}, viewNames: ['Commissions Configuration']},
						{header: 'Fix Default Commission Per Unit', width: 65, dataIndex: 'mappingConfiguration.fixDefaultCommissionPerUnit', filter: {searchFieldName: 'fixDefaultCommissionPerUnit'}, hidden: true, viewNames: ['Commissions Configuration']},


						{header: 'Active', width: 23, dataIndex: 'active', type: 'boolean', allViews: true},
						{header: 'Start Date', width: 25, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 25, dataIndex: 'endDate', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.trade.destination.TradeDestinationMappingWindow'
					}
				}]
			},


			{
				title: 'Destination Mapping Configurations',
				items: [{
					name: 'tradeDestinationMappingConfigurationListFind',
					xtype: 'gridpanel',
					instructions: 'Trade Destination Mapping Configuration sets for use in Trade Destination Mappings',
					topToolbarSearchParameter: 'searchPattern',
					viewNames: ['Default View', 'Order Execution Configuration', 'Allocation Configuration', 'Commissions Configuration'],
					columns: [
						{header: 'ID', type: 'int', width: 20, dataIndex: 'id', hidden: true},
						{header: 'Configuration Name', width: 150, dataIndex: 'name', allViews: true},
						{header: 'Description', width: 275, dataIndex: 'description', hidden: true},

						// Order Execution Configuration
						{header: 'Fix Trade Grouping Allowed', type: 'boolean', width: 65, dataIndex: 'fixTradeGroupingAllowed', tooltip: 'When creating orders the trades will be grouped by direction (buy/sell), security, executing broker, trader.', viewNames: ['Default View', 'Order Execution Configuration']},
						{header: 'Fix Group By Holding Account', type: 'boolean', width: 65, dataIndex: 'fixTradeGroupingByHoldingAccount', viewNames: ['Default View', 'Order Execution Configuration']},
						{header: 'Fix MIC Exchange Code Required On Order Messages', type: 'boolean', width: 65, dataIndex: 'fixMICExchangeCodeRequiredOnOrderMessages', hidden: true, tooltip: 'Send security exchange code in tag 207 on order messages. Uses code from Exchange Mappings if it exists otherwise uses the exchangeCode on the Investment Exchange.', viewNames: ['Order Execution Configuration']},
						{header: 'Send Client Account With Order', type: 'boolean', width: 65, dataIndex: 'sendClientAccountWithOrder', hidden: true, viewNames: ['Order Execution Configuration']},
						{header: 'Send Restrictive Broker List', type: 'boolean', width: 65, dataIndex: 'sendRestrictiveBrokerList', hidden: true, viewNames: ['Order Execution Configuration']},
						{header: 'Broker Restricted To Related Holding AccountList', type: 'boolean', width: 65, dataIndex: 'brokerRestrictedToRelatedHoldingAccountList', hidden: true, viewNames: ['Order Execution Configuration']},
						{header: 'Search Company Hierarchy For Broker Match', type: 'boolean', width: 65, dataIndex: 'searchCompanyHierarchyForBrokerMatch', hidden: true, tooltip: 'Search for the executing broker based on the issuer of the holding account being a subsidiary at any level of the mapped company.', viewNames: ['Order Execution Configuration']},
						{header: 'Fix Use Symbol For Equity Orders', type: 'boolean', width: 65, dataIndex: 'fixUseSymbolForEquityOrders', hidden: true, viewNames: ['Order Execution Configuration']},

						// Allocation Configuration
						{header: 'Fix Allocation Type', width: 65, dataIndex: 'fixAllocationType', filter: {type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.AllocationTypes}}, viewNames: ['Default View', 'Allocation Configuration']},
						{header: 'Fix Post-Trade Allocation', type: 'boolean', width: 65, dataIndex: 'fixPostTradeAllocation', viewNames: ['Default View', 'Allocation Configuration']},
						{header: 'Disable Pre-Trade Allocations', type: 'boolean', width: 65, dataIndex: 'fixDisablePreTradeAllocations', hidden: true, tooltip: 'Disable pre-trade allocations from being included in the order.  Pre-trade allocations are automatically enabled if the "Trades are allocated via FIX after execution" flag is unchecked.  This flag will provide the ability to disable pre-trade allocations.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Allocation Report Expected', type: 'boolean', width: 70, dataIndex: 'fixAllocationReportExpected', viewNames: ['Default View', 'Allocation Configuration']},
						{header: 'Force Booking Average Price', type: 'boolean', width: 65, dataIndex: 'fixForceExecutionAveragePriceWithAllocationReport', viewNames: ['Default View', 'Allocation Configuration']},
						{header: 'Fix Allocation Complete When Status Is Received', type: 'boolean', width: 65, dataIndex: 'fixAllocationCompleteWhenStatusIsReceived', hidden: true, viewNames: ['Allocation Configuration']},
						{header: 'Fix Executing Account Used', type: 'boolean', width: 65, dataIndex: 'fixExecutingAccountUsed', hidden: true, tooltip: 'Indicates the trade will be allocated to an execution account.  This requires that the holding account has a related execution account.  For example, for Stocks, in order to execute the holding account needs to be related to a custodian account with purpose "Executing Stocks"', viewNames: ['Allocation Configuration']},
						{header: 'Fix Allocation Price Required In Allocation Details', type: 'boolean', width: 65, dataIndex: 'fixAllocationPriceRequiredInAllocationDetails', filter: {searchFieldName: 'fixAllocationPriceRequiredInAllocationDetails'}, hidden: true, tooltip: 'Include the allocation price in (tag 366) in the allocation details section on allocation messages.  This price will be the equivalent of the average price of the allocation.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Currency Code Required On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'fixCurrencyCodeRequiredOnAllocationMessages', filter: {searchFieldName: 'fixCurrencyCodeRequiredOnAllocationMessages'}, hidden: true, tooltip: 'Send currency code in tag 15 on allocation messages.  Uses security symbol code from Paying Security of the Trade.', viewNames: ['Allocation Configuration']},
						{header: 'Fix MIC Exchange Code Required On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'fixMICExchangeCodeRequiredOnAllocationMessages', hidden: true, tooltip: 'Send security exchange code in tag 207 on allocation messages.  Uses code from Exchange Mappings if it exists otherwise uses the exchangeCode on the Investment Exchange.', viewNames: ['Allocation Configuration']},
						{header: 'Fix CFI Code Required On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'fixCFICodeRequiredOnAllocationMessages', filter: {searchFieldName: 'fixCFICodeRequiredOnAllocationMessages'}, hidden: true, tooltip: 'Send security CFI code in tag 461 on allocation messages.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Secondary Client Order ID Used For Allocations', type: 'boolean', width: 65, dataIndex: 'fixSecondaryClientOrderIdUsedForAllocations', hidden: true, tooltip: 'Indicates that we should send SecondaryClOrdID (tag 526) from the execution reports instead of our ClOrdID from tag 11.  NOTE: This is currently used for allocations with CITI via REDI.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Send Top Account On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'fixSendTopAccountOnAllocationMessages', hidden: true, tooltip: 'Send the post trade allocation top account number on the allocation message and not the order.', viewNames: ['Allocation Configuration']},
						{header: 'Fix SendBroker Party Ids On Allocation Messages', type: 'boolean', width: 65, dataIndex: 'fixSendBrokerPartyIdsOnAllocationMessages', hidden: true, tooltip: 'When this is true, each allocation on the fix message will have a party id that indicates who the clearing broker account is.', viewNames: ['Allocation Configuration']},
						{header: 'Fix Send Clearing Firm LEI With Allocation', type: 'boolean', width: 65, dataIndex: 'fixSendClearingFirmLEIWithAllocation', hidden: true, qtip: 'If true, we will send a nested party id with the LEI of the issuing company of the holding account. Currently used for sending pre-trade allocations to Bloomberg for Cleared CDS/IRS traded on the SEF.', viewNames: ['Allocation Configuration']},

						// Commissions Configuration
						{header: 'Fix Commission Type', width: 55, dataIndex: 'fixCommissionType', filter: {type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.CommissionTypes}}, viewNames: ['Default View', 'Commissions Configuration']},
						{header: 'Fix Commission Process Code', width: 65, dataIndex: 'fixProcessCode', hidden: true, filter: {type: 'combo', displayField: 'name', valueField: 'value', mode: 'local', store: {xtype: 'arraystore', fields: ['name', 'value'], data: Clifton.fix.order.ProcessCodes}}, viewNames: ['Commissions Configuration']},
						{header: 'Fix Default Commission Per Unit', width: 65, dataIndex: 'fixDefaultCommissionPerUnit', hidden: true, viewNames: ['Commissions Configuration']}
					],
					editor: {
						detailPageClass: 'Clifton.trade.destination.TradeDestinationMappingConfigurationWindow'
					}
				}]
			},


			{
				title: 'Trade Destinations',
				items: [{
					name: 'tradeDestinationListFind',
					xtype: 'gridpanel',
					instructions: 'Trade Destination defines location how/where a trade must be routed: Desk Order, Trade Ticket, Manual, etc.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Destination Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Destination Type', width: 70, dataIndex: 'type.name', filter: {searchFieldName: 'destinationTypeId', type: 'combo', url: 'tradeDestinationTypeListFind.json'}},
						{header: 'Data Source', width: 90, dataIndex: 'marketDataSource.name', filter: {searchFieldName: 'marketDataSourceId', type: 'combo', url: 'marketDataSourceListFind.json'}},
						{header: 'Data Source Purpose', width: 100, dataIndex: 'marketDataSourcePurpose.name', filter: {searchFieldName: 'marketDataSourcePurposeId', type: 'combo', url: 'marketDataSourcePurposeListFind.json'}},
						{header: 'Connection', width: 120, dataIndex: 'connection.name', filter: {searchFieldName: 'connectionId', type: 'combo', url: 'tradeDestinationConnectionListFind.json'}},
						{header: 'FIX Target', width: 50, dataIndex: 'fixTargetSubId', tooltip: 'The fix target that tags messages for appropriate FIX target destination. For example, TKTS, GSDESK, etc.'},
						{header: 'FIX Handling Instructions', width: 50, dataIndex: 'fixHandlingInstructions'},
						{header: 'Allocate After Done for Day', width: 50, dataIndex: 'allocateAfterDoneForDay', hidden: true, type: 'boolean', tooltip: 'If this flag is enabled, trade allocations will only be performed after receiving the FIX Done For Day message.'},
						{header: 'Quantity OR Notional', width: 50, dataIndex: 'optionalQuantityOrNotionalEntry', hidden: true, type: 'boolean', tooltip: 'Trades for this destination must have either quantityIntended or AccountingNotional populated but not both.'},
						{header: 'Allow Time in Force', width: 50, dataIndex: 'enableTimeInForceSelection', hidden: true, type: 'boolean', tooltip: 'Trades for this destination support time-in-force selections.'},
						{header: 'Trade Modify Condition', width: 90, dataIndex: 'tradeModifySystemCondition.name', filter: {searchFieldName: 'tradeModifySystemConditionName'}},
						{header: 'Order', width: 40, dataIndex: 'destinationOrder', type: 'int', defaultSortColumn: true}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow'
					}
				}]
			},


			{
				title: 'Destination Types',
				items: [{
					name: 'tradeDestinationTypeList',
					xtype: 'gridpanel',
					instructions: 'All Trade Destinations are classified into one of the following types.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Manual', width: 50, dataIndex: 'manual', type: 'boolean'},
						{header: 'FIX Only', width: 50, dataIndex: 'fixTradeOnly', type: 'boolean'},
						{header: 'Create Order', width: 50, dataIndex: 'createOrder', type: 'boolean'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.trade.destination.TradeDestinationTypeWindow'
					}
				}]
			},


			{
				title: 'Destination Connections',
				items: [{
					name: 'tradeDestinationConnectionListFind',
					xtype: 'gridpanel',
					instructions: 'Trade Destination Connection defines a specific connection to send FIX messages to.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Connection Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Allocation Connection', width: 100, dataIndex: 'allocationConnection.name', idDataIndex: 'allocationConnection.id'},
						{header: 'FIX Destination Name', width: 60, dataIndex: 'fixDestinationName'}
					],
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.trade.destination.TradeDestinationConnectionWindow'
					}
				}]
			}
		]
	}]
});
