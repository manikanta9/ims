Clifton.trade.destination.TradeDestinationMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Destination Mapping',
	iconCls: 'shopping-cart',
	width: 900,
	height: 625,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Defines allowed trade destinations and executing brokers for each trade type. If Instrument Group is selected, then only securities in that group will be mapped.',
					url: 'tradeDestinationMapping.json',
					labelWidth: 220,
					listeners: {
						afterload: function(panel) {
							const f = panel;
							f.updateFixFields(f.getForm().formValues.tradeDestination.type.fixTradeOnly);
						}
					},
					getDefaultData: function(win) {
						let dd = win.defaultData || {};
						if (win.defaultDataIsReal) {
							return dd;
						}
						const defaultTradeDestination = TCG.data.getData('tradeDestinationByName.json?name=Manual', this, 'trade.tradeDestination.Manual');

						dd = Ext.apply({
							tradeDestination: defaultTradeDestination
						}, dd);
						return dd;
					},
					updateFixFields: function(isFix) {
						const f = this;
						if (TCG.isTrue(isFix)) {
							f.enableField('overrideBrokerCode');
							f.enableField('fixPostTradeAllocationTopAccountNumber');
							f.enableField('fixGlobalTradeNote');
							f.enableField('orderAccountMappingPurpose.name');
							f.enableField('allocationAccountMappingPurpose.name');
						}
						else {
							f.disableField('overrideBrokerCode');
							f.disableField('fixPostTradeAllocationTopAccountNumber');
							f.disableField('fixGlobalTradeNote');
							f.disableField('orderAccountMappingPurpose.name');
							f.disableField('allocationAccountMappingPurpose.name');
						}
					},
					items: [
						{fieldLabel: 'Trade Type', name: 'tradeType.name', hiddenName: 'tradeType.id', xtype: 'combo', url: 'tradeTypeListFind.json', detailPageClass: 'Clifton.trade.TypeWindow'},
						{
							fieldLabel: 'Trade Destination', name: 'tradeDestination.name', hiddenName: 'tradeDestination.id', limitRequestedProperties: false, xtype: 'combo', disableAddNewItem: true, detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', url: 'tradeDestinationListFind.json',
							listeners: {
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									fp.updateFixFields(record.json.type.fixTradeOnly);
								}
							}
						},
						{fieldLabel: 'Destination Connection Override', name: 'overrideConnection.name', hiddenName: 'overrideConnection.id', xtype: 'combo', loadAll: true, url: 'tradeDestinationConnectionListFind.json', detailPageClass: 'Clifton.trade.destination.TradeDestinationConnectionWindow', qtip: 'Optionally overrides Destination Connection specified on the Trade Destination.'},
						{fieldLabel: 'Executing Broker', name: 'executingBrokerCompany.name', hiddenName: 'executingBrokerCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow', url: 'businessCompanyListFind.json?tradingAllowed=true'},
						{fieldLabel: 'Default Instrument Group', name: 'defaultInvestmentGroup.name', hiddenName: 'defaultInvestmentGroup.id', displayField: 'name', xtype: 'combo', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},

						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
						{boxLabel: 'Default Destination', name: 'defaultDestinationMapping', xtype: 'checkbox', labelSeparator: ''},

						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Destination Mapping Configuration', name: 'mappingConfiguration.name', hiddenName: 'mappingConfiguration.id', xtype: 'combo', url: 'tradeDestinationMappingConfigurationListFind.json', detailPageClass: 'Clifton.trade.destination.TradeDestinationMappingConfigurationWindow'},

						{fieldLabel: 'Override Broker Code', name: 'overrideBrokerCode', qtip: 'OVERRIDES the default broker code set for the trade destination connection.  The broker code sent on the order to indicate which broker will execute the order.'},

						{xtype: 'sectionheaderfield', header: 'Order Execution Configuration'},
						{fieldLabel: 'FIX Global Trade Note', qtip: 'Note that will be appended to the trade notes for all fix orders to this destination.', name: 'fixGlobalTradeNote', xtype: 'textarea', height: 50},
						{
							fieldLabel: 'Order Account Mapping Purpose', name: 'orderAccountMappingPurpose.name', hiddenName: 'orderAccountMappingPurpose.id', xtype: 'combo', url: 'investmentAccountMappingPurposeListFind.json', detailPageClass: 'Clifton.investment.account.mapping.AccountMappingPurposeWindow',
							qtip: 'The mapping purpose to use for account mapping in orders.', displayField: 'name'
						},
						{xtype: 'sectionheaderfield', header: 'Allocation Configuration'},
						{fieldLabel: 'FIX Post Trade Allocation Top Account', name: 'fixPostTradeAllocationTopAccountNumber', qtip: 'For post trade allocations, this is the account that the orders will be put in before they are allocated.'},
						{
							fieldLabel: 'Allocation Account Mapping Purpose', name: 'allocationAccountMappingPurpose.name', hiddenName: 'allocationAccountMappingPurpose.id', xtype: 'combo', url: 'investmentAccountMappingPurposeListFind.json', detailPageClass: 'Clifton.investment.account.mapping.AccountMappingPurposeWindow',
							qtip: 'The mapping purpose to use for account mapping in allocations.', displayField: 'name'
						},
						{fieldLabel: 'Override Allocation Broker Code', name: 'overrideAllocationBrokerCode', qtip: 'OVERRIDES the default broker code set for the trade allocation connection.  The broker code sent in the allocation instruction to indicate the broker that will allocate the order.'}

					]
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'TradeDestinationMapping'
				}]
			}
		]
	}]
});
