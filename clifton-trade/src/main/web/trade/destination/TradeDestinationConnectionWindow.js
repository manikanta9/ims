Clifton.trade.destination.TradeDestinationConnectionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Destination Connection',
	iconCls: 'shopping-cart',
	height: 575,
	width: 750,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Trade Destination Connection defines a specific connection to send FIX messages to.',
					url: 'tradeDestinationConnection.json',
					readOnly: true,
					labelWidth: 150,
					items: [
						{fieldLabel: 'Connection Name', name: 'name'},
						{fieldLabel: 'Allocation Connection', name: 'allocationConnection.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.destination.TradeDestinationConnectionWindow', detailIdField: 'allocationConnection.id'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

						{xtype: 'sectionheaderfield', header: 'FIX Connection Info'},
						{fieldLabel: 'FIX Destination Name', name: 'fixDestinationName', qtip: 'FIX destination name used by FIX system to obtain session information.'},
						{fieldLabel: 'FIX On Behalf Of Comp ID', name: 'fixOnBehalfOfCompId'}
					]
				}]
			},


			{
				title: 'Trade Destinations',
				items: [{
					name: 'tradeDestinationListFind',
					xtype: 'gridpanel',
					instructions: 'A list of Trade Destinations that use this connection.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Destination Name', width: 120, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Destination Type', width: 80, dataIndex: 'type.name', filter: {searchFieldName: 'destinationTypeId', type: 'combo', url: 'tradeDestinationTypeListFind.json'}},
						{header: 'Data Source', width: 90, dataIndex: 'marketDataSource.name', filter: {searchFieldName: 'marketDataSourceId', type: 'combo', url: 'marketDataSourceListFind.json'}},
						{header: 'Data Source Purpose', width: 100, dataIndex: 'marketDataSourcePurpose.name', filter: {searchFieldName: 'marketDataSourcePurposeId', type: 'combo', url: 'marketDataSourcePurposeListFind.json'}},
						{header: 'FIX Target', width: 70, dataIndex: 'fixTargetSubId', hidden: true},
						{header: 'FIX Handling Instructions', width: 100, dataIndex: 'fixHandlingInstructions', hidden: true},
						{header: 'Modify Condition', width: 90, dataIndex: 'tradeModifySystemCondition.name', hidden: true, filter: {searchFieldName: 'tradeModifySystemConditionName'}},
						{header: 'Order', width: 50, dataIndex: 'destinationOrder', type: 'int', defaultSortColumn: true}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {connectionId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow'
					}
				}]
			}
		]
	}]
});
