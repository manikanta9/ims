Clifton.trade.assetclass.PositionAllocationWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Trade Position Allocation',
	width: 700,
	height: 600,

	items: [{
		xtype: 'formpanel',
		labelWidth: 100,
		url: 'tradeAssetClassPositionAllocation.json?enableOpenSessionInView=true',
		instructions: 'Trade Asset Class Position Allocations allow explicitly defining contract splitting when trading.  These are automatically created for you when trading on Trade Creation screen in asset classes that are split, or when rolling contracts.  If the contract is explicitly split in the same asset class/replication this allocation will also be automatically rolled into the following days splits.' +
			'<br />Virtual Trades are those not associated with a trade.  For example, entering B and S of same security that updates the splits, but does not create a real trade.  They are considered "Pending" until processed the next day.' +
			'<br />If a referencing trade exists, and is canceled the split will be skipped in the next day allocations (as long as not already processed).',

		listeners: {
			afterload: function(panel) {
				if (TCG.isTrue(this.getFormValue('processed'))) {
					this.setReadOnly(true);
				}
				if (TCG.isNotBlank(this.getFormValue('trade.id'))) {
					this.getForm().findField('tradeDate').setReadOnly(true);
					this.getForm().findField('security.label').setReadOnly(true);
					this.getForm().findField('accountAssetClass.account.label').setReadOnly(true);
				}
			}
		},
		items: [
			{fieldLabel: 'Client Account', name: 'accountAssetClass.account.label', hiddenName: 'accountAssetClass.account.id', detailPageClass: 'Clifton.investment.account.AccountWindow', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', submitValue: false, displayField: 'label'},
			{
				fieldLabel: 'Asset Class', name: 'accountAssetClass.label', hiddenName: 'accountAssetClass.id', xtype: 'combo', detailPageClass: 'Clifton.investment.account.assetclass.AssetClassWindowSelector', url: 'investmentAccountAssetClassListFind.json', displayField: 'label',
				requiredFields: ['accountAssetClass.account.label'],
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						accountId: f.findField('accountAssetClass.account.id').value,
						usesPrimaryReplication: true
					};
				}
			},
			{
				fieldLabel: 'Replication', name: 'replication.label', hiddenName: 'replication.id', detailPageClass: 'Clifton.investment.replication.ReplicationWindow', xtype: 'combo', url: 'investmentReplicationListForAccountAssetClass.json', displayField: 'label',
				tooltip: 'Select a replication only if the security is split within the same asset class (i.e. Used by both Primary and Secondary replication)',
				requiredFields: ['accountAssetClass.id'],
				loadAll: true,
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					queryEvent.combo.store.baseParams = {
						accountAssetClassId: f.findField('accountAssetClass.id').value
					};
				}
			},
			{xtype: 'label', html: '<hr />'},
			{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield'},
			{fieldLabel: 'Trade', xtype: 'linkfield', name: 'trade.labelShort', detailIdField: 'trade.id', detailPageClass: 'Clifton.trade.TradeWindow'},
			{fieldLabel: 'Workflow State', xtype: 'displayfield', name: 'trade.workflow.state.name'},
			{fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=false', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', displayField: 'label'},
			{fieldLabel: 'Quantity', name: 'allocationQuantity', xtype: 'integerfield'},
			{fieldLabel: '', boxLabel: 'Processed', xtype: 'checkbox', name: 'processed', disabled: true}
		]
	}]
});
