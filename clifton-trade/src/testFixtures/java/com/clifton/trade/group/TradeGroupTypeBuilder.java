package com.clifton.trade.group;

import com.clifton.system.schema.SystemTableBuilder;


public class TradeGroupTypeBuilder {

	private TradeGroupType tradeGroupType;


	private TradeGroupTypeBuilder(TradeGroupType tradeGroupType) {
		this.tradeGroupType = tradeGroupType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeGroupTypeBuilder createPortfolioRun() {
		TradeGroupType tradeGroupType = new TradeGroupType();

		tradeGroupType.setId((short) 3);
		tradeGroupType.setName("Portfolio Run");
		tradeGroupType.setDescription("Trades directly created from a PIOS run.");
		tradeGroupType.setSourceSystemTable(SystemTableBuilder.createPortfolioRun().toSystemTable());

		return new TradeGroupTypeBuilder(tradeGroupType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeGroupTypeBuilder withId(short id) {
		getTradeGroupType().setId(id);
		return this;
	}


	public TradeGroupTypeBuilder withName(String name) {
		getTradeGroupType().setName(name);
		return this;
	}


	public TradeGroupTypeBuilder withDescription(String description) {
		getTradeGroupType().setDescription(description);
		return this;
	}


	public TradeGroupType toTradeGroupType() {
		return this.tradeGroupType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeGroupType getTradeGroupType() {
		return this.tradeGroupType;
	}
}
