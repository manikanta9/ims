package com.clifton.trade.group;

import com.clifton.core.util.date.DateUtils;


public class TradeGroupBuilder {

	private TradeGroup tradeGroup;


	private TradeGroupBuilder(TradeGroup tradeGroup) {
		this.tradeGroup = tradeGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeGroupBuilder createPortfolioRun() {
		TradeGroup tradeGroup = new TradeGroup();

		tradeGroup.setId(69393);
		tradeGroup.setTradeGroupType(TradeGroupTypeBuilder.createPortfolioRun().toTradeGroupType());
		tradeGroup.setTradeDate(DateUtils.toDate("08/22/2016"));
		tradeGroup.setNote("Group Test Run");
		tradeGroup.setSourceFkFieldId(541798);

		return new TradeGroupBuilder(tradeGroup);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeGroupBuilder withId(int id) {
		getTradeGroup().setId(id);
		return this;
	}


	public TradeGroup toTradeGroup() {
		return this.tradeGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeGroup getTradeGroup() {
		return this.tradeGroup;
	}
}
