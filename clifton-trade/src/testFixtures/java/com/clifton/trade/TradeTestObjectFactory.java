package com.clifton.trade;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculator;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.marketdata.MarketDataTestObjectFactory;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.accounting.booking.rule.TradeAccrualBookingRule;
import com.clifton.trade.accounting.booking.rule.TradeAccruedInterestBookingRule;
import com.clifton.trade.accounting.booking.rule.TradeCommissionBookingRule;
import com.clifton.trade.accounting.booking.rule.TradeCreateCurrencyPositionBookingRule;
import com.clifton.trade.accounting.booking.rule.TradeCreatePositionBookingRule;
import com.clifton.trade.accounting.booking.rule.TradeKeepNotionalBookingRule;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Date;


public class TradeTestObjectFactory {

	private static final Lazy<InvestmentCurrencyService> INVESTMENT_CURRENCY_SERVICE_LAZY = new Lazy<>(() -> {
		InvestmentCurrencyService currencyService = Mockito.mock(InvestmentCurrencyService.class);
		Mockito.when(currencyService.isInvestmentCurrencyMultiplyConvention(Mockito.anyString(), Mockito.anyString()))
				.then(invocation -> {
					String from = invocation.getArgument(0);
					String to = invocation.getArgument(1);
					switch (from) {
						case "EUR": {
							return true;
						}
						case "GBP": {
							if ("EUR".equals(to)) {
								return false;
							}
							return true;
						}
						case "AUD": {
							switch (to) {
								case "GBP":
								case "EUR": {
									return false;
								}
								default: {
									return true;
								}
							}
						}
						case "USD": {
							switch (to) {
								case "GBP":
								case "EUR":
								case "AUD":
								case "NZD": {
									return false;
								}
								default: {
									return true;
								}
							}
						}

						case "CAD": {
							switch (to) {
								case "GBP":
								case "EUR":
								case "AUD":
								case "NZD":
								case "USD": {
									return false;
								}
								default: {
									return true;
								}
							}
						}
						default: {
							return false;
						}
					}
				});
		return currencyService;
	});

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeCreatePositionBookingRule newTradeCreatePositionBookingRule() {
		TradeCreatePositionBookingRule bookingRule = new TradeCreatePositionBookingRule();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		bookingRule.setInvestmentCalculator(new InvestmentCalculatorImpl());
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		bookingRule.setSystemSchemaService(Mockito.mock(SystemSchemaService.class));

		Mockito.when(bookingRule.getSystemSchemaService().getSystemTableByName("TradeFill")).thenReturn(new SystemTable());
		return bookingRule;
	}


	public static TradeAccrualBookingRule newTradeAccrualBookingRule() {
		TradeAccrualBookingRule bookingRule = new TradeAccrualBookingRule();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		return bookingRule;
	}


	public static TradeCreateCurrencyPositionBookingRule newTradeCreateCurrencyPositionBookingRule() {
		TradeCreateCurrencyPositionBookingRule bookingRule = new TradeCreateCurrencyPositionBookingRule();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setInvestmentCurrencyService(getInvestmentCurrencyService());
		bookingRule.setSystemSchemaService(Mockito.mock(SystemSchemaService.class));
		Mockito.when(bookingRule.getSystemSchemaService().getSystemTableByName("TradeFill")).thenReturn(new SystemTable());
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		return bookingRule;
	}


	public static TradeCreateCurrencyPositionBookingRule newTradeCreateCurrencyPositionBookingRule(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, BigDecimal rate) {
		TradeCreateCurrencyPositionBookingRule bookingRule = newTradeCreateCurrencyPositionBookingRule();
		bookingRule.setMarketDataExchangeRatesApiService(MarketDataTestObjectFactory.newMarketDataExchangeRatesApiService(fromCurrency, toCurrency, rate));
		return bookingRule;
	}


	public static TradeCommissionBookingRule newTradeCommissionBookingRule() {
		TradeCommissionBookingRule bookingRule = new TradeCommissionBookingRule();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setAccountingCommissionCalculator(Mockito.mock(AccountingCommissionCalculator.class));
		bookingRule.setTradeCommissionService(Mockito.mock(TradeCommissionService.class));
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());

		return bookingRule;
	}


	public static TradeKeepNotionalBookingRule newTradeKeepNotionalBookingRule() {
		TradeKeepNotionalBookingRule bookingRule = new TradeKeepNotionalBookingRule();
		bookingRule.setAccountingPositionService(Mockito.mock(AccountingPositionService.class));
		bookingRule.setAccountingTransactionService(Mockito.mock(AccountingTransactionService.class));
		bookingRule.setSystemColumnValueHandler(Mockito.mock(SystemColumnValueHandler.class));
		bookingRule.setInvestmentCalculator(new InvestmentCalculatorImpl());

		return bookingRule;
	}


	public static TradeAccruedInterestBookingRule newTradeAccruedInterestBookingRule() {
		TradeAccruedInterestBookingRule bookingRule = new TradeAccruedInterestBookingRule();
		bookingRule.setAccountingAccountService(AccountingTestObjectFactory.newAccountingAccountService());
		bookingRule.setInvestmentSecurityUtilHandler(new InvestmentSecurityUtilHandlerImpl());
		return bookingRule;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static Trade newTrade_Currency(boolean buy, InvestmentSecurity security, BigDecimal notional, BigDecimal exchangeRate, Date tradeDate) {
		return newTrade_Currency(buy, security, notional, exchangeRate, tradeDate, null);
	}


	public static Trade newTrade_Currency(boolean buy, InvestmentSecurity security, BigDecimal notional, BigDecimal exchangeRate, Date tradeDate, InvestmentSecurity accountBaseCurrency) {
		Trade trade = new Trade();
		trade.setBuy(buy);
		trade.setClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		if (accountBaseCurrency != null) {
			trade.getClientInvestmentAccount().setBaseCurrency(accountBaseCurrency);
		}
		trade.setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		trade.setPayingSecurity(accountBaseCurrency != null ? accountBaseCurrency : InvestmentSecurityBuilder.newUSD());
		trade.setInvestmentSecurity(security);
		trade.setAccountingNotional(notional);
		trade.setExchangeRateToBase(exchangeRate);
		trade.setTradeDate(tradeDate);
		trade.setSettlementDate(tradeDate);

		trade.setTradeType(new TradeType());

		TradeFill fill = new TradeFill();
		fill.setTrade(trade);
		trade.addFill(fill);
		fill.setPaymentTotalPrice(InvestmentCalculatorUtils.calculateBaseAmount(notional, exchangeRate, trade.getClientInvestmentAccount()));
		fill.setNotionalTotalPrice(notional);

		return trade;
	}


	public static Trade newTrade_OneFill(boolean buy, InvestmentSecurity security, BigDecimal quantity, BigDecimal price, BigDecimal exchangeRate, Date tradeDate) {
		Trade trade = new Trade();
		trade.setBuy(buy);
		trade.setClientInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault());
		trade.setHoldingInvestmentAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault());
		trade.setPayingSecurity(InvestmentSecurityBuilder.newUSD());
		trade.setInvestmentSecurity(security);
		trade.setQuantityIntended(quantity);
		trade.setAverageUnitPrice(price);
		trade.setAccountingNotional(MathUtils.multiply(MathUtils.multiply(price, quantity), exchangeRate, 2));
		trade.setExchangeRateToBase(exchangeRate);
		trade.setTradeDate(tradeDate);
		trade.setSettlementDate(tradeDate);

		trade.setTradeType(new TradeType());
		AccountingAccountService accountingAccountService = AccountingTestObjectFactory.newAccountingAccountService();
		trade.getTradeType().setCommissionAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.EXPENSE_COMMISSION));
		trade.getTradeType().setFeeAccountingAccount(accountingAccountService.getAccountingAccountByName(AccountingAccount.EXPENSE_SEC_FEES));

		TradeFill fill = new TradeFill();
		fill.setTrade(trade);
		trade.addFill(fill);
		fill.setQuantity(quantity);
		fill.setPaymentUnitPrice(price);
		fill.setPaymentTotalPrice(trade.getAccountingNotional());
		fill.setNotionalUnitPrice(price);
		fill.setNotionalTotalPrice(trade.getAccountingNotional());

		return trade;
	}


	public static InvestmentCurrencyService getInvestmentCurrencyService() {
		return INVESTMENT_CURRENCY_SERVICE_LAZY.get();
	}
}
