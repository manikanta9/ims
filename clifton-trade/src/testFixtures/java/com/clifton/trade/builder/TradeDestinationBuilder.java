package com.clifton.trade.builder;


import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceBuilder;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurposeBuilder;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationType;
import com.clifton.trade.destination.TradeDestinationTypeBuilder;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import com.clifton.trade.destination.connection.TradeDestinationConnectionBuilder;


public class TradeDestinationBuilder {

	private TradeDestination tradeDestination;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeDestinationBuilder(TradeDestination tradeDestination) {
		this.tradeDestination = tradeDestination;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeDestinationBuilder createEmptyTradeDestination() {
		return new TradeDestinationBuilder(new TradeDestination());
	}


	/**
	 * Creates an "existing" tradedestination with id = 42.
	 */
	public static TradeDestinationBuilder createTradeDestination() {
		return createTradeDestination(new Short("42"));
	}


	/**
	 * Creates a new Trade: trade.isNewBean() == true
	 */
	public static TradeDestinationBuilder createNewTradeDestination() {
		return createTradeDestination(null);
	}


	public static TradeDestinationBuilder createTradeDestination(Short id) {
		TradeDestination innerTrade = new TradeDestination();
		innerTrade.setId(id);
		TradeDestinationType type = new TradeDestinationType();
		if (id != null) {
			type.setId((short) (10 + id));
		}
		innerTrade.setType(type);
		//innerTrade.setFixHandlingInstructions("AUTOMATIC");
		//innerTrade.setFixTargetSubId("TKTS");
		return new TradeDestinationBuilder(innerTrade);
	}


	public static TradeDestinationBuilder createRediTicket() {
		TradeDestination tradeDestination = new TradeDestination();

		tradeDestination.setId((short) 1);
		tradeDestination.setName("REDI Ticket");
		tradeDestination.setDescription("Goldman Sachs REDI trade ticket via FIX.");
		tradeDestination.setType(TradeDestinationTypeBuilder.createFixTrade().toTradeDestinationType());
		tradeDestination.setConnection(TradeDestinationConnectionBuilder.createRediFix().toTradeDestinationConnection());
		tradeDestination.setMarketDataSource(MarketDataSourceBuilder.createRedi().toMarketDataSource());
		tradeDestination.setMarketDataSourcePurpose(MarketDataSourcePurposeBuilder.createFixMessaging().toMarketDataSourcePurpose());
		tradeDestination.setDestinationOrder(10);
		tradeDestination.setFixTargetSubId("TKTS");
		tradeDestination.setFixHandlingInstructions("AUTOMATIC");

		return new TradeDestinationBuilder(tradeDestination);
	}


	public static TradeDestinationBuilder createEMSXDestination() {
		TradeDestination tradeDestination = new TradeDestination();

		tradeDestination.setId((short) 7);
		tradeDestination.setName("Bloomberg EMSX Ticket");
		tradeDestination.setDescription("Bloomberg trade ticket on EMSX via FIX.");
		tradeDestination.setType(TradeDestinationTypeBuilder.createFixTrade().toTradeDestinationType());
		tradeDestination.setConnection(TradeDestinationConnectionBuilder.createBloombergEMSXFIXConnection().toTradeDestinationConnection());
		tradeDestination.setMarketDataSource(MarketDataSourceBuilder.createBloomberg().toMarketDataSource());
		tradeDestination.setMarketDataSourcePurpose(MarketDataSourcePurposeBuilder.createFixEMSXMessaging().toMarketDataSourcePurpose());
		tradeDestination.setDestinationOrder(35);
		tradeDestination.setFixHandlingInstructions("MANUAL");

		return new TradeDestinationBuilder(tradeDestination);
	}


	public static TradeDestinationBuilder createBloombergDestination() {
		TradeDestination tradeDestination = new TradeDestination();

		tradeDestination.setId((short) 4);
		tradeDestination.setName("Bloomberg Ticket");
		tradeDestination.setDescription("Bloomberg trade ticket via FIX.");
		tradeDestination.setType(TradeDestinationTypeBuilder.createFixTrade().toTradeDestinationType());
		tradeDestination.setConnection(TradeDestinationConnectionBuilder.createBloombergFIXConnection().toTradeDestinationConnection());
		tradeDestination.setMarketDataSource(MarketDataSourceBuilder.createBloomberg().toMarketDataSource());
		tradeDestination.setMarketDataSourcePurpose(MarketDataSourcePurposeBuilder.createFixMessaging().toMarketDataSourcePurpose());
		tradeDestination.setDestinationOrder(30);
		tradeDestination.setFixHandlingInstructions("MANUAL");

		return new TradeDestinationBuilder(tradeDestination);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationBuilder withId(Short id) {
		getTradeDestination().setId(id);
		return this;
	}


	public TradeDestinationBuilder withName(String name) {
		getTradeDestination().setName(name);
		return this;
	}


	public TradeDestinationBuilder withDescription(String description) {
		getTradeDestination().setDescription(description);
		return this;
	}


	public TradeDestinationBuilder withType(TradeDestinationType type) {
		getTradeDestination().setType(type);
		return this;
	}


	public TradeDestinationBuilder withConnection(TradeDestinationConnection connection) {
		getTradeDestination().setConnection(connection);
		return this;
	}


	public TradeDestinationBuilder withMarketDataSource(MarketDataSource marketDataSource) {
		getTradeDestination().setMarketDataSource(marketDataSource);
		return this;
	}


	public TradeDestinationBuilder withMarketDataSourcePurpose(MarketDataSourcePurpose marketDataSourcePurpose) {
		getTradeDestination().setMarketDataSourcePurpose(marketDataSourcePurpose);
		return this;
	}


	public TradeDestinationBuilder withDestinationOrder(int destinationOrder) {
		getTradeDestination().setDestinationOrder(destinationOrder);
		return this;
	}


	public TradeDestinationBuilder withFixTargetSubId(String fixTargetSubId) {
		getTradeDestination().setFixTargetSubId(fixTargetSubId);
		return this;
	}


	public TradeDestinationBuilder withFixHandlingInstructions(String fixHandlingInstructions) {
		getTradeDestination().setFixHandlingInstructions(fixHandlingInstructions);
		return this;
	}


	public TradeDestination toTradeDestination() {
		return this.tradeDestination;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeDestination getTradeDestination() {
		return this.tradeDestination;
	}
}
