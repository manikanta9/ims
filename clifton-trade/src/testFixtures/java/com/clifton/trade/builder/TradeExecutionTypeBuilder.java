package com.clifton.trade.builder;

import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.InvestmentAccountTypeBuilder;
import com.clifton.system.condition.SystemCondition;
import com.clifton.trade.execution.TradeExecutionType;


/**
 * @author TerryS
 */
public final class TradeExecutionTypeBuilder {

	private boolean marketOnCloseTrade;
	private boolean bticTrade;
	private boolean clsTrade;
	private InvestmentAccountType requiredHoldingInvestmentAccountType;
	private InvestmentAccountType excludeHoldingInvestmentAccountType;
	private SystemCondition executingBrokerFilter;
	private String label;
	private String name;
	private String description;
	private Short id;


	private TradeExecutionTypeBuilder() {
	}


	public static TradeExecutionTypeBuilder moc() {
		return new TradeExecutionTypeBuilder()
				.withId((short) 2)
				.withName("MOC Trade")
				.withMarketOnCloseTrade(true);
	}


	public static TradeExecutionTypeBuilder btic() {
		return new TradeExecutionTypeBuilder()
				.withId((short) 4)
				.withName("BTIC Trade")
				.withBticTrade(true);
	}


	public static TradeExecutionTypeBuilder cls() {
		return new TradeExecutionTypeBuilder()
				.withId((short) 6)
				.withName("CLS")
				.withRequiredHoldingInvestmentAccountType(InvestmentAccountTypeBuilder.createOtcCls().toInvestmentAccountType())
				.withClsTrade(true);
	}


	public static TradeExecutionTypeBuilder nonCls() {
		return new TradeExecutionTypeBuilder()
				.withId((short) 7)
				.withName("Non-CLS");
	}


	public TradeExecutionTypeBuilder withMarketOnCloseTrade(boolean marketOnCloseTrade) {
		this.marketOnCloseTrade = marketOnCloseTrade;
		return this;
	}


	public TradeExecutionTypeBuilder withBticTrade(boolean bticTrade) {
		this.bticTrade = bticTrade;
		return this;
	}


	public TradeExecutionTypeBuilder withClsTrade(boolean clsTrade) {
		this.clsTrade = clsTrade;
		return this;
	}


	public TradeExecutionTypeBuilder withRequiredHoldingInvestmentAccountType(InvestmentAccountType requiredHoldingInvestmentAccountType) {
		this.requiredHoldingInvestmentAccountType = requiredHoldingInvestmentAccountType;
		return this;
	}


	public TradeExecutionTypeBuilder withExcludeHoldingInvestmentAccountType(InvestmentAccountType excludeHoldingInvestmentAccountType) {
		this.excludeHoldingInvestmentAccountType = excludeHoldingInvestmentAccountType;
		return this;
	}


	public TradeExecutionTypeBuilder withExecutingBrokerFilter(SystemCondition executingBrokerFilter) {
		this.executingBrokerFilter = executingBrokerFilter;
		return this;
	}


	public TradeExecutionTypeBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public TradeExecutionTypeBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public TradeExecutionTypeBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public TradeExecutionTypeBuilder withId(Short id) {
		this.id = id;
		return this;
	}


	public TradeExecutionType build() {
		TradeExecutionType tradeExecutionType = new TradeExecutionType();
		tradeExecutionType.setMarketOnCloseTrade(this.marketOnCloseTrade);
		tradeExecutionType.setBticTrade(this.bticTrade);
		tradeExecutionType.setClsTrade(this.clsTrade);
		tradeExecutionType.setRequiredHoldingInvestmentAccountType(this.requiredHoldingInvestmentAccountType);
		tradeExecutionType.setExcludeHoldingInvestmentAccountType(this.excludeHoldingInvestmentAccountType);
		tradeExecutionType.setExecutingBrokerFilter(this.executingBrokerFilter);
		tradeExecutionType.setLabel(this.label);
		tradeExecutionType.setName(this.name);
		tradeExecutionType.setDescription(this.description);
		tradeExecutionType.setId(this.id);
		return tradeExecutionType;
	}
}
