package com.clifton.trade.builder;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountBuilder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurposeBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeBuilder;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.group.InvestmentGroupBuilder;
import com.clifton.report.definition.Report;
import com.clifton.report.definition.ReportBuilder;
import com.clifton.trade.TradeExchangeRateDateSelectors;
import com.clifton.trade.TradeType;


public class TradeTypeBuilder {

	private TradeType tradeType;


	private TradeTypeBuilder(TradeType tradeType) {
		this.tradeType = tradeType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeTypeBuilder createEmptyTradeType() {
		return new TradeTypeBuilder(new TradeType());
	}


	public static TradeTypeBuilder forSecurity(InvestmentSecurity security) {
		ValidationUtils.assertTrue(security.getInstrument().getHierarchy().getInvestmentType() != null,
				"Security must be populated with a valid instrument, hierarchy, and investment type in order to build a trade type from it.");
		String investmentTypeName = security.getInstrument().getHierarchy().getInvestmentType().getName();
		TradeTypeBuilder builder = new TradeTypeBuilder(new TradeType())
				.withId((short) 1)
				.withName(investmentTypeName)
				.withHoldingAccountPurpose(InvestmentAccountRelationshipPurposeBuilder.forTradingInvestmentType(investmentTypeName).toInvestmentAccountRelationshipPurpose())
				.withCommissionAccountingAccount(AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.EXPENSE_COMMISSION))
				.withFeeAccountingAccount(AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.EXPENSE_SEC_FEES))
				.withCurrencyConventionFollowed(InvestmentUtils.isSecurityOfType(security, InvestmentType.CURRENCY));

		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.SWAPS)) {
			AccountingAccount revenueInterestLeg = AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_INTEREST_LEG);
			builder.withBuyAccrualAccountingAccount(revenueInterestLeg)
					.withSellAccrualAccountingAccount(revenueInterestLeg)
					.withSingleFillTrade(true);
		}

		if (InvestmentUtils.isSecurityOfTypeSubType(security, InvestmentTypeSubType.CREDIT_DEFAULT_SWAPS)) {
			AccountingAccount revenuePremiumLeg = AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_PREMIUM_LEG);
			builder.withAmountsSignAccountsForBuy(true)
					.withAccrualSignAccountsForBuy(true)
					.withBuyAccrualAccountingAccount(revenuePremiumLeg)
					.withSellAccrualAccountingAccount(revenuePremiumLeg);
		}
		else if (InvestmentUtils.isSecurityOfTypeSubType(security, InvestmentTypeSubType.INTEREST_RATE_SWAPS)) {
			AccountingAccount revenueInterestLeg = AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.REVENUE_INTEREST_LEG);
			builder.withAmountsSignAccountsForBuy(true)
					.withAccrualSignAccountsForBuy(true)
					.withBuyAccrualAccountingAccount(revenueInterestLeg)
					.withSellAccrualAccountingAccount(revenueInterestLeg);
		}
		else if (InvestmentUtils.isSecurityOfTypeSubType(security, InvestmentTypeSubType.TOTAL_RETURN_SWAPS)) {
			builder.withAccrualSignAccountsForBuy(true);
		}
		else if (InvestmentUtils.isSecurityOfType(security, InvestmentType.BONDS)) {
			AccountingAccount incomeAccount = AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.INTEREST_INCOME);
			AccountingAccount expenseAccount = AccountingTestObjectFactory.newAccountingAccountService().getAccountingAccountByName(AccountingAccount.EXPENSE_INTEREST_EXPENSE);
			builder.withBuyAccrualAccountingAccount(expenseAccount)
					.withSellAccrualAccountingAccount(incomeAccount)
					.withSingleFillTrade(true);
		}
		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.OPTIONS)) {
			builder.withCrossingZeroAllowed(false);
		}
		return builder;
	}


	public static TradeTypeBuilder createFunds() {
		TradeType tradeType = new TradeType();

		tradeType.setId((short) 8);
		tradeType.setName("Funds");
		tradeType.setDescription("Funds: ETF, CEF, Mutual Funds, etc.");
		tradeType.setInvestmentType(InvestmentTypeBuilder.createFunds().toInvestmentType());
		tradeType.setHoldingAccountPurpose(InvestmentAccountRelationshipPurposeBuilder.createTradingFunds().toInvestmentAccountRelationshipPurpose());
		tradeType.setCommissionAccountingAccount(AccountingAccountBuilder.createCommission().build());
		tradeType.setFeeAccountingAccount(AccountingAccountBuilder.createSecFees().build());
		tradeType.setTradeReport(ReportBuilder.createGenericTrade().toReport());
		tradeType.setFixSupported(true);
		tradeType.setCrossingZeroAllowed(true);

		return new TradeTypeBuilder(tradeType);
	}


	public static TradeTypeBuilder createStocks() {
		TradeType tradeType = new TradeType();

		tradeType.setId((short) 2);
		tradeType.setName("Stocks");
		tradeType.setDescription("Stocks");
		tradeType.setInvestmentType(InvestmentTypeBuilder.createStocks().toInvestmentType());
		tradeType.setHoldingAccountPurpose(InvestmentAccountRelationshipPurposeBuilder.createTradingStocks().toInvestmentAccountRelationshipPurpose());
		tradeType.setCommissionAccountingAccount(AccountingAccountBuilder.createCommission().build());
		tradeType.setFeeAccountingAccount(AccountingAccountBuilder.createSecFees().build());
		tradeType.setTradeReport(ReportBuilder.createGenericTrade().toReport());
		tradeType.setCrossingZeroAllowed(true);
		tradeType.setCollateralSupported(true);
		tradeType.setFixSupported(true);
		tradeType.setTradeExecutionTypeSupported(true);
		tradeType.setCrossingZeroAllowed(true);

		return new TradeTypeBuilder(tradeType);
	}


	public static TradeTypeBuilder createFutures() {
		TradeType tradeType = new TradeType();

		tradeType.setId((short) 1);
		tradeType.setName("Futures");
		tradeType.setDescription("Futures");
		tradeType.setInvestmentType(InvestmentTypeBuilder.createFutures().toInvestmentType());
		tradeType.setHoldingAccountPurpose(InvestmentAccountRelationshipPurposeBuilder.createTradingFutures().toInvestmentAccountRelationshipPurpose());
		tradeType.setCommissionAccountingAccount(AccountingAccountBuilder.createCommission().build());
		tradeType.setFeeAccountingAccount(AccountingAccountBuilder.createNfaFees().build());
		tradeType.setTradeReport(ReportBuilder.createGenericTrade().toReport());
		tradeType.setFixSupported(true);
		tradeType.setCrossingZeroAllowed(true);

		return new TradeTypeBuilder(tradeType);
	}


	public static TradeTypeBuilder createClearedCDS() {
		TradeType tradeType = new TradeType();

		tradeType.setId((short) 11);
		tradeType.setName("Cleared: CDS");
		tradeType.setDescription("Centrally Cleared Credit Default Swap trades");
		tradeType.setInvestmentType(InvestmentTypeBuilder.createSwaps().toInvestmentType());
		tradeType.setInvestmentGroup(InvestmentGroupBuilder.createSwapsCDSCleared().toInvestmentGroup());
		tradeType.setHoldingAccountPurpose(InvestmentAccountRelationshipPurposeBuilder.createTradingSwaps().toInvestmentAccountRelationshipPurpose());
		tradeType.setCommissionAccountingAccount(AccountingAccountBuilder.createCommission().build());
		tradeType.setFeeAccountingAccount(AccountingAccountBuilder.createTransactionFee().build());
		tradeType.setBuyAccrualAccountingAccount(AccountingAccountBuilder.createPremiumLeg().build());
		tradeType.setSellAccrualAccountingAccount(AccountingAccountBuilder.createPremiumLeg().build());
		tradeType.setTradeReport(ReportBuilder.createGenericTrade().toReport());
		tradeType.setExchangeRateDateSelector(TradeExchangeRateDateSelectors.TRADE_DATE);
		tradeType.setSingleFillTrade(true);
		tradeType.setAmountsSignAccountsForBuy(true);
		tradeType.setAccrualSignAccountsForBuy(true);
		tradeType.setBrokerSelectionAtExecutionAllowed(true);
		tradeType.setCrossingZeroAllowed(true);
		tradeType.setFixSupported(true);

		return new TradeTypeBuilder(tradeType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeTypeBuilder withId(short id) {
		getTradeType().setId(id);
		return this;
	}


	public TradeTypeBuilder withName(String name) {
		getTradeType().setName(name);
		return this;
	}


	public TradeTypeBuilder withDescription(String description) {
		getTradeType().setDescription(description);
		return this;
	}


	public TradeTypeBuilder withInvestmentType(InvestmentType investmentType) {
		getTradeType().setInvestmentType(investmentType);
		return this;
	}


	public TradeTypeBuilder withHoldingAccountPurpose(InvestmentAccountRelationshipPurpose holdingAccountPurpose) {
		getTradeType().setHoldingAccountPurpose(holdingAccountPurpose);
		return this;
	}


	public TradeTypeBuilder withCommissionAccountingAccount(AccountingAccount commissionAccountingAccount) {
		getTradeType().setCommissionAccountingAccount(commissionAccountingAccount);
		return this;
	}


	public TradeTypeBuilder withFeeAccountingAccount(AccountingAccount feeAccountingAccount) {
		getTradeType().setFeeAccountingAccount(feeAccountingAccount);
		return this;
	}


	public TradeTypeBuilder withTradeReport(Report tradeReport) {
		getTradeType().setTradeReport(tradeReport);
		return this;
	}


	public TradeTypeBuilder withFixSupported(boolean fixSupported) {
		getTradeType().setFixSupported(fixSupported);
		return this;
	}


	public TradeTypeBuilder withCrossingZeroAllowed(boolean crossingZeroAllowed) {
		getTradeType().setCrossingZeroAllowed(crossingZeroAllowed);
		return this;
	}


	public TradeTypeBuilder withBuyAccrualAccountingAccount(AccountingAccount buyAccrualAccountingAccount) {
		getTradeType().setBuyAccrualAccountingAccount(buyAccrualAccountingAccount);
		return this;
	}


	public TradeTypeBuilder withSellAccrualAccountingAccount(AccountingAccount sellAccrualAccountingAccount) {
		getTradeType().setSellAccrualAccountingAccount(sellAccrualAccountingAccount);
		return this;
	}


	public TradeTypeBuilder withAccrualSignAccountsForBuy(boolean accrualSignAccountsForBuy) {
		getTradeType().setAccrualSignAccountsForBuy(accrualSignAccountsForBuy);
		return this;
	}


	public TradeTypeBuilder withAmountsSignAccountsForBuy(boolean amountsSignAccountsForBuy) {
		getTradeType().setAmountsSignAccountsForBuy(amountsSignAccountsForBuy);
		return this;
	}


	public TradeTypeBuilder withSingleFillTrade(boolean singleFillTrade) {
		getTradeType().setSingleFillTrade(singleFillTrade);
		return this;
	}


	public TradeTypeBuilder withCurrencyConventionFollowed(boolean currencyConventionFollowed) {
		getTradeType().setCurrencyConventionFollowed(currencyConventionFollowed);
		return this;
	}


	public TradeType toTradeType() {
		return this.tradeType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeType getTradeType() {
		return this.tradeType;
	}
}
