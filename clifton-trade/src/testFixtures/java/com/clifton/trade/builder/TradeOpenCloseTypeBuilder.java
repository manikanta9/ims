package com.clifton.trade.builder;


import com.clifton.trade.TradeOpenCloseType;


/**
 * @author TerryS
 */
public final class TradeOpenCloseTypeBuilder {

	private boolean buy;
	private boolean open;
	private boolean close;
	private String label;
	private String name;
	private String description;
	private Short id;


	private TradeOpenCloseTypeBuilder() {
	}


	public static TradeOpenCloseTypeBuilder buy() {
		return new TradeOpenCloseTypeBuilder()
				.withId((short) 1)
				.withName("Buy")
				.withBuy(true)
				.withOpen(true)
				.withClose(true);
	}

	public static TradeOpenCloseTypeBuilder buyToOpen() {
		return new TradeOpenCloseTypeBuilder()
				.withId((short) 2)
				.withName("Buy to Open")
				.withBuy(true)
				.withOpen(true);
	}

	public static TradeOpenCloseTypeBuilder buyToClose() {
		return new TradeOpenCloseTypeBuilder()
				.withId((short) 3)
				.withName("Buy to Close")
				.withBuy(true)
				.withClose(true);
	}

	public static TradeOpenCloseTypeBuilder sell() {
		return new TradeOpenCloseTypeBuilder()
				.withId((short) 4)
				.withName("Sell")
				.withOpen(true)
				.withClose(true);
	}

	public static TradeOpenCloseTypeBuilder sellToOpen() {
		return new TradeOpenCloseTypeBuilder()
				.withId((short) 5)
				.withName("Sell to Open")
				.withOpen(true);
	}

	public static TradeOpenCloseTypeBuilder sellToClose() {
		return new TradeOpenCloseTypeBuilder()
				.withId((short) 6)
				.withName("Sell to Close")
				.withClose(true);
	}


	public TradeOpenCloseTypeBuilder withBuy(boolean buy) {
		this.buy = buy;
		return this;
	}


	public TradeOpenCloseTypeBuilder withOpen(boolean open) {
		this.open = open;
		return this;
	}


	public TradeOpenCloseTypeBuilder withClose(boolean close) {
		this.close = close;
		return this;
	}


	public TradeOpenCloseTypeBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public TradeOpenCloseTypeBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public TradeOpenCloseTypeBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public TradeOpenCloseTypeBuilder withId(Short id) {
		this.id = id;
		return this;
	}


	public TradeOpenCloseType build() {
		TradeOpenCloseType tradeOpenCloseType = new TradeOpenCloseType();
		tradeOpenCloseType.setBuy(this.buy);
		tradeOpenCloseType.setOpen(this.open);
		tradeOpenCloseType.setClose(this.close);
		tradeOpenCloseType.setLabel(this.label);
		tradeOpenCloseType.setName(this.name);
		tradeOpenCloseType.setDescription(this.description);
		tradeOpenCloseType.setId(this.id);
		return tradeOpenCloseType;
	}
}
