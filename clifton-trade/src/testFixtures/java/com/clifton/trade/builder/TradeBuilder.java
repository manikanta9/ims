package com.clifton.trade.builder;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.group.TradeGroup;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;


public class TradeBuilder {

	public static final Lazy<Random> RANDOM_LAZY = new Lazy<>(Random::new);
	public static final TradeOpenCloseType BUY, BUY_TO_OPEN, BUY_TO_CLOSE, SELL, SELL_TO_OPEN, SELL_TO_CLOSE;


	static {
		BUY = new TradeOpenCloseType();
		BUY.setId((short) 1);
		BUY.setName("Buy");
		BUY.setBuy(true);
		BUY.setOpen(true);
		BUY.setClose(true);

		BUY_TO_OPEN = new TradeOpenCloseType();
		BUY_TO_OPEN.setId((short) 2);
		BUY_TO_OPEN.setName("Buy to Open");
		BUY_TO_OPEN.setBuy(true);
		BUY_TO_OPEN.setOpen(true);
		BUY_TO_OPEN.setClose(false);

		BUY_TO_CLOSE = new TradeOpenCloseType();
		BUY_TO_CLOSE.setId((short) 3);
		BUY_TO_CLOSE.setName("Buy to Close");
		BUY_TO_CLOSE.setBuy(true);
		BUY_TO_CLOSE.setOpen(false);
		BUY_TO_CLOSE.setClose(true);

		SELL = new TradeOpenCloseType();
		SELL.setId((short) 4);
		SELL.setName("Sell");
		SELL.setBuy(false);
		SELL.setOpen(true);
		SELL.setClose(true);

		SELL_TO_OPEN = new TradeOpenCloseType();
		SELL_TO_OPEN.setId((short) 5);
		SELL_TO_OPEN.setName("Sell to Open");
		SELL_TO_OPEN.setBuy(false);
		SELL_TO_OPEN.setOpen(true);
		SELL_TO_OPEN.setClose(false);

		SELL_TO_CLOSE = new TradeOpenCloseType();
		SELL_TO_CLOSE.setId((short) 6);
		SELL_TO_CLOSE.setName("Sell to Close");
		SELL_TO_CLOSE.setBuy(false);
		SELL_TO_CLOSE.setOpen(false);
		SELL_TO_CLOSE.setClose(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private Trade trade;


	private TradeBuilder(Trade trade) {
		this.trade = trade;
	}


	private TradeBuilder() {
		this(new Trade());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static final Date DEFAULT_TRADE_DATE = DateUtils.toDate("10/17/2012");


	public static TradeBuilder createEmptyTrade() {
		return new TradeBuilder(new Trade());
	}


	/**
	 * Creates a new trade builder with defaults populated for major fields: client/holding accounts, paying security, open close type (SELL), trader, etc.
	 */
	public static TradeBuilder createNewTrade() {
		return new TradeBuilder()
				.sell()
				.onTradeDate(DEFAULT_TRADE_DATE)
				.withExecutingBroker(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany())
				.withFeeAmount("0")
				.withExchangeRate("1")
				.withExpectedUnitPrice("1")
				.withTradeDestination(TradeDestinationBuilder.createTradeDestination((short) 1).toTradeDestination())
				.forClient(InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault())
				.forHoldingAccount(InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault())
				.forTrader("gFawkes");
	}


	/**
	 * Creates a new TradeBuilder for the specified security. Populates defaults for major fields using {@link #createNewTrade()}.
	 * Populates and configures {@link TradeType} based on the type of the specified security.
	 */
	public static TradeBuilder newTradeForSecurity(InvestmentSecurity security) {
		return createNewTrade().forSecurity(security);
	}


	public static TradeBuilder toBuilder(Trade trade) {
		if (trade == null) {
			throw new IllegalArgumentException("Unable to create a TradeBuilder from a null Trade.");
		}
		return new TradeBuilder(trade);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Trade createFills() {
		return createFills(false);
	}


	public Trade createFills(boolean populateFillIds) {
		BigDecimal totalNotional = BigDecimal.ZERO;
		TradeOpenCloseType fillOpenCloseType = getTrade().getOpenCloseType().isBuy() ? TradeBuilder.BUY : TradeBuilder.SELL;
		for (TradeFill fill : CollectionUtils.getIterable(getTrade().getTradeFillList())) {
			if (populateFillIds) {
				fill.setId(10000 + RANDOM_LAZY.get().nextInt() * 100);
			}
			if (InvestmentType.FUTURES.equals(getTrade().getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName())) {
				fill.setPaymentUnitPrice(BigDecimal.ZERO);
			}
			else if (fill.getPaymentUnitPrice() == null) {
				fill.setPaymentUnitPrice(getTrade().getExpectedUnitPrice());
			}
			if (fill.getNotionalUnitPrice() == null) {
				fill.setNotionalUnitPrice(getTrade().getExpectedUnitPrice());
			}
			InvestmentCalculator calculator = new InvestmentCalculatorImpl();
			fill.setPaymentTotalPrice(calculator.calculateNotional(fill.getTrade().getInvestmentSecurity(), fill.getPaymentUnitPrice(), fill.getQuantity(), BigDecimal.ONE));
			fill.setNotionalTotalPrice(calculator.calculateNotional(fill.getTrade().getInvestmentSecurity(), fill.getNotionalUnitPrice(), fill.getQuantity(), BigDecimal.ONE));
			if (fill.getOpenCloseType() == null) {
				fill.setOpenCloseType(fillOpenCloseType);
			}
			totalNotional = MathUtils.add(totalNotional, fill.getNotionalTotalPrice());
		}
		getTrade().setAccountingNotional(totalNotional);
		return getTrade();
	}


	public TradeBuilder buy() {
		getTrade().setBuy(true);
		getTrade().setOpenCloseType(BUY);
		return this;
	}


	public TradeBuilder sell() {
		getTrade().setBuy(false);
		getTrade().setOpenCloseType(SELL);
		return this;
	}


	public TradeBuilder withId(Integer id) {
		getTrade().setId(id);
		return this;
	}


	public TradeBuilder withOpenCloseType(TradeOpenCloseType tradeOpenCloseType) {
		getTrade().setOpenCloseType(tradeOpenCloseType);
		return this;
	}


	public TradeBuilder withTradeGroup(TradeGroup tradeGroup) {
		getTrade().setTradeGroup(tradeGroup);
		return this;
	}


	public TradeBuilder inUSD() {
		getTrade().setPayingSecurity(InvestmentSecurityBuilder.newUSD());
		return this;
	}


	public TradeBuilder inEUR() {
		getTrade().setPayingSecurity(InvestmentSecurityBuilder.newEUR());
		return this;
	}


	public TradeBuilder addFillWithQuantity(int quantity) {
		return addFillWithQuantity(new BigDecimal(quantity));
	}


	public TradeBuilder addFillWithQuantity(String quantity) {
		return addFillWithQuantity(new BigDecimal(quantity));
	}


	private TradeBuilder addFillWithQuantity(BigDecimal quantity) {
		TradeFill fill = new TradeFill();
		fill.setQuantity(quantity);
		fill.setTrade(getTrade());
		getTrade().addFill(fill);
		withQuantityIntended(quantity);
		return this;
	}


	/**
	 * Adds a new {@TradeFill} to this trade using the specified price and quantity.
	 * Increments the quantity on the trade and recalculates and updates accounting notional on the trade.
	 */
	public TradeBuilder addFill(String quantity, String price) {
		return addFill(new BigDecimal(quantity), new BigDecimal(price));
	}


	public TradeBuilder addFill(TradeFill tradeFill) {
		tradeFill.setTrade(getTrade());
		getTrade().addFill(tradeFill);
		if (getTrade().getQuantityIntended() != null) {
			withQuantityIntended(MathUtils.add(getTrade().getQuantityIntended(), tradeFill.getQuantity()));
		}
		else {
			withQuantityIntended(tradeFill.getQuantity());
		}
		createFills();
		return this;
	}


	/**
	 * Adds a new {@TradeFill} to this trade using the specified price and quantity.
	 * Increments the quantity on the trade and recalculates and updates accounting notional on the trade.
	 */
	private TradeBuilder addFill(BigDecimal quantity, BigDecimal price) {
		TradeFill fill = new TradeFill();
		fill.setQuantity(quantity);
		fill.setNotionalUnitPrice(price);
		if (!getTrade().getInvestmentSecurity().getInstrument().getHierarchy().isNoPaymentOnOpen()) {
			fill.setPaymentUnitPrice(price);
		}
		fill.setTrade(getTrade());
		getTrade().addFill(fill);
		if (getTrade().getQuantityIntended() != null) {
			withQuantityIntended(MathUtils.add(getTrade().getQuantityIntended(), quantity));
		}
		else {
			withQuantityIntended(quantity);
		}
		createFills();
		return this;
	}


	public TradeBuilder addCurrencyFill(String accountingNotionalString) {
		BigDecimal accountingNotional = new BigDecimal(accountingNotionalString);
		AssertUtils.assertEmpty(getTrade().getTradeFillList(), "Currency Trade can only have one fill.");
		AssertUtils.assertNotNull(getTrade().getExchangeRateToBase(), "Exchange Rate is required on the trade.");
		AssertUtils.assertTrue(getTrade().getInvestmentSecurity().isCurrency(), "Security for Currency Trade must be a Currency.");

		TradeFill fill = new TradeFill();
		fill.setTrade(getTrade());

		BigDecimal exchangeRate = getTrade().getExchangeRateToBase();
		if (getTrade().getTradeType().isCurrencyConventionFollowed()) {
			if (!TradeTestObjectFactory.getInvestmentCurrencyService().isInvestmentCurrencyMultiplyConvention(getTrade().getInvestmentSecurity().getSymbol(), getTrade().getSettlementCurrency().getSymbol())) {
				exchangeRate = MathUtils.divide(BigDecimal.ONE, exchangeRate);
			}
		}

		fill.setPaymentTotalPrice(InvestmentCalculatorUtils.calculateBaseAmount(accountingNotional, exchangeRate, getTrade().getClientInvestmentAccount()));
		fill.setNotionalTotalPrice(accountingNotional);
		getTrade().addFill(fill);

		getTrade().setAccountingNotional(accountingNotional);

		return this;
	}


	public TradeBuilder withQuantityIntended(int quantityIntended) {
		return withQuantityIntended(new BigDecimal(quantityIntended));
	}


	public TradeBuilder withQuantityIntended(String quantityIntended) {
		return withQuantityIntended(new BigDecimal(quantityIntended));
	}


	private TradeBuilder withQuantityIntended(BigDecimal quantityIntended) {
		getTrade().setQuantityIntended(quantityIntended);
		if (getTrade().getOriginalFace() == null) {
			getTrade().setOriginalFace(quantityIntended);
		}
		return this;
	}


	public TradeBuilder withTradeExecutionType(TradeExecutionType tradeExecutionType) {
		getTrade().setTradeExecutionType(tradeExecutionType);
		return this;
	}


	public TradeBuilder withOriginalFace(String originalFace) {
		getTrade().setOriginalFace(new BigDecimal(originalFace));
		return this;
	}


	public TradeBuilder withCurrentFactor(String currentFactor) {
		getTrade().setCurrentFactor(new BigDecimal(currentFactor));
		return this;
	}


	public TradeBuilder withSecurityEndDate(Date endDate) {
		InvestmentSecurity security = getTrade().getInvestmentSecurity();
		if (security != null) {
			security.setEndDate(endDate);
		}
		return this;
	}


	public TradeBuilder withSecurityExchange(String name, String marketIdentifierCode) {
		InvestmentSecurity security = getTrade().getInvestmentSecurity();
		if (security != null) {
			InvestmentExchange exchange = new InvestmentExchange();
			exchange.setName(name);
			exchange.setMarketIdentifierCode(marketIdentifierCode);
			security.getInstrument().setExchange(exchange);
		}
		return this;
	}


	public TradeBuilder forSecurity(InvestmentSecurity security) {
		getTrade().setInvestmentSecurity(security);
		return withTradeType(TradeTypeBuilder.forSecurity(security).toTradeType());
	}


	public TradeBuilder forClient(InvestmentAccount clientAccount) {
		getTrade().setClientInvestmentAccount(clientAccount);
		return (clientAccount.getBaseCurrency() == null) ? this : withPayingSecurity(clientAccount.getBaseCurrency());
	}


	public TradeBuilder forClientAccountInBaseCurrency(InvestmentSecurity baseCurrency) {
		if (getTrade().getClientInvestmentAccount() != null) {
			getTrade().getClientInvestmentAccount().setBaseCurrency(baseCurrency);
			return withPayingSecurity(baseCurrency);
		}
		InvestmentAccount clientAccount = InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault();
		clientAccount.setBaseCurrency(baseCurrency);
		return forClient(clientAccount);
	}


	public TradeBuilder forHoldingAccount(InvestmentAccount holdingAccount) {
		getTrade().setHoldingInvestmentAccount(holdingAccount);
		return this;
	}


	public TradeBuilder forHoldingAccountIssuingCompany(BusinessCompany issuingCompany) {
		if (getTrade().getHoldingInvestmentAccount() != null) {
			getTrade().getHoldingInvestmentAccount().setIssuingCompany(issuingCompany);
			return this;
		}
		InvestmentAccount holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault();
		holdingAccount.setIssuingCompany(issuingCompany);
		return forHoldingAccount(holdingAccount);
	}


	public TradeBuilder withExecutingBroker(BusinessCompany executingBroker) {
		getTrade().setExecutingBrokerCompany(executingBroker);
		return this;
	}


	public TradeBuilder withIssuingCompany(BusinessCompany issuingCompany) {
		getTrade().getHoldingInvestmentAccount().setIssuingCompany(issuingCompany);
		return this;
	}


	public TradeBuilder withInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		getTrade().setInvestmentSecurity(investmentSecurity);
		return this;
	}


	public TradeBuilder withClientInvestmentAccount(InvestmentAccount investmentAccount) {
		getTrade().setClientInvestmentAccount(investmentAccount);
		return this;
	}


	public TradeBuilder withHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		getTrade().setHoldingInvestmentAccount(holdingInvestmentAccount);
		return this;
	}


	public TradeBuilder withNote(String note) {
		getTrade().setDescription(note);
		return this;
	}


	public TradeBuilder withPayingSecurity(InvestmentSecurity payingSecurity) {
		getTrade().setPayingSecurity(payingSecurity);
		return this;
	}


	public TradeBuilder withExchangeRate(String exchangeRateToBase) {
		getTrade().setExchangeRateToBase(new BigDecimal(exchangeRateToBase));
		return this;
	}


	public TradeBuilder withAccrualAmount1(String accrualAmount1) {
		getTrade().setAccrualAmount1(new BigDecimal(accrualAmount1));
		return this;
	}


	public TradeBuilder withAccrualAmount2(String accrualAmount2) {
		getTrade().setAccrualAmount2(new BigDecimal(accrualAmount2));
		return this;
	}


	public TradeBuilder withAmountsSignAccountsForBuy(boolean amountsSignAccountsForBuy) {
		getTrade().getTradeType().setAmountsSignAccountsForBuy(amountsSignAccountsForBuy);
		return this;
	}


	public TradeBuilder withAccrualSignAccountsForBuy(boolean accrualSignAccountsForBuy) {
		getTrade().getTradeType().setAccrualSignAccountsForBuy(accrualSignAccountsForBuy);
		return this;
	}


	public TradeBuilder withCollateralTrade(boolean collateralTrade) {
		getTrade().setCollateralTrade(collateralTrade);
		return this;
	}


	public TradeBuilder withAccountingNotional(String accountingNotional) {
		getTrade().setAccountingNotional(new BigDecimal(accountingNotional));
		return this;
	}


	public TradeBuilder withAverageUnitPrice(String averageUnitPrice) {
		getTrade().setAverageUnitPrice(new BigDecimal(averageUnitPrice));
		return this;
	}


	public TradeBuilder withCommissionPerUnit(double commission) {
		getTrade().setCommissionPerUnit(new BigDecimal(Double.toString(commission)));
		return this;
	}


	public TradeBuilder withCommissionPerUnit(String commissionPerUnit) {
		getTrade().setCommissionPerUnit(new BigDecimal(commissionPerUnit));
		return this;
	}


	public TradeBuilder withFeeAmount(String feeAmount) {
		getTrade().setFeeAmount(new BigDecimal(feeAmount));
		return this;
	}


	public TradeBuilder withTotalCommission(String commission) {
		getTrade().setCommissionAmount(new BigDecimal(commission));
		return this;
	}


	public TradeBuilder withTradeDestination(TradeDestination destination) {
		getTrade().setTradeDestination(destination);
		return this;
	}


	public TradeBuilder onTradeDate(String tradeDateString) {
		return onTradeDate(DateUtils.toDate(tradeDateString));
	}


	public TradeBuilder onTradeDate(Date tradeDate) {
		getTrade().setTradeDate(tradeDate);
		if (getTrade().getSettlementDate() == null || getTrade().getSettlementDate().equals(DEFAULT_TRADE_DATE)) {
			getTrade().setSettlementDate(tradeDate);
		}
		return this;
	}


	public TradeBuilder withSettlementDate(String settlementDateString) {
		return withSettlementDate(DateUtils.toDate(settlementDateString));
	}


	public TradeBuilder withTradeDate(Date tradeDate) {
		getTrade().setTradeDate(tradeDate);
		return this;
	}


	public TradeBuilder withSettlementDate(Date settlementDate) {
		getTrade().setSettlementDate(settlementDate);
		return this;
	}


	public TradeBuilder withActualSettlementDate(Date actualSettlementDate) {
		getTrade().setActualSettlementDate(actualSettlementDate);
		return this;
	}


	public TradeBuilder withTradeType(TradeType tradeType) {
		getTrade().setTradeType(tradeType);
		return this;
	}


	public TradeBuilder forTrader(String traderName) {
		SecurityUser securityUser = new SecurityUser();
		securityUser.setUserName(traderName);
		securityUser.setId((short) 1);
		getTrade().setTraderUser(securityUser);
		return this;
	}


	public TradeBuilder withTraderUser(SecurityUser traderUser) {
		this.trade.setTraderUser(traderUser);
		return this;
	}


	public TradeBuilder withExpectedUnitPrice(String unitPrice) {
		getTrade().setExpectedUnitPrice(new BigDecimal(unitPrice));
		return this;
	}


	public Trade build() {
		// clone local trade to allow reusing the builder for more than one trade
		return BeanUtils.cloneBean(getTrade(), true, true);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private Trade getTrade() {
		return this.trade;
	}
}
