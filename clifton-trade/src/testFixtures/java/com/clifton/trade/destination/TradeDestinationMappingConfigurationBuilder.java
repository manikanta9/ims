package com.clifton.trade.destination;

import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;


public class TradeDestinationMappingConfigurationBuilder {

	private TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration;


	private TradeDestinationMappingConfigurationBuilder(TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration) {
		this.tradeDestinationMappingConfiguration = tradeDestinationMappingConfiguration;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeDestinationMappingConfigurationBuilder createEmptyTradeDestinationMappingConfigurationBuilder() {
		return new TradeDestinationMappingConfigurationBuilder(new TradeDestinationMappingConfiguration());
	}


	public static TradeDestinationMappingConfigurationBuilder createGoldmanSachsEquityConfigurationBuilder() {
		TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration = new TradeDestinationMappingConfiguration();
		tradeDestinationMappingConfiguration.setId(10);
		tradeDestinationMappingConfiguration.setName("Goldman Sachs Equity Configuration");
		tradeDestinationMappingConfiguration.setDescription("This group is configured for post-trade allocations and to send the Executing Account along with commission information with Fix Commission Type: PER_UNIT, FIX Process Code: SOFT_DOLLAR, and a Default Commission Per Unit of $0.01.");
		tradeDestinationMappingConfiguration.setFixCommissionType(CommissionTypes.PER_UNIT);
		tradeDestinationMappingConfiguration.setFixProcessCode(ProcessCodes.SOFT_DOLLAR);
		tradeDestinationMappingConfiguration.setFixDefaultCommissionPerUnit(new BigDecimal("0.01"));
		tradeDestinationMappingConfiguration.setFixPostTradeAllocation(true);
		tradeDestinationMappingConfiguration.setFixExecutingAccountUsed(true);
		return new TradeDestinationMappingConfigurationBuilder(tradeDestinationMappingConfiguration);
	}


	public static TradeDestinationMappingConfigurationBuilder createDefaultPostTradeAllocationConfigurationBuilder() {
		TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration = new TradeDestinationMappingConfiguration();
		tradeDestinationMappingConfiguration.setId(12);
		tradeDestinationMappingConfiguration.setName("Default Post Trade Allocation Configuration");
		tradeDestinationMappingConfiguration.setDescription("This group is configured for post-trade allocations, and supports trade grouping. This configuration expects a FIX allocation report from broker.");
		tradeDestinationMappingConfiguration.setFixPostTradeAllocation(true);
		tradeDestinationMappingConfiguration.setFixAllocationReportExpected(true);
		tradeDestinationMappingConfiguration.setFixTradeGroupingAllowed(true);
		return new TradeDestinationMappingConfigurationBuilder(tradeDestinationMappingConfiguration);
	}


	public static TradeDestinationMappingConfigurationBuilder createBloombergFITCDSConfigurationBuilder() {
		TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration = new TradeDestinationMappingConfiguration();
		tradeDestinationMappingConfiguration.setId(4);
		tradeDestinationMappingConfiguration.setName("Bloomberg FIT CDS Configuration");
		tradeDestinationMappingConfiguration.setDescription("Setting for trading CDS on Bloomberg CDS");
		tradeDestinationMappingConfiguration.setFixTradeGroupingAllowed(true);
		tradeDestinationMappingConfiguration.setSendRestrictiveBrokerList(true);
		tradeDestinationMappingConfiguration.setFixSendClearingFirmLEIWithAllocation(true);
		return new TradeDestinationMappingConfigurationBuilder(tradeDestinationMappingConfiguration);
	}


	public static TradeDestinationMappingConfigurationBuilder createBloombergEMSXMappingConfigurationBuilder() {
		TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration = new TradeDestinationMappingConfiguration();
		tradeDestinationMappingConfiguration.setId(21);
		tradeDestinationMappingConfiguration.setName("Bloomberg EMSX Futures, Funds, Note and Stocks Configuration");
		tradeDestinationMappingConfiguration.setDescription("Trade grouping allowed, no other settings.");
		tradeDestinationMappingConfiguration.setFixTradeGroupingAllowed(true);
		return new TradeDestinationMappingConfigurationBuilder(tradeDestinationMappingConfiguration);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationMappingConfigurationBuilder withId(Integer id) {
		getTradeDestinationMappingConfiguration().setId(id);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withLabel(String label) {
		getTradeDestinationMappingConfiguration().setLabel(label);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withName(String name) {
		getTradeDestinationMappingConfiguration().setName(name);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withDescription(String description) {
		getTradeDestinationMappingConfiguration().setDescription(description);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixPostTradeAllocation(boolean fixPostTradeAllocation) {
		getTradeDestinationMappingConfiguration().setFixPostTradeAllocation(fixPostTradeAllocation);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixDisablePreTradeAllocations(boolean fixDisablePreTradeAllocations) {
		getTradeDestinationMappingConfiguration().setFixDisablePreTradeAllocations(fixDisablePreTradeAllocations);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixTradeGroupingAllowed(boolean fixTradeGroupingAllowed) {
		getTradeDestinationMappingConfiguration().setFixTradeGroupingAllowed(fixTradeGroupingAllowed);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixTradeGroupingByHoldingAccount(boolean fixTradeGroupingByHoldingAccount) {
		getTradeDestinationMappingConfiguration().setFixTradeGroupingByHoldingAccount(fixTradeGroupingByHoldingAccount);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixExecutingAccountUsed(boolean fixExecutingAccountUsed) {
		getTradeDestinationMappingConfiguration().setFixExecutingAccountUsed(fixExecutingAccountUsed);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixAllocationReportExpected(boolean fixAllocationReportExpected) {
		getTradeDestinationMappingConfiguration().setFixAllocationReportExpected(fixAllocationReportExpected);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixCommissionType(CommissionTypes fixCommissionType) {
		getTradeDestinationMappingConfiguration().setFixCommissionType(fixCommissionType);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixProcessCode(ProcessCodes fixProcessCode) {
		getTradeDestinationMappingConfiguration().setFixProcessCode(fixProcessCode);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixDefaultCommissionPerUnit(BigDecimal fixDefaultCommissionPerUnit) {
		getTradeDestinationMappingConfiguration().setFixDefaultCommissionPerUnit(fixDefaultCommissionPerUnit);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixAllocationType(AllocationTypes fixAllocationType) {
		getTradeDestinationMappingConfiguration().setFixAllocationType(fixAllocationType);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixAllocationCompleteWhenStatusIsReceived(boolean fixAllocationCompleteWhenStatusIsReceived) {
		getTradeDestinationMappingConfiguration().setFixAllocationCompleteWhenStatusIsReceived(fixAllocationCompleteWhenStatusIsReceived);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixMICExchangeCodeRequiredOnAllocationMessages(boolean fixMICExchangeCodeRequiredOnAllocationMessages) {
		getTradeDestinationMappingConfiguration().setFixMICExchangeCodeRequiredOnAllocationMessages(fixMICExchangeCodeRequiredOnAllocationMessages);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixSecondaryClientOrderIdUsedForAllocations(boolean fixSecondaryClientOrderIdUsedForAllocations) {
		getTradeDestinationMappingConfiguration().setFixSecondaryClientOrderIdUsedForAllocations(fixSecondaryClientOrderIdUsedForAllocations);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixSendTopAccountOnAllocationMessages(boolean fixSendTopAccountOnAllocationMessages) {
		getTradeDestinationMappingConfiguration().setFixSendTopAccountOnAllocationMessages(fixSendTopAccountOnAllocationMessages);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixMICExchangeCodeRequiredOnOrderMessages(boolean fixMICExchangeCodeRequiredOnOrderMessages) {
		getTradeDestinationMappingConfiguration().setFixMICExchangeCodeRequiredOnOrderMessages(fixMICExchangeCodeRequiredOnOrderMessages);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixSendBrokerPartyIdsOnAllocationMessages(boolean fixSendBrokerPartyIdsOnAllocationMessages) {
		getTradeDestinationMappingConfiguration().setFixSendBrokerPartyIdsOnAllocationMessages(fixSendBrokerPartyIdsOnAllocationMessages);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withSendClientAccountWithOrder(boolean sendClientAccountWithOrder) {
		getTradeDestinationMappingConfiguration().setSendClientAccountWithOrder(sendClientAccountWithOrder);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withSendRestrictiveBrokerList(boolean sendRestrictiveBrokerList) {
		getTradeDestinationMappingConfiguration().setSendRestrictiveBrokerList(sendRestrictiveBrokerList);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withBrokerRestrictedToRelatedHoldingAccountList(boolean sendRestrictiveBrokerList) {
		getTradeDestinationMappingConfiguration().setBrokerRestrictedToRelatedHoldingAccountList(sendRestrictiveBrokerList);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withSearchCompanyHierarchyForBrokerMatch(boolean searchCompanyHierarchyForBrokerMatch) {
		getTradeDestinationMappingConfiguration().setSearchCompanyHierarchyForBrokerMatch(searchCompanyHierarchyForBrokerMatch);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixUseSymbolForEquityOrders(boolean fixUseSymbolForEquityOrders) {
		getTradeDestinationMappingConfiguration().setFixUseSymbolForEquityOrders(fixUseSymbolForEquityOrders);
		return this;
	}


	public TradeDestinationMappingConfigurationBuilder withFixSendClearingFirmLEIWithAllocation(boolean fixSendClearingFirmLEIWithAllocation) {
		getTradeDestinationMappingConfiguration().setFixSendClearingFirmLEIWithAllocation(fixSendClearingFirmLEIWithAllocation);
		return this;
	}


	public TradeDestinationMappingConfiguration toTradeDestinationMappingConfiguration() {
		return this.tradeDestinationMappingConfiguration;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeDestinationMappingConfiguration getTradeDestinationMappingConfiguration() {
		return this.tradeDestinationMappingConfiguration;
	}
}
