package com.clifton.trade.destination;

public class TradeDestinationTypeBuilder {

	private TradeDestinationType tradeDestinationType;


	private TradeDestinationTypeBuilder(TradeDestinationType tradeDestinationType) {
		this.tradeDestinationType = tradeDestinationType;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeDestinationTypeBuilder createFixTrade() {
		TradeDestinationType tradeDestinationType = new TradeDestinationType();

		tradeDestinationType.setId((short) 1);
		tradeDestinationType.setName("FIX Trade");
		tradeDestinationType.setDescription("An electron order executed and/or allocated via FIX.");
		tradeDestinationType.setFixTradeOnly(true);
		tradeDestinationType.setCreateOrder(true);

		return new TradeDestinationTypeBuilder(tradeDestinationType);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationTypeBuilder withId(short id) {
		getTradeDestinationType().setId(id);
		return this;
	}


	public TradeDestinationTypeBuilder withName(String name) {
		getTradeDestinationType().setName(name);
		return this;
	}


	public TradeDestinationTypeBuilder withDescription(String description) {
		getTradeDestinationType().setDescription(description);
		return this;
	}


	public TradeDestinationTypeBuilder withFixTradeOnly(boolean fixTradeOnly) {
		getTradeDestinationType().setFixTradeOnly(fixTradeOnly);
		return this;
	}


	public TradeDestinationTypeBuilder withCreateOrder(boolean createOrder) {
		getTradeDestinationType().setCreateOrder(createOrder);
		return this;
	}


	public TradeDestinationType toTradeDestinationType() {
		return this.tradeDestinationType;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeDestinationType getTradeDestinationType() {
		return this.tradeDestinationType;
	}
}
