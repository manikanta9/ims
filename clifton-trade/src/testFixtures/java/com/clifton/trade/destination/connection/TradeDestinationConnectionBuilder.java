package com.clifton.trade.destination.connection;

public class TradeDestinationConnectionBuilder {

	private TradeDestinationConnection tradeDestinationConnection;


	private TradeDestinationConnectionBuilder(TradeDestinationConnection tradeDestinationConnection) {
		this.tradeDestinationConnection = tradeDestinationConnection;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeDestinationConnectionBuilder createEmptyTradeDestinationConnection() {
		return new TradeDestinationConnectionBuilder(new TradeDestinationConnection());
	}


	public static TradeDestinationConnectionBuilder createRediFix() {
		TradeDestinationConnection tradeDestinationConnection = new TradeDestinationConnection();

		tradeDestinationConnection.setId((short) 1);
		tradeDestinationConnection.setName("REDI FIX Connection");
		tradeDestinationConnection.setFixDestinationName("Goldman Sachs");

		return new TradeDestinationConnectionBuilder(tradeDestinationConnection);
	}


	public static TradeDestinationConnectionBuilder createBloombergEMSXFIXConnection() {
		TradeDestinationConnection tradeDestinationConnection = new TradeDestinationConnection();

		tradeDestinationConnection.setId((short) 4);
		tradeDestinationConnection.setName("Bloomberg EMSX FIX Connection");
		tradeDestinationConnection.setFixDestinationName("Bloomberg EMSX");

		return new TradeDestinationConnectionBuilder(tradeDestinationConnection);
	}


	public static TradeDestinationConnectionBuilder createBloombergFIXConnection() {
		TradeDestinationConnection tradeDestinationConnection = new TradeDestinationConnection();

		tradeDestinationConnection.setId((short) 3);
		tradeDestinationConnection.setName("Bloomberg FIX Connection");
		tradeDestinationConnection.setFixDestinationName("Bloomberg (Parametric Minneapolis Fixed Income)");

		return new TradeDestinationConnectionBuilder(tradeDestinationConnection);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationConnectionBuilder withId(short id) {
		getTradeDestinationConnection().setId(id);
		return this;
	}


	public TradeDestinationConnectionBuilder withName(String name) {
		getTradeDestinationConnection().setName(name);
		return this;
	}


	public TradeDestinationConnectionBuilder withDescription(String description) {
		getTradeDestinationConnection().setDescription(description);
		return this;
	}


	public TradeDestinationConnection toTradeDestinationConnection() {
		return this.tradeDestinationConnection;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeDestinationConnection getTradeDestinationConnection() {
		return this.tradeDestinationConnection;
	}
}
