package com.clifton.trade.destination;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.order.beans.ProcessCodes;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurpose;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurposeBuilder;
import com.clifton.investment.setup.group.InvestmentGroupBuilder;
import com.clifton.trade.TradeType;
import com.clifton.trade.builder.TradeDestinationBuilder;
import com.clifton.trade.builder.TradeTypeBuilder;

import java.math.BigDecimal;
import java.util.Date;


public class TradeDestinationMappingBuilder {

	private TradeDestinationMapping tradeDestinationMapping;


	private TradeDestinationMappingBuilder(TradeDestinationMapping tradeDestinationMapping) {
		this.tradeDestinationMapping = tradeDestinationMapping;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeDestinationMappingBuilder createEmptyTradeDestinationBuilder() {
		return new TradeDestinationMappingBuilder(new TradeDestinationMapping());
	}


	public static TradeDestinationMappingBuilder createGoldmanFutures() {
		TradeDestinationMapping tradeDestinationMapping = new TradeDestinationMapping();

		tradeDestinationMapping.setId(45);
		tradeDestinationMapping.setTradeType(TradeTypeBuilder.createFutures().toTradeType());
		tradeDestinationMapping.setTradeDestination(TradeDestinationBuilder.createRediTicket().toTradeDestination());
		tradeDestinationMapping.setMappingConfiguration(TradeDestinationMappingConfigurationBuilder.createGoldmanSachsEquityConfigurationBuilder()
				.toTradeDestinationMappingConfiguration());
		tradeDestinationMapping.setExecutingBrokerCompany(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany());
		tradeDestinationMapping.setDefaultInvestmentGroup(InvestmentGroupBuilder.createFixContracts().toInvestmentGroup());
		tradeDestinationMapping.getMappingConfiguration().setFixPostTradeAllocation(true);
		tradeDestinationMapping.getMappingConfiguration().setFixTradeGroupingAllowed(true);
		tradeDestinationMapping.setFixPostTradeAllocationTopAccountNumber("12132123");
		tradeDestinationMapping.getMappingConfiguration().setFixAllocationReportExpected(true);
		tradeDestinationMapping.setStartDate(DateUtils.toDate("11/11/2011"));

		return new TradeDestinationMappingBuilder(tradeDestinationMapping);
	}


	public static TradeDestinationMappingBuilder createMorganStocks() {
		TradeDestinationMapping tradeDestinationMapping = new TradeDestinationMapping();

		tradeDestinationMapping.setId(134);
		tradeDestinationMapping.setTradeType(TradeTypeBuilder.createStocks().toTradeType());
		tradeDestinationMapping.setTradeDestination(TradeDestinationBuilder.createRediTicket().toTradeDestination());
		tradeDestinationMapping.setMappingConfiguration(TradeDestinationMappingConfigurationBuilder.createBloombergEMSXMappingConfigurationBuilder()
				.toTradeDestinationMappingConfiguration());
		tradeDestinationMapping.setExecutingBrokerCompany(BusinessCompanyBuilder.createMorganStanley().toBusinessCompany());
		tradeDestinationMapping.setDefaultInvestmentGroup(InvestmentGroupBuilder.createFixContracts().toInvestmentGroup());
		tradeDestinationMapping.getMappingConfiguration().setFixPostTradeAllocation(true);
		tradeDestinationMapping.getMappingConfiguration().setFixTradeGroupingAllowed(true);
		tradeDestinationMapping.setFixPostTradeAllocationTopAccountNumber("12132123");
		tradeDestinationMapping.getMappingConfiguration().setFixAllocationReportExpected(true);
		tradeDestinationMapping.setStartDate(DateUtils.toDate("11/11/2011"));

		return new TradeDestinationMappingBuilder(tradeDestinationMapping);
	}


	public static TradeDestinationMappingBuilder createGoldmanFunds() {
		TradeDestinationMapping tradeDestinationMapping = new TradeDestinationMapping();

		tradeDestinationMapping.setId(204);
		tradeDestinationMapping.setTradeType(TradeTypeBuilder.createFunds().toTradeType());
		tradeDestinationMapping.setTradeDestination(TradeDestinationBuilder.createRediTicket().toTradeDestination());
		tradeDestinationMapping.setMappingConfiguration(TradeDestinationMappingConfigurationBuilder.createDefaultPostTradeAllocationConfigurationBuilder()
				.toTradeDestinationMappingConfiguration());
		tradeDestinationMapping.setExecutingBrokerCompany(BusinessCompanyBuilder.createGoldmanExecuteClearing().toBusinessCompany());
		tradeDestinationMapping.getMappingConfiguration().setFixPostTradeAllocation(true);
		tradeDestinationMapping.setFixPostTradeAllocationTopAccountNumber("12121215");
		tradeDestinationMapping.getMappingConfiguration().setFixExecutingAccountUsed(true);
		tradeDestinationMapping.getMappingConfiguration().setFixCommissionType(CommissionTypes.PER_UNIT);
		tradeDestinationMapping.getMappingConfiguration().setFixProcessCode(ProcessCodes.REGULAR);
		tradeDestinationMapping.getMappingConfiguration().setFixDefaultCommissionPerUnit(BigDecimal.valueOf(0.0100000000));
		tradeDestinationMapping.setStartDate(DateUtils.toDate("11/11/2011"));

		return new TradeDestinationMappingBuilder(tradeDestinationMapping);
	}


	public static TradeDestinationMappingBuilder createClearedCDS() {
		TradeDestinationMapping tradeDestinationMapping = new TradeDestinationMapping();

		tradeDestinationMapping.setTradeType(TradeTypeBuilder.createClearedCDS().toTradeType());
		tradeDestinationMapping.setTradeDestination(TradeDestinationBuilder.createBloombergDestination().toTradeDestination());
		tradeDestinationMapping.setMappingConfiguration(TradeDestinationMappingConfigurationBuilder.createBloombergFITCDSConfigurationBuilder()
				.toTradeDestinationMappingConfiguration());
		tradeDestinationMapping.setStartDate(DateUtils.toDate("11/11/2011"));
		tradeDestinationMapping.setAllocationAccountMappingPurpose(InvestmentAccountMappingPurposeBuilder.createBloombergFITAccountBuilder().toInvestmentAccountMappingPurpose());

		return new TradeDestinationMappingBuilder(tradeDestinationMapping);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationMappingBuilder withId(Integer id) {
		getTradeDestinationMapping().setId(id);
		return this;
	}


	public TradeDestinationMappingBuilder withTradeType(TradeType tradeType) {
		getTradeDestinationMapping().setTradeType(tradeType);
		return this;
	}


	public TradeDestinationMappingBuilder withTradeDestination(TradeDestination tradeDestination) {
		getTradeDestinationMapping().setTradeDestination(tradeDestination);
		return this;
	}


	public TradeDestinationMappingBuilder withMappingConfiguration(TradeDestinationMappingConfiguration mappingConfiguration) {
		getTradeDestinationMapping().setMappingConfiguration(mappingConfiguration);
		return this;
	}


	public TradeDestinationMappingBuilder withExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		getTradeDestinationMapping().setExecutingBrokerCompany(executingBrokerCompany);
		return this;
	}


	public TradeDestinationMappingBuilder withFixPostTradeAllocation(boolean fixPostTradeAllocation) {
		getTradeDestinationMapping().getMappingConfiguration().setFixPostTradeAllocation(fixPostTradeAllocation);
		return this;
	}


	public TradeDestinationMappingBuilder withFixPostTradeAllocationTopAccountNumber(String fixPostTradeAllocationTopAccountNumber) {
		getTradeDestinationMapping().setFixPostTradeAllocationTopAccountNumber(fixPostTradeAllocationTopAccountNumber);
		return this;
	}


	public TradeDestinationMappingBuilder withFixExecutingAccountUsed(boolean fixExecutingAccountUsed) {
		getTradeDestinationMapping().getMappingConfiguration().setFixExecutingAccountUsed(fixExecutingAccountUsed);
		return this;
	}


	public TradeDestinationMappingBuilder withFixCommissionType(CommissionTypes commissionTypes) {
		getTradeDestinationMapping().getMappingConfiguration().setFixCommissionType(commissionTypes);
		return this;
	}


	public TradeDestinationMappingBuilder withFixProcessCode(ProcessCodes processCode) {
		getTradeDestinationMapping().getMappingConfiguration().setFixProcessCode(processCode);
		return this;
	}


	public TradeDestinationMappingBuilder withFixDefaultCommissionPerUnit(BigDecimal fixDefaultCommissionPerUnit) {
		getTradeDestinationMapping().getMappingConfiguration().setFixDefaultCommissionPerUnit(fixDefaultCommissionPerUnit);
		return this;
	}


	public TradeDestinationMappingBuilder withStartDate(Date startDate) {
		getTradeDestinationMapping().setStartDate(startDate);
		return this;
	}


	public TradeDestinationMappingBuilder withSendRestrictiveBrokerList(boolean sendRestrictiveBrokerList) {
		getTradeDestinationMapping().getMappingConfiguration().setSendRestrictiveBrokerList(sendRestrictiveBrokerList);
		return this;
	}


	public TradeDestinationMappingBuilder withOrderAccountMappingPurpose(InvestmentAccountMappingPurpose orderAccountMappingPurpose) {
		getTradeDestinationMapping().setOrderAccountMappingPurpose(orderAccountMappingPurpose);
		return this;
	}


	public TradeDestinationMappingBuilder withAllocationAccountMappingPurpose(InvestmentAccountMappingPurpose allocationAccountMappingPurpose) {
		getTradeDestinationMapping().setAllocationAccountMappingPurpose(allocationAccountMappingPurpose);
		return this;
	}


	public TradeDestinationMapping toTradeDestinationMapping() {
		return this.tradeDestinationMapping;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeDestinationMapping getTradeDestinationMapping() {
		return this.tradeDestinationMapping;
	}
}
