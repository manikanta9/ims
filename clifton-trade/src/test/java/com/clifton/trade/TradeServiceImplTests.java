package com.clifton.trade;


import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.builder.TradeDestinationBuilder;
import com.clifton.trade.destination.TradeDestination;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class TradeServiceImplTests extends BaseTradeServiceTests {

	////////////////////////////////////////////////////////////////////////////
	////////                 Test SAVE TradeFill                   /////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTestAccountRelationshipForPurposes() {
		Mockito.when(
				this.investmentAccountRelationshipService.isInvestmentAccountRelationshipForPurposeValid(ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_CLIENT_DEFAULT), ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_HOLDING_DEFAULT), ArgumentMatchers.contains("Trading: Futures"),
						ArgumentMatchers.any())).thenReturn(true);
		Mockito.when(
				this.investmentAccountRelationshipService.isInvestmentAccountRelationshipForPurposeValid(ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_CLIENT_DEFAULT), ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_HOLDING_DEFAULT), ArgumentMatchers.contains("Trading: Forwards"),
						ArgumentMatchers.any())).thenReturn(true);
	}


	@Test
	public void testSaveTrade_SaveFillList() {
		Trade trade = TradeBuilder.newTradeForSecurity(FAU3)
				.withExpectedUnitPrice("1176.8428571")
				.withQuantityIntended(10)
				.withTradeDestination(TradeDestinationBuilder.createTradeDestination(TRADE_DESTINATION_ID).toTradeDestination())
				.build();
		trade.getTradeType().setId(FUTURES_TRADE_TYPE_ID);
		trade.getExecutingBrokerCompany().setId(EXECUTING_BROKER_ID);

		trade = this.tradeService.saveTrade(trade);

		trade = TradeBuilder.toBuilder(trade)
				.addFill("2", "1176.6")
				.addFill("2", "1177")
				.addFill("1", "1176.9")
				.addFill("1", "1176.7")
				.addFill("3", "1176.8")
				.addFill("1", "1177.1")
				.build();

		this.tradeService.saveTradeFillList(trade.getTradeFillList());
		List<TradeFill> savedFillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(6, savedFillList.size());

		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(CoreMathUtils.formatNumber(new BigDecimal("1176.8300000"), "#,###.###############"), CoreMathUtils.formatNumber(trade.getAverageUnitPrice(), "#,###.###############"));
	}


	@Test
	public void testSaveTrade_SaveFillList_NotionalCalculation() {
		Trade trade = TradeBuilder.newTradeForSecurity(TYM7)
				.withExpectedUnitPrice("125.640625")
				.withQuantityIntended(78)
				.withTradeDestination(TradeDestinationBuilder.createTradeDestination(TRADE_DESTINATION_ID).toTradeDestination())
				.build();

		trade.getTradeType().setId(FUTURES_TRADE_TYPE_ID);
		trade.getExecutingBrokerCompany().setId(EXECUTING_BROKER_ID);

		trade = this.tradeService.saveTrade(trade);

		trade = TradeBuilder.toBuilder(trade)
				.addFill("1", "125.984375")
				.addFill("1", "125.890625")
				.addFill("3", "125.953125")
				.addFill("17", "125.828125")
				.addFill("3", "125.796875")
				.addFill("6", "125.8125")
				.addFill("1", "125.921875")
				.addFill("5", "125.96875")
				.addFill("2", "125.9375")
				.addFill("14", "125.84375")
				.addFill("1", "125.78125")
				.addFill("2", "125.765625")
				.addFill("2", "125.875")
				.addFill("20", "125.859375")
				.build();

		this.tradeService.saveTradeFillList(trade.getTradeFillList());
		List<TradeFill> savedFillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(14, savedFillList.size());

		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(CoreMathUtils.formatNumber(new BigDecimal("9816781.4900000"), "#,###.###############"), CoreMathUtils.formatNumber(trade.getAccountingNotional(), "#,###.###############"));
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Test SAVE Trade                     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidateRequiredFields() {
		DAOConfiguration<Trade> config = this.tradeDAO.getConfiguration();
		for (Column column : CollectionUtils.getIterable(config.getTable().getColumnList())) {
			if ("NotionalMultiplier".equals(column.getName())
					|| "FXRateToBase".equals(column.getName())
					|| "TradeOpenCloseTypeID".equals(column.getName())
					|| "WorkflowStateID".equals(column.getName())
					|| "WorkflowStatusID".equals(column.getName())) {
				Assertions.assertTrue(column.getColumnRequired(), column.getName() + " field is required");
				Assertions.assertFalse(column.getColumnRequiredForValidation(), column.getName() + " field is not required for validation");
			}
			else {
				Assertions.assertEquals(column.getColumnRequiredForValidation(), column.getColumnRequired(), column.getName());
			}
		}
	}


	@Test
	public void testSaveTrade_WithoutQuantity() {
		final Trade trade = TradeBuilder.newTradeForSecurity(TUH0)
				.withExpectedUnitPrice("108.9453125000")
				.build();

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.tradeService.saveTrade(trade));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Trade quantity cannot be empty for non-currency trades."));
	}


	@Test
	public void testSaveTrade_WithZeroQuantity() {
		final Trade trade = TradeBuilder.newTradeForSecurity(TUH0)
				.withExpectedUnitPrice("108.9453125000")
				.withQuantityIntended(0)
				.build();

		FieldValidationException fieldValidationException = Assertions.assertThrows(FieldValidationException.class, () -> this.tradeService.saveTrade(trade));
		MatcherAssert.assertThat(fieldValidationException.getMessage(), StringContains.containsString("Trade quantity [0] must be greater than zero for non-currency trades."));
	}


	@Test
	public void testSaveTrade_SimpleFuture() {
		Trade trade = TradeBuilder.newTradeForSecurity(TUH0)
				.withExpectedUnitPrice("108.9453125000")
				.withCommissionPerUnit(4.52)
				.build();
		trade.setQuantityIntended(new BigDecimal(10));

		Assertions.assertNull(trade.getOriginalFace(), "Original Face should not be set");
		this.tradeService.saveTrade(trade);
		Assertions.assertEquals(trade.getQuantityIntended(), trade.getOriginalFace(), "Original Face must be equal to Quantity Intended");
	}


	@Test
	public void testSaveTrade_PennyNotionalOverride() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newForward("EUR/USD20170621").withId(EUR_USD20170621_SECURITY_ID).build())
				.withExpectedUnitPrice("1.094353")
				.withQuantityIntended("1959148.46")
				// calculated notional is 2143999.99 (it's ok to be off by 1 cent)
				.withAccountingNotional("2144000.00")
				.build();

		trade.getTradeType().setId(FORWARDS_TRADE_TYPE_ID);

		trade = this.tradeService.saveTrade(trade);
		Assertions.assertEquals(new BigDecimal("2144000.00"), trade.getAccountingNotional(), "Accounting Notional must not change");


		// calculated notional is 2143999.99 (it's ok to be off by 1 cent)
		trade.setId(null);
		trade.setAccountingNotional(new BigDecimal("2143999.98"));
		trade = this.tradeService.saveTrade(trade);
		Assertions.assertEquals(new BigDecimal("2143999.98"), trade.getAccountingNotional(), "Accounting Notional must change");


		// calculated notional is 2143999.99 (it's NOT ok to be off by 2 cents)
		trade.setId(null);
		trade.setAccountingNotional(new BigDecimal("2144000.01"));
		trade = this.tradeService.saveTrade(trade);
		Assertions.assertEquals(new BigDecimal("2143999.99"), trade.getAccountingNotional(), "Accounting Notional must change");


		// calculated notional is 2143999.99 (it's NOT ok to be off by 2 cents)
		trade.setId(null);
		trade.setAccountingNotional(new BigDecimal("2143999.97"));
		trade = this.tradeService.saveTrade(trade);
		Assertions.assertEquals(new BigDecimal("2143999.99"), trade.getAccountingNotional(), "Accounting Notional must change");
	}


	/**
	 * Note: Although this test is used to validate the account relationship, because of way tests are done for Trades
	 * the method from InvestmentAccountRelationshipService is mocked and this method just validates the error message.
	 * <p>
	 * The method itself in InvestmentAccountRelationshipService for validating investment account relationships already has tests for it
	 */
	@Test
	public void testSaveTrade_InactiveRelationship() {
		final Trade trade = TradeBuilder.newTradeForSecurity(TUH0)
				.withExpectedUnitPrice("108.9453125000")
				.withQuantityIntended(10)
				.build();
		trade.getClientInvestmentAccount().setId(1);

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.tradeService.saveTrade(trade));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Missing active relationship from Client Account [100000] to holding account [H-200: Test Holding Account (Broker)] for purpose [Trading: Futures] on [10/17/2012]"));
	}


	@Test
	public void testSplitTrade_UnsavedTrade() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				this.tradeService.splitTrade(
						TradeBuilder.newTradeForSecurity(TUH0)
								.withQuantityIntended(10)
								.build(),
						new BigDecimal(12))
		);
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot split an unsaved Trade."));
	}


	@Test
	public void testSplitTrade_QuantityIntendedLowerThanQuantityToSplit() {
		Trade trade = TradeBuilder.newTradeForSecurity(TUH0)
				.withQuantityIntended(10)
				.build();

		trade = this.tradeService.saveTrade(trade);

		final Trade finalTrade = trade;
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.tradeService.splitTrade(finalTrade, new BigDecimal(12)));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Quantity to split must be less than the Trade to split's Quantity Intended."));
	}


	@Test
	public void testSplitTrade_WithFills() {
		Trade trade = TradeBuilder.newTradeForSecurity(FAU3)
				.withQuantityIntended(10)
				.withTradeDestination(TradeDestinationBuilder.createTradeDestination(TRADE_DESTINATION_ID).toTradeDestination())
				.build();
		trade.getTradeType().setId(FUTURES_TRADE_TYPE_ID);
		trade.getExecutingBrokerCompany().setId(EXECUTING_BROKER_ID);

		trade = this.tradeService.saveTrade(trade);

		trade = TradeBuilder.toBuilder(trade)
				.addFill("2", "1176.6")
				.addFill("2", "1177")
				.addFill("1", "1176.9")
				.addFill("1", "1176.7")
				.addFill("3", "1176.8")
				.addFill("1", "1177.1")
				.build();

		this.tradeService.saveTradeFillList(trade.getTradeFillList());

		final Trade finalTrade = trade;
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.tradeService.splitTrade(finalTrade, BigDecimal.ONE));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot split a Trade with Fills."));
	}


	@Test
	public void testSplitTrade_WithCommissionOverrides() {
		Trade trade = TradeBuilder.newTradeForSecurity(TUH0)
				.withQuantityIntended(10)
				.withCommissionPerUnit("1")
				.withFeeAmount("10")
				.build();

		trade = this.tradeService.saveTrade(trade);
		Trade[] splitTrades = this.tradeService.splitTrade(trade, new BigDecimal(6));

		validateSplitTrade(splitTrades[0], new BigDecimal(4), trade.getCommissionPerUnit(), new BigDecimal(4));
		validateSplitTrade(splitTrades[1], new BigDecimal(6), trade.getCommissionPerUnit(), new BigDecimal(6));
	}


	@Test
	public void testSplitTrade_NoCommissionOverride() {
		Trade trade = TradeBuilder.newTradeForSecurity(TUH0)
				.withQuantityIntended(10)
				.build();
		trade = this.tradeService.saveTrade(trade);

		Trade[] splitTrades = this.tradeService.splitTrade(trade, new BigDecimal(6));

		validateSplitTrade(splitTrades[0], new BigDecimal(4), trade.getCommissionPerUnit(), trade.getFeeAmount());
		validateSplitTrade(splitTrades[1], new BigDecimal(6), trade.getCommissionPerUnit(), trade.getFeeAmount());
	}


	@Test
	public void testTradeUpload_AssetType_InstrumentIdentifierPrefix_TemplateConverter() {
		// See System List: Trade Upload: AssetType-Identifier Prefix
		// This test is designed to ensure the syntax for various types works correctly
		// Options Listed: SO-${(bean.investmentSecurity.symbol)?keep_before(" ")}
		InvestmentSecurity security = new InvestmentSecurity();
		security.setSymbol("SPY US 09/16/16 C215");
		Trade trade = new Trade();
		trade.setInvestmentSecurity(security);

		TemplateConfig config = new TemplateConfig("SO-${(bean.investmentSecurity.symbol)?keep_before(\" \")}");
		config.addBeanToContext("bean", trade);
		Assertions.assertEquals("SO-SPY", this.templateConverter.convert(config));

		security.setSymbol("EFA US 09/16/16 C64");
		Assertions.assertEquals("SO-EFA", this.templateConverter.convert(config));

		// FLEX Options: FO-${(bean.investmentSecurity.occSymbol)?keep_before(" ")?replace("[0-9]","", "r")}
		security.setSymbol("SPX FLEX 03/09/17 P1790.61");
		security.setOccSymbol("4SPX 170309P01790610");

		config = new TemplateConfig("FO-${(bean.investmentSecurity.occSymbol)?keep_before(\" \")?replace(\"[0-9]\",\"\", \"r\")}");
		config.addBeanToContext("bean", trade);

		Assertions.assertEquals("FO-SPX", this.templateConverter.convert(config));

		security.setSymbol("RUT FLEX 03/09/17 P957.59");
		security.setOccSymbol("4RUT 170309P00957590");

		Assertions.assertEquals("FO-RUT", this.templateConverter.convert(config));
	}


	@Test
	public void testSaveTrade_modifyConditionFails() {
		Trade trade = initTrade();

		TradeDestination dest = this.tradeDestinationService.getTradeDestination(trade.getTradeDestination().getId());
		try {
			dest.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(5));
			this.tradeDestinationService.saveTradeDestination(dest);
			trade.setCreateUserId((short) 999);
			trade.setTradeDestination(dest);
			TestUtils.expectException(RuntimeException.class, () -> this.tradeService.saveTrade(trade), "User does not have permission to");
		}
		finally {
			dest.setTradeModifySystemCondition(null);
			this.tradeDestinationService.saveTradeDestination(dest);
		}
	}


	@Test
	public void testSaveTrade_modifyConditionSucceeds() {
		Trade trade = initTrade();

		TradeDestination dest = this.tradeDestinationService.getTradeDestination(trade.getTradeDestination().getId());
		try {
			dest.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(5));
			this.tradeDestinationService.saveTradeDestination(dest);
			trade.setCreateUserId((short) 777);
			trade.setTradeDestination(dest);
			this.tradeService.saveTrade(trade);
		}
		finally {
			dest.setTradeModifySystemCondition(null);
			this.tradeDestinationService.saveTradeDestination(dest);
		}
	}


	@Test
	public void testUpdateTrade_modifyConditionFailNew() {

		Trade trade = initTrade();

		TradeDestination dest = this.tradeDestinationService.getTradeDestination(trade.getTradeDestination().getId());
		try {
			// Updated trade should pass original condition but fail new condition
			dest.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(5));
			this.tradeDestinationService.saveTradeDestination(dest);
			trade.setCreateUserId((short) 777);
			trade.setTradeDestination(dest);
			this.tradeService.saveTrade(trade);

			dest.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(2));
			this.tradeDestinationService.saveTradeDestination(dest);
			trade.setTradeDestination(dest);
			TestUtils.expectException(RuntimeException.class, () -> this.tradeService.saveTrade(trade), "User does not have permission to");
		}
		finally {
			dest.setTradeModifySystemCondition(null);
			this.tradeDestinationService.saveTradeDestination(dest);
		}
	}


	@Test
	public void testUpdateTrade_modifyConditionFailOriginal() {

		Trade trade = initTrade();

		TradeDestination dest = this.tradeDestinationService.getTradeDestination(trade.getTradeDestination().getId());
		try {
			// Updated trade should pass new condition, but fail old condition
			dest.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(5));
			this.tradeDestinationService.saveTradeDestination(dest);
			trade.setCreateUserId((short) 777);
			trade.setTradeDestination(dest);
			this.tradeService.saveTrade(trade);

			dest.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(2));
			this.tradeDestinationService.saveTradeDestination(dest);
			trade.setCreateUserId((short) 999);
			trade.setTradeDestination(dest);
			TestUtils.expectException(RuntimeException.class, () -> this.tradeService.saveTrade(trade), "User does not have permission to");
		}
		finally {
			dest.setTradeModifySystemCondition(null);
			this.tradeDestinationService.saveTradeDestination(dest);
		}
	}


	@Test
	public void testUpdateTrade_modifyConditionChangeDestination() {

		Trade trade = initTrade();

		TradeDestination dest = this.tradeDestinationService.getTradeDestination(trade.getTradeDestination().getId());
		try {
			// Updated trade should pass new condition, but fail old condition
			dest.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(5));
			this.tradeDestinationService.saveTradeDestination(dest);
			trade.setCreateUserId((short) 777);
			trade.setTradeDestination(dest);
			this.tradeService.saveTrade(trade);

			TradeDestination dest2 = this.tradeDestinationService.getTradeDestination((short) 2);
			dest2.setTradeModifySystemCondition(this.systemConditionService.getSystemCondition(2));
			this.tradeDestinationService.saveTradeDestination(dest2);
			trade.setTradeDestination(dest2);
			TestUtils.expectException(RuntimeException.class, () -> this.tradeService.saveTrade(trade), "User does not have permission to");
		}
		finally {
			dest.setTradeModifySystemCondition(null);
			this.tradeDestinationService.saveTradeDestination(dest);
		}
	}
}
