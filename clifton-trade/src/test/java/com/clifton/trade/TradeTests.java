package com.clifton.trade;

import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
public class TradeTests {


	@Test
	public void testGetNetPayment_CDS() {
		Assertions.assertEquals(new BigDecimal("1717484.16"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCDS("XY2901D22U0500XXI").build())
				.buy() // over par
				.withOriginalFace("19400000.00")
				.withTotalCommission("-485.00")
				.withFeeAmount("-116.40")
				.withAccrualAmount1("118555.56")
				.withAccountingNotional("1599530.00")
				.build().getNetPayment());

		Assertions.assertEquals(new BigDecimal("-1565597.33"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCDS("XM2701J22U0100XXI").build())
				.buy() // under par
				.withOriginalFace("56800000.00")
				.withTotalCommission("-1420.00")
				.withFeeAmount("-340.80")
				.withAccrualAmount1("23666.67")
				.withAccountingNotional("-1587503.20")
				.build().getNetPayment());

		Assertions.assertEquals(new BigDecimal("-1125730.13"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCDS("XY2901D22U0500XXI").build())
				.sell() // over par
				.withOriginalFace("12800000.00")
				.withTotalCommission("-320.00")
				.withFeeAmount("-76.80")
				.withAccrualAmount1("-85333.33")
				.withAccountingNotional("-1040000.00")
				.build().getNetPayment());

		Assertions.assertEquals(new BigDecimal("2036412.53"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCDS("XM2801D22U0100XXI").build())
				.sell() // under par
				.withOriginalFace("56800000.00")
				.withTotalCommission("-1420.00")
				.withFeeAmount("-340.80")
				.withAccrualAmount1("-23666.67")
				.withAccountingNotional("2061840.00")
				.build().getNetPayment());
	}


	@Test
	public void testGetNetPayment_IRS() {
		Assertions.assertEquals(new BigDecimal("16072271.00"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newIRS("USD3L-20160808-20460808-1.8235").build())
				.buy()
				.withQuantityIntended("120000000.00")
				.withTotalCommission("-3000.00")
				.withFeeAmount("-2100.00")
				.withAccrualAmount1("-600367.10")
				.withAccountingNotional("16677738.10")
				.build().getNetPayment());

		Assertions.assertEquals(new BigDecimal("517191.00"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newIRS("USD3L-20161130-20311130-2.3565").build())
				.sell()
				.withQuantityIntended("112000000.00")
				.withTotalCommission("-2800.00")
				.withFeeAmount("-1120.00")
				.withAccrualAmount1("0.00")
				.withAccountingNotional("521111.00")
				.build().getNetPayment());
	}


	@Test
	public void testGetNetPayment_Funds() {
		Assertions.assertEquals(new BigDecimal("-563923.64"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newStock("AGG").build())
				.buy()
				.withQuantityIntended("5158")
				.withTotalCommission("-51.58")
				.withFeeAmount("0.00")
				.withAccountingNotional("563872.06")
				.build().getNetPayment());

		Assertions.assertEquals(new BigDecimal("654752.51"), TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newStock("VOO").build())
				.sell()
				.withQuantityIntended("2772")
				.withTotalCommission("-27.71")
				.withFeeAmount("-15.13")
				.withAccountingNotional("654795.35")
				.build().getNetPayment());
	}
}
