package com.clifton.trade.restriction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.restriction.search.TradeRestrictionBrokerSearchForm;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author davidi
 */
@TradeInMemoryDatabaseContext
public class TradeRestrictionBrokerServiceImplTests extends BaseInMemoryDatabaseTests {

	@Resource
	TradeRestrictionBrokerService tradeRestrictionBrokerService;

	@Resource
	BusinessCompanyService businessCompanyService;

	@Resource
	InvestmentAccountService investmentAccountService;

	@Resource
	AdvancedUpdatableDAO<TradeRestrictionBroker, Criteria> tradeRestrictionBrokerDAO;

	private boolean isInitialized;


	@BeforeEach
	public void InitializeData() {
		if (!this.isInitialized) {
			this.isInitialized = true;

			List<TradeRestrictionBroker> tradeRestrictionBrokerDataList = new ArrayList<>();

			TradeRestrictionBroker entry = new TradeRestrictionBroker();
			entry.setClientAccount(this.investmentAccountService.getInvestmentAccount(4203));
			entry.setBrokerCompany(this.businessCompanyService.getBusinessCompany(4468));
			entry.setInclude(true);
			tradeRestrictionBrokerDataList.add(entry);

			entry = new TradeRestrictionBroker();
			entry.setClientAccount(this.investmentAccountService.getInvestmentAccount(4203));
			entry.setBrokerCompany(this.businessCompanyService.getBusinessCompany(10240));
			entry.setInclude(true);
			tradeRestrictionBrokerDataList.add(entry);

			entry = new TradeRestrictionBroker();
			entry.setClientAccount(this.investmentAccountService.getInvestmentAccount(4203));
			entry.setBrokerCompany(this.businessCompanyService.getBusinessCompany(4467));
			entry.setInclude(true);
			tradeRestrictionBrokerDataList.add(entry);

			entry = new TradeRestrictionBroker();
			entry.setClientAccount(this.investmentAccountService.getInvestmentAccount(4205));
			entry.setBrokerCompany(this.businessCompanyService.getBusinessCompany(4468));
			entry.setInclude(false);
			tradeRestrictionBrokerDataList.add(entry);

			entry = new TradeRestrictionBroker();
			entry.setClientAccount(this.investmentAccountService.getInvestmentAccount(4207));
			entry.setBrokerCompany(this.businessCompanyService.getBusinessCompany(4468));
			entry.setInclude(false);
			tradeRestrictionBrokerDataList.add(entry);

			this.tradeRestrictionBrokerDAO.saveList(tradeRestrictionBrokerDataList);
		}
	}


	@Test
	public void testGetTradeRestrictionBroker() {
		TradeRestrictionBroker entry1 = this.tradeRestrictionBrokerService.getTradeRestrictionBroker(1);
		TradeRestrictionBroker entry2 = this.tradeRestrictionBrokerService.getTradeRestrictionBroker(2);

		Assertions.assertEquals(1, (int) entry1.getId());
		Assertions.assertEquals(2, (int) entry2.getId());
	}


	@Test
	public void testSearchformOf_Include_True() {
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccount(4203);
		TradeRestrictionBrokerSearchForm searchForm = TradeRestrictionBrokerSearchForm.ofClientAccount(account, true);
		List<TradeRestrictionBroker> tradeRestrictionBrokerList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<BusinessCompany> businessCompanySet = CollectionUtils.getStream(tradeRestrictionBrokerList).map(TradeRestrictionBroker::getBrokerCompany).collect(Collectors.toSet());
		Set<Integer> businessCompanyIdSet = CollectionUtils.getStream(businessCompanySet).map(BusinessCompany::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(4467, 4468, 10240);

		Assertions.assertEquals(expectedSet, businessCompanyIdSet);
	}


	@Test
	public void testSearchformOf_Include_False() {
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccount(4203);
		TradeRestrictionBrokerSearchForm searchForm = TradeRestrictionBrokerSearchForm.ofClientAccount(account, false);
		List<TradeRestrictionBroker> tradeRestrictionBrokerList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, tradeRestrictionBrokerList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_IncludeTrue() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setInclude(true);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(1, 2, 3);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_IncludeFalse() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setInclude(false);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(4, 5);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testSaveTradeRestrictionBroker() {

		TradeRestrictionBroker entry = new TradeRestrictionBroker();
		entry.setClientAccount(this.investmentAccountService.getInvestmentAccount(4203));
		entry.setBrokerCompany(this.businessCompanyService.getBusinessCompany(3));

		TradeRestrictionBroker savedEntry = this.tradeRestrictionBrokerService.saveTradeRestrictionBroker(entry);
		Assertions.assertNotNull(savedEntry);
		TradeRestrictionBroker retrievedEntry = this.tradeRestrictionBrokerDAO.findByPrimaryKey(savedEntry.getId());

		Assertions.assertEquals(savedEntry.getId(), retrievedEntry.getId());

		// clean-up -- remove added test entry
		this.tradeRestrictionBrokerDAO.delete(retrievedEntry);
	}


	@Test
	public void testDeleteTradeRestrictionBroker() {

		TradeRestrictionBroker entry = new TradeRestrictionBroker();
		entry.setClientAccount(this.investmentAccountService.getInvestmentAccount(4207));
		entry.setBrokerCompany(this.businessCompanyService.getBusinessCompany(2));

		// Save the entry and verify it exists in the DB
		TradeRestrictionBroker savedEntry = this.tradeRestrictionBrokerService.saveTradeRestrictionBroker(entry);
		Assertions.assertNotNull(savedEntry);
		TradeRestrictionBroker retrievedEntry = this.tradeRestrictionBrokerDAO.findByPrimaryKey(savedEntry.getId());
		Assertions.assertEquals(savedEntry.getId(), retrievedEntry.getId());

		// Delete the entry and verify it no longer exists in the DB
		this.tradeRestrictionBrokerService.deleteTradeRestrictionBroker(savedEntry);
		retrievedEntry = this.tradeRestrictionBrokerDAO.findByPrimaryKey(savedEntry.getId());
		Assertions.assertNull(retrievedEntry);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ById() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setId(2);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(2);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ById_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setId(122343);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByIds() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setIds(new Integer[]{5, 4});
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(4, 5);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByIds_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setIds(new Integer[]{555, 432});
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByClientAccountId() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountId(4203);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(1, 2, 3);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByClientAccountId_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountId(124203);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByClientAccountIds() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountIds(new Integer[]{4205, 4207});
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(4, 5);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByClientAccountIds_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountIds(new Integer[]{1111, 2222});
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByClientAccountNumber_MultiMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountNumber("10400");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(1, 2, 3, 4, 5);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByClientAccountNumber_OneMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountNumber("104002");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(4);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByClientAccountNumberEquals() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setClientAccountNumberEquals("104001");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(1, 2, 3);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyId_MultiMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyId(4468);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(1, 4, 5);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyId_SingleMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyId(10240);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(2);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyId_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyId(99999);
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyName_MultiMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyName("Goldman");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(1, 4, 5);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyName_SingleMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyName("Citi");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(3);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyName_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyName("NoExist");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyNameEquals_MultiMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyNameEquals("Goldman Sachs");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(1, 4, 5);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyNameEquals_SingleMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyNameEquals("Citigroup");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);
		Set<Integer> entitySet = CollectionUtils.getStream(entityList).map(TradeRestrictionBroker::getId).collect(Collectors.toSet());
		Set<Integer> expectedSet = CollectionUtils.createHashSet(3);

		Assertions.assertEquals(expectedSet, entitySet);
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyNameEquals_Substring_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyNameEquals("Citi");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}


	@Test
	public void testGetTradeRestrictionBrokerList_ByBrokerCompanyNameEquals_NoMatch() {
		TradeRestrictionBrokerSearchForm searchForm = new TradeRestrictionBrokerSearchForm();
		searchForm.setBrokerCompanyNameEquals("NonExist");
		List<TradeRestrictionBroker> entityList = this.tradeRestrictionBrokerService.getTradeRestrictionBrokerList(searchForm);

		Assertions.assertEquals(0, entityList.size());
	}
}
