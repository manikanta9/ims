package com.clifton.trade.restriction.rule;

import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import com.clifton.trade.restriction.type.TradeRestrictionTypeService;
import com.clifton.trade.restriction.type.evaluator.BaseTradeRestrictionTypeEvaluator;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeNoBuyEvaluator;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeNoSellEvaluator;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeNoSellToOpenEvaluator;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeNoTradeEvaluator;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeSTOMinimumStrikeLimitEvaluator;
import com.clifton.trade.restriction.type.evaluator.TradeRestrictionTypeSTOMinimumUnderlyingLimitEvaluator;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * A set of tests to validate {@link TradeRestrictionEvaluator} behavior.
 *
 * @author MikeH
 */
public class TradeRestrictionEvaluatorTests {

	/* Mocked objects */
	private TradeRestrictionEvaluator tradeRestrictionEvaluator;
	@Mock
	private MarketDataRetriever marketDataRetriever;
	@Mock
	private RuleViolationService ruleViolationService;
	@Mock
	private SystemBeanService systemBeanService;
	@Mock
	private SystemSchemaService systemSchemaService;
	@Mock
	private TradeService tradeService;
	@Mock
	private TradeRestrictionService tradeRestrictionService;
	@Mock
	private TradeRestrictionTypeService tradeRestrictionTypeService;
	@Mock
	private WorkflowHistoryService workflowHistoryService;
	@Mock
	private RuleConfig ruleConfig;
	@Mock
	private EntityConfig entityConfig;
	@Mock
	private TradeRuleEvaluatorContext tradeRuleEvaluatorContext;

	/* Test data fields */
	private short id;
	private List<TradeRestriction> restrictionList = new ArrayList<>();
	private Map<String, TradeOpenCloseType> tradeOpenCloseTypeMap = new HashMap<>();

	private static final String RESTRICTION_TYPE_NO_TRADE = "No Trade";
	private static final String RESTRICTION_TYPE_NO_BUY = "No Buy";
	private static final String RESTRICTION_TYPE_NO_SELL = "No Sell";
	private static final String RESTRICTION_TYPE_NO_SELL_TO_OPEN = "No Sell-to-Open";
	private static final String RESTRICTION_TYPE_STO_MINIMUM_STRIKE = "STO Minimum Strike";
	private static final String RESTRICTION_TYPE_STO_MINIMUM_UNDERLYING = "STO Minimum Underlying";
	private static final String RESTRICTION_TYPE_STO_MINIMUM_OTM_PERCENT = "S/O Option Minimum OTM Percent";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		// Explicitly inject mocks to fail-fast on injection failure, unlike @InjectMocks
		this.tradeRestrictionEvaluator = new TradeRestrictionEvaluator();
		this.tradeRestrictionEvaluator.setRuleViolationService(this.ruleViolationService);
		this.tradeRestrictionEvaluator.setSystemBeanService(this.systemBeanService);
		this.tradeRestrictionEvaluator.setSystemSchemaService(this.systemSchemaService);
		this.tradeRestrictionEvaluator.setTradeRestrictionService(this.tradeRestrictionService);
		this.tradeRestrictionEvaluator.setWorkflowHistoryService(this.workflowHistoryService);

		// Set default mocked return values
		Mockito.when(this.entityConfig.isExcluded()).thenReturn(false);
		Mockito.when(this.ruleConfig.getEntityConfig(Mockito.any())).thenReturn(this.entityConfig);
		Mockito.when(this.ruleViolationService.createRuleViolationWithCause(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
				.thenAnswer(invocation -> {
					RuleViolation violation = new RuleViolation();
					violation.setLinkedFkFieldId(MathUtils.getNumberAsLong(invocation.getArgument(1)));
					violation.setCauseFkFieldId(MathUtils.getNumberAsLong(invocation.getArgument(2)));
					violation.setCauseTable(invocation.getArgument(3));
					return violation;
				});
		Mockito.when(this.systemSchemaService.getSystemTableByName(Mockito.anyString()))
				.thenAnswer(invocation -> {
					SystemTable table = new SystemTable();
					table.setName(invocation.getArgument(0));
					return table;
				});
		Mockito.when(this.tradeService.getTradeOpenCloseTypeByName(Mockito.anyString()))
				.thenAnswer(invocation -> {
					String typeName = invocation.getArgument(0);
					return this.tradeOpenCloseTypeMap.get(typeName);
				});

		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			BaseTradeRestrictionTypeEvaluator evaluator = null;
			switch (bean.getName()) {
				case RESTRICTION_TYPE_NO_TRADE:
					evaluator = new TradeRestrictionTypeNoTradeEvaluator();
					break;
				case RESTRICTION_TYPE_NO_BUY:
					evaluator = new TradeRestrictionTypeNoBuyEvaluator();
					break;
				case RESTRICTION_TYPE_NO_SELL:
					evaluator = new TradeRestrictionTypeNoSellEvaluator();
					break;
				case RESTRICTION_TYPE_NO_SELL_TO_OPEN:
					evaluator = new TradeRestrictionTypeNoSellToOpenEvaluator();
					break;
				case RESTRICTION_TYPE_STO_MINIMUM_STRIKE:
					evaluator = new TradeRestrictionTypeSTOMinimumStrikeLimitEvaluator();
					break;
				case RESTRICTION_TYPE_STO_MINIMUM_UNDERLYING:
					evaluator = new TradeRestrictionTypeSTOMinimumUnderlyingLimitEvaluator();
					break;
				case RESTRICTION_TYPE_STO_MINIMUM_OTM_PERCENT:
					TradeRestrictionTypeSTOMinimumUnderlyingLimitEvaluator minimumUnderlyingLimitEvaluator = new TradeRestrictionTypeSTOMinimumUnderlyingLimitEvaluator();
					minimumUnderlyingLimitEvaluator.setUsePercentOutOfTheMoney(true);
					evaluator = minimumUnderlyingLimitEvaluator;
					break;
			}
			if (evaluator != null) {
				evaluator.setMarketDataRetriever(this.marketDataRetriever);
				evaluator.setTradeService(this.tradeService);
			}
			return evaluator;
		});

		Mockito.when(this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(Mockito.anyString()))
				.thenAnswer(invocation -> {
					String typeName = invocation.getArgument(0);
					TradeRestrictionType restrictionType = new TradeRestrictionType();
					restrictionType.setName(typeName);
					SystemBean systemBean = new SystemBean();
					systemBean.setName(typeName);
					restrictionType.setEvaluatorBean(systemBean);
					return restrictionType;
				});
		Mockito.when(this.tradeRestrictionService.getTradeRestrictionListForTrade(Mockito.any(Trade.class)))
				.thenAnswer(invocation -> {
					// Get start and end-date range for restriction list
					Trade trade = invocation.getArgument(0);
					Date currentDate = getCurrentDate();
					boolean isTradeToday = DateUtils.isEqualWithoutTime(currentDate, trade.getTradeDate());
					Date startDate = isTradeToday ? currentDate : DateUtils.clearTime(trade.getTradeDate());
					Date endDate = isTradeToday ? currentDate : DateUtils.getEndOfDay(startDate);

					// Get restriction list
					return this.restrictionList.stream()
							.filter(restriction -> DateUtils.isDateAfterOrEqual(endDate, restriction.getStartDate()))
							.filter(restriction -> restriction.getEndDate() == null
									|| DateUtils.isDateAfterOrEqual(restriction.getEndDate(), startDate))
							.filter(restriction -> restriction.getClientInvestmentAccount() == null
									|| Objects.equals(restriction.getClientInvestmentAccount(), trade.getClientInvestmentAccount()))
							.filter(restriction -> restriction.getInvestmentSecurity() == null
									|| Objects.equals(restriction.getInvestmentSecurity(), trade.getInvestmentSecurity())
									|| Objects.equals(restriction.getInvestmentSecurity(), trade.getInvestmentSecurity().getUnderlyingSecurity()))
							.collect(Collectors.toList());
				});

		// Populate TradeOpenCloseType entities
		Arrays.asList(TradeOpenCloseTypes.values()).forEach(typeTemplate -> {
			TradeOpenCloseType type = new TradeOpenCloseType();
			type.setId(getId());
			type.setName(typeTemplate.getName());
			type.setBuy(typeTemplate.isBuy());
			type.setOpen(typeTemplate.isOpen());
			type.setClose(typeTemplate.isClose());
			this.tradeOpenCloseTypeMap.put(type.getName(), type);
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTradeDateInPresent() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade trade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(executeTrades(trade), createViolationMatcher(trade, restriction));
	}


	@Test
	public void testTradeDateInFuture() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = DateUtils.addDays(getHistoricDate(), 10);
		Trade trade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(executeTrades(trade), createViolationMatcher(trade, restriction));
	}


	@Test
	@SuppressWarnings("unused")
	public void testTradeDateInPast() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = DateUtils.addDays(getHistoricDate(), -10);
		Trade trade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restrictionPast = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restrictionCurrent = createRestriction(clientAccount, security, getHistoricDate(), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(executeTrades(trade), createViolationMatcher(trade, restrictionPast));
	}


	@Test
	@SuppressWarnings("unused")
	public void testExpiredRestriction() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade trade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), DateUtils.addDays(tradeDate, -5), this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(executeTrades(trade));
	}


	@Test
	public void testNoTrade() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade buyTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		Trade sellTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL);
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(executeTrades(buyTrade, sellTrade), createViolationMatcher(buyTrade, restriction), createViolationMatcher(sellTrade, restriction));
	}


	@Test
	public void testNoBuy() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade buyTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		Trade sellTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL);
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_BUY), null);
		validateRuleViolations(executeTrades(buyTrade, sellTrade), createViolationMatcher(buyTrade, restriction));
	}


	@Test
	public void testNoSell() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade buyTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		Trade sellTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL);
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_SELL), null);
		validateRuleViolations(executeTrades(buyTrade, sellTrade), createViolationMatcher(sellTrade, restriction));
	}


	@Test
	public void testNoSellToOpen() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade sellTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL);
		Trade sellToOpenTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellToCloseTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL_CLOSE);
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_SELL_TO_OPEN), null);
		validateRuleViolations(executeTrades(sellTrade, sellToOpenTrade, sellToCloseTrade), createViolationMatcher(sellToOpenTrade, restriction));
	}


	@Test
	@SuppressWarnings("unused")
	public void testBuyAndSellViolations() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade buyTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		Trade sellTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL);
		TradeRestriction sellRestriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_SELL), null);
		TradeRestriction sellRestrictionFuture = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, 10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_SELL), null);
		TradeRestriction buyRestriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_BUY), null);
		validateRuleViolations(
				executeTrades(buyTrade, sellTrade),
				createViolationMatcher(sellTrade, sellRestriction),
				createViolationMatcher(buyTrade, buyRestriction)
		);
	}


	@Test
	@SuppressWarnings("unused")
	public void testMultipleViolationsOnSingleTrade() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade trade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction tradeRestriction1 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction tradeRestriction2 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction buyRestriction1 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_BUY), null);
		TradeRestriction tradeRestrictionNotApplicable = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, 1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction sellRestrictionNotApplicable = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_SELL), null);
		TradeRestriction buyRestrictionNotApplicable = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, 1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_BUY), null);
		validateRuleViolations(
				executeTrades(trade),
				createViolationMatcher(trade, tradeRestriction1),
				createViolationMatcher(trade, tradeRestriction2),
				createViolationMatcher(trade, buyRestriction1)
		);
	}


	@Test
	@SuppressWarnings("unused")
	public void testStoMinimumUnderlyingLimit() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		InvestmentSecurity underlyingSecurity = createSecurity();
		security.setUnderlyingSecurity(underlyingSecurity);
		Mockito.when(this.marketDataRetriever.getLivePrice(underlyingSecurity)).thenReturn(new MarketDataLive(underlyingSecurity.getId(), BigDecimal.ONE, tradeDate));
		Trade sellToOpenTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL);
		TradeRestriction restrictionLimit0 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_UNDERLYING), BigDecimal.ZERO);
		TradeRestriction restrictionLimit1 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_UNDERLYING), BigDecimal.ONE);
		TradeRestriction restrictionLimit2 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_UNDERLYING), BigDecimal.valueOf(2));
		TradeRestriction restrictionLimit10 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_UNDERLYING), BigDecimal.TEN);
		validateRuleViolations(
				executeTrades(sellTrade, sellToOpenTrade),
				createViolationMatcher(sellToOpenTrade, restrictionLimit2),
				createViolationMatcher(sellToOpenTrade, restrictionLimit10)
		);
	}


	@Test
	@SuppressWarnings("unused")
	public void testStoMinimumOTMPercentLimit() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity underlyingSecurity = createSecurity(); // price is 1000.00

		InvestmentSecurity securityCall1 = createSecurity();
		securityCall1.setUnderlyingSecurity(underlyingSecurity);
		securityCall1.setOptionType(InvestmentSecurityOptionTypes.CALL);
		securityCall1.setOptionStrikePrice(BigDecimal.valueOf(1040.00));

		InvestmentSecurity securityCall2 = createSecurity();
		securityCall2.setUnderlyingSecurity(underlyingSecurity);
		securityCall2.setOptionType(InvestmentSecurityOptionTypes.CALL);
		securityCall2.setOptionStrikePrice(BigDecimal.valueOf(1050.00));

		InvestmentSecurity securityCall3 = createSecurity();
		securityCall3.setUnderlyingSecurity(underlyingSecurity);
		securityCall3.setOptionType(InvestmentSecurityOptionTypes.CALL);
		securityCall3.setOptionStrikePrice(BigDecimal.valueOf(1060.00));

		InvestmentSecurity securityPut1 = createSecurity();
		securityPut1.setUnderlyingSecurity(underlyingSecurity);
		securityPut1.setOptionType(InvestmentSecurityOptionTypes.PUT);
		securityPut1.setOptionStrikePrice(BigDecimal.valueOf(960.00));

		InvestmentSecurity securityPut2 = createSecurity();
		securityPut2.setUnderlyingSecurity(underlyingSecurity);
		securityPut2.setOptionType(InvestmentSecurityOptionTypes.PUT);
		securityPut2.setOptionStrikePrice(BigDecimal.valueOf(950.00));

		InvestmentSecurity securityPut3 = createSecurity();
		securityPut3.setUnderlyingSecurity(underlyingSecurity);
		securityPut3.setOptionType(InvestmentSecurityOptionTypes.PUT);
		securityPut3.setOptionStrikePrice(BigDecimal.valueOf(940.00));

		Date tradeDate = DateUtils.toDate("06/05/2020");

		Mockito.when(this.marketDataRetriever.getLivePrice(underlyingSecurity)).thenReturn(new MarketDataLive(underlyingSecurity.getId(), BigDecimal.valueOf(1000.00), tradeDate));
		Trade sellToOpenTrade1 = createTrade(clientAccount, securityCall1, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellToOpenTrade2 = createTrade(clientAccount, securityCall2, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellToOpenTrade3 = createTrade(clientAccount, securityCall3, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellToOpenTrade4 = createTrade(clientAccount, securityPut1, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellToOpenTrade5 = createTrade(clientAccount, securityPut2, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellToOpenTrade6 = createTrade(clientAccount, securityPut3, tradeDate, TradeOpenCloseTypes.SELL_OPEN);

		TradeRestriction restrictionLimit = createRestriction(clientAccount, underlyingSecurity, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_OTM_PERCENT), BigDecimal.valueOf(5.00));

		validateRuleViolations(
				executeTrades(sellToOpenTrade1, sellToOpenTrade2, sellToOpenTrade3, sellToOpenTrade4, sellToOpenTrade5, sellToOpenTrade6),
				createViolationMatcher(sellToOpenTrade1, restrictionLimit),
				createViolationMatcher(sellToOpenTrade4, restrictionLimit)
		);
	}


	@SuppressWarnings("unused")
	@Test
	public void testStoMinimumStrikeLimit() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		InvestmentSecurity underlyingSecurity = createSecurity();
		security.setUnderlyingSecurity(underlyingSecurity);
		security.setOptionStrikePrice(BigDecimal.ONE);
		Trade sellToOpenTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL_OPEN);
		Trade sellTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.SELL);
		TradeType optionsTradeType = new TradeType();
		optionsTradeType.setName(TradeType.OPTIONS);
		sellToOpenTrade.setTradeType(optionsTradeType);
		sellTrade.setTradeType(optionsTradeType);
		TradeRestriction restrictionLimit0 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_STRIKE), BigDecimal.ZERO);
		TradeRestriction restrictionLimit1 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_STRIKE), BigDecimal.ONE);
		TradeRestriction restrictionLimit2 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_STRIKE), BigDecimal.valueOf(2));
		TradeRestriction restrictionLimit10 = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -10), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_STO_MINIMUM_STRIKE), BigDecimal.TEN);
		validateRuleViolations(
				executeTrades(sellTrade, sellToOpenTrade),
				createViolationMatcher(sellToOpenTrade, restrictionLimit2),
				createViolationMatcher(sellToOpenTrade, restrictionLimit10)
		);
	}


	@Test
	public void testPreviouslyExecutedTrade() {
		// Test workflow "Executed" state look-up
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade executedTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		Trade newTrade = createTrade(clientAccount, security, tradeDate, TradeOpenCloseTypes.BUY);
		Mockito.when(this.workflowHistoryService.getWorkflowHistoryList(Mockito.argThat(arg -> MathUtils.isEqual(arg.getEntityId(), executedTrade.getId()))))
				.thenReturn(Collections.singletonList(new WorkflowHistory()));
		TradeRestriction restriction = createRestriction(clientAccount, security, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(
				executeTrades(executedTrade, newTrade),
				createViolationMatcher(newTrade, restriction)
		);
	}


	@Test
	@SuppressWarnings("unused")
	public void testHistoricVsPresentTrade() {
		InvestmentAccount clientAccount = createClientAccount();
		InvestmentSecurity security = createSecurity();
		Date previousTradeDate = DateUtils.addDays(getHistoricDate(), -1);
		Date newTradeDate = getCurrentDate();
		Trade previousTrade = createTrade(clientAccount, security, previousTradeDate, TradeOpenCloseTypes.BUY);
		Trade newTrade = createTrade(clientAccount, security, newTradeDate, TradeOpenCloseTypes.BUY);
		Date previousDateInterval1 = DateUtils.addMinutes(previousTradeDate, 30);
		Date previousDateInterval2 = DateUtils.addMinutes(previousDateInterval1, 30);
		Date previousDateInterval3 = DateUtils.addMinutes(previousDateInterval2, 30);
		Date newDateInterval1 = DateUtils.addMinutes(newTradeDate, 30);
		Date newDateInterval2 = DateUtils.addMinutes(newDateInterval1, 30);
		Date newDateInterval3 = DateUtils.addMinutes(newDateInterval2, 30);
		TradeRestriction restriction1 = createRestriction(clientAccount, security, previousTradeDate, previousDateInterval1, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction2 = createRestriction(clientAccount, security, previousDateInterval1, previousDateInterval2, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction3 = createRestriction(clientAccount, security, previousDateInterval2, previousDateInterval3, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction4 = createRestriction(clientAccount, security, previousDateInterval3, newTradeDate, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction5 = createRestriction(clientAccount, security, newTradeDate, newDateInterval1, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction6 = createRestriction(clientAccount, security, newDateInterval1, newDateInterval2, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction7 = createRestriction(clientAccount, security, newDateInterval2, newDateInterval3, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction8 = createRestriction(clientAccount, security, newDateInterval3, null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(
				executeTrades(previousTrade, newTrade),
				createViolationMatcher(previousTrade, restriction1),
				createViolationMatcher(previousTrade, restriction2),
				createViolationMatcher(previousTrade, restriction3),
				createViolationMatcher(previousTrade, restriction4),
				createViolationMatcher(newTrade, restriction4), // Active dates are compared inclusively, so restrictions with an end date matching the trade time are included
				createViolationMatcher(newTrade, restriction5)
		);
	}


	@Test
	public void testSecuritySpecificRestriction() {
		InvestmentAccount clientAccount1 = createClientAccount();
		InvestmentAccount clientAccount2 = createClientAccount();
		InvestmentSecurity security1 = createSecurity();
		InvestmentSecurity security2 = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade trade1 = createTrade(clientAccount1, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade2 = createTrade(clientAccount2, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade3 = createTrade(clientAccount1, security2, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade4 = createTrade(clientAccount2, security2, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restriction = createRestriction(null, security1, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(
				executeTrades(trade1, trade2, trade3, trade4),
				createViolationMatcher(trade1, restriction),
				createViolationMatcher(trade2, restriction)
		);
	}


	@Test
	public void testAccountSpecificRestriction() {
		InvestmentAccount clientAccount1 = createClientAccount();
		InvestmentAccount clientAccount2 = createClientAccount();
		InvestmentSecurity security1 = createSecurity();
		InvestmentSecurity security2 = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade trade1 = createTrade(clientAccount1, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade2 = createTrade(clientAccount2, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade3 = createTrade(clientAccount1, security2, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade4 = createTrade(clientAccount2, security2, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restriction = createRestriction(clientAccount1, null, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(
				executeTrades(trade1, trade2, trade3, trade4),
				createViolationMatcher(trade1, restriction),
				createViolationMatcher(trade3, restriction)
		);
	}


	@Test
	public void testPositionRestriction() {
		InvestmentAccount clientAccount1 = createClientAccount();
		InvestmentAccount clientAccount2 = createClientAccount();
		InvestmentSecurity security1 = createSecurity();
		InvestmentSecurity security2 = createSecurity();
		Date tradeDate = getHistoricDate();
		Trade trade1 = createTrade(clientAccount1, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade2 = createTrade(clientAccount2, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade3 = createTrade(clientAccount1, security2, tradeDate, TradeOpenCloseTypes.BUY);
		Trade trade4 = createTrade(clientAccount2, security2, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restriction1 = createRestriction(clientAccount1, security1, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction2 = createRestriction(clientAccount2, security2, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(
				executeTrades(trade1, trade2, trade3, trade4),
				createViolationMatcher(trade1, restriction1),
				createViolationMatcher(trade4, restriction2)
		);
	}


	@Test
	@SuppressWarnings("unused")
	public void testUnderlyingSecurityFreeze() {
		InvestmentAccount clientAccount1 = createClientAccount();
		InvestmentAccount clientAccount2 = createClientAccount();
		InvestmentSecurity security1 = createSecurity();
		InvestmentSecurity underlyingSecurity1 = createSecurity();
		security1.setUnderlyingSecurity(underlyingSecurity1);
		InvestmentSecurity security2 = createSecurity();
		InvestmentSecurity underlyingSecurity2 = createSecurity();
		security2.setUnderlyingSecurity(underlyingSecurity2);
		Date tradeDate = getHistoricDate();
		Trade invalidTrade1 = createTrade(clientAccount1, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade invalidTrade2 = createTrade(clientAccount2, security1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade invalidTrade3 = createTrade(clientAccount1, underlyingSecurity1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade invalidTrade4 = createTrade(clientAccount2, underlyingSecurity1, tradeDate, TradeOpenCloseTypes.BUY);
		Trade invalidTrade5 = createTrade(clientAccount1, security2, tradeDate, TradeOpenCloseTypes.BUY);
		Trade invalidTrade6 = createTrade(clientAccount2, security2, tradeDate, TradeOpenCloseTypes.BUY);
		Trade validTrade1 = createTrade(clientAccount1, underlyingSecurity2, tradeDate, TradeOpenCloseTypes.BUY);
		Trade validTrade2 = createTrade(clientAccount2, underlyingSecurity2, tradeDate, TradeOpenCloseTypes.BUY);
		TradeRestriction restriction1 = createRestriction(null, underlyingSecurity1, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		TradeRestriction restriction2 = createRestriction(null, security2, DateUtils.addDays(tradeDate, -1), null, this.tradeRestrictionTypeService.getTradeRestrictionTypeByName(RESTRICTION_TYPE_NO_TRADE), null);
		validateRuleViolations(
				executeTrades(invalidTrade1, invalidTrade3, invalidTrade5, invalidTrade2, invalidTrade4, invalidTrade6, validTrade1, validTrade2),
				createViolationMatcher(invalidTrade1, restriction1),
				createViolationMatcher(invalidTrade2, restriction1),
				createViolationMatcher(invalidTrade3, restriction1),
				createViolationMatcher(invalidTrade4, restriction1),
				createViolationMatcher(invalidTrade5, restriction2),
				createViolationMatcher(invalidTrade6, restriction2)
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount createClientAccount() {
		InvestmentAccount account = new InvestmentAccount();
		account.setId((int) getId());
		return account;
	}


	private InvestmentSecurity createSecurity() {
		InvestmentSecurity security = new InvestmentSecurity();
		security.setId((int) getId());
		return security;
	}


	private Trade createTrade(InvestmentAccount clientAccount, InvestmentSecurity security, Date tradeDate, TradeOpenCloseTypes openCloseType) {
		Trade trade = new Trade();
		trade.setId((int) getId());
		trade.setTradeDate(tradeDate);
		trade.setClientInvestmentAccount(clientAccount);
		trade.setInvestmentSecurity(security);
		trade.setBuy(openCloseType.isBuy());
		trade.setOpenCloseType(this.tradeOpenCloseTypeMap.get(openCloseType.getName()));
		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.OPTIONS);
		trade.setTradeType(tradeType);
		return trade;
	}


	private TradeRestriction createRestriction(InvestmentAccount clientAccount, InvestmentSecurity security, Date startDate, Date endDate, TradeRestrictionType restrictionType, BigDecimal restrictionValue) {
		TradeRestriction restriction = new TradeRestriction();
		restriction.setId((int) getId());
		restriction.setClientInvestmentAccount(clientAccount);
		restriction.setInvestmentSecurity(security);
		restriction.setStartDate(startDate);
		restriction.setEndDate(endDate);
		restriction.setRestrictionType(restrictionType);
		restriction.setRestrictionValue(restrictionValue);
		this.restrictionList.add(restriction);
		return restriction;
	}


	private Predicate<RuleViolation> createViolationMatcher(Trade trade, TradeRestriction restriction) {
		return violation -> MathUtils.isEqual(violation.getLinkedFkFieldId(), trade.getSourceEntityId())
				&& MathUtils.isEqual(violation.getCauseFkFieldId(), restriction.getId())
				&& CompareUtils.isEqual(violation.getCauseTable().getName(), "TradeRestriction");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private short getId() {
		return ++this.id;
	}


	private Date getHistoricDate() {
		return DateUtils.toDate("2018-01-01 00:00:00");
	}


	private Date getCurrentDate() {
		return DateUtils.setTime(new Date(), Time.parse("12:00:00"));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<RuleViolation> executeTrades(Trade... trades) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		for (Trade trade : trades) {
			ruleViolationList.addAll(this.tradeRestrictionEvaluator.evaluateRule(trade, this.ruleConfig, this.tradeRuleEvaluatorContext));
		}
		return ruleViolationList;
	}


	@SafeVarargs
	private final void validateRuleViolations(List<RuleViolation> ruleViolationList, Predicate<RuleViolation>... violationMatchers) {
		List<RuleViolation> ruleViolationListClone = new ArrayList<>(ruleViolationList);
		List<Predicate<RuleViolation>> violationMatcherList = Arrays.asList(violationMatchers);
		Assertions.assertEquals(violationMatcherList.size(), ruleViolationList.size(), String.format("The number of violations [%d] does not match the expected number of violations [%d].", ruleViolationListClone.size(), violationMatcherList.size()));
		for (int i = 0; i < violationMatcherList.size(); i++) {
			Predicate<RuleViolation> violationPredicate = violationMatcherList.get(i);
			Optional<RuleViolation> match = ruleViolationListClone.stream().filter(violationPredicate).findFirst();
			Assertions.assertTrue(match.isPresent(), String.format("No remaining match was found for the given violation predicate [%s]. [%d] of [%d] predicates were matched.", violationPredicate, i, violationMatcherList.size()));
			ruleViolationListClone.remove(match.get());
		}
		Assertions.assertTrue(ruleViolationListClone.isEmpty());
	}
}
