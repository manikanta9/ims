SELECT 'TradeRestrictionBroker' AS entityTableName, TradeRestrictionBrokerID AS entityId FROM TradeRestrictionBroker WHERE TradeRestrictionBrokerID = 0
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID as entityId FROM InvestmentAccount WHERE AccountNumber in ('104001', '104002', '104003')
UNION
SELECT 'BusinessCompany' AS entityTableName, BusinessCompanyID as entityId FROM BusinessCompany WHERE BusinessCompanyID BETWEEN 1 AND 10;
