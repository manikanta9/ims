package com.clifton.trade;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class TradeProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "trade";
	}


	@Override
	public boolean isMethodSkipped(Method method) {
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("saveTradeGroup");
		skipMethods.add("saveTrade");
		skipMethods.add("saveTradeFill");
		skipMethods.add("deleteTradeFill");
		skipMethods.add("saveTradeDestinationMapping");
		skipMethods.add("saveTradeCommissionOverride");
		skipMethods.add("saveTradeQuote");
		skipMethods.add("deleteTradeQuote");
		skipMethods.add("getTradeMarketDataValue");
		return skipMethods.contains(method.getName());
	}


	@Override
	protected void configureApprovedPackageNames(@SuppressWarnings("unused") Set<String> approvedList) {
		approvedList.add("restriction");
		approvedList.add("strangle");
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveTradeCommissionOverride");
		ignoredVoidSaveMethodSet.add("saveTradeEntry");
		ignoredVoidSaveMethodSet.add("saveTradeRollEntry");
		ignoredVoidSaveMethodSet.add("saveTradeRollGroup");
		return ignoredVoidSaveMethodSet;
	}


	@Override
	protected void configureDTOSkipPropertyNames(Set<String> skipPropertyNames) {
		skipPropertyNames.add("noteWithDefaults");
	}


	@Override
	protected Set<String> configureIgnoreCaches(Set<String> ignoreCaches) {
		// The trade market data value cache is primarily used in first-level cache (e.g. compliance validation); we do not want to pollute the second-level cache with this
		ignoreCaches.add("tradeMarketDataValueLatestByTradeAndBackingFieldIdCache");
		return ignoreCaches;
	}
}
