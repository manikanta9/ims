package com.clifton.trade.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.ResetTypes;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.accounting.booking.rule.TradeKeepNotionalBookingRule;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class TradeKeepNotionalBookingRuleTest {

	@Test
	public void testTRSPartialUnwindLoss() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newTRS("3870491").build())
				.addFill("475.834636", "483.3612")
				.onTradeDate("03/31/2016")
				.withSettlementDate("04/05/2016")
				.build();

		trade.getTradeFillList().get(0).setPaymentTotalPrice(BigDecimal.ZERO);
		trade.getTradeFillList().get(0).setPaymentUnitPrice(BigDecimal.ZERO);
		trade.getInvestmentSecurity().getInstrument().getHierarchy().setNoPaymentOnOpen(true);

		bookAndValidateTradeBooking(trade,
				"-12	5273162	100000	H-200	Position	3870491	483.3612	-475.834636	0	03/31/2016	1	0	-235,739.2	01/14/2016	04/05/2016	Partial position close for 3870491",
				"-13	-12	100000	H-200	Realized Gain / Loss	3870491	483.3612	475.834636	5,739.2	03/31/2016	1	5,739.2	0	01/14/2016	04/05/2016	Realized Gain / Loss loss for 3870491",
				"-14	-12	100000	H-200	Cash	USD			-5,739.2	03/31/2016	1	-5,739.2	0	01/14/2016	04/05/2016	Cash expense from close of 3870491",
				"-15	5273162	100000	H-200	Position	3870491	495.7312	-60,040.832464	0	03/31/2016	1	0	-29,764,260.8	01/14/2016	04/05/2016	Full close of remaining lot for 3870491",
				"-16	null	100000	H-200	Position	3870491	495.7312	60,052.70598	0	03/31/2016	1	0	29,770,000	01/14/2016	04/05/2016	Position opening of 3870491"
		);
	}


	@Test
	public void testTRSPartialUnwindGain() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newTRS("3870491").build())
				.withExpectedUnitPrice("508.1012")
				.addFill("452.66573", "508.1012")
				.onTradeDate("03/31/2016")
				.withSettlementDate("04/05/2016")
				.build();

		trade.getTradeFillList().get(0).setPaymentTotalPrice(BigDecimal.ZERO);
		trade.getTradeFillList().get(0).setPaymentUnitPrice(BigDecimal.ZERO);
		trade.getInvestmentSecurity().getInstrument().getHierarchy().setNoPaymentOnOpen(true);

		bookAndValidateTradeBooking(trade,
				"-12	5273162	100000	H-200	Position	3870491	508.1012	-452.66573	0	03/31/2016	1	0	-224,260.8	01/14/2016	04/05/2016	Partial position close for 3870491",
				"-13	-12	100000	H-200	Realized Gain / Loss	3870491	508.1012	452.66573	-5,739.2	03/31/2016	1	-5,739.2	0	01/14/2016	04/05/2016	Realized Gain / Loss gain for 3870491",
				"-14	-12	100000	H-200	Cash	USD			5,739.2	03/31/2016	1	5,739.2	0	01/14/2016	04/05/2016	Cash proceeds from close of 3870491",
				"-15	5273162	100000	H-200	Position	3870491	495.7312	-60,064.00137	0	03/31/2016	1	0	-29,775,739.2	01/14/2016	04/05/2016	Full close of remaining lot for 3870491",
				"-16	null	100000	H-200	Position	3870491	495.7312	60,052.70598	0	03/31/2016	1	0	29,770,000	01/14/2016	04/05/2016	Position opening of 3870491"
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void bookAndValidateTradeBooking(Trade trade, String... expectedJournalDetails) {
		List<AccountingTransaction> tradeTransactions = getTransactionListForTrade(trade);
		AccountingPositionSplitterBookingRule<TradeFill> splitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tradeTransactions.toArray(new AccountingTransaction[0]));

		TradeKeepNotionalBookingRule keepNotionalBookingRule = newTradeKeepNotionalBookingRule(tradeTransactions, splitterRule.getAccountingPositionService());
		splitterRule.setAccountingTransactionService(keepNotionalBookingRule.getAccountingTransactionService());

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						splitterRule,
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						TradeTestObjectFactory.newTradeCommissionBookingRule(),
						keepNotionalBookingRule,
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule()
				)
				.withExpectedResults(expectedJournalDetails)
				.execute();
	}


	private List<AccountingTransaction> getTransactionListForTrade(Trade trade) {
		List<AccountingTransaction> transactions = new ArrayList<>();
		transactions.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(5273162L, trade.getInvestmentSecurity()).qty(new BigDecimal("60516.6671")).price("495.7312").on("02/29/2016").build());
		transactions.get(0).setClientInvestmentAccount(trade.getClientInvestmentAccount());
		transactions.get(0).setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
		transactions.get(0).setOriginalTransactionDate(DateUtils.toDate("01/14/2016"));

		transactions.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(5273164L, trade.getInvestmentSecurity()).qty(new BigDecimal("685.85556")).price("495.7312").on("02/29/2016").build());
		transactions.get(1).setClientInvestmentAccount(trade.getClientInvestmentAccount());
		transactions.get(1).setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
		return transactions;
	}


	private TradeKeepNotionalBookingRule newTradeKeepNotionalBookingRule(List<AccountingTransaction> tradeTransactionList, AccountingPositionService accountingPositionService) {
		TradeKeepNotionalBookingRule result = TradeTestObjectFactory.newTradeKeepNotionalBookingRule();
		result.setAccountingPositionService(accountingPositionService);

		Mockito.when(
				result.getAccountingTransactionService().getAccountingTransaction(ArgumentMatchers.eq(5273162L))).thenReturn(tradeTransactionList.get(0));
		Mockito.when(
				result.getAccountingTransactionService().getAccountingTransaction(ArgumentMatchers.eq(5273164L))).thenReturn(tradeTransactionList.get(1));

		Mockito.when(result.getSystemColumnValueHandler().getSystemColumnValueForEntity(ArgumentMatchers.any(InvestmentSecurity.class), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyBoolean())).thenReturn(ResetTypes.KEEP_NOTIONAL.toString());

		return result;
	}
}
