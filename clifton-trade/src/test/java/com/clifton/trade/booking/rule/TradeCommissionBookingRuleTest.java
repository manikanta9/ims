package com.clifton.trade.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccountBuilder;
import com.clifton.accounting.commission.AccountingCommission;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationProperties;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.accounting.booking.rule.TradeCommissionBookingRule;
import com.clifton.trade.accounting.booking.rule.TradeCreatePositionBookingRule;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.Collections;


public class TradeCommissionBookingRuleTest {

	@Test
	public void testCommissionBookingRule_StockOpen() {
		// buy to open 14400 shares of BWR
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.ofType(3108, "BWR", InvestmentType.STOCKS).build())
				.withId(1)
				.buy()
				.withExpectedUnitPrice("6.94544")
				.withCommissionPerUnit(0.02)
				.addFillWithQuantity(14400)
				.createFills();

		// create and wire booking rules
		TradeCreatePositionBookingRule positionRule = TradeTestObjectFactory.newTradeCreatePositionBookingRule();
		AccountingRemoveZeroValueRecordBookingRule<TradeFill> removeRule = new AccountingRemoveZeroValueRecordBookingRule<>();

		// set up commission rule
		TradeCommissionBookingRule commissionRule = TradeTestObjectFactory.newTradeCommissionBookingRule();
		AccountingCommission commission = new AccountingCommission(AccountingAccountBuilder.createCommission().build(), trade.getPayingSecurity());
		commission.setCommissionAmount(MathUtils.multiply(trade.getCommissionPerUnit(), trade.getTradeFillList().get(0).getQuantity()));
		Mockito.when(commissionRule.getAccountingCommissionCalculator().getAccountingCommissionListForJournalDetail(Mockito.any())).thenReturn(Collections.singletonList(commission));

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(positionRule, commissionRule, removeRule)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	BWR	6.94544	14,400	100,014.34	10/17/2012	1	100,014.34	100,014.34	10/17/2012	10/17/2012	null",
						"-11	-10	100000	H-200	Cash	USD			-100,014.34	10/17/2012	1	-100,014.34	0	10/17/2012	10/17/2012	Cash expense from opening of BWR",
						"-12	-10	100000	H-200	Commission	BWR	0.02	14,400	288	10/17/2012	1	288	0	10/17/2012	10/17/2012	Commission expense for BWR",
						"-13	-10	100000	H-200	Cash	USD			-288	10/17/2012	1	-288	0	10/17/2012	10/17/2012	Cash expense from opening of BWR"
				)
				.execute();
	}


	@Test
	public void testCommissionBookingRule_FxRateOnForeignFutureClose() {
		// buy to open 14400 shares of BWR
		Trade trade = TradeBuilder.newTradeForSecurity(
				InvestmentSecurityBuilder.ofType(3108, "VGM2", InvestmentType.FUTURES)
						.withPriceMultiplier(10)
						.withTradingCurrency(InvestmentSecurityBuilder.newEUR())
						.build())
				.inEUR()
				.withExpectedUnitPrice("2152")
				.withCommissionPerUnit(2)
				.withExchangeRate("1.2498")
				.onTradeDate("06/12/2012")
				.addFillWithQuantity(450)
				.createFills();

		TradeCreatePositionBookingRule positionRule = TradeTestObjectFactory.newTradeCreatePositionBookingRule();
		AccountingRealizedGainLossBookingRule<TradeFill> gainLossRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule();
		TradeCommissionBookingRule commissionRule = TradeTestObjectFactory.newTradeCommissionBookingRule();

		// Set up splitter rule
		AccountingTransaction transaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, trade.getInvestmentSecurity())
				.qty(450).price("2100").costBasis("9450000").exchangeRateToBase("1.31").on("1/1/2010").build();
		AccountingPositionSplitterBookingRule<TradeFill> splitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(transaction);

		// set up commission rule
		AccountingCommission commission = new AccountingCommission(AccountingAccountBuilder.createCommission().build(), trade.getPayingSecurity());
		commission.setCommissionAmount(MathUtils.multiply(trade.getCommissionPerUnit(), trade.getTradeFillList().get(0).getQuantity()));
		Mockito.when(commissionRule.getAccountingCommissionCalculator().getAccountingCommissionListForJournalDetail(Mockito.any())).thenReturn(Collections.singletonList(commission));

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(positionRule, splitterRule, gainLossRule, commissionRule)
				.withExpectedResults(
						"-12	11696	100000	H-200	Position	VGM2	2,152	-450	0	06/12/2012	1.31	0	-9,450,000	01/01/2010	06/12/2012	Full position close for VGM2",
						"-13	-12	100000	H-200	Realized Gain / Loss	VGM2	2,152	450	-234,000	06/12/2012	1.2498	-292,453.2	0	01/01/2010	06/12/2012	Realized Gain / Loss gain for VGM2",
						"-14	-12	100000	H-200	Currency	EUR			234,000	06/12/2012	1.2498	292,453.2	0	01/01/2010	06/12/2012	Currency proceeds from close of VGM2",
						"-15	-12	100000	H-200	Commission	VGM2	2	450	900	06/12/2012	1.2498	1,124.82	0	01/01/2010	06/12/2012	Commission expense for VGM2",
						"-16	-12	100000	H-200	Currency	EUR			-900	06/12/2012	1.2498	-1,124.82	0	01/01/2010	06/12/2012	Currency expense from close of VGM2"
				)
				.execute();
	}


	@Test
	public void testCommissionBookingRule_FeeOverrideAllocatedAcrossTwoLots() {
		// fill closes 2 lots and trade has Fee populated (check it's allocated proportionally)
		InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(3238, "EFA", InvestmentType.STOCKS);
		Trade trade = TradeTestObjectFactory.newTrade_OneFill(false, security, new BigDecimal("81895"), new BigDecimal("57.38"), BigDecimal.ONE, DateUtils.toDate("06/28/2013"));
		trade.setId(1);
		trade.setFeeAmount(new BigDecimal("81.77"));
		trade.setCommissionPerUnit(new BigDecimal("0.03"));

		// set up splitter rule
		AccountingTransaction transaction1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1589985L, trade.getInvestmentSecurity()).qty(538).price("49.1232").on("01/04/2012").build();
		AccountingTransaction transaction2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1589989L, trade.getInvestmentSecurity()).qty(325500).price("58.81").on("01/04/2012").build();
		AccountingPositionSplitterBookingRule<TradeFill> splitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(transaction1, transaction2);

		// set up commission rule
		TradeCommissionBookingRule commissionRule = TradeTestObjectFactory.newTradeCommissionBookingRule();
		AccountingCommission fullyCloseCommission = new AccountingCommission(AccountingAccountBuilder.createCommission().build(), trade.getPayingSecurity());
		fullyCloseCommission.setCommissionAmount(MathUtils.multiply(new BigDecimal(538), trade.getCommissionPerUnit()));
		AccountingCommission fullyCloseFee = new AccountingCommission(AccountingAccountBuilder.createSecFees().build(), trade.getPayingSecurity());
		fullyCloseFee.setCommissionAmount(InvestmentCalculatorUtils.roundLocalAmount(MathUtils.multiply(trade.getFeeAmount(), MathUtils.divide(new BigDecimal(538), trade.getQuantityIntended())), security));
		AccountingCommission partialCloseCommission = new AccountingCommission(AccountingAccountBuilder.createCommission().build(), trade.getPayingSecurity());
		partialCloseCommission.setCommissionAmount(MathUtils.multiply(new BigDecimal(81357), trade.getCommissionPerUnit()));
		AccountingCommission partialCloseFee = new AccountingCommission(AccountingAccountBuilder.createSecFees().build(), trade.getPayingSecurity());
		partialCloseFee.setCommissionAmount(InvestmentCalculatorUtils.roundLocalAmount(MathUtils.multiply(trade.getFeeAmount(), MathUtils.divide(new BigDecimal(81357), trade.getQuantityIntended())), security));
		Mockito.doAnswer(invocationOnMock -> {
			AccountingCommissionCalculationProperties<?> properties = invocationOnMock.getArgument(0);
			if (properties.getPositionDetail().getParentTransaction().getId().equals(transaction1.getId())) {
				return CollectionUtils.createList(fullyCloseCommission, fullyCloseFee);
			}
			else {
				return CollectionUtils.createList(partialCloseCommission, partialCloseFee);
			}
		}).when(commissionRule.getAccountingCommissionCalculator()).getAccountingCommissionListForJournalDetail(Mockito.any());

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						splitterRule,
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						commissionRule,
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule())
				.withExpectedResults(
						"-12	1589985	100000	H-200	Position	EFA	57.38	-538	-26,428.28	06/28/2013	1	-26,428.28	-26,428.28	01/04/2012	06/28/2013	Full position close for EFA",
						"-13	1589989	100000	H-200	Position	EFA	57.38	-81,357	-4,784,605.17	06/28/2013	1	-4,784,605.17	-4,784,605.17	01/04/2012	06/28/2013	Partial position close for EFA",
						"-16	-12	100000	H-200	Realized Gain / Loss	EFA	57.38	538	-4,442.16	06/28/2013	1	-4,442.16	0	01/04/2012	06/28/2013	Realized Gain / Loss gain for EFA",
						"-17	-13	100000	H-200	Realized Gain / Loss	EFA	57.38	81,357	116,340.51	06/28/2013	1	116,340.51	0	01/04/2012	06/28/2013	Realized Gain / Loss loss for EFA",
						"-18	-12	100000	H-200	Commission	EFA	0.03	538	16.14	06/28/2013	1	16.14	0	01/04/2012	06/28/2013	Commission expense for EFA",
						"-20	-12	100000	H-200	SEC Fees	EFA	0.001	538	0.54	06/28/2013	1	0.54	0	01/04/2012	06/28/2013	SEC Fees expense for EFA",
						"-22	-13	100000	H-200	Commission	EFA	0.03	81,357	2,440.71	06/28/2013	1	2,440.71	0	01/04/2012	06/28/2013	Commission expense for EFA",
						"-24	-13	100000	H-200	SEC Fees	EFA	0.0009984	81,357	81.23	06/28/2013	1	81.23	0	01/04/2012	06/28/2013	SEC Fees expense for EFA",
						"-26	-12	100000	H-200	Cash	USD			30,853.76	06/28/2013	1	30,853.76	0	01/04/2012	06/28/2013	Cash proceeds from close of EFA",
						"-27	-13	100000	H-200	Cash	USD			4,665,742.72	06/28/2013	1	4,665,742.72	0	01/04/2012	06/28/2013	Cash proceeds from close of EFA"
				)
				.execute();
	}
}
