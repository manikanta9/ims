package com.clifton.trade.booking.rule;


import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.accounting.booking.rule.TradeCreateCurrencyPositionBookingRule;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class TradeCreateCurrencyPositionBookingRuleTests {

	@Test
	public void testCurrencyTrade_Buy() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newJPY())
				.buy()
				.withExchangeRate("79.813")
				.onTradeDate("05/08/2012")
				.addCurrencyFill("518784500.00")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRule(TradeTestObjectFactory.newTradeCreateCurrencyPositionBookingRule())
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	JPY			518,784,500	05/08/2012	0.0125292872	6,500,000	0	05/08/2012	05/08/2012	Converting JPY to USD",
						"-11	-10	100000	H-200	Cash	USD			-6,500,000	05/08/2012	1	-6,500,000	0	05/08/2012	05/08/2012	Cash expense from opening of JPY"
				).execute();
	}


	@Test
	public void testCurrencyTrade_Sell() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newGBP())
				.sell()
				.withExchangeRate("1.5891")
				.onTradeDate("04/11/2012")
				.addCurrencyFill("35000.00")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRule(TradeTestObjectFactory.newTradeCreateCurrencyPositionBookingRule())
				.withAdditionalFields()
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	GBP	true	Goldman Sachs & Co.			-35,000	04/11/2012	1.5891	-55,618.5	0	04/11/2012	04/11/2012	Converting GBP to USD",
						"-11	-10	100000	H-200	Cash	USD	false	Goldman Sachs & Co.			55,618.5	04/11/2012	1	55,618.5	0	04/11/2012	04/11/2012	Cash proceeds from opening of GBP"
				).execute();
	}


	@Test
	public void testCurrencyTrade_Buy_InJPYAccount() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCAD())
				.buy()
				.forClientAccountInBaseCurrency(InvestmentSecurityBuilder.newJPY())
				.withExchangeRate("83.89332125")
				.onTradeDate("05/08/2012")
				.addCurrencyFill("6500000")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRule(TradeTestObjectFactory.newTradeCreateCurrencyPositionBookingRule())
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	CAD			6,500,000	05/08/2012	83.89332125	545,306,588	0	05/08/2012	05/08/2012	Converting CAD to JPY",
						"-11	-10	100000	H-200	Cash	JPY			-545,306,588	05/08/2012	1	-545,306,588	0	05/08/2012	05/08/2012	Cash expense from opening of CAD"
				).execute();
	}


	@Test
	public void testCurrencyTrade_Sell_InJPYAccount() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCAD())
				.sell()
				.forClientAccountInBaseCurrency(InvestmentSecurityBuilder.newJPY())
				.withExchangeRate("83.89332125")
				.onTradeDate("05/08/2012")
				.addCurrencyFill("6500000")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRule(TradeTestObjectFactory.newTradeCreateCurrencyPositionBookingRule())
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	CAD			-6,500,000	05/08/2012	83.89332125	-545,306,588	0	05/08/2012	05/08/2012	Converting CAD to JPY",
						"-11	-10	100000	H-200	Cash	JPY			545,306,588	05/08/2012	1	545,306,588	0	05/08/2012	05/08/2012	Cash proceeds from opening of CAD"
				).execute();
	}


	@Test
	public void testCurrencyTrade_Three_Currencies() {
		// Sell USD with settlement currency EUR for client account with base currency GBP
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newUSD())
				.sell()
				.forClientAccountInBaseCurrency(InvestmentSecurityBuilder.newGBP())
				.withPayingSecurity(InvestmentSecurityBuilder.newEUR())
				.withExchangeRate("1.085350000000")
				.onTradeDate("04/29/2020")
				.addCurrencyFill("7000000")
				.build();

		TradeCreateCurrencyPositionBookingRule currencyPositionBookingRule = TradeTestObjectFactory.newTradeCreateCurrencyPositionBookingRule(trade.getInvestmentSecurity(), trade.getClientInvestmentAccount().getBaseCurrency(), new BigDecimal("0.804084750533"));

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRule(currencyPositionBookingRule)
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	USD			-7,000,000	04/29/2020	0.8040847505	-5,628,593.25	0	04/29/2020	04/29/2020	Converting USD to GBP",
						"-11	-10	100000	H-200	Currency	EUR			6,449,532.41	04/29/2020	0.872713383	5,628,593.25	0	04/29/2020	04/29/2020	Converting EUR to GBP"
				).execute();
	}

	@Test
	public void testCurrencyTrade_CAD_For_USD_With_AUD_Base() {
		// Buy CAD with USD settlement currency for client account with base currency AUD
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCAD())
				.buy()
				.forClientAccountInBaseCurrency(InvestmentSecurityBuilder.newAUD())
				.withPayingSecurity(InvestmentSecurityBuilder.newUSD())
				.withExchangeRate("1.389060000000")
				.onTradeDate("05/19/2020")
				.addCurrencyFill("19911.21")
				.build();

		TradeCreateCurrencyPositionBookingRule currencyPositionBookingRule = TradeTestObjectFactory.newTradeCreateCurrencyPositionBookingRule(trade.getInvestmentSecurity(), trade.getClientInvestmentAccount().getBaseCurrency(), new BigDecimal("1.0905273964"));

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRule(currencyPositionBookingRule)
				.withExpectedResults(
						"-10	null	100000	H-200	Currency	CAD			19,911.21	05/19/2020	1.0905273964	21,713.72	0	05/19/2020	05/19/2020	Converting CAD to AUD",
						"-11	-10	100000	H-200	Currency	USD			-14,334.31	05/19/2020	1.5148075	-21,713.72	0	05/19/2020	05/19/2020	Converting USD to AUD"
				).execute();
	}
}
