package com.clifton.trade.booking.rule;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountBuilder;
import com.clifton.accounting.commission.AccountingCommission;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetailBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferTypeBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.system.schema.SystemTableBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.accounting.booking.rule.TradeCommissionBookingRule;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author terrys
 */
public class TradePositionCollateralOffsetsBookingRuleTest {

	@Test
	public void testPartialClose() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newBond("912796TV1").withPriceMultiplier(new BigDecimal("0.0100000000")).build())
				.withExpectedUnitPrice("99.243816666700000")
				.onTradeDate("01/15/2020")
				.addFillWithQuantity(15500000)
				.withCollateralTrade(true)
				.withAccrualAmount1("500")
				.withCommissionPerUnit(0.02)
				.createFills();

		// accounting position transfer
		AccountingPositionTransferDetail accountingPositionTransferDetail = AccountingPositionTransferDetailBuilder.createEmpty()
				.withPositionTransfer(AccountingPositionTransferBuilder
						.createEmpty()
						.withId(49913)
						.withType(AccountingPositionTransferTypeBuilder.createClientCollateralTransferPost().toAccountingPositionTransferType())
						.toAccountPositionTransfer()
				)
				.withId(334464)
				.toAccountingPositionTransferDetail();
		// set up commission rule
		TradeCommissionBookingRule commissionRule = TradeTestObjectFactory.newTradeCommissionBookingRule();
		AccountingCommission commission = new AccountingCommission(AccountingAccountBuilder.createCommission().build(), trade.getPayingSecurity());
		commission.setCommissionAmount(MathUtils.multiply(trade.getCommissionPerUnit(), trade.getTradeFillList().get(0).getQuantity()));
		Mockito.when(commissionRule.getAccountingCommissionCalculator().getAccountingCommissionListForJournalDetail(Mockito.any())).thenReturn(Collections.singletonList(commission));
		// realized gain loss rule.
		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(createAccountingTransactions(trade.getInvestmentSecurity()).toArray(new AccountingTransaction[0])),
						AccountingTestObjectFactory.newAccountingPositionCollateralOffsetsBookingRule(
								accountingPositionTransferDetail,
								createAccountingTransactions(trade.getInvestmentSecurity()).toArray(new AccountingTransaction[0])
						),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						commissionRule,
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withExpectedResults(
						// ID, ParentID, ClientAccountNumber, HoldingAccountNumber, AccountingAccountName, Symbol, price, quantity, localdebitcredit, transactionDate, fxrate, basedebitcredit, positionCostbasis, originalDate, settlementDate, description
						"-11	-12	100000	H-200	Cash Collateral	USD			15,382,791.58	01/15/2020	1	15,382,791.58	0	null	01/15/2020	Cash proceeds from close of 912796TV1",
						"-12	16344153	100000	H-200	Position Collateral	912796TV1	99.2438166667	-15,500,000	-15,382,791.59	01/15/2020	1	-15,382,791.59	-15,382,791.59	null	01/15/2020	Partial position close for 912796TV1",
						"-13	16344154	100000	H-200	Position Collateral Receivable	912796TV1	99.2438166667	-15,500,000	-15,382,791.59	01/15/2020	1	-15,382,791.59	-15,382,791.59	null	01/15/2020	[Position Collateral Receivable] offset for [912796TV1]",
						"-14	16344155	100000	H-200	Position Collateral Payable	912796TV1	99.2438166667	15,500,000	15,382,791.59	01/15/2020	1	15,382,791.59	15,382,791.59	null	01/15/2020	[Position Collateral Payable] offset for [912796TV1]",
						"-15	-12	100000	H-200	Interest Income	912796TV1			-500	01/15/2020	1	-500	0	null	01/15/2020	Accrued Interest Income for 912796TV1",
						"-16	-12	100000	H-200	Cash	USD			500	01/15/2020	1	500	0	null	01/15/2020	Cash proceeds from close of 912796TV1",
						"-17	-12	100000	H-200	Realized Gain / Loss	912796TV1	99.2438166667	15,500,000	0.01	01/15/2020	1	0.01	0	null	01/15/2020	Realized Gain / Loss loss for 912796TV1",
						"-20	-12	100000	H-200	Commission	912796TV1	0.02	15,500,000	310,000	01/15/2020	1	310,000	0	null	01/15/2020	Commission expense for 912796TV1",
						"-21	-12	100000	H-200	Cash	USD			-310,000	01/15/2020	1	-310,000	0	null	01/15/2020	Cash expense from close of 912796TV1"
				)
				.execute();
	}


	@Test
	public void testFullClose() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newBond("912796TV1").withPriceMultiplier(new BigDecimal("0.0100000000")).build())
				.withExpectedUnitPrice("99.243816666700000")
				.onTradeDate("01/15/2020")
				.addFillWithQuantity(31000000)
				.withCollateralTrade(true)
				.withAccrualAmount1("500")
				.withCommissionPerUnit(0.02)
				.createFills();

		// accounting position transfer
		AccountingPositionTransferDetail accountingPositionTransferDetail = AccountingPositionTransferDetailBuilder.createEmpty()
				.withPositionTransfer(AccountingPositionTransferBuilder
						.createEmpty()
						.withId(49913)
						.withType(AccountingPositionTransferTypeBuilder.createClientCollateralTransferPost().toAccountingPositionTransferType())
						.toAccountPositionTransfer()
				)
				.withId(334464)
				.toAccountingPositionTransferDetail();
		// set up commission rule
		TradeCommissionBookingRule commissionRule = TradeTestObjectFactory.newTradeCommissionBookingRule();
		AccountingCommission commission = new AccountingCommission(AccountingAccountBuilder.createCommission().build(), trade.getPayingSecurity());
		commission.setCommissionAmount(MathUtils.multiply(trade.getCommissionPerUnit(), trade.getTradeFillList().get(0).getQuantity()));
		Mockito.when(commissionRule.getAccountingCommissionCalculator().getAccountingCommissionListForJournalDetail(Mockito.any())).thenReturn(Collections.singletonList(commission));
		// realized gain loss rule.
		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(createAccountingTransactions(trade.getInvestmentSecurity()).toArray(new AccountingTransaction[0])),
						AccountingTestObjectFactory.newAccountingPositionCollateralOffsetsBookingRule(
								accountingPositionTransferDetail,
								createAccountingTransactions(trade.getInvestmentSecurity()).toArray(new AccountingTransaction[0])
						),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						commissionRule,
						AccountingTestObjectFactory.newRemoveZeroValueRecordBookingRule()
				)
				.withExpectedResults(
						// ID, ParentID, ClientAccountNumber, HoldingAccountNumber, AccountingAccountName, Symbol, price, quantity, localdebitcredit, transactionDate, fxrate, basedebitcredit, positionCostbasis, originalDate, settlementDate, description
						"-11	-12	100000	H-200	Cash Collateral	USD			30,765,583.17	01/15/2020	1	30,765,583.17	0	null	01/15/2020	Cash proceeds from close of 912796TV1",
						"-12	16344153	100000	H-200	Position Collateral	912796TV1	99.2438166667	-31,000,000	-30,765,583.17	01/15/2020	1	-30,765,583.17	-30,765,583.17	null	01/15/2020	Full position close for 912796TV1",
						"-13	16344154	100000	H-200	Position Collateral Receivable	912796TV1	99.2438166667	-31,000,000	-30,765,583.17	01/15/2020	1	-30,765,583.17	-30,765,583.17	null	01/15/2020	[Position Collateral Receivable] offset for [912796TV1]",
						"-14	16344155	100000	H-200	Position Collateral Payable	912796TV1	99.2438166667	31,000,000	30,765,583.17	01/15/2020	1	30,765,583.17	30,765,583.17	null	01/15/2020	[Position Collateral Payable] offset for [912796TV1]",
						"-15	-12	100000	H-200	Interest Income	912796TV1			-500	01/15/2020	1	-500	0	null	01/15/2020	Accrued Interest Income for 912796TV1",
						"-16	-12	100000	H-200	Cash	USD			500	01/15/2020	1	500	0	null	01/15/2020	Cash proceeds from close of 912796TV1",
						"-20	-12	100000	H-200	Commission	912796TV1	0.02	31,000,000	620,000	01/15/2020	1	620,000	0	null	01/15/2020	Commission expense for 912796TV1",
						"-21	-12	100000	H-200	Cash	USD			-620,000	01/15/2020	1	-620,000	0	null	01/15/2020	Cash expense from close of 912796TV1"
				)
				.execute();
	}


	private List<AccountingTransaction> createAccountingTransactions(InvestmentSecurity security) {
		final List<AccountingTransaction> accountingTransactionList = new ArrayList<>();

		AccountingTransaction parentTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16342599)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION).asPosition().withId(120).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("30765583.17")
				.build();
		parentTransaction.setDescription("Positions in a security instrument");
		parentTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		parentTransaction.setFkFieldId(333709);
		accountingTransactionList.add(parentTransaction);

		AccountingTransaction accountingTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16344153)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION_COLLATERAL).asPosition().asCollateral().withId(753).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("30765583.17")
				.build();
		accountingTransaction.setParentTransaction(parentTransaction);
		accountingTransaction.setDescription("Position opening from transfer from 668451: Verizon - BAMT DB Large Cap");
		accountingTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		accountingTransaction.setFkFieldId(334464);
		accountingTransactionList.add(accountingTransaction);

		AccountingTransaction offsetTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16344154)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.POSITION_COLLATERAL_RECEIVABLE).asPosition().asCollateral().asReceivable().withId(800).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("30765583.17")
				.build();
		offsetTransaction.setDescription("Position opening from transfer from 668451: Verizon - BAMT DB Large Cap");
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setParentTransaction(accountingTransaction);
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setFkFieldId(334464);
		accountingTransactionList.add(offsetTransaction);

		offsetTransaction = AccountingTransactionBuilder.newTransactionForIdAndSecurity(1, security)
				.id(16344155)
				.accountingAccount(AccountingAccountBuilder.newAsset(AccountingAccount.POSITION_COLLATERAL_PAYABLE).asPosition().asCollateral().asReceivable().asExecutingBrokerSpecific().withId(800).build())
				.journal(AccountingTestObjectFactory.newAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, (short) 176))
				.qty("-31000000.0000000000")
				.price("99.243816666700000")
				.costBasis("-30765583.17")
				.build();
		offsetTransaction.setDescription("Position opening from transfer from 668451: Verizon - BAMT DB Large Cap");
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setParentTransaction(accountingTransaction);
		offsetTransaction.setSystemTable(SystemTableBuilder.createAccountingPositionTransferDetail().toSystemTable());
		offsetTransaction.setFkFieldId(334464);
		accountingTransactionList.add(offsetTransaction);

		return accountingTransactionList;
	}
}
