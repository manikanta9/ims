package com.clifton.trade.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingAggregateCurrencyBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Test;


public class TradeAggregateCashBookingRuleTest {

	@Test
	public void testAggregateMultipleCashRecords() {
		// single sell of 214 that fully closes 3 different existing lots
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("TUH0").withPriceMultiplier(2000).build())
				.withExpectedUnitPrice("108.9453125000")
				.withTotalCommission("100")
				.addFillWithQuantity(214)
				.createFills();

		// define 3 existing lots being closed
		AccountingTransaction tran1 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, trade.getInvestmentSecurity())
				.qty(211).price("217562.5").costBasis("45905687.50").on("1/1/2010").build();
		AccountingTransaction tran2 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11732L, trade.getInvestmentSecurity())
				.qty(2).price("217906.25").costBasis("435812.50").on("2/1/2010").build();
		AccountingTransaction tran3 = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11796L, trade.getInvestmentSecurity())
				.qty(1).price("216140.63").costBasis("216140.63").on("3/1/2010").build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-10	null	100000	H-200	Position	TUH0	true	Goldman Sachs & Co.	108.9453125	-214	0	10/17/2012	1	0	-46,628,593.75	10/17/2012	10/17/2012	null",
						"-11	-10	100000	H-200	Cash	USD	true	Goldman Sachs & Co.			0	10/17/2012	1	0	0	10/17/2012	10/17/2012	Cash proceeds from opening of TUH0"
				)
				.execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran1, tran2, tran3)
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-12	11696	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-211	0	10/17/2012	1	0	-45,974,921.87	01/01/2010	10/17/2012	Full position close for TUH0",
						"-13	11732	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-2	0	10/17/2012	1	0	-435,781.25	02/01/2010	10/17/2012	Full position close for TUH0",
						"-14	11796	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-1	0	10/17/2012	1	0	-217,890.63	03/01/2010	10/17/2012	Full position close for TUH0"
				)
				.execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran1, tran2, tran3),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-12	11696	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-211	0	10/17/2012	1	0	-45,905,687.5	01/01/2010	10/17/2012	Full position close for TUH0",
						"-13	11732	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-2	0	10/17/2012	1	0	-435,812.5	02/01/2010	10/17/2012	Full position close for TUH0",
						"-14	11796	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-1	0	10/17/2012	1	0	-216,140.63	03/01/2010	10/17/2012	Full position close for TUH0",
						"-15	-12	100000	H-200	Realized Gain / Loss	TUH0	false	Goldman Sachs & Co.	108.9453125	211	-69,234.37	10/17/2012	1	-69,234.37	0	01/01/2010	10/17/2012	Realized Gain / Loss gain for TUH0",
						"-16	-12	100000	H-200	Cash	USD	true	Goldman Sachs & Co.			69,234.37	10/17/2012	1	69,234.37	0	01/01/2010	10/17/2012	Cash proceeds from close of TUH0",
						"-17	-13	100000	H-200	Realized Gain / Loss	TUH0	false	Goldman Sachs & Co.	108.9453125	2	31.25	10/17/2012	1	31.25	0	02/01/2010	10/17/2012	Realized Gain / Loss loss for TUH0",
						"-18	-13	100000	H-200	Cash	USD	true	Goldman Sachs & Co.			-31.25	10/17/2012	1	-31.25	0	02/01/2010	10/17/2012	Cash expense from close of TUH0",
						"-19	-14	100000	H-200	Realized Gain / Loss	TUH0	false	Goldman Sachs & Co.	108.9453125	1	-1,750	10/17/2012	1	-1,750	0	03/01/2010	10/17/2012	Realized Gain / Loss gain for TUH0",
						"-20	-14	100000	H-200	Cash	USD	true	Goldman Sachs & Co.			1,750	10/17/2012	1	1,750	0	03/01/2010	10/17/2012	Cash proceeds from close of TUH0"
				)
				.execute();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran1, tran2, tran3),
						AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule(),
						new AccountingAggregateCurrencyBookingRule<>(),
						new AccountingRemoveZeroValueRecordBookingRule<>()
				)
				.withAdditionalFields()
				.withExpectedResults(
						"-12	11696	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-211	0	10/17/2012	1	0	-45,905,687.5	01/01/2010	10/17/2012	Full position close for TUH0",
						"-13	11732	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-2	0	10/17/2012	1	0	-435,812.5	02/01/2010	10/17/2012	Full position close for TUH0",
						"-14	11796	100000	H-200	Position	TUH0	false	Goldman Sachs & Co.	108.9453125	-1	0	10/17/2012	1	0	-216,140.63	03/01/2010	10/17/2012	Full position close for TUH0",
						"-15	-12	100000	H-200	Realized Gain / Loss	TUH0	false	Goldman Sachs & Co.	108.9453125	211	-69,234.37	10/17/2012	1	-69,234.37	0	01/01/2010	10/17/2012	Realized Gain / Loss gain for TUH0",
						"-16	-12	100000	H-200	Cash	USD	true	Goldman Sachs & Co.			69,234.37	10/17/2012	1	69,234.37	0	01/01/2010	10/17/2012	Cash proceeds from close of TUH0",
						"-17	-13	100000	H-200	Realized Gain / Loss	TUH0	false	Goldman Sachs & Co.	108.9453125	2	31.25	10/17/2012	1	31.25	0	02/01/2010	10/17/2012	Realized Gain / Loss loss for TUH0",
						"-18	-13	100000	H-200	Cash	USD	true	Goldman Sachs & Co.			-31.25	10/17/2012	1	-31.25	0	02/01/2010	10/17/2012	Cash expense from close of TUH0",
						"-19	-14	100000	H-200	Realized Gain / Loss	TUH0	false	Goldman Sachs & Co.	108.9453125	1	-1,750	10/17/2012	1	-1,750	0	03/01/2010	10/17/2012	Realized Gain / Loss gain for TUH0",
						"-20	-14	100000	H-200	Cash	USD	true	Goldman Sachs & Co.			1,750	10/17/2012	1	1,750	0	03/01/2010	10/17/2012	Cash proceeds from close of TUH0"
				)
				.execute();
	}
}
