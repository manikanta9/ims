package com.clifton.trade.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.accounting.booking.rule.TradeCreatePositionBookingRule;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class TradeCreatePositionBookingRuleTest {

	@Test
	public void testOpenFutureLong_BUY() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FUH0").withPriceMultiplier(50).build())
				.buy()
				.withExpectedUnitPrice("10.32423")
				.addFillWithQuantity(10)
				.createFills();

		evaluateBookingRuleForFillWithResults(trade.getTradeFillList().get(0),
				"-10	null	100000	H-200	Position	FUH0	10.32423	10	0	10/17/2012	1	0	5,162.12	10/17/2012	10/17/2012	null",
				"-11	-10	100000	H-200	Cash	USD			0	10/17/2012	1	0	0	10/17/2012	10/17/2012	Cash proceeds from opening of FUH0"
		);
	}


	@Test
	public void testFutureTradeOpenShort_SELL() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FVU0").withPriceMultiplier(1000).build())
				.withExpectedUnitPrice("116.8976563")
				.addFillWithQuantity(60)
				.createFills();

		evaluateBookingRuleForFillWithResults(trade.getTradeFillList().get(0),
				"-10	null	100000	H-200	Position	FVU0	116.8976563	-60	0	10/17/2012	1	0	-7,013,859.38	10/17/2012	10/17/2012	null",
				"-11	-10	100000	H-200	Cash	USD			0	10/17/2012	1	0	0	10/17/2012	10/17/2012	Cash proceeds from opening of FVU0"
		);
	}


	@Test
	public void testOpenStock_BUY() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newStock("BWR").build())
				.buy()
				.withExpectedUnitPrice("6.94544")
				.addFillWithQuantity(14400)
				.createFills();

		evaluateBookingRuleForFillWithResults(trade.getTradeFillList().get(0),
				"-10	null	100000	H-200	Position	BWR	6.94544	14,400	100,014.34	10/17/2012	1	100,014.34	100,014.34	10/17/2012	10/17/2012	null",
				"-11	-10	100000	H-200	Cash	USD			-100,014.34	10/17/2012	1	-100,014.34	0	10/17/2012	10/17/2012	Cash expense from opening of BWR"
		);
	}


	@Test
	public void testOpenStockShort_SELL() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newStock("BWR").build())
				.withExpectedUnitPrice("6.94544")
				.addFillWithQuantity(100)
				.createFills();

		evaluateBookingRuleForFillWithResults(trade.getTradeFillList().get(0),
				"-10	null	100000	H-200	Position	BWR	6.94544	-100	-694.54	10/17/2012	1	-694.54	-694.54	10/17/2012	10/17/2012	null",
				"-11	-10	100000	H-200	Cash	USD			694.54	10/17/2012	1	694.54	0	10/17/2012	10/17/2012	Cash proceeds from opening of BWR"
		);
	}


	@Test
	public void testBondTrade_BuyWithFactor2() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newBond("84604KAD1").build())
				.buy()
				.withExpectedUnitPrice("103.625")
				.withCurrentFactor("0.7376357612")
				.addFillWithQuantity("2139143.70")
				.createFills();
		trade.getInvestmentSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		// must set the original face after the fill is created to avoid notional from being correctly calculated - results in only two journal details
		trade.setOriginalFace(new BigDecimal("2900000.00"));

		evaluateBookingRuleForFillWithResults(trade.getTradeFillList().get(0),
				"-10	null	100000	H-200	Position	84604KAD1	103.625	2,900,000	3,005,125	10/17/2012	1	3,005,125	3,005,125	10/17/2012	10/17/2012	Position opening at Original Face for 84604KAD1",
				"-11	-10	100000	H-200	Cash	USD			-3,005,125	10/17/2012	1	-3,005,125	0	10/17/2012	10/17/2012	Cash expense from opening of 84604KAD1",
				"-12	-10	100000	H-200	Position	84604KAD1	103.625	-760,856.3	-788,437.34	10/17/2012	1	-788,437.34	-788,437.34	10/17/2012	10/17/2012	Reducing Original Face to Current Face for 84604KAD1",
				"-13	-12	100000	H-200	Cash	USD			788,437.34	10/17/2012	1	788,437.34	0	10/17/2012	10/17/2012	Cash proceeds from close of 84604KAD1"
		);
	}


	@Test
	public void testBondTrade_SellWithFactor2() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newBond("84604KAD1").build())
				.withExpectedUnitPrice("103.625")
				.withCurrentFactor("0.7376357612")
				.addFillWithQuantity("2139143.70")
				.createFills();
		trade.getInvestmentSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		// must set the original face after the fill is created to avoid notional from being correctly calculated - results in only two journal details
		trade.setOriginalFace(new BigDecimal("2900000.00"));

		evaluateBookingRuleForFillWithResults(trade.getTradeFillList().get(0),
				"-10	null	100000	H-200	Position	84604KAD1	103.625	-2,900,000	-3,005,125	10/17/2012	1	-3,005,125	-3,005,125	10/17/2012	10/17/2012	Position opening at Original Face for 84604KAD1",
				"-11	-10	100000	H-200	Cash	USD			3,005,125	10/17/2012	1	3,005,125	0	10/17/2012	10/17/2012	Cash proceeds from opening of 84604KAD1",
				"-12	-10	100000	H-200	Position	84604KAD1	103.625	760,856.3	788,437.34	10/17/2012	1	788,437.34	788,437.34	10/17/2012	10/17/2012	Reducing Original Face to Current Face for 84604KAD1",
				"-13	-12	100000	H-200	Cash	USD			-788,437.34	10/17/2012	1	-788,437.34	0	10/17/2012	10/17/2012	Cash expense from close of 84604KAD1"
		);
	}


	@Test
	public void testCDS_Swap_Trade_SellProtectionUnderPar() {
		Trade trade = TradeBuilder.newTradeForSecurity(
				InvestmentSecurityBuilder.ofSubType(1, "10386587", InvestmentTypeSubType.CREDIT_DEFAULT_SWAPS)
						.withPriceMultiplier(new BigDecimal("0.01"))
						.withNotionalCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP)
						.build())
				.withExpectedUnitPrice("98.43382957")
				.withAmountsSignAccountsForBuy(true)
				.withAccrualAmount1("-6666.67")
				.addFillWithQuantity("30000000.00")
				.createFills();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	10386587	98.43382957	-30,000,000	-469,851.13	10/17/2012	1	-469,851.13	-469,851.13	10/17/2012	10/17/2012	null",
						"-11	-10	100000	H-200	Cash	USD			469,851.13	10/17/2012	1	469,851.13	0	10/17/2012	10/17/2012	Cash proceeds from opening of 10386587",
						"-12	-10	100000	H-200	Premium Leg	10386587			6,666.67	10/17/2012	1	6,666.67	0	10/17/2012	10/17/2012	Accrued Premium Leg for 10386587",
						"-13	-10	100000	H-200	Cash	USD			-6,666.67	10/17/2012	1	-6,666.67	0	10/17/2012	10/17/2012	Cash expense from opening of 10386587"
				).execute();
	}


	@Test
	public void testCDS_Swap_Trade_SellProtectionOverPar() {
		Trade trade = TradeBuilder.newTradeForSecurity(
				InvestmentSecurityBuilder.ofSubType(1, "SDBB4QT333333FGWCC.0.1.0", InvestmentTypeSubType.CREDIT_DEFAULT_SWAPS)
						.withPriceMultiplier(new BigDecimal("0.01"))
						.withNotionalCalculator(InvestmentNotionalCalculatorTypes.BPS_DISCOUNT_MONEY_HALF_UP)
						.build())
				.withExpectedUnitPrice("100.32755206")
				.withAmountsSignAccountsForBuy(true)
				.withAccrualAmount1("-24000.00")
				.addFillWithQuantity("54000000.00")
				.createFills();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	SDBB4QT333333FGWCC.0.1.0	100.32755206	-54,000,000	176,878.11	10/17/2012	1	176,878.11	176,878.11	10/17/2012	10/17/2012	null",
						"-11	-10	100000	H-200	Cash	USD			-176,878.11	10/17/2012	1	-176,878.11	0	10/17/2012	10/17/2012	Cash expense from opening of SDBB4QT333333FGWCC.0.1.0",
						"-12	-10	100000	H-200	Premium Leg	SDBB4QT333333FGWCC.0.1.0			24,000	10/17/2012	1	24,000	0	10/17/2012	10/17/2012	Accrued Premium Leg for SDBB4QT333333FGWCC.0.1.0",
						"-13	-10	100000	H-200	Cash	USD			-24,000	10/17/2012	1	-24,000	0	10/17/2012	10/17/2012	Cash expense from opening of SDBB4QT333333FGWCC.0.1.0"
				).execute();
	}


	@Test
	public void testCloseStockSpecificLot() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newStock("IBM").build())
				.sell()
				.withExpectedUnitPrice("153.37")
				.addFillWithQuantity(3000)
				.createFills();

		TradeFill fill = trade.getTradeFillList().get(0);
		fill.setOpeningDate(DateUtils.toDate("01/02/2018"));
		fill.setOpeningPrice(new BigDecimal("156.06"));

		AccountingBookingRuleTestExecutor.newTestForEntity(fill)
				.forBookingRules(createLotsAndServicesForStockClosingTest(fill))
				.withExpectedResults(
						"-12	11696	100000	H-200	Position	IBM	153.37	-1,000	-153,370	10/17/2012	1	-153,370	-153,370	01/02/2018	10/17/2012	Full position close for IBM",
						"-13	11697	100000	H-200	Position	IBM	153.37	-1,200	-184,044	10/17/2012	1	-184,044	-184,044	01/02/2018	10/17/2012	Full position close for IBM",
						"-14	11698	100000	H-200	Position	IBM	153.37	-800	-122,696	10/17/2012	1	-122,696	-122,696	01/02/2018	10/17/2012	Partial position close for IBM",
						"-15	-12	100000	H-200	Cash	USD			153,370	10/17/2012	1	153,370	0	01/02/2018	10/17/2012	Cash proceeds from close of IBM",
						"-16	-13	100000	H-200	Cash	USD			184,044	10/17/2012	1	184,044	0	01/02/2018	10/17/2012	Cash proceeds from close of IBM",
						"-17	-14	100000	H-200	Cash	USD			122,696	10/17/2012	1	122,696	0	01/02/2018	10/17/2012	Cash proceeds from close of IBM"
				)
				.execute();
	}


	@Test
	public void testBondTrade_SpecificLotClosingWithFactor2() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newBond("84604KAD1").build())
				.withExpectedUnitPrice("103.625")
				.withCurrentFactor("0.7376357612")
				.addFillWithQuantity("2139143.70")
				.createFills();
		trade.getInvestmentSecurity().getInstrument().getHierarchy().setFactorChangeEventType(new InvestmentSecurityEventType());
		// must set the original face after the fill is created to avoid notional from being correctly calculated - results in only two journal details
		trade.setOriginalFace(new BigDecimal("2900000.00"));

		TradeFill fill = trade.getTradeFillList().get(0);
		fill.setOpeningDate(DateUtils.toDate("02/01/2012"));
		fill.setOpeningPrice(new BigDecimal("103.625"));

		AccountingBookingRuleTestExecutor.newTestForEntity(fill)
				.forBookingRules(createLotsAndServicesForFactorClosingTest(fill))
				.withExpectedResults(
						"-15	11653957	100000	H-200	Position	84604KAD1	103.625	-2,139,143.7	-2,216,687.66	10/17/2012	1	-2,216,687.66	-2,216,687.66	02/01/2012	10/17/2012	Partial position close  at Original Face for 84604KAD1",
						"-16	11653957	100000	H-200	Position	84604KAD1	103.625	-760,856.3	-788,437.34	10/17/2012	1	-788,437.34	-788,437.34	02/01/2012	10/17/2012	Full position close for 84604KAD1",
						"-17	11653957	100000	H-200	Position	84604KAD1	103.625	1,031,479.69	1,068,870.82	10/17/2012	1	1,068,870.82	1,068,870.82	02/01/2012	10/17/2012	Position opening at new Original Face for 84604KAD1",
						"-18	-17	100000	H-200	Position	84604KAD1	103.625	-270,623.39	-280,433.48	10/17/2012	1	-280,433.48	-280,433.48	02/01/2012	10/17/2012	Reducing Original Face to Current Face for 84604KAD1",
						"-19	-15	100000	H-200	Cash	USD			2,216,687.66	10/17/2012	1	2,216,687.66	0	02/01/2012	10/17/2012	Cash proceeds from close of 84604KAD1",
						"-20	-16	100000	H-200	Cash	USD			788,437.34	10/17/2012	1	788,437.34	0	02/01/2012	10/17/2012	Cash proceeds from close of 84604KAD1",
						"-21	-17	100000	H-200	Cash	USD			-1,068,870.83	10/17/2012	1	-1,068,870.83	0	02/01/2012	10/17/2012	Cash expense from opening of 84604KAD1",
						"-22	-18	100000	H-200	Cash	USD			280,433.49	10/17/2012	1	280,433.49	0	02/01/2012	10/17/2012	Cash proceeds from close of 84604KAD1"

				)
				.execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<AccountingBookingRule<TradeFill>> createLotsAndServicesForStockClosingTest(TradeFill fill) {
		List<AccountingTransaction> transactionList = new ArrayList<>();
		transactionList.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11695L, fill.getTrade().getInvestmentSecurity()).qty(5000).price("150.06").on("12/31/2017").build());
		transactionList.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, fill.getTrade().getInvestmentSecurity()).qty(1000).price("156.06").on("01/02/2018").build());
		transactionList.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11697L, fill.getTrade().getInvestmentSecurity()).qty(1200).price("156.06").on("01/02/2018").build());
		transactionList.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11698L, fill.getTrade().getInvestmentSecurity()).qty(1500).price("156.06").on("01/02/2018").build());

		return createLotsAndServices(fill, transactionList);
	}


	private List<AccountingBookingRule<TradeFill>> createLotsAndServicesForFactorClosingTest(TradeFill fill) {
		List<AccountingTransaction> transactionList = new ArrayList<>();
		transactionList.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11653959L, fill.getTrade().getInvestmentSecurity()).qty(3000000).price("103.825").on("02/01/2012").build());
		transactionList.add(AccountingTransactionBuilder.newTransactionForIdAndSecurity(11653957L, fill.getTrade().getInvestmentSecurity()).qty(2900000).price("103.625").on("02/01/2012").build());

		return createLotsAndServices(fill, transactionList);
	}


	private List<AccountingBookingRule<TradeFill>> createLotsAndServices(TradeFill fill, List<AccountingTransaction> transactionList) {
		Trade trade = fill.getTrade();

		TradeCreatePositionBookingRule rule = TradeTestObjectFactory.newTradeCreatePositionBookingRule();
		List<AccountingPosition> existingPositions = new ArrayList<>();
		transactionList.forEach(transaction -> {
			transaction.setClientInvestmentAccount(trade.getClientInvestmentAccount());
			transaction.setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
			existingPositions.add(AccountingPosition.forOpeningTransaction(transaction));
			Mockito.when(rule.getAccountingTransactionService().getAccountingTransaction(ArgumentMatchers.eq(transaction.getId()))).thenReturn(transaction);
		});


		Mockito.when(rule.getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forClientAccount(trade.getClientInvestmentAccount().getId())
						.forHoldingAccount(trade.getHoldingInvestmentAccount().getId())
						.forInvestmentSecurity(trade.getInvestmentSecurity().getId())
		))).thenReturn(existingPositions);


		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setHoldingInvestmentAccountId(trade.getHoldingInvestmentAccount().getId());
		Mockito.when(rule.getAccountingTransactionService().getAccountingTransactionList(AccountingTestObjectFactory.newAccountingTransactionSearchFormMatcher(searchForm))).then(invocationOnMock -> {
			Integer holdingInvestmentAccountId = ((AccountingTransactionSearchForm) invocationOnMock.getArgument(0)).getHoldingInvestmentAccountId();
			if (holdingInvestmentAccountId != null) {
				return CollectionUtils.getFiltered(transactionList, transaction -> transaction.getHoldingInvestmentAccount().getId().equals(holdingInvestmentAccountId));
			}
			return transactionList;
		});

		AccountingTestObjectFactory.newAccountingBookingPositionRetriever(rule);

		AccountingPositionSplitterBookingRule<TradeFill> splitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRule();
		splitterRule.setAccountingTransactionService(rule.getAccountingTransactionService());
		splitterRule.setAccountingPositionService(rule.getAccountingPositionService());
		splitterRule.setAccountingBookingPositionRetriever(rule.getAccountingBookingPositionRetriever());

		return CollectionUtils.createList(rule, splitterRule);
	}


	private void evaluateBookingRuleForFillWithResults(TradeFill fill, String... expectedResults) {
		AccountingBookingRuleTestExecutor.newTestForEntity(fill)
				.forBookingRule(TradeTestObjectFactory.newTradeCreatePositionBookingRule())
				.withExpectedResults(expectedResults)
				.execute();
	}
}
