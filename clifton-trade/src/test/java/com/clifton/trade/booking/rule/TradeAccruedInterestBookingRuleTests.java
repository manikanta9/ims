package com.clifton.trade.booking.rule;


import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class TradeAccruedInterestBookingRuleTests {


	////////////////////////////////////////////////////////////////////////////
	///////////                    CDS                               ///////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCDS_Swap_Trade_BuyProtectionOverPar() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCDS("6F2G8").build())
				.sell()
				.withAccrualAmount1("-333333.33")
				.onTradeDate("10/17/2012")
				.addFill("150000000", "100.29914951")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	6F2G8	100.29914951	-150,000,000	448,724.27	10/17/2012	1	448,724.27	448,724.27	10/17/2012	10/17/2012	null",
						"-11	-10	100000	H-200	Cash	USD			-448,724.27	10/17/2012	1	-448,724.27	0	10/17/2012	10/17/2012	Cash expense from opening of 6F2G8",
						"-12	-10	100000	H-200	Premium Leg	6F2G8			333,333.33	10/17/2012	1	333,333.33	0	10/17/2012	10/17/2012	Accrued Premium Leg for 6F2G8",
						"-13	-10	100000	H-200	Cash	USD			-333,333.33	10/17/2012	1	-333,333.33	0	10/17/2012	10/17/2012	Cash expense from opening of 6F2G8"
				).execute();
	}


	@Test
	public void testCDS_Swap_SecondPartialClose() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCDS("810RI20902595").build())
				.buy()
				.withOriginalFace("1000000.00")
				.withQuantityIntended("940000.0000000000")
				.withCurrentFactor("0.94")
				.withAccrualAmount1("4569.44")
				.onTradeDate("07/24/2017")
				.withSettlementDate("07/27/2017")
				.addFill("940000.00", "101")
				.withAccountingNotional("9400.00")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						getSplitterRule(getTransactionList(trade)),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule()
				)
				.withExpectedResults(
						"-15	3583624	100000	H-200	Position	810RI20902595	101	940,000	-9,400	07/24/2017	1	-9,400	-9,400	01/10/2013	07/27/2017	Partial position close  at Original Notional for 810RI20902595",
						"-16	3583624	100000	H-200	Position	810RI20902595	102.5	18,988,000	-474,700	07/24/2017	1	-474,700	-474,700	01/10/2013	07/27/2017	Full position close  at new Original Notional for 810RI20902595",
						"-17	2846031	100000	H-200	Position	810RI20902595	102.5	-20,200,000	505,000	07/24/2017	1	505,000	505,000	01/10/2013	07/27/2017	Position opening at new Original Notional for 810RI20902595",
						"-18	-17	100000	H-200	Position	810RI20902595	102.5	1,212,000	-30,300	07/24/2017	1	-30,300	-30,300	01/10/2013	07/27/2017	Reducing Original Notional to Current Notional for 810RI20902595",
						"-19	-15	100000	H-200	Cash	USD			9,400	07/24/2017	1	9,400	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595",
						"-20	-16	100000	H-200	Cash	USD			189,880	07/24/2017	1	189,880	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595",
						"-21	-17	100000	H-200	Cash	USD			-202,000	07/24/2017	1	-202,000	0	01/10/2013	07/27/2017	Cash expense from opening of 810RI20902595",
						"-22	-18	100000	H-200	Cash	USD			12,120	07/24/2017	1	12,120	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595",
						"-23	-15	100000	H-200	Premium Leg	810RI20902595			-4,569.44	07/24/2017	1	-4,569.44	0	01/10/2013	07/27/2017	Accrued Premium Leg for 810RI20902595",
						"-24	-15	100000	H-200	Cash	USD			4,569.44	07/24/2017	1	4,569.44	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595"


				).execute();
	}


	@Test
	public void testCDS_Swap_SecondPartialCloseAtSamePrice() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newCDS("810RI20902595").build())
				.buy()
				.withOriginalFace("1000000.00")
				.withQuantityIntended("940000.0000000000")
				.withCurrentFactor("0.94")
				.withAccrualAmount1("4569.44")
				.onTradeDate("07/24/2017")
				.withSettlementDate("07/27/2017")
				.addFill("940000.00", "102.5")
				.withAccountingNotional("9400.00")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						getSplitterRule(getTransactionList(trade)),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule()
				)
				.withExpectedResults(
						"-15	3583624	100000	H-200	Position	810RI20902595	102.5	940,000	-23,500	07/24/2017	1	-23,500	-23,500	01/10/2013	07/27/2017	Partial position close  at Original Notional for 810RI20902595",
						"-16	3583624	100000	H-200	Position	810RI20902595	102.5	18,988,000	-474,700	07/24/2017	1	-474,700	-474,700	01/10/2013	07/27/2017	Full position close  at new Original Notional for 810RI20902595",
						"-17	2846031	100000	H-200	Position	810RI20902595	102.5	-20,200,000	505,000	07/24/2017	1	505,000	505,000	01/10/2013	07/27/2017	Position opening at new Original Notional for 810RI20902595",
						"-18	-17	100000	H-200	Position	810RI20902595	102.5	1,212,000	-30,300	07/24/2017	1	-30,300	-30,300	01/10/2013	07/27/2017	Reducing Original Notional to Current Notional for 810RI20902595",
						"-19	-15	100000	H-200	Cash	USD			23,500	07/24/2017	1	23,500	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595",
						"-20	-16	100000	H-200	Cash	USD			474,700	07/24/2017	1	474,700	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595",
						"-21	-17	100000	H-200	Cash	USD			-505,000	07/24/2017	1	-505,000	0	01/10/2013	07/27/2017	Cash expense from opening of 810RI20902595",
						"-22	-18	100000	H-200	Cash	USD			30,300	07/24/2017	1	30,300	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595",
						"-23	-15	100000	H-200	Premium Leg	810RI20902595			-4,569.44	07/24/2017	1	-4,569.44	0	01/10/2013	07/27/2017	Accrued Premium Leg for 810RI20902595",
						"-24	-15	100000	H-200	Cash	USD			4,569.44	07/24/2017	1	4,569.44	0	01/10/2013	07/27/2017	Cash proceeds from close of 810RI20902595"

				).execute();
	}


	@Test
	public void testCDS_Swap_FullCloseAtSamePrice() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newBond("3136A42Y4").build())
				.sell()
				.withOriginalFace("2800000.00")
				.withQuantityIntended("1409246.9400000000")
				.withCurrentFactor("0.5033024800")
				.withAccrualAmount1("1282.32")
				.onTradeDate("11/10/2017")
				.withSettlementDate("11/15/2017")
				.addFill("1409246.9400000000", "100.25")
				.withAccountingNotional("1412770.06")
				.build();

		trade.getInvestmentSecurity().getInstrument().getHierarchy().setFactorChangeEventType(InvestmentSecurityEventType.ofTypeName(InvestmentSecurityEventType.FACTOR_CHANGE));

		List<AccountingTransaction> transactionList = getTransactionListForFullClose(trade);
		List<AccountingPosition> existingPositionList = getPositionListForFullClose(transactionList);

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						getSplitterRule(transactionList, existingPositionList),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule()
				)
				.withExpectedResults(
						"-14	-15	100000	H-200	Cash	USD			1,412,770.06	11/10/2017	1	1,412,770.06	0	11/05/2015	11/15/2017	Cash proceeds from close of 3136A42Y4",
						"-15	4905676	100000	H-200	Position	3136A42Y4	100.25	-1,409,246.94	-1,412,770.06	11/10/2017	1	-1,412,770.06	-1,412,770.06	11/05/2015	11/15/2017	Full position close  at Original Face for 3136A42Y4",
						"-16	-15	100000	H-200	Interest Income	3136A42Y4			-1,282.32	11/10/2017	1	-1,282.32	0	11/05/2015	11/15/2017	Accrued Interest Income for 3136A42Y4",
						"-17	-15	100000	H-200	Cash	USD			1,282.32	11/10/2017	1	1,282.32	0	11/05/2015	11/15/2017	Cash proceeds from close of 3136A42Y4"
				).execute();
	}


	private AccountingPositionSplitterBookingRule<TradeFill> getSplitterRule(List<AccountingTransaction> transactionList) {
		List<AccountingPosition> existingPositions = new ArrayList<>();
		AccountingPosition existingPosition = AccountingTestObjectFactory.newAccountingPosition(3583624L, transactionList.get(0).getInvestmentSecurity()
				, new BigDecimal("-19928000"), new BigDecimal("102.5"), new BigDecimal("498200.00"), BigDecimal.ONE,
				DateUtils.toDate("03/04/2013"));

		existingPosition.getOpeningTransaction().setOriginalTransactionDate(DateUtils.toDate("01/10/2013"));
		existingPosition.getOpeningTransaction().setQuantity(new BigDecimal("-21200000"));

		existingPositions.add(existingPosition);

		return getSplitterRule(transactionList, existingPositions);
	}


	private AccountingPositionSplitterBookingRule<TradeFill> getSplitterRule(List<AccountingTransaction> transactionList, List<AccountingPosition> existingPositions) {
		AccountingPositionSplitterBookingRule<TradeFill> splitterRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(transactionList.toArray(new AccountingTransaction[0]));

		Mockito.when(splitterRule.getAccountingPositionService().getAccountingPositionListUsingCommand(AccountingTestObjectFactory.newAccountingPositionCommandMatcher(
				new AccountingPositionCommand().forInvestmentSecurity(existingPositions.get(0).getInvestmentSecurity().getId())
		))).thenReturn(existingPositions);

		return splitterRule;
	}


	private List<AccountingTransaction> getTransactionList(Trade trade) {
		AccountingTransaction secondOpening = AccountingTransactionBuilder.newTransactionForIdAndSecurity(3583624L, trade.getInvestmentSecurity()).qty(-21200000).price("102.5").on("03/04/2013").build();
		secondOpening.getJournal().setId(300L);
		secondOpening.setOpening(true);
		secondOpening.setOriginalTransactionDate(DateUtils.toDate("01/10/2013"));
		secondOpening.setDescription("Position opening at new Original Notional for 810RI20902595");

		AccountingTransaction firstOpening = AccountingTransactionBuilder.newTransactionForIdAndSecurity(2846031L, trade.getInvestmentSecurity()).qty(-31800000).price("102.5").on("01/10/2013").build();
		firstOpening.getJournal().setId(100L);
		firstOpening.setOpening(true);
		secondOpening.setParentTransaction(firstOpening);

		List<AccountingTransaction> transactionList = new ArrayList<>();
		transactionList.add(secondOpening);
		transactionList.add(firstOpening);
		return transactionList;
	}


	private List<AccountingTransaction> getTransactionListForFullClose(Trade trade) {
		AccountingTransaction firstOpening = AccountingTransactionBuilder.newTransactionForIdAndSecurity(4905676L, trade.getInvestmentSecurity()).qty(2800000).price("100.25").on("11/05/2015").build();
		firstOpening.getJournal().setId(100L);
		firstOpening.setOpening(true);

		List<AccountingTransaction> transactionList = new ArrayList<>();
		transactionList.add(firstOpening);
		return transactionList;
	}


	private List<AccountingPosition> getPositionListForFullClose(List<AccountingTransaction> transactionList) {
		List<AccountingPosition> existingPositions = new ArrayList<>();
		AccountingPosition existingPosition = AccountingTestObjectFactory.newAccountingPosition(transactionList.get(0));
		existingPosition.setRemainingQuantity(new BigDecimal("1409246.94"));
		existingPosition.setRemainingBaseDebitCredit(new BigDecimal("1412770.05"));
		existingPosition.setRemainingCostBasis(new BigDecimal("1412770.05"));

		existingPositions.add(existingPosition);

		return existingPositions;
	}

	////////////////////////////////////////////////////////////////////////////
	///////////                    TRS                               ///////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTRS_Swap_Trade_Buy_PositiveAccrual() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newTRS("SDB4179917243").build())
				.buy()
				.withAccrualAmount1("2325.79")
				.onTradeDate("10/17/2012")
				.addFill("4289.29", "5936.132")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	SDB4179917243	5,936.132	4,289.29	25,461,791.63	10/17/2012	1	25,461,791.63	25,461,791.63	10/17/2012	10/17/2012	null",
						"-12	-10	100000	H-200	Interest Leg	SDB4179917243			-2,325.79	10/17/2012	1	-2,325.79	0	10/17/2012	10/17/2012	Accrued Interest Leg for SDB4179917243",
						"-13	-10	100000	H-200	Cash	USD			-25,459,465.84	10/17/2012	1	-25,459,465.84	0	10/17/2012	10/17/2012	Cash expense from opening of SDB4179917243"
				).execute();
	}


	@Test
	public void testTRS_Swap_Trade_Sell_PositiveAccrual() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newTRS("SDB4179917243").build())
				.sell()
				.withAccrualAmount1("2325.79")
				.onTradeDate("10/17/2012")
				.addFill("4289.29", "5936.132")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	SDB4179917243	5,936.132	-4,289.29	-25,461,791.63	10/17/2012	1	-25,461,791.63	-25,461,791.63	10/17/2012	10/17/2012	null",
						"-12	-10	100000	H-200	Interest Leg	SDB4179917243			-2,325.79	10/17/2012	1	-2,325.79	0	10/17/2012	10/17/2012	Accrued Interest Leg for SDB4179917243",
						"-13	-10	100000	H-200	Cash	USD			25,464,117.42	10/17/2012	1	25,464,117.42	0	10/17/2012	10/17/2012	Cash proceeds from opening of SDB4179917243"
				).execute();
	}


	@Test
	public void testTRS_Swap_Trade_Buy_NegativeAccrual() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newTRS("SDB4179917243").build())
				.buy()
				.withAccrualAmount1("-2325.79")
				.onTradeDate("10/17/2012")
				.addFill("4289.29", "5936.132")
				.build();


		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	SDB4179917243	5,936.132	4,289.29	25,461,791.63	10/17/2012	1	25,461,791.63	25,461,791.63	10/17/2012	10/17/2012	null",
						"-12	-10	100000	H-200	Interest Leg	SDB4179917243			2,325.79	10/17/2012	1	2,325.79	0	10/17/2012	10/17/2012	Accrued Interest Leg for SDB4179917243",
						"-13	-10	100000	H-200	Cash	USD			-25,464,117.42	10/17/2012	1	-25,464,117.42	0	10/17/2012	10/17/2012	Cash expense from opening of SDB4179917243"
				).execute();
	}


	@Test
	public void testTRS_Swap_Trade_Sell_NegativeAccrual() {
		Trade trade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newTRS("SDB4179917243").build())
				.sell()
				.withAccrualAmount1("-2325.79")
				.onTradeDate("10/17/2012")
				.addFill("4289.29", "5936.132")
				.build();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						AccountingTestObjectFactory.newAccountingAggregateCurrencyBookingRule()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	SDB4179917243	5,936.132	-4,289.29	-25,461,791.63	10/17/2012	1	-25,461,791.63	-25,461,791.63	10/17/2012	10/17/2012	null",
						"-12	-10	100000	H-200	Interest Leg	SDB4179917243			2,325.79	10/17/2012	1	2,325.79	0	10/17/2012	10/17/2012	Accrued Interest Leg for SDB4179917243",
						"-13	-10	100000	H-200	Cash	USD			25,459,465.84	10/17/2012	1	25,459,465.84	0	10/17/2012	10/17/2012	Cash proceeds from opening of SDB4179917243"
				).execute();
	}
}
