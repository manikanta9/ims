package com.clifton.trade.instruction;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.account.search.AccountingAccountSearchForm;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.StringUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.InstructionUtilHandler;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.TradeService;
import com.clifton.core.util.MathUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


@TradeInMemoryDatabaseContext
public class TradeInstructionUtilHandlerTest<I extends Instruction> extends BaseInMemoryDatabaseTests {

	@Resource
	private TradeInstructionUtilHandler tradeInstructionUtilHandler;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InstructionUtilHandler<I, InstructionDeliveryFieldCommand> instructionUtilHandler;

	@Resource
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentInstructionService investmentInstructionService;

	@Resource
	private AccountingTransactionService accountingTransactionService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private AccountingAccountService accountingAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void getISITCCodeTests() {
		List<String> investmentTypeSubTypeList = Arrays.asList("Corporate Bonds", "CMO", "Government Bonds", "Central Government Bonds");
		List<String> investmentTypeSubType2List = Arrays.asList("Money Market Instruments", "Commerical Paper", "Municipal Government Bonds", "Bills", "Inflation LInked", "Notes and Bonds", "STRIPS");

		InstrumentSearchForm searchForm = new InstrumentSearchForm();
		searchForm.setInvestmentType(InvestmentType.BONDS);
		List<InvestmentInstrument> investmentInstrumentList = this.investmentInstrumentService.getInvestmentInstrumentList(searchForm).stream()
				.filter(i -> Objects.equals(InvestmentType.BONDS, i.getHierarchy().getInvestmentType().getName()))
				.filter(i -> investmentTypeSubTypeList.contains(i.getHierarchy().getInvestmentTypeSubType().getName()))
				.filter(i -> investmentTypeSubType2List.contains(i.getHierarchy().getInvestmentTypeSubType2().getName()))
				.collect(Collectors.toList());
		Set<ISITCCodes> codeList = new HashSet<>();
		for (InvestmentInstrument investmentInstrument : investmentInstrumentList) {
			ISITCCodes isitcCodes = this.investmentSecurityUtilHandler.getISITCCodeStrict(investmentInstrument);
			codeList.add(isitcCodes);
		}
		codeList.removeAll(Arrays.asList(ISITCCodes.CORP, ISITCCodes.CD, ISITCCodes.BA, ISITCCodes.CP, ISITCCodes.CMO, ISITCCodes.MUNI, ISITCCodes.USTB, ISITCCodes.TIPS, ISITCCodes.TN, ISITCCodes.STRP));
		Assertions.assertTrue(codeList.isEmpty());
	}


	@Test
	public void getStrikePrice() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(79015);
		Assertions.assertNotNull(security);
		BigDecimal strikePrice = security.getOptionStrikePrice();
		Assertions.assertTrue(MathUtils.isEqual(strikePrice, new BigDecimal("160.5")));
	}


	@Test
	public void getOptionType() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(79015);
		Assertions.assertNotNull(security);
		InvestmentSecurityOptionTypes optionType = security.getOptionType();
		Assertions.assertEquals(InvestmentSecurityOptionTypes.PUT, optionType);
	}


	@Test
	public void getOptionStyle() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(79015);
		Assertions.assertNotNull(security);
		OptionStyleTypes optionStyle = security.getOptionStyle();
		MatcherAssert.assertThat(optionStyle, IsEqual.equalTo(OptionStyleTypes.AMERICAN));
	}


	@Test
	public void getPlaceOfSettlementStrict() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurity(40582);
		String placeOfSettlement = this.investmentSecurityUtilHandler.getPlaceOfSettlementStrict(investmentSecurity);
		MatcherAssert.assertThat(placeOfSettlement, IsEqual.equalTo("FRNYUS33"));
	}


	@Test
	public void getParametricBusinessIdentifierCodeTests() {
		MatcherAssert.assertThat("PPSCUS66", IsEqual.equalTo(this.tradeInstructionUtilHandler.getParametricBusinessIdentifierCode()));
	}


	@Test
	public void getTransactionReferenceNumberTests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<InvestmentInstructionItem> itemList = this.investmentInstructionService.getInvestmentInstructionItemListForEntityObject(trade);
		Assertions.assertEquals(1, itemList.size());
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		SystemTable table = this.systemSchemaService.getSystemTableByName(TradeFill.class.getSimpleName());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(fillList.get(0).getId());
		searchForm.setTableId(table.getId());
		List<AccountingTransaction> transactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		String referenceNumber = this.instructionUtilHandler.getTransactionReferenceNumber(Arrays.asList(itemList.get(0), trade, fillList.get(0), transactionList.get(0)));
		MatcherAssert.assertThat(referenceNumber, IsEqual.equalTo("IMS" + itemList.get(0).getId() + "II;" + "IMS" + trade.getId() + "T;" + "IMS" + fillList.get(0).getId() + "TF;" + "IMS" + transactionList.get(0).getId() + "AT"));
	}


	@Test
	public void getAccountingTransactionPositionList767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		SystemTable table = this.systemSchemaService.getSystemTableByName(TradeFill.class.getSimpleName());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(tradeFill.getId());
		searchForm.setTableId(table.getId());
		List<AccountingTransaction> transactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm).stream()
				.filter(a -> a.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(transactionList.isEmpty());
		Assertions.assertEquals(1, transactionList.size());
		List<AccountingTransaction> accountingTransactionList = this.tradeInstructionUtilHandler.getAccountingTransactionPositionList(tradeFill);
		MatcherAssert.assertThat(transactionList, IsEqual.equalTo(accountingTransactionList));
	}


	@Test
	public void getAccountingTransactionPositionList765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		SystemTable table = this.systemSchemaService.getSystemTableByName(TradeFill.class.getSimpleName());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(tradeFill.getId());
		searchForm.setTableId(table.getId());
		List<AccountingTransaction> transactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm).stream()
				.filter(a -> a.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(transactionList.isEmpty());
		Assertions.assertEquals(1, transactionList.size());
		List<AccountingTransaction> accountingTransactionList = this.tradeInstructionUtilHandler.getAccountingTransactionPositionList(tradeFill);
		MatcherAssert.assertThat(transactionList, IsEqual.equalTo(accountingTransactionList));
	}


	@Test
	public void getAccountingTransactionPositionList766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		SystemTable table = this.systemSchemaService.getSystemTableByName(TradeFill.class.getSimpleName());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(tradeFill.getId());
		searchForm.setTableId(table.getId());
		List<AccountingTransaction> transactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm).stream()
				.filter(a -> a.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(transactionList.isEmpty());
		Assertions.assertEquals(4, transactionList.size());
		List<AccountingTransaction> accountingTransactionList = this.tradeInstructionUtilHandler.getAccountingTransactionPositionList(tradeFill);
		MatcherAssert.assertThat(transactionList, IsEqual.equalTo(accountingTransactionList));
	}


	@Test
	public void getCustodianAccount767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		InvestmentAccount investmentAccount = trade.getClientInvestmentAccount();
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setMainAccountId(investmentAccount.getId());
		List<InvestmentAccount> accountList = this.investmentAccountService.getInvestmentAccountList(searchForm).stream()
				.filter(a -> StringUtils.isEqual(a.getType().getName(), InvestmentAccountType.CUSTODIAN))
				.collect(Collectors.toList());
		Assertions.assertFalse(accountList.isEmpty());
		Assertions.assertEquals(1, accountList.size());
		MatcherAssert.assertThat(this.tradeInstructionUtilHandler.getCustodianAccount(trade), IsEqual.equalTo(accountList.get(0)));
	}


	@Test
	public void getCustodianAccount765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		InvestmentAccount investmentAccount = trade.getClientInvestmentAccount();
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setMainAccountId(investmentAccount.getId());
		searchForm.setMainPurpose(InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME);
		searchForm.setMainPurposeActiveOnDate(trade.getTradeDate());
		List<InvestmentAccount> accountList = this.investmentAccountService.getInvestmentAccountList(searchForm);
		Assertions.assertFalse(accountList.isEmpty());
		Assertions.assertEquals(1, accountList.size());
		MatcherAssert.assertThat(this.tradeInstructionUtilHandler.getCustodianAccount(trade), IsEqual.equalTo(accountList.get(0)));
	}


	@Test
	public void getCustodianAccount766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		InvestmentAccount investmentAccount = trade.getClientInvestmentAccount();
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setMainAccountId(investmentAccount.getId());
		List<InvestmentAccount> accountList = this.investmentAccountService.getInvestmentAccountList(searchForm).stream()
				.filter(a -> StringUtils.isEqual(a.getType().getName(), InvestmentAccountType.CUSTODIAN))
				.collect(Collectors.toList());
		Assertions.assertFalse(accountList.isEmpty());
		Assertions.assertEquals(1, accountList.size());
		MatcherAssert.assertThat(this.tradeInstructionUtilHandler.getCustodianAccount(trade), IsEqual.equalTo(accountList.get(0)));
	}


	@Test
	public void getCommissionAndFeeAmountTradeFill767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		BigDecimal commissionsAndFees = this.tradeInstructionUtilHandler.getCommissionAndFeeAmount(fillList.get(0));
		MatcherAssert.assertThat(new BigDecimal("13.05"), IsEqual.equalTo(commissionsAndFees));
	}


	@Test
	public void getCommissionAndFeeAmountTradeFill765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		BigDecimal commissionsAndFees = this.tradeInstructionUtilHandler.getCommissionAndFeeAmount(fillList.get(0));
		MatcherAssert.assertThat(new BigDecimal("15.18"), IsEqual.equalTo(commissionsAndFees));
	}


	@Test
	public void getCommissionAndFeeAmountTradeFill766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		BigDecimal commissionsAndFees = this.tradeInstructionUtilHandler.getCommissionAndFeeAmount(fillList.get(0));
		MatcherAssert.assertThat(BigDecimal.ZERO, IsEqual.equalTo(commissionsAndFees));
	}


	@Test
	public void getCommissionAndFeeAmountAccountingTransaction767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(fillList.get(0).getId());
		List<AccountingTransaction> accountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		accountingTransactionList = accountingTransactionList.stream()
				.filter(t -> t.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(accountingTransactionList.isEmpty());
		Assertions.assertEquals(1, accountingTransactionList.size());
		BigDecimal commissionsAndFees = this.tradeInstructionUtilHandler.getCommissionAndFeeAmount(accountingTransactionList.get(0));
		MatcherAssert.assertThat(new BigDecimal("13.05"), IsEqual.equalTo(commissionsAndFees));
	}


	@Test
	public void getCommissionAndFeeAmountAccountingTransaction765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(fillList.get(0).getId());
		List<AccountingTransaction> accountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		accountingTransactionList = accountingTransactionList.stream()
				.filter(t -> t.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(accountingTransactionList.isEmpty());
		Assertions.assertEquals(1, accountingTransactionList.size());
		BigDecimal commissionsAndFees = this.tradeInstructionUtilHandler.getCommissionAndFeeAmount(accountingTransactionList.get(0));
		MatcherAssert.assertThat(new BigDecimal("15.18"), IsEqual.equalTo(commissionsAndFees));
	}


	@Test
	public void getCommissionAndFeeAmountAccountingTransaction766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(fillList.get(0).getId());
		List<AccountingTransaction> accountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		accountingTransactionList = accountingTransactionList.stream()
				.filter(t -> t.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(accountingTransactionList.isEmpty());
		Assertions.assertEquals(4, accountingTransactionList.size());
		BigDecimal commissionsAndFees = accountingTransactionList.stream()
				.map(t -> this.tradeInstructionUtilHandler.getCommissionAndFeeAmount(t))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		MatcherAssert.assertThat(BigDecimal.ZERO, IsEqual.equalTo(commissionsAndFees));
	}


	@Test
	public void getGainLossTradeFill767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal gainLoss = this.tradeInstructionUtilHandler.getGainLoss(tradeFill);
		MatcherAssert.assertThat(BigDecimal.ZERO, IsEqual.equalTo(gainLoss));
	}


	@Test
	public void getGainLossTradeFill765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal gainLoss = this.tradeInstructionUtilHandler.getGainLoss(tradeFill);
		MatcherAssert.assertThat(new BigDecimal("-48284.60"), IsEqual.equalTo(gainLoss));
	}


	@Test
	public void getGainLossTradeFill766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal gainLoss = this.tradeInstructionUtilHandler.getGainLoss(tradeFill);
		MatcherAssert.assertThat(new BigDecimal("-82557.96"), IsEqual.equalTo(gainLoss));
	}


	@Test
	public void getGainLossAccountTransaction767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(fillList.get(0).getId());
		List<AccountingTransaction> accountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		accountingTransactionList = accountingTransactionList.stream()
				.filter(t -> t.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(accountingTransactionList.isEmpty());
		Assertions.assertEquals(1, accountingTransactionList.size());
		BigDecimal gainLoss = this.tradeInstructionUtilHandler.getGainLoss(accountingTransactionList.get(0));
		MatcherAssert.assertThat(BigDecimal.ZERO, IsEqual.equalTo(gainLoss));
	}


	@Test
	public void getGainLossAccountTransaction765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(fillList.get(0).getId());
		List<AccountingTransaction> accountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		accountingTransactionList = accountingTransactionList.stream()
				.filter(t -> t.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(accountingTransactionList.isEmpty());
		Assertions.assertEquals(1, accountingTransactionList.size());
		BigDecimal gainLoss = this.tradeInstructionUtilHandler.getGainLoss(accountingTransactionList.get(0));
		MatcherAssert.assertThat(new BigDecimal("-48284.60"), IsEqual.equalTo(gainLoss));
	}


	@Test
	public void getGainLossAccountTransaction766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(fillList.get(0).getId());
		List<AccountingTransaction> accountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		accountingTransactionList = accountingTransactionList.stream()
				.filter(t -> t.getAccountingAccount().isPosition())
				.collect(Collectors.toList());
		Assertions.assertFalse(accountingTransactionList.isEmpty());
		Assertions.assertEquals(4, accountingTransactionList.size());
		BigDecimal gainLoss = accountingTransactionList.stream()
				.map(t -> this.tradeInstructionUtilHandler.getGainLoss(t))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
		MatcherAssert.assertThat(gainLoss, IsEqual.equalTo(new BigDecimal("-82557.96")));
	}


	@Test
	public void getInterestRate767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		BigDecimal interestRate = this.investmentSecurityUtilHandler.getInterestRate(trade.getInvestmentSecurity());
		MatcherAssert.assertThat(interestRate, IsEqual.equalTo(BigDecimal.ZERO));
	}


	@Test
	public void getInterestRate765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		BigDecimal interestRate = this.investmentSecurityUtilHandler.getInterestRate(trade.getInvestmentSecurity());
		MatcherAssert.assertThat(interestRate, IsEqual.equalTo(BigDecimal.ZERO));
	}


	@Test
	public void getInterestRate766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		BigDecimal interestRate = this.investmentSecurityUtilHandler.getInterestRate(trade.getInvestmentSecurity());
		MatcherAssert.assertThat(interestRate, IsEqual.equalTo(new BigDecimal("1.125")));
	}


	@Test
	public void getRegulatoryAmount767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal regulatoryAmount = this.tradeInstructionUtilHandler.getRegulatoryAmount(tradeFill);
		MatcherAssert.assertThat(regulatoryAmount, IsEqual.equalTo(BigDecimal.ZERO));
	}


	@Test
	public void getRegulatoryAmount765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal regulatoryAmount = this.tradeInstructionUtilHandler.getRegulatoryAmount(tradeFill);
		MatcherAssert.assertThat(regulatoryAmount, IsEqual.equalTo(new BigDecimal("5.38")));
	}


	@Test
	public void getRegulatoryAmount766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal regulatoryAmount = this.tradeInstructionUtilHandler.getRegulatoryAmount(tradeFill);
		MatcherAssert.assertThat(regulatoryAmount, IsEqual.equalTo(BigDecimal.ZERO));
	}


	@Test
	public void getBrokerCommissionAmount767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal brokerCommissionAmount = this.tradeInstructionUtilHandler.getBrokerCommissionAmount(tradeFill);
		MatcherAssert.assertThat(brokerCommissionAmount, IsEqual.equalTo(new BigDecimal("13.05")));
	}


	@Test
	public void getBrokerCommissionAmount765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal brokerCommissionAmount = this.tradeInstructionUtilHandler.getBrokerCommissionAmount(tradeFill);
		MatcherAssert.assertThat(brokerCommissionAmount, IsEqual.equalTo(new BigDecimal("9.80")));
	}


	@Test
	public void getBrokerCommissionAmount766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal brokerCommissionAmount = this.tradeInstructionUtilHandler.getBrokerCommissionAmount(tradeFill);
		MatcherAssert.assertThat(brokerCommissionAmount, IsEqual.equalTo(BigDecimal.ZERO));
	}


	@Test
	public void getAmortizedFaceAmount767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		BigDecimal amortizedFaceAmount = this.tradeInstructionUtilHandler.getAmortizedFaceAmount(trade);
		MatcherAssert.assertThat(amortizedFaceAmount, IsEqual.equalTo(new BigDecimal("62500000.00")));
	}


	@Test
	public void getAmortizedFaceAmount765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		BigDecimal amortizedFaceAmount = this.tradeInstructionUtilHandler.getAmortizedFaceAmount(trade);
		MatcherAssert.assertThat(amortizedFaceAmount, IsEqual.equalTo(new BigDecimal("98000.00")));
	}


	@Test
	public void getAmortizedFaceAmount766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		BigDecimal amortizedFaceAmount = this.tradeInstructionUtilHandler.getAmortizedFaceAmount(trade);
		MatcherAssert.assertThat(amortizedFaceAmount, IsEqual.equalTo(new BigDecimal("10927244.00")));
	}


	@Test
	public void getNetNotionalAmount767191Tests() {
		Trade trade = this.tradeService.getTrade(767191);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal netNotionalAmount = this.tradeInstructionUtilHandler.getNetNotionalAmount(tradeFill);
		MatcherAssert.assertThat(netNotionalAmount, IsEqual.equalTo(new BigDecimal("737531.25")));
	}


	@Test
	public void getNetNotionalAmount765932Tests() {
		Trade trade = this.tradeService.getTrade(765932);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal netNotionalAmount = this.tradeInstructionUtilHandler.getNetNotionalAmount(tradeFill);
		MatcherAssert.assertThat(netNotionalAmount, IsEqual.equalTo(new BigDecimal("232774.02")));
	}


	@Test
	public void getNetNotionalAmount766838Tests() {
		Trade trade = this.tradeService.getTrade(766838);
		List<TradeFill> fillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertFalse(fillList.isEmpty());
		Assertions.assertEquals(1, fillList.size());
		TradeFill tradeFill = fillList.get(0);
		BigDecimal netNotionalAmount = this.tradeInstructionUtilHandler.getNetNotionalAmount(tradeFill);
		MatcherAssert.assertThat(netNotionalAmount, IsEqual.equalTo(new BigDecimal("11270427.76")));
	}


	@Test
	public void getCommissionAndFeeAccounts() {
		Assertions.assertTrue(BeanUtils.isPropertyPresent(AccountingAccount.class, "commission"), "Expected AccountingAccount to have [commission] field.");
		Assertions.assertTrue(BeanUtils.isPropertyPresent(AccountingAccount.class, "fee"), "Expected AccountingAccount to have [fee] field.");

		AccountingAccountSearchForm searchForm = new AccountingAccountSearchForm();
		searchForm.setLimit(50);
		List<AccountingAccount> accountingAccounts = this.accountingAccountService.getAccountingAccountList(searchForm).stream()
				.filter(a -> a.isCommission() || a.isFee())
				.collect(Collectors.toList());
		List<AccountingAccount> accountList = this.tradeInstructionUtilHandler.getCommissionAndFeeAccounts();
		Assertions.assertFalse(accountList.isEmpty());
		MatcherAssert.assertThat(accountList, IsEqual.equalTo(accountingAccounts));
	}
}
