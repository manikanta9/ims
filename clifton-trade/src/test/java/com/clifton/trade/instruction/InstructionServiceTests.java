package com.clifton.trade.instruction;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.export.InstructionExportHandler;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.AbstractTradeMessage;
import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.clifton.instruction.messages.trade.beans.MessagePartyIdentifierTypes;
import com.clifton.instruction.messages.trade.beans.PartyIdentifier;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;


@ContextConfiguration
@Transactional
public class InstructionServiceTests extends BaseInMemoryDatabaseTests {

	public static final String PARAMETRIC_BIC = "PPSCUS66";

	@Resource
	public InvestmentInstructionService investmentInstructionService;

	@Resource
	public TradeService tradeService;

	@Resource
	public AccountingTransactionService accountingTransactionService;

	@Resource
	public SystemSchemaService systemSchemaService;

	@Resource
	public InvestmentAccountRelationshipService investmentAccountRelationshipService;

	@Resource
	public BusinessCompanyService businessCompanyService;

	// mock, testing the generation of Instruction Messages, conversion to SWIFT messages is tested elsewhere
	@Resource
	public InstructionExportHandler instructionExportHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		Mockito.reset(this.instructionExportHandler);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetInstructionPreviewFuture() {
		final int TRADE_ID = 728954;
		final int TRADE_FILL_ID = 914297;
		final long ACCOUNTING_TRANS_ONE_ID = 7313574;
		final long ACCOUNTING_TRANS_TWO_ID = 7313575;
		final int INVESTMENT_DEFINITION_ID = 141;

		Trade trade = this.tradeService.getTrade(TRADE_ID);
		Assertions.assertNotNull(trade);
		Assertions.assertEquals(1, trade.getTradeFillList().size());
		MatcherAssert.assertThat(trade.getTradeFillList().get(0).getId(), IsEqual.equalTo(TRADE_FILL_ID));

		List<AccountingTransaction> accountingTransactions = getAccountingPositionTransactionList(trade.getTradeFillList().get(0));
		Assertions.assertNotNull(accountingTransactions);
		Assertions.assertEquals(2, accountingTransactions.size());
		MatcherAssert.assertThat(accountingTransactions.get(0).getId(), IsEqual.equalTo(ACCOUNTING_TRANS_ONE_ID));
		MatcherAssert.assertThat(accountingTransactions.get(1).getId(), IsEqual.equalTo(ACCOUNTING_TRANS_TWO_ID));

		MatcherAssert.assertThat(trade.getQuantity(), IsEqual.equalTo(new BigDecimal("7.0000000000")));
		MatcherAssert.assertThat(accountingTransactions.get(0).getQuantity(), IsEqual.equalTo(new BigDecimal("-1.0000000000")));
		MatcherAssert.assertThat(accountingTransactions.get(1).getQuantity(), IsEqual.equalTo(new BigDecimal("-6.0000000000")));
		MatcherAssert.assertThat(accountingTransactions.get(0).getPrice(), IsEqual.equalTo(accountingTransactions.get(1).getPrice()));
		MatcherAssert.assertThat(accountingTransactions.get(0).getPrice(), IsEqual.equalTo(new BigDecimal("1702.771428000000000")));

		this.investmentInstructionService.getInvestmentInstructionPreview(TRADE_ID, "Trade", INVESTMENT_DEFINITION_ID);
		@SuppressWarnings("unchecked")
		ArgumentCaptor<List<InstructionMessage>> captor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(this.instructionExportHandler, Mockito.times(1)).previewExportMessage(captor.capture());
		Assertions.assertNotNull(captor.getAllValues());
		Assertions.assertEquals(1, captor.getAllValues().size());
		List<InstructionMessage> instructionMessageList = captor.getAllValues().get(0);
		Assertions.assertNotNull(instructionMessageList);
		Assertions.assertEquals(1, instructionMessageList.size());

		InstructionMessage instructionMessage = instructionMessageList.get(0);
		Assertions.assertTrue(instructionMessage instanceof ReceiveAgainstPayment);
		MatcherAssert.assertThat(instructionMessage.getTransactionReferenceNumber(), IsEqual.equalTo(
				"IMS1II;IMS728954T;IMS914297TF"
		));

		AbstractTradeMessage tradeMessage = (AbstractTradeMessage) instructionMessage;
		MatcherAssert.assertThat(tradeMessage.isBuy(), IsEqual.equalTo(trade.isBuy()));
		MatcherAssert.assertThat(tradeMessage.getReceiverBIC(), IsEqual.equalTo("SBOSUS3UIMS"));
		MatcherAssert.assertThat(tradeMessage.getSenderBIC(), IsEqual.equalTo(PARAMETRIC_BIC));
		MatcherAssert.assertThat(tradeMessage.getMessageFunctions(), IsEqual.equalTo(MessageFunctions.NEW_MESSAGE));
		MatcherAssert.assertThat(tradeMessage.getOpen(), IsEqual.equalTo(false));
		MatcherAssert.assertThat(tradeMessage.getCustodyBIC(), IsEqual.equalTo("SBOSUS3UIMS"));
		MatcherAssert.assertThat(tradeMessage.getCustodyAccountNumber(), IsEqual.equalTo("OSMO"));

		Assertions.assertFalse(trade.isBuy());
		MatcherAssert.assertThat(tradeMessage.getClearingBrokerIdentifier().getIdentifier(), IsEqual.equalTo(trade.getHoldingInvestmentAccount().getIssuingCompany().getBusinessIdentifierCode()));
		MatcherAssert.assertThat(tradeMessage.getExecutingBrokerIdentifier().getIdentifier(), IsEqual.equalTo(trade.getExecutingBrokerCompany().getBusinessIdentifierCode()));
		MatcherAssert.assertThat(tradeMessage.getTradeDate(), IsEqual.equalTo(trade.getTradeDate()));
		MatcherAssert.assertThat(tradeMessage.getSettlementDate(), IsEqual.equalTo(trade.getSettlementDate()));
		MatcherAssert.assertThat(tradeMessage.getPriceType(), IsEqual.equalTo(TradeMessagePriceTypes.ACTUAL));
		MatcherAssert.assertThat(tradeMessage.getPrice(), IsEqual.equalTo(new BigDecimal("1702.771428000000000")));
		MatcherAssert.assertThat(tradeMessage.getQuantity(), IsEqual.equalTo(new BigDecimal("7.0000000000")));
		MatcherAssert.assertThat(tradeMessage.getIsitcCode(), IsEqual.equalTo(ISITCCodes.FUT));
		Assertions.assertNull(tradeMessage.getIssueDate());
		Assertions.assertNull(tradeMessage.getMaturityDate());

		BigDecimal commissionAmount = accountingTransactions.stream().map(t -> sumChildAccounts(t.getId(), "Commission")).reduce(BigDecimal.ZERO, BigDecimal::add);
		BigDecimal gainLoss = accountingTransactions.stream().map(t -> sumChildAccounts(t.getId(), "Realized Gain / Loss")).reduce(BigDecimal.ZERO, BigDecimal::add);
		BigDecimal positionCostBasis = accountingTransactions.stream().map(AccountingTransaction::getPositionCostBasis).reduce(BigDecimal.ZERO, BigDecimal::add);

		MatcherAssert.assertThat(tradeMessage.getAccountingNotional(), IsEqual.equalTo(positionCostBasis.abs().add(gainLoss)));
		MatcherAssert.assertThat(tradeMessage.getCommissionAmount(), IsEqual.equalTo(commissionAmount));
		MatcherAssert.assertThat(tradeMessage.getCurrentFactor(), IsEqual.equalTo(trade.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted() ? trade.getNotionalMultiplier() : null));
		Assertions.assertNull(tradeMessage.getInterestRate());
		Assertions.assertNull(tradeMessage.getIsin());
		Assertions.assertNull(tradeMessage.getAccruedInterest());
		MatcherAssert.assertThat(tradeMessage.getPlaceOfSettlement(), IsEqual.equalTo("XX"));
		Assertions.assertNull(tradeMessage.getRegulatoryAmount());
		Assertions.assertNull(tradeMessage.getBrokerCommissionAmount());
		Assertions.assertNull(tradeMessage.getAmortizedFaceAmount());
		Assertions.assertNull(tradeMessage.getOriginalFaceAmount());
		MatcherAssert.assertThat(tradeMessage.getDealAmount(), IsEqual.equalTo(BigDecimal.ZERO));
		MatcherAssert.assertThat(tradeMessage.getNetSettlementAmount(), IsEqual.equalTo(commissionAmount));
	}


	@SuppressWarnings("unchecked")
	@Test
	public void testGetInstructionPreviewFund() {
		final int TRADE_ID = 732372;
		final int TRADE_FILL_ID = 918278;
		final long ACCOUNTING_TRANS_ID = 7344681;
		final int INVESTMENT_DEFINITION_ID = 52;

		Trade trade = this.tradeService.getTrade(TRADE_ID);
		Assertions.assertNotNull(trade);
		Assertions.assertEquals(1, trade.getTradeFillList().size());
		MatcherAssert.assertThat(trade.getTradeFillList().get(0).getId(), IsEqual.equalTo(TRADE_FILL_ID));

		List<AccountingTransaction> accountingTransactions = getAccountingPositionTransactionList(trade.getTradeFillList().get(0));
		Assertions.assertNotNull(accountingTransactions);
		Assertions.assertEquals(1, accountingTransactions.size());
		AccountingTransaction accountingTransaction = accountingTransactions.get(0);
		MatcherAssert.assertThat(accountingTransaction.getId(), IsEqual.equalTo(ACCOUNTING_TRANS_ID));

		MatcherAssert.assertThat(trade.getQuantity(), IsEqual.equalTo(new BigDecimal("1125.0000000000")));
		MatcherAssert.assertThat(accountingTransaction.getQuantity(), IsEqual.equalTo(new BigDecimal("-1125.0000000000")));
		MatcherAssert.assertThat(accountingTransaction.getPrice(), IsEqual.equalTo(new BigDecimal("227.300000000000000")));

		this.investmentInstructionService.getInvestmentInstructionPreview(TRADE_ID, "Trade", INVESTMENT_DEFINITION_ID);
		ArgumentCaptor<List<InstructionMessage>> captor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(this.instructionExportHandler, Mockito.times(1)).previewExportMessage(captor.capture());
		Assertions.assertNotNull(captor.getAllValues());
		Assertions.assertEquals(1, captor.getAllValues().size());
		List<InstructionMessage> instructionMessageList = captor.getAllValues().get(0);
		Assertions.assertNotNull(instructionMessageList);
		Assertions.assertEquals(1, instructionMessageList.size());

		// transaction 1
		InstructionMessage instructionMessage = instructionMessageList.get(0);
		Assertions.assertTrue(instructionMessage instanceof ReceiveAgainstPayment);
		MatcherAssert.assertThat(instructionMessage.getTransactionReferenceNumber(), IsEqual.equalTo(
				"IMS1II;IMS732372T;IMS918278TF"
		));

		AbstractTradeMessage tradeMessage = (AbstractTradeMessage) instructionMessage;
		MatcherAssert.assertThat(tradeMessage.isBuy(), IsEqual.equalTo(trade.isBuy()));
		MatcherAssert.assertThat(tradeMessage.getReceiverBIC(), IsEqual.equalTo("IRVTUS3NIBK"));
		MatcherAssert.assertThat(tradeMessage.getSenderBIC(), IsEqual.equalTo(PARAMETRIC_BIC));
		MatcherAssert.assertThat(tradeMessage.getMessageFunctions(), IsEqual.equalTo(MessageFunctions.NEW_MESSAGE));
		Assertions.assertNull(tradeMessage.getOpen());
		MatcherAssert.assertThat(tradeMessage.getCustodyBIC(), IsEqual.equalTo("IRVTUS3NIBK"));
		MatcherAssert.assertThat(tradeMessage.getCustodyAccountNumber(), IsEqual.equalTo("549807"));
		MatcherAssert.assertThat(tradeMessage.getClearingBrokerIdentifier(), IsEqual.equalTo(new PartyIdentifier(trade.getExecutingBrokerCompany().getDtcNumber(), MessagePartyIdentifierTypes.DTC)));
		MatcherAssert.assertThat(tradeMessage.getExecutingBrokerIdentifier(), IsEqual.equalTo(new PartyIdentifier(trade.getExecutingBrokerCompany().getDtcNumber(), MessagePartyIdentifierTypes.DTC)));
		MatcherAssert.assertThat(tradeMessage.getTradeDate(), IsEqual.equalTo(DateUtils.toDate("08/31/2017")));
		MatcherAssert.assertThat(tradeMessage.getSettlementDate(), IsEqual.equalTo(DateUtils.toDate("09/06/2017")));
		MatcherAssert.assertThat(tradeMessage.getPriceType(), IsEqual.equalTo(TradeMessagePriceTypes.ACTUAL));
		MatcherAssert.assertThat(tradeMessage.getPrice(), IsEqual.equalTo(new BigDecimal("227.300000000000000")));
		MatcherAssert.assertThat(tradeMessage.getQuantity(), IsEqual.equalTo(new BigDecimal("1125.0000000000")));
		MatcherAssert.assertThat(tradeMessage.getIsitcCode(), IsEqual.equalTo(ISITCCodes.ETF));
		Assertions.assertNull(tradeMessage.getAccountingNotional());
		Assertions.assertNull(tradeMessage.getIssueDate());
		Assertions.assertNull(tradeMessage.getMaturityDate());
		MatcherAssert.assertThat(tradeMessage.getCommissionAmount(), IsEqual.equalTo(new BigDecimal("11.25")));
		MatcherAssert.assertThat(tradeMessage.getCurrentFactor(), IsEqual.equalTo(trade.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted() ? trade.getNotionalMultiplier() : null));
		Assertions.assertNull(tradeMessage.getInterestRate());
		MatcherAssert.assertThat(tradeMessage.getIsin(), IsEqual.equalTo("US9229083632"));
		Assertions.assertNull(tradeMessage.getAccruedInterest());
		MatcherAssert.assertThat(tradeMessage.getPlaceOfSettlement(), IsEqual.equalTo("DTCYUS33"));
		MatcherAssert.assertThat(tradeMessage.getRegulatoryAmount(), IsEqual.equalTo(new BigDecimal("5.91")));
		Assertions.assertNull(tradeMessage.getBrokerCommissionAmount());
		Assertions.assertNull(tradeMessage.getAmortizedFaceAmount());
		Assertions.assertNull(tradeMessage.getOriginalFaceAmount());
		MatcherAssert.assertThat(tradeMessage.getDealAmount(), IsEqual.equalTo(new BigDecimal("255712.50")));
		MatcherAssert.assertThat(tradeMessage.getNetSettlementAmount(), IsEqual.equalTo(new BigDecimal("255695.34")));
	}


	@SuppressWarnings("unchecked")
	@Test
	public void testGetInstructionPreviewBond() {
		final int TRADE_ID = 733326;
		final int TRADE_FILL_ID = 919301;
		final long ACCOUNTING_TRANS_ID = 7354573;
		final int INVESTMENT_DEFINITION_ID = 338;

		Trade trade = this.tradeService.getTrade(TRADE_ID);
		Assertions.assertNotNull(trade);
		Assertions.assertEquals(1, trade.getTradeFillList().size());
		MatcherAssert.assertThat(trade.getTradeFillList().get(0).getId(), IsEqual.equalTo(TRADE_FILL_ID));

		List<AccountingTransaction> accountingTransactions = getAccountingPositionTransactionList(trade.getTradeFillList().get(0));
		Assertions.assertNotNull(accountingTransactions);
		Assertions.assertEquals(1, accountingTransactions.size());
		AccountingTransaction accountingTransaction = accountingTransactions.get(0);
		MatcherAssert.assertThat(accountingTransaction.getId(), IsEqual.equalTo(ACCOUNTING_TRANS_ID));

		MatcherAssert.assertThat(trade.getQuantity(), IsEqual.equalTo(new BigDecimal("1593000.0000000000")));
		MatcherAssert.assertThat(accountingTransaction.getQuantity(), IsEqual.equalTo(new BigDecimal("-1593000.0000000000")));

		MatcherAssert.assertThat(accountingTransaction.getPrice(), IsEqual.equalTo(new BigDecimal("105.640625000000000")));
		MatcherAssert.assertThat(trade.getAverageUnitPrice(), IsEqual.equalTo(new BigDecimal("105.640625000000000")));
		MatcherAssert.assertThat(trade.getTradeFillList().get(0).getNotionalUnitPrice(), IsEqual.equalTo(new BigDecimal("105.640625000000000")));

		this.investmentInstructionService.getInvestmentInstructionPreview(TRADE_ID, "Trade", INVESTMENT_DEFINITION_ID);
		ArgumentCaptor<List<InstructionMessage>> captor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(this.instructionExportHandler, Mockito.times(1)).previewExportMessage(captor.capture());
		Assertions.assertNotNull(captor.getAllValues());
		Assertions.assertEquals(1, captor.getAllValues().size());
		List<InstructionMessage> instructionMessageList = captor.getAllValues().get(0);
		Assertions.assertNotNull(instructionMessageList);
		Assertions.assertEquals(1, instructionMessageList.size());

		// transaction 1
		InstructionMessage instructionMessage = instructionMessageList.get(0);
		Assertions.assertTrue(instructionMessage instanceof ReceiveAgainstPayment);
		MatcherAssert.assertThat(instructionMessage.getTransactionReferenceNumber(), IsEqual.equalTo(
				"IMS1II;IMS733326T;IMS919301TF"
		));

		AbstractTradeMessage tradeMessage = (AbstractTradeMessage) instructionMessage;
		MatcherAssert.assertThat(tradeMessage.isBuy(), IsEqual.equalTo(trade.isBuy()));
		MatcherAssert.assertThat(tradeMessage.getReceiverBIC(), IsEqual.equalTo("CNORUS44ETD"));
		MatcherAssert.assertThat(tradeMessage.getSenderBIC(), IsEqual.equalTo(PARAMETRIC_BIC));
		MatcherAssert.assertThat(tradeMessage.getMessageFunctions(), IsEqual.equalTo(MessageFunctions.NEW_MESSAGE));
		Assertions.assertNull(tradeMessage.getOpen());
		MatcherAssert.assertThat(tradeMessage.getCustodyBIC(), IsEqual.equalTo("CNORUS44ETD"));
		MatcherAssert.assertThat(tradeMessage.getCustodyAccountNumber(), IsEqual.equalTo("26-81780"));
		MatcherAssert.assertThat(tradeMessage.getClearingBrokerIdentifier(), IsEqual.equalTo(new PartyIdentifier("021000018", MessagePartyIdentifierTypes.USFW)));
		Assertions.assertNotNull(tradeMessage.getExecutingBrokerIdentifier());
		MatcherAssert.assertThat(tradeMessage.getTradeDate(), IsEqual.equalTo(DateUtils.toDate("09/05/2017")));
		MatcherAssert.assertThat(tradeMessage.getSettlementDate(), IsEqual.equalTo(DateUtils.toDate("09/06/2017")));
		MatcherAssert.assertThat(tradeMessage.getPriceType(), IsEqual.equalTo(TradeMessagePriceTypes.PERCENTAGE));
		MatcherAssert.assertThat(tradeMessage.getPrice(), IsEqual.equalTo(new BigDecimal("105.640625000000000")));
		Assertions.assertNull(tradeMessage.getQuantity());
		MatcherAssert.assertThat(tradeMessage.getIsitcCode(), IsEqual.equalTo(ISITCCodes.TN));
		Assertions.assertNull(tradeMessage.getAccountingNotional());
		Assertions.assertNull(tradeMessage.getCommissionAmount());
		MatcherAssert.assertThat(tradeMessage.getIssueDate(), IsEqual.equalTo(trade.getInvestmentSecurity().getStartDate()));
		MatcherAssert.assertThat(tradeMessage.getMaturityDate(), IsEqual.equalTo(trade.getInvestmentSecurity().getEndDate()));
		MatcherAssert.assertThat(tradeMessage.getCurrentFactor(), IsEqual.equalTo(trade.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted() ? trade.getNotionalMultiplier() : null));
		MatcherAssert.assertThat(tradeMessage.getInterestRate(), IsEqual.equalTo(new BigDecimal("3.5")));
		MatcherAssert.assertThat(tradeMessage.getIsin(), IsEqual.equalTo(trade.getInvestmentSecurity().getIsin()));
		MatcherAssert.assertThat(tradeMessage.getAccruedInterest(), IsEqual.equalTo(new BigDecimal("17271.93")));
		MatcherAssert.assertThat(tradeMessage.getPlaceOfSettlement(), IsEqual.equalTo("FRNYUS33"));
		Assertions.assertNull(tradeMessage.getRegulatoryAmount());
		Assertions.assertNull(tradeMessage.getBrokerCommissionAmount());
		Assertions.assertNull(tradeMessage.getAmortizedFaceAmount());
		MatcherAssert.assertThat(tradeMessage.getOriginalFaceAmount(), IsEqual.equalTo(new BigDecimal("1593000.00")));
		MatcherAssert.assertThat(tradeMessage.getDealAmount(), IsEqual.equalTo(trade.getAccountingNotional()));
		MatcherAssert.assertThat(tradeMessage.getNetSettlementAmount(), IsEqual.equalTo(new BigDecimal("1700127.09")));
	}


	@SuppressWarnings("unchecked")
	@Test
	public void testGetInstructionPreviewTips() {
		final int TRADE_ID = 744861;
		final int TRADE_FILL_ID = 931818;
		final long ACCOUNTING_TRANS_ID = 7432701;
		final int INVESTMENT_DEFINITION_ID = 325;

		Trade trade = this.tradeService.getTrade(TRADE_ID);
		Assertions.assertNotNull(trade);
		Assertions.assertEquals(1, trade.getTradeFillList().size());
		MatcherAssert.assertThat(trade.getTradeFillList().get(0).getId(), IsEqual.equalTo(TRADE_FILL_ID));

		List<AccountingTransaction> accountingTransactions = getAccountingPositionTransactionList(trade.getTradeFillList().get(0));
		Assertions.assertNotNull(accountingTransactions);
		Assertions.assertEquals(1, accountingTransactions.size());
		AccountingTransaction accountingTransaction = accountingTransactions.get(0);
		MatcherAssert.assertThat(accountingTransaction.getId(), IsEqual.equalTo(ACCOUNTING_TRANS_ID));

		MatcherAssert.assertThat(trade.getQuantity(), IsEqual.equalTo(new BigDecimal("4424000.0000000000")));
		MatcherAssert.assertThat(accountingTransaction.getQuantity(), IsEqual.equalTo(new BigDecimal("4424000.0000000000")));

		MatcherAssert.assertThat(accountingTransaction.getPrice(), IsEqual.equalTo(new BigDecimal("103.171875000000000")));
		MatcherAssert.assertThat(trade.getAverageUnitPrice(), IsEqual.equalTo(new BigDecimal("103.171875000000000")));
		MatcherAssert.assertThat(trade.getTradeFillList().get(0).getNotionalUnitPrice(), IsEqual.equalTo(new BigDecimal("103.171875000000000")));

		this.investmentInstructionService.getInvestmentInstructionPreview(TRADE_ID, "Trade", INVESTMENT_DEFINITION_ID);
		ArgumentCaptor<List<InstructionMessage>> captor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(this.instructionExportHandler, Mockito.times(1)).previewExportMessage(captor.capture());
		Assertions.assertNotNull(captor.getAllValues());
		Assertions.assertEquals(1, captor.getAllValues().size());
		List<InstructionMessage> instructionMessageList = captor.getAllValues().get(0);
		Assertions.assertNotNull(instructionMessageList);
		Assertions.assertEquals(1, instructionMessageList.size());

		// transaction 1
		InstructionMessage instructionMessage = instructionMessageList.get(0);
		Assertions.assertTrue(instructionMessage instanceof ReceiveAgainstPayment);
		MatcherAssert.assertThat(instructionMessage.getTransactionReferenceNumber(), IsEqual.equalTo(
				"IMS1II;IMS744861T;IMS931818TF"
		));

		AbstractTradeMessage tradeMessage = (AbstractTradeMessage) instructionMessage;
		MatcherAssert.assertThat(tradeMessage.isBuy(), IsEqual.equalTo(trade.isBuy()));
		MatcherAssert.assertThat(tradeMessage.getReceiverBIC(), IsEqual.equalTo("IRVTUS3NIBK"));
		MatcherAssert.assertThat(tradeMessage.getSenderBIC(), IsEqual.equalTo(PARAMETRIC_BIC));
		MatcherAssert.assertThat(tradeMessage.getMessageFunctions(), IsEqual.equalTo(MessageFunctions.NEW_MESSAGE));
		Assertions.assertNull(tradeMessage.getOpen());
		MatcherAssert.assertThat(tradeMessage.getCustodyBIC(), IsEqual.equalTo("IRVTUS3NIBK"));
		MatcherAssert.assertThat(tradeMessage.getCustodyAccountNumber(), IsEqual.equalTo("648525"));
		MatcherAssert.assertThat(tradeMessage.getClearingBrokerIdentifier(), IsEqual.equalTo(new PartyIdentifier("021000018", MessagePartyIdentifierTypes.USFW)));
		MatcherAssert.assertThat(tradeMessage.getExecutingBrokerIdentifier().getIdentifier(), IsEqual.equalTo("0161"));
		MatcherAssert.assertThat(tradeMessage.getTradeDate(), IsEqual.equalTo(DateUtils.toDate("09/15/2017")));
		MatcherAssert.assertThat(tradeMessage.getSettlementDate(), IsEqual.equalTo(DateUtils.toDate("09/18/2017")));
		MatcherAssert.assertThat(tradeMessage.getPriceType(), IsEqual.equalTo(TradeMessagePriceTypes.PERCENTAGE));
		MatcherAssert.assertThat(tradeMessage.getPrice(), IsEqual.equalTo(new BigDecimal("103.171875000000000")));
		Assertions.assertNull(tradeMessage.getQuantity());
		MatcherAssert.assertThat(tradeMessage.getIsitcCode(), IsEqual.equalTo(ISITCCodes.TIPS));
		Assertions.assertNull(tradeMessage.getAccountingNotional());
		Assertions.assertNull(tradeMessage.getCommissionAmount());
		MatcherAssert.assertThat(tradeMessage.getIssueDate(), IsEqual.equalTo(trade.getInvestmentSecurity().getStartDate()));
		MatcherAssert.assertThat(tradeMessage.getMaturityDate(), IsEqual.equalTo(trade.getInvestmentSecurity().getEndDate()));
		MatcherAssert.assertThat(tradeMessage.getCurrentFactor(), IsEqual.equalTo(trade.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted() ? trade.getNotionalMultiplier() : null));
		MatcherAssert.assertThat(tradeMessage.getInterestRate(), IsEqual.equalTo(new BigDecimal("0.625")));
		MatcherAssert.assertThat(tradeMessage.getIsin(), IsEqual.equalTo(trade.getInvestmentSecurity().getIsin()));
		MatcherAssert.assertThat(tradeMessage.getAccruedInterest(), IsEqual.equalTo(trade.getAccrualAmount()));
		MatcherAssert.assertThat(tradeMessage.getPlaceOfSettlement(), IsEqual.equalTo("FRNYUS33"));
		Assertions.assertNull(tradeMessage.getRegulatoryAmount());
		Assertions.assertNull(tradeMessage.getBrokerCommissionAmount());
		MatcherAssert.assertThat(tradeMessage.getAmortizedFaceAmount(), IsEqual.equalTo(new BigDecimal("4806277.84")));
		MatcherAssert.assertThat(tradeMessage.getOriginalFaceAmount(), IsEqual.equalTo(new BigDecimal("4424000.00")));
		MatcherAssert.assertThat(tradeMessage.getDealAmount(), IsEqual.equalTo(trade.getAccountingNotional()));
		MatcherAssert.assertThat(tradeMessage.getNetSettlementAmount(), IsEqual.equalTo(new BigDecimal("4964032.81")));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BigDecimal sumChildAccounts(long parentTransactionId, String... accountNames) {
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setParentId(parentTransactionId);
		List<AccountingTransaction> childTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		List<String> accountNameList = CollectionUtils.createList(accountNames);
		List<BigDecimal> amountList = childTransactionList.stream()
				.filter(t -> accountNameList.contains(t.getAccountingAccount().getName()))
				.map(AccountingJournalDetail::getLocalDebitCredit)
				.map(BigDecimal::abs)
				.collect(Collectors.toList());
		BigDecimal summation = new BigDecimal("0");
		for (BigDecimal c : amountList) {
			summation = summation.add(c);
		}
		return summation;
	}


	private List<AccountingTransaction> getAccountingPositionTransactionList(TradeFill tradeFill) {
		SystemTable table = this.systemSchemaService.getSystemTableByName(TradeFill.class.getSimpleName());
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setFkFieldId(tradeFill.getId());
		searchForm.setTableId(table.getId());
		searchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
		return this.accountingTransactionService.getAccountingTransactionList(searchForm);
	}
}
