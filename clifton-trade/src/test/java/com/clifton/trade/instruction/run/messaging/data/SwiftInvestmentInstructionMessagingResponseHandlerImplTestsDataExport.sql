SELECT 'InvestmentInstruction' AS entityTableName, InvestmentInstructionID AS entityId FROM InvestmentInstruction WHERE InvestmentInstructionID IN (41328)
UNION
SELECT 'Trade' AS entityTableName, TradeID AS entityId FROM Trade WHERE TradeID IN (SELECT FKFieldID FROM InvestmentInstructionItem WHERE InvestmentInstructionID IN (41328))
UNION
SELECT 'InvestmentInstructionItem' AS entityTableName, InvestmentInstructionItemID AS entityId FROM InvestmentInstructionItem WHERE InvestmentInstructionID IN (41328)
UNION
SELECT 'InvestmentInstructionStatus' AS entityTableName, InvestmentInstructionStatusID AS entityId FROM InvestmentInstructionStatus
UNION
SELECT 'InvestmentInstructionRun' AS entityTableName, InvestmentInstructionRunID AS entityId FROM InvestmentInstructionRun WHERE InvestmentInstructionID IN (41328) AND RunType = 'SWIFT'
UNION
SELECT 'InvestmentInstructionRunInstructionItem' AS entityTableName, InvestmentInstructionRunInstructionItemID AS entityId FROM InvestmentInstructionRunInstructionItem WHERE InvestmentInstructionItemID IN (SELECT InvestmentInstructionItemID FROM InvestmentInstructionItem WHERE InvestmentInstructionID IN (41328))
UNION
SELECT 'SystemBeanType' AS entityTableName, SystemBeanTypeID AS entityId FROM SystemBeanType WHERE SystemBeanTypeName = 'Instruction SWIFT'
UNION
SELECT 'SystemTable' AS entityTableName, SystemTableID AS entityId FROM SystemTable WHERE TableName = 'InvestmentInstructionItem';
