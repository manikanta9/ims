package com.clifton.trade.instruction;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.investment.instruction.search.InvestmentInstructionSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


@TradeInMemoryDatabaseContext
public class InvestmentInstructionServiceCancelTests extends BaseInMemoryDatabaseTests {

	@Resource
	private InvestmentInstructionService investmentInstructionService;


	@Test
	public void testUpdateInstructionItemsCancel() {
		Assertions.assertNotNull(this.investmentInstructionService);
		InvestmentInstructionSearchForm investmentInstructionSearchForm = new InvestmentInstructionSearchForm();
		investmentInstructionSearchForm.setTableName("Trade");
		List<InvestmentInstruction> investmentInstructionList = this.investmentInstructionService.getInvestmentInstructionList(investmentInstructionSearchForm);
		Assertions.assertFalse(investmentInstructionList.isEmpty());
		Assertions.assertEquals(1, investmentInstructionList.size());
		InvestmentInstruction instruction = investmentInstructionList.get(0);

		InvestmentInstructionItemSearchForm investmentInstructionItemSearchForm = new InvestmentInstructionItemSearchForm();
		investmentInstructionItemSearchForm.setInstructionId(instruction.getId());
		List<InvestmentInstructionItem> itemList = this.investmentInstructionService.getInvestmentInstructionItemList(investmentInstructionItemSearchForm);
		Assertions.assertFalse(itemList.isEmpty());
		Integer[] items = itemList.stream().map(InvestmentInstructionItem::getId).toArray(Integer[]::new);
		this.investmentInstructionService.cancelInvestmentInstructionItemList(items);

		investmentInstructionItemSearchForm = new InvestmentInstructionItemSearchForm();
		investmentInstructionItemSearchForm.setInstructionId(instruction.getId());
		List<InvestmentInstructionItem> cancelledItemList = this.investmentInstructionService.getInvestmentInstructionItemList(investmentInstructionItemSearchForm);
		Assertions.assertFalse(cancelledItemList.isEmpty());
		MatcherAssert.assertThat(cancelledItemList.size(), IsEqual.equalTo(itemList.size() * 2));

		Assertions.assertTrue(cancelledItemList.removeAll(itemList));
		MatcherAssert.assertThat(cancelledItemList.size(), IsEqual.equalTo(itemList.size()));

		final Map<Integer, InvestmentInstructionItem> itemMap = itemList.stream().collect(Collectors.toMap(InvestmentInstructionItem::getId, Function.identity()));
		Map<InvestmentInstructionItem, InvestmentInstructionItem> mapped = cancelledItemList.stream()
				.peek(i -> MatcherAssert.assertThat(i.getStatus().getInvestmentInstructionStatusName(), IsEqual.equalTo(InvestmentInstructionStatusNames.OPEN)))
				.peek(i -> MatcherAssert.assertThat(i.getInstruction(), IsEqual.equalTo(instruction)))
				.peek(i -> Assertions.assertNotNull(i.getReferencedInvestmentInstructionItem()))
				.peek(i -> Assertions.assertTrue(itemMap.containsKey(i.getReferencedInvestmentInstructionItem().getId())))
				.collect(Collectors.toMap(i -> this.investmentInstructionService.getInvestmentInstructionItem(i.getReferencedInvestmentInstructionItem().getId()), Function.identity(),
						CollectionUtils.throwingMerger("Duplicate key %s"), LinkedHashMap::new));
		MatcherAssert.assertThat(new HashSet<>(itemList), IsEqual.equalTo(mapped.keySet()));
		mapped.keySet().stream()
				.peek(i -> Assertions.assertNotNull(i.getStatus()))
				.peek(i -> MatcherAssert.assertThat(i.getStatus().getInvestmentInstructionStatusName(), IsEqual.equalTo(InvestmentInstructionStatusNames.PENDING_CANCEL)));
		InvestmentInstruction updatedInstruction = this.investmentInstructionService.getInvestmentInstruction(instruction.getId());
		Assertions.assertNotNull(updatedInstruction.getStatus());
		MatcherAssert.assertThat(updatedInstruction.getStatus().getInvestmentInstructionStatusName(), IsEqual.equalTo(InvestmentInstructionStatusNames.PENDING_CANCEL));
	}
}
