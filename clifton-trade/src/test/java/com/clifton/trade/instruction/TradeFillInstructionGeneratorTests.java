package com.clifton.trade.instruction;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.rule.AccountingPositionSplitterBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.session.BookingSession;
import com.clifton.accounting.gl.booking.session.SimpleBookingSession;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyUtilHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.Instruction;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.beans.ISITCCodes;
import com.clifton.instruction.messages.beans.MessageFunctions;
import com.clifton.instruction.messages.trade.DeliverAgainstPayment;
import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.clifton.instruction.messages.trade.beans.TradeMessagePriceTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.InstructionUtilHandlerImpl;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InstructionDeliveryFieldCommand;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandlerImpl;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.SystemTableBuilder;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.system.schema.util.SystemSchemaUtilHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.accounting.booking.rule.TradeCreatePositionBookingRule;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.instruction.populator.TradeInstructionFuturesPopulator;
import com.clifton.trade.instruction.populator.TradeInstructionPopulator;
import com.clifton.trade.instruction.populator.TradeInstructionPopulatorLocator;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.stubbing.Answer;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author mwacker
 */
public class TradeFillInstructionGeneratorTests {

	private static final InvestmentSecurity FUH0 = InvestmentSecurityBuilder.ofType(1, "FUH0", InvestmentType.FUTURES)
			.withPriceMultiplier(50)
			.build();

	private static final BusinessCompany GS, NTG, PPA_MPLS, CITI;


	static {
		GS = new BusinessCompany();
		GS.setId(7);
		GS.setName("Goldman Sachs & Co.");
		GS.setBusinessIdentifierCode("GOLDUS33");

		CITI = new BusinessCompany();
		CITI.setId(8);
		CITI.setName("Citigroup Global Markets Inc.");
		CITI.setBusinessIdentifierCode("SBNYUS33");

		NTG = new BusinessCompany();
		NTG.setId(5079);
		NTG.setName("Northern Trust Co. IL (Global)");
		NTG.setBusinessIdentifierCode("CNORUS40COL");

		PPA_MPLS = new BusinessCompany();
		PPA_MPLS.setId(1);
		PPA_MPLS.setName(BusinessCompanyUtilHandler.PPA_MINNEAPOLIS_COMPANY_NAME);
		PPA_MPLS.setBusinessIdentifierCode("PPSCUS66");

		FUH0.getInstrument().getHierarchy().setName("Equities");
	}


	@Mock
	private BusinessCompanyUtilHandler businessCompanyUtilHandler;

	@Mock
	private SystemSchemaUtilHandler systemSchemaUtilHandler;

	@Mock
	private SystemSchemaService systemSchemaService;

	@Mock
	private AccountingTransactionService accountingTransactionService;

	@Mock
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	@Mock
	private TradeService tradeService;

	@Mock
	private InvestmentSpecificityService investmentSpecificityService;

	@Mock
	private SystemColumnValueHandler systemColumnValueHandler;

	@Mock
	private InvestmentCalculatorService investmentCalculatorService;

	@Mock
	private InvestmentInstructionDeliveryUtilHandler investmentInstructionDeliveryUtilHandler;

	@Mock
	private DaoLocator daoLocator;

	@Mock
	private ReadOnlyDAO<Trade> dao;

	@Spy
	private TradeInstructionUtilHandlerImpl tradeInstructionUtilHandler;

	@Spy
	private InvestmentSecurityUtilHandlerImpl investmentSecurityUtilHandler;

	@Spy
	private InstructionUtilHandlerImpl instructionUtilHandler;


	private final TradeInstructionPopulatorLocator tradeInstructionPopulatorLocator = new TradeInstructionPopulatorLocator();

	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void init() throws NoSuchFieldException, IllegalAccessException {
		MockitoAnnotations.initMocks(this);

		// wire mocks into the tradeInstructionUtilHandler spy
		this.tradeInstructionUtilHandler.setAccountingTransactionService(this.accountingTransactionService);
		this.tradeInstructionUtilHandler.setBusinessCompanyUtilHandler(this.businessCompanyUtilHandler);
		this.tradeInstructionUtilHandler.setInvestmentAccountRelationshipService(this.investmentAccountRelationshipService);
		this.tradeInstructionUtilHandler.setInvestmentCalculatorService(this.investmentCalculatorService);

		this.investmentSecurityUtilHandler.setSystemColumnValueHandler(this.systemColumnValueHandler);
		this.investmentSecurityUtilHandler.setInvestmentSpecificityService(this.investmentSpecificityService);

		this.instructionUtilHandler.setSystemSchemaService(this.systemSchemaService);
		this.instructionUtilHandler.setSystemSchemaUtilHandler(this.systemSchemaUtilHandler);

		Mockito.doAnswer((Answer<ISITCCodes>) invocation -> {
			InvestmentInstrument investmentInstrument = (InvestmentInstrument) invocation.getArguments()[0];
			if ("Futures".equals(investmentInstrument.getHierarchy().getInvestmentType().getName())) {
				return ISITCCodes.FUT;
			}
			AssertUtils.fail("Cannot determine ISITC Code for the investment type " + investmentInstrument.getHierarchy().getInvestmentType().getName());
			return null;
		}).when(this.investmentSecurityUtilHandler).getISITCCodeStrict(ArgumentMatchers.any());

		Mockito.when(this.investmentInstructionDeliveryUtilHandler.getAdditionalPartyDeliveryFieldBicList(ArgumentMatchers.any(InstructionDeliveryFieldCommand.class))).thenReturn(Collections.emptyList());

		// only get the futures populator.
		final TradeInstructionFuturesPopulator<InvestmentInstructionItem> tradeFuturesInstructionPopulator = new TradeInstructionFuturesPopulator<>();
		tradeFuturesInstructionPopulator.setTradeInstructionUtilHandler(this.tradeInstructionUtilHandler);
		tradeFuturesInstructionPopulator.setInvestmentSecurityUtilHandler(this.investmentSecurityUtilHandler);
		tradeFuturesInstructionPopulator.setInstructionUtilHandler(this.instructionUtilHandler);
		tradeFuturesInstructionPopulator.setInvestmentInstructionDeliveryUtilHandler(this.investmentInstructionDeliveryUtilHandler);
		Map<String, TradeInstructionPopulator> handlerMap = new ConcurrentHashMap<>();
		tradeFuturesInstructionPopulator.getSupportedTypeKeyList().forEach(k -> handlerMap.put(k, tradeFuturesInstructionPopulator));
		Field field = TradeInstructionPopulatorLocator.class.getDeclaredField("handlerMap");
		field.setAccessible(true);
		field.set(this.tradeInstructionPopulatorLocator, handlerMap);

		Mockito.when(this.systemSchemaUtilHandler.getEntityUniqueId(ArgumentMatchers.any())).thenAnswer(invocation -> {
			IdentityObject id = (IdentityObject) invocation.getArguments()[0];
			if (id instanceof AccountingTransaction) {
				return "IMS" + id.getIdentity() + "AT";
			}
			else if (id instanceof Trade) {
				return "IMS" + id.getIdentity() + "T";
			}
			else if (id instanceof TradeFill) {
				return "IMS" + id.getIdentity() + "TF";
			}
			else if (Instruction.class.isAssignableFrom(id.getClass())) {
				return "IMS" + id.getIdentity() + "II";
			}
			return "IMS" + id.getIdentity() + "UNKNOWN";
		});

		InvestmentAccount custodianAccount = new InvestmentAccount();
		custodianAccount.setId(100);
		custodianAccount.setName(NTG.getName() + " XYZ123");
		custodianAccount.setNumber("XYZ123");

		custodianAccount.setIssuingCompany(NTG);

		Mockito.when(this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurposeStrict(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.any(Date.class)))
				.thenReturn(custodianAccount);

		SystemTable tradeFillTable = new SystemTable();
		tradeFillTable.setId((short) 199);
		tradeFillTable.setName(TradeFill.TABLE_NAME);
		Mockito.when(this.systemSchemaService.getSystemTableByName("TradeFill")).thenReturn(tradeFillTable);

		Mockito.when(this.businessCompanyUtilHandler.getParametricBusinessIdentifierCode()).thenReturn(PPA_MPLS.getBusinessIdentifierCode());

		Mockito.when(this.daoLocator.<Trade>locate("Trade")).thenReturn(this.dao);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDeliverAgainstPaymentFuture() {
		Trade trade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withSecurityEndDate(DateUtils.toDate("03/10/2017"))
				.withSecurityExchange("Chicago Mercantile Exchange", "XCME")
				.forHoldingAccountIssuingCompany(GS)
				.withExecutingBroker(CITI)
				.addFillWithQuantity(10)
				.createFills(true);

		Mockito.when(this.dao.findByPrimaryKey(42)).thenReturn(trade);

		AccountingTransaction transaction = getTradeFillTransaction(trade, 1000);
		mockTransactionLookup(trade, transaction);

		Mockito.when(this.tradeService.getTradeFillListByTrade(trade.getId())).thenReturn(trade.getTradeFillList());
		Assertions.assertEquals(1, trade.getTradeFillList().size());

		trade.getTradeFillList().get(0).setOpenCloseType(TradeBuilder.BUY_TO_OPEN);

		TradeInstructionGenerator converter = new TradeInstructionGenerator();
		converter.setTradeService(this.tradeService);
		converter.setTradeInstructionPopulatorLocator(this.tradeInstructionPopulatorLocator);
		converter.setDaoLocator(this.daoLocator);
		List<InstructionMessage> messages = converter.generateInstructionMessageList(new Instruction() {
			@Override
			public Integer getFkFieldId() {
				return trade.getId();
			}


			@Override
			public SystemTable getSystemTable() {
				return SystemTableBuilder.createTrade().toSystemTable();
			}


			@Override
			public Serializable getIdentity() {
				return 111;
			}


			@Override
			public boolean isNewBean() {
				return false;
			}
		});

		Assertions.assertTrue(messages.stream().allMatch(m -> m instanceof DeliverAgainstPayment));
		Assertions.assertEquals(1, messages.size());

		DeliverAgainstPayment tradeMessage = (DeliverAgainstPayment) messages.get(0);
		Assertions.assertEquals("PPSCUS66", tradeMessage.getSenderBIC());
		Assertions.assertEquals("CNORUS40COL", tradeMessage.getReceiverBIC());
		Assertions.assertEquals("CNORUS40COL", tradeMessage.getCustodyBIC());
		Assertions.assertEquals("XYZ123", tradeMessage.getCustodyAccountNumber());

		Assertions.assertTrue(trade.isBuy());
		Assertions.assertEquals("SBNYUS33", tradeMessage.getExecutingBrokerIdentifier().getIdentifier());
		Assertions.assertEquals("GOLDUS33", tradeMessage.getClearingBrokerIdentifier().getIdentifier());

		Assertions.assertEquals("IMS" + 111 + "II;" + "IMS" + trade.getId() + "T" + ";" + "IMS" + trade.getTradeFillList().get(0).getId() + "TF", tradeMessage.getTransactionReferenceNumber());
		Assertions.assertEquals(MessageFunctions.NEW_MESSAGE, tradeMessage.getMessageFunctions());
		Assertions.assertEquals(DateUtils.fromDate(trade.getTradeDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeMessage.getTradeDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(DateUtils.fromDate(trade.getSettlementDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeMessage.getSettlementDate(), DateUtils.DATE_FORMAT_INPUT));

		Assertions.assertEquals(true, tradeMessage.getOpen());

		Assertions.assertEquals(ISITCCodes.FUT, tradeMessage.getIsitcCode());
		Assertions.assertEquals("FUH0", tradeMessage.getSecuritySymbol());
		Assertions.assertNull(tradeMessage.getSecurityDescription());

		Assertions.assertEquals("USD", tradeMessage.getSecurityCurrency());
		Assertions.assertEquals("XCME", tradeMessage.getSecurityExchangeCode());
		Assertions.assertEquals("03/10/2017", DateUtils.fromDate(tradeMessage.getSecurityExpirationDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(50), tradeMessage.getPriceMultiplier()));

		Assertions.assertEquals(TradeMessagePriceTypes.ACTUAL, tradeMessage.getPriceType());
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("10.32423"), tradeMessage.getPrice()));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(10), tradeMessage.getQuantity()));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("5162.12"), tradeMessage.getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("102"), tradeMessage.getCommissionAmount()));
	}


	@Test
	public void testDeliverAgainstPaymentClosingFuture() {
		Trade trade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.withExpectedUnitPrice("10.32423")
				.withSecurityEndDate(DateUtils.toDate("03/10/2017"))
				.withSecurityExchange("Chicago Mercantile Exchange", "XCME")
				.forHoldingAccountIssuingCompany(GS)
				.withExecutingBroker(CITI)
				.addFillWithQuantity(10)
				.createFills(true);

		Assertions.assertFalse(trade.isBuy());
		trade.getTradeFillList().forEach(tf -> tf.setOpenCloseType(TradeBuilder.SELL_TO_CLOSE));

		Mockito.when(this.dao.findByPrimaryKey(42)).thenReturn(trade);

		AccountingTransaction transaction = getTradeFillTransactionWithSplitter(trade, 1000);
		mockTransactionLookup(trade, transaction);

		Mockito.when(this.tradeService.getTradeFillListByTrade(trade.getId())).thenReturn(trade.getTradeFillList());

		TradeInstructionGenerator converter = new TradeInstructionGenerator();
		converter.setTradeService(this.tradeService);
		converter.setTradeInstructionPopulatorLocator(this.tradeInstructionPopulatorLocator);
		converter.setDaoLocator(this.daoLocator);
		InstructionMessage message = converter.generateInstructionMessageList(new Instruction() {
			@Override
			public Integer getFkFieldId() {
				return trade.getId();
			}


			@Override
			public SystemTable getSystemTable() {
				return SystemTableBuilder.createTrade().toSystemTable();
			}


			@Override
			public Serializable getIdentity() {
				return 111;
			}


			@Override
			public boolean isNewBean() {
				return false;
			}
		}).get(0);

		Assertions.assertTrue(message instanceof ReceiveAgainstPayment);

		ReceiveAgainstPayment tradeMessage = (ReceiveAgainstPayment) message;
		Assertions.assertEquals("PPSCUS66", tradeMessage.getSenderBIC());
		Assertions.assertEquals("CNORUS40COL", tradeMessage.getReceiverBIC());
		Assertions.assertEquals("CNORUS40COL", tradeMessage.getCustodyBIC());
		Assertions.assertEquals("XYZ123", tradeMessage.getCustodyAccountNumber());

		Assertions.assertFalse(trade.isBuy());
		Assertions.assertEquals("SBNYUS33", tradeMessage.getExecutingBrokerIdentifier().getIdentifier());
		Assertions.assertEquals("GOLDUS33", tradeMessage.getClearingBrokerIdentifier().getIdentifier());

		Assertions.assertEquals("IMS" + 111 + "II;" + "IMS" + trade.getId() + "T" + ";" + "IMS" + trade.getTradeFillList().get(0).getId() + "TF", tradeMessage.getTransactionReferenceNumber());
		Assertions.assertEquals(MessageFunctions.NEW_MESSAGE, tradeMessage.getMessageFunctions());
		Assertions.assertEquals(DateUtils.fromDate(trade.getTradeDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeMessage.getTradeDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(DateUtils.fromDate(trade.getSettlementDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeMessage.getSettlementDate(), DateUtils.DATE_FORMAT_INPUT));

		Assertions.assertEquals(false, tradeMessage.getOpen());

		Assertions.assertEquals(ISITCCodes.FUT, tradeMessage.getIsitcCode());
		Assertions.assertEquals("FUH0", tradeMessage.getSecuritySymbol());
		Assertions.assertNull(tradeMessage.getSecurityDescription());

		Assertions.assertEquals("USD", tradeMessage.getSecurityCurrency());
		Assertions.assertEquals("XCME", tradeMessage.getSecurityExchangeCode());
		Assertions.assertEquals("03/10/2017", DateUtils.fromDate(tradeMessage.getSecurityExpirationDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(50), tradeMessage.getPriceMultiplier()));

		Assertions.assertEquals(TradeMessagePriceTypes.ACTUAL, tradeMessage.getPriceType());
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("10.32423"), tradeMessage.getPrice()));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(10), tradeMessage.getQuantity()));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("5162.12"), tradeMessage.getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("102"), tradeMessage.getCommissionAmount()));
	}


	@Test
	public void testReceiveAgainstPaymentFuture() {
		Trade trade = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.withExpectedUnitPrice("10.32423")
				.withSecurityEndDate(DateUtils.toDate("03/10/2017"))
				.withSecurityExchange("Chicago Mercantile Exchange", "XCME")
				.forHoldingAccountIssuingCompany(GS)
				.withExecutingBroker(CITI)
				.addFillWithQuantity(10)
				.createFills(true);

		Mockito.when(this.dao.findByPrimaryKey(42)).thenReturn(trade);

		AccountingTransaction transaction = getTradeFillTransaction(trade, 1000);
		mockTransactionLookup(trade, transaction);

		Mockito.when(this.tradeService.getTradeFillListByTrade(trade.getId())).thenReturn(trade.getTradeFillList());
		Assertions.assertEquals(1, trade.getTradeFillList().size());

		trade.getTradeFillList().get(0).setOpenCloseType(TradeBuilder.SELL_TO_OPEN);

		TradeInstructionGenerator converter = new TradeInstructionGenerator();
		converter.setTradeService(this.tradeService);
		converter.setTradeInstructionPopulatorLocator(this.tradeInstructionPopulatorLocator);
		converter.setDaoLocator(this.daoLocator);
		InstructionMessage message = converter.generateInstructionMessageList(new Instruction() {
			@Override
			public Integer getFkFieldId() {
				return trade.getId();
			}


			@Override
			public SystemTable getSystemTable() {
				return SystemTableBuilder.createTrade().toSystemTable();
			}


			@Override
			public Serializable getIdentity() {
				return 111;
			}


			@Override
			public boolean isNewBean() {
				return false;
			}
		}).get(0);

		Assertions.assertTrue(message instanceof ReceiveAgainstPayment);

		ReceiveAgainstPayment tradeMessage = (ReceiveAgainstPayment) message;
		Assertions.assertEquals("PPSCUS66", tradeMessage.getSenderBIC());
		Assertions.assertEquals("CNORUS40COL", tradeMessage.getReceiverBIC());
		Assertions.assertEquals("CNORUS40COL", tradeMessage.getCustodyBIC());
		Assertions.assertEquals("XYZ123", tradeMessage.getCustodyAccountNumber());

		Assertions.assertFalse(trade.isBuy());
		Assertions.assertEquals("SBNYUS33", tradeMessage.getExecutingBrokerIdentifier().getIdentifier());
		Assertions.assertEquals("GOLDUS33", tradeMessage.getClearingBrokerIdentifier().getIdentifier());

		Assertions.assertEquals("IMS" + 111 + "II;" + "IMS" + trade.getId() + "T" + ";" + "IMS" + trade.getTradeFillList().get(0).getId() + "TF", tradeMessage.getTransactionReferenceNumber());
		Assertions.assertEquals(MessageFunctions.NEW_MESSAGE, tradeMessage.getMessageFunctions());
		Assertions.assertEquals(DateUtils.fromDate(trade.getTradeDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeMessage.getTradeDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals(DateUtils.fromDate(trade.getSettlementDate(), DateUtils.DATE_FORMAT_INPUT), DateUtils.fromDate(tradeMessage.getSettlementDate(), DateUtils.DATE_FORMAT_INPUT));

		Assertions.assertEquals(true, tradeMessage.getOpen());

		Assertions.assertEquals(ISITCCodes.FUT, tradeMessage.getIsitcCode());
		Assertions.assertEquals("FUH0", tradeMessage.getSecuritySymbol());
		Assertions.assertNull(tradeMessage.getSecurityDescription());

		Assertions.assertEquals("USD", tradeMessage.getSecurityCurrency());
		Assertions.assertEquals("XCME", tradeMessage.getSecurityExchangeCode());
		Assertions.assertEquals("03/10/2017", DateUtils.fromDate(tradeMessage.getSecurityExpirationDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(50), tradeMessage.getPriceMultiplier()));

		Assertions.assertEquals(TradeMessagePriceTypes.ACTUAL, tradeMessage.getPriceType());
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("10.32423"), tradeMessage.getPrice()));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(10), tradeMessage.getQuantity()));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("5162.12"), tradeMessage.getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("102"), tradeMessage.getCommissionAmount()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingTransaction getTradeFillTransaction(Trade trade, long transactionId) {
		BookingSession<TradeFill> session = new SimpleBookingSession<>(AccountingTestObjectFactory.newAccountingJournal(), trade.getTradeFillList().get(0));
		TradeCreatePositionBookingRule bookingRule = TradeTestObjectFactory.newTradeCreatePositionBookingRule();
		bookingRule.applyRule(session);

		List<? extends AccountingJournalDetailDefinition> entries = session.getJournal().getJournalDetailList();
		AccountingTransaction result = new AccountingTransaction();
		BeanUtils.copyProperties(entries.get(0), result);
		result.setId(transactionId);
		return result;
	}


	private AccountingTransaction getTradeFillTransactionWithSplitter(Trade trade, long transactionId) {
		AccountingTransaction tran = AccountingTransactionBuilder.newTransactionForIdAndSecurity(11696L, trade.getInvestmentSecurity()).qty(10).price("9.5").on("1/1/2010").build();
		AccountingPositionSplitterBookingRule<TradeFill> splitterBookingRule = AccountingTestObjectFactory.newAccountingPositionSplitterBookingRuleByTransaction(tran);

		AccountingRealizedGainLossBookingRule<TradeFill> gainLossBookingRule = AccountingTestObjectFactory.newAccountingRealizedGainLossBookingRule();

		BookingSession<TradeFill> session = new SimpleBookingSession<>(AccountingTestObjectFactory.newAccountingJournal(), trade.getTradeFillList().get(0));
		TradeCreatePositionBookingRule bookingRule = TradeTestObjectFactory.newTradeCreatePositionBookingRule();
		bookingRule.applyRule(session);
		splitterBookingRule.applyRule(session);
		gainLossBookingRule.applyRule(session);

		List<? extends AccountingJournalDetailDefinition> entries = session.getJournal().getJournalDetailList();
		AccountingTransaction result = new AccountingTransaction();
		BeanUtils.copyProperties(entries.get(0), result);
		result.setId(transactionId);
		return result;
	}


	private void mockTransactionLookup(Trade trade, AccountingTransaction transaction) {
		Mockito.when(this.accountingTransactionService.getAccountingTransactionList(ArgumentMatchers.argThat(searchForm ->
				searchForm != null && searchForm.getFkFieldId().equals(trade.getTradeFillList().get(0).getId()) &&
						(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS == searchForm.getAccountingAccountIdName())
		))).thenReturn(CollectionUtils.createList(transaction));


		AccountingTransaction commissionTransaction = new AccountingTransaction();
		commissionTransaction.setLocalDebitCredit(new BigDecimal("102"));
		Mockito.when(this.accountingTransactionService.getAccountingTransactionList(ArgumentMatchers.argThat(searchForm ->
				searchForm != null && searchForm.getParentId() != null &&
						(AccountingAccountIdsCacheImpl.AccountingAccountIds.COMMISSION_ACCOUNTS == searchForm.getAccountingAccountIdName())

		))).thenReturn(CollectionUtils.createList(commissionTransaction));

		AccountingTransaction gainLossTransaction = new AccountingTransaction();
		gainLossTransaction.setLocalDebitCredit(new BigDecimal("412.12"));
		Mockito.when(this.accountingTransactionService.getAccountingTransactionList(ArgumentMatchers.argThat(searchForm ->
				searchForm != null && searchForm.getParentId() != null &&
						(AccountingAccountIdsCacheImpl.AccountingAccountIds.GAIN_LOSS_ACCOUNTS == searchForm.getAccountingAccountIdName())

		))).thenReturn(CollectionUtils.createList(gainLossTransaction));

//		AccountingTransaction commissionTransaction = new AccountingTransaction();
//		commissionTransaction.setLocalDebitCredit(new BigDecimal("102"));
//
//		AccountingTransaction gainLossTransaction = new AccountingTransaction();
//		commissionTransaction.setLocalDebitCredit(new BigDecimal("412.12"));
//
//
//		Mockito.when(this.accountingTransactionService.getAccountingTransactionList(ArgumentMatchers.any())).thenAnswer(new Answer<List<AccountingTransaction>>() {
//			@Override
//			public List<AccountingTransaction> answer(InvocationOnMock invocation) throws Throwable {
//				AccountingTransactionSearchForm searchForm = (AccountingTransactionSearchForm) invocation.getArguments()[0];
//				if (searchForm.getFkFieldId() != null && searchForm.getFkFieldId().equals(trade.getTradeFillList().get(0).getId()) &&
//						BooleanUtils.isTrue(searchForm.getPositionAccountingAccount())) {
//					return CollectionUtils.createList(transaction);
//				}
//				else if (searchForm.getParentId() != null &&
//						BooleanUtils.isTrue(searchForm.getCommissionAccountingAccount())) {
//					return CollectionUtils.createList(commissionTransaction);
//				}
//				else if (searchForm.getParentId() != null &&
//						BooleanUtils.isTrue(searchForm.getGainLossAccountingAccount())) {
//					return CollectionUtils.createList(gainLossTransaction);
//				}
//				return null;
//			}
//		});

	}
}
