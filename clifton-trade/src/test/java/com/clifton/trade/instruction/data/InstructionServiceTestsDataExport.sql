SELECT 'Trade' AS entityTableName, TradeID AS entityId FROM Trade WHERE TradeID = 728954
UNION
SELECT 'TradeFill' AS entityTableName, TradeFillID AS entityId FROM TradeFill WHERE TradeID = 728954
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE InvestmentAccountID IN (3139, 3240)
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountID IN (3139, 3240) OR RelatedAccountID IN (3139, 3240)
UNION
SELECT 'SystemTable' AS entityTableName, SystemTableID AS entityId FROM SystemTable WHERE TableName IN ('TradeFill', 'InvestmentInstructionItem', 'Trade', 'AccountingTransaction')
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue WHERE FKFieldID = 16944
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE FKFieldID = 914297 AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'TradeFill')
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE ParentTransactionID IN (7313574, 7313575)
UNION
SELECT 'Trade' AS entityTableName, TradeID AS entityId FROM Trade WHERE TradeID = 732372
UNION
SELECT 'TradeFill' AS entityTableName, TradeFillID AS entityId FROM TradeFill WHERE TradeID = 732372
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE InvestmentAccountID IN (3794, 3795)
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountID IN (3794, 3795) OR RelatedAccountID IN (3794, 3795)
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE FKFieldID = 918278 AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'TradeFill')
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE ParentTransactionID = 7344681
UNION
SELECT 'Trade' AS entityTableName, TradeID AS entityId FROM Trade WHERE TradeID = 733326
UNION
SELECT 'TradeFill' AS entityTableName, TradeFillID AS entityId FROM TradeFill WHERE TradeID = 733326
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE InvestmentAccountID IN (233, 2277)
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountID IN (233, 2277) OR RelatedAccountID IN (233, 2277)
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE FKFieldID = 919301 AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'TradeFill')
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE ParentTransactionID = 7344681
UNION
SELECT 'InvestmentSecurityEvent' AS entityTableName, InvestmentSecurityEventID AS entityId FROM InvestmentSecurityEvent WHERE InvestmentSecurityID = 18772
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue WHERE FKFieldID = 18772
UNION
SELECT 'Trade' AS entityTableName, TradeID AS entityId FROM Trade WHERE TradeID = 744861
UNION
SELECT 'TradeFill' AS entityTableName, TradeFillID AS entityId FROM TradeFill WHERE TradeID = 744861
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE InvestmentAccountID IN (5879, 5901)
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountID IN (5879, 5901) OR RelatedAccountID IN (5879, 5901)
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE FKFieldID = 931818 AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'TradeFill')
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE ParentTransactionID = 7432701
UNION
SELECT 'InvestmentSecurityEvent' AS entityTableName, InvestmentSecurityEventID AS entityId FROM InvestmentSecurityEvent WHERE InvestmentSecurityID = 16944
UNION
SELECT 'InvestmentSpecificityDefinition' AS entityTableName, InvestmentSpecificityDefinitionID AS entityId FROM InvestmentSpecificityDefinition WHERE InvestmentSpecificityDefinitionID = 5
UNION
SELECT 'InvestmentSpecificityEntry' AS entityTableName, InvestmentSpecificityEntryID AS entityId FROM InvestmentSpecificityEntry
UNION
SELECT 'InvestmentSpecificityField' AS entityTableName, InvestmentSpecificityFieldID AS entityId FROM InvestmentSpecificityField WHERE InvestmentSpecificityDefinitionID = 5
UNION
SELECT 'InvestmentSpecificityEntryProperty' AS entityTableName, InvestmentSpecificityEntryPropertyID AS entityId FROM InvestmentSpecificityEntryProperty
UNION
SELECT 'InvestmentSpecificityEntryPropertyType' AS entityTableName, InvestmentSpecificityEntryPropertyTypeID AS entityId FROM InvestmentSpecificityEntryPropertyType
UNION
SELECT 'InvestmentInstructionDefinition' AS entityTableName, InvestmentInstructionDefinitionID as entityId FROM InvestmentInstructionDefinition WHERE InvestmentInstructionDefinitionID in (141, 52, 338, 325)
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID as entityId FROM InvestmentSecurity WHERE InvestmentSecurityID in (1927)
UNION
SELECT 'BusinessCompany' AS entityTableName, BusinessCompanyID AS entityId FROM BusinessCompany WHERE BusinessCompanyID IN (7, 4073, 209)
UNION
SELECT 'InvestmentInstructionDelivery' AS entityTableName, InvestmentInstructionDeliveryID AS entityId FROM InvestmentInstructionDelivery WHERE InvestmentInstructionDeliveryID IN (10, 34, 20)
UNION
SELECT 'InvestmentInstructionDeliveryType' AS entityTableName, InvestmentInstructionDeliveryTypeID AS entityId FROM InvestmentInstructionDeliveryType
UNION
SELECT 'InvestmentInstructionDeliveryFieldType' AS entityTableName, InvestmentInstructionDeliveryFieldTypeID AS entityId FROM InvestmentInstructionDeliveryFieldType
UNION
SELECT 'InvestmentInstructionDeliveryBusinessCompany' AS entityTableName, InvestmentInstructionDeliveryBusinessCompanyID AS entityId FROM InvestmentInstructionDeliveryBusinessCompany;

