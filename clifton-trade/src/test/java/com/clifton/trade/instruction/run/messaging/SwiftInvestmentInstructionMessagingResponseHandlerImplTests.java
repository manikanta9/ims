package com.clifton.trade.instruction.run.messaging;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messaging.InstructionMessagingResponseHandler;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.instruction.messaging.message.InstructionMessagingStatusMessage;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunInstructionItem;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.InvestmentInstructionRunTypes;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@TradeInMemoryDatabaseContext
public class SwiftInvestmentInstructionMessagingResponseHandlerImplTests extends BaseInMemoryDatabaseTests {

	public static final int TRADE_ID = 755433;
	public static final int INSTRUCTION_ID = 41328;

	@Resource
	private InstructionMessagingResponseHandler instructionMessagingResponseHandler;

	@Resource
	private InvestmentInstructionRunService investmentInstructionRunService;

	@Resource
	private InvestmentInstructionStatusService investmentInstructionStatusService;

	@Resource
	private InvestmentInstructionService investmentInstructionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void before() {
		cancelAllExistingItems();
		List<InvestmentInstructionItem> itemList = getInvestmentInstructionItems(InvestmentInstructionStatusNames.PENDING_CANCEL, null);
		Assertions.assertFalse(itemList.isEmpty());
		createInstructionRun(itemList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void processTest() {
		InvestmentInstructionItem pendingCancelItem = getPendingCancelInvestmentItem();
		Assertions.assertNotNull(pendingCancelItem);

		// receive ACK for pending cancel
		InstructionMessagingStatusMessage instructionMessagingStatusMessage = createInstructionStatusMessage(pendingCancelItem.getId(), TRADE_ID);
		this.instructionMessagingResponseHandler.process(instructionMessagingStatusMessage);

		InvestmentInstruction instruction = this.investmentInstructionService.getInvestmentInstruction(INSTRUCTION_ID);
		MatcherAssert.assertThat(instruction.getStatus().getInvestmentInstructionStatusName(), IsEqual.equalTo(InvestmentInstructionStatusNames.OPEN));

		// the original is cancelled the replacement is open.
		List<InvestmentInstructionItem> itemList = getInvestmentInstructionItems(null, TRADE_ID);
		List<InvestmentInstructionStatusNames> statusNamesList = itemList.stream()
				.map(InvestmentInstructionItem::getStatus)
				.map(InvestmentInstructionStatus::getInvestmentInstructionStatusName)
				.collect(Collectors.toList());
		MatcherAssert.assertThat(statusNamesList.size(), IsEqual.equalTo(2));
		Assertions.assertTrue(statusNamesList.contains(InvestmentInstructionStatusNames.CANCELED));
		Assertions.assertTrue(statusNamesList.contains(InvestmentInstructionStatusNames.OPEN));

		Optional<InvestmentInstructionItem> cancelled = itemList.stream().filter(i -> InvestmentInstructionStatusNames.CANCELED == i.getStatus().getInvestmentInstructionStatusName()).findFirst();
		Assertions.assertTrue(cancelled.isPresent());
		Assertions.assertNull(cancelled.get().getReferencedInvestmentInstructionItem());

		Optional<InvestmentInstructionItem> replacement = itemList.stream().filter(i -> InvestmentInstructionStatusNames.OPEN == i.getStatus().getInvestmentInstructionStatusName()).findFirst();
		Assertions.assertTrue(replacement.isPresent());
		MatcherAssert.assertThat(replacement.get().getReferencedInvestmentInstructionItem(), IsEqual.equalTo(cancelled.get()));

		itemList = getInvestmentInstructionItems(InvestmentInstructionStatusNames.OPEN, TRADE_ID);
		createInstructionRun(itemList);

		// send the replacement instruction item.
		InvestmentInstructionItem replacementInvestmentItem = getReplacementInvestmentItem(pendingCancelItem);
		instructionMessagingStatusMessage = createInstructionStatusMessage(replacementInvestmentItem.getId(), TRADE_ID);
		this.instructionMessagingResponseHandler.process(instructionMessagingStatusMessage);

		instruction = this.investmentInstructionService.getInvestmentInstruction(INSTRUCTION_ID);
		MatcherAssert.assertThat(instruction.getStatus().getInvestmentInstructionStatusName(), IsEqual.equalTo(InvestmentInstructionStatusNames.OPEN));
		itemList = getInvestmentInstructionItems(null, TRADE_ID);
		statusNamesList = itemList.stream()
				.map(InvestmentInstructionItem::getStatus)
				.map(InvestmentInstructionStatus::getInvestmentInstructionStatusName)
				.collect(Collectors.toList());
		MatcherAssert.assertThat(statusNamesList.size(), IsEqual.equalTo(2));
		Assertions.assertTrue(statusNamesList.contains(InvestmentInstructionStatusNames.CANCELED), "Does not contain CANCELLED " + statusNamesList);
		Assertions.assertTrue(statusNamesList.contains(InvestmentInstructionStatusNames.COMPLETED), "Does not contain COMPLETED " + statusNamesList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentInstructionItem getPendingCancelInvestmentItem() {
		InvestmentInstructionStatus pendingCancelStatus = this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.PENDING_CANCEL);
		InvestmentInstructionItemSearchForm searchForm = new InvestmentInstructionItemSearchForm();
		searchForm.setFkFieldId(TRADE_ID);
		searchForm.setItemStatus(pendingCancelStatus.getId());
		List<InvestmentInstructionItem> pendingCancelItems = this.investmentInstructionService.getInvestmentInstructionItemList(searchForm);
		return CollectionUtils.getOnlyElement(pendingCancelItems);
	}


	private InvestmentInstructionItem getReplacementInvestmentItem(InvestmentInstructionItem referencedInvestmentInstructionItem) {
		InvestmentInstructionStatus openStatus = this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN);
		InvestmentInstructionItemSearchForm searchForm = new InvestmentInstructionItemSearchForm();
		searchForm.setFkFieldId(TRADE_ID);
		searchForm.setItemStatus(openStatus.getId());
		List<InvestmentInstructionItem> referencedInvestmentItems = this.investmentInstructionService.getInvestmentInstructionItemList(searchForm);
		referencedInvestmentItems = referencedInvestmentItems.stream()
				.filter(i -> Objects.equals(referencedInvestmentInstructionItem, i.getReferencedInvestmentInstructionItem()))
				.collect(Collectors.toList());
		return CollectionUtils.getOnlyElement(referencedInvestmentItems);
	}


	private void cancelAllExistingItems() {
		List<InvestmentInstructionItem> itemList = getInvestmentInstructionItems(null, null);
		Integer[] itemIdList = itemList.stream().map(InvestmentInstructionItem::getId).toArray(Integer[]::new);
		this.investmentInstructionService.cancelInvestmentInstructionItemList(itemIdList);
	}


	private void createInstructionRun(List<InvestmentInstructionItem> itemList) {
		InvestmentInstruction instruction = this.investmentInstructionService.getInvestmentInstruction(INSTRUCTION_ID);

		Date date = new Date();
		InvestmentInstructionStatus completed = this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED);
		for (InvestmentInstructionItem item : itemList) {
			date = DateUtils.addMilliseconds(date, 1);
			InvestmentInstructionRun investmentInstructionRun = new InvestmentInstructionRun();
			investmentInstructionRun.setStartDate(date);
			investmentInstructionRun.setInvestmentInstruction(instruction);
			investmentInstructionRun.setDetached(false);
			investmentInstructionRun.setRunType(InvestmentInstructionRunTypes.SWIFT);
			investmentInstructionRun.setRunStatus(completed);
			investmentInstructionRun.setScheduledDate(date);
			investmentInstructionRun.setEndDate(date);
			investmentInstructionRun.setHistoryDescription("JUNIT");
			this.investmentInstructionRunService.saveInvestmentInstructionRun(investmentInstructionRun);

			InvestmentInstructionRunInstructionItem runInstructionItem = new InvestmentInstructionRunInstructionItem();
			runInstructionItem.setReferenceOne(investmentInstructionRun);
			runInstructionItem.setReferenceTwo(item);

			this.investmentInstructionRunService.saveInvestmentInstructionRunInstructionItem(runInstructionItem);
		}
	}


	private List<InvestmentInstructionItem> getInvestmentInstructionItems(InvestmentInstructionStatusNames statusName, Integer tradeId) {
		InvestmentInstruction instruction = this.investmentInstructionService.getInvestmentInstruction(INSTRUCTION_ID);

		InvestmentInstructionItemSearchForm searchForm = new InvestmentInstructionItemSearchForm();
		searchForm.setInstructionId(instruction.getId());
		if (tradeId != null) {
			searchForm.setFkFieldId(tradeId);
		}
		if (statusName != null) {
			searchForm.setItemStatus(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(statusName).getId());
		}
		return this.investmentInstructionService.getInvestmentInstructionItemList(searchForm);
	}


	private InstructionMessagingStatusMessage createInstructionStatusMessage(int investmentInstructionItemId, int tradeId) {
		InstructionMessagingStatusMessage instructionMessagingStatusMessage = new InstructionMessagingStatusMessage();
		instructionMessagingStatusMessage.setUniqueStringIdentifier("IMS" + investmentInstructionItemId + "II;" + "IMS" + tradeId + "T;" + "IMS943823TF;IMS7591528AT");
		instructionMessagingStatusMessage.setMessage("Junit Test message.");
		instructionMessagingStatusMessage.setStatus(InstructionStatuses.PROCESSED);
		return instructionMessagingStatusMessage;
	}
}
