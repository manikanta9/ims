SELECT 'InvestmentInstrument' AS entityTableName, InvestmentInstrumentID AS entityId FROM InvestmentInstrument WHERE InvestmentInstrumentID IN
		(select MAX(instr.InvestmentInstrumentID) from InvestmentInstrumentHierarchy hier INNER JOIN InvestmentInstrument instr ON instr.InvestmentInstrumentHierarchyID = hier.InvestmentInstrumentHierarchyID GROUP BY hier.InvestmentTypeID, hier.InvestmentTypeSubTypeID, hier.InvestmentTypeSubType2ID)
UNION
SELECT 'InvestmentSpecificityDefinition' AS entityTableName, InvestmentSpecificityDefinitionID AS entityId FROM InvestmentSpecificityDefinition WHERE InvestmentSpecificityDefinitionID IN (5, 6)
UNION
SELECT 'InvestmentSpecificityEntry' AS entityTableName, InvestmentSpecificityEntryID AS entityId FROM InvestmentSpecificityEntry
UNION
SELECT 'InvestmentSpecificityField' AS entityTableName, InvestmentSpecificityFieldID AS entityId FROM InvestmentSpecificityField WHERE InvestmentSpecificityDefinitionID IN (5, 6)
UNION
SELECT 'InvestmentSpecificityEntryProperty' AS entityTableName, InvestmentSpecificityEntryPropertyID AS entityId FROM InvestmentSpecificityEntryProperty
UNION
SELECT 'InvestmentSpecificityEntryPropertyType' AS entityTableName, InvestmentSpecificityEntryPropertyTypeID AS entityId FROM InvestmentSpecificityEntryPropertyType
UNION
SELECT 'Trade' AS entityTableName, TradeID AS entityId FROM Trade where TradeID in (767191, 765932, 766838)
UNION
SELECT 'TradeFill' AS entityTableName, TradeFillID AS entityId FROM TradeFill where TradeID in (767191, 765932, 766838)
UNION
SELECT 'InvestmentInstructionItem' AS entityTableName, InvestmentInstructionItemID AS entityId FROM InvestmentInstructionItem where FKFieldID in (767191, 765932, 766838)
UNION
SELECT 'SystemTable' AS entityTableName, SystemTableID AS entityId FROM SystemTable
UNION
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityId FROM AccountingTransaction WHERE FKFieldID in (SELECT TradeFillID FROM TradeFill WHERE TradeID in (767191, 765932, 766838)) AND SystemTableID = 199
UNION
SELECT 'InvestmentAccount' AS entityTableName, InvestmentAccountID AS entityId FROM InvestmentAccount WHERE InvestmentAccountID in (SELECT ClientInvestmentAccountID FROM Trade where TradeID in (767191, 765932, 766838))
UNION
SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityId FROM InvestmentAccountRelationship WHERE MainAccountID in (SELECT ClientInvestmentAccountID FROM Trade where TradeID in (767191, 765932, 766838))
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue WHERE FKFieldID in (16193, 79015, 40582)
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId FROM InvestmentSecurity WHERE InvestmentSecurityID in (79015, 40582)
UNION
SELECT 'BusinessCompany' AS entityTableName, BusinessCompanyID AS entityId FROM BusinessCompany WHERE BusinessCompanyID in (5171);
