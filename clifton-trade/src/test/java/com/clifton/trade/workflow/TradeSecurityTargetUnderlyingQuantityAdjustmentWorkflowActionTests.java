package com.clifton.trade.workflow;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.account.securitytarget.util.InvestmentAccountSecurityTargetAdjustmentUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;


/**
 * A set of tests to test the {@link TradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction} for proper operation.
 *
 * @author davidi
 */
@TradeInMemoryDatabaseContext
public class TradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowActionTests extends BaseInMemoryDatabaseTests {

	boolean isInitialized = false;

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	InvestmentAccountService investmentAccountService;

	@Resource
	InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	@Resource
	TradeService tradeService;

	@Resource
	InvestmentAccountSecurityTargetAdjustmentUtilHandler investmentAccountSecurityTargetAdjustmentUtilHandler;

	TradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		if (!this.isInitialized) {

			this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction = new TradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction();
			this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setInvestmentAccountSecurityTargetAdjustmentUtilHandler(this.investmentAccountSecurityTargetAdjustmentUtilHandler);
			this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setInvestmentAccountSecurityTargetService(this.investmentAccountSecurityTargetService);
			this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setTradeService(this.tradeService);

			this.isInitialized = true;
		}

		resetSecurityTargetForTest("W-000340", "PG");
		resetSecurityTargetForTest("W-001556", "IBM");
	}


	private void resetSecurityTargetForTest(String clientAccountNumber, String securitySymbol) {
		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(clientAccountNumber);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(securitySymbol);
		InvestmentAccountSecurityTarget target = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(clientAccount.getId(), security.getId(), DateUtils.toDate("02/21/2019"));
		if (target != null && target.getEndDate() != null) {
			target.setEndDate(null);
			this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(target);
		}
	}


	@Test
	public void testProcessAction_book_buy() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-000340", "PG", quantity, 10.00, tradeDate, true, true);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.add(initialQuantity, BigDecimal.valueOf(quantity));

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(true);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_unbook_buy() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-000340", "PG", quantity, 10.00, tradeDate, true, false);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.subtract(initialQuantity, BigDecimal.valueOf(quantity));

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(false);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_book_sell() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-000340", "PG", quantity, 10.00, tradeDate, false, true);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.subtract(initialQuantity, BigDecimal.valueOf(quantity));

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(true);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_unbook_sell() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-000340", "PG", quantity, 10.00, tradeDate, false, false);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.add(initialQuantity, BigDecimal.valueOf(quantity));

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(false);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_notional_book_buy() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-001556", "IBM", quantity, 120.0, tradeDate, true, true);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		secTarget.setTargetUnderlyingQuantity(null);
		secTarget.setTargetUnderlyingNotional(BigDecimal.valueOf(100000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialNotional = secTarget.getTargetUnderlyingNotional();
		BigDecimal expectedNotional = MathUtils.add(initialNotional, BigDecimal.valueOf(12000.00)).setScale(2, RoundingMode.DOWN);

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(true);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_notional_unbook_buy() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-001556", "IBM", quantity, 120.0, tradeDate, true, false);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		secTarget.setTargetUnderlyingQuantity(null);
		secTarget.setTargetUnderlyingNotional(BigDecimal.valueOf(100000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialNotional = secTarget.getTargetUnderlyingNotional();
		BigDecimal expectedNotional = MathUtils.subtract(initialNotional, BigDecimal.valueOf(12000.00)).setScale(2, RoundingMode.DOWN);

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(false);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_notional_book_sell() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-001556", "IBM", quantity, 120.0, tradeDate, false, true);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		secTarget.setTargetUnderlyingQuantity(null);
		secTarget.setTargetUnderlyingNotional(BigDecimal.valueOf(100000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialNotional = secTarget.getTargetUnderlyingNotional();
		BigDecimal expectedNotional = MathUtils.subtract(initialNotional, BigDecimal.valueOf(12000.00)).setScale(2, RoundingMode.DOWN);

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(true);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_notional_unbook_sell() {
		Date tradeDate = DateUtils.clearTime(new Date());
		double quantity = 100.0;
		Trade trade = createTrade("W-001556", "IBM", quantity, 120.0, tradeDate, false, false);
		InvestmentSecurity investmentSecurity = trade.getInvestmentSecurity();
		InvestmentAccountSecurityTarget secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(trade.getClientInvestmentAccount().getId(),
				investmentSecurity.getId(), tradeDate);
		secTarget.setStartDate(tradeDate);
		secTarget.setTargetUnderlyingQuantity(null);
		secTarget.setTargetUnderlyingNotional(BigDecimal.valueOf(100000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);

		BigDecimal initialNotional = secTarget.getTargetUnderlyingNotional();
		BigDecimal expectedNotional = MathUtils.add(initialNotional, BigDecimal.valueOf(12000.00)).setScale(2, RoundingMode.DOWN);

		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.setBooking(false);
		this.tradeSecurityTargetUnderlyingQuantityAdjustmentWorkflowAction.processAction(trade, null);

		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(trade.getClientInvestmentAccount(), investmentSecurity);
		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Trade createTrade(String accountNumber, String ticker, double quantity, double averagePrice, Date tradeDate, boolean isBuy, boolean isBook) {
		InvestmentSecurity testSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(ticker);
		InvestmentAccount investmentAccount = this.investmentAccountService.getInvestmentAccountByNumber(accountNumber);

		Trade trade = new Trade();
		trade.setClientInvestmentAccount(investmentAccount);
		trade.setTradeType(new TradeType());
		trade.getTradeType().setName(TradeType.STOCKS);
		trade.setTradeDate(tradeDate);
		trade.setFillQuantity(BigDecimal.valueOf(quantity));
		trade.setQuantityIntended(trade.getFillQuantity());
		trade.setInvestmentSecurity(testSecurity);
		trade.setAverageUnitPrice(BigDecimal.valueOf(averagePrice));
		trade.setBuy(isBuy);

		WorkflowState workflowState = new WorkflowState();
		if (isBook) {
			workflowState.setName(TradeService.TRADE_BOOKED_STATE_NAME);
		}
		else {
			workflowState.setName(TradeService.TRADE_UNBOOKED_STATE_NAME);
		}
		trade.setWorkflowState(workflowState);

		return trade;
	}


	private InvestmentAccountSecurityTarget getActiveInvestmentAccountSecurityTarget(InvestmentAccount account, InvestmentSecurity security) {
		InvestmentAccountSecurityTargetSearchForm investmentAccountSecurityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();

		investmentAccountSecurityTargetSearchForm.setClientInvestmentAccountId(account.getId());
		investmentAccountSecurityTargetSearchForm.setTargetUnderlyingSecurityId(security.getId());
		investmentAccountSecurityTargetSearchForm.setActive(true);
		investmentAccountSecurityTargetSearchForm.setOrderBy("startDate:desc");

		List<InvestmentAccountSecurityTarget> targetList = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetList(investmentAccountSecurityTargetSearchForm);
		if (!targetList.isEmpty()) {
			return targetList.get(0);
		}
		return null;
	}
}
