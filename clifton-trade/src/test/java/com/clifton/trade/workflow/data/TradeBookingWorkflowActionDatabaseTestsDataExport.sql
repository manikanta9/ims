SELECT 'Trade' "entityTableName", TradeID "entityId" FROM Trade t WHERE TradeID IN (746918)
UNION ALL
SELECT 'WorkflowAssignment' AS entityTableName, WorkflowAssignmentID AS entityId FROM WorkflowAssignment WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Trade Workflow')
UNION ALL
SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityId FROM WorkflowTransition WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Trade Workflow'))
UNION
SELECT 'WorkflowTransitionAction' AS entityTableName, WorkflowTransitionActionID AS entityId FROM WorkflowTransitionAction WHERE WorkflowTransitionID IN (SELECT WorkflowTransitionID FROM WorkflowTransition WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Trade Workflow')))
	AND ActionSystemBeanID <> (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Trade Intraday Matching Book and Post Workflow Action')
UNION
SELECT 'WorkflowTransitionAction' AS entityTableName, WorkflowTransitionActionID AS entityId FROM WorkflowTransitionAction WHERE WorkflowTransitionID IN (SELECT WorkflowTransitionID FROM WorkflowTransition WHERE EndWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Trade Workflow')))
	AND ActionSystemBeanID <> (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Trade Intraday Matching Book and Post Workflow Action')
UNION
SELECT 'AccountingJournalType' AS entityTableName, AccountingJournalTypeID AS entityId FROM AccountingJournalType WHERE JournalTypeName = 'Trade Journal'
UNION
SELECT 'AccountingJournalStatus' AS entityTableName, AccountingJournalStatusID AS entityId FROM AccountingJournalStatus
UNION
SELECT 'AccountingAccount' AS entityTableName, AccountingAccountID AS entityId FROM AccountingAccount
UNION
SELECT DISTINCT 'InvestmentAccountRelationship', InvestmentAccountRelationshipID FROM InvestmentAccountRelationship WHERE MainAccountID IN (
	SELECT InvestmentAccountID
	FROM
		(
			SELECT TradeID, ClientInvestmentAccountID, HoldingInvestmentAccountID
			FROM Trade t
			WHERE TradeID IN (746918)
		) p
		UNPIVOT (InvestmentAccountID FOR ColumnName IN (ClientInvestmentAccountID, HoldingInvestmentAccountID)
	) AS t
)
OR RelatedAccountID IN (
	SELECT InvestmentAccountID
	FROM
		(
			SELECT TradeID,ClientInvestmentAccountID, HoldingInvestmentAccountID
			FROM Trade t
			WHERE TradeID IN (746918)
		) p
		UNPIVOT (InvestmentAccountID FOR ColumnName IN (ClientInvestmentAccountID, HoldingInvestmentAccountID)
	) AS t
)
UNION
SELECT 'TradeOpenCloseType', TradeOpenCloseTypeID FROM TradeOpenCloseType;
