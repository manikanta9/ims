package com.clifton.trade.workflow.splitting;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionCommand;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionRetriever;
import com.clifton.accounting.gl.booking.session.BookingPosition;
import com.clifton.accounting.gl.position.AccountingPositionOrders;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.instruction.messages.InstructionMessage;
import com.clifton.instruction.messages.trade.ReceiveAgainstPayment;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.builder.TradeFillBuilder;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.instruction.TradeInstructionGenerator;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.search.WorkflowTransitionSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@ContextConfiguration("classpath:com/clifton/trade/TradeInMemoryDatabaseTests-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class TradeFillSplittingWorkflowActionInMemoryTest extends BaseInMemoryDatabaseTests {

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private AccountingBookingPositionRetriever accountingBookingPositionRetriever;

	@Resource
	private AccountingAccountService accountingAccountService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	@Resource
	private AccountingTransactionService accountingTransactionService;

	@Resource
	private TradeInstructionGenerator tradeInstructionGenerator;

	@Resource
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	private static final Map<AccountingTransactionGrouping, AccountingTransactionTotals> expectedGlDistribution = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public static void setup() {
		expectedGlDistribution.put(
				new AccountingTransactionGrouping(100, 1, 0, true),
				new AccountingTransactionTotals(BigDecimal.ZERO, BigDecimal.ZERO, new BigDecimal("11047405.00"), BigDecimal.ZERO)
		);
		expectedGlDistribution.put(
				new AccountingTransactionGrouping(510, -1, 0, false),
				new AccountingTransactionTotals(new BigDecimal("3125.0000000000"), new BigDecimal("11047405.00"), BigDecimal.ZERO, BigDecimal.ZERO)
		);
		expectedGlDistribution.put(
				new AccountingTransactionGrouping(120, 0, -1, true),
				new AccountingTransactionTotals(new BigDecimal("-1.0000000000"), BigDecimal.ZERO, BigDecimal.ZERO, new BigDecimal("-82285.00"))
		);
		expectedGlDistribution.put(
				new AccountingTransactionGrouping(120, 0, -1, false),
				new AccountingTransactionTotals(new BigDecimal("-3125.0000000000"), BigDecimal.ZERO, BigDecimal.ZERO, new BigDecimal("-246093220.00"))
		);
	}


	@Override
	public void beforeTest() {
		this.contextHandler.setBean(Context.USER_BEAN_NAME, this.securityUserService.getSecurityUserByPseudonym("imstestuser1"));
	}


	@Test
	public void testClosingMultipleLots() {
		Trade trade = TradeBuilder.createEmptyTrade()
				.sell()
				.withOpenCloseType(this.tradeService.getTradeOpenCloseTypeByName(TradeBuilder.SELL.getName()))
				.withQuantityIntended("3126")
				.withInvestmentSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly("RTYZ19"))
				.withPayingSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly("USD"))
				.withExecutingBroker(this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "Goldman Sachs & Co."))
				.withTradeDestination(this.tradeDestinationService.getTradeDestinationByName("GS Desk"))
				.withTradeType(this.tradeService.getTradeTypeByName("Futures"))
				.withTradeDate(DateUtils.toDate("12/13/2019"))
				.withSettlementDate(DateUtils.toDate("12/13/2019"))
				.withClientInvestmentAccount(this.investmentAccountService.getInvestmentAccountByNumber("058220"))
				.withHoldingInvestmentAccount(this.investmentAccountService.getInvestmentAccountByNumber("157-24060"))
				.withExpectedUnitPrice("1645.700000000000000")
				.withAverageUnitPrice("1645.700000000000000")
				.withExchangeRate("1")
				.withCommissionPerUnit(1.89)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.build();

		this.tradeService.saveTrade(trade);

		TradeFill tradeFill = TradeFillBuilder.aTradeFill()
				.withInvestmentSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly("RTYZ9"))
				.withQuantity("3126")
				.withNotionalUnitPrice("1645.700000000000000")
				.withTrade(trade)
				.build();

		this.tradeService.saveTradeFill(tradeFill);

		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setDeleted(false);
		searchForm.setHoldingAccountNumber("157-24060");
		List<AccountingTransaction> accountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		Assertions.assertFalse(accountingTransactionList.isEmpty());

		assertPositions(tradeFill);

		approve(trade.getId());
		finishExecution(trade.getId());
		bookAndPost(trade.getId());

		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(trade.getWorkflowState().getName(), "Booked");
		Assertions.assertTrue(this.tradeService.isTradeFullyBooked(trade.getId()));

		List<TradeFill> tradeFillList = this.tradeService.getTradeFillListByTrade(trade.getId());
		assertTradeFills(tradeFillList);

		searchForm = new AccountingTransactionSearchForm();
		searchForm.setDeleted(false);
		searchForm.setHoldingAccountNumber("157-24060");
		searchForm.setFkFieldIds(tradeFillList.stream().mapToInt(TradeFill::getId).boxed().toArray(Integer[]::new));
		List<AccountingTransaction> postedAccountingTransactionList = this.accountingTransactionService.getAccountingTransactionList(searchForm);
		Assertions.assertFalse(postedAccountingTransactionList.isEmpty());

		postedAccountingTransactionList.removeAll(accountingTransactionList);
		assertGlDistribution(postedAccountingTransactionList);

		InvestmentInstructionCategory category = this.investmentInstructionSetupService.getInvestmentInstructionCategoryByName("Trade Counterparty");
		InvestmentInstructionDefinition definition = new InvestmentInstructionDefinition();
		definition.setCategory(category);
		InvestmentInstruction investmentInstruction = new InvestmentInstruction();
		investmentInstruction.setDefinition(definition);
		InvestmentInstructionItem item = new InvestmentInstructionItem();
		item.setId(1);
		item.setFkFieldId(tradeFillList.get(0).getId());
		item.setInstruction(investmentInstruction);

		List<InstructionMessage> messageList = this.tradeInstructionGenerator.generateInstructionMessageList(item);
		assertInvestmentMessages(messageList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertPositions(TradeFill tradeFill) {
		AccountingAccount positionAccount = tradeFill.getTrade().isCollateralTrade() ? this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION_COLLATERAL)
				: this.accountingAccountService.getAccountingAccountByName(AccountingAccount.ASSET_POSITION);
		AccountingBookingPositionCommand command = new AccountingBookingPositionCommand(tradeFill, getPositionOrder(tradeFill), true, tradeFill.getSettlementDate(),
				tradeFill.getTransactionDate(), tradeFill.getClientInvestmentAccount().getId(), tradeFill.getHoldingInvestmentAccount().getId(), tradeFill.getInvestmentSecurity().getId(),
				positionAccount, true);
		List<BookingPosition> openPositions = this.accountingBookingPositionRetriever.getAccountingBookingOpenPositionList(command);
		Assertions.assertEquals(373, openPositions.size());
	}


	private void assertGlDistribution(List<AccountingTransaction> postedAccountingTransactionList) {
		final Map<AccountingTransactionGrouping, AccountingTransactionTotals> glDistribution = postedAccountingTransactionList.stream()
				.collect(Collectors.groupingBy(AccountingTransactionGrouping::new,
						Collectors.mapping(AccountingTransactionTotals::new,
								Collectors.reducing(AccountingTransactionTotals.ZERO, AccountingTransactionTotals::add)))
				);
		Assertions.assertEquals(expectedGlDistribution.keySet().size(), glDistribution.keySet().size());
		Assertions.assertEquals(expectedGlDistribution.values().size(), glDistribution.values().size());

		Assertions.assertTrue(glDistribution.keySet().containsAll(expectedGlDistribution.keySet()));
		Assertions.assertTrue(glDistribution.values().containsAll(expectedGlDistribution.values()));
	}


	private void assertTradeFills(List<TradeFill> tradeFillList) {
		Assertions.assertEquals(tradeFillList.size(), 2);
		Map<Boolean, List<TradeFill>> fillMap = tradeFillList.stream()
				.filter(tf -> tf.getOpenCloseType() != null)
				.filter(tf -> tf.getOpenCloseType().isExplicitOpenClose())
				.collect(Collectors.groupingBy(tf -> tf.getOpenCloseType().isOpen()));
		Assertions.assertEquals(2, fillMap.size());

		Assertions.assertEquals(1, fillMap.get(true).size());
		Assertions.assertEquals(1, fillMap.get(false).size());

		TradeFill closeFill = fillMap.get(false).get(0);
		TradeFill openFill = fillMap.get(true).get(0);

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("3126"), MathUtils.add(closeFill.getQuantity(), openFill.getQuantity())));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("257222910.00"), MathUtils.add(closeFill.getNotionalTotalPrice(), openFill.getNotionalTotalPrice())));
	}


	private void assertInvestmentMessages(List<InstructionMessage> messageList) {
		Assertions.assertEquals(2, messageList.size());

		final Map<Boolean, List<ReceiveAgainstPayment>> msgMap = messageList.stream()
				.peek(m -> Assertions.assertTrue(m instanceof ReceiveAgainstPayment))
				.map(m -> (ReceiveAgainstPayment) m)
				.collect(Collectors.groupingBy(ReceiveAgainstPayment::getOpen));
		Assertions.assertEquals(2, msgMap.keySet().size());

		List<ReceiveAgainstPayment> closedMsg = msgMap.get(false);
		List<ReceiveAgainstPayment> opedMsg = msgMap.get(true);
		Assertions.assertEquals(1, closedMsg.size());
		Assertions.assertEquals(1, opedMsg.size());

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("3125.0000000000"), closedMsg.get(0).getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("1645.700000000000000"), closedMsg.get(0).getPrice()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("257140625.00"), closedMsg.get(0).getAccountingNotional()));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("1.0000000000"), opedMsg.get(0).getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("1645.700000000000000"), opedMsg.get(0).getPrice()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("82285.00"), opedMsg.get(0).getAccountingNotional()));
	}


	private AccountingPositionOrders getPositionOrder(TradeFill tradeFill) {
		AccountingPositionOrders closingOrder = null;
		AccountingClosingMethods closingMethod = tradeFill.getAccountingClosingMethod();
		if (closingMethod != null) {
			closingOrder = AccountingPositionOrders.valueOf(closingMethod.name());
		}
		return closingOrder;
	}


	private void approve(int tradeId) {
		Trade trade = this.tradeService.getTrade(tradeId);
		WorkflowTransitionSearchForm searchForm = new WorkflowTransitionSearchForm();
		searchForm.setNameEquals("Approve Trade");
		searchForm.setWorkflowNameEquals("Trade Workflow");
		List<WorkflowTransition> transitions = this.workflowTransitionService.getWorkflowTransitionList(searchForm);
		transitions = transitions.stream().filter(t -> t.getStartWorkflowState().getName().equals(trade.getWorkflowState().getName())).collect(Collectors.toList());
		Assertions.assertEquals(1, transitions.size());
		this.workflowTransitionService.executeWorkflowTransitionByTransition("Trade", trade.getId(), transitions.get(0).getId());
	}


	private void finishExecution(int tradeId) {
		Trade trade = this.tradeService.getTrade(tradeId);
		WorkflowTransitionSearchForm searchForm = new WorkflowTransitionSearchForm();
		searchForm.setNameEquals("Finish Execution");
		searchForm.setWorkflowNameEquals("Trade Workflow");
		List<WorkflowTransition> transitions = this.workflowTransitionService.getWorkflowTransitionList(searchForm);
		transitions = transitions.stream().filter(t -> t.getStartWorkflowState().getName().equals(trade.getWorkflowState().getName())).collect(Collectors.toList());
		Assertions.assertEquals(1, transitions.size());
		this.workflowTransitionService.executeWorkflowTransitionByTransition("Trade", trade.getId(), transitions.get(0).getId());
	}


	private void bookAndPost(int tradeId) {
		Trade trade = this.tradeService.getTrade(tradeId);
		WorkflowTransitionSearchForm searchForm = new WorkflowTransitionSearchForm();
		searchForm.setNameEquals("Book and Post Trade");
		searchForm.setWorkflowNameEquals("Trade Workflow");
		List<WorkflowTransition> transitions = this.workflowTransitionService.getWorkflowTransitionList(searchForm);
		transitions = transitions.stream().filter(t -> t.getStartWorkflowState().getName().equals(trade.getWorkflowState().getName())).collect(Collectors.toList());
		Assertions.assertEquals(1, transitions.size());
		this.workflowTransitionService.executeWorkflowTransitionByTransition("Trade", trade.getId(), transitions.get(0).getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class AccountingTransactionGrouping {

		private final int accountId;
		private final int debitCreditSign;
		private final int positionCostBasisSign;
		private final boolean opening;


		public AccountingTransactionGrouping(AccountingTransaction accountingTransaction) {
			this(
					accountingTransaction.getAccountingAccount().getId(),
					accountingTransaction.getLocalDebitCredit().signum(),
					MathUtils.isNullOrZero(accountingTransaction.getPositionCostBasis()) ? 0 : accountingTransaction.getPositionCostBasis().signum(),
					accountingTransaction.isOpening()
			);
		}


		public AccountingTransactionGrouping(int accountId, int debitCreditSign, int positionCostBasisSign, boolean opening) {
			this.accountId = accountId;
			this.debitCreditSign = debitCreditSign;
			this.positionCostBasisSign = positionCostBasisSign;
			this.opening = opening;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			AccountingTransactionGrouping that = (AccountingTransactionGrouping) o;
			return this.accountId == that.accountId &&
					this.debitCreditSign == that.debitCreditSign &&
					this.positionCostBasisSign == that.positionCostBasisSign &&
					this.opening == that.opening;
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.accountId, this.debitCreditSign, this.positionCostBasisSign, this.opening);
		}
	}

	private static class AccountingTransactionTotals {

		public static AccountingTransactionTotals ZERO = new AccountingTransactionTotals(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO);

		private final BigDecimal quantity;
		private final BigDecimal credit;
		private final BigDecimal debit;
		private final BigDecimal positionCostBasis;


		public AccountingTransactionTotals(AccountingTransaction accountingTransaction) {
			this(
					MathUtils.isNullOrZero(accountingTransaction.getQuantity()) ? BigDecimal.ZERO : accountingTransaction.getQuantity(),
					accountingTransaction.getLocalCredit(),
					accountingTransaction.getLocalDebit(),
					MathUtils.isNullOrZero(accountingTransaction.getPositionCostBasis()) ? BigDecimal.ZERO : accountingTransaction.getPositionCostBasis()
			);
		}


		public AccountingTransactionTotals(BigDecimal quantity, BigDecimal credit, BigDecimal debit, BigDecimal positionCostBasis) {
			this.quantity = quantity;
			this.credit = credit;
			this.debit = debit;
			this.positionCostBasis = positionCostBasis;
		}


		public AccountingTransactionTotals add(AccountingTransactionTotals accountingTransactionTotal) {
			return new AccountingTransactionTotals(
					this.quantity.add(accountingTransactionTotal.quantity),
					this.credit.add(accountingTransactionTotal.credit),
					this.debit.add(accountingTransactionTotal.debit),
					this.positionCostBasis.add(accountingTransactionTotal.positionCostBasis)
			);
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			AccountingTransactionTotals that = (AccountingTransactionTotals) o;
			return MathUtils.isEqual(this.quantity, that.quantity) &&
					MathUtils.isEqual(this.credit, that.credit) &&
					MathUtils.isEqual(this.debit, that.debit) &&
					MathUtils.isEqual(this.positionCostBasis, that.positionCostBasis);
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.quantity, this.credit, this.debit, this.positionCostBasis);
		}
	}
}
