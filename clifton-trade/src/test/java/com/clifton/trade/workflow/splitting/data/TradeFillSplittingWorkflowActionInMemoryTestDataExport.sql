-- unpost 2162923 before exporting
SELECT 'AccountingTransaction' AS entityTableName, AccountingTransactionID AS entityID
FROM AccountingTransaction
WHERE ClientInvestmentAccountID = (SELECT IA.InvestmentAccountID
								   FROM InvestmentAccount IA
										INNER JOIN BusinessClient BC ON IA.BusinessClientID = BC.BusinessClientID
										INNER JOIN InvestmentAccountType IAT ON IA.InvestmentAccountTypeID = IAT.InvestmentAccountTypeID
								   WHERE IA.AccountName = 'American Beacon Small Cap Value Fund'
									 AND BC.ClientName = 'American Beacon Small Cap Value Fund'
									 AND IAT.AccountTypeName = 'Client')
  AND IsDeleted = 0
  AND HoldingInvestmentAccountID = (SELECT InvestmentAccountID
									FROM InvestmentAccount IA
										 INNER JOIN BusinessClient BC ON IA.BusinessClientID = BC.BusinessClientID
										 INNER JOIN InvestmentAccountType IAT ON IA.InvestmentAccountTypeID = IAT.InvestmentAccountTypeID
									WHERE IA.AccountName = 'American Beacon Small Cap Value Fund'
									  AND BC.ClientName = 'American Beacon Small Cap Value Fund'
									  AND IAT.AccountTypeName = 'Futures Broker')
  AND AccountingAccountID = (SELECT AccountingAccountID FROM AccountingAccount WHERE AccountingAccountName = 'Position')
  AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'RTYZ19')
  AND SettlementDate < '2019-12-13'

UNION

SELECT 'AccountingAccount' AS entityTableName, AccountingAccountID AS entityID
FROM AccountingAccount

UNION

SELECT 'InvestmentAccountRelationship' AS entityTableName, InvestmentAccountRelationshipID AS entityID
FROM InvestmentAccountRelationship
WHERE MainAccountID IN (SELECT IA.InvestmentAccountID
						FROM InvestmentAccount IA
							 INNER JOIN BusinessClient BC ON IA.BusinessClientID = BC.BusinessClientID
							 INNER JOIN InvestmentAccountType IAT ON IA.InvestmentAccountTypeID = IAT.InvestmentAccountTypeID
						WHERE IA.AccountName = 'American Beacon Small Cap Value Fund'
						  AND BC.ClientName = 'American Beacon Small Cap Value Fund'
						  AND IAT.AccountTypeName = 'Client')

UNION

SELECT 'TradeOpenCloseType' AS entityTableName, TradeOpenCloseTypeID AS entityID
FROM TradeOpenCloseType

UNION

SELECT 'TradeDestination' AS entityTableName, TradeDestinationID AS entityID
FROM TradeDestination
WHERE TradeDestinationName = 'GS Desk'

UNION

SELECT 'TradeType' AS entityTableName, TradeTypeID AS entityID
FROM TradeType
WHERE TradeTypeName = 'Futures'

UNION

SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityID
FROM MarketDataValue
WHERE InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'RTYZ9' AND MeasureDate = '12/13/2019')

UNION

SELECT 'SystemTable' AS entityTableName, SystemTableID AS entityID
FROM SystemTable
WHERE TableName IN ('Trade', 'TradeFill', 'InvestmentInstructionItem', 'AccountingTransaction')
  AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')

UNION

SELECT 'WorkflowAssignment' AS entityTableName, WorkflowAssignmentID AS entityID
FROM WorkflowAssignment
WHERE SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'Trade')

UNION

SELECT 'Workflow' AS entityTableName, WorkflowID AS entityID
FROM Workflow
WHERE WorkflowName = 'Trade Workflow'

UNION

SELECT 'WorkflowState' AS entityTableName, WorkflowStateID AS entityID
FROM WorkflowState
WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Trade Workflow')

UNION

SELECT 'WorkflowTransitionAction' AS entityTableName, WorkflowTransitionActionID AS entityID
FROM WorkflowTransitionAction

UNION

SELECT 'SecurityUser' AS entityTableName, SecurityUserID AS entityID
FROM SecurityUser
WHERE SecurityUserName = 'imstestuser1'

UNION

SELECT 'SecurityUserGroup' AS entityTableName, SecurityUserGroupID AS entityID
FROM SecurityUserGroup
WHERE SecurityUserID IN (SELECT SecurityUserID FROM SecurityUser WHERE SecurityUserName = 'imstestuser1')

UNION

SELECT 'SecurityGroup' AS entityTableName, SecurityGroupID AS entityID
FROM SecurityGroup

UNION

SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityID
FROM WorkflowTransition
WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Trade Workflow'))
   OR EndWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = (SELECT WorkflowID FROM Workflow WHERE WorkflowName = 'Trade Workflow'))

UNION

SELECT 'SystemCondition' AS entityTableName, SystemConditionID AS entityID
FROM SystemCondition

UNION

SELECT 'SystemConditionEntry' AS entityTableName, SystemConditionEntryID AS entityID
FROM SystemConditionEntry

UNION

SELECT 'SystemBeanType' AS entityTableName, SystemBeanTypeID AS entityID
FROM SystemBeanType
WHERE SystemBeanGroupID IN (SELECT SystemBeanGroupID FROM SystemBeanGroup WHERE SystemBeanGroupName = 'Workflow Transition Action')

UNION

SELECT 'SystemBean' AS entityTableName, SystemBeanID AS entityID
FROM SystemBean
WHERE SystemBeanID IN (SELECT SystemBeanID
					   FROM SystemConditionEntry
					   UNION
					   SELECT SystemBeanID
					   FROM SystemBean
					   WHERE SystemBeanTypeID IN (
						   SELECT SystemBeanTypeID
						   FROM SystemBeanType
						   WHERE SystemBeanGroupID IN (
							   SELECT SystemBeanGroupID
							   FROM SystemBeanGroup
							   WHERE SystemBeanGroupName = 'Workflow Transition Action'
						   )
					   )
					   UNION
					   SELECT ActionSystemBeanID
					   FROM WorkflowTransitionAction
)

UNION

SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID AS entityID
FROM SystemBeanProperty
WHERE SystemBeanID IN (
	SELECT SystemBeanID
	FROM SystemConditionEntry
	UNION
	SELECT SystemBeanID
	FROM SystemBean
	WHERE SystemBeanTypeID IN (
		SELECT SystemBeanTypeID
		FROM SystemBeanType
		WHERE SystemBeanGroupID IN (
			SELECT SystemBeanGroupID
			FROM SystemBeanGroup
			WHERE SystemBeanGroupName = 'Workflow Transition Action'
		)
	)
	UNION
	SELECT ActionSystemBeanID
	FROM WorkflowTransitionAction
)

UNION

SELECT 'InvestmentAccountGroup' AS entityTableName, InvestmentAccountGroupID AS entityID
FROM InvestmentAccountGroup

UNION

SELECT 'InvestmentInstructionCategory' AS entityTableName, InvestmentInstructionCategoryID AS entityID
FROM InvestmentInstructionCategory
WHERE CategoryName = 'Trade Counterparty'

UNION

SELECT 'RuleCategory' AS entityTableName, RuleCategoryID AS entityID
FROM RuleCategory

UNION

SELECT 'AccountingJournalStatus' AS entityTableName, AccountingJournalStatusID AS entityID
FROM AccountingJournalStatus;
