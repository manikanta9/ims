package com.clifton.trade.workflow;


import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.util.TradeUtilHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;


public class TradeBookingWorkflowActionTest {

	@SuppressWarnings("unchecked")
	@Test
	public void testOrderMultipleFills() {
		Trade futureOpenTrade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("ESZ1").build())
				.withId(42)
				.withExpectedUnitPrice("6.94544")
				.withCommissionPerUnit(0.02)
				.addFill("10", "46.21")
				.addFill("10", "44.03")
				.addFill("10", "45.43")
				.createFills();
		//make sure out of order
		Assertions.assertEquals(futureOpenTrade.getTradeFillList().get(0).getNotionalUnitPrice().doubleValue(), 46.21, 0.01);
		Assertions.assertEquals(futureOpenTrade.getTradeFillList().get(1).getNotionalUnitPrice().doubleValue(), 44.03, 0.01);
		Assertions.assertEquals(futureOpenTrade.getTradeFillList().get(2).getNotionalUnitPrice().doubleValue(), 45.43, 0.01);

		TradeService tradeService = Mockito.mock(TradeService.class);
		Mockito.when(tradeService.getTradeFillListByTrade(ArgumentMatchers.anyInt())).thenReturn(futureOpenTrade.getTradeFillList());

		TradeBookingWorkflowAction action = new TradeBookingWorkflowAction();
		action.setAccountingBookingService(Mockito.mock(AccountingBookingService.class));
		action.setTradeUtilHandler(Mockito.mock(TradeUtilHandler.class));
		Mockito.when(action.getAccountingBookingService().bookAccountingJournal(ArgumentMatchers.eq(AccountingJournalType.TRADE_JOURNAL), ArgumentMatchers.any(TradeFill.class), ArgumentMatchers.eq(true))).thenReturn(
				new AccountingJournal());
		action.setTradeService(tradeService);
		action.processAction(futureOpenTrade, null);
		Mockito.verify(action.getAccountingBookingService(), new Times(3)).bookAccountingJournal(ArgumentMatchers.eq(AccountingJournalType.TRADE_JOURNAL), ArgumentMatchers.any(TradeFill.class), ArgumentMatchers.eq(true));

		//make sure in order
		Assertions.assertEquals(44.03, futureOpenTrade.getTradeFillList().get(0).getNotionalUnitPrice().doubleValue(), 0.01);
		Assertions.assertEquals(45.43, futureOpenTrade.getTradeFillList().get(1).getNotionalUnitPrice().doubleValue(), 0.01);
		Assertions.assertEquals(46.21, futureOpenTrade.getTradeFillList().get(2).getNotionalUnitPrice().doubleValue(), 0.01);
	}
}
