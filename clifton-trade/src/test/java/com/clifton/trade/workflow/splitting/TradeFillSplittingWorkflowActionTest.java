package com.clifton.trade.workflow.splitting;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountBuilder;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionBuilder;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.BookableEntityLockProvider;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionRetriever;
import com.clifton.accounting.gl.booking.position.AccountingBookingPositionRetrieverImpl;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalStatus;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.SystemTableBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeServiceImpl;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.builder.TradeTypeBuilder;
import com.clifton.workflow.transition.WorkflowTransition;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;


@ExtendWith(MockitoExtension.class)
class TradeFillSplittingWorkflowActionTest {

	@Spy
	private final TradeService tradeService = new TradeServiceImpl();

	@Mock
	private AdvancedUpdatableDAO<TradeFill, Criteria> tradeFillDAO;

	@Spy
	private final DaoNamedEntityCache<TradeOpenCloseType> tradeOpenCloseTypeCache = new DaoNamedEntityCache<TradeOpenCloseType>() {

		private final Map<String, TradeOpenCloseType> cache = new HashMap<>();


		@Override
		public void put(String key, TradeOpenCloseType bean) {
			this.cache.put(key, bean);
		}


		@Override
		public TradeOpenCloseType getBeanForKeyValue(ReadOnlyDAO<TradeOpenCloseType> dao, String value) {
			return this.cache.get(value);
		}


		@Override
		public TradeOpenCloseType getBeanForKeyValueStrict(ReadOnlyDAO<TradeOpenCloseType> dao, String value) {
			return this.cache.get(value);
		}
	};

	@Mock
	private AccountingAccountService accountingAccountService;

	@Mock
	private AccountingPositionService accountingPositionService;

	@Mock
	private AccountingTransactionService accountingTransactionService;

	@Spy
	private final AccountingBookingPositionRetriever accountingBookingPositionRetriever = new AccountingBookingPositionRetrieverImpl();

	@Mock
	private InvestmentInstrumentService investmentInstrumentService;

	@Spy
	@SuppressWarnings("unused")
	private final BookableEntityLockProvider<TradeFill> bookableEntityLockProvider = new BookableEntityLockProvider<>();

	@Spy
	@SuppressWarnings("unused")
	private final InvestmentCalculator investmentCalculator = new InvestmentCalculatorImpl();

	@InjectMocks
	private TradeFillSplittingWorkflowAction tradeFillSplittingWorkflowAction;

	private final Lazy<SystemTable> tradeFillSystemTable = new Lazy<>(SystemTableBuilder.createTradeFill()::toSystemTable);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void beforeEach() {
		((TradeServiceImpl) this.tradeService).setTradeFillDAO(this.tradeFillDAO);
		((TradeServiceImpl) this.tradeService).setTradeOpenCloseTypeCache(this.tradeOpenCloseTypeCache);

		((AccountingBookingPositionRetrieverImpl) this.accountingBookingPositionRetriever).setAccountingPositionService(this.accountingPositionService);
		((AccountingBookingPositionRetrieverImpl) this.accountingBookingPositionRetriever).setAccountingTransactionService(this.accountingTransactionService);
		((AccountingBookingPositionRetrieverImpl) this.accountingBookingPositionRetriever).setInvestmentInstrumentService(this.investmentInstrumentService);

		Mockito.when(this.accountingAccountService.getAccountingAccountByName(ArgumentMatchers.anyString()))
				.thenAnswer(a -> {
					switch (a.getArgument(0).toString()) {
						case AccountingAccount.ASSET_POSITION_COLLATERAL:
							return AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION_COLLATERAL).asPosition().asCollateral().build();
						case AccountingAccount.ASSET_POSITION:
							return AccountingAccountBuilder.newAsset(AccountingAccount.ASSET_POSITION).asPosition().build();
						default:
							Assertions.fail("Unknown account " + a.getArgument(0));
					}
					return null;
				});

		this.tradeOpenCloseTypeCache.put(TradeBuilder.BUY.getName(), TradeBuilder.BUY);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.BUY_TO_CLOSE.getName(), TradeBuilder.BUY_TO_CLOSE);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.BUY_TO_OPEN.getName(), TradeBuilder.BUY_TO_OPEN);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.SELL.getName(), TradeBuilder.SELL);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.SELL_TO_CLOSE.getName(), TradeBuilder.SELL_TO_CLOSE);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.SELL_TO_OPEN.getName(), TradeBuilder.SELL_TO_OPEN);
	}


	@Test
	public void testLongPositionSplitting() {
		Trade futureTrade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("PTH0")
				.withId(1)
				.build())
				.withId(2208923)
				.sell()
				.withQuantityIntended("8.0000000000")
				.withTradeType(TradeTypeBuilder.createFutures().toTradeType())
				.addFill("8.0000000000", "1056.050000000000000")
				.createFills();

		TradeFill originalTradeFill = new TradeFill();
		BeanUtils.copyProperties(futureTrade.getTradeFillList().get(0), originalTradeFill);

		final AtomicInteger sequence = new AtomicInteger(1);
		CollectionUtils.getStream(futureTrade.getTradeFillList()).forEach(ft -> ft.setId(sequence.getAndIncrement()));

		Mockito.doReturn(futureTrade).when(this.tradeService).getTrade(futureTrade.getId());

		AccountingAccount positionAccount = AccountingAccountBuilder.newAsset("Position")
				.asPosition()
				.build();

		final AccountingJournal journal = new AccountingJournal();
		journal.setId((long) 4760106);
		journal.setJournalType(createAccountingJournalType());
		journal.setJournalStatus(createAccountingJournalStatus());
		journal.setPostingDate(DateUtils.toDate("2020-02-04 13:26:45.807", DateUtils.DATE_FORMAT_FULL_PRECISE));
		journal.setDescription("BUY 7 Contracts of PTH0");
		journal.setSystemTable(this.tradeFillSystemTable.get());
		journal.setFkFieldId(2391657);

		AccountingTransaction accountingPosition = AccountingTransactionBuilder.newTransactionForIdAndSecurity(16882164, futureTrade.getInvestmentSecurity())
				.id(16882164)
				.on(DateUtils.toDate("02/04/2020"))
				.settlementDate("02/04/2020")
				.accountingAccount(positionAccount)
				.journal(journal)
				.clientInvestmentAccount(InvestmentAccountBuilder.createEmptyInvestmentAccount()
						.withId(13019)
						.toInvestmentAccount()
				)
				.holdingInvestmentAccount(InvestmentAccountBuilder.createEmptyInvestmentAccount()
						.withId(13023)
						.toInvestmentAccount()
				)
				.security(futureTrade.getInvestmentSecurity())
				.accountingAccount(positionAccount)
				.price("1045.600000000000000")
				.qty("5.0000000000")
				.costBasis("1045600.00")
				.baseDebitCredit(BigDecimal.ZERO)
				.localDebitCredit(BigDecimal.ZERO)
				.build();
		accountingPosition.setSystemTable(this.tradeFillSystemTable.get());
		accountingPosition.setFkFieldId(2391657);
		accountingPosition.setOpening(true);

		Mockito.when(this.accountingPositionService.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(
				Collections.singletonList(AccountingPosition.forOpeningTransaction(accountingPosition))
		);

		Mockito.when(this.investmentInstrumentService.getInvestmentSecurity(ArgumentMatchers.anyInt())).thenReturn(futureTrade.getInvestmentSecurity());

		ArgumentCaptor<TradeFill> tradeFillCapture = ArgumentCaptor.forClass(TradeFill.class);
		Trade results = this.tradeFillSplittingWorkflowAction.processAction(futureTrade, new WorkflowTransition());
		Mockito.verify(this.tradeFillDAO, Mockito.times(2)).save(tradeFillCapture.capture());
		List<TradeFill> savedTradeFills = tradeFillCapture.getAllValues();

		Assertions.assertEquals(2, results.getTradeFillList().size());
		Assertions.assertEquals(0, (int) results.getTradeFillList().stream().map(TradeFill::getOpenCloseType).filter(Objects::isNull).count());
		Assertions.assertEquals(1, (int) results.getTradeFillList().stream()
				.filter(tf -> tf.getOpenCloseType() != null)
				.filter(tf -> tf.getOpenCloseType().isExplicitOpenClose())
				.filter(tf -> tf.getOpenCloseType().isOpen())
				.count());
		Assertions.assertEquals(1, (int) results.getTradeFillList().stream().filter(tf -> tf.getOpenCloseType() != null).filter(tf -> tf.getOpenCloseType().isClose()).count());

		Assertions.assertEquals(2, savedTradeFills.size());
		Assertions.assertEquals(0, (int) savedTradeFills.stream().map(TradeFill::getOpenCloseType).filter(Objects::isNull).count());
		Assertions.assertEquals(1, (int) savedTradeFills.stream()
				.filter(tf -> tf.getOpenCloseType() != null)
				.filter(tf -> tf.getOpenCloseType().isExplicitOpenClose())
				.filter(tf -> tf.getOpenCloseType().isOpen())
				.count());
		Assertions.assertEquals(1, (int) savedTradeFills.stream().filter(tf -> tf.getOpenCloseType() != null).filter(tf -> tf.getOpenCloseType().isClose()).count());

		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getQuantity(), MathUtils.add(savedTradeFills.get(0).getQuantity(), savedTradeFills.get(1).getQuantity())));
		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getNotionalTotalPrice(), MathUtils.add(savedTradeFills.get(0).getNotionalTotalPrice(), savedTradeFills.get(1).getNotionalTotalPrice())));
		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getPaymentTotalPrice(), MathUtils.add(savedTradeFills.get(0).getPaymentTotalPrice(), savedTradeFills.get(1).getPaymentTotalPrice())));

		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getPaymentUnitPrice(), savedTradeFills.get(0).getPaymentUnitPrice()));
		Assertions.assertTrue(MathUtils.isEqual(savedTradeFills.get(0).getPaymentUnitPrice(), savedTradeFills.get(1).getPaymentUnitPrice()));

		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getNotionalUnitPrice(), savedTradeFills.get(0).getNotionalUnitPrice()));
		Assertions.assertTrue(MathUtils.isEqual(savedTradeFills.get(0).getNotionalUnitPrice(), savedTradeFills.get(1).getNotionalUnitPrice()));
	}


	@Test
	public void testShortPositionSplitting() {
		Trade futureTrade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("PTH0")
				.withId(1)
				.build())
				.withId(1)
				.buy()
				.withQuantityIntended("8.0000000000")
				.withTradeType(TradeTypeBuilder.createFutures().toTradeType())
				.addFill("8.0000000000", "1056.050000000000000")
				.createFills();

		TradeFill originalTradeFill = new TradeFill();
		BeanUtils.copyProperties(futureTrade.getTradeFillList().get(0), originalTradeFill);

		final AtomicInteger sequence = new AtomicInteger(1);
		CollectionUtils.getStream(futureTrade.getTradeFillList()).forEach(ft -> ft.setId(sequence.getAndIncrement()));

		Mockito.doReturn(futureTrade).when(this.tradeService).getTrade(futureTrade.getId());

		AccountingAccount positionAccount = AccountingAccountBuilder.newAsset("Position")
				.asPosition()
				.build();

		final AccountingJournal journal = new AccountingJournal();
		journal.setId((long) 4670980);
		journal.setJournalType(createAccountingJournalType());
		journal.setJournalStatus(createAccountingJournalStatus());
		journal.setPostingDate(DateUtils.toDate("2019-12-16 10:55:47.550", DateUtils.DATE_FORMAT_FULL_PRECISE));
		journal.setDescription("BUY 7 Contracts of PTH0");
		journal.setSystemTable(this.tradeFillSystemTable.get());
		journal.setFkFieldId(2353226);

		AccountingTransaction accountingPosition = AccountingTransactionBuilder.newTransactionForIdAndSecurity(4, futureTrade.getInvestmentSecurity())
				.id(16372609)
				.on(DateUtils.toDate("12/16/2019"))
				.settlementDate("12/16/2019")
				.accountingAccount(positionAccount)
				.journal(journal)
				.clientInvestmentAccount(InvestmentAccountBuilder.createEmptyInvestmentAccount()
						.withId(241)
						.toInvestmentAccount()
				)
				.holdingInvestmentAccount(InvestmentAccountBuilder.createEmptyInvestmentAccount()
						.withId(4737)
						.toInvestmentAccount()
				)
				.security(futureTrade.getInvestmentSecurity())
				.accountingAccount(positionAccount)
				.price("1011.974743600000000")
				.qty("-7.0000000000")
				.costBasis("-1416764.64")
				.baseDebitCredit(BigDecimal.ZERO)
				.localDebitCredit(BigDecimal.ZERO)
				.build();
		accountingPosition.setSystemTable(this.tradeFillSystemTable.get());
		accountingPosition.setFkFieldId(2353226);
		accountingPosition.setOpening(true);

		Mockito.when(this.accountingPositionService.getAccountingPositionListUsingCommand(ArgumentMatchers.any(AccountingPositionCommand.class))).thenReturn(
				Collections.singletonList(AccountingPosition.forOpeningTransaction(accountingPosition))
		);

		Mockito.when(this.investmentInstrumentService.getInvestmentSecurity(ArgumentMatchers.anyInt())).thenReturn(futureTrade.getInvestmentSecurity());

		ArgumentCaptor<TradeFill> tradeFillCapture = ArgumentCaptor.forClass(TradeFill.class);
		Trade results = this.tradeFillSplittingWorkflowAction.processAction(futureTrade, new WorkflowTransition());
		Mockito.verify(this.tradeFillDAO, Mockito.times(2)).save(tradeFillCapture.capture());
		List<TradeFill> savedTradeFills = tradeFillCapture.getAllValues();

		Assertions.assertEquals(2, results.getTradeFillList().size());
		Assertions.assertEquals(0, (int) results.getTradeFillList().stream().map(TradeFill::getOpenCloseType).filter(Objects::isNull).count());
		Assertions.assertEquals(1, (int) results.getTradeFillList().stream()
				.filter(tf -> tf.getOpenCloseType() != null)
				.filter(tf -> tf.getOpenCloseType().isExplicitOpenClose())
				.filter(tf -> tf.getOpenCloseType().isOpen())
				.count());
		Assertions.assertEquals(1, (int) results.getTradeFillList().stream().filter(tf -> tf.getOpenCloseType() != null).filter(tf -> tf.getOpenCloseType().isClose()).count());

		Assertions.assertEquals(2, savedTradeFills.size());
		Assertions.assertEquals(0, (int) savedTradeFills.stream().map(TradeFill::getOpenCloseType).filter(Objects::isNull).count());
		Assertions.assertEquals(1, (int) savedTradeFills.stream()
				.filter(tf -> tf.getOpenCloseType() != null)
				.filter(tf -> tf.getOpenCloseType().isExplicitOpenClose())
				.filter(tf -> tf.getOpenCloseType().isOpen())
				.count());
		Assertions.assertEquals(1, (int) savedTradeFills.stream().filter(tf -> tf.getOpenCloseType() != null).filter(tf -> tf.getOpenCloseType().isClose()).count());

		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getQuantity(), MathUtils.add(savedTradeFills.get(0).getQuantity(), savedTradeFills.get(1).getQuantity())));
		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getNotionalTotalPrice(), MathUtils.add(savedTradeFills.get(0).getNotionalTotalPrice(), savedTradeFills.get(1).getNotionalTotalPrice())));
		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getPaymentTotalPrice(), MathUtils.add(savedTradeFills.get(0).getPaymentTotalPrice(), savedTradeFills.get(1).getPaymentTotalPrice())));

		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getPaymentUnitPrice(), savedTradeFills.get(0).getPaymentUnitPrice()));
		Assertions.assertTrue(MathUtils.isEqual(savedTradeFills.get(0).getPaymentUnitPrice(), savedTradeFills.get(1).getPaymentUnitPrice()));

		Assertions.assertTrue(MathUtils.isEqual(originalTradeFill.getNotionalUnitPrice(), savedTradeFills.get(0).getNotionalUnitPrice()));
		Assertions.assertTrue(MathUtils.isEqual(savedTradeFills.get(0).getNotionalUnitPrice(), savedTradeFills.get(1).getNotionalUnitPrice()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingJournalType createAccountingJournalType() {
		AccountingJournalType accountingJournalType = new AccountingJournalType();
		accountingJournalType.setId((short) 10);
		accountingJournalType.setName("Trade Journal");
		accountingJournalType.setSystemTable(this.tradeFillSystemTable.get());
		accountingJournalType.setTransactionTable(this.tradeFillSystemTable.get());
		accountingJournalType.setSubsystemJournal(true);
		return accountingJournalType;
	}


	private AccountingJournalStatus createAccountingJournalStatus() {
		AccountingJournalStatus status = new AccountingJournalStatus();
		status.setId((short) 2);
		status.setName("Original");
		status.setDescription("When a journal is Posted for the first time for an entity (99%+ of all journals)");
		status.setPosted(true);
		return status;
	}
}
