package com.clifton.trade.workflow;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.TradeService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


@TradeInMemoryDatabaseContext
public class TradeBookingWorkflowActionDatabaseTests extends BaseInMemoryDatabaseTests {

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	@Resource
	private TradeService tradeService;

	@Resource
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////


	@Override
	protected IdentityObject getUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 1);
		user.setUserName("TestUser");
		return user;
	}


	@Override
	public void beforeTest() {
		// save user for foreign key constraint on Trade and TradeGroup
		SecurityUser userTemplate = (SecurityUser) getUser();
		SecurityUser user = this.securityUserService.getSecurityUser(userTemplate.getId());
		if (user == null) {
			this.securityUserService.saveSecurityUser(userTemplate);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCDSTradeWorkflowTransitionAction() {
		Trade trade = transitionIfPresent("Book and Post Trade", "Trade", 746918);
		ValidationUtils.assertNotNull(trade, "Failed to find trade with ID: " + 746918);
		Assertions.assertTrue(trade.getOpenCloseType().isExplicitOpenClose());
		Assertions.assertEquals("Buy to Open", trade.getOpenCloseType().getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Trade transitionIfPresent(String transitionName, String tableName, Integer id) {
		List<WorkflowTransition> transitions = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(tableName, MathUtils.getNumberAsLong(id));
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitions)) {
			if (transition.getName().equalsIgnoreCase(transitionName)) {
				return (Trade) this.workflowTransitionService.executeWorkflowTransitionByTransition(tableName, id, transition.getId());
			}
		}
		return this.tradeService.getTrade(id);
	}
}
