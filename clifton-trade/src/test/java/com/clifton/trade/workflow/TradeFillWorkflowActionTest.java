package com.clifton.trade.workflow;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.util.TradeFillUtilHandlerImpl;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;


public class TradeFillWorkflowActionTest {

	/**
	 * Tests the TradeFillWorkflowAction to verify the payment total price calculation rounds correctly for JPY.
	 */
	@Test
	public void testTradeFillWorkflowAction_JPYRounding() {
		final Trade trade = TradeTestObjectFactory.newTrade_Currency(true, InvestmentSecurityBuilder.newCAD(), new BigDecimal("6500000"), new BigDecimal("0.0119199"),
				DateUtils.toDate("05/08/2012"), InvestmentSecurityBuilder.newJPY());
		trade.setId(10);
		trade.setTradeFillList(null);

		TradeService tradeService = Mockito.mock(TradeService.class);
		Mockito.when(tradeService.getTrade(ArgumentMatchers.anyInt())).thenReturn(trade);
		Mockito.when(tradeService.saveTradeFill(ArgumentMatchers.any(TradeFill.class))).then(invocationOnMock -> {
			Object[] args = invocationOnMock.getArguments();
			TradeFill fill = (TradeFill) args[0];
			trade.setTradeFillList(CollectionUtils.createList(fill));
			return fill;
		});

		InvestmentCurrencyService investmentCurrencyService = Mockito.mock(InvestmentCurrencyService.class);
		Mockito.when(investmentCurrencyService.isInvestmentCurrencyMultiplyConvention(ArgumentMatchers.eq("CAD"), ArgumentMatchers.eq("JPY"))).thenReturn(false);
		Mockito.when(investmentCurrencyService.isInvestmentCurrencyMultiplyConvention(ArgumentMatchers.eq("JPY"), ArgumentMatchers.eq("CAD"))).thenReturn(true);

		TradeFillUtilHandlerImpl tradeFillUtilHandler = new TradeFillUtilHandlerImpl();
		tradeFillUtilHandler.setTradeService(tradeService);
		tradeFillUtilHandler.setInvestmentCurrencyService(investmentCurrencyService);

		TradeFillWorkflowAction action = new TradeFillWorkflowAction();
		action.setTradeService(tradeService);
		action.setTradeFillUtilHandler(tradeFillUtilHandler);
		action.processAction(trade, null);

		//make sure in order
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("545306588"), trade.getTradeFillList().get(0).getPaymentTotalPrice()));
	}
}
