package com.clifton.trade.workflow.splitting;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeServiceImpl;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.builder.TradeFillBuilder;
import com.clifton.trade.builder.TradeTypeBuilder;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowStatusBuilder;
import com.clifton.workflow.transition.WorkflowTransition;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(MockitoExtension.class)
class TradeFillUnSplittingWorkflowActionTest {

	@Spy
	private final TradeService tradeService = new TradeServiceImpl();

	@Mock
	private AdvancedUpdatableDAO<TradeFill, Criteria> tradeFillDAO;

	@Spy
	private final DaoNamedEntityCache<TradeOpenCloseType> tradeOpenCloseTypeCache = new DaoNamedEntityCache<TradeOpenCloseType>() {

		private final Map<String, TradeOpenCloseType> cache = new HashMap<>();


		@Override
		public void put(String key, TradeOpenCloseType bean) {
			this.cache.put(key, bean);
		}


		@Override
		public TradeOpenCloseType getBeanForKeyValue(ReadOnlyDAO<TradeOpenCloseType> dao, String value) {
			return this.cache.get(value);
		}


		@Override
		public TradeOpenCloseType getBeanForKeyValueStrict(ReadOnlyDAO<TradeOpenCloseType> dao, String value) {
			return this.cache.get(value);
		}
	};

	@Spy
	@SuppressWarnings("unused")
	private final InvestmentCalculator investmentCalculator = new InvestmentCalculatorImpl();

	@InjectMocks
	private TradeFillUnSplittingWorkflowAction tradeFillUnSplittingWorkflowAction;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void beforeEach() {
		((TradeServiceImpl) this.tradeService).setTradeFillDAO(this.tradeFillDAO);
		((TradeServiceImpl) this.tradeService).setTradeOpenCloseTypeCache(this.tradeOpenCloseTypeCache);

		this.tradeOpenCloseTypeCache.put(TradeBuilder.BUY.getName(), TradeBuilder.BUY);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.BUY_TO_CLOSE.getName(), TradeBuilder.BUY_TO_CLOSE);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.BUY_TO_OPEN.getName(), TradeBuilder.BUY_TO_OPEN);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.SELL.getName(), TradeBuilder.SELL);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.SELL_TO_CLOSE.getName(), TradeBuilder.SELL_TO_CLOSE);
		this.tradeOpenCloseTypeCache.put(TradeBuilder.SELL_TO_OPEN.getName(), TradeBuilder.SELL_TO_OPEN);
	}


	@Test
	public void testMerging() {
		Trade futureTrade = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("PTH0")
				.build())
				.withId(2208923)
				.sell()
				.withTradeType(TradeTypeBuilder.createFutures().toTradeType())
				.addFill(TradeFillBuilder.aTradeFill()
						.withId(1)
						.withNotionalUnitPrice("1056.050000000000000")
						.withQuantity("4.0000000000")
						.withOpenCloseType(TradeBuilder.SELL_TO_OPEN)
						.build()
				)
				.addFill(TradeFillBuilder.aTradeFill()
						.withId(2)
						.withNotionalUnitPrice("1056.050000000000000")
						.withQuantity("4.0000000000")
						.withOpenCloseType(TradeBuilder.SELL_TO_CLOSE)
						.build())
				.createFills();

		final List<TradeFill> tradeFillList = new ArrayList<>(futureTrade.getTradeFillList());

		futureTrade.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());

		Mockito.doAnswer(i -> {
			Integer fillId = i.getArgument(0);
			return tradeFillList.stream()
					.filter(f -> MathUtils.isEqual(f.getId(), fillId))
					.findFirst()
					.orElse(null);
		}).when(this.tradeService).getTradeFill(ArgumentMatchers.anyInt());
		Mockito.doReturn(futureTrade).when(this.tradeService).getTrade(futureTrade.getId());

		ArgumentCaptor<TradeFill> deleteTradeFillCapture = ArgumentCaptor.forClass(TradeFill.class);
		Trade results = this.tradeFillUnSplittingWorkflowAction.processAction(futureTrade, new WorkflowTransition());
		Mockito.verify(this.tradeFillDAO, Mockito.times(1)).delete(deleteTradeFillCapture.capture());
		List<TradeFill> deletedTradeFills = deleteTradeFillCapture.getAllValues();
		Assertions.assertEquals(1, deletedTradeFills.size());
		Assertions.assertEquals(1, results.getTradeFillList().size());

		ArgumentCaptor<TradeFill> saveTradeFillCapture = ArgumentCaptor.forClass(TradeFill.class);
		Mockito.verify(this.tradeFillDAO, Mockito.times(1)).save(saveTradeFillCapture.capture());
		TradeFill savedTradeFill = saveTradeFillCapture.getValue();
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("8"), savedTradeFill.getQuantity()));
		Assertions.assertTrue(MathUtils.isEqual(results.getTradeFillList().get(0).getQuantity(), futureTrade.getQuantityIntended()));
	}
}
