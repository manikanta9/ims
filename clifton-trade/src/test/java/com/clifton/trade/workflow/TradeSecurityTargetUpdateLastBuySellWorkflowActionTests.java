package com.clifton.trade.workflow;


import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;


/**
 * A  test suite to test the proper operation of the TradeSecurityTargetUpdateLastBuySellWorkflowAction class.  The class under test
 * updates the Last Buy / Last Sell Trade Date on the security target when trades are booked or unbooked.
 */
public class TradeSecurityTargetUpdateLastBuySellWorkflowActionTests {

	private static final Date DEFAULT_TRADE_DATE = DateUtils.toDate("1/16/2019");
	private static final Date DEFAULT_LAST_BUY_TRADE_DATE = DateUtils.toDate("12/28/2018");
	private static final Date DEFAULT_LAST_SELL_TRADE_DATE = DateUtils.toDate("12/21/2018");

	private boolean initialized = false;

	private InvestmentAccount investmentAccount;
	private InvestmentAccountSecurityTarget securityTarget;
	private InvestmentAccountSecurityTarget savedSecurityTarget;
	private List<Trade> previousBuyTrades;
	private List<Trade> previousSellTrades;
	private Trade testTrade;

	private BusinessServiceProcessingType callWritingProcessingType;
	private BusinessServiceProcessingType ldiProcessingType;
	private BusinessServiceProcessingType oarsProcessingType;

	@Mock
	InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	@Mock
	TradeService tradeService;

	@InjectMocks
	TradeSecurityTargetUpdateLastBuySellWorkflowAction workflowAction = new TradeSecurityTargetUpdateLastBuySellWorkflowAction();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		if (!this.initialized) {

			this.callWritingProcessingType = new BusinessServiceProcessingType();
			this.callWritingProcessingType.setId((short) 7);
			this.callWritingProcessingType.setName("Call Writing");
			this.callWritingProcessingType.setProcessingType(ServiceProcessingTypes.SECURITY_TARGET);

			this.ldiProcessingType = new BusinessServiceProcessingType();
			this.ldiProcessingType.setId((short) 3);
			this.ldiProcessingType.setName("LDI");
			this.ldiProcessingType.setProcessingType(ServiceProcessingTypes.LDI);

			this.oarsProcessingType = new BusinessServiceProcessingType();
			this.oarsProcessingType.setId((short) 11);
			this.oarsProcessingType.setName("OARS");
			this.oarsProcessingType.setProcessingType(ServiceProcessingTypes.SECURITY_TARGET);

			this.investmentAccount = new InvestmentAccount();
			this.investmentAccount.setId(1);
			this.investmentAccount.setNumber("0000-0001");
			this.investmentAccount.setServiceProcessingType(this.callWritingProcessingType);

			InvestmentSecurity underlyingSecurity = new InvestmentSecurity();
			underlyingSecurity.setId(2000);
			underlyingSecurity.setSymbol("IBM");
			InvestmentSecurity investmentSecurity = new InvestmentSecurity();
			investmentSecurity.setId(14000);
			investmentSecurity.setUnderlyingSecurity(underlyingSecurity);
			investmentSecurity.setSymbol("IBM US 12/31/2200 C4000");
			investmentSecurity.setInstrument(new InvestmentInstrument());
			investmentSecurity.getInstrument().setHierarchy(new InvestmentInstrumentHierarchy());
			investmentSecurity.getInstrument().getHierarchy().setInvestmentType(new InvestmentType());
			investmentSecurity.getInstrument().getHierarchy().getInvestmentType().setName(InvestmentType.OPTIONS);

			MockitoAnnotations.initMocks(this);
			this.securityTarget = new InvestmentAccountSecurityTarget();
			this.securityTarget.setTargetUnderlyingSecurity(underlyingSecurity);

			this.testTrade = new Trade();
			this.testTrade.setWorkflowState(new WorkflowState());
			this.testTrade.getWorkflowState().setName(TradeService.TRADE_BOOKED_STATE_NAME);
			this.testTrade.setClientInvestmentAccount(getInvestmentAccount());
			this.testTrade.setInvestmentSecurity(investmentSecurity);
			this.testTrade.setQuantityIntended(BigDecimal.ONE);
			this.testTrade.setTradeDate(DEFAULT_TRADE_DATE);
			this.testTrade.setBuy(false);

			this.workflowAction.setBusinessServiceProcessingTypeIds(new ArrayList<>());

			Answer<InvestmentAccountSecurityTarget> securityTargetSaveAnswer = invocation -> {
				setSavedSecurityTarget(invocation.getArgument(0));
				return getSavedSecurityTarget();
			};

			Mockito.when(this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(Mockito.anyInt(), Mockito.anyInt(), any())).thenAnswer(invocation -> getSecurityTarget());
			Mockito.when(this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(any(InvestmentAccountSecurityTarget.class))).thenAnswer(securityTargetSaveAnswer);

			this.initialized = true;
		}

		this.securityTarget.setLastSellTradeDate(DEFAULT_LAST_SELL_TRADE_DATE);
		this.securityTarget.setLastBuyTradeDate(DEFAULT_LAST_BUY_TRADE_DATE);
		this.savedSecurityTarget = null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testProcessAction_bookTrade_buy() {
		Trade testTrade = getTestTrade();
		testTrade.setBuy(true);
		TradeSecurityTargetUpdateLastBuySellWorkflowAction testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(true);

		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		Assertions.assertEquals(DEFAULT_TRADE_DATE, getSavedSecurityTarget().getLastBuyTradeDate());
		Assertions.assertEquals(DEFAULT_LAST_SELL_TRADE_DATE, getSavedSecurityTarget().getLastSellTradeDate());
	}


	@Test
	public void testProcessAction_unbookTrade_buy() {

		// Book trade first
		Trade testTrade = getTestTrade();
		testTrade.setBuy(true);
		TradeSecurityTargetUpdateLastBuySellWorkflowAction testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(true);
		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		Assertions.assertEquals(DEFAULT_TRADE_DATE, getSavedSecurityTarget().getLastBuyTradeDate());
		Assertions.assertEquals(DEFAULT_LAST_SELL_TRADE_DATE, getSavedSecurityTarget().getLastSellTradeDate());

		// Unbook trade
		resetPreviousTrades(true);
		testTrade = getTestTrade();
		testTrade.setBuy(true);
		testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(false);
		testTrade.getWorkflowState().setName(TradeService.TRADE_UNBOOKED_STATE_NAME);

		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		Assertions.assertEquals(DEFAULT_LAST_BUY_TRADE_DATE, getSavedSecurityTarget().getLastBuyTradeDate());
		Assertions.assertEquals(DEFAULT_LAST_SELL_TRADE_DATE, getSavedSecurityTarget().getLastSellTradeDate());
	}


	@Test
	public void testProcessAction_bookTrade_sell() {
		Trade testTrade = getTestTrade();
		TradeSecurityTargetUpdateLastBuySellWorkflowAction testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(true);

		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		Assertions.assertEquals(DEFAULT_LAST_BUY_TRADE_DATE, getSavedSecurityTarget().getLastBuyTradeDate());
		Assertions.assertEquals(DEFAULT_TRADE_DATE, getSavedSecurityTarget().getLastSellTradeDate());
	}


	@Test
	public void testProcessAction_unbookTrade_sell() {

		// Book trade first
		Trade testTrade = getTestTrade();
		testTrade.setBuy(false);
		TradeSecurityTargetUpdateLastBuySellWorkflowAction testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(true);
		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		Assertions.assertEquals(DEFAULT_LAST_BUY_TRADE_DATE, getSavedSecurityTarget().getLastBuyTradeDate());
		Assertions.assertEquals(DEFAULT_TRADE_DATE, getSavedSecurityTarget().getLastSellTradeDate());

		// Unbook trade
		resetPreviousTrades(false);
		testTrade = getTestTrade();
		testTrade.setBuy(false);
		testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(false);
		testTrade.getWorkflowState().setName(TradeService.TRADE_UNBOOKED_STATE_NAME);

		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		Assertions.assertEquals(DEFAULT_LAST_BUY_TRADE_DATE, getSavedSecurityTarget().getLastBuyTradeDate());
		Assertions.assertEquals(DEFAULT_LAST_SELL_TRADE_DATE, getSavedSecurityTarget().getLastSellTradeDate());
	}


	@Test
	public void testProcessAction_bookTrade_sell_account_has_wrong_processing_type() {
		Trade testTrade = getTestTrade();
		testTrade.getClientInvestmentAccount().setServiceProcessingType(getLdiProcessingType());
		testTrade.setBuy(false);
		TradeSecurityTargetUpdateLastBuySellWorkflowAction testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(true);

		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		// Nothing should have been saved due to target not having a processing type of type SECURITY_TARGET
		Assertions.assertNull(getSavedSecurityTarget());
	}


	@Test
	public void testProcessAction_bookTrade_sell_account_has_non_matching_processing_type() {
		Trade testTrade = getTestTrade();
		testTrade.getClientInvestmentAccount().setServiceProcessingType(getOarsProcessingType());
		testTrade.setBuy(false);
		TradeSecurityTargetUpdateLastBuySellWorkflowAction testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(true);

		// Service processing type for ID 7 is Call Writing
		testWorkFlowAction.setBusinessServiceProcessingTypeIds(CollectionUtils.createList((short) 7));

		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		// Nothing should have been saved due to target not having a BusinessServiceProcessingType that does not match the configured type.
		Assertions.assertNull(getSavedSecurityTarget());
	}


	@Test
	public void testProcessAction_bookTrade_sell_account_has_matching_processing_type() {
		Trade testTrade = getTestTrade();
		testTrade.getClientInvestmentAccount().setServiceProcessingType(getOarsProcessingType());
		testTrade.setBuy(false);
		TradeSecurityTargetUpdateLastBuySellWorkflowAction testWorkFlowAction = getWorkflowAction();
		testWorkFlowAction.setBooking(true);
		// Service processing type for ID 7 is Call Writing, ID 11 is OARS
		testWorkFlowAction.setBusinessServiceProcessingTypeIds(CollectionUtils.createList((short) 7, (short) 11));

		testWorkFlowAction.processAction(testTrade, new WorkflowTransition());

		// Should be unchanged due to target not having a BusinessServiceProcessingType that does not match the configured type.
		Assertions.assertEquals(DEFAULT_LAST_BUY_TRADE_DATE, getSavedSecurityTarget().getLastBuyTradeDate());
		Assertions.assertEquals(DEFAULT_TRADE_DATE, getSavedSecurityTarget().getLastSellTradeDate());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void resetPreviousTrades(boolean isBuy) {
		Trade previousBuyTrade1 = new Trade();
		Trade previousBuyTrade2 = new Trade();
		previousBuyTrade1.setTradeDate(DEFAULT_LAST_BUY_TRADE_DATE);
		previousBuyTrade1.setBuy(true);
		previousBuyTrade2.setTradeDate(DateUtils.toDate("11/16/2018"));
		previousBuyTrade2.setBuy(true);
		this.previousBuyTrades = CollectionUtils.createList(previousBuyTrade1, previousBuyTrade2);
		Trade previousSellTrade1 = new Trade();
		Trade previousSellTrade2 = new Trade();
		previousSellTrade1.setTradeDate(DEFAULT_LAST_SELL_TRADE_DATE);
		previousSellTrade1.setBuy(false);
		previousSellTrade2.setTradeDate(DateUtils.toDate("12/14/2018"));
		previousSellTrade2.setBuy(false);
		this.previousSellTrades = CollectionUtils.createList(previousSellTrade1, previousSellTrade2);

		Mockito.when(this.tradeService.getTradeList(any(TradeSearchForm.class))).thenReturn(isBuy ? getPreviousBuyTrades() : getPreviousSellTrades());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public InvestmentAccountSecurityTarget getSecurityTarget() {
		return this.securityTarget;
	}


	public void setSecurityTarget(InvestmentAccountSecurityTarget securityTarget) {
		this.securityTarget = securityTarget;
	}


	public InvestmentAccountSecurityTarget getSavedSecurityTarget() {
		return this.savedSecurityTarget;
	}


	public void setSavedSecurityTarget(InvestmentAccountSecurityTarget savedSecurityTarget) {
		this.savedSecurityTarget = savedSecurityTarget;
	}


	public List<Trade> getPreviousBuyTrades() {
		return this.previousBuyTrades;
	}


	public void setPreviousBuyTrades(List<Trade> previousBuyTrades) {
		this.previousBuyTrades = previousBuyTrades;
	}


	public List<Trade> getPreviousSellTrades() {
		return this.previousSellTrades;
	}


	public void setPreviousSellTrades(List<Trade> previousSellTrades) {
		this.previousSellTrades = previousSellTrades;
	}


	public Trade getTestTrade() {
		return this.testTrade;
	}


	public TradeSecurityTargetUpdateLastBuySellWorkflowAction getWorkflowAction() {
		return this.workflowAction;
	}


	public BusinessServiceProcessingType getCallWritingProcessingType() {
		return this.callWritingProcessingType;
	}


	public BusinessServiceProcessingType getLdiProcessingType() {
		return this.ldiProcessingType;
	}


	public BusinessServiceProcessingType getOarsProcessingType() {
		return this.oarsProcessingType;
	}
}
