package com.clifton.trade.accounting.booking.rule;

import com.clifton.accounting.gl.booking.AccountingBookingRuleTestExecutor;
import com.clifton.accounting.gl.booking.rule.AccountingAggregateCurrencyBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRealizedGainLossBookingRule;
import com.clifton.accounting.gl.booking.rule.AccountingRemoveZeroValueRecordBookingRule;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeTestObjectFactory;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Test;


class TradeAccrualBookingRuleTest {

	@Test
	public void testAccrualBookingRule_NoActualSettlementDate() {
		InvestmentSecurity swap = InvestmentSecurityBuilder.ofType(156150, "810RI61106017", InvestmentType.SWAPS)
				.build();
		swap.getInstrument().getHierarchy().setIncludeAccrualReceivables(true);
		Trade trade = TradeBuilder.newTradeForSecurity(swap)
				.withId(890)
				.buy()
				.withAccrualAmount2("1736.11")
				.withExpectedUnitPrice("98.57")
				.addFillWithQuantity(2_500_000)
				.createFills();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						new AccountingRealizedGainLossBookingRule<>(),
						new AccountingAggregateCurrencyBookingRule<>(),
						TradeTestObjectFactory.newTradeAccrualBookingRule(),
						new AccountingRemoveZeroValueRecordBookingRule<>()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	810RI61106017	98.57	2,500,000	246,425,000	10/17/2012	1	246,425,000	246,425,000	10/17/2012	10/17/2012	null",
						"-12	-10	100000	H-200	Interest Leg	810RI61106017			1,736.11	10/17/2012	1	1,736.11	0	10/17/2012	10/17/2012	Accrued Interest Leg for 810RI61106017",
						"-13	-10	100000	H-200	Payment Receivable	USD			-246,426,736.11	10/17/2012	1	-246,426,736.11	0	10/17/2012	10/17/2012	Accrual Cash expense from opening of 810RI61106017"
				)
				.execute();
	}


	@Test
	public void testAccrualBookingRule_WithActualSettlementDate() {
		InvestmentSecurity swap = InvestmentSecurityBuilder.ofType(156150, "810RI61106017", InvestmentType.SWAPS)
				.build();
		swap.getInstrument().getHierarchy().setIncludeAccrualReceivables(true);
		Trade trade = TradeBuilder.newTradeForSecurity(swap)
				.withId(890)
				.buy()
				.withAccrualAmount2("1736.11")
				.withExpectedUnitPrice("98.57")
				.addFillWithQuantity(2_500_000)
				.withActualSettlementDate(DateUtils.toDate("10/18/2012"))
				.createFills();

		AccountingBookingRuleTestExecutor.newTestForEntity(trade.getTradeFillList().get(0))
				.forBookingRules(
						TradeTestObjectFactory.newTradeCreatePositionBookingRule(),
						TradeTestObjectFactory.newTradeAccruedInterestBookingRule(),
						new AccountingRealizedGainLossBookingRule<>(),
						new AccountingAggregateCurrencyBookingRule<>(),
						TradeTestObjectFactory.newTradeAccrualBookingRule(),
						new AccountingRemoveZeroValueRecordBookingRule<>()
				)
				.withExpectedResults(
						"-10	null	100000	H-200	Position	810RI61106017	98.57	2,500,000	246,425,000	10/17/2012	1	246,425,000	246,425,000	10/17/2012	10/17/2012	null",
						"-12	-10	100000	H-200	Interest Leg	810RI61106017			1,736.11	10/17/2012	1	1,736.11	0	10/17/2012	10/17/2012	Accrued Interest Leg for 810RI61106017",
						// ID\tPID\tClient #\tHolding #\tGL Account\tSecurity\tPrice\tQty\tLocal Debit/Credit\tTran Date\tFX Rate\tBase Debit/Credit\tCost Basis\tOriginal Date\tSettlement Date\tDescription\
						"-13	-10	100000	H-200	Cash	USD			-246,426,736.11	10/18/2012	1	-246,426,736.11	0	10/17/2012	10/18/2012	Cash expense from opening of 810RI61106017"
				)
				.execute();
	}
}
