package com.clifton.trade.options.combination;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import org.hamcrest.BaseMatcher;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.clifton.trade.options.combination.TradeOptionLegTypes.LONG_CALL;
import static com.clifton.trade.options.combination.TradeOptionLegTypes.LONG_PUT;
import static com.clifton.trade.options.combination.TradeOptionLegTypes.SHORT_CALL;
import static com.clifton.trade.options.combination.TradeOptionLegTypes.SHORT_PUT;


/**
 * The test class for {@link TradeOptionCombinationHandlerImpl}.
 *
 * @author MikeH
 */
@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
public class TradeOptionCombinationHandlerImplTest {

	@InjectMocks
	private TradeOptionCombinationHandler tradeOptionCombinationHandler = new TradeOptionCombinationHandlerImpl();

	private final Date openDate = DateUtils.toDate("01/01/2018");
	private final Date expirationDate1 = DateUtils.toDate("02/01/2018");
	private final Date expirationDate2 = DateUtils.toDate("03/01/2018");
	private final InvestmentAccount clientInvestmentAccount = TestUtils.generateEntity(InvestmentAccount.class);
	private final InvestmentSecurity underlyingSecurity1 = TestUtils.generateEntity(InvestmentSecurity.class);
	private final InvestmentSecurity underlyingSecurity2 = TestUtils.generateEntity(InvestmentSecurity.class);
	private final InvestmentInstrumentHierarchy optionsHierarchy = TestUtils.generateEntity(InvestmentInstrumentHierarchy.class, Short.class);
	private final InvestmentInstrumentHierarchy stocksHierarchy = TestUtils.generateEntity(InvestmentInstrumentHierarchy.class, Short.class);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.underlyingSecurity1.setSymbol("SPX");
		this.underlyingSecurity2.setSymbol("IBM");
		this.optionsHierarchy.setInvestmentType(TestUtils.generateEntity(InvestmentType.class, Short.class));
		this.optionsHierarchy.getInvestmentType().setName(InvestmentType.OPTIONS);
		this.stocksHierarchy.setInvestmentType(TestUtils.generateEntity(InvestmentType.class, Short.class));
		this.stocksHierarchy.getInvestmentType().setName(InvestmentType.STOCKS);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Option Combination List Retrieval Tests         ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetTradeOptionCombinationListLongPut() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.LONG_PUT,
					CollectionUtils.combineCollections(
							generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
									Arrays.asList(LONG_PUT, LONG_PUT, LONG_PUT, LONG_PUT),
									1, 2, 3, 4),
							generatePositionList(this.underlyingSecurity2, 100, this.expirationDate1,
									Arrays.asList(LONG_PUT, LONG_PUT, LONG_PUT, LONG_PUT),
									1, 2, 3, 4)
					),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 2),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 3),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 4),
					createCombinationMatcher(this.underlyingSecurity2, TradeOptionCombinationTypes.LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity2, TradeOptionCombinationTypes.LONG_PUT, 10000, 2),
					createCombinationMatcher(this.underlyingSecurity2, TradeOptionCombinationTypes.LONG_PUT, 10000, 3),
					createCombinationMatcher(this.underlyingSecurity2, TradeOptionCombinationTypes.LONG_PUT, 10000, 4)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListLongPutExcess() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.LONG_PUT,
					generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
							Arrays.asList(LONG_PUT, LONG_PUT, LONG_PUT, LONG_PUT, SHORT_PUT),
							1, 2, 3, 4, 5),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 2),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 3),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.LONG_PUT, 10000, 4),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_PUT, 10000, 5)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondor() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
							Arrays.asList(LONG_PUT, SHORT_PUT, SHORT_CALL, LONG_CALL),
							1, 2, 3, 4),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.IRON_CONDOR, 10000, 1, 2, 3, 4)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorMultiple() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
							Arrays.asList(LONG_PUT, SHORT_PUT, SHORT_CALL, LONG_CALL, LONG_PUT, SHORT_PUT, SHORT_CALL, LONG_CALL),
							1, 2, 3, 4, 5, 6, 7, 8),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.IRON_CONDOR, 10000, 1, 2, 3, 4),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.IRON_CONDOR, 10000, 5, 6, 7, 8)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorMultipleStaggered() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
							Arrays.asList(LONG_PUT, LONG_PUT, SHORT_PUT, SHORT_PUT, SHORT_CALL, SHORT_CALL, LONG_CALL, LONG_CALL),
							1, 2, 3, 4, 5, 6, 7, 8),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.IRON_CONDOR, 10000, 1, 3, 5, 7),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.IRON_CONDOR, 10000, 2, 4, 6, 8)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorMissing() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
							Arrays.asList(LONG_PUT, SHORT_PUT),
							1, 2),
					createCombinationMatcher(this.underlyingSecurity1, LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_PUT, 10000, 2)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorWrongOrder() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
							Arrays.asList(LONG_PUT, SHORT_CALL, SHORT_PUT, LONG_CALL),
							1, 2, 3, 4),
					createCombinationMatcher(this.underlyingSecurity1, LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_CALL, 10000, 2),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_PUT, 10000, 3),
					createCombinationMatcher(this.underlyingSecurity1, LONG_CALL, 10000, 4)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorExcess() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
							Arrays.asList(LONG_PUT, SHORT_PUT, SHORT_CALL, LONG_CALL, LONG_PUT, SHORT_PUT),
							1, 2, 3, 4, 5, 6),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.IRON_CONDOR, 10000, 1, 2, 3, 4),
					createCombinationMatcher(this.underlyingSecurity1, LONG_PUT, 10000, 5),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_PUT, 10000, 6)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorMismatchedUnderlying() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					CollectionUtils.combineCollections(
							generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
									Arrays.asList(LONG_PUT, SHORT_PUT),
									1, 2),
							generatePositionList(this.underlyingSecurity2, 100, this.expirationDate1,
									Arrays.asList(SHORT_CALL, LONG_CALL),
									3, 4)
					),
					createCombinationMatcher(this.underlyingSecurity1, LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_PUT, 10000, 2),
					createCombinationMatcher(this.underlyingSecurity2, SHORT_CALL, 10000, 3),
					createCombinationMatcher(this.underlyingSecurity2, LONG_CALL, 10000, 4)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorMismatchedQuantity() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					CollectionUtils.combineCollections(
							generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
									Arrays.asList(LONG_PUT, SHORT_PUT),
									1, 2),
							generatePositionList(this.underlyingSecurity1, 200, this.expirationDate1,
									Arrays.asList(SHORT_CALL, LONG_CALL),
									3, 4)
					),
					createCombinationMatcher(this.underlyingSecurity1, LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_PUT, 10000, 2),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_CALL, 20000, 3),
					createCombinationMatcher(this.underlyingSecurity1, LONG_CALL, 20000, 4)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListIronCondorMismatchedExpiration() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					TradeOptionCombinationTypes.IRON_CONDOR,
					CollectionUtils.combineCollections(
							generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
									Arrays.asList(LONG_PUT, SHORT_PUT),
									1, 2),
							generatePositionList(this.underlyingSecurity1, 100, this.expirationDate2,
									Arrays.asList(SHORT_CALL, LONG_CALL),
									3, 4)
					),
					createCombinationMatcher(this.underlyingSecurity1, LONG_PUT, 10000, 1),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_PUT, 10000, 2),
					createCombinationMatcher(this.underlyingSecurity1, SHORT_CALL, 10000, 3),
					createCombinationMatcher(this.underlyingSecurity1, LONG_CALL, 10000, 4)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListMultipleCombinationTypes() {
		Assertions.assertDoesNotThrow(() -> {
			evaluateCombinationResult(
					Arrays.asList(TradeOptionCombinationTypes.BULL_PUT_SPREAD, TradeOptionCombinationTypes.BEAR_CALL_SPREAD),
					CollectionUtils.combineCollections(
							generatePositionList(this.underlyingSecurity1, 100, this.expirationDate1,
									Arrays.asList(LONG_PUT, SHORT_PUT),
									1, 2),
							generatePositionList(this.underlyingSecurity1, 100, this.expirationDate2,
									Arrays.asList(SHORT_CALL, LONG_CALL),
									3, 4)
					),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.BULL_PUT_SPREAD, 10000, 1, 2),
					createCombinationMatcher(this.underlyingSecurity1, TradeOptionCombinationTypes.BEAR_CALL_SPREAD, 10000, 3, 4)
			);
		});
	}


	@Test
	public void testGetTradeOptionCombinationListInvalidPositionType() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> evaluateCombinationResult(
				TradeOptionCombinationTypes.IRON_CONDOR,
				Arrays.asList(generatePosition(this.underlyingSecurity1, this.stocksHierarchy, LONG_CALL, 0, 1, this.expirationDate1))
		));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Non-option securities found when building option combinations: [SPX LONG_CALL 0 [INACTIVE]]."));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Option Combination Downside Potential Tests     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetOptionCombinationDownsidePotentialSingleOptions() {
		// Downside potential only exists with short positions
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.LONG_CALL, 100, 5), 0);
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.LONG_PUT, 100, 5), 0);
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.SHORT_CALL, 100, 5), null);
		Assertions.assertDoesNotThrow(() -> evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.SHORT_PUT, 100, 5), null));
	}


	@Test
	public void testGetOptionCombinationDownsidePotentialSpreads() {
		// Downside potential only exists with spreads when the short position is more ITM (or less OTM) than the long position
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.BEAR_CALL_SPREAD, 100, 5, 7), 20_000);
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.BULL_CALL_SPREAD, 100, 5, 7), 0);
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.BEAR_PUT_SPREAD, 100, 5, 7), 0);
		Assertions.assertDoesNotThrow(() -> evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.BULL_PUT_SPREAD, 100, 5, 7), 20_000));
	}


	@Test
	public void testGetOptionCombinationDownsidePotentialCondors() {
		// Long condors always have a limited downside potential
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.IRON_CONDOR, 100, 1, 2, 4, 5), 10_000);
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.IRON_CONDOR, 100, 0, 5, 10, 15), 50_000);
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.IRON_CONDOR, 100, 0, 9, 10, 11), 90_000);
		Assertions.assertDoesNotThrow(() -> evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.IRON_CONDOR, 100, 0, 1, 10, 20), 100_000));
	}


	@Test
	public void testGetOptionCombinationDownsidePotentialCollars() {
		// Downside potential from collars is typically limited by a long position in the underlying, but this position is not considered here
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.COLLAR, 100, 10, 20), null);
		evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.COLLAR, 100, 0, 10), null);
		Assertions.assertDoesNotThrow(() -> evaluateDownsidePotential(generateOptionCombination(TradeOptionCombinationTypes.COLLAR, 100, 0, 1), null));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingPosition generatePosition(InvestmentSecurity underlyingSecurity, InvestmentInstrumentHierarchy instrumentHierarchy, TradeOptionLegTypes legType, int strikePrice, int quantity, Date expirationDate) {
		// Create security
		InvestmentSecurity security = TestUtils.generateEntity(InvestmentSecurity.class);
		security.setSymbol(String.format("%s %s %d", underlyingSecurity.getSymbol(), legType, strikePrice));
		security.setUnderlyingSecurity(underlyingSecurity);
		security.setEndDate(expirationDate);
		security.setInstrument(TestUtils.generateEntity(InvestmentInstrument.class));
		security.getInstrument().setHierarchy(instrumentHierarchy);
		security.getInstrument().setPriceMultiplier(MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		security.setOptionType(legType.isCall() ? InvestmentSecurityOptionTypes.CALL : InvestmentSecurityOptionTypes.PUT);
		security.setOptionStrikePrice(BigDecimal.valueOf(strikePrice));

		// Create position
		AccountingTransaction transaction = TestUtils.generateEntity(AccountingTransaction.class, Long.class);
		transaction.setTransactionDate(this.openDate);
		transaction.setClientInvestmentAccount(this.clientInvestmentAccount);
		transaction.setInvestmentSecurity(security);
		transaction.setQuantity(BigDecimal.valueOf(quantity * (legType.isShort() ? -1 : 1)));
		return AccountingPosition.forOpeningTransaction(transaction);
	}


	private List<AccountingPosition> generatePositionList(InvestmentSecurity underlyingSecurity, int quantity, Date expirationDate, List<TradeOptionLegTypes> legTypesList, int... strikePrices) {
		Assertions.assertEquals(legTypesList.size(), strikePrices.length);
		List<AccountingPosition> positionList = new ArrayList<>();
		for (int i = 0; i < legTypesList.size(); i++) {
			positionList.add(generatePosition(underlyingSecurity, this.optionsHierarchy, legTypesList.get(i), strikePrices[i], quantity, expirationDate));
		}
		return positionList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOptionCombination generateOptionCombination(TradeOptionCombinationTypes optionCombinationType, int quantity, int... strikePrices) {
		Assertions.assertEquals(optionCombinationType.getOptionLegTypes().length, strikePrices.length);
		List<AccountingPosition> positionList = generatePositionList(this.underlyingSecurity1, quantity, this.expirationDate1, Arrays.asList(optionCombinationType.getOptionLegTypes()), strikePrices);
		Collection<TradeOptionCombination> combinationList = this.tradeOptionCombinationHandler.getTradeOptionCombinationList(positionList, optionCombinationType);
		return CollectionUtils.getOnlyElementStrict(combinationList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Matcher<TradeOptionCombination> createCombinationMatcher(InvestmentSecurity expectedUnderlyingSecurity, TradeOptionLegTypes expectedLegType, Integer expectedUnderlyingRemainingQuantity, int... expectedStrikePrices) {
		return createCombinationMatcher(expectedUnderlyingSecurity, null, Arrays.asList(expectedLegType), expectedUnderlyingRemainingQuantity, expectedStrikePrices);
	}


	private Matcher<TradeOptionCombination> createCombinationMatcher(InvestmentSecurity expectedUnderlyingSecurity, TradeOptionCombinationTypes expectedCombinationType, Integer expectedUnderlyingRemainingQuantity, int... expectedStrikePrices) {
		return createCombinationMatcher(expectedUnderlyingSecurity, expectedCombinationType, Arrays.asList(expectedCombinationType.getOptionLegTypes()), expectedUnderlyingRemainingQuantity, expectedStrikePrices);
	}


	private Matcher<TradeOptionCombination> createCombinationMatcher(InvestmentSecurity expectedUnderlyingSecurity, TradeOptionCombinationTypes expectedCombinationType, List<TradeOptionLegTypes> expectedLegTypeList, Integer expectedUnderlyingRemainingQuantity, int... expectedStrikePrices) {
		// Prepare expected data
		if (expectedCombinationType == null) {
			Assertions.assertEquals(1, expectedStrikePrices.length);
		}
		else {
			Assertions.assertEquals(expectedCombinationType.getOptionLegTypes().length, expectedStrikePrices.length);
		}
		BigDecimal underlyingRemainingQuantityBigDecimal = expectedUnderlyingRemainingQuantity != null ? BigDecimal.valueOf(expectedUnderlyingRemainingQuantity) : null;

		// Generate matcher
		return new BaseMatcher<TradeOptionCombination>() {
			@Override
			public boolean matches(Object item) {
				final boolean matches;
				if (!(item instanceof TradeOptionCombination)) {
					matches = false;
				}
				else {
					TradeOptionCombination optionCombination = (TradeOptionCombination) item;
					BigDecimal actualUnderlyingRemainingQuantity = (optionCombination.isUnknownType() || optionCombination.getCombinationType().isSameUnderlyingQuantity())
							? optionCombination.getUnderlyingRemainingQuantity()
							: null;
					matches = Objects.equals(optionCombination.getUnderlyingSecurity(), expectedUnderlyingSecurity)
							&& optionCombination.getCombinationType() == expectedCombinationType
							&& MathUtils.isEqual(actualUnderlyingRemainingQuantity, underlyingRemainingQuantityBigDecimal)
							&& Objects.equals(CollectionUtils.getConverted(optionCombination.getOptionLegList(), TradeOptionLeg::getLegType), expectedLegTypeList)
							&& Objects.equals(CollectionUtils.getConverted(optionCombination.getOptionLegList(), TradeOptionLeg::getStrikePrice), Arrays.stream(expectedStrikePrices).mapToObj(BigDecimal::valueOf).collect(Collectors.toList()));
				}
				return matches;
			}


			@Override
			public void describeTo(Description description) {
				description.appendText("combination[")
						.appendText("underlying: ").appendValue(expectedUnderlyingSecurity.getSymbol())
						.appendText(", type: ").appendValue(BeanUtils.getLabel(expectedCombinationType))
						.appendText(", quantity: ").appendValue(underlyingRemainingQuantityBigDecimal)
						.appendText(", legs: ").appendValue(expectedLegTypeList)
						.appendText(", and strikes: ").appendValue(expectedStrikePrices)
						.appendText("]");
			}


			@Override
			public void describeMismatch(Object item, Description description) {
				description.appendText("was ");
				if (!(item instanceof TradeOptionCombination)) {
					description.appendText("an instance of ").appendValue(item.getClass().getSimpleName());
				}
				else {
					TradeOptionCombination optionCombination = (TradeOptionCombination) item;
					BigDecimal actualUnderlyingRemainingQuantity = (optionCombination.isUnknownType() || optionCombination.getCombinationType().isSameUnderlyingQuantity())
							? optionCombination.getUnderlyingRemainingQuantity()
							: null;
					description.appendText("combination[")
							.appendText("underlying: ").appendValue(optionCombination.getUnderlyingSecurity().getSymbol())
							.appendText(", type: ").appendValue(BeanUtils.getLabel(optionCombination.getCombinationType()))
							.appendText(", quantity: ").appendValue(actualUnderlyingRemainingQuantity)
							.appendText(", legs: ").appendValue(CollectionUtils.getConverted(optionCombination.getOptionLegList(), TradeOptionLeg::getLegType))
							.appendText(", and strikes: ").appendValue(CollectionUtils.getConverted(optionCombination.getOptionLegList(), TradeOptionLeg::getStrikePrice))
							.appendText("]");
				}
			}
		};
	}


	private Matcher<BigDecimal> createDownsidePotentialMatcher(Integer expectedDownsidePotential) {
		final BigDecimal expectedDownsidePotentialBigDecimal = expectedDownsidePotential != null ? BigDecimal.valueOf(expectedDownsidePotential) : null;
		return new BaseMatcher<BigDecimal>() {
			@Override
			public boolean matches(Object item) {
				final boolean matches;
				if (item != null && !(item instanceof BigDecimal)) {
					matches = false;
				}
				else {
					matches = MathUtils.isEqual((BigDecimal) item, expectedDownsidePotentialBigDecimal);
				}
				return matches;
			}


			@Override
			public void describeTo(Description description) {
				description.appendValue(expectedDownsidePotential);
			}


			@Override
			public void describeMismatch(Object item, Description description) {
				if (item != null && !(item instanceof BigDecimal)) {
					description.appendText("an instance of ").appendValue(item.getClass().getSimpleName());
				}
				else {
					description.appendValue(item);
				}
			}
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SafeVarargs
	private final void evaluateCombinationResult(TradeOptionCombinationTypes allowedCombinationType, List<AccountingPosition> positionList, Matcher<TradeOptionCombination>... expectedMatchers) {
		evaluateCombinationResult(Arrays.asList(allowedCombinationType), positionList, expectedMatchers);
	}


	@SafeVarargs
	private final void evaluateCombinationResult(List<TradeOptionCombinationTypes> allowedCombinationTypeList, List<AccountingPosition> positionList, Matcher<TradeOptionCombination>... expectedMatchers) {
		Collection<TradeOptionCombination> combinationList = this.tradeOptionCombinationHandler.getTradeOptionCombinationList(positionList, allowedCombinationTypeList.toArray(new TradeOptionCombinationTypes[0]));
		MatcherAssert.assertThat(combinationList, CoreMatchers.hasItems(expectedMatchers));
		Assertions.assertEquals(expectedMatchers.length, combinationList.size());
	}


	private void evaluateDownsidePotential(TradeOptionCombination optionCombination, Integer expectedDownsidePotential) {
		MatcherAssert.assertThat(this.tradeOptionCombinationHandler.getOptionCombinationDownsidePotential(optionCombination), createDownsidePotentialMatcher(expectedDownsidePotential));
	}
}
