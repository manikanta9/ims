package com.clifton.trade.rule;

import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListEntity;
import com.clifton.system.list.SystemListService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author mitchellf
 */
@TradeInMemoryDatabaseContext
public class ActiveTradesInOppositeDirectionRuleEvaluatorTests<L extends SystemList> extends BaseInMemoryDatabaseTests {

	private static final String ADEPT_19_GBP = "051451";
	private static final String ADEPT_19 = "051450";
	private static final String TRADE_TYPE = "Forwards";
	private static final String HOLDING_ACCOUNT = "AS19-BNYM";
	private static final String USD_GBP_FORWARD = "USD/GBP20220131";
	private static final String TRADE_DATE = "01/24/2022";
	private static final String SETTLEMENT_DATE = "01/31/2022";
	private static final String BROKER_COMPANY = "Bank of New York Mellon";
	private static final String COMPANY_TYPE = "Custodian";
	private static final String ADEPT_19_ACCT_GROUP = "Adept 19 - Active Client Accts.";
	private static final String RULE_DEFINITION_NAME = "Trade For Same Client Account Group In Opposite Direction Rule Evaluator";

	@Resource
	private InvestmentAccountService investmentAccountService;
	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;
	@Resource
	private TradeService tradeService;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private BusinessCompanyService businessCompanyService;
	@Resource
	private RuleDefinitionService ruleDefinitionService;
	@Resource
	private RuleViolationService ruleViolationService;
	@Resource
	private SystemListService<L> systemListService;


	@Test
	public void testForwardTradeSameAccount_violation() {
		Trade sellTrade = setupTrade(ADEPT_19_GBP);

		ActiveTradesInOppositeDirectionRuleEvaluator<L> ruleEvaluator = setupRuleEvaluator();

		RuleViolation violation = CollectionUtils.getOnlyElement(ruleEvaluator.evaluateRule(sellTrade, getRuleConfig(), getTradeRuleEvaluatorContext()));
		if (violation != null) {
			String expectedViolation = "There exists a non-closed trade for the same trade date in the opposite direction for security USD/GBP20220131 (USD/GBP Curr Fwd 01/31/2022) [INACTIVE] for the same beneficial owner. Please ensure execution does not violate wash trade rule. Other trades include \n" +
					"051451-AS19-BNYM: BUY 100 of USD/GBP20220131 on 01/24/2022\n";
			ValidationUtils.assertEquals(violation.getViolationNote(), expectedViolation,
					"Did not find expected violation - expected [" + expectedViolation + "] and found [" + violation.getViolationNote() + "].");
		}
		else {
			throw new ValidationException("Expected to find a trade rule violation, but did not find any.");
		}
	}


	@Test
	public void testForwardTradeViolationDifferentAccounts_violation() {
		Trade sellTrade = setupTrade(ADEPT_19);

		ActiveTradesInOppositeDirectionRuleEvaluator<L> ruleEvaluator = setupRuleEvaluator();
		ruleEvaluator.setDifferentClientAccountListItemId(123);

		RuleViolation violation = CollectionUtils.getOnlyElement(ruleEvaluator.evaluateRule(sellTrade, getRuleConfig(), getTradeRuleEvaluatorContext()));
		if (violation != null) {
			String expectedViolation = "There exists a non-closed trade for the same trade date in the opposite direction for security USD/GBP20220131 (USD/GBP Curr Fwd 01/31/2022) [INACTIVE] for the same beneficial owner. Please ensure execution does not violate wash trade rule. Other trades include \n" +
					"051451-AS19-BNYM: BUY 100 of USD/GBP20220131 on 01/24/2022\n";
			ValidationUtils.assertEquals(violation.getViolationNote(), expectedViolation,
					"Did not find expected violation - expected [" + expectedViolation + "] and found [" + violation.getViolationNote() + "].");
		}
		else {
			throw new ValidationException("Expected to find a trade rule violation, but did not find any.");
		}
	}

	@Test
	public void testForwardTradeViolationDifferentAccountsPropertyNull_violation() {
		Trade sellTrade = setupTrade(ADEPT_19);

		ActiveTradesInOppositeDirectionRuleEvaluator<L> ruleEvaluator = setupRuleEvaluator();

		RuleViolation violation = CollectionUtils.getOnlyElement(ruleEvaluator.evaluateRule(sellTrade, getRuleConfig(), getTradeRuleEvaluatorContext()));
		if (violation != null) {
			String expectedViolation = "There exists a non-closed trade for the same trade date in the opposite direction for security USD/GBP20220131 (USD/GBP Curr Fwd 01/31/2022) [INACTIVE] for the same beneficial owner. Please ensure execution does not violate wash trade rule. Other trades include \n" +
					"051451-AS19-BNYM: BUY 100 of USD/GBP20220131 on 01/24/2022\n";
			ValidationUtils.assertEquals(violation.getViolationNote(), expectedViolation,
					"Did not find expected violation - expected [" + expectedViolation + "] and found [" + violation.getViolationNote() + "].");
		}
		else {
			throw new ValidationException("Expected to find a trade rule violation, but did not find any.");
		}
	}


	@Test
	public void testForwardTradeViolationDifferentAccounts_noViolation() {
		Trade sellTrade = setupTrade(ADEPT_19);

		ActiveTradesInOppositeDirectionRuleEvaluator<L> ruleEvaluator = setupRuleEvaluator();
		ruleEvaluator.setDifferentClientAccountListItemId(124);

		ValidationUtils.assertEmpty(ruleEvaluator.evaluateRule(sellTrade, getRuleConfig(), getTradeRuleEvaluatorContext()), "Did not expect to find any violations, but found at least one violation.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Trade setupTrade(String investmentAccount) {
		Trade sellTrade = new Trade();
		sellTrade.setClientInvestmentAccount(this.investmentAccountService.getInvestmentAccountByNumber(investmentAccount));
		sellTrade.setHoldingInvestmentAccount(this.investmentAccountService.getInvestmentAccountByNumber(HOLDING_ACCOUNT));
		sellTrade.setBuy(false);
		sellTrade.setInvestmentSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol(USD_GBP_FORWARD, false));
		sellTrade.setQuantityIntended(new BigDecimal("100"));
		TradeType tradeType = this.tradeService.getTradeTypeByName(TRADE_TYPE);
		sellTrade.setTradeType(tradeType);
		sellTrade.setTradeDate(DateUtils.toDate(TRADE_DATE));
		sellTrade.setSettlementDate(DateUtils.toDate(SETTLEMENT_DATE));
		sellTrade.setExecutingBrokerCompany(this.businessCompanyService.getBusinessCompanyByTypeAndName(COMPANY_TYPE, BROKER_COMPANY));
		sellTrade.setPayingSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true));
		sellTrade.setId(123456789);

		return sellTrade;
	}


	private ActiveTradesInOppositeDirectionRuleEvaluator<L> setupRuleEvaluator() {
		ActiveTradesInOppositeDirectionRuleEvaluator<L> ruleEvaluator = new ActiveTradesInOppositeDirectionRuleEvaluator<L>();
		ruleEvaluator.setTradeTypeIds(new Short[]{this.tradeService.getTradeTypeByName(TRADE_TYPE).getId()});
		InvestmentAccountGroup accountGroup = this.investmentAccountGroupService.getInvestmentAccountGroupByName(ADEPT_19_ACCT_GROUP);
		ruleEvaluator.setInvestmentAccountGroupIds(new Integer[]{accountGroup.getId()});
		ruleEvaluator.setTradeService(this.tradeService);
		ruleEvaluator.setInvestmentAccountGroupService(this.investmentAccountGroupService);
		ruleEvaluator.setRuleViolationService(this.ruleViolationService);
		ruleEvaluator.setSystemListService(this.systemListService);

		return ruleEvaluator;
	}


	private RuleConfig getRuleConfig() {
		RuleAssignment assignment = this.ruleDefinitionService.getRuleAssignmentGlobalByDefinitionName(RULE_DEFINITION_NAME);
		RuleConfig ruleConfig = new RuleConfig(assignment);
		EntityConfig entityConfig = new EntityConfig(assignment);
		ruleConfig.addEntityConfig(entityConfig);
		return ruleConfig;
	}


	private TradeRuleEvaluatorContext getTradeRuleEvaluatorContext() {
		TradeRuleEvaluatorContext tradeRuleEvaluatorContext = new TradeRuleEvaluatorContext();
		((ConfigurableApplicationContext) this.applicationContext).getBeanFactory().autowireBeanProperties(tradeRuleEvaluatorContext, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return tradeRuleEvaluatorContext;
	}
}
