SELECT 'InvestmentAccountGroupAccount' AS entityTableName, InvestmentAccountGroupAccountID AS entityId
FROM InvestmentAccountGroupAccount
WHERE InvestmentAccountGroupID = 541
UNION
SELECT 'Trade' AS entityTableName, TradeID AS entityId
FROM Trade
WHERE TradeID = 2899123
UNION
SELECT 'WorkflowTransition' AS entityTableName, WorkflowTransitionID AS entityId
FROM WorkflowTransition
WHERE StartWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = 6)
   OR EndWorkflowStateID IN (SELECT WorkflowStateID FROM WorkflowState WHERE WorkflowID = 6)
UNION
SELECT 'WorkflowAssignment' AS entityTableName, WorkflowAssignmentID AS entityId
FROM WorkflowAssignment
WHERE WorkflowID = 6
UNION
SELECT 'RuleAssignment' AS entityTableName, RuleAssignmentID AS entityId
FROM RuleAssignment
WHERE RuleAssignmentID = 8585
UNION
SELECT 'SystemListItem' AS entityTableName, SystemListItemID AS entityId
FROM SystemListItem
WHERE SystemListID = 14;

