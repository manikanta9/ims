package com.clifton.trade.fxrate;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.TradeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * A Set of tests used to verify the proper calculations of values based on FX rates for various
 * currency denominations. The TradeService method getTradeAmountForSettlementCurrency() function implements this functionality.
 *
 * @author davidi
 */
@TradeInMemoryDatabaseContext
public class TradeServiceSettlementRateTests extends BaseInMemoryDatabaseTests {

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentInstrumentService investmentSecurityService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_PLN_PLN() {
		Trade trade = this.tradeService.getTrade(2851139);
		BigDecimal expected = BigDecimal.valueOf(200.00);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_PLN_USD() {
		Trade trade = this.tradeService.getTrade(2851140);
		BigDecimal expected = BigDecimal.valueOf(49.13);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_USD_PLN() {
		Trade trade = this.tradeService.getTrade(2851140);
		InvestmentSecurity usd = this.investmentSecurityService.getInvestmentSecurityBySymbol("USD", true);
		InvestmentSecurity pln = this.investmentSecurityService.getInvestmentSecurityBySymbol("PLN", true);
		trade.getInvestmentSecurity().getInstrument().setTradingCurrency(usd);
		trade.setPayingSecurity(pln);
		BigDecimal expected = BigDecimal.valueOf(814.22);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_HKD_USD() {
		Trade trade = this.tradeService.getTrade(2845105);
		BigDecimal expected = BigDecimal.valueOf(25.66);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_USD_HKD() {
		Trade trade = this.tradeService.getTrade(2845105);
		InvestmentSecurity usd = this.investmentSecurityService.getInvestmentSecurityBySymbol("USD", true);
		InvestmentSecurity hkd = this.investmentSecurityService.getInvestmentSecurityBySymbol("HKD", true);
		trade.setInvestmentSecurity(usd);
		trade.getInvestmentSecurity().setSettlementCurrency(usd);
		trade.setPayingSecurity(hkd);
		// Manual scaling set for expected value due to valueOf() dropping the last 0, retuning 1558.6 instead of 1558.60.
		BigDecimal expected = BigDecimal.valueOf(1558.60).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_JPY_GBP() {
		Trade trade = this.tradeService.getTrade(2845226);
		BigDecimal expected = BigDecimal.valueOf(1.33);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_USD_JPY() {
		Trade trade = this.tradeService.getTrade(2845226);
		InvestmentSecurity usd = this.investmentSecurityService.getInvestmentSecurityBySymbol("USD", true);
		InvestmentSecurity jpy = this.investmentSecurityService.getInvestmentSecurityBySymbol("JPY", true);
		trade.setInvestmentSecurity(usd);
		trade.getInvestmentSecurity().setSettlementCurrency(usd);
		trade.setPayingSecurity(jpy);
		BigDecimal expected = BigDecimal.valueOf(22593);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_EUR_GBP() {
		Trade trade = this.tradeService.getTrade(2843120);
		BigDecimal expected = BigDecimal.valueOf(170.16);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_GBP_EUR() {
		Trade trade = this.tradeService.getTrade(2843120);
		InvestmentSecurity gbp = this.investmentSecurityService.getInvestmentSecurityBySymbol("GBP", true);
		InvestmentSecurity eur = this.investmentSecurityService.getInvestmentSecurityBySymbol("EUR", true);
		trade.setInvestmentSecurity(gbp);
		trade.getInvestmentSecurity().setSettlementCurrency(gbp);
		trade.setPayingSecurity(eur);
		BigDecimal expected = BigDecimal.valueOf(235.07);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_CAD_USD() {
		Trade trade = this.tradeService.getTrade(2847851);
		BigDecimal expected = BigDecimal.valueOf(156.37);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}


	@Test
	public void testGetTradeAmountForTradeSettlementCurrency_ccy_USD_CAD() {
		Trade trade = this.tradeService.getTrade(2847851);
		InvestmentSecurity usd = this.investmentSecurityService.getInvestmentSecurityBySymbol("USD", true);
		InvestmentSecurity cad = this.investmentSecurityService.getInvestmentSecurityBySymbol("CAD", true);
		trade.setInvestmentSecurity(usd);
		trade.getInvestmentSecurity().setSettlementCurrency(usd);
		trade.setPayingSecurity(cad);
		BigDecimal expected = BigDecimal.valueOf(255.81);
		BigDecimal result = this.tradeService.getTradeAmountForTradeSettlementCurrency(trade, BigDecimal.valueOf(200.00));
		Assertions.assertEquals(expected, result);
	}
}
