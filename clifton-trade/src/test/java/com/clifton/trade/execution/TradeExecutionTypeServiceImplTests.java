package com.clifton.trade.execution;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.execution.search.TradeExecutionTypeSearchForm;
import com.clifton.trade.execution.search.TradeTypeTradeExecutionTypeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Tests to verify the service methods of  the TradeExecutionTypeService
 *
 * @author davidi
 */
@TradeInMemoryDatabaseContext
public class TradeExecutionTypeServiceImplTests extends BaseInMemoryDatabaseTests {

	@Resource
	TradeExecutionTypeService tradeExecutionTypeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetTradeExecutionType() {
		final short testId = (short) 2;
		final short testUnknownId = (short) 9999;
		TradeExecutionType tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionType(testId);
		Assertions.assertNotNull(tradeExecutionType);
		Assertions.assertEquals(testId, (short) tradeExecutionType.getId());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionType(testUnknownId);
		Assertions.assertNull(tradeExecutionType);
	}


	@Test
	public void testGetTradeExecutionTypeByName() {
		final String testName = TradeExecutionType.CLS_TRADE;
		final String testUnknownName = "ACCEDA";
		TradeExecutionType tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(testName);
		Assertions.assertNotNull(tradeExecutionType);
		Assertions.assertEquals(testName, tradeExecutionType.getName());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(testUnknownName);
		Assertions.assertNull(tradeExecutionType);
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_ById() {
		// Note:  more detailed tests on querying TradeExecutionTypes via SearchForms are in the TradeExecutionTypeTests test class.
		final Short id = 2;
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setId(id);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertEquals(id, tradeExecutionTypeList.get(0).getId());
	}


	@Test
	public void testGetTradeExecutionTypeListForTradeType() {
		// Forwards
		final short tradeTypeId = 7;
		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeListForTradeType(tradeTypeId);
		List<String> tradeExecutionNames = CollectionUtils.getStream(tradeExecutionTypeList).map(TradeExecutionType::getName).distinct().collect(Collectors.toList());

		// Expect 2 associated  types
		Assertions.assertEquals(2, tradeExecutionNames.size());
		Assertions.assertTrue(tradeExecutionNames.contains(TradeExecutionType.CLS_TRADE));
		Assertions.assertTrue(tradeExecutionNames.contains(TradeExecutionType.NON_CLS_TRADE));
	}


	@Test
	public void testSaveTradeExecutionType() {
		final String testName = "BTIC Trade";

		TradeExecutionType tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(testName);
		Assertions.assertTrue(tradeExecutionType.isBticTrade());

		tradeExecutionType.setBticTrade(false);
		this.tradeExecutionTypeService.saveTradeExecutionType(tradeExecutionType);

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(testName);
		Assertions.assertFalse(tradeExecutionType.isBticTrade());
	}


	@Test
	public void testGetTradeTypeTradeExecutionType() {
		final short testId = (short) 2;
		final short testUnknownId = (short) 9999;
		TradeTypeTradeExecutionType tradeTypeTradeExecutionType = this.tradeExecutionTypeService.getTradeTypeTradeExecutionType(testId);
		Assertions.assertNotNull(tradeTypeTradeExecutionType);
		Assertions.assertEquals(testId, (short) tradeTypeTradeExecutionType.getId());

		tradeTypeTradeExecutionType = this.tradeExecutionTypeService.getTradeTypeTradeExecutionType(testUnknownId);
		Assertions.assertNull(tradeTypeTradeExecutionType);
	}


	@Test
	public void testGetTradeTypeTradeExecutionTypeList_SearchForm_ById() {
		final Short id = 2;
		TradeTypeTradeExecutionTypeSearchForm searchForm = new TradeTypeTradeExecutionTypeSearchForm();
		searchForm.setId(id);

		List<TradeTypeTradeExecutionType> tradeTypeTradeExecutionTypeList = this.tradeExecutionTypeService.getTradeTypeTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeTypeTradeExecutionTypeList.size());
		Assertions.assertEquals(id, tradeTypeTradeExecutionTypeList.get(0).getId());
	}


	@Test
	public void testSaveTradeTypeTradeExecutionType() {
		final short testId = (short) 2;
		TradeTypeTradeExecutionType tradeTypeTradeExecutionType = this.tradeExecutionTypeService.getTradeTypeTradeExecutionType(testId);
		Assertions.assertNotNull(tradeTypeTradeExecutionType);
		Assertions.assertEquals(testId, (short) tradeTypeTradeExecutionType.getId());
		Assertions.assertFalse(tradeTypeTradeExecutionType.isDefaultExecutionType());

		tradeTypeTradeExecutionType.setDefaultExecutionType(true);
		this.tradeExecutionTypeService.saveTradeTypeTradeExecutionType(tradeTypeTradeExecutionType);

		tradeTypeTradeExecutionType = this.tradeExecutionTypeService.getTradeTypeTradeExecutionType(testId);
		Assertions.assertTrue(tradeTypeTradeExecutionType.isDefaultExecutionType());
	}


	@Test
	public void testDeleteTradeTypeTradeExecutionType() {
		final short testId = (short) 2;
		TradeTypeTradeExecutionType tradeTypeTradeExecutionType = this.tradeExecutionTypeService.getTradeTypeTradeExecutionType(testId);
		Assertions.assertNotNull(tradeTypeTradeExecutionType);
		Assertions.assertEquals(testId, (short) tradeTypeTradeExecutionType.getId());

		this.tradeExecutionTypeService.deleteTradeTypeTradeExecutionType(tradeTypeTradeExecutionType.getId());

		tradeTypeTradeExecutionType = this.tradeExecutionTypeService.getTradeTypeTradeExecutionType(testId);
		Assertions.assertNull(tradeTypeTradeExecutionType);
	}
}
