package com.clifton.trade.execution;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.TradeService;
import com.clifton.trade.execution.search.TradeExecutionTypeSearchForm;
import com.clifton.trade.search.TradeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;


@TradeInMemoryDatabaseContext
public class TradeExecutionTypeTests extends BaseInMemoryDatabaseTests {

	@Resource
	private TradeService tradeService;

	@Resource
	private TradeExecutionTypeService tradeExecutionTypeService;

	boolean isInitialized = false;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void initData() {
		if (this.isInitialized) {
			return;
		}

		Trade trade = this.tradeService.getTrade(855522);
		trade.setTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionType((short) 4));
		trade.setLimitPrice(new BigDecimal("11.111"));
		this.tradeService.saveTrade(trade);

		trade = this.tradeService.getTrade(855521);
		trade.setTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionType((short) 6));
		trade.setLimitPrice(new BigDecimal("12.222"));
		this.tradeService.saveTrade(trade);

		trade = this.tradeService.getTrade(855520);
		trade.setTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionType((short) 2));
		this.tradeService.saveTrade(trade);

		trade = this.tradeService.getTrade(855519);
		trade.setTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionType((short) 2));
		trade.setLimitPrice(new BigDecimal("13.333"));
		this.tradeService.saveTrade(trade);

		trade = this.tradeService.getTrade(855523);
		trade.setTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionType((short) 4));
		this.tradeService.saveTrade(trade);

		trade = this.tradeService.getTrade(855524);
		trade.setTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionType((short) 7));
		trade.setLimitPrice(new BigDecimal("5.55"));
		this.tradeService.saveTrade(trade);

		this.isInitialized = true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetTradeExecutionType() {
		TradeExecutionType tradeExecutionType;

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionType((short) 2);
		Assertions.assertEquals((short) 2, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.MOC_TRADE, tradeExecutionType.getName());


		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionType((short) 4);
		Assertions.assertEquals((short) 4, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.BTIC_TRADE, tradeExecutionType.getName());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionType((short) 6);
		Assertions.assertEquals((short) 6, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.CLS_TRADE, tradeExecutionType.getName());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionType((short) 7);
		Assertions.assertEquals((short) 7, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.NON_CLS_TRADE, tradeExecutionType.getName());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionType((short) 999);
		Assertions.assertNull(tradeExecutionType);
	}


	@Test
	public void testGetTradeExecutionTypeByName() {
		TradeExecutionType tradeExecutionType;

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(TradeExecutionType.MOC_TRADE);
		Assertions.assertEquals((short) 2, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.MOC_TRADE, tradeExecutionType.getName());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(TradeExecutionType.BTIC_TRADE);
		Assertions.assertEquals((short) 4, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.BTIC_TRADE, tradeExecutionType.getName());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(TradeExecutionType.CLS_TRADE);
		Assertions.assertEquals((short) 6, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.CLS_TRADE, tradeExecutionType.getName());

		tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(TradeExecutionType.NON_CLS_TRADE);
		Assertions.assertEquals((short) 7, (short) tradeExecutionType.getId());
		Assertions.assertEquals(TradeExecutionType.NON_CLS_TRADE, tradeExecutionType.getName());
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_Searchpattern_Name_Description_SubMatch() {
		final String pattern = "MOC";
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setSearchPattern(pattern);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertTrue(StringUtils.contains(tradeExecutionTypeList.get(0).getName() + tradeExecutionTypeList.get(0).getDescription(), pattern));
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_TypeName_Submatch() {
		final String pattern = "MOC";
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setTypeName(pattern);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertTrue(tradeExecutionTypeList.get(0).getName().contains(pattern));
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_ById() {
		final Short id = 2;
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setId(id);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertEquals(id, tradeExecutionTypeList.get(0).getId());
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_IsMarketOnCloseTrade() {
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setMarketOnCloseTrade(true);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertTrue(tradeExecutionTypeList.get(0).isMarketOnCloseTrade());

		searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setMarketOnCloseTrade(false);

		tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(3, tradeExecutionTypeList.size());
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_IsClsTrade() {
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setClsTrade(true);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertTrue(tradeExecutionTypeList.get(0).isClsTrade());
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_ClsTrade_Properties_Correct() {
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setClsTrade(true);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertEquals(TradeExecutionType.CLS_TRADE, tradeExecutionTypeList.get(0).getName());

		TradeExecutionType tradeExecutionType = tradeExecutionTypeList.get(0);
		Assertions.assertNotNull(tradeExecutionType.getRequiredHoldingInvestmentAccountType());
		Assertions.assertEquals(InvestmentAccountType.OTC_CLS, tradeExecutionType.getRequiredHoldingInvestmentAccountType().getName());

		Assertions.assertNull(tradeExecutionType.getExcludeHoldingInvestmentAccountType());

		Assertions.assertNotNull(tradeExecutionType.getExecutingBrokerFilter());
		Assertions.assertEquals("Executing Broker CLS Filter System Condition", tradeExecutionType.getExecutingBrokerFilter().getName());
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_Non_ClsTrade_Properties_Correct() {
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		searchForm.setTypeName(TradeExecutionType.NON_CLS_TRADE);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertEquals(TradeExecutionType.NON_CLS_TRADE, tradeExecutionTypeList.get(0).getName());

		TradeExecutionType tradeExecutionType = tradeExecutionTypeList.get(0);
		Assertions.assertNull(tradeExecutionType.getExcludeHoldingInvestmentAccountType());

		Assertions.assertNull(tradeExecutionType.getRequiredHoldingInvestmentAccountType());

		Assertions.assertNull(tradeExecutionType.getExecutingBrokerFilter());
	}


	@Test
	public void testGetTradeExecutionTypeList_SearchForm_BticTrade_With_TypeName() {
		TradeExecutionTypeSearchForm searchForm = new TradeExecutionTypeSearchForm();
		final String pattern = "BTIC Trade";
		searchForm.setTypeNameEquals(pattern);

		List<TradeExecutionType> tradeExecutionTypeList = this.tradeExecutionTypeService.getTradeExecutionTypeList(searchForm);
		Assertions.assertEquals(1, tradeExecutionTypeList.size());
		Assertions.assertTrue(tradeExecutionTypeList.get(0).isBticTrade());
		Assertions.assertEquals(pattern, tradeExecutionTypeList.get(0).getName());
	}


	@Test
	public void testGetTradeList_SearchForm_By_TradeExecutionTypeID() {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeExecutionTypeId((short) 2);
		List<Trade> tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(2, tradeList.size());
		List<Long> expectedIdList = CollectionUtils.createList((long) 855519, (long) 855520);
		Assertions.assertNotEquals((long) tradeList.get(0).getId(), (long) tradeList.get(1).getId());
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(0).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(1).getId()));

		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeExecutionTypeId((short) 4);
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(2, tradeList.size());
		expectedIdList = CollectionUtils.createList((long) 855522, (long) 855523);
		Assertions.assertNotEquals((long) tradeList.get(0).getId(), (long) tradeList.get(1).getId());
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(0).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(1).getId()));

		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeExecutionTypeId((short) 6);
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(1, tradeList.size());
		Assertions.assertEquals(855521, (long) tradeList.get(0).getId());


		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeExecutionTypeId((short) 7);
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(1, tradeList.size());
		Assertions.assertEquals(855524, (long) tradeList.get(0).getId());
	}


	@Test
	public void testGetTradeList_SearchForm_By_TradeExecutionTypeName_Equals() {
		TradeSearchForm tradeSearchForm;
		List<Trade> tradeList;

		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeExecutionTypeNameEquals(TradeExecutionType.MOC_TRADE);
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(2, tradeList.size());
		List<Long> expectedIdList = CollectionUtils.createList((long) 855519, (long) 855520);
		Assertions.assertNotEquals((long) tradeList.get(0).getId(), (long) tradeList.get(1).getId());
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(0).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(1).getId()));


		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeExecutionTypeNameEquals(TradeExecutionType.BTIC_TRADE);
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(2, tradeList.size());
		expectedIdList = CollectionUtils.createList((long) 855522, (long) 855523);
		Assertions.assertNotEquals((long) tradeList.get(0).getId(), (long) tradeList.get(1).getId());
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(0).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(1).getId()));
	}


	@Test
	public void testGetTradeList_SearchForm_By_MarketOnCloseTradeFlag() {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setMarketOnCloseTrade(true);
		List<Trade> tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(2, tradeList.size());
		List<Long> expectedIdList = CollectionUtils.createList((long) 855519, (long) 855520);
		Assertions.assertNotEquals((long) tradeList.get(0).getId(), (long) tradeList.get(1).getId());
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(0).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(1).getId()));


		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setMarketOnCloseTrade(false);
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(4, tradeList.size());
		expectedIdList = CollectionUtils.createList((long) 855521, (long) 855522, (long) 855523, (long) 855524);

		// Check to ensure no duplicates
		HashSet<Trade> returnedTradesSet = new HashSet<>(tradeList);
		Assertions.assertEquals(4, returnedTradesSet.size());

		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(0).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(1).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(2).getId()));
		Assertions.assertTrue(expectedIdList.contains((long) tradeList.get(3).getId()));
	}


	@Test
	public void testGetTradeList_SearchForm_By_LimitPrice() {
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setLimitPrice(new BigDecimal("11.111"));
		List<Trade> tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(1, tradeList.size());
		Assertions.assertEquals(855522, (long) tradeList.get(0).getId());

		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setLimitPrice(new BigDecimal("12.222"));
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(1, tradeList.size());
		Assertions.assertEquals(855521, (long) tradeList.get(0).getId());

		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setLimitPrice(new BigDecimal("13.333"));
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(1, tradeList.size());
		Assertions.assertEquals(855519, (long) tradeList.get(0).getId());

		// no trade with limit price 15.333 -- expect empty list
		tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setLimitPrice(new BigDecimal("15.333"));
		tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(0, tradeList.size());
	}
}
