package com.clifton.trade.destination.cache;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.SimpleCacheHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMapping;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMappingSearchForm;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.TradeDestinationServiceImpl;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchForm;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyServiceImpl;
import com.clifton.trade.execution.search.TradeExecutingBrokerSearchCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class TradeDestinationMappingSpecificityCacheTests {

	private TradeDestinationService tradeDestinationService;

	private TradeExecutingBrokerCompanyServiceImpl tradeExecutingBrokerCompanyService;

	private TradeDestinationMappingCacheImpl tradeDestinationMappingCache;

	@Mock
	private InvestmentGroupService investmentGroupService;

	@Mock
	private MarketDataSourceCompanyService marketDataSourceCompanyService;

	@Mock
	private TradeService tradeService;

	private static TradeType FUTURES_TYPE;
	private static TradeDestination REDI_TICKET, GS_DESK, MANUAL;
	private static TradeDestinationConnection REDI_FIX_CONNECTION;
	private static BusinessCompany JPM, MS, CITI, UBS, GS, MERRILL;
	private static InvestmentGroup FIX_INVESTMENT_GROUP;
	private static MarketDataSource GOLDMAN_MDS;


	static {
		GOLDMAN_MDS = new MarketDataSource();
		GOLDMAN_MDS.setId((short) 20);

		FUTURES_TYPE = new TradeType();
		FUTURES_TYPE.setId((short) 1);
		FUTURES_TYPE.setName("Futures");

		REDI_FIX_CONNECTION = new TradeDestinationConnection();
		REDI_FIX_CONNECTION.setId((short) 1);
		REDI_FIX_CONNECTION.setName("REDI FIX Connection");

		REDI_TICKET = new TradeDestination();
		REDI_TICKET.setId((short) 1);
		REDI_TICKET.setName(TradeDestination.REDI_TICKET);
		REDI_TICKET.setDestinationOrder(10);
		REDI_TICKET.setConnection(REDI_FIX_CONNECTION);
		REDI_TICKET.setMarketDataSource(GOLDMAN_MDS);

		GS_DESK = new TradeDestination();
		GS_DESK.setId((short) 2);
		GS_DESK.setName("GS Desk");
		GS_DESK.setDestinationOrder(20);
		GS_DESK.setConnection(REDI_FIX_CONNECTION);
		GS_DESK.setMarketDataSource(GOLDMAN_MDS);

		MANUAL = new TradeDestination();
		MANUAL.setId((short) 3);
		MANUAL.setName(TradeDestination.MANUAL);
		MANUAL.setDestinationOrder(1);

		JPM = new BusinessCompany();
		JPM.setId(3);
		JPM.setName("J.P. Morgan Securities LLC");

		MS = new BusinessCompany();
		MS.setId(4);
		MS.setName("Morgan Stanley & Co. Inc.");

		CITI = new BusinessCompany();
		CITI.setId(8);
		CITI.setName("Citigroup Global Markets Inc.");

		UBS = new BusinessCompany();
		UBS.setId(205);
		UBS.setName("UBS Securities, LLC");

		GS = new BusinessCompany();
		GS.setId(7);
		GS.setName("Goldman Sachs & Co.");

		MERRILL = new BusinessCompany();
		MERRILL.setId(5);
		MERRILL.setName("Merrill Lynch Pierce Fenner & Smith Inc.");

		FIX_INVESTMENT_GROUP = new InvestmentGroup();
		FIX_INVESTMENT_GROUP.setId((short) 6);
		FIX_INVESTMENT_GROUP.setName("FIX Contracts");
	}


	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.tradeDestinationService = Mockito.spy(new TradeDestinationServiceImpl());
		this.tradeExecutingBrokerCompanyService = new TradeExecutingBrokerCompanyServiceImpl(new ArrayList<>());
		this.tradeExecutingBrokerCompanyService.setTradeDestinationService(this.tradeDestinationService);

		List<TradeDestinationMapping> mappingList = new ArrayList<>();
		mappingList.add(buildTradeDestination(1, FUTURES_TYPE, MANUAL, JPM, null, false, DateUtils.toDate("11/11/2011"), null));
		mappingList.add(buildTradeDestination(2, FUTURES_TYPE, MANUAL, MS, null, false, DateUtils.toDate("11/11/2011"), null));
		mappingList.add(buildTradeDestination(3, FUTURES_TYPE, MANUAL, CITI, null, false, DateUtils.toDate("11/11/2011"), null));
		mappingList.add(buildTradeDestination(4, FUTURES_TYPE, MANUAL, UBS, null, false, DateUtils.toDate("11/11/2011"), null));
		mappingList.add(buildTradeDestination(45, FUTURES_TYPE, REDI_TICKET, GS, FIX_INVESTMENT_GROUP, false, DateUtils.toDate("11/11/2011"), null));
		mappingList.add(buildTradeDestination(46, FUTURES_TYPE, GS_DESK, GS, null, false, DateUtils.toDate("11/11/2011"), null));
		mappingList.add(buildTradeDestination(47, FUTURES_TYPE, MANUAL, GS, null, true, DateUtils.toDate("11/11/2011"), null));
		mappingList.add(buildTradeDestination(55, FUTURES_TYPE, MANUAL, MERRILL, null, false, DateUtils.toDate("11/29/2011"), null));
		mappingList.add(buildTradeDestination(174, FUTURES_TYPE, REDI_TICKET, CITI, FIX_INVESTMENT_GROUP, false, DateUtils.toDate("10/09/2013"), null));
		mappingList.add(buildTradeDestination(200, FUTURES_TYPE, REDI_TICKET, null, FIX_INVESTMENT_GROUP, false, DateUtils.toDate("10/09/2013"), DateUtils.toDate("10/09/2013")));

		BeanUtils.sortWithFunctions(mappingList, CollectionUtils.createList(
				TradeDestinationMapping::isDefaultDestinationMapping,
				tradeDestinationMapping -> tradeDestinationMapping.getTradeDestination().getDestinationOrder(),
				TradeDestinationMapping::getStartDate,
				TradeDestinationMapping::getId),
				CollectionUtils.createList(false, true, true, true));

		Mockito.doReturn(mappingList).when(this.tradeDestinationService).getTradeDestinationMappingList(ArgumentMatchers.any(TradeDestinationMappingSearchForm.class));

		// setup the company mappings
		MarketDataSourceCompanyMapping cm = new MarketDataSourceCompanyMapping();
		cm.setId(140);
		cm.setExternalCompanyName("citi");
		final List<MarketDataSourceCompanyMapping> cmList = Collections.singletonList(cm);

		Mockito.doAnswer((Answer<List<MarketDataSourceCompanyMapping>>) invocation -> {
			Object[] args = invocation.getArguments();
			MarketDataSourceCompanyMappingSearchForm sf = ((MarketDataSourceCompanyMappingSearchForm) args[0]);
			if ((sf.getMarketDataSourceId().equals((short) 20)) && (sf.getBusinessCompanyId().equals(8))) {
				return cmList;
			}
			return null;
		}).when(this.marketDataSourceCompanyService).getMarketDataSourceCompanyMappingList(ArgumentMatchers.any(MarketDataSourceCompanyMappingSearchForm.class));

		// setup the trade service
		Mockito.when(this.tradeService.getTradeTypeForSecurity(ArgumentMatchers.anyInt())).thenReturn(FUTURES_TYPE);

		// populate the service objects
		this.tradeDestinationMappingCache = new TradeDestinationMappingCacheImpl();
		this.tradeDestinationMappingCache.setTradeDestinationService(this.tradeDestinationService);
		this.tradeDestinationMappingCache.setMarketDataSourceCompanyService(this.marketDataSourceCompanyService);
		this.tradeDestinationMappingCache.setInvestmentGroupService(this.investmentGroupService);
		this.tradeDestinationMappingCache.setCacheHandler(new SimpleCacheHandler<>());

		((TradeDestinationServiceImpl) this.tradeDestinationService).setTradeDestinationMappingCache(this.tradeDestinationMappingCache);
		((TradeDestinationServiceImpl) this.tradeDestinationService).setTradeService(this.tradeService);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testScreenSecurityLookup() {
		long startTime = System.currentTimeMillis();

		InvestmentSecurity esz3 = newInvestmentSecurity(10, "ESZ3", "Futures", "USD", FIX_INVESTMENT_GROUP.getName());
		TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand();
		command.setSecurityId(esz3.getId());
		command.setActiveOnDate(DateUtils.toDate("01/08/2014"));
		TradeDestinationMapping mapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(command);
		Assertions.assertEquals((Integer) 45, mapping.getId());
		System.out.println("Processing Time: " + (System.currentTimeMillis() - startTime));

		startTime = System.currentTimeMillis();
		mapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(command);
		Assertions.assertEquals((Integer) 45, mapping.getId());
		System.out.println("Processing Time: " + (System.currentTimeMillis() - startTime));
	}


	@Test
	public void testDefaultMappingLookup() {
		TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand();
		command.setTradeTypeId((short) 1);
		command.setDefaultDestinationMapping(true);
		command.setActiveOnDate(DateUtils.toDate("01/08/2014"));
		TradeDestinationMapping mapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(command);
		Assertions.assertEquals((Integer) 47, mapping.getId());

		command = new TradeDestinationMappingSearchCommand();
		command.setTradeTypeId((short) 1);
		command.setTradeDestinationId((short) 3);

		mapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(command);
		Assertions.assertEquals((Integer) 47, mapping.getId());
	}


	@Test
	public void testMappingListLookup() {
		TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand();
		command.setTradeTypeId((short) 1);
		command.setTradeDestinationId((short) 1);

		List<TradeDestinationMapping> mappingList = this.tradeDestinationService.getTradeDestinationMappingListByCommand(command);
		Assertions.assertEquals(3, mappingList.size());
		Assertions.assertEquals((Integer) 45, mappingList.get(0).getId());
		Assertions.assertEquals((Integer) 174, mappingList.get(1).getId());
		Assertions.assertEquals((Integer) 200, mappingList.get(2).getId());

		command.setActive(true);

		mappingList = this.tradeDestinationService.getTradeDestinationMappingListByCommand(command);
		Assertions.assertEquals(2, mappingList.size());
		Assertions.assertEquals((Integer) 45, mappingList.get(0).getId());
		Assertions.assertEquals((Integer) 174, mappingList.get(1).getId());

		command.setExecutingBrokerCompanyId(CITI.getId());
		mappingList = this.tradeDestinationService.getTradeDestinationMappingListByCommand(command);
		Assertions.assertEquals(1, mappingList.size());
		Assertions.assertEquals((Integer) 174, mappingList.get(0).getId());

		command.setExecutingBrokerCompanyId(null);
		command.setTradeDestinationId((short) 3);

		mappingList = this.tradeDestinationService.getTradeDestinationMappingListByCommand(command);
		Assertions.assertEquals(6, mappingList.size());
		Assertions.assertEquals((Integer) 47, mappingList.get(0).getId());
		Assertions.assertEquals((Integer) 1, mappingList.get(1).getId());
		Assertions.assertEquals((Integer) 2, mappingList.get(2).getId());
		Assertions.assertEquals((Integer) 3, mappingList.get(3).getId());
		Assertions.assertEquals((Integer) 4, mappingList.get(4).getId());
		Assertions.assertEquals((Integer) 55, mappingList.get(5).getId());
	}


	@Test
	public void testBrokerCodeLookup() {
		//executingBrokerCompanyName:asc
		TradeDestinationMappingSearchCommand searchForm = new TradeDestinationMappingSearchCommand();
		searchForm.setTradeTypeId((short) 1);
		searchForm.setTradeDestinationId((short) 1);
		searchForm.setBrokerCode("citi");
		searchForm.setActive(true);

		List<TradeDestinationMapping> mappingList = this.tradeDestinationService.getTradeDestinationMappingListByCommand(searchForm);
		Assertions.assertEquals(1, mappingList.size());
		Assertions.assertEquals((Integer) 174, mappingList.get(0).getId());

		searchForm.setActive(false);
		mappingList = this.tradeDestinationService.getTradeDestinationMappingListByCommand(searchForm);
		Assertions.assertEquals(1, mappingList.size());
		Assertions.assertEquals((Integer) 174, mappingList.get(0).getId());
	}


	@Test
	public void testBrokerListLookup() {
		TradeExecutingBrokerSearchCommand searchForm = new TradeExecutingBrokerSearchCommand();
		searchForm.setTradeTypeId((short) 1);
		searchForm.setTradeDestinationId((short) 3);
		searchForm.setOrderBy("executingBrokerCompany.name:asc");
		searchForm.setActive(true);

		List<BusinessCompany> companyList = this.tradeExecutingBrokerCompanyService.getTradeExecutingBrokerCompanyList(searchForm);
		Assertions.assertEquals(6, companyList.size());

		Assertions.assertEquals((Integer) 8, companyList.get(0).getId());
		Assertions.assertEquals((Integer) 7, companyList.get(1).getId());
		Assertions.assertEquals((Integer) 3, companyList.get(2).getId());
		Assertions.assertEquals((Integer) 5, companyList.get(3).getId());
		Assertions.assertEquals((Integer) 4, companyList.get(4).getId());
		Assertions.assertEquals((Integer) 205, companyList.get(5).getId());
	}


	@Test
	public void testBrokerCodeLookup_NonExistentTradeDestination() {
		TradeDestinationMappingSearchCommand searchForm = new TradeDestinationMappingSearchCommand();
		searchForm.setTradeTypeId((short) 1);
		searchForm.setTradeDestinationId((short) 999);
		searchForm.setActive(true);

		testNonMatchedSearchResults(searchForm);

		searchForm.setActive(false);
		testNonMatchedSearchResults(searchForm);
	}


	@Test
	public void testBrokerCodeLookup_NonExistentBroker() {
		TradeDestinationMappingSearchCommand searchForm = new TradeDestinationMappingSearchCommand();
		searchForm.setTradeTypeId((short) 1);
		searchForm.setTradeDestinationId((short) 1);
		searchForm.setBrokerCode("noexist");
		searchForm.setActive(true);

		testNonMatchedSearchResults(searchForm);

		searchForm.setActive(false);
		testNonMatchedSearchResults(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentSecurity newInvestmentSecurity(int id, String symbol, String investmentType, String ccyDenominationSymbol, String... investmentGroupNames) {
		InvestmentSecurity result = InvestmentTestObjectFactory.newInvestmentSecurity(id, symbol, investmentType, ccyDenominationSymbol);
		for (String groupName : investmentGroupNames) {
			if (!StringUtils.isEmpty(groupName)) {
				// setup the investment group
				Mockito.when(this.investmentGroupService.isInvestmentSecurityInGroup(ArgumentMatchers.eq(groupName), ArgumentMatchers.eq(result.getId()))).thenReturn(true);
			}
		}
		return result;
	}


	private TradeDestinationMapping buildTradeDestination(int id, TradeType tradeType, TradeDestination tradeDestination, BusinessCompany executingBrokerCompany,
	                                                      InvestmentGroup defaultInvestmentGroup, boolean defaultDestinationMapping, Date startDate, Date endDate) {
		TradeDestinationMapping mapping = new TradeDestinationMapping();
		mapping.setId(id);
		mapping.setTradeType(tradeType);
		mapping.setTradeDestination(tradeDestination);
		mapping.setExecutingBrokerCompany(executingBrokerCompany);
		mapping.setDefaultInvestmentGroup(defaultInvestmentGroup);
		mapping.setDefaultDestinationMapping(defaultDestinationMapping);
		mapping.setStartDate(startDate);
		mapping.setEndDate(endDate);

		Mockito.doReturn(mapping).when(this.tradeDestinationService).getTradeDestinationMapping(ArgumentMatchers.eq(id));

		return mapping;
	}


	/**
	 * A function to verify that getTradeDestinationMappingListByCommand() and getTradeDestinationMappingByCommand() return
	 * an empty list or null (respectively) if no results are returned by the search criteria.
	 */
	private void testNonMatchedSearchResults(TradeDestinationMappingSearchCommand searchForm) {

		searchForm.setActive(true);

		List<TradeDestinationMapping> mappingList = this.tradeDestinationService.getTradeDestinationMappingListByCommand(searchForm);
		Assertions.assertEquals(0, mappingList.size());

		TradeDestinationMapping mapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(searchForm);
		Assertions.assertNull(mapping);
	}
}
