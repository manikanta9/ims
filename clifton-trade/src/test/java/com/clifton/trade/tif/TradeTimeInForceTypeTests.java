package com.clifton.trade.tif;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.TradeInMemoryDatabaseContext;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeTimeInForceType;
import com.clifton.trade.search.TradeTimeInForceTypeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * Tests for saving and retrieving TradeTimeInForce entities via the TradeService facility.
 *
 * @author davidi
 */
@TradeInMemoryDatabaseContext
public class TradeTimeInForceTypeTests extends BaseInMemoryDatabaseTests {

	@Resource
	TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetTradeInForceType() {
		TradeTimeInForceType tradeTimeInForceType = this.tradeService.getTradeTimeInForceType((short) 1);
		Assertions.assertNotNull(tradeTimeInForceType);
		Assertions.assertEquals((short) 1, tradeTimeInForceType.getId());
	}


	@Test
	public void testGetTradeTimeInForceTypeList() {
		TradeTimeInForceTypeSearchForm searchForm = new TradeTimeInForceTypeSearchForm();
		searchForm.setName("GTC");
		List<TradeTimeInForceType> tradeTimeInForceTypeList = this.tradeService.getTradeTimeInForceTypeList(searchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(tradeTimeInForceTypeList));
		Assertions.assertEquals("GTC", tradeTimeInForceTypeList.get(0).getName());
	}


	@Test
	public void testGetTradeTimeInForceTypeList_full_list() {
		TradeTimeInForceTypeSearchForm searchForm = new TradeTimeInForceTypeSearchForm();
		List<TradeTimeInForceType> tradeTimeInForceTypeList = this.tradeService.getTradeTimeInForceTypeList(searchForm);
		Assertions.assertNotNull(tradeTimeInForceTypeList);
		Assertions.assertEquals(3, CollectionUtils.getSize(tradeTimeInForceTypeList));
		Assertions.assertEquals((short) 1, tradeTimeInForceTypeList.get(0).getId());
		Assertions.assertEquals((short) 2, tradeTimeInForceTypeList.get(1).getId());
		Assertions.assertEquals((short) 3, tradeTimeInForceTypeList.get(2).getId());
	}


	@Test
	public void testGetTradeTimeInForceTypeByName() {
		TradeTimeInForceType tradeTimeInForceType = this.tradeService.getTradeTimeInForceTypeByName("GTD");
		Assertions.assertNotNull(tradeTimeInForceType);
		Assertions.assertEquals("GTD", tradeTimeInForceType.getName());
	}


	@Test
	public void testGetTradeTimeInForceTypeByLabel() {
		TradeTimeInForceTypeSearchForm searchForm = new TradeTimeInForceTypeSearchForm();
		searchForm.setLabel("Good Till Date");
		List<TradeTimeInForceType> tradeTimeInForceTypeList = this.tradeService.getTradeTimeInForceTypeList(searchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(tradeTimeInForceTypeList));
		Assertions.assertEquals("Good Till Date", tradeTimeInForceTypeList.get(0).getLabel());
	}


	@Test
	public void testSaveTradeTimeInForceType() {
		TradeTimeInForceType tradeTimeInForceType = new TradeTimeInForceType();
		tradeTimeInForceType.setName("IOC");
		tradeTimeInForceType.setLabel("IOC Lbl");
		tradeTimeInForceType.setDescription("Immediate or cancel");
		TradeTimeInForceType savedEntity = this.tradeService.saveTradeTimeInForceType(tradeTimeInForceType);

		Assertions.assertNotNull(savedEntity);
		Assertions.assertNotNull(savedEntity.getId());
		Assertions.assertEquals("IOC", savedEntity.getName());
		Assertions.assertEquals("IOC Lbl", savedEntity.getLabel());

		TradeTimeInForceType retrievedEntity = this.tradeService.getTradeTimeInForceType(savedEntity.getId());
		Assertions.assertEquals(savedEntity, retrievedEntity);
	}
}
