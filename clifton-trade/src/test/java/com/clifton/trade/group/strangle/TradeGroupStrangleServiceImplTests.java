package com.clifton.trade.group.strangle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationType;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.strangle.calculator.TradeGroupStrangleCalculator;
import com.clifton.trade.group.strangle.validate.TradeGroupStrangleOptionTradeValidator;
import com.clifton.workflow.definition.WorkflowState;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


@ExtendWith(MockitoExtension.class)
public class TradeGroupStrangleServiceImplTests {

	@Mock
	private SystemColumnValueHandler systemColumnValueHandler;

	@Mock
	private TradeGroupService tradeGroupService;

	@Mock
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeGroupStrangleService getTradeGroupStrangleService() {
		TradeGroupStrangleServiceImpl strangleService = new TradeGroupStrangleServiceImpl();
		strangleService.setSystemColumnValueHandler(this.systemColumnValueHandler);
		strangleService.setTradeGroupService(this.tradeGroupService);
		strangleService.setTradeService(this.tradeService);
		return strangleService;
	}


	public void prepareForSave() {
		TradeService ts = this.tradeService;
		Mockito.lenient().when(ts.splitTrade(ArgumentMatchers.any(Trade.class), ArgumentMatchers.any(BigDecimal.class))).then(invocationOnMock -> {
			Object[] args = invocationOnMock.getArguments();
			Trade toSplit = (Trade) args[0];
			BigDecimal quantity = (BigDecimal) args[1];
			Trade newBean = BeanUtils.cloneBean(toSplit, false, true);
			toSplit.setQuantityIntended(MathUtils.subtract(toSplit.getQuantityIntended(), quantity));
			newBean.setQuantityIntended(quantity);
			return new Trade[]{toSplit, newBean};
		});
		Mockito.when(ts.saveTrade(ArgumentMatchers.any(Trade.class))).then(invocationOnMock -> {
			Object[] args = invocationOnMock.getArguments();
			Trade toSave = (Trade) args[0];
			if (toSave.isNewBean()) {
				toSave.setId(100);
			}
			return toSave;
		});
		Mockito.when(this.tradeGroupService.saveTradeGroup(ArgumentMatchers.any(TradeGroup.class))).then(invocationOnMock -> {
			Object[] args = invocationOnMock.getArguments();
			TradeGroup toSave = (TradeGroup) args[0];
			toSave.setId(100);
			for (Trade trade : CollectionUtils.getIterable(toSave.getTradeList())) {
				ts.saveTrade(trade);
			}
			return toSave;
		});
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void populateStrangleNullTradeListTest() {
		NoSuchElementException noSuchElementException = Assertions.assertThrows(NoSuchElementException.class, () -> getTradeGroupStrangleService().populateTradeGroupStranglePreview(null, new TradeGroupStrangleOptionTradeValidator(), TradeGroupStrangleCalculatorMethods.OPTIONS_BY_ACCOUNT));
		MatcherAssert.assertThat(noSuchElementException.getMessage(), StringContains.containsString("A strangle cannot be created with no trades within itself or one of its tail groups."));
	}


	@Test
	public void populateStrangleEmptyTradeListTest() {
		NoSuchElementException noSuchElementException = Assertions.assertThrows(NoSuchElementException.class, () -> getTradeGroupStrangleService().populateTradeGroupStranglePreview(new ArrayList<>(), new TradeGroupStrangleOptionTradeValidator(), TradeGroupStrangleCalculatorMethods.OPTIONS_BY_ACCOUNT));
		MatcherAssert.assertThat(noSuchElementException.getMessage(), StringContains.containsString("A strangle cannot be created with no trades within itself or one of its tail groups."));
	}


	@Test
	public void populateStrangleOptionsByAccountWithCancelledTradeTest() {
		List<Trade> tradeList = createTradeList();
		WorkflowState cancelledState = new WorkflowState();
		cancelledState.setName("Cancelled");
		tradeList.get(0).setWorkflowState(cancelledState);
		TradeGroupStrangle strangle = getTradeGroupStrangleService().populateTradeGroupStranglePreview(tradeList, new TradeGroupStrangleOptionTradeValidator(), TradeGroupStrangleCalculatorMethods.OPTIONS_BY_ACCOUNT);
		Assertions.assertNotNull(strangle);
		Assertions.assertNull(strangle.getCallTailGroup());
		Assertions.assertNotNull(strangle.getPutTailGroup());

		tradeList = strangle.getPutTailGroup().getTradeList();
		Assertions.assertEquals(new BigDecimal("88"), CoreMathUtils.sumProperty(tradeList, Trade::getQuantityIntended));
		String[] expectedTradeStrings = new String[]{
				"null	Trinity Health	TQJ3P Y 1515	3",
				"null	Trinity Pension	TQJ3P Y 1515	3",
				"null	Uniroyal	TQJ3P Y 1515	3",
				"44	Alfred	TQJ3P Y 1515	25",
				"43	Pitt	TQJ3P Y 1515	27",
				"null	Clifton	TQJ3P Y 1515	27"
		};
		checkOptionTradeList(expectedTradeStrings, tradeList, true);

		tradeList = strangle.getTradeList();
		Assertions.assertEquals(new BigDecimal("324"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 1), Trade::getQuantityIntended));
		Assertions.assertEquals(new BigDecimal("324"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 2), Trade::getQuantityIntended));
		expectedTradeStrings = new String[]{
				"51	Uniroyal	TQJ3C Y 1600	29",
				"60	Uniroyal	TQJ3P Y 1515	29",
				"47	Trinity Health	TQJ3C Y 1600	40",
				"49	Trinity Pension	TQJ3C Y 1600	40",
				"48	Trinity Health	TQJ3P Y 1515	40",
				"50	Trinity Pension	TQJ3P Y 1515	40",
				"45	Clifton	TQJ3C Y 1600	215",
				"46	Clifton	TQJ3P Y 1515	215"
		};
		checkOptionTradeList(expectedTradeStrings, tradeList, true);
	}


	@Test
	public void populateStrangleOptionsByAccountTest() {
		List<Trade> tradeList = createTradeList();
		TradeGroupStrangle strangle = getTradeGroupStrangleService().populateTradeGroupStranglePreview(tradeList, new TradeGroupStrangleOptionTradeValidator(),
				TradeGroupStrangleCalculatorMethods.OPTIONS_BY_ACCOUNT);
		Assertions.assertNotNull(strangle);
		Assertions.assertNotNull(strangle.getCallTailGroup());
		Assertions.assertNotNull(strangle.getPutTailGroup());

		checkOptionStrangleListByAccount(strangle.getTradeList(), true);
		checkOptionCallTailListByAccount(strangle.getCallTailGroup().getTradeList(), true);
		checkOptionPutTailListByAccount(strangle.getPutTailGroup().getTradeList(), true);
	}


	@Test
	public void populateStrangleOptionsAcrossAccountTest() {
		List<Trade> tradeList = createTradeList();
		TradeGroupStrangle strangle = getTradeGroupStrangleService().populateTradeGroupStranglePreview(tradeList, new TradeGroupStrangleOptionTradeValidator(),
				TradeGroupStrangleCalculatorMethods.OPTIONS_ACROSS_ACCOUNTS);
		Assertions.assertNotNull(strangle);
		Assertions.assertNotNull(strangle.getPutTailGroup());
		Assertions.assertNull(strangle.getCallTailGroup());

		checkOptionStrangleListAcrossAccounts(strangle.getTradeList(), true);
		checkOptionPutTailListAcrossAccounts(strangle.getPutTailGroup().getTradeList(), true);
	}


	@Test
	public void populateStrangleOptionsNoTailTest() {
		List<Trade> tradeList = createTradeList();
		TradeGroupStrangle strangle = getTradeGroupStrangleService().populateTradeGroupStranglePreview(tradeList, new TradeGroupStrangleOptionTradeValidator(),
				TradeGroupStrangleCalculatorMethods.OPTIONS_NO_TAIL);
		Assertions.assertNotNull(strangle);
		Assertions.assertNull(strangle.getPutTailGroup());
		Assertions.assertNull(strangle.getCallTailGroup());

		checkOptionStrangleListNoTail(strangle.getTradeList(), true);
	}


	@Test
	public void createStrangleOptionsByAccountTest() {
		List<Trade> tradeList = createTradeList();
		prepareForSave();
		TradeGroupStrangle strangle = getTradeGroupStrangleService().createTradeGroupStrangle(tradeList, new TradeGroupStrangleOptionTradeValidator(),
				TradeGroupStrangleCalculatorMethods.OPTIONS_BY_ACCOUNT);
		Assertions.assertFalse(strangle.isNewBean());
		Assertions.assertFalse(strangle.getCallTailGroup().isNewBean());
		Assertions.assertFalse(strangle.getPutTailGroup().isNewBean());

		checkOptionStrangleListByAccount(strangle.getTradeList(), false);
		checkOptionCallTailListByAccount(strangle.getCallTailGroup().getTradeList(), false);
		checkOptionPutTailListByAccount(strangle.getPutTailGroup().getTradeList(), false);
	}


	@Test
	public void createStrangleOptionsAcrossAccountTest() {
		List<Trade> tradeList = createTradeList();
		prepareForSave();
		TradeGroupStrangle strangle = getTradeGroupStrangleService().createTradeGroupStrangle(tradeList, new TradeGroupStrangleOptionTradeValidator(),
				TradeGroupStrangleCalculatorMethods.OPTIONS_ACROSS_ACCOUNTS);
		Assertions.assertFalse(strangle.isNewBean());
		Assertions.assertFalse(strangle.getPutTailGroup().isNewBean());
		Assertions.assertNull(strangle.getCallTailGroup());

		checkOptionStrangleListAcrossAccounts(strangle.getTradeList(), false);
		checkOptionPutTailListAcrossAccounts(strangle.getPutTailGroup().getTradeList(), false);
	}


	@Test
	public void createStrangleOptionsNoTailTest() {
		List<Trade> tradeList = createTradeList();
		prepareForSave();
		TradeGroupStrangle strangle = getTradeGroupStrangleService().createTradeGroupStrangle(tradeList, new TradeGroupStrangleOptionTradeValidator(),
				TradeGroupStrangleCalculatorMethods.OPTIONS_NO_TAIL);
		Assertions.assertFalse(strangle.isNewBean());
		Assertions.assertNull(strangle.getPutTailGroup());
		Assertions.assertNull(strangle.getCallTailGroup());

		checkOptionStrangleListNoTail(strangle.getTradeList(), false);
	}


	@Test
	public void saveTradeGroupStrangleUnsavedTradesTest() {
		List<Trade> tradeList = createTradeList();

		TradeGroupStrangleCalculatorContext context = new TradeGroupStrangleCalculatorContext(tradeList, new TradeGroupStrangleOptionTradeValidator());
		context.setSystemColumnValueHandler(this.systemColumnValueHandler);
		context.setTradeGroupService(this.tradeGroupService);

		TradeGroupStrangleCalculator calculator = TradeGroupStrangleCalculatorMethods.OPTIONS_ACROSS_ACCOUNTS.getStrangleCalculator();
		TradeGroupStrangle strangle = calculator.populateTradeGroupStrangle(context);

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> getTradeGroupStrangleService().saveTradeGroupStrangle(strangle));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot save a Trade Group Strangle with new Trades. The trades should be split."));
	}


	@Test
	public void saveTradeGroupStrangleSplitTradesTest() {
		List<Trade> tradeList = createTradeList();

		prepareForSave();
		TradeGroupStrangleCalculatorContext context = new TradeGroupStrangleCalculatorContext(tradeList, new TradeGroupStrangleOptionTradeValidator());
		context.setSystemColumnValueHandler(this.systemColumnValueHandler);
		context.setTradeGroupService(this.tradeGroupService);
		context.setTradeService(this.tradeService);
		context.setSaveSplitTrade(true);

		TradeGroupStrangleCalculator calculator = TradeGroupStrangleCalculatorMethods.OPTIONS_ACROSS_ACCOUNTS.getStrangleCalculator();
		TradeGroupStrangle strangle = calculator.populateTradeGroupStrangle(context);

		strangle = getTradeGroupStrangleService().saveTradeGroupStrangle(strangle);
		Assertions.assertFalse(strangle.isNewBean());
		Assertions.assertFalse(strangle.getPutTailGroup().isNewBean());
		Assertions.assertNull(strangle.getCallTailGroup());

		checkOptionStrangleListAcrossAccounts(strangle.getTradeList(), false);
		checkOptionPutTailListAcrossAccounts(strangle.getPutTailGroup().getTradeList(), false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getTradeString(Trade trade, boolean includeId) {
		String tradeId;
		if (includeId) {
			tradeId = trade.isNewBean() ? "null" : trade.getId().toString();
		}
		else {
			tradeId = "";
		}
		return new StringBuilder(tradeId)
				.append("\t").append(trade.getClientInvestmentAccount().getName())
				.append("\t").append(trade.getInvestmentSecurity().getSymbol())
				.append("\t").append(trade.getQuantityIntended()).toString();
	}


	private void checkOptionTradeList(String[] expectedTradeStrings, List<Trade> tradeList, boolean unsavedTrades) {
		BeanUtils.sortWithFunctions(tradeList, CollectionUtils.createList(
				Trade::getQuantityIntended,
				trade -> trade.getInvestmentSecurity().getSymbol()),
				CollectionUtils.createList(true, true));
		Assertions.assertEquals(expectedTradeStrings.length, tradeList.size());

		for (int i = 0; i < expectedTradeStrings.length; i++) {
			if (!unsavedTrades) {
				Assertions.assertFalse(tradeList.get(i).isNewBean(), "Expected a non-new Trade.");
			}
			Assertions.assertEquals(unsavedTrades ? expectedTradeStrings[i] : expectedTradeStrings[i].substring(expectedTradeStrings[i].indexOf("\t")), getTradeString(tradeList.get(i), unsavedTrades));
		}
	}


	private void checkOptionCallTailListByAccount(List<Trade> tradeList, boolean unsavedTrades) {
		Assertions.assertEquals(new BigDecimal("49"), CoreMathUtils.sumProperty(tradeList, Trade::getQuantityIntended));
		String[] expectedTradeStrings = new String[]{"null	Pitt	TQJ3C Y 1600	49"};
		checkOptionTradeList(expectedTradeStrings, tradeList, unsavedTrades);
	}


	private void checkOptionPutTailListByAccount(List<Trade> tradeList, boolean unsavedTrades) {
		Assertions.assertEquals(new BigDecimal("61"), CoreMathUtils.sumProperty(tradeList, Trade::getQuantityIntended));
		String[] expectedTradeStrings = new String[]{
				"null	Trinity Health	TQJ3P Y 1515	3",
				"null	Trinity Pension	TQJ3P Y 1515	3",
				"null	Uniroyal	TQJ3P Y 1515	3",
				"44	Alfred	TQJ3P Y 1515	25",
				"null	Clifton	TQJ3P Y 1515	27"
		};
		checkOptionTradeList(expectedTradeStrings, tradeList, unsavedTrades);
	}


	private void checkOptionStrangleListByAccount(List<Trade> tradeList, boolean unsavedTrades) {
		Assertions.assertEquals(new BigDecimal("351"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 1), Trade::getQuantityIntended));
		Assertions.assertEquals(new BigDecimal("351"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 2), Trade::getQuantityIntended));
		String[] expectedTradeStrings = new String[]{
				"42	Pitt	TQJ3C Y 1600	27",
				"43	Pitt	TQJ3P Y 1515	27",
				"51	Uniroyal	TQJ3C Y 1600	29",
				"60	Uniroyal	TQJ3P Y 1515	29",
				"47	Trinity Health	TQJ3C Y 1600	40",
				"49	Trinity Pension	TQJ3C Y 1600	40",
				"48	Trinity Health	TQJ3P Y 1515	40",
				"50	Trinity Pension	TQJ3P Y 1515	40",
				"45	Clifton	TQJ3C Y 1600	215",
				"46	Clifton	TQJ3P Y 1515	215"
		};
		checkOptionTradeList(expectedTradeStrings, tradeList, unsavedTrades);
	}


	private void checkOptionPutTailListAcrossAccounts(List<Trade> tradeList, boolean unsavedTrades) {
		Assertions.assertEquals(new BigDecimal("12"), CoreMathUtils.sumProperty(tradeList, Trade::getQuantityIntended));
		String[] expectedTradeStrings = new String[]{
				"43	Pitt	TQJ3P Y 1515	1",
				"44	Alfred	TQJ3P Y 1515	1",
				"48	Trinity Health	TQJ3P Y 1515	1",
				"50	Trinity Pension	TQJ3P Y 1515	1",
				"60	Uniroyal	TQJ3P Y 1515	1",
				"46	Clifton	TQJ3P Y 1515	7"
		};
		checkOptionTradeList(expectedTradeStrings, tradeList, unsavedTrades);
	}


	private void checkOptionStrangleListAcrossAccounts(List<Trade> tradeList, boolean unsavedTrades) {
		Assertions.assertEquals(new BigDecimal("400"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 1), Trade::getQuantityIntended));
		Assertions.assertEquals(new BigDecimal("400"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 2), Trade::getQuantityIntended));
		String[] expectedTradeStrings = new String[]{
				"null	Alfred	TQJ3P Y 1515	24",
				"null	Pitt	TQJ3P Y 1515	26",
				"51	Uniroyal	TQJ3C Y 1600	29",
				"null	Uniroyal	TQJ3P Y 1515	31",
				"47	Trinity Health	TQJ3C Y 1600	40",
				"49	Trinity Pension	TQJ3C Y 1600	40",
				"null	Trinity Health	TQJ3P Y 1515	42",
				"null	Trinity Pension	TQJ3P Y 1515	42",
				"42	Pitt	TQJ3C Y 1600	76",
				"45	Clifton	TQJ3C Y 1600	215",
				"null	Clifton	TQJ3P Y 1515	235"
		};
		checkOptionTradeList(expectedTradeStrings, tradeList, unsavedTrades);
	}


	private void checkOptionStrangleListNoTail(List<Trade> tradeList, boolean unsavedTrades) {
		Assertions.assertEquals(new BigDecimal("400"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 1), Trade::getQuantityIntended));
		Assertions.assertEquals(new BigDecimal("412"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 2), Trade::getQuantityIntended));
		String[] expectedTradeStrings = new String[]{
				"44	Alfred	TQJ3P Y 1515	25",
				"43	Pitt	TQJ3P Y 1515	27",
				"51	Uniroyal	TQJ3C Y 1600	29",
				"60	Uniroyal	TQJ3P Y 1515	32",
				"47	Trinity Health	TQJ3C Y 1600	40",
				"49	Trinity Pension	TQJ3C Y 1600	40",
				"48	Trinity Health	TQJ3P Y 1515	43",
				"50	Trinity Pension	TQJ3P Y 1515	43",
				"42	Pitt	TQJ3C Y 1600	76",
				"45	Clifton	TQJ3C Y 1600	215",
				"46	Clifton	TQJ3P Y 1515	242"
		};
		checkOptionTradeList(expectedTradeStrings, tradeList, unsavedTrades);
	}


	private List<Trade> createTradeList() {
		List<Trade> tradeList = new ArrayList<>();
		TradeDestination destination = new TradeDestination();
		TradeDestinationType destinationType = new TradeDestinationType();
		destinationType.setCreateOrder(true);
		destination.setType(destinationType);

		InvestmentSecurity option1600 = InvestmentSecurityBuilder.ofType(1, "TQJ3C Y 1600", InvestmentType.OPTIONS)
				.withName("TQJ3C Y 1600")
				.withOptionType(InvestmentSecurityOptionTypes.CALL)
				.build();
		InvestmentSecurity option1515 = InvestmentSecurityBuilder.ofType(2, "TQJ3P Y 1515", InvestmentType.OPTIONS)
				.withName("TQJ3P Y 1515")
				.withOptionType(InvestmentSecurityOptionTypes.PUT)
				.build();

		InvestmentAccount pittAccount = createClientAccount(1, "Pitt");
		InvestmentAccount alfredAccount = createClientAccount(2, "Alfred");
		InvestmentAccount cliftonAccount = createClientAccount(3, "Clifton");
		InvestmentAccount trinityHealthAccount = createClientAccount(4, "Trinity Health");
		InvestmentAccount trinityPensionAccount = createClientAccount(5, "Trinity Pension");
		InvestmentAccount uniroyalAccount = createClientAccount(6, "Uniroyal");

		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(42)
				.buy()
				.forClient(pittAccount)
				.withQuantityIntended(76)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(43)
				.buy()
				.forClient(pittAccount)
				.withQuantityIntended(27)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(44)
				.buy()
				.forClient(alfredAccount)
				.withQuantityIntended(25)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(45)
				.buy()
				.forClient(cliftonAccount)
				.withQuantityIntended(215)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(46)
				.buy()
				.forClient(cliftonAccount)
				.withQuantityIntended(242)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(47)
				.buy()
				.forClient(trinityHealthAccount)
				.withQuantityIntended(40)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(48)
				.buy()
				.forClient(trinityHealthAccount)
				.withQuantityIntended(43)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(49)
				.buy()
				.forClient(trinityPensionAccount)
				.withQuantityIntended(40)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(50)
				.buy()
				.forClient(trinityPensionAccount)
				.withQuantityIntended(43)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(51)
				.buy()
				.forClient(uniroyalAccount)
				.withQuantityIntended(29)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(60)
				.buy()
				.forClient(uniroyalAccount)
				.withQuantityIntended(32)
				.withTradeDestination(destination)
				.build());

		return tradeList;
	}


	private InvestmentAccount createClientAccount(Integer id, String name) {
		InvestmentAccount account = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault();
		account.setId(id);
		account.setName(name);
		return account;
	}
}
