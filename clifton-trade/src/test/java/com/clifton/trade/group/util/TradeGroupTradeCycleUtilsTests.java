package com.clifton.trade.group.util;

import com.clifton.accounting.AccountingTestObjectFactory;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionBalance;
import com.clifton.accounting.gl.position.AccountingPositionUtils;
import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.trade.options.securitytarget.group.util.TradeGroupTradeCycleUtilHandler;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * A set of tests to test the functionality of the TradeGroupTrancheCountCalculator
 *
 * @author davidi
 */
@ContextConfiguration
@Transactional
public class TradeGroupTradeCycleUtilsTests extends BaseInMemoryDatabaseTests {

	private static final String DYNAMIC_HEDGED_EQUITY_SERVICE_PROCESSING_NAME = "DHE";
	private static final String OARS_SERVICE_PROCESSING_NAME = "OARS";
	private static final String RPS_SERVICE_PROCESSING_NAME = "RPS";
	private static final String DYNAMIC_PUT_SELLING_PROCESSING_NAME = "Put Selling";
	private static final String DYNAMIC_CALL_SELLING_PROCESSING_NAME = "Call Selling";


	private final Date START_OF_CYCLE = DateUtils.toDate("09/03/2018");
	private final List<Integer> DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES = CollectionUtils.createList(11, 25, 4, 18, 9, 23, 2, 16, 7, 21, 0);
	private final List<Integer> DAY_OFFSET_3_WEEK_CYCLE_LIST_3_TRANCHES = CollectionUtils.createList(18, 4);

	@Resource
	TradeGroupTradeCycleUtilHandler tradeGroupTradeCycleUtilHandler;

	@Resource
	private AdvancedReadOnlyDAO<BusinessServiceProcessingType, Criteria> businessServiceProcessingTypeDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_4_Tranches_fromDateWithinFirstTranche() {
		Date balanceDate = DateUtils.toDate("9/4/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 4);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_4_Tranches_fromDateWithinSecondTranche() {
		Date balanceDate = DateUtils.toDate("9/12/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 4);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_4_Tranches_fromDateWithinThirdTranche() {
		Date balanceDate = DateUtils.toDate("9/19/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 4);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_4_Tranches_fromDateWithinFourthTranche() {
		Date balanceDate = DateUtils.toDate("9/28/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 4);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_5_Tranches_fromDateWithinFirstTranche() {
		Date balanceDate = DateUtils.toDate("8/24/2018");
		Date expectedDate = DateUtils.toDate("8/20/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 5);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_5_Tranches_fromDateWithinSecondTranche() {
		Date balanceDate = DateUtils.toDate("8/31/2018");
		Date expectedDate = DateUtils.toDate("8/20/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 5);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_5_Tranches_fromDateWithinThirdTranche() {
		Date balanceDate = DateUtils.toDate("9/3/2018");
		Date expectedDate = DateUtils.toDate("8/20/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 5);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_5_Tranches_fromDateWithinFourthTranche() {
		Date balanceDate = DateUtils.toDate("9/13/2018");
		Date expectedDate = DateUtils.toDate("8/20/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 5);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_5_Tranches_fromDateWithinFifthTranche() {
		Date balanceDate = DateUtils.toDate("9/21/2018");
		Date expectedDate = DateUtils.toDate("8/20/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 5);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_6_Tranches_fromDateWithinFirstTranche() {
		Date balanceDate = DateUtils.toDate("9/4/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 6);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_6_Tranches_fromDateWithinSecondTranche() {
		Date balanceDate = DateUtils.toDate("9/12/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 6);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_6_Tranches_fromDateWithinThirdTranche() {
		Date balanceDate = DateUtils.toDate("9/19/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 6);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_6_Tranches_fromDateWithinFourthTranche() {
		Date balanceDate = DateUtils.toDate("9/28/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 6);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_6_Tranches_fromDateWithinFifthTranche() {
		Date balanceDate = DateUtils.toDate("10/03/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 6);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test
	public void testGetDateForStartOfCurrentCycle_6_Tranches_fromDateWithinSixthTranche() {
		Date balanceDate = DateUtils.toDate("10/12/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 6);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test void testDateForStartOfCurrentCycle_3_Tranches_fromDateWithinFirstTranche() {
		Date balanceDate = DateUtils.toDate("9/7/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 3);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test void testDateForStartOfCurrentCycle_3_Tranches_fromDateWithinSecondTranche() {
		Date balanceDate = DateUtils.toDate("9/14/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 3);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test void testDateForStartOfCurrentCycle_3_Tranches_fromDateWithinThirdTranche() {
		Date balanceDate = DateUtils.toDate("9/21/2018");
		Date expectedDate = DateUtils.toDate("9/3/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 3);

		Assertions.assertEquals(expectedDate, returnedDate);
	}


	@Test void testDateForStartOfCurrentCycle_3_Tranches_fromDateWithinNextFirstTranche() {
		Date balanceDate = DateUtils.toDate("9/28/2018");
		Date expectedDate = DateUtils.toDate("9/24/2018");

		Date returnedDate = this.tradeGroupTradeCycleUtilHandler.getStartOfCurrentCycleDate(balanceDate, 3);

		Assertions.assertEquals(expectedDate, returnedDate);
	}



	@Test
	public void testGetUnexpiredContractCount_OARS() {

		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("10/8/2018", "10/10/2018", "10/12/2018", "10/15/2018", "10/18/2018");
		List<AccountingPositionBalance> accountingPositionBalanceList = generateAccountingPositionBalanceList(5, new BigDecimal(6), securityExpirationList, InvestmentSecurityOptionTypes.PUT);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = new BigDecimal(30);

		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionBalanceList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}


	@Test
	public void testGetUnexpiredContractCount_OARS_withEntriesOutOfDateRange() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("09/03/18", "09/10/18", "09/14/18", "10/8/2018", "10/10/2018", "10/12/2018", "10/15/2018", "10/18/2018");
		List<AccountingPositionBalance> accountingPositionBalanceList = generateAccountingPositionBalanceList(8, new BigDecimal(6), securityExpirationList, InvestmentSecurityOptionTypes.PUT);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = new BigDecimal(30);

		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionBalanceList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}


	@Test
	public void testGetUnexpiredContractCount_DHE() {

		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, DYNAMIC_HEDGED_EQUITY_SERVICE_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("10/8/2018", "10/10/2018", "10/12/2018", "10/15/2018", "10/18/2018");
		List<AccountingPositionBalance> accountingPositionBalanceList = generateAccountingPositionBalanceList(5, new BigDecimal(6), securityExpirationList, InvestmentSecurityOptionTypes.PUT);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = new BigDecimal(30);

		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionBalanceList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}


	@Test
	public void testGetUnexpiredContractCount_RPS() {

		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, RPS_SERVICE_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("10/8/2018", "10/10/2018", "10/12/2018", "10/15/2018", "10/18/2018");
		List<AccountingPositionBalance> accountingPositionBalanceList = generateAccountingPositionBalanceList(5, new BigDecimal(6), securityExpirationList, InvestmentSecurityOptionTypes.PUT);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = new BigDecimal(30);

		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionBalanceList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}


	@Test
	public void testGetUnexpiredContractCount_TDP() {

		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, DYNAMIC_PUT_SELLING_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("10/8/2018", "10/10/2018", "10/12/2018", "10/15/2018", "10/18/2018");
		List<AccountingPositionBalance> accountingPositionBalanceList = generateAccountingPositionBalanceList(5, new BigDecimal(-6), securityExpirationList, InvestmentSecurityOptionTypes.PUT);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = new BigDecimal(30);

		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionBalanceList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}


	@Test
	public void testGetUnexpiredContractCount_three_week_cycle_TDP() {

		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 3, (short) 3, DYNAMIC_PUT_SELLING_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("10/8/2018", "10/10/2018", "10/12/2018");
		List<AccountingPositionBalance> accountingPositionBalanceList = generateAccountingPositionBalanceList(3, new BigDecimal(-6), securityExpirationList, InvestmentSecurityOptionTypes.PUT);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = new BigDecimal(18);

		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionBalanceList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}



	@Test
	public void testGetUnexpiredContractCount_TDC() {

		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, DYNAMIC_CALL_SELLING_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("10/8/2018", "10/10/2018", "10/12/2018", "10/15/2018", "10/18/2018");
		List<AccountingPositionBalance> accountingPositionBalanceList = generateAccountingPositionBalanceList(5, new BigDecimal(-6), securityExpirationList, InvestmentSecurityOptionTypes.CALL);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = new BigDecimal(30);

		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionBalanceList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}


	@Test
	public void testGetUnexpiredContractCount_WrongOptionTypeReturned() {

		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, DYNAMIC_CALL_SELLING_PROCESSING_NAME);
		List<String> securityExpirationList = CollectionUtils.createList("10/8/2018", "10/10/2018", "10/12/2018", "10/15/2018", "10/18/2018");
		List<AccountingPositionBalance> accountingPositionList = generateAccountingPositionBalanceList(5, new BigDecimal(-6), securityExpirationList, InvestmentSecurityOptionTypes.PUT);

		Date balanceDate = DateUtils.toDate("9/14/2018");

		BigDecimal expectedResult = BigDecimal.ZERO;

		// instead of CALL options, the mock simulated a return of PUT options, which should be ignored by the code
		BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.getUnexpiredContractCount(securityTarget, accountingPositionList, balanceDate);

		Assertions.assertEquals(expectedResult, contractCount);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_0_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = new ArrayList<>();
		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 0, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_1_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 1, securityTarget);
	}


	@Test
	@Disabled
	public void testCalculateContractsForCurrentTrade_1_excess_contracts_cycle_not_supported() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 6, OARS_SERVICE_PROCESSING_NAME);

		TestUtils.expectException(ValidationException.class, () -> {
			List<Date> expectedDistributionDates = CollectionUtils.createList(
					DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0))
			);
			testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 1, securityTarget);
		}, "Excess contract calculations are currently not supported for services with 12 tranches and a trade cycle duration of 6 weeks.");
	}


	@Test
	@Disabled
	public void testCalculateContractsForCurrentTrade_1_excess_contracts_tranche_count_not_supported() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 16, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		TestUtils.expectException(ValidationException.class, () -> {
			List<Date> expectedDistributionDates = CollectionUtils.createList(
					DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0))
			);
			testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 1, securityTarget);
		}, "Excess contract calculations are currently not supported for services with 16 tranches and a trade cycle duration of 4 weeks.");
	}


	@Test
	public void testCalculateContractsForCurrentTrade_2_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 2, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_3_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 3, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_4_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 4, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_5_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(4))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 5, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_6_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(4)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(5))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 6, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_7_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(4)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(5)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(6))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 7, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_8_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(4)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(5)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(6)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(7))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 8, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_9_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(4)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(5)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(6)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(7)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(8))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 9, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_10_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(4)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(5)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(6)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(7)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(8)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(9))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 10, securityTarget);
	}


	@Test
	public void testCalculateContractsForCurrentTrade_11_excess_contracts() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(1)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(2)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(3)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(4)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(5)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(6)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(7)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(8)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(9)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_4_WEEK_CYCLE_LIST_12_TRANCHES.get(10))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 11, securityTarget);
	}


	@Test
	void testCalculateContractsForCurrentTrade_1_excess_contracts_3_week_duration() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 3, (short) 3, DYNAMIC_PUT_SELLING_PROCESSING_NAME);

		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_3_WEEK_CYCLE_LIST_3_TRANCHES.get(0))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 1, securityTarget);
	}


	@Test
	void testCalculateContractsForCurrentTrade_2_excess_contracts_3_week_duration() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 3, (short) 3, DYNAMIC_PUT_SELLING_PROCESSING_NAME);
		List<Date> expectedDistributionDates = CollectionUtils.createList(
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_3_WEEK_CYCLE_LIST_3_TRANCHES.get(0)),
				DateUtils.addDays(this.START_OF_CYCLE, this.DAY_OFFSET_3_WEEK_CYCLE_LIST_3_TRANCHES.get(1))
		);

		testExcessContractDates(this.START_OF_CYCLE, expectedDistributionDates, 2, securityTarget);
	}


	@Test
	public void testGetCycleDurationWeeks_from_securityTarget() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		Short cycleDurationWeeks = this.tradeGroupTradeCycleUtilHandler.getCycleDurationWeeks(securityTarget);

		Short expectedValue = 4;
		Assertions.assertEquals(expectedValue, cycleDurationWeeks);
	}


	@Test
	public void testGetCycleDurationWeeks_from_customField() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);
		securityTarget.setCycleDurationWeeks(null);
		securityTarget.setTrancheCount(null);

		Short cycleDurationWeeks = this.tradeGroupTradeCycleUtilHandler.getCycleDurationWeeks(securityTarget);

		Short expectedValue = 4;
		Assertions.assertEquals(expectedValue, cycleDurationWeeks);
	}


	@Test
	public void testGetTrancheCount_from_securityTarget() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);

		Short cycleDurationWeeks = this.tradeGroupTradeCycleUtilHandler.getTrancheCount(securityTarget);

		Short expectedValue = 12;
		Assertions.assertEquals(expectedValue, cycleDurationWeeks);
	}


	@Test
	public void testGetTrancheCount_from_customField() {
		InvestmentAccountSecurityTarget securityTarget = getTestSecurityTarget("0000-0001", (short) 12, (short) 4, OARS_SERVICE_PROCESSING_NAME);
		securityTarget.setCycleDurationWeeks(null);
		securityTarget.setTrancheCount(null);

		Short cycleDurationWeeks = this.tradeGroupTradeCycleUtilHandler.getTrancheCount(securityTarget);

		Short expectedValue = 12;
		Assertions.assertEquals(expectedValue, cycleDurationWeeks);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void testExcessContractDates(Date startOfCycleDate, List<Date> expectedDistributionDateList, int excessContracts, InvestmentAccountSecurityTarget securityTarget) {

		BigDecimal distributedContracts = BigDecimal.ZERO;
		Set<Date> distributionDates = new HashSet<>();
		BigDecimal excessContractCount = new BigDecimal(excessContracts);
		int dayCount = securityTarget.getCycleDurationWeeks() * 7;


		for (int daysOffset = 0; daysOffset < dayCount; ++daysOffset) {
			Date balanceDate = DateUtils.addDays(startOfCycleDate, daysOffset);
			BigDecimal contractCount = this.tradeGroupTradeCycleUtilHandler.calculateExcessContractDistributionForCurrentTrade(securityTarget, balanceDate, excessContractCount);
			distributedContracts = MathUtils.add(distributedContracts, contractCount);

			if (MathUtils.isEqual(contractCount, BigDecimal.ONE)) {
				distributionDates.add(balanceDate);
			}
		}

		Assertions.assertEquals(excessContractCount, distributedContracts);
		Set<Date> expectedDistributionDateSet = new HashSet<>(expectedDistributionDateList);
		Assertions.assertEquals(expectedDistributionDateSet, distributionDates);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates the specified number of trade objects with the quantityIntended set to the quantityIntended parameter value.
	 */
	private List<AccountingPositionBalance> generateAccountingPositionBalanceList(int count, BigDecimal quantity, List<String> securityExpiryDateList, InvestmentSecurityOptionTypes investmentOptionType) {

		Date testDate = DateUtils.toDate("5/5/2020");
		List<AccountingPosition> accountingPositionList = new ArrayList<>();

		for (int i = 0; i < count; ++i) {
			InvestmentSecurity security = InvestmentTestObjectFactory.newInvestmentSecurity(17216 + i, "IBM US " + securityExpiryDateList.get(i) + 500 + i, "Options", BigDecimal.valueOf(100.0), "USD");
			security.setEndDate(DateUtils.toDate(securityExpiryDateList.get(i)));
			security.setOptionType(investmentOptionType);
			AccountingJournalDetail detail = AccountingTestObjectFactory.newAccountingJournalDetail(security, quantity, BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE, testDate);
			accountingPositionList.add(AccountingPosition.forOpeningTransaction(detail));
		}

		return AccountingPositionUtils.mergeAccountingPositionList(accountingPositionList);
	}


	/**
	 * Creates and returns an InvestmentAccountSecurityTarget for use in testing.
	 */
	private InvestmentAccountSecurityTarget getTestSecurityTarget(String clientAccountNumber, Short trancheCount, Short cycleDuration, String businessServiceProcessingTypeName) {
		InvestmentAccountSecurityTarget securityTarget = new InvestmentAccountSecurityTarget();
		securityTarget.setClientInvestmentAccount(new InvestmentAccount());
		BusinessServiceProcessingType businessServiceProcessingType = this.businessServiceProcessingTypeDAO.findOneByField("name", businessServiceProcessingTypeName);
		AssertUtils.assertNotNull(businessServiceProcessingType.getId(), "No ID on BusinessServiceProcessingType");
		securityTarget.getClientInvestmentAccount().setServiceProcessingType(businessServiceProcessingType);
		AssertUtils.assertNotNull(businessServiceProcessingType, "Cannot find BusinessServiceProcessingType with name: " + businessServiceProcessingTypeName);
		BusinessService businessService = new BusinessService();
		securityTarget.getClientInvestmentAccount().setNumber(clientAccountNumber);
		securityTarget.getClientInvestmentAccount().setBusinessService(businessService);
		securityTarget.setTrancheCount(trancheCount);
		securityTarget.setCycleDurationWeeks(cycleDuration);
		securityTarget.setTargetUnderlyingSecurity(new InvestmentSecurity());
		securityTarget.getTargetUnderlyingSecurity().setSymbol("SPX");
		return securityTarget;
	}
}
