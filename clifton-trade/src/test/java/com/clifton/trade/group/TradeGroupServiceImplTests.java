package com.clifton.trade.group;


import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.workflow.definition.WorkflowStatus;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@ContextConfiguration(locations = "../TradeServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class TradeGroupServiceImplTests {

	private static final Integer FUTURE_SECURITY_ID = 10;

	@Resource
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	private TradeGroupService tradeGroupService;


	@Test
	public void testApplyProportionally_BondTrade() {
		TradeEntry tradeEntry = new TradeEntry();
		tradeEntry.setOriginalFace(new BigDecimal("12500000"));
		Trade trade = new Trade();

		trade.setOriginalFace(new BigDecimal("5000000"));
		Assertions.assertEquals(new BigDecimal("331.17"), ((TradeGroupServiceImpl) this.tradeGroupService).applyProportionally(new BigDecimal("827.92"), trade, tradeEntry));

		trade.setOriginalFace(new BigDecimal("3000000"));
		Assertions.assertEquals(new BigDecimal("198.70"), ((TradeGroupServiceImpl) this.tradeGroupService).applyProportionally(new BigDecimal("827.92"), trade, tradeEntry));

		trade.setOriginalFace(new BigDecimal("1500000"));
		Assertions.assertEquals(new BigDecimal("99.35"), ((TradeGroupServiceImpl) this.tradeGroupService).applyProportionally(new BigDecimal("827.92"), trade, tradeEntry));
	}


	@Test
	public void saveTradeEntry_SameClientAccount() {
		TradeEntry te = setupTradeEntry();
		InvestmentAccount clientAccount = InvestmentTestObjectFactory.newInvestmentAccount_ClientDefault();
		InvestmentAccount holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault();

		TradeEntryDetail d1 = new TradeEntryDetail();
		d1.setClientInvestmentAccount(clientAccount);
		d1.setHoldingInvestmentAccount(holdingAccount);
		d1.setQuantityIntended(new BigDecimal(10));

		TradeEntryDetail d2 = new TradeEntryDetail();
		d2.setClientInvestmentAccount(clientAccount);
		d2.setHoldingInvestmentAccount(holdingAccount);
		d2.setQuantityIntended(new BigDecimal(20));

		List<TradeEntryDetail> list = new ArrayList<>();
		list.add(d1);
		list.add(d2);
		te.setTradeDetailList(list);

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> this.tradeGroupService.saveTradeEntry(te));
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Each Client + Holding Account selection for Multi-Client Trade Entry must be unique. The account [100000: Test Client Account]-[H-200: Test Holding Account (Broker)] has more than one trade entry listed."));
	}


	@Test
	public void saveTradeEntry_DifferentClientAccounts() {
		Mockito.when(
				this.investmentAccountRelationshipService.isInvestmentAccountRelationshipForPurposeValid(ArgumentMatchers.anyInt(), ArgumentMatchers.eq(50), ArgumentMatchers.contains("Trading: Futures"),
						ArgumentMatchers.any())).thenReturn(true);

		WorkflowStatus active = new WorkflowStatus();
		active.setName("Active");

		TradeEntry te = setupTradeEntry();
		InvestmentAccount clientAccount = new InvestmentAccount(1, "Test Client Account 1");
		clientAccount.setNumber("000001");
		clientAccount.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		clientAccount.setWorkflowStatus(active);
		InvestmentAccount holdingAccount = new InvestmentAccount(50, "Test Holding Account 1");
		holdingAccount.setType(new InvestmentAccountType()); // Avoid Null Pointer
		holdingAccount.setBaseCurrency(InvestmentSecurityBuilder.newUSD()); //Avoid Null Pointer
		holdingAccount.setWorkflowStatus(active);

		TradeEntryDetail d1 = new TradeEntryDetail();
		d1.setClientInvestmentAccount(clientAccount);
		d1.setHoldingInvestmentAccount(holdingAccount);
		d1.setQuantityIntended(new BigDecimal(10));
		d1.setExpectedUnitPrice(new BigDecimal("1000"));

		InvestmentAccount clientAccount2 = new InvestmentAccount(2, "Test Client Account 2");
		clientAccount2.setNumber("000002");
		clientAccount2.setBaseCurrency(InvestmentSecurityBuilder.newUSD());
		clientAccount2.setWorkflowStatus(active);

		TradeEntryDetail d2 = new TradeEntryDetail();
		d2.setClientInvestmentAccount(clientAccount2);
		d2.setHoldingInvestmentAccount(holdingAccount);
		d2.setQuantityIntended(new BigDecimal(20));
		d2.setExpectedUnitPrice(new BigDecimal("1000"));

		List<TradeEntryDetail> list = new ArrayList<>();
		list.add(d1);
		list.add(d2);
		te.setTradeDetailList(list);
		this.tradeGroupService.saveTradeEntry(te);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeEntry setupTradeEntry() {
		// Trade Entry Save method checks Ids and re-populates object from the db, so just set the id value
		// for real db objects in context
		TradeEntry te = new TradeEntry();
		InvestmentSecurity usd = this.investmentInstrumentService.getInvestmentSecurity(InvestmentSecurityBuilder.newUSD().getId());
		InvestmentSecurity fut = this.investmentInstrumentService.getInvestmentSecurity(FUTURE_SECURITY_ID);
		SecurityUser trader = new SecurityUser();
		trader.setId(new Integer(1).shortValue());
		TradeType futures = new TradeType();
		futures.setId(MathUtils.SHORT_ONE);
		futures.setName("Futures");
		TradeDestination manual = this.tradeDestinationService.getTradeDestination(MathUtils.SHORT_ONE);

		te.setPayingSecurity(usd);
		te.setInvestmentSecurity(fut);
		te.setTradeType(futures);
		te.setTraderUser(trader);
		te.setTradeDestination(manual);
		te.setQuantityIntended(BigDecimal.valueOf(30));
		te.setExecutingBrokerCompany(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany());

		Date date = DateUtils.toDate("1/1/2013");
		te.setTradeDate(date);
		te.setSettlementDate(date);

		return te;
	}
}
