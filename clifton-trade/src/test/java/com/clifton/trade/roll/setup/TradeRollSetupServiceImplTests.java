package com.clifton.trade.roll.setup;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@ContextConfiguration
public class TradeRollSetupServiceImplTests extends BaseInMemoryDatabaseTests {

	private static final String DEFAULT_FUTURE_ROLLS_NAME = "Default Future Rolls";

	@Resource
	private TradeRollSetupService tradeRollSetupService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveTradeRollDatesNotActiveOnStartDate() {
		TradeRollDate tradeRollDate = new TradeRollDate();
		tradeRollDate.setRollType(this.tradeRollSetupService.getTradeRollTypeByName(DEFAULT_FUTURE_ROLLS_NAME));
		tradeRollDate.setRollFromSecurity(getSecurityBySymbol("1XU2"));
		tradeRollDate.setDefaultNewSecurity(getSecurityBySymbol("1XV2"));
		tradeRollDate.setStartDate(DateUtils.toDate("12/25/2012"));

		TestUtils.expectException(ValidationException.class, () -> this.tradeRollSetupService.saveTradeRollDate(tradeRollDate), "Default New Security is invalid because it is not active on the Roll Start date.");
	}


	@Test
	public void testSaveTradeRollDatesInvalidStartEndDate() {
		TradeRollDate tradeRollDate = new TradeRollDate();
		tradeRollDate.setRollType(this.tradeRollSetupService.getTradeRollTypeByName(DEFAULT_FUTURE_ROLLS_NAME));
		tradeRollDate.setRollFromSecurity(getSecurityBySymbol("1XU2"));
		tradeRollDate.setStartDate(DateUtils.toDate("09/25/2012"));
		tradeRollDate.setEndDate(DateUtils.toDate("08/25/2012"));

		TestUtils.expectException(FieldValidationException.class, () -> this.tradeRollSetupService.saveTradeRollDate(tradeRollDate), "End date must be after start date 2012-09-25");
	}


	@Test
	public void testSaveTradeRollDatesDifferentInstruments() {
		TradeRollDate tradeRollDate = new TradeRollDate();
		tradeRollDate.setRollType(this.tradeRollSetupService.getTradeRollTypeByName(DEFAULT_FUTURE_ROLLS_NAME));
		tradeRollDate.setRollFromSecurity(getSecurityBySymbol("1XU2"));
		tradeRollDate.setDefaultNewSecurity(getSecurityBySymbol("2XU2"));
		tradeRollDate.setStartDate(DateUtils.toDate("09/25/2012"));
		tradeRollDate.setEndDate(DateUtils.toDate("09/25/2012"));

		TestUtils.expectException(FieldValidationException.class, () -> this.tradeRollSetupService.saveTradeRollDate(tradeRollDate), "Roll From Security [1XU2] belongs to instrument [1X: Test Instrument 1], but Default New Security [2XU2] belongs to instrument [2X: Test Instrument 2].  Both securities must belong to the same instrument.");
	}


	@Test
	public void testSaveTradeRollDatesUpdateCurrentSecurities() {
		Date startDate = new Date();

		InvestmentSecurity rollFromSecurity = getSecurityBySymbol("2XU2");
		InvestmentSecurity rollToSecurity = getSecurityBySymbol("2XV2");
		InvestmentSecurity currentSecurity = getInvestmentSecurityCurrentForInstrument(rollFromSecurity.getInstrument().getId());

		Assertions.assertEquals(rollFromSecurity, currentSecurity, "Expected [" + rollFromSecurity.getLabel() + "] to be the current security before save roll dates.");

		TradeRollType tradeRollType = this.tradeRollSetupService.getTradeRollTypeByName(DEFAULT_FUTURE_ROLLS_NAME);

		TradeRollDate bean = new TradeRollDate();
		bean.setRollType(tradeRollType);
		bean.setRollFromSecurity(rollFromSecurity);
		bean.setDefaultNewSecurity(rollToSecurity);
		bean.setStartDate(startDate);
		bean.setEndDate(startDate);
		this.tradeRollSetupService.saveTradeRollDate(bean);

		currentSecurity = getInvestmentSecurityCurrentForInstrument(rollFromSecurity.getInstrument().getId());

		Assertions.assertEquals(rollToSecurity, currentSecurity, "Expected [" + rollToSecurity.getLabel() + "] to be the current security AFTER save roll dates.");

		Assertions.assertTrue(this.tradeRollSetupService.isTradeRollSecurityOnDate(rollFromSecurity.getId(), startDate),
				"Expected TRUE that we are currently rolling FROM security " + rollFromSecurity.getLabel());
		Assertions.assertFalse(this.tradeRollSetupService.isTradeRollSecurityOnDate(rollFromSecurity.getId(), DateUtils.addDays(startDate, -1)),
				"Expected FALSE that yesterday we were rolling FROM security " + rollFromSecurity.getLabel());

		Assertions.assertTrue(this.tradeRollSetupService.isTradeRollToSecurityOnDate(rollToSecurity.getId(), startDate),
				"Expected TRUE that we are currently rolling TO security " + rollToSecurity.getLabel());
		Assertions.assertFalse(this.tradeRollSetupService.isTradeRollToSecurityOnDate(rollToSecurity.getId(), DateUtils.addDays(startDate, -1)),
				"Expected FALSE that yesterday we were rolling TO security " + rollToSecurity.getLabel());

		// Change the Default New Security on the Roll Date to confirm the group was updated
		InvestmentSecurity newRollToSecurity = getSecurityBySymbol("2XX2");
		bean.setDefaultNewSecurity(newRollToSecurity);
		this.tradeRollSetupService.saveTradeRollDate(bean);


		currentSecurity = getInvestmentSecurityCurrentForInstrument(rollFromSecurity.getInstrument().getId());

		Assertions.assertEquals(newRollToSecurity, currentSecurity, "Expected [" + newRollToSecurity.getLabel() + "] to be the current security AFTER updating roll dates.");
	}


	@Test
	public void testSaveTradeRollDatesDoNotUpdateCurrentSecurities() {
		Date startDate = DateUtils.addDays(new Date(), 1);

		InvestmentSecurity rollFromSecurity = getSecurityBySymbol("2XU2");
		InvestmentSecurity rollToSecurity = getSecurityBySymbol("2XV2");
		InvestmentSecurity currentSecurity = getInvestmentSecurityCurrentForInstrument(rollFromSecurity.getInstrument().getId());

		Assertions.assertEquals(rollFromSecurity, currentSecurity, "Expected [" + rollFromSecurity.getLabel() + "] to be the current security before save roll dates.");

		TradeRollDate bean = new TradeRollDate();
		bean.setRollType(this.tradeRollSetupService.getTradeRollTypeByName(DEFAULT_FUTURE_ROLLS_NAME));
		bean.setRollFromSecurity(rollFromSecurity);
		bean.setDefaultNewSecurity(rollToSecurity);
		bean.setStartDate(startDate);
		bean.setEndDate(startDate);
		this.tradeRollSetupService.saveTradeRollDate(bean);

		currentSecurity = getInvestmentSecurityCurrentForInstrument(rollFromSecurity.getInstrument().getId());

		Assertions.assertEquals(rollFromSecurity, currentSecurity, "Expected [" + rollFromSecurity.getLabel() + "] to STILL be the current security AFTER save roll dates.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Special method because investment method that does this goes against security table and does a where exists which xml dao's don't support
	 */
	private InvestmentSecurity getInvestmentSecurityCurrentForInstrument(int instrumentId) {
		InvestmentSecurityGroup group = this.investmentSecurityGroupService.getInvestmentSecurityGroupByName(InvestmentSecurityGroupServiceImpl.INVESTMENT_SECURITY_CURRENT_GROUP);
		List<InvestmentSecurityGroupSecurity> list = this.investmentSecurityGroupService.getInvestmentSecurityGroupSecurityListByGroup(group.getId());
		InvestmentSecurityGroupSecurity current = CollectionUtils.getOnlyElement(BeanUtils.filter(list, investmentSecurityGroupSecurity -> investmentSecurityGroupSecurity.getReferenceTwo().getInstrument().getId(), instrumentId));
		if (current != null) {
			return current.getReferenceTwo();
		}
		return null;
	}


	private InvestmentSecurity getSecurityBySymbol(String symbol) {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setSymbol(symbol);
		return CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm));
	}
}
