SELECT 'InvestmentInstrumentHierarchy' AS entityTableName, InvestmentInstrumentHierarchyID AS entityId
FROM InvestmentInstrumentHierarchy
WHERE InvestmentInstrumentHierarchyID = 6
UNION
SELECT 'InvestmentSecurity' AS entitytablename, InvestmentSecurityID AS EntityID
FROM InvestmentSecurity
WHERE Symbol = 'USD'
  AND IsCurrency = 1
UNION
SELECT 'SystemBean', SystemBeanID
FROM SystemBean
WHERE SystemBeanName = 'Rebuild Security Group Linked to Trade Roll Type';
