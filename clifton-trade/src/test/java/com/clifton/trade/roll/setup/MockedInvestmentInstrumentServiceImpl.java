package com.clifton.trade.roll.setup;


import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.setup.group.InvestmentSecurityGroupSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroupServiceImpl;

import java.util.List;


/**
 * The <code>MockedInvestmentInstrumentServiceImpl</code> ...
 *
 * @author manderson
 */
public class MockedInvestmentInstrumentServiceImpl extends InvestmentSecurityGroupServiceImpl {

	@Override
	public List<InvestmentSecurityGroupSecurity> getInvestmentSecurityGroupSecurityListByInstrument(final short groupId, final int instrumentId) {
		List<InvestmentSecurityGroupSecurity> list = getInvestmentSecurityGroupSecurityListByGroup(groupId);
		return BeanUtils.filter(list, investmentSecurityGroupSecurity -> investmentSecurityGroupSecurity.getReferenceTwo().getInstrument().getId(), instrumentId);
	}
}
