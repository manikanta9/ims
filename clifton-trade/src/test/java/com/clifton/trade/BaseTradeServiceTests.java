package com.clifton.trade;

import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.builder.TradeDestinationBuilder;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * Class to hold common resources for {@link Trade} related service tests (e.g. {@link TradeServiceImplTests} and {@link TradeManagementServiceImplTests}).
 *
 * @author michaelm
 */
@ExtendWith(SpringExtension.class)
public abstract class BaseTradeServiceTests {

	protected static final short TRADE_DESTINATION_ID = 1;

	protected static final int EXECUTING_BROKER_ID = 7;

	protected static final short FUTURES_TRADE_TYPE_ID = 1;
	protected static final short FORWARDS_TRADE_TYPE_ID = 2;

	protected static final int FAU3_SECURITY_ID = 11;
	protected static final int TYM7_SECURITY_ID = 12;
	protected static final int TUH0_SECURITY_ID = 13;
	protected static final int EUR_USD20170621_SECURITY_ID = 14;

	protected static final short TEST_USER_SECURITY_USER_ID = 1;


	protected static final InvestmentSecurity TUH0 = InvestmentSecurityBuilder.newFuture("TUH0")
			.withId(TUH0_SECURITY_ID)
			.withPriceMultiplier(2000)
			.build();
	protected static final InvestmentSecurity TYM7 = InvestmentSecurityBuilder.newFuture("TYM7")
			.withId(TYM7_SECURITY_ID)
			.withPriceMultiplier(1000)
			.withNotionalCalculator(InvestmentNotionalCalculatorTypes.MONEY_UNIT_HALF_UP)
			.build();
	protected static final InvestmentSecurity FAU3 = InvestmentSecurityBuilder.newFuture("FAU3")
			.withId(FAU3_SECURITY_ID)
			.withPriceMultiplier(100)
			.build();

	@Resource
	protected TradeService tradeService;

	@Resource
	protected TradeManagementService tradeManagementService;

	@Resource
	protected TemplateConverter templateConverter;

	@Resource
	protected ReadOnlyDAO<Trade> tradeDAO;

	@Resource
	protected InvestmentAccountRelationshipService investmentAccountRelationshipService;

	@Resource
	protected SystemConditionService systemConditionService;

	@Resource
	protected TradeDestinationService tradeDestinationService;

	@Resource
	protected SecurityUserService securityUserService;

	@Resource
	protected SecurityAuthorizationService securityAuthorizationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected Trade initTrade(WorkflowStatus workflowStatus) {
		Trade trade = initTrade();
		trade.setWorkflowStatus(workflowStatus);
		return trade;
	}


	protected Trade initTrade() {
		Trade trade = TradeBuilder.newTradeForSecurity(TYM7)
				.withExpectedUnitPrice("125.640625")
				.withQuantityIntended(5)
				.withTradeDestination(TradeDestinationBuilder.createTradeDestination(TRADE_DESTINATION_ID).toTradeDestination())
				.build();

		trade.getTradeType().setId(FUTURES_TRADE_TYPE_ID);
		trade.getExecutingBrokerCompany().setId(EXECUTING_BROKER_ID);
		return trade;
	}


	protected void validateSplitTrade(Trade trade, BigDecimal quantity, BigDecimal commissionPerUnit, BigDecimal feeAmount) {
		Assertions.assertEquals(quantity, trade.getQuantityIntended());
		Assertions.assertEquals(commissionPerUnit, trade.getCommissionPerUnit());
		Assertions.assertEquals(feeAmount, trade.getFeeAmount());
	}
}
