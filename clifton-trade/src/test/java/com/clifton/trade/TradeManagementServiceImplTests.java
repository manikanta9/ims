package com.clifton.trade;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.security.user.SecurityUser;
import com.clifton.workflow.definition.WorkflowStatusBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>TradeManagementServiceImplTests</code> test the logic in the {@link TradeManagementService}.
 */
@ContextConfiguration("TradeServiceImplTests-context.xml")
@ExtendWith({SpringExtension.class})
public class TradeManagementServiceImplTests extends BaseTradeServiceTests {


	@BeforeEach
	public void setupTestAccountRelationshipForPurposes() {
		Mockito.when(
				this.investmentAccountRelationshipService.isInvestmentAccountRelationshipForPurposeValid(ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_CLIENT_DEFAULT), ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_HOLDING_DEFAULT), ArgumentMatchers.contains("Trading: Futures"),
						ArgumentMatchers.any())).thenReturn(true);
		Mockito.when(
				this.investmentAccountRelationshipService.isInvestmentAccountRelationshipForPurposeValid(ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_CLIENT_DEFAULT), ArgumentMatchers.eq(InvestmentTestObjectFactory.INVESTMENT_ACCOUNT_HOLDING_DEFAULT), ArgumentMatchers.contains("Trading: Forwards"),
						ArgumentMatchers.any())).thenReturn(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                 Test Trade Reassignment               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testReassignTrade_WorkflowStatus_Draft() {
		Trade trade = this.tradeService.saveTrade(initTrade(WorkflowStatusBuilder.createDraft().toWorkflowStatus()));
		Mockito.when(this.securityAuthorizationService.isSecurityAccessAllowed(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(true);
		Status status = testReassignedTraderUserEquals(TEST_USER_SECURITY_USER_ID, trade);
		Assertions.assertEquals(0, status.getErrorCount());
	}


	@Test
	public void testReassignTrade_WorkflowStatus_Pending() {
		Trade trade = this.tradeService.saveTrade(initTrade(WorkflowStatusBuilder.createPending().toWorkflowStatus()));
		Mockito.when(this.securityAuthorizationService.isSecurityAccessAllowed(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(true);
		Status status = testReassignedTraderUserEquals(TEST_USER_SECURITY_USER_ID, trade);
		Assertions.assertEquals(0, status.getErrorCount());
	}


	@Test
	public void testReassignTrade_WorkflowStatus_Approved() {
		Trade trade = this.tradeService.saveTrade(initTrade(WorkflowStatusBuilder.createApproved().toWorkflowStatus()));
		Mockito.when(this.securityAuthorizationService.isSecurityAccessAllowed(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(true);
		Status status = testReassignedTraderUserEquals(TEST_USER_SECURITY_USER_ID, trade);
		Assertions.assertEquals(0, status.getErrorCount());
	}


	@Test
	public void testReassignTrade_WorkflowStatus_Active_Fail() {
		Trade trade = this.tradeService.saveTrade(initTrade(WorkflowStatusBuilder.createActive().toWorkflowStatus()));
		Mockito.when(this.securityAuthorizationService.isSecurityAccessAllowed(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(true);
		Status status = testReassignedTraderUserEquals(TEST_USER_SECURITY_USER_ID, trade);

		String expectedErrorMessage = String.format("%s with Workflow Status [%s] cannot be reassigned because Trades can only be reassigned in workflow status [%s].",
				String.format("Error encountered while reassigning trade: Trade [%s]", trade.getLabel()), trade.getWorkflowStatus().getName(), StringUtils.join(TradeManagementServiceImpl.REASSIGNABLE_WORKFLOW_STATUS_NAME_LIST, ", "));
		Assertions.assertEquals(1, status.getErrorCount());
		Assertions.assertEquals(expectedErrorMessage, status.getDetailList().get(0).getNote());
	}


	@Test
	public void testReassignTrade_NewAssigneeMissingWriteAccess() {
		Trade trade = this.tradeService.saveTrade(initTrade(WorkflowStatusBuilder.createActive().toWorkflowStatus()));
		SecurityUser newAssignee = this.securityUserService.getSecurityUser(TEST_USER_SECURITY_USER_ID);
		Mockito.when(this.securityAuthorizationService.isSecurityAccessAllowed(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyInt())).thenReturn(false);
		Status status = testReassignedTraderUserEquals(newAssignee, trade);

		String expectedErrorMessage = String.format("%s cannot be reassigned to [%s] because they do not have write permissions to [%s].",
				String.format("Error encountered while reassigning trade: Trade [%s]", trade.getLabel()), newAssignee.getLabel(), Trade.class.getName());
		Assertions.assertEquals(1, status.getErrorCount());
		Assertions.assertEquals(expectedErrorMessage, status.getDetailList().get(0).getNote());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Status testReassignedTraderUserEquals(short newAssigneeUserId, Trade trade) {
		return testReassignedTraderUserEquals(newAssigneeUserId, Collections.singletonList(trade));
	}


	private Status testReassignedTraderUserEquals(short newAssigneeUserId, List<Trade> tradeList) {
		return testReassignedTraderUserEquals(this.securityUserService.getSecurityUser(newAssigneeUserId), tradeList);
	}


	private Status testReassignedTraderUserEquals(SecurityUser newAssignee, Trade trade) {
		return testReassignedTraderUserEquals(newAssignee, Collections.singletonList(trade));
	}


	private Status testReassignedTraderUserEquals(SecurityUser newAssignee, List<Trade> tradeList) {
		Status status = tradeReassignExecute(newAssignee, tradeList);
		if (status.getErrorCount() == 0) {
			List<Trade> updatedTradeList = CollectionUtils.getStream(tradeList).map(entry -> this.tradeService.getTrade(entry.getId())).collect(Collectors.toList());
			assertTraderUserEquals(newAssignee, updatedTradeList);
		}
		return status;
	}


	private void assertTraderUserEquals(SecurityUser newAssignee, List<Trade> reassignedTradeList) {
		CollectionUtils.getIterable(reassignedTradeList).forEach(reassignedTrade ->
				ValidationUtils.assertEquals(newAssignee, reassignedTrade.getTraderUser(), String.format("New Assignee [%s] should be the Trader for all reassigned Trades [%s].",
						newAssignee.getUserName(), StringUtils.join(reassignedTradeList, Trade::getLabel, ", "))));
	}


	private Status tradeReassignExecute(SecurityUser newAssignee, List<Trade> tradeList) {
		TradeListActionCommand reassignmentCommand = TradeListActionCommand.forReassignWithAssigneeAndTradeList(newAssignee, tradeList);
		return this.tradeManagementService.executeTradeListActionCommand(reassignmentCommand);
	}
}
