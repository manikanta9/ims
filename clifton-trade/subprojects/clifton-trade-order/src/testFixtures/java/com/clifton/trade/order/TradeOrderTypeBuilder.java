package com.clifton.trade.order;

import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;


public class TradeOrderTypeBuilder {

	private TradeOrderType tradeOrderType;


	private TradeOrderTypeBuilder(TradeOrderType tradeOrderType) {
		this.tradeOrderType = tradeOrderType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeOrderTypeBuilder createEmptyTradeOrderType() {
		return new TradeOrderTypeBuilder(new TradeOrderType());
	}


	public static TradeOrderTypeBuilder createExecuteSeparately() {
		TradeOrderType tradeOrderType = new TradeOrderType();

		tradeOrderType.setId((short) 1);
		tradeOrderType.setName("EXECUTE_SEPARATELY");
		tradeOrderType.setDescription("Execute all trades individually, 1 order per trade.");
		tradeOrderType.setGroupingType(TradeOrderGroupingTypes.EXECUTE_SEPARATELY);

		return new TradeOrderTypeBuilder(tradeOrderType);
	}


	public static TradeOrderTypeBuilder createDefaultGrouping() {
		TradeOrderType tradeOrderType = new TradeOrderType();

		tradeOrderType.setId((short) 2);
		tradeOrderType.setName("DEFAULT");
		tradeOrderType.setDescription("Default grouping by security, direction, etc.");
		tradeOrderType.setGroupingType(TradeOrderGroupingTypes.DEFAULT);

		return new TradeOrderTypeBuilder(tradeOrderType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderTypeBuilder withId(short id) {
		getTradeOrderType().setId(id);
		return this;
	}


	public TradeOrderTypeBuilder withName(String name) {
		getTradeOrderType().setName(name);
		return this;
	}


	public TradeOrderTypeBuilder withDescription(String description) {
		getTradeOrderType().setDescription(description);
		return this;
	}


	public TradeOrderType toTradeOrderType() {
		return this.tradeOrderType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrderType getTradeOrderType() {
		return this.tradeOrderType;
	}
}
