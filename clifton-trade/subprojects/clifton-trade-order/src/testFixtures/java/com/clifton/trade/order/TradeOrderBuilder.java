package com.clifton.trade.order;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class TradeOrderBuilder {

	private TradeOrder tradeOrder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrderBuilder(TradeOrder tradeOrder) {
		this.tradeOrder = tradeOrder;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeOrderBuilder createEmptyTradeOrder() {
		return new TradeOrderBuilder(new TradeOrder());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderBuilder withOriginalTradeOrder(TradeOrder originalTradeOrder) {
		getTradeOrder().setOriginalTradeOrder(originalTradeOrder);
		return this;
	}


	public TradeOrderBuilder withStatus(TradeOrderStatus status) {
		getTradeOrder().setStatus(status);
		return this;
	}


	public TradeOrderBuilder withAllocationStatus(TradeOrderAllocationStatus allocationStatus) {
		getTradeOrder().setAllocationStatus(allocationStatus);
		return this;
	}


	public TradeOrderBuilder withValidationError(boolean validationError) {
		getTradeOrder().setValidationError(validationError);
		return this;
	}


	public TradeOrderBuilder withAllocationValidationError(boolean allocationValidationError) {
		getTradeOrder().setAllocationValidationError(allocationValidationError);
		return this;
	}


	public TradeOrderBuilder withOrderGroup(Integer orderGroup) {
		getTradeOrder().setOrderGroup(orderGroup);
		return this;
	}


	public TradeOrderBuilder withActiveOrder(boolean activeOrder) {
		getTradeOrder().setActiveOrder(activeOrder);
		return this;
	}


	public TradeOrderBuilder withExternalIdentifier(Integer externalIdentifier) {
		getTradeOrder().setExternalIdentifier(externalIdentifier);
		return this;
	}


	public TradeOrderBuilder withInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		getTradeOrder().setInvestmentSecurity(investmentSecurity);
		return this;
	}


	public TradeOrderBuilder withOriginalInvestmentSecurity(InvestmentSecurity originalInvestmentSecurity) {
		getTradeOrder().setOriginalInvestmentSecurity(originalInvestmentSecurity);
		return this;
	}


	public TradeOrderBuilder withSecurityList(List<TradeOrderSecurity> securityList) {
		getTradeOrder().setSecurityList(securityList);
		return this;
	}


	public TradeOrderBuilder withQuantity(BigDecimal quantity) {
		getTradeOrder().setQuantity(quantity);
		return this;
	}


	public TradeOrderBuilder withQuantityFilled(BigDecimal quantityFilled) {
		getTradeOrder().setQuantityFilled(quantityFilled);
		return this;
	}


	public TradeOrderBuilder withAveragePrice(BigDecimal averagePrice) {
		getTradeOrder().setAveragePrice(averagePrice);
		return this;
	}


	public TradeOrderBuilder withCommissionPerUnit(BigDecimal commissionPerUnit) {
		getTradeOrder().setCommissionPerUnit(commissionPerUnit);
		return this;
	}


	public TradeOrderBuilder withBuy(boolean buy) {
		getTradeOrder().setBuy(buy);
		return this;
	}


	public TradeOrderBuilder withTraderUser(SecurityUser traderUser) {
		getTradeOrder().setTraderUser(traderUser);
		return this;
	}


	public TradeOrderBuilder withDestinationMapping(TradeDestinationMapping destinationMapping) {
		getTradeOrder().setDestinationMapping(destinationMapping);
		return this;
	}


	public TradeOrderBuilder withTradeDate(Date tradeDate) {
		getTradeOrder().setTradeDate(tradeDate);
		return this;
	}


	public TradeOrderBuilder withTradeList(List<Trade> tradeList) {
		getTradeOrder().setTradeList(tradeList);
		return this;
	}


	public TradeOrderBuilder withType(TradeOrderType type) {
		getTradeOrder().setType(type);
		return this;
	}


	public TradeOrderBuilder withIndexRatio(BigDecimal indexRatio) {
		getTradeOrder().setIndexRatio(indexRatio);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrder getTradeOrder() {
		return this.tradeOrder;
	}


	public TradeOrder toTradeOrder() {
		return this.tradeOrder;
	}
}
