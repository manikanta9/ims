package com.clifton.trade.order;

public class TradeOrderStatusBuilder {

	private TradeOrderStatus tradeOrderStatus;


	private TradeOrderStatusBuilder(TradeOrderStatus tradeOrderStatus) {
		this.tradeOrderStatus = tradeOrderStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeOrderStatusBuilder createPartiallyFilled() {
		TradeOrderStatus tradeOrderStatus = new TradeOrderStatus();

		tradeOrderStatus.setId((short) 5);
		tradeOrderStatus.setName("Partially Filled");
		tradeOrderStatus.setOrderStatus(TradeOrderStatuses.PARTIALLY_FILLED);
		tradeOrderStatus.setPartialFill(true);

		return new TradeOrderStatusBuilder(tradeOrderStatus);
	}


	public static TradeOrderStatusBuilder createFilled() {
		TradeOrderStatus tradeOrderStatus = new TradeOrderStatus();

		tradeOrderStatus.setId((short) 6);
		tradeOrderStatus.setName("Filled");
		tradeOrderStatus.setOrderStatus(TradeOrderStatuses.FILLED);
		tradeOrderStatus.setFillComplete(true);

		return new TradeOrderStatusBuilder(tradeOrderStatus);
	}


	public static TradeOrderStatusBuilder createPartiallyFilledCanceled() {
		TradeOrderStatus tradeOrderStatus = new TradeOrderStatus();

		tradeOrderStatus.setId((short) 7);
		tradeOrderStatus.setName("Partially Filled Canceled");
		tradeOrderStatus.setOrderStatus(TradeOrderStatuses.PARTIALLY_FILLED_CANCELED);
		tradeOrderStatus.setPartialFill(true);

		return new TradeOrderStatusBuilder(tradeOrderStatus);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderStatusBuilder withId(short id) {
		getTradeOrderStatus().setId(id);
		return this;
	}


	public TradeOrderStatusBuilder withName(String name) {
		getTradeOrderStatus().setName(name);
		return this;
	}


	public TradeOrderStatusBuilder withDescription(String description) {
		getTradeOrderStatus().setDescription(description);
		return this;
	}


	public TradeOrderStatusBuilder withOrderStatus(TradeOrderStatuses tradeOrderStatuses) {
		getTradeOrderStatus().setOrderStatus(tradeOrderStatuses);
		return this;
	}


	public TradeOrderStatusBuilder withCancelIfNotFilled(boolean cancelIfNotFilled) {
		getTradeOrderStatus().setCancelIfNotFilled(cancelIfNotFilled);
		return this;
	}


	public TradeOrderStatusBuilder withPartialFill(boolean partialFill) {
		getTradeOrderStatus().setPartialFill(partialFill);
		return this;
	}


	public TradeOrderStatusBuilder withFillComplete(boolean fillComplete) {
		getTradeOrderStatus().setFillComplete(fillComplete);
		return this;
	}


	public TradeOrderStatus toTradeOrderStatus() {
		return this.tradeOrderStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrderStatus getTradeOrderStatus() {
		return this.tradeOrderStatus;
	}
}
