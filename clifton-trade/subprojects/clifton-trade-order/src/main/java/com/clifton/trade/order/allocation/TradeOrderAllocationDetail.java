package com.clifton.trade.order.allocation;


import java.math.BigDecimal;


/**
 * The <code>TradeOrderAllocationDetail</code> is a non-DAO class that contains the details of the
 * allocation the were receive from the broker or built from the execution reports.
 *
 * @author mwacker
 */
public class TradeOrderAllocationDetail {

	/**
	 * Manually assigned id used to distinguish between details when creating the trade fills.
	 */
	private String id;

	private String allocationAccount;
	private BigDecimal allocationAveragePrice;
	private BigDecimal allocationPrice;
	private BigDecimal allocationShares;
	private BigDecimal allocationNetMoney;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAllocationAccount() {
		return this.allocationAccount;
	}


	public void setAllocationAccount(String allocationAccount) {
		this.allocationAccount = allocationAccount;
	}


	public BigDecimal getAllocationAveragePrice() {
		return this.allocationAveragePrice;
	}


	public void setAllocationAveragePrice(BigDecimal allocationAveragePrice) {
		this.allocationAveragePrice = allocationAveragePrice;
	}


	public BigDecimal getAllocationPrice() {
		return this.allocationPrice;
	}


	public void setAllocationPrice(BigDecimal allocationPrice) {
		this.allocationPrice = allocationPrice;
	}


	public BigDecimal getAllocationShares() {
		return this.allocationShares;
	}


	public void setAllocationShares(BigDecimal allocationShares) {
		this.allocationShares = allocationShares;
	}


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public BigDecimal getAllocationNetMoney() {
		return this.allocationNetMoney;
	}


	public void setAllocationNetMoney(BigDecimal allocationNetMoney) {
		this.allocationNetMoney = allocationNetMoney;
	}
}
