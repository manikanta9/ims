package com.clifton.trade.order.allocation;


public enum TradeOrderAllocationStatuses {
	/**
	 * The allocation was sent, but no acknowledgment has been received.
	 */
	SENT,
	/**
	 * The allocation was not sent due to an error.
	 */
	FAILED_TO_SEND,
	/**
	 * The allocation was received by the counter party.
	 */
	RECEIVED,
	/**
	 * The allocation was accepted by the counter party.  At this stage if the counter party
	 * supports AllocationReports, we wait to receive the report otherwise we book the trade at
	 * the average price.
	 */
	ACCEPTED,
	/**
	 * The allocation was rejected because of an issue with the allocation as a whole.
	 * <p/>
	 * For example, if number of shares we allocated doesn't match the shares in orders
	 * and executions that were allocated.
	 */
	BLOCK_LEVEL_REJECT,
	/**
	 * The allocation was rejected because of an account.
	 */
	ACCOUNT_LEVEL_REJECT,
	/**
	 * Incomplete ??
	 * <p/>
	 * TODO: what is this status?
	 */
	INCOMPLETE,
	/**
	 * Rejected by ???
	 * <p/>
	 * TODO: what is this status?
	 */
	REJECTED_BY_INTERMEDIARY,
	/**
	 * Pending
	 */
	ALLOCATION_PENDING,
	/**
	 * The allocation was reversed.
	 */
	REVERSED,
	/**
	 * The allocation was canceled.
	 */
	CANCELED,
	/**
	 * The allocation is complete and the allocation report has been received.
	 */
	COMPLETE
}
