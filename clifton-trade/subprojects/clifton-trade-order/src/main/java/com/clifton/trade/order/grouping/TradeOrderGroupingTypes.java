package com.clifton.trade.order.grouping;


public enum TradeOrderGroupingTypes {
	/**
	 * Nothing will be grouped, the original list will be returned.
	 */
	EXECUTE_SEPARATELY,
	EXECUTE_SEPARATELY_LIMIT,
	DEFAULT,
	DEFAULT_LIMIT,
	DEFAULT_BY_HOLDING_ACCOUNT,

	/**
	 * Group PUT and CALL options by account and smallest quantity to create
	 * an option strangle, which is a trade of a PUT and CALL at a single quantity.
	 * <p/>
	 * For example:
	 * <p/>
	 * Account	Qty	 Put/Call
	 * 123456	10	 P
	 * 123456	15	 C
	 * <p/>
	 * Results in:
	 * 123456	10	 STRANGLE(P and C)
	 * 123456	5	 C
	 */
	OPTIONS_STRANGLE,
	EXTERNAL_DROP_COPY_ORDER;
}
