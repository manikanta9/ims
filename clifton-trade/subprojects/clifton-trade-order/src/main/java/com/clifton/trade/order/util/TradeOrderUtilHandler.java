package com.clifton.trade.order.util;

import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.trade.Trade;


/**
 * Utility methods for common TradeOrder actions.
 *
 * @author mwacker
 */
public interface TradeOrderUtilHandler {

	/**
	 * Finds a holding account for the client on the trade issued by the provided company.
	 */
	public InvestmentAccount getHoldingAccountForBroker(Trade trade, BusinessCompany executingBroker);
}
