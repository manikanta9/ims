package com.clifton.trade.order.event;

import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderStatuses;

import java.util.Arrays;
import java.util.List;


/**
 * <code>TradeOrderCancellationEvent</code>  represents an {@link com.clifton.core.util.event.Event} for
 * cancelled / rejected trade order statuses.
 */
public class TradeOrderCancellationEvent extends TradeOrderStatusEvent {

	public static final String EVENT_NAME = "Trade Order Cancellation";


	protected static final List<TradeOrderStatuses> cancellationStatuses = Arrays.asList(
			TradeOrderStatuses.CANCELED,
			TradeOrderStatuses.PARTIALLY_FILLED_CANCELED,
			TradeOrderStatuses.REJECTED
	);

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeOrderCancellationEvent(TradeOrder tradeOrder) {
		super(tradeOrder, EVENT_NAME);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static boolean isCancellationStatus(TradeOrderStatuses original, TradeOrderStatuses updated, boolean isActiveOrder) {
		return (!cancellationStatuses.contains(original) && cancellationStatuses.contains(updated) && isActiveOrder);
	}
}
