package com.clifton.trade.order.allocation;


import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationSearchForm;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationStatusSearchForm;

import java.util.List;


public interface TradeOrderAllocationService {

	////////////////////////////////////////////////////////////////////////////
	//////              Trade Order Allocation Methods                   /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderAllocation getTradeOrderAllocation(int id);


	public TradeOrderAllocation getTradeOrderAllocationActiveByOrder(TradeOrder tradeOrder);


	public List<TradeOrderAllocation> getTradeOrderAllocationListByExternalOrder(Integer externalOrderIdentifier);


	public List<TradeOrderAllocation> getTradeOrderAllocationList(TradeOrderAllocationSearchForm searchForm);


	public TradeOrderAllocation saveTradeOrderAllocation(TradeOrderAllocation bean);


	////////////////////////////////////////////////////////////////////////////
	//////           Trade Order - Trade Order Allocation Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	List<TradeOrderTradeOrderAllocation> getTradeOrderTradeOrderAllocationList(int allocationId);


	////////////////////////////////////////////////////////////////////////////
	//////           Trade Order Allocation Status Methods               /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderAllocationStatus getTradeOrderAllocationStatus(short id);


	public TradeOrderAllocationStatus getTradeOrderAllocationStatusByStatuses(TradeOrderAllocationStatuses allocationStatus, Boolean allocationFillReceived);


	public List<TradeOrderAllocationStatus> getTradeOrderAllocationStatusListByStatuses(TradeOrderAllocationStatuses allocationStatus);


	public List<TradeOrderAllocationStatus> getTradeOrderAllocationStatusList(TradeOrderAllocationStatusSearchForm searchForm);
}
