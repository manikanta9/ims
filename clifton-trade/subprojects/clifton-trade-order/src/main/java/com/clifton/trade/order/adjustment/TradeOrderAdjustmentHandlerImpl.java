package com.clifton.trade.order.adjustment;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class TradeOrderAdjustmentHandlerImpl implements TradeOrderAdjustmentHandler {

	/**
	 * See clifton-trade-order-context.xml for the definition of this map
	 */
	private Map<String, List<TradeOrderAdjustment>> tradeOrderAdjustmentMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T> void applyAdjustments(T obj, String datasourceName) {
		List<TradeOrderAdjustment> adjustmentList = getTradeOrderAdjustmentMap().get(datasourceName);
		BeanUtils.sortWithFunction(adjustmentList, TradeOrderAdjustment::getOrder, true);
		for (TradeOrderAdjustment adjustment : CollectionUtils.getIterable(adjustmentList)) {
			if (adjustment.isSupported(obj)) {
				obj = adjustment.adjust(obj);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, List<TradeOrderAdjustment>> getTradeOrderAdjustmentMap() {
		return this.tradeOrderAdjustmentMap;
	}


	public void setTradeOrderAdjustmentMap(Map<String, List<TradeOrderAdjustment>> tradeOrderAdjustmentMap) {
		this.tradeOrderAdjustmentMap = tradeOrderAdjustmentMap;
	}
}
