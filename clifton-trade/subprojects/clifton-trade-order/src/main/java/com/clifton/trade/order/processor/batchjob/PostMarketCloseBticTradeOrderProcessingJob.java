package com.clifton.trade.order.processor.batchjob;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.processor.TradeOrderProcessingService;
import com.clifton.trade.order.search.TradeOrderSearchForm;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * A task intended to run on trading days after market close.  The job will retrieve and price
 * all TradeOrders with an order status of HOLD_MARKET_CLOSE.  These orders are then sent to
 * the TradeOrderProcessor for final processing and allocation.
 *
 * @author davidi
 */
public class PostMarketCloseBticTradeOrderProcessingJob implements Task {

	private static final String CACHE_LIVE_PRICE = "CACHE-LIVE-PRICE-POST-MARKET-CLOSE";

	private static final TradeOrderStatuses[] TRADE_ORDER_HOLD_STATUSES = {TradeOrderStatuses.HOLD_MARKET_CLOSE, TradeOrderStatuses.HOLD_MARKET_CLOSE_REPLACED};

	private static final int DEFAULT_MAX_AGE_FOR_CACHE_PRICE_SECONDS = 300;


	private Integer maxAgeForCachedPriceSeconds;

	private MarketDataLiveService marketDataLiveService;

	private TradeOrderService tradeOrderService;
	private TradeOrderProcessingService tradeOrderProcessingService;

	private CacheHandler<Object, MarketDataLive> cacheHandler;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status jobStatus = new Status();
		Date processingDate = DateUtils.clearTime(new Date());

		for (TradeOrder tradeOrder : CollectionUtils.getIterable(getHoldMarketCloseTradeOrderListForCurrentDate(processingDate))) {
			try {
				if (!applyLiveSecurityPriceToOrder(tradeOrder, jobStatus)) {
					continue;
				}

				if (!applyProcessingTradeOrderStatus(tradeOrder, jobStatus)) {
					continue;
				}

				getTradeOrderService().saveTradeOrder(tradeOrder);
				Integer[] tradeOrderArray = new Integer[]{tradeOrder.getId()};
				processTradeOrders(tradeOrderArray, jobStatus);
				jobStatus.addDetail(StatusDetail.CATEGORY_SUCCESS, "Successfully processed order: " + tradeOrder.getId() + ".");
			}
			catch (Exception exc) {
				jobStatus.addError("Failed to process order: " + tradeOrder.getId() + ".  Error: " + ExceptionUtils.getDetailedMessage(exc));
			}
		}
		return jobStatus;
	}


	/**
	 * Returns a list of TradeOrders from today that have a status of HOLD_MARKET_CLOSE.
	 */
	private List<TradeOrder> getHoldMarketCloseTradeOrderListForCurrentDate(Date processingDate) {

		// Get orders for today with HOLD_MARKET_CLOSE status
		TradeOrderSearchForm tradeOrderSearchForm = new TradeOrderSearchForm();
		tradeOrderSearchForm.setTradeDate(processingDate);
		tradeOrderSearchForm.setOrderStatusList(TRADE_ORDER_HOLD_STATUSES);
		return getTradeOrderService().getTradeOrderList(tradeOrderSearchForm);
	}


	/**
	 * Retrieves and applies the live price of the InvestmentSecurity to the TradeOrder's ReferenceSecurityPrice property.
	 * Returns true if successful, false on failure.  Updates the  jobStatus if errors occur.
	 */
	private boolean applyLiveSecurityPriceToOrder(TradeOrder tradeOrder, Status jobStatus) {
		final InvestmentSecurity underlyingSecurity = tradeOrder.getInvestmentSecurity().getUnderlyingSecurity();
		if (underlyingSecurity == null) {
			jobStatus.addError("No underlying security defined for: " + tradeOrder.getInvestmentSecurity().getLabel() + " for trade order: " + tradeOrder.getId());
			return false;
		}

		MarketDataLive underlyingPrice = getCachedLiveSecurityPrice(underlyingSecurity);

		if (underlyingPrice == null || underlyingPrice.getValue() == null || !underlyingPrice.isNumeric()) {
			jobStatus.addError("No live price available for security: " + underlyingSecurity.getLabel() + " for trade order: " + tradeOrder.getId());
			return false;
		}

		tradeOrder.setReferenceSecurityPrice(underlyingPrice.asNumericValue());

		return true;
	}


	/**
	 * Attempts to get an unexpired MarketDataLive entry from the cache or MarketDataLivService if not cached.
	 * Returns null if no pricing data could be found for the specified InvestmentSecurity.
	 */
	private MarketDataLive getCachedLiveSecurityPrice(InvestmentSecurity investmentSecurity) {

		// get and use unexpired cache data
		MarketDataLive marketDataLive = getCacheHandler().get(CACHE_LIVE_PRICE, investmentSecurity.getId());
		if (marketDataLive != null) {
			long marketDataLiveAgeInSeconds = DateUtils.getSecondsDifference(new Date(), marketDataLive.getMeasureDate());
			if (marketDataLiveAgeInSeconds <= getMaxAgeForCachedPricesSeconds()) {
				return marketDataLive;
			}
			getCacheHandler().remove(CACHE_LIVE_PRICE, investmentSecurity.getId());
		}

		MarketDataLiveCommand command = new MarketDataLiveCommand();
		command.setMaxAgeInSeconds(1);
		marketDataLive = getMarketDataLiveService().getMarketDataLivePrice(investmentSecurity.getId(), command);

		if (marketDataLive != null) {
			getCacheHandler().put(CACHE_LIVE_PRICE, investmentSecurity.getId(), marketDataLive);
		}

		return marketDataLive;
	}


	/**
	 * Returns the maximum time, in seconds, that an price entry from the cache may be used before it needs to be updated.
	 */
	private long getMaxAgeForCachedPricesSeconds() {
		return getMaxAgeForCachedPriceSeconds() == null ? DEFAULT_MAX_AGE_FOR_CACHE_PRICE_SECONDS : (long) getMaxAgeForCachedPriceSeconds();
	}


	/**
	 * Applies a processing status (which will lead to a fill action) for further processing based on the fill quantity within the order.
	 * Returns true on success, false on failure.  Updates the Status reference on error conditions.
	 */
	private boolean applyProcessingTradeOrderStatus(TradeOrder tradeOrder, Status jobStatus) {

		boolean isAllocateAfterDoneForDay = tradeOrder.getDestinationMapping().getTradeDestination().isAllocateAfterDoneForDay();

		if (MathUtils.isEqual(tradeOrder.getQuantityFilled(), BigDecimal.ZERO)) {
			jobStatus.addError("There is no filled quantity for trade order: " + tradeOrder.getId() + ". Skipping allocation processing.");
			return false;
		}
		else if (MathUtils.isEqual(tradeOrder.getQuantityFilled(), tradeOrder.getQuantity())) {
			if (isAllocateAfterDoneForDay) {
				tradeOrder.setStatus(getTradeOrderService().getTradeOrderStatusByStatuses(TradeOrderStatuses.DONE_FOR_DAY));
			}
			else {
				tradeOrder.setStatus(getTradeOrderService().getTradeOrderStatusByStatuses(TradeOrderStatuses.FILLED));
			}
			return true;
		}
		else if (MathUtils.isLessThan(tradeOrder.getQuantityFilled(), tradeOrder.getQuantity())) {
			tradeOrder.setStatus(getTradeOrderService().getTradeOrderStatusByStatuses(TradeOrderStatuses.PARTIALLY_FILLED_CANCELED));
			return true;
		}

		jobStatus.addError("Cannot apply an appropriate processing status trade order: " + tradeOrder.getId() + ". Skipping allocation processing.");
		return false;
	}


	/**
	 * Sends the trade order ID list to the TradeOrderProcessingService for continued processing and allocation.
	 */
	private boolean processTradeOrders(Integer[] tradeOrderIds, Status jobStatus) {
		try {
			getTradeOrderProcessingService().processTradeOrderList(tradeOrderIds);
		}
		catch (Exception exc) {
			jobStatus.addError("Failed to process order: " + tradeOrderIds[0] + ".  Error: " + ExceptionUtils.getDetailedMessage(exc));
			return false;
		}

		return true;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Integer getMaxAgeForCachedPriceSeconds() {
		return this.maxAgeForCachedPriceSeconds;
	}


	public void setMaxAgeForCachedPriceSeconds(Integer maxAgeForCachedPriceSeconds) {
		this.maxAgeForCachedPriceSeconds = maxAgeForCachedPriceSeconds;
	}


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderProcessingService getTradeOrderProcessingService() {
		return this.tradeOrderProcessingService;
	}


	public void setTradeOrderProcessingService(TradeOrderProcessingService tradeOrderProcessingService) {
		this.tradeOrderProcessingService = tradeOrderProcessingService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public CacheHandler<Object, MarketDataLive> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Object, MarketDataLive> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
