package com.clifton.trade.order.allocation.processor;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurpose;
import com.clifton.investment.account.mapping.InvestmentAccountMappingService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.shared.InvestmentNotionalCalculatorTypes;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderSecurity;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.adjustment.TradeOrderAdjustmentHandler;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetail;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.trade.order.util.TradeOrderUtilHandler;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import com.clifton.trade.search.TradeFillSearchForm;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Component
public class TradeOrderAllocationProcessorImpl implements TradeOrderAllocationProcessor {

	private InvestmentAccountMappingService investmentAccountMappingService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentCurrencyService investmentCurrencyService;
	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataSourceService marketDataSourceService;

	private TradeCommissionService tradeCommissionService;
	private TradeOrderAdjustmentHandler tradeOrderAdjustmentHandler;
	private TradeOrderWorkflowService tradeOrderWorkflowService;
	private TradeOrderUtilHandler tradeOrderUtilHandler;
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void processAllocationFills(TradeOrderProcessorParameters orderParameters) {
		// the order has been filled, allocation accepted and the fills have been received
		TradeOrder firstOrder = orderParameters.getTradeOrderList().get(0);
		if (firstOrder.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation() && !orderParameters.isForceAveragePriceAllocation()) {
			// use second if statement to be sure post trade allocations don't end up in the pre trade.
			if (firstOrder.getAllocationStatus().isAllocationComplete()) {
				doProcessAllocationFills(orderParameters.getTradeOrderList(), orderParameters.getAllocationDetailList());
			}
		}
		else {
			orderParameters.setForceAveragePriceAllocation(true);
			doProcessAllocationFillPreTrade(orderParameters);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void doProcessAllocationFills(List<TradeOrder> tradeOrderList, List<TradeOrderAllocationDetail> details) {
		// create the fills for each trade
		createTradeFillsFromAllocationDetails(tradeOrderList, details);

		for (TradeOrder order : CollectionUtils.getIterable(tradeOrderList)) {
			// the order is fully filled, allocated and fills have been received
			if (!order.getStatus().isPartialFill()) {
				// update the executing broker
				Trade firstTrade = order.getTradeList().get(0);
				if (!order.getDestinationMapping().getExecutingBrokerCompany().equals(firstTrade.getExecutingBrokerCompany())) {
					List<Trade> newTradeList = new ArrayList<>();
					for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
						trade.setExecutingBrokerCompany(order.getDestinationMapping().getExecutingBrokerCompany());
						newTradeList.add(saveTrade(trade));
					}
					order.setTradeList(newTradeList);
				}
				// set the workflow state
				getTradeOrderWorkflowService().setTradeWorkflowStates(order, TradeService.TRADE_EXECUTED_STATE_NAME);
			}
			else {
				processPartiallyFilledOrder(order);
			}
		}
	}


	private void doProcessAllocationFillPreTrade(TradeOrderProcessorParameters orderParameters) {
		for (TradeOrder order : CollectionUtils.getIterable(orderParameters.getTradeOrderList())) {
			List<Trade> tradeList = order.getTradeList();
			Trade firstTrade = tradeList.get(0);

			boolean useNotional = false;
			if (InvestmentUtils.isSecurityOfType(firstTrade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
				if (!firstTrade.getSettlementCurrency().equals(firstTrade.getInvestmentSecurity().getInstrument().getTradingCurrency())) {
					useNotional = true;
				}
			}
			else if (firstTrade.getInvestmentSecurity().isCurrency()) {
				useNotional = true;
			}

			// update quantity intended for partial fill with single trade.
			if (TradeOrderStatuses.PARTIALLY_FILLED_CANCELED == order.getStatus().getOrderStatus() && CollectionUtils.asNonNullList(tradeList).size() == 1) {
				if (useNotional) {
					firstTrade.setAccountingNotional(order.getQuantityFilled());
				}
				else {
					firstTrade.setQuantityIntended(order.getQuantityFilled());
				}
				// recalculate accounting notional
				getTradeService().saveTrade(firstTrade);
			}

			// validate the quantity filled
			BigDecimal totalQuantityIntended = CoreMathUtils.sumProperty(tradeList, (useNotional ? Trade::getAccountingNotional : Trade::getQuantityIntended));
			if (order.isMultipleSecurityOrder()) {
				totalQuantityIntended = MathUtils.divide(totalQuantityIntended, new BigDecimal(order.getSecurityList().size()));
			}

			BigDecimal orderQuantityFilled = order.getQuantityFilled();
			if (!order.isMultipleSecurityOrder() && order.getInvestmentSecurity() != null) {
				/*
				 * For orders of securities with factor adjustments, the order quantity has to be adjusted by the factor to match the IMS quantity intended.
				 * If there are multiple trades, it is possible the first trade has a bad factor and will be caught in the following validation.
				 *
				 * After validation, fills are created below from the trade(s) quantity intended, which has already been adjusted.
				 */
				orderQuantityFilled = adjustQuantityByTradeFactor(firstTrade, orderQuantityFilled);
			}
			ValidationUtils.assertTrue(
					MathUtils.compare(totalQuantityIntended, orderQuantityFilled) == 0,
					"Quantity filled for the order is [" + CoreMathUtils.formatNumberDecimal(orderQuantityFilled) + "] but the total quantity of all trades is ["
							+ CoreMathUtils.formatNumberDecimal(totalQuantityIntended) + "].");

			for (Trade trade : tradeList) {
				if (TradeService.TRADE_BOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
					continue;
				}
				// get the security for multiple security orders
				TradeOrderSecurity security = getTradeOrderSecurity(trade, order);
				validateTradeAndOrder(trade, order, security);

				// clear existing fills
				getTradeService().deleteTradeFillList(getTradeService().getTradeFillListByTrade(trade.getId()));
				trade = getTradeService().getTrade(trade.getId());

				// get the quantity from the notional or the notional for the quantity and FX rate and save it on the trade
				boolean saveTrade = useNotional && updateTradeQuantityWhenUsingNotional(trade, order);

				// set the executing broker
				boolean brokerUpdated = updateExecutingBroker(trade, order);
				if (brokerUpdated || saveTrade) {
					trade = saveTrade(trade);
				}
				// create commission overrides
				createCommissionOverride(trade, security);

				createAndSaveSingleFill(trade, trade.getQuantityIntended(), order.isMultipleSecurityOrder() && security != null ? security.getAveragePrice() : order.getAveragePrice(),
						orderParameters.isForceAveragePriceAllocation());
				getTradeOrderWorkflowService().setTradeWorkflowState(trade, TradeService.TRADE_EXECUTED_STATE_NAME);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private void processPartiallyFilledOrder(TradeOrder order) {
		for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
			if (TradeService.TRADE_BOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				continue;
			}
			List<TradeFill> tradeFillList = getTradeService().getTradeFillListByTrade(trade.getId());
			BigDecimal fillTotal = CoreMathUtils.sumProperty(tradeFillList, TradeFill::getQuantity);

			// cancel the trade if the are no fills
			if (order.getStatus().isPartialFill() && (fillTotal.compareTo(BigDecimal.ZERO) == 0)) {
				cancelTrade(trade);
			}
			else {
				fillPartiallyFilledOrder(trade, order, fillTotal);
			}
		}
	}


	private void fillPartiallyFilledOrder(Trade trade, TradeOrder order, BigDecimal fillTotal) {
		boolean saveTrade = false;
		// if the trade is partially filled then move it too "Cannot Fill" and save the trade
		if (!MathUtils.round(fillTotal, 7).equals(MathUtils.round(trade.getQuantityIntended(), 7))) {
			if (!TradeService.TRADE_CANNOT_FILL_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				final Trade tradeToSave = trade; // this is used because intellij inspections where complaining about needing a final in the next line...
				trade = DaoUtils.executeWithPostUpdateFlushEnabled(() -> getTradeOrderWorkflowService().setTradeWorkflowState(tradeToSave, TradeService.TRADE_CANNOT_FILL_STATE_NAME));
			}
			trade.setQuantityIntended(MathUtils.round(fillTotal, 7));
			saveTrade = true;
		}

		// update the executing broker
		if (!order.getDestinationMapping().getExecutingBrokerCompany().equals(trade.getExecutingBrokerCompany())) {
			trade.setExecutingBrokerCompany(order.getDestinationMapping().getExecutingBrokerCompany());
			saveTrade = true;
		}
		if (saveTrade) {
			trade = saveTrade(trade);
		}

		// move the trade to the Executed state
		getTradeOrderWorkflowService().setTradeWorkflowState(trade, TradeService.TRADE_EXECUTED_STATE_NAME);
	}


	private void createCommissionOverride(Trade trade, TradeOrderSecurity security) {
		// get the commission for the trade
		BigDecimal commissionPerUnit = null;
		if ((security != null) && (security.getCommissionPerUnit() != null)) {
			commissionPerUnit = security.getCommissionPerUnit();
		}

		//compare the commission to the existing commission and set if it's different
		if ((commissionPerUnit != null) && MathUtils.compare(commissionPerUnit, trade.getCommissionPerUnit()) != 0) {
			AccountingAccount commissionAccount = trade.getTradeType().getCommissionAccountingAccount();
			getTradeCommissionService().createOrUpdateTradeCommissionOverride(trade, commissionAccount, null, commissionPerUnit);
		}
	}


	private boolean updateExecutingBroker(Trade trade, TradeOrder order) {
		// update the executing broker
		if ((order.getDestinationMapping().getExecutingBrokerCompany() == null) || !order.getDestinationMapping().getExecutingBrokerCompany().equals(trade.getExecutingBrokerCompany())
				|| (trade.getHoldingInvestmentAccount() == null)) {
			trade.setExecutingBrokerCompany(order.getDestinationMapping().getExecutingBrokerCompany());
			if ((trade.getHoldingInvestmentAccount() == null) || trade.getTradeType().isHoldingAccountSelectionAtExecutionAllowed()) {
				trade.setHoldingInvestmentAccount(getTradeOrderUtilHandler().getHoldingAccountForBroker(trade, trade.getExecutingBrokerCompany()));
			}
			return true;
		}
		return false;
	}


	private boolean updateTradeQuantityWhenUsingNotional(Trade trade, TradeOrder order) {
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)
				|| InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.CURRENCY)) {
			// get the quantity from the notional or the notional for the quantity and FX rate and save it on the trade
			if (trade.getInvestmentSecurity().isCurrency()) {
				trade.setExchangeRateToBase(order.getAveragePrice());
			}
			else {
				InvestmentSecurity security = trade.getInvestmentSecurity();
				if (security.getInstrument().getUnderlyingInstrument() != null) {
					security = getInvestmentInstrumentService().getInvestmentSecurityByInstrument(security.getInstrument().getUnderlyingInstrument().getId());
				}
				InvestmentNotionalCalculatorTypes roundingCalculator = InvestmentCalculatorUtils.getNotionalCalculator(security);
				BigDecimal calculatedQuantity = getInvestmentCalculator().calculateQuantityFromNotional(trade.getInvestmentSecurity(), order.getAveragePrice(), trade.getAccountingNotional(),
						trade.getNotionalMultiplier());
				trade.setQuantityIntended(roundingCalculator.round(calculatedQuantity));
			}
			return true;
		}
		return false;
	}


	private TradeOrderSecurity getTradeOrderSecurity(Trade trade, TradeOrder order) {
		if (order.isMultipleSecurityOrder()) {
			for (TradeOrderSecurity security : CollectionUtils.getIterable(order.getSecurityList())) {
				if (security.getInvestmentSecurity().equals(trade.getInvestmentSecurity())) {
					return security;
				}
			}
		}
		return null;
	}


	private void validateTradeAndOrder(Trade trade, TradeOrder order, TradeOrderSecurity security) {
		if (order.isMultipleSecurityOrder()) {
			ValidationUtils.assertNotNull(security, "No [TradeOrderSecurity] found for trade [" + trade + "] in order [" + order + "].");
			ValidationUtils.assertNotNull(security.getAveragePrice(), "Cannot process pre-trade or manual allocations without an average price.");
		}
		else {
			ValidationUtils.assertNotNull(order.getAveragePrice(), "Cannot process pre-trade allocations without an average price.");
		}
		ValidationUtils.assertNotNull(order.getDestinationMapping().getExecutingBrokerCompany(), "Cannot process pre-trade allocations without an executing broker.");
	}


	private BigDecimal getCurrencyNotional(Trade trade) {
		String currencyTraded = trade.getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol();
		String baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(trade).getSymbol();


		BigDecimal currencyNotional;
		if (!getInvestmentCurrencyService().isInvestmentCurrencyMultiplyConvention(currencyTraded, baseCurrency)) {
			currencyNotional = MathUtils.divide(trade.getAccountingNotional(), trade.getExchangeRateToBase(), 2);
		}
		else {
			currencyNotional = MathUtils.multiply(trade.getAccountingNotional(), trade.getExchangeRateToBase(), 2);
		}
		return currencyNotional;
	}


	private boolean isTradeFilled(Trade trade) {
		TradeFillSearchForm searchForm = new TradeFillSearchForm();
		searchForm.setTradeId(trade.getId());
		List<TradeFill> fills = getTradeService().getTradeFillList(searchForm);

		return fills != null && !fills.isEmpty();
	}


	private TradeFill createSingleFill(Trade trade, BigDecimal shares, BigDecimal price) {
		/*
		 * If we create fills from allocation details of orders for securities with factors (hierarchy has factor event type),
		 * we may have to adjust the shares (original face) by the security's factor to get the right quantity intended.
		 *
		 * Currently this method is called from three places passing the detail shares/quantity directly from allocation details.
		 * FIX messages for factor affected securities use original face quantity and adjustments are required in IMS when creating fills.
		 *
		 * If we decide to adjust allocation detail quantities for factor adjusted securities, we can use the following for this method:
		 * BigDecimal adjustedShares = adjustQuantityByTradeFactor(trade, shares);
		 * return doCreateSingleFill(trade, adjustedShares, price, false, false);
		 */
		return doCreateSingleFill(trade, shares, price, false, false);
	}


	private TradeFill createAndSaveSingleFill(Trade trade, BigDecimal shares, BigDecimal price, boolean forceAveragePriceAllocation) {
		return doCreateSingleFill(trade, shares, price, true, forceAveragePriceAllocation);
	}


	/**
	 * Trades for securities with factor adjustments, need to have the quantity adjusted by the factor (when not 1).
	 */
	private BigDecimal adjustQuantityByTradeFactor(Trade trade, BigDecimal quantity) {
		BigDecimal factor = trade.getCurrentFactor();
		if (quantity != null && factor != null && MathUtils.isNotEqual(BigDecimal.ONE, factor)) {
			return MathUtils.multiply(factor, quantity);
		}
		return quantity;
	}


	/**
	 * Create a fill for the provided trade with the shares and price.
	 * <p>
	 * If shares needs to be adjusted, such as for orders for securities with factor adjustments, the shares quantity
	 * should be adjusted prior to calling this method.
	 */
	private TradeFill doCreateSingleFill(Trade trade, BigDecimal shares, BigDecimal price, boolean saveFill, boolean forceAveragePriceAllocation) {
		// get the latest trade
		trade = getTradeService().getTrade(trade.getId());

		TradeFill fill = new TradeFill();
		fill.setTrade(trade);
		fill.setQuantity(shares);
		fill.setNotionalUnitPrice(price);

		if (trade.getInvestmentSecurity().isCurrency()) {
			fill.setNotionalTotalPrice(trade.getAccountingNotional());
			fill.setPaymentTotalPrice(getCurrencyNotional(trade));
		}

		if (!forceAveragePriceAllocation) {
			try {
				getTradeOrderAdjustmentHandler().applyAdjustments(fill, getMarketDataSourceService().getMarketDataSourceName(trade.getExecutingBrokerCompany()));
			}
			catch (Throwable e) {
				// only log the error so the adjustment to cause the order handling to fail
				LogUtils.error(getClass(), "Failed to run fill adjustments.", e);
			}
		}

		if (saveFill) {
			getTradeService().saveTradeFill(fill);
		}
		trade.addFill(fill);
		return fill;
	}


	private void cancelTrade(Trade trade) {
		getTradeOrderWorkflowService().setTradeWorkflowState(trade, TradeService.TRADES_CANCELLED_STATE_NAME);
	}


	private Trade saveTrade(Trade trade) {
		return DaoUtils.executeWithAllObserversDisabled(() -> DaoUtils.executeWithPostUpdateFlushEnabled(() -> getTradeService().saveTrade(trade)));
	}


	/**
	 * Method adds fills to each trade starting with the lowest quantity trade and fill.
	 */
	private void createTradeFillsFromAllocationDetails(List<TradeOrder> tradeOrderList, List<TradeOrderAllocationDetail> detailList) {
		reverseTranslateAllocationDetailAccounts(detailList, tradeOrderList.get(0).getDestinationMapping().getAllocationAccountMappingPurpose());
		List<Trade> tradeList = getTradeListFromOrder(tradeOrderList);
		List<TradeFill> tradeFillList = new ArrayList<>();

		// if there is only one trade set the allocation and skip the rest of the processing
		if (tradeList.size() == 1) {
			if (!isTradeFilled(tradeList.get(0))) {
				for (TradeOrderAllocationDetail detail : CollectionUtils.getIterable(detailList)) {
					tradeFillList.add(createSingleFill(tradeList.get(0), detail.getAllocationShares(), detail.getAllocationPrice()));
				}
			}
		}
		else {
			Set<String> accountNumbers = new HashSet<>();
			// loop through the accounts
			for (TradeOrderAllocationDetail detail : CollectionUtils.getIterable(detailList)) {
				if (!accountNumbers.add(detail.getAllocationAccount())) {
					// only process the account once
					continue;
				}

				// get the list of trades and allocations for the account
				List<TradeOrderAllocationDetail> accountDetailList = BeanUtils.filter(detailList, TradeOrderAllocationDetail::getAllocationAccount, detail.getAllocationAccount());

				List<Trade> accountTradeList = BeanUtils.filter(tradeList, (Trade t) -> t.getHoldingInvestmentAccount().getNumber(), detail.getAllocationAccount());

				TradeOrderAllocationDetail[] accountDetails = getSortedAccountDetails(accountDetailList);
				Trade[] accountTrades = getSortedAccountTrades(accountTradeList);

				// if there is only 1 trade, then create a fill for each detail
				if (accountTrades.length == 1) {
					if (!isTradeFilled(accountTrades[0])) {
						for (TradeOrderAllocationDetail accountDetail : accountDetails) {
							tradeFillList.add(createSingleFill(accountTrades[0], accountDetail.getAllocationShares(), accountDetail.getAllocationPrice()));
						}
					}
				}
				else if ((accountTrades.length > 1) && (accountDetails.length >= 1)) {
					// if there are multiple trades and details, then fill each trade starting with the lowest qty/price first.
					tradeFillList.addAll(createFillsForMultipleTradesAndDetails(accountTrades, accountDetails));
				}
			}
		}
		if (!CollectionUtils.isEmpty(tradeFillList)) {
			DaoUtils.executeWithAllObserversDisabled(() -> DaoUtils.executeWithPostUpdateFlushEnabled(() -> getTradeService().saveTradeFillList(tradeFillList)));
		}
	}


	/**
	 * Checks each TradeOrderAllocationDetail to determine if it has a reverse mapping for a given OrderMappingPurpose.  Updates each entity's allocation account with the
	 * account number found from the reverse mapping.  If no account number is returned from the reverse mapping call, the allocation account in the detail entity remains unchanged.
	 */
	private void reverseTranslateAllocationDetailAccounts(List<TradeOrderAllocationDetail> detailList, InvestmentAccountMappingPurpose allocationAccountMappingPurpose) {
		if (allocationAccountMappingPurpose == null) {
			return;
		}

		for (TradeOrderAllocationDetail detail : CollectionUtils.getIterable(detailList)) {
			try {
				String reverseTranslatedAccount = getInvestmentAccountMappingService().getInvestmentAccountReverseMapping(detail.getAllocationAccount(),
						allocationAccountMappingPurpose);
				if (reverseTranslatedAccount != null) {
					detail.setAllocationAccount(reverseTranslatedAccount);
				}
			}
			catch (RuntimeException exc) {
				LogUtils.error(TradeOrderAllocationProcessor.class, String.format("Reverse mapping unsuccessful due to:  %s", exc.getMessage()));
			}
		}
	}


	/**
	 * Create fill for each trade using the provided allocation details.  Trades will be filled from starting at the lowest qty.
	 * <p>
	 * <p>
	 * For example, if we have 2 trades one for 25 and one for 50.  Then we have 3 fills of 10 @ $2, 25 @ $3 and 40 @ $4.  The fills will be:<br><br>
	 * Trade QTY - 25<br>
	 * Fill 1 - 10 @ $2<br>
	 * Fill 2 - 15 @ $3<br>
	 * <p>
	 * Trade QTY - 50<br>
	 * Fill 1 - 10 @ $3<br>
	 * Fill 2 - 40 @ $4<br>
	 */
	private List<TradeFill> createFillsForMultipleTradesAndDetails(Trade[] accountTrades, TradeOrderAllocationDetail[] accountDetails) {
		List<TradeFill> result = new ArrayList<>();
		Map<String, BigDecimal> sharesLeft = new HashMap<>();
		for (Trade accountTrade : accountTrades) {
			double totalTradeFill = 0.0;
			if (!isTradeFilled(accountTrade)) {
				for (TradeOrderAllocationDetail accountDetail : accountDetails) {
					if (!sharesLeft.containsKey(accountDetail.getId())) {
						sharesLeft.put(accountDetail.getId(), accountDetail.getAllocationShares());
					}
					if (sharesLeft.get(accountDetail.getId()).doubleValue() > 0) {
						BigDecimal sharesLeftToFill = sharesLeft.get(accountDetail.getId());
						BigDecimal sharesToFill;

						if (accountTrade.getQuantityIntended().subtract(BigDecimal.valueOf(totalTradeFill)).doubleValue() <= sharesLeftToFill.doubleValue()) {
							sharesToFill = accountTrade.getQuantityIntended().subtract(BigDecimal.valueOf(totalTradeFill));
						}
						else {
							sharesToFill = sharesLeftToFill;
						}
						totalTradeFill += sharesToFill.doubleValue();
						result.add(createSingleFill(accountTrade, sharesToFill, accountDetail.getAllocationPrice()));
						sharesLeft.put(accountDetail.getId(), sharesLeftToFill.subtract(sharesToFill));
					}
					if (totalTradeFill >= accountTrade.getQuantityIntended().doubleValue()) {
						// the trade is filled move to the next
						break;
					}
				}
			}
		}
		return result;
	}


	/**
	 * Sort the allocation details be shares (quantity) and then by price lowest to highest.
	 */
	private TradeOrderAllocationDetail[] getSortedAccountDetails(List<TradeOrderAllocationDetail> accountDetailList) {
		TradeOrderAllocationDetail[] accountDetails = new TradeOrderAllocationDetail[accountDetailList.size()];
		accountDetailList.toArray(accountDetails);

		// sort the arrays so that we are filling like quantities
		Arrays.sort(accountDetails, Comparator.comparing(TradeOrderAllocationDetail::getAllocationShares).thenComparing(TradeOrderAllocationDetail::getAllocationPrice));

		return accountDetails;
	}


	/**
	 * Sort the trades by quantity lowest to highest.
	 */
	private Trade[] getSortedAccountTrades(List<Trade> accountTradeList) {
		Trade[] accountTrades = new Trade[accountTradeList.size()];
		accountTradeList.toArray(accountTrades);

		Arrays.sort(accountTrades, Comparator.comparing(Trade::getQuantityIntended));
		return accountTrades;
	}


	private List<Trade> getTradeListFromOrder(List<TradeOrder> tradeOrderList) {
		// create a list of trades from the orders
		List<Trade> tradeList = new ArrayList<>();
		for (TradeOrder order : CollectionUtils.getIterable(tradeOrderList)) {
			if (order.getTradeList() == null) {
				TradeSearchForm tradeSearchForm = new TradeSearchForm();
				tradeSearchForm.setOrderIdentifier(order.getId());
				tradeList.addAll(getTradeService().getTradeList(tradeSearchForm));
			}
			else {
				tradeList.addAll(order.getTradeList());
			}
		}
		return tradeList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public TradeOrderWorkflowService getTradeOrderWorkflowService() {
		return this.tradeOrderWorkflowService;
	}


	public void setTradeOrderWorkflowService(TradeOrderWorkflowService tradeOrderWorkflowService) {
		this.tradeOrderWorkflowService = tradeOrderWorkflowService;
	}


	public InvestmentAccountMappingService getInvestmentAccountMappingService() {
		return this.investmentAccountMappingService;
	}


	public void setInvestmentAccountMappingService(InvestmentAccountMappingService investmentAccountMappingService) {
		this.investmentAccountMappingService = investmentAccountMappingService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}


	public TradeOrderAdjustmentHandler getTradeOrderAdjustmentHandler() {
		return this.tradeOrderAdjustmentHandler;
	}


	public void setTradeOrderAdjustmentHandler(TradeOrderAdjustmentHandler tradeOrderAdjustmentHandler) {
		this.tradeOrderAdjustmentHandler = tradeOrderAdjustmentHandler;
	}


	public InvestmentCurrencyService getInvestmentCurrencyService() {
		return this.investmentCurrencyService;
	}


	public void setInvestmentCurrencyService(InvestmentCurrencyService investmentCurrencyService) {
		this.investmentCurrencyService = investmentCurrencyService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public TradeOrderUtilHandler getTradeOrderUtilHandler() {
		return this.tradeOrderUtilHandler;
	}


	public void setTradeOrderUtilHandler(TradeOrderUtilHandler tradeOrderUtilHandler) {
		this.tradeOrderUtilHandler = tradeOrderUtilHandler;
	}
}
