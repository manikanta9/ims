package com.clifton.trade.order.adjustment;


/**
 * The <code>TradeOrderAdjustmentHandler</code> defines methods for adjusting objects when processing trade orders.
 *
 * @author mwacker
 */
public interface TradeOrderAdjustmentHandler {


	/**
	 * Applies the adjustments to the object (TradeOrder or TradeFill) for the given datasource.
	 */
	public <T> void applyAdjustments(T obj, String datasourceName);
}
