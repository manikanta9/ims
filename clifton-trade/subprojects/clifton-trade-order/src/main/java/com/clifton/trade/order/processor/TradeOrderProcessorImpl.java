package com.clifton.trade.order.processor;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatus;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetail;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetailProvider;
import com.clifton.trade.order.allocation.TradeOrderAllocationExecutionHandler;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessor;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessorParameters;
import com.clifton.trade.order.search.TradeOrderSearchForm;
import com.clifton.trade.order.util.TradeOrderUtilHandler;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>TradeOrderProcessorImpl</code> implements an object used to process TradeOrder's after messages
 * have been received.
 *
 * @author mwacker
 */
@Component
public class TradeOrderProcessorImpl implements TradeOrderProcessor {

	private TradeService tradeService;
	private TradeCommissionService tradeCommissionService;
	private TradeOrderService tradeOrderService;
	private TradeOrderAllocationService tradeOrderAllocationService;
	private TradeOrderWorkflowService tradeOrderWorkflowService;
	private TradeOrderAllocationExecutionHandler tradeOrderAllocationExecutionHandler;

	private TradeOrderAllocationProcessor tradeOrderAllocationProcessor;
	private TradeOrderUtilHandler tradeOrderUtilHandler;
	/**
	 * Used to get allocation details.  This might eventually be a list of provider
	 * but for now there is on FIX so we'll leave it as a single provider for now.
	 */
	private TradeOrderAllocationDetailProvider tradeOrderAllocationDetailProvider;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(timeout = 180, propagation = Propagation.REQUIRES_NEW)
	public void processTradeOrder(TradeOrderProcessorParameters orderProcessorParameters) {
		validateOrderProcessorParameters(orderProcessorParameters);
		doProcessTradeOrder(orderProcessorParameters);
	}


	@Override
	public void processTradeOrderList(Integer[] tradeOrderList) {
		List<TradeOrderProcessorParameters> parameterList = new ArrayList<>();
		Set<Integer> addedOrders = new HashSet<>();
		// build the TradeOrderProcessorParameters for each order
		for (Integer tradeOrderId : tradeOrderList) {
			// if the order is already add to a parameter item skip it
			if (addedOrders.contains(tradeOrderId)) {
				continue;
			}

			// get the order and build the parameter item
			TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
			TradeOrderProcessorParameters parameters = getProcessorParametersForOrder(order);
			parameterList.add(parameters);

			// since allocation can have multiple orders, keep a list of the orders that have been added to
			// parameter items to avoid creating duplicate parameter entries
			for (TradeOrder addedOrder : CollectionUtils.getIterable(parameters.getTradeOrderList())) {
				addedOrders.add(addedOrder.getId());
			}
		}

		for (TradeOrderProcessorParameters parameter : CollectionUtils.getIterable(parameterList)) {
			processTradeOrder(parameter);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrderProcessorParameters getProcessorParametersForOrder(TradeOrder order) {
		TradeOrderProcessorParameters parameters;
		if (order.isAllocationSent() && order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation()) {
			parameters = new TradeOrderProcessorParameters(TradeOrderProcessorActonTypes.PROCESS_ORDER_ALLOCATION);
			TradeOrderAllocation allocation = getTradeOrderAllocationService().getTradeOrderAllocationActiveByOrder(order);
			TradeOrderSearchForm searchForm = new TradeOrderSearchForm();
			searchForm.setAllocationIdForAllOrders(allocation.getId());
			List<TradeOrder> orderList = getTradeOrderService().getTradeOrderListPopulated(searchForm);
			parameters.setTradeOrderList(orderList);

			TradeOrderAllocationProcessorParameters allocationParameters = new TradeOrderAllocationProcessorParameters();
			allocationParameters.setExternalAllocationIdentifier(allocation.getExternalIdentifier());
			allocationParameters.setDestinationMapping(order.getDestinationMapping());
			parameters.setAllocationDetailList(getTradeOrderAllocationDetailProvider().getTradeOrderAllocationDetails(allocationParameters));
		}
		else {
			parameters = new TradeOrderProcessorParameters(TradeOrderProcessorActonTypes.PROCESS_ORDER);
			parameters.setTradeOrder(order);
		}
		return parameters;
	}


	private void doProcessTradeOrder(TradeOrderProcessorParameters orderProcessorParameters) {
		switch (orderProcessorParameters.getActonType()) {
			// process a single order
			case PROCESS_ORDER: {
				TradeOrder order = orderProcessorParameters.getTradeOrder();
				if (processSingleOrder(order)) {
					// if the destination mapping is post trade allocated send the allocation message
					if (order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation()) {
						getTradeOrderAllocationExecutionHandler().allocationTradeOrder(CollectionUtils.createList(order));
					}
					else {
						// otherwise allocate the order with quantity filled and average price
						orderProcessorParameters.setTradeOrderList(new ArrayList<>());
						orderProcessorParameters.getTradeOrderList().add(orderProcessorParameters.getTradeOrder());
						processAllocationFills(orderProcessorParameters);
					}
				}
				break;
			}
			// process multiple orders as single orders
			case PROCESS_ORDER_MULTIPLE: {
				for (TradeOrder order : CollectionUtils.getIterable(orderProcessorParameters.getTradeOrderList())) {
					if (processSingleOrder(order)) {
						getTradeOrderAllocationExecutionHandler().allocationTradeOrder(CollectionUtils.createList(order));
					}
				}
				break;
			}
			// process multiple orders and do a block allocation
			case PROCESS_ORDER_MULTIPLE_BLOCK_ALLOCATE: {
				boolean ready = true;
				StringBuilder sb = new StringBuilder();
				for (TradeOrder order : CollectionUtils.getIterable(orderProcessorParameters.getTradeOrderList())) {
					boolean orderReady = processSingleOrder(order);
					if (!orderReady) {
						sb.append("Order [").append(order.getId()).append("] with status [").append(order.getStatus()).append("] cannot be allocated.\n");
						ready = false;
					}
				}
				// verify that all orders are ready to be allocated
				ValidationUtils.assertTrue(ready, sb.toString());
				getTradeOrderAllocationExecutionHandler().allocationTradeOrder(orderProcessorParameters.getTradeOrderList());
				break;
			}
			case PROCESS_ORDER_ALLOCATION: {
				validateOrderAllocationDetailList(orderProcessorParameters);
				TradeOrder order = orderProcessorParameters.getTradeOrderList().get(0);

				// Validations for diagnostic purposes: NullPointerExceptions
				ValidationUtils.assertNotNull(order, "Trade order must not be null.");
				ValidationUtils.assertNotNull(order.getAllocationStatus(), "Trade order: " + order.getId() + " cannot have a null allocation status.");

				if (order.getAllocationStatus().isAllocationComplete()) {
					processAllocationFills(orderProcessorParameters);
				}
				break;
			}
		}
	}


	/**
	 * Processed an order and returns true if the order is ready to be allocated.
	 *
	 * @param order
	 */
	private boolean processSingleOrder(TradeOrder order) {
		if (((order.getTradeList() == null) || order.getTradeList().isEmpty()) && (order.getStatus().getOrderStatus() != TradeOrderStatuses.REPLACED)) {
			return false;
		}

		boolean isAllocateAfterDoneForDay = order.getDestinationMapping().getTradeDestination().isAllocateAfterDoneForDay();

		switch (order.getStatus().getOrderStatus()) {
			case HOLD_MARKET_CLOSE:
				return false;

			case HOLD_MARKET_CLOSE_REPLACED:
				processReplace(order);
				return false;

			case FILLED:
				if (isAllocateAfterDoneForDay) {
					return false;  // defer processing until DONE_FOR_DAY message
				}
				return processFill(order);

			case DONE_FOR_DAY:
				// process if flag is set, otherwise fall through
				if (isAllocateAfterDoneForDay) {
					return processFill(order);
				}
			case CANCELED:
				// if nothing has been filled, the move the trades for the order to the canceled state
				if (!order.getStatus().isFillComplete() && !order.getStatus().isPartialFill() && order.getStatus().isCancelIfNotFilled()) {
					getTradeOrderWorkflowService().setTradeWorkflowStates(order, TradeService.FIX_CANCELED_STATE_NAME);
				}
				return false;
			case PARTIALLY_FILLED_DONE_FOR_DAY:
			case PARTIALLY_FILLED_CANCELED:
				// update the trade prices
				processTradePrices(order);
				// update the executing broker on the trade is it's available
				processExecutionBroker(order);

				if (order.getStatus().isFillComplete() && !order.isAllocationSent() && order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation()) {
					// if only one trade and partially filled set the quantity of trade
					if (order.getTradeList().size() == 1) {
						Trade trade = order.getTradeList().get(0);
						// do not save the trade just set the quantityIntended temporarily so the correct quantity gets sent for the allocation
						trade.setQuantityIntended(order.getQuantityFilled());
						return true;
					}
					// if the total shares of all trades equals the filled quantity then allocate the trade
					BigDecimal totalShares = CoreMathUtils.sumProperty(order.getTradeList(), Trade::getQuantityIntended);
					if (MathUtils.round(order.getQuantityFilled(), 7).equals(MathUtils.round(totalShares, 7))) {
						return true;
					}
				}
				return false;
			case REPLACED: {
				return processReplace(order);
			}
			case REPLACED_FILLED: {
				processReplace(order);
				return processFill(order);
			}
			default:
				return false;
		}
	}


	private boolean processFill(TradeOrder order) {
		// update the trade prices
		processTradePrices(order);
		// update the executing broker on the trade if it's available
		processExecutionBroker(order);
		// if the order is filled, then simply return weather or not it's ready to be allocated.
		return !order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation() || !order.isAllocationSent();
	}


	private boolean processReplace(TradeOrder order) {
		TradeOrder originalOrder = getTradeOrderService().getTradeOrder(order.getOriginalTradeOrder().getId());
		if (originalOrder.getStatus().getOrderStatus() != TradeOrderStatuses.REPLACED) {
			TradeOrderStatus originalStatus = order.getOriginalTradeOrder().getStatus();
			// set the status on the original order
			originalOrder.setStatus(order.getStatus());
			originalOrder.setActiveOrder(false);
			getTradeOrderService().saveTradeOrder(originalOrder);
			// copy the status of the original order to the new order
			order.setStatus(originalStatus);
			order.setActiveOrder(true);
			getTradeOrderService().saveTradeOrder(order);

			// move the trades to the new order;
			for (Trade trade : CollectionUtils.getIterable(originalOrder.getTradeList())) {
				trade.setOrderIdentifier(order.getId());
				trade.setTraderUser(order.getTraderUser());

				Trade savedTrade = DaoUtils.executeWithAllObserversDisabled(() -> DaoUtils.executeWithPostUpdateFlushEnabled(() -> getTradeService().saveTrade(trade)));

				if (order.getCommissionPerUnit() != null) {
					AccountingAccount commissionAccount = savedTrade.getTradeType().getCommissionAccountingAccount();
					getTradeCommissionService().createOrUpdateTradeCommissionOverride(savedTrade, commissionAccount, null, order.getCommissionPerUnit());
				}
			}
		}
		return false;
	}


	private void processAllocationFills(TradeOrderProcessorParameters orderProcessorParameters) {
		getTradeOrderAllocationProcessor().processAllocationFills(orderProcessorParameters);
	}


	private void validateOrderProcessorParameters(TradeOrderProcessorParameters orderProcessorParameters) {
		ValidationUtils.assertNotNull(orderProcessorParameters.getActonType(), "Action type is required to process orders");
		switch (orderProcessorParameters.getActonType()) {
			case PROCESS_ORDER:
				ValidationUtils.assertNotNull(orderProcessorParameters.getTradeOrder(), "[tradeOrder] is required for processing type [" + orderProcessorParameters.getActonType() + "]");
				break;
			case PROCESS_ORDER_MULTIPLE_BLOCK_ALLOCATE:
			case PROCESS_ORDER_MULTIPLE:
				ValidationUtils.assertNotEmpty(orderProcessorParameters.getTradeOrderList(), "[tradeOrderList] is required for processing type [" + orderProcessorParameters.getActonType() + "]");
				break;
			case PROCESS_ORDER_ALLOCATION:
				ValidationUtils.assertNotEmpty(orderProcessorParameters.getTradeOrderList(), "[tradeOrderList] is required for processing type [" + orderProcessorParameters.getActonType() + "]");
				ValidationUtils.assertNotEmpty(orderProcessorParameters.getAllocationDetailList(), "[allocationDetailList] is required for processing type [" + orderProcessorParameters.getActonType()
						+ "] for order [" + orderProcessorParameters.getTradeOrderList().get(0).getId() + "].");
				break;
		}
	}


	private void validateOrderAllocationDetailList(TradeOrderProcessorParameters orderProcessorParameters) {
		BigDecimal orderQty = CoreMathUtils.sumProperty(orderProcessorParameters.getTradeOrderList(), TradeOrder::getQuantityFilled);
		BigDecimal allocationQty = CoreMathUtils.sumProperty(orderProcessorParameters.getAllocationDetailList(), TradeOrderAllocationDetail::getAllocationShares);
		ValidationUtils.assertTrue(MathUtils.compare(orderQty, allocationQty) == 0, "Cannot generate fills because the quantity filled [" + CoreMathUtils.formatNumberInteger(orderQty)
				+ "] does not match the allocated quantity [" + CoreMathUtils.formatNumberInteger(allocationQty) + "].");
	}


	private void processExecutionBroker(TradeOrder order) {
		Trade firstTrade = order.getTradeList().get(0);
		Set<Trade> tradeSaveSet = new LinkedHashSet<>();
		if (firstTrade.getTradeType().isBrokerSelectionAtExecutionAllowed() || firstTrade.getTradeType().isHoldingAccountSelectionAtExecutionAllowed()) {
			for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
				if (order.getDestinationMapping().getExecutingBrokerCompany() != null && !order.getDestinationMapping().getExecutingBrokerCompany().equals(trade.getExecutingBrokerCompany())) {
					trade.setExecutingBrokerCompany(order.getDestinationMapping().getExecutingBrokerCompany());
					tradeSaveSet.add(trade);
				}
				// update the holding account if possible
				if ((order.getDestinationMapping().getExecutingBrokerCompany() != null) && (trade.getTradeType().isHoldingAccountSelectionAtExecutionAllowed() && (trade.getHoldingInvestmentAccount() == null))) {
					trade.setHoldingInvestmentAccount(getTradeOrderUtilHandler().getHoldingAccountForBroker(trade, trade.getExecutingBrokerCompany()));
					if ((trade.getHoldingInvestmentAccount() != null)) {
						tradeSaveSet.add(trade);
					}
				}
			}
		}
		// save the trades
		for (Trade trade : CollectionUtils.getIterable(tradeSaveSet)) {
			getTradeService().saveTrade(trade);
		}
	}


	private void processTradePrices(TradeOrder order) {
		Trade firstTrade = order.getTradeList().get(0);
		if (firstTrade.getTradeType().isSingleFillTrade()) {
			int size = CollectionUtils.getSize(order.getTradeList());
			for (int i = 0; i < size; i++) {
				Trade trade = order.getTradeList().get(i);
				if (trade.getInvestmentSecurity().isCurrency()) {
					trade.setExchangeRateToBase(order.getAveragePrice());
				}
				else {
					trade.setAverageUnitPrice(order.getAveragePrice());
				}
				if (trade.getInvestmentSecurity().getInstrument().getHierarchy().isIndexRatioAdjusted() && !MathUtils.isNullOrZero(order.getIndexRatio())) {
					trade.setNotionalMultiplier(order.getIndexRatio());
				}
				order.getTradeList().set(i, getTradeService().saveTrade(trade));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderWorkflowService getTradeOrderWorkflowService() {
		return this.tradeOrderWorkflowService;
	}


	public void setTradeOrderWorkflowService(TradeOrderWorkflowService tradeOrderWorkflowService) {
		this.tradeOrderWorkflowService = tradeOrderWorkflowService;
	}


	public TradeOrderAllocationDetailProvider getTradeOrderAllocationDetailProvider() {
		return this.tradeOrderAllocationDetailProvider;
	}


	public void setTradeOrderAllocationDetailProvider(TradeOrderAllocationDetailProvider tradeOrderAllocationDetailProvider) {
		this.tradeOrderAllocationDetailProvider = tradeOrderAllocationDetailProvider;
	}


	public TradeOrderAllocationService getTradeOrderAllocationService() {
		return this.tradeOrderAllocationService;
	}


	public void setTradeOrderAllocationService(TradeOrderAllocationService tradeOrderAllocationService) {
		this.tradeOrderAllocationService = tradeOrderAllocationService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}


	public TradeOrderAllocationExecutionHandler getTradeOrderAllocationExecutionHandler() {
		return this.tradeOrderAllocationExecutionHandler;
	}


	public void setTradeOrderAllocationExecutionHandler(TradeOrderAllocationExecutionHandler tradeOrderAllocationExecutionHandler) {
		this.tradeOrderAllocationExecutionHandler = tradeOrderAllocationExecutionHandler;
	}


	public TradeOrderAllocationProcessor getTradeOrderAllocationProcessor() {
		return this.tradeOrderAllocationProcessor;
	}


	public void setTradeOrderAllocationProcessor(TradeOrderAllocationProcessor tradeOrderAllocationProcessor) {
		this.tradeOrderAllocationProcessor = tradeOrderAllocationProcessor;
	}


	public TradeOrderUtilHandler getTradeOrderUtilHandler() {
		return this.tradeOrderUtilHandler;
	}


	public void setTradeOrderUtilHandler(TradeOrderUtilHandler tradeOrderUtilHandler) {
		this.tradeOrderUtilHandler = tradeOrderUtilHandler;
	}
}
