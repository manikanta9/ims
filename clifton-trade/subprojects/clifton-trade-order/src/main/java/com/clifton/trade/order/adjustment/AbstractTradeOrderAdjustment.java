package com.clifton.trade.order.adjustment;


/**
 * The <code>AbstractTradeOrderAdjustment</code> defines a base TradeOrderAdjustment implementation.
 *
 * @author mwacker
 */
public abstract class AbstractTradeOrderAdjustment implements TradeOrderAdjustment {

	/**
	 * When multiple adjustments are applied, this defines the order in which those adjustment occur
	 */
	private int order;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
