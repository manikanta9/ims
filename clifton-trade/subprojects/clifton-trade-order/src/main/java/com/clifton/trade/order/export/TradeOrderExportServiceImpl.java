package com.clifton.trade.order.export;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * The <code>TradeOrderExportServiceImpl</code> implements a download service for custom order exports.  Specific order list
 * converters are registered in the spring session by populating tradeOrderExportConverterMap.  Service calls must
 * provide the name of a register converter.
 *
 * @author mwacker
 */
@Service
public class TradeOrderExportServiceImpl implements TradeOrderExportService {

	private TradeOrderService tradeOrderService;

	private Map<String, Converter<List<TradeOrder>, FileWrapper>> tradeOrderExportConverterMap;


	@Override
	public FileWrapper downloadTradeOrderExport(String downloadName, Integer[] tradeOrderIds) {
		List<TradeOrder> orderList = new ArrayList<>();
		TradeOrderGroupingTypes groupingType = null;
		for (Integer id : CollectionUtils.getIterable(Arrays.asList(tradeOrderIds))) {
			TradeOrder order = getTradeOrderService().getTradeOrder(id);
			if (groupingType == null) {
				groupingType = order.getType().getGroupingType();
			}
			else {
				ValidationUtils.assertTrue(groupingType == order.getType().getGroupingType(), "Cannot export orders with different grouping types.");
			}
			orderList.add(order);
		}
		ValidationUtils.assertTrue(getTradeOrderExportConverterMap().containsKey(downloadName), "No converter is registered for download [" + downloadName + "].");
		return getTradeOrderExportConverterMap().get(downloadName).convert(orderList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public Map<String, Converter<List<TradeOrder>, FileWrapper>> getTradeOrderExportConverterMap() {
		return this.tradeOrderExportConverterMap;
	}


	public void setTradeOrderExportConverterMap(Map<String, Converter<List<TradeOrder>, FileWrapper>> tradeOrderExportConverterMap) {
		this.tradeOrderExportConverterMap = tradeOrderExportConverterMap;
	}
}
