package com.clifton.trade.order.notification;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.AbstractTimedEvictionCache;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.priority.SystemPriorityService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.websocket.WebSocketHandler;
import com.clifton.websocket.alert.WebsocketAlertMessage;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <code>TradeOrderStatusNotificationHandler</code> handle sending notifications for trades that exceed a timeout period before receiving their execution report.
 */
@Component
public class TradeOrderTimedStatusCache extends AbstractTimedEvictionCache<IdentityObject> {


	private SecurityUserService securityUserService;

	private SystemPriorityService systemPriorityService;
	private SystemSchemaService systemSchemaService;

	private TradeOrderService tradeOrderService;

	private WebSocketHandler webSocketHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderTimedStatusCache() {
		super(Duration.ofSeconds(30), Duration.ofMinutes(3));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(Serializable identifier) {
		TradeOrder tradeOrder = getTradeOrderService().getTradeOrder((Integer) identifier);
		if (Objects.isNull(tradeOrder)) {
			LogUtils.error(LogCommand.ofMessageSupplier(this.getClass(), () -> String.format("Could not create 'SEND' status timeout notification because trade order [%s] was not found", identifier)));
		}
		else {
			sendNotification(tradeOrder);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Send a transient notification message for this timed-out trade order.
	 */
	private void sendNotification(TradeOrder tradeOrder) {
		WebsocketAlertMessage notificationMessage = new WebsocketAlertMessage(
				"Trade Order Timeout",
				createSubjectLine(tradeOrder),
				getSystemPriorityService().getSystemPriorityByName(SystemPriority.SYSTEM_PRIORITY_IMMEDIATE),
				getSystemSchemaService().getSystemTableByName(TradeOrder.TRADE_ORDER_TABLE_NAME),
				tradeOrder.getId()
		);
		List<SecurityUser> recipientList = getTradeOrderTeamSecurityUserList(tradeOrder);
		getWebSocketHandler().sendMessageToUserList(recipientList, WebsocketAlertMessage.CHANNEL_USER_TOPIC_IMMEDIATE_SYSTEM_TOAST_MESSAGES, notificationMessage);
	}


	/**
	 * Example:  Antony's Trade has been Canceled: Sell 3 TUM8 for Account #423000
	 */
	private String createSubjectLine(final TradeOrder tradeOrder) {
		StringBuilder builder = new StringBuilder();
		builder.append(tradeOrder.getTraderUser().getDisplayName());
		builder.append("'s");
		builder.append(" Trade has not been successfully Sent: ");
		builder.append(tradeOrder.isBuy() ? "BUY" : "SELL");
		builder.append(" ");
		builder.append(CoreMathUtils.formatNumber(tradeOrder.getQuantity(), null));
		builder.append(" ");
		builder.append(tradeOrder.getInvestmentSecurity().getSymbol());
		builder.append(" (Order ID: ");
		builder.append(tradeOrder.getId());
		builder.append(")");
		return builder.toString();
	}


	/**
	 * Get the unique list of users from the trade team security groups (include the trader too).
	 */
	private List<SecurityUser> getTradeOrderTeamSecurityUserList(final TradeOrder tradeOrder) {
		// get unique list of recipients from the trade order's -> trade -> client account -> servicing team
		List<SecurityUser> results = tradeOrder.getTradeList().stream()
				.map(Trade::getClientInvestmentAccount)
				.filter(Objects::nonNull)
				.map(InvestmentAccount::getTeamSecurityGroup)
				.map(SecurityGroup::getId)
				.flatMap(grp -> getSecurityUserService().getSecurityUserListByGroup(grp).stream())
				.distinct()
				.collect(Collectors.toList());
		if (!results.contains(tradeOrder.getTraderUser())) {
			results.add(tradeOrder.getTraderUser());
		}
		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemPriorityService getSystemPriorityService() {
		return this.systemPriorityService;
	}


	public void setSystemPriorityService(SystemPriorityService systemPriorityService) {
		this.systemPriorityService = systemPriorityService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}
}
