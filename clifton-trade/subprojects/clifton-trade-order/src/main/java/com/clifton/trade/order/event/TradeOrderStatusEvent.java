package com.clifton.trade.order.event;

import com.clifton.core.util.event.EventObject;
import com.clifton.trade.order.TradeOrder;


/**
 * <code>TradeOrderStatusEvent</code> base event for changes to the trade order status.
 */
public abstract class TradeOrderStatusEvent extends EventObject<TradeOrder, Object> {

	public TradeOrderStatusEvent(TradeOrder tradeOrder, String eventName) {
		setEventName(eventName);
		setTarget(tradeOrder);
	}
}
