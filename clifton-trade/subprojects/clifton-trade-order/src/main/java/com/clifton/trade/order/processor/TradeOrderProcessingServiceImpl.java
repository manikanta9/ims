package com.clifton.trade.order.processor;


import org.springframework.stereotype.Service;


@Service
public class TradeOrderProcessingServiceImpl implements TradeOrderProcessingService {

	private TradeOrderProcessor tradeOrderProcessor;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processTradeOrderList(Integer[] tradeOrderList) {
		getTradeOrderProcessor().processTradeOrderList(tradeOrderList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderProcessor getTradeOrderProcessor() {
		return this.tradeOrderProcessor;
	}


	public void setTradeOrderProcessor(TradeOrderProcessor tradeOrderProcessor) {
		this.tradeOrderProcessor = tradeOrderProcessor;
	}
}
