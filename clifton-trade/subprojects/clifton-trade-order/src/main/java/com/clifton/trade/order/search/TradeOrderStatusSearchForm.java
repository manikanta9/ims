package com.clifton.trade.order.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.trade.order.TradeOrderStatuses;


public class TradeOrderStatusSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private TradeOrderStatuses orderStatus;

	@SearchField
	private Boolean cancelIfNotFilled;

	@SearchField
	private Boolean partialFill;

	@SearchField
	private Boolean fillComplete;


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public TradeOrderStatuses getOrderStatus() {
		return this.orderStatus;
	}


	public void setOrderStatus(TradeOrderStatuses orderStatus) {
		this.orderStatus = orderStatus;
	}


	public Boolean getCancelIfNotFilled() {
		return this.cancelIfNotFilled;
	}


	public void setCancelIfNotFilled(Boolean cancelIfNotFilled) {
		this.cancelIfNotFilled = cancelIfNotFilled;
	}


	public Boolean getPartialFill() {
		return this.partialFill;
	}


	public void setPartialFill(Boolean partialFill) {
		this.partialFill = partialFill;
	}


	public Boolean getFillComplete() {
		return this.fillComplete;
	}


	public void setFillComplete(Boolean fillComplete) {
		this.fillComplete = fillComplete;
	}
}
