package com.clifton.trade.order.allocation;


import com.clifton.core.beans.NamedEntity;


public class TradeOrderAllocationStatus extends NamedEntity<Short> {

	private TradeOrderAllocationStatuses allocationStatus;

	/**
	 * The allocation report with the actual fills has been received.
	 */
	private boolean allocationFillReceived;

	/**
	 * Indicates that all allocation data has been received.
	 */
	private boolean allocationComplete;

	/**
	 * Indicates whether this allocation has been rejected
	 */
	private boolean rejected;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return getAllocationStatus().toString();
	}


	public TradeOrderAllocationStatuses getAllocationStatus() {
		return this.allocationStatus;
	}


	public void setAllocationStatus(TradeOrderAllocationStatuses allocationStatus) {
		this.allocationStatus = allocationStatus;
	}


	public boolean isAllocationFillReceived() {
		return this.allocationFillReceived;
	}


	public void setAllocationFillReceived(boolean allocationFillReceived) {
		this.allocationFillReceived = allocationFillReceived;
	}


	public boolean isAllocationComplete() {
		return this.allocationComplete;
	}


	public void setAllocationComplete(boolean allocationComplete) {
		this.allocationComplete = allocationComplete;
	}


	public boolean isRejected() {
		return this.rejected;
	}


	public void setRejected(boolean rejected) {
		this.rejected = rejected;
	}
}
