package com.clifton.trade.order.grouping;

import com.clifton.core.util.validation.ValidationException;
import com.clifton.trade.Trade;

import java.util.List;


/**
 * <code>TradeOrderGroupingCommand</code> is a command used to group trades for order processing.
 *
 * @author NickK
 */
public class TradeOrderGroupingCommand {

	private TradeOrderGroupingTypes groupingType;
	private List<Trade> tradeList;

	/**
	 * Properties used for customizing validation while grouping trades. By default, trades will be validated.
	 */
	private boolean ignoreValidation;
	/**
	 * Optional service class to use in a {@link com.clifton.core.validation.UserIgnorableValidationException} upon validation issues.
	 * If null, {@link ValidationException} will be thrown.
	 */
	private Class<?> ignoreValidationServiceClass;
	/**
	 * Optional service class method to used in a {@link com.clifton.core.validation.UserIgnorableValidationException} upon validation issues.
	 * If null, {@link ValidationException} will be thrown.
	 */
	private String ignoreValidationServiceClassMethod;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static TradeOrderGroupingCommand of(TradeOrderGroupingTypes groupingType) {
		TradeOrderGroupingCommand command = new TradeOrderGroupingCommand();
		command.setGroupingType(groupingType);
		return command;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public TradeOrderGroupingTypes getGroupingType() {
		return this.groupingType;
	}


	public void setGroupingType(TradeOrderGroupingTypes groupingType) {
		this.groupingType = groupingType;
	}


	public List<Trade> getTradeList() {
		return this.tradeList;
	}


	public void setTradeList(List<Trade> tradeList) {
		this.tradeList = tradeList;
	}


	public boolean isIgnoreValidation() {
		return this.ignoreValidation;
	}


	public void setIgnoreValidation(boolean ignoreValidation) {
		this.ignoreValidation = ignoreValidation;
	}


	public Class<?> getIgnoreValidationServiceClass() {
		return this.ignoreValidationServiceClass;
	}


	public void setIgnoreValidationServiceClass(Class<?> ignoreValidationServiceClass) {
		this.ignoreValidationServiceClass = ignoreValidationServiceClass;
	}


	public String getIgnoreValidationServiceClassMethod() {
		return this.ignoreValidationServiceClassMethod;
	}


	public void setIgnoreValidationServiceClassMethod(String ignoreValidationServiceClassMethod) {
		this.ignoreValidationServiceClassMethod = ignoreValidationServiceClassMethod;
	}
}
