package com.clifton.trade.order.allocation.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class TradeOrderAllocationSearchForm extends BaseAuditableEntitySearchForm {

	// Custom Search Filter
	private Integer tradeOrderId;

	// Custom Search Filter
	private Integer tradeOrderGroup;

	// Custom Search Filter
	private Integer externalOrderIdentifier;

	@SearchField
	private Boolean active;

	@SearchField(searchField = "originalOrderAllocation.id")
	private Integer originalOrderAllocationId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getTradeOrderId() {
		return this.tradeOrderId;
	}


	public void setTradeOrderId(Integer tradeOrderId) {
		this.tradeOrderId = tradeOrderId;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Integer getOriginalOrderAllocationId() {
		return this.originalOrderAllocationId;
	}


	public void setOriginalOrderAllocationId(Integer originalOrderAllocationId) {
		this.originalOrderAllocationId = originalOrderAllocationId;
	}


	public Integer getExternalOrderIdentifier() {
		return this.externalOrderIdentifier;
	}


	public void setExternalOrderIdentifier(Integer externalOrderIdentifier) {
		this.externalOrderIdentifier = externalOrderIdentifier;
	}


	public Integer getTradeOrderGroup() {
		return this.tradeOrderGroup;
	}


	public void setTradeOrderGroup(Integer tradeOrderGroup) {
		this.tradeOrderGroup = tradeOrderGroup;
	}
}
