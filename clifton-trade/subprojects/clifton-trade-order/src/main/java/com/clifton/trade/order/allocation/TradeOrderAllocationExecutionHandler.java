package com.clifton.trade.order.allocation;


import com.clifton.trade.order.TradeOrder;

import java.util.List;


/**
 * The <code>TradeOrderAllocationExecutionHandler</code> defines methods used to allocate a list of trade orders.
 * <p/>
 * NOTE: This is currently only implemented by the TradeOrderFixAllocationExecutionService class, but if there are more
 * allocation execution methods in the future then we can register a map in the TradeOrderProcessorImpl and lookup the correct
 * TradeOrderAllocationExecutionHandler implementation.
 *
 * @author mwacker
 */
public interface TradeOrderAllocationExecutionHandler {

	/**
	 * Send the order allocation message.
	 */
	public void allocationTradeOrder(List<TradeOrder> order);
}
