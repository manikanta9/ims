package com.clifton.trade.order.grouping;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class TradeOrderGroupingServiceImpl implements TradeOrderGroupingService {

	private TradeGroupService tradeGroupService;
	private Map<TradeOrderGroupingTypes, TradeOrderGroupingGrouper> tradeOrderGroupingGrouperMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<List<Trade>> groupTrades(TradeOrderGroupingCommand tradeOrderGroupingCommand) {
		validateTradeOrderGroupingCommand(tradeOrderGroupingCommand);
		TradeOrderGroupingGrouper grouper = getTradeOrderGroupingGrouper(tradeOrderGroupingCommand.getGroupingType());
		return grouper.groupTrades(tradeOrderGroupingCommand.getTradeList(), tradeOrderGroupingCommand.getGroupingType());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrderGroupingGrouper getTradeOrderGroupingGrouper(TradeOrderGroupingTypes groupingType) {
		ValidationUtils.assertTrue(getTradeOrderGroupingGrouperMap().containsKey(groupingType), "No [" + TradeOrderGroupingGrouper.class + "] is defined for [" + groupingType + "].");
		return getTradeOrderGroupingGrouperMap().get(groupingType);
	}


	private void validateTradeOrderGroupingCommand(TradeOrderGroupingCommand tradeOrderGroupingCommand) {
		if (!tradeOrderGroupingCommand.isIgnoreValidation()) {
			// Validate that trades of a group are in the same state as its peers for execution
			Map<Boolean, List<Trade>> tradeGroupTradeListMap = CollectionUtils.getPartitioned(tradeOrderGroupingCommand.getTradeList(), trade -> trade.getTradeGroup() == null);
			List<Trade> tradeGroupTradeList = tradeGroupTradeListMap.get(Boolean.FALSE);
			if (!CollectionUtils.isEmpty(tradeGroupTradeList)) {
				// Get applicable trade groups for trades
				Map<Integer, TradeGroup> tradeGroupIdToEntityMap = new HashMap<>();
				tradeGroupTradeList.forEach(trade -> tradeGroupIdToEntityMap.computeIfAbsent(trade.getTradeGroup().getId(), id -> getTradeGroupService().getTradeGroup(id)));
				// Map trades of distinct trade groups by state
				Map<String, List<Trade>> stateNameToTradeListMap = tradeGroupIdToEntityMap.values().stream().distinct()
						.map(TradeGroup::getTradeList)
						.flatMap(CollectionUtils::getStream)
						.collect(Collectors.groupingBy(trade -> trade.getWorkflowState().getName()));
				if (stateNameToTradeListMap.size() > 1) {
					String message = stateNameToTradeListMap.entrySet().stream()
							.map(entry -> entry.getKey() + ": count=" + entry.getValue().size())
							.collect(Collectors.joining("<br/>", "Found trade group trades in multiple workflow states and they may need to be executed together.<br/><br/>", ""));
					throw new UserIgnorableValidationException(message);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public Map<TradeOrderGroupingTypes, TradeOrderGroupingGrouper> getTradeOrderGroupingGrouperMap() {
		return this.tradeOrderGroupingGrouperMap;
	}


	public void setTradeOrderGroupingGrouperMap(Map<TradeOrderGroupingTypes, TradeOrderGroupingGrouper> tradeOrderGroupingGrouperMap) {
		this.tradeOrderGroupingGrouperMap = tradeOrderGroupingGrouperMap;
	}
}
