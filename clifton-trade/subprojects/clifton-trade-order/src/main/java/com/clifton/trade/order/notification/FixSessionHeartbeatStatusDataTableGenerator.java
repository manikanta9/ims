package com.clifton.trade.order.notification;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableGenerator;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.fix.api.client.FixMessageApi;
import com.clifton.fix.api.client.model.FixMessageEntryHeartbeatStatus;
import com.clifton.fix.api.client.model.FixMessageEntryHeartbeatStatusCommand;

import java.util.List;


/**
 * A DataTableGenerator for use with the FixSessionHeartbeatStatusNotification
 *
 * @author davidi
 */
public class FixSessionHeartbeatStatusDataTableGenerator implements DataTableGenerator {

	private static final String COLUMNS = "ID:sessionDefinitionId|" + //
			"Session:sessionDefinitionLabel|" + //
			"LastHeartBeatTime:lastHeartbeatDateAndTime|" + //
			"Failed:isFailed|" + //
			"Status Message:statusMessage|";
	private FixMessageApi fixMessageApi;
	private ApplicationContextService applicationContextService;
	private short maximumAllowedHeartbeatIntervalMinutes;
	private boolean activeSessionsOnly;
	private boolean returnWarningsOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable getDataTable() {
		FixMessageEntryHeartbeatStatusCommand statusCommand = new FixMessageEntryHeartbeatStatusCommand();
		statusCommand.setMaximumMinutesSinceLastHeartbeatWarn((int) getMaximumAllowedHeartbeatIntervalMinutes());
		statusCommand.setActiveSessionsOnly(isActiveSessionsOnly());
		statusCommand.setReturnWarningsOnly(isReturnWarningsOnly());
		List<FixMessageEntryHeartbeatStatus> heartbeatStatusList = getFixMessageApi().getFixMessageLastHeartbeatBySession(statusCommand);

		// get status and implement data table
		BeanCollectionToDataTableConverter<FixMessageEntryHeartbeatStatus> converter = new BeanCollectionToDataTableConverter<>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(),
				FixMessageEntryHeartbeatStatus.class, COLUMNS));
		return converter.convert(heartbeatStatusList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageApi getFixMessageApi() {
		return this.fixMessageApi;
	}


	public void setFixMessageApi(FixMessageApi fixMessageApi) {
		this.fixMessageApi = fixMessageApi;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public short getMaximumAllowedHeartbeatIntervalMinutes() {
		return this.maximumAllowedHeartbeatIntervalMinutes;
	}


	public void setMaximumAllowedHeartbeatIntervalMinutes(short maximumAllowedHeartbeatIntervalMinutes) {
		this.maximumAllowedHeartbeatIntervalMinutes = maximumAllowedHeartbeatIntervalMinutes;
	}


	public boolean isActiveSessionsOnly() {
		return this.activeSessionsOnly;
	}


	public void setActiveSessionsOnly(boolean activeSessionsOnly) {
		this.activeSessionsOnly = activeSessionsOnly;
	}


	public boolean isReturnWarningsOnly() {
		return this.returnWarningsOnly;
	}


	public void setReturnWarningsOnly(boolean returnWarningsOnly) {
		this.returnWarningsOnly = returnWarningsOnly;
	}
}
