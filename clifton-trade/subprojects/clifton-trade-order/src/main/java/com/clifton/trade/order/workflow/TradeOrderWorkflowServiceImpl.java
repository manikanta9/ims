package com.clifton.trade.order.workflow;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class TradeOrderWorkflowServiceImpl implements TradeOrderWorkflowService {

	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade setTradeWorkflowState(Trade trade, String stateName) {
		WorkflowState workflowState = getNextTradeWorkflowState(trade, stateName);
		if (workflowState != null) {
			return (Trade) getWorkflowTransitionService().executeWorkflowTransitionSystemDefined("Trade", trade.getId(), workflowState.getId());
		}
		throw new RuntimeException("Workflow state [" + stateName + "] does not exist.");
	}


	@Override
	public void setTradeWorkflowStates(TradeOrder order, String stateName) {
		for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
			WorkflowState workflowState = getNextTradeWorkflowState(trade, stateName);
			if (workflowState != null) {
				getWorkflowTransitionService().executeWorkflowTransitionSystemDefined("Trade", trade.getId(), workflowState.getId());
			}
		}
	}


	@Override
	public WorkflowState getNextTradeWorkflowState(Trade trade, String stateName) {
		List<WorkflowState> nextStates = getWorkflowDefinitionService().getWorkflowStateNextAllowedList("Trade", BeanUtils.getIdentityAsLong(trade));
		for (WorkflowState state : CollectionUtils.getIterable(nextStates)) {
			if (stateName.equals(state.getName())) {
				return state;
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
