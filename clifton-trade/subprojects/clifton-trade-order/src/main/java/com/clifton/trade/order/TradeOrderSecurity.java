package com.clifton.trade.order;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;


/**
 * The <code>TradeOrderSecurity</code> contains a link to a security for orders that contain
 * multiple securities.
 *
 * @author mwacker
 */
public class TradeOrderSecurity extends BaseEntity<Integer> {

	private TradeOrder order;
	private InvestmentSecurity investmentSecurity;

	private BigDecimal averagePrice;
	private BigDecimal commissionPerUnit;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public TradeOrder getOrder() {
		return this.order;
	}


	public void setOrder(TradeOrder tradeOrder) {
		this.order = tradeOrder;
	}


	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}


	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}
}
