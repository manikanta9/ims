package com.clifton.trade.order.allocation;


import com.clifton.core.beans.BaseEntity;
import com.clifton.trade.order.TradeOrder;

import java.util.List;


/**
 * The <code>TradeOrderAllocation</code> defines an allocation that can be across multiple orders.
 *
 * @author mwacker
 */
public class TradeOrderAllocation extends BaseEntity<Integer> {

	private TradeOrderAllocation originalOrderAllocation;

	/**
	 * The unique id provided by an external application.  The typical instance
	 * is the FixIdentifierID which reference the FixIdentifier in the CliftonFIX database.
	 */
	private Integer externalIdentifier;

	/**
	 * Indicated that this is the active allocation.
	 */
	private boolean active;


	private List<TradeOrder> tradeOrderList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderAllocation getOriginalOrderAllocation() {
		return this.originalOrderAllocation;
	}


	public void setOriginalOrderAllocation(TradeOrderAllocation originalOrderAllocation) {
		this.originalOrderAllocation = originalOrderAllocation;
	}


	public List<TradeOrder> getTradeOrderList() {
		return this.tradeOrderList;
	}


	public void setTradeOrderList(List<TradeOrder> tradeOrderList) {
		this.tradeOrderList = tradeOrderList;
	}


	public boolean isActive() {
		return this.active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public Integer getExternalIdentifier() {
		return this.externalIdentifier;
	}


	public void setExternalIdentifier(Integer externalIdentifier) {
		this.externalIdentifier = externalIdentifier;
	}
}
