package com.clifton.trade.order.adjustment;


import com.clifton.core.comparison.Ordered;


/**
 * The <code>TradeOrderAdjustment</code> defined methods used to apply adjustments to incoming order messages.
 *
 * @author mwacker
 */
public interface TradeOrderAdjustment extends Ordered {


	/**
	 * If the object to be "adjusted" is supported by this specific implementation
	 * These are used for TradeOrders and TradeFills
	 */
	public boolean isSupported(Object obj);


	/**
	 * Perform the adjustment on the object (TradeOrder or TradeFill)
	 */
	public <T> T adjust(T obj);
}
