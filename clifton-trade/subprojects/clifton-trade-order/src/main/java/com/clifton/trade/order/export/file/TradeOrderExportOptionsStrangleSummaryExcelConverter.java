package com.clifton.trade.order.export.file;


import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderSecurity;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;
import com.clifton.core.util.MathUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Creates an excel file the options strangle summary information.
 */
public class TradeOrderExportOptionsStrangleSummaryExcelConverter extends AbstractTradeOrderExportExcelConverter {

	private SystemColumnValueHandler systemColumnValueHandler;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataLiveService marketDataLiveService;


	@Override
	public FileWrapper convert(List<TradeOrder> tradeOrderList) {
		Workbook workbook = ExcelFileUtils.createWorkbook(FileFormats.EXCEL_OPEN_XML.getFormatString());

		Map<InvestmentSecurity, String> putCallMap = getPutCallMap(tradeOrderList);

		Sheet sheet = workbook.getSheetAt(0);
		int rowIndex = -1;
		for (TradeOrder order : CollectionUtils.getIterable(tradeOrderList)) {
			ValidationUtils.assertTrue(TradeOrderGroupingTypes.OPTIONS_STRANGLE == order.getType().getGroupingType(), "[" + getClass().getName() + "] does not support with grouping type ["
					+ order.getType().getGroupingType() + "].");

			rowIndex++;
			rowIndex = addOrderDetail(order, sheet, rowIndex, putCallMap);
			rowIndex++;
		}

		// resize the columns
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);

		return new FileWrapper(ExcelFileUtils.getFileFromWorkbook(workbook, "OptionsStrangleSummaryExport"), "OptionsStrangleSummaryExport." + FileFormats.EXCEL_OPEN_XML.getFormatString(), true);
	}


	private int addOrderDetail(TradeOrder order, Sheet sheet, int rowIndex, Map<InvestmentSecurity, String> putCallMap) {
		addCell(sheet, rowIndex, 0, (order.isBuy() ? "Buy" : "Sell") + " " + (order.isMultipleSecurityOrder() ? "STRANGLE" : putCallMap.get(order.getInvestmentSecurity()).toUpperCase()), true);

		rowIndex++;
		addCell(sheet, rowIndex, 0, "Notional:", true);
		BigDecimal notional = sumOptionNotional(order.getTradeList());
		if (order.isMultipleSecurityOrder()) {
			notional = MathUtils.divide(notional, new BigDecimal(order.getSecurityList().size()));
		}
		addCell(sheet, rowIndex, 1, notional, DataTypes.MONEY);
		if (order.isMultipleSecurityOrder()) {
			addCell(sheet, rowIndex, 2, "(each leg)", false);
		}

		Date endDate;
		rowIndex++;
		if (!order.isMultipleSecurityOrder()) {
			boolean put = "put".equals(putCallMap.get(order.getInvestmentSecurity()));
			addCell(sheet, rowIndex, 0, put ? "Put Option:" : "Call Option:", true);
			addCell(sheet, rowIndex, 1, order.getInvestmentSecurity().getDescription());
			endDate = order.getInvestmentSecurity().getEndDate();
		}
		else {
			// add the put
			addSecurity(order, sheet, rowIndex, true, putCallMap);
			rowIndex++;
			// add the call
			addSecurity(order, sheet, rowIndex, false, putCallMap);
			endDate = order.getSecurityList().get(0).getInvestmentSecurity().getEndDate();
		}

		rowIndex++;
		addCell(sheet, rowIndex, 0, "Expiration:", true);
		addCell(sheet, rowIndex, 1, endDate, DataTypes.DATE);

		rowIndex++;
		addCell(sheet, rowIndex, 0, "# Contracts:", true);
		addCell(sheet, rowIndex, 1, (order.isBuy() ? "buy " : "sell ") + CoreMathUtils.formatNumberInteger(order.getQuantity()) + " contracts");
		if (order.isMultipleSecurityOrder()) {
			addCell(sheet, rowIndex, 2, "(each leg)", false);
		}

		return rowIndex;
	}


	private BigDecimal sumOptionNotional(List<Trade> tradeList) {
		BigDecimal result = BigDecimal.ZERO;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurityByInstrument(trade.getInvestmentSecurity().getInstrument().getUnderlyingInstrument().getId());
			MarketDataLiveCommand command = new MarketDataLiveCommand();
			command.setMaxAgeInSeconds(60 * 60); // One hour
			BigDecimal price = getMarketDataLiveService().getMarketDataLivePrice(security.getId(), command).asNumericValue();
			BigDecimal n = MathUtils.multiply(MathUtils.multiply(price, trade.getInvestmentSecurity().getPriceMultiplier()), trade.getQuantityIntended(), 2);
			result = MathUtils.add(result, n);
		}
		return result;
	}


	protected void addSecurity(TradeOrder order, Sheet sheet, int rowIndex, boolean put, Map<InvestmentSecurity, String> putCallMap) {
		for (TradeOrderSecurity security : CollectionUtils.getIterable(order.getSecurityList())) {
			boolean securityIsPut = "put".equals(putCallMap.get(security.getInvestmentSecurity()));
			if (put && securityIsPut) {
				addCell(sheet, rowIndex, 0, "Put Option:", true);
				addCell(sheet, rowIndex, 1, security.getInvestmentSecurity().getDescription());
				return;
			}
			else if (!put && !securityIsPut) {
				addCell(sheet, rowIndex, 0, "Call Option:", true);
				addCell(sheet, rowIndex, 1, security.getInvestmentSecurity().getDescription());
				return;
			}
		}
	}


	public Map<InvestmentSecurity, String> getPutCallMap(List<TradeOrder> tradeOrderList) {
		Map<InvestmentSecurity, String> result = new HashMap<>();
		for (TradeOrder order : CollectionUtils.getIterable(tradeOrderList)) {
			if (order.isMultipleSecurityOrder()) {
				for (TradeOrderSecurity security : CollectionUtils.getIterable(order.getSecurityList())) {
					if (!result.containsKey(order.getInvestmentSecurity())) {
						String putCall = security.getInvestmentSecurity().getOptionType().toString();
						result.put(order.getInvestmentSecurity(), putCall.toLowerCase());
					}
				}
			}
			else {
				if (!result.containsKey(order.getInvestmentSecurity())) {
					String putCall = order.getInvestmentSecurity().getOptionType().toString();
					result.put(order.getInvestmentSecurity(), putCall.toLowerCase());
				}
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
