package com.clifton.trade.order.export.file;


import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.trade.order.TradeOrder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Date;
import java.util.List;


public abstract class AbstractTradeOrderExportExcelConverter implements Converter<List<TradeOrder>, FileWrapper> {

	protected Cell addCell(Sheet sheet, int rowIndex, int columnIndex, Object value) {
		return addCell(sheet, rowIndex, columnIndex, value, null, false);
	}


	protected Cell addCell(Sheet sheet, int rowIndex, int columnIndex, Object value, boolean bold) {
		return addCell(sheet, rowIndex, columnIndex, value, null, bold);
	}


	protected Cell addCell(Sheet sheet, int rowIndex, int columnIndex, Object value, DataTypes dataType) {
		return addCell(sheet, rowIndex, columnIndex, value, dataType, false);
	}


	protected Cell addCell(Sheet sheet, int rowIndex, int columnIndex, Object value, DataTypes dataType, boolean bold) {
		Row row = ExcelFileUtils.getRow(sheet, rowIndex, true);
		Cell cell = ExcelFileUtils.getCell(sheet, rowIndex, columnIndex, true);
		if (cell == null) {
			cell = row.createCell(columnIndex);
		}
		if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		}
		else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		}
		else if (value instanceof Number) {
			cell.setCellValue(((Number) value).doubleValue());
		}
		else if (value != null) {
			cell.setCellValue(value.toString());
		}
		CellStyle style = getCellStyle(cell, dataType, bold);
		if (style != null) {
			cell.setCellStyle(style);
		}
		return cell;
	}


	private CellStyle getCellStyle(Cell cell, DataTypes dataType, boolean bold) {
		Workbook workbook = cell.getSheet().getWorkbook();
		Font font = workbook.createFont();
		CellStyle cellStyle = workbook.createCellStyle();

		// Setup Font
		font.setFontHeightInPoints((short) 10);
		if (bold) {
			font.setBold(true);
		}

		DataFormat df = workbook.createDataFormat();
		if (dataType != null) {
			switch (dataType.getTypeName()) {
				case INTEGER:
					cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_INTEGER));
					break;
				case DATE:
					cellStyle.setDataFormat(df.getFormat(DateUtils.DATE_FORMAT_INPUT));
					break;
				case DECIMAL:
					if (dataType == DataTypes.MONEY) {
						cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_MONEY));
					}
					else if (dataType == DataTypes.MONEY4) {
						cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_MONEY4));
					}
					else if (dataType.isPercent()) {
						cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_PERCENT));
					}
					else {
						cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_DECIMAL));
					}
					break;
				default:
					cellStyle.setDataFormat(df.getFormat("TEXT"));
					break;
			}
		}
		cellStyle.setFont(font);
		return cellStyle;
	}
}
