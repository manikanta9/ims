package com.clifton.trade.order.adjustment;


import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.trade.TradeFill;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;

import java.math.BigDecimal;


public class TradeOrderAdjustmentPriceMultiplier extends AbstractTradeOrderAdjustment {

	private MarketDataSourceService marketDataSourceService;

	private TradeOrderService tradeOrderService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isSupported(Object obj) {
		return obj instanceof TradeOrder || obj instanceof TradeFill;
	}


	//	public boolean isSupported(Object obj) {
	//		return obj instanceof TradeFill;
	//	}


	@Override
	public <T> T adjust(T obj) {
		if (obj instanceof TradeOrder) {
			TradeOrder order = (TradeOrder) obj;
			MarketDataSourcePurpose purpose = order.getDestinationMapping().getTradeDestination().getMarketDataSourcePurpose();
			order.setAveragePrice(adjust(order.getAveragePrice(), order.getInvestmentSecurity().getInstrument(), order.getDestinationMapping().getExecutingBrokerCompany(), purpose));
		}
		else if (obj instanceof TradeFill) {
			TradeFill fill = (TradeFill) obj;
			MarketDataSourcePurpose purpose = null;
			if (fill.getTrade().getOrderIdentifier() != null) {
				TradeOrder tradeOrder = getTradeOrderService().getTradeOrder(fill.getTrade().getOrderIdentifier());
				if (tradeOrder != null) {
					purpose = tradeOrder.getDestinationMapping().getTradeDestination().getMarketDataSourcePurpose();
				}
			}
			fill.setNotionalUnitPrice(adjust(fill.getNotionalUnitPrice(), fill.getTrade().getInvestmentSecurity().getInstrument(), fill.getTrade().getExecutingBrokerCompany(), purpose));
		}
		return obj;
	}


	public BigDecimal adjust(BigDecimal price, InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose) {
		if (price != null) {
			return getMarketDataSourceService().adjustIncomingMarketDataSourcePrice(price, instrument, company, purpose);
		}
		return price;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}
}
