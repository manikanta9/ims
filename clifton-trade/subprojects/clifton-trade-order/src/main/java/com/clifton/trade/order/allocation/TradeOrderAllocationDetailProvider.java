package com.clifton.trade.order.allocation;


import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessorParameters;

import java.util.List;


/**
 * The <code>TradeOrderAllocationDetailProvider</code> defines method(s) used to populate allocation details
 * from a specific order provider i.e. FIX.
 *
 * @author mwacker
 */
public interface TradeOrderAllocationDetailProvider {

	/**
	 * Gets the list of allocation details.
	 */
	public List<TradeOrderAllocationDetail> getTradeOrderAllocationDetails(TradeOrderAllocationProcessorParameters allocationId);
}
