package com.clifton.trade.order.grouping;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.mapping.InvestmentAccountMappingService;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeTimeInForceType;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyService;
import com.clifton.trade.group.TradeGroupType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DefaultTradeOrderGroupingGrouper implements TradeOrderGroupingGrouper {

	private TradeDestinationService tradeDestinationService;
	private TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService;
	private InvestmentAccountMappingService investmentAccountMappingService;
	private InvestmentAccountUtilHandler investmentAccountUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<List<Trade>> groupTrades(List<Trade> tradeList, TradeOrderGroupingTypes groupingType) {
		ValidationUtils.assertTrue(TradeOrderGroupingTypes.DEFAULT == groupingType || TradeOrderGroupingTypes.DEFAULT_BY_HOLDING_ACCOUNT == groupingType || TradeOrderGroupingTypes.DEFAULT_LIMIT == groupingType,
				"[" + this.getClass() + "] is not compatible with grouping type [" + groupingType + "].");

		List<List<Trade>> result = new ArrayList<>();
		for (Trade trade : tradeList) {
			if (!trade.isFixTrade()) {
				throw new RuntimeException("Trade '" + trade.getId() + "' is not a fix trade.");
			}
			List<Trade> targetList = getTradeListForGrouping(result, trade, groupingType);
			if (targetList == null) {
				targetList = new ArrayList<>();
				result.add(targetList);
			}
			targetList.add(trade);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<Trade> getTradeListForGrouping(List<List<Trade>> tradeGroupList, Trade trade, TradeOrderGroupingTypes groupingType) {
		Map<Integer, String> tradeBrokerIdKeyMap = new HashMap<>();
		for (List<Trade> tradeList : CollectionUtils.getIterable(tradeGroupList)) {
			if (!tradeList.isEmpty() && isGroupingForExecutionAllowed(trade, tradeList.get(0), groupingType, tradeBrokerIdKeyMap)) {
				return tradeList;
			}
		}
		return null;
	}


	/**
	 * Compares two trades to determine if they can be grouped into a single TradeOrder.  Note that the calling function must ensure that all trades are FIX trades.
	 *
	 * @return True - if the two trades can be grouped, False if the trades cannot be grouped.
	 */
	private boolean isGroupingForExecutionAllowed(Trade trade1, Trade trade2, TradeOrderGroupingTypes groupingType, Map<Integer, String> tradeBrokerIdKeyMap) {
		boolean groupByHoldingAccount = TradeOrderGroupingTypes.DEFAULT_BY_HOLDING_ACCOUNT == groupingType;

		TradeDestinationMapping mapping1 = getTradeDestinationService().getTradeDestinationMappingByCommand(new TradeDestinationMappingSearchCommand(trade1));
		TradeDestinationMapping mapping2 = getTradeDestinationService().getTradeDestinationMappingByCommand(new TradeDestinationMappingSearchCommand(trade2));

		if (!mapping1.getMappingConfiguration().isFixTradeGroupingAllowed() || !mapping2.getMappingConfiguration().isFixTradeGroupingAllowed()) {
			return false;
		}

		// Trades can only be grouped if the mapping purposes are the same.
		String orderMappingPurposeName1 = mapping1.getOrderAccountMappingPurpose() == null ? null : mapping1.getOrderAccountMappingPurpose().getName();
		String orderMappingPurposeName2 = mapping2.getOrderAccountMappingPurpose() == null ? null : mapping2.getOrderAccountMappingPurpose().getName();
		if (!StringUtils.isEqual(orderMappingPurposeName1, orderMappingPurposeName2)) {
			return false;
		}

		// For order grouping mapped values must also match.
		if (orderMappingPurposeName1 != null && orderMappingPurposeName2 != null) {
			String mappedValue1 = this.getInvestmentAccountMappingService().getInvestmentAccountMappedValue(trade1.getHoldingInvestmentAccount(), mapping1.getOrderAccountMappingPurpose());
			String mappedValue2 = this.getInvestmentAccountMappingService().getInvestmentAccountMappedValue(trade2.getHoldingInvestmentAccount(), mapping2.getOrderAccountMappingPurpose());

			if (!StringUtils.isEqual(mappedValue1, mappedValue2)) {
				return false;
			}
		}

		if (mapping1.getMappingConfiguration().isFixTradeGroupingByHoldingAccount() || mapping2.getMappingConfiguration().isFixTradeGroupingByHoldingAccount()) {
			groupByHoldingAccount = true;
		}

		if (!mapping1.getMappingConfiguration().isFixTradeGroupingAllowed() || !mapping2.getMappingConfiguration().isFixTradeGroupingAllowed()) {
			return false;
		}

		if (groupByHoldingAccount) {
			if (!trade1.getHoldingInvestmentAccount().equals(trade2.getHoldingInvestmentAccount())) {
				return false;
			}
		}
		if (!trade1.getInvestmentSecurity().equals(trade2.getInvestmentSecurity())) {
			return false;
		}
		if (!isExecutingBrokerCompanyListEqual(trade1, trade2, tradeBrokerIdKeyMap)) {
			return false;
		}
		if (!CompareUtils.isEqual(trade1.getExecutingSponsorCompany(), trade2.getExecutingSponsorCompany())) {
			return false;
		}
		if (trade1.isBuy() != trade2.isBuy()) {
			return false;
		}
		// only group like open/close types (could replace the buy/sell...?)
		if (!trade1.getOpenCloseType().equals(trade2.getOpenCloseType())) {
			return false;
		}
		if (!trade1.getTraderUser().equals(trade2.getTraderUser())) {
			return false;
		}
		if (!trade1.getTradeDestination().equals(trade2.getTradeDestination())) {
			return false;
		}
		if (isTradeBondRollTradeGroupMember(trade1) && isTradeBondRollTradeGroupMember(trade2)
				&& !trade1.getTradeGroup().equals(trade2.getTradeGroup())) {
			/*
			 * Bond Roll trade groups are capable of splitting a total rolled face amount proportionally across weighted security allocations.
			 * During execution of these trades, they should be grouped by the trade group representing the weighted security allocation. A use
			 * case for this is when a large total face amount for a particular CUSIP needs to be traded in a Bond Roll across accounts. The total
			 * face amount can be split up for that CUSIP to avoid moving the market (e.g. Need to trade $2,400,000,000 for a CUSIP; the total can be
			 * broken up into two allocations of 40% and 60% resulting in executions of $960,000,000 and $1,440,000,000 respectively).
			 */
			return false;
		}
		if (!CompareUtils.isEqual(trade1.getTradeExecutionType(), trade2.getTradeExecutionType())) {
			return false;
		}
		if (!MathUtils.isEqual(trade1.getLimitPrice(), trade2.getLimitPrice())) {
			return false;
		}
		if (!isTradeTimeInForceTypeEqual(trade1, trade2)) {
			return false;
		}
		if (!isTimeInForceExpirationDateEqual(trade1, trade2)) {
			return false;
		}

		return true;
	}


	/**
	 * Determines if the TradeTimeInForce between trade1 and trade2 are logically equal, and returns true if they are equal, false if not.
	 */
	private boolean isTradeTimeInForceTypeEqual(Trade trade1, Trade trade2) {

		if (trade1.getTradeTimeInForceType() == null && trade2.getTradeTimeInForceType() == null) {
			return true;
		}

		if (trade1.getTradeTimeInForceType() != null && trade1.getTradeTimeInForceType().equals(trade2.getTradeTimeInForceType())) {
			return true;
		}

		if (trade2.getTradeTimeInForceType() != null && trade2.getTradeTimeInForceType().equals(trade1.getTradeTimeInForceType())) {
			return true;
		}

		// TradeTimeInForceType "DAY" is equivalent to a null TradeTimeInForce.
		if (trade1.getTradeTimeInForceType() != null && TradeTimeInForceType.DAY.equals(trade1.getTradeTimeInForceType().getName()) && trade2.getTradeTimeInForceType() == null) {
			return true;
		}

		if (trade2.getTradeTimeInForceType() != null && TradeTimeInForceType.DAY.equals(trade2.getTradeTimeInForceType().getName()) && trade1.getTradeTimeInForceType() == null) {
			return true;
		}

		return false;
	}


	/**
	 * Determines if the TimeInForceExpirationDate values between trade 1 and trade 2 are logically equal based on multiple conditions.
	 *
	 */
	private boolean isTimeInForceExpirationDateEqual(Trade trade1, Trade trade2) {
		if (trade1.getTimeInForceExpirationDate() == null && trade2.getTimeInForceExpirationDate() == null) {
			return true;
		}

		if (trade1.getTimeInForceExpirationDate() != null && trade1.getTimeInForceExpirationDate().equals(trade2.getTimeInForceExpirationDate())) {
			return true;
		}

		if (trade2.getTimeInForceExpirationDate() != null && trade2.getTimeInForceExpirationDate().equals(trade1.getTimeInForceExpirationDate())) {
			return true;
		}

		return false;
	}

	private boolean isExecutingBrokerCompanyListEqual(Trade trade1, Trade trade2, Map<Integer, String> tradeBrokerIdKeyMap) {
		String trade1BrokerKey = getExecutingBrokerListKeyForTrade(trade1, tradeBrokerIdKeyMap);
		String trade2BrokerKey = getExecutingBrokerListKeyForTrade(trade2, tradeBrokerIdKeyMap);
		return StringUtils.isEqual(trade1BrokerKey, trade2BrokerKey);
	}


	private String getExecutingBrokerListKeyForTrade(Trade trade, Map<Integer, String> tradeBrokerIdKeyMap) {
		if (tradeBrokerIdKeyMap.containsKey(trade.getId())) {
			return tradeBrokerIdKeyMap.get(trade.getId());
		}
		List<BusinessCompany> executingBrokerList = getExecutingBrokerListForTrade(trade);
		executingBrokerList = BeanUtils.sortWithFunction(executingBrokerList, BusinessCompany::getId, true);
		String result = Arrays.toString(BeanUtils.getPropertyValuesExcludeNull(executingBrokerList, BusinessCompany::getId));
		tradeBrokerIdKeyMap.put(trade.getId(), result);
		return result;
	}


	private boolean isTradeBondRollTradeGroupMember(Trade trade) {
		return trade.getTradeGroup() != null && trade.getTradeGroup().getTradeGroupType().getGroupType() == TradeGroupType.GroupTypes.BOND_ROLL;
	}


	private List<BusinessCompany> getExecutingBrokerListForTrade(Trade trade) {
		if (trade.getExecutingBrokerCompany() == null) {
			return getTradeExecutingBrokerCompanyService().getTradeExecutingBrokerCompanyListByTrade(trade);
		}
		return CollectionUtils.createList(trade.getExecutingBrokerCompany());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeExecutingBrokerCompanyService getTradeExecutingBrokerCompanyService() {
		return this.tradeExecutingBrokerCompanyService;
	}


	public void setTradeExecutingBrokerCompanyService(TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService) {
		this.tradeExecutingBrokerCompanyService = tradeExecutingBrokerCompanyService;
	}


	public InvestmentAccountMappingService getInvestmentAccountMappingService() {
		return this.investmentAccountMappingService;
	}


	public void setInvestmentAccountMappingService(InvestmentAccountMappingService investmentAccountMappingService) {
		this.investmentAccountMappingService = investmentAccountMappingService;
	}


	public InvestmentAccountUtilHandler getInvestmentAccountUtilHandler() {
		return this.investmentAccountUtilHandler;
	}


	public void setInvestmentAccountUtilHandler(InvestmentAccountUtilHandler investmentAccountUtilHandler) {
		this.investmentAccountUtilHandler = investmentAccountUtilHandler;
	}
}
