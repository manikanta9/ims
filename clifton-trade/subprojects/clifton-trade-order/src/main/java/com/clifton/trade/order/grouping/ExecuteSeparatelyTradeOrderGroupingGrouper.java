package com.clifton.trade.order.grouping;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;

import java.util.ArrayList;
import java.util.List;


public class ExecuteSeparatelyTradeOrderGroupingGrouper implements TradeOrderGroupingGrouper {

	@Override
	public List<List<Trade>> groupTrades(List<Trade> tradeList, TradeOrderGroupingTypes groupingType) {
		ValidationUtils.assertTrue(TradeOrderGroupingTypes.EXECUTE_SEPARATELY == groupingType || TradeOrderGroupingTypes.EXECUTE_SEPARATELY_LIMIT == groupingType, "[" + this.getClass() + "] is not compatible with grouping type [" + groupingType + "].");
		List<List<Trade>> result = new ArrayList<>();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			List<Trade> rt = CollectionUtils.createList(trade);
			result.add(rt);
		}
		return result;
	}
}
