package com.clifton.trade.order.adjustment;


import com.clifton.trade.TradeFill;
import com.clifton.trade.order.TradeOrder;
import com.clifton.core.util.MathUtils;


/**
 * The <code>TradeOrderAdjustmentTenDecimalPrice</code> truncates the incoming price to 10 decimal places (or 11 characters including the '.')
 * <p/>
 * For example, when the price is 1256.7894561 @ 10 precision places the function returns 1256.789456 and 45678.4561237
 * and 10 precision places the function returns 45678.45612.
 *
 * @author mwacker
 */
public class TradeOrderAdjustmentTenDecimalPrice extends AbstractTradeOrderAdjustment {

	@Override
	public boolean isSupported(Object obj) {
		return obj instanceof TradeOrder || obj instanceof TradeFill;
	}


	@Override
	public <T> T adjust(T obj) {
		if (obj instanceof TradeOrder) {
			TradeOrder order = (TradeOrder) obj;
			if (order.getAveragePrice() != null) {
				order.setAveragePrice(MathUtils.truncateDecimals(order.getAveragePrice(), 10));
			}
		}
		else if (obj instanceof TradeFill) {
			TradeFill fill = (TradeFill) obj;
			fill.setNotionalUnitPrice(MathUtils.truncateDecimals(fill.getNotionalUnitPrice(), 10));
		}
		return obj;
	}
}
