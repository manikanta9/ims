package com.clifton.trade.order.grouping;


import com.clifton.trade.Trade;

import java.util.List;


/**
 * The <code>TradeOrderGroupingHandler</code> ...
 *
 * @author mwacker
 */
public interface TradeOrderGroupingGrouper {

	public List<List<Trade>> groupTrades(List<Trade> tradeList, TradeOrderGroupingTypes groupingType);
}
