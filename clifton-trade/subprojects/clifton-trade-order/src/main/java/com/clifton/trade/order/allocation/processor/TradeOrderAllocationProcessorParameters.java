package com.clifton.trade.order.allocation.processor;


import com.clifton.trade.destination.TradeDestinationMapping;

import java.util.List;


/**
 * The <code>TradeOrderAllocationProcessorParameters</code> defines the parameters needed by <code>TradeOrderAllocationDetailProvider</code>
 * to generate <code>TradeOrderAllocationDetail</code>'s.
 *
 * @author mwacker
 */
public class TradeOrderAllocationProcessorParameters {

	private TradeDestinationMapping destinationMapping;
	private Integer externalAllocationIdentifier;
	@SuppressWarnings("rawtypes")
	private List allocationReports;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeDestinationMapping getDestinationMapping() {
		return this.destinationMapping;
	}


	public void setDestinationMapping(TradeDestinationMapping destinationMapping) {
		this.destinationMapping = destinationMapping;
	}


	@SuppressWarnings("unchecked")
	public <T> List<T> getAllocationReports() {
		return this.allocationReports;
	}


	public <T> void setAllocationReports(List<T> allocationReports) {
		this.allocationReports = allocationReports;
	}


	public Integer getExternalAllocationIdentifier() {
		return this.externalAllocationIdentifier;
	}


	public void setExternalAllocationIdentifier(Integer externalAllocationIdentifier) {
		this.externalAllocationIdentifier = externalAllocationIdentifier;
	}
}
