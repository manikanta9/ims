package com.clifton.trade.order.allocation.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeOrderAllocationStatusCache</code> class provides caching and retrieval from cache of TradeOrderAllocationStatus objects
 * by name.
 *
 * @author manderson
 */
@Component
public class TradeOrderAllocationStatusCache extends SelfRegisteringSingleKeyDaoListCache<TradeOrderAllocationStatus, TradeOrderAllocationStatuses> {

	@Override
	protected String getBeanKeyProperty() {
		return "allocationStatus";
	}


	@Override
	protected TradeOrderAllocationStatuses getBeanKeyValue(TradeOrderAllocationStatus bean) {
		return bean.getAllocationStatus();
	}
}
