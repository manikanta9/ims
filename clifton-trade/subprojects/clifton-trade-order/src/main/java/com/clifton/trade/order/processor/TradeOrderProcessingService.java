package com.clifton.trade.order.processor;


public interface TradeOrderProcessingService {

	/**
	 * Process a list of orders.  Generates a list of <code>TradeOrderProcessorParameters</code> with type for each order that has not been allocated PROCESS_ORDER
	 * and type PROCESS_ORDER_ALLOCATION for orders that have been allocated.  It then calls processTradeOrder for each <code>TradeOrderProcessorParameters</code>.
	 *
	 * @param tradeOrderList
	 */
	public void processTradeOrderList(Integer[] tradeOrderList);
}
