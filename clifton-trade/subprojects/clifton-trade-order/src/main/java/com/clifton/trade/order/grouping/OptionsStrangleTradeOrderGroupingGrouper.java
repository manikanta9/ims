package com.clifton.trade.order.grouping;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class OptionsStrangleTradeOrderGroupingGrouper implements TradeOrderGroupingGrouper {

	private SystemColumnValueHandler systemColumnValueHandler;
	private TradeService tradeService;
	private TradeOrderWorkflowService tradeOrderWorkflowService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<List<Trade>> groupTrades(List<Trade> tradeList, TradeOrderGroupingTypes groupingType) {
		ValidationUtils.assertTrue(TradeOrderGroupingTypes.OPTIONS_STRANGLE == groupingType, "[" + this.getClass() + "] is not compatible with grouping type [" + groupingType + "].");
		Map<InvestmentSecurity, String> putCallMap = validateTradeList(tradeList);
		Map<InvestmentAccount, List<Trade>> groupedTradeList = groupByHoldingAccount(tradeList);

		Map<String, List<Trade>> tradeListByGroup = new HashMap<>();
		for (Map.Entry<InvestmentAccount, List<Trade>> investmentAccountListEntry : groupedTradeList.entrySet()) {
			Map<String, List<Trade>> accountGroup = splitAndGroupTrades(investmentAccountListEntry.getValue(), putCallMap);
			for (Map.Entry<String, List<Trade>> stringListEntry : accountGroup.entrySet()) {
				if (!tradeListByGroup.containsKey(stringListEntry.getKey())) {
					tradeListByGroup.put(stringListEntry.getKey(), new ArrayList<>());
				}
				tradeListByGroup.get(stringListEntry.getKey()).addAll(stringListEntry.getValue());
			}
		}

		List<List<Trade>> result = new ArrayList<>();
		for (Map.Entry<String, List<Trade>> stringListEntry : tradeListByGroup.entrySet()) {
			result.add(stringListEntry.getValue());
		}
		return result;
	}


	private Map<String, List<Trade>> splitAndGroupTrades(List<Trade> tradeList, Map<InvestmentSecurity, String> putCallMap) {
		BigDecimal sumPuts = BigDecimal.ZERO;
		BigDecimal sumCalls = BigDecimal.ZERO;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if ("call".equals(putCallMap.get(trade.getInvestmentSecurity()))) {
				sumCalls = MathUtils.add(sumCalls, trade.getQuantityIntended());
			}
			else {
				sumPuts = MathUtils.add(sumPuts, trade.getQuantityIntended());
			}
		}
		tradeList = BeanUtils.sortWithFunction(tradeList, Trade::getQuantityIntended, true);

		String extraOptionType;
		BigDecimal strangleTotal;
		int compareResult = MathUtils.compare(sumCalls, sumPuts);
		if (compareResult < 0) {
			// calls < puts
			extraOptionType = "put";
			strangleTotal = sumCalls;
		}
		else {
			// calls > puts
			extraOptionType = "call";
			strangleTotal = sumPuts;
		}
		return splitTrades(tradeList, strangleTotal, extraOptionType, putCallMap);
	}


	private Map<String, List<Trade>> splitTrades(List<Trade> tradeList, BigDecimal strangleTotal, String extraOptionType, Map<InvestmentSecurity, String> putCallMap) {
		Map<String, List<Trade>> result = new HashMap<>();
		result.put("STRANGLE", new ArrayList<>());
		result.put(extraOptionType, new ArrayList<>());

		BigDecimal sumExtra = BigDecimal.ZERO;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if (extraOptionType.equalsIgnoreCase(putCallMap.get(trade.getInvestmentSecurity()))) {
				if (MathUtils.compare(MathUtils.add(sumExtra, trade.getQuantityIntended()), strangleTotal) <= 0) {
					result.get("STRANGLE").add(trade);
					sumExtra = MathUtils.add(sumExtra, trade.getQuantityIntended());
				}
				else {
					BigDecimal strangleQty = MathUtils.subtract(strangleTotal, sumExtra);
					if (MathUtils.compare(BigDecimal.ZERO, strangleQty) < 0) {
						BigDecimal extraQty = MathUtils.subtract(MathUtils.add(sumExtra, trade.getQuantityIntended()), strangleTotal);
						if (MathUtils.compare(extraQty, BigDecimal.ZERO) > 0) {
							Trade newTrade = BeanUtils.cloneBean(trade, false, false);
							newTrade.setId(null);
							newTrade.setQuantityIntended(extraQty);

							Trade nt = saveTrade(newTrade);
							// for testing, mocked call to saveTrade returns null
							if (nt != null) {
								newTrade = nt;
							}

							result.get(extraOptionType).add(newTrade);
						}
						trade.setQuantityIntended(strangleQty);
						saveTrade(trade);
						result.get("STRANGLE").add(trade);

						sumExtra = strangleQty;
					}
					else {
						result.get(extraOptionType).add(trade);
						sumExtra = MathUtils.add(sumExtra, trade.getQuantityIntended());
					}
				}
			}
			else {
				result.get("STRANGLE").add(trade);
			}
		}
		return result;
	}


	private Trade saveTrade(Trade trade) {
		final Trade t = trade;
		final TradeService service = getTradeService();
		return DaoUtils.executeWithAllObserversDisabled(() -> service.saveTrade(t));
	}


	private Map<InvestmentAccount, List<Trade>> groupByHoldingAccount(List<Trade> tradeList) {
		Map<InvestmentAccount, List<Trade>> result = new HashMap<>();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if (result.containsKey(trade.getHoldingInvestmentAccount())) {
				result.get(trade.getHoldingInvestmentAccount()).add(trade);
			}
			else {
				List<Trade> tl = new ArrayList<>();
				tl.add(trade);
				result.put(trade.getHoldingInvestmentAccount(), tl);
			}
		}
		return result;
	}


	private Map<InvestmentSecurity, String> validateTradeList(List<Trade> tradeList) {
		Map<InvestmentSecurity, String> result = new HashMap<>();
		List<InvestmentSecurity> securityList = new ArrayList<>();
		Boolean isBuy = null;
		Date endDate = null;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			ValidationUtils.assertTrue(trade.isCreateOrder(), "Trade destination [" + trade.getTradeDestination().getName() + "] does not support order creation.");
			if (endDate == null) {
				endDate = trade.getInvestmentSecurity().getEndDate();
			}
			else {
				ValidationUtils.assertTrue(DateUtils.compare(endDate, trade.getInvestmentSecurity().getEndDate(), false) == 0, "Options must expire on the same day. ["
						+ trade.getInvestmentSecurity().getSymbol() + "] expires on [" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), DateUtils.DATE_FORMAT_INPUT) + "] but ["
						+ DateUtils.fromDate(endDate, DateUtils.DATE_FORMAT_INPUT) + "] was expected.");
			}
			if (!securityList.contains(trade.getInvestmentSecurity())) {
				securityList.add(trade.getInvestmentSecurity());
				ValidationUtils.assertTrue(securityList.size() <= 2, "There can only be 2 individual securities (1 put and 1 call) for [" + TradeOrderGroupingTypes.OPTIONS_STRANGLE + "] grouping.");
			}
			if (isBuy == null) {
				isBuy = trade.isBuy();
			}
			else {
				ValidationUtils.assertTrue(isBuy == trade.isBuy(), "All trades must be in the same direction for [" + TradeOrderGroupingTypes.OPTIONS_STRANGLE + "] grouping.");
			}
			ValidationUtils.assertTrue(InvestmentType.OPTIONS.equalsIgnoreCase(trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName()), "["
					+ trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType() + "] is not supported by [" + TradeOrderGroupingTypes.OPTIONS_STRANGLE + "] grouping.");
			String putCall = trade.getInvestmentSecurity().getOptionType().toString();
			if (!result.containsKey(trade.getInvestmentSecurity())) {
				result.put(trade.getInvestmentSecurity(), putCall.toLowerCase());
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeOrderWorkflowService getTradeOrderWorkflowService() {
		return this.tradeOrderWorkflowService;
	}


	public void setTradeOrderWorkflowService(TradeOrderWorkflowService tradeOrderWorkflowService) {
		this.tradeOrderWorkflowService = tradeOrderWorkflowService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
