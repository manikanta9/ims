package com.clifton.trade.order;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>TradeOrderStatus</code> defines the status of a TradeOrder.
 *
 * @author mwacker
 */
public class TradeOrderStatus extends NamedEntity<Short> {

	private TradeOrderStatuses orderStatus;

	/**
	 * If no fills are received and the order is cancelled, then cancel the underlying trades.
	 */
	private boolean cancelIfNotFilled;
	/**
	 * The order has been partially filled.
	 */
	private boolean partialFill;
	/**
	 * The order fill is complete, either FILLED or PARTIALLY_FILLED_CANCELED
	 */
	private boolean fillComplete;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean hasFills() {
		return isFillComplete() || isPartialFill();
	}


	@Override
	public String toString() {
		return getOrderStatus().toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderStatuses getOrderStatus() {
		return this.orderStatus;
	}


	public void setOrderStatus(TradeOrderStatuses orderStatus) {
		this.orderStatus = orderStatus;
	}


	public boolean isFillComplete() {
		return this.fillComplete;
	}


	public void setFillComplete(boolean fillComplete) {
		this.fillComplete = fillComplete;
	}


	public boolean isCancelIfNotFilled() {
		return this.cancelIfNotFilled;
	}


	public void setCancelIfNotFilled(boolean cancelIfNotFilled) {
		this.cancelIfNotFilled = cancelIfNotFilled;
	}


	public boolean isPartialFill() {
		return this.partialFill;
	}


	public void setPartialFill(boolean partialFill) {
		this.partialFill = partialFill;
	}
}
