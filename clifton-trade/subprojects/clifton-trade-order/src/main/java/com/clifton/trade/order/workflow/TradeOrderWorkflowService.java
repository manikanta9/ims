package com.clifton.trade.order.workflow;


import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.workflow.definition.WorkflowState;


/**
 * The <code>TradeOrderWorkflowService</code> defines a helper service used to transition trades
 * through work flow states in the TradeOrder package.
 *
 * @author mwacker
 */
public interface TradeOrderWorkflowService {

	public Trade setTradeWorkflowState(Trade trade, String stateName);


	public void setTradeWorkflowStates(TradeOrder order, String stateName);


	public WorkflowState getNextTradeWorkflowState(Trade trade, String stateName);
}
