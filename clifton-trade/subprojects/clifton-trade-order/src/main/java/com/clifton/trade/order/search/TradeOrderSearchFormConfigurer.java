package com.clifton.trade.order.search;


import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderSecurity;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.allocation.TradeOrderTradeOrderAllocation;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowStatus;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;

import java.util.ArrayList;
import java.util.List;


public class TradeOrderSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final TradeOrderAllocationService tradeOrderAllocationService;
	private final WorkflowDefinitionService workflowDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderSearchFormConfigurer(BaseEntitySearchForm searchForm, TradeOrderAllocationService tradeOrderAllocationService, WorkflowDefinitionService workflowDefinitionService) {
		super(searchForm);
		this.tradeOrderAllocationService = tradeOrderAllocationService;
		this.workflowDefinitionService = workflowDefinitionService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		TradeOrderSearchForm searchForm = (TradeOrderSearchForm) getSortableSearchForm();
		configureSecurity(criteria, searchForm);
		configureTradeAndWorkflow(criteria, searchForm);
		configureAllocation(criteria, searchForm);
		configureAllocationStatusList(criteria, searchForm);
		configureClientAccountGroup(criteria, searchForm);
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(getSortableSearchForm().getOrderBy());
		if (CollectionUtils.isEmpty(orderByList)) {
			return false;
		}
		for (OrderByField field : orderByList) {
			String name = getOrderByFieldName(field, criteria);
			if ("investmentSecurityId".equals(name)) {
				name = "investmentSecurity.id";
			}
			criteria.addOrder(OrderByDirections.ASC == field.getDirection() ? Order.asc(name) : Order.desc(name));
		}
		return true;
	}


	private void configureSecurity(Criteria criteria, TradeOrderSearchForm searchForm) {
		// Need to look at the investment security of both the one on TradeOrder and TradeOrderSecurity, so
		// track the AND clauses for each table to be added with an OR clause later.
		List<Criterion> orderAndCriterionList = new ArrayList<>();
		List<Criterion> orderSecurityAndCriterionList = new ArrayList<>();
		if (!StringUtils.isEmpty(searchForm.getSymbol()) || searchForm.getInvestmentSecurityId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(TradeOrderSecurity.class, "orderSecurity");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("order.id", criteria.getAlias() + ".id"));
			if (!StringUtils.isEmpty(searchForm.getSymbol())) {
				sub.createAlias("investmentSecurity", "security");
				sub.add(Restrictions.eq("security.symbol", searchForm.getSymbol()));
				orderAndCriterionList.add(Restrictions.eq(getPathAlias("investmentSecurity", criteria, JoinType.LEFT_OUTER_JOIN) + ".symbol", searchForm.getSymbol()));
				if (searchForm.getInvestmentSecurityId() == null) {
					// Only add the exists predicate if investment security ID is not defined to avoid adding the same one twice
					orderSecurityAndCriterionList.add(Subqueries.exists(sub));
				}
			}
			if (searchForm.getInvestmentSecurityId() != null) {
				sub.add(Restrictions.eq("investmentSecurity.id", searchForm.getInvestmentSecurityId()));
				orderAndCriterionList.add(Restrictions.eq("investmentSecurity.id", searchForm.getInvestmentSecurityId()));
				orderSecurityAndCriterionList.add(Subqueries.exists(sub));
			}
		}
		if (searchForm.getInvestmentGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentSecurity.class, "security");
			sub.setProjection(Projections.id());
			sub.createAlias("security.instrument", "instrument");
			sub.createAlias("instrument.groupItemList", "groupItemList");
			sub.createAlias("groupItemList.referenceOne", "investmentGroupItem");
			sub.createAlias("investmentGroupItem.group", "group");
			sub.add((Restrictions.eq("group.id", searchForm.getInvestmentGroupId())));
			orderAndCriterionList.add(Subqueries.propertyIn("investmentSecurity.id", sub));
			orderSecurityAndCriterionList.add(Subqueries.propertyIn(getPathAlias("securityList.investmentSecurity", criteria, JoinType.LEFT_OUTER_JOIN) + ".id", sub));
		}

		if (!orderAndCriterionList.isEmpty()) {
			orderAndCriterionList.add(0, Restrictions.isNotNull("investmentSecurity.id"));
			orderSecurityAndCriterionList.add(0, Restrictions.isNull("investmentSecurity.id"));
			criteria.add(Restrictions.or(
					Restrictions.and(orderAndCriterionList.toArray(new Criterion[0])),
					Restrictions.and(orderSecurityAndCriterionList.toArray(new Criterion[0]))
			));
		}
	}


	private void configureTradeAndWorkflow(Criteria criteria, TradeOrderSearchForm searchForm) {
		if ((searchForm.getTradeId() != null) || (searchForm.getTraderId() != null) || (searchForm.getTradeWorkflowStateName() != null) || (searchForm.getTradeWorkflowStatusName() != null)
				|| (searchForm.getTeamSecurityGroupId() != null)) {
			DetachedCriteria sub = DetachedCriteria.forClass(Trade.class, "trade");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("orderIdentifier", criteria.getAlias() + ".id"));
			if (searchForm.getTradeId() != null) {
				sub.add(Restrictions.eq("trade.id", searchForm.getTradeId()));
			}
			if (searchForm.getTradeWorkflowStateName() != null) {
				sub.createAlias("workflowState", "workflowState");
				sub.add(Restrictions.eq("workflowState.name", searchForm.getTradeWorkflowStateName()));
			}
			if (searchForm.getTradeWorkflowStatusName() != null) {
				WorkflowStatus status = this.workflowDefinitionService.getWorkflowStatusByName(searchForm.getTradeWorkflowStatusName());
				sub.add(Restrictions.eq("workflowStatus.id", status.getId()));
			}
			if (searchForm.getTeamSecurityGroupId() != null) {
				sub.createAlias("clientInvestmentAccount", "clientInvestmentAccount");
				sub.add(Restrictions.eq("clientInvestmentAccount.teamSecurityGroup.id", searchForm.getTeamSecurityGroupId()));
			}
			criteria.add(Subqueries.exists(sub));
		}
	}


	private void configureAllocationStatusList(Criteria criteria, TradeOrderSearchForm searchForm) {
		if (searchForm.getAllocationStatusList() != null) {
			List<Short> allocationStatusIdList = new ArrayList<>();
			for (int i = 0; i < searchForm.getAllocationStatusList().length; i++) {
				TradeOrderAllocationStatuses status = searchForm.getAllocationStatusList()[i];
				List<TradeOrderAllocationStatus> allocationStatusList = this.tradeOrderAllocationService.getTradeOrderAllocationStatusListByStatuses(status);
				for (TradeOrderAllocationStatus allocationStatus : CollectionUtils.getIterable(allocationStatusList)) {
					if (!allocationStatusIdList.contains(allocationStatus.getId())) {
						allocationStatusIdList.add(allocationStatus.getId());
					}
				}
			}
			criteria.add(Restrictions.in("allocationStatus.id", allocationStatusIdList.toArray()));
		}
	}


	private void configureAllocation(Criteria criteria, TradeOrderSearchForm searchForm) {
		if (searchForm.getAllocationIdForAllOrders() != null) {
			List<TradeOrderTradeOrderAllocation> tradeOrderTradeOrderAllocationList = this.tradeOrderAllocationService.getTradeOrderTradeOrderAllocationList(searchForm.getAllocationIdForAllOrders());

			ValidationUtils.assertNotEmpty(tradeOrderTradeOrderAllocationList, String.format("No TradeOrders found for allocation id: %d", searchForm.getAllocationIdForAllOrders()));

			// Use an optimized query for most cases where the list size is 1.
			if (tradeOrderTradeOrderAllocationList.size() == 1) {
				TradeOrderTradeOrderAllocation tradeOrderTradeOrderAllocation = tradeOrderTradeOrderAllocationList.get(0);
				TradeOrder tradeOrder = tradeOrderTradeOrderAllocation.getReferenceOne();

				if (tradeOrder.getOrderGroup() == null) {
					criteria.add(Restrictions.eq("id", tradeOrder.getId()));
				}
				else {
					criteria.add(Restrictions.eq("orderGroup", tradeOrder.getOrderGroup()));
				}
			}
			else {
				DetachedCriteria sub = DetachedCriteria.forClass(TradeOrderTradeOrderAllocation.class, "link");
				sub.setProjection(Projections.property("id"));
				// add these to search sub properties and change referenceOne, referenceTwo  -- not adding right now to avoid extra joins
				sub.createAlias("referenceOne", "order");
				//sub.createAlias("referenceTwo", "allocation");
				sub.add(Restrictions.or(Restrictions.eqProperty("order.orderGroup", criteria.getAlias() + ".orderGroup"), Restrictions.eqProperty("order.id", criteria.getAlias() + ".id")));
				sub.add(Restrictions.eq("referenceTwo.id", searchForm.getAllocationIdForAllOrders()));
				criteria.add(Subqueries.exists(sub));
			}
		}
		else if (searchForm.getAllocatedOrderId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(TradeOrderTradeOrderAllocation.class, "link");
			sub.setProjection(Projections.property("id"));
			// add these to search sub properties and change referenceOne, referenceTwo -- not adding right now to avoid extra joins
			//sub.createAlias("referenceOne", "order");
			//sub.createAlias("referenceTwo", "allocation");
			sub.add(Restrictions.eqProperty("referenceOne.id", criteria.getAlias() + ".id"));

			DetachedCriteria sub2 = DetachedCriteria.forClass(TradeOrderTradeOrderAllocation.class, "link2");
			sub2.setProjection(Projections.property("id"));
			sub2.add(Restrictions.eq("referenceOne.id", searchForm.getAllocatedOrderId()));
			sub.add(Subqueries.propertyEq("referenceTwo.id", sub2));

			criteria.add(Subqueries.exists(sub));
		}
		if (searchForm.getAllocationId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(TradeOrderTradeOrderAllocation.class, "link");
			sub.setProjection(Projections.property("id"));
			// add these to search sub properties and change referenceOne, referenceTwo  -- not adding right now to avoid extra joins
			//sub.createAlias("referenceOne", "order");
			//sub.createAlias("referenceTwo", "allocation");
			sub.add(Restrictions.eqProperty("referenceOne.id", criteria.getAlias() + ".id"));
			sub.add(Restrictions.eq("referenceTwo.id", searchForm.getAllocationId()));
			criteria.add(Subqueries.exists(sub));
		}
	}


	private void configureClientAccountGroup(Criteria criteria, TradeOrderSearchForm searchForm) {
		if (searchForm.getClientInvestmentAccountGroupId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "iaga");
			sub.setProjection(Projections.property("id"));
			sub.createAlias("referenceOne", "group");
			sub.createAlias("referenceTwo", "acct");
			sub.add(Restrictions.eq("group.id", searchForm.getClientInvestmentAccountGroupId()));
			sub.add(Restrictions.eqProperty("acct.id", getPathAlias("tradeList.clientInvestmentAccount", criteria) + ".id"));
			criteria.add(Subqueries.exists(sub));
		}
	}
}
