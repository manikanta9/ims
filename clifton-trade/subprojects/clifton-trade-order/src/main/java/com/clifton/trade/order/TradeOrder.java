package com.clifton.trade.order;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>TradeOrder</code> is a grouping of one or multiple trades used for electronic or manual
 * execution with external parties/systems.
 * <p>
 * TradeOrder is usually for same security: multiple orders in same direction.  However, it also supports
 * orders for multiple securities: options strangle, futures roll, etc.
 *
 * @author mwacker
 */
public class TradeOrder extends BaseEntity<Integer> {

	public static final String TRADE_ORDER_TABLE_NAME = "TradeOrder";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrder originalTradeOrder;

	private TradeOrderStatus status;
	private TradeOrderAllocationStatus allocationStatus;

	private boolean validationError = false;
	private boolean allocationValidationError = false;

	/**
	 * Identifies a group of orders for cancel/replace logic.  OrderGroup is always the id
	 * of the first order in the chain.  This will always be NULL, unless the order has been
	 * cancelled and replaced.
	 */
	private Integer orderGroup;
	/**
	 * Identifies the order in a group this is active.
	 */
	private boolean activeOrder = true;
	/**
	 * The unique id provided by an external application.  The typical instance
	 * is the FixIdentifierID which reference the FixIdentifier in the CliftonFIX database.
	 */
	private Integer externalIdentifier;

	/**
	 * Most orders are for a single security (investmentSecurity is set and securityList is null).
	 * However, some atomic orders are for multiple securities: options strangle, futures roll, etc.
	 * In this case, investmentSecurity is null and securityList contains multiple securities that go into this order.
	 */
	private InvestmentSecurity investmentSecurity;
	/**
	 * The original investment security used to create the order.
	 * <p>
	 * For example, when the order is a forward, the investmentSecurity property will have the given currency for the
	 * forward trade and the actual forward security will be in originalInvestmentSecurity.
	 */
	private InvestmentSecurity originalInvestmentSecurity;

	private List<TradeOrderSecurity> securityList;

	private BigDecimal quantity;
	private BigDecimal quantityFilled;
	private BigDecimal averagePrice;
	private BigDecimal limitPrice;

	/**
	 * ReferenceSecurityPrice may be used in different contexts to store the price of a related security for processing purposes.
	 * In the case of BTIC Orders, the price represents the price of the Future's underlying security at market close.
	 */
	private BigDecimal referenceSecurityPrice;

	/**
	 * Non-persisted field used by the user interface to update the commission on trades for manual single security orders.
	 */
	@NonPersistentField
	private BigDecimal commissionPerUnit;

	private boolean buy;
	private SecurityUser traderUser;
	private TradeDestinationMapping destinationMapping;

	/**
	 * Trade date - added here to avoid having to using an exists clause on the trade table to filter orders by tradeDate.
	 */
	private Date tradeDate;

	private List<Trade> tradeList;

	/**
	 * The grouping type used to generate the order.
	 */
	private TradeOrderType type;

	private BigDecimal indexRatio;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabelShort() {
		StringBuilder sb = new StringBuilder(64);
		sb.append(isBuy() ? "BUY " : "SELL ");
		sb.append(CoreMathUtils.formatNumberDecimal(getQuantity()));
		if (getInvestmentSecurity() != null) {
			sb.append(" of ");
			sb.append(getInvestmentSecurity().getSymbol());
		}
		if (getTradeDate() != null) {
			sb.append(" on ");
			sb.append(DateUtils.fromDateShort(getTradeDate()));
		}
		return sb.toString();
	}


	/**
	 * True if the order allocation message has been sent, false otherwise.
	 */
	public boolean isAllocationSent() {
		return getAllocationStatus() != null && getAllocationStatus().getAllocationStatus() != TradeOrderAllocationStatuses.FAILED_TO_SEND;
	}


	/**
	 * Returns true if this TradeOrder is for multiple securities.
	 * <p>
	 * Most orders are for a single security (investmentSecurity is set and securityList is null).
	 * However, some atomic orders are for multiple securities: options strangle, future roll, etc.
	 * In this case, investmentSecurity = null and securityList will contain multiple securities that go into this order.
	 */
	public boolean isMultipleSecurityOrder() {
		return getInvestmentSecurity() == null && getType() != null && !getType().isInvestmentSecurityAllowedToBeNull();
	}


	public TradeType getTradeType() {
		Trade firstTrade = CollectionUtils.getFirstElement(getTradeList());
		if (firstTrade != null) {
			return firstTrade.getTradeType();
		}
		return null;
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderStatus getStatus() {
		return this.status;
	}


	public void setStatus(TradeOrderStatus status) {
		this.status = status;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getQuantityFilled() {
		return this.quantityFilled;
	}


	public void setQuantityFilled(BigDecimal quantityFilled) {
		this.quantityFilled = quantityFilled;
	}


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public SecurityUser getTraderUser() {
		return this.traderUser;
	}


	public void setTraderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
	}


	public TradeDestinationMapping getDestinationMapping() {
		return this.destinationMapping;
	}


	public void setDestinationMapping(TradeDestinationMapping destinationMapping) {
		this.destinationMapping = destinationMapping;
	}


	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}


	public BigDecimal getLimitPrice() {
		return this.limitPrice;
	}


	public void setLimitPrice(BigDecimal limitPrice) {
		this.limitPrice = limitPrice;
	}


	public BigDecimal getReferenceSecurityPrice() {
		return this.referenceSecurityPrice;
	}


	public void setReferenceSecurityPrice(BigDecimal referenceSecurityPrice) {
		this.referenceSecurityPrice = referenceSecurityPrice;
	}


	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}


	public List<Trade> getTradeList() {
		return this.tradeList;
	}


	public void setTradeList(List<Trade> tradeList) {
		this.tradeList = tradeList;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Integer getExternalIdentifier() {
		return this.externalIdentifier;
	}


	public void setExternalIdentifier(Integer externalIdentifier) {
		this.externalIdentifier = externalIdentifier;
	}


	public List<TradeOrderSecurity> getSecurityList() {
		return this.securityList;
	}


	public void setSecurityList(List<TradeOrderSecurity> tradeOrderSecurityList) {
		this.securityList = tradeOrderSecurityList;
	}


	public TradeOrderType getType() {
		return this.type;
	}


	public void setType(TradeOrderType type) {
		this.type = type;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public InvestmentSecurity getOriginalInvestmentSecurity() {
		return this.originalInvestmentSecurity;
	}


	public void setOriginalInvestmentSecurity(InvestmentSecurity originalInvestmentSecurity) {
		this.originalInvestmentSecurity = originalInvestmentSecurity;
	}


	public TradeOrder getOriginalTradeOrder() {
		return this.originalTradeOrder;
	}


	public void setOriginalTradeOrder(TradeOrder originalTradeOrder) {
		this.originalTradeOrder = originalTradeOrder;
	}


	public TradeOrderAllocationStatus getAllocationStatus() {
		return this.allocationStatus;
	}


	public void setAllocationStatus(TradeOrderAllocationStatus allocationStatus) {
		this.allocationStatus = allocationStatus;
	}


	public boolean isValidationError() {
		return this.validationError;
	}


	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}


	public boolean isAllocationValidationError() {
		return this.allocationValidationError;
	}


	public void setAllocationValidationError(boolean allocationValidationError) {
		this.allocationValidationError = allocationValidationError;
	}


	public Integer getOrderGroup() {
		return this.orderGroup;
	}


	public void setOrderGroup(Integer orderGroup) {
		this.orderGroup = orderGroup;
	}


	public boolean isActiveOrder() {
		return this.activeOrder;
	}


	public void setActiveOrder(boolean activeOrder) {
		this.activeOrder = activeOrder;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}
}
