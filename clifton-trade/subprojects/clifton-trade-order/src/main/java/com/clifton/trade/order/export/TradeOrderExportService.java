package com.clifton.trade.order.export;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.order.TradeOrder;
import org.springframework.web.bind.annotation.ModelAttribute;


/**
 * The <code>TradeOrderExportService</code> defines custom order downloads/exports.
 *
 * @author mwacker
 */
public interface TradeOrderExportService {

	@ModelAttribute("data")
	@SecureMethod(dtoClass = TradeOrder.class)
	public FileWrapper downloadTradeOrderExport(String downloadName, Integer[] tradeOrderIds);
}
