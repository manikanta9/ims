package com.clifton.trade.order.allocation;


import com.clifton.core.beans.ManyToManySimpleEntity;
import com.clifton.trade.order.TradeOrder;


/**
 * The <code>TradeOrderTradeOrderAllocation</code> links TradeOrder's to TradeOrderAllocation's
 *
 * @author mwacker
 */
public class TradeOrderTradeOrderAllocation extends ManyToManySimpleEntity<TradeOrder, TradeOrderAllocation, Integer> {

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !o.getClass().equals(getClass())) {
			return false;
		}
		TradeOrderTradeOrderAllocation entity = (TradeOrderTradeOrderAllocation) o;
		return getReferenceOne().equals(entity.getReferenceOne()) && getReferenceTwo().equals(entity.getReferenceTwo());
	}
}
