package com.clifton.trade.order.allocation.processor;


import com.clifton.trade.order.processor.TradeOrderProcessorParameters;


/**
 * The <code>TradeOrderProcessorAllocation</code> defines the method(s) used to process that allocation when all
 * fills are available.
 *
 * @author mwacker
 */
public interface TradeOrderAllocationProcessor {

	public void processAllocationFills(TradeOrderProcessorParameters orderParameters);
}
