package com.clifton.trade.order.event;

import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.priority.SystemPriority;
import com.clifton.system.priority.SystemPriorityService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.websocket.WebSocketHandler;
import com.clifton.websocket.alert.WebsocketAlertMessage;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * <code>TradeOrderStatusEventListener</code> is an event listener for {@link TradeOrderStatusEvent}s,
 * which use the event name {@link TradeOrderStatusEvent#getEventName()}.
 */
@Component
public class TradeOrderStatusEventListener extends BaseEventListener<TradeOrderStatusEvent> {

	private SecurityUserService securityUserService;

	private SystemPriorityService systemPriorityService;
	private SystemSchemaService systemSchemaService;

	private WebSocketHandler webSocketHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(TradeOrderStatusEvent event) {
		Optional.of(event)
				.map(TradeOrderStatusEvent::getTarget)
				.ifPresent(this::createTradeNotifications);
	}


	@Override
	public String getEventName() {
		return TradeOrderCancellationEvent.EVENT_NAME;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void createTradeNotifications(final TradeOrder tradeOrder) {
		// get unique list of recipients from the trade order's -> trade -> client account -> servicing team
		List<SecurityUser> recipientList = tradeOrder.getTradeList().stream()
				.map(Trade::getClientInvestmentAccount)
				.filter(Objects::nonNull)
				.map(InvestmentAccount::getTeamSecurityGroup)
				.map(SecurityGroup::getId)
				.flatMap(grp -> getSecurityUserService().getSecurityUserListByGroup(grp).stream())
				.distinct()
				.collect(Collectors.toList());
		WebsocketAlertMessage notificationMessage = new WebsocketAlertMessage(
				"Trade Order Status Change",
				createSubjectLine(tradeOrder),
				getSystemPriorityService().getSystemPriorityByName(SystemPriority.SYSTEM_PRIORITY_IMMEDIATE),
				getSystemSchemaService().getSystemTableByName(TradeOrder.TRADE_ORDER_TABLE_NAME),
				tradeOrder.getId()
		);
		getWebSocketHandler().sendMessageToUserList(recipientList, WebsocketAlertMessage.CHANNEL_USER_TOPIC_IMMEDIATE_SYSTEM_TOAST_MESSAGES, notificationMessage);
	}


	/**
	 * Example:  Antony's Trade has been Canceled: Sell 3 TUM8 for Account #423000
	 */
	private String createSubjectLine(final TradeOrder tradeOrder) {
		StringBuilder builder = new StringBuilder();
		builder.append(tradeOrder.getTraderUser().getDisplayName());
		builder.append("'s");
		builder.append(" Trade Order has been ");
		builder.append(tradeOrder.getStatus().getLabel());
		builder.append(":  ");
		builder.append(tradeOrder.isBuy() ? "BUY" : "SELL");
		builder.append(" ");
		builder.append(CoreMathUtils.formatNumber(tradeOrder.getQuantity(), null));
		builder.append(" ");
		builder.append(tradeOrder.getInvestmentSecurity().getSymbol());
		builder.append(" (Order ID: ");
		builder.append(tradeOrder.getId());
		builder.append(")");
		return builder.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemPriorityService getSystemPriorityService() {
		return this.systemPriorityService;
	}


	public void setSystemPriorityService(SystemPriorityService systemPriorityService) {
		this.systemPriorityService = systemPriorityService;
	}
}
