package com.clifton.trade.order.util;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author mwacker
 */
public class TradeOrderUtils {

	public static BigDecimal getTotalTradeQuantity(List<Trade> tradeList) {
		BigDecimal total = BigDecimal.ZERO;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			total = MathUtils.add(total, TradeOrderUtils.getTradeQuantity(trade));
		}
		return total;
	}


	public static BigDecimal getTradeQuantity(Trade trade) {
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			if (!trade.getSettlementCurrency().equals(trade.getInvestmentSecurity().getInstrument().getTradingCurrency())) {
				return trade.getAccountingNotional();
			}
			return trade.getQuantityIntended();
		}
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.CURRENCY)) {
			return trade.getAccountingNotional();
		}
		return InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.BONDS)
				|| InvestmentUtils.isSecurityOfTypeSubType(trade.getInvestmentSecurity(), InvestmentTypeSubType.CREDIT_DEFAULT_SWAPS) ? trade.getOriginalFace() : trade.getQuantityIntended();
	}


	public static boolean isBticOrder(TradeOrder tradeOrder) {
		if (tradeOrder != null) {
			Trade firstTrade = CollectionUtils.getFirstElement(tradeOrder.getTradeList());
			return firstTrade != null && firstTrade.getTradeExecutionType() != null && firstTrade.getTradeExecutionType().isBticTrade();
		}
		return false;
	}


	public static boolean isAllocationConnectionUsed(TradeOrder tradeOrder) {
		if (tradeOrder != null) {
			return tradeOrder.getDestinationMapping().getTradeDestination().getConnection().getAllocationConnection() != null;
		}
		return false;
	}
}
