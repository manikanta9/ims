package com.clifton.trade.order.processor;


/**
 * The <code>TradeOrderProcessorActonTypes</code> list the action types for the TradeOrderProcessor.
 *
 * @author mwacker
 */
public enum TradeOrderProcessorActonTypes {
	/**
	 * Handle a single order.
	 */
	PROCESS_ORDER,
	/**
	 * Handle a list of orders as individual order.
	 * <p/>
	 * For example, if the orders are ready to allocate, allocate them individually.
	 */
	PROCESS_ORDER_MULTIPLE,
	/**
	 * Handle a list of orders as individual order but allocate the orders together.
	 * <p/>
	 * For example, if the orders are ready to allocate, allocate together.
	 */
	PROCESS_ORDER_MULTIPLE_BLOCK_ALLOCATE,
	/**
	 * Handle the allocation.
	 */
	PROCESS_ORDER_ALLOCATION
}
