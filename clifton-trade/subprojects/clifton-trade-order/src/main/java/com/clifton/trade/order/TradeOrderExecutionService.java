package com.clifton.trade.order;


import java.util.List;
import java.util.Map;


/**
 * The <code>TradeOrderExecutionService</code> defines methods used to execute a list of trade orders.
 * <p/>
 * NOTE: This is currently only implemented by the TradeOrderFixExecutionService class, but if there are more
 * execution methods in the future then we can register a map in the TradeOrderService and lookup the correct
 * TradeOrderExecutionService implementation.
 *
 * @author mwacker
 */
public interface TradeOrderExecutionService<T> {


	public void executeTradeOrderList(List<TradeOrder> orderList);


	/**
	 * Creates a converted order object for each TradeOrder.
	 */
	public Map<TradeOrder, T> createTradeOrderToTargetObjectMap(List<TradeOrder> orderList);


	public void executeTradeOrderTargetObjectMap(Map<TradeOrder, T> fixTradeOrderMap);
}
