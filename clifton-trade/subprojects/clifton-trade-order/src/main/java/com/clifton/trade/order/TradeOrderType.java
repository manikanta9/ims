package com.clifton.trade.order;


import com.clifton.core.beans.NamedEntity;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;


/**
 * The <code>TradeOrderGroupingType</code> represents an trade grouping strategy used to create orders.
 *
 * @author mwacker
 */
public class TradeOrderType extends NamedEntity<Short> {

	/**
	 * The grouping type for the order.
	 */
	private TradeOrderGroupingTypes groupingType;

	private boolean investmentSecurityAllowedToBeNull;

	/**
	 * This flag, indicates whether or not this TradeOrderType represents a Limit Order.  If this flag's value
	 * is true, then the TradeOrderType represents a limit order.  If not true, it represents a market order.
	 */
	private boolean limitOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderGroupingTypes getGroupingType() {
		return this.groupingType;
	}


	public void setGroupingType(TradeOrderGroupingTypes groupingType) {
		this.groupingType = groupingType;
	}


	public boolean isInvestmentSecurityAllowedToBeNull() {
		return this.investmentSecurityAllowedToBeNull;
	}


	public void setInvestmentSecurityAllowedToBeNull(boolean investmentSecurityAllowedToBeNull) {
		this.investmentSecurityAllowedToBeNull = investmentSecurityAllowedToBeNull;
	}


	public boolean isLimitOrder() {
		return this.limitOrder;
	}


	public void setLimitOrder(boolean limitOrder) {
		this.limitOrder = limitOrder;
	}
}
