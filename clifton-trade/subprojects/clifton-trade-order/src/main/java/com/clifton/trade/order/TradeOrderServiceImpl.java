package com.clifton.trade.order;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationStatusSearchForm;
import com.clifton.trade.order.grouping.TradeOrderGroupingCommand;
import com.clifton.trade.order.grouping.TradeOrderGroupingService;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;
import com.clifton.trade.order.search.TradeOrderSearchForm;
import com.clifton.trade.order.search.TradeOrderSearchFormConfigurer;
import com.clifton.trade.order.search.TradeOrderStatusSearchForm;
import com.clifton.trade.order.util.TradeOrderUtils;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class TradeOrderServiceImpl implements TradeOrderService {

	private AdvancedUpdatableDAO<TradeOrder, Criteria> tradeOrderDAO;
	private AdvancedUpdatableDAO<TradeOrderSecurity, Criteria> tradeOrderSecurityDAO;
	private AdvancedUpdatableDAO<TradeOrderStatus, Criteria> tradeOrderStatusDAO;
	private AdvancedUpdatableDAO<TradeOrderType, Criteria> tradeOrderTypeDAO;

	private TradeService tradeService;
	private TradeCommissionService tradeCommissionService;
	private TradeGroupService tradeGroupService;
	private TradeDestinationService tradeDestinationService;
	private TradeOrderWorkflowService tradeOrderWorkflowService;
	@SuppressWarnings("rawtypes")
	private TradeOrderExecutionService tradeOrderExecutionService;
	private TradeOrderGroupingService tradeOrderGroupingService;
	private TradeOrderAllocationService tradeOrderAllocationService;
	private WorkflowDefinitionService workflowDefinitionService;
	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	//////                  Trade Order Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public TradeOrder getTradeOrder(int id) {
		TradeOrder result = getTradeOrderDAO().findByPrimaryKey(id);
		populateTradeOrder(result);
		return result;
	}


	@Override
	@Transactional(readOnly = true)
	public TradeOrder getTradeOrderByTrade(int tradeId) {
		TradeOrderSearchForm searchForm = new TradeOrderSearchForm();
		searchForm.setTradeId(tradeId);
		TradeOrder result = CollectionUtils.getOnlyElement(getTradeOrderList(searchForm));
		populateTradeOrder(result);
		return result;
	}


	@Override
	@Transactional(readOnly = true)
	public TradeOrder getTradeOrderByExternalIdentifier(Integer externalIdentifier) {
		TradeOrder result = getTradeOrderDAO().findOneByField("externalIdentifier", externalIdentifier);
		populateTradeOrder(result);
		return result;
	}


	private void populateTradeOrder(TradeOrder order) {
		if (order != null) {
			if (CollectionUtils.isEmpty(order.getTradeList())) {
				TradeSearchForm sf = new TradeSearchForm();
				sf.setOrderIdentifier(order.getId());
				order.setTradeList(getTradeService().getTradeList(sf));
			}

			if (order.isMultipleSecurityOrder()) {
				order.setSecurityList(getTradeOrderSecurityListByOrder(order.getId()));
			}
		}
	}


	@Override
	public List<TradeOrder> getTradeOrderListByAllocationId(int orderAllocationId) {
		TradeOrderSearchForm searchForm = new TradeOrderSearchForm();
		searchForm.setAllocationIdForAllOrders(orderAllocationId);
		searchForm.setActiveOrder(true);
		return getTradeOrderListPopulated(searchForm);
	}


	@Override
	@Transactional(readOnly = true)
	public List<TradeOrder> getTradeOrderListPopulated(TradeOrderSearchForm searchForm) {
		List<TradeOrder> orderList = getTradeOrderList(searchForm);
		for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
			populateTradeOrder(order);
		}
		return orderList;
	}


	@Override
	public List<TradeOrder> getTradeOrderList(final TradeOrderSearchForm searchForm) {
		return getTradeOrderDAO().findBySearchCriteria(new TradeOrderSearchFormConfigurer(searchForm, getTradeOrderAllocationService(), getWorkflowDefinitionService()));
	}


	@Transactional
	@Override
	public TradeOrder saveTradeOrder(TradeOrder bean) {
		return doSaveTradeOrder(bean, false);
	}


	@Transactional
	@Override
	public TradeOrder updateTradeOrderLimitPrice(Integer tradeOrderId, BigDecimal limitPrice) {
		TradeOrder tradeOrder = getTradeOrder(tradeOrderId);
		tradeOrder.setLimitPrice(limitPrice);
		saveTradeOrder(tradeOrder);
		return tradeOrder;
	}


	@Transactional
	protected TradeOrder doSaveTradeOrder(TradeOrder bean, boolean cancelReplace) {
		// Validate that the current order is the only active order for the group
		if (bean.isActiveOrder() && bean.getOrderGroup() != null) {
			TradeOrderSearchForm sf = new TradeOrderSearchForm();
			sf.setOrderGroup(bean.getOrderGroup());
			sf.setActiveOrder(true);
			sf.setExcludeTradeOrderId(bean.getId());
			List<TradeOrder> tradeOrderList = getTradeOrderList(sf);
			ValidationUtils.assertTrue(CollectionUtils.isEmpty(tradeOrderList), "An active order already exists for order group [" + bean.getOrderGroup() + "].");
		}
		if (bean.isMultipleSecurityOrder()) {
			ValidationUtils.assertTrue(!CollectionUtils.isEmpty(bean.getSecurityList()),
					"Cannot save an order without a security.  [securityList] or [investmentSecurity] must be populated.");
		}
		else if (!bean.getType().isInvestmentSecurityAllowedToBeNull()) {
			ValidationUtils.assertTrue(CollectionUtils.isEmpty(bean.getSecurityList()), "Cannot save multiple securities for a single security order.");
		}

		if (!bean.getType().isInvestmentSecurityAllowedToBeNull()) {
			ValidationUtils.assertNotNull(bean.getDestinationMapping(), "Required when an investment security is present.", "destinationMapping");
		}


		boolean saveTrades = isTradeSaveRequired(bean);

		TradeOrder result = getTradeOrderDAO().save(bean);

		BigDecimal commissionPerUnit = bean.getCommissionPerUnit();
		if (saveTrades && !cancelReplace) {
			List<Trade> newTradeList = new ArrayList<>();
			for (Trade trade : CollectionUtils.getIterable(bean.getTradeList())) {
				trade.setOrderIdentifier(result.getId());
				trade = getTradeService().saveTrade(trade);
				newTradeList.add(trade);

				//If the commission per unit is not null, then create or update the overrides for each trade
				if (commissionPerUnit != null) {
					AccountingAccount commissionAccount = trade.getTradeType().getCommissionAccountingAccount();
					getTradeCommissionService().createOrUpdateTradeCommissionOverride(trade, commissionAccount, null, commissionPerUnit);
				}
			}
			result.setTradeList(newTradeList);
		}
		else {
			populateTradeOrder(result);
		}
		if (result.isMultipleSecurityOrder()) {
			List<TradeOrderSecurity> resultSecurityList = new ArrayList<>();
			for (TradeOrderSecurity os : CollectionUtils.getIterable(bean.getSecurityList())) {
				os.setOrder(result);
				resultSecurityList.add(getTradeOrderSecurityDAO().save(os));
			}
			result.setSecurityList(resultSecurityList);
		}
		return result;
	}


	private List<TradeOrder> saveTradeOrderList(List<TradeOrder> tradeOrderList) {
		if (!CollectionUtils.isEmpty(tradeOrderList)) {
			for (TradeOrder tradeOrder : tradeOrderList) {
				doSaveTradeOrder(tradeOrder, false);
			}
		}
		return tradeOrderList;
	}


	private boolean isTradeSaveRequired(TradeOrder order) {
		BigDecimal commissionPerUnit = order.getCommissionPerUnit();
		return order.isNewBean() || (commissionPerUnit != null);
	}


	@Override
	@UserIgnorableValidation
	public void executeTradeOrderTradeGroupList(Integer[] tradeGroupIds, TradeOrderGroupingTypes groupingType, boolean ignoreValidation) {
		TradeOrderGroupingCommand orderGroupingCommand = TradeOrderGroupingCommand.of(groupingType);
		orderGroupingCommand.setIgnoreValidation(ignoreValidation);

		executeTradeOrderTradeGroupListImpl(tradeGroupIds, orderGroupingCommand);
	}


	private void executeTradeOrderTradeGroupListImpl(Integer[] tradeGroupIds, TradeOrderGroupingCommand tradeOrderGroupingCommand) {
		List<Integer> tradeIds = new ArrayList<>();
		if (tradeGroupIds != null && tradeGroupIds.length > 0) {
			for (Integer tradeGroupId : tradeGroupIds) {
				TradeGroup group = getTradeGroupService().getTradeGroup(tradeGroupId);

				// TODO - IS THERE A BETTER WAY TO HANDLE THIS?  EVEN THOUGH THE GROUP IS IN CORRECT WORKFLOW STATE - WANT TO NOT ATTEMPT TO SEND CANCELLED TRADES, ETC.
				// ALSO - ONLY INDICATION THAT FIX IS ALLOWED IS ON EACH TRADE.
				for (Trade trade : CollectionUtils.getIterable(group.getTradeList())) {
					if (trade.isFixTrade() && TradeService.TRADE_APPROVED_STATE_NAME.equalsIgnoreCase(trade.getWorkflowState().getName())) {
						tradeIds.add(trade.getId());
					}
				}
			}
		}
		if (CollectionUtils.isEmpty(tradeIds)) {
			throw new ValidationException("There are no trades in selected group(s) that are available for Execution.");
		}

		createAndExecuteTradeOrderTradeListInternal(tradeIds.toArray(new Integer[0]), tradeOrderGroupingCommand);
	}


	@Override
	@UserIgnorableValidation
	public void executeTradeOrderTradeList(Integer[] tradeIdList, TradeOrderGroupingTypes groupingType, boolean ignoreValidation) {
		TradeOrderGroupingCommand orderGroupingCommand = TradeOrderGroupingCommand.of(groupingType);
		orderGroupingCommand.setIgnoreValidation(ignoreValidation);

		createAndExecuteTradeOrderTradeListInternal(tradeIdList, orderGroupingCommand);
	}


	private void createAndExecuteTradeOrderTradeListInternal(Integer[] tradeIdList, TradeOrderGroupingCommand tradeOrderGroupingCommand) {
		ValidationUtils.assertFalse(TransactionSynchronizationManager.isActualTransactionActive(), "Trade order creation and execution cannot be executed in a pre-existing transaction.");
		Map<TradeOrder, ?> orderMap = createTradeOrderTradeListInternalWithTransaction(tradeIdList, tradeOrderGroupingCommand);
		if (orderMap != null) {
			executeTradeOrderMapInternalWithTransaction(orderMap);
		}
	}


	@Transactional
	@SuppressWarnings("unchecked")
	protected Map<TradeOrder, ?> createTradeOrderTradeListInternalWithTransaction(Integer[] tradeIdList, TradeOrderGroupingCommand tradeOrderGroupingCommand) {
		List<TradeOrder> orderList = generateTradeOrder(tradeIdList, tradeOrderGroupingCommand);
		if (!getTradeOrderTypeByGroupingType(tradeOrderGroupingCommand.getGroupingType()).isLimitOrder()) {
			return getTradeOrderExecutionService().createTradeOrderToTargetObjectMap(orderList);
		}
		return null;
	}


	@Transactional
	@SuppressWarnings("unchecked")
	protected void executeTradeOrderMapInternalWithTransaction(Map<TradeOrder, ?> orderMap) {
		getTradeOrderExecutionService().executeTradeOrderTargetObjectMap(orderMap);
	}


	/**
	 * Executes the orders in the order list.  If isExecuteAsLimitOrder is set to true, each order
	 * will be checked to ensure it has a limit price.  If isExecuteAsLimitOrder is set to false,
	 * the order grouping type is updated, limit price is removed, so order gets executed as a market order.
	 * Note that this routine will process orders that contain trades without pre-configured TradeExecutionTypes.
	 */
	@Override
	@Transactional
	public void executeTradeOrderList(Integer[] tradeOrderIdList, boolean executeAsLimitOrder) {
		if (ArrayUtils.isEmpty(tradeOrderIdList)) {
			return;
		}

		List<TradeOrder> tradeOrders = new ArrayList<>();
		for (Integer id : tradeOrderIdList) {
			tradeOrders.add(getTradeOrder(id));
		}

		if (executeAsLimitOrder) {
			this.validateLimitPrice(tradeOrders);
		}


		for (TradeOrder tradeOrder : tradeOrders) {
			tradeOrder.setStatus(getTradeOrderStatusByStatuses(TradeOrderStatuses.SENT));

			// if the order is being executed as a limit order,
			// then push the order's limit price into the trade for consistency purposes.
			if (executeAsLimitOrder) {
				for (Trade trade : CollectionUtils.getIterable(tradeOrder.getTradeList())) {
					trade.setLimitPrice(tradeOrder.getLimitPrice());
					getTradeService().saveTrade(trade);
				}
			}
			else {
				// user has chosen to execute as a market order, so we need to remove limit price, reset grouping type to a non-limit type
				tradeOrder.setLimitPrice(null);
				if (tradeOrder.getType().getGroupingType() == TradeOrderGroupingTypes.EXECUTE_SEPARATELY_LIMIT) {
					tradeOrder.setType(getTradeOrderTypeByGroupingType(TradeOrderGroupingTypes.EXECUTE_SEPARATELY));
				}
				else if (tradeOrder.getType().getGroupingType() == TradeOrderGroupingTypes.DEFAULT_LIMIT) {
					tradeOrder.setType(getTradeOrderTypeByGroupingType(TradeOrderGroupingTypes.DEFAULT));
				}
			}
		}

		this.saveTradeOrderList(tradeOrders);
		getTradeOrderExecutionService().executeTradeOrderList(tradeOrders);
	}


	@Override
	@Transactional
	public void executeTradeOrderCancelReplace(Integer tradeOrderId) {
		executeTradeOrderCancelReplace(tradeOrderId, null);
	}


	@Override
	@Transactional
	public void executeTradeOrderAssignToNewTrader(Integer[] tradeOrderIdList, short securityUserId) {
		SecurityUser trader = getSecurityUserService().getSecurityUser(securityUserId);
		for (Integer tradeOrderId : tradeOrderIdList) {
			executeTradeOrderCancelReplace(tradeOrderId, trader);
		}
	}


	private List<TradeOrder> generateTradeOrder(Integer[] tradeIdList, TradeOrderGroupingCommand tradeOrderGroupingCommand) {
		List<TradeOrder> result = new ArrayList<>();
		// group the trades
		List<List<Trade>> tradeGroupList = populateAndGroupTrades(tradeIdList, tradeOrderGroupingCommand);
		for (List<Trade> tradeList : CollectionUtils.getIterable(tradeGroupList)) {
			if (!CollectionUtils.isEmpty(tradeList)) {
				result.add(generateTradeOrder(tradeList, tradeOrderGroupingCommand.getGroupingType()));
			}
		}
		return result;
	}


	private void executeTradeOrderCancelReplace(Integer tradeOrderId, SecurityUser trader) {
		TradeOrder originalOrder = getTradeOrder(tradeOrderId);
		if (originalOrder == null) {
			throw new RuntimeException("Cannot send the order cancel replace message because no order exists.");
		}
		SecurityUser currentTrader = getSecurityUserService().getSecurityUserCurrent();
		ValidationUtils.assertEquals(currentTrader, originalOrder.getTraderUser(), "Orders cannot be reassigned by user [" + currentTrader + "] because the current trader is [" + originalOrder.getTraderUser() + "].");

		if (originalOrder.getOrderGroup() == null) {
			originalOrder.setOrderGroup(originalOrder.getId());
			doSaveTradeOrder(originalOrder, false);
		}

		TradeOrder order = BeanUtils.cloneBean(originalOrder, false, false);
		order.setOriginalTradeOrder(originalOrder);
		order.setExternalIdentifier(null);
		BigDecimal orderQty = TradeOrderUtils.getTotalTradeQuantity(originalOrder.getTradeList());
		order.setQuantity(orderQty);
		order.setActiveOrder(false);
		if (trader != null) {
			order.setTraderUser(trader);
		}
		TradeOrder result = doSaveTradeOrder(order, true);

		List<TradeOrder> orderList = new ArrayList<>();
		orderList.add(result);
		getTradeOrderExecutionService().executeTradeOrderList(orderList);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                  Trade Order Type Methods                     ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeOrderType getTradeOrderType(short id) {
		return getTradeOrderTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeOrderType getTradeOrderTypeByName(String name) {
		return getTradeOrderTypeDAO().findOneByField("name", name);
	}


	@Override
	public TradeOrderType getTradeOrderTypeByGroupingType(TradeOrderGroupingTypes groupingType) {
		return getTradeOrderTypeDAO().findOneByField("groupingType", groupingType);
	}


	////////////////////////////////////////////////////////////////////////////
	//////              Trade Order Status Methods                       ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeOrderStatus getTradeOrderStatus(short id) {
		return getTradeOrderStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeOrderStatus getTradeOrderStatusByStatuses(TradeOrderStatuses orderStatus) {
		TradeOrderStatusSearchForm searchForm = new TradeOrderStatusSearchForm();
		searchForm.setOrderStatus(orderStatus);
		return CollectionUtils.getOnlyElement(getTradeOrderStatusList(searchForm));
	}


	@Override
	public List<TradeOrderStatus> getTradeOrderStatusList(TradeOrderStatusSearchForm searchForm) {
		return getTradeOrderStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrderStatus getManualTradeOrderStatus() {
		TradeOrderStatusSearchForm searchForm = new TradeOrderStatusSearchForm();
		searchForm.setOrderStatus(TradeOrderStatuses.FILLED);
		return CollectionUtils.getOnlyElement(getTradeOrderStatusList(searchForm));
	}


	private TradeOrderAllocationStatus getManualTradeOrderAllocationStatus() {
		TradeOrderAllocationStatusSearchForm searchForm = new TradeOrderAllocationStatusSearchForm();
		searchForm.setAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);
		searchForm.setAllocationFillReceived(false);
		return CollectionUtils.getOnlyElement(getTradeOrderAllocationService().getTradeOrderAllocationStatusList(searchForm));
	}


	private TradeOrder generateTradeOrder(List<Trade> tradeList, TradeOrderGroupingTypes groupingType) {
		// populate a list of securities for the order and determine if any of the trades are already part of an order.
		List<InvestmentSecurity> securityList = new ArrayList<>();
		for (Trade trade : tradeList) {
			TradeOrder order = getTradeOrderByTrade(trade.getId());
			if (!securityList.contains(trade.getInvestmentSecurity())) {
				securityList.add(trade.getInvestmentSecurity());
			}
			if (order != null) {
				throw new RuntimeException("Cannot generate Trade Order because one already exists for selected trade(s): " + order);
			}
		}
		TradeOrderType gt = getTradeOrderTypeByGroupingType(groupingType);

		TradeDestinationMapping mapping = getTradeDestinationService().getTradeDestinationMappingByCommand(new TradeDestinationMappingSearchCommand(tradeList.get(0)));
		SecurityUser traderUser = tradeList.get(0).getTraderUser();
		boolean buy = tradeList.get(0).isBuy();
		Date tradeDate = tradeList.get(0).getTradeDate();

		TradeOrder order = new TradeOrder();
		order.setType(gt);
		order.setTradeList(tradeList);
		order.setStatus(getTradeOrderStatusByStatuses(gt.isLimitOrder() ? TradeOrderStatuses.DRAFT : TradeOrderStatuses.SENT));
		order.setDestinationMapping(mapping);
		order.setTradeList(tradeList);
		order.setTraderUser(traderUser);
		order.setBuy(buy);
		order.setTradeDate(tradeDate);
		order.setActiveOrder(true);
		order.setLimitPrice(tradeList.get(0).getLimitPrice());

		populateOrderSecurityAndQuantity(order, securityList);

		if (!order.getDestinationMapping().getTradeDestination().getType().isFixTradeOnly()) {
			order.setStatus(getManualTradeOrderStatus());
			order.setAllocationStatus(getManualTradeOrderAllocationStatus());
		}

		TradeOrder result = saveTradeOrder(order);
		getTradeOrderWorkflowService().setTradeWorkflowStates(result, TradeService.TRADE_EXECUTION_STATE_NAME);

		return result;
	}


	private void populateOrderSecurityAndQuantity(TradeOrder order, List<InvestmentSecurity> securityList) {
		List<Trade> tradeList = order.getTradeList();

		if (securityList.size() <= 1) {
			InvestmentSecurity security = tradeList.get(0).getInvestmentSecurity();
			// use the sum of original face for bonds and the sum of quantity intended for everything else
			BigDecimal orderQty = TradeOrderUtils.getTotalTradeQuantity(tradeList);
			order.setQuantity(orderQty);
			if (InvestmentUtils.isSecurityOfType(security, InvestmentType.FORWARDS)) {
				order.setOriginalInvestmentSecurity(security);

				Trade firstTrade = tradeList.get(0);
				if (!firstTrade.getSettlementCurrency().equals(security.getInstrument().getTradingCurrency())) {
					order.setBuy(!order.isBuy());
					order.setInvestmentSecurity(security.getInstrument().getTradingCurrency());

					// reverse buy and sell
					// use notional
					// given is security.getInstrument().getTradingCurrency()
					// sett is always sett
				}
				else {
					order.setInvestmentSecurity(security.getInstrument().getUnderlyingInstrument().getTradingCurrency());
				}
			}
			else {
				order.setInvestmentSecurity(security);
			}
		}
		else {
			order.setSecurityList(new ArrayList<>());
			order.setQuantity(getAndValidateMultipleSecurityOrderQuantity(tradeList));
			for (InvestmentSecurity s : CollectionUtils.getIterable(securityList)) {
				TradeOrderSecurity os = new TradeOrderSecurity();
				os.setOrder(order);
				os.setInvestmentSecurity(s);
				order.getSecurityList().add(os);
			}
		}
	}


	private List<List<Trade>> populateAndGroupTrades(Integer[] tradeIdList, TradeOrderGroupingCommand tradeOrderGroupingCommand) {
		List<Trade> tradeList = new ArrayList<>();
		for (Integer tradeId : tradeIdList) {
			tradeList.add(getTradeService().getTrade(tradeId));
		}
		tradeOrderGroupingCommand.setTradeList(tradeList);
		return getTradeOrderGroupingService().groupTrades(tradeOrderGroupingCommand);
	}


	private BigDecimal getAndValidateMultipleSecurityOrderQuantity(List<Trade> tradeList) {
		Map<InvestmentSecurity, BigDecimal> qtyMap = new HashMap<>();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if (!qtyMap.containsKey(trade.getInvestmentSecurity())) {
				qtyMap.put(trade.getInvestmentSecurity(), TradeOrderUtils.getTradeQuantity(trade));
			}
			else {
				qtyMap.put(trade.getInvestmentSecurity(), MathUtils.add(qtyMap.get(trade.getInvestmentSecurity()), TradeOrderUtils.getTradeQuantity(trade)));
			}
		}

		BigDecimal lastQty = null;
		for (BigDecimal qty : CollectionUtils.getIterable(qtyMap.values())) {
			if (lastQty == null) {
				lastQty = qty;
			}
			else {
				ValidationUtils.assertTrue(MathUtils.compare(lastQty, qty) == 0, "For multiple security order's all quantities of individual securities must be equal.");
			}
		}
		return lastQty;
	}

	////////////////////////////////////////////////////////////////////////////
	//////             Trade Order Security Methods                      ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<TradeOrderSecurity> getTradeOrderSecurityListByOrder(int orderId) {
		return getTradeOrderSecurityDAO().findByField("order.id", orderId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateLimitPrice(TradeOrder tradeOrder) {
		ValidationUtils.assertNotNull(tradeOrder.getLimitPrice(), "Limit prices cannot be null.");
		ValidationUtils.assertTrue(MathUtils.isGreaterThan(tradeOrder.getLimitPrice(), BigDecimal.ZERO), "Limit prices must be greater than 0.00.");
	}


	private void validateLimitPrice(List<TradeOrder> tradeOrderList) {
		if (CollectionUtils.isEmpty(tradeOrderList)) {
			return;
		}

		for (TradeOrder tradeOrder : tradeOrderList) {
			validateLimitPrice(tradeOrder);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeOrder, Criteria> getTradeOrderDAO() {
		return this.tradeOrderDAO;
	}


	public void setTradeOrderDAO(AdvancedUpdatableDAO<TradeOrder, Criteria> tradeOrderDAO) {
		this.tradeOrderDAO = tradeOrderDAO;
	}


	public AdvancedUpdatableDAO<TradeOrderStatus, Criteria> getTradeOrderStatusDAO() {
		return this.tradeOrderStatusDAO;
	}


	public void setTradeOrderStatusDAO(AdvancedUpdatableDAO<TradeOrderStatus, Criteria> tradeOrderStatusDAO) {
		this.tradeOrderStatusDAO = tradeOrderStatusDAO;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeOrderWorkflowService getTradeOrderWorkflowService() {
		return this.tradeOrderWorkflowService;
	}


	public void setTradeOrderWorkflowService(TradeOrderWorkflowService tradeOrderWorkflowService) {
		this.tradeOrderWorkflowService = tradeOrderWorkflowService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	@SuppressWarnings("rawtypes")
	public TradeOrderExecutionService getTradeOrderExecutionService() {
		return this.tradeOrderExecutionService;
	}


	@SuppressWarnings("rawtypes")
	public void setTradeOrderExecutionService(TradeOrderExecutionService tradeOrderExecutionService) {
		this.tradeOrderExecutionService = tradeOrderExecutionService;
	}


	public TradeOrderGroupingService getTradeOrderGroupingService() {
		return this.tradeOrderGroupingService;
	}


	public void setTradeOrderGroupingService(TradeOrderGroupingService tradeOrderGroupingService) {
		this.tradeOrderGroupingService = tradeOrderGroupingService;
	}


	public AdvancedUpdatableDAO<TradeOrderSecurity, Criteria> getTradeOrderSecurityDAO() {
		return this.tradeOrderSecurityDAO;
	}


	public void setTradeOrderSecurityDAO(AdvancedUpdatableDAO<TradeOrderSecurity, Criteria> tradeOrderSecurityDAO) {
		this.tradeOrderSecurityDAO = tradeOrderSecurityDAO;
	}


	public AdvancedUpdatableDAO<TradeOrderType, Criteria> getTradeOrderTypeDAO() {
		return this.tradeOrderTypeDAO;
	}


	public void setTradeOrderTypeDAO(AdvancedUpdatableDAO<TradeOrderType, Criteria> tradeOrderTypeDAO) {
		this.tradeOrderTypeDAO = tradeOrderTypeDAO;
	}


	public TradeOrderAllocationService getTradeOrderAllocationService() {
		return this.tradeOrderAllocationService;
	}


	public void setTradeOrderAllocationService(TradeOrderAllocationService tradeOrderAllocationService) {
		this.tradeOrderAllocationService = tradeOrderAllocationService;
	}


	public TradeCommissionService getTradeCommissionService() {
		return this.tradeCommissionService;
	}


	public void setTradeCommissionService(TradeCommissionService tradeCommissionService) {
		this.tradeCommissionService = tradeCommissionService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
