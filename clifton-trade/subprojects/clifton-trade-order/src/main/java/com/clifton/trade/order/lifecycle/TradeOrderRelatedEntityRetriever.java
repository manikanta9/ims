package com.clifton.trade.order.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class TradeOrderRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private TradeOrderService tradeOrderService;

	private TradeOrderAllocationService tradeOrderAllocationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		if (entity instanceof Trade) {
			return ((Trade) entity).getOrderIdentifier() != null;
		}
		return entity instanceof TradeOrder;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();

		TradeOrder tradeOrder;
		if (entity instanceof  Trade) {
			Trade trade = (Trade) entity;
			tradeOrder = getTradeOrderService().getTradeOrderByTrade(trade.getId());
			if (tradeOrder != null) {
				relatedEntityList.add(tradeOrder);
			}
		}
		else {
			tradeOrder = (TradeOrder) entity;
		}

		if (tradeOrder != null) {
			TradeOrderAllocationSearchForm searchForm = new TradeOrderAllocationSearchForm();
			searchForm.setTradeOrderId(tradeOrder.getId());
			List<TradeOrderAllocation> allocations = getTradeOrderAllocationService().getTradeOrderAllocationList(searchForm);
			if (!CollectionUtils.isEmpty(allocations)) {
				relatedEntityList.addAll(allocations);
			}
		}

		return relatedEntityList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderAllocationService getTradeOrderAllocationService() {
		return this.tradeOrderAllocationService;
	}


	public void setTradeOrderAllocationService(TradeOrderAllocationService tradeOrderAllocationService) {
		this.tradeOrderAllocationService = tradeOrderAllocationService;
	}
}
