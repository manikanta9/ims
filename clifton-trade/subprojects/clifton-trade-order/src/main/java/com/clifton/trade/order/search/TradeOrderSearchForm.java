package com.clifton.trade.order.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;

import java.math.BigDecimal;
import java.util.Date;


public class TradeOrderSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Integer excludeTradeOrderId;

	/**
	 * Look up all orders for a given allocation id.
	 * <p/>
	 * NOTE: This will look up the active order, not the order that is linked via the TradeOrderTradeOrderAllocation table.
	 */
	// Custom Search Filter
	private Integer allocationIdForAllOrders;

	private Integer allocationId;

	/**
	 * Using an order id, lookup all the orders that are allocated together.
	 */
	// Custom Search Filter
	private Integer allocatedOrderId;

	@SearchField
	private Integer id;

	// Custom Search Filter
	private Integer tradeId;

	// Custom Search Filter
	private String tradeWorkflowStatusName;

	// Custom Search Filter
	private String tradeWorkflowStateName;

	@SearchField
	private Date tradeDate;

	@SearchField(searchField = "status.orderStatus", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private TradeOrderStatuses notEqualOrderStatus;

	@SearchField(searchField = "traderUser.id", sortField = "traderUser.displayName")
	private Short traderId;

	// Custom Search Filter
	private Short teamSecurityGroupId;

	@SearchField(searchField = "orderStatus", searchFieldPath = "status")
	private TradeOrderStatuses orderStatus;

	@SearchField(searchField = "orderStatus", searchFieldPath = "status", comparisonConditions = ComparisonConditions.IN)
	private TradeOrderStatuses[] orderStatusList;

	@SearchField(searchField = "allocationStatus", searchFieldPath = "allocationStatus")
	private TradeOrderAllocationStatuses allocationStatus;

	private TradeOrderAllocationStatuses[] allocationStatusList;

	@SearchField
	private BigDecimal quantityFilled;

	@SearchField
	private BigDecimal averagePrice;

	@SearchField(searchField = "allocationFillReceived", searchFieldPath = "allocationStatus")
	private Boolean allocationFillReceived;

	@SearchField
	private Boolean buy;

	@SearchField
	private BigDecimal quantity;

	// Custom Search Filter - look locally and in TradeOrderSecurity
	private String symbol;

	// Custom Search Filter - look locally and in TradeOrderSecurity
	private Integer investmentSecurityId;

	// Custom Search Filter - look locally and in TradeOrderSecurity
	private Short investmentGroupId;

	@SearchField(searchFieldPath = "destinationMapping", searchField = "tradeDestination.id")
	private Short tradeDestinationId;

	@SearchField(searchFieldPath = "destinationMapping.tradeDestination", searchField = "name")
	private String tradeDestinationName;

	@SearchField(searchFieldPath = "type", searchField = "name")
	private String tradeOrderTypeName;

	@SearchField(searchFieldPath = "type", searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] tradeOrderTypeNameList;

	@SearchField(searchFieldPath = "destinationMapping", searchField = "executingBrokerCompany.id")
	private Integer executingBrokerId;

	@SearchField
	private Boolean activeOrder;

	@SearchField
	private Integer orderGroup;

	// Custom Search Filter
	private Integer clientInvestmentAccountGroupId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	//	public Integer getAllocationId() {
	//		return this.allocationId;
	//	}
	//
	//
	//	public void setAllocationId(Integer allocationId) {
	//		this.allocationId = allocationId;
	//	}


	public Integer getAllocatedOrderId() {
		return this.allocatedOrderId;
	}


	public void setAllocatedOrderId(Integer allocatedOrderId) {
		this.allocatedOrderId = allocatedOrderId;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public String getTradeWorkflowStatusName() {
		return this.tradeWorkflowStatusName;
	}


	public void setTradeWorkflowStatusName(String tradeWorkflowStatusName) {
		this.tradeWorkflowStatusName = tradeWorkflowStatusName;
	}


	public String getTradeWorkflowStateName() {
		return this.tradeWorkflowStateName;
	}


	public void setTradeWorkflowStateName(String tradeWorkflowStateName) {
		this.tradeWorkflowStateName = tradeWorkflowStateName;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public TradeOrderStatuses getNotEqualOrderStatus() {
		return this.notEqualOrderStatus;
	}


	public void setNotEqualOrderStatus(TradeOrderStatuses notEqualOrderStatus) {
		this.notEqualOrderStatus = notEqualOrderStatus;
	}


	public Short getTraderId() {
		return this.traderId;
	}


	public void setTraderId(Short traderId) {
		this.traderId = traderId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public TradeOrderStatuses getOrderStatus() {
		return this.orderStatus;
	}


	public void setOrderStatus(TradeOrderStatuses orderStatus) {
		this.orderStatus = orderStatus;
	}


	public TradeOrderStatuses[] getOrderStatusList() {
		return this.orderStatusList;
	}


	public void setOrderStatusList(TradeOrderStatuses[] orderStatusList) {
		this.orderStatusList = orderStatusList;
	}


	public TradeOrderAllocationStatuses getAllocationStatus() {
		return this.allocationStatus;
	}


	public void setAllocationStatus(TradeOrderAllocationStatuses allocationStatus) {
		this.allocationStatus = allocationStatus;
	}


	public BigDecimal getQuantityFilled() {
		return this.quantityFilled;
	}


	public void setQuantityFilled(BigDecimal quantityFilled) {
		this.quantityFilled = quantityFilled;
	}


	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}


	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}


	public Boolean getAllocationFillReceived() {
		return this.allocationFillReceived;
	}


	public void setAllocationFillReceived(Boolean allocationFillReceived) {
		this.allocationFillReceived = allocationFillReceived;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getTradeDestinationId() {
		return this.tradeDestinationId;
	}


	public void setTradeDestinationId(Short tradeDestinationId) {
		this.tradeDestinationId = tradeDestinationId;
	}


	public String getTradeDestinationName() {
		return this.tradeDestinationName;
	}


	public void setTradeDestinationName(String tradeDestinationName) {
		this.tradeDestinationName = tradeDestinationName;
	}


	public String getTradeOrderTypeName() {
		return this.tradeOrderTypeName;
	}


	public void setTradeOrderTypeName(String tradeOrderTypeName) {
		this.tradeOrderTypeName = tradeOrderTypeName;
	}


	public String[] getTradeOrderTypeNameList() {
		return this.tradeOrderTypeNameList;
	}


	public void setTradeOrderTypeNameList(String[] tradeOrderTypeNameList) {
		this.tradeOrderTypeNameList = tradeOrderTypeNameList;
	}


	public Integer getExecutingBrokerId() {
		return this.executingBrokerId;
	}


	public void setExecutingBrokerId(Integer executingBrokerId) {
		this.executingBrokerId = executingBrokerId;
	}


	public Boolean getActiveOrder() {
		return this.activeOrder;
	}


	public void setActiveOrder(Boolean activeOrder) {
		this.activeOrder = activeOrder;
	}


	public Integer getOrderGroup() {
		return this.orderGroup;
	}


	public void setOrderGroup(Integer orderGroup) {
		this.orderGroup = orderGroup;
	}


	public Integer getAllocationIdForAllOrders() {
		return this.allocationIdForAllOrders;
	}


	public void setAllocationIdForAllOrders(Integer allocationIdForAllOrders) {
		this.allocationIdForAllOrders = allocationIdForAllOrders;
	}


	public Integer getAllocationId() {
		return this.allocationId;
	}


	public void setAllocationId(Integer allocationId) {
		this.allocationId = allocationId;
	}


	public TradeOrderAllocationStatuses[] getAllocationStatusList() {
		return this.allocationStatusList;
	}


	public void setAllocationStatusList(TradeOrderAllocationStatuses[] allocationStatusList) {
		this.allocationStatusList = allocationStatusList;
	}


	public Integer getExcludeTradeOrderId() {
		return this.excludeTradeOrderId;
	}


	public void setExcludeTradeOrderId(Integer excludeTradeOrderId) {
		this.excludeTradeOrderId = excludeTradeOrderId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}
}
