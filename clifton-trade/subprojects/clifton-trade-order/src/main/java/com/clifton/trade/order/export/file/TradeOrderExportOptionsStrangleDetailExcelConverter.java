package com.clifton.trade.order.export.file;


import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderSecurity;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Creates an excel file the options strangle details information.
 */
public class TradeOrderExportOptionsStrangleDetailExcelConverter extends TradeOrderExportOptionsStrangleSummaryExcelConverter {


	@Override
	public FileWrapper convert(List<TradeOrder> tradeOrderList) {
		Workbook workbook = ExcelFileUtils.createWorkbook(FileFormats.EXCEL_OPEN_XML.getFormatString());

		Map<InvestmentSecurity, String> putCallMap = getPutCallMap(tradeOrderList);

		Sheet sheet = workbook.getSheetAt(0);
		int rowIndex = -1;
		for (TradeOrder order : CollectionUtils.getIterable(tradeOrderList)) {
			ValidationUtils.assertTrue(TradeOrderGroupingTypes.OPTIONS_STRANGLE == order.getType().getGroupingType(), "[" + getClass().getName() + "] does not support with grouping type ["
					+ order.getType().getGroupingType() + "].");

			rowIndex++;
			rowIndex = addOrderDetail(order, null, sheet, rowIndex, putCallMap);
			rowIndex++;
			List<InvestmentAccount> accountList = new ArrayList<>();
			for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
				// only add accounts once
				if (!accountList.contains(trade.getHoldingInvestmentAccount())) {
					rowIndex = addTradeDetail(trade, sheet, rowIndex);
					accountList.add(trade.getHoldingInvestmentAccount());
					rowIndex++;
				}
			}
		}

		// resize the columns
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);

		return new FileWrapper(ExcelFileUtils.getFileFromWorkbook(workbook, "OptionsStrangleDetailsExport"), "OptionsStrangleDetailsExport." + FileFormats.EXCEL_OPEN_XML.getFormatString(), true);
	}


	private int addTradeDetail(Trade trade, Sheet sheet, int rowIndex) {
		addCell(sheet, rowIndex, 0, trade.getHoldingInvestmentAccount().getName());
		addCell(sheet, rowIndex, 3, trade.getHoldingInvestmentAccount().getNumber());
		addCell(sheet, rowIndex, 4, trade.getQuantityIntended(), DataTypes.INT);

		CellRangeAddress mergeRange = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
		sheet.addMergedRegion(mergeRange);

		return rowIndex;
	}


	private int addOrderDetail(TradeOrder order, TradeOrderSecurity security, Sheet sheet, int rowIndex, Map<InvestmentSecurity, String> putCallMap) {
		// add the individual securities
		if (order.isMultipleSecurityOrder() && (security == null)) {
			int r = rowIndex;
			for (TradeOrderSecurity s : CollectionUtils.getIterable(order.getSecurityList())) {
				r = addOrderDetail(order, s, sheet, r, putCallMap);
				r++;
			}
			return r - 1;
		}
		InvestmentSecurity is = security != null ? security.getInvestmentSecurity() : order.getInvestmentSecurity();
		BigDecimal averagePrice = security != null ? security.getAveragePrice() : order.getAveragePrice();
		BigDecimal commissionPerUnit = security != null ? security.getCommissionPerUnit() : order.getTradeList().get(0).getCommissionPerUnit();

		addCell(sheet, rowIndex, 0, order.isBuy() ? "B" : "S", true);
		addCell(sheet, rowIndex, 1, order.getQuantity(), DataTypes.INT, true);
		addCell(sheet, rowIndex, 2, is.getDescription(), true);
		addCell(sheet, rowIndex, 3, averagePrice, DataTypes.PRICE, true);
		addCell(sheet, rowIndex, 4, commissionPerUnit, DataTypes.DECIMAL_PRECISE, true);

		return rowIndex;
	}


	@Override
	protected void addSecurity(TradeOrder order, Sheet sheet, int rowIndex, boolean put, Map<InvestmentSecurity, String> putCallMap) {
		for (TradeOrderSecurity security : CollectionUtils.getIterable(order.getSecurityList())) {
			boolean securityIsPut = "put".equals(putCallMap.get(security.getInvestmentSecurity()));
			if (put && securityIsPut) {
				addCell(sheet, rowIndex, 0, "Put Option:", true);
				addCell(sheet, rowIndex, 1, security.getInvestmentSecurity().getDescription());
			}
			else if (!put && !securityIsPut) {
				addCell(sheet, rowIndex, 0, "Call Option:", true);
				addCell(sheet, rowIndex, 1, security.getInvestmentSecurity().getDescription());
			}
		}
	}
}
