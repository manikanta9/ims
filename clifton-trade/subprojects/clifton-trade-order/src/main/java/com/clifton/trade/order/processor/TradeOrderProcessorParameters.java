package com.clifton.trade.order.processor;


import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetail;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>TradeOrderProcessorParameters</code> a holder class used to pass the parameters need to
 * process an order at it's current state.
 * <p/>
 * NOTE: Depending on the actionType different properties need to be populated.
 * <p/>
 * PROCESS_ORDER requires tradeOrder
 * PROCESS_ORDER_MULTIPLE_BLOCK_ALLOCATE and PROCESS_ORDER_MULTIPLE require tradeOrderList
 * PROCESS_ORDER_ALLOCATION requires both tradeOrderList and allocationDetailList
 *
 * @author mwacker
 */
public class TradeOrderProcessorParameters {

	/**
	 * Indicates the type of processing for the orders.
	 */
	private TradeOrderProcessorActonTypes actonType;
	/**
	 * The order to be processed.
	 */
	private TradeOrder tradeOrder;

	/**
	 * The list of orders that were updated and need to be processed.
	 */
	private List<TradeOrder> tradeOrderList;
	/**
	 * If the allocation is complete, this will contain the allocations that will be used
	 * to create the trade fills.
	 */
	private List<TradeOrderAllocationDetail> allocationDetailList = new ArrayList<>();
	/**
	 * This is used to indicated the trades should be filled with the average price ignoring the
	 * defined process.
	 */
	private boolean forceAveragePriceAllocation;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderProcessorParameters() {
		//
	}


	public TradeOrderProcessorParameters(TradeOrderProcessorActonTypes actonType) {
		this.actonType = actonType;
	}


	public TradeOrderProcessorParameters(TradeOrder tradeOrder, TradeOrderProcessorActonTypes actonType) {
		this(actonType);
		this.tradeOrder = tradeOrder;
	}


	public TradeOrderProcessorParameters(List<TradeOrder> tradeOrderList, TradeOrderProcessorActonTypes actonType) {
		this(actonType);
		this.tradeOrderList = tradeOrderList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<TradeOrder> getTradeOrderList() {
		return this.tradeOrderList;
	}


	public void setTradeOrderList(List<TradeOrder> tradeOrderList) {
		this.tradeOrderList = tradeOrderList;
	}


	public List<TradeOrderAllocationDetail> getAllocationDetailList() {
		return this.allocationDetailList;
	}


	public void setAllocationDetailList(List<TradeOrderAllocationDetail> allocationDetailList) {
		this.allocationDetailList = allocationDetailList;
	}


	public TradeOrder getTradeOrder() {
		return this.tradeOrder;
	}


	public void setTradeOrder(TradeOrder tradeOrder) {
		this.tradeOrder = tradeOrder;
	}


	public TradeOrderProcessorActonTypes getActonType() {
		return this.actonType;
	}


	public void setActonType(TradeOrderProcessorActonTypes actonType) {
		this.actonType = actonType;
	}


	public boolean isForceAveragePriceAllocation() {
		return this.forceAveragePriceAllocation;
	}


	public void setForceAveragePriceAllocation(boolean forceAveragePriceAllocation) {
		this.forceAveragePriceAllocation = forceAveragePriceAllocation;
	}
}
