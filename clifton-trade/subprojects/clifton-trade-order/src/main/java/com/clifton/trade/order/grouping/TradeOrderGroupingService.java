package com.clifton.trade.order.grouping;


import com.clifton.trade.Trade;

import java.util.List;


public interface TradeOrderGroupingService {

	/**
	 * Groups the a list of trades on the provided command for execution based on the command's defined grouping type.
	 * <p>
	 * Validation is performed on the provided trades to ensure they can be executed. An example of where this validation is useful is
	 * ensuring all trades of a trade group are included in execution for best execution and to prevent multi-leg trades from being stranded.
	 */
	public List<List<Trade>> groupTrades(TradeOrderGroupingCommand tradeOrderGroupingCommand);
}
