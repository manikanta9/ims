package com.clifton.trade.order;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.trade.Trade;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;
import com.clifton.trade.order.search.TradeOrderSearchForm;
import com.clifton.trade.order.search.TradeOrderStatusSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.List;


public interface TradeOrderService {

	public static final String DROP_COPY_TRADE_ORDER_TYPE_NAME = "EXTERNAL_DROP_COPY_ORDER";


	//////////////////////////////////////////////////////////////////////////// 
	//////                  Trade Order Methods                          /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeOrder getTradeOrder(int id);


	public TradeOrder getTradeOrderByTrade(int tradeId);


	public TradeOrder getTradeOrderByExternalIdentifier(Integer externalIdentifier);


	public List<TradeOrder> getTradeOrderListByAllocationId(int orderAllocationId);


	@DoNotAddRequestMapping
	public List<TradeOrder> getTradeOrderListPopulated(TradeOrderSearchForm searchForm);


	public List<TradeOrder> getTradeOrderList(TradeOrderSearchForm searchForm);


	public TradeOrder saveTradeOrder(TradeOrder bean);


	public TradeOrder updateTradeOrderLimitPrice(Integer tradeOrderId, BigDecimal limitPrice);


	/**
	 * Used for Trade Groups (currently Roll Trades) - Finds all applicable trades in the selected group(s).
	 * <p>
	 * Validates trades of a trade group have the same workflow states as sibling trades.
	 */
	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderTradeGroupListExecute")
	@UserIgnorableValidation
	public void executeTradeOrderTradeGroupList(Integer[] tradeGroupIds, TradeOrderGroupingTypes groupingType, boolean ignoreValidation);


	/**
	 * Used to create and execute trade order(s) for the specified grouping type.
	 * <p>
	 * Validates that trades that are a member of a group have the same state as sibling trades.
	 */
	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderTradeListExecute")
	@UserIgnorableValidation
	public void executeTradeOrderTradeList(Integer[] tradeIdList, TradeOrderGroupingTypes groupingType, boolean ignoreValidation);


	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderListExecute")
	public void executeTradeOrderList(Integer[] tradeOrderIdList, boolean executeAsLimitOrder);


	@SecureMethod
	@RequestMapping("tradeOrderCancelReplaceExecute")
	public void executeTradeOrderCancelReplace(Integer tradeOrderId);


	@SecureMethod
	@RequestMapping("tradeOrderAssignToNewTraderExecute")
	public void executeTradeOrderAssignToNewTrader(Integer[] tradeOrderIdList, short securityUserId);

	//////////////////////////////////////////////////////////////////////////// 
	//////              Trade Order Status Methods                       /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderStatus getTradeOrderStatus(short id);


	public TradeOrderStatus getTradeOrderStatusByStatuses(TradeOrderStatuses orderStatus);


	public List<TradeOrderStatus> getTradeOrderStatusList(TradeOrderStatusSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	//////             Trade Order Security Methods                      /////// 
	////////////////////////////////////////////////////////////////////////////


	public List<TradeOrderSecurity> getTradeOrderSecurityListByOrder(int orderId);


	////////////////////////////////////////////////////////////////////////////
	//////                  Trade Order Type Methods                     /////// 
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderType getTradeOrderType(short id);


	public TradeOrderType getTradeOrderTypeByName(String name);


	public TradeOrderType getTradeOrderTypeByGroupingType(TradeOrderGroupingTypes groupingType);
}
