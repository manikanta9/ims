package com.clifton.trade.order.allocation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationSearchForm;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationStatusSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class TradeOrderAllocationServiceImpl implements TradeOrderAllocationService {

	private AdvancedUpdatableDAO<TradeOrderAllocation, Criteria> tradeOrderAllocationDAO;
	private AdvancedUpdatableDAO<TradeOrderAllocationStatus, Criteria> tradeOrderAllocationStatusDAO;
	private AdvancedUpdatableDAO<TradeOrderTradeOrderAllocation, Criteria> tradeOrderTradeOrderAllocationDAO;

	private DaoSingleKeyListCache<TradeOrderAllocationStatus, TradeOrderAllocationStatuses> tradeOrderAllocationStatusCache;


	////////////////////////////////////////////////////////////////////////////
	//////              Trade Order Allocation Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeOrderAllocation getTradeOrderAllocation(int id) {
		return getTradeOrderAllocationDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeOrderAllocation getTradeOrderAllocationActiveByOrder(TradeOrder tradeOrder) {
		TradeOrderAllocationSearchForm searchForm = new TradeOrderAllocationSearchForm();
		if (tradeOrder.getOrderGroup() != null) {
			searchForm.setTradeOrderGroup(tradeOrder.getOrderGroup());
		}
		else {
			searchForm.setTradeOrderId(tradeOrder.getId());
		}
		searchForm.setActive(true);
		return CollectionUtils.getOnlyElement(getTradeOrderAllocationList(searchForm));
	}


	@Override
	public List<TradeOrderAllocation> getTradeOrderAllocationListByExternalOrder(Integer externalOrderIdentifier) {
		TradeOrderAllocationSearchForm searchForm = new TradeOrderAllocationSearchForm();
		searchForm.setExternalOrderIdentifier(externalOrderIdentifier);
		return getTradeOrderAllocationList(searchForm);
	}


	@Override
	public List<TradeOrderAllocation> getTradeOrderAllocationList(final TradeOrderAllocationSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if ((searchForm.getTradeOrderGroup() != null) || (searchForm.getTradeOrderId() != null)) {

					DetachedCriteria sub = DetachedCriteria.forClass(TradeOrderTradeOrderAllocation.class, "link");
					sub.setProjection(Projections.property("id"));
					// add these to search sub properties
					sub.createAlias("referenceOne", "order");
					sub.createAlias("referenceTwo", "allocation");
					if (searchForm.getTradeOrderGroup() != null) {
						sub.add(Restrictions.eq("order.orderGroup", searchForm.getTradeOrderGroup()));
					}
					else {
						sub.add(Restrictions.eq("order.id", searchForm.getTradeOrderId()));
					}
					sub.add(Restrictions.eqProperty("allocation.id", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.exists(sub));
				}
				if (searchForm.getExternalOrderIdentifier() != null) {

					DetachedCriteria sub = DetachedCriteria.forClass(TradeOrderTradeOrderAllocation.class, "link");
					sub.setProjection(Projections.property("id"));
					// add these to search sub properties
					sub.createAlias("referenceOne", "order");
					sub.createAlias("referenceTwo", "allocation");
					sub.add(Restrictions.eq("order.externalIdentifier", searchForm.getExternalOrderIdentifier()));
					sub.add(Restrictions.eqProperty("allocation.id", criteria.getAlias() + ".id"));
					criteria.add(Subqueries.exists(sub));
				}
			}
		};
		return getTradeOrderAllocationDAO().findBySearchCriteria(config);
	}


	@Override
	@Transactional
	public TradeOrderAllocation saveTradeOrderAllocation(TradeOrderAllocation bean) {
		boolean newBean = bean.isNewBean();
		TradeOrderAllocation allocation = getTradeOrderAllocationDAO().save(bean);

		if (newBean) {
			ValidationUtils.assertNotEmpty(bean.getTradeOrderList(), "Cannot create [TradeOrderAllocation] entry without a [TradeOrder] list.");
			for (TradeOrder order : CollectionUtils.getIterable(bean.getTradeOrderList())) {
				if (bean.isActive()) {
					TradeOrderAllocation activeAllocation = getTradeOrderAllocationActiveByOrder(order);
					ValidationUtils.assertNull(activeAllocation, "An active allocation already exists for order [" + order.getId() + "].");
				}

				TradeOrderTradeOrderAllocation oa = new TradeOrderTradeOrderAllocation();
				oa.setReferenceOne(order);
				oa.setReferenceTwo(allocation);
				getTradeOrderTradeOrderAllocationDAO().save(oa);
			}
		}
		// set the order list on the resulting allocation object
		allocation.setTradeOrderList(bean.getTradeOrderList());
		return allocation;
	}


	////////////////////////////////////////////////////////////////////////////
	//////           Trade Order - Trade Order Allocation Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<TradeOrderTradeOrderAllocation> getTradeOrderTradeOrderAllocationList(int allocationId) {
		return getTradeOrderTradeOrderAllocationDAO().findByField("referenceTwo.id", allocationId);
	}


	////////////////////////////////////////////////////////////////////////////
	//////           Trade Order Allocation Status Methods               ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeOrderAllocationStatus getTradeOrderAllocationStatus(short id) {
		return getTradeOrderAllocationStatusDAO().findByPrimaryKey(id);
	}


	@Override
	public TradeOrderAllocationStatus getTradeOrderAllocationStatusByStatuses(TradeOrderAllocationStatuses allocationStatus, Boolean allocationFillReceived) {
		List<TradeOrderAllocationStatus> allocationStatuses = getTradeOrderAllocationStatusCache().getBeanListForKeyValue(getTradeOrderAllocationStatusDAO(), allocationStatus);
		return CollectionUtils.getOnlyElement(BeanUtils.filter(allocationStatuses, TradeOrderAllocationStatus::isAllocationFillReceived, allocationFillReceived));
	}


	@Override
	public List<TradeOrderAllocationStatus> getTradeOrderAllocationStatusListByStatuses(TradeOrderAllocationStatuses allocationStatus) {
		return getTradeOrderAllocationStatusCache().getBeanListForKeyValue(getTradeOrderAllocationStatusDAO(), allocationStatus);
	}


	@Override
	public List<TradeOrderAllocationStatus> getTradeOrderAllocationStatusList(TradeOrderAllocationStatusSearchForm searchForm) {
		return getTradeOrderAllocationStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<TradeOrderAllocation, Criteria> getTradeOrderAllocationDAO() {
		return this.tradeOrderAllocationDAO;
	}


	public void setTradeOrderAllocationDAO(AdvancedUpdatableDAO<TradeOrderAllocation, Criteria> tradeOrderAllocationDAO) {
		this.tradeOrderAllocationDAO = tradeOrderAllocationDAO;
	}


	public AdvancedUpdatableDAO<TradeOrderTradeOrderAllocation, Criteria> getTradeOrderTradeOrderAllocationDAO() {
		return this.tradeOrderTradeOrderAllocationDAO;
	}


	public void setTradeOrderTradeOrderAllocationDAO(AdvancedUpdatableDAO<TradeOrderTradeOrderAllocation, Criteria> tradeOrderTradeOrderAllocationDAO) {
		this.tradeOrderTradeOrderAllocationDAO = tradeOrderTradeOrderAllocationDAO;
	}


	public AdvancedUpdatableDAO<TradeOrderAllocationStatus, Criteria> getTradeOrderAllocationStatusDAO() {
		return this.tradeOrderAllocationStatusDAO;
	}


	public void setTradeOrderAllocationStatusDAO(AdvancedUpdatableDAO<TradeOrderAllocationStatus, Criteria> tradeOrderAllocationStatusDAO) {
		this.tradeOrderAllocationStatusDAO = tradeOrderAllocationStatusDAO;
	}


	public DaoSingleKeyListCache<TradeOrderAllocationStatus, TradeOrderAllocationStatuses> getTradeOrderAllocationStatusCache() {
		return this.tradeOrderAllocationStatusCache;
	}


	public void setTradeOrderAllocationStatusCache(DaoSingleKeyListCache<TradeOrderAllocationStatus, TradeOrderAllocationStatuses> tradeOrderAllocationStatusCache) {
		this.tradeOrderAllocationStatusCache = tradeOrderAllocationStatusCache;
	}
}
