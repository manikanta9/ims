package com.clifton.trade.order.util;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class TradeOrderUtilHandlerImpl implements TradeOrderUtilHandler {

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getHoldingAccountForBroker(Trade trade, BusinessCompany executingBroker) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setMainAccountId(trade.getClientInvestmentAccount().getId());
		searchForm.setMainPurpose(TradeService.TRADING_PURPOSE_NAME_PREFIX + trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName());
		searchForm.setMainPurposeActiveOnDate(trade.getTradeDate());
		searchForm.setIssuingCompanyId(executingBroker.getId());
		searchForm.setOurAccount(false);
		searchForm.setWorkflowStatusName(WorkflowStatus.STATUS_ACTIVE);
		return CollectionUtils.getFirstElement(getInvestmentAccountService().getInvestmentAccountList(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
