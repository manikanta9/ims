package com.clifton.trade.order;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.trade.Trade;


public enum TradeOrderStatuses {
	/**
	 * The order has been created, but has not yet been sent.
	 */
	DRAFT,
	/**
	 * The order was sent, but no execution report(s) has been received.
	 */
	SENT,
	/**
	 * The order was received and created by the counter party, but nothing has been executed against it yet.
	 */
	NEW,
	/**
	 * The order was rejected.
	 */
	REJECTED,
	/**
	 * The order was canceled without any fills.
	 */
	CANCELED,
	/**
	 * The order is partially executed.
	 */
	PARTIALLY_FILLED,
	/**
	 * The order is fully executed.  At this point allocations can be sent.
	 */
	FILLED,
	/**
	 * The order was partially executed and then canceled.  At this point allocations can be sent.
	 */
	PARTIALLY_FILLED_CANCELED,
	/**
	 * The order was replaced.
	 */
	REPLACED,
	/**
	 * The order was replaced and filled.
	 */
	REPLACED_FILLED,
	/**
	 * The order is done for the day.
	 */
	DONE_FOR_DAY,
	/**
	 * The order is done for the day and partially filled.
	 */
	PARTIALLY_FILLED_DONE_FOR_DAY,
	/**
	 * The order is on hold for further allocation processing until the market has closed for the day.
	 */
	HOLD_MARKET_CLOSE,
	/**
	 * The order has an execution type of REPLACE, and is on hold for further allocation processing until the market has closed for the day,
	 */
	HOLD_MARKET_CLOSE_REPLACED;


	public static TradeOrderStatuses getTradeOrderStatus(TradeOrder order, OrderStatuses orderStatus) {
		return getTradeOrderStatus(order, orderStatus, null, null, false);
	}


	public static TradeOrderStatuses getTradeOrderStatus(TradeOrder order, FixExecutionReport report, boolean tradeListInExecutionState) {
		OrderStatuses orderStatus = report.getOrderStatus();
		ExecutionTypes executionType = report.getExecutionType();
		String text = report.getText();
		return getTradeOrderStatus(order, orderStatus, executionType, text, tradeListInExecutionState);
	}


	private static TradeOrderStatuses getTradeOrderStatus(TradeOrder order, OrderStatuses orderStatus, ExecutionTypes executionType, String text, boolean tradeListInExecutionState) {
		if (orderStatus != null) {
			boolean isBticTrade = false;
			final Trade firstTrade = CollectionUtils.getFirstElement(order.getTradeList());
			if (firstTrade != null && firstTrade.getTradeExecutionType() != null && firstTrade.getTradeExecutionType().isBticTrade()) {
				isBticTrade = true;
			}

			switch (orderStatus) {
				case PARTIALLY_FILLED:
					if (executionType == ExecutionTypes.REPLACE) {
						return TradeOrderStatuses.REPLACED;
					}
					// if the trades are still be executed or the status is not fully filled, allow updating the order status to partially filled.
					// This prevents updating the order status to partially filled after by reprocessing FIX messages after an order is complete.
					if (tradeListInExecutionState || !order.getStatus().isFillComplete()) {
						return TradeOrderStatuses.PARTIALLY_FILLED;
					}
					break;
				case FILLED:
					if (executionType == ExecutionTypes.REPLACE) {
						return isBticTrade ? TradeOrderStatuses.HOLD_MARKET_CLOSE_REPLACED : TradeOrderStatuses.REPLACED_FILLED;
					}
					return isBticTrade ? TradeOrderStatuses.HOLD_MARKET_CLOSE : TradeOrderStatuses.FILLED;
				case CANCELED:
					if (order.getStatus().getOrderStatus() == TradeOrderStatuses.PARTIALLY_FILLED) {
						return isBticTrade ? TradeOrderStatuses.HOLD_MARKET_CLOSE : TradeOrderStatuses.PARTIALLY_FILLED_CANCELED;
					}
					return TradeOrderStatuses.CANCELED;
				case REJECTED:
					if (!order.getStatus().isFillComplete() || order.getOriginalTradeOrder() != null) {
						boolean duplicateReject = !StringUtils.isEmpty(text) && (text.toLowerCase().contains("duplicate"));
						if (duplicateReject && (order.getStatus() != null)) {
							// do nothing: duplicate order reject
						}
						else {
							return TradeOrderStatuses.REJECTED;
						}
					}
					break;

				case NEW:
					if (order.getOriginalTradeOrder() != null) {
						return TradeOrderStatuses.REPLACED;
					}
					return TradeOrderStatuses.NEW;

				case REPLACED:
					return TradeOrderStatuses.REPLACED;
				case DONE_FOR_DAY:
					if (isBticTrade) {
						return TradeOrderStatuses.HOLD_MARKET_CLOSE;
					}

					if (order.getStatus().isFillComplete()) {
						return TradeOrderStatuses.DONE_FOR_DAY;
					}

					if (order.getStatus().isPartialFill()) {
						return TradeOrderStatuses.PARTIALLY_FILLED_DONE_FOR_DAY;
					}
					//Fall through for orders that are not filled or not partially filled.

				case PENDING_REPLACE:
				case PENDING_CANCEL:
				case STOPPED:
				default:
					break;
			}
		}
		return order.getStatus().getOrderStatus();
	}
}
