package com.clifton.trade.order.allocation;

public class TradeOrderAllocationStatusBuilder {

	private TradeOrderAllocationStatus tradeOrderAllocationStatus;


	private TradeOrderAllocationStatusBuilder(TradeOrderAllocationStatus tradeOrderAllocationStatus) {
		this.tradeOrderAllocationStatus = tradeOrderAllocationStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TradeOrderAllocationStatusBuilder createCompleteNoAllocationReport() {
		TradeOrderAllocationStatus tradeOrderAllocationStatus = new TradeOrderAllocationStatus();

		tradeOrderAllocationStatus.setId((short) 6);
		tradeOrderAllocationStatus.setName("Allocation Complete without Allocation Report");
		tradeOrderAllocationStatus.setAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);
		tradeOrderAllocationStatus.setAllocationComplete(true);

		return new TradeOrderAllocationStatusBuilder(tradeOrderAllocationStatus);
	}


	public static TradeOrderAllocationStatusBuilder createRejectedAccountLevel() {
		TradeOrderAllocationStatus tradeOrderAllocationStatus = new TradeOrderAllocationStatus();

		tradeOrderAllocationStatus.setId((short) 2);
		tradeOrderAllocationStatus.setName("Allocation Account Level Reject");
		tradeOrderAllocationStatus.setAllocationStatus(TradeOrderAllocationStatuses.ACCOUNT_LEVEL_REJECT);
		tradeOrderAllocationStatus.setRejected(true);

		return new TradeOrderAllocationStatusBuilder(tradeOrderAllocationStatus);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderAllocationStatusBuilder withId(short id) {
		getTradeOrderAllocationStatus().setId(id);
		return this;
	}


	public TradeOrderAllocationStatus toTradeOrderAllocationStatus() {
		return this.tradeOrderAllocationStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrderAllocationStatus getTradeOrderAllocationStatus() {
		return this.tradeOrderAllocationStatus;
	}
}
