Clifton.trade.order.TradeOrderStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Order Status',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'tradeOrderStatus.json',
		labelWidth: 125,
		readOnly: true,
		items: [
			{fieldLabel: 'Order Status', name: 'orderStatus', xtype: 'displayfield'},
			{fieldLabel: 'Status Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'Cancel If Not Filled', name: 'cancelIfNotFilled', xtype: 'checkbox'},
			{boxLabel: 'Partial Fill', name: 'partialFill', xtype: 'checkbox'},
			{boxLabel: 'Fill Complete', name: 'fillComplete', xtype: 'checkbox'}
		]
	}]
});
