Clifton.trade.order.TradeOrderWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'tradeOrder.json?requestedMaxDepth=5',

	getClassName: function(config, entity) {
		if (entity.destinationMapping
			&& entity.destinationMapping.tradeDestination
			&& entity.destinationMapping.tradeDestination.type
			&& !entity.destinationMapping.tradeDestination.type.fixTradeOnly
			&& entity.destinationMapping.tradeDestination.type.manual === true) {

			return entity.multipleSecurityOrder ? 'Clifton.trade.order.ManualMultipleSecuritiesTradeOrderWindow' : 'Clifton.trade.order.ManualTradeOrderWindow';
		}
		return Clifton.trade.order.GetTradeOrderFixWindowClassName();
	}
});
