Clifton.trade.order.ManualTradeOrderWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'manualTradeOrderWindow',
	titlePrefix: 'Trade Order',
	iconCls: 'shopping-cart-blue',
	width: 1100,
	height: 600,


	tbar: [{
		text: 'Actions',
		iconCls: 'run',
		menu: {
			xtype: 'menu',
			items: [{
				text: 'Book with Average Price',
				tooltip: 'Book child trades with the average price from order.',
				iconCls: 'run',
				handler: function() {
					const gridPanel = this;
					const win = gridPanel.getWindow();
					const tradeOrderList = [];
					tradeOrderList[0] = win.params.id;
					const loader = new TCG.data.JsonLoader({
						waitMsg: 'Processing Orders',
						waitTarget: this,
						params: {
							tradeOrderList: tradeOrderList
						},
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					loader.load('tradeOrderListProcess.json');
				}
			}]
		}
	}],

	items: [{
		height: 130,
		xtype: 'formpanel',
		url: 'tradeOrder.json',
		loadDefaultDataAfterRender: true,
		labelFieldName: 'id',

		items: [
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .4,
						layout: 'form',
						items: [
							{
								fieldLabel: 'Buy/Sell', name: 'buy', xtype: 'displayfield', width: 125,
								setRawValue: function(v) {
									this.el.addClass(v ? 'buy' : 'sell');
									TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
								}
							},
							{fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield', width: 129, readOnly: true, allowBlank: false},
							{fieldLabel: 'Security', name: 'investmentSecurity.label', xtype: 'linkfield', detailIdField: 'investmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
							//{fieldLabel: 'Executing Broker', name: 'destinationMapping.executingBrokerCompany.name', xtype: 'linkfield', detailIdField: 'destinationMapping.executingBrokerCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
							{
								fieldLabel: 'Executing Broker', name: 'destinationMapping.executingBrokerCompany.label', width: 250,
								hiddenName: 'destinationMapping.id', xtype: 'combo',
								displayField: 'executingBrokerCompany.label',
								queryParam: 'executingBrokerCompanyName',
								detailPageClass: 'Clifton.business.company.CompanyWindow',
								url: 'tradeDestinationMappingListByCommandFind.json',
								valueNotFoundText: '',
								allowBlank: true,
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const fp = combo.getParentForm();
										combo.store.baseParams = {
											tradeDestinationId: fp.getFormValue('destinationMapping.tradeDestination.id'),
											tradeTypeId: fp.getFormValue('tradeType.id'),
											activeOnDate: TCG.parseDate(fp.getFormValue('tradeDate')).format('m/d/Y')
										};
									}
								}
							},
							{fieldLabel: 'Trade Destination', name: 'destinationMapping.tradeDestination.name', xtype: 'displayfield'}
						]
					},
					{
						columnWidth: .23,
						layout: 'form',
						labelWidth: 90,
						items: [
							{fieldLabel: 'Average Price', name: 'averagePrice', width: 120, xtype: 'floatfield', allowBlank: true},
							{fieldLabel: 'Quantity Filled', name: 'quantityFilled', width: 120, xtype: 'floatfield', allowBlank: true},
							{xtype: 'hidden', name: 'commissionPerUnit'}
						]
					},
					{
						columnWidth: .37,
						layout: 'form',
						labelWidth: 110,
						items: [
							{fieldLabel: 'Order Type', name: 'type.name', xtype: 'displayfield'},
							{fieldLabel: 'Order Status', name: 'status.name', xtype: 'linkfield', detailIdField: 'status.id', detailPageClass: 'Clifton.trade.order.TradeOrderStatusWindow'},
							{fieldLabel: 'Allocation Status', name: 'allocationStatus.name', xtype: 'linkfield', detailIdField: 'allocationStatus.id', detailPageClass: 'Clifton.trade.order.TradeOrderAllocationStatusWindow'},
							{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
							{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
						]
					}
				]
			},

			{
				xtype: 'formgrid',
				title: 'Trades',
				storeRoot: 'tradeList',
				dtoClass: 'com.clifton.trade.Trade',
				readOnly: true,
				addToolbarButtons: function(toolBar) {
					const gp = this;
					toolBar.add({
						text: 'Reload',
						tooltip: 'Reload trades for this trade group.',
						iconCls: 'table-refresh',
						scope: this,
						handler: function() {
							gp.reload();
						}
					});

					toolBar.addFill();
					toolBar.add({xtype: 'label', html: 'Commission Per Unit:&nbsp;'});
					toolBar.add({xtype: 'pricefield', name: 'commissionPerUnitToApply', width: 70});

					toolBar.add({
						text: 'Apply',
						iconCls: 'add',
						tooltip: 'Apply entered Commission Per Unit value to each Trade.',
						scope: toolBar,
						handler: function() {
							if (gp.submitField.getValue() === '[]') {
								TCG.showError('No rolls selected.  Please select at least one roll to fill.');
								return;
							}
							const commissionPerUnit = TCG.getChildByName(toolBar, 'commissionPerUnitToApply').getValue();
							if (TCG.isBlank(commissionPerUnit)) {
								TCG.showError('Commission Per Unit must be populated.', 'Validation');
								return;
							}
							const panel = TCG.getParentFormPanel(this);
							const form = panel.getForm();
							form.findField('commissionPerUnit').setValue(commissionPerUnit);
							gp.getWindow().saveWindow(false);
						}
					});
				},
				columnsConfig: [
					{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', width: 220},
					{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', width: 240},
					{header: 'Quantity', dataIndex: 'quantityIntended', width: 80, type: 'float', editor: {xtype: 'numberfield', allowBlank: false}, summaryType: 'sum'},
					{header: 'Average Unit Price', dataIndex: 'averageUnitPrice', width: 80, type: 'currency', editor: {xtype: 'numberfield', allowBlank: false}, useNull: true},
					{header: 'Unit Price', dataIndex: 'expectedUnitPrice', width: 80, type: 'currency', useNull: true},
					{header: 'Commission Per Unit', dataIndex: 'commissionPerUnit', width: 90, type: 'currency', editor: {xtype: 'numberfield', allowBlank: true}, useNull: true},
					{header: 'Fee', dataIndex: 'feeAmount', width: 60, type: 'currency', useNull: true, summaryType: 'sum'},
					{header: 'Accounting Notional', width: 120, dataIndex: 'accountingNotional', useNull: true, type: 'currency', summaryType: 'sum'}
				],
				plugins: {ptype: 'gridsummary'}
			}
		]
	}]
});
