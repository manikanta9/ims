Clifton.trade.order.TradeOrderAdminWindow = Ext.extend(TCG.app.Window, {
	id: 'tradeOrderAdminWindow',
	title: 'Trade Order Administration',
	iconCls: 'shopping-cart',
	width: 1150,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Orders',
				items: [{
					name: 'tradeOrderListFind',
					xtype: 'gridpanel',
					rowSelectionModel: 'checkbox',
					instructions: 'A list of all orders executed via FIX. A single FIX Order maybe for one or multiple trades.',
					border: 1,
					flex: 1,
					additionalPropertiesToRequest: 'id|destinationMapping.tradeDestination.connection',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Order Status', width: 30, dataIndex: 'status.orderStatus', filter: {type: 'list', options: Clifton.trade.order.TradeOrderStatuses}},
						{header: 'Allocation Status', width: 30, dataIndex: 'status.allocationStatus', filter: {type: 'list', searchFieldName: 'allocationStatusList', sortFieldName: 'allocationStatus', options: Clifton.trade.order.TradeOrderAllocationStatuses}},
						{
							header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{header: 'Qty', width: 10, dataIndex: 'quantity', type: 'float', useNull: true},
						{header: 'Qty Filled', width: 20, dataIndex: 'quantityFilled', type: 'float', useNull: true},
						{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Average Price', width: 25, dataIndex: 'averagePrice', type: 'float', useNull: true},
						{header: 'Trader', width: 15, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
						{header: 'Traded On', width: 20, dataIndex: 'tradeDate', searchFieldName: 'tradeDate', defaultSortDirection: 'DESC'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -1)});
						}
						return {
							requestedMaxDepth: 3
						};
					},
					editor: {
						detailPageClass: 'Clifton.trade.order.TradeOrderWindow',
						drillDownOnly: true,
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								sessionParameters: row.json.destinationMapping.tradeDestination.connection,
								tradeOrderId: row.id
							};
						},
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Actions',
								iconCls: 'run',
								menu: new Ext.menu.Menu({
									items: [{
										text: 'Allocate',
										tooltip: 'Manually allocate a partially filled and canceled trade.',
										iconCls: 'run',
										scope: this.getGridPanel(),
										handler: function() {
											const sm = gridPanel.grid.getSelectionModel();
											const orders = sm.getSelections();
											if (sm.getCount() !== 1) {
												TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
											}
											else if ((orders[0].json.status.orderStatus !== 'PARTIALLY_FILLED_CANCELED') && (orders[0].json.status.orderStatus !== 'PARTIALLY_FILLED_DONE_FOR_DAY')) {
												TCG.showError('Orders in this state can only be edited if the fill status is \'PARTIALLY_FILLED_CANCELED\'.', 'Incorrect Selection');
											}
											else {
												const className = 'Clifton.trade.order.TradeOrderWindow';
												const id = orders[0].id;
												const params = id ? {id: id} : undefined;
												const cmpId = TCG.getComponentId(className, id);
												TCG.createComponent(className, {
													id: cmpId,
													params: params,
													openerCt: gridPanel,
													defaultIconCls: gridPanel.getWindow().iconCls
												});
											}
										}
									},

										{
											text: 'Process Selected Orders',
											tooltip: 'Force FIX order processing to move orders to the appropriate state(s).',
											iconCls: 'run',
											scope: this.getGridPanel(),
											handler: function() {
												const gridPanel = this;
												const sm = this.grid.getSelectionModel();
												if (sm.getCount() === 0) {
													TCG.showError('Please select at least one order to be processed.', 'No Order(s) Selected');
												}
												else {
													const orders = sm.getSelections();
													const tradeOrderList = [];
													for (let i = 0; i < orders.length; i++) {
														tradeOrderList[i] = orders[i].id;
													}
													const loader = new TCG.data.JsonLoader({
														waitMsg: 'Processing Orders',
														waitTarget: this,
														params: {
															tradeOrderList: tradeOrderList
														},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('tradeOrderListProcess.json');
												}
											}
										},

										{
											text: 'Resend Order',
											iconCls: 'run',
											scope: this.getGridPanel(),
											handler: function() {
												const gridPanel = this;
												const sm = this.grid.getSelectionModel();
												if (sm.getCount() !== 1) {
													TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
												}
												else {
													const orders = sm.getSelections();
													const loader = new TCG.data.JsonLoader({
														waitMsg: 'Resending Order',
														waitTarget: this,
														params: {tradeOrderId: orders[0].id},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('tradeOrderFixResend.json');
												}
											}
										},

										{
											text: 'Cancel Selected Order',
											tooltip: 'Send FIX message to cancel the active order.',
											iconCls: 'run',
											scope: this.getGridPanel(),
											handler: function() {
												const gridPanel = this;
												const sm = this.grid.getSelectionModel();
												if (sm.getCount() !== 1) {
													TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
												}
												else {
													const orders = sm.getSelections();
													const loader = new TCG.data.JsonLoader({
														waitMsg: 'Canceling Order',
														waitTarget: this,
														params: {tradeOrderId: orders[0].id},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('tradeOrderFixCancelRequestSend.json');
												}
											}
										}]
								})
							});
						}
					}
				}]
			},


			{
				title: 'Messages',
				items: [{
					xtype: 'fix-messageGrid'
				}]
			},


			{
				title: 'Error Queue',
				items: [{
					xtype: 'fix-messageErrorGrid'
				}]
			}
		]
	}]
});
