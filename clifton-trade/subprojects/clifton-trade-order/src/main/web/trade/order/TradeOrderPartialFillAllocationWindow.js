Clifton.trade.order.TradeOrderPartialFillAllocationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'FIX Trade Order Allocation',
	width: 1000,
	height: 550,
	iconCls: 'shopping-cart',
	forceModified: true,
	doNotWarnOnCloseModified: true,
	hideApplyButton: true,
	saveTimeout: 180,

	items: [{
		xtype: 'formpanel',
		instructions: 'Automatically updates the quantities of the trades to match the amount filled before execution was cancelled.',
		url: 'tradeOrder.json?requestedMaxDepth=5',

		getSaveURL: function() {
			return 'tradeOrderFixAllocationInstructionPartiallyFilledSend.json?requestedMaxDepth=2';
		},
		updateTitle: function() {
			const w = this.getWindow();
			const suffix = 'Order: ' + this.getFormValue('id');
			w.setTitle(suffix);
		},
		items: [
			{fieldLabel: 'Quantity Filled', name: 'quantityFilled', allowBlank: false, readOnly: true},
			{
				xtype: 'formgrid',
				title: 'Order Trades',
				storeRoot: 'tradeList',
				dtoClass: 'com.clifton.trade.Trade',
				columnsConfig: [
					{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', width: 200},
					{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', width: 200},
					{header: 'Quantity', dataIndex: 'quantityIntended', width: 70, summaryType: 'sum', useNull: true},
					{header: 'Original Face', dataIndex: 'originalFace', width: 70, summaryType: 'sum', useNull: true},
					{header: 'Unit Price', dataIndex: 'expectedUnitPrice', width: 70, type: 'currency', useNull: true},
					{header: 'Commission Per Unit', dataIndex: 'commissionPerUnit', width: 80, type: 'currency', useNull: true},
					{header: 'Fee', dataIndex: 'feeAmount', width: 60, type: 'currency', useNull: true, summaryType: 'sum'},
					{header: 'Accounting Notional', width: 100, dataIndex: 'accountingNotional', useNull: true, type: 'currency', summaryType: 'sum'}
				],
				plugins: {ptype: 'gridsummary'},
				addToolbarButtons: function(toolbar, grid) {
					// Remove existing "Add" and "Remove" buttons
					toolbar.items.clear();

					toolbar.add({
						text: 'Calculate Quantities',
						tooltip: 'Calculates new proportionally reduced quantities based on available fill quantity and the original trade quantity.',
						iconCls: 'run',
						scope: this,
						handler: async function() {
							const grid = this;
							const store = grid.getStore();
							const fp = TCG.getParentFormPanel(this);
							const tradeOrderId = fp.getFormValue('id');
							fp.gridToolbar = toolbar;

							const url = 'tradeOrderTradeProportionalQuantityCalculate.json?enableOpenSessionInView=true&tradeOrderId=' + tradeOrderId + '&requestedPropertiesRoot=data&requestedProperties=' +
								'id|quantityIntended|originalFace|accountingNotional';
							const tradeList = await TCG.data.getDataPromise(url, this);
							const tradeMap = new Map();
							let hasZeroQtyTrades = false;
							tradeList.forEach(trade => tradeMap.set(trade.id, trade));

							// apply proportional quantities
							store.data.items.forEach(dataRow => {
								const trade = tradeMap.get(dataRow.id);
								dataRow.beginEdit();
								if (TCG.isNotBlank(trade.accountingNotional)) {
									dataRow.set('accountingNotional', trade.accountingNotional);
								}
								if (TCG.isNotBlank(trade.quantityIntended)) {
									dataRow.set('quantityIntended', trade.quantityIntended);
									if (TCG.isFalse(hasZeroQtyTrades) && trade.quantityIntended === 0.0) {
										hasZeroQtyTrades = true;
									}
								}
								if (TCG.isNotBlank(trade.originalFace)) {
									dataRow.set('originalFace', trade.originalFace);
								}
								dataRow.endEdit();
							});
							grid.markModified(true);

							if (TCG.isTrue(hasZeroQtyTrades)) {
								TCG.showInfo('The trade quantity reduction resulted in one or more trades with a zero quantity. These trades will automatically be cancelled.');
							}
						}
					});
				}
			}
		]
	}]
});
