Clifton.trade.order.ManualMultipleSecuritiesTradeOrderWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'manualTradeOrderWindow',
	titlePrefix: 'Trade Order',
	iconCls: 'shopping-cart-blue',
	width: 1100,
	height: 600,

	tbar: [{
		text: 'Actions',
		iconCls: 'run',
		menu: {
			xtype: 'menu',
			items: [{
				text: 'Book with Average Price',
				tooltip: 'Book child trades with the average price from order.',
				iconCls: 'run',
				handler: function() {
					const win = this.getWindow();
					const tradeOrderList = [];
					tradeOrderList[0] = win.params.id;
					const loader = new TCG.data.JsonLoader({
						waitMsg: 'Processing Orders',
						waitTarget: this,
						params: {
							tradeOrderList: tradeOrderList
						},
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					loader.load('tradeOrderListProcess.json');
				}
			}]
		}
	}],

	items: [{
		height: 130,
		xtype: 'formpanel',
		url: 'tradeOrder.json',
		loadDefaultDataAfterRender: true,
		labelFieldName: 'id',

		items: [
			{
				xtype: 'panel',
				layout: 'column',
				items: [
					{
						columnWidth: .5,
						layout: 'form',
						items: [
							{
								fieldLabel: 'Buy/Sell', name: 'buy', xtype: 'displayfield', width: 125,
								setRawValue: function(v) {
									this.el.addClass(v ? 'buy' : 'sell');
									TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
								}
							},
							{fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield', width: 129, readOnly: true, allowBlank: false},
							{fieldLabel: 'Quantity Filled', name: 'quantityFilled', xtype: 'floatfield', width: 129, allowBlank: true},
							//{fieldLabel: 'Executing Broker', name: 'destinationMapping.executingBrokerCompany.name', xtype: 'linkfield', detailIdField: 'destinationMapping.executingBrokerCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
							{
								fieldLabel: 'Executing Broker', name: 'destinationMapping.executingBrokerCompany.label', width: 250,
								hiddenName: 'destinationMapping.id', xtype: 'combo',
								displayField: 'executingBrokerCompany.label',
								queryParam: 'executingBrokerCompanyName',
								detailPageClass: 'Clifton.business.company.CompanyWindow',
								url: 'tradeDestinationMappingListByCommandFind.json',
								valueNotFoundText: '',
								allowBlank: true,
								listeners: {
									beforequery: function(queryEvent) {
										const combo = queryEvent.combo;
										const fp = combo.getParentForm();
										combo.store.baseParams = {
											tradeDestinationId: fp.getFormValue('destinationMapping.tradeDestination.id'),
											tradeTypeId: fp.getFormValue('tradeType.id'),
											activeOnDate: TCG.parseDate(fp.getFormValue('tradeDate')).format('m/d/Y')
										};
									}
								}
							},
							{fieldLabel: 'Trade Destination', name: 'destinationMapping.tradeDestination.name', xtype: 'displayfield', detailIdField: 'destinationMapping.tradeDestination.id', detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow'}
						]
					},
					{
						columnWidth: .5,
						layout: 'form',
						labelWidth: 110,
						items: [
							{fieldLabel: 'Order Type', name: 'type.name', xtype: 'displayfield'},
							{fieldLabel: 'Order Status', name: 'status.name', xtype: 'linkfield', detailIdField: 'status.id', detailPageClass: 'Clifton.trade.order.TradeOrderStatusWindow'},
							{fieldLabel: 'Allocation Status', name: 'allocationStatus.name', xtype: 'linkfield', detailIdField: 'allocationStatus.id', detailPageClass: 'Clifton.trade.order.TradeOrderAllocationStatusWindow'},
							{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
							{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
						]
					}
				]
			},

			{
				xtype: 'formgrid',
				title: 'Order Securities',
				storeRoot: 'securityList',
				dtoClass: 'com.clifton.trade.order.TradeOrderSecurity',
				readOnly: true,
				addToolbarButtons: function(toolBar) {
					//
				},
				columnsConfig: [
					{header: 'Security', dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', width: 200},
					{header: 'Average Price', dataIndex: 'averagePrice', width: 100, type: 'currency', editor: {xtype: 'numberfield', allowBlank: false}, useNull: true},
					{header: 'Commission', dataIndex: 'commissionPerUnit', width: 100, type: 'currency', editor: {xtype: 'numberfield', allowBlank: false}, useNull: true}
				]
			},

			{
				xtype: 'formgrid',
				title: 'Order Trades',
				storeRoot: 'tradeList',
				dtoClass: 'com.clifton.trade.Trade',
				readOnly: true,
				addToolbarButtons: function(toolBar) {
					//
				},
				columnsConfig: [
					{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', idDataIndex: 'clientInvestmentAccount.id', width: 200},
					{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.label', idDataIndex: 'holdingInvestmentAccount.id', width: 200},
					{header: 'Quantity', dataIndex: 'quantityIntended', width: 70, type: 'float', editor: {xtype: 'numberfield', allowBlank: false}, summaryType: 'sum'},
					{header: 'Security', dataIndex: 'investmentSecurity.symbol', idDataIndex: 'investmentSecurity.id', width: 120},
					{header: 'Average Unit Price', dataIndex: 'averageUnitPrice', width: 70, type: 'currency', useNull: true},
					{header: 'Expected Unit Price', dataIndex: 'expectedUnitPrice', width: 70, type: 'currency', useNull: true},
					{header: 'Commission Per Unit', dataIndex: 'commissionPerUnit', width: 80, type: 'currency', useNull: true},
					{header: 'Fee', dataIndex: 'feeAmount', width: 60, type: 'currency', useNull: true, summaryType: 'sum'},
					{header: 'Accounting Notional', width: 100, dataIndex: 'accountingNotional', useNull: true, type: 'currency', summaryType: 'sum'}
				],
				plugins: {ptype: 'gridsummary'},
				editor: {
					detailPageClass: 'Clifton.trade.TradeWindow',
					drillDownOnly: true
				}
			}
		]
	}]
});
