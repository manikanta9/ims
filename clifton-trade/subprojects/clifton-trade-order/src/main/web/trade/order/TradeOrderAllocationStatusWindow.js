Clifton.trade.order.TradeOrderAllocationStatusWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Order Status',
	iconCls: 'shopping-cart',

	items: [{
		xtype: 'formpanel',
		url: 'tradeOrderAllocationStatus.json',
		labelWidth: 125,
		readOnly: true,
		items: [
			{fieldLabel: 'Allocation Status', name: 'allocationStatus', xtype: 'displayfield'},
			{fieldLabel: 'Allocation Status Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'Allocation Fill Received', name: 'allocationFillReceived', xtype: 'checkbox'},
			{boxLabel: 'Allocation Complete', name: 'allocationComplete', xtype: 'checkbox'},
			{boxLabel: 'Allocation Rejected', name: 'rejected', xtype: 'checkbox'}
		]
	}]
});
