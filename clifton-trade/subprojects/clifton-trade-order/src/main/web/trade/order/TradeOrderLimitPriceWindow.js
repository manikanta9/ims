Clifton.trade.order.TradeOrderLimitPriceWindow = Ext.extend(TCG.app.Window, {
	title: 'Apply Limit Prices',
	iconCls: 'add',
	height: 400,
	width: 1200,

	items: [
		{
			name: 'tradeOrderListFind',
			xtype: 'editorgrid',
			rowSelectionModel: 'checkbox',
			title: 'Draft FIX Orders Awaiting Limit Prices',
			instructions: 'Draft orders that require a limit price before execution.',
			wikiPage: 'IT/FIX Trouble Shooting',
			appendStandardColumns: false,
			updateRecord: function(store, record, operation) {
				if (operation === Ext.data.Record.EDIT) {
					const loader = new TCG.data.JsonLoader({
						waitTarget: this,
						waitMsg: 'Saving...',
						params: {tradeOrderId: record.data.id, limitPrice: record.data.limitPrice},
						conf: {rowId: record.id},
						onLoad: function(record, conf) {
							store.commitChanges();
						},
						failure: function() {
							store.rejectChanges();
							if (this.isUseWaitMsg()) {
								this.getMsgTarget().unmask();
							}
							TCG.showError('Error sending server request to:<br /><br />' + gridPanel.getSaveURL() + '<br /><br />Response status: ' + response.statusText, 'Server Error');
						}
					});
					loader.load('tradeOrderLimitPriceUpdate.json');
				}
			},
			getTopToolbarFilters: function(toolbar) {
				return [
					{fieldLabel: 'Team', name: 'teamSecurityGroup.id', width: 150, minListWidth: 150, xtype: 'combo', url: 'securityGroupTeamList.json', loadAll: true}
				];
			},
			addFirstToolbarButtons: function(t, gridPanel) {
				t.add(
					{
						text: 'Execute as Limit Orders',
						tooltip: 'Execute selected orders as Limit Orders',
						iconCls: 'shopping-cart-blue',
						scope: gridPanel,
						handler: function(boolean) {
							gridPanel.executeOrders(true);
						}
					},
					'-',
					{
						text: 'Execute as Market Orders',
						tooltip: 'Execute selected orders as Market Orders, ignoring the Limit Price',
						iconCls: 'shopping-cart-blue',
						scope: gridPanel,
						handler: function(boolean) {
							gridPanel.executeOrders(false);
						}
					},
					'-',
					{
						xtype: 'button', name: 'bloombergPricingButton', iconCls: 'bloomberg', text: 'Update Prices', width: 25, tooltip: 'Click to load price from Bloomberg for the securities listed below.',
						scope: this,
						handler: this.updateMarketPrices
					},
					'-'
				);
			},
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					const trader = TCG.getCurrentUser();
					this.setFilterValue('traderUser.label', {value: trader.id, text: trader.label});
				}
				const params = this.getStandardLoadParams();
				params['orderStatus'] = 'DRAFT';
				return this.applyTopToolbarFilterLoadParams(this, params);
			},
			getStandardLoadParams: function() {
				const t = this.getTopToolbar();
				const teamSecurityGroupValue = TCG.getChildByName(t, 'teamSecurityGroup.id').getValue();

				return {
					tradeWorkflowStatusName: 'Active',
					tradeWorkflowStateName: 'Execution',
					activeOrder: true,
					teamSecurityGroupId: teamSecurityGroupValue && TCG.isNotNull(teamSecurityGroupValue) ? teamSecurityGroupValue : undefined,
					requestedMaxDepth: 6,
					readUncommittedRequested: true
				};
			},
			columns: [
				{header: 'Order ID', width: 12, dataIndex: 'id'},
				{
					header: 'Executing Broker',
					width: 30,
					dataIndex: 'destinationMapping.executingBrokerCompany.name',
					filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}
				},
				{header: 'Order Type', width: 24, dataIndex: 'type.name', filter: {type: 'list', searchFieldName: 'tradeOrderTypeNameList', options: Clifton.trade.order.TradeOrderTypeNames}, hidden: true},
				{header: 'Order Status', width: 24, dataIndex: 'status.orderStatus', filter: {type: 'list', options: Clifton.trade.order.TradeOrderStatuses}, hidden: true},
				{
					header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
					renderer: function(v, metaData) {
						metaData.css = v ? 'buy-light' : 'sell-light';
						return v ? 'BUY' : 'SELL';
					}
				},
				{header: 'Quantity', width: 15, dataIndex: 'quantity', type: 'float', useNull: true},
				{header: 'InvestmentSecurity ID', width: 12, dataIndex: 'investmentSecurity.id', hidden: true},
				{
					header: 'Security',
					width: 40,
					dataIndex: 'investmentSecurity.symbol',
					filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}
				},
				{header: 'Bid Price', width: 15, dataIndex: 'bidPrice', type: 'float', useNull: true},
				{header: 'Mid Price', width: 15, dataIndex: 'midPrice', type: 'float', useNull: true},
				{header: 'Ask Price', width: 15, dataIndex: 'askPrice', type: 'float', useNull: true},
				{
					header: 'Limit Price', width: 15, dataIndex: 'limitPrice', type: 'float', useNull: true,
					editor: {
						xtype: Ext.editor,
						allowBlank: false,
						minValue: 0.01,
						stateful: false
					}
				},
				{
					header: 'Trader',
					width: 15,
					dataIndex: 'traderUser.label',
					filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}
				},
				{header: 'Destination', width: 30, dataIndex: 'destinationMapping.tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}}
			],
			editor: {
				detailPageClass: 'Clifton.trade.order.TradeOrderWindow',
				drillDownOnly: true,
				getDefaultData: function(gridPanel, row) {
					return {
						externalIdentifier: row.json.externalIdentifier,
						tradeOrderStatus: row.json.status
					};
				}
			},
			applyTopToolbarFilterLoadParams: function(gridPanel, params) {
				const t = gridPanel.getTopToolbar();
				let grp = TCG.getChildByName(t, 'clientGroupName');
				if (grp && !TCG.isBlank(grp.getValue())) {
					params.clientInvestmentAccountGroupId = grp.getValue();
				}
				grp = TCG.getChildByName(t, 'investmentGroupId');
				if (grp && !TCG.isBlank(grp.getValue())) {
					params.investmentGroupId = grp.getValue();
				}
				return params;
			},
			executeOrders: function(execAsLimitOrder) {
				const gridPanel = this;
				const rowSelectionModel = this.getRowSelectionModel();

				if (rowSelectionModel.getCount() === 0) {
					TCG.showError('Please select one or more orders to execute.', 'No Orders Selected');
					return;
				}

				const tradeOrderIdArray = [];
				const selectedTradeOrders = rowSelectionModel.getSelections();
				for (let index = 0; index < selectedTradeOrders.length; index++) {
					tradeOrderIdArray[index] = selectedTradeOrders[index].data.id;
				}

				const loader = new TCG.data.JsonLoader({
					waitMsg: 'Executing trade orders',
					waitTarget: this,
					params: {
						tradeOrderIdList: tradeOrderIdArray,
						executeAsLimitOrder: execAsLimitOrder
					},
					onLoad: function(record, conf) {
						gridPanel.reload();
					}
				});
				loader.load('tradeOrderListExecute.json');
			},
			listeners: {
				afterrender: function() {
					const gridPanel = this;
					gridPanel.grid.store.on('load', gridPanel.updateMarketPrices, gridPanel);
				}
			},
			updateMarketPrices: function() {
				const gridPanel = this;
				const bloombergButton = TCG.getChildByName(gridPanel.toolbars[0], 'bloombergPricingButton', true, 2);
				// Map rows to a security id
				gridPanel.securityIdToRowsMap = {};
				gridPanel.grid.store.each(
					function(record) {
						const securityId = TCG.getValue('investmentSecurity.id', record.json);
						if (TCG.isNull(gridPanel.securityIdToRowsMap[securityId])) {
							gridPanel.securityIdToRowsMap[securityId] = [];
						}
						gridPanel.securityIdToRowsMap[securityId].push(record);
					}
				);

				// lookup the ask, bid, and mid prices for the rows
				const priceLookupFunction = function(securityId) {
					Clifton.marketdata.field.getSecurityLiveMarketDataValue(securityId, 'Ask Price', {maxAgeInSeconds: 0})
						.then(function(price) {
							gridPanel.updateGridRowsWithMarketPrices('askPrice', price);
							return Clifton.marketdata.field.getSecurityLiveMarketDataValue(securityId, 'Bid Price', {maxAgeInSeconds: 0});
						}).then(function(price) {
						gridPanel.updateGridRowsWithMarketPrices('bidPrice', price);
						return Clifton.marketdata.field.getSecurityLiveMarketDataValue(securityId, 'Mid Price', {maxAgeInSeconds: 0});
					}).then(function(price) {
						gridPanel.updateGridRowsWithMarketPrices('midPrice', price);
						bloombergButton.enable();
					});
				};

				const keyArray = Object.keys(gridPanel.securityIdToRowsMap);
				if (keyArray.length > 0) {
					bloombergButton.disable();
				}
				for (let index = 0; index < keyArray.length; index++) {
					const securityId = keyArray[index];
					priceLookupFunction(securityId);
				}
			},
			updateGridRowsWithMarketPrices: function(fieldName, price) {
				if (TCG.isNull(price)) {
					return;
				}
				const gridPanel = this;
				const recordArray = gridPanel.securityIdToRowsMap[price.securityId];
				for (let index = 0; index < recordArray.length; index++) {
					const record = recordArray[index];
					record.beginEdit();
					if (TCG.isBlank(price.value)) {
						record.set(fieldName, null);
					}
					else {
						record.set(fieldName, price.value);
					}
					record.endEdit();
				}
			}
		}
	]
});




