Ext.ns('Clifton.trade.order');


// Overridden in Trade Order Fix Shared
Clifton.trade.order.GetTradeOrderFixWindowClassName = function() {
	return 'Clifton.trade.order.ManualTradeOrderWindow';
};


Clifton.trade.order.TradeOrderStatuses = [
	['CANCELED', 'CANCELED'],
	['DONE_FOR_DAY', 'DONE_FOR_DAY'],
	['FILLED', 'FILLED'],
	['NEW', 'NEW'],
	['PARTIALLY_FILLED', 'PARTIALLY_FILLED'],
	['PARTIALLY_FILLED_CANCELED', 'PARTIALLY_FILLED_CANCELED'],
	['PARTIALLY_FILLED_DONE_FOR_DAY', 'PARTIALLY_FILLED_DONE_FOR_DAY'],
	['REJECTED', 'REJECTED'],
	['REPLACED', 'REPLACED'],
	['SENT', 'SENT']
];

Clifton.trade.order.TradeOrderAllocationStatuses = [
	['ACCEPTED', 'ACCEPTED'],
	['ACCOUNT_LEVEL_REJECT', 'ACCOUNT_LEVEL_REJECT'],
	['ALLOCATION_PENDING', 'ALLOCATION_PENDING'],
	['BLOCK_LEVEL_REJECT', 'BLOCK_LEVEL_REJECT'],
	['CANCELED', 'CANCELED'],
	['COMPLETE', 'COMPLETE'],
	['FAILED_TO_SEND', 'FAILED_TO_SEND'],
	['INCOMPLETE', 'INCOMPLETE'],
	['RECEIVED', 'RECEIVED'],
	['REJECTED_BY_INTERMEDIARY', 'REJECTED_BY_INTERMEDIARY'],
	['REVERSED', 'REVERSED'],
	['SENT', 'SENT']
];


Clifton.trade.order.TradeOrderTypeNames = [
	['DEFAULT', 'DEFAULT'],
	['DEFAULT_BY_HOLDING_ACCOUNT', 'DEFAULT_BY_HOLDING_ACCOUNT'],
	['EXECUTE_SEPARATELY', 'EXECUTE_SEPARATELY'],
	['EXTERNAL_DROP_COPY_ORDER', 'EXTERNAL_DROP_COPY_ORDER'],
	['OPTIONS_STRANGLE', 'OPTIONS_STRANGLE']
];


// Overrides the registration in trade to link it to the order
Ext.override(Clifton.trade.TradeDestinationLink, {
	xtype: 'linkfield',
	fieldLabel: 'Trade Destination',
	name: 'tradeDestination.name',
	detailPageClass: 'Clifton.trade.order.TradeOrderWindow',
	getDetailIdFieldValue: function(formPanel) {
		const createOrder = formPanel.getFormValue('tradeDestination.type.createOrder');
		if (createOrder === true) {
			const orderId = formPanel.getFormValue('orderIdentifier');
			return TCG.isNull(orderId) ? false : orderId;
		}
		TCG.showInfo('Selected Trade Destination does not support orders.', 'Trade Order');
		return false;
	}
});

// Overrides to include trade order actions
Ext.override(Clifton.trade.blotter.ApprovedTradesGridPanel, {
	groupAndExecute: function(groupingType) {
		const gridPanel = this;
		const sm = this.grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select at least one trade to be executed.', 'No Trade(s) Selected');
		}
		else {
			const trades = sm.getSelections();
			const tradeIdList = [];
			for (let i = 0; i < trades.length; i++) {
				tradeIdList[i] = trades[i].id;
			}
			const loader = new TCG.data.JsonLoader({
				waitMsg: 'Grouping and Executing Trades',
				waitTarget: this,
				params: {
					tradeIdList: tradeIdList,
					groupingType: groupingType,
					isSaveAsDraftOnly: false
				},
				onLoad: function(record, conf) {
					gridPanel.reload();
				}
			});
			loader.load('tradeOrderTradeListExecute.json');
		}
	},
	groupAndExecuteWithLimitPrice: function(groupingType) {
		const gridPanel = this;
		const sm = this.grid.getSelectionModel();
		if (sm.getCount() > 0) {
			const trades = sm.getSelections();
			const tradeIdList = [];
			let errorFlag = false;
			for (let i = 0; i < trades.length; i++) {
				// alert user that trades already configured with a TradeExecutionType and will not be processed
				// using this method.  Exclude these from the submission.
				if (TCG.isNotBlank(trades[i].json.tradeExecutionType)) {
					errorFlag = true;
					continue;
				}
				tradeIdList.push(trades[i].id);
			}

			if (TCG.isTrue(errorFlag)) {
				TCG.showError('One or more selected trades have a "Trade Execution Type" setting, and will not be processed. ' +
					'These trades should be sent via "Group and Execute" or "Execute Separately" actions instead.', 'Trades With "Trade Execution Type" Setting Detected');
			}

			if (tradeIdList.length > 0) {
				const loader = new TCG.data.JsonLoader({
					waitMsg: 'Grouping Trades and saving Trade Orders',
					waitTarget: this,
					params: {
						tradeIdList: tradeIdList,
						groupingType: groupingType
					},
					onLoad: function(record, conf) {
						gridPanel.reload();
						TCG.createComponent('Clifton.trade.order.TradeOrderLimitPriceWindow', {
							openerCt: gridPanel
						});
					}
				});
				loader.load('tradeOrderTradeListExecute.json');
			}
		}
	},
	addFirstToolbarButtons: function(t, gridPanel) {
		t.add({
			text: 'Group and Execute',
			tooltip: 'Group selected trades into as few orders as possible (same security, direction) and submit them for execution.',
			iconCls: 'shopping-cart-blue',
			scope: gridPanel,
			handler: function() {
				gridPanel.groupAndExecute('DEFAULT');
			}
		}, '-', {
			text: 'Execute Separately',
			tooltip: 'Create a separate order for each selected trade and submit them for execution.',
			iconCls: 'shopping-cart-blue',
			scope: gridPanel,
			handler: function() {
				gridPanel.groupAndExecute('EXECUTE_SEPARATELY');
			}
		}, '-', {
			text: 'Execute Other',
			iconCls: 'shopping-cart-blue',
			menu: new Ext.menu.Menu({
				items: [{
					text: 'Options Strangle Grouping',
					tooltip: 'Group trades for an Options Strangle trade.',
					iconCls: 'shopping-cart-blue',
					scope: gridPanel,
					handler: function() {
						gridPanel.groupAndExecute('OPTIONS_STRANGLE');
					}
				}, '-', {
					text: 'Group and Execute <b>(By Holding Account)</b>',
					tooltip: 'Group selected trades into as few orders as possible by holding account (same security, direction) and submit them for execution.',
					iconCls: 'shopping-cart-blue',
					scope: gridPanel,
					handler: function() {
						gridPanel.groupAndExecute('DEFAULT_BY_HOLDING_ACCOUNT');
					}
				}, '-', {
					text: 'Group and Execute With Limit Price',
					tooltip: 'Group selected trades into as few orders as possible (same security, direction) and submit them for execution with a limit price.',
					iconCls: 'shopping-cart-blue',
					scope: gridPanel,
					handler: function() {
						gridPanel.groupAndExecuteWithLimitPrice('DEFAULT_LIMIT');
					}
				},
					{
						text: 'Execute Separately With Limit Price',
						tooltip: 'Group selected trades into as few orders as possible (same security, direction) and submit them for execution with a limit price.',
						iconCls: 'shopping-cart-blue',
						scope: gridPanel,
						handler: function() {
							gridPanel.groupAndExecuteWithLimitPrice('EXECUTE_SEPARATELY_LIMIT');
						}
					}
				]
			})
		});
	}
});


Clifton.trade.order.NonFixActiveTradesGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'activeTradeOrderListFind',
	getLoadURL: function() {
		return 'tradeListFind.json';
	},
	xtype: 'gridpanel',
	title: 'Non-FIX Active Trades',
	instructions: 'Active non-FIX trades that are currently being executed: waiting for fills.',
	border: 1,
	flex: 1,
	rowSelectionModel: 'checkbox',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{
			header: 'Workflow Status',
			width: 15,
			hidden: true,
			dataIndex: 'workflowStatus.name',
			filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}
		},
		{
			header: 'Workflow State', width: 15, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, defaultSortColumn: true,
			renderer: function(value, metadata, record) {
				if (value && value === 'Executed Invalid') {
					metadata.css = 'ruleViolation';
				}
				return value;
			}
		},
		{header: 'Trade Type', width: 11, hidden: true, dataIndex: 'tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}},
		{
			header: 'Trade Group',
			width: 11,
			hidden: true,
			dataIndex: 'tradeGroup.id',
			filter: {type: 'combo', searchFieldName: 'tradeGroupId', url: 'tradeGroupListFind.json'}
		},
		{
			header: 'Trade Group Type',
			width: 11,
			hidden: true,
			dataIndex: 'tradeGroup.tradeGroupType.name',
			filter: {type: 'combo', searchFieldName: 'tradeGroupTypeId', url: 'tradeGroupTypeListFind.json'}
		},
		{
			header: 'Client Account',
			width: 40,
			dataIndex: 'clientInvestmentAccount.label',
			filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}
		},
		{
			header: 'Holding Account',
			width: 40,
			dataIndex: 'holdingInvestmentAccount.label',
			filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}
		},
		{
			header: 'Buy/Sell', width: 9, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
		{header: 'Security', width: 25, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', width: 25, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Exchange Rate', width: 12, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
		{header: 'Avg Price', width: 15, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
		{header: 'Accounting Notional', width: 17, dataIndex: 'accountingNotional', type: 'currency'},
		{header: 'Accrual 1', width: 15, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual 2', width: 15, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
		{header: 'Accrual', width: 15, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
		{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
		{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
		{
			header: 'Team',
			width: 15,
			dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name',
			filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true},
			hidden: true
		},
		{
			header: 'Trader',
			width: 15,
			dataIndex: 'traderUser.label',
			filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}
		},
		{header: 'Execution Type', width: 18, dataIndex: 'tradeExecutionType.name', filter: {searchFieldName: 'tradeExecutionTypeName'}, hidden: true},
		{header: 'Trade Note', width: 50, dataIndex: 'description', renderer: TCG.renderTextNowrapWithTooltip, hidden: true},
		{header: 'Limit Price', width: 15, dataIndex: 'limitPrice', type: 'float', useNull: true, hidden: true},
		{header: 'Traded On', width: 11, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
		{header: 'Settled On', width: 11, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Team', width: 150, minListWidth: 150, xtype: 'combo', url: 'securityGroupTeamList.json', loadAll: true, linkedFilter: 'clientInvestmentAccount.teamSecurityGroup.name'}
		];
	},
	getStandardLoadParams: function() {
		return {
			workflowStatusNameEquals: 'Active',
			workflowStateNames: ['Awaiting Fills', 'Executed Invalid'],
			//excludeTradeGroupTypeName: 'Roll Trade',
			readUncommittedRequested: true
		};
	},
	editor: {
		detailPageClass: 'Clifton.trade.TradeWindow',
		drillDownOnly: true,
		ptype: 'workflowAwareEntity-grideditor',
		executeMultipleInBatch: true,
		// ignore all response data, which is ignored to speed up each row's transition request
		requestedPropertiesToExclude: 'workflowTransitionCommand',
		tableName: 'Trade',
		transitionWorkflowStateList: [
			{stateName: 'Cancelled', iconCls: 'run', buttonText: 'Cancel Selected', buttonTooltip: 'Cancel Selected Trades', requireConfirm: true}
		],
		validateRowsToBeTransitioned: Clifton.trade.validateWorkflowTransition
	},
	configureToolsMenu: function(menu) {
		menu.add({
			text: 'Manual Trade Allocation',
			tooltip: 'View selected trades by executing broker for viewing and sending manual trade allocations',
			iconCls: 'export',
			handler: function() {
				const tradeIdList = [];
				const gp = this.parentMenu.ownerCt.ownerCt.ownerCt;
				const selectionModel = gp.grid.getSelectionModel();
				selectionModel.selections.items.forEach(row => {
					tradeIdList.push(row.get('id'));
				});
				if (tradeIdList.length === 0) {
					TCG.showError('No trades selected. Please select one or more trades from the grid.', 'Data Submission Error');
				}
				else {
					TCG.createComponent('Clifton.trade.TradeAllocationWindow', {defaultData: {tradeIds: tradeIdList}});
				}
			}
		});
	},
	listeners: {
		afterrender: function(fp) {
			const toolbar = fp.toolbars[0];
			if (TCG.isNotBlank(toolbar)) {
				const index = toolbar.items.findIndex('fieldLabel', 'Instrument Group') - 1;
				toolbar.insert(index, '-');
				toolbar.insert(index, {xtype: 'pricefield', name: 'fillPrice', width: 70});
				toolbar.insert(index, {xtype: 'label', html: '&nbsp;Fill Price:&nbsp;', qtip: 'Fill price to apply to selected trades when \'Fill Trades\' is clicked.'});
				toolbar.insert(index, '-');
				toolbar.insertButton(index, {
					text: 'Fill Trades',
					xtype: 'button',
					tooltip: 'Create fills for selected Trades with the specified fill price and order quantity. Selected trades must have the same side and security.',
					iconCls: 'add',
					scope: this,
					handler: function() {
						const fp = this;
						const tradeRows = fp.grid.selModel.selections;
						const fillPriceValue = TCG.getChildByName(fp.toolbars[0], 'fillPrice', false).getValue();

						if (TCG.isBlank(tradeRows) || tradeRows.length === 0) {
							TCG.showError('At least one trade must be selected.', 'Data Entry Error');
							return;
						}

						if (TCG.isBlank(fillPriceValue)) {
							TCG.showError('A fill price is required for the processing of trade fills.', 'Data Entry Error');
							return;
						}

						const tradeList = [];
						tradeRows.each(function(entry) {
							const tradeEntry = {
								id: entry.json.id,
								class: 'com.clifton.trade.Trade'
							};
							tradeList.push(tradeEntry);
						});

						const params = {beanList: JSON.stringify(tradeList), fillPrice: fillPriceValue, actionType: 'FILL_EXECUTE'};

						TCG.data.getDataPromise('tradeListActionCommandExecute.json', fp, {waitMsg: 'Filling Trades', waitTarget: fp, timeout: 180000, params: params})
							.then(statusData => {
								TCG.createComponent('Clifton.core.StatusWindow', {
									title: 'Trade Fill Status',
									defaultData: {status: statusData}
								});
								fp.reload();
							});
					}
				});
			}
		}
	}
});
Ext.reg('tradeOrder-nonFixActiveTradesGrid', Clifton.trade.order.NonFixActiveTradesGrid);
Ext.reg('trade-nonFixActiveTradesGrid', Clifton.trade.order.NonFixActiveTradesGrid); // keep for now to be backward compatible
