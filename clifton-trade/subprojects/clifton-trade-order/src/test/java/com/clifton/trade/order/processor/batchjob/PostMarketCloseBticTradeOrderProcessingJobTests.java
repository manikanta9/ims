package com.clifton.trade.order.processor.batchjob;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatus;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.processor.TradeOrderProcessingService;
import com.clifton.trade.order.search.TradeOrderSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;


/**
 * Tests for the PostMarketCloseBticTradeOrderProcessingJob to verify proper operation.
 *
 * @author davidi
 */
public class PostMarketCloseBticTradeOrderProcessingJobTests {

	static final BigDecimal AVERAGE_PRICE = new BigDecimal("-10.00");
	static final BigDecimal UNDERLYING_PRICE = new BigDecimal("2800.00");

	@Mock
	TradeOrderService tradeOrderService;

	@Mock
	TradeOrderProcessingService tradeOrderProcessingService;

	@Mock
	MarketDataLiveService marketDataLiveService;

	@Mock
	CacheHandler<Object, MarketDataLive> cacheHandler;

	@InjectMocks
	PostMarketCloseBticTradeOrderProcessingJob postMarketCloseBticTradeOrderProcessingJob = new PostMarketCloseBticTradeOrderProcessingJob();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void TestRun_ReferenceSecurityPrice_set() {
		TradeOrder tradeOrder = createTradeOrder();
		TradeOrderStatus finalTradeOrderStatus = new TradeOrderStatus();
		finalTradeOrderStatus.setOrderStatus(TradeOrderStatuses.FILLED);
		MarketDataLive marketDataLive = new MarketDataLive(tradeOrder.getInvestmentSecurity().getUnderlyingSecurity().getId(), UNDERLYING_PRICE, DateUtils.toDate("06/01/2019"));
		Mockito.when(this.tradeOrderService.getTradeOrderList(any(TradeOrderSearchForm.class))).thenReturn(CollectionUtils.createList(tradeOrder));
		Mockito.when(this.tradeOrderService.saveTradeOrder(any(TradeOrder.class))).thenReturn(tradeOrder);
		Mockito.when(this.tradeOrderService.getTradeOrderStatusByStatuses(any(TradeOrderStatuses.class))).thenReturn(finalTradeOrderStatus);
		Mockito.when(this.cacheHandler.get(any(String.class), any(int.class))).thenReturn(marketDataLive);
		Mockito.when(this.marketDataLiveService.getMarketDataLivePrice(any(int.class), any(MarketDataLiveCommand.class))).thenReturn(marketDataLive);

		Map<String, Object> context = new HashMap<>();
		Status status = this.postMarketCloseBticTradeOrderProcessingJob.run(context);

		// Verify no errors
		Assertions.assertEquals(0, status.getErrorCount());

		// Average Price should not be changed
		Assertions.assertEquals(AVERAGE_PRICE, tradeOrder.getAveragePrice());

		// ReferenceSecurityPrice should match underlying price.
		Assertions.assertEquals(UNDERLYING_PRICE, tradeOrder.getReferenceSecurityPrice());
	}


	@Test
	public void TestRun_ReferenceSecurityPrice_no_live_price() {
		TradeOrder tradeOrder = createTradeOrder();
		TradeOrderStatus finalTradeOrderStatus = new TradeOrderStatus();
		finalTradeOrderStatus.setOrderStatus(TradeOrderStatuses.FILLED);
		MarketDataLive marketDataLive = new MarketDataLive(tradeOrder.getInvestmentSecurity().getUnderlyingSecurity().getId(), BigDecimal.ZERO, DateUtils.toDate("06/01/2019"));
		Mockito.when(this.tradeOrderService.getTradeOrderList(any(TradeOrderSearchForm.class))).thenReturn(CollectionUtils.createList(tradeOrder));
		Mockito.when(this.tradeOrderService.saveTradeOrder(any(TradeOrder.class))).thenReturn(tradeOrder);
		Mockito.when(this.tradeOrderService.getTradeOrderStatusByStatuses(any(TradeOrderStatuses.class))).thenReturn(finalTradeOrderStatus);
		Mockito.when(this.cacheHandler.get(any(String.class), any(int.class))).thenReturn(marketDataLive);
		Mockito.when(this.marketDataLiveService.getMarketDataLivePrice(any(int.class), any(MarketDataLiveCommand.class))).thenReturn(null);

		Map<String, Object> context = new HashMap<>();
		Status status = this.postMarketCloseBticTradeOrderProcessingJob.run(context);

		// Verify error: No live price available
		Assertions.assertEquals(1, status.getErrorCount());
		Assertions.assertEquals("No live price available for security: SPX for trade order: 11", status.getDetailList().get(0).getNote());


		// Average Price should not be changed
		Assertions.assertEquals(AVERAGE_PRICE, tradeOrder.getAveragePrice());

		// ReferenceSecurityPrice should be null.
		Assertions.assertNull(tradeOrder.getReferenceSecurityPrice());
	}


	@Test
	public void TestRun_ReferenceSecurityPrice_no_underlying_security() {
		TradeOrder tradeOrder = createTradeOrder();
		TradeOrderStatus finalTradeOrderStatus = new TradeOrderStatus();
		finalTradeOrderStatus.setOrderStatus(TradeOrderStatuses.FILLED);
		MarketDataLive marketDataLive = new MarketDataLive(tradeOrder.getInvestmentSecurity().getUnderlyingSecurity().getId(), UNDERLYING_PRICE, DateUtils.toDate("06/01/2019"));
		Mockito.when(this.tradeOrderService.getTradeOrderList(any(TradeOrderSearchForm.class))).thenReturn(CollectionUtils.createList(tradeOrder));
		Mockito.when(this.tradeOrderService.saveTradeOrder(any(TradeOrder.class))).thenReturn(tradeOrder);
		Mockito.when(this.tradeOrderService.getTradeOrderStatusByStatuses(any(TradeOrderStatuses.class))).thenReturn(finalTradeOrderStatus);
		Mockito.when(this.cacheHandler.get(any(String.class), any(int.class))).thenReturn(marketDataLive);
		Mockito.when(this.marketDataLiveService.getMarketDataLivePrice(any(int.class), any(MarketDataLiveCommand.class))).thenReturn(marketDataLive);

		tradeOrder.getInvestmentSecurity().setUnderlyingSecurity(null);

		Map<String, Object> context = new HashMap<>();
		Status status = this.postMarketCloseBticTradeOrderProcessingJob.run(context);

		// Verify error: No underlying security defined
		//Assertions.assertEquals(1, status.getErrorCount());
		Assertions.assertEquals("No underlying security defined for: ESM9 for trade order: 11", status.getDetailList().get(0).getNote());


		// Average Price should not be changed
		Assertions.assertEquals(AVERAGE_PRICE, tradeOrder.getAveragePrice());

		// ReferenceSecurityPrice should be null.
		Assertions.assertNull(tradeOrder.getReferenceSecurityPrice());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrder createTradeOrder() {
		InvestmentSecurity futureSecurity = new InvestmentSecurity("ESM9", 1000);
		InvestmentSecurity underlyingSecurity = new InvestmentSecurity("SPX", 900);
		futureSecurity.setUnderlyingSecurity(underlyingSecurity);
		TradeOrder tradeOrder = new TradeOrder();
		tradeOrder.setId(11);
		Trade trade = new Trade();
		trade.setId(10);
		trade.setQuantityIntended(BigDecimal.ONE);
		trade.setInvestmentSecurity(futureSecurity);
		tradeOrder.setTradeList(CollectionUtils.createList(trade));
		TradeOrderStatus tradeOrderStatus = new TradeOrderStatus();
		tradeOrderStatus.setOrderStatus(TradeOrderStatuses.HOLD_MARKET_CLOSE);
		tradeOrder.setStatus(tradeOrderStatus);
		tradeOrder.setInvestmentSecurity(futureSecurity);
		tradeOrder.setAveragePrice(AVERAGE_PRICE);

		tradeOrder.setQuantity(BigDecimal.ONE);
		tradeOrder.setQuantityFilled(BigDecimal.ONE);
		tradeOrder.setQuantityFilled(BigDecimal.ONE);

		TradeDestinationMapping mapping = new TradeDestinationMapping();
		TradeDestination tradeDestination = new TradeDestination();
		mapping.setTradeDestination(tradeDestination);
		tradeOrder.setDestinationMapping(mapping);

		return tradeOrder;
	}
}
