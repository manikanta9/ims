package com.clifton.trade.order.adjustment;


import com.clifton.trade.TradeFill;
import com.clifton.trade.order.TradeOrder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class TradeOrderPriceAdjustmentTests {

	private TradeOrderAdjustmentTenDecimalPrice tenDecimalFixOrderPriceAdjustment = new TradeOrderAdjustmentTenDecimalPrice();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTenDecimalFixOrderPriceAdjustment() {
		TradeFill fill = new TradeFill();
		fill.setNotionalUnitPrice(new BigDecimal("1256.7894561").setScale(10, RoundingMode.FLOOR));
		Assertions.assertEquals(new BigDecimal("1256.789456"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("1256.7894566").setScale(7, RoundingMode.FLOOR));
		Assertions.assertEquals(new BigDecimal("1256.789456"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("1.2567894561").setScale(10, RoundingMode.FLOOR));
		Assertions.assertEquals(new BigDecimal("1.256789456"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("12567894561.0000").setScale(4, RoundingMode.FLOOR));
		Assertions.assertEquals(new BigDecimal("1256789456"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("45678.4561237").setScale(12, RoundingMode.FLOOR));
		Assertions.assertEquals(new BigDecimal("45678.45612"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("125.456").setScale(12, RoundingMode.FLOOR));
		Assertions.assertEquals(new BigDecimal("125.456"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("12.25678945614564").setScale(10, RoundingMode.FLOOR));
		Assertions.assertEquals(new BigDecimal("12.25678945"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("721.8714286").setScale(7, RoundingMode.HALF_DOWN));
		Assertions.assertEquals(new BigDecimal("721.8714286"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());

		fill.setNotionalUnitPrice(new BigDecimal("721.871428599").setScale(7, RoundingMode.HALF_DOWN));
		Assertions.assertEquals(new BigDecimal("721.8714286"), this.tenDecimalFixOrderPriceAdjustment.adjust(fill).getNotionalUnitPrice());
	}


	@Test
	public void testTenDecimalFixOrderPriceAdjustmentError() {
		TradeFill fill = new TradeFill();
		fill.setNotionalUnitPrice(new BigDecimal("125678945612345.0000").setScale(4, RoundingMode.FLOOR));
		Assertions.assertThrows(Exception.class, () -> this.tenDecimalFixOrderPriceAdjustment.adjust(fill));
	}


	@Test
	public void testTenDecimalFixOrderPriceAdjustmentOrder() {
		TradeOrder order = new TradeOrder();
		order.setAveragePrice(new BigDecimal("721.871428599").setScale(7, RoundingMode.HALF_DOWN));
		Assertions.assertEquals(new BigDecimal("721.8714286"), this.tenDecimalFixOrderPriceAdjustment.adjust(order).getAveragePrice());
	}
}
