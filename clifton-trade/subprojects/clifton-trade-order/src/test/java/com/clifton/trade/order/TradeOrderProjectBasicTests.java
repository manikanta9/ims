package com.clifton.trade.order;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class TradeOrderProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "trade-order";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.sql.Timestamp");
		imports.add("org.apache.poi.ss.usermodel.Sheet");
		imports.add("org.apache.poi.ss.usermodel.Workbook");
		imports.add("org.apache.poi.ss.util.CellRangeAddress");
		imports.add("org.apache.poi.ss.usermodel.Cell");
		imports.add("org.apache.poi.ss.usermodel.CellStyle");
		imports.add("org.apache.poi.ss.usermodel.DataFormat");
		imports.add("org.apache.poi.ss.usermodel.Font");
		imports.add("org.apache.poi.ss.usermodel.Row");
		imports.add("org.springframework.beans.BeansException");
		imports.add("org.springframework.transaction.support.TransactionSynchronizationManager");
		imports.add("org.springframework.core.Ordered");
	}
}
