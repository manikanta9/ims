SELECT 'TradeFill' AS entityTableName, TradeFillID AS entityId
FROM TradeFill
WHERE TradeID IN (2303291, 2303292)
UNION
SELECT 'TradeOrder' AS entityTableName, TradeOrderID AS entityId
FROM TradeOrder
WHERE TradeOrderID IN (SELECT TradeOrderID FROM Trade WHERE TradeID IN (2303291, 2303292))
UNION
SELECT 'MarketDataSourcePriceMultiplier', MarketDataSourcePriceMultiplierID
FROM MarketDataSourcePriceMultiplier m
	 INNER JOIN MarketDataSource ds ON m.MarketDataSourceID = ds.MarketDataSourceID
WHERE ds.DataSourceName = 'Morgan Stanley & Co. Inc.';
