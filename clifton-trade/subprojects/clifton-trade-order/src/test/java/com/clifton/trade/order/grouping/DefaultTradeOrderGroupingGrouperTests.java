package com.clifton.trade.order.grouping;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.mapping.InvestmentAccountMappingService;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeTimeInForceType;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationMappingConfiguration;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.TradeDestinationType;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyService;
import com.clifton.trade.execution.TradeExecutionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.clifton.core.util.date.DateUtils.DATE_FORMAT_FULL;


/**
 * Unit tests for the DefaultTradeOrderGroupingGrouper
 *
 * @author: davidi
 */
public class DefaultTradeOrderGroupingGrouperTests {

	private static TradeTimeInForceType timeInForceTypeDay;
	private static TradeTimeInForceType timeInForceTypeGtc;
	private static TradeTimeInForceType timeInForceTypeGtd;

	@Mock
	private InvestmentAccountUtilHandler investmentAccountUtilHandler;

	@Mock
	private TradeDestinationService tradeDestinationService;

	@Mock
	TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService;

	@Mock
	InvestmentAccountMappingService investmentAccountMappingService;


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeAll
	static void initialize() {
		timeInForceTypeDay = new TradeTimeInForceType();
		timeInForceTypeDay.setId((short)1);
		timeInForceTypeDay.setName(TradeTimeInForceType.DAY);
		timeInForceTypeGtc = new TradeTimeInForceType();
		timeInForceTypeGtc.setId((short)2);
		timeInForceTypeGtc.setName(TradeTimeInForceType.GOOD_TILL_CANCELED);
		timeInForceTypeGtd = new TradeTimeInForceType();
		timeInForceTypeGtd.setId((short)3);
		timeInForceTypeGtd.setName(TradeTimeInForceType.GOOD_TILL_DATE);
	}




	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void groupTrades_directed_brokerage_holdingAccount_match() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();
		List<Trade> tradeList = createTradeList();
		// for this test, make holding accounts match
		tradeList.get(1).setHoldingInvestmentAccount(tradeList.get(0).getHoldingInvestmentAccount());
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);
		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(true);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_directed_brokerage_holdingAccount_notMatch() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();
		List<Trade> tradeList = createTradeList();
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);
		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(true);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_not_directed_brokerage_holdingAccount_match() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();
		List<Trade> tradeList = createTradeList();
		// for this test, make holding accounts match
		tradeList.get(1).setHoldingInvestmentAccount(tradeList.get(0).getHoldingInvestmentAccount());
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);
		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_not_directed_brokerage_holdingAccount_notMatch() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();
		List<Trade> tradeList = createTradeList();
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_default_by_holdingAccount_match() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();
		List<Trade> tradeList = createTradeList();
		// for this test, make holding accounts match
		tradeList.get(1).setHoldingInvestmentAccount(tradeList.get(0).getHoldingInvestmentAccount());
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT_BY_HOLDING_ACCOUNT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_default_by_holdingAccount_not_match() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();
		List<Trade> tradeList = createTradeList();
		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT_BY_HOLDING_ACCOUNT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_trade_execution_type_both_null() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();
		List<Trade> tradeList = createTradeList();
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_trade_execution_type_equal() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		TradeExecutionType tradeExecutionType1 = new TradeExecutionType();
		tradeExecutionType1.setId((short) 2);
		tradeExecutionType1.setName("MOC Trade");

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(1).setTradeExecutionType(tradeExecutionType1);

		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_trade_execution_type_not_equal_one_is_null() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		TradeExecutionType tradeExecutionType1 = new TradeExecutionType();
		tradeExecutionType1.setId((short) 2);
		tradeExecutionType1.setName("MOC Trade");

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(1).setTradeExecutionType(null);

		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_trade_execution_type_not_equal() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		TradeExecutionType tradeExecutionType1 = new TradeExecutionType();
		tradeExecutionType1.setId((short) 2);
		tradeExecutionType1.setName("MOC Trade");

		TradeExecutionType tradeExecutionType2 = new TradeExecutionType();
		tradeExecutionType1.setId((short) 1);
		tradeExecutionType1.setName("Limit Trade");


		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(1).setTradeExecutionType(tradeExecutionType2);

		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_limit_price_both_null() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		TradeExecutionType tradeExecutionType1 = new TradeExecutionType();
		tradeExecutionType1.setId((short) 1);
		tradeExecutionType1.setName("Limit Trade");

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(1).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(0).setLimitPrice(null);
		tradeList.get(1).setLimitPrice(null);
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_limit_price_one_null() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		TradeExecutionType tradeExecutionType1 = new TradeExecutionType();
		tradeExecutionType1.setId((short) 1);
		tradeExecutionType1.setName("Limit Trade");

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(1).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(0).setLimitPrice(new BigDecimal("1.00"));
		tradeList.get(1).setLimitPrice(null);
		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_limit_price_not_equal() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		TradeExecutionType tradeExecutionType1 = new TradeExecutionType();
		tradeExecutionType1.setId((short) 1);
		tradeExecutionType1.setName("Limit Trade");

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(1).setTradeExecutionType(tradeExecutionType1);
		tradeList.get(0).setLimitPrice(new BigDecimal("1.00"));
		tradeList.get(1).setLimitPrice(new BigDecimal("2.00"));
		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_limit_price_equal() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setLimitPrice(new BigDecimal("5.00"));
		tradeList.get(1).setLimitPrice(new BigDecimal("5.00"));
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_timeInForceTypeEqual() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeTimeInForceType(timeInForceTypeGtc);
		tradeList.get(1).setTradeTimeInForceType(timeInForceTypeGtc);
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_timeInForceEqualDayVsNull() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeTimeInForceType(timeInForceTypeDay);
		tradeList.get(1).setTradeTimeInForceType(null);
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_timeInForceNotEqualGtcVsNull() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeTimeInForceType(timeInForceTypeGtc);
		tradeList.get(1).setTradeTimeInForceType(null);
		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_timeInForceNotEqualGtcVsGtd() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeTimeInForceType(timeInForceTypeGtc);
		tradeList.get(1).setTradeTimeInForceType(timeInForceTypeGtd);
		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_timeInForceNotEqualDayVsGtd() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeTimeInForceType(timeInForceTypeDay);
		tradeList.get(1).setTradeTimeInForceType(timeInForceTypeGtd);
		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_timeInForceNotEqualGtdVsExpirationDate() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeTimeInForceType(timeInForceTypeGtd);
		tradeList.get(0).setTimeInForceExpirationDate(DateUtils.toDate("2025-01-15 16:30:00", DATE_FORMAT_FULL));
		tradeList.get(1).setTradeTimeInForceType(timeInForceTypeGtd);
		tradeList.get(0).setTimeInForceExpirationDate(DateUtils.toDate("2025-01-16 16:30:00", DATE_FORMAT_FULL));
		List<Trade> expectedList1 = CollectionUtils.createList(tradeList.get(0));
		List<Trade> expectedList2 = CollectionUtils.createList(tradeList.get(1));

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(2, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
		Assertions.assertEquals(expectedList2, tradeGroupingList.get(1), "Expected list does not match list returned by grouper.");
	}


	@Test
	public void groupTrades_timeInForceEqualGtdVsExpirationDate() {
		DefaultTradeOrderGroupingGrouper defaultGrouper = createDefaultTradeOrderGroupingGrouper();

		List<Trade> tradeList = createTradeList();
		tradeList.get(0).setTradeTimeInForceType(timeInForceTypeGtd);
		tradeList.get(0).setTimeInForceExpirationDate(DateUtils.toDate("2025-01-15 16:30:00", DATE_FORMAT_FULL));
		tradeList.get(1).setTradeTimeInForceType(timeInForceTypeGtd);
		tradeList.get(1).setTimeInForceExpirationDate(DateUtils.toDate("2025-01-15 16:30:00", DATE_FORMAT_FULL));
		List<Trade> expectedList1 = CoreCollectionUtils.clone(tradeList);

		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByCommand(ArgumentMatchers.any()))
				.thenReturn(createTradeDestinationMapping(tradeList.get(0).getTradeDestination(), 1));
		Mockito.when(this.investmentAccountUtilHandler.isHoldingInvestmentAccountDirectedBroker(ArgumentMatchers.any())).thenReturn(false);

		List<List<Trade>> tradeGroupingList = defaultGrouper.groupTrades(tradeList, TradeOrderGroupingTypes.DEFAULT);

		Assertions.assertEquals(1, tradeGroupingList.size(), "Unexpected size for tradeGroupingList.");
		Assertions.assertEquals(expectedList1, tradeGroupingList.get(0), "Expected list does not match list returned by grouper.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<Trade> createTradeList() {
		List<Trade> tradeList = new ArrayList<>();

		TradeDestinationType destinationType = new TradeDestinationType();
		destinationType.setCreateOrder(true);
		destinationType.setFixTradeOnly(true);

		TradeDestination destination01 = new TradeDestination();
		destination01.setId((short) 1);
		destination01.setType(destinationType);

		BusinessCompany issuingCompany01 = new BusinessCompany();
		issuingCompany01.setName("AlphaCompany");
		issuingCompany01.setId(1);

		InvestmentSecurity option1600 = InvestmentSecurityBuilder.ofType(1, "TQJ3C Y 1600", InvestmentType.OPTIONS)
				.withName("TQJ3C Y 1600")
				.build();


		InvestmentAccount holdingAccount01 = createHoldingAccount(1, "Alpha");
		InvestmentAccount holdingAccount02 = createHoldingAccount(2, "Beta");

		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(1)
				.buy()
				.forHoldingAccount(holdingAccount01)
				.withQuantityIntended(76)
				.withTradeDestination(destination01)
				.withIssuingCompany(issuingCompany01)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(2)
				.buy()
				.forHoldingAccount(holdingAccount02)
				.withQuantityIntended(27)
				.withTradeDestination(destination01)
				.withIssuingCompany(issuingCompany01)
				.build());

		return tradeList;
	}


	private InvestmentAccount createHoldingAccount(Integer id, String name) {
		InvestmentAccount holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault();
		holdingAccount.setId(id);
		holdingAccount.setName(name);
		return holdingAccount;
	}


	private TradeDestinationMapping createTradeDestinationMapping(TradeDestination destination, int id) {
		TradeDestinationMapping mapping = new TradeDestinationMapping();
		mapping.setId(id);
		mapping.setTradeDestination(destination);
		TradeDestinationMappingConfiguration configuration = new TradeDestinationMappingConfiguration();
		configuration.setFixTradeGroupingAllowed(true);
		mapping.setMappingConfiguration(configuration);
		return mapping;
	}


	private DefaultTradeOrderGroupingGrouper createDefaultTradeOrderGroupingGrouper() {
		DefaultTradeOrderGroupingGrouper grouper = new DefaultTradeOrderGroupingGrouper();
		grouper.setTradeDestinationService(this.tradeDestinationService);
		grouper.setTradeExecutingBrokerCompanyService(this.tradeExecutingBrokerCompanyService);
		grouper.setInvestmentAccountMappingService(this.investmentAccountMappingService);
		grouper.setInvestmentAccountUtilHandler(this.investmentAccountUtilHandler);

		return grouper;
	}
}
