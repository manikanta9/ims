package com.clifton.trade.order.adjustment;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author manderson
 */
@ContextConfiguration
@Transactional
public class TradeOrderAdjustmentPriceMultiplierTests extends BaseInMemoryDatabaseTests {


	@Resource
	private TradeOrderAdjustmentHandler tradeOrderAdjustmentHandler;

	@Resource
	private TradeService tradeService;

	@Resource
	private TradeOrderService tradeOrderService;


	private static final int MORGAN_STANLEY_JPY_FUTURE_TRADE_ID = 2303292; // Price should * 100
	private static final int MORGAN_STANLEY_SF_FUTURE_TRADE_ID = 2303291; // Price should NOT change ( * 1)


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPriceMultiplyBy100() {
		// Morgan Stanley JP Futures * 100 for FIX Messaging
		Trade trade = this.tradeService.getTrade(MORGAN_STANLEY_JPY_FUTURE_TRADE_ID);

		TradeOrder tradeOrder = this.tradeOrderService.getTradeOrder(trade.getOrderIdentifier());
		BigDecimal originalPrice = tradeOrder.getAveragePrice();
		tradeOrder.setAveragePrice(MathUtils.divide(tradeOrder.getAveragePrice(), MathUtils.BIG_DECIMAL_ONE_HUNDRED));
		this.tradeOrderAdjustmentHandler.applyAdjustments(tradeOrder, trade.getExecutingBrokerCompany().getName());
		Assertions.assertTrue(MathUtils.isEqual(originalPrice, tradeOrder.getAveragePrice()));

		TradeFill tradeFill = CollectionUtils.getOnlyElement(this.tradeService.getTradeFillListByTrade(trade.getId()));
		originalPrice = tradeFill.getNotionalUnitPrice();
		tradeFill.setNotionalUnitPrice(MathUtils.divide(tradeFill.getNotionalUnitPrice(), MathUtils.BIG_DECIMAL_ONE_HUNDRED));
		this.tradeOrderAdjustmentHandler.applyAdjustments(tradeFill, trade.getExecutingBrokerCompany().getName());
		Assertions.assertTrue(MathUtils.isEqual(originalPrice, tradeFill.getNotionalUnitPrice()));
	}


	@Test
	public void testPriceNoChange() {
		// Morgan Stanley SF Futures * 1 for FIX Messaging
		Trade trade = this.tradeService.getTrade(MORGAN_STANLEY_SF_FUTURE_TRADE_ID);

		TradeOrder tradeOrder = this.tradeOrderService.getTradeOrder(trade.getOrderIdentifier());
		BigDecimal originalPrice = tradeOrder.getAveragePrice();
		this.tradeOrderAdjustmentHandler.applyAdjustments(tradeOrder, trade.getExecutingBrokerCompany().getName());
		Assertions.assertTrue(MathUtils.isEqual(originalPrice, tradeOrder.getAveragePrice()));

		TradeFill tradeFill = CollectionUtils.getOnlyElement(this.tradeService.getTradeFillListByTrade(trade.getId()));
		originalPrice = tradeFill.getNotionalUnitPrice();
		this.tradeOrderAdjustmentHandler.applyAdjustments(tradeFill, trade.getExecutingBrokerCompany().getName());
		Assertions.assertTrue(MathUtils.isEqual(originalPrice, tradeFill.getNotionalUnitPrice()));
	}
}
