package com.clifton.trade.order.grouping;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.InvestmentTestObjectFactory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationType;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class OptionsStrangleTradeOrderGroupingGrouperTests {

	@Mock
	private SystemColumnValueHandler systemColumnValueHandler;

	@Mock
	private TradeService tradeService;

	@Mock
	private TradeOrderWorkflowService tradeOrderWorkflowService;


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void grouperTest() {
		List<Trade> tradeList = createTradeList();

		OptionsStrangleTradeOrderGroupingGrouper grouper = new OptionsStrangleTradeOrderGroupingGrouper();
		grouper.setSystemColumnValueHandler(this.systemColumnValueHandler);
		grouper.setTradeService(this.tradeService);
		grouper.setTradeOrderWorkflowService(this.tradeOrderWorkflowService);

		List<List<Trade>> resultTradeList = grouper.groupTrades(tradeList, TradeOrderGroupingTypes.OPTIONS_STRANGLE);
		Assertions.assertEquals(3, resultTradeList.size());

		for (List<Trade> tl : CollectionUtils.getIterable(resultTradeList)) {
			InvestmentSecurity lastSecurity = null;
			boolean strangle = false;
			BeanUtils.sortWithFunctions(tl, CollectionUtils.createList(
					Trade::getQuantityIntended,
					trade -> trade.getInvestmentSecurity().getSymbol()),
					CollectionUtils.createList(true, true));

			for (Trade t : CollectionUtils.getIterable(tl)) {
				if (lastSecurity == null) {
					lastSecurity = t.getInvestmentSecurity();
				}
				else {
					if (!lastSecurity.equals(t.getInvestmentSecurity())) {
						strangle = true;
						//break;
					}
				}
			}

			if (strangle) {
				checkStrangleList(tl);
			}
			else if (lastSecurity != null) {
				if (lastSecurity.getId().equals(1)) {
					checkCallList(tl);
				}
				else {
					checkPutList(tl);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getTradeString(Trade trade) {
		return trade.getId() + "\t" + trade.getHoldingInvestmentAccount().getName() + "\t" + trade.getInvestmentSecurity().getSymbol() + "\t" + trade.getQuantityIntended();
	}


	private void checkCallList(List<Trade> tradeList) {
		Assertions.assertEquals(1, tradeList.size());
		Trade trade = tradeList.get(0);

		Assertions.assertEquals(new BigDecimal("49"), trade.getQuantityIntended());

		Assertions.assertEquals("null	Pitt	TQJ3C Y 1600	49", getTradeString(trade));
	}


	private void checkPutList(List<Trade> tradeList) {
		Assertions.assertEquals(5, tradeList.size());

		Assertions.assertEquals(new BigDecimal("61"), CoreMathUtils.sumProperty(tradeList, Trade::getQuantityIntended));

		Assertions.assertEquals("null	Trinity Health	TQJ3P Y 1515	3", getTradeString(tradeList.get(0)));
		Assertions.assertEquals("null	Trinity Pension	TQJ3P Y 1515	3", getTradeString(tradeList.get(1)));
		Assertions.assertEquals("null	Uniroyal	TQJ3P Y 1515	3", getTradeString(tradeList.get(2)));
		Assertions.assertEquals("44	Alfred	TQJ3P Y 1515	25", getTradeString(tradeList.get(3)));
		Assertions.assertEquals("null	Clifton	TQJ3P Y 1515	27", getTradeString(tradeList.get(4)));
	}


	private void checkStrangleList(List<Trade> tradeList) {
		Assertions.assertEquals(10, tradeList.size());

		Assertions.assertEquals(new BigDecimal("351"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 1), Trade::getQuantityIntended));
		Assertions.assertEquals(new BigDecimal("351"), CoreMathUtils.sumProperty(BeanUtils.filter(tradeList, trade -> trade.getInvestmentSecurity().getId(), 2), Trade::getQuantityIntended));

		Assertions.assertEquals("42	Pitt	TQJ3C Y 1600	27", getTradeString(tradeList.get(0)));
		Assertions.assertEquals("43	Pitt	TQJ3P Y 1515	27", getTradeString(tradeList.get(1)));
		Assertions.assertEquals("51	Uniroyal	TQJ3C Y 1600	29", getTradeString(tradeList.get(2)));
		Assertions.assertEquals("60	Uniroyal	TQJ3P Y 1515	29", getTradeString(tradeList.get(3)));
		Assertions.assertEquals("47	Trinity Health	TQJ3C Y 1600	40", getTradeString(tradeList.get(4)));
		Assertions.assertEquals("49	Trinity Pension	TQJ3C Y 1600	40", getTradeString(tradeList.get(5)));
		Assertions.assertEquals("48	Trinity Health	TQJ3P Y 1515	40", getTradeString(tradeList.get(6)));
		Assertions.assertEquals("50	Trinity Pension	TQJ3P Y 1515	40", getTradeString(tradeList.get(7)));
		Assertions.assertEquals("45	Clifton	TQJ3C Y 1600	215", getTradeString(tradeList.get(8)));
		Assertions.assertEquals("46	Clifton	TQJ3P Y 1515	215", getTradeString(tradeList.get(9)));
	}


	private List<Trade> createTradeList() {
		List<Trade> tradeList = new ArrayList<>();
		TradeDestination destination = new TradeDestination();
		TradeDestinationType destinationType = new TradeDestinationType();
		destinationType.setCreateOrder(true);
		destination.setType(destinationType);

		InvestmentSecurity option1600 = InvestmentSecurityBuilder.ofType(1, "TQJ3C Y 1600", InvestmentType.OPTIONS)
				.withName("TQJ3C Y 1600")
				.withOptionType(InvestmentSecurityOptionTypes.CALL)
				.build();
		InvestmentSecurity option1515 = InvestmentSecurityBuilder.ofType(2, "TQJ3P Y 1515", InvestmentType.OPTIONS)
				.withName("TQJ3P Y 1515")
				.withOptionType(InvestmentSecurityOptionTypes.PUT)
				.build();

		InvestmentAccount pittHoldingAccount = createHoldingAccount(1, "Pitt");
		InvestmentAccount alfredHoldingAccount = createHoldingAccount(2, "Alfred");
		InvestmentAccount cliftonHoldingAccount = createHoldingAccount(3, "Clifton");
		InvestmentAccount trinityHealthHoldingAccount = createHoldingAccount(4, "Trinity Health");
		InvestmentAccount trinityPensionHoldingAccount = createHoldingAccount(5, "Trinity Pension");
		InvestmentAccount uniroyalHoldingAccount = createHoldingAccount(6, "Uniroyal");

		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(42)
				.buy()
				.forHoldingAccount(pittHoldingAccount)
				.withQuantityIntended(76)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(43)
				.buy()
				.forHoldingAccount(pittHoldingAccount)
				.withQuantityIntended(27)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(44)
				.buy()
				.forHoldingAccount(alfredHoldingAccount)
				.withQuantityIntended(25)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(45)
				.buy()
				.forHoldingAccount(cliftonHoldingAccount)
				.withQuantityIntended(215)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(46)
				.buy()
				.forHoldingAccount(cliftonHoldingAccount)
				.withQuantityIntended(242)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(47)
				.buy()
				.forHoldingAccount(trinityHealthHoldingAccount)
				.withQuantityIntended(40)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(48)
				.buy()
				.forHoldingAccount(trinityHealthHoldingAccount)
				.withQuantityIntended(43)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(49)
				.buy()
				.forHoldingAccount(trinityPensionHoldingAccount)
				.withQuantityIntended(40)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(50)
				.buy()
				.forHoldingAccount(trinityPensionHoldingAccount)
				.withQuantityIntended(43)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1600)
				.withId(51)
				.buy()
				.forHoldingAccount(uniroyalHoldingAccount)
				.withQuantityIntended(29)
				.withTradeDestination(destination)
				.build());
		tradeList.add(TradeBuilder.newTradeForSecurity(option1515)
				.withId(60)
				.buy()
				.forHoldingAccount(uniroyalHoldingAccount)
				.withQuantityIntended(32)
				.withTradeDestination(destination)
				.build());

		return tradeList;
	}


	private InvestmentAccount createHoldingAccount(Integer id, String name) {
		InvestmentAccount holdingAccount = InvestmentTestObjectFactory.newInvestmentAccount_HoldingDefault();
		holdingAccount.setId(id);
		holdingAccount.setName(name);
		return holdingAccount;
	}
}
