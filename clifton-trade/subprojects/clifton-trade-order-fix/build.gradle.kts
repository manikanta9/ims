import com.clifton.gradle.plugin.build.usingVariant

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-core"))
	api(project(":clifton-core:clifton-core-messaging"))
	api(project(":clifton-system"))

	api(project(":clifton-fix")) { usingVariant("api") }

	api(project(":clifton-trade:clifton-trade-order"))

	javascript(project(":clifton-fix"))
	javascript(project(":clifton-trade"))
	javascript(project(":clifton-trade:clifton-trade-order"))


	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-trade")))
	testFixturesApi(testFixtures(project(":clifton-trade:clifton-trade-order")))
}
