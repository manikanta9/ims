package com.clifton.trade.order.fix.order.allocation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderServiceImpl;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A  set of tests designed to test the calculation of proportional trade quantities for partially filled trade orders.
 *
 * @author davidi
 */
public class CalculateTradeOrderTradeProportionalQuantityTests {

	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_wholeQuantity_noRemainder() {
		TradeOrder tradeOrder = createTradeOrder(1000, 100, 50, TradeType.OPTIONS, CollectionUtils.createList(20.0, 30.0, 50.0), false, false, "USD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);


		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(10.0), tradeMap.get(1000).getQuantityIntended()));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(15.0), tradeMap.get(1001).getQuantityIntended()));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(25.0), tradeMap.get(1002).getQuantityIntended()));
	}


	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_wholeQuantity_singleTrade() {
		TradeOrder tradeOrder = createTradeOrder(1000, 20, 10, TradeType.OPTIONS, CollectionUtils.createList(20.0), false, false, "USD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);


		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(10.0), tradeMap.get(1000).getQuantityIntended()));
	}


	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_wholeQuantity_withRemainder() {
		TradeOrder tradeOrder = createTradeOrder(1000, 100, 47, TradeType.OPTIONS, CollectionUtils.createList(20.0, 30.0, 50.0), true, false, "USD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);


		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(9.0), tradeMap.get(1000).getQuantityIntended()));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(14.0), tradeMap.get(1001).getQuantityIntended()));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(24.0), tradeMap.get(1002).getQuantityIntended()));
	}


	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_wholeQuantity_withRemainder_equalQuantityOrders() {
		TradeOrder tradeOrder = createTradeOrder(1000, 60, 56, TradeType.OPTIONS, CollectionUtils.createList(20.0, 20.0, 20.0), true, false, "USD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);


		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		BigDecimal tradeQuantity1 = tradeMap.get(1000).getQuantityIntended();
		BigDecimal tradeQuantity2 = tradeMap.get(1001).getQuantityIntended();
		BigDecimal tradeQuantity3 = tradeMap.get(1002).getQuantityIntended();

		// Random distribution of excess share.
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(18.0), tradeQuantity1) || MathUtils.isEqual(BigDecimal.valueOf(19.0), tradeQuantity1));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(18.0), tradeQuantity2) || MathUtils.isEqual(BigDecimal.valueOf(19.0), tradeQuantity2));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(18.0), tradeQuantity3) || MathUtils.isEqual(BigDecimal.valueOf(19.0), tradeQuantity3));

		Assertions.assertTrue(MathUtils.isEqual(tradeOrder.getQuantityFilled(), CoreMathUtils.sum(tradeQuantity1, tradeQuantity2, tradeQuantity3)));
	}


	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_fractionalQuantity_forward() {
		TradeOrder tradeOrder = createTradeOrder(1000, 7200000.0, 6800000.0, TradeType.FORWARDS, CollectionUtils.createList(2200000.00, 2000000.0, 3000000.0), false, true, "USD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);

		BigDecimal proportion = MathUtils.divide(tradeOrder.getQuantityFilled(), tradeOrder.getQuantity());
		BigDecimal expectedQuantity1 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(0).getQuantityIntended());
		BigDecimal expectedQuantity2 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(1).getQuantityIntended());
		BigDecimal expectedQuantity3 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(2).getQuantityIntended());

		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity1, tradeMap.get(1000).getQuantityIntended()));
		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity2, tradeMap.get(1001).getQuantityIntended()));
		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity3, tradeMap.get(1002).getQuantityIntended()));
	}


	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_fractionalQuantity_forward_singleTrade() {
		TradeOrder tradeOrder = createTradeOrder(1000, 2200000.00, 1000000, TradeType.FORWARDS, CollectionUtils.createList(2200000.00), false, true, "USD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);

		BigDecimal proportion = MathUtils.divide(tradeOrder.getQuantityFilled(), tradeOrder.getQuantity());
		BigDecimal expectedQuantity1 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(0).getQuantityIntended());

		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity1, tradeMap.get(1000).getQuantityIntended()));
	}


	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_fractionalQuantity_currency() {
		TradeOrder tradeOrder = createTradeOrder(1000, 8000000.0, 6000000.0, TradeType.CURRENCY, CollectionUtils.createList(2000000.00, 1000000.0, 4000000.0), true, true, "USD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);

		BigDecimal proportion = MathUtils.divide(tradeOrder.getQuantityFilled(), tradeOrder.getQuantity());
		BigDecimal expectedQuantity1 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(0).getAccountingNotional());
		BigDecimal expectedQuantity2 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(1).getAccountingNotional());
		BigDecimal expectedQuantity3 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(2).getAccountingNotional());

		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity1, tradeMap.get(1000).getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity2, tradeMap.get(1001).getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity3, tradeMap.get(1002).getAccountingNotional()));
	}


	@Test
	public void testCalculateTradeOrderTradeProportionalQuantity_fractionalQuantity_forwards_usingNotional() {
		TradeOrder tradeOrder = createTradeOrder(1000, 8000000.0, 6000000.0, TradeType.FORWARDS, CollectionUtils.createList(2000000.00, 1000000.0, 4000000.0), true, true, "CAD");
		MockedTradeOrderService tradeOrderService = new MockedTradeOrderService();
		tradeOrderService.setTradeOrder(tradeOrder);
		TradeOrderAllocationFixServiceImpl tradeOrderAllocationFixService = new TradeOrderAllocationFixServiceImpl();
		tradeOrderAllocationFixService.setTradeOrderService(tradeOrderService);

		BigDecimal proportion = MathUtils.divide(tradeOrder.getQuantityFilled(), tradeOrder.getQuantity());
		BigDecimal expectedQuantity1 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(0).getAccountingNotional());
		BigDecimal expectedQuantity2 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(1).getAccountingNotional());
		BigDecimal expectedQuantity3 = MathUtils.multiply(proportion, tradeOrder.getTradeList().get(2).getAccountingNotional());

		List<Trade> updatedTrades = tradeOrderAllocationFixService.calculateTradeOrderTradeProportionalQuantity(tradeOrder.getId());
		Map<Integer, Trade> tradeMap = getTradeMap(updatedTrades);

		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity1, tradeMap.get(1000).getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity2, tradeMap.get(1001).getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(expectedQuantity3, tradeMap.get(1002).getAccountingNotional()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrder createTradeOrder(int tradeOrderId, double orderQuantity, double fillQuantity, String tradeTypeName, List<Double> tradeQuantities, boolean isBuy, boolean quantityIsFractional, String settlementCurrencySymbol) {
		TradeOrder tradeOrder = new TradeOrder();
		tradeOrder.setId(tradeOrderId);
		tradeOrder.setQuantity(BigDecimal.valueOf(orderQuantity));
		tradeOrder.setQuantityFilled(BigDecimal.valueOf(fillQuantity));
		tradeOrder.setTradeList(new ArrayList<>());
		tradeOrder.setInvestmentSecurity(new InvestmentSecurity());
		tradeOrder.getInvestmentSecurity().setInstrument(new InvestmentInstrument());
		tradeOrder.getInvestmentSecurity().getInstrument().setHierarchy(new InvestmentInstrumentHierarchy());
		tradeOrder.getInvestmentSecurity().getInstrument().getHierarchy().setInvestmentType(new InvestmentType());
		tradeOrder.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().setQuantityDecimalPrecision(quantityIsFractional ? (short) 2 : (short) 0);

		tradeOrder.setBuy(isBuy);

		InvestmentSecurity tradingCurrency = new InvestmentSecurity();
		InvestmentSecurity settlementCurrency = tradingCurrency;
		tradingCurrency.setSymbol("USD");
		tradeOrder.getInvestmentSecurity().getInstrument().setTradingCurrency(tradingCurrency);

		if (settlementCurrencySymbol != null && !settlementCurrencySymbol.equals(tradingCurrency.getSymbol())) {
			settlementCurrency = new InvestmentSecurity();
			settlementCurrency.setSymbol(settlementCurrencySymbol);
		}

		int idCounter = tradeOrderId;
		for (Double quantity : tradeQuantities) {
			Trade trade = new Trade();
			tradeOrder.getTradeList().add(trade);
			TradeType tradeType = new TradeType();
			tradeType.setName(tradeTypeName);
			trade.setTradeType(tradeType);
			trade.setId(idCounter++);
			trade.setInvestmentSecurity(tradeOrder.getInvestmentSecurity());
			trade.setPayingSecurity(settlementCurrency);

			if (TradeType.CURRENCY.equals(tradeTypeName) || (TradeType.FORWARDS.equals(tradeTypeName) && settlementCurrency != tradingCurrency)) {
				trade.setAccountingNotional(BigDecimal.valueOf(quantity));
			}
			else {
				trade.setQuantityIntended(BigDecimal.valueOf(quantity));
			}
		}

		return tradeOrder;
	}


	private Map<Integer, Trade> getTradeMap(List<Trade> tradeList) {
		HashMap<Integer, Trade> tradeMap = new HashMap<>();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			tradeMap.put(trade.getId(), trade);
		}

		return tradeMap;
	}


	private static class MockedTradeOrderService extends TradeOrderServiceImpl {

		private TradeOrder tradeOrder;


		@Override
		public TradeOrder getTradeOrder(int tradeOrderId) {
			return this.tradeOrder;
		}


		public void setTradeOrder(TradeOrder tradeOrder) {
			this.tradeOrder = tradeOrder;
		}
	}
}


