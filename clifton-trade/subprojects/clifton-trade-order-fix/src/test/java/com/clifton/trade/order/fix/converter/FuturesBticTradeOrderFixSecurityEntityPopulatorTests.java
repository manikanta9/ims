package com.clifton.trade.order.fix.converter;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceSector;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.impl.FuturesBticTradeOrderFixSecurityEntityPopulator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 * Tests to verify the FuturesBticTradeOrderFixSecurityEntityPopulator operates properly.  The conversion of the original Future symbol to the BTIC symbol
 * occurs by isolating the original symbol's month and year suffix and prefixing this suffix with the provided BTIC prefix (instead of the original symbols prefix).
 *
 * @author davidi
 */
public class FuturesBticTradeOrderFixSecurityEntityPopulatorTests {

	@Mock
	MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	@Mock
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	@InjectMocks
	private FuturesBticTradeOrderFixSecurityEntityPopulator populator = new FuturesBticTradeOrderFixSecurityEntityPopulator();

	private boolean initialized = false;
	private TradeExecutionType tradeExecutionType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// Commented out until later due to disablement of BTIC trades


	@BeforeEach
	public void setUp() {
		if (!this.initialized) {
			MockitoAnnotations.initMocks(this);
			MarketDataFieldMapping fieldMapping = new MarketDataFieldMapping();
			fieldMapping.setMarketSector(new MarketDataSourceSector(null, "Index"));
			Mockito.when(this.marketDataFieldMappingRetriever.getMarketDataFieldMappingByInstrument(
					ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.nullable(MarketDataSource.class))).thenReturn(fieldMapping);

			this.tradeExecutionType = new TradeExecutionType();
			this.tradeExecutionType.setName("BTIC Trade");
			this.tradeExecutionType.setBticTrade(true);
			this.initialized = true;
		}
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Jan() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESF8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEF8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEF8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Feb() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESG8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEG8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEG8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Mar() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESH8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEH8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEH8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Apr() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESJ8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEJ8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEJ8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_May() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESK8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEK8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEK8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Jun() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESM8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEM8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEM8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Jul() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESN8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEN8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEN8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Aug() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESQ8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEQ8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEQ8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Sep() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESU8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEU8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEU8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Oct() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESV8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEV8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEV8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Nov() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESX8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEX8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEX8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_For_Month_Dec() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEZ8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEZ8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_Single_Character_Prefix() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("TZ8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEZ8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEZ8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_Three_Character_Prefix() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ABCZ8");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEZ8", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEZ8 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Symbol_Two_Digit_Year_Code() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ18");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade);

		Assertions.assertEquals("STEZ18", securityEntity.getSymbol(), "Unexpected FixSecurityEntity symbol: " + securityEntity.getSymbol());
		Assertions.assertEquals("STEZ18 Index", securityEntity.getSecurityId(), "Unexpected FixSecurityEntity securityId: " + securityEntity.getSecurityId());
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Bad_Input_Symbol_1_Character_Length() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("T");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		Assertions.assertThrows(RuntimeException.class, () -> this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade));
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Bad_Input_Symbol_2_Character_Length() {
		setupSystemColumnValueMock("STE");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("AG");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		Assertions.assertThrows(RuntimeException.class, () -> this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade));
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Btic_Prefix_Null() {
		setupSystemColumnValueMock(null);
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ18");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		Assertions.assertThrows(RuntimeException.class, () -> this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade));
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Btic_Prefix_Empty() {
		setupSystemColumnValueMock("");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ18");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		Assertions.assertThrows(RuntimeException.class, () -> this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade));
	}


	@Test
	public void testPopulateFixSecurityEntityFields_Btic_Prefix_WhiteSpace() {
		setupSystemColumnValueMock(" ");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ18");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		final FixSecurityEntity securityEntity = new FixSecurityEntity();
		Assertions.assertThrows(RuntimeException.class, () -> this.populator.populateFixSecurityEntityFields(securityEntity, futuresTradeOrder, futuresTrade));
	}


	@Test
	public void testIsApplicable_Futures_with_BTIC_trade() {
		setupSystemColumnValueMock("ABC");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ18");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		boolean returnedValue = this.populator.isApplicable(futuresTradeOrder, futuresTrade);
		Assertions.assertTrue(returnedValue, "Expected isApplicable() to return True for Futures trades.");
	}


	@Test
	public void testIsApplicable_Futures_not_BTIC_trade() {
		setupSystemColumnValueMock("ABC");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ18");
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000");

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		boolean returnedValue = this.populator.isApplicable(futuresTradeOrder, futuresTrade);
		Assertions.assertFalse(returnedValue, "Expected isApplicable() to return False for non-BTIC trades.");
	}


	@Test
	public void testIsApplicable_Non_Futures() {
		setupSystemColumnValueMock("ABC");
		InvestmentSecurity investmentSecurity = createFutureInvestmentSecurity("ESZ18");
		InvestmentType investmentType = new InvestmentType();
		investmentType.setName("Stocks");
		investmentType.setId((short) 9);
		investmentSecurity.getInstrument().getHierarchy().setInvestmentType(investmentType);

		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(investmentSecurity)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(this.tradeExecutionType);

		Trade futuresTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();

		final TradeOrder futuresTradeOrder = createOrder(investmentSecurity);
		boolean returnedValue = this.populator.isApplicable(futuresTradeOrder, futuresTrade);
		Assertions.assertFalse(returnedValue, "Expected isApplicable() to return False for Non-Future trades.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setupSystemColumnValueMock(String prefixToReturn) {
		Mockito.doReturn(prefixToReturn).when(this.investmentSecurityUtilHandler).getBticPrefix(ArgumentMatchers.any(InvestmentSecurity.class));
	}


	private InvestmentSecurity createFutureInvestmentSecurity(String symbol) {
		InvestmentSecurity investmentSecurity = new InvestmentSecurity();
		InvestmentInstrument instrument = new InvestmentInstrument();
		InvestmentInstrumentHierarchy hierarchy = new InvestmentInstrumentHierarchy();
		InvestmentType investmentType = new InvestmentType();
		investmentType.setName("Futures");
		investmentType.setId((short) 4);
		hierarchy.setInvestmentType(investmentType);
		instrument.setId(100);
		instrument.setHierarchy(hierarchy);
		investmentSecurity.setInstrument(instrument);
		investmentSecurity.setSymbol(symbol);
		return investmentSecurity;
	}


	private TradeOrder createOrder(InvestmentSecurity security) {
		TradeOrder order = new TradeOrder();
		order.setInvestmentSecurity(security);
		return order;
	}
}
