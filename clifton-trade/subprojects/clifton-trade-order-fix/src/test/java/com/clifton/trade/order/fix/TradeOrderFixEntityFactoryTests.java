package com.clifton.trade.order.fix;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationDetailNestedParty;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.ExecutionTransactionTypes;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.ProcessCodes;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.TimeInForceTypes;
import com.clifton.fix.order.factory.FixEntityFactoryGenerationTypes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurpose;
import com.clifton.investment.account.mapping.InvestmentAccountMappingService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceSector;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMapping;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMappingSearchForm;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeOpenCloseTypes;
import com.clifton.trade.TradeType;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationMappingConfiguration;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatus;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.fix.converter.TradeOrderFixOrderConverter;
import com.clifton.trade.order.fix.converter.util.populators.TradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.BondsTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.ClearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.ClearedInterestRateSwapsTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.CurrencyTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.EquityTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.ForwardsTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.FuturesBticTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.FuturesTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.converter.util.populators.impl.OptionsTradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import com.clifton.trade.order.fix.order.allocation.converter.TradeOrderFixAllocationConverter;
import com.clifton.trade.order.fix.util.TradeOrderFixUtilHandlerImpl;
import com.thoughtworks.xstream.XStream;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TradeOrderFixEntityFactoryTests<T extends SystemColumn> {

	private static final InvestmentSecurity FUH0 = InvestmentSecurityBuilder.newFuture("FUH0")
			.withPriceMultiplier(50)
			.build();

	private TradeOrderFixEntityFactory messageFactory;
	@Mock
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	@Mock
	private TradeOrderService tradeOrderService;
	@Mock
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	@Mock
	private TradeDestinationService tradeDestinationService;
	@Mock
	private MarketDataSourceCompanyService marketDataSourceCompanyMappingService;
	@Mock
	private TradeOrderFixOrderService tradeOrderFixOrderService;
	@Mock
	private MarketDataSourceService marketDataSourceService;
	@Mock
	private InvestmentAccountMappingService investmentAccountMappingService;
	@Mock
	private SystemColumnValueService systemColumnValueService;
	@Mock
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private BondsTradeOrderFixSecurityEntityPopulator bondsTradeOrderFixSecurityEntityPopulator = new BondsTradeOrderFixSecurityEntityPopulator();
	private ClearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator clearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator = new ClearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator();
	private ClearedInterestRateSwapsTradeOrderFixSecurityEntityPopulator clearedInterestRateSwapsTradeOrderFixSecurityEntityPopulator = new ClearedInterestRateSwapsTradeOrderFixSecurityEntityPopulator();
	private CurrencyTradeOrderFixSecurityEntityPopulator currencyTradeOrderFixSecurityEntityPopulator = new CurrencyTradeOrderFixSecurityEntityPopulator();
	private ForwardsTradeOrderFixSecurityEntityPopulator forwardsTradeOrderFixSecurityEntityPopulator = new ForwardsTradeOrderFixSecurityEntityPopulator();
	private FuturesBticTradeOrderFixSecurityEntityPopulator futuresBticTradeOrderFixSecurityEntityPopulator = new FuturesBticTradeOrderFixSecurityEntityPopulator();
	private FuturesTradeOrderFixSecurityEntityPopulator futuresTradeOrderFixSecurityEntityPopulator = new FuturesTradeOrderFixSecurityEntityPopulator();
	private OptionsTradeOrderFixSecurityEntityPopulator optionsTradeOrderFixSecurityEntityPopulator = new OptionsTradeOrderFixSecurityEntityPopulator();
	private EquityTradeOrderFixSecurityEntityPopulator stocksTradeOrderFixSecurityEntityPopulator = new EquityTradeOrderFixSecurityEntityPopulator();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTest() {
		MockitoAnnotations.initMocks(this);
		MarketDataFieldMapping fieldMapping = new MarketDataFieldMapping();
		fieldMapping.setMarketSector(new MarketDataSourceSector(null, "Index"));
		Mockito.when(this.marketDataFieldMappingRetriever.getMarketDataFieldMappingByInstrument(ArgumentMatchers.any(InvestmentInstrument.class), ArgumentMatchers.nullable(MarketDataSource.class))).thenReturn(
				fieldMapping);
		this.messageFactory = new TradeOrderFixEntityFactory();
		TradeOrderFixOrderConverter orderToFixConverter = new TradeOrderFixOrderConverter();

		this.messageFactory.setTradeOrderFixOrderConverter(orderToFixConverter);

		TradeOrderFixAllocationConverter allocationFactory = new TradeOrderFixAllocationConverter();
		allocationFactory.setTradeOrderFixOrderService(this.tradeOrderFixOrderService);

		this.messageFactory.setTradeOrderAllocationConverter(allocationFactory);

		MarketDataSourcePurpose purpose = new MarketDataSourcePurpose();
		purpose.setName("FIX Messaging");

		final TradeDestinationMapping mapping = new TradeDestinationMapping();
		TradeDestination dest = new TradeDestination();
		dest.setFixTargetSubId("TKTS");
		dest.setFixHandlingInstructions("AUTOMATIC");
		dest.setMarketDataSourcePurpose(purpose);
		TradeDestinationConnection connection = new TradeDestinationConnection();
		connection.setFixDestinationName("Goldman Sachs");
		dest.setConnection(connection);
		mapping.setTradeDestination(dest);

		mapping.setFixGlobalTradeNote("Test");
		mapping.setMappingConfiguration(new TradeDestinationMappingConfiguration());

		final TradeDestinationMapping bloombergMapping = new TradeDestinationMapping();
		TradeDestination bloombergDest = new TradeDestination();
		bloombergDest.setFixHandlingInstructions("MANUAL");
		TradeDestinationConnection bloombergConnection = new TradeDestinationConnection();
		bloombergConnection.setFixDestinationName("Bloomberg (Parametric Minneapolis Fixed Income)");
		MarketDataSource mds = new MarketDataSource();
		mds.setId((short) 2);
		mds.setName("Goldman");
		bloombergDest.setMarketDataSource(mds);
		bloombergDest.setConnection(bloombergConnection);
		bloombergDest.setMarketDataSourcePurpose(purpose);
		bloombergMapping.setTradeDestination(bloombergDest);
		bloombergMapping.setFixGlobalTradeNote("Test");
		bloombergMapping.setMappingConfiguration(new TradeDestinationMappingConfiguration());
		bloombergMapping.getMappingConfiguration().setFixAllocationType(AllocationTypes.CALCULATED);

		List<TradeDestinationMapping> mappingList = new ArrayList<>();
		mappingList.add(mapping);

		//this.tradeDestinationService = Mockito.mock(TradeDestinationService.class);
		Mockito.when(this.tradeDestinationService.getTradeDestinationMappingListByCommand(ArgumentMatchers.any(TradeDestinationMappingSearchCommand.class))).thenReturn(mappingList);
		//Mockito.when(this.tradeDestinationService.getTradeDestinationMappingByTrade(Matchers.any(Trade.class))).thenReturn(mapping);

		Mockito.when(this.marketDataSourceService.getMarketDataSourceByCompany(ArgumentMatchers.any(BusinessCompany.class))).thenReturn(mds);

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			Trade trade = ((TradeDestinationMappingSearchCommand) args[0]).getTrade();
			if ("Bonds".equals(trade.getTradeType().getName())) {
				return bloombergMapping;
			}
			return mapping;
		}).when(this.tradeDestinationService).getTradeDestinationMappingByCommand(ArgumentMatchers.any(TradeDestinationMappingSearchCommand.class));

		mockBrokerMapping();

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentAccountRelationshipSearchForm searchForm = (InvestmentAccountRelationshipSearchForm) args[0];
			if ((2 == searchForm.getMainAccountId()) && "Executing: Stocks".equals(searchForm.getPurposeName())) {
				InvestmentAccountRelationship rel = new InvestmentAccountRelationship();
				InvestmentAccount account = new InvestmentAccount();
				account.setNumber("X");
				rel.setReferenceTwo(account);
				List<InvestmentAccountRelationship> result = new ArrayList<>();
				result.add(rel);
				return result;
			}
			if ((1 == searchForm.getMainAccountId()) && "Executing: Stocks".equals(searchForm.getPurposeName())) {
				InvestmentAccountRelationship rel = new InvestmentAccountRelationship();
				InvestmentAccount account = new InvestmentAccount();
				account.setNumber("X1");
				rel.setReferenceTwo(account);
				List<InvestmentAccountRelationship> result = new ArrayList<>();
				result.add(rel);
				return result;
			}
			return null;
		}).when(this.investmentAccountRelationshipService).getInvestmentAccountRelationshipList(ArgumentMatchers.any(InvestmentAccountRelationshipSearchForm.class));

		TradeOrderStatus status = new TradeOrderStatus();
		status.setOrderStatus(TradeOrderStatuses.SENT);
		Mockito.when(this.tradeOrderService.getTradeOrderStatusByStatuses(ArgumentMatchers.eq(TradeOrderStatuses.SENT))).thenReturn(status);

		//getMessageFactory().setTradeOrderService(this.tradeOrderService);
		orderToFixConverter.setInvestmentAccountRelationshipService(this.investmentAccountRelationshipService);
		getMessageFactory().setTradeOrderFixOrderService(this.tradeOrderFixOrderService);
		allocationFactory.setInvestmentAccountRelationshipService(this.investmentAccountRelationshipService);
		allocationFactory.setTradeOrderFixOrderService(this.tradeOrderFixOrderService);
		allocationFactory.setMarketDataSourceCompanyService(this.marketDataSourceCompanyMappingService);
		allocationFactory.setMarketDataSourceService(this.marketDataSourceService);
		//getMessageFactory().setTradeService(this.tradeService);
		//getMessageFactory().setTradeDestinationService(tradeDestinationService);

		Mockito.doReturn(null).when(this.investmentAccountMappingService).getInvestmentAccountMappingByAccountNumberAndPurpose(ArgumentMatchers.any(InvestmentAccount.class), ArgumentMatchers.any(InvestmentAccountMappingPurpose.class));
		Mockito.doReturn("0000-0000").when(this.investmentAccountMappingService).getInvestmentAccountMappedValue(ArgumentMatchers.any(InvestmentAccount.class), ArgumentMatchers.any(InvestmentAccountMappingPurpose.class));

		TradeOrderFixUtilHandlerImpl tradeOrderFixUtilHandler = new TradeOrderFixUtilHandlerImpl();
		tradeOrderFixUtilHandler.setMarketDataSourceCompanyService(this.marketDataSourceCompanyMappingService);
		tradeOrderFixUtilHandler.setTradeDestinationService(this.tradeDestinationService);
		tradeOrderFixUtilHandler.setMarketDataSourceService(this.marketDataSourceService);
		tradeOrderFixUtilHandler.setInvestmentAccountMappingService(this.investmentAccountMappingService);

		this.futuresBticTradeOrderFixSecurityEntityPopulator.setInvestmentSecurityUtilHandler(this.investmentSecurityUtilHandler);
		this.futuresBticTradeOrderFixSecurityEntityPopulator.setMarketDataFieldMappingRetriever(this.marketDataFieldMappingRetriever);
		this.futuresTradeOrderFixSecurityEntityPopulator.setMarketDataFieldMappingRetriever(this.marketDataFieldMappingRetriever);
		this.clearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator.setInvestmentSecurityUtilHandler(this.investmentSecurityUtilHandler);
		this.optionsTradeOrderFixSecurityEntityPopulator.setInvestmentSecurityUtilHandler(this.investmentSecurityUtilHandler);

		List<TradeOrderFixSecurityEntityPopulator> tradeOrderFixSecurityEntityPopulatorList = new ArrayList<>();
		tradeOrderFixSecurityEntityPopulatorList.add(this.bondsTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.clearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.clearedInterestRateSwapsTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.currencyTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.forwardsTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.futuresBticTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.futuresTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.optionsTradeOrderFixSecurityEntityPopulator);
		tradeOrderFixSecurityEntityPopulatorList.add(this.stocksTradeOrderFixSecurityEntityPopulator);

		orderToFixConverter.setTradeOrderFixSecurityEntityPopulatorList(tradeOrderFixSecurityEntityPopulatorList);
		orderToFixConverter.setTradeOrderFixUtilHandler(tradeOrderFixUtilHandler);
		// Manual sorting required as Spring is not instantiating this class.
		CollectionUtils.sort(orderToFixConverter.getTradeOrderFixSecurityEntityPopulatorList(), (element1, element2) -> MathUtils.compare(element1.getOrder(), element2.getOrder()));
		allocationFactory.setTradeOrderFixUtilHandler(tradeOrderFixUtilHandler);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNewOrderBuyMessage() {
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000");

		Trade futureTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();
		Trade futureTrade2 = tradeBuilder.withId(43)
				.withQuantityIntended(7)
				.withNote("Test 43")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, futureTrade, futureTrade2);
		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertNull(fixOrder.getFixTargetLocId());
		Assertions.assertNull(fixOrder.getFixSessionQualifier());

		//assertEquals((Integer) 50, fixOrder.getMessageIdentifier());
		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals("FUH0", fixOrder.getSymbol());
		Assertions.assertEquals("FUH0 Index", fixOrder.getSecurityId());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(SecurityIDSources.BID, fixOrder.getSecurityIDSource());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("17.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Btic_Symbol() {
		TradeExecutionType tradeExecutionType = new TradeExecutionType();
		tradeExecutionType.setName("BTIC Trade");
		tradeExecutionType.setBticTrade(true);

		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeExecutionType(tradeExecutionType);

		Trade futureTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();
		Trade futureTrade2 = tradeBuilder.withId(43)
				.withQuantityIntended(7)
				.withNote("Test 43")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, futureTrade, futureTrade2);

		Mockito.doReturn("STE").when(this.investmentSecurityUtilHandler).getBticPrefix(ArgumentMatchers.any(InvestmentSecurity.class));

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertNull(fixOrder.getFixTargetLocId());
		Assertions.assertNull(fixOrder.getFixSessionQualifier());

		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals("STEH0", fixOrder.getSymbol());
		Assertions.assertEquals("STEH0 Index", fixOrder.getSecurityId());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(SecurityIDSources.BID, fixOrder.getSecurityIDSource());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("17.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Bond_Trade() {
		InvestmentSecurity bondSecurity = InvestmentSecurityBuilder.newBond("001055AM4")
				.withCusip("001055")
				.build();

		Trade trade1 = TradeBuilder.newTradeForSecurity(bondSecurity)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withNote("Test 42")
				.build();

		Mockito.doReturn("5").when(this.systemColumnValueService).getSystemColumnCustomValue(ArgumentMatchers.anyInt(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString());

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Bloomberg (Parametric Minneapolis Fixed Income)", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertNull(fixOrder.getFixTargetSubId());
		Assertions.assertNull(fixOrder.getFixTargetLocId());
		Assertions.assertNull(fixOrder.getFixSessionQualifier());

		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals("001055", fixOrder.getSecurityId());
		Assertions.assertEquals(SecurityIDSources.CUSIP, fixOrder.getSecurityIDSource());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.MANUAL, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Cleared_Credit_Default_Swap_Trade() {
		InvestmentSecurity bondSecurity = InvestmentSecurityBuilder.newCDS("001055AM4").build();
		bondSecurity.setCusip("11110000");
		InvestmentSecurity underlyingSecurity = new InvestmentSecurity();
		underlyingSecurity.setCusip("1110111");
		bondSecurity.setUnderlyingSecurity(underlyingSecurity);
		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS);

		Trade trade1 = TradeBuilder.newTradeForSecurity(bondSecurity)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withTradeType(tradeType)
				.build();

		Mockito.doReturn(5).when(this.investmentSecurityUtilHandler).getCustomColumnValue(ArgumentMatchers.any(InvestmentSecurity.class), ArgumentMatchers.anyString());

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertNull(fixOrder.getFixTargetLocId());
		Assertions.assertNull(fixOrder.getFixSessionQualifier());

		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals("1110111", fixOrder.getSecurityId());
		Assertions.assertEquals(SecurityIDSources.RED_CODE, fixOrder.getSecurityIDSource());
		Assertions.assertEquals("Y5", fixOrder.getTenor());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Currency_Trade() {
		InvestmentSecurity currencySecurity1 = InvestmentSecurityBuilder.newGBP();
		InvestmentSecurity currencySecurity2 = InvestmentSecurityBuilder.newUSD();

		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.CURRENCY);
		Trade trade1 = TradeBuilder.newTradeForSecurity(currencySecurity1)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withTradeType(tradeType)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withPayingSecurity(currencySecurity2)
				.withNote("Test 42")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		String currencySymbol = currencySecurity1.getSymbol();
		String payingSecuritySymbol = currencySecurity2.getSymbol();
		String expectedSymbol = currencySymbol + "/" + payingSecuritySymbol;

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertNull(fixOrder.getFixTargetLocId());
		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals(expectedSymbol, fixOrder.getSymbol());
		Assertions.assertEquals(currencySymbol, fixOrder.getCurrency());
		Assertions.assertEquals(payingSecuritySymbol, fixOrder.getSettlementCurrency());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Forwards_Trade() {
		InvestmentSecurity forwardSecurity = InvestmentSecurityBuilder.newForward("GPB")
				.build();
		InvestmentSecurity originalSecurity = InvestmentSecurityBuilder.newForward("USD")
				.build();

		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.FORWARDS);
		Trade trade1 = TradeBuilder.newTradeForSecurity(forwardSecurity)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withTradeType(tradeType)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withNote("Test 42")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);
		order.setOriginalInvestmentSecurity(originalSecurity);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		String symbolForOrder = forwardSecurity.getSymbol();
		String symbolOrig = originalSecurity.getInstrument().getTradingCurrency().getSymbol();
		String expectedSymbol = symbolForOrder + "/" + symbolOrig;

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertNull(fixOrder.getFixTargetLocId());
		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals(expectedSymbol, fixOrder.getSymbol());
		Assertions.assertEquals(symbolForOrder, fixOrder.getCurrency());
		Assertions.assertEquals(symbolOrig, fixOrder.getSettlementCurrency());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Stock_Trade() {
		InvestmentSecurity stockSecurity = InvestmentSecurityBuilder.ofType("AAPL", InvestmentType.STOCKS)
				.withCusip("00000002")
				.withSedol("10000002")
				.build();

		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.STOCKS);
		Trade trade1 = TradeBuilder.newTradeForSecurity(stockSecurity)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withTradeType(tradeType)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withNote("Test 42")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals(stockSecurity.getSedol(), fixOrder.getSymbol());
		Assertions.assertEquals(SecurityIDSources.SEDOL, fixOrder.getSecurityIDSource());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Funds_Trade() {
		InvestmentSecurity fundsSecurity = InvestmentSecurityBuilder.ofType("PPAFUND", InvestmentType.FUNDS)
				.withCusip("00000002")
				.withSedol("10000002")
				.build();

		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.FUNDS);
		Trade trade1 = TradeBuilder.newTradeForSecurity(fundsSecurity)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withTradeType(tradeType)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withNote("Test 42")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertNull(fixOrder.getFixTargetLocId());
		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals(fundsSecurity.getSedol(), fixOrder.getSymbol());
		Assertions.assertEquals(SecurityIDSources.SEDOL, fixOrder.getSecurityIDSource());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Notes_Trade() {
		InvestmentSecurity notesSecurity = InvestmentSecurityBuilder.ofType("SDA", InvestmentType.NOTES)
				.withCusip("00000004")
				.withSedol("10000004")
				.build();

		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.NOTES);
		Trade trade1 = TradeBuilder.newTradeForSecurity(notesSecurity)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withTradeType(tradeType)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withNote("Test 42")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals(notesSecurity.getSedol(), fixOrder.getSymbol());
		Assertions.assertEquals(SecurityIDSources.SEDOL, fixOrder.getSecurityIDSource());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testNewOrderBuyMessage_Equity_Option_Trade() {
		InvestmentSecurity underlyingSecurity = InvestmentSecurityBuilder.newStock("AAPL")
				.build();
		InvestmentSecurity optionSecurity = InvestmentSecurityBuilder.ofType("AAPL US 05/31/19 C200", InvestmentType.OPTIONS)
				.withCusip("00000002")
				.withOptionType(InvestmentSecurityOptionTypes.CALL)
				.withOptionStrikePrice(new BigDecimal(200))
				.build();
		optionSecurity.setUnderlyingSecurity(underlyingSecurity);
		optionSecurity.setLastDeliveryDate(DateUtils.toDate("05/31/2019"));
		optionSecurity.setOptionType(InvestmentSecurityOptionTypes.CALL);
		optionSecurity.setOptionStrikePrice(BigDecimal.valueOf(200));

		TradeType tradeType = new TradeType();
		tradeType.setName(TradeType.OPTIONS);
		Trade trade1 = TradeBuilder.newTradeForSecurity(optionSecurity)
				.withId(42)
				.buy()
				.withQuantityIntended(3)
				.withTradeType(tradeType)
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000")
				.withNote("Test 42")
				.build();
		TradeOpenCloseType tradeOpenCloseType = new TradeOpenCloseType();
		tradeOpenCloseType.setOpen(true);
		tradeOpenCloseType.setBuy(true);
		tradeOpenCloseType.setName(TradeOpenCloseTypes.BUY_OPEN.getName());

		trade1.setOpenCloseType(tradeOpenCloseType);
		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, trade1);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);

		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(fixOrder.getFixBeginString());
		Assertions.assertNull(fixOrder.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", fixOrder.getSenderUserName());
		Assertions.assertNull(fixOrder.getFixSenderLocId());

		Assertions.assertNull(fixOrder.getFixTargetCompId());
		Assertions.assertEquals("TKTS", fixOrder.getFixTargetSubId());
		Assertions.assertEquals("O_50_20130126", fixOrder.getClientOrderStringId());
		Assertions.assertEquals("AAPL", fixOrder.getSymbol());
		Assertions.assertTrue(MathUtils.isEqual(fixOrder.getStrikePrice(), BigDecimal.valueOf(200.0)));
		Assertions.assertEquals(InvestmentSecurityOptionTypes.CALL.name(), fixOrder.getPutOrCall().name());
		Assertions.assertEquals(DateUtils.fromDate(optionSecurity.getLastDeliveryDate(), "dd"), fixOrder.getMaturityDay());
		Assertions.assertEquals(DateUtils.fromDate(optionSecurity.getLastDeliveryDate(), "yyyyMM"), fixOrder.getMaturityMonthYear());
		Assertions.assertNull(fixOrder.getSecurityIDSource());
		Assertions.assertNull(fixOrder.getExDestination());
		Assertions.assertEquals(OrderSides.BUY, fixOrder.getSide());
		Assertions.assertEquals(HandlingInstructions.AUTOMATIC, fixOrder.getHandlingInstructions());
		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("3.0"), fixOrder.getOrderQty()));
		Assertions.assertEquals(OrderTypes.MARKET, fixOrder.getOrderType());
		Assertions.assertEquals(TimeInForceTypes.DAY, fixOrder.getTimeInForce());
	}


	@Test
	public void testFixOrderCancelMessage() {
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000");

		Trade futureTrade = tradeBuilder.withQuantityIntended(10)
				.withNote("Test 42")
				.build();
		Trade futureTrade2 = tradeBuilder.withId(43)
				.withQuantityIntended(7)
				.withNote("Test 43")
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, futureTrade, futureTrade2);

		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);
		Mockito.when(this.tradeOrderFixOrderService.getTradeOrderFixOrder(ArgumentMatchers.anyInt(), ArgumentMatchers.anyBoolean())).thenReturn(fixOrder);

		FixOrderCancelRequest cancel = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_CANCEL_REQUEST);
		Assertions.assertEquals("Goldman Sachs", fixOrder.getFixDestinationName());
		Assertions.assertNull(cancel.getFixBeginString());
		Assertions.assertNull(cancel.getFixSenderCompId());
		Assertions.assertEquals("gFawkes", cancel.getSenderUserName());
		Assertions.assertNull(cancel.getFixSenderLocId());

		Assertions.assertNull(cancel.getFixTargetCompId());
		Assertions.assertEquals("TKTS", cancel.getFixTargetSubId());
		Assertions.assertNull(cancel.getFixTargetLocId());
		Assertions.assertNull(cancel.getFixSessionQualifier());

		Assertions.assertEquals("O_50_20130126", cancel.getOriginalClientOrderStringId());
	}


	@Test
	public void testBloombergAllocationMessage() {
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newBond("912828BW9").build())
				.withId(42)
				.buy()
				.withExpectedUnitPrice("100")
				.withCommissionPerUnit("0.000000000");

		Trade bondTrade = tradeBuilder.withQuantityIntended(1000000)
				.build();
		Trade bondTrade2 = tradeBuilder.withId(43)
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.FILLED, bondTrade, bondTrade2);
		order.setId(55641);
		order.setExternalIdentifier(111547);

		XStream stream = new XStream();
		FixExecutionReport report2 = (FixExecutionReport) stream.fromXML("<com.clifton.fix.order.FixExecutionReport><sequenceNumber>12</sequenceNumber><fixBeginString>FIX.4.4</fixBeginString><fixSenderCompId>BLP</fixSenderCompId><fixSenderSubId>6036836</fixSenderSubId><fixTargetCompId>CLIFTONPARA</fixTargetCompId><incoming>false</incoming><orderId>23</orderId><secondaryOrderId>1403:20130509:145:6</secondaryOrderId><clientOrderStringId>O_55642_20130509072350</clientOrderStringId><executionType>TRADE</executionType><executionId>D2:20130509:145:6:12</executionId><orderStatus>FILLED</orderStatus><symbol>[N/A]</symbol><securityId>912828BW9</securityId><securityIDSource>CUSIP</securityIDSource><securityType>TREASURY_INFLATION_PROTECTED_SECURITIES</securityType><side>BUY</side><orderQuantity>2000000.000000000000000</orderQuantity><price>0E-15</price><currency>USD</currency><timeInForceType>DAY</timeInForceType><lastShares>2000000.000000000000000</lastShares><lastPrice>99.535156250000000</lastPrice><cumulativeQuantity>2000000.000000000000000</cumulativeQuantity><leavesQuantity>0E-15</leavesQuantity><averagePrice>99.535156250000000</averagePrice><transactionTime>2013-05-09 12:25:36.0 UTC</transactionTime><accruedInterestAmount>12707.18</accruedInterestAmount><netMoney>2003410.31</netMoney><grossTradeAmount>1990703.13</grossTradeAmount><couponRate>0.02</couponRate><yield>0.0268980515</yield><concession>0</concession><issuer>TSY INFL IX N/B</issuer><priceType>PERCENTAGE</priceType><partyList><com.clifton.fix.order.FixParty>      <partyId>CHASKAMP</partyId>      <partyIdSource>PROPRIETARY_CUSTOM_CODE</partyIdSource>      <partyRole>ORDER_ORIGINATION_TRADER</partyRole></com.clifton.fix.order.FixParty><com.clifton.fix.order.FixParty>      <partyId>PARAMETRIC PORTFOLIO ASSOCIATES LLC</partyId>      <partyIdSource>PROPRIETARY_CUSTOM_CODE</partyIdSource>      <partyRole>ORDER_ORIGINATION_FIRM</partyRole></com.clifton.fix.order.FixParty><com.clifton.fix.order.FixParty>      <partyId>TSOX</partyId>      <partyIdSource>PROPRIETARY_CUSTOM_CODE</partyIdSource>      <partyRole>EXECUTING_SYSTEM</partyRole></com.clifton.fix.order.FixParty><com.clifton.fix.order.FixParty>      <partyId>9001</partyId>      <partyIdSource>PROPRIETARY_CUSTOM_CODE</partyIdSource>      <partyRole>EXECUTING_FIRM</partyRole></com.clifton.fix.order.FixParty></partyList></com.clifton.fix.order.FixExecutionReport>");
		FixExecutionReport report1 = (FixExecutionReport) stream.fromXML("<com.clifton.fix.order.FixExecutionReport><sequenceNumber>97</sequenceNumber><fixBeginString>FIX.4.4</fixBeginString><fixSenderCompId>BLP</fixSenderCompId><fixSenderSubId>6036836</fixSenderSubId><fixTargetCompId>CLIFTONPARA</fixTargetCompId><incoming>false</incoming><orderId>19</orderId><clientOrderStringId>O_55212_20130507092230</clientOrderStringId><executionType>NEW</executionType><executionId>51890e2814640075</executionId><orderStatus>NEW</orderStatus><symbol>N/A</symbol><securityId>912828BW9</securityId><securityIDSource>CUSIP</securityIDSource><side>BUY</side><orderQuantity>2000000.000000000000000</orderQuantity><orderType>MARKET</orderType><currency>USD</currency><timeInForceType>DAY</timeInForceType><lastShares>0E-15</lastShares><lastPrice>0E-15</lastPrice><cumulativeQuantity>0E-15</cumulativeQuantity><leavesQuantity>2000000.000000000000000</leavesQuantity><averagePrice>0E-15</averagePrice><transactionTime>2013-05-07 14:22:30.0 UTC</transactionTime><text>New Order (912828BW9) received, ORDER ID #19</text><couponRate>0.02</couponRate><yield>0.0</yield></com.clifton.fix.order.FixExecutionReport>");
		FixOrder fixOrder = (FixOrder) stream.fromXML("<com.clifton.fix.order.FixOrder><sequenceNumber>48</sequenceNumber><fixBeginString>FIX.4.4</fixBeginString><fixSenderCompId>CLIFTONPARA</fixSenderCompId><fixSenderSubId></fixSenderSubId><fixTargetCompId>BLP</fixTargetCompId><fixTargetSubId>6036836</fixTargetSubId><fixDestinationName>Bloomberg (Parametric Minneapolis Fixed Income)</fixDestinationName><incoming>false</incoming><symbol>N/A</symbol><securityId>912828BW9</securityId><securityIDSource>CUSIP</securityIDSource><clientOrderStringId>O_55641_20130508092045</clientOrderStringId><senderUserName></senderUserName><handlingInstructions>MANUAL</handlingInstructions><side>BUY</side><orderQty>2000000.0000000000</orderQty><orderType>MARKET</orderType><timeInForce>DAY</timeInForce><transactionTime>2013-05-08 14:20:45.809 UTC</transactionTime><tradeDate>2013-05-08 05:00:00.0 UTC</tradeDate><settlementDate>2013-05-09 05:00:00.0 UTC</settlementDate><partyList/></com.clifton.fix.order.FixOrder>");

		FixAllocationInstruction allocation = executeTradeOrder(order, fixOrder, report1, report2);

		Assertions.assertEquals("Bloomberg (Parametric Minneapolis Fixed Income)", fixOrder.getFixDestinationName());
		Assertions.assertNull(allocation.getFixBeginString());

		Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("2003410.31"), allocation.getNetMoney()));
		Assertions.assertEquals(4, allocation.getPartyList().size());
		Assertions.assertEquals(PriceTypes.PERCENTAGE, allocation.getPriceType());
		Assertions.assertEquals(AllocationTypes.CALCULATED, allocation.getAllocationType());

		int index = 0;
		for (FixAllocationDetail detail : CollectionUtils.getIterable(allocation.getFixAllocationDetailList())) {
			if (index == 0) {
				Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("1001705.16"), detail.getAllocationNetMoney()));
			}
			else {
				Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("1001705.15"), detail.getAllocationNetMoney()));
			}
			Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("99.53515625"), detail.getAllocationPrice()));
			Assertions.assertEquals(0, MathUtils.compare(new BigDecimal("1000000"), detail.getAllocationShares()));
			index++;
		}
	}


	@Test
	public void testStockAllocationMessage() {
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.ofType("FUH0", InvestmentType.STOCKS).withCusip("111111111").withSedol("211111111").build())
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000");

		Trade futureTrade = tradeBuilder.withQuantityIntended(10)
				.build();
		Trade futureTrade2 = tradeBuilder.withId(43)
				.withQuantityIntended(7)
				.build();

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, futureTrade, futureTrade2);
		final FixExecutionReport executionReport = createFixExecutionReport(order, "FUH0 Index", new BigDecimal("10.32465"));
		FixAllocationInstruction allocation = executeTradeOrder(order, executionReport);

		Assertions.assertEquals("Goldman Sachs", allocation.getFixDestinationName());
		Assertions.assertNull(allocation.getFixBeginString());
		Assertions.assertNull(allocation.getFixSenderCompId());
		Assertions.assertNull(allocation.getFixSenderSubId()); // NOTE: for stock allocations to Goldman this will be overwritten by the server to "CLIFTON"
		Assertions.assertNull(allocation.getFixSenderLocId());

		Assertions.assertNull(allocation.getFixTargetCompId());
		Assertions.assertEquals("TKTS", allocation.getFixTargetSubId()); // NOTE: for stock allocations to Goldman this will be overwritten by the server to "SLKEXE"
		Assertions.assertNull(allocation.getFixTargetLocId());
		Assertions.assertNull(allocation.getFixSessionQualifier());

		Assertions.assertNull(allocation.getAllocationType());
	}


	@Test
	public void testAllocationMessage() {
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000");

		Trade futureTrade = tradeBuilder.withQuantityIntended(10)
				.build();
		futureTrade.getHoldingInvestmentAccount().setNumber("2736000");
		Trade futureTrade2 = tradeBuilder.withId(43)
				.withQuantityIntended(7)
				.build();
		futureTrade2.getHoldingInvestmentAccount().setNumber("1736000");

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, futureTrade, futureTrade2);
		final FixExecutionReport executionReport = createFixExecutionReport(order, "FUH0 Index", new BigDecimal("10.32465"));
		FixAllocationInstruction allocation = executeTradeOrder(order, executionReport);

		Assertions.assertEquals("Goldman Sachs", allocation.getFixDestinationName());
		Assertions.assertNull(allocation.getFixBeginString());
		Assertions.assertNull(allocation.getFixSenderCompId());
		Assertions.assertNull(allocation.getFixSenderSubId()); // NOTE: for stock allocations to Goldman this will be overwritten by the server to "CLIFTON"
		Assertions.assertNull(allocation.getFixSenderLocId());

		Assertions.assertNull(allocation.getFixTargetCompId());
		Assertions.assertEquals("TKTS", allocation.getFixTargetSubId()); // NOTE: for stock allocations to Goldman this will be overwritten by the server to "SLKEXE"
		Assertions.assertNull(allocation.getFixTargetLocId());
		Assertions.assertNull(allocation.getFixSessionQualifier());

		Assertions.assertEquals(AllocationTransactionTypes.NEW, allocation.getAllocationTransactionType());
		Assertions.assertEquals("FUH0", allocation.getSymbol());
		Assertions.assertEquals("FUH0 Index", allocation.getSecurityId());
		Assertions.assertEquals(SecurityIDSources.BID, allocation.getSecurityIDSource());
		Assertions.assertEquals(OrderSides.BUY, allocation.getSide());
		Assertions.assertEquals(new BigDecimal("10.32465").compareTo(allocation.getAveragePrice()), 0);
		Assertions.assertEquals(DateUtils.fromDate(DateUtils.toDate("10/17/2012", DateUtils.DATE_FORMAT_INPUT), DateUtils.FIX_DATE_FORMAT_INPUT),
				DateUtils.fromDate(allocation.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));

		FixAllocationDetail detail1 = allocation.getFixAllocationDetailList().get(0);
		Assertions.assertEquals("2736000", detail1.getAllocationAccount());
		Assertions.assertEquals(new BigDecimal("10").compareTo(detail1.getAllocationShares()), 0);

		FixAllocationDetail detail2 = allocation.getFixAllocationDetailList().get(1);
		Assertions.assertEquals("1736000", detail2.getAllocationAccount());
		Assertions.assertEquals(new BigDecimal("7").compareTo(detail2.getAllocationShares()), 0);
	}


	@Test
	public void testCitiAllocationMessage() {
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(FUH0)
				.withId(42)
				.buy()
				.withExpectedUnitPrice("10.32423")
				.withCommissionPerUnit("0.000000000");

		Trade futureTrade = tradeBuilder.withQuantityIntended(10)
				.withIssuingCompany(BusinessCompanyBuilder.createCitigroup().toBusinessCompany())
				.withExecutingBroker(BusinessCompanyBuilder.createCitigroup().toBusinessCompany())
				.build();
		futureTrade.getHoldingInvestmentAccount().setNumber("2736000");
		Trade futureTrade2 = tradeBuilder.withId(43)
				.withQuantityIntended(7)
				.withIssuingCompany(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany())
				.withExecutingBroker(BusinessCompanyBuilder.createCitigroup().toBusinessCompany())
				.build();
		futureTrade2.getHoldingInvestmentAccount().setNumber("1736000");

		final TradeOrder order = createOrder(TradeOrderStatuses.SENT, futureTrade, futureTrade2);
		final FixExecutionReport executionReport = createFixExecutionReport(order, "FUH0 Index", new BigDecimal("10.32465"));
		FixAllocationInstruction allocation = executeTradeOrder(order, executionReport);

		Assertions.assertEquals("Goldman Sachs", allocation.getFixDestinationName());
		Assertions.assertNull(allocation.getFixBeginString());
		Assertions.assertNull(allocation.getFixSenderCompId());
		Assertions.assertNull(allocation.getFixSenderSubId()); // NOTE: for stock allocations to Goldman this will be overwritten by the server to "CLIFTON"
		Assertions.assertNull(allocation.getFixSenderLocId());

		Assertions.assertNull(allocation.getFixTargetCompId());
		Assertions.assertEquals("TKTS", allocation.getFixTargetSubId()); // NOTE: for stock allocations to Goldman this will be overwritten by the server to "SLKEXE"
		Assertions.assertNull(allocation.getFixTargetLocId());
		Assertions.assertNull(allocation.getFixSessionQualifier());

		Assertions.assertEquals(AllocationTransactionTypes.NEW, allocation.getAllocationTransactionType());
		Assertions.assertEquals("FUH0", allocation.getSymbol());
		Assertions.assertEquals("FUH0 Index", allocation.getSecurityId());
		Assertions.assertEquals(SecurityIDSources.BID, allocation.getSecurityIDSource());
		Assertions.assertEquals(OrderSides.BUY, allocation.getSide());
		Assertions.assertEquals(new BigDecimal("10.32465").compareTo(allocation.getAveragePrice()), 0);
		Assertions.assertEquals(DateUtils.fromDate(DateUtils.toDate("10/17/2012", DateUtils.DATE_FORMAT_INPUT), DateUtils.FIX_DATE_FORMAT_INPUT),
				DateUtils.fromDate(allocation.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));

		FixAllocationDetail detail1 = allocation.getFixAllocationDetailList().get(0);
		Assertions.assertEquals("2736000", detail1.getAllocationAccount());
		Assertions.assertEquals(new BigDecimal("10").compareTo(detail1.getAllocationShares()), 0);
		Assertions.assertEquals(ProcessCodes.REGULAR, detail1.getProcessCode());
		Assertions.assertNotNull(detail1.getNestedPartyList());
		FixAllocationDetailNestedParty nestedParty1 = detail1.getNestedPartyList().get(0);
		Assertions.assertEquals("CG", nestedParty1.getNestedPartyId());
		Assertions.assertEquals(PartyRoles.CLEARING_FIRM, nestedParty1.getNestedPartyRole());
		Assertions.assertEquals(PartyIdSources.PROPRIETARY_CUSTOM_CODE, nestedParty1.getNestedPartyIDSource());

		FixAllocationDetail detail2 = allocation.getFixAllocationDetailList().get(1);
		Assertions.assertEquals("1736000", detail2.getAllocationAccount());
		Assertions.assertEquals(new BigDecimal("7").compareTo(detail2.getAllocationShares()), 0);
		Assertions.assertEquals(ProcessCodes.STEP_OUT, detail2.getProcessCode());
		Assertions.assertNotNull(detail1.getNestedPartyList());
		FixAllocationDetailNestedParty nestedParty2 = detail2.getNestedPartyList().get(0);
		Assertions.assertEquals("GS", nestedParty2.getNestedPartyId());
		Assertions.assertEquals(PartyRoles.CLEARING_FIRM, nestedParty2.getNestedPartyRole());
		Assertions.assertEquals(PartyIdSources.PROPRIETARY_CUSTOM_CODE, nestedParty2.getNestedPartyIDSource());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeOrder createOrder(TradeOrderStatuses status, Trade... trades) {
		if (trades == null) {
			return null;
		}
		Trade firstTrade = null;
		final List<Trade> tradeList = new ArrayList<>(trades.length);
		BigDecimal quantity = BigDecimal.ZERO;
		for (Trade trade : trades) {
			if (firstTrade == null) {
				firstTrade = trade;
			}
			tradeList.add(trade);
			quantity = MathUtils.add(quantity, trade.getQuantityIntended());
		}
		if (firstTrade == null) {
			return null;
		}

		TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration = new TradeDestinationMappingConfiguration();
		tradeDestinationMappingConfiguration.setId(1);

		final TradeOrder order = new TradeOrder();
		order.setTradeList(tradeList);
		order.setId(50);
		order.setExternalIdentifier(51);
		order.setQuantity(quantity);
		order.setQuantityFilled(quantity);
		order.setBuy(firstTrade.isBuy());
		order.setCreateDate(DateUtils.toDate("01/26/2013"));
		order.setTradeDate(DateUtils.toDate("10/17/2012"));
		order.setStatus(this.tradeOrderService.getTradeOrderStatusByStatuses(status));
		order.setDestinationMapping(this.tradeDestinationService.getTradeDestinationMappingByCommand(new TradeDestinationMappingSearchCommand(firstTrade)));
		order.setTraderUser(firstTrade.getTraderUser());
		order.setInvestmentSecurity(firstTrade.getInvestmentSecurity());
		order.getDestinationMapping().getMappingConfiguration().setFixSendBrokerPartyIdsOnAllocationMessages(true);
		return order;
	}


	private FixExecutionReport createFixExecutionReport(TradeOrder order, String securitySymbol, BigDecimal price) {
		FixExecutionReport executionMessage = new FixExecutionReport();
		executionMessage.setClientOrderStringId("50_20130126000000");
		executionMessage.setOrderId("42_test");
		executionMessage.setExecutionId("1");
		executionMessage.setExecutionTransactionType(ExecutionTransactionTypes.NEW);
		executionMessage.setExecutionType(ExecutionTypes.FILL);
		executionMessage.setOrderStatus(OrderStatuses.FILLED);
		executionMessage.setSide(order.isBuy() ? OrderSides.BUY : OrderSides.SELL);
		executionMessage.setLeavesQuantity(new BigDecimal("0"));
		executionMessage.setTimeInForceType(TimeInForceTypes.DAY);
		executionMessage.setSymbol(securitySymbol);
		executionMessage.setOrderQuantity(order.getQuantity());
		executionMessage.setCumulativeQuantity(order.getQuantity());
		executionMessage.setLastShares(order.getQuantity());
		executionMessage.setLastPrice(price);
		executionMessage.setAveragePrice(price);
		return executionMessage;
	}


	private FixAllocationInstruction executeTradeOrder(TradeOrder order, FixExecutionReport... executionReports) {
		FixOrder fixOrder = getMessageFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);
		return executeTradeOrder(order, fixOrder, executionReports);
	}


	private FixAllocationInstruction executeTradeOrder(TradeOrder order, FixOrder fixOrder, FixExecutionReport... executionReports) {
		TradeOrderAllocation tradeAllocation = new TradeOrderAllocation();
		tradeAllocation.setId(order.getId());
		tradeAllocation.setExternalIdentifier(order.getExternalIdentifier());
		tradeAllocation.setTradeOrderList(Collections.singletonList(order));
		Mockito.when(this.tradeOrderService.getTradeOrder(ArgumentMatchers.anyInt())).thenReturn(order);

		FixIdentifier id = new FixIdentifier();
		id.setId(order.getExternalIdentifier());
		fixOrder.setIdentifier(id);

		Mockito.when(this.tradeOrderFixOrderService.getTradeOrderFixOrder(ArgumentMatchers.anyInt(), ArgumentMatchers.anyBoolean())).thenReturn(fixOrder);
		Mockito.when(this.tradeOrderFixOrderService.getTradeOrderFixExecutionReportListForAllocation(ArgumentMatchers.anyInt(), ArgumentMatchers.anyBoolean())).thenReturn(CollectionUtils.createList(executionReports));
		Mockito.when(this.tradeOrderFixOrderService.getTradeOrderFixExecutionReportList(ArgumentMatchers.anyInt(), ArgumentMatchers.anyBoolean())).thenReturn(CollectionUtils.createList(executionReports));
		Mockito.when(this.tradeOrderFixOrderService.getTradeOrderFixExecutionReportLast(ArgumentMatchers.anyInt(), ArgumentMatchers.anyBoolean(), ArgumentMatchers.anyBoolean())).thenReturn(executionReports[executionReports.length - 1]);

		return getMessageFactory().create(tradeAllocation, FixEntityFactoryGenerationTypes.ALLOCATION);
	}


	public TradeOrderFixEntityFactory getMessageFactory() {
		return this.messageFactory;
	}


	public void setMessageFactory(TradeOrderFixEntityFactory messageFactory) {
		this.messageFactory = messageFactory;
	}


	private void mockBrokerMapping() {
		final MarketDataSourceCompanyMapping defaultBrokerMapping = new MarketDataSourceCompanyMapping();
		final Map<Integer, MarketDataSourceCompanyMapping> brokerMapping = new HashMap<>();

		brokerMapping.put(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany().getId(), createCompanyMapping(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany(), "GS"));
		brokerMapping.put(BusinessCompanyBuilder.createCitigroup().toBusinessCompany().getId(), createCompanyMapping(BusinessCompanyBuilder.createCitigroup().toBusinessCompany(), "CG"));

		Mockito.doAnswer(invocation -> {
			MarketDataSourceCompanyMappingSearchForm searchForm = (MarketDataSourceCompanyMappingSearchForm) invocation.getArguments()[0];
			if (brokerMapping.containsKey(searchForm.getBusinessCompanyId())) {
				return Collections.singletonList(brokerMapping.get(searchForm.getBusinessCompanyId()));
			}
			return Collections.singletonList(defaultBrokerMapping);
		}).when(this.marketDataSourceCompanyMappingService).getMarketDataSourceCompanyMappingList(ArgumentMatchers.any(MarketDataSourceCompanyMappingSearchForm.class));
	}


	private MarketDataSourceCompanyMapping createCompanyMapping(BusinessCompany businessCompany, String externalName) {
		MarketDataSourceCompanyMapping brokerMapping = new MarketDataSourceCompanyMapping();
		brokerMapping.setBusinessCompany(businessCompany);
		brokerMapping.setExternalCompanyName(externalName);
		return brokerMapping;
	}
}
