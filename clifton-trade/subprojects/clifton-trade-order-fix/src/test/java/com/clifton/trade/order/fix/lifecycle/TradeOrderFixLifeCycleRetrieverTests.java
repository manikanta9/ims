package com.clifton.trade.order.fix.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.lifecycle.BaseFixMessageLifeCycleRetrieverTests;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleTestExecutor;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class TradeOrderFixLifeCycleRetrieverTests extends BaseFixMessageLifeCycleRetrieverTests {

	@Resource
	private TradeOrderFixOrderService tradeOrderFixOrderService;

	@Resource
	private TradeOrderFixLifeCycleRetriever tradeOrderFixLifeCycleRetriever;

	@Resource
	private TradeOrderService tradeOrderService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected SystemLifeCycleEventRetriever getSystemLifeCycleRetriever() {
		return this.tradeOrderFixLifeCycleRetriever;
	}


	@Override
	protected List<IdentityObject> getApplyToEntityFalseList() {
		// Trade Order ID 2 has NO external identifier
		return CollectionUtils.createList(new Trade(), new TradeFill(), this.tradeOrderService.getTradeOrder(2));
	}


	@Override
	protected List<IdentityObject> getApplyToEntityTrueList() {
		return CollectionUtils.createList(this.tradeOrderService.getTradeOrder(1));
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_NoResults() {
		Mockito.when(this.tradeOrderFixOrderService.getTradeOrderFixMessageListForOrder(ArgumentMatchers.any())).thenReturn(null);
		SystemLifeCycleTestExecutor.forTableAndEntityAndRetriever("TradeOrder", this.tradeOrderService.getTradeOrder(1), getSystemLifeCycleRetriever())
				.execute();
	}


	@Test
	@Override
	public void testGetSystemLifeCycleEventListForEntity_Results() {
		Mockito.when(this.tradeOrderFixOrderService.getTradeOrderFixMessageListForOrder(ArgumentMatchers.any())).thenReturn(getTestFixMessageList());
		SystemLifeCycleTestExecutor.forTableAndEntityAndRetriever("TradeOrder", this.tradeOrderService.getTradeOrder(1), getSystemLifeCycleRetriever())
				.withExpectedResults("Source: N/A\tLink: TradeOrder [1]\tEvent Source: Fix Message\tAction: New Order Single\tDate: 2021-07-29 10:00:00\tUser: Unknown\tDetails: Sent New Order Single to Test Session",
						"Source: N/A\tLink: TradeOrder [1]\tEvent Source: Fix Message\tAction: Execution Report\tDate: 2021-07-29 10:00:00\tUser: Unknown\tDetails: Received Execution Report from Test Session",
						"Source: N/A\tLink: TradeOrder [1]\tEvent Source: Fix Message\tAction: New Order Single\tDate: 2021-07-29 10:02:30\tUser: Unknown\tDetails: Sent New Order Single to Test Session")
				.execute();
	}
}
