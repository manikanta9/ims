package com.clifton.trade.order.fix.util;

import com.clifton.core.util.StringUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixEntityImpl;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * A set of tests to test the proper behavior of the TradeOrderFixUtils static methods.
 *
 * @author davidi
 */
public class TradeOrderFixUtilsTests {

	@Test
	public void testSetEntityItems_with_fixTargetSubId_in_destination_include_on_behalf_of_false() {
		FixEntity entity = new FixEntityImpl();
		TradeDestinationMapping mapping = buildTradeDestinationMapping("Bloomberg", "subIdDest", "cnx", null, "fixDestinationName");
		entity = TradeOrderFixUtils.setEntityItems(entity, mapping, false);

		Assertions.assertNull(entity.getFixBeginString());
		Assertions.assertNull(entity.getFixSenderCompId());
		Assertions.assertNull(entity.getFixSenderSubId());
		Assertions.assertNull(entity.getFixSenderLocId());
		Assertions.assertNull(entity.getFixTargetCompId());
		Assertions.assertNull(entity.getFixTargetLocId());
		Assertions.assertNull(entity.getFixSessionQualifier());

		Assertions.assertEquals("subIdDest", entity.getFixTargetSubId());
		Assertions.assertEquals("fixDestinationName", entity.getFixDestinationName());
		Assertions.assertNull(entity.getFixOnBehalfOfCompID());
	}


	@Test
	public void testSetEntityItems_with_fixTargetSubId_in_destination_include_on_behalf_of_true() {
		FixEntity entity = new FixEntityImpl();
		TradeDestinationMapping mapping = buildTradeDestinationMapping("Bloomberg", "subIdDest", "cnx", null, "fixDestinationName");
		entity = TradeOrderFixUtils.setEntityItems(entity, mapping, true);

		Assertions.assertNull(entity.getFixBeginString());
		Assertions.assertNull(entity.getFixSenderCompId());
		Assertions.assertNull(entity.getFixSenderSubId());
		Assertions.assertNull(entity.getFixSenderLocId());
		Assertions.assertNull(entity.getFixTargetCompId());
		Assertions.assertNull(entity.getFixTargetLocId());
		Assertions.assertNull(entity.getFixSessionQualifier());

		Assertions.assertEquals("subIdDest", entity.getFixTargetSubId());
		Assertions.assertEquals("fixDestinationName", entity.getFixDestinationName());
		Assertions.assertEquals("cnx_FixOnBehalfOfCompId", entity.getFixOnBehalfOfCompID());
	}


	@Test
	public void testSetEntityItems_with_fixTargetSubId_in_connection() {
		FixEntity entity = new FixEntityImpl();
		TradeDestinationMapping mapping = buildTradeDestinationMapping("Bloomberg", null, "cnx", null, "fixDestinationName");
		entity = TradeOrderFixUtils.setEntityItems(entity, mapping, false);

		Assertions.assertNull(entity.getFixBeginString());
		Assertions.assertNull(entity.getFixSenderCompId());
		Assertions.assertNull(entity.getFixSenderSubId());
		Assertions.assertNull(entity.getFixSenderLocId());
		Assertions.assertNull(entity.getFixTargetCompId());
		Assertions.assertNull(entity.getFixTargetLocId());
		Assertions.assertNull(entity.getFixSessionQualifier());

		Assertions.assertNull(entity.getFixTargetSubId());
		Assertions.assertEquals("fixDestinationName", entity.getFixDestinationName());
		Assertions.assertNull(entity.getFixOnBehalfOfCompID());
	}


	@Test
	public void testSetEntityItems_with_null_allocation_connection() {
		FixEntity entity = new FixEntityImpl();
		TradeDestinationMapping mapping = buildTradeDestinationMapping("Bloomberg", "subIdDest", "cnx", null, "fixDestinationName");
		entity = TradeOrderFixUtils.setEntityItemsForAllocations(entity, mapping, true);

		Assertions.assertNull(entity.getFixBeginString());
		Assertions.assertNull(entity.getFixSenderCompId());
		Assertions.assertNull(entity.getFixSenderSubId());
		Assertions.assertNull(entity.getFixSenderLocId());
		Assertions.assertNull(entity.getFixTargetCompId());
		Assertions.assertNull(entity.getFixTargetLocId());
		Assertions.assertNull(entity.getFixSessionQualifier());

		Assertions.assertEquals("subIdDest", entity.getFixTargetSubId());
		Assertions.assertEquals("fixDestinationName", entity.getFixDestinationName());
		Assertions.assertEquals("cnx_FixOnBehalfOfCompId", entity.getFixOnBehalfOfCompID());
	}


	@Test
	public void testSetEntityItems_with_allocation_connection() {
		FixEntity entity = new FixEntityImpl();
		TradeDestinationMapping mapping = buildTradeDestinationMapping("Bloomberg", null, "cnx", "alloc", "fixDestinationName");
		entity = TradeOrderFixUtils.setEntityItemsForAllocations(entity, mapping, true);

		Assertions.assertNull(entity.getFixBeginString());
		Assertions.assertNull(entity.getFixSenderCompId());
		Assertions.assertNull(entity.getFixSenderSubId());
		Assertions.assertNull(entity.getFixSenderLocId());
		Assertions.assertNull(entity.getFixTargetCompId());
		Assertions.assertNull(entity.getFixTargetLocId());
		Assertions.assertNull(entity.getFixSessionQualifier());

		Assertions.assertNull(entity.getFixTargetSubId());
		Assertions.assertEquals("fixDestinationName", entity.getFixDestinationName());
		Assertions.assertEquals("alloc_FixOnBehalfOfCompId", entity.getFixOnBehalfOfCompID());
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private TradeDestinationMapping buildTradeDestinationMapping(String destinationName, String destinationFixTargetSubId, String destinationConnectionPrefixTag, String allocationConnectionPrefixTag, String fixDestinationName) {
		TradeDestinationMapping mapping = new TradeDestinationMapping();
		TradeDestination tradeDestination = new TradeDestination();
		tradeDestination.setFixTargetSubId(destinationFixTargetSubId);
		tradeDestination.setName(destinationName);
		mapping.setTradeDestination(tradeDestination);

		TradeDestinationConnection tradeDestinationConnection = buildTradeDestinationConnection(destinationConnectionPrefixTag, fixDestinationName);
		if (!StringUtils.isEmpty(allocationConnectionPrefixTag)) {
			TradeDestinationConnection allocationConnection = buildTradeDestinationConnection(allocationConnectionPrefixTag, fixDestinationName);
			tradeDestinationConnection.setAllocationConnection(allocationConnection);
		}
		mapping.getTradeDestination().setConnection(tradeDestinationConnection);

		return mapping;
	}


	private TradeDestinationConnection buildTradeDestinationConnection(String identifierPrefix, String fixDestinationName) {
		TradeDestinationConnection connection = new TradeDestinationConnection();


		connection.setName(identifierPrefix + "_Connection");
		connection.setFixOnBehalfOfCompId(identifierPrefix + "_FixOnBehalfOfCompId");

		if (!StringUtils.isEmpty(fixDestinationName)) {
			connection.setFixDestinationName(fixDestinationName);
		}

		return connection;
	}
}
